#include "InputManager.h"


//Scroll Distance
//MoveMouseDistance

void CU::InputManager::Update()
{
	SetMousePosDifferenceSinceLastUpdate(myMouseXPos, myMouseYPos);
	SetMousePosOnUpdate(myMouseXPos, myMouseYPos);

	//Pressed to Down
	for (std::map<char, KeyState>::iterator it = myKeyStates.begin(); it != myKeyStates.end(); it++)
	{
		if (it->second == KeyState::Pressed)
		{
			it->second = KeyState::Down;
		}
	}

	//Released to Up
	for (std::map<char, KeyState>::iterator it = myKeyStates.begin(); it != myKeyStates.end(); it++)
	{
		if (it->second == KeyState::Released)
		{
			it->second = KeyState::Up;
		}
	}
}

void CU::InputManager::InputHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	myHandleWindow = hWnd;
	switch (message)
	{
		case WM_MOUSEMOVE:
		{
			
			myMouseXPos = GET_X_LPARAM(lParam);
			myMouseYPos = GET_Y_LPARAM(lParam);

		}
		case WM_LBUTTONDOWN:
		{
			if (!GetIsKeyDown(wParam))
			{
				myKeyStates[wParam] = KeyState::Pressed;
			}
			break;
		}
		case WM_LBUTTONUP:
		{
			myKeyStates[wParam+1] = KeyState::Released;
			break;
		}
		case WM_RBUTTONDOWN:
		{
			if (!GetIsKeyDown(wParam))
			{
				myKeyStates[wParam] = KeyState::Pressed;
			}
			break;
		}
		case WM_RBUTTONUP:
		{
			myKeyStates[wParam+2] = KeyState::Released;
			break;
		}
		case WM_MBUTTONDOWN:
		{
			if (!GetIsKeyDown(wParam))
			{
				myKeyStates[wParam] = KeyState::Pressed;
			}
			break;
		}
		case WM_MBUTTONUP:
		{
			myKeyStates[wParam+16] = KeyState::Released;
			break;
		}
		case WM_KEYDOWN:
		{	
			if (!GetIsKeyDown(wParam))
			{
				myKeyStates[wParam] = KeyState::Pressed;
			}
			break;
		}
		case WM_KEYUP:
		{
			myKeyStates[wParam] = KeyState::Released;
			break;
		}
		case WM_MOUSEWHEEL:
		{
			myMouseWheelDeltaScroll = GET_WHEEL_DELTA_WPARAM(wParam);
		}
	}
}

bool CU::InputManager::GetIsKeyDown(char aKeyCode)
{
	if (myKeyStates.find(aKeyCode) != myKeyStates.end())
	{
		return myKeyStates[aKeyCode] <= KeyState::Down;
	}
	return false;
}

bool CU::InputManager::GetKeyJustPressed(char aKeyCode)
{
	if (myKeyStates.find(aKeyCode) != myKeyStates.end())
	{
		return myKeyStates[aKeyCode] == KeyState::Pressed;
	}
	return false;
}

bool CU::InputManager::GetKeyReleased(char aKeyCode)
{
	if (myKeyStates.find(aKeyCode) != myKeyStates.end())
	{
	return myKeyStates[aKeyCode] == KeyState::Released;
	}
	return false;
}

int CU::InputManager::GetMouseXPos()
{
	return myMouseXPos;
}

int CU::InputManager::GetMouseYPos()
{
	return myMouseYPos;
}

void CU::InputManager::SetMousePosOnUpdate(const int &aMouseXPos, const int &aMouseYPos)
{
	myMouseXPosOnUpdate = aMouseXPos;
	myMouseYPosOnUpdate = aMouseYPos;
}

void CU::InputManager::SetMousePosDifferenceSinceLastUpdate(const int &aMouseXPosThisUpdate, const int &aMouseYPosThisUpdate)
{
	if (aMouseXPosThisUpdate != 0 && aMouseYPosThisUpdate != 0)
	{
		myMouseXPosDifferenceSinceLastFrame = myMouseXPosOnUpdate - aMouseXPosThisUpdate;
		myMouseYPosDifferenceSinceLastFrame = myMouseYPosOnUpdate - aMouseYPosThisUpdate;
	}
}

int CU::InputManager::GetMouseXPosDifferenceSinceLastUpdate()
{
	return myMouseXPosDifferenceSinceLastFrame;
}

int CU::InputManager::GetMouseYPosDifferenceSinceLastUpdate()
{
	return myMouseYPosDifferenceSinceLastFrame;
}

int CU::InputManager::GetMouseWheelDelta()
{
	return myMouseWheelDeltaScroll;
}

void CU::InputManager::SetMousePosition(int aMouseXPos, int aMouseYPos)
{
	POINT myMousePos;
	myMousePos.x = aMouseXPos;
	myMousePos.y = aMouseYPos;
	ClientToScreen(myHandleWindow, &myMousePos);
	SetCursorPos(myMousePos.x, myMousePos.y);
}
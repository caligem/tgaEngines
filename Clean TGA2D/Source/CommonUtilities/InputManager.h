#pragma once
#include <windows.h>
#include <map>
#include <iostream>
#include <windowsx.h>

namespace CU {

	class InputManager
	{
	public:

		void InputHandler(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
		void Update();

		//keys
		bool GetIsKeyDown(char aKeyCode);
		bool GetKeyJustPressed(char aKeyCode);
		bool GetKeyReleased(char aKeyCode);

		//mouse
		int GetMouseXPos();
		int GetMouseYPos();
		void SetMousePosOnUpdate(const int &aMouseXPos, const int &aMouseYPos);
		void SetMousePosDifferenceSinceLastUpdate(const int &aMouseXPosOnLastUpdate, const int &aMouseYPosOnLastUpdate);
		int GetMouseXPosDifferenceSinceLastUpdate();
		int GetMouseYPosDifferenceSinceLastUpdate();
		int GetMouseWheelDelta();
		void SetMousePosition(int aMouseXPos, int aMouseYPos);

	private:
		enum class KeyState
		{
			Pressed,
			Down,
			Released,
			Up
		};

		int myMouseXPos = 0;
		int myMouseYPos = 0;
		int myMouseXPosOnUpdate = 0;
		int myMouseYPosOnUpdate = 0;
		int myMouseXPosDifferenceSinceLastFrame = 0;
		int myMouseYPosDifferenceSinceLastFrame = 0;
		int myMouseWheelDeltaScroll = 0;
		HWND myHandleWindow;

		std::map<char, KeyState> myKeyStates;
	};
}
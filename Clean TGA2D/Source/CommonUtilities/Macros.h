#pragma once
#include <vector>

#define MAX(a, b)	(((a) > (b)) ? (a) : (b))	


#define MIN(a, b)	(((a) < (b)) ? (a) : (b))	


#define SAFE_DELETE(apointer)					\
delete apointer;								\
apointer = nullptr;								

#define CYCLING_ERASE(avector, aindex)			\
avector[aindex] = avector[avector.size()-1];	\
avector.pop_back();
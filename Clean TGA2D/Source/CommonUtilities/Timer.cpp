#include "Timer.h"


CU::Timer::Timer()
	: myTotalTime(0.0)
	, myDeltaTime(0.0f)
{
	myDeltaTimeDuration = std::chrono::duration<float>::zero();
	myNewTime = std::chrono::high_resolution_clock::now();
	myOldTime = std::chrono::high_resolution_clock::now();
	myStartTime = std::chrono::high_resolution_clock::now();
}

void CU::Timer::Update()
{
	if (!myZeroValue)
	{
		myNewTime = std::chrono::high_resolution_clock::now();
		myDeltaTimeDuration = std::chrono::duration_cast<std::chrono::duration<float>> (myNewTime - myOldTime);
		myDeltaTime = myDeltaTimeDuration.count();
		myTotalTimeDuration = std::chrono::duration_cast<std::chrono::duration<double>>(myNewTime - myStartTime);
		myTotalTime = myTotalTimeDuration.count();
		myOldTime = myNewTime;
	}
	else
	{
		myTotalTime = myDeltaTime;
		myZeroValue = false;
	}
}


float CU::Timer::GetDeltaTime() const
{
	return myDeltaTime;
}

double CU::Timer::GetTotalTime() const
{
	return myTotalTime;
}
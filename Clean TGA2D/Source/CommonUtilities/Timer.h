#pragma once
#include <iostream>
#include <chrono>
#include <ctime>
#include <ratio>

namespace CU {

	class Timer
	{
	public:
		Timer();
		Timer(const Timer& aTimer) = delete;
		Timer& operator=(const Timer& aTimer) = delete;

		void Update();

		float GetDeltaTime() const;
		double GetTotalTime() const;

	private:
		std::chrono::high_resolution_clock::time_point myNewTime;
		std::chrono::high_resolution_clock::time_point myOldTime;
		std::chrono::duration<float> myDeltaTimeDuration;
		std::chrono::duration<double> myTotalTimeDuration;
		std::chrono::high_resolution_clock::time_point myStartTime;

		double myTotalTime;
		float myDeltaTime;
		bool myZeroValue = true;
	};
}
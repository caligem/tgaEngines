#pragma once
#include <math.h>
namespace CommonUtilities
{

	template <class T>
	class Vector4
	{
	public:
		T x;
		T y;
		T z;
		T w;

		Vector4<T>();

		Vector4<T>(const T& aX, const T& aY, const T& aZ, const T& aW);

		Vector4<T>(const Vector4<T>& aVector) = default;

		Vector4<T>& operator=(const Vector4<T>& aVector4) = default;

		~Vector4<T>() = default;

		T Length2() const;

		T Length() const;

		Vector4<T> GetNormalized() const;

		void Normalize();

		T Dot(const Vector4<T>& aVector) const;

	};
	typedef Vector4<float> Vector4f;

	template <class T> Vector4<T>::Vector4()
	{
		x = 0;
		y = 0;
		z = 0;
		w = 0;
	}

	template <class T> Vector4<T>::Vector4(const T& aX, const T& aY, const T& aZ, const T& aW)
	{
		x = aX;
		y = aY;
		z = aZ;
		w = aW;
	}

	template <class T> T Vector4<T>::Length2() const
	{
		return (x*x) + (y*y) + (z*z) + (w*w);
	}

	template <class T> T Vector4<T>::Length() const
	{
		return sqrt((x*x) + (y*y) + (z*z) + (w*w));
	}

	template <class T> Vector4<T> Vector4<T>::GetNormalized() const
	{
		return (*this) * (1 / Length());
	}

	template <class T> void Vector4<T>::Normalize()
	{
		(*this) *= (1 / Length());
	}

	template <class T> T Vector4<T>::Dot(const Vector4<T>& aVector) const
	{
		return (x * aVector.x) + (y * aVector.y) + (z * aVector.z) + (w * aVector.w);
	}

	template <class T> Vector4<T> operator+(const Vector4<T>& aVector0, const Vector4<T>& aVector1)
	{
		Vector4<T> countWith = aVector0;
		countWith.x += aVector1.x;
		countWith.y += aVector1.y;
		countWith.z += aVector1.z;
		countWith.w += aVector1.w;
		return countWith;
	}

	template <class T> Vector4<T> operator-(const Vector4<T>& aVector0, const Vector4<T>& aVector1)
	{
		Vector4<T> countWith = aVector0;
		countWith.x -= aVector1.x;
		countWith.y -= aVector1.y;
		countWith.z -= aVector1.z;
		countWith.w -= aVector1.w;
		return countWith;
	}

	template <class T> Vector4<T> operator*(const Vector4<T>& aVector, const T& aScalar)
	{
		Vector4<T> countWith = aVector;
		countWith.x *= aScalar;
		countWith.y *= aScalar;
		countWith.z *= aScalar;
		countWith.w *= aScalar;
		return countWith;
	}

	template <class T> Vector4<T> operator*(const T& aScalar, const Vector4<T>& aVector)
	{
		Vector4<T> countWith = aVector;
		countWith.x *= aScalar;
		countWith.y *= aScalar;
		countWith.z *= aScalar;
		countWith.w *= aScalar;
		return countWith;
	}

	template <class T> Vector4<T> operator/(const Vector4<T>& aVector, const T& aScalar)
	{
		Vector4<T> countWith = aVector;
		countWith.x /= aScalar;
		countWith.y /= aScalar;
		countWith.z /= aScalar;
		countWith.w /= aScalar;
		return countWith;
	}

	template <class T> Vector4<T> operator+=(Vector4<T>& aVector0, const Vector4<T>& aVector1)
	{
		aVector0.x += aVector1.x;
		aVector0.y += aVector1.y;
		aVector0.z += aVector1.z;
		aVector0.w += aVector1.w;
		return aVector0;
	}

	template <class T> Vector4<T> operator-=(Vector4<T>& aVector0, const Vector4<T>& aVector1)
	{
		aVector0.x -= aVector1.x;
		aVector0.y -= aVector1.y;
		aVector0.z -= aVector1.z;
		aVector0.w -= aVector1.w;
		return aVector0;
	}

	template <class T> Vector4<T> operator*=(Vector4<T>& aVector, const T& aScalar)
	{
		aVector.x *= aScalar;
		aVector.y *= aScalar;
		aVector.z *= aScalar;
		aVector.w *= aScalar;
		return aVector;
	}

	template <class T> Vector4<T> operator/=(Vector4<T>& aVector, const T& aScalar)
	{
		aVector.x /= aScalar;
		aVector.y /= aScalar;
		aVector.z /= aScalar;
		aVector.w /= aScalar;
		return aVector;
	}

}

#pragma once

#include <Vector.h>
#include "GameObject.h"

enum class ColliderTag
{
	//Fill List As Tags Arise
	None,
	Player,
	Enemy,
	Tile,
	MovingTile,
	Grabbable,
	Pushable,
	Pullable,
	Climbable,
	Hook,
	Trap,
	Checkpoint,
	Door,
	HookableTile,
	Trigger,
	GroundChecker,
	Collectible,
	BouncingPlatform,
	Projectile,
	CrewMember,
	HookPickup
};


class Collider
{
public:
		struct Hit
		{
			CommonUtilities::Vector2f normal;
			CommonUtilities::Vector2f delta;
		};

		Collider();
		virtual ~Collider();

		static void UpdateAllColliders(const std::vector<GameObject*> &aGrowingArrayOfGameObjects);
		static void UpdateAgainstList(GameObject &aGO, std::vector<GameObject*> &aGrowingArrayOfGameObjects);
		static void DrawAllColliders(const std::vector<GameObject*> &aGrowingArrayOfGameObjects);
		virtual void Draw();
		void SetTag(ColliderTag aTag);
		ColliderTag GetTag() const;

		void SetOwner(GameObject* aOwner);
		GameObject* GetOwner();
		virtual bool IsCollidedWith(Collider *aCol) = 0;
		virtual bool IsCollidedWith(BoxCollider *aBoxCol, const CommonUtilities::Vector2f &aGOPos) = 0;
		virtual bool IsCollidedWith(CircleCollider *aCircleCol, const CommonUtilities::Vector2f &aGOPos) = 0;
		virtual bool IsCollidedWith(LineCollider *aLineCol, const CommonUtilities::Vector2f &aGOPos) = 0;
		virtual void CheckIfCollided(const std::vector<GameObject*> &aGrowingArrayOfGameObjects);

		virtual Hit CollisionDirectionWith(Collider *aCol);
		virtual Hit CollisionDirectionWith(BoxCollider *aCol);
		virtual CommonUtilities::Vector2f RelativePoint(const CommonUtilities::Vector2f &aPoint) = 0;
		virtual CommonUtilities::Vector2f &GetOffset() = 0;
		void SetOffset(const CommonUtilities::Vector2f& aOffset);

		const bool GetIsSolid() const;
		void SetIsSolid(const bool aValueToSet);
	protected:
		ColliderTag myTag;
		bool PointInBox(CommonUtilities::Vector2f aBox1, float aBox1Width, float aBox1Height, CommonUtilities::Vector2f aPoint);
		bool CircleWithBoxCollision(CommonUtilities::Vector2f  aCircleCenter, float aCircleRadius, CommonUtilities::Vector2f  aBox2, float aBox2Width, float aBox2Height);
		bool CircleWithCircleCollision(CommonUtilities::Vector2f aCircle1, float aCircleRadius1, CommonUtilities::Vector2f aCircle2, float aCircleRadius2);
		bool CircleWithLineCollision(CommonUtilities::Vector2f aCircle, float aCircleRadius, CommonUtilities::Vector2f aLineStart, CommonUtilities::Vector2f aLineEnd);
		bool BoxWithBoxCollision(CommonUtilities::Vector2f aBox1, float aBox1Width, float aBox1Height, CommonUtilities::Vector2f aBox2, float aBox2Width, float aBox2Height);
		bool LineWithLineCollision(CommonUtilities::Vector2f aLineStart1, CommonUtilities::Vector2f aLineEnd1, CommonUtilities::Vector2f aLineStart2, CommonUtilities::Vector2f aLineEnd2);
		bool BoxWithLineCollision(CommonUtilities::Vector2f aBox, float aBoxWidth, float aBoxHeight, CommonUtilities::Vector2f aLineStart, CommonUtilities::Vector2f aLineEnd);
		GameObject *myGameObject;
		CommonUtilities::Vector2f myOffset;
		bool myIsSolid;
		void Update();

	private:
		enum class CollisionState
		{
			Fresh,
			Enter,
			Stay,
			Leave
		};
		struct Collision
		{
			Collider* myOtherCol = nullptr;
			CollisionState myColStateWithOther = CollisionState::Fresh;
			bool myHitThisFrame = false;
		};
		std::vector<Collision> myCollisions;
		void AddToHit(Collider* aOther);
	};
};


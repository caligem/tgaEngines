#pragma once
#include "Collider.h"
#include "Vector.h"
#include "GameObject.h"
#include <vector>

typedef CommonUtilities::Vector2f vector2f;

class GameObject
{
public:
	GameObject();
	virtual ~GameObject();

	void InitGO(const CommonUtilities::Vector2f &aPosition, const char* aPath);
	static void UpdateAllGameObjects(std::vector<GameObject*> & aGrowingArrayOfGameObjects);
	virtual void Reset();
	void AttachCollider(BoxCollider &aBoxColToAttach);
	void AttachCollider(CircleCollider &aCircleColToAttach);
	void AttachCollider(LineCollider &aCircleColToAttach);
	virtual void FillCollisionBuffer(std::vector<GameObject*> &aBufferToAddTo);
	void SetPosition(const CommonUtilities::Vector2f &aPosToSet);
	void SetRotation(const float aRotation);
	float GetRotation();

//	virtual void FillRenderBuffer(std::vector<Drawable*> &aRenderBuffer);
	const CommonUtilities::Vector2f & GetPosition() const;

//	Drawable& AccessDrawable();
	Collider *GetCollider();
	bool GetShouldRemove();
	void SetShouldRemove(const bool aShouldRemove);
	virtual void OnTriggerd() {};

	virtual void OnUpdate();

	virtual void SetOriginalPosition();

protected:
	virtual void OnCollisionEnter(Collider  &other);
	virtual void OnCollisionStay(Collider  &other);
	virtual void OnCollisionLeave(Collider  &other);

	Collider *myCollider;
	Drawable *myDrawable;
	CommonUtilities::Vector2f myPosition;
	CommonUtilities::Vector2f myOrginalPosition;
	float myZ;
	float myRotation;
	bool myShouldRemove;
private:
};


extern vec2 mousePos;
extern vec2 furthestPoint;
extern vec2 darknessSize;
extern vec2 resolution;
extern Image darknessMap;
//extern vec2 fireFlyPos;

float y1(float x)
{
	return 2 - x - x*x;
}

float y2(float x)
{
	return sin(x*6+2) + 1.5;
}

float smin(float a, float b, float k)
{
	a = pow(a, k);
	b = pow(b, k);
	return pow((a*b)/(a+b), 1.f/k);
}

float GetDistance(vec2 point1, vec2 point2)
{
	return sqrt(pow((point1.x - point2.x), 2) + pow((point1.y - point2.y), 2));
}

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
	float distanceToFurthestPoint = length(mousePos - furthestPoint);
	float maxDistance = 1920; //length(vec2(960, 1080) - furthestPoint);
	float pixelDistanceToMouse = length(mousePos - screen_coords);
	float normalizedDistanceToFurthestPoint = clamp(distanceToFurthestPoint / maxDistance, 0.1, 1.0);
	float normalizedPixelDistance = clamp(pixelDistanceToMouse / (500 * (1.0 - normalizedDistanceToFurthestPoint)), 0.0, 1.0);

	vec4 pixelColor = Texel(texture, texture_coords ); //This is the current pixel color
	//vec4 defaultPixelColor = Texel(texture, texture_coords );
	float x;

	float k = 5.0f;

	float light =
		(10 - (1.0 - normalizedDistanceToFurthestPoint) * 8) *
		(0.05 + Texel(darknessMap, (screen_coords/resolution)*(resolution/darknessSize)).a) *
		smin(y1(normalizedPixelDistance), y2(normalizedPixelDistance), k)+1;


	light *= 1-Texel(darknessMap, (screen_coords/resolution)*(resolution/darknessSize)).a;

	vec3 lightColor = vec3(1, 1, pow(1-(smin(y1(normalizedPixelDistance), y2(normalizedPixelDistance), k))/10, 4));

	pixelColor.rgb *= light * lightColor;

	//pixelColor = clamp(pixelColor, defaultPixelColor, defaultPixelColor);
/*	if (fireFlyPos != vec2(0,0))
	{
		float flyDistance = GetDistance(fireFlyPos, screen_coords);

		if (flyDistance <= 100)
		{
			//if(pixelColor.r )
			pixelColor.r = mix(0, 0, (100-flyDistance)/100);
			pixelColor.g = mix(0, pixelColor.g, (100-flyDistance)/100);
			pixelColor.b = mix(0, pixelColor.b * 0, (100-flyDistance)/100);
			pixelColor.a = mix(pixelColor.a, 1, (100-flyDistance)/100);
		}
	}
*/
  	return pixelColor;
}

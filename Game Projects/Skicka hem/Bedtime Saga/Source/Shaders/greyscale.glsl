vec4 effect( vec4 col, Image texture, vec2 texturePos, vec2 screenPos )
{
	vec4 sourceCol = texture2D( texture, texturePos );
	number greyCol = (sourceCol.x * 0.21 + sourceCol.y * 0.71 + sourceCol.z * 0.07)*0.5;

    vec4 resultCol = vec4( greyCol, greyCol, greyCol, sourceCol.w*0.7);
    return resultCol;
}
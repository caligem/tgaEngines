//PI definitions
float PI = radians(180.0);
float PI2 = radians(360.0);

//External parameters
extern vec2 characterPos = vec2(500, 500);
extern vec2 flashlightOffset = vec2(0, 0);
extern float coneAngle = 0.75;
extern float rotation = 0.0;
extern int lightReach = 300;

float wrapRadAngle(float value)
{	
	while (value >= PI2)
	{
		value -= PI2;
	}
	while (value < 0)
	{
		value += PI2;
	}
	return value;
}

float angleDif(float a, float b)
{
	float dif = wrapRadAngle( mod((b - a), PI2));

	if (dif > PI)
	{
		dif = PI2 - dif;
	}

	return dif;
}

vec4 effect( vec4 color, Image texture, vec2 texturePos, vec2 screenPos )
{
	vec4 sourceCol = texture2D( texture, texturePos );
	vec4 shadedCol = sourceCol;

	//Distance to pixel
	vec2 pixelPos = screenPos + texturePos;
	vec2 flashlightOrigin = characterPos + flashlightOffset*0.00001;
	float lightDistance = distance(pixelPos, flashlightOrigin);

	//Angle to pixel
	float deltaX = flashlightOrigin.x - pixelPos.x;
	float deltaY = flashlightOrigin.y - pixelPos.y;
	float angleBetween = atan(deltaY, deltaX) + PI;

	//shadedCol.x = clamp(angleDif(angleBetween, rotation), 0, 1); //Debug angle
	if (angleDif(angleBetween, rotation) < coneAngle)
	{
		shadedCol.w = clamp(lightDistance/lightReach, 0, shadedCol.w);
	}
	else
	{
		shadedCol.w = clamp(lightDistance/lightReach*3, 0, shadedCol.w);
	}

	return shadedCol;
}
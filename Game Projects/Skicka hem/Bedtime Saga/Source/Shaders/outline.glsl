vec4 resultCol;
extern vec3 outlineColor = vec3(0.0f, 1.0f, 1.0f);
extern vec2 size;
extern int samples = 3; // pixels per axis; higher = bigger glow, worse performance
extern float quality = 3; // lower = smaller glow, better quality


vec4 effect( vec4 col, Image texture, vec2 texturePos, vec2 screenPos )
{
	number sourceAlpha = 4*texture2D( texture, texturePos ).a;
	number alpha = 0;
	int diff = (samples - 1) / 2;
  	vec2 sizeFactor = vec2(1) / size * quality;

	for (int x = -diff; x <= diff; x++)
  	{
		for (int y = -diff; y <= diff; y++)
    	{
    		vec2 offset = vec2(x, y) * sizeFactor;
    		alpha += texture2D( texture, texturePos + offset).a;
    	}
    }
    alpha = alpha/(samples);
    resultCol = vec4( outlineColor.x, outlineColor.y, outlineColor.z, alpha);
    return resultCol;
}
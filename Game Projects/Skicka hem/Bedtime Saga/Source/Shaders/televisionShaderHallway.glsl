extern vec3 shaderColor = vec3(1);
extern float systemTime = 0;
extern bool tvOn = true;
extern Image tvLight;

float hash11(float p)
{
	vec2 p2 = fract(vec2(p * 5.3983, p * 5.4427));
  p2 += dot(p2.yx, p2.xy + vec2(21.5351, 14.3137));
	return fract(p2.x * p2.y * 95.4337);
}

float noise(float x)
{
    float i=floor(x);
    float f=fract(x);
    f=f*f*(3.0-2.0*f);
    float y=3.0*mix(hash11(i), hash11(i+1.), f);
    return y;
}

vec2 RealPos(vec2 texturePos)
{
  texturePos *= vec2(0.9375, 0.52734375);
  return texturePos;
}

vec4 GreyBase(vec4 color)
{
  if(color.a == 0)
  {
    color.rgb = vec3(0.5);
  }
  color.a = 1;
  return color;
}

float OverlayChannel(float bgChannel, float olChannel)
{
  if (bgChannel < 0.5)
  {
    return (2 * bgChannel * olChannel);
  }
  else
  {
    return (1.0 - 2.0 * (1.0 - bgChannel) * (1.0 - olChannel));
  }
}

vec4 Overlay(vec4 backgroundColor, vec4 overlayColor)
{
  backgroundColor.r = OverlayChannel(backgroundColor.r, overlayColor.r);
  backgroundColor.g = OverlayChannel(backgroundColor.g, overlayColor.g);
  backgroundColor.b = OverlayChannel(backgroundColor.b, overlayColor.b);
  backgroundColor.a = 1;
  return backgroundColor;
}

float totalColorDifference(vec3 color1, vec3 color2)
{
  vec3 difference = (color1-color2);
  float totalDifference = (abs(difference.r) + abs(difference.g) + abs(difference.b));
  return totalDifference/3;
}

vec4 effect(vec4 color, Image texture, vec2 texturePos, vec2 screenPos)
{
  vec4 backgroundColor = Texel(texture, texturePos);
  //backgroundColor = clamp(backgroundColor, vec4(0.49), vec4(0.51)); //For debugging lightmap
  vec4 lightColor = Texel(tvLight, RealPos(texturePos));

  if(tvOn == true)
  {   
    float light1Strength = totalColorDifference(lightColor.rgb, vec3(0.5));
    lightColor.rgb = mix(lightColor.rgb, shaderColor, light1Strength); 

    float lightStrength = abs(sin(noise(systemTime))/2) + 0.2;
    lightColor.rgb = mix(vec3(0.5), lightColor.rgb, lightStrength);
  }
  else
  {
    lightColor.rgb = vec3(0.5);
  }

  backgroundColor = Overlay(backgroundColor, lightColor);
  return backgroundColor;
}

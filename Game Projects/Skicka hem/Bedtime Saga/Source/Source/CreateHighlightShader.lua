
function Shader_load()
  glowShader = love.graphics.newShader[[
 	extern vec2 size;

	vec2 clamp(vec2 pos) {
		number x = pos.x;
		number y = pos.y;
		if (x < 0.0) x = 0.0;
		if (y < 0.0) y = 0.0;
		if (x > 1) x = 1;
		if (y > 1) y = 1;
		return vec2(x, y);
	}

	vec4 effect ( vec4 color, Image texture, vec2 tc, vec2 screen_coords ) {
		number distance = 1;
		number num = 0.0;
		
		vec4 clr;
		clr = Texel(texture, tc);
		if (clr.a > 0.003921568627451 * 100) {
			return clr;
		}
		
		number maxLength = length(vec2(2.0/size.x,2.0/size.y));
		
		for (number x = -3 ; x <= 3; x++)
		for (number y = -3 ; y <= 3; y++) {
			vec2 disp = vec2(x/size.x,y/size.y);
			clr = Texel(texture, clamp(tc+disp));
			if (clr.a > 0.0) {
				num = num + 0.5;
				number dist = length(disp) / maxLength;
				if (dist < distance) {
					distance = dist;
				}
			}
		}
		
		color.r = 0.003921568627451 * 150;
		color.g = 0.003921568627451 * 249;
		color.b = 0.003921568627451 * 255;
		return vec4(color.r,color.g,color.b,1.0-distance);
	}
	]]
	msgs = glowShader:getWarnings()
	glowShader:send('size',{300,235})
end
 
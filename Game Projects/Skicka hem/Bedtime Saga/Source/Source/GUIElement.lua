local function GUIElement_OnClick(aGUIElement)
	if (aGUIElement.myName == "Floor1_Livingroom_BallonPuzzle_Button") then
		pressingPressureMeterButton = true
	end

	if (aGUIElement.myName == "Flashlight_Button_Unlit" or aGUIElement.myName == "Flashlight_Button_Lit") then
		if(showFlashlight == true) then
			GUIManager_ToggleFlashlight()
		end
	end
end

local function GUIElement_Draw(aGUIElement)
	if (aGUIElement.myIsHidden == false) then
		if (aGUIElement.myShader ~= nil) then
			aGUIElement.myShader:send( "size",{aGUIElement.myWidth,aGUIElement.myHeight} )
			PushShader(aGUIElement.myShader)
			love.graphics.draw(aGUIElement.mySprite, aGUIElement.myPosX, aGUIElement.myPosY, aGUIElement.myRotation, aGUIElement.myScaleX, aGUIElement.myScaleY,
						   aGUIElement.myOriginX, aGUIElement.myOriginY)
			PopShader()
		else
			--print(aGUIElement.myShader)
			love.graphics.draw(aGUIElement.mySprite, aGUIElement.myPosX, aGUIElement.myPosY, aGUIElement.myRotation, aGUIElement.myScaleX, aGUIElement.myScaleY,
							   aGUIElement.myOriginX, aGUIElement.myOriginY)
		end
	end
end

local function GUIElement_SetPos(aGUIElement, aPosX, aPosY)
	aGUIElement.myPosX = aPosX
	aGUIElement.myPosY = aPosY
end

local function GUIElement_GetPos(aGUIElement)
	return aGUIElement.myPosX, aGUIElement.myPosY
end

local function GUIElement_GetSize(aGUIElement)
	return aGUIElement.myWidth, aGUIElement.myHeight
end

local function GUIElement_SetRotation(aGUIElement, aRotation)
	aGUIElement.myRotation = aRotation
end

local function GUIElement_SetScale(aGUIElement, aScale)
	aGUIElement.myScaleX = aScale
	aGUIElement.myScaleY = aScale
end

local function GUIElement_SetOrigin(aGUIElement, aOriginX, aOriginY)
	aGUIElement.myOriginX = aOriginX
	aGUIElement.myOriginY = aOriginY
end

local function GUIElement_SetShader(aGUIElement, aShader) 
	aGUIElement.myShader = aShader
end

local function GUIElement_Hide(aGUIElement)
	aGUIElement.myIsHidden = true
end

local function GUIElement_Show(aGUIElement)
	aGUIElement.myIsHidden = false
end

local function CreateGUIElement(aName, aSpritePath, aPosX, aPosY)
	local newGUIElement = {}
	newGUIElement.myName = aName;
	newGUIElement.mySpritePath = aSpritePath
	newGUIElement.mySprite = LoadSprite(aSpritePath .. aName .. ".png")
	newGUIElement.myPosX = aPosX
	newGUIElement.myPosY = aPosY
	newGUIElement.myWidth = newGUIElement.mySprite:getWidth()
	newGUIElement.myHeight = newGUIElement.mySprite:getHeight()
	newGUIElement.myRotation = 0
	newGUIElement.myScaleX = 1
	newGUIElement.myScaleY = 1
	newGUIElement.myOriginX = newGUIElement.myWidth /2
	newGUIElement.myOriginY = newGUIElement.myHeight /2
	newGUIElement.myColor = {255, 255, 255, 255}
	newGUIElement.myShader = nil
	newGUIElement.myIsHidden = false

	newGUIElement.Draw = GUIElement_Draw
	newGUIElement.GetSize = GUIElement_GetSize
	newGUIElement.SetPos = GUIElement_SetPos
	newGUIElement.GetPos = GUIElement_GetPos
	newGUIElement.SetRotation = GUIElement_SetRotation
	newGUIElement.SetScale = GUIElement_SetScale
	newGUIElement.SetOrigin = GUIElement_SetOrigin
	newGUIElement.SetShader = GUIElement_SetShader
	newGUIElement.Hide = GUIElement_Hide
	newGUIElement.Show = GUIElement_Show

	return newGUIElement
end

local function CreateGUISpriteElement(aName, aSprite, aPosX, aPosY)
	local newGUIElement = {}
	newGUIElement.myName = aName
	newGUIElement.mySprite = aSprite
	newGUIElement.myPosX = aPosX
	newGUIElement.myPosY = aPosY
	newGUIElement.myWidth = newGUIElement.mySprite:getWidth()
	newGUIElement.myHeight = newGUIElement.mySprite:getHeight()
	newGUIElement.myRotation = 0
	newGUIElement.myScaleX = 1
	newGUIElement.myScaleY = 1
	newGUIElement.myOriginX = newGUIElement.myWidth /2
	newGUIElement.myOriginY = newGUIElement.myHeight /2
	newGUIElement.myColor = {255, 255, 255, 255}
	newGUIElement.myIsHidden = false

	newGUIElement.Draw = GUIElement_Draw
	newGUIElement.GetSize = GUIElement_GetSize
	newGUIElement.GetPos = GUIElement_GetPos
	newGUIElement.SetRotation = GUIElement_SetRotation
	newGUIElement.SetScale = GUIElement_SetScale
	newGUIElement.SetOrigin = GUIElement_SetOrigin
	newGUIElement.SetShader = GUIElement_SetShader
	newGUIElement.Hide = GUIElement_Hide
	newGUIElement.Show = GUIElement_Show
	return newGUIElement
end

function CreateButton(aName, aSpritePath, aPosX, aPosY)
	local newButton = CreateGUIElement(aName, aSpritePath, aPosX, aPosY)
	newButton.myType = "button"
	newButton.IsHovered = GUIElement_IsHovered
	newButton.OnClick = GUIElement_OnClick

	return newButton
end

function CreateButtonWithCallback(aName, aSpritePath, aPosX, aPosY, aCallbackFunction)
	local newButton = CreateGUIElement(aName, aSpritePath, aPosX, aPosY)
	newButton.IsHovered = GUIElement_IsHovered
	newButton.OnClick = aCallbackFunction
	newButton.myType = "button"

	return newButton
end

function CreateSpriteButtonWithCallback(aName, aSprite, aPosX, aPosY, aCallbackFunction)
	local newButton = CreateGUISpriteElement(aName, aSprite, aPosX, aPosY)
	newButton.IsHovered = GUIElement_IsHovered
	newButton.OnClick = aCallbackFunction
	newButton.myType = "button"

	return newButton
end

function CreatePicture(aName, aSpritePath, aPosX, aPosY)
	local newPicture = CreateGUIElement(aName, aSpritePath, aPosX, aPosY)
	newPicture.myType = "picture"
	return newPicture
end

function GetHoveredGUIElement()
	local allCurrentGUIElements = {}

	if (GetTableSize(scenes[currentScene].GUIElements) > 0) then
		for i, sceneGUIElement in ipairs(scenes[currentScene].GUIElements) do
			table.insert(allCurrentGUIElements, sceneGUIElement)
		end
	end

	if (GetTableSize(globalConstantGUIElements) > 0) then
		for i2, constantGUIElement in ipairs(globalConstantGUIElements) do
			if (constantGUIElement.myName == "Flashlight_Button_Unlit") then
				if(flashlightButton.myIsGrey == false) then
					table.insert(allCurrentGUIElements, constantGUIElement)
				end
			else
				table.insert(allCurrentGUIElements, constantGUIElement)
			end
		end
	end

	for i, GUIElement in ipairs(allCurrentGUIElements) do
		if (GUIElement.IsHovered ~= nil) then
			if (GUIElement:IsHovered(true)) then
				if (GUIElement.myIsHidden == false) then
					return GUIElement
				end
			end
		end
	end

	return nil
end

--function CreateInteractableText(aText, aPosX, aPosY, aColor) then
--	local CreateInteractableText = CreateGUIElement
--end

function GUIElement_IsHovered(aGUIElement, aUseAlpha)
    local mouseX, mouseY = GetMousePosition()
    if(IsPositionInBounds(mouseX, mouseY, aGUIElement.myPosX, aGUIElement.myPosY, aGUIElement.myWidth, aGUIElement.myHeight, true) == true) then
        local spriteData = aGUIElement.mySprite:getData()
        local pixelPosX, pixelPosY = ToObjectLocalPosition(aGUIElement, mouseX, mouseY)
        local _, _, _, pixelAlphaValue = spriteData:getPixel(round(pixelPosX), round(pixelPosY))
        if (pixelAlphaValue <= 0) and aUseAlpha then
            return false
        else
            return true
        end
    else 
        return false
    end
end
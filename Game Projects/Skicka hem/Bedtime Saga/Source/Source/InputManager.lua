globalCanPlayClickSound = true
local mouseOffsetX, mouseOffsetY = 0, 0
local grabbedObjectLastSlot = nil

function ResetInputManager()
	globalCanPlayClickSound = true
	mouseOffsetX, mouseOffsetY = 0, 0
	grabbedObjectLastSlot = nil
end

function GetGrabbedObjectPosition()
	return love.mouse.getX() - mouseOffsetX, love.mouse.getY() - mouseOffsetY
end

function SetGrabbedObject(aSlotIndex)
	if (aSlotIndex ~= nil) then
		grabbedObject = objectsInInventory[aSlotIndex]
		if (grabbedObject ~= nil) then
			grabbedObjectLastSlot = aSlotIndex
			RemoveObjectFromInventory(grabbedObject.myName)
			local xOff, yOff = GetCursorImageOffset()
			mouseOffsetX = grabbedObject.mySprite:getWidth()/2 - 32 - xOff
			mouseOffsetY = grabbedObject.mySprite:getHeight()/2 - 64 - yOff
		end
	else
		if (grabbedObjectLastSlot ~= nil) and (grabbedObject ~= nil) then
			objectsInInventory[grabbedObjectLastSlot] = grabbedObject
		end
		grabbedObject = nil
		grabbedObjectLastSlot = nil
		mouseOffsetX = 0
		mouseOffsetY = 0
	end
end

function ClickObject(aObject)
	if (aObject ~= nil) then

		local heldName = nil
		local heldType = nil
		if (grabbedObject ~= nil) then
			heldName = grabbedObject.myName
			heldType = grabbedObject.myObjectType
		end

		local inventoryObjectsCounted = GetTableSize(objectsInInventory)
		local interaction = GetInteraction(aObject.myName, heldName) -- WTF IS THIS MESS!!! COMMENT PLZ!
		if (interaction ~= nil) then
			if ((currentScene ~= "Outdoor_Flowerbed" and currentScene ~= "Outdoor_EndScene" and currentScene ~= "Floor0_CatScene") or (globalAllowClick == true)) or (GetInventoryObject(aObject.myName) ~= nil) then
				removeObject = interaction.doAction()

				if (removeObject ~= nil) then

					if ((removeObject[1] == false or removeObject[2] == false) and
						inventoryObjectsCounted == GetTableSize(objectsInInventory)) then
						PlaySound("DropObject", true)
					end

					if (removeObject[1] == true) then

						RemoveObjectFromInventory(interaction.components[1])
						RemoveObjectFromScene(interaction.components[1])

						if (heldName == interaction.components[1]) then
							grabbedObjectLastSlot = nil --Wont give you back held item
						end
					end

					if (removeObject[2] == true) then

						RemoveObjectFromInventory(interaction.components[2])
						RemoveObjectFromScene(interaction.components[2])

						if (heldName == interaction.components[2]) then
							grabbedObjectLastSlot = nil --Wont give you back held item
						end
					end
				end
			end
		else
			if (grabbedObject ~= nil) then
				local mouseX, mouseY = love.mouse.getPosition()
				local textToPrint
				local rnd = love.math.random(0, 2)

				if (rnd == 0) then
					textToPrint = "Hmm, that doesn't seem right..."
				elseif (rnd == 1) then
					textToPrint = "I can't do that..."
				elseif (rnd == 2) then
					textToPrint = "Nothing happens..."
				end

				GUIManager_PrintNewText(mouseX, mouseY, textToPrint)
				objectsInInventory[grabbedObjectLastSlot] = grabbedObject
				PlaySound("DropObject", true)
			else
				if ((currentScene ~= "Outdoor_Flowerbed" and currentScene ~= "Outdoor_EndScene" and currentScene ~= "Floor0_CatScene") or (globalAllowClick == true)) then
					aObject:OnClick()
				end
			end
		end
	end
end

function love.mousemoved(x, y, dx, dy)
	if (state == "play") then
		if (lastClickedObject == objectsInInventory[GetClickedSlot()]) and (grabbedObject == nil) then
			SetGrabbedObject(GetClickedSlot())
		end
	end
end

function love.mousepressed(mouseX, mouseY, buttonPressed)
	if state == "play" then
		if (isChangingScene == true) then
			SkipFade()
		end
		if (buttonPressed == 1) then
			SetCursorClicked(true)

			--Click gui element
			local hoveredGUIElement = GetHoveredGUIElement()
			if (hoveredGUIElement ~= nil) then
				print("Clicked " .. hoveredGUIElement.myName)
				hoveredGUIElement:OnClick()
				if (hoveredGUIElement.myName == "Flashlight_Button_Unlit" or hoveredGUIElement.myName == "Flashlight_Button_Lit") then
					globalCanPlayClickSound = false
				end

			--Click inventory objects
			elseif (objectsInInventory[GetClickedSlot()] ~= nil) then
				lastClickedObject = objectsInInventory[GetClickedSlot()]

			--Click scene objects
			elseif (hoveredObject ~= nil) then
				lastClickedObject = hoveredObject
				if (lastClickedObject.myName == "Floor1_Livingroom_Yarn" and globalPlayerScaredCat == false) then
					PlaySound("CatNormalMeow", false)
				end

				if (flashlightTurnedOn == true and (lastClickedObject.myName == "Outdoor_Flowerbed_Firefly_Left" or
					lastClickedObject.myName == "Outdoor_Flowerbed_Firefly_Right" or
					lastClickedObject.myName == "Outdoor_EndScene_Firefly_Left" or
					lastClickedObject.myName == "Outdoor_EndScene_Firefly_Right")) then
					PlaySound("FireFlyBuzz", false)
				end

			--Click nothing
			else
				if (objectsInInventory[GetClickedSlot()] ~= nil) then
					PlaySound("DropObject", true)
				end
				lastClickedObject = nil
			end

			if (lastClickedObject ~= nil) then
				print("Clicked " .. lastClickedObject.myName)
			end

			-- Ignores click sound in some circumstances like toggling flashlight.
			if (globalCanPlayClickSound == true) then
				PlaySound("MouseClick", true)
			end

			globalCanPlayClickSound = true

		elseif (buttonPressed == 2) then
			if (grabbedObject ~= nil) then
				PlaySound("DropObject", true)
				SetGrabbedObject(nil)
				--	Should unpress cursor on rightclick? // No ?
				SetCursorClicked(false)
			end

			GUIManager_ForceDialogBoxesToDissapear(false)
		end
	elseif (state == "pause") then
		if (buttonPressed == 1) then
			PauseMenuButtonClick(GetMousePosition())
		end
	elseif (state == "mainmenu") then
		if (buttonPressed == 1) then
			MainMenuButtonClick(GetMousePosition())
		end
	elseif (state == "cutscene") then
		globalCurrentVideo:Skip()
	end
end

function love.mousereleased(mouseX, mouseY, buttonReleased)
	if (state == "play") then
		if (buttonReleased == 1) then

			local clickedSlot = GetClickedSlot()
			SetCursorClicked(false)

			if (lastClickedObject == nil) then
				if (grabbedObject ~= nil) then
					PlaySound("DropObject", true)
				end

				SetGrabbedObject(nil)
			else
				if (grabbedObject == nil) then
					--Clicked inventory with nothing
					if (clickedSlot ~= nil) and (lastClickedObject == objectsInInventory[clickedSlot]) then
						SetGrabbedObject(clickedSlot)
						SetCursorClicked(true)

					--Clicked hoveredObject with nothing
					elseif (lastClickedObject == hoveredObject) then
						ClickObject(hoveredObject)
					end
				else
					local canPlaySound = true
					-- Drops the object
					if (clickedSlot ~= nil) and (objectsInInventory[clickedSlot] == nil) then
						print("Placed " .. tostring(grabbedObject.myName) .. " in inventory slot " .. tostring(clickedSlot))
						--objectsInInventory[clickedSlot] = grabbedObject
						grabbedObjectLastSlot = clickedSlot
					else
						clickedObject = nil
						if (hoveredObject ~= nil and hoveredObject ~= grabbedObject) then
							clickedObject = hoveredObject

						-- Places grabbed object on an object in the inventory
						elseif (objectsInInventory[clickedSlot] ~= nil) then
							clickedObject = objectsInInventory[clickedSlot]
						end

						if (clickedObject ~= nil) then
							ClickObject(clickedObject)

							-- If objects has been combined then don't the play sound effect.
							if (clickedObject ~= objectsInInventory[clickedSlot]) then
								canPlaySound = false
							end
						end
					end
					SetGrabbedObject(nil)

					if (canPlaySound == true) then
						PlaySound("DropObject", true)
					end
				end
			end

			if (scenes[currentScene].myName == "Floor1_Livingroom_BalloonPuzzle") then
				RemoveGUIElement(draggedBalloon)

				if (tankNozzle:IsHovered()) then
					tankNozzle:OnClick()
				end

				isDraggingBalloon = false
			end
		end

		lastClickedObject = nil
	end
end

function love.keypressed(aKey, aScancode, isRepeat)
	if (aKey == "escape") then
		if (state == "play") then
			PauseGame()
		elseif (state == "pause") then
			state = "play"
		end
	end

	if (state == "cutscene") then
		globalCurrentVideo:Skip()
	end
end

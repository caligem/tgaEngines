local inventoryImage
local inventoryXPosition
local inventoryYPosition
local firstSlotPosX
local slotPosY
local slotSize = 96

local grabbedX, grabbedY

function ResetInventoryManager()
	inventoryImage = nil
	inventoryXPosition = nil
	inventoryYPosition = nil
	firstSlotPosX = 0
	slotPosY = 0
	slotSize = 96

	grabbedX = nil
	grabbedY = nil
end

function InventoryLoad()
	inventoryImage = love.graphics.newImage("Graphics/GUI/Inventory/Inventory_Background.png")
	inventoryXPosition = love.graphics.getWidth() / 2
	inventoryYPosition = love.graphics.getHeight() - inventoryImage:getHeight() / 2
	firstSlotPosX = love.graphics.getWidth() / 2 - 210
	slotPosY = love.graphics.getHeight() - 37
end

function GetInventoryObject(aObjectName)
	for index, object in ipairs(objectsInInventory) do
		if (object.myName == aObjectName) then
			return object
		end
	end
	return nil
end

local function GetEmptySlot()
	for index = 1, 5, 1 do
		if (objectsInInventory[index] == nil) then
			return index
		end
	end
	return nil
end

function HasEmptySlot()
	if (GetEmptySlot() ~= nil) then
		return true
	else
		return false
	end
end

function AddObjectToInventory(aObject)
	PlaySound("PickupObject", true)
	
	if (string.match(aObject.myName, "Outdoor_Flowerbed_Stick")) then
		if (globalStickCount == 0) then
			aObject.myName = "Outdoor_Flowerbed_Sticks"
			table.insert(objectsInInventory, aObject)
		end

		RemoveObjectFromScene(aObject.myName)
		globalStickCount = globalStickCount + 1
		object = GetInventoryObject("Outdoor_Flowerbed_Sticks")

		if (object ~= nil) then
			object.mySprite = LoadSprite("Graphics/GUI/Inventory/Outdoor_Flowerbed_Sticks" .. tostring(globalStickCount) .. ".png")
		end

		return 0
	else
		if (HasEmptySlot() == true) then
			objectsInInventory[GetEmptySlot()] = aObject
			print("Added " .. aObject.myName .. " to inventory")
			-- This is supposed to change object's outlook and position if it is added to the inventory
			--[[aObject.mySpritePath = "Graphics/GUI/Inventory/" .. a.myName .. ".png"]]

			if (love.filesystem.exists("Graphics/GUI/Inventory/" .. aObject.myName .. ".png")) then
				aObject.mySprite = LoadSprite("Graphics/GUI/Inventory/" .. aObject.myName .. ".png")
			end

			aObject.myIsInventoryObject = true
			RemoveObjectFromScene(aObject.myName)

			return 0 -- vad returnar den här till?
		end
	end

	return -1
end

function RemoveObjectFromInventory(aObjectName)
	for index in pairs(objectsInInventory) do
		if (objectsInInventory[index].myName == aObjectName) then
			objectsInInventory[index] = nil
			break
		end
	end
end

function GetClickedSlot()
	if (showInventory == true) then
		local mouseX, mouseY = love.mouse.getPosition()
		for index = 1, 5, 1 do
			local posX, posY = GetSlotPosition(index)
			if (IsPositionInBounds(mouseX, mouseY, posX, posY, slotSize, slotSize, true) == true) then
				return index
			end
		end
	end
	
	return nil
end

function GetSlotPosition(aSlotIndex)
	local xOrigin = firstSlotPosX + ((aSlotIndex - 1) * slotSize) + ((aSlotIndex - 1) * 14)--(inventoryXPosition - (slotSize+10)*2) + (aSlot-1)*(slotSize+10)
	return xOrigin, slotPosY
end

function InventoryDraw()
	love.graphics.draw(inventoryImage, inventoryXPosition, inventoryYPosition, 0, 1, 1, inventoryImage:getWidth()/2, inventoryImage:getHeight()/2)
	for index in pairs(objectsInInventory) do
		if (objectsInInventory[index] ~= grabbedObject) and objectsInInventory[index] ~= nil then
			local posX, posY = GetSlotPosition(index)
			love.graphics.draw(objectsInInventory[index].mySprite, posX, posY, 0, 1, 1, objectsInInventory[index].mySprite:getWidth() / 2, objectsInInventory[index].mySprite:getHeight() / 2)
		end
	end

	if (state == "play") then
		grabbedX, grabbedY = GetGrabbedObjectPosition()
	end
	if (grabbedObject ~= nil) then
		love.graphics.draw(grabbedObject.mySprite, grabbedX, grabbedY, 0, 1, 1, slotSize/2, slotSize/2)
	end
end

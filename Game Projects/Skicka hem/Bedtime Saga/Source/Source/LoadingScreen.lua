local locCurrentImage = {}
local locTGAImage = nil
local locTinyStudioImage = nil
local locLoadingImage = nil
local locCounter = 0
local locWindowWidth = 0
local locWindowHeight = 0
local locIsLoading = false
local loadingBarPosX = 910
local loadingBarWidth = 115
local loadingPercentage = 0

local nextLoadFunction = nil

local function SetCurrentLoadImage(aImage)
	if (locCurrentImage.myImage ~= aImage) then
		FadeOut(function () end)
		locCurrentImage.myImage = aImage
		locCurrentImage.myWidth = aImage:getWidth()
		locCurrentImage.myHeight = aImage:getHeight()
	end
end

local function LoadGameAndThenGoToMainMenu(aLoadingPercentage)
	locIsLoading = true
	LoadGame(aLoadingPercentage)
	state = "mainmenu"
end

local function Load_Interactions() 
	
	SaveInteractions()

	loadingPercentage = loadingPercentage + (1 / 7)

	nextLoadFunction = function ()
		state = "mainmenu"
	end
end

local function Load_PauseMenu() 
	
	PauseMenuLoad()

	loadingPercentage = loadingPercentage + (1 / 7)

	nextLoadFunction = Load_Interactions
end

local function Load_GUIManager() 
	
	GUIManager_Load()

	loadingPercentage = loadingPercentage + (1 / 7)

	nextLoadFunction = Load_PauseMenu
end

local function Load_MainMenu() 
	
	MainMenuLoad()

	loadingPercentage = loadingPercentage + (1 / 7)

	nextLoadFunction = Load_GUIManager
end

local function Load_SceneManager() 
	
	SceneManagerLoad()

	loadingPercentage = loadingPercentage + (1 / 7)

	nextLoadFunction = Load_MainMenu
end


local function Load_Shaders() 
	
	outlineShader = love.graphics.newShader("Shaders/outline.glsl")
	greyscaleShader = love.graphics.newShader("Shaders/greyscale.glsl")
	flashlightShader = love.graphics.newShader("Shaders/flashlight.glsl")
	mazeLightShader = love.graphics.newShader("Shaders/mazeLight.glsl")
	televisionShader = love.graphics.newShader("Shaders/televisionShader.glsl")

	loadingPercentage = loadingPercentage + (1 / 7)

	nextLoadFunction = Load_SceneManager
end

local function Load_Fonts() 
	
	love.graphics.setNewFont("Text.ttf", 22)

	loadingPercentage = loadingPercentage + (1 / 7)

	nextLoadFunction = Load_Shaders
end

local function Load_SoundManager() 
	SoundManager_Load()
	loadingPercentage = loadingPercentage + (1 / 7)

	nextLoadFunction = Load_Fonts
end

function LoadLoadingScreen() 
	love.graphics.setBackgroundColor(0, 0, 0)

	--FadeIn()

	locTGAImage = love.graphics.newImage("Graphics/LoadingScreen/TGA.png")
	locTinyStudioImage = love.graphics.newImage("Graphics/LoadingScreen/TinyStudio.png")
	locLoadingImage = love.graphics.newImage("Graphics/LoadingScreen/Loading.png")

	locWindowWidth = love.graphics.getWidth()
	locWindowHeight = love.graphics.getHeight()

	SetCurrentLoadImage(locTGAImage)
end

function UpdateLoadingScreen(aDeltaTime) 
	locCounter = locCounter + (0.75 * aDeltaTime)
	if (locCounter >= 2 and locCounter < 3) then
		SetCurrentLoadImage(locTinyStudioImage)
	elseif (locCounter >= 3 and locCounter < 3.5) then
		SetCurrentLoadImage(locLoadingImage)
	elseif (locCounter >= 3.5) then
		if (locIsLoading == false) then
			locIsLoading = true
			nextLoadFunction = Load_SoundManager
			--LoadGameAndThenGoToMainMenu(loadingPercentage)
		end
	end
end

function DrawLoadingScreen() 
	if (locCurrentImage.myImage ~= nil) then
		if (locCounter >= 3) then
			love.graphics.setShader(greyscalePercentageShader)
			local borderPosX = loadingBarPosX + (loadingBarWidth * loadingPercentage)
			greyscalePercentageShader:send("greyBorderPosX", borderPosX)
			love.graphics.draw(locCurrentImage.myImage)
			love.graphics.setShader()

			if (nextLoadFunction ~= nil) then
				nextLoadFunction()
			end 
		else
			love.graphics.draw(locCurrentImage.myImage)
		end
	end
end
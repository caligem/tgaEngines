local button = {}
local mainMenuBG = nil
introVideo = nil
local creditsVideo = nil
local hasPressedStart = false

function MainMenuButtonSpawn(x, y, text, id)

	table.insert(button, {x = x, y = y, text = text, id = id, mouseover = false})

end

function MainMenuLoad()

	mainMenuBG = love.graphics.newImage("Graphics/Backgrounds/MainMenuBG.png")

	MainMenuButtonSpawn(275, 125, "Start", "start")
	MainMenuButtonSpawn(275, 220, "Credits", "credits")
	MainMenuButtonSpawn(275, 315, "Exit", "exit")

	currentFont = love.graphics.getFont()
	mainMenuFont = love.graphics.newFont("Moon Flower Bold.ttf", 80)
	hoverMainMenuFont = love.graphics.newFont("Moon Flower Bold.ttf", 90)

	introVideo = CreateVideo("Intro", function() FadeIn() end)
	creditsVideo = CreateVideo("Credits", function() FadeIn() end, "mainmenu")

end


function MainMenuDraw()
	if state == "mainmenu" then
		love.graphics.draw(mainMenuBG, 0, 0)
	    love.graphics.setFont(mainMenuFont)
	    MainMenuButtonDraw()
	    love.graphics.setFont(currentFont)
	end
end

function MainMenuUpdate()
	PlayMusic("MainMenu")
	MainMenuHighlightButton()
end

function MainMenuButtonDraw()
	for i, v in ipairs(button)
	do
		if v.mouseover == false then
        	love.graphics.setFont(mainMenuFont)
			love.graphics.setColor(255, 255, 255)
		end
		if v.mouseover == true then
	     	love.graphics.setFont(hoverMainMenuFont)
			love.graphics.setColor(255, 255, 255)
		end
	    if (love.graphics.getFont() == mainMenuFont) then
	      love.graphics.print(v.text, (v.x - (mainMenuFont:getWidth(v.text) / 2)), (v.y - (mainMenuFont:getHeight(v.text) / 2)))
	      love.graphics.setColor(255, 255, 255)
	    elseif (love.graphics.getFont() == hoverMainMenuFont) then
	      love.graphics.print(v.text, (v.x - (hoverMainMenuFont:getWidth(v.text) / 2)), (v.y - (hoverMainMenuFont:getHeight(v.text) / 2)))
	      love.graphics.setColor(255, 255, 255)
	    end
	end
end

function MainMenuHighlightButton()

	-- Get Mouse Pos
  -- local mouseX, mouseY = TLfres.TransformCoords(love.mouse.getPosition())
  local mouseX, mouseY = GetMousePosition()

	--Check if Mouseover
	for i,v in ipairs(button) do
		if (mouseX > (v.x - (mainMenuFont:getWidth(v.text) / 2)))
		and (mouseX < (v.x - (mainMenuFont:getWidth(v.text) / 2)) + mainMenuFont:getWidth(v.text))
		and	(mouseY > (v.y - (mainMenuFont:getHeight(v.text) / 2)))
		and (mouseY < (v.y - (mainMenuFont:getHeight(v.text) / 2)) + mainMenuFont:getHeight(v.text)) then
			v.mouseover = true
		else
			v.mouseover = false
		end
	end
end

function MainMenuButtonClick(x,y)
	for i,v in ipairs(button) do
		if (x > (v.x - (mainMenuFont:getWidth(v.text) / 2)))
		and (x < (v.x - (mainMenuFont:getWidth(v.text) / 2)) + mainMenuFont:getWidth(v.text))
		and (y > (v.y - (mainMenuFont:getHeight(v.text) / 2)))
		and (y < (v.y - (mainMenuFont:getHeight(v.text) / 2)) + mainMenuFont:getHeight(v.text)) then
			if (v.id == "exit") then
				love.event.push("quit")
			end

			if (v.id == "start" and hasPressedStart == false) then
				hasPressedStart = true
				if (state == "mainmenu") then
					StopCurrentMusicWithFadeOut(0.5)

					if (firstTimePlaying == false) then
						ResetGame()
					else
						firstTimePlaying = false
						state = "cutscene"
						globalCurrentVideo = introVideo
						globalCurrentVideo:Play()
					end
				end 

				hasPressedStart = false
			end

			if (v.id == "credits") then
				state = "cutscene"
				globalCurrentVideo = creditsVideo
				globalCurrentVideo:Play()
			end
		end
	end
end

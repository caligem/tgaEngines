 local function CreateGeneralObject(aName, aSpritePath, aDescriptions, aPosX, aPosY, aDepthIndex)
	local newObject = {}

	-- Set object variables that will be saved in file
	-- newObject.myName = aSpritePath -- Decide name from filename, add 01, 02, 03... for multiple objects with same sprite. Can be changed later in editor.

	newObject.myName = aName-- GetAvailableName(aName)
	newObject.mySpritePath = aSpritePath
	newObject.myDescriptions = aDescriptions
	newObject.myPosX = aPosX
	newObject.myPosY = aPosY
	newObject.myDepthLevel = aDepthIndex
	newObject.myColor = {255, 255, 255, 255}

	newObject.myIsInventoryObject = false

	newObject.myRotation = 0
	newObject.myScaleX = 1
	newObject.myScaleY = 1

	objectsInWorld[newObject.myName] = newObject

	return newObject
end

function CreateInventoryObject(aName, aSpritePath, aDescriptions)
	local newObject = CreateGeneralObject(aName, aSpritePath, aDescriptions, 0, 0, 0)

	newObject.myObjectType = "inventory"
	newObject.mySprite = love.graphics.newImage(aSpritePath)

	return newObject
end

function InitObject(aObject)
	aObject.GetDescription = GetObjectDescription
	aObject.mySprite = LoadSprite(aObject.mySpritePath)
	aObject.myWidth = aObject.mySprite:getWidth()
	aObject.myHeight = aObject.mySprite:getHeight()

	aObject.Draw = function ()
		if (aObject.mySprite ~= nil) then
			if (aObject.myColor ~= nil) then
				love.graphics.setColor(aObject.myColor)
			end
			love.graphics.draw(aObject.mySprite, aObject.myPosX, aObject.myPosY, aObject.myRotation,
							   aObject.myScaleX, aObject.myScaleY,	aObject.myWidth / 2, aObject.myHeight / 2)
			love.graphics.setColor(255, 255, 255, 255)
		else
			--Error message, no sprite
		end
	end

	aObject.GetPos = function ()
		return aObject.myPosX, aObject.myPosY
	end
	aObject.GetSize = function ()
		return aObject.myWidth, aObject.myHeight
	end

	aObject.SetPos = SetObjectPosition
	aObject.SetRotation = SetObjectRotation
	aObject.SetScaleX = SetObjectScaleX
	aObject.SetScaleY = SetObjectScaleY
	if (aObject.myObjectType == "pickupable") then
		aObject.OnClick = PickupableOnClick
	elseif (aObject.myObjectType == "passage") then
		aObject.OnClick = PassageOnClick
	elseif (aObject.myObjectType == "inspectable") then
		aObject.OnClick = InspectableOnClick
	elseif (aObject.myObjectType == "solid") then
		aObject.OnClick = SolidOnClick
	end
end

function GetObject(aObjectName)
	return objectsInWorld[aObjectName]
end

function GetAvailableName(aName)
	local objectNum = 1
	local prefix = "_0"
	if (objectsInWorld ~= nil) then
		while (objectsInWorld[aName .. prefix .. tostring(objectNum)] ~= nil) do
			objectNum = objectNum+1
			if(objectNum >= 10) then
				prefix = "_"
			end
		end
	end
	return (aName .. prefix .. tostring(objectNum))
end

function CreateInteraction(aObjectName1, aObjectName2, aFunction)
	if (GetInteraction(aObjectName1, aObjectName2) == nil) then
		print("Creating new interaction with " .. tostring(aObjectName1) .. " and " .. tostring(aObjectName2))
		local action = {}
		action.components = {[1]=aObjectName1, [2]=aObjectName2}
		action.done = false
		action.removeObjects = {}
		action.doAction = function()
			if (action.done == false) then
				action.removeObjects = {aFunction()}
				if (GetTableSize(action.removeObjects) > 0) then
					print("Interaction with " .. tostring(action.components[1]) .. " and " .. tostring(action.components[2]) .. " is done!")
					action.done = true
				end
				return action.removeObjects
			end
		end
		table.insert(interactions, action)
		return true
	else
		print("Interaction with " .. tostring(aObjectName1) .. " and " .. tostring(aObjectName2) .. " exists")
		return false
	end
end

function GetInteraction(aObjectName1, aObjectName2)
	for _, action in ipairs(interactions) do
		if (action.components[1] == aObjectName1) and (action.components[2] == aObjectName2)
		or (action.components[1] == aObjectName2) and (action.components[2] == aObjectName1) then
			--print("Interaction with " .. tostring(aObjectName1) .. " and " .. tostring(aObjectName2))
			return action
		end
	end
	--print("No interaction with " .. tostring(aObjectName1) .. " and " .. tostring(aObjectName2))
	return nil
end

function GetObjectDescription(aObject)
  if (aObject.myObjectType == "solid") then
    return nil
  end
	local namesOfStates = {"one", "two", "three", "four", "five"}
	local aRoomState = scenes[currentScene].roomState
	local foundDescription = ""

	local numberOfTries = 0

	while (foundDescription == "") do
		if (aRoomState - numberOfTries > 0) then
			nameOfCurrentState = namesOfStates[aRoomState - numberOfTries]

			if (aObject.myDescriptions ~= nil) and (aObject.myDescriptions[nameOfCurrentState] ~= nil) then
				foundDescription = aObject.myDescriptions[nameOfCurrentState]
			else
				foundDescription = ""
				numberOfTries = numberOfTries + 1
			end
		else
			print("Error, no description found!")
			return nil
		end
	end

	return foundDescription
end

function CreatePickupableObject(aName, aDescriptions, aPosX, aPosY, aDepthIndex, aNeededRoomState)
	local newObject = CreateGeneralObject(aName, "Graphics/Objects/Pickupable/" .. aName .. ".png", aDescriptions, aPosX, aPosY, aDepthIndex)
	newObject.myObjectType = "pickupable"
	newObject.myAllowState = aNeededRoomState or 1-- Negative number stops allowing after
	InitObject(newObject)

	return newObject
end

function CreatePassageObject(aName, aDescriptions, aPosX, aPosY, aDepthIndex, aDestinationScene, aNeededRoomState)
	local newObject = CreateGeneralObject(aName, "Graphics/Objects/Passages/" .. aName .. ".png", aDescriptions, aPosX, aPosY, aDepthIndex)
	newObject.myObjectType = "passage"
	newObject.myDestinationScene = aDestinationScene
	newObject.myAllowState = aNeededRoomState or 1 -- Negative number stops allowing after

	InitObject(newObject)

	return newObject
end

function CreateInspectableObject(aName, aDescriptions, aPosX, aPosY, aDepthIndex)
	local newObject = CreateGeneralObject(aName, "Graphics/Objects/Inspectable/" .. aName .. ".png", aDescriptions, aPosX, aPosY, aDepthIndex)
	newObject.myObjectType = "inspectable"

	InitObject(newObject)

	return newObject
end

function CreateSolidObject(aName, aPosX, aPosY, aDepthIndex)
	local newObject = CreateGeneralObject(aName, "Graphics/Objects/Solid/" .. aName .. ".png", {["one"] = ""}, aPosX, aPosY, aDepthIndex)
	newObject.myObjectType = "solid"

	InitObject(newObject)

	return newObject
end

function SetObjectPosition(aObject, posX, posY)
	aObject.myPosX = posX
	aObject.myPosY = posY
end

function SetObjectRotation(aObject, aNewRotation)
	aObject.myRotation = aNewRotation
end

function SetObjectScaleX(aObject, aNewScaleX)
	aObject.myScaleX = aNewScaleX
end

function SetObjectScaleY(aObject, aNewScaleY)
	aObject.myScaleY = aNewScaleY
end

function GetObjectRotation(aObject)
	return aObject.myRotation
end

function PickupableOnClick(aObject)
	if (scenes[currentScene].roomState >= math.abs(aObject.myAllowState)) and (aObject.myAllowState > 0)
	or (scenes[currentScene].roomState <  math.abs(aObject.myAllowState)) and (aObject.myAllowState < -1) then
		AddObjectToInventory(aObject)
	end

	GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), GetObjectDescription(aObject))
end

function PassageOnClick(aObject)
	if (scenes[currentScene].roomState >= math.abs(aObject.myAllowState)) and (aObject.myAllowState > 0)
	or (scenes[currentScene].roomState <  math.abs(aObject.myAllowState)) and (aObject.myAllowState < -1) then
		FadeOut(function() ChangeScene(aObject.myDestinationScene)end)
	else
		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), GetObjectDescription(aObject))
	end
end

function InspectableOnClick(aObject)
	GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), GetObjectDescription(aObject))
end

function SolidOnClick(aObject)
end

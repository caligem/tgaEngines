local button = {}

function ResetPauseMenu()
	button = {}
end

function PauseButtonSpawn(x, y, text, id)
	table.insert(button, {x = x, y = y, text = text, id = id, mouseover = false})
end

-------------------------------------------------------------------------------

function PauseMenuLoad()

  	PauseMenuBG = love.graphics.newImage("Graphics/Backgrounds/PauseMenuBG.png")
  	PauseButtonSpawn(extent/2, 520, "Resume", "resume")
  	PauseButtonSpawn(extent/2, 625, "Exit", "exit")


  	currentFont = love.graphics.getFont()
		pauseMenuFont = love.graphics.newFont("Moon Flower Bold.ttf", 90)
		hoverPauseMenuFont = love.graphics.newFont("Moon Flower Bold.ttf", 110)

end

function PauseMenuDraw()

	if state == "pause" then

		-- BG draw
		love.graphics.draw(PauseMenuBG, 0, 0)

		-- Button draw
		love.graphics.setFont(pauseMenuFont)
		PauseButtonDraw()
		love.graphics.setFont(currentFont)
	end
end

function PauseMenuUpdate()
  PauseMenuHighlightButton()
end

function PauseMenuExit()
 -- ?
end

-------------------------------------------------------------------------------

function PauseButtonDraw()
	for i, v in ipairs(button)
	do
		if v.mouseover == false
			then
			love.graphics.setFont(pauseMenuFont)
			love.graphics.setColor(255, 255, 255)
		end
		if v.mouseover == true
			then
			love.graphics.setFont(hoverPauseMenuFont)
			love.graphics.setColor(255, 255, 255)
		end
			if (love.graphics.getFont() == pauseMenuFont) then
				love.graphics.print(v.text, (v.x - (pauseMenuFont:getWidth(v.text) / 2)), (v.y - (pauseMenuFont:getHeight(v.text) / 2)))
				love.graphics.setColor(255, 255, 255)
			elseif (love.graphics.getFont() == hoverPauseMenuFont) then
				love.graphics.print(v.text, (v.x - (hoverPauseMenuFont:getWidth(v.text) / 2)), (v.y - (hoverPauseMenuFont:getHeight(v.text) / 2)))
				love.graphics.setColor(255, 255, 255)
		end
	end
end


function PauseMenuHighlightButton()

	-- Get Mouse Pos
  -- local mouseX, mouseY = TLfres.TransformCoords(love.mouse.getPosition())
  local mouseX, mouseY = GetMousePosition()

	--Check if Mouseover
	for i,v in ipairs(button) do
		if (mouseX > (v.x - (pauseMenuFont:getWidth(v.text) / 2)))
		and (mouseX < (v.x - (pauseMenuFont:getWidth(v.text) / 2)) + pauseMenuFont:getWidth(v.text))
		and	(mouseY > (v.y - (pauseMenuFont:getHeight(v.text) / 2)))
		and (mouseY < (v.y - (pauseMenuFont:getHeight(v.text) / 2)) + pauseMenuFont:getHeight(v.text)) then
			v.mouseover = true
		else
			v.mouseover = false
		end
	end
end

function PauseMenuButtonClick(x,y)
	for i,v in ipairs(button) do
		if (x > (v.x - (pauseMenuFont:getWidth(v.text) / 2)))
		and (x < (v.x - (pauseMenuFont:getWidth(v.text) / 2)) + pauseMenuFont:getWidth(v.text))
		and (y > (v.y - (pauseMenuFont:getHeight(v.text) / 2)))
		and (y < (v.y - (pauseMenuFont:getHeight(v.text) / 2)) + pauseMenuFont:getHeight(v.text)) then
			if (v.id == "exit") then
				love.event.push("quit")
			end

			if (v.id == "resume") then
				ResumeAllSounds()
				state = "play"
			end
		end
	end
end

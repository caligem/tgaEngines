local locCutScene = {}
-- The cat's eye variable(s)
local locCatEye = {}
local locCatEyeImages = {}
local locCatBlinks = false
-- The cat's paw variable(s)
local locCatPaw = {}
local locMoveCatsPaw = false
local locCatPawsStartPosX = 0
local locCatPawsStartPosY = 0
local locCatPawsStartRotation = 0

function ResetCatScene()
	locCutScene = {}
	-- The cat's eye variable(s)
	locCatEye = {}
	locCatEyeImages = {}
	locCatBlinks = false
	-- The cat's paw variable(s)
	locCatPaw = {}
	locMoveCatsPaw = false
	locCatPawsStartPosX = 0
	locCatPawsStartPosY = 0
	locCatPawsStartRotation = 0
end

-- Private function(s)

local function CatIsHurt()
	if (Floor0_CatScene.roomState == 2) then
		--MoveOverTime("Floor0_CatScene_Catpaw", 0.8, 50, 50, nil)
		state = "cutscene"
		globalCurrentVideo = locCutScene
		globalCurrentVideo:Play()

		objectsInWorld["Floor0_CatScene_Mousetrap"].mySprite = LoadSprite("Graphics/Objects/Inspectable/Floor0_CatScene_Used_Mousetrap.png")
		objectsInWorld["Floor0_CatScene_Mousetrap"].myObjectType = "solid"
		Floor0_CatScene.roomState = 3

		RemoveObjectFromScene("Floor0_CatScene_Armed_CatEye")
		RemoveObjectFromScene("Floor0_CatScene_CatPaw")
		
		-- Creates new passage after the cat has been scared.
		local passageObject = CreatePassageObject("Floor0_CatScene_Rathole",
			{},
			locCatEye.myPosX + 3,
			locCatEye.myPosY - 13,
			4, -- Very high, doesn't matter.
			"Outdoor_Flowerbed",
			3)
		table.insert(Floor0_CatScene.objects, passageObject)
	end
end

local function AnimateCatEye()
	local additionalTime = 0.1
	local holdUpTime = 1.0
	local timeToWait = additionalTime

	Wait(timeToWait, function()
		locCatEye.mySprite = locCatEyeImages.myEye1
		locCatEye.myColor[4] = 255
		end)

	timeToWait = timeToWait + additionalTime
	Wait(timeToWait, function() locCatEye.mySprite = locCatEyeImages.myEye2 end)

	timeToWait = timeToWait + additionalTime
	Wait(timeToWait, function() locCatEye.mySprite = locCatEyeImages.myEye3 end)

	timeToWait = timeToWait + additionalTime
	Wait(timeToWait, function() locCatEye.mySprite = locCatEyeImages.myEye4 end)

	timeToWait = timeToWait + additionalTime + holdUpTime
	Wait(timeToWait, function() locCatEye.mySprite = locCatEyeImages.myEye3 end)

	timeToWait = timeToWait + additionalTime
	Wait(timeToWait, function() locCatEye.mySprite = locCatEyeImages.myEye2 end)

	timeToWait = timeToWait + additionalTime
	Wait(timeToWait, function() locCatEye.mySprite = locCatEyeImages.myEye1 end)

	timeToWait = timeToWait + additionalTime
	Wait(timeToWait,
		function()
			locCatEye.mySprite = locCatEyeImages.myEyeClosed
			locCatBlinks = false
			locCatEye.myColor[4] = 1
			locMoveCatsPaw = true
		end)
end

local function CatsPawUpdate(aDeltaTime)
	local maxXDistance = 300
	local maxYDistance = 60
	local triggerXDistance = locCatPawsStartPosX + maxXDistance / 1.6
	local xSpeed = 400 * aDeltaTime
	local ySpeed = 400 * aDeltaTime
	local radiansPerSecond = (math.pi / 2) * aDeltaTime
	local rotateState = 0

	-- Changes the paw's position.
	if (locMoveCatsPaw == true) then
		-- X position update.
		if (locCatPaw.myPosX < locCatPawsStartPosX + maxXDistance) then
			locCatPaw.myPosX = locCatPaw.myPosX + xSpeed
		end
		-- Check distance to enable rotation.
		if (locCatPaw.myPosX > triggerXDistance) then
			rotateState = 1

			if (locCatPaw.myPosY >= locCatPawsStartPosY and locCatPaw.myPosY < locCatPawsStartPosY + ySpeed) then
				PlaySound("CatAttack", true)
			end
			-- Y position update.
			if (locCatPaw.myPosY < locCatPawsStartPosY + maxYDistance) then
				locCatPaw.myPosY = locCatPaw.myPosY + ySpeed
			end
		end
		-- Checks if the positions are in place.
		if (locCatPaw.myPosX >= locCatPawsStartPosX + maxXDistance and locCatPaw.myPosY >= locCatPawsStartPosY + maxYDistance) then
			locMoveCatsPaw = false
			CatIsHurt()
		end

	elseif (locCatPaw.myPosX > locCatPawsStartPosX or locCatPaw.myPosY > locCatPawsStartPosY) then
		-- X position update.
		locCatPaw.myPosX = locCatPaw.myPosX - xSpeed

		if (locCatPaw.myPosX < locCatPawsStartPosX) then
			locCatPaw.myPosX = locCatPawsStartPosX
		end
		-- Check distance to enable rotation.
		if (locCatPaw.myPosX <= triggerXDistance) then
			rotateState = 2
			-- Y position update.
			locCatPaw.myPosY = locCatPaw.myPosY - ySpeed

			if (locCatPaw.myPosY < locCatPawsStartPosY) then
				locCatPaw.myPosY = locCatPawsStartPosY
			end
		end
	end

	-- Changes the paw's rotation.
	if (rotateState == 1) then
		if (locCatPaw.myRotation > 0) then
			locCatPaw.myRotation = locCatPaw.myRotation - radiansPerSecond
		end

		if (locCatPaw.myRotation < 0) then
			locCatPaw.myRotation = 0
		end
	elseif (rotateState == 2 and locCatPaw.myRotation < locCatPawsStartRotation) then
		locCatPaw.myRotation = locCatPaw.myRotation + radiansPerSecond

		if (locCatPaw.myRotation > locCatPawsStartRotation) then
			locCatPaw.myRotation = locCatPawsStartRotation
			rotateState = 0
		end
	end
end

-- Public function(s)

function Floor0_CatScene_Load()
	Floor0_CatScene = LoadScene("Floor0_CatScene")
	Floor0_CatScene.OnEnter = Floor0_CatScene_OnEnter
	Floor0_CatScene.OnLeave = Floor0_CatScene_OnLeave
	Floor0_CatScene.OnUpdate = Floor0_CatScene_OnUpdate
	Floor0_CatScene.roomState = 1
	Floor0_CatScene.backgroundImage = love.graphics.newImage("Graphics/Backgrounds/" .. "Floor0_CatScene" .. ".png")
	Floor0_CatScene.darknessMap = love.graphics.newImage("/Graphics/Backgrounds/Floor0_CatScene_DarknessMap.png")
	Floor0_CatScene.furthestPoint = {975, 300}

	locCutScene = CreateVideo("CatScene", function() end)

	CreateInteraction("Floor0_CatScene_FluffyInsolation", "Floor0_CatScene_Shoelace",
		function()
			AddObjectToInventory(CreateInventoryObject("Floor0_CatScene_FluffyCatToy", "Graphics/GUI/Inventory/Floor0_CatScene_FluffyCatToy.png", "FluffyCatToy"))
			return true, true
		end)

	CreateInteraction("Floor0_CatScene_FluffyCatToy", "Floor0_CatScene_Mousetrap",
		function()
			objectsInWorld["Floor0_CatScene_Mousetrap"].mySprite = LoadSprite("Graphics/Objects/Inspectable/Floor0_CatScene_Armed_Mousetrap.png") --Ready to catch a cat
			objectsInWorld["Floor0_CatScene_Mousetrap"].myDescriptions["two"] = "It's a trap! "
			Floor0_CatScene.roomState = 2
			return true, false
		end)

	-- Saves images for cats eye animation.
	locCatEyeImages =
	{
		myEyeClosed = LoadSprite("Graphics/Objects/Inspectable/Floor0_CatScene_Armed_CatEye.png"),
		myEye1 = LoadSprite("Graphics/Animation/CatEye1.png"),
		myEye2 = LoadSprite("Graphics/Animation/CatEye2.png"),
		myEye3 = LoadSprite("Graphics/Animation/CatEye3.png"),
		myEye4 = LoadSprite("Graphics/Animation/CatEye4.png")
	}
	-- Creates one cat eye as an inspectable object.
	locCatEye = CreateInspectableObject("Floor0_CatScene_Armed_CatEye",
		{ ["one"] = "Is that my Cat? It seems like it won't let me go through. There should be some way to scare it away..."},
		924,
		447,
		4)
	locCatEye.myColor[4] = 1
	table.insert(Floor0_CatScene.objects, locCatEye)
	-- Creates an interaction with the cat's eye.
	CreateInteraction("Floor0_CatScene_Armed_CatEye", nil,
		function()
		if (Floor0_CatScene.roomState <= 2) then
			if (locCatBlinks == false) then
				locCatBlinks = true
				AnimateCatEye()
			end
		end
	end)
	-- Creates left and right wall.
	local leftWall = CreateSolidObject("Floor0_CatScene_RatholeLeftWall", 809, 295, 1)
	table.insert(Floor0_CatScene.objects, leftWall)
	local rightWall = CreateSolidObject("Floor0_CatScene_RatholeRightWall", 1043, 425, 3)
	table.insert(Floor0_CatScene.objects, rightWall)
	-- Creates a cats paw.
	local catPawImage = LoadSprite("Graphics/Animation/catPaw.png")
	locCatPaw = CreateInspectableObject("Floor0_CatScene_CatPaw",
		{ ["one"] = "It almost got me!"},
		locCatEye.myPosX - locCatEye.myWidth / 2 - catPawImage:getWidth() / 3,
		locCatEye.myPosY - 60,
		2)
	locCatPaw.mySprite = catPawImage
	locCatPaw.myWidth = catPawImage:getWidth()
	locCatPaw.myHeight = catPawImage:getHeight()
	locCatPaw.myRotation = math.pi / 4
	locCatPawsStartRotation = locCatPaw.myRotation
	locCatPawsStartPosX = locCatPaw.myPosX
	locCatPawsStartPosY = locCatPaw.myPosY
	table.insert(Floor0_CatScene.objects, locCatPaw)

	SortObjectsByDepth(Floor0_CatScene.objects)
	return Floor0_CatScene
end

function Floor0_CatScene_OnEnter()
	flashlightButton.myIsGrey = false
	globalAllowClick = false
	showFlashlight = true
	GUIManager_FlashlightOn()
end

function Floor0_CatScene_OnLeave()
	globalAllowClick = true
end

function Floor0_CatScene_OnUpdate(aDeltaTime)
	CatsPawUpdate(aDeltaTime)
end

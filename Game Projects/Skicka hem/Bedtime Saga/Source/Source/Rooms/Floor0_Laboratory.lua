local waterParticleImage = love.graphics.newImage("Graphics/Particles/Floor0_Laboratory_WaterDrop.png")
local waterParticle = love.graphics.newParticleSystem(waterParticleImage, 3200)
local blueBubbleSprite1 = {}
local blueBubbleSprite2 = {}
local blueBubbleSprite3 = {}
local greenBubbleSprite1 = {}
local greenBubbleSprite2 = {}
local greenBubbleSprite3 = {}
local redBubbleSprite1 = {}
local redBubbleSprite2 = {}
local redBubbleSprite3 = {}
local timer = 0.5
local index = 1
local spiderUp = false
local spiderDown = false

local prevCanPlayWaterDropSound = true
local canPlayWaterDropSound = true

function ResetLaboratory()
	waterParticleImage = love.graphics.newImage("Graphics/Particles/Floor0_Laboratory_WaterDrop.png")
	waterParticle = love.graphics.newParticleSystem(waterParticleImage, 3200)
	blueBubbleSprite1 = {}
	blueBubbleSprite2 = {}
	blueBubbleSprite3 = {}
	greenBubbleSprite1 = {}
	greenBubbleSprite2 = {}
	greenBubbleSprite3 = {}
	redBubbleSprite1 = {}
	redBubbleSprite2 = {}
	redBubbleSprite3 = {}
	
	prevCanPlayWaterDropSound = true
	canPlayWaterDropSound = true
end

local function LoopSpiderSprites(aDeltaTime)
		timer = timer - aDeltaTime
		if (timer <= 0) then
			if (spiderUp == true) then
				index = index + 1
			elseif (spiderDown == true) then
				index = index - 1
			end

			if (index == 1) then
				spiderUp = true
				spiderDown = false
				timer = 5
				objectsInWorld["Spider"].mySprite = mySpiderSprites.mySpider1
			elseif (index == 2) then
				timer = 0.1
				objectsInWorld["Spider"].mySprite = mySpiderSprites.mySpider2
			elseif (index == 3) then
				timer = 0.1
				objectsInWorld["Spider"].mySprite = mySpiderSprites.mySpider3
			elseif (index == 4) then
				timer = 0.1
				objectsInWorld["Spider"].mySprite = mySpiderSprites.mySpider4
			elseif (index == 5) then
				spiderDown = true
				spiderUp = false
				timer = 3
				objectsInWorld["Spider"].mySprite = mySpiderSprites.mySpider5
			end
		end
end

local function SetWaterParticle()
	waterParticle:setParticleLifetime(0.97)
	waterParticle:setEmissionRate(0.5)
	waterParticle:setSizes(0.03)
	waterParticle:setLinearAcceleration(0, 800, 0, 800)
	waterParticle:setAreaSpread("uniform", 0, 0 )
	prevCanPlayWaterDropSound = true
	canPlayWaterDropSound = true
end
local function LoadBubbles()
	-- blue 1
	blueBubbleSprite1.myImage = love.graphics.newImage("Graphics/Particles/Floor0_Laboratory_Bubble.png")
	blueBubbleSprite1.myPosX = 640
	blueBubbleSprite1.myPosY = 790
	blueBubbleSprite1.mySize = 0.05
	-- blue 2
	blueBubbleSprite2.myImage = love.graphics.newImage("Graphics/Particles/Floor0_Laboratory_Bubble.png")
	blueBubbleSprite2.myPosX = 680
	blueBubbleSprite2.myPosY = 690
	blueBubbleSprite2.mySize = 0.05
	-- blue 3
	blueBubbleSprite3.myImage = love.graphics.newImage("Graphics/Particles/Floor0_Laboratory_Bubble.png")
	blueBubbleSprite3.myPosX = 700
	blueBubbleSprite3.myPosY = 740
	blueBubbleSprite3.mySize = 0.05

	-- green 1
	greenBubbleSprite1.myImage = love.graphics.newImage("Graphics/Particles/Floor0_Laboratory_Bubble.png")
	greenBubbleSprite1.myPosX = 840
	greenBubbleSprite1.myPosY = 790
	greenBubbleSprite1.mySize = 0.05
	-- green 2
	greenBubbleSprite2.myImage = love.graphics.newImage("Graphics/Particles/Floor0_Laboratory_Bubble.png")
	greenBubbleSprite2.myPosX = 870
	greenBubbleSprite2.myPosY = 730
	greenBubbleSprite2.mySize = 0.05
	-- green 3
	greenBubbleSprite3.myImage = love.graphics.newImage("Graphics/Particles/Floor0_Laboratory_Bubble.png")
	greenBubbleSprite3.myPosX = 890
	greenBubbleSprite3.myPosY = 720
	greenBubbleSprite3.mySize = 0.05

	-- red 1
	redBubbleSprite1.myImage = love.graphics.newImage("Graphics/Particles/Floor0_Laboratory_Bubble.png")
	redBubbleSprite1.myPosX = 1090
	redBubbleSprite1.myPosY = 780
	redBubbleSprite1.mySize = 0.05
	-- red 2
	redBubbleSprite2.myImage = love.graphics.newImage("Graphics/Particles/Floor0_Laboratory_Bubble.png")
	redBubbleSprite2.myPosX = 1080
	redBubbleSprite2.myPosY = 690
	redBubbleSprite2.mySize = 0.05
	-- red 3
	redBubbleSprite3.myImage = love.graphics.newImage("Graphics/Particles/Floor0_Laboratory_Bubble.png")
	redBubbleSprite3.myPosX = 1060
	redBubbleSprite3.myPosY = 735
	redBubbleSprite3.mySize = 0.05
end

function Floor0_Laboratory_Load()
	Floor0_Laboratory = LoadScene("Floor0_Laboratory")
	Floor0_Laboratory.OnEnter = Floor0_Laboratory_OnEnter
	Floor0_Laboratory.OnLeave = Floor0_Laboratory_OnLeave
	Floor0_Laboratory.OnUpdate = Floor0_Laboratory_OnUpdate
	Floor0_Laboratory.OnDraw = Floor0_Laboratory_OnDraw
	Floor0_Laboratory.roomState = 1
	Floor0_Laboratory.backgroundImage = love.graphics.newImage("Graphics/Backgrounds/" .. "Floor0_Laboratory" .. ".png")

	SetWaterParticle()
	LoadBubbles()

	spider = CreateSolidObject("Spider", 1720, 468, 1)
	table.insert(Floor0_Laboratory.objects, spider)

	mySpiderSprites =
	{
		mySpider1 = spider.mySprite,
		mySpider2 = LoadSprite("Graphics/Animation/Spider2.png"),
		mySpider3 = LoadSprite("Graphics/Animation/Spider3.png"),
		mySpider4 = LoadSprite("Graphics/Animation/Spider4.png"),
		mySpider5 = LoadSprite("Graphics/Animation/Spider5.png")
	}

	return Floor0_Laboratory
end

local function UpdateWaterSoundEffect()
	prevCanPlayWaterDropSound = canPlayWaterDropSound
	if (waterParticle:getCount() == 0) then
		canPlayWaterDropSound = true
	else
		canPlayWaterDropSound = false
	end

	if (prevCanPlayWaterDropSound == false and canPlayWaterDropSound == true) then

		PlaySound("WaterDrop", true)
	end
end
local function RandomDirection(aPosX)
	if (aPosX > 620 and aPosX < 730) then
		if (aPosX > 655 and aPosX < 700) then
			return math.random(-20,20)
		elseif (aPosX <= 655) then
			return math.random(0,20)
		elseif (aPosX >= 700) then
			return math.random(-20,0)
		end
	elseif (aPosX > 820 and aPosX < 930) then
		if (aPosX > 855 and aPosX < 900) then
			return math.random(-20,20)
		elseif (aPosX <= 855) then
			return math.random(0,20)
		elseif (aPosX >= 900) then
			return math.random(-20,0)
		end
	elseif (aPosX > 1000 and aPosX < 1120) then
		if (aPosX > 1050 and aPosX < 1090) then
			return math.random(-20,20)
		elseif (aPosX <= 1050) then
			return math.random(0,20)
		elseif (aPosX >= 1090) then
			return math.random(-20,0)
		end
	end
end

local function OutOfBounds()
	local minimum = 3
	if (blueBubbleSprite1.myPosY <= 600) then
		blueBubbleSprite1.myPosY = 790
		blueBubbleSprite1.myPosX = blueBubbleSprite1.myPosX + RandomDirection(blueBubbleSprite1.myPosX)
		blueBubbleSprite1.mySize = (math.random(minimum, 5) / 100)
	end
	if (blueBubbleSprite2.myPosY <= 600) then
		blueBubbleSprite2.myPosY = 790
		blueBubbleSprite2.myPosX = blueBubbleSprite2.myPosX + RandomDirection(blueBubbleSprite2.myPosX)
		blueBubbleSprite2.mySize = (math.random(minimum, 5) / 100)
	end
	if (blueBubbleSprite3.myPosY <= 600) then
		blueBubbleSprite3.myPosY = 790
		blueBubbleSprite3.myPosX = blueBubbleSprite3.myPosX + RandomDirection(blueBubbleSprite3.myPosX)
		blueBubbleSprite3.mySize = (math.random(minimum, 5) / 100)
	end

	if (greenBubbleSprite1.myPosY <= 600) then
		greenBubbleSprite1.myPosY = 790
		greenBubbleSprite1.myPosX = greenBubbleSprite1.myPosX + RandomDirection(greenBubbleSprite1.myPosX)
		greenBubbleSprite1.mySize = (math.random(minimum, 5) / 100)
	end
	if (greenBubbleSprite2.myPosY <= 600) then
		greenBubbleSprite2.myPosY = 790
		greenBubbleSprite2.myPosX = greenBubbleSprite2.myPosX + RandomDirection(greenBubbleSprite2.myPosX)
		greenBubbleSprite2.mySize = (math.random(minimum, 5) / 100)
	end
	if (greenBubbleSprite3.myPosY <= 600) then
		greenBubbleSprite3.myPosY = 790
		greenBubbleSprite3.myPosX = greenBubbleSprite3.myPosX + RandomDirection(greenBubbleSprite3.myPosX)
		greenBubbleSprite3.mySize = (math.random(minimum, 5) / 100)
	end

	if (redBubbleSprite1.myPosY <= 600) then
		redBubbleSprite1.myPosY = 790
		redBubbleSprite1.myPosX = redBubbleSprite1.myPosX + RandomDirection(redBubbleSprite1.myPosX)
		redBubbleSprite1.mySize = (math.random(minimum, 5) / 100)
	end
	if (redBubbleSprite2.myPosY <= 600) then
		redBubbleSprite2.myPosY = 790
		redBubbleSprite2.myPosX = redBubbleSprite2.myPosX + RandomDirection(redBubbleSprite2.myPosX)
		redBubbleSprite2.mySize = (math.random(minimum, 5) / 100)
	end
	if (redBubbleSprite3.myPosY <= 600) then
		redBubbleSprite3.myPosY = 790
		redBubbleSprite3.myPosX = redBubbleSprite3.myPosX + RandomDirection(redBubbleSprite3.myPosX)
		redBubbleSprite3.mySize = (math.random(minimum, 5) / 100)
	end
end
local function UpdateBubbles(aDeltaTime)
	blueBubbleSprite1.myPosY = blueBubbleSprite1.myPosY - 50 * aDeltaTime
	blueBubbleSprite2.myPosY = blueBubbleSprite2.myPosY - 70 * aDeltaTime
	blueBubbleSprite3.myPosY = blueBubbleSprite3.myPosY - 35 * aDeltaTime

	greenBubbleSprite1.myPosY = greenBubbleSprite1.myPosY - 31 * aDeltaTime
	greenBubbleSprite2.myPosY = greenBubbleSprite2.myPosY - 54 * aDeltaTime
	greenBubbleSprite3.myPosY = greenBubbleSprite3.myPosY - 42 * aDeltaTime

	redBubbleSprite1.myPosY = redBubbleSprite1.myPosY - 40 * aDeltaTime
	redBubbleSprite2.myPosY = redBubbleSprite2.myPosY - 67 * aDeltaTime
	redBubbleSprite3.myPosY = redBubbleSprite3.myPosY - 31 * aDeltaTime
end
function Floor0_Laboratory_OnUpdate(aDeltaTime)
	waterParticle:update(aDeltaTime)
	--bubbleParticle1:update(aDeltaTime)
	--bubbleParticle2:update(aDeltaTime)
	--bubbleParticle3:update(aDeltaTime)
	UpdateWaterSoundEffect()
	UpdateBubbles(aDeltaTime)
	LoopSpiderSprites(aDeltaTime)
	OutOfBounds()
end

function Floor0_Laboratory_OnEnter()
	flashlightButton.myIsGrey = true
end

function Floor0_Laboratory_OnLeave()
end

local function DrawBubbles()
	love.graphics.draw(blueBubbleSprite1.myImage, blueBubbleSprite1.myPosX, blueBubbleSprite1.myPosY, 0, blueBubbleSprite1.mySize, blueBubbleSprite1.mySize)
	love.graphics.draw(blueBubbleSprite2.myImage, blueBubbleSprite2.myPosX, blueBubbleSprite2.myPosY, 0,  blueBubbleSprite2.mySize,  blueBubbleSprite2.mySize)
	love.graphics.draw(blueBubbleSprite3.myImage, blueBubbleSprite3.myPosX, blueBubbleSprite3.myPosY, 0,  blueBubbleSprite3.mySize, blueBubbleSprite3.mySize)

	love.graphics.draw(greenBubbleSprite1.myImage, greenBubbleSprite1.myPosX, greenBubbleSprite1.myPosY, 0, greenBubbleSprite1.mySize, greenBubbleSprite1.mySize)
	love.graphics.draw(greenBubbleSprite2.myImage, greenBubbleSprite2.myPosX, greenBubbleSprite2.myPosY, 0, greenBubbleSprite2.mySize, greenBubbleSprite2.mySize)
	love.graphics.draw(greenBubbleSprite3.myImage, greenBubbleSprite3.myPosX, greenBubbleSprite3.myPosY, 0, greenBubbleSprite3.mySize, greenBubbleSprite3.mySize)

	love.graphics.draw(redBubbleSprite1.myImage, redBubbleSprite1.myPosX, redBubbleSprite1.myPosY, 0, redBubbleSprite1.mySize, redBubbleSprite1.mySize)
	love.graphics.draw(redBubbleSprite2.myImage, redBubbleSprite2.myPosX, redBubbleSprite2.myPosY, 0, redBubbleSprite2.mySize, redBubbleSprite2.mySize)
	love.graphics.draw(redBubbleSprite3.myImage, redBubbleSprite3.myPosX, redBubbleSprite3.myPosY, 0, redBubbleSprite3.mySize, redBubbleSprite3.mySize)
end

function DrawParticlesInLab()
	love.graphics.draw(waterParticle, 462, 450)
	--if (bubbleParticle1) then
	DrawBubbles()

	--love.graphics.draw(bubbleParticle2, 660, 790)
	--love.graphics.draw(bubbleParticle3, 695, 790)
end

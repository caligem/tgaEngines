﻿local cutScene = nil
-- Flask variable(s)
local flask = {}
local flaskBack = {}
local flaskStartXPos = 	0
local flaskStartYPos = 	0
-- Flask content variable(s)
local flaskContent = {}
local flashContentEmptyingSprite = nil
local currentFlaskFill = 0
local triggerColorValues = {85, 42.5, 127.5, 255}
local redCountFilled = 0
local greenCountFilled = 0
local blueCountFilled = 0
-- Button variable(s)
local buttons = {}
local buttonRotationSpeed = 0.15
local buttonRotationLimit = math.pi / 2
local buttonRotationTimerLimit = 0.5
local buttonsRotationVariables =
{
	button1 = {canRotate = false, rotationStopTimer = 0},
	button2 = {canRotate = false, rotationStopTimer = 0},
	button3 = {canRotate = false, rotationStopTimer = 0}
}
-- Flask clearing variable(s)
local startTimerWhenFlaskIsFilled = true

--	1 = "empty"
--	2 = "flask"
--	3 = "blender"
--	4 = "flask+blender"

local puzzleState = 1

function ResetColorPuzzle()
	cutScene = nil
	-- Flask variable(s)
	flask = {}
	flaskBack = {}
	flaskStartXPos = 	0
	flaskStartYPos = 	0
	-- Flask content variable(s)
	flaskContent = {}
	flashContentEmptyingSprite = nil
	currentFlaskFill = 0
	triggerColorValues = {85, 42.5, 127.5, 255}
	redCountFilled = 0
	greenCountFilled = 0
	blueCountFilled = 0
	-- Button variable(s)
	buttons = {}
	buttonRotationSpeed = 0.15
	buttonRotationLimit = math.pi / 2
	buttonRotationTimerLimit = 0.5
	buttonsRotationVariables =
	{
		button1 = {canRotate = false, rotationStopTimer = 0},
		button2 = {canRotate = false, rotationStopTimer = 0},
		button3 = {canRotate = false, rotationStopTimer = 0}
	}
	-- Flask clearing variable(s)
	startTimerWhenFlaskIsFilled = true

	--	1 = "empty"
	--	2 = "flask"
	--	3 = "blender"
	--	4 = "flask+blender"

	puzzleState = 1
end

-- Private function(s)

local function TriggerCutScene()
	state = "cutscene"
	globalCurrentVideo = cutScene
	globalCurrentVideo:Play()

	ChangeScene("Floor0_RatMaze")
end

local function ResetFlaskPosition()
	flask:SetRotation(0)
	flaskBack:SetRotation(0)
	flaskContent:SetRotation(0)

	flask:SetPos(flaskStartXPos, flaskStartYPos)
	flaskBack:SetPos(flaskStartXPos, flaskStartYPos)
	flaskContent:SetPos(flaskStartXPos, flaskStartYPos)
end

local function ResetFlaskContent()
	flaskContent.myColor = {0, 0, 0, 200}
	currentFlaskFill = 0
	redCountFilled = 0
	greenCountFilled = 0
	blueCountFilled = 0
	flaskContent.mySprite = love.graphics.newImage(flaskContent.mySpritePath .. "Floor0_Laboratory_ColorPuzzle_Liquid" .. currentFlaskFill .. ".png")
	startTimerWhenFlaskIsFilled = true
end

local function EmptyBottle()
	flask:SetRotation(-2)
	flaskBack:SetRotation(-2)
	flaskContent:SetRotation(0.04)

	flask:SetPos(600, 600)
	flaskBack:SetPos(600, 600)
	flaskContent:SetPos(600, 600)

	flaskContent.mySprite = flashContentEmptyingSprite

	if (startTimerWhenFlaskIsFilled == true) then
		Wait(2, ResetFlaskContent)
		Wait(2, ResetFlaskPosition)
		startTimerWhenFlaskIsFilled = false
	end
	PlaySound("EmptyFlask", true)
end

function DebugEmptyBottle()
	EmptyBottle()
	ResetFlaskContent()
end

local function AddRedColor()
	redCountFilled = redCountFilled + 1
	-- Updates the color
	flaskContent.myColor[1] = (redCountFilled / currentFlaskFill) * 255
	flaskContent.myColor[2] = (greenCountFilled / currentFlaskFill) * 255
	flaskContent.myColor[3] = (blueCountFilled / currentFlaskFill) * 255
end

local function AddGreenColor()
	greenCountFilled = greenCountFilled + 1
	-- Updates the color
	flaskContent.myColor[1] = (redCountFilled / currentFlaskFill) * 255
	flaskContent.myColor[2] = (greenCountFilled / currentFlaskFill) * 255
	flaskContent.myColor[3] = (blueCountFilled / currentFlaskFill) * 255
end

local function AddBlueColor()
	blueCountFilled = blueCountFilled + 1
	-- Updates the color
	flaskContent.myColor[1] = (redCountFilled / currentFlaskFill) * 255
	flaskContent.myColor[2] = (greenCountFilled / currentFlaskFill) * 255
	flaskContent.myColor[3] = (blueCountFilled / currentFlaskFill) * 255
end

local function RotateButtons(aDeltaTime)
		for index, button in ipairs(buttons) do
		if (buttonsRotationVariables["button" .. index].canRotate and
			buttonsRotationVariables["button" .. index].rotationStopTimer < buttonRotationTimerLimit and
			Floor0_Laboratory_ColorPuzzle.roomState == 2 and currentFlaskFill < 6) then
			if (button.myRotation < buttonRotationLimit) then
				button.myRotation = button.myRotation + buttonRotationSpeed
				PlaySound("ColorPuzzleCrank", true)
			else
				if (buttonsRotationVariables["button" .. index].rotationStopTimer == 0) then
					currentFlaskFill = currentFlaskFill + 1
					flaskContent.mySprite = love.graphics.newImage(flaskContent.mySpritePath .. "Floor0_Laboratory_ColorPuzzle_Liquid" .. currentFlaskFill .. ".png")

					if (index == 1)then
						AddRedColor()
					elseif (index == 2)then
						AddGreenColor()
					elseif (index == 3)then
						AddBlueColor()
					end
					print("r: " .. flaskContent.myColor[1] .. " - g: " .. flaskContent.myColor[2] .. " - b: " .. flaskContent.myColor[3])
				end

				buttonsRotationVariables["button" .. index].rotationStopTimer = buttonsRotationVariables["button" .. index].rotationStopTimer + aDeltaTime
			end
		elseif (buttonsRotationVariables["button" .. index].canRotate) then
			if (button.myRotation > 0) then
				button.myRotation = button.myRotation - buttonRotationSpeed
			else
				button.myRotation = 0
				buttonsRotationVariables["button" .. index].rotationStopTimer = 0
				buttonsRotationVariables["button" .. index].canRotate = false
			end
		end
	end
end

local function CheckFlaskContent(aDeltaTime)
	if (currentFlaskFill == 6 and triggerColorValues[1] == flaskContent.myColor[1] and
		triggerColorValues[2] == flaskContent.myColor[2] and triggerColorValues[3] == flaskContent.myColor[3]) then
		Floor0_Laboratory_ColorPuzzle.roomState = Floor0_Laboratory_ColorPuzzle.roomState + 1
		flaskContent.mySprite = LoadSprite("Graphics/Puzzle/Floor0_Laboratory_ColorPuzzle_Liquid_Glowing.png")
		flaskContent.myColor = { 75, 35, 90, 255 }
	end
end


local function DrinkShrinkingPotion()
	if (Floor0_Laboratory_ColorPuzzle.roomState == 2) then
		if (currentFlaskFill == 6) then
			GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), "The colour does not match. Let's try again!")

			EmptyBottle()
		end

	elseif (Floor0_Laboratory_ColorPuzzle.roomState == 3) then
		Floor0_Laboratory_ColorPuzzle.roomState = Floor0_Laboratory_ColorPuzzle.roomState + 1
		FadeOut(TriggerCutScene)
	end
end

-- Puzzle states
--	1 = "empty"
--	2 = "flask"
--	3 = "blender"
--	4 = "flask+blender"

local function PlaceFlask()
	if(puzzleState > 1) then
		puzzleState = 4
		objectsInWorld["Floor0_Laboratory_ColorPuzzle"].mySprite = LoadSprite("Graphics/Objects/Passages/Floor0_Laboratory_ColorPuzzleNuzzleFlask.png")
	else
		puzzleState = 2
		objectsInWorld["Floor0_Laboratory_ColorPuzzle"].mySprite = LoadSprite("Graphics/Objects/Passages/Floor0_Laboratory_ColorPuzzleFlask.png")
	end

	flaskBack.myColor[4] = 255

	-- BORDE ANVÄNDA OBJECT MED NIL INTERACTION, INTE BUTTON
	print("Replaced flask")
	flaskStartXPos = objectsInWorld["Floor0_Laboratory_ColorPuzzle_Flask"].myPosX
	flaskStartYPos = objectsInWorld["Floor0_Laboratory_ColorPuzzle_Flask"].myPosY
	flask = CreateButtonWithCallback("Floor0_Laboratory_ColorPuzzle_Flask", "Graphics/Puzzle/",
								 	flaskStartXPos,
								 	flaskStartYPos,
								 	DrinkShrinkingPotion)
	table.insert(Floor0_Laboratory_ColorPuzzle.GUIElements, flask)
	RemoveObjectFromScene("Floor0_Laboratory_ColorPuzzle_Flask")
	flaskContent.myPosX = flask.myPosX
	flaskBack.myPosX = flask.myPosX

	flaskContent.myPosY = flask.myPosY
	flaskBack.myPosY = flask.myPosY
end

local function PlaceBlender()
	if(puzzleState > 1) then
		puzzleState = 4
		objectsInWorld["Floor0_Laboratory_ColorPuzzle"].mySprite = LoadSprite("Graphics/Objects/Passages/Floor0_Laboratory_ColorPuzzleNuzzleFlask.png")
	else
		puzzleState = 3
		objectsInWorld["Floor0_Laboratory_ColorPuzzle"].mySprite = LoadSprite("Graphics/Objects/Passages/Floor0_Laboratory_ColorPuzzleNuzzle.png")
	end
	objectsInWorld["Floor0_Laboratory_ColorPuzzle_NozzleTank"].myColor[4] = 255
end


local function SetButtonsStartValues()
	local nozzleTankPosX = objectsInWorld["Floor0_Laboratory_ColorPuzzle_NozzleTank"].myPosX
	local nozzleTankPosY = objectsInWorld["Floor0_Laboratory_ColorPuzzle_NozzleTank"].myPosY
	local nozzleTankWidth = objectsInWorld["Floor0_Laboratory_ColorPuzzle_NozzleTank"].myWidth
	local nozzleTankHeight = objectsInWorld["Floor0_Laboratory_ColorPuzzle_NozzleTank"].myHeight

	for i = 1, GetTableSize(buttons), 1 do
		buttons[i].myPosY = nozzleTankPosY - nozzleTankHeight / 2 + 180
		buttons[i].myPosX = nozzleTankPosX - nozzleTankWidth / 2 + 35 + (100 * i) + (13 * i)
		buttons[i].myColor[4] = 255
	end
end

-- Public function(s)

function Floor0_Laboratory_ColorPuzzle_Load()
	Floor0_Laboratory_ColorPuzzle = LoadScene("Floor0_Laboratory_ColorPuzzle")
	Floor0_Laboratory_ColorPuzzle.OnEnter = Floor0_Laboratory_ColorPuzzle_OnEnter
	Floor0_Laboratory_ColorPuzzle.OnLeave = Floor0_Laboratory_ColorPuzzle_OnLeave
	Floor0_Laboratory_ColorPuzzle.OnUpdate = Floor0_Laboratory_ColorPuzzle_OnUpdate
	Floor0_Laboratory_ColorPuzzle.roomState = 1
	Floor0_Laboratory_ColorPuzzle.GUIElements = {}
	Floor0_Laboratory_ColorPuzzle.backgroundImage = love.graphics.newImage("Graphics/Backgrounds/" .. "Floor0_Laboratory_ColorPuzzle" .. ".png")

	--Hide flash
	objectsInWorld["Floor0_Laboratory_ColorPuzzle_Flask"].myColor = {255, 255, 255, 0}

	-- Create flask button
	CreateInteraction("Floor0_Laboratory_ColorPuzzle_Flask", "Floor0_Laboratory_Bottle", function()
		PlaceFlask()
		return false, true
	end)

	CreateInteraction("Floor0_Laboratory_ColorPuzzle", "Floor0_Laboratory_Bottle", function()
		PlaceFlask()
		return false, true
	end)


	objectsInWorld["Floor0_Laboratory_ColorPuzzle_NozzleTank"].myColor = {255, 255, 255, 0}
	-- Creates nozzle tank
	CreateInteraction("Floor0_Laboratory_ColorPuzzle_NozzleTank", "Floor0_Laboratory_Blender", function()
		PlaceBlender()
		return false, true
	end)

	CreateInteraction("Floor0_Laboratory_ColorPuzzle", "Floor0_Laboratory_Blender", function()
		PlaceBlender()
		return false, true
	end)

	--BackButton
	local arrowToLaboratory = CreateButtonWithCallback("BackButton", "Graphics/GUI/", 1805, 980, function()
		FadeOut(function() ChangeScene("Floor0_Laboratory") end)
	end)
	table.insert(Floor0_Laboratory_ColorPuzzle.GUIElements, arrowToLaboratory)

	-- Creates the flask's back
	flaskBack = CreatePicture("Floor0_Laboratory_ColorPuzzle_FlaskBottom", "Graphics/Puzzle/", flaskContent.myPosX, flaskContent.myPosY)
	flaskBack.myColor[4] = 0
	table.insert(Floor0_Laboratory_ColorPuzzle.GUIElements, flaskBack)
	-- Creates "flask content"
	flaskContent = CreatePicture("Floor0_Laboratory_ColorPuzzle_Liquid0", "Graphics/Puzzle/", 0, 0)
	flaskContent.myColor = {0, 0, 0, 240}
	table.insert(Floor0_Laboratory_ColorPuzzle.GUIElements, flaskContent)

	flashContentEmptyingSprite = LoadSprite("Graphics/Puzzle/Floor0_Laboratory_ColorPuzzle_LiquidDab.png")

	-- Creates buttons
	local noFlaskText = "I can't waste the pretty colours!"
	buttons[1] = CreateButtonWithCallback("Floor0_Laboratory_ColorPuzzle_RedButton", "Graphics/Puzzle/", 0, 0,
												 function()
												 		print("wot")
												 	if (flask.myPosX  == flaskStartXPos) then
												 		print("wot2")
												 		buttonsRotationVariables["button1"].canRotate = true
												 		globalCanPlayClickSound = false
												 	else
												 		print("wot3")
												 		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), noFlaskText)
												 	end
												 end)
	buttons[1].myColor[4] = 0
	table.insert(Floor0_Laboratory_ColorPuzzle.GUIElements, buttons[1])
	buttons[2] = CreateButtonWithCallback("Floor0_Laboratory_ColorPuzzle_GreenButton", "Graphics/Puzzle/", 0, 0,
												 function()
												 	if (flask.myPosX  == flaskStartXPos) then
												 		buttonsRotationVariables["button2"].canRotate = true
												 		globalCanPlayClickSound = false
												 	else
												 		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), noFlaskText)
												 	end
												 end)
	buttons[2].myColor[4] = 0
	table.insert(Floor0_Laboratory_ColorPuzzle.GUIElements, buttons[2])
	buttons[3] = CreateButtonWithCallback("Floor0_Laboratory_ColorPuzzle_BlueButton", "Graphics/Puzzle/", 0, 0,
												 function()
												 	if (flask.myPosX  == flaskStartXPos) then
												 		buttonsRotationVariables["button3"].canRotate = true
												 		globalCanPlayClickSound = false
												 	else
												 		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), noFlaskText)
												 	end
												 end)
	buttons[3].myColor[4] = 0
	table.insert(Floor0_Laboratory_ColorPuzzle.GUIElements, buttons[3])

	cutScene = CreateVideo("Shrinking", function() end)

	return Floor0_Laboratory_ColorPuzzle
end

function Floor0_Laboratory_ColorPuzzle_OnEnter()
	flashlightButton.myIsGrey = true
end

function Floor0_Laboratory_ColorPuzzle_OnLeave()
end

function Floor0_Laboratory_ColorPuzzle_OnUpdate(aDeltaTime)
	if (Floor0_Laboratory_ColorPuzzle.roomState >= 2) then
		RotateButtons(aDeltaTime)
	end

	if (Floor0_Laboratory_ColorPuzzle.roomState == 1) then
		if (objectsInWorld["Floor0_Laboratory_ColorPuzzle_NozzleTank"].myColor[4] == 255 and buttons[1].myColor[4] ~= 255) then
			SetButtonsStartValues()
		end

		if (GetTableSize(flask) ~= 0 and objectsInWorld["Floor0_Laboratory_ColorPuzzle_NozzleTank"].myColor[4] == 255) then
			Floor0_Laboratory_ColorPuzzle.roomState = 2
		end

	elseif (Floor0_Laboratory_ColorPuzzle.roomState == 2) then
		CheckFlaskContent(aDeltaTime)
	end
end

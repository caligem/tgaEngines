local mazeSize = 7
local gridOriginX, gridOriginY = 505, 75
local squareSize = 128
local myCharX, myCharY = 5, 6
local flashlightOffset = -50
local ratMaze = {}
local arrowSprites = {}
local moveButtons = {}
local wayToMove = "right"
local characterMoving = false
local ratMazeLightmap = nil
local angle = 0
local targetAngle = 0
local isNowNil = false

function ResetRatMaze() 
	mazeSize = 7
	gridOriginX, gridOriginY = 505, 75
	squareSize = 128
	myCharX, myCharY = 5, 6
	flashlightOffset = -50
	ratMaze = {}
	arrowSprites = {}
	moveButtons = {}
	wayToMove = "right"
	characterMoving = false
	angle = 0
	targetAngle = 0
	isNowNil = false
end

local function CanCharMove(aDirectionString)
	print("Testing move " .. aDirectionString .. " from X:" .. tostring(myCharX) .. " Y:" .. tostring(myCharY))
	if (aDirectionString == "up") and (myCharY >= 2) then
		return (ratMaze[myCharX][myCharY - 1].down)

	elseif (aDirectionString == "right") and (myCharX < mazeSize) then
		return (ratMaze[myCharX][myCharY].right)

	elseif (aDirectionString == "left") and (myCharX >= 2) then
		return (ratMaze[myCharX - 1][myCharY].right)

	elseif (aDirectionString == "down") and (myCharY < mazeSize)then
		return (ratMaze[myCharX][myCharY].down)
	else
		print("Direction not allowed")
		return false
	end
end
local function GetGridPosOrigin(aPosX, aPosY)
	return (gridOriginX + (aPosX)*squareSize - squareSize/2), (gridOriginY + (aPosY)*squareSize - squareSize/2)
end
local function Floor0_RatMaze_SetPos(aPosX, aPosY)
	print("Moving to pos X:" .. tostring(aPosX) .. "  Y:" .. tostring(aPosY))
	myCharX = aPosX
	myCharY = aPosY
	local posX, posY = GetGridPosOrigin(myCharX, myCharY)
	character.myPosX = posX
	character.myPosY = posY
	mazeLightShader:send("characterPos", {posX, posY})
end

local function MoveCharacter(aDeltaTime)
	if (wayToMove == "right") then
		targetAngle = targetAngle + math.pi/2
	elseif (wayToMove == "left") then
		targetAngle = targetAngle - math.pi/2
	elseif (wayToMove == "forward") then
		if ( CanCharMove(GetDirectionFromRotation(targetAngle))) then
			print(GetDirectionFromRotation(targetAngle))
			Floor0_RatMaze_SetPos( myCharX + round(math.cos(targetAngle)), myCharY + round(math.sin(targetAngle)) )
			PlaySound("Footsteps", true)
		elseif (GetDirectionFromRotation(targetAngle) == "left" and myCharX == 1 and myCharY == 3) then
			FadeOut(function() ChangeScene("Floor0_CatScene") end)
		end
	end
end

function Floor0_RatMaze_OnUpdate(aDeltaTime)
	StopCurrentMusic()
	
	if (ratMaze[myCharX][myCharY] == nil) and (isNowNil == false) then
		print("Shit Gejm")
		isNowNil = true
	end
	if (characterMoving == true) then
		characterMoving = false
		MoveCharacter(aDeltaTime)
	end

	angle = angle + (targetAngle-angle) * aDeltaTime * 10

	character.myRotation = angle
	--mazeLightShader:send("rotation", character.myRotation)
	mazeLightShader:send("rotation", angle)
	local flOffX = math.cos(character.myRotation) * flashlightOffset
	local flOffY = math.sin(character.myRotation) * flashlightOffset
	mazeLightShader:send("flashlightOffset", {flOffX, flOffY})
end

function Floor0_RatMaze_OnDraw(aSceneCanvas)
	if (flashlightTurnedOn == true and showFlashlight == true) then
		PushShader(mazeLightShader)
		love.graphics.draw(ratMazeLightmap, 0, 0)
		PopShader()
	else
		love.graphics.draw(ratMazeLightmap, 0, 0)
	end
end

function Floor0_RatMaze_Load()
	Floor0_RatMaze = LoadScene("Floor0_RatMaze")
	Floor0_RatMaze.OnEnter = Floor0_RatMaze_OnEnter
	Floor0_RatMaze.OnLeave = Floor0_RatMaze_OnLeave
	Floor0_RatMaze.OnUpdate = Floor0_RatMaze_OnUpdate
	Floor0_RatMaze.OnDraw = Floor0_RatMaze_OnDraw
	Floor0_RatMaze.roomState = 1
	Floor0_RatMaze.backgroundImage = LoadSprite("Graphics/Backgrounds/Floor0_RatMaze.png")
	ratMazeLightmap = LoadSprite("Graphics/Backgrounds/Floor0_RatMaze_DarknessMap.png")
	Floor0_RatMaze.GUIElements = {}

	CreateInteraction("Floor0_RatMaze_DollHead", nil, function()
		if (myCharX == 5) and (myCharY == 3) then
			PlaySound("RollingHead", false)
			MoveOverTime(objectsInWorld["Floor0_RatMaze_DollHead"], 2, 0, -128, function()
				ratMaze[4][3].right = true
				Floor0_RatMaze_SetPos(myCharX, myCharY)
				objectsInWorld["Floor0_RatMaze_DollHead"].myObjectType = "solid"
			end)
			return false
		end
	end)
	CreateInteraction("Floor0_RatMaze_Key", nil, function()
		if (myCharX == 1 )
		and ((myCharY == 6) or (myCharY == 7)) then
			AddObjectToInventory(objectsInWorld["Floor0_RatMaze_Key"])
		end
	end)


	CreateInteraction("Floor0_RatMaze_Teeth", "Floor0_RatMaze_Key", function()
		if (myCharX == 3) and (myCharY == 5) then
			objectsInWorld["Floor0_RatMaze_Teeth"].mySprite = LoadSprite("/Graphics/Puzzle/Floor0_RatMaze_SoulEater_With_Key.png")
			PlaySound("WindupToy", false)
			MoveOverTime(objectsInWorld["Floor0_RatMaze_Teeth"], 2, -128, 0, function()
				ratMaze[2][5].right = true
				Floor0_RatMaze_SetPos(myCharX, myCharY)
				objectsInWorld["Floor0_RatMaze_Teeth"].myObjectType = "solid"
			end)
			return false, true
		end
	end)

	-- Left Arrow
	leftArrow = CreateButtonWithCallback("Floor0_RatMaze_Arrow_Left","Graphics/Puzzle/", 1600, 605, function()
		if (characterMoving == false) then
			characterMoving = true
			wayToMove = "left"
		end
	end)
	table.insert(Floor0_RatMaze.GUIElements, leftArrow)

	rightArrow = CreateButtonWithCallback("Floor0_RatMaze_Arrow_Right","Graphics/Puzzle/", 1720, 600, function()
		if (characterMoving == false) then
			characterMoving = true
			wayToMove = "right"
		end
	end)
	table.insert(Floor0_RatMaze.GUIElements, rightArrow)

	-- Forward Arrow
	forwardArrow = CreateButtonWithCallback("Floor0_RatMaze_Arrow_Up","Graphics/Puzzle/", 1660, 550, function()
		if (characterMoving == false) then
			characterMoving = true
			wayToMove = "forward"
		end
	end)
	table.insert(Floor0_RatMaze.GUIElements, forwardArrow)

	-- Right Arrow

	-- Character
	character = CreateSolidObject("Floor0_RatMaze_Character", "", myCharX, myCharY)
	table.insert(Floor0_RatMaze.objects, character)

	grankula = CreateSolidObject("Floor0_RatMaze_Grankula", 958, 220, 1)
	table.insert(Floor0_RatMaze.objects, grankula)

	-- BLESS THIS MESS (Grid initialization)
	for x = 1, mazeSize, 1 do
		yColumn = {}
		for y = 1, mazeSize, 1 do
			mazeSquare = {
				down = false,
				right = false
			}
			table.insert(yColumn, mazeSquare)
		end
		table.insert(ratMaze, yColumn)
	end

	ratMaze[1][3].right = true
	ratMaze[1][6].down = true
	ratMaze[1][7].right = true

	ratMaze[2][3].down = true
	ratMaze[2][4].down = true
	ratMaze[2][7].right = true

	ratMaze[3][5].right = true
	ratMaze[3][5].down = true
	ratMaze[3][6].down = true

	ratMaze[4][3].down = true
	ratMaze[4][4].down = true
	ratMaze[4][4].right = true
	ratMaze[4][5].right = true

	ratMaze[5][1].down = true
	ratMaze[5][1].right = true
	ratMaze[5][2].down = true
	ratMaze[5][3].right = true
	ratMaze[5][4].down = true
	ratMaze[5][6].right = true

	ratMaze[6][1].down = true
	ratMaze[6][2].right = true
	ratMaze[6][3].right = true
	ratMaze[6][5].down = true
	ratMaze[6][5].right = true

	ratMaze[7][2].down = true
	ratMaze[7][3].down = true
	ratMaze[7][4].down = true

	character.myRotation = facingDirection
	Floor0_RatMaze_SetPos(myCharX, myCharY)
	return Floor0_RatMaze
end

function Floor0_RatMaze_OnEnter()
	flashlightButton.myIsGrey = false
	flashlightTurnedOn = false
	StopCurrentMusicWithFadeOut(0.5)
end

function Floor0_RatMaze_OnLeave()
end

globalPlayerScaredCat = false
globalCatIsInLivingroom = true
globalTVOn = true;
local colorTimer = 0
local televisionShader = nil
local scaredCatSprite = nil

function ResetLivingroom() 
	globalPlayerScaredCat = false
	globalCatIsInLivingroom = true
	colorTimer = 0
	scaredCatSprite = nil
end

function Floor1_Livingroom_Load()
	Floor1_Livingroom = LoadScene("Floor1_Livingroom")
	Floor1_Livingroom.OnEnter = Floor1_Livingroom_OnEnter
	Floor1_Livingroom.OnLeave = Floor1_Livingroom_OnLeave
	Floor1_Livingroom.OnUpdate = Floor1_Livingroom_OnUpdate
	Floor1_Livingroom.OnDraw = Floor1_Livingroom_OnDraw
	Floor1_Livingroom.roomState = 1
	Floor1_Livingroom.backgroundImage = love.graphics.newImage("Graphics/Backgrounds/" .. "Floor1_Livingroom" .. ".png")

	--TV shader
	televisionShader = love.graphics.newShader("Shaders/televisionShader.glsl")
	televisionLightmap1 = LoadSprite("Graphics/Backgrounds/Floor1_Livingroom_TvLightmap.png")
	televisionLightmap2 = LoadSprite("Graphics/Backgrounds/Floor1_Livingroom_TvLightmap1.png")
	televisionShader:send("tvLight", televisionLightmap1)
	televisionShader:send("tvLight2", televisionLightmap2)

	CreateInteraction("Floor1_Livingroom_BalloonBox", "Floor1_Livingroom_HeliumTank", function()
		ChangeScene("Floor1_Livingroom_BalloonPuzzle")
	end)

	scaredCatSprite = LoadSprite("Graphics/Objects/Inspectable/Floor1_Livingroom_Cat2.png")

	CreateInteraction("Floor1_Livingroom_Cat", "Floor1_Livingroom_PartyHorn", function()
		PlaySound("BlowingPartyHorn", true)
		Wait(0.5, function()
			globalPlayerScaredCat = true
			PlaySound("CatScaredMeow")
			objectsInWorld["Floor1_Livingroom_Cat"].mySprite = scaredCatSprite
			MoveToOverTime(objectsInWorld["Floor1_Livingroom_Cat"], 0.75, 1020, 450, function()
				RemoveObjectFromScene("Floor1_Livingroom_Cat")
			end)
		end)
		return false, true
	end)


	CreateInteraction("Floor1_Livingroom_Yarn", nil, function()
		if (globalPlayerScaredCat == true) then
			AddObjectToInventory(objectsInWorld["Floor1_Livingroom_Yarn"])
		else
			GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), "Whiskers shouldn't play with the yarn. It will get ruined!")
		end
	end)
	--[[balloonScale
	if (aObject.myName == "Floor1_Livingroom_Yarn" and globalPlayerScaredCat == false) then
		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), "Whiskers shouldn't play with the yarn. It will get ruined!")
		return
	end]]

	CreateInteraction("Floor1_Livingroom_BalloonPuzzle_BundleOfBalloons", "Floor1_Livingroom_Yarn", function()
		if (globalPlayerScaredCat == true) then
			AddObjectToInventory(CreateInventoryObject("Floor1_Livingroom_TiedBundleOfBalloons",
														"/Graphics/GUI/Inventory/Floor1_Livingroom_TiedBundleOfBalloons.png",
														"I like balloons.. They always lift the mood"))
			return true, true
		else
			GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), "Whiskers is playing with the yarn, I need to scare it away somehow..")
		end
	end)

	CreateInteraction("Floor1_Livingroom_TiedBundleOfBalloons", "Floor1_Livingroom_SafetyGate", function()
		PlaySound("SafetyGateOpen", false)
		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), 'There we go!')
		objectsInWorld["Floor1_Livingroom_SafetyGate"].mySprite = LoadSprite("Graphics/Objects/Passages/Floor1_Livingroom_SafetyGate_Open.png")
		scenes["Floor1_Livingroom"].roomState = 2
		return true, false
	end)

	return Floor1_Livingroom
end

function Floor1_Livingroom_OnUpdate(aDeltaTime)
	televisionShader:send("systemTime", os.clock())
	colorTimer = colorTimer - aDeltaTime

	if (colorTimer <= 0) then
		colorTimer = math.random(0.5, 2)

		local tvRed = math.random(100, 255)
		local tvGreen = math.random(200, 255)
		local tvBlue = math.random(150, 255)

		tvRed = tvRed/255
		tvGreen = tvGreen/255
		tvBlue = tvBlue/255

		televisionShader:send("shaderColor", {tvRed, tvGreen, tvBlue})
	end
end

local tvCanvas = love.graphics.newCanvas(love.graphics.getWidth())
function Floor1_Livingroom_OnDraw()
	love.graphics.setCanvas(tvCanvas)
	PushShader(televisionShader)
	love.graphics.draw(sceneCanvas)
	PopShader()
	love.graphics.setCanvas(sceneCanvas)
	love.graphics.draw(tvCanvas)
end


function Floor1_Livingroom_OnEnter()
	flashlightButton.myIsGrey = true
end

function Floor1_Livingroom_OnLeave()

end


local Floor1_Livingroom_BalloonPuzzle = {}

local backgroundImage = nil

-- Variable(s) for pressure meter
local pressureMeterImage = nil
local pressureMeterPosX = 0
local pressureMeterPosY = 0
local pressureMeterArrowImage = nil
local pressureMeterArrowRotation = 0.05
local preasureMeterRotationSpeed = 12

local successCounter = 0
pressingPressureMeterButton = false
isDraggingBalloon = false
draggedBalloon = {}
tankNozzle = {}
local tankHasBalloon = false

local balloon1 = {}
local balloon2 = {}
local balloon3 = {}
local blowUpBalloon = {}
local pressureMeterArrow = {}
local brokenBalloon = {}
local pressureButton = {}

-- Variable(s) for the balloon // Values only changes the first balloon (Don't change //Axel)
local balloonScale = 0
local balloonScaleSpeed = 0.5

-- Other variable(s)
local showText = false


function ResetBalloonPuzzle()
	Floor1_Livingroom_BalloonPuzzle = {}

	backgroundImage = nil

	-- Variable(s) for pressure meter
	pressureMeterImage = nil
	pressureMeterPosX = 0
	pressureMeterPosY = 0
	pressureMeterArrowImage = nil
	pressureMeterArrowRotation = 0.05
	preasureMeterRotationSpeed = 12

	successCounter = 0
	pressingPressureMeterButton = false
	isDraggingBalloon = false
	draggedBalloon = {}
	tankNozzle = {}
	tankHasBalloon = false

	balloon1 = {}
	balloon2 = {}
	balloon3 = {}
	blowUpBalloon = {}
	pressureMeterArrow = {}
	brokenBalloon = {}
	pressureButton = {}

	-- Variable(s) for the balloon // Values only changes the first balloon (Don't change //Axel)
	balloonScale = 0
	balloonScaleSpeed = 0.5

	-- Other variable(s)
	showText = false
end

-- Private function(s)

local function ResetBalloon()
	-- Pressure Meter
	-- pressureMeterArrowRotation = 0.05
	-- preasureMeterRotationSpeed = 12
	-- Balloon scale stuff
	balloonScale = 0
	balloonScaleSpeed = 0.5
	tankHasBalloon = false
	pressingPressureMeterButton = false
end

local function AddBrokenBalloon()
	brokenBalloon = CreatePicture("Floor1_Livingroom_BallonPuzzle_BrokenBalloon", "Graphics/Puzzle/", 1164, 664)
	brokenBalloon:SetOrigin(brokenBalloon.myWidth, brokenBalloon.myHeight /2)
	table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, brokenBalloon)
	PlaySound("BalloonExplosion", true)
end

local function AddBalloon(aSuccessCounter)
	if (aSuccessCounter == 1) then
		balloon1.myIsGrey = false
		PlaySound("BallonPickup", false)
	elseif (aSuccessCounter == 2) then
		balloon2.myIsGrey = false
		PlaySound("BallonPickup", false)
	elseif (aSuccessCounter == 3) then
		PlaySound("BallonPickup", false)
		balloon3.myIsGrey = false
		RemoveObjectFromInventory("Floor1_Livingroom_BalloonBox")
		AddObjectToInventory(CreateInventoryObject("Floor1_Livingroom_BalloonPuzzle_BundleOfBalloons",
												   "Graphics/GUI/Inventory/Floor1_Livingroom_BalloonPuzzle_BundleOfBalloons.png"))

		FadeOut(function() ChangeScene("Floor1_Livingroom")end)
	end
end

local function UpdatePressureMeter(aDeltaTime)
	if (love.mouse.isDown(1)) then
		PlaySound("BalloonInflation", false)
		preasureMeterRotationSpeed = preasureMeterRotationSpeed + 0.03
		balloonScaleSpeed = balloonScaleSpeed + 2

		pressureMeterArrowRotation = pressureMeterArrowRotation + preasureMeterRotationSpeed * aDeltaTime
		balloonScale = balloonScale + balloonScaleSpeed * aDeltaTime

		if (pressureMeterArrowRotation > math.pi * 2) then
			StopSound("BalloonInflation")
			ResetBalloon()
			AddBrokenBalloon()
		end

	elseif (love.mouse.isDown(1) == false) then
		if (pressureMeterArrowRotation >= 4.658 and pressureMeterArrowRotation <= 5.351) then
			StopSound("BalloonInflation")
			successCounter = successCounter + 1
			AddBalloon(successCounter)
			ResetBalloon()
		elseif (pressureMeterArrowRotation > 5.351) then
			StopSound("BalloonInflation")
			AddBrokenBalloon()
			ResetBalloon()
		else
			PauseSound("BalloonInflation")
		end
	end
end

-- Public function(s)

function TestPrint()
	print("TEST")
end

function Floor1_Livingroom_BalloonPuzzle_Load()
	Floor1_Livingroom_BalloonPuzzle =
	{
		myName = "Floor1_Livingroom_BalloonPuzzle",
		Load = Floor1_Livingroom_BalloonPuzzle_Load,
		OnEnter = Floor1_Livingroom_BalloonPuzzle_OnEnter,
		OnUpdate = Floor1_Livingroom_BalloonPuzzle_OnUpdate,
		OnDraw = Floor1_Livingroom_BalloonPuzzle_OnDraw,
		OnLeave = Floor1_Livingroom_BalloonPuzzle_OnLeave,
		objects = {},
		GUIElements = {},
		HandlePassages = Floor2_Hallway_HandlePassages,
		HandlePlacing = Floor2_Hallway_HandlePlacing,
		roomState = 1,
		backgroundImage = love.graphics.newImage("Graphics/Backgrounds/Floor1_Livingroom_BalloonPuzzle.png")
	}

	draggedBalloon = CreatePicture("Floor1_Livingroom_BallonPuzzle_Balloon","Graphics/Puzzle/", GetMousePosition())

	-- Add Balloons
	balloon1 = CreatePicture("Floor1_Livingroom_BallonPuzzle_FinishedBalloon","Graphics/Puzzle/", 480, 250)
	balloon1.myIsGrey = true
	table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, balloon1)

	balloon2 = CreatePicture("Floor1_Livingroom_BallonPuzzle_FinishedBalloon","Graphics/Puzzle/", 960, 250)
	balloon2.myIsGrey = true
	table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, balloon2)

	balloon3 = CreatePicture("Floor1_Livingroom_BallonPuzzle_FinishedBalloon","Graphics/Puzzle/", 1440, 250)
	balloon3.myIsGrey = true
	table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, balloon3)


	balloonHand = CreatePicture("Floor1_Livingroom_BallonPuzzle_Hand", "Graphics/Puzzle/", 250, 880)							--HAND
	table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, balloonHand)

	balloonPile = CreateButtonWithCallback("Floor1_Livingroom_BallonPuzzle_BalloonPile", "Graphics/Puzzle/", 270, 990, function() --BALLOON PILE
		if (isDraggingBalloon == false) then
			draggedBalloon:SetPos(GetMousePosition())
			table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, draggedBalloon)
			isDraggingBalloon = true
		end
	end)
	table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, balloonPile)

	local arrowToLivingroom = CreateButtonWithCallback("BackButton", "Graphics/GUI/", 1805, 980, function()
	 	FadeOut(function() ChangeScene("Floor1_Livingroom") end)
	end)
	table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, arrowToLivingroom)

	local pressureMeter = CreatePicture("Floor1_Livingroom_BallonPuzzle_PressureMeter", "Graphics/Puzzle/", 1400, 655)
	table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, pressureMeter)

	pressureButton = CreateButtonWithCallback("Floor1_Livingroom_BallonPuzzle_Button", "Graphics/Puzzle/", 1585, 565, function()
		pressingPressureMeterButton = true
	end)
	table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, pressureButton)

	tankNozzle = CreateButtonWithCallback("TankNozzle", "Graphics/Puzzle/", 1185, 667, function ()
		if (isDraggingBalloon and tankHasBalloon == false) then
			tankHasBalloon = true
			balloonScale = 0.2
			blowUpBalloon:SetScale(balloonScale)
		end

		brokenBalloon = {}
		for i = GetTableSize(Floor1_Livingroom_BalloonPuzzle.GUIElements), 1, -1 do
			if (Floor1_Livingroom_BalloonPuzzle.GUIElements[i].myName == "Floor1_Livingroom_BallonPuzzle_BrokenBalloon") then
				table.remove(Floor1_Livingroom_BalloonPuzzle.GUIElements, i)
			end
		end
	end)
	table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, tankNozzle)

	blowUpBalloon = CreatePicture("Floor1_Livingroom_BallonPuzzle_BlowUpBalloon", "Graphics/Puzzle/", 1164, 664)
	blowUpBalloon:SetScale(balloonScale)
	blowUpBalloon:SetOrigin(blowUpBalloon.myWidth, blowUpBalloon.myHeight /2)
	table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, blowUpBalloon)

	pressureMeterArrow = CreatePicture("Floor1_Livingroom_BallonPuzzle_Arrow", "Graphics/Puzzle/", 1403.5, 660)
	pressureMeterArrow:SetRotation(pressureMeterArrowRotation)
	table.insert(Floor1_Livingroom_BalloonPuzzle.GUIElements, pressureMeterArrow)

	return Floor1_Livingroom_BalloonPuzzle
end

function Floor1_Livingroom_BalloonPuzzle_OnUpdate(aDeltaTime)
	SortObjectsByDepth(Floor1_Livingroom_BalloonPuzzle.objects)

	if (balloonHand.myPosY > 830 and balloonHand.myIsHidden == false) then
		balloonHand.myPosY = balloonHand.myPosY - 2
		balloonPile.myPosY = balloonPile.myPosY - 2
		balloonHand.myPosX = balloonHand.myPosX + 1
		balloonPile.myPosX = balloonPile.myPosX + 1
	end
	if (pressingPressureMeterButton and tankHasBalloon) then
		UpdatePressureMeter(aDeltaTime)
		if (pressureButton.myRotation < 1) then
			pressureButton.myRotation = pressureButton.myRotation + math.pi * 2 * aDeltaTime
		end
	else
		if (pressingPressureMeterButton == true) then
			PlaySound("TankBlowingWithoutBalloon", false)
		else
			StopSound("TankBlowingWithoutBalloon")
		end

		if (pressingPressureMeterButton and tankHasBalloon == false and pressureMeterArrowRotation <= 1) then
			pressureMeterArrowRotation = pressureMeterArrowRotation + 4 * aDeltaTime

		-- reverse rotation when u let go of the button
		elseif (pressureMeterArrowRotation > 0 and tankHasBalloon == false) then
			pressureMeterArrowRotation = pressureMeterArrowRotation - 20 * aDeltaTime
			if (pressureMeterArrowRotation <= 0) then
				pressureMeterArrowRotation = 0.0
			end
			print(pressureMeterArrowRotation)
		end
	end
	if (love.mouse.isDown(1) == false) then
		pressingPressureMeterButton = false
	end
	if (pressingPressureMeterButton == true and pressureButton.myRotation < 1) then
		pressureButton.myRotation = pressureButton.myRotation + math.pi * 2 * aDeltaTime
	elseif (pressingPressureMeterButton == false and pressureButton.myRotation > 0) then
		pressureButton.myRotation = pressureButton.myRotation - math.pi * 2 * aDeltaTime
	end
	if (isDraggingBalloon == true) then
		local mouseX, mouseY = GetMousePosition()
		draggedBalloon:SetPos(mouseX - 37, mouseY + 75)
	end

	balloonScaleSpeed = 0.5
	pressureMeterArrowRotationSpeed = 0.5

	blowUpBalloon:SetScale(balloonScale)
	pressureMeterArrow:SetRotation(pressureMeterArrowRotation)
end

function Floor1_Livingroom_BalloonPuzzle_OnDraw()
	if (pressureMeterImage ~= nil and pressureButton.mySprite ~= nil and pressureMeterArrowImage ~= nil) then
		love.graphics.draw(pressureMeterImage, pressureMeterPosX, pressureMeterPosY, 0, 1, 1, pressureMeterImage:getWidth() / 2, pressureMeterImage:getHeight() / 2)
		love.graphics.draw(pressureButton.mySprite, pressureButton.myPosX, pressureButton.myPosY, 0, 1, 1, pressureButton.myWidth / 2, pressureButton.myHeight / 2)
		love.graphics.draw(pressureMeterArrowImage, pressureMeterPosX, pressureMeterPosY, pressureMeterArrowRotation, 1, 1,
			pressureMeterArrowImage:getWidth() / 2, pressureMeterArrowImage:getHeight() / 2)

		love.graphics.print(pressureMeterArrowRotation, 1000, 600)

		love.graphics.draw(balloonImage,
			500,
			500,
			-math.pi / 2,
			balloonScale,
			balloonScale,
			0 + (balloonImage:getWidth() * balloonScale) / (2 * balloonScale),
			balloonImage:getHeight() / 2 + (balloonImage:getHeight() * balloonScale) / (2 * balloonScale))
	end
end

function Floor1_Livingroom_BalloonPuzzle_OnEnter()
	showFlashlight = false

	if (DoesExistInTable(objectsInInventory, GetInventoryObject("Floor1_Livingroom_BalloonBox")) or
		(grabbedObject ~= nil and grabbedObject.myName == "Floor1_Livingroom_BalloonBox")) then
		showInventory = false
		balloonPile:Show()
		balloonHand:Show()
		balloon1:Show()
		balloon2:Show()
		balloon3:Show()
	else
		showInventory = false
		balloonPile:Hide()
		balloonHand:Hide()
		balloon1:Hide()
		balloon2:Hide()
		balloon3:Hide()
	end
end

function Floor1_Livingroom_BalloonPuzzle_OnLeave()
	showInventory = true
	showFlashlight = true
	startedUsingBalloonBox = false
end

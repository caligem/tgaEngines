local locCatClockRotatingRight = true

function ResetBedroom() 
	locCatClockRotatingRight = true
end

function FillChest()
	Floor2_Bedroom.roomState = Floor2_Bedroom.roomState + 1
	chestState = 4
	objectsInWorld["Floor2_Bedroom_ChestEmpty"].mySprite = LoadSprite("Graphics/Objects/Inspectable/Floor2_Bedroom_Chest.png")
end

function Floor2_Bedroom_Load()
	Floor2_Bedroom = LoadScene("Floor2_Bedroom")
	Floor2_Bedroom.OnEnter = Floor2_Bedroom_OnEnter
	Floor2_Bedroom.OnLeave = Floor2_Bedroom_OnLeave
	Floor2_Bedroom.OnUpdate = Floor2_Bedroom_OnUpdate
	Floor2_Bedroom.roomState = 1
	Floor2_Bedroom.backgroundImage = LoadSprite("Graphics/Backgrounds/Floor2_Bedroom.png")

	CreateInteraction("Floor2_Bedroom_MrTiny", "Floor2_Bedroom_Dollhouse", function()
		objectsInWorld['Floor2_Bedroom_Dollhouse'].mySprite = LoadSprite('Graphics/Objects/Inspectable/Floor2_Bedroom_Dollhouse2.png')
		--GUISetTextToPrint('There, now i can go find my daddy!')
		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), 'Sleep tight mr.Tiny!')
		Floor2_Bedroom.roomState = Floor2_Bedroom.roomState + 1
		return true, false
	end)

	--Chest states
	-- 1 = empty
	-- 2 = airplane
	-- 3 = duck
	-- 4 = airplane+duck
	local chestState = 1
	local catClockTail = CreateSolidObject("Floor2_Bedroom_CatTail", 388, 338, 7)
	table.insert(Floor2_Bedroom.objects, catClockTail)
	table.insert(objectsInWorld, catClockTail)
	SortObjectsByDepth(Floor2_Bedroom.objects)

	CreateInteraction("Floor2_Bedroom_ChestEmpty", "Floor2_Bedroom_Airplane", function()
		if (chestState > 1) then
			FillChest(Floor2_Bedroom_CatTail)
		else
			chestState = 2
			objectsInWorld["Floor2_Bedroom_ChestEmpty"].mySprite = LoadSprite("Graphics/Objects/Inspectable/Floor2_Bedroom_ChestAirplane.png")
		end
		return false, true
	end)

	CreateInteraction("Floor2_Bedroom_ChestEmpty", "Floor2_Bedroom_Duck", function()
		if (chestState > 1) then
			FillChest()
		else
			chestState = 3
			objectsInWorld["Floor2_Bedroom_ChestEmpty"].mySprite = LoadSprite("Graphics/Objects/Inspectable/Floor2_Bedroom_ChestDuck.png")
		end
		return false, true
	end)

	CreateInteraction("Floor2_Bedroom_ChestEmpty", "Floor2_Bedroom_MrTiny", function()
		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), "That's not where Mr.Tiny lives..")
	end)

	return Floor2_Bedroom
end

function Floor2_Bedroom_OnUpdate(aDeltaTime)
	PlayMusic("House")
	
	if (Floor2_Bedroom.roomState == 2) and (chestState == 4) then
		objectsInWorld["Floor2_Bedroom_Door"].myDescriptions["two"] = "I need to put mr.Tiny to bed first."
	elseif (Floor2_Bedroom.roomState 	== 2) and (chestState ~= 4) then
		objectsInWorld["Floor2_Bedroom_Door"].myDescriptions["two"] = "I need to tidy my room first."
	end

	if (locCatClockRotatingRight) then
		if (objectsInWorld["Floor2_Bedroom_CatTail"].myRotation >= -0.25) then
			objectsInWorld["Floor2_Bedroom_CatTail"]:SetRotation(objectsInWorld["Floor2_Bedroom_CatTail"].myRotation - (0.5 * aDeltaTime))
		else
			locCatClockRotatingRight = false
		end
	else
		if (objectsInWorld["Floor2_Bedroom_CatTail"].myRotation <= 0.25) then
			objectsInWorld["Floor2_Bedroom_CatTail"]:SetRotation(objectsInWorld["Floor2_Bedroom_CatTail"].myRotation + (0.5 * aDeltaTime))
		else
			locCatClockRotatingRight = true
		end
	end

end

function Floor2_Bedroom_OnEnter()
	flashlightButton.myIsGrey = true
end

function Floor2_Bedroom_OnLeave()

end

local fly = {}
local counter = 0

local colorTimer = 0

function ResetHallway()
	fly = {}
	counter = 0

	colorTimer = 0
end

function Floor2_Hallway_Load()
	Floor2_Hallway = LoadScene("Floor2_Hallway")
	Floor2_Hallway.OnEnter = Floor2_Hallway_OnEnter
	Floor2_Hallway.OnLeave = Floor2_Hallway_OnLeave
	Floor2_Hallway.OnUpdate = Floor2_Hallway_OnUpdate
	Floor2_Hallway.OnDraw = Floor2_Hallway_OnDraw
	Floor2_Hallway.roomState = 1
	Floor2_Hallway.backgroundImage = love.graphics.newImage("Graphics/Backgrounds/Floor2_Hallway.png")
	Floor2_Hallway.darknessMap = love.graphics.newImage("/Graphics/Backgrounds/Floor2_Hallway_DarknessMap.png")
	Floor2_Hallway.furthestPoint = {1150, 450}
	Floor2_Hallway.GUIElements = {}

	local lightswitchUp = LoadSprite("/Graphics/Objects/Inspectable/Floor2_Hallway_Lightswitch_Up.png")
	local lightswitchDown = LoadSprite("/Graphics/Objects/Inspectable/Floor2_Hallway_Lightswitch_Down.png")
	local lightswitch = false

		--TV shader
	televisionShader = love.graphics.newShader("Shaders/televisionShaderHallway.glsl")
	televisionLightmap1 = LoadSprite("/Graphics/Backgrounds/Floor2_Hallway_TvLightmap.png")
	televisionShader:send("tvLight", televisionLightmap1)

	CreateInteraction("Floor2_Hallway_Car", nil, function()
		local car = objectsInWorld["Floor2_Hallway_Car"]
		if(car.hasBatteries == nil) then
			car.hasBatteries = true
			car.mySprite = LoadSprite("Graphics/Objects/Inspectable/Floor2_Hallway_Car_Flipped.png")
		elseif (car.hasBatteries == true and HasEmptySlot() == true) then
			car.hasBatteries = false
			car.mySprite = LoadSprite("Graphics/Objects/Inspectable/Floor2_Hallway_Car_Empty.png")
			AddObjectToInventory(CreatePickupableObject("Floor2_Hallway_Batteries", {["one"] = "Hmm, i suppose i could use for something"}, 0,0,0,0))
		end
	end)

	CreateInteraction("Floor2_Hallway_Flashlight", "Floor2_Hallway_Batteries", function ()
		GUIManager_EnableFlashlight()
		return true, true
	end)

	CreateInteraction("Floor2_Hallway_Lightswitch_Down", nil,
	function()
		if (lightswitch == false) then
			objectsInWorld["Floor2_Hallway_Lightswitch_Down"].mySprite = lightswitchUp
			lightswitch = true
		else
			objectsInWorld["Floor2_Hallway_Lightswitch_Down"].mySprite = lightswitchDown
			lightswitch = false
		end
		local mouseX, mouseY = love.mouse.getPosition()
		GUIManager_PrintNewText(mouseX, mouseY, "This is not good! Seems like the light is out.")
	end)

	fly = CreatePicture("Fly", "Graphics/Animation/", 1600, 500)
	table.insert(Floor2_Hallway.GUIElements, fly)

	return Floor2_Hallway
end

function Floor2_Hallway_OnUpdate(aDeltaTime)
	PlaySound("FlyBuzz", false)
	televisionShader:send("systemTime", os.clock())
	counter = counter + (0.2 * aDeltaTime)
	fly.myPosX = 1600 + (100 * math.sin(4 * counter) * 1 * math.cos(1.23 *counter))
	fly.myPosY = 500 + (100 * math.sin(4 * counter) * 1 * math.sin(1.56 * counter))

	colorTimer = colorTimer - aDeltaTime

	if (colorTimer <= 0) then
		colorTimer = math.random(0.5, 2)

		local tvRed = math.random(100, 255)
		local tvGreen = math.random(200, 255)
		local tvBlue = math.random(150, 255)

		tvRed = tvRed/255
		tvGreen = tvGreen/255
		tvBlue = tvBlue/255

		televisionShader:send("shaderColor", {tvRed, tvGreen, tvBlue})
	end
end

local tvCanvas = love.graphics.newCanvas(love.graphics.getWidth())
function Floor2_Hallway_OnDraw()
	love.graphics.setCanvas(tvCanvas)
	PushShader(televisionShader)
	love.graphics.draw(sceneCanvas)
	PopShader()
	love.graphics.setCanvas(sceneCanvas)
	love.graphics.draw(tvCanvas)
end

function Floor2_Hallway_OnEnter()
	flashlightButton.myIsGrey = false
end

function Floor2_Hallway_OnLeave()
	flashlightButton.myIsGrey = true
	StopSound("FlyBuzz")
end

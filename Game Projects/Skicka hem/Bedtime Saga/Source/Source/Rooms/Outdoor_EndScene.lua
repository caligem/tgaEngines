local credits = nil
local hasStartedEndScene = false
local hasStartedCredits = false
local leftFlowerCut = false
local rightFlowerCut = false

function ResetEndScene()
	credits = nil
	hasStartedEndScene = false
	hasStartedCredits = false
	leftFlowerCut = false
	rightFlowerCut = false
end

function Outdoor_EndScene_Load()
	Outdoor_EndScene = LoadScene("Outdoor_EndScene")
	Outdoor_EndScene.OnEnter = Outdoor_EndScene_OnEnter
	Outdoor_EndScene.OnLeave = Outdoor_EndScene_OnLeave
	Outdoor_EndScene.OnUpdate = Outdoor_EndScene_OnUpdate
	Outdoor_EndScene.roomState = 1
	Outdoor_EndScene.backgroundImage = love.graphics.newImage("Graphics/Backgrounds/" .. "Outdoor_EndScene" .. ".png")
	Outdoor_EndScene.darknessMap = love.graphics.newImage("/Graphics/Backgrounds/Outdoor_EndScene_DarknessMap.png")
	Outdoor_EndScene.furthestPoint = {975, 300}
	objectsInWorld["Outdoor_EndScene_Web"].myObjectType = "solid"
	objectsInWorld["Outdoor_EndScene_Rathole"].myObjectType = "solid"


	CreateInteraction("Outdoor_EndScene_Stick", "Outdoor_EndScene_Soda_Can",
	function()
		objectsInWorld["Outdoor_EndScene_Soda_Can"].mySprite = LoadSprite("/Graphics/Objects/Inspectable/Outdoor_EndScene_Soda_Can_Without_Thing.png")
		objectsInWorld["Outdoor_EndScene_Soda_Can"].myObjectType = "solid"
		AddObjectToInventory(CreateInventoryObject("Outdoor_EndScene_Pulltab", "/Graphics/GUI/Inventory/Outdoor_EndScene_Pulltab.png", "Not so shiny"))
		PlaySound("CanAndStickCombination", false)
		return true, false
	end)

	CreateInteraction("Outdoor_EndScene_Pulltab_Sharpened", "Outdoor_EndScene_LeftFlower",
	function()
		PlaySound("CutDownFlower", true)
		objectsInWorld["Outdoor_EndScene_LeftFlower"].mySprite = LoadSprite("/Graphics/Objects/Inspectable/Outdoor_EndScene_LeftFlower_Cut.png")
		leftFlowerCut = true
		objectsInWorld["Outdoor_EndScene_LeftFlower"].myObjectType = "solid"
		if (rightFlowerCut == true) then
			Outdoor_EndScene.roomState = 2
		else
			objectsInWorld["Outdoor_EndScene_Web"].mySprite = LoadSprite("/Graphics/Objects/Inspectable/Outdoor_EndScene_Web_LeftCut.png")
		end
		return false, false
	end)

	CreateInteraction("Outdoor_EndScene_Pulltab_Sharpened", "Outdoor_EndScene_Trapped_Parent", function()
		GUIManager_PrintNewText(objectsInWorld["Outdoor_EndScene_Trapped_Parent"].myPosX,
								objectsInWorld["Outdoor_EndScene_Trapped_Parent"].myPosY,
								"Ouw! Please don't do that darling...")
	end)

	CreateInteraction("Outdoor_EndScene_Pulltab_Sharpened", "Outdoor_EndScene_RightFlower",
	function()
		PlaySound("CutDownFlower", true)
		objectsInWorld["Outdoor_EndScene_RightFlower"].mySprite = LoadSprite("/Graphics/Objects/Inspectable/Outdoor_EndScene_RightFlower_Cut.png")
		rightFlowerCut = true
		objectsInWorld["Outdoor_EndScene_RightFlower"].myObjectType = "solid"
		if (leftFlowerCut == true) then
			Outdoor_EndScene.roomState = 2
		else
			objectsInWorld["Outdoor_EndScene_Web"].mySprite = LoadSprite("/Graphics/Objects/Inspectable/Outdoor_EndScene_Web_RightCut.png")
		end
		return false, false
	end)

	CreateInteraction("Outdoor_EndScene_Dad_Down", nil,
	function()
			globalCurrentVideo = orgEndScene
			globalCurrentVideo:Play()
			hasStartedEndScene = true
			state = "cutscene"
	end)

	CreateInteraction("Outdoor_EndScene_Pulltab", "Outdoor_EndScene_LeftFlower",
	function()
		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), "It's not sharp enough.")
	end)

	CreateInteraction("Outdoor_EndScene_Pulltab", "Outdoor_EndScene_RightFlower",
	function()
		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), "It's not sharp enough.")
	end)

	CreateInteraction("Outdoor_EndScene_Pulltab", "Outdoor_EndScene_Web",
	function()
		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), "It's the flowerstalks that keep him up.")
	end)

	CreateInteraction("Outdoor_EndScene_Pulltab_Sharpened", "Outdoor_EndScene_Web",
	function()
		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), "It's the flowerstalks that keep him up.")
	end)

	CreateInteraction("Outdoor_EndScene_Pulltab_Sharpened", "Outdoor_EndScene_Web_LeftCut",
	function()
		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), "It's the flowerstalks that keep him up.")
	end)

	CreateInteraction("Outdoor_EndScene_Pulltab_Sharpened", "Outdoor_EndScene_Web_RightCut",
	function()
		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), "It's the flowerstalks that keep him up.")
	end)

	CreateInteraction("Outdoor_EndScene_Pulltab_Sharpened", "Outdoor_EndScene_Firefly_Right",
	function()
		GUIManager_PrintNewText(love.mouse.getX(), love.mouse.getY(), "I dont want to hurt anyone.")
	end)

	CreateInteraction("Outdoor_EndScene_Rathole", nil,
	function()
		if (Outdoor_EndScene.roomState == 3) then
			globalCurrentVideo = altEndScene
			globalCurrentVideo:Play()
			hasStartedEndScene = true
			state = "cutscene"
		end
	end)

	credits = CreateVideo("Credits", function() end, "mainmenu")

	altEndScene = CreateVideo("AltEndScene", function() end)

	orgEndScene = CreateVideo("EndScene", function() end)

	return Outdoor_EndScene
end

function Outdoor_EndScene_OnUpdate()
	if(showFlashlight == true) then
		print("Oliver pls reset stuff, showFlashlight = true")
	else
		ChangeScene("Floor2_Bedroom")
	end
	if (Outdoor_EndScene.roomState == 2) then
		RemoveObjectFromInventory("Outdoor_EndScene_Pulltab_Sharpened")
		RemoveObjectFromScene("Outdoor_EndScene_LeftFlower")
		RemoveObjectFromScene("Outdoor_EndScene_RightFlower")
		RemoveObjectFromScene("Outdoor_EndScene_Web")
		RemoveObjectFromScene("Outdoor_EndScene_Trapped_Parent")
		DadDown = CreateInspectableObject("Outdoor_EndScene_Dad_Down", { ["one"] = "YEY"}, 1024, 1022, 1)
		--objectsInWorld["Outdoor_EndScene_Dad_Down"].myObjectType = "solid"
		table.insert(Outdoor_EndScene.objects, DadDown)
		Outdoor_EndScene.roomState = 3
	end

	if (hasStartedCredits == false and hasStartedEndScene == true and globalCurrentVideo:IsPlaying() == false) then
			globalCurrentVideo = credits
			globalCurrentVideo:Play()
			hasStartedCredits = true
			state = "cutscene"
	end

	if (hasStartedCredits == true and globalCurrentVideo:IsPlaying() == false) then
		state = "mainmenu"
	end

end

function Outdoor_EndScene_OnEnter()
	flashlightButton.myIsGrey = false
	globalAllowClick = false
	GUIManager_FlashlightOn()
end

function Outdoor_EndScene_OnLeave()
	globalAllowClick = true
end

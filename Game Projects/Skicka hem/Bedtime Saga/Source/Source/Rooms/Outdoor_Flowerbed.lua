function ResetFlowerBed()
	--Nothing to reset
end

function Outdoor_Flowerbed_Load()
	Outdoor_Flowerbed = LoadScene("Outdoor_Flowerbed")
	Outdoor_Flowerbed.OnEnter = Outdoor_Flowerbed_OnEnter
	Outdoor_Flowerbed.OnLeave = Outdoor_Flowerbed_OnLeave
	Outdoor_Flowerbed.OnUpdate = Outdoor_Flowerbed_OnUpdate
	Outdoor_Flowerbed.roomState = 1
	Outdoor_Flowerbed.backgroundImage = love.graphics.newImage("Graphics/Backgrounds/" .. "Outdoor_Flowerbed" .. ".png")
	Outdoor_Flowerbed.darknessMap = love.graphics.newImage("/Graphics/Backgrounds/Outdoor_Flowerbed_DarknessMap.png")
	Outdoor_Flowerbed.dark = love.graphics.newImage("/Graphics/Backgrounds/Outdoor_Flowerbed_Dark.png")
	Outdoor_Flowerbed.furthestPoint = {1000, 350}

	CreateInteraction("Outdoor_Flowerbed_Sticks", "Outdoor_Flowerbed_Gum",
	function()
		if (globalStickCount == 4) then
			AddObjectToInventory(CreateInventoryObject("Outdoor_Flowerbed_Ladder",
														"/Graphics/GUI/Inventory/Outdoor_Flowerbed_Ladder.png",
														"Next lvl ladder, #prog art"))
			return true, true
		end
	end)

	CreateInteraction("Outdoor_Flowerbed_Ladder", "Outdoor_Flowerbed_Trelli",
	function()
		PlaySound("RepairBrokenTrelli", false)
		Outdoor_Flowerbed.roomState = 3
		objectsInWorld["Outdoor_Flowerbed_Trelli"].mySprite = LoadSprite("/Graphics/Objects/Passages/Outdoor_Flowerbed_Repaired_Trelli.png")
		return true, false
	end)

	CreateInteraction("Outdoor_EndScene_Pulltab", "Outdoor_Flowerbed_GnomeGrindingStone",
	function()
		AddObjectToInventory(CreateInventoryObject("Outdoor_EndScene_Pulltab_Sharpened", "/Graphics/GUI/Inventory/Outdoor_EndScene_Pulltab_Sharpened.png", "So shiny, mMm"))
		PlaySound("GrindingStone", false)
		return true, false
	end)

	return Outdoor_Flowerbed
end

function Outdoor_Flowerbed_OnUpdate()
	if (globalStickCount == 0) then
		local stickText = "I can count!... Stick one."
		objectsInWorld["Outdoor_Flowerbed_Stick1"].myDescriptions["one"] = stickText
		objectsInWorld["Outdoor_Flowerbed_Stick2"].myDescriptions["one"] = stickText
		objectsInWorld["Outdoor_Flowerbed_Stick3"].myDescriptions["one"] = stickText
		objectsInWorld["Outdoor_Flowerbed_Stick4"].myDescriptions["one"] = stickText
	elseif (globalStickCount == 1) then
		local stickText = "Stick two..."
		objectsInWorld["Outdoor_Flowerbed_Stick1"].myDescriptions["one"] = stickText
		objectsInWorld["Outdoor_Flowerbed_Stick2"].myDescriptions["one"] = stickText
		objectsInWorld["Outdoor_Flowerbed_Stick3"].myDescriptions["one"] = stickText
		objectsInWorld["Outdoor_Flowerbed_Stick4"].myDescriptions["one"] = stickText
	elseif (globalStickCount == 2) then
		local stickText = "Another stick! Stick three!"
		objectsInWorld["Outdoor_Flowerbed_Stick1"].myDescriptions["one"] = stickText
		objectsInWorld["Outdoor_Flowerbed_Stick2"].myDescriptions["one"] = stickText
		objectsInWorld["Outdoor_Flowerbed_Stick3"].myDescriptions["one"] = stickText
		objectsInWorld["Outdoor_Flowerbed_Stick4"].myDescriptions["one"] = stickText
	elseif (globalStickCount == 3) then
		local stickText = "The last stick. I call you number four!"
		objectsInWorld["Outdoor_Flowerbed_Stick1"].myDescriptions["one"] = stickText
		objectsInWorld["Outdoor_Flowerbed_Stick2"].myDescriptions["one"] = stickText
		objectsInWorld["Outdoor_Flowerbed_Stick3"].myDescriptions["one"] = stickText
		objectsInWorld["Outdoor_Flowerbed_Stick4"].myDescriptions["one"] = stickText

	end
end

function Outdoor_Flowerbed_OnEnter()
	flashlightButton.myIsGrey = false
	showInventory = true
	globalAllowClick = false
	GUIManager_FlashlightOn()
end

function Outdoor_Flowerbed_OnLeave()
	globalAllowClick = true
end

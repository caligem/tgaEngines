require("Source/Rooms/Floor2_Bedroom")
require("Source/Rooms/Floor2_Hallway")
require("Source/Rooms/Floor1_Livingroom")
require("Source/Rooms/Floor1_Livingroom_BalloonPuzzle")
require("Source/Rooms/Floor0_Laboratory")
require("Source/Rooms/Floor0_Laboratory_ColorPuzzle")
require("Source/Rooms/Floor0_RatMaze")
require("Source/Rooms/Floor0_CatScene")
require("Source/Rooms/Outdoor_Flowerbed")
require("Source/Rooms/Outdoor_EndScene")


--[[
----------- RoomScenes ----------
1. Floor2_Bedroom
2. Floor2_Hallway
3. Floor1_LivingRoom
4. Floor1_LivingRoom_BallonPuzzle
5. Floor0_Laboratory
6. Floor0_Laboratory_ColorPuzzle
7. Floor0_RatMaze
8. Floor0_CatScene
9. Outdoor_Flowerbed
10. Outdoor_EndScene
]]--

local moveBuffer = {}
local scaleBuffer = {}
local rotateBuffer = {}

local UpdateMoveBuffers
local UpdateScaleBuffer
local UpdateRotationBuffer

sceneCanvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight())
local flashlightCanvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight())

function ResetSceneManager()
	moveBuffer = {}
	scaleBuffer = {}
	rotateBuffer = {}
	sceneCanvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight())
	flashlightCanvas = love.graphics.newCanvas(love.graphics.getWidth(), love.graphics.getHeight())

	UpdateMoveBuffer = function(aDeltaTime)
		local indicesToEmpty = {}
		for i, movement in ipairs(moveBuffer) do
			movement.duration = movement.duration - aDeltaTime
			movement.object.myPosX = movement.object.myPosX + movement.moveSpeedX*aDeltaTime
			movement.object.myPosY = movement.object.myPosY + movement.moveSpeedY*aDeltaTime

			if(movement.duration <= 0) then
				table.insert(indicesToEmpty, i)
				print("Moving of " .. tostring(movement.object.myName) .. " complete")
				if(movement.callback ~= nil) then
					movement.callback()
				end
			end
		end
		for _, index in ipairs(indicesToEmpty) do
			table.remove(moveBuffer, i)
		end
	end

	UpdateAngleBuffer = function(aDeltaTime)
		local indicesToEmpty = {}

		for i, angle in ipairs(rotateBuffer) do
			angle.duration = angle.duration - aDeltaTime


			angle.object.myRotation = WrapRadAngle(angle.object.myRotation + angle.rotationSpeed * aDeltaTime)

			if(angle.duration <= 0) then
				print("Rotation of " .. tostring(angle.object.myName) .. " complete")
				if(angle.callback ~= nil) then
					angle.callback()
				end
				table.insert(indicesToEmpty, i)
			end
		end
		for _, index in ipairs(indicesToEmpty) do
			table.remove(rotateBuffer, i)
		end
	end

	UpdateRotationBuffer = function(aDeltaTime)
		local indicesToEmpty = {}

		for i, rotation in ipairs(rotateBuffer) do
			rotation.duration = rotation.duration - aDeltaTime
			rotation.object.myRotation = WrapRadAngle(rotation.object.myRotation + rotation.rotationSpeed * aDeltaTime)

				if(rotation.duration <= 0) then
					print("Rotation of " .. tostring(rotation.object.myName) .. " complete")
					if(rotation.callback ~= nil) then
						rotation.callback()
					end
					table.insert(indicesToEmpty, i)
				end
		end
		for _, index in ipairs(indicesToEmpty) do
			table.remove(rotateBuffer, i)
		end
	end

	UpdateScaleBuffer = function(aDeltaTime)
		local indicesToEmpty = {}

		for i, scaling in ipairs(scaleBuffer) do
			scaling.duration = scaling.duration - aDeltaTime
			scaling.object.myScaleX = scaling.object.myScaleX + deltaScaleX * aDeltaTime
			scaling.object.myScaleY = scaling.object.myScaleY + deltaScaleY * aDeltaTime

			if(scaling.duration <= 0) then
				print("Scaling of " .. tostring(scaling.object.myName) .. " complete")
				if(scaling.callback ~= nil) then
					scaling.callback()
				end
				table.insert(indicesToEmpty, i)
			end
		end
		for _, index in ipairs(indicesToEmpty) do
			table.remove(scaleBuffer, i)
		end
	end
end

function SceneManagerLoad()
	scenes["Floor2_Bedroom"] = Floor2_Bedroom_Load()
	print("\n")
	scenes["Floor2_Hallway"] = Floor2_Hallway_Load()
	print("\n")
	scenes["Floor1_Livingroom"] = Floor1_Livingroom_Load()
	print("\n")
	scenes["Floor1_Livingroom_BalloonPuzzle"] = Floor1_Livingroom_BalloonPuzzle_Load()
	print("\n")
	scenes["Floor0_Laboratory"] = Floor0_Laboratory_Load()
	print("\n")
	scenes["Floor0_Laboratory_ColorPuzzle"] = Floor0_Laboratory_ColorPuzzle_Load()
	print("\n")
	scenes["Floor0_RatMaze"] = Floor0_RatMaze_Load()
	print("\n")
	scenes["Floor0_CatScene"] = Floor0_CatScene_Load()
	print("\n")
	scenes["Outdoor_Flowerbed"] = Outdoor_Flowerbed_Load()
	print("\n")
	scenes["Outdoor_EndScene"] = Outdoor_EndScene_Load()
	print("\n")

	scenes[currentScene].OnEnter()
	if(scenes[currentScene].furthestPoint ~= nil) then
		flashlightShader:send("furthestPoint", scenes[currentScene].furthestPoint)
	end
	if(scenes[currentScene].darknessMap ~= nil) then
		flashlightShader:send("darknessMap", scenes[currentScene].darknessMap)
		flashlightShader:send("darknessSize", {scenes[currentScene].darknessMap:getWidth(), scenes[currentScene].darknessMap:getHeight()})
	end

	flashlightShader:send("resolution", {love.graphics.getWidth(), love.graphics.getHeight()})
end

function SceneManagerUpdate(aDeltaTime)
	UpdateMoveBuffer(aDeltaTime)
	UpdateRotationBuffer(aDeltaTime)
	UpdateScaleBuffer(aDeltaTime)
	UpdateAngleBuffer(aDeltaTime)

	if (scenes[currentScene].OnUpdate ~= nil) then
		scenes[currentScene].OnUpdate(aDeltaTime)
	end
end

function SceneManagerDraw()
	love.graphics.setCanvas(sceneCanvas)
	love.graphics.clear()

	--Draw background
	love.graphics.draw(scenes[currentScene].backgroundImage, 0, 0)

	-- Draw objects
	if (GetTableSize(scenes[currentScene].objects) ~= nil) then
		for i, object in ipairs(scenes[currentScene].objects) do

			--Outline
			if (hoveredObject ~= nil and object.myName == hoveredObject.myName and object.myObjectType == "pickupable") then
				if (globalAllowClick == true or (currentScene ~= "Outdoor_Flowerbed" and currentScene ~= "Outdoor_EndScene" and currentScene ~= "Floor0_CatScene")) then
					outlineShader:send( "size",{object.myWidth,object.myHeight} )
					PushShader(outlineShader)
					object.Draw()
					PopShader()
				end
				object.Draw()

			--Grey out balloons
			elseif (object.myName == "Floor1_Livingroom_BallonPuzzle_Balloon") and (object.myIsGrey == true) then
					PushShader(greyscaleShader)
					object.Draw()
					PopShader()
			else
				object.Draw()
			end
		end
	end

	-- Custom scene draws
	if (scenes[currentScene].OnDraw ~= nil) then
		scenes[currentScene].OnDraw()
	end

	if (scenes[currentScene].darknessMap ~= nil and scenes[currentScene].furthestPoint ~= nil) then
		if (flashlightTurnedOn == true and showFlashlight == true) then
			love.graphics.setCanvas(flashlightCanvas)
			flashlightShader:send("mousePos", {GetMousePosition()})
			PushShader(flashlightShader)
			love.graphics.draw(sceneCanvas)
			PopShader()
			love.graphics.setCanvas(sceneCanvas)
			love.graphics.draw(flashlightCanvas)
		else
			if (currentScene == "Outdoor_Flowerbed") then
				love.graphics.draw(scenes[currentScene].dark)
			else
				love.graphics.draw(scenes[currentScene].darknessMap)
			end
		end
	end

	love.graphics.setCanvas()
	love.graphics.draw(sceneCanvas)
end

function ChangeScene(aSceneDestinationName)
	-- Replaces the music depending on the destination.
	if (aSceneDestinationName == "Floor1_Livingroom") then
		ReplaceMusicWithFadeOut("House", 0.5)
	elseif (aSceneDestinationName == "Floor0_Laboratory") then
		ReplaceMusicWithFadeOut("LaboratoryAndCatScene", 0.5)
	elseif (aSceneDestinationName == "Floor0_CatScene") then
		ReplaceMusicWithFadeOut("LaboratoryAndCatScene", 0.5)
	elseif (aSceneDestinationName == "Outdoor_Flowerbed") then
		ReplaceMusicWithFadeOut("Outdoor", 0.5)
	end

	GUIManager_ForceDialogBoxesToDissapear(true)

	
	scenes[currentScene].OnLeave()
	GUIManager_FlashlightOff()

	currentScene = aSceneDestinationName

	scenes[currentScene].OnEnter()

	if(scenes[currentScene].furthestPoint ~= nil) then
		flashlightShader:send("furthestPoint", scenes[currentScene].furthestPoint)
	end
	if(scenes[currentScene].darknessMap ~= nil) then
		flashlightShader:send("darknessMap", scenes[currentScene].darknessMap)
		flashlightShader:send("darknessSize", {scenes[currentScene].darknessMap:getWidth(), scenes[currentScene].darknessMap:getHeight()})
	end
end

function FadeToScene(aSceneName)
	FadeOut(function() ChangeScene(aSceneName) end)
end

function RemoveObjectFromWorld(aObjectName)
	for sceneIndex = GetTableSize(scenes), 1, -1 do
		for objectIndex = GetTableSize(scenes[sceneIndex].objects), 1, -1 do
			if (scenes[sceneIndex].objects[objectIndex].myName == aObjectName) then
				table.remove(scenes[sceneIndex].objects, objectIndex)
				break
			end
		end
	end
end

function RemoveObjectFromScene(aObjectName)
	for objectIndex = GetTableSize(scenes[currentScene].objects), 1, -1 do
		if (scenes[currentScene].objects[objectIndex].myName == aObjectName) then
			table.remove(scenes[currentScene].objects, objectIndex)
			break
		end
	end
end

function RemoveObjectFromOtherScene(aObjectName, aSceneName)
	for objectIndex = GetTableSize(scenes[aSceneName].objects), 1, -1 do
		if (scenes[aSceneName].objects[objectIndex].myName == aObjectName) then
			table.remove(scenes[aSceneName].objects, objectIndex)
			break
		end
	end
end

function ObjectInCurrentSceneExist(aObjectName)
	for j, object in ipairs(scenes[currentScene].objects) do
			if (object.myName == aObjectName) then
				return true
			end
		end

	return false
end

function SortObjectsByDepth(aObjectsInScene)
	sortedTable = {}

	for i, obj in pairs(aObjectsInScene) do
		if (i > 1) then
			local inserted = false
			for j, sortedObj in ipairs(sortedTable) do
				if (obj.myDepthLevel > sortedObj.myDepthLevel) then
					table.insert(sortedTable, j, obj)
					inserted = true
					break
				end
			end
			if not inserted then
				table.insert(sortedTable, obj)
			end
		else
			table.insert(sortedTable, 1, obj)
		end
	end

	for i in pairs (aObjectsInScene) do
    	aObjectsInScene[i] = nil
	end

	for i in pairs (sortedTable) do
    	table.insert(aObjectsInScene, sortedTable[i])
	end
end

function MoveOverTime(aObject, aDuration, aDistX, aDistY, aCallback)
	local movement = {}
	movement.object = aObject
	movement.duration = aDuration
	movement.moveSpeedX = aDistX / aDuration
	movement.moveSpeedY = aDistY / aDuration
	movement.callback = aCallback
	table.insert(moveBuffer, movement)
	print("Moving " .. tostring(aObject.myName) .. " X: " .. tostring(aDistX) .. " Y: " .. tostring(aDistY))
end

function MoveToOverTime(aObject, aDuration, aPosX, aPosY, aCallback)
	MoveOverTime(aObject, aDuration, aPosX - aObject.myPosX, aPosY - aObject.myPosY, aCallback)
end

function ScaleOverTime(aObject, aDuration, aTargetScaleX, aTargetScaleY, aCallback)
	local scaling = {}
	scaling.object = aObject
	scaling.duration = aDuration
	scaling.deltaScaleX = (aTargetScaleX-aObject.myScaleX) / aDuration
	scaling.deltaScaleY = (aTargetScaleY-aObject.myScaleY) / aDuration
	scaling.callback = aCallback
	table.insert(scaleBuffer, scaling)
	print("Scaling " .. tostring(aObject.myName) .. " X:" .. tostring(aDistX) .. " Y: " .. tostring(aDistY))
end

function ScaleOverTime(aObject, aDuration, aTargetScale, aCallback)
    ScaleOverTime(aObject, aDuration, aTargetScale, aTargetScale, aCallback)
end

function RotateToAngle(aObject, aDuration, aTargetRotationName, aCallback)
	local targetRotation = GetDirectionFromName(aTargetRotationName)
    local angle = {}
    angle.targetRotation = targetRotation
    angle.targetRotationName = aTargetRotationName 
    angle.object = aObject
    angle.duration = aDuration
    angle.rotationSpeed = aDuration / WrapRadAngle((aObject.myRotation - targetRotation))
    angle.callback = aCallback
    table.insert(rotateBuffer, angle)
    print("Rotating " .. tostring(aObject.myName) .. " " .. tostring(targetRotation) .. " radians")
end
function RotateOverTime(aObject, aDuration, aRotation, aCallback)
    local rotation = {}
    rotation.object = aObject
    rotation.duration = aDuration
    rotation.rotationSpeed = aRotation / aDuration
    rotation.callback = aCallback
    table.insert(rotateBuffer, rotation)
    print("Rotating " .. tostring(aObject.myName) .. " " .. tostring(aRotation) .. " radians")
end

UpdateMoveBuffer = function(aDeltaTime)
	local indicesToEmpty = {}
	for i, movement in ipairs(moveBuffer) do
		movement.duration = movement.duration - aDeltaTime
		movement.object.myPosX = movement.object.myPosX + movement.moveSpeedX*aDeltaTime
		movement.object.myPosY = movement.object.myPosY + movement.moveSpeedY*aDeltaTime

		if(movement.duration <= 0) then
			table.insert(indicesToEmpty, i)
			print("Moving of " .. tostring(movement.object.myName) .. " complete")
			if(movement.callback ~= nil) then
				movement.callback()
			end
		end
	end
	for _, index in ipairs(indicesToEmpty) do
		table.remove(moveBuffer, i)
	end
end

UpdateAngleBuffer = function(aDeltaTime)
	local indicesToEmpty = {}

	for i, angle in ipairs(rotateBuffer) do
		angle.duration = angle.duration - aDeltaTime


		angle.object.myRotation = WrapRadAngle(angle.object.myRotation + angle.rotationSpeed * aDeltaTime)

		if(angle.duration <= 0) then
			print("Rotation of " .. tostring(angle.object.myName) .. " complete")
			if(angle.callback ~= nil) then
				angle.callback()
			end
			table.insert(indicesToEmpty, i)
		end
	end
	for _, index in ipairs(indicesToEmpty) do
		table.remove(rotateBuffer, i)
	end
end

UpdateRotationBuffer = function(aDeltaTime)
	local indicesToEmpty = {}

	for i, rotation in ipairs(rotateBuffer) do
		rotation.duration = rotation.duration - aDeltaTime
		rotation.object.myRotation = WrapRadAngle(rotation.object.myRotation + rotation.rotationSpeed * aDeltaTime)

			if(rotation.duration <= 0) then
				print("Rotation of " .. tostring(rotation.object.myName) .. " complete")
				if(rotation.callback ~= nil) then
					rotation.callback()
				end
				table.insert(indicesToEmpty, i)
			end
	end
	for _, index in ipairs(indicesToEmpty) do
		table.remove(rotateBuffer, i)
	end
end

UpdateScaleBuffer = function(aDeltaTime)
	local indicesToEmpty = {}

	for i, scaling in ipairs(scaleBuffer) do
		scaling.duration = scaling.duration - aDeltaTime
		scaling.object.myScaleX = scaling.object.myScaleX + deltaScaleX * aDeltaTime
		scaling.object.myScaleY = scaling.object.myScaleY + deltaScaleY * aDeltaTime

		if(scaling.duration <= 0) then
			print("Scaling of " .. tostring(scaling.object.myName) .. " complete")
			if(scaling.callback ~= nil) then
				scaling.callback()
			end
			table.insert(indicesToEmpty, i)
		end
	end
	for _, index in ipairs(indicesToEmpty) do
		table.remove(scaleBuffer, i)
	end
end
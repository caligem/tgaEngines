-- Sound variable(s)
-- Creates a list of songs because it should be possible to play multiple songs at a time.
local locCurrentSounds = {}
local locSounds = {}
-- Music variable(s)
local locMusic = {}
local locCurrentMusic = nil
local locMusicVolumeScalingFactor = 0
local locIsMusicPauseEnabled = false
local locIsMusicStopEnabled = false

function ResetSoundManager()
	locCurrentSounds = {}
	locCurrentMusic = nil
	locSounds = {}
	locMusic = {}
end

-- Private function(s)

local function CreateSound(aName, aFilePath, aVolume)
	local newSound = {}
	newSound.mySound = love.audio.newSource(aFilePath, "static")
	newSound.myName = aName
	newSound.mySound:setLooping(false)
	newSound.mySound:setVolume(aVolume)
	table.insert(locSounds, newSound)
end

local function CreateMusic(aName, aFilePath, aVolume, aShouldPauseInCutScenes)
	local newMusic = {}
	newMusic.mySound = love.audio.newSource(aFilePath)
	newMusic.myName = aName
	newMusic.mySound:setLooping(true)
	newMusic.mySound:setVolume(aVolume)
	newMusic.myShouldPauseInCutScenes = aShouldPauseInCutScenes
	newMusic.myStartVolume = aVolume
	table.insert(locMusic, newMusic)
end

local function EnableMusicPauseOrStopWithFadeOut(aSeconds, aIsPauseEnabled, aIsStopEnabled)
	locMusicVolumeScalingFactor = aSeconds / locCurrentMusic.mySound:getVolume()
	locIsMusicPauseEnabled = aIsPauseEnabled
	locIsMusicStopEnabled = aIsStopEnabled
end

-- Public function(s)

function SoundManager_Load()
	-- Music
	CreateMusic("MainMenu", "Audio/Music/Test 1 - godnattlat_mixdown.wav", 1.0, false)
	CreateMusic("House", "Audio/Music/Test 7.wav", 0.2, true)
	CreateMusic("LaboratoryAndCatScene", "Audio/Music/Test 15.wav", 1.0, true)
	CreateMusic("Outdoor", "Audio/Music/Test 13 - med faglar.wav", 1.0, true)
	-- Sound
	CreateSound("MouseClick", "Audio/Sound effects/MouseClick.wav", 0.7)
	CreateSound("CatNormalMeow", "Audio/Sound effects/CatNormalMeow.wav", 0.2)
	CreateSound("CatScaredMeow", "Audio/Sound effects/CatScaredMeow.wav", 0.4)
	CreateSound("BalloonInflation", "Audio/Sound effects/BalloonInflation.wav", 0.4)
	CreateSound("ColorPuzzleCrank", "Audio/Sound effects/ColorPuzzleCrank.wav", 1.0)
	CreateSound("DropObject", "Audio/Sound effects/DropObjectSound.mp3", 0.6)
	CreateSound("PickupObject", "Audio/Sound effects/PickUpObject.wav", 1.0)
	CreateSound("FlashlightSwitch", "Audio/Sound effects/FlashLightSwitch.wav", 0.8)
	CreateSound("SafetyGateOpen", "Audio/Sound effects/SafetyGateOpen.wav", 0.9)
	CreateSound("FireFlyBuzz", "Audio/Sound effects/FireFlyBuzz.wav", 0.8)
	CreateSound("GrindingStone", "Audio/Sound effects/GrindStoneSharpen.wav", 1.0)
	CreateSound("CanAndStickCombination", "Audio/Sound effects/Pulltab_Remove.wav", 0.9)
	CreateSound("WaterDrop", "Audio/Sound effects/WaterDrop.wav", 0.2)
	CreateSound("BallonPickup", "Audio/Sound effects/BallonPickup.wav", 0.4)
	CreateSound("WindupToy", "Audio/Sound effects/WindupToy.wav", 0.8)
	CreateSound("RollingHead", "Audio/Sound effects/RollingHeadRatMaze.wav", 1.0)
	CreateSound("RepairBrokenTrelli", "Audio/Sound effects/RepairBrokenTrelli.wav", 1.0)
	CreateSound("Footsteps", "Audio/Sound effects/FootstepsRatMazeScene.wav", 1.0)
	CreateSound("FlyBuzz", "Audio/Sound effects/FlyBuzz.wav", 1.0)
	CreateSound("CutDownFlower", "Audio/Sound effects/CutDownFlower.wav", 0.8)
	CreateSound("BalloonExplosion", "Audio/Sound effects/BalloonBurst.wav", 1.0)--PlaySound("BalloonExplosion", true)
	CreateSound("CatAttack", "Audio/Sound effects/CatAttackSound.wav", 0.3)
	CreateSound("EmptyFlask", "Audio/Sound effects/EmptyPotion.wav", 0.5)
	CreateSound("TankBlowingWithoutBalloon", "Audio/Sound effects/NoBalloonOnTank.wav", 0.5)
	CreateSound("BlowingPartyHorn", "Audio/Sound effects/PartyHorn.wav", 0.5)
end

-- Sound function(s)
function PlaySound(aSoundName, aForceReplay)
	for i = GetTableSize(locCurrentSounds), 1, -1 do
		-- Removes the song if it is not playing anymore.
		if (locCurrentSounds[i].mySound:isPlaying() == false) then
			table.remove(locCurrentSounds, i)
		end
	end

	for i, sound in pairs(locSounds) do
		if (sound.myName == aSoundName) then
			-- Searches for existing wanted sound
			local indexOfSoughtSound = nil
			for i, currentSound in ipairs(locCurrentSounds) do
				if (currentSound.myName == aSoundName) then
					indexOfSoughtSound = i
					break
				end
			end

			if (indexOfSoughtSound ~= nil) then
				-- Forces a sound to replay.
				if (aForceReplay == true) then
					locCurrentSounds[indexOfSoughtSound].mySound:rewind()
				end
			else
				-- Inserts a new song if it doesn't already exist.
				table.insert(locCurrentSounds, sound)
				locCurrentSounds[GetTableSize(locCurrentSounds)].mySound:play()
			end
			break

			-- Old code. NOT COMPATIBLE WITH NEW VERSION! OLIVER PLS ACCEPT THE NEW VERSION \/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
			--[[if (locCurrentSound ~= nil and locCurrentSound.mySound:isPlaying() == true and aForceReplay == true) then
				locCurrentSound.mySound:stop()

				print("----------------------------------------")
				print("Sound name: " .. aSoundName .. "\nForce replay: " .. tostring(aForceReplay))
				print("Current play time: " .. tostring(locCurrentSound.mySound:tell("seconds")))
				print("The sound's duration: " .. tostring(locCurrentSound.mySound:getDuration("seconds")))
				print("----------------------------------------")
			end

			locCurrentSound = sound
			locCurrentSound.mySound:play()]]
		end
	end
end

function StopSound(aSoundName)
	for i = GetTableSize(locCurrentSounds), 1, -1 do
		-- Stops the sound and removes it.
		if (locCurrentSounds[i].myName == aSoundName) then
			locCurrentSounds[i].mySound:stop()
			table.remove(locCurrentSounds, i)
			break
		end
	end
end

function PauseSound(aSoundName)
	for i = GetTableSize(locCurrentSounds), 1, -1 do
		if (locCurrentSounds[i].myName == aSoundName) then
			locCurrentSounds[i].mySound:pause()
			break
		end
	end
end

function PauseAllSounds()
	for i = GetTableSize(locCurrentSounds), 1, -1 do
		locCurrentSounds[i].mySound:pause()
	end
end

function ResumeAllSounds()
	for i = GetTableSize(locCurrentSounds), 1, -1 do
		locCurrentSounds[i].mySound:resume()
	end
end

-- Music function(s)
function PlayMusic(aMusicName)
	if (locCurrentMusic ~= nil and locCurrentMusic.mySound:isPlaying() ~= true) then
		for i, music in pairs(locMusic) do
			if (music.myName == aMusicName) then
				locCurrentMusic = music
				locCurrentMusic.mySound:play()
			end
		end
	elseif (locCurrentMusic == nil) then
		for i, music in pairs(locMusic) do
			if (music.myName == aMusicName) then
				locCurrentMusic = music
				locCurrentMusic.mySound:play()
			end
		end
	end
end

function PauseMusicIfNeeded()
	if (locCurrentMusic ~= nil) then
		if (locCurrentMusic.myShouldPauseInCutScenes) then
			EnableMusicPauseOrStopWithFadeOut(0.5, true, false)
		end
	end
end

function ContinueMusicIfNeeded()
	if (locCurrentMusic ~= nil) then
		if (locCurrentMusic.myShouldPauseInCutScenes and locCurrentMusic.mySound:isPlaying() == false) then
			locCurrentMusic.mySound:play()
		end
	end
end

function PauseCurrentMusicWithFadeOut(aSeconds)
	if (locCurrentMusic ~= nil and locCurrentMusic.mySound:isPlaying() == true) then
		EnableMusicPauseOrStopWithFadeOut(aSeconds, true, false)
	end
end

function StopCurrentMusicWithFadeOut(aSeconds)
	if (locCurrentMusic ~= nil and locCurrentMusic.mySound:isPlaying() == true) then
		EnableMusicPauseOrStopWithFadeOut(aSeconds, false, true)
	end
end

function StopCurrentMusic()
	if (locCurrentMusic ~= nil and locCurrentMusic.mySound:isPlaying() == true) then
		locCurrentMusic.mySound:stop()
	end
end

function ReplaceMusicWithFadeOut(aMusicToEnableName, aSeconds)
	if (locCurrentMusic ~= nil and locCurrentMusic.myName ~= aMusicToEnableName) then
		for i, music in pairs(locMusic) do
			if (music.myName == aMusicToEnableName) then
				if (locCurrentMusic.mySound:isPlaying() == true) then
					EnableMusicPauseOrStopWithFadeOut(aSeconds, false, true)
					Wait(aSeconds + 0.01, function() PlayMusic(aMusicToEnableName) end)
				else
					locCurrentMusic = music
					locCurrentMusic.mySound:play()
				end
				break
			end
		end
	elseif (locCurrentMusic ~= nil and locCurrentMusic.myName == aMusicToEnableName and locCurrentMusic.mySound:isPlaying() == false) then
		for i, music in pairs(locMusic) do
			if (music.myName == aMusicToEnableName) then
				locCurrentMusic = music
				locCurrentMusic.mySound:play()
				break
			end
		end
	elseif (locCurrentMusic == nil) then
		PlayMusic(aMusicToEnableName)
	end
end

function SetCurrentMusicVolume(aVolume)
	if (locCurrentMusic ~= nil) then
		locCurrentMusic.mySound:setVolume(aVolume)
	end
end

function MusicFadeOutUpdate(aDeltaTime)
	if (locCurrentMusic ~= nil and locCurrentMusic.mySound:getVolume() > 0 and locMusicVolumeScalingFactor > 0) then
		local newVolume = locCurrentMusic.mySound:getVolume() - aDeltaTime / locMusicVolumeScalingFactor

		if (newVolume < 0) then
			locMusicVolumeScalingFactor = 0
			locCurrentMusic.mySound:setVolume(locCurrentMusic.myStartVolume)

			if (locIsMusicPauseEnabled == true and locIsMusicStopEnabled == false) then
				locCurrentMusic.mySound:pause()
			elseif (locIsMusicStopEnabled == true and locIsMusicPauseEnabled == false) then
				locCurrentMusic.mySound:stop()
			else
				-- Creates error to correct the setting of the variables.
				local createError = 0 / 1
			end
		else
			locCurrentMusic.mySound:setVolume(newVolume)
		end
	end
end
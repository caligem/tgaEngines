local isFading = false
local fadeOut = true
local alpha = 0
local callbackFunction

function UpdateFade(dt)
	if (isFading) then
	    if (fadeOut == true) then
	    	if (alpha < 190) then
				alpha = alpha + 800 * dt
		    else
				isFading = false
				callbackFunction()
				FadeIn()
			end
	    else
	    	if (alpha > 0) then
				alpha = alpha - 800 * dt
		    else
				isFading = false
				isChangingScene = false
			end
	    end
	end
end

function DrawFade()
	if (isFading) then
		love.graphics.setColor(0, 0, 0, alpha)
	    love.graphics.rectangle("fill", 0, 0, love.graphics.getWidth(), love.graphics.getHeight())
	    love.graphics.setColor(255, 255, 255, 255)
	end
end

function FadeOut(aCallbackFunction) 
	alpha = 0
	callbackFunction = aCallbackFunction
	fadeOut = true
	isFading = true
	isChangingScene = true
end

function FadeIn() 
	alpha = 190
	fadeOut = false
	isFading = true
	isChangingScene = true
end

function SkipFade()
	isFading = false
	fadeOut = false
	alpha = 0
	isChangingScene = false
	callbackFunction()
end
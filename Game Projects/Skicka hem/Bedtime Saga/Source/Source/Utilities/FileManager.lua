require("Source/Utilities/serializer")

function GetObjectDataTable(aObject)
	local objectTable = {}

	objectTable.myName = aObject.myName
	objectTable.myDescriptions = aObject.myDescriptions 

	objectTable.myObjectType = aObject.myObjectType

	objectTable.myRotation = aObject.myRotation
	objectTable.myScaleX = aObject.myScaleX
	objectTable.myScaleY = aObject.myScaleY

	if(aObject.myObjectType == "pickupable") then
		objectTable.myAllowState = aObject.myAllowState
		objectTable.mySpritePath = "/Graphics/Objects/Pickupable/" .. objectTable.myName .. ".png"
	elseif(aObject.myObjectType == "passage") then
		objectTable.myDestinationScene = aObject.myDestinationScene
		objectTable.myAllowState = aObject.myAllowState
		objectTable.mySpritePath = "/Graphics/Objects/Passages/" .. objectTable.myName .. ".png"
	elseif(aObject.myObjectType == "inspectable") then
		objectTable.mySpritePath = "/Graphics/Objects/Inspectable/" .. objectTable.myName .. ".png"
	end

	return objectTable
end

function GetRoomDataTable(aRoom)
	local roomTable = {}

	roomTable.myName = aRoom.myName
	--roomTable.backgroundImage = aRoom.backgroundImage 								REMEMBER TO SAVE BACKGROUND IMAGE LOCATION, NOT BACKGROUND IMAGE!
	roomTable.objects = {}

	for i, object in ipairs(aRoom.objects) do
		table.insert(roomTable.objects, GetObjectDataTable(object))
	end

	return roomTable
end

function SaveInteractions()
	local serialData = serialize(interactions)
	local filename = "Source/Rooms/Interactions.txt"
	local file, err = io.open(filename, "w+")
	if (err == nil) then
		print("File saved: " .. filename)
		file:write(serialData)
		file:close()
	else
		print(err)
	end
end

function LoadInteractions()
	local filename = "Source/Rooms/Interactions.txt"
	interactions = love.filesystem.load(filename)()
end

function SaveScene(aRoom)
	local roomTable = GetRoomDataTable(aRoom)
	local serialData = serialize(roomTable)
	local filename = sceneFileFolder .. aRoom.myName .. "_DATA.txt"
	local file, err = io.open(filename, "w+")
	if err == nil then
		print("File saved: " .. filename)
		file:write(serialData)
		file:close()
	else
		print(err)
	end
end

function LoadScene(aSceneName)
	local scenePath = sceneFileFolder .. aSceneName .. "_DATA.txt"
	if (love.filesystem.exists(scenePath)) then
		local getRoom = love.filesystem.load(scenePath)
		local loadedRoom = getRoom()
		if(loadedRoom ~= nil) then

			for i, object in ipairs(loadedRoom.objects) do
				
				if(object.myObjectType == "pickupable") then
					object.mySpritePath = "/Graphics/Objects/Pickupable/" .. object.myName .. ".png"
				elseif(object.myObjectType == "passage") then
					object.mySpritePath = "/Graphics/Objects/Passages/" .. object.myName .. ".png"
				elseif(object.myObjectType == "inspectable") then
					object.mySpritePath = "/Graphics/Objects/Inspectable/" .. object.myName .. ".png"
				end

				objectsInWorld[object.myName] = object
				InitObject(object)
			end
			print("Scene data loaded: " .. scenePath)
		else
			print("Load failed: " .. scenePath .. " could not load!")
		end
		return loadedRoom
	else
		print("Load failed: " .. scenePath .. " does not exist!")
		love.event.quit()
		return nil
	end
end
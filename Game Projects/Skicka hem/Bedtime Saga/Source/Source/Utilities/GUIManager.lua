-- Other variables(s)
local deltaTime = 0
local fixedDeltaTime = 0
local deltaTimeContainer = 0
local updatesCounted = 0
-- Variable(s) for text printing
local timerForPrinting = 0
local maxTimeForAddingAWord = 0.005
local timerForEndOfPrinting = 0
local maxTimeForTextToDissapear = 0
local currentTextToPrint = ""
local newTextToPrint = ""
local textPosX = 0
local textPosY = 0
local textIndex = 1
local printText = false
local maxTextLengthToSnap = 206
-- Variable(s) for dialogue box
local dialogueBoxes = {}
local dialogueBoxSprites = {}
local dialogueBoxTimers = {}
-- Object(s)
flashlightButton = {}
showFlashlight = false
flashlightTurnedOn = false
Cursor = {point = {}, grab = {}, inspect = {}, walk = {}}
local currentCursor = Cursor.point
local cursorState = "default"
local cursorOffsetX, cursorOffsetY = 26, 10

function ResetGUIManager()
	-- Other variables(s)
	deltaTime = 0
	fixedDeltaTime = 0
	deltaTimeContainer = 0
	updatesCounted = 0
	-- Variable(s) for text printing
	timerForPrinting = 0
	maxTimeForAddingAWord = 0.005
	timerForEndOfPrinting = 0
	maxTimeForTextToDissapear = 0
	currentTextToPrint = ""
	newTextToPrint = ""
	textPosX = 0
	textPosY = 0
	textIndex = 1
	printText = false
	maxTextLengthToSnap = 206
	-- Variable(s) for dialogue box
	dialogueBoxes = {}
	dialogueBoxSprites = {}
	dialogueBoxTimers = {}
	-- Object(s)
	flashlightButton = {}
	showFlashlight = false
	flashlightTurnedOn = false
	Cursor = {point = {}, grab = {}, inspect = {}, walk = {}}
	currentCursor = Cursor.point
	cursorState = "default"
	cursorOffsetX, cursorOffsetY = 26, 10
end

-- Private function(s)

function GetCursorImageOffset()
	return cursorOffsetX, cursorOffsetY
end

function SetCursorState(aState)
	cursorState = aState
end

local function ResetTextPrinting()
	print("Removing active dialogue box")
	currentTextToPrint = ""
	textIndex = 1
	timerForPrinting = 0
	timerForEndOfPrinting = 0
	printText = false
	prevClickedObjectName = ""
end

local function ScalingFunction(aXValue)
	local result = 0
	local period = 4

	-- Stops the value to shrink after the limit
	if (aXValue > math.pi / (period * 2)) then
		result = 1.0001
	else
		result = math.sin(period * aXValue)
	end

	return result
end

-- Scales the dialogue boxes and enables text printing
local function UpdateDialogueBoxes()
	for index = GetTableSize(dialogueBoxes), 1, -1 do
		if (dialogueBoxes[index].myForceScaleDown == false) then
			if (dialogueBoxes[index].myCanScaleUp and dialogueBoxes[index].myScale <= 1) then
				dialogueBoxTimers[index] = dialogueBoxTimers[index] + fixedDeltaTime
				dialogueBoxes[index].myScale = ScalingFunction(dialogueBoxTimers[index])

			elseif (dialogueBoxes[index].myCanScaleUp and dialogueBoxes[index].myScale > 1) then
				dialogueBoxes[index].myScale = 1
				dialogueBoxes[index].myCanScaleUp = false

				-- Enables printing only if the index of dialogueBoxes is equal to the index of the last element in the table
				if (index == GetTableSize(dialogueBoxes)) then
					printText = true
				end
			end
		end

		if ((dialogueBoxes[index].myCanScaleDown or dialogueBoxes[index].myForceScaleDown) and dialogueBoxes[index].myScale >= 0) then
			dialogueBoxes[index].myScale = dialogueBoxes[index].myScale - 0.05

		elseif ((dialogueBoxes[index].myCanScaleDown or dialogueBoxes[index].myForceScaleDown) and dialogueBoxes[index].myScale < 0) then
			if (index == GetTableSize(dialogueBoxes)) then
				ResetTextPrinting()
			end
			table.remove(dialogueBoxes, index)
			table.remove(dialogueBoxTimers, index)

		end
		--love.timer.sleep(2)
	end
end

local function PrintText()
	if (printText) then
		if (state == "play") then
			-- Add one word to printing after some time
			if (textIndex < string.len(currentTextToPrint)) then
				if (timerForPrinting <= maxTimeForAddingAWord) then
					timerForPrinting = timerForPrinting + deltaTime

				elseif (timerForPrinting > maxTimeForAddingAWord) then
					timerForPrinting = timerForPrinting - maxTimeForAddingAWord
					textIndex = textIndex + 1
				end

			-- Empty and reset timer on last
			elseif (textIndex == string.len(currentTextToPrint)) then
				timerForPrinting = 0
				timerForEndOfPrinting = timerForEndOfPrinting + deltaTime

				if (timerForEndOfPrinting > maxTimeForTextToDissapear) then
					ResetTextPrinting()
					dialogueBoxes[GetTableSize(dialogueBoxes)].myCanScaleDown = true
				end
			end
		end

		if (timerForEndOfPrinting <= maxTimeForTextToDissapear) then
			love.graphics.setColor(0, 0, 0)
			love.graphics.printf(string.sub(currentTextToPrint, 0, textIndex), textPosX, textPosY, maxTextLengthToSnap, "left")
			love.graphics.setColor(255, 255, 255)
		end
	end
end

local function SetBlueprintValues(aTextsPixelWidth, aTextsPixelHeight)
	local dialogueBoxBlueprint =
	{
		myImage = nil,
		myPosX = 0,
		myPosY = 0,
		myScale = 0,
		myScaleXManipulator = 1,
		myScaleYManipulator = 1,
		myOffsetX = 0,
		myOffsetY = 0,
		myCanScaleUp = false,
		myCanScaleDown = false,
		myForceScaleDown = false
	}

	-- Sets max values for the for-loops
	local maxRows = 0
	local countedHeight = 0
	for i = 0, aTextsPixelWidth, maxTextLengthToSnap do
		countedHeight = countedHeight + aTextsPixelHeight

		if (countedHeight >= 64 - 15) then
			maxRows = maxRows + 1
			countedHeight = 0
		end
	end
	maxRows = maxRows + 1

	local maxWidth = 0
	for i = 1, maxTextLengthToSnap - 64, 64 do
		maxWidth = maxWidth + 64
	end

	-- Fills sprite container with wanted sprites
	local spriteContainer = {}
	if (aTextsPixelWidth >= 100 and maxRows > 1) then
		for y = 1, maxRows, 1 do
			for x = 0, maxWidth, 64 do
				-- Checks upper side
				if (y == 1 and x == 0) then
					table.insert(spriteContainer, dialogueBoxSprites.upperLeftCorner)

				elseif (y == 1 and x > 0 and x < maxWidth) then
					table.insert(spriteContainer, dialogueBoxSprites.upperEdge)

				elseif (y == 1 and x >= maxWidth) then
					table.insert(spriteContainer, dialogueBoxSprites.upperRightCorner)

				-- Checks middle
				elseif (y > 1 and y ~= maxRows and x == 0) then
					table.insert(spriteContainer, dialogueBoxSprites.leftEdge)

				elseif (y > 1 and y ~= maxRows and x > 0 and x < maxWidth) then
					table.insert(spriteContainer, dialogueBoxSprites.middle)

				elseif (y > 1 and y ~= maxRows and x >= maxWidth) then
					table.insert(spriteContainer, dialogueBoxSprites.rightEdge)

				-- Checks bottom side
				elseif (y == maxRows and x == 0) then
					table.insert(spriteContainer, dialogueBoxSprites.bottomLeftCorner)

				elseif (y == maxRows and x > 0 and x < maxWidth) then
					table.insert(spriteContainer, dialogueBoxSprites.bottomEdge)

				elseif (y == maxRows and x >= maxWidth) then
					table.insert(spriteContainer, dialogueBoxSprites.bottomRightCorner)
				end
			end
		end

	elseif (aTextsPixelWidth >= 100 and maxRows == 1) then
		for y = 1, 2, 1 do
			for x = 0, maxWidth, 64 do
				-- Checks upper side
				if (y == 1 and x == 0) then
					table.insert(spriteContainer, dialogueBoxSprites.upperLeftCorner)

				elseif (y == 1 and x > 0 and x < maxWidth) then
					table.insert(spriteContainer, dialogueBoxSprites.upperEdge)

				elseif (y == 1 and x >= maxWidth) then
					table.insert(spriteContainer, dialogueBoxSprites.upperRightCorner)

				-- Checks bottom side
				elseif (y == 2 and x == 0) then
					table.insert(spriteContainer, dialogueBoxSprites.bottomLeftCorner)

				elseif (y == 2 and x > 0 and x < maxWidth) then
					table.insert(spriteContainer, dialogueBoxSprites.bottomEdge)

				elseif (y == 2 and x >= maxWidth) then
					table.insert(spriteContainer, dialogueBoxSprites.bottomRightCorner)
				end
			end
		end

	else
		-- Dialogue box for the smallest text
		table.insert(spriteContainer, dialogueBoxSprites.upperLeftCorner)
		table.insert(spriteContainer, dialogueBoxSprites.upperRightCorner)
		table.insert(spriteContainer, dialogueBoxSprites.bottomLeftCorner)
		table.insert(spriteContainer, dialogueBoxSprites.bottomRightCorner)
	end

	-- Sets size values for the canvas
	local newWidth = 0
	local newHeight = 0
	for i, sprite in ipairs(spriteContainer) do
		if (sprite == dialogueBoxSprites.upperLeftCorner or
			sprite == dialogueBoxSprites.leftEdge or
			sprite == dialogueBoxSprites.bottomLeftCorner) then
			newHeight = newHeight + sprite:getHeight()
		end

		if (sprite == dialogueBoxSprites.upperLeftCorner or
			sprite == dialogueBoxSprites.upperEdge or
			sprite == dialogueBoxSprites.upperRightCorner) then
			newWidth = newWidth + sprite:getWidth()
		end
	end

	-- Fills canvas with contained sprites
	local canvas = love.graphics.newCanvas(newWidth, newHeight)
	canvas:renderTo(
		function()
			local y = 0
			local x = 0
			local countedWidth = 0

			for i, sprite in ipairs(spriteContainer) do
				countedWidth = countedWidth + 1

				if (countedWidth * sprite:getWidth() > newWidth) then
					countedWidth = 1
					x = 0
					y = y + sprite:getHeight()
				else
					x = (countedWidth - 1) * sprite:getWidth()
				end

				love.graphics.draw(sprite, x, y)
			end
		end
	)

	-- Creates one image from the canvas
	dialogueBoxBlueprint.myImage = love.graphics.newImage(canvas:newImageData())

	return dialogueBoxBlueprint
end

local function CreateOneDialogueBox(aXPos, aYPos, aTextsPixelWidth, aTextsPixelHeight)
	local posX = -1
	local posY = -1
	local dialogueBoxBlueprint = {}

	dialogueBoxBlueprint = SetBlueprintValues(aTextsPixelWidth, aTextsPixelHeight)

	local textOffsetX = 26
	local textOffsetY = 17
	if (aXPos <= love.graphics.getWidth() / 2 and aYPos <= love.graphics.getHeight() / 2) then
		posX = aXPos + 32
		posY = aYPos + 32
		-- Sets position for the text
		textPosX = posX + textOffsetX
		textPosY = posY + textOffsetY

	elseif (aXPos > love.graphics.getWidth() / 2 and aYPos <= love.graphics.getHeight() / 2) then
		posX = aXPos - 20
		posY = aYPos + 20
		dialogueBoxBlueprint.myScaleXManipulator = -1
		-- Sets position for the text
		textPosX = posX - dialogueBoxBlueprint.myImage:getWidth() + textOffsetX
		textPosY = posY + textOffsetY

	elseif (aXPos <= love.graphics.getWidth() / 2 and aYPos > love.graphics.getHeight() / 2) then
		posX = aXPos + 10
		posY = aYPos - 10
		dialogueBoxBlueprint.myScaleYManipulator = -1
		-- Sets position for the text
		textPosX = posX + textOffsetX
		textPosY = posY - dialogueBoxBlueprint.myImage:getHeight() + textOffsetY

	elseif (aXPos > love.graphics.getWidth() / 2 and aYPos > love.graphics.getHeight() / 2) then
		posX = aXPos - 15
		posY = aYPos - 15
		dialogueBoxBlueprint.myScaleXManipulator = -1
		dialogueBoxBlueprint.myScaleYManipulator = -1
		-- Sets position for the text
		textPosX = posX - dialogueBoxBlueprint.myImage:getWidth() + textOffsetX
		textPosY = posY - dialogueBoxBlueprint.myImage:getHeight() + textOffsetY
	end
	--maxTextLengthToSnap = dialogueBoxBlueprint.myImage:getWidth() - dialogueBoxBlueprint.myImage:getWidth() / 3.9 - 35

	table.insert(dialogueBoxes, dialogueBoxBlueprint)
	table.insert(dialogueBoxTimers, 0)
	dialogueBoxes[GetTableSize(dialogueBoxes)].myCanScaleUp = true
	dialogueBoxes[GetTableSize(dialogueBoxes)].myPosX = posX
	dialogueBoxes[GetTableSize(dialogueBoxes)].myPosY = posY
end

-- Public function(s)

function GUIManager_ForceDialogBoxesToDissapear(aDeleteDialogueBoxesInstantaneously)

	for index = GetTableSize(dialogueBoxes), 1, -1 do
		if (aDeleteDialogueBoxesInstantaneously == false) then
			dialogueBoxes[index].myForceScaleDown = true
		else
			table.remove(dialogueBoxes, index)
			table.remove(dialogueBoxTimers, index)
		end
	end

	ResetTextPrinting()
end

function GUIManager_PrintNewText(aXPos, aYPos, aTextToPrint)
	if (aTextToPrint ~= currentTextToPrint and aTextToPrint ~= "" and aTextToPrint ~= nil) then
		print("Printing to textbox: \"" .. tostring(aTextToPrint) .. "\"")

		CreateOneDialogueBox(aXPos, aYPos, currentFont:getWidth(aTextToPrint), currentFont:getHeight(aTextToPrint))

		-- Forces downscaling on almost all dialogue boxes. The exeption is the last dialogue box because it is the one that we want to keep.
		for i, dialogueBox in pairs(dialogueBoxes) do
			if (i ~= GetTableSize(dialogueBoxes)) then
				dialogueBox.myForceScaleDown = true
			end
		end

		ResetTextPrinting()
		currentTextToPrint = aTextToPrint
		maxTimeForTextToDissapear = 0.05 * string.len(currentTextToPrint) + 0.5
	end
end

function GUIManager_EnableFlashlight()

	showFlashlight = true
	flashlightTurnedOn = false

	flashlightButton = CreateButton("Flashlight_Button_Unlit", "Graphics/GUI/",
									(love.graphics.getWidth() / 4) * 3,
								 	love.graphics.getHeight(), function ()
								 		if(flashlightButton.myIsGrey == false) then
											GUIManager_ToggleFlashlight()
										end
								 	end)
	flashlightButton.myPosY = love.graphics.getHeight() - flashlightButton.myHeight / 2

	flashlightButton.myLitSprite = love.graphics.newImage("Graphics/GUI/Flashlight_Button_Lit.png")
	flashlightButton.myUnlitSprite = love.graphics.newImage("Graphics/GUI/Flashlight_Button_Unlit.png")

	if (scenes[currentScene].myName == "Floor2_Bedroom") then
		flashlightButton.myIsGrey = true
	else
		flashlightButton.myIsGrey = false
	end

	table.insert(globalConstantGUIElements, flashlightButton)
	PlaySound("PickupObject", true)
end

function GUIManager_ToggleFlashlight()
	if (flashlightButton.mySprite ~= nil and showFlashlight == true) then
		if (flashlightTurnedOn) then
			GUIManager_FlashlightOff()
			PlaySound("FlashlightSwitch", true)
		else
			GUIManager_FlashlightOn()
			PlaySound("FlashlightSwitch", true)
		end
	end
end

function GUIManager_FlashlightOn()
	if (flashlightButton.mySprite ~= nil and showFlashlight == true) then
		if (scenes[currentScene].myName == "Outdoor_Flowerbed") or (scenes[currentScene].myName == "Outdoor_EndScene") or (scenes[currentScene].myName == "Floor0_CatScene") then
			globalAllowClick = true
		elseif (scenes[currentScene].myName == "Floor2_Hallway") then
			scenes["Floor2_Hallway"].roomState = 2
		end

		print(tostring(globalAllowClick))

		flashlightTurnedOn = true
		flashlightButton.myName = "Flashlight_Button_Lit"
		flashlightButton.mySprite = flashlightButton.myLitSprite
	end
end

function GUIManager_FlashlightOff()
	if (flashlightButton.mySprite ~= nil and showFlashlight == true) then
		if (scenes[currentScene].myName == "Outdoor_Flowerbed") or (scenes[currentScene].myName == "Outdoor_EndScene") or (scenes[currentScene].myName == "Floor0_CatScene") then
			globalAllowClick = false
		elseif (scenes[currentScene].myName == "Floor2_Hallway") then
			scenes["Floor2_Hallway"].roomState = 1
		end

		print(tostring(globalAllowClick))

		flashlightTurnedOn = false
		flashlightButton.myName = "Flashlight_Button_Unlit"
		flashlightButton.mySprite = flashlightButton.myUnlitSprite
	end
end

function GUIManager_Load()
	Cursor.point.default = love.mouse.newCursor("Graphics/GUI/mouse_Neutral.png", cursorOffsetX, cursorOffsetY )
	Cursor.point.clicked= love.mouse.newCursor("Graphics/GUI/mouse_NeutralClick.png", cursorOffsetX, cursorOffsetY )
	Cursor.grab.default  = love.mouse.newCursor("Graphics/GUI/mouse_Grab.png", cursorOffsetX, cursorOffsetY )
	Cursor.grab.clicked = love.mouse.newCursor("Graphics/GUI/mouse_GrabClick.png", cursorOffsetX, cursorOffsetY )
	Cursor.inspect.default  = love.mouse.newCursor("Graphics/GUI/mouse_Inspect.png", cursorOffsetX, cursorOffsetY )
	Cursor.inspect.clicked = love.mouse.newCursor("Graphics/GUI/mouse_InspectClick.png", cursorOffsetX, cursorOffsetY )
	Cursor.walk.default  = love.mouse.newCursor("Graphics/GUI/mouse_passage.png", 32, 22 )
 	InventoryLoad()

 	dialogueBoxSprites =
 	{
	 	upperLeftCorner = LoadSprite("Graphics/GUI/DialogueBox_Pieces/UpperLeftCorner.png"),
	 	upperEdge = LoadSprite("Graphics/GUI/DialogueBox_Pieces/UpperEdge.png"),
	 	upperRightCorner = LoadSprite("Graphics/GUI/DialogueBox_Pieces/UpperRightCorner.png"),
	 	leftEdge = LoadSprite("Graphics/GUI/DialogueBox_Pieces/LeftEdge.png"),
	 	middle = LoadSprite("Graphics/GUI/DialogueBox_Pieces/Middle.png"),
	 	rightEdge = LoadSprite("Graphics/GUI/DialogueBox_Pieces/RightEdge.png"),
	 	bottomLeftCorner = LoadSprite("Graphics/GUI/DialogueBox_Pieces/BottomLeftCorner.png"),
	 	bottomEdge = LoadSprite("Graphics/GUI/DialogueBox_Pieces/BottomEdge.png"),
	 	bottomRightCorner = LoadSprite("Graphics/GUI/DialogueBox_Pieces/BottomRightCorner.png")
	}

	pauseMenuButton = CreateButtonWithCallback("MenuButton", "Graphics/GUI/", 42, 42, function() PauseGame() end)
	table.insert(globalConstantGUIElements, pauseMenuButton)
end

function GUIManager_Update(dt)
	deltaTime = dt
	deltaTimeContainer = deltaTimeContainer + dt
	updatesCounted = updatesCounted + 1
	-- Calculates fixed delta time for stable game time
	if (deltaTimeContainer >= 1) then
		fixedDeltaTime = 1 / updatesCounted
		deltaTimeContainer = 0
		updatesCounted = 0
	end

	UpdateDialogueBoxes()
end

function SetCursor(aCursor)
	if(love.mouse.getCursor() ~= aCursor[cursorState]) then
		love.mouse.setCursor(aCursor[cursorState])
		currentCursor = aCursor
	end
end

function SetCursorClicked(aClicked)
	if(aClicked == true) and (currentCursor.clicked ~= nil) then
		cursorState = "clicked"
	else
		cursorState = "default"
	end
	SetCursor(currentCursor)
end

function SetCursorByHovered()
	if (state == "play") then
		if (grabbedObject == nil) then

			-- Fuck this mess of code

			if (love.mouse.isDown(1) == false) or ((love.mouse.isDown(1) == true) and (lastClickedObject == hoveredObject)) then
				--Inventory object
			    if (GetClickedSlot() ~= nil) and (objectsInInventory[GetClickedSlot()] ~= nil) then
			        SetCursor(Cursor.grab)

			    --GUI object
			    elseif(GetHoveredGUIElement() ~= nil) and (GetHoveredGUIElement().myType == "button") then
			    	SetCursor(Cursor.grab)

			    --Scene object
			    elseif (hoveredObject == nil) then
			    	SetCursor(Cursor.point)
			    elseif (GetInteraction(hoveredObject.myName, nil) ~= nil) and (GetInteraction(hoveredObject.myName, nil).isDone ~= true) then
			    	SetCursor(Cursor.grab)

			    elseif (hoveredObject.myObjectType == "inspectable") then
					if (globalAllowClick == true) then
			       		SetCursor(Cursor.inspect)
					end
			    elseif (hoveredObject.myObjectType == "pickupable") then
					if (globalAllowClick == true) then
			    	    SetCursor(Cursor.grab)
					end
			    elseif (hoveredObject.myObjectType == "passage") then
					if (globalAllowClick == true) then
				        SetCursor(Cursor.walk)
					end
				elseif (hoveredObject.myObjectType == "solid") then
					if (globalAllowClick == true) then
				        SetCursor(Cursor.point)
					end
			    end
			elseif (love.mouse.isDown(1) == true) and (lastClickedObject ~= hoveredObject) then
				SetCursor(Cursor.point)
			end
		else
			SetCursor(Cursor.grab)
		end
	else
		SetCursor(Cursor.point)
	end
end

function GUIManager_Draw()
	if (scenes[currentScene].GUIElements ~= nil) then
		for i, GUIElement in ipairs(scenes[currentScene].GUIElements) do
			love.graphics.setColor(GUIElement.myColor)

			if (GUIElement.myIsHidden == false) then
				if (GUIElement.myIsGrey == true) then
					PushShader(greyscaleShader)
					GUIElement:Draw()
					PopShader()
				else
					GUIElement:Draw()
				end
			end
			love.graphics.setColor(255, 255, 255, 255)
		end
	end

	if (GetTableSize(globalConstantGUIElements) > 0) then
		for i, constantGUIElement in ipairs(globalConstantGUIElements) do
			if (constantGUIElement.myName == "Flashlight_Button_Unlit") then
				if (showFlashlight == true) then
					if (constantGUIElement.myIsGrey == true) then
						PushShader(greyscaleShader)
						constantGUIElement:Draw()
						PopShader()
					else
						constantGUIElement:Draw()
					end
				end
			else
				constantGUIElement:Draw()
			end
		end
	end

	for i, dialogueBox in pairs(dialogueBoxes) do
		love.graphics.draw(dialogueBox.myImage, dialogueBox.myPosX, dialogueBox.myPosY, 0, dialogueBox.myScaleXManipulator * dialogueBox.myScale,
						   dialogueBox.myScaleYManipulator * dialogueBox.myScale, dialogueBox.myOffsetX, dialogueBox.myOffsetY)
	end

	PrintText()
end

function RemoveGUIElement(aGUIElement)
	GUIElements = scenes[currentScene].GUIElements
	for i = GetTableSize(GUIElements), 1, -1 do
		if (GUIElements[i] == aGUIElement) then
			table.remove(scenes[currentScene].GUIElements, i)
		end
	end
end

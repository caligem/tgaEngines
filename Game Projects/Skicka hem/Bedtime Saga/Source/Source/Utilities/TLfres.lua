-- TLfres v1.04, the world's easiest way to give a game resolution-independence
-- by Taehl (SelfMadeSpirit@gmail.com), modified and updated for personal use

TLfres = {}	-- namespace
local translate, scale, color, rect = love.graphics.translate, love.graphics.scale, love.graphics.setColor, love.graphics.rectangle
local width, height, stretch, centered, widthScale, heightScale, widthTranslate, heightTranslate

-- Sets up TLfres and sets the screen mode. Default parameters should work fine in most cases.
function TLfres.setScreen(aWidth, aHeight, aStretch, aMode)
	
	-- Calculate translation and scale
	local windowWidth, windowHeight, _ = love.window.getMode()
	width = aWidth > 0 and aWidth or windowWidth
	height = aHeight > 0 and aWidth or windowHeight
	stretch = aStretch
	centered = aMode.centered or false
	widthScale = width/extent
	heightScale = height/extent
	if (aMode.centered == true) then 
		widthTranslate = width/2
		heightTranslate = width/2-(width-height)/2
	else 
		widthTranslate = 0
		heightTranslate = 0
	end

	-- Change res
	local keepMouse = love.mouse.isVisible()	-- Fixes  Love forgetting if mouse is visible upon changing res
	love.window.setMode(aWidth, aHeight, aMode)
	love.mouse.setVisible(keepMouse)
end

-- Transforms screen geometry. Call this at the beginning of love.draw().
function TLfres.transform()
	translate(widthTranslate, heightTranslate)
	scale(widthScale, stretch and heightScale or widthScale)
end

function TLfres.TransformCoords(aPosX, aPosY)
	return TLfres.ScaleCoords(aPosX-widthTranslate, aPosY-heightTranslate)
end

function TLfres.ScaleCoords(aPosX, aPosY)
	local posX = aPosX/widthScale
	local posY = aPosY
	if(strech == true) then
		posY = posY/heightScale
	else
		posY = posY/widthScale
	end
	return posX, posY
end

-- Draws rectangles at the top and bottom of the screen to ensure proper aspect ratio. Call this at the end of love.draw() if you're not using stretch mode.
function TLfres.letterbox(w, h, c)
	local w = w or 4
	local h = h or 3 
	local c = c or {0,0,0, 255}
	
	color(c)
	
	local tall = e/w*h
	local de = e*2
	if TLfres.centered then
		rect("fill", -e,-e, de, e-tall)
		rect("fill", -e, e, de, tall-e)
	else
		local offset = (widthScale-heightScale) / widthScale * (e-1)
		rect("fill", 0,-offset, de, e-tall)
		rect("fill", 0,-offset, de, tall-e)
	end
end
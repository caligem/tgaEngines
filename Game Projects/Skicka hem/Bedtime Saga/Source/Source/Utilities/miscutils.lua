function GetTableSize(aTable)
	if (aTable ~= nil) then
        local count = 0;
        for _ in pairs(aTable) do
            count = count + 1
        end
        return count
    end

    return 0
end

function LoadSprite(aSpritePath)
    if(love.filesystem.exists(aSpritePath)) then
        --print("Image loaded: " .. tostring(aSpritePath))
        return love.graphics.newImage(aSpritePath)
    else
        print("Error loading image: " .. tostring(aSpritePath))
        return love.graphics.newImage("Graphics/error.png")
    end
end

local shaderBuffer = {}

function ClearShaderBuffer()
    shaderBuffer = {}
end

function PushShader(aShader)
    table.insert(shaderBuffer, aShader)
    love.graphics.setShader(aShader)
end

function PopShader()
    if (GetTableSize(shaderBuffer) > 0) then
        shaderBuffer[GetTableSize(shaderBuffer)] = nil
        love.graphics.setShader(shaderBuffer[GetTableSize(shaderBuffer)])
    end
end

function round(num)
  return tonumber(string.format("%." .. 0 .. "f", num))
end

function SortTableByName(aTable, aAscending)
    return aTable -- Sort
end

function GetMousePosition()
    return TLfres.TransformCoords(love.mouse.getX(), love.mouse.getY())
end

function GetSpriteOrigo(aObject)
	if (aObject ~= nil) then
		if (aObject.mySprite ~= nil) then
			local width = aObject.mySprite:getWidth()
			local height = aObject.mySprite:getHeight()
			local xOrigo = width/2
			local yOrigo = height/2
			return xOrigo, yOrigo
		else
			print("Didnt find a Sprite for " .. tostring(aObject.myName))
		end
	else
		print("Invalid Object")
	end

end

function Wait(aSeconds, aCallbackFunction)
    if (DoesExistInTable(globalWaitList, aCallbackFunction) == false) then
        local newWait = {}
        newWait.mySeconds = aSeconds
        newWait.myCallbackFunction = aCallbackFunction
        table.insert(globalWaitList, newWait)
    end
end

function DoesExistInTable(aTable, aElement)
    for i, element in ipairs(aTable) do
        if (element == aElement) then
            return true
        end
    end
    return false
end

function RemoveElementFromTable(aTable, aElement)
    for key, element in ipairs(aTable) do 
        if (element == aElement) then
            table.remove(aTable, key)
            return
        end
    end
end

function CountdownWaitList(aDeltaTime)
    for i = GetTableSize(globalWaitList), 1, -1 do
        globalWaitList[i].mySeconds = globalWaitList[i].mySeconds - (1 * aDeltaTime)
        if (globalWaitList[i].mySeconds <= 0) then
            --table.remove(globalWaitList, i)
            
            --if (globalWaitList[i].myCallbackFunction ~= nil) then
                globalWaitList[i].myCallbackFunction()
            --end
            RemoveElementFromTable(globalWaitList, globalWaitList[i])
        end
    end
end

function GetHoveredObject()
    local objectsInScene = scenes[currentScene].objects
    for i = GetTableSize(objectsInScene), 1, -1 do
        if (IsMouseOverObject(objectsInScene[i], true)) then
            return objectsInScene[i]
        end
    end
    return nil
end

function IsMouseOverObject(aObject, aUseAlpha)
    local mouseX, mouseY = GetMousePosition()
    local myX, myY = aObject.GetPos()
    local myWidth, myHeight = aObject.GetSize()
    if (IsPositionInBounds(mouseX, mouseY, myX, myY, myWidth, myHeight, true) == true) then
        objectSpriteData = aObject.mySprite:getData()
        local pixelPosX, pixelPosY = ToObjectLocalPosition(aObject, mouseX, mouseY)
        local _, _, _, pixelAlphaValue = objectSpriteData:getPixel(round(pixelPosX), round(pixelPosY))
        if (aObject.myName == "Floor2_Bedroom_CatClock") then
            table.insert(debugMessages, "X: " .. tostring(round(pixelPosX)))
            table.insert(debugMessages, "Y: " .. tostring(round(pixelPosY)))
            table.insert(debugMessages, "Alpha: " .. tostring(pixelAlphaValue))
        end
        if (pixelAlphaValue <= 0) and aUseAlpha then
            return false
        else
            return true
        end
    else
        return false
    end
end

function IsPositionInBounds(aPosX, aPosY, aBoundX, aBoundY, aBoundWidth, aBoundHeight, isCentered)
    local minBoundX, minBoundY, maxBoundX, maxBoundY = GetBounds(aBoundX, aBoundY, aBoundWidth, aBoundHeight, isCentered)

    if (aPosX > minBoundX) and (aPosY > minBoundY) and (aPosX < maxBoundX) and (aPosY < maxBoundY) then
        return true
    else
        return false
    end
end

function GetBounds(aPosX, aPosY, aWidth, aHeight, isCentered)
    if(isCentered == true) then
        return (aPosX-aWidth/2), (aPosY-aHeight/2), (aPosX+aWidth/2), (aPosY+aHeight/2)
    else
        return aPosX, aPosY, aPosX+aWidth, aPosY+aHeight
    end
end

function ToObjectLocalPosition(aObject, aPosX, aPosY)
    local locPosX = Bind((aPosX - aObject.myPosX + aObject.myWidth/2), 0, aObject.myWidth-1)
    local locPosY = Bind((aPosY - aObject.myPosY + aObject.myHeight/2), 0, aObject.myHeight-1)
    return locPosX, locPosY
end

function Bind(aValue, aMin, aMax)
    if(aValue > aMax) then
        return aMax
    elseif(aValue < aMin) then
        return aMin
    else
        return aValue
    end
end


function GetPolarity(aValue)
    if(aValue ~= 0) then
        return (aValue/math.abs(aValue))
    else
        return 1
    end
end

function WrapRadAngle(aRadianRotation)
    local resultRotation = aRadianRotation
    while (resultRotation >= math.pi*2) do
        resultRotation = resultRotation - math.pi*2
    end
    while (resultRotation < 0) do
        resultRotation = resultRotation + math.pi*2
    end
    return resultRotation
end

function GetDirectionFromRotation(aRadianRotation)
    aRadianRotation = WrapRadAngle(aRadianRotation)
    if (aRadianRotation >= math.pi*(1/4)) and (aRadianRotation < math.pi*(3/4)) then
        return "down"
    elseif (aRadianRotation >= math.pi*(3/4)) and (aRadianRotation < math.pi*(5/4)) then
        return "left"
    elseif (aRadianRotation >= math.pi*(5/4)) and (aRadianRotation < math.pi*(7/4)) then
        return "up"
    else
        return "right"
    end
end

function GetDirectionFromName(aDirectionName)
    if (aDirectionName == "left") then
        return direction.Left
    elseif(aDirectionName == "right") then
        return direction.Right
    elseif(aDirectionName == "up") then  
        return direction.Up
    elseif(aDirectionName == "down") then
        return direction.Down
    end 
end

function GetDistance(aDistX, aDistY)
    return sqrt( pow(aDistX, 2) + pow(aDistY, 2))
end

function GetDistance(aPos1X, aPos1Y, aPos2X, aPos2Y)
    return sqrt( pow( (aPos1X-aPos2X), 2) + pow( (aPos1Y-aPos2Y), 2))
end
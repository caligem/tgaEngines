local locVideoName = ""

local function locPlay (aVideo)
	love.mouse.setVisible(false)
	PauseMusicIfNeeded()
	aVideo.myClip:rewind()
	aVideo.myClip:play()
end

local function locStop (aVideo)
	aVideo.myClip:pause()
	aVideo.myClip:rewind()
end

local function locPerformCallbackFunction (aVideo)
	love.mouse.setVisible(true)
	state = aVideo.myNextState
	if (locVideoName ~= "Credits" and locVideoName ~= "EndScene" and locVideoName ~= "AltEndScene") then
		ContinueMusicIfNeeded()
	end
	aVideo:myCallbackFunction()
end

local function locSkip (aVideo)
	aVideo:Stop()
	aVideo:PerformCallbackFunction()
end

local function locGetClipPosition (aVideo)
	return aVideo.myClip:getSource():tell()
end

local function locIsPlaying (aVideo)
	return aVideo.myClip:isPlaying()
end

local function locDraw (aVideo)
	love.graphics.draw(aVideo.myClip, love.graphics.getWidth() /2, love.graphics.getHeight() /2, 0, 1, 1, aVideo.myClipWidth / 2, aVideo.myClipHeight / 2)
end

function CreateVideo(aVideoName, aCallbackFunction, aNextState) 
	locVideoName = aVideoName

	local newVideo = {}
	newVideo.myClip = love.graphics.newVideo("Video/" .. aVideoName .. ".ogv") 
	newVideo.myLength = newVideo.myClip:getSource():getDuration()
	newVideo.myClipWidth = newVideo.myClip:getWidth()
	newVideo.myClipHeight = newVideo.myClip:getHeight()

	if (aNextState ~= nil) then
		newVideo.myNextState = aNextState
	else
		newVideo.myNextState = "play"
	end 

	newVideo.Play = locPlay
	newVideo.Stop = locStop
	newVideo.Skip = locSkip
	newVideo.IsPlaying = locIsPlaying
	newVideo.Draw = locDraw
	newVideo.GetClipPosition = locGetClipPosition
	newVideo.myCallbackFunction = aCallbackFunction
	newVideo.PerformCallbackFunction = locPerformCallbackFunction

	return newVideo
end
function love.conf(t)
	t.window.title = "Bedtime Saga"
    t.window.fullscreen = true
    t.window.resizable = falses
    t.console = false
    io.stdout:setvbuf("no")
end

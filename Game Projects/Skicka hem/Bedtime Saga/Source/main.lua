require("Source/InputManager")
require("Source/Object")
require("Source/Utilities/Fading")
require("Source/SceneManager")
require("Source/InventoryManager")
require("Source/Utilities/miscutils")
require("Source/Utilities/FileManager")
require("Source/PauseMenu")
require("Source/Utilities/GUIManager")
require("Source/Utilities/TLfres")
require("Source/MainMenu")
require("Source/Video")
require("Source/LoadingScreen")
require("Source/SoundManager")
require("Source/GUIElement")


firstTimePlaying = true --Dont reset this
resetBackground = nil

extent = 1920 -- intended width
debug = false
debugMessages = {}
objectsInWorld = {}
globalCurrentVideo = nil
globalWaitList = {}
scenes = {}
state = "loading"
currentScene = "Floor2_Bedroom"
sceneFileFolder = "/Source/Rooms/Data/"
isChangingScene = false
--variables for flowerbed scene
globalAllowClick = true
globalStickCount = 0
deltaTime = 0

-- SceneList
globalSceneList = {}
globalSceneList[1] = "Floor2_Bedroom"
globalSceneList[2] = "Floor2_Hallway"
globalSceneList[3] = "Floor1_Livingroom"
globalSceneList[4] = "Floor1_Livingroom_BalloonPuzzle"
globalSceneList[5] = "Floor0_Laboratory"
globalSceneList[6] = "Floor0_Laboratory_ColorPuzzle"
globalSceneList[7] = "Floor0_RatMaze"
globalSceneList[8] = "Floor0_CatScene"
globalSceneList[9] = "Outdoor_Flowerbed"
globalSceneList[10] = "Outdoor_EndScene"
--UI
globalConstantGUIElements = {}
hoveredObject = nil
outlineShader = nil
greyscaleShader = nil
greyscalePercentageShader = nil
flashlightShader = nil
mazeLightShader = nil
televisionShader = nil
lastClickedObject = nil
grabbedObject = nil
showInventory = true
objectsInInventory = {}

--Interactions
interactions = {}

-- Audio
currentMusic = nil

function love.resize(w, h)
	TLfres.setScreen(w, h, false, {resizable = true, fullscreen = love.window.getFullscreen()})
end

function love.load()
	local windowWidth, windowHeight = love.window.getMode()
	TLfres.setScreen(windowWidth, windowHeight, false, {fullscreen = love.window.getFullscreen(), resizable = true})

	--Load shaders
	greyscalePercentageShader = love.graphics.newShader("Shaders/greyscale_percentage.glsl")
	greyscalePercentageShader:send("bottomPosY", 550)

  	outlineShader = love.graphics.newShader("Shaders/outline.glsl")
	greyscaleShader = love.graphics.newShader("Shaders/greyscale.glsl")
	mazeLightShader = love.graphics.newShader("Shaders/mazeLight.glsl")
	flashlightShader = love.graphics.newShader("Shaders/flashlight.glsl")
	resetBackground = LoadSprite("Graphics/Backgrounds/Resetting.png")

	if (state == "loading") then
		LoadLoadingScreen()
	else
		LoadGame()
	end
	PushShader(nil) -- default
end

function testfunction(aNumber)
	aNumber = aNumber + 1
end

function LoadGame()
	SoundManager_Load()

  	love.graphics.setNewFont("Text.ttf", 22)

	SceneManagerLoad()
	MainMenuLoad()
	GUIManager_Load()
	PauseMenuLoad()
	--SaveInteractions()
end

function love.update(dt)
	MusicFadeOutUpdate(dt)
	SetCursorByHovered()
	deltaTime = dt

	if state == "loading" then
		UpdateFade(dt)
		UpdateLoadingScreen(dt)
	elseif state == "mainmenu" then
		MainMenuUpdate()
	elseif state == "play" then
		SceneManagerUpdate(dt)
		hoveredObject = GetHoveredObject()
		GUIManager_Update(dt)
		if (GetTableSize(globalWaitList) > 0) then
			CountdownWaitList(dt)
		end
		UpdateFade(dt)
	elseif state == "pause" then
		PauseMenuUpdate(dt)
	elseif state == "cutscene" then
		UpdateFade(dt)
	elseif state == "reset" then

	else
		print("Wtf, stop using an invalid state, game cant do shit")
	end
end

local function ResetMain ()
	extent = 1920 -- intended width
	debug = false
	debugMessages = {}
	objectsInWorld = {}
	globalCurrentVideo = nil
	globalWaitList = {}
	scenes = {}
	currentScene = "Floor2_Bedroom"
	sceneFileFolder = "/Source/Rooms/Data/"
	isChangingScene = false
	--variables for flowerbed scene
	globalAllowClick = true
	globalStickCount = 0
	deltaTime = 0

	-- SceneList
	globalSceneList = {}
	globalSceneList[1] = "Floor2_Bedroom"
	globalSceneList[2] = "Floor2_Hallway"
	globalSceneList[3] = "Floor1_Livingroom"
	globalSceneList[4] = "Floor1_Livingroom_BalloonPuzzle"
	globalSceneList[5] = "Floor0_Laboratory"
	globalSceneList[6] = "Floor0_Laboratory_ColorPuzzle"
	globalSceneList[7] = "Floor0_RatMaze"
	globalSceneList[8] = "Floor0_CatScene"
	globalSceneList[9] = "Outdoor_Flowerbed"
	globalSceneList[10] = "Outdoor_EndScene"
	--UI
	globalConstantGUIElements = {}
	hoveredObject = nil
	outlineShader = nil
	greyscaleShader = nil
	greyscalePercentageShader = nil
	flashlightShader = nil
	mazeLightShader = nil
	televisionShader = nil
	lastClickedObject = nil
	grabbedObject = nil
	showInventory = true
	objectsInInventory = {}

	outlineShader = love.graphics.newShader("Shaders/outline.glsl")
	greyscaleShader = love.graphics.newShader("Shaders/greyscale.glsl")
	mazeLightShader = love.graphics.newShader("Shaders/mazeLight.glsl")
	flashlightShader = love.graphics.newShader("Shaders/flashlight.glsl")
	resetBackground = LoadSprite("Graphics/Backgrounds/Resetting.png")

	--Interactions
	interactions = {}

	-- Audio
	currentMusic = nil

	greyscalePercentageShader = love.graphics.newShader("Shaders/greyscale_percentage.glsl")
	greyscalePercentageShader:send("bottomPosY", 550)
	PushShader(nil) -- default
end

function ResetGameWithLoadingScreen()
	state = "reset"
	shouldStartReset = true
end

function ResetGame()
	state = "reset"

	print("------------------------> RESETTING <------------------------")

	ResetMain()

	ResetBedroom()
	ResetHallway()
	ResetLivingroom()
	ResetBalloonPuzzle()
	ResetLaboratory()
	ResetColorPuzzle()
	ResetRatMaze()
	ResetCatScene()
	ResetFlowerBed()
	ResetEndScene()

	--ResetInputManager()
	ResetInventoryManager()

	ResetPauseMenu()

	ResetSceneManager()

	ResetSoundManager()

	ResetGUIManager()

	LoadGame()

	ClearShaderBuffer()

	collectgarbage()

	ChangeScene("Floor2_Bedroom")

	state = "cutscene"
	globalCurrentVideo = introVideo
	globalCurrentVideo:Play()

	print("Current Scene: " .. currentScene)
end

function PauseGame()
	state = "pause"
	SetCursor(Cursor.point)
	PauseAllSounds()
end

function DrawCurrentVideoUntilFinished()
	if (globalCurrentVideo:IsPlaying() == true) then
		globalCurrentVideo:Draw()
	else
		globalCurrentVideo:PerformCallbackFunction()
	end
end

function love.draw()
	if state == "loading" then
		DrawLoadingScreen()
	elseif state == "mainmenu" then
		MainMenuDraw()
	elseif state == "cutscene" then
		DrawCurrentVideoUntilFinished()
	elseif state == "reset" then
		--love.graphics.draw(resetBackground)
		--if (shouldStartReset == true) then
		--	shouldStartReset = false
		--	ResetGame()
		--end
	else
		--Scaled drawing
		local mouseX, mouseY = love.mouse.getPosition()
	  	love.graphics.push()
		TLfres.transform()
		SceneManagerDraw()

		if (debug) then
			love.graphics.setColor(255,0,0,255)
			for i, object in ipairs(scenes[currentScene].objects) do
				love.graphics.rectangle("line", object.myPosX-object.myWidth/2, object.myPosY-object.myHeight/2, object.myWidth, object.myHeight)
			end
			love.graphics.setColor(255,255,255,255)
			local hoveredName = nil
			if(GetHoveredObject() ~= nil) then
				hoveredName = GetHoveredObject().myName
			end
			love.graphics.print("Hovered object: " .. tostring(hoveredName))
		end

		ClearShaderBuffer()

		--GUI drawing
	  	if (showInventory == true) then
			InventoryDraw()
		end

		GUIManager_Draw()

		if (scenes[currentScene].myName == "Floor0_Laboratory") then
			DrawParticlesInLab()
		end

		DrawFade()
		PauseMenuDraw()
		love.graphics.pop()
		--TLfres.letterbox(16,9) -- Needs to be drawn last
	end
	ClearShaderBuffer()
end

require("Assets/Scripts/MessageType")

local gameObjectID


function Init(aGameObjectID, aTriggerID)
	RegisterCallback(aGameObjectID, Event.OnEnter, aTriggerID, "OnEnter")
	gameObjectID = aGameObjectID
end

function OnEnter()
		SetStartPositionForPlayerWithGameObject(gameObjectID)  
end
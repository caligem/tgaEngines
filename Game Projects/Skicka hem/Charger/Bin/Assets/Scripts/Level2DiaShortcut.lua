require("Assets/Scripts/MessageType")

local gameObjectID
local Trigger = false

function Init(aGameObjectID, aTriggerID)
	RegisterCallback(aGameObjectID, Event.OnEnter, aTriggerID, "OnEnter")
end

function OnEnter()
	if (Trigger == false) then
		RunDialogue("Level2Shortcut.dlg")
		Trigger = true
	end
end
require("Assets/Scripts/MessageType")

local gameObjectID
local Trigger = false

function Init(aGameObjectID, aTriggerID)
	RegisterCallback(aGameObjectID, Event.OnEnter, aTriggerID, "OnEnter")
end

function OnEnter()
	if (Trigger == false) then 
		RunDialogue("Level3EndGame.dlg")
		Trigger = true
	end

end
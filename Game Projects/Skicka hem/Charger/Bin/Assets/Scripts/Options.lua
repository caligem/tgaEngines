function OnButtonClick(buttonName)
	if(buttonName == "Back") then
		HideCanvas("Options")
		ShowCanvas("MainMenu")
	elseif(buttonName == "PauseBack") then
		HideCanvas("Options")
		ShowCanvas("PauseMenu")
	end
end

function OnSliderChange(sliderName, value)
	if(sliderName == "MasterVolume") then
		SetMasterVolume(value);
	end
	if(sliderName == "SFXVolume") then
		SetSFXVolume(value);
	end
	if(sliderName == "MusicVolume") then
		SetMusicVolume(value);
	end

end
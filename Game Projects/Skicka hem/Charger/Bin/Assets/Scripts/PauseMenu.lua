function OnButtonClick(buttonName)
	if(buttonName == "Continue") then
		HideCanvas("PauseMenu")
		UnpauseGame()
	elseif(buttonName == "Options") then
		ShowCanvas("Options")
		HideCanvas("PauseMenu")
	elseif(buttonName == "HowToPlay") then
		ShowCanvas("HowToPlay")
		HideCanvas("PauseMenu")
	elseif(buttonName == "ExitToMenu") then
		ForceUnpauseGame()
		ExitGame()
	elseif(buttonName == "ExitToDesktop") then
		ExitToDesktop()
	end
end
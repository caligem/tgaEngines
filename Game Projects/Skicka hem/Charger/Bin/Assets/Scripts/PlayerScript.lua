require("Assets/Scripts/MessageType")

local gameObjectID
local triggerID

function Init(aGameObjectID, aTriggerID)
	gameObjectID = aGameObjectID
	triggerID = aTriggerID
	
	ShouldUpdate(gameObjectID, true)
	RegisterCallback(gameObjectID, Event.TriggerTest, triggerID, "Update")
end

function Update()
	
end

require("Assets/Scripts/MessageType")

local gameObjectID

function Init(aGameObjectID)
	gameObjectID = aGameObjectID
	Print("hello world")
	
	ParticleStop(gameObjectID)
	ParticlePlay(gameObjectID)
	triggerID = 0
	RegisterCallback(gameObjectID, Event.TriggerTest, triggerID, "LuaFunction")
	Notify(Event.TriggerTest, triggerID)
end

function Update(aDeltaTime, aTotalTime)
	local distance = DistanceBetween(gameObjectID, gameObjectID)
	if (distance >= 0) then
		SetPosition(gameObjectID, 0.0, math.cos(aTotalTime) * 10.0,0.0)
	end
end

function LuaFunction()
	ShouldUpdate(gameObjectID, false)
end


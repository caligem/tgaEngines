function OnButtonClick(buttonName)
	if(buttonName == "StartGame") then
		HideCanvas("MainMenu")
		ShowCanvas("HowToPlayPopup")
	elseif(buttonName == "LevelSelect") then
		ShowCanvas("LevelSelect")
		HideCanvas("MainMenu")
	elseif(buttonName == "Options") then
		ShowCanvas("Options")
		HideCanvas("MainMenu")
	elseif(buttonName == "HowToPlay") then
		ShowCanvas("HowToPlay")
		HideCanvas("MainMenu")
	elseif(buttonName == "Showroom") then
		StartShowroom()
	elseif(buttonName == "Credits") then
		ShowCanvas("Credits")
		HideCanvas("MainMenu")
	elseif(buttonName == "ExitToDesktop") then
		ExitToDesktop()
	end
end

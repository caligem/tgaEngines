#include "../Deferred/DataPass.si"
#include "../CameraBuffer.si"

cbuffer InstanceData : register(b1)
{
	float4x4 toWorld;
}

cbuffer BoneData : register(b2)
{
	float4x4 bones[64];
}

cbuffer VFXData : register(b3)
{
	float totalTime;
	float3 trash;
}

PixelInput main(VertexInput input)
{
	PixelInput output;

	input.myPosition.w = 1.f;
	
	float v = max(0, input.myPosition.y-2.f); //Hela Trädet
	
	float x = smoothstep(0.5f, 0.4f, input.myUV.x);//Bladen
	float l = length(float3(input.myPosition.x, 0.f, input.myPosition.z));
	
	x *= l;
	
	input.myPosition.x += x * cos(totalTime*1.f + toWorld._14) * 0.04f;//Bladen
	input.myPosition.z += x * cos(totalTime*0.86f + toWorld._24) * 0.02f;
	input.myPosition.y += x * sin(totalTime*0.53f+ toWorld._34) * 0.1f;
	
	input.myPosition.x += v * (cos(totalTime*1.0f + toWorld._14) * 0.01f)*0.6f;//Hela Trädet
	input.myPosition.z += v * (sin(totalTime*0.7f+ toWorld._34) * 0.01f)*0.5f;
	
	float4x4 rot180 = float4x4(
		-1.f, 0.f, 0.f, 0.f,
		0.f, 1.f, 0.f, 0.f,
		0.f, 0.f, -1.f, 0.f,
		0.f, 0.f, 0.f, 1.f
	);

	output.myPosition = mul(toWorld, mul(rot180, input.myPosition));
	
	output.myWorldPosition = output.myPosition;
	output.myPosition = mul(viewProjection, output.myPosition);

	output.myUV = input.myUV;
	input.myNormal.w = 0.f;
	output.myNormal.xyz = normalize(mul((float3x3)toWorld, mul((float3x3)rot180, input.myNormal.xyz)));
	output.myTangent.xyz = normalize(mul((float3x3)toWorld, mul((float3x3)rot180, input.myTangent.xyz)));
	output.myBinormal.xyz = normalize(mul((float3x3)toWorld, mul((float3x3)rot180, input.myBinormal.xyz)));

	output.myViewPosition = float4(cameraOrientation._14, cameraOrientation._24, cameraOrientation._34, 1.f);

	return output;
}

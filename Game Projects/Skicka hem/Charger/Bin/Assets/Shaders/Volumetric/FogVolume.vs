#include "FogVolume.si"

cbuffer InstanceData : register(b1)
{
	float4x4 toWorld;
}

PixelInput main(in VertexInput input)
{
	PixelInput output;
	
	input.myPosition.w = 1.f;

	output.myWorldPosition = mul(toWorld, input.myPosition);
	output.myPosition = mul(viewProjection, output.myWorldPosition);
	
	output.myUV = input.myUV;
	
	return output;
}
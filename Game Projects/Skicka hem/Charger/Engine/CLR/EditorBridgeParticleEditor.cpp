#include "stdafx.h"
#include "EditorBridgeParticleEditor.h"

#include "EditorProxyParticleEditor.h"

#define ValidateEditorProxy() if(myEditorProxy == nullptr) { return; }

typedef void(*VoidCallback)(void);

CEditorBridgeParticleEditor::CEditorBridgeParticleEditor()
{
	myEditorProxy = nullptr;
}

void CEditorBridgeParticleEditor::Init(int aEditorType, System::IntPtr aHandle)
{
	if (myEditorProxy != nullptr)
	{
		myEditorProxy->Shutdown();
		delete myEditorProxy;
	}

	EEditorType type = static_cast<EEditorType>(aEditorType);

	if (type == EEditorType_ParticleEditor)
	{
		CEditorProxyParticleEditor* newEditorProxy = new CEditorProxyParticleEditor();
		newEditorProxy->Init(type, aHandle.ToPointer());
		myEditorProxy = newEditorProxy;
		myEditorProxy->StartEngine();
	}
}

void CEditorBridgeParticleEditor::WndProc(System::Windows::Forms::Message aMessage)
{
	ValidateEditorProxy();

	System::IntPtr hwnd = aMessage.HWnd;
	System::Int32 msg = aMessage.Msg;
	System::IntPtr wParam = aMessage.WParam;
	System::IntPtr lParam = aMessage.LParam;
	
	myEditorProxy->SendWindowMessage(
		(unsigned int)msg,
		wParam.ToPointer(),
		lParam.ToPointer()
	);

	aMessage.Result = System::IntPtr::Zero;
}

void CEditorBridgeParticleEditor::Shutdown()
{
	ValidateEditorProxy();

	myEditorProxy->Shutdown();
}

bool CEditorBridgeParticleEditor::IsRunning()
{
	if (myEditorProxy == nullptr)
	{
		return false;
	}

	return myEditorProxy->IsRunning();
}

bool CEditorBridgeParticleEditor::GetHasFinishedLoading()
{
	return myEditorProxy->GetHasFinishedLoading();
}

void CEditorBridgeParticleEditor::SaveFile(System::String ^ aFilePath)
{
	char* filePath = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(aFilePath).ToPointer();
	myEditorProxy->SaveFile(filePath);
}

void CEditorBridgeParticleEditor::LoadFile(System::String ^ aFilePath, System::IntPtr aCallback)
{
	char* filePath = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(aFilePath).ToPointer();
	myEditorProxy->LoadFile(filePath);

	static_cast<VoidCallback>(aCallback.ToPointer())();
}

void CEditorBridgeParticleEditor::GotFocus()
{
	ValidateEditorProxy();
	myEditorProxy->GotFocus();
}

void CEditorBridgeParticleEditor::LostFocus()
{
	ValidateEditorProxy();
	myEditorProxy->LostFocus();
}

void CEditorBridgeParticleEditor::ResetParticleToDefault()
{
	myEditorProxy->ResetParticleToDefault();
}

void CEditorBridgeParticleEditor::ToggleDebugLines()
{
	myEditorProxy->ToggleDebugLines();
}

bool CEditorBridgeParticleEditor::IsPlaying()
{
	return myEditorProxy->IsPlaying();
}

void CEditorBridgeParticleEditor::ResetCameraPivot()
{
	myEditorProxy->ResetCameraPivot();
}

void CEditorBridgeParticleEditor::ResetCameraScale()
{
	myEditorProxy->ResetCameraScale();
}

void CEditorBridgeParticleEditor::ResetCameraRotation()
{
	myEditorProxy->ResetCameraRotation();
}

void CEditorBridgeParticleEditor::SetCubemapTexture(System::String^ aCubemapTexture)
{
	char* cubemapTexture = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(aCubemapTexture).ToPointer();
	myEditorProxy->SetCubemapTexture(cubemapTexture);
}

void CEditorBridgeParticleEditor::SetTexture(System::String^  aTexturePath)
{
	char* particleTexture = (char*)System::Runtime::InteropServices::Marshal::StringToHGlobalAnsi(aTexturePath).ToPointer();
	myEditorProxy->SetTexture(particleTexture);
}

void CEditorBridgeParticleEditor::SetDuration(float aDuration)
{
	myEditorProxy->SetDuration(aDuration);
}

void CEditorBridgeParticleEditor::SetIsLoopable(bool aIsLoopable)
{
	myEditorProxy->SetIsLoopable(aIsLoopable);
}
void CEditorBridgeParticleEditor::SetSpawnRate(float aSpawnRate)
{
	myEditorProxy->SetSpawnRate(aSpawnRate);
}

void CEditorBridgeParticleEditor::SetLifetime(float aLifetime)
{
	myEditorProxy->SetLifetime(aLifetime);
}

void CEditorBridgeParticleEditor::SetAcceleration(EditorTools::Vector3 ^ aAcceleration)
{
	float buf[3] = { aAcceleration->x, aAcceleration->y, aAcceleration->z };
	myEditorProxy->SetAcceleration(buf);
}

void CEditorBridgeParticleEditor::SetStartVelocity(EditorTools::Vector3 ^ aStartVelocity)
{
	float buf[3] = { aStartVelocity->x, aStartVelocity->y, aStartVelocity->z };
	myEditorProxy->SetStartVelocity(buf);
}

void CEditorBridgeParticleEditor::SetStartRotation(EditorTools::Vector2 ^ aStartRotation)
{
	float buf[2] = { aStartRotation->x, aStartRotation->y};
	myEditorProxy->SetStartRotation(buf);
}

void CEditorBridgeParticleEditor::SetRotationVelocity(EditorTools::Vector2 ^ aRotationVelocity)
{
	float buf[2] = { aRotationVelocity->x, aRotationVelocity->y };
	myEditorProxy->SetRotationVelocity(buf);
}

void CEditorBridgeParticleEditor::SetGravityModifier(float aGravityModifier)
{
	myEditorProxy->SetGravityModifier(aGravityModifier);
}

void CEditorBridgeParticleEditor::SetStartColor(EditorTools::Vector4 ^ aStartColor)
{
	float buf[4] = {
		aStartColor->x,
		aStartColor->y,
		aStartColor->z,
		aStartColor->w
	};
	myEditorProxy->SetStartColor(buf);
}

void CEditorBridgeParticleEditor::SetEndColor(EditorTools::Vector4 ^ aEndColor)
{
	float buf[4] = {
		aEndColor->x,
		aEndColor->y,
		aEndColor->z,
		aEndColor->w
	};
	myEditorProxy->SetEndColor(buf);
}

void CEditorBridgeParticleEditor::SetStartSize(EditorTools::Vector2 ^ aStartSize)
{
	float buf[2] = {
		aStartSize->x,
		aStartSize->y
	};
	myEditorProxy->SetStartSize(buf);
}

void CEditorBridgeParticleEditor::SetEndSize(EditorTools::Vector2 ^ aEndSize)
{
	float buf[2] = {
		aEndSize->x,
		aEndSize->y
	};
	myEditorProxy->SetEndSize(buf);
}

const float CEditorBridgeParticleEditor::GetDuration()
{
	return myEditorProxy->GetDuration();
}

System::String^ CEditorBridgeParticleEditor::GetTexturePath()
{
	const char* texturePath = myEditorProxy->GetTexturePath();

	System::String^ returnValue = gcnew System::String(texturePath);
	return returnValue;
}

const float CEditorBridgeParticleEditor::GetSpawnRate()
{
	return myEditorProxy->GetSpawnRate();
}

const float CEditorBridgeParticleEditor::GetLifeTime()
{
	return myEditorProxy->GetLifeTime();
}

const bool CEditorBridgeParticleEditor::GetIsLoopable()
{
	return myEditorProxy->GetIsLoopable();
}

EditorTools::Vector3^ CEditorBridgeParticleEditor::GetAcceleration()
{
	const float* acc = myEditorProxy->GetAcceleration();
	EditorTools::Vector3^ vec = gcnew EditorTools::Vector3(acc[0], acc[1], acc[2]);
	return vec;
}

EditorTools::Vector3 ^ CEditorBridgeParticleEditor::GetStartVelocity()
{
	const float* vel = myEditorProxy->GetStartVelocity();
	EditorTools::Vector3^ vec = gcnew EditorTools::Vector3(vel[0], vel[1], vel[2]);
	return vec;
}

EditorTools::Vector2 ^ CEditorBridgeParticleEditor::GetStartRotation()
{
	const float* startRotation = myEditorProxy->GetStartRotation();
	EditorTools::Vector2^ vec = gcnew EditorTools::Vector2(startRotation[0], startRotation[1]);
	return vec;
}

EditorTools::Vector2 ^ CEditorBridgeParticleEditor::GetRotationVelocity()
{
	const float* rotationVelocity = myEditorProxy->GetRotationVelocity();
	EditorTools::Vector2^ vec = gcnew EditorTools::Vector2(rotationVelocity[0], rotationVelocity[1]);
	return vec;
}

const float CEditorBridgeParticleEditor::GetGravityModifier()
{
	return myEditorProxy->GetGravityModifier();
}

EditorTools::Vector4 ^ CEditorBridgeParticleEditor::GetStartColor()
{
	const float* startColor = myEditorProxy->GetStartColor();
	EditorTools::Vector4^ vec = gcnew EditorTools::Vector4(startColor[0], startColor[1], startColor[2], startColor[3]);
	return vec;
}

EditorTools::Vector4 ^ CEditorBridgeParticleEditor::GetEndColor()
{
	const float* endColor = myEditorProxy->GetEndColor();
	EditorTools::Vector4^ vec = gcnew EditorTools::Vector4(endColor[0], endColor[1], endColor[2], endColor[3]);
	return vec;
}

EditorTools::Vector2 ^ CEditorBridgeParticleEditor::GetStartSize()
{
	const float* startSize = myEditorProxy->GetStartSize();
	EditorTools::Vector2^ vec = gcnew EditorTools::Vector2(startSize[0], startSize[1]);
	return vec;
}

EditorTools::Vector2 ^ CEditorBridgeParticleEditor::GetEndSize()
{
	const float* endSize = myEditorProxy->GetEndSize();
	EditorTools::Vector2^ vec = gcnew EditorTools::Vector2(endSize[0], endSize[1]);
	return vec;
}

void CEditorBridgeParticleEditor::SetBlendState(int aBlendState)
{
	myEditorProxy->SetBlendState(aBlendState);
}

int CEditorBridgeParticleEditor::GetBlendState()
{
	return myEditorProxy->GetBlendState();
}

void CEditorBridgeParticleEditor::SetShapeType(int aIndex)
{
	myEditorProxy->SetShapeType(aIndex);
}

int CEditorBridgeParticleEditor::GetShapeType()
{
	return myEditorProxy->GetShapeType();
}

void CEditorBridgeParticleEditor::SetSphereRadius(float aRadius)
{
	myEditorProxy->SetSphereRadius(aRadius);
}

void CEditorBridgeParticleEditor::SetSphereRadiusThickness(float aRadiusThickness)
{
	myEditorProxy->SetSphereRadiusThickness(aRadiusThickness);
}

float CEditorBridgeParticleEditor::GetSphereRadius()
{
	return myEditorProxy->GetSphereRadius();
}

float CEditorBridgeParticleEditor::GetSphereRadiusThickness()
{
	return myEditorProxy->GetSphereRadiusThickness();
}

void CEditorBridgeParticleEditor::SetBoxSize(EditorTools::Vector3^ aBoxSize)
{
	float buf[3] = {
		aBoxSize->x,
		aBoxSize->y,
		aBoxSize->z
	};
	myEditorProxy->SetBoxSize(buf);
}

void CEditorBridgeParticleEditor::SetBoxThickness(float aBoxThickness)
{
	myEditorProxy->SetBoxThickness(aBoxThickness);
}

EditorTools::Vector3 ^ CEditorBridgeParticleEditor::GetBoxSize()
{
	const float* boxSize = myEditorProxy->GetBoxSize();
	EditorTools::Vector3^ vec = gcnew EditorTools::Vector3(boxSize[0], boxSize[1], boxSize[2]);
	return vec;
}

const float CEditorBridgeParticleEditor::GetBoxThickness()
{
	return myEditorProxy->GetBoxThickness();
}

void CEditorBridgeParticleEditor::ParticleSystemPlay()
{
	myEditorProxy->ParticleSystemPlay();
}

void CEditorBridgeParticleEditor::ParticleSystemStop()
{
	myEditorProxy->ParticleSystemStop();
}

void CEditorBridgeParticleEditor::ParticleSystemPause()
{
	myEditorProxy->ParticleSystemPause();
}

#pragma once

#include "GrowingArray.h"
#include "Vector.h"

namespace CommonUtilities
{
	class BezierCurve
	{
	public:
		BezierCurve();
		~BezierCurve();

		void AddPoint(const Vector3f& aPoint);
		void PopPoint();
		void ClearPoints();

		const Vector3f GetPoint(const float t);
		const Vector3f GetPointAbsolute(const float t);
		inline const float GetLength() const { return myLength; }
		void BuildCurve();

	private:
		float CalcLength(float end=1.f);
		void CacheLength();
		void CacheLUT();

		bool myIsBuilt;
		GrowingArray<Vector3f> myPoints;
		GrowingArray<Vector3f> myBuffer;
		float myLength;

		static const int LUTSize = 128;
		struct Point
		{
			Vector3f myPoint;
			float myTime;
		};
		Point myLUT[LUTSize];
	};
}

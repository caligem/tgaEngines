#pragma once
#include "Vector.h"

template<class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
almost_equal(T x, T y, int ulp)
{
	// the machine epsilon has to be scaled to the magnitude of the values used
	// and multiplied by the desired precision in ULPs (units in the last place)

	return std::abs(x - y) <= std::numeric_limits<T>::epsilon() * std::abs(x + y) * ulp
		// unless the result is subnormal
		|| std::abs(x - y) < std::numeric_limits<T>::min();
}

template<class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
almost_equal(CommonUtilities::Vector3<T> lhs, CommonUtilities::Vector3<T> rhs, int ulp)
{
	// the machine epsilon has to be scaled to the magnitude of the values used
	// and multiplied by the desired precision in ULPs (units in the last place)

	return (std::abs(lhs.x - rhs.x) <= std::numeric_limits<T>::epsilon() * std::abs(lhs.x + rhs.x) * ulp
		//&& std::abs(lhs.y - rhs.y) <= std::numeric_limits<T>::epsilon() * std::abs(lhs.y + rhs.y) * ulp
		&&  std::abs(lhs.z - rhs.z) <= std::numeric_limits<T>::epsilon() * std::abs(lhs.z + rhs.z) * ulp)
		
		// unless the result is subnormal
		|| (std::abs(lhs.x - rhs.x) < std::numeric_limits<T>::min() && 
			//std::abs(lhs.y - rhs.y) < std::numeric_limits<T>::min() &&
			std::abs(lhs.z - rhs.z) < std::numeric_limits<T>::min());
}
#pragma once
#pragma warning( push )
#pragma warning( disable : 4201 )
//#include "Macros.h"
//#include "Matrix44.h"
#include "Vector3.h"
namespace CommonUtilities
{
	template <class T>
	class Quaternion
	{
	public:
		union
		{
			T myValues[4];
			//JA JAG VET!!! Men typ de flesta st�llen �r w f�rst!!!
			struct { T w; T x; T y; T z; };
		};

		Quaternion<T>();
		Quaternion<T>(const T& aW, const T& aX, const T& aY, const T& aZ);
		Quaternion<T>(const T& aYaw, const T& aPitch, const T& aRoll);
		Quaternion<T>(const Vector3<T>& aYawPitchRoll);
		Quaternion<T>(const Vector3<T>& aVector, const T aAngle);

		inline void Normalize();
		inline Quaternion<T> GetNormalized() const;

		Quaternion<T>& operator=(const Quaternion<T>& aQuat) = default;

		T Length() const;
		T Length2() const;
		inline Vector3<T> GetEulerAngles() const;
		inline Matrix33<T> GetRotationMatrix33() const;
		inline Matrix44<T> GetRotationMatrix44() const;
		inline T Dot(const Quaternion<T>& aQuat) const;


		inline static Quaternion<T> Slerp(const Quaternion<T>& aQuatA, const Quaternion<T>& aQuatB, const T& aDelta);
	};

	template<class T>
	inline Quaternion<T>::Quaternion()
	{
		w = static_cast<T>(1);
		x = static_cast<T>(0);
		y = static_cast<T>(0);
		z = static_cast<T>(0);
	}

	template<class T>
	inline Quaternion<T>::Quaternion(const T& aW, const T& aX, const T& aY, const T& aZ)
	{
		w = aW;
		x = aX;
		y = aY;
		z = aZ;
	}

	template<class T>
	inline Quaternion<T>::Quaternion(const T& aYaw, const T& aPitch, const T& aRoll)
	{
		T cy = cos(aYaw * T(0.5));
		T sy = sin(aYaw * T(0.5));
		T cr = cos(aRoll * T(0.5));
		T sr = sin(aRoll * T(0.5));
		T cp = cos(aPitch * T(0.5));
		T sp = sin(aPitch * T(0.5));

		w = cy * cr * cp + sy * sr * sp;
		x = cy * sr * cp - sy * cr * sp;
		y = cy * cr * sp + sy * sr * cp;
		z = sy * cr * cp - cy * sr * sp;
	}

	template<class T>
	inline Quaternion<T>::Quaternion(const Vector3<T>& aYawPitchRoll)
	{
		T cy = cos(aYawPitchRoll.y * T(0.5));
		T sy = sin(aYawPitchRoll.y * T(0.5));
		T cr = cos(aYawPitchRoll.z * T(0.5));
		T sr = sin(aYawPitchRoll.z * T(0.5));
		T cp = cos(aYawPitchRoll.x * T(0.5));
		T sp = sin(aYawPitchRoll.x * T(0.5));

		w = cy * cr * cp + sy * sr * sp;
		x = cy * sr * cp - sy * cr * sp;
		y = cy * cr * sp + sy * sr * cp;
		z = sy * cr * cp - cy * sr * sp;
	}


	template<class T>
	inline Quaternion<T>::Quaternion(const Vector3<T>& aVector, const T aAngle)
	{
		w = cos(aAngle / T(2));
		x = axis.x * sin(aAngle / T(2));
		y = axis.y * sin(aAngle / T(2));
		z = axis.z * sin(aAngle / T(2));
	}

	template <class T> inline Quaternion<T> operator*(const Quaternion<T>& aQuat, const T& aScalar)
	{
		return Quaternion<T>(aQuat.w * aScalar, aQuat.x * aScalar, aQuat.y * aScalar, aQuat.z * aScalar);
	}

	template <class T> inline Quaternion<T> operator*(const T& aScalar, const Quaternion<T>& aQuat)
	{
		return Quaternion<T>(aQuat.w * aScalar, aQuat.x * aScalar, aQuat.y * aScalar, aQuat.z * aScalar);
	}

	template <class T> inline Quaternion<T> operator*(const Quaternion<T>& aQuat0, const Quaternion<T>& aQuat1)
	{
		return Quaternion<T>(
			(aQuat1.w * aQuat0.w) - (aQuat1.x * aQuat0.x) - (aQuat1.y * aQuat0.y) - (aQuat1.z * aQuat0.z),
			(aQuat1.w * aQuat0.x) + (aQuat1.x * aQuat0.w) + (aQuat1.y * aQuat0.z) - (aQuat1.z * aQuat0.y),
			(aQuat1.w * aQuat0.y) + (aQuat1.y * aQuat0.w) + (aQuat1.z * aQuat0.x) - (aQuat1.x * aQuat0.z),
			(aQuat1.w * aQuat0.z) + (aQuat1.z * aQuat0.w) + (aQuat1.x * aQuat0.y) - (aQuat1.y * aQuat0.x)
			);
	}

	template <class T> inline void operator*=(Quaternion<T>& aQuat, const T& aScalar)
	{
		aQuat.w *= aScalar;
		aQuat.x *= aScalar;
		aQuat.y *= aScalar;
		aQuat.z *= aScalar;
	}

	template <class T> inline void operator*=(Quaternion<T>& aQuat0, const Quaternion<T>& aQuat1)
	{
		T w = aQuat0.w;
		T x = aQuat0.x;
		T y = aQuat0.y;
		T z = aQuat0.z;
		
		//aQuat0.w = w * aQuat1.w - x * aQuat1.x - y * aQuat1.y - z * aQuat1.z;
		//aQuat0.x = w * aQuat1.x + x * aQuat1.w + y * aQuat1.z - z * aQuat1.y;
		//aQuat0.y = w * aQuat1.y - x * aQuat1.z + y * aQuat1.w + z * aQuat1.x;
		//aQuat0.z = w * aQuat1.z + x * aQuat1.y - y * aQuat1.x + z * aQuat1.w;


		aQuat0.w = (aQuat1.w * w) - (aQuat1.x * x) - (aQuat1.y * y) - (aQuat1.z * z);
		aQuat0.x = (aQuat1.w * x) + (aQuat1.x * w) + (aQuat1.y * z) - (aQuat1.z * y);
		aQuat0.y = (aQuat1.w * y) + (aQuat1.y * w) + (aQuat1.z * x) - (aQuat1.x * z);
		aQuat0.z = (aQuat1.w * z) + (aQuat1.z * w) + (aQuat1.x * y) - (aQuat1.y * x);

	}

	template <class T> inline Quaternion<T> operator/(const Quaternion<T>& aQuat, const T& aScalar)
	{
		return Quaternion<T>(aQuat.w / aScalar, aQuat.x / aScalar, aQuat.y / aScalar, aQuat.z / aScalar);
	}

	template <class T> inline Quaternion<T> operator-(const Quaternion<T>& aQuatA, const Quaternion<T>& aQuatB)
	{
		return Quaternion<T>(aQuatA.w - aQuatB.w, aQuatA.x - aQuatB.x, aQuatA.y - aQuatB.y, aQuatA.z - aQuatB.z);
	}

	template <class T> inline Quaternion<T> operator-(const Quaternion<T>& aQuat)
	{
		return Quaternion<T>(-aQuat.w, -aQuat.x, -aQuat.y, -aQuat.z);
	}

	template <class T> inline Quaternion<T> operator+(const Quaternion<T>& aQuatA, const Quaternion<T>& aQuatB)
	{
		return Quaternion<T>(aQuatA.w + aQuatB.w, aQuatA.x + aQuatB.x, aQuatA.y + aQuatB.y, aQuatA.z + aQuatB.z);
	}

	template<class T>
	inline void Quaternion<T>::Normalize()
	{
		T length = Length();
		w /= length;
		x /= length;
		y /= length;
		z /= length;
	}

	template<class T>
	inline Quaternion<T> Quaternion<T>::GetNormalized() const
	{
		T length = Length();
		return Quaternion<T>( w / length, x / length, y / length, z / length);
	}

	template<class T>
	inline T Quaternion<T>::Length2() const
	{
		return (x * x) + (y * y) + (z * z) + (w * w);
	}

	template<class T>
	inline T Quaternion<T>::Length() const
	{
		return sqrt(Length2());
	}

	template<class T>
	inline Matrix33<T> Quaternion<T>::GetRotationMatrix33() const
	{
		Matrix33<T> result;
		T qxx(x * x);
		T qyy(y * y);
		T qzz(z * z);
		T qxz(x * z);
		T qxy(x * y);
		T qyz(y * z);
		T qwx(w * x);
		T qwy(w * y);
		T qwz(w * z);
		
		result.myRows[0][0] = T(1) - T(2) * (qyy + qzz);
		result.myRows[0][1] = T(2) * (qxy + qwz);
		result.myRows[0][2] = T(2) * (qxz - qwy);
		
		result.myRows[1][0] = T(2) * (qxy - qwz);
		result.myRows[1][1] = T(1) - T(2) * (qxx + qzz);
		result.myRows[1][2] = T(2) * (qyz + qwx);
		
		result.myRows[2][0] = T(2) * (qxz + qwy);
		result.myRows[2][1] = T(2) * (qyz - qwx);
		result.myRows[2][2] = T(1) - T(2) * (qxx + qyy);
		return result;
	}

	template<class T>
	inline Matrix44<T> Quaternion<T>::GetRotationMatrix44() const
	{
		Matrix44<T> result;
		T qxx(x * x);
		T qyy(y * y);
		T qzz(z * z);
		T qxz(x * z);
		T qxy(x * y);
		T qyz(y * z);
		T qwx(w * x);
		T qwy(w * y);
		T qwz(w * z);
		
		result.myRows[0][0] = T(1) - T(2) * (qyy + qzz);
		result.myRows[0][1] = T(2) * (qxy + qwz);
		result.myRows[0][2] = T(2) * (qxz - qwy);
		
		result.myRows[1][0] = T(2) * (qxy - qwz);
		result.myRows[1][1] = T(1) - T(2) * (qxx + qzz);
		result.myRows[1][2] = T(2) * (qyz + qwx);
		
		result.myRows[2][0] = T(2) * (qxz + qwy);
		result.myRows[2][1] = T(2) * (qyz - qwx);
		result.myRows[2][2] = T(1) - T(2) * (qxx + qyy);
		return result;
	}

	template<class T>
	inline T Quaternion<T>::Dot(const Quaternion<T>& aQuat) const
	{
		return x * aQuat.x + y * aQuat.y + z * aQuat.z + w * aQuat.w;
	}

	template<class T>
	inline Vector3<T> Quaternion<T>::GetEulerAngles() const
	{
		// roll (x-axis rotation)
		T sinr = T(2.0) * (w * x + y * z);
		T cosr = T(1.0) - T(2.0) * (x * x + y * y);
		T roll = atan2(sinr, cosr);

		// pitch (y-axis rotation)
		/*
		T sinp = T(2.0) * (w * y - z * x);
		T pitch = T(0.0);
		if (fabs(sinp) > T(1))
		{
			pitch = copysign(CommonUtilities::Pif * T(0.5), sinp); // use 90 degrees if out of range
		}
		else
		{
			pitch = asin(sinp);
		}
		*/
		T t2 = T(2.0) * (w * y - z * x);
		if (t2 > T(1.0))
		{
			t2 = T(1.0);
		}
		if (t2 < T(-1.0))
		{
			t2 = T(-1.0);
		}
		T pitch = asin(t2);


		// yaw (z-axis rotation)
		T siny = T(2.0) * (w * z + x * y);
		T cosy = T(1.0) - T(2.0) * (y * y + z * z);
		T yaw = atan2(siny, cosy);

		return Vector3<T>(roll, pitch, yaw);
	}

	template<class T>
	inline Quaternion<T> Quaternion<T>::Slerp(const Quaternion<T>& aQuatA, const Quaternion<T>& aQuatB, const T& aDelta) 
	{
		Quaternion<T> qz = aQuatB;
		
		T cosTheta = aQuatA.Dot(aQuatB);
		
		// If cosTheta < 0, the interpolation will take the long way around the sphere. 
		// To fix this, one quat must be negated.
		if (cosTheta < T(0))
		{
			qz = -qz;
			cosTheta = -cosTheta;
		}
		
		const T dotThreshold = static_cast<T>(0.9995);
		// Perform a linear interpolation when cosTheta is close to 1 to avoid side effect of sin(angle) becoming a zero denominator
		if (cosTheta > T(1) - dotThreshold)
		{
			// Linear interpolation
			return Lerp(aQuatA, qz, aDelta);
		}
		else
		{
			// Essential Mathematics, page 467
			T angle = acos(cosTheta);
			return (sin((T(1) - aDelta) * angle) * aQuatA + sin(aDelta * angle) * qz) / sin(angle);
		}

	}

	typedef Quaternion<float> Quaternionf;
	typedef Quaternion<double> Quaterniond;
	typedef Quaternion<int> Quaternioni;
	typedef Quaternion<unsigned int> Quaternionui;
	typedef Quaternion<short>Quaternions;
	typedef Quaternion<unsigned short> Quaternionus;
	typedef Quaternion<char> Quaternionc;
	typedef Quaternion<unsigned char> Quaternionuc;

	typedef Quaternion<float> Quatf;
	typedef Quaternion<double> Quatd;
	typedef Quaternion<int> Quati;
	typedef Quaternion<unsigned int> Quatui;
	typedef Quaternion<short>Quats;
	typedef Quaternion<unsigned short> Quatus;
	typedef Quaternion<char> Quatc;
	typedef Quaternion<unsigned char> Quatuc;
}
namespace CU = CommonUtilities;
#pragma warning( pop )
# CommonUtilities

This repository contains a lot of *Utilities* that will be used during my time at The Game Assembly.

## What's in it?

These *CommonUtilities* contain classes for things like:

- Time handling
- Input handling
- Math library
  - Vector math
  - Matrix math
- Simple physics
  - 2D Lines and Line Volumes
  - 3D Planes and Plane Volumes
- Random class
- Debug logging

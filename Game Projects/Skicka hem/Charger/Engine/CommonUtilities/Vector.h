#pragma once

#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"

namespace CommonUtilities
{
	typedef Vector2<double> Vector2d;
	typedef Vector2<float> Vector2f;
	typedef Vector2<int> Vector2i;
	typedef Vector2<short> Vector2s;

	typedef Vector3<double> Vector3d;
	typedef Vector3<float> Vector3f;
	typedef Vector3<int> Vector3i;
	typedef Vector3<short> Vector3s;

	typedef Vector4<double> Vector4d;
	typedef Vector4<float> Vector4f;
	typedef Vector4<int> Vector4i;
	typedef Vector4<short> Vector4s;
}

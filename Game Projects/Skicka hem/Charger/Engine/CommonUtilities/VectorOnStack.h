#pragma once

#include <assert.h>

#include "Macros.h"

namespace CommonUtilities
{
	
	template <typename T, int size, typename CountType = unsigned short, bool useSafeModeFlag = true>
	class VectorOnStack
	{
	public:
		VectorOnStack();
		VectorOnStack(const VectorOnStack& aVectorOnStack);

		~VectorOnStack();

		VectorOnStack& operator=(const VectorOnStack& aVectorOnStack);

		inline const T& operator[](const CountType& aIndex) const;
		inline T& operator[](const CountType& aIndex);

		inline const T& GetLast() const;
		inline T& GetLast();

		inline void Add(const T& aObject);
		inline void Insert(CountType aIndex, T& aObject);

		inline void DeleteCyclic(T& aObject);
		inline void DeleteCyclicAtIndex(CountType aItemNumber);

		inline void RemoveCyclic(T& aObject);
		inline void RemoveCyclicAtIndex(CountType aItemNumber);

		inline void DeleteAll();
		inline void Clear();

		inline CountType Size() const;

	private:
		T myData[size];

		CountType myCount;
	};
	

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline VectorOnStack<T, size, CountType, useSafeModeFlag>::VectorOnStack()
	{
		myCount = static_cast<CountType>(0);
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline VectorOnStack<T, size, CountType, useSafeModeFlag>::VectorOnStack(const VectorOnStack & aVectorOnStack)
	{
		if (useSafeModeFlag)
		{
			for (CountType i = 0; i < aVectorOnStack.Size(); ++i)
			{
				myData[i] = aVectorOnStack[i];
			}
		}
		else
		{
			memcpy(myData, aVectorOnStack.myData, sizeof(T)*size);
		}
		myCount = aVectorOnStack.Size();
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline VectorOnStack<T, size, CountType, useSafeModeFlag>::~VectorOnStack()
	{
		myCount = 0;
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline VectorOnStack<T, size, CountType, useSafeModeFlag> & VectorOnStack<T, size, CountType, useSafeModeFlag>::operator=(const VectorOnStack & aVectorOnStack)
	{
		if (useSafeModeFlag)
		{
			for (CountType i = 0; i < aVectorOnStack.Size(); ++i)
			{
				myData[i] = aVectorOnStack[i];
			}
		}
		else
		{
			memcpy(myData, aVectorOnStack.myData, sizeof(T)*size);
		}
		myCount = aVectorOnStack.Size();
		return *this;
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline const T & VectorOnStack<T, size, CountType, useSafeModeFlag>::operator[](const CountType & aIndex) const
	{
		assert(aIndex >= 0 && aIndex < myCount && "Subscript index out of bounds");
		return myData[aIndex];
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline T & VectorOnStack<T, size, CountType, useSafeModeFlag>::operator[](const CountType & aIndex)
	{
		assert(aIndex >= 0 && aIndex < myCount && "Subscript index out of bounds");
		return myData[aIndex];
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline const T & VectorOnStack<T, size, CountType, useSafeModeFlag>::GetLast() const
	{
		assert(myCount > 0 && "GetLast on empty array");
		return myData[myCount - 1];
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline T & VectorOnStack<T, size, CountType, useSafeModeFlag>::GetLast()
	{
		assert(myCount > 0 && "GetLast on empty array");
		return myData[myCount - 1];
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline void VectorOnStack<T, size, CountType, useSafeModeFlag>::Add(const T & aObject)
	{
		assert(myCount < size && "Add when array is full");
		myData[myCount] = aObject;
		++myCount;
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline void VectorOnStack<T, size, CountType, useSafeModeFlag>::Insert(CountType aIndex, T & aObject)
	{
		assert(myCount < size && "Add when array is full");
		for (CountType i = myCount - 1; i > aIndex; --i)
		{
			myData[i] = myData[i - 1];
		}
		myData[aIndex] = aObject;
		++myCount;
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline void VectorOnStack<T, size, CountType, useSafeModeFlag>::DeleteCyclic(T & aObject)
	{
		for (CountType i = myCount; i > 0; --i)
		{
			if (myData[i - 1] == aObject)
			{
				SAFE_DELETE(myData[i - 1]);
				myData[i - 1] = myData[myCount - 1];
				--myCount;
			}
		}
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline void VectorOnStack<T, size, CountType, useSafeModeFlag>::DeleteCyclicAtIndex(CountType aItemNumber)
	{
		assert(aItemNumber >= 0 && aItemNumber < myCount && "DeleteCyclicAtIndex out of bounds");
		SAFE_DELETE(myData[aItemNumber]);
		myData[aItemNumber] = myData[myCount - 1];
		--myCount;
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline void VectorOnStack<T, size, CountType, useSafeModeFlag>::RemoveCyclic(T & aObject)
	{
		for (CountType i = myCount; i > 0; --i)
		{
			if (myData[i - 1] == aObject)
			{
				myData[i - 1] = myData[myCount - 1];
				--myCount;
			}
		}
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline void VectorOnStack<T, size, CountType, useSafeModeFlag>::RemoveCyclicAtIndex(CountType aItemNumber)
	{
		assert(aItemNumber >= 0 && aItemNumber < myCount && "DeleteCyclicAtIndex out of bounds");
		myData[aItemNumber] = myData[myCount - 1];
		--myCount;
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline void VectorOnStack<T, size, CountType, useSafeModeFlag>::Clear()
	{
		myCount = 0;
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline void VectorOnStack<T, size, CountType, useSafeModeFlag>::DeleteAll()
	{
		for (CountType i = 0; i < myCount; ++i)
		{
			SAFE_DELETE(myData[i]);
		}
		Clear();
	}

	template<typename T, int size, typename CountType, bool useSafeModeFlag>
	inline CountType VectorOnStack<T, size, CountType, useSafeModeFlag>::Size() const
	{
		return myCount;
	}
}
#pragma once

#include <Windows.h>
#include <Xinput.h>
#pragma comment(lib, "XInput.lib")

#include "Vector.h"

namespace CommonUtilities
{
	enum XAxis
	{
		XAxis_LX,
		XAxis_LY,
		XAxis_RX,
		XAxis_RY,
		XAxis_LT,
		XAxis_RT
	};
	enum XButton
	{
		XButton_Up = 0x01,
		XButton_Down = 0x02,
		XButton_Left = 0x04,
		XButton_Right = 0x08,
		XButton_Start = 0x10,
		XButton_Back = 0x20,
		XButton_LS = 0x40,
		XButton_RS = 0x80,
		XButton_LB = 0x100,
		XButton_RB = 0x200,
		XButton_A = 0x1000,
		XButton_B = 0x2000,
		XButton_X = 0x4000,
		XButton_Y = 0x8000,
	};
	enum XStick
	{
		XStick_Left,
		XStick_Right
	};

	class XBOXController
	{
	public:
		XBOXController(int aPlayerIndex);
		~XBOXController();

		bool IsConnected();

		void Update();

		bool IsButtonPressed(XButton aButton);
		bool IsButtonDown(XButton aButton);
		bool IsButtonReleased(XButton aButton);

		float GetAxis(XAxis aAxis);

		CommonUtilities::Vector2f GetStick(XStick aStick);

		void Vibrate(float aLeftMotorVel = 0.25f, float aRightMotorVel = 0.25f);

		void SetVibration(bool aVibrate);
		bool GetVibration() const { return myVibrate; }
		XINPUT_STATE GetCurrentState() const { return myCurrentState; }
	private:
		XINPUT_STATE myBufferState;
		XINPUT_STATE myPreviousState;
		XINPUT_STATE myCurrentState;
		int myPlayerIndex;

		bool myVibrate;
		
	};

}

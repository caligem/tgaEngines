#pragma once

#include "Vector.h"

#include "GraphicsPipeline.h"

namespace CommonUtilities
{
	class Timer;
}
namespace Input
{
	class CInputManager;
}
namespace CommonUtilities
{
	class XBOXController;
}

class IEditor
{
public:

	static const CommonUtilities::Vector2f GetWindowSize();
	static const CommonUtilities::Vector2f GetCanvasSize();

	static CommonUtilities::Timer& Time();
	static CommonUtilities::XBOXController& XBOX();
	static Input::CInputManager& Input();

};


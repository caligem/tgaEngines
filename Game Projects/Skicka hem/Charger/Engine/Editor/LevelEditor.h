#pragma once

#include "Editor.h"
#include "State.h"
#include "ManipulationTool.h"

class CLevelEditor : public CEditor, public CState
{
public:
	CLevelEditor();
	~CLevelEditor();

	virtual void InitCallback() override;
	virtual void UpdateCallback() override;

	virtual EStateUpdate Update() override { return EDoNothing; }
	virtual void OnEnter() override {}
	virtual void OnLeave() override {}

private:
	void UpdateCameraControls();
	void ManipulationToolHandler();
	void DrawGrid();

	CManipulationTool myManipulationTool;

	CommonUtilities::Vector3f myPivot;
	CommonUtilities::Vector2f myRotation;
	float myZoom;

	CGameObject myFocus;
};


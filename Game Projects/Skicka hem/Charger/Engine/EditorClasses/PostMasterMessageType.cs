﻿#if __cplusplus
#pragma once
#define public
#endif
#if !__cplusplus
namespace PostMaster
{
#endif
    public enum EDataType
    {
        EDataType_Invalid = -1,
        EDataType_Vector2 = 8,
        EDataType_Vector3 = 12,
        EDataType_Vector4 = 16,
		EDataType_String = 4,
        EDataType_Count
    };
	public enum EMessageType
	{
		EMessageType_Invalid = -1,
		//Transform
		EMessageType_Transform_SetPosition = 0,
		EMessageType_Transform_SetRotation,
		EMessageType_Transform_SetScale,
		//MeshFilter
		EMessageType_MeshFilter_AddMesh,

		EMessageType_Count
	};
    public enum EEditorType
    {
        EEditorType_Invalid = -1,
        EEditorType_LevelEditor = 0,
        EEditorType_ParticleEditor,
        EEditorType_ModelViewer,
        EEditorType_Count
    };
#if !__cplusplus
}
#endif
#if __cplusplus
#undef public
#endif

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EditorControls
{
	public partial class EngineCanvas : UserControl
	{
		public CEditorBridgeWrapper myBridge;
		public EngineCanvas()
		{
			InitializeComponent();
		}
		private void EngineCanvas_Load(object sender, EventArgs e)
		{
			if (this.Parent != null)
			{
				this.Size = this.Parent.Size;
			}

			myBridge = new CEditorBridgeWrapper();
			myBridge.Init(this.Handle);
		}

		protected override void WndProc(ref Message m)
		{
			if(myBridge != null)
			{
				myBridge.WndProc(ref m);
			}
			base.WndProc(ref m);
		}
		public void Shutdown()
		{
			myBridge.Shutdown();
		}

        public void GetFocus()
        {
            myBridge.GotFocus();
        }

        public void LoseFocus()
        {
            myBridge.LostFocus();
        }
    }
}

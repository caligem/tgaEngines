#include "stdafx.h"
#include "EditorProxyParticleEditor.h"

#include "ParticleEditor.h"
#include <windows.h>
#include <DL_Debug.h>

#include "Editor.h"

#ifdef _DEBUG
#pragma comment(lib,"Editor_Debug.lib")
#endif // DEBUG
#ifdef _RELEASE
#pragma comment(lib,"Editor_Release.lib")
#endif // _RELEASE

#define ValidateEditor() if(myEditor == nullptr) { return; }

CEditorProxyParticleEditor::CEditorProxyParticleEditor()
{
}


CEditorProxyParticleEditor::~CEditorProxyParticleEditor()
{
}

void CEditorProxyParticleEditor::Init(EEditorType aEditorType, void * aHandle)
{
	switch (aEditorType)
	{
	case EEditorType_ParticleEditor:
		myEditor = new CParticleEditor();
		myEditor->Init((HWND)aHandle);
		break;
	}
}

void CEditorProxyParticleEditor::Shutdown()
{
	ValidateEditor();
	myEditor->Shutdown();
}

void CEditorProxyParticleEditor::StartEngine()
{
	ValidateEditor();
	myEditor->StartEngine();
}

bool CEditorProxyParticleEditor::IsRunning()
{
	return myEditor->IsRunning();
}

bool CEditorProxyParticleEditor::GetHasFinishedLoading()
{
	return static_cast<CParticleEditor*>(myEditor)->GetHasFinishedLoading();
}

void CEditorProxyParticleEditor::SaveFile(const char * aFilePath)
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SaveFile(aFilePath);
}

void CEditorProxyParticleEditor::LoadFile(const char * aFilePath)
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->LoadFile(aFilePath);
}

void CEditorProxyParticleEditor::GotFocus()
{
	ValidateEditor();
	myEditor->GotFocus();
}

void CEditorProxyParticleEditor::LostFocus()
{
	ValidateEditor();
	myEditor->LostFocus();
}

void CEditorProxyParticleEditor::SendWindowMessage(unsigned int Msg, void* wParam, void* lParam)
{
	ValidateEditor();

	myEditor->SendWindowMessage(Msg, wParam, lParam);
}

void CEditorProxyParticleEditor::ToggleDebugLines()
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->ToggleDebugLines();
}

bool CEditorProxyParticleEditor::IsPlaying()
{
	return static_cast<CParticleEditor*>(myEditor)->IsPlaying();
}

void CEditorProxyParticleEditor::ResetCameraPivot()
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->ResetCameraPivot();
}

void CEditorProxyParticleEditor::ResetCameraScale()
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->ResetCameraScale();
}

void CEditorProxyParticleEditor::ResetCameraRotation()
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->ResetCameraRotation();
}

void CEditorProxyParticleEditor::ResetParticleToDefault()
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->ResetParticleToDefault();
}

void CEditorProxyParticleEditor::SetCubemapTexture(const char * aCubemapTexturePath)
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetCubemapTexture(aCubemapTexturePath);
}

void CEditorProxyParticleEditor::SetTexture(const char * aTexturePath)
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetTexture(aTexturePath);
}

void CEditorProxyParticleEditor::SetDuration(float aDuration)
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetDuration(aDuration);
}

void CEditorProxyParticleEditor::SetSpawnRate(float aSpawnRate)
{
	static_cast<CParticleEditor*>(myEditor)->SetSpawnRate(aSpawnRate);
}

void CEditorProxyParticleEditor::SetLifetime(float aLifetime)
{
	static_cast<CParticleEditor*>(myEditor)->SetLifetime(aLifetime);
}

void CEditorProxyParticleEditor::SetIsLoopable(bool aIsLoopable)
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetIsLoopable(aIsLoopable);
}

void CEditorProxyParticleEditor::SetAcceleration(float aAcceleration[3])
{
	static_cast<CParticleEditor*>(myEditor)->SetAcceleration(aAcceleration);
}

void CEditorProxyParticleEditor::SetStartVelocity(float aAcceleration[3])
{
	static_cast<CParticleEditor*>(myEditor)->SetStartVelocity(aAcceleration);
}

void CEditorProxyParticleEditor::SetStartColor(float aStartColor[4])
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetStartColor(aStartColor);
}

void CEditorProxyParticleEditor::SetEndColor(float aEndColor[4])
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetEndColor(aEndColor);
}

void CEditorProxyParticleEditor::SetStartSize(float aStartSize[2])
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetStartSize(aStartSize);
}

void CEditorProxyParticleEditor::SetEndSize(float aEndSize[2])
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetEndSize(aEndSize);
}

const float CEditorProxyParticleEditor::GetDuration()
{
	return static_cast<CParticleEditor*>(myEditor)->GetDuration();
}

const char * CEditorProxyParticleEditor::GetTexturePath()
{
	return static_cast<CParticleEditor*>(myEditor)->GetTexturePath();
}

const float CEditorProxyParticleEditor::GetSpawnRate()
{
	return static_cast<CParticleEditor*>(myEditor)->GetSpawnRate();
}

const float CEditorProxyParticleEditor::GetLifeTime()
{
	return static_cast<CParticleEditor*>(myEditor)->GetLifeTime();
}

const bool CEditorProxyParticleEditor::GetIsLoopable()
{
	return static_cast<CParticleEditor*>(myEditor)->GetIsLoopable();
}

const float * CEditorProxyParticleEditor::GetAcceleration()
{
	return static_cast<CParticleEditor*>(myEditor)->GetAcceleration();
}

const float * CEditorProxyParticleEditor::GetStartVelocity()
{
	return static_cast<CParticleEditor*>(myEditor)->GetStartVelocity();
}

const float * CEditorProxyParticleEditor::GetStartColor()
{
	return static_cast<CParticleEditor*>(myEditor)->GetStartColor();
}

const float * CEditorProxyParticleEditor::GetEndColor()
{
	return static_cast<CParticleEditor*>(myEditor)->GetEndColor();
}

const float * CEditorProxyParticleEditor::GetStartSize()
{
	return static_cast<CParticleEditor*>(myEditor)->GetStartSize();
}

const float * CEditorProxyParticleEditor::GetEndSize()
{
	return static_cast<CParticleEditor*>(myEditor)->GetEndSize();
}

void CEditorProxyParticleEditor::SetBlendState(int aBlendState)
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetBlendState(aBlendState);
}

int CEditorProxyParticleEditor::GetBlendState()
{
	return static_cast<CParticleEditor*>(myEditor)->GetBlendState();
}

void CEditorProxyParticleEditor::SetShapeType(int aIndex)
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetShapeType(aIndex);
}

int CEditorProxyParticleEditor::GetShapeType()
{
	return static_cast<CParticleEditor*>(myEditor)->GetShapeType();
}

void CEditorProxyParticleEditor::SetSphereRadius(float aRadius)
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetSphereRadius(aRadius);
}

void CEditorProxyParticleEditor::SetSphereRadiusThickness(float aRadiusThickness)
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetSphereRadiusThickness(aRadiusThickness);
}

float CEditorProxyParticleEditor::GetSphereRadius()
{
	return static_cast<CParticleEditor*>(myEditor)->GetSphereRadius();
}

float CEditorProxyParticleEditor::GetSphereRadiusThickness()
{
	return static_cast<CParticleEditor*>(myEditor)->GetSphereRadiusThickness();
}

void CEditorProxyParticleEditor::SetBoxSize(float aBoxSize[3])
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetBoxSize(aBoxSize);
}

void CEditorProxyParticleEditor::SetBoxThickness(float aBoxThickness)
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetBoxThickness(aBoxThickness);
}

const float * CEditorProxyParticleEditor::GetBoxSize()
{
	return static_cast<CParticleEditor*>(myEditor)->GetBoxSize();
}

const float CEditorProxyParticleEditor::GetBoxThickness()
{
	return static_cast<CParticleEditor*>(myEditor)->GetBoxThickness();
}

void CEditorProxyParticleEditor::SetStartRotation(float aStartRotation[2])
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetStartRotation(aStartRotation);
}

void CEditorProxyParticleEditor::SetRotationVelocity(float aRotationVelocity[2])
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetRotationVelocity(aRotationVelocity);
}

void CEditorProxyParticleEditor::SetGravityModifier(float aGravityModifier)
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->SetGravityModifier(aGravityModifier);
}

const float* CEditorProxyParticleEditor::GetStartRotation()
{
	return static_cast<CParticleEditor*>(myEditor)->GetStartRotation();
}

const float* CEditorProxyParticleEditor::GetRotationVelocity()
{
	return static_cast<CParticleEditor*>(myEditor)->GetRotationVelocity();
}

const float CEditorProxyParticleEditor::GetGravityModifier()
{
	return static_cast<CParticleEditor*>(myEditor)->GetGravityModifier();
}
 
#define CopyDataToCPPArray(T) \
newData = new unsigned char[sizeof(T)]; \
memcpy(newData, pData, sizeof(T)); 

void CEditorProxyParticleEditor::SendMessageToEngine(unsigned char* pData, EMessageType aMessageType, EDataType aDataType)
{
	pData; aMessageType; aDataType;

	CommonUtilities::Vector3f vec;

	unsigned char* newData = nullptr;

	using namespace CommonUtilities;

	switch (aDataType)
	{
	case EDataType_Vector2:
		CopyDataToCPPArray(Vector2f);
		break;
	case EDataType_Vector3:
		CopyDataToCPPArray(Vector3f);
		break;
	case EDataType_Vector4:
		CopyDataToCPPArray(Vector4f);
		break;
	}

	myEditor->RecieveMessage(newData, aMessageType, aDataType);
}

void CEditorProxyParticleEditor::SetEditorMessageCallback(void(*aCallback)(const unsigned char *, EMessageType, EDataType))
{
	ValidateEditor();
	myEditor->SetEditorMessageCallback(aCallback);
}

void CEditorProxyParticleEditor::ParticleSystemPlay()
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->ParticleSystemPlay();
}

void CEditorProxyParticleEditor::ParticleSystemStop()
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->ParticleSystemStop();
}

void CEditorProxyParticleEditor::ParticleSystemPause()
{
	ValidateEditor();
	static_cast<CParticleEditor*>(myEditor)->ParticleSystemPause();
}

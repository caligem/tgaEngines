#pragma once
#include "Component.h"

class CAnimationControllerComponent : public CComponent
{
public:
	CAnimationControllerComponent();
	~CAnimationControllerComponent();

	inline const float GetTime() const { return myTimer; }
	void SetTime(const float& aTime) { myTimer = aTime; }
	void SetAnimation(const std::string& aAnimation);
	void SetAnimationForced(const std::string& aAnimation);
	void SetLooping(const bool aLooping) { myIsLooping = aLooping; }
	const std::string& GetAnimation() const { return myAnimation; }

	const float GetCurrentAnimationDuration() const { return myDuration; }
	const float GetCurrentAnimationTicksPerSecond() const { return myTicksPerSeconds; }

private:
	friend class CComponentSystem;
	friend class CGraphicsPipeline;

	const int32_t GetAnimationIndex() const { return myAnimationIndex; }
	void Init(CModelComponent* aComponent);

	void Release() override;

	void Update();
	ID_T(CModel) myModelID;
	std::string myAnimation;

	float myTicksPerSeconds;
	float myDuration;
	float myTimer;
	int32_t myAnimationIndex;
	bool myIsLooping;
};


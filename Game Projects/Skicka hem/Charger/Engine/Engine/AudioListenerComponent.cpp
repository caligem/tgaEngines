#include "stdafx.h"
#include "AudioListenerComponent.h"
#include "AudioManager.h"

CAudioListenerComponent::CAudioListenerComponent()
{
}


CAudioListenerComponent::~CAudioListenerComponent()
{
}

bool CAudioListenerComponent::Init()
{
	//Must be first
	AM.SetAudioListener(&myAudioListener);
	//--------
	return true;
}

void CAudioListenerComponent::OnStart()
{
}

void CAudioListenerComponent::Update()
{
}

void CAudioListenerComponent::SetPosition(const CommonUtilities::Vector3f& aPosition)
{
	myAudioListener.SetPosition(aPosition);
}

void CAudioListenerComponent::OnOwnerDestroyed()
{

}

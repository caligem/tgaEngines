#include "stdafx.h"
#include "CanvasComponent.h"

#include "IWorld.h"
#include "ScriptManager.h"
#include "FileWatcher.h"
#include "AudioManager.h"

#define FocusedUIElement(Code) if(myFocusedUIElement != nullptr) {myFocusedUIElement->Code;}
#define ActiveUIElement(Code) if(myActiveUIElement != nullptr) {myActiveUIElement->Code;}

CCanvasComponent::CCanvasComponent()
	: myFocusedUIElement(nullptr)
	, myActiveUIElement(nullptr)
	, myIsRendered(false)
{
	myStartAlpha = 0.f;
}


CCanvasComponent::~CCanvasComponent()
{
}

void CCanvasComponent::SetScript(const std::string & aScriptPath)
{
	myLuaScript = IWorld::Script().AquireLuaScriptForNonGameObjects(aScriptPath.c_str());
}

void CCanvasComponent::Show()
{
	myState = EState_Fade;
	myTimer = 0.f;
	myStartAlpha = myAlpha;
	myTargetAlpha = 1.f;
	myIsRendered = true;
}

void CCanvasComponent::Hide()
{
	myState = EState_Fade;
	myTimer = 0.f;
	myStartAlpha = myAlpha;
	myTargetAlpha = 0.f;
	myIsRendered = false;
}

void CCanvasComponent::ForceHide()
{
	myState = EState_Hidden;
	myTimer = 0.f;
	myAlpha = 0.f;
	myTargetAlpha = 0.f;
	myIsRendered = false;
}

void CCanvasComponent::ForceShow()
{
	myState = EState_Shown;
	myTimer = 0.f;
	myAlpha = 1.f;
	myTargetAlpha = 1.f;
	myIsRendered = true;
}

void CCanvasComponent::Init(const SComponentData & aComponentData)
{
	myUIElements.Init(8);

	if ((aComponentData.mySize.x <= 0.f) || (aComponentData.mySize.y <= 0.f))
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to init Canvas component with < 0.f size");
		return;
	}

	if (aComponentData.myRenderMode == ERenderMode_WorldSpace)
	{
		myCanvas.Init(aComponentData.mySize);
	}
	else if (aComponentData.myRenderMode == ERenderMode_ScreenSpaceOverlay)
	{
		myCanvas.Init(IEngine::GetCanvasSize());
	}

	mySpriteRenderCommands.Init();
	myTextRenderCommands.Init();

	myRenderMode = aComponentData.myRenderMode;

	myState = EState_Shown;
	myTimer = 0.f;
	myAlpha = 1.f;
	myTargetAlpha = 0.f;
}

void CCanvasComponent::Init(ERenderMode aRenderMode, CommonUtilities::Vector2f aSize)
{
	Init({ aRenderMode, aSize });
}

void CCanvasComponent::Update()
{
	if (myState == EState_Fade)
	{
		HandleTransition();
	}
	if (myState != EState_Shown)
	{
		return;
	}

	CalculateIntersectionPoint();
	SetFocusUI();
	
	HandleInput();
}

void CCanvasComponent::FillRenderCommands()
{
	if (myState == EState_Hidden)
	{
		return;
	}
	for (auto uiElement : myUIElements)
	{
		uiElement->FillRenderCommands(mySpriteRenderCommands, myTextRenderCommands);
	}
}

void CCanvasComponent::SwapBuffers()
{
	myTextRenderCommands.SwapBuffers();
	mySpriteRenderCommands.SwapBuffers();
}

void CCanvasComponent::OnEnter()
{
	for (auto element : myUIElements)
	{
		if (element->GetName() == "MasterVolume")
		{
			CSliderComponent* component = dynamic_cast<CSliderComponent*>(element);
			if (component)
			{
				component->SetCurrentValue(AM.GetVolume(AudioChannel::Master) / 100.f);
			}
		}
		else if (element->GetName() == "SFXVolume")
		{
			CSliderComponent* component = dynamic_cast<CSliderComponent*>(element);
			if (component)
			{
				component->SetCurrentValue(AM.GetVolume(AudioChannel::SoundEffects) / 100.f);
			}
		}
		else if (element->GetName() == "MusicVolume")
		{
			CSliderComponent* component = dynamic_cast<CSliderComponent*>(element);
			if (component)
			{
				component->SetCurrentValue(AM.GetVolume(AudioChannel::Music) / 100.f);
			}
		}
	}
}

void CCanvasComponent::Release()
{
	myUIElements.RemoveAll();
}

void CCanvasComponent::SetFocusUI()
{
	CUIElement* newFocus = nullptr;

	for (auto uiElement : myUIElements)
	{
		CUIElement* element = uiElement->GetFocus(myIntersection);
		if (element->GetBounds().Contains(myIntersection))
		{
			newFocus = element;
			break;
		}
	}

	if (!myActiveUIElement)
	{
		if (myFocusedUIElement != newFocus)
		{
			FocusedUIElement(OnLeave());
			myFocusedUIElement = newFocus;
			FocusedUIElement(OnEnter());
		}
	}
	else
	{
		if (myFocusedUIElement == myActiveUIElement && newFocus != myActiveUIElement)
		{
			ActiveUIElement(OnLeave())
		}
		else if (myFocusedUIElement != myActiveUIElement && newFocus == myActiveUIElement)
		{
			ActiveUIElement(OnEnter());
		}
		myFocusedUIElement = newFocus;
	}
}

void CCanvasComponent::CalculateIntersectionPoint()
{
	Input::CInputManager& input = IEngine::GetInputManager();


	CommonUtilities::Vector2f mousePos = {
		input.GetMousePosition(1).x / IEngine::GetWindowSize().x,
		input.GetMousePosition(1).y / IEngine::GetWindowSize().y
	};

	if (myRenderMode == ERenderMode_WorldSpace)
	{
		CommonUtilities::Matrix44f viewProjInv = CommonUtilities::Matrix44f::Inverse(IEngine::GetGraphicsPipeline().GetSavedCameraBuffer().myViewProjection);

		mousePos.x = mousePos.x * 2.f - 1.f;
		mousePos.y = (1.f - mousePos.y) * 2.f - 1.f;


		CommonUtilities::Vector4f rayOrigin = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 0.f, 1.f) * viewProjInv;
		rayOrigin /= rayOrigin.w;
		CommonUtilities::Vector4f rayEnd = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 1.f, 1.f) * viewProjInv;
		rayEnd /= rayEnd.w;

		CommonUtilities::Vector3f rayDir = rayEnd - rayOrigin;
		rayDir.Normalize();

		CommonUtilities::Vector3f normal = myGameObjectData->GetTransform().GetForward().GetNormalized();

		float denom = normal.Dot(rayDir);
		if (denom > 1e-6)
		{
			CommonUtilities::Vector3f p0l0 = myGameObjectData->GetTransform().GetPosition() - CommonUtilities::Vector3f(rayOrigin);

			float t = p0l0.Dot(normal) / denom;

			CommonUtilities::Vector3f intersection = CommonUtilities::Vector3f(rayOrigin) + rayDir*t;
			intersection -= myGameObjectData->GetTransform().GetPosition();

			intersection = {
				intersection.Dot(myGameObjectData->GetTransform().GetRight().GetNormalized()) / (myCanvas.GetSize().x*myGameObjectData->GetTransform().GetLocalScale().x),
				intersection.Dot(myGameObjectData->GetTransform().GetUp().GetNormalized()) / (myCanvas.GetSize().y*myGameObjectData->GetTransform().GetLocalScale().y),
				intersection.Dot(myGameObjectData->GetTransform().GetForward())
			};

			myIntersection.x = (intersection.x + 1.f) / 2.f;
			myIntersection.y = 1.f - ((intersection.y + 1.f) / 2.f);
		}
	}
	else if (myRenderMode == ERenderMode_ScreenSpaceOverlay)
	{
		myIntersection = mousePos;
	}
}

void CCanvasComponent::HandleTransition()
{
	myTimer += IWorld::Time().GetRealDeltaTime();

	float f = CommonUtilities::Clamp01(myTimer / TransitionTime);

	myAlpha = CommonUtilities::Lerp(myStartAlpha, myTargetAlpha, f);

	if (f >= 1.f)
	{
		if (myTargetAlpha <= 0.f)
		{
			myState = EState_Hidden;
		}
		else
		{
			myState = EState_Shown;
		}
	}
}

void CCanvasComponent::HandleInput()
{
	Input::CInputManager& input = IEngine::GetInputManager();
	ActiveUIElement(OnMouseMove(myIntersection));
	if (input.IsButtonPressed(Input::Button_Left, 1))
	{
		myActiveUIElement = myFocusedUIElement;
		ActiveUIElement(OnMouseDown(myIntersection));
	}
	if (input.IsButtonReleased(Input::Button_Left, 1))
	{
		ActiveUIElement(OnMouseUp(myIntersection));
		if (myFocusedUIElement != myActiveUIElement)
		{
			FocusedUIElement(OnMouseUp(myIntersection));
		}
		myActiveUIElement = nullptr;
	}
}

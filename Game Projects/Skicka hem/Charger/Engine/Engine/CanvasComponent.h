#pragma once
#include "Component.h"
#include "FullscreenTexture.h"
#include "DoubleBuffer.h"

class CComponentSystem;
class CUIElement;
class CGraphicsPipeline;
class C2DRenderer;
class CLuaScript;

class CCanvasComponent : public CComponent
{
public:
	enum ERenderMode
	{
		ERenderMode_ScreenSpaceOverlay,
		ERenderMode_ScreenSpaceCamera,
		ERenderMode_WorldSpace,
		ERenderMode_Count
	};

	struct SComponentData
	{
		ERenderMode myRenderMode;
		CommonUtilities::Vector2f mySize;
	};

	CCanvasComponent();
	~CCanvasComponent();

	template<typename T>
	T* AddUIElement(const typename T::SComponentData& aComponentData);

	void SetRenderMode(ERenderMode aRenderMode) { myRenderMode = aRenderMode; }
	ERenderMode GetRenderMode() const { return myRenderMode; }

	void SetScript(const std::string& aScriptPath);

	bool IsMouseInside() const { return myIntersection.x >= 0.f && myIntersection.x <= 1.f && myIntersection.y >= 0.f && myIntersection.y <= 1.f; } 
	const CommonUtilities::Vector2f& GetMousePoint() const { return myIntersection; }

	void Show();
	void Hide();
	void ForceHide();
	void ForceShow();

	void OnEnter();
	void OnLeave() {};

	float GetAlpha() const { return myAlpha; }

	//bool GetWasContinueButtonPressed(); //ugly, but is for howToPlay-popup
	bool GetIsRendered() const { return myIsRendered; }

private:
	friend CComponentSystem;
	friend CGraphicsPipeline;
	friend C2DRenderer;

	void Init(const SComponentData& aComponentData);
	void Init(
		ERenderMode aRenderMode,
		CommonUtilities::Vector2f aSize
	);
	void Update();
	void HandleInput();

	void FillRenderCommands();
	void SwapBuffers();

	void Release() override;

	void SetFocusUI();

	const CommonUtilities::GrowingArray<SSpriteRenderCommand>& GetSpriteReadBuffer() { return mySpriteRenderCommands.GetReadBuffer(); }
	const CommonUtilities::GrowingArray<STextRenderCommand>& GetTextReadBuffer() { return myTextRenderCommands.GetReadBuffer(); }
	void CalculateIntersectionPoint();
	void HandleTransition();
	CDoubleBuffer<SSpriteRenderCommand> mySpriteRenderCommands;
	CDoubleBuffer<STextRenderCommand> myTextRenderCommands;

	CommonUtilities::GrowingArray<CUIElement*> myUIElements;

	CFullscreenTexture myCanvas;
	CommonUtilities::Vector2f myIntersection;

	CUIElement* myFocusedUIElement;
	CUIElement* myActiveUIElement;

	ERenderMode myRenderMode;

	ID_T(CLuaScript) myLuaScript;

	enum EState
	{
		EState_Fade,
		EState_Shown,
		EState_Hidden
	};
	static constexpr float TransitionTime = 0.2f;
	float myTimer = 0.f;
	float myAlpha;
	float myTargetAlpha;
	float myStartAlpha;
	bool myIsRendered;
	EState myState;
};

template<typename T>
inline T* CCanvasComponent::AddUIElement(const typename T::SComponentData & aComponentData)
{
	CGameObject temp;
	temp.Init(mySceneID, -1, myGameObjectDataID);
	T* uiComponent = temp.AddComponent<T>(aComponentData);
	uiComponent->SetCanvasLuaScript(&myLuaScript);
	myUIElements.Add(uiComponent);
	return uiComponent;
}

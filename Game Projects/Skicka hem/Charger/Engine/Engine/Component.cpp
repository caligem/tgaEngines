#include "stdafx.h"
#include "Component.h"

#include "ComponentSystem.h"

CComponent::CComponent()
{
}

CComponent::~CComponent()
{
}

void CComponent::SetParent(ID_T(CGameObjectData) aParentID, ID_T(CScene) aSceneID, int aID, CGameObjectData* aGameObjectData)
{
	myGameObjectDataID = aParentID;
	mySceneID = aSceneID;
	myID = aID;
	myGameObjectData = aGameObjectData;
}

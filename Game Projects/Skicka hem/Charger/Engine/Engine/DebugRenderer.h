#pragma once

#include "Vector.h"
#include "DoubleBuffer.h"
#include <mutex>

#include "VertexDataWrapper.h"
#include "ConstantBuffer.h"
#include "RenderCommand.h"
#include "TextComponent.h"
#include "Model.h"

class CVertexShader;
class CPixelShader;

#ifndef _RETAIL

class CDebugRenderer
{
	struct STextQuadBufferData
	{
		CommonUtilities::Vector2f myScreenSize;
		CommonUtilities::Vector2f mySpriteSize;
		CommonUtilities::Vector2f myPosition;
		CommonUtilities::Vector2f myScale;
		CommonUtilities::Vector2f myPivot;
		CommonUtilities::Vector2f myUVOffset;
		CommonUtilities::Vector2f myUVScale;
		float myRotation;
		float myPadding;
		CommonUtilities::Vector4f myTint;
		CommonUtilities::Vector4f myOutline;
	};

public:
	CDebugRenderer();
	~CDebugRenderer();

	bool Init();

	void RenderDebugMeshes(CConstantBuffer & aCameraBufferData);
	void RenderDebugLines(CConstantBuffer & aCameraBufferData);
	void RenderText();
	void SwapBuffers();

	void DrawDebugLine(
		const CommonUtilities::Vector3f& aSource = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aDestination = { 1.f, 1.f, 1.f }
	);
	void DrawDebugWireCube(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	void DrawDebugWireSphere(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	void DrawDebugWireCircle(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	void DrawDebugCube(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	void DrawDebugSphere(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	void DrawDebugArrow(
		const CommonUtilities::Vector3f& aStartPosition = { 0.f, 0.f, 0.f},
		const CommonUtilities::Vector3f& aEndPosition = { 1.f, 1.f, 1.f}
	);
	void SetColor(const CommonUtilities::Vector4f & aColor);

	void DrawDebugText(const std::string& aDebugText);

private:
	bool InitWireLineModel();
	bool InitWireBoxModel();
	bool InitWireSphereModel();
	bool InitWireCircleModel();
	bool InitMeshModels();

	void RenderWireModel(SVertexDataWrapper& aModel, const CommonUtilities::Matrix44f& aMatrix, const CommonUtilities::Vector4f& aColor);
	void RenderMeshModel(CModel::SModelData& aModel, const CommonUtilities::Matrix44f& aMatrix);

	struct SLineData{
		CommonUtilities::Vector3f mySource;
		CommonUtilities::Vector3f myDestination;
		CommonUtilities::Vector4f myColor;
	};
	struct SBoxData{
		CommonUtilities::Vector3f myPosition;
		CommonUtilities::Vector3f myScale;
		CommonUtilities::Vector3f myRotation;
		CommonUtilities::Vector4f myColor;
	};
	struct SSphereData{
		CommonUtilities::Vector3f myPosition;
		CommonUtilities::Vector3f myScale;
		CommonUtilities::Vector3f myRotation;
		CommonUtilities::Vector4f myColor;
	};
	struct SInstanceBufferData
	{
		CommonUtilities::Matrix44f myToWorld;
		CommonUtilities::Vector4f myColor;
	};

	CVertexShader* myDebugVertexShader;
	CPixelShader* myDebugPixelShader;
	CVertexShader* myMeshVertexShader;
	CPixelShader* myMeshPixelShader;

	CConstantBuffer myInstanceBuffer;
	CConstantBuffer myQuadBuffer;

	CDoubleBuffer<SLineData> myLineBuffer;
	CDoubleBuffer<SBoxData> myBoxBuffer;
	CDoubleBuffer<SSphereData> mySphereBuffer;
	CDoubleBuffer<SSphereData> myCircleBuffer;
	CDoubleBuffer<STextRenderCommand> myTextBuffer;
	CDoubleBuffer<SBoxData> myMeshCubeBuffer;
	CDoubleBuffer<SSphereData> myMeshSphereBuffer;

	SVertexDataWrapper myLineModel;
	SVertexDataWrapper myWireCubeModel;
	SVertexDataWrapper myWireSphereModel;
	SVertexDataWrapper myWireCircleModel;
	CModel::SModelData myMeshCubeModel;
	CModel::SModelData myMeshSphereModel;

	std::mutex myLineMutex;
	std::mutex myWireCubeMutex;
	std::mutex myWireSphereMutex;
	std::mutex myWireCircleMutex;
	std::mutex myWireTextMutex;
	std::mutex myMeshCubeMutex;
	std::mutex myMeshSphereMutex;

	CommonUtilities::Vector4f myColor;

	CTextComponent myTextComponent;
};

#else

class CDebugRenderer
{
public:
	CDebugRenderer() {}
	~CDebugRenderer() {}

	bool Init() { return true; }

	void RenderDebugMeshes(CConstantBuffer &) {}
	void RenderDebugLines(CConstantBuffer & ) {}
	void RenderText() {}
	void SwapBuffers() {}

	void DrawDebugLine(
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&
	) {}
	void DrawDebugWireCube(
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&
	) {}
	void DrawDebugWireSphere(
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&
	) {}
	void DrawDebugWireCircle(
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&
	) {}
	void DrawDebugCube(
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&
	) {}
	void DrawDebugSphere(
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&
	) {}
	void DrawDebugArrow(
		const CommonUtilities::Vector3f&, 
		const CommonUtilities::Vector3f&
	) {}

	void SetColor(const CommonUtilities::Vector4f &) {}
	void DrawDebugText(const std::string&) {}
};

#endif
#include "stdafx.h"
#include "DeferredRenderer.h"
#include "DirectXFramework.h"
#include "RenderCommand.h"
#include "ModelManager.h"
#include "IEngine.h"
#include "IWorld.h"
#include "Scene.h"
#include "SceneManager.h"
#include "JsonDocument.h"

#include "ShaderManager.h"
#include "Shader.h"

#include <d3d11.h>

#include "TextureManager.h"

CDeferredRenderer::CDeferredRenderer()
{
	myDataPassVertexShader = nullptr;
	myDataPassPixelShader = nullptr;
	myLightPassScreenVertexShader = nullptr;
	myLightPassPixelShaders = { nullptr };
}

CDeferredRenderer::~CDeferredRenderer()
{
}

bool CDeferredRenderer::Init()
{
	if (!myCameraBuffer.Init(sizeof(SCameraBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create CDeferredRenderer::myCameraBuffer");
	}
	if (!myLightCameraBuffer.Init(sizeof(SLightCameraBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create CDeferredRenderer::myCameraBuffer");
	}

	if (!myInstanceBuffer.Init(sizeof(SInstanceBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myInstanceBuffer");
		return false;
	}
	if (!myBoneBuffer.Init(sizeof(SBoneBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myBoneBuffer");
		return false;
	}
	if (!myVFXBuffer.Init(sizeof(SVFXBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myVFXBuffer");
		return false;
	}

	if (!myDirectionalLightBuffer.Init(sizeof(SLightBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myDirectionalLightBuffer");
		return false;
	}
	if (!myPointLightBuffer.Init(sizeof(SPointLightBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myPointLightBuffer");
		return false;
	}
	if (!mySpotLightBuffer.Init(sizeof(SSpotLightBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::mySpotLightBuffer");
		return false;
	}
	if (!myScreenBuffer.Init(sizeof(SScreenData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myScreenBuffer");
		return false;
	}
	if (!mySquareShadowMap.Init({ 1024.f, 1024.f }, DXGI_FORMAT_R32_FLOAT, true))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::mySquareShadowMap");
		return false;
	}
	if (!myBigSquareShadowMap.Init({ 2048.f, 2048.f }, DXGI_FORMAT_R32_FLOAT, true))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myBigSquareShadowMap");
		return false;
	}
	if (!myCubeShadowMap.InitCube({ 512.f, 512.f }, DXGI_FORMAT_R32_FLOAT, true))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myCubeShadowMap");
		return false;
	}

	CShaderManager& shaderManager = IEngine::GetShaderManager();

	myDataPassVertexShader = &shaderManager.GetVertexShader(L"Assets/Shaders/Deferred/DataPass.vs", EShaderInputLayoutType_PBR);
	myDataPassPixelShader = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/DataPass.ps");
	myDepthVertexShader = &shaderManager.GetVertexShader(L"Assets/Shaders/Deferred/Depth.vs", EShaderInputLayoutType_PBR);
	myDepthPixelShader = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/Depth.ps");

	myLightPassScreenVertexShader = &shaderManager.GetVertexShader(L"Assets/Shaders/Deferred/LightPass.vs", EShaderInputLayoutType_PPFX);
	myLightPassWorldVertexShader = &shaderManager.GetVertexShader(L"Assets/Shaders/Deferred/LightPassWorld.vs", EShaderInputLayoutType_PPFX);
	myLightPassPixelShaders[ELightType_Ambient] = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/AmbientLightPass.ps");
	myLightPassPixelShaders[ELightType_Direct] = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/DirectLightPass.ps");
	myLightPassPixelShaders[ELightType_Point] = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/PointLightPass.ps");
	myLightPassPixelShaders[ELightType_Spot] = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/SpotLightPass.ps");

	if (!CreateVertexBuffer())
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myVertexBuffer");
		return false;
	}

	myIcosphere = IEngine::GetModelManager().myModelLoader.CreateIcosphere(1);

	return true;
}

void CDeferredRenderer::RenderDataPass(const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender, CConstantBuffer & aCameraBuffer, const CommonUtilities::Vector4f & aCameraPosition, const CommonUtilities::CFrustum & aFrustum)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);

	SInstanceBufferData instanceData;

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	
	SVFXBufferData vfxData;
	vfxData.myTotalTime = IEngine::Time().GetTotalTime();
	vfxData.myPlayerPosition = IEngine::GetGraphicsPipeline().GetSavedPlayerPosition();
	myVFXBuffer.SetData(&vfxData);
	myVFXBuffer.SetBuffer(3, EShaderType_Vertex);
	myVFXBuffer.SetBuffer(3, EShaderType_Pixel);

	for (const SModelRenderCommand& command : aModelsToRender)
	{
		CModel* model = IEngine::GetModelManager().GetModel(command.myModelToRender);
		if (!model)
		{
			continue;
		}
		if (aFrustum.CullSphere(command.myTransform.myPosition, model->myModelData.myRadius * command.myScale.Length()))
		{
			continue;
		}

		CMaterial* modelMaterial = &model->myMaterial;
		if (command.myMaterial.GetVertexShader() != nullptr) 
		{
			command.myMaterial.GetVertexShader()->Bind();
			command.myMaterial.GetVertexShader()->BindLayout();
		}
		else if (modelMaterial->GetVertexShader() != nullptr) 
		{ 
			modelMaterial->GetVertexShader()->Bind(); 
			modelMaterial->GetVertexShader()->BindLayout();
		}
		else 
		{ 
			myDataPassVertexShader->Bind(); 
			myDataPassVertexShader->BindLayout();
		}
		
		if (command.myMaterial.GetPixelShader() != nullptr)
		{
			command.myMaterial.GetPixelShader()->Bind();
		}
		else if (modelMaterial->GetPixelShader() != nullptr)
		{
			modelMaterial->GetPixelShader()->Bind();
		}
		else 
		{
			myDataPassPixelShader->Bind();
		}		

		if (command.myMaterial.HasCustomData())
		{
			vfxData.myCustomData = command.myMaterial.GetCustomData();
			myVFXBuffer.SetData(&vfxData);
			myVFXBuffer.SetBuffer(3, EShaderType_Vertex);
			myVFXBuffer.SetBuffer(3, EShaderType_Pixel);
		}
		else if (modelMaterial->HasCustomData())
		{
			vfxData.myCustomData = modelMaterial->GetCustomData();
			myVFXBuffer.SetData(&vfxData);
			myVFXBuffer.SetBuffer(3, EShaderType_Vertex);
			myVFXBuffer.SetBuffer(3, EShaderType_Pixel);
		}

		float distance = (command.myTransform.myPosition - CommonUtilities::Vector3f(aCameraPosition)).Length2();

		const SVertexDataWrapper& vertexData = model->GetVertexData(model->CalculateLodLevel(distance));
		STextureDataWrapper& textureData = model->GetTextureData();

		instanceData.myToWorld = command.myTransform;

		myInstanceBuffer.SetData(&instanceData);

		context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
		context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

		if (model->myModelData.mySceneAnimator && model->myModelData.mySceneAnimator->HasSkeleton())
		{
			model->myModelData.mySceneAnimator->SetAnimIndex(command.myAnimationID);

			auto& transforms = model->myModelData.mySceneAnimator->GetTransforms(command.myAnimationTime);

			myBoneBuffer.SetData(&transforms[0], static_cast<int>(transforms.size()) * sizeof(CommonUtilities::Matrix44f));
		}
		else
		{
			auto specialVec = CommonUtilities::Vector4f(0.f, 0.f, 0.f, -1.f);
			myBoneBuffer.SetData(&specialVec, sizeof(CommonUtilities::Vector4f));
		}

		myBoneBuffer.SetBuffer(2, EShaderType_Vertex);

		ID3D11ShaderResourceView* textures[STextureDataWrapper::maxTextures];
		for (int i = 0; i < STextureDataWrapper::maxTextures; ++i)
		{
			textures[i] = IEngine::GetTextureManager().GetTexture(textureData.myTextures[i]);
		}
		context->PSSetShaderResources(0, STextureDataWrapper::maxTextures, textures);

		context->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);
	}

	myDataPassVertexShader->Unbind();
	myDataPassVertexShader->UnbindLayout();
	myDataPassPixelShader->Unbind();
}

void CDeferredRenderer::RenderLightPass(CConstantBuffer & aCameraBuffer, const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender, const CommonUtilities::GrowingArray<SDirectionalLightRenderCommand>& aDirectionalLightRenderCommands, const CommonUtilities::GrowingArray<SPointLightRenderCommand>& aPointLightRenderCommands, const CommonUtilities::GrowingArray<SSpotLightRenderCommand>& aSpotLightRenderCommands, CFullscreenTexture & aGBuffer, const CommonUtilities::CFrustum & aFrustum, CFullscreenTexture & aLightTexture, CGraphicsStateManager & aStateManager, SRV aNoiseVolumeTexture)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	ID3D11ShaderResourceView* cubemap = IEngine::GetTextureManager().GetTexture(IEngine::GetGraphicsPipeline().GetCubemap());

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	SScreenData screenData;
	screenData.myScreenSize = IEngine::GetCanvasSize();
	screenData.myScreenSize = aLightTexture.GetSize();
	myScreenBuffer.SetData(&screenData);
	myScreenBuffer.SetBuffer(1, EShaderType_Pixel);

	aLightTexture.SetAsActiveTarget();
	aStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
	aStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Additive);

	aGBuffer.SetAsResourceOnSlot(0);
	context->PSSetShaderResources(aGBuffer.GetNumTargets(), 1, &cubemap);

	context->IASetVertexBuffers(0, 1, &myVertexData.myVertexBuffer, &myVertexData.myStride, &myVertexData.myOffset);
	context->IASetIndexBuffer(myVertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	aCameraBuffer.SetBuffer(0, EShaderType_Pixel);
	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);

	SInstanceBufferData instanceData;
	instanceData.myToWorld = CommonUtilities::Matrix44f();
	myInstanceBuffer.SetData(&instanceData);
	myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

	myLightPassScreenVertexShader->Bind();
	myLightPassScreenVertexShader->BindLayout();
	myLightPassPixelShaders[ELightType_Ambient]->Bind();
	RenderAmbientLight();

	myLightPassPixelShaders[ELightType_Direct]->Bind();
	RenderDirectionalLights(aCameraBuffer, aDirectionalLightRenderCommands, aModelsToRender, aFrustum, aGBuffer, aLightTexture, aStateManager);

	myLightPassWorldVertexShader->Bind();
	myLightPassWorldVertexShader->BindLayout();

	RenderPointLights(aCameraBuffer, aPointLightRenderCommands, aModelsToRender, aFrustum, aGBuffer, aLightTexture, aStateManager);

	RenderSpotLights(aCameraBuffer, aSpotLightRenderCommands, aModelsToRender, aFrustum, aGBuffer, aLightTexture, aStateManager, aNoiseVolumeTexture);

	myLightPassScreenVertexShader->Unbind();
	myLightPassScreenVertexShader->UnbindLayout();
	myLightPassPixelShaders[ELightType_Ambient]->Unbind();
}

bool CDeferredRenderer::CreateVertexBuffer()
{
	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
		float u, v;
	} vertices[4] =
	{
		{-1.f, +1.f, 0.f, 1.f,	0.f, 0.f},
		{+1.f, +1.f, 0.f, 1.f,	1.f, 0.f},
		{-1.f, -1.f, 0.f, 1.f,	0.f, 1.f},
		{+1.f, -1.f, 0.f, 1.f,	1.f, 1.f}
	};
	unsigned int indices[6] =
	{
		0,1,2,
		1,3,2
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = IEngine::GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		return false;
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = IEngine::GetDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		return false;
	}

	//Setup VertexData
	myVertexData.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	myVertexData.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	myVertexData.myStride = sizeof(Vertex);
	myVertexData.myOffset = 0;
	myVertexData.myVertexBuffer = vertexBuffer;
	myVertexData.myIndexBuffer = indexBuffer;

	return true;
}

void CDeferredRenderer::RenderAmbientLight()
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	context->DrawIndexed(myVertexData.myNumberOfIndices, 0, 0);
}

void CDeferredRenderer::RenderDirectionalLights(CConstantBuffer & aCameraBuffer, const CommonUtilities::GrowingArray<SDirectionalLightRenderCommand>& aLightRenderCommands, const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender, const CommonUtilities::CFrustum & aFrustum, CFullscreenTexture & aGBuffer, CFullscreenTexture & aLightTexture, CGraphicsStateManager & aStateManager)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	ID3D11ShaderResourceView* cubemap = IEngine::GetTextureManager().GetTexture(IEngine::GetGraphicsPipeline().GetCubemap());

	SLightBufferData lightData;

	aFrustum;

	for (const SDirectionalLightRenderCommand& lightRenderCommand : aLightRenderCommands)
	{
		auto& cameraOrientation = IEngine::GetGraphicsPipeline().GetSavedCameraBuffer().myCameraOrientation;
		CTransform transform;
		transform.SetPosition(cameraOrientation.myPosition + cameraOrientation.myForwardAxis*cameraOrientation.myPosition.y - lightRenderCommand.myDirection*40.f);
		if (std::fabsf(lightRenderCommand.myDirection.Dot({ 0.f, 1.f, 0.f })) == 1.f)
		{
			transform.SetLookDirection(lightRenderCommand.myDirection, { 0.f, 0.f, 1.0f });
		}
		else
		{
			transform.SetLookDirection(lightRenderCommand.myDirection);
		}

		auto lightViewMatrix = transform.GetOrientation();
		lightViewMatrix = lightViewMatrix.GetFastInverse();
		auto lightProjectionMatrix = CommonUtilities::CreateOrthographicProjectionMatrix(60.f, IEngine::GetWindowRatio(), 0.1f, 100.f);
		auto frustum = CommonUtilities::BuildFrustumFromViewProjectionMatrix(lightViewMatrix * lightProjectionMatrix);

		SCameraBufferData data;
		data.myCameraOrientation = transform.GetOrientation();
		data.myToCamera = data.myCameraOrientation.GetFastInverse();
		data.myProjection = lightProjectionMatrix;
		data.myInvertedProjection = CommonUtilities::Matrix44f::Inverse(data.myProjection);
		data.myViewProjection = data.myToCamera * data.myProjection;
		data.myInvertedViewProjection = CommonUtilities::Matrix44f::Inverse(data.myViewProjection);

		SLightCameraBufferData lightCameraBufferData;
		lightCameraBufferData.myViewProjection[0] = data.myViewProjection;
		myLightCameraBuffer.SetData(&lightCameraBufferData);

		myCameraBuffer.SetData(&data);

		myCameraBuffer.SetBuffer(0, EShaderType_Vertex);
		myCameraBuffer.SetBuffer(0, EShaderType_Pixel);

		myBigSquareShadowMap.ClearTexture({ FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX });
		myBigSquareShadowMap.SetAsActiveTarget();
		aStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_Depth);
		aStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
		RenderDepthToTexture(aModelsToRender, frustum, transform.GetPosition());

		myLightPassScreenVertexShader->Bind();
		myLightPassScreenVertexShader->BindLayout();
		myLightPassPixelShaders[ELightType_Direct]->Bind();

		aStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Additive);
		aStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
		aLightTexture.SetAsActiveTarget();
		aGBuffer.SetAsResourceOnSlot(0);
		context->PSSetShaderResources(aGBuffer.GetNumTargets(), 1, &cubemap);
		myBigSquareShadowMap.SetAsResourceOnSlot(aGBuffer.GetNumTargets() + 1);
		aCameraBuffer.SetBuffer(0, EShaderType_Vertex);
		aCameraBuffer.SetBuffer(0, EShaderType_Pixel);
		myScreenBuffer.SetBuffer(1, EShaderType_Pixel);
		myLightCameraBuffer.SetBuffer(3, EShaderType_Pixel);

		SInstanceBufferData instanceData;
		instanceData.myToWorld = CommonUtilities::Matrix44f();
		myInstanceBuffer.SetData(&instanceData);
		myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

		lightData.myDirectionalLight = lightRenderCommand.myDirection;
		lightData.myDirectionalLightColor = {
			lightRenderCommand.myColor.x,
			lightRenderCommand.myColor.y,
			lightRenderCommand.myColor.z,
			lightRenderCommand.myIntensity
		};

		myDirectionalLightBuffer.SetData(&lightData);
		myDirectionalLightBuffer.SetBuffer(2, EShaderType_Pixel);

		context->IASetVertexBuffers(0, 1, &myVertexData.myVertexBuffer, &myVertexData.myStride, &myVertexData.myOffset);
		context->IASetIndexBuffer(myVertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		context->DrawIndexed(myVertexData.myNumberOfIndices, 0, 0);
	}
}

void CDeferredRenderer::RenderPointLights(CConstantBuffer & aCameraBuffer, const CommonUtilities::GrowingArray<SPointLightRenderCommand>& aLightRenderCommands, const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender, const CommonUtilities::CFrustum & aFrustum, CFullscreenTexture & aGBuffer, CFullscreenTexture & aLightTexture, CGraphicsStateManager & aStateManager)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	ID3D11ShaderResourceView* cubemap = IEngine::GetTextureManager().GetTexture(IEngine::GetGraphicsPipeline().GetCubemap());

	SInstanceBufferData instanceData;
	instanceData.myToWorld = CommonUtilities::Matrix44f();

	static constexpr int NumDirections = 6;
	static const CommonUtilities::Vector3f Directions[NumDirections] =
	{
		{ 0.f, CommonUtilities::Pif / 2.f, 0.f },
		{ 0.f,-CommonUtilities::Pif / 2.f, 0.f },
		{-CommonUtilities::Pif / 2.f, 0.f, 0.f },
		{ CommonUtilities::Pif / 2.f, 0.f, 0.f },
		{ 0.f, 0.f, 0.f },
		{ 0.f, CommonUtilities::Pif, 0.f }
	};

	SPointLightBufferData pointLightData;
	for (const SPointLightRenderCommand& lightRenderCommand : aLightRenderCommands)
	{
		CTransform transform;
		transform.SetPosition(lightRenderCommand.myPosition);
		if (aFrustum.CullSphere(transform.GetPosition(), lightRenderCommand.myRange*transform.GetScale().Length()))
		{
			continue;
		}

		myCubeShadowMap.ClearTexture({ FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX });
		if (lightRenderCommand.myCastShadows)
		{
			aStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_Depth);
			aStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);

			SLightCameraBufferData lightCameraBufferData;

			for (int i = 0; i < NumDirections; ++i)
			{
				myCubeShadowMap.ClearDepth();
				transform.SetLookDirection({ 0.f, 0.f, 1.f });
				transform.Rotate(Directions[i]);
				auto lightViewMatrix = transform.GetOrientation();
				lightViewMatrix = lightViewMatrix.GetFastInverse();
				auto lightProjectionMatrix = CommonUtilities::CreatePerspectiveProjectionMatrix(90.f, 1.f, 0.01f, lightRenderCommand.myRange);
				auto frustum = CommonUtilities::BuildFrustumFromViewProjectionMatrix(lightViewMatrix * lightProjectionMatrix);

				SCameraBufferData data;
				data.myCameraOrientation = transform.GetOrientation();
				data.myToCamera = data.myCameraOrientation.GetFastInverse();
				data.myProjection = lightProjectionMatrix;
				data.myInvertedProjection = CommonUtilities::Matrix44f::Inverse(data.myProjection);
				data.myViewProjection = data.myToCamera * data.myProjection;
				data.myInvertedViewProjection = CommonUtilities::Matrix44f::Inverse(data.myViewProjection);

				lightCameraBufferData.myViewProjection[i] = data.myViewProjection;

				myCameraBuffer.SetData(&data);

				myCameraBuffer.SetBuffer(0, EShaderType_Vertex);
				myCameraBuffer.SetBuffer(0, EShaderType_Pixel);

				myCubeShadowMap.SetTextureAsActiveTarget(i);
				RenderDepthToTexture(aModelsToRender, frustum, transform.GetPosition());
			}

			myLightCameraBuffer.SetData(&lightCameraBufferData);
		}
		else
		{
			auto specialVec = CommonUtilities::Vector4f(0.f, 0.f, 0.f, -1.f);
			myLightCameraBuffer.SetData(&specialVec, sizeof(CommonUtilities::Vector4f));
		}

		myLightPassWorldVertexShader->Bind();
		myLightPassWorldVertexShader->BindLayout();
		myLightPassPixelShaders[ELightType_Point]->Bind();

		aStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Additive);
		aStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
		aLightTexture.SetAsActiveTarget();
		aGBuffer.SetAsResourceOnSlot(0);
		context->PSSetShaderResources(aGBuffer.GetNumTargets(), 1, &cubemap);
		myCubeShadowMap.SetAsResourceOnSlot(aGBuffer.GetNumTargets() + 1);
		aCameraBuffer.SetBuffer(0, EShaderType_Vertex);
		aCameraBuffer.SetBuffer(0, EShaderType_Pixel);
		myScreenBuffer.SetBuffer(1, EShaderType_Pixel);
		myLightCameraBuffer.SetBuffer(3, EShaderType_Pixel);

		const SVertexDataWrapper& vertexData = myIcosphere.myVertexData[0];
		context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
		context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		instanceData.myToWorld.myPosition = lightRenderCommand.myPosition;
		instanceData.myToWorld.myRightAxis.x = -lightRenderCommand.myRange;
		instanceData.myToWorld.myUpAxis.y = lightRenderCommand.myRange;
		instanceData.myToWorld.myForwardAxis.z = lightRenderCommand.myRange;

		myInstanceBuffer.SetData(&instanceData);
		myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

		pointLightData.position = lightRenderCommand.myPosition;
		pointLightData.range = lightRenderCommand.myRange;
		pointLightData.color = {
			lightRenderCommand.myColor.x,
			lightRenderCommand.myColor.y,
			lightRenderCommand.myColor.z,
			lightRenderCommand.myIntensity
		};

		myPointLightBuffer.SetData(&pointLightData);
		myPointLightBuffer.SetBuffer(2, EShaderType_Pixel);

		context->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);
	}
}

void CDeferredRenderer::RenderSpotLights(CConstantBuffer & aCameraBuffer, const CommonUtilities::GrowingArray<SSpotLightRenderCommand>& aLightRenderCommands, const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender, const CommonUtilities::CFrustum & aFrustum, CFullscreenTexture & aGBuffer, CFullscreenTexture & aLightTexture, CGraphicsStateManager & aStateManager, SRV aNoiseVolumeTexture)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	ID3D11ShaderResourceView* cubemap = IEngine::GetTextureManager().GetTexture(IEngine::GetGraphicsPipeline().GetCubemap());
	ID3D11ShaderResourceView* noiseVolumeTexture = IEngine::GetTextureManager().GetTexture(aNoiseVolumeTexture);

	SInstanceBufferData instanceData;
	instanceData.myToWorld = CommonUtilities::Matrix44f();

	SSpotLightBufferData spotLightData;
	for (const SSpotLightRenderCommand& lightRenderCommand : aLightRenderCommands)
	{
		CTransform transform;
		transform.SetPosition(lightRenderCommand.myPosition);
		transform.SetLookDirection(lightRenderCommand.myDirection);
		if (aFrustum.CullSphere(transform.GetPosition(), lightRenderCommand.myRange*transform.GetScale().Length()))
		{
			continue;
		}

		mySquareShadowMap.ClearTexture({ FLT_MAX, FLT_MAX, FLT_MAX, FLT_MAX });
		if (lightRenderCommand.myCastShadows)
		{
			auto lightViewMatrix = transform.GetOrientation();
			lightViewMatrix = lightViewMatrix.GetFastInverse();
			auto lightProjectionMatrix = CommonUtilities::CreatePerspectiveProjectionMatrix(lightRenderCommand.myAngle*CommonUtilities::Rad2Deg, 1.f, 0.01f, lightRenderCommand.myRange);
			auto frustum = CommonUtilities::BuildFrustumFromViewProjectionMatrix(lightViewMatrix * lightProjectionMatrix);

			SCameraBufferData data;
			data.myCameraOrientation = transform.GetOrientation();
			data.myToCamera = data.myCameraOrientation.GetFastInverse();
			data.myProjection = lightProjectionMatrix;
			data.myInvertedProjection = CommonUtilities::Matrix44f::Inverse(data.myProjection);
			data.myViewProjection = data.myToCamera * data.myProjection;
			data.myInvertedViewProjection = CommonUtilities::Matrix44f::Inverse(data.myViewProjection);

			SLightCameraBufferData lightCameraBufferData;
			lightCameraBufferData.myViewProjection[0] = data.myViewProjection;
			myLightCameraBuffer.SetData(&lightCameraBufferData);

			myCameraBuffer.SetData(&data);

			myCameraBuffer.SetBuffer(0, EShaderType_Vertex);
			myCameraBuffer.SetBuffer(0, EShaderType_Pixel);

			mySquareShadowMap.SetAsActiveTarget();
			aStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_Depth);
			aStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
			RenderDepthToTexture(aModelsToRender, frustum, transform.GetPosition());
		}
		else
		{
			auto specialVec = CommonUtilities::Vector4f(0.f, 0.f, 0.f, -1.f);
			myLightCameraBuffer.SetData(&specialVec, sizeof(CommonUtilities::Vector4f));
		}

		myLightPassWorldVertexShader->Bind();
		myLightPassWorldVertexShader->BindLayout();
		myLightPassPixelShaders[ELightType_Spot]->Bind();

		aStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Additive);
		aStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
		aLightTexture.SetAsActiveTarget();
		aGBuffer.SetAsResourceOnSlot(0);
		context->PSSetShaderResources(aGBuffer.GetNumTargets(), 1, &cubemap);
		mySquareShadowMap.SetAsResourceOnSlot(aGBuffer.GetNumTargets() + 1);
		context->PSSetShaderResources(aGBuffer.GetNumTargets() + 2, 1, &noiseVolumeTexture);
		aCameraBuffer.SetBuffer(0, EShaderType_Vertex);
		aCameraBuffer.SetBuffer(0, EShaderType_Pixel);
		myScreenBuffer.SetBuffer(1, EShaderType_Pixel);
		myLightCameraBuffer.SetBuffer(3, EShaderType_Pixel);

		const SVertexDataWrapper& vertexData = myIcosphere.myVertexData[0];
		context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
		context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		instanceData.myToWorld.myPosition = lightRenderCommand.myPosition;
		instanceData.myToWorld.myRightAxis = { -lightRenderCommand.myRange, 0.f, 0.f };
		instanceData.myToWorld.myUpAxis = { 0.f, lightRenderCommand.myRange, 0.f };
		instanceData.myToWorld.myForwardAxis = { 0.f, 0.f, lightRenderCommand.myRange };

		myInstanceBuffer.SetData(&instanceData);
		myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

		spotLightData.position = lightRenderCommand.myPosition;
		spotLightData.direction = lightRenderCommand.myDirection;
		spotLightData.color = {
			lightRenderCommand.myColor.x,
			lightRenderCommand.myColor.y,
			lightRenderCommand.myColor.z,
			lightRenderCommand.myIntensity
		};
		spotLightData.range = lightRenderCommand.myRange;
		spotLightData.angle = lightRenderCommand.myAngle;

		mySpotLightBuffer.SetData(&spotLightData);
		mySpotLightBuffer.SetBuffer(2, EShaderType_Pixel);

		context->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);
	}
}

void CDeferredRenderer::RenderDepthToTexture(const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender, const CommonUtilities::CFrustum & aFrustum, const CommonUtilities::Vector3f & aCameraPosition)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	SInstanceBufferData instanceData;

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	myDepthPixelShader->Bind();

	for (const SModelRenderCommand& command : aModelsToRender)
	{
		CModel* model = IEngine::GetModelManager().GetModel(command.myModelToRender);
		if (!model)
		{
			continue;
		}
		if (aFrustum.CullSphere(command.myTransform.myPosition, model->myModelData.myRadius * command.myScale.Length()))
		{
			continue;
		}

		CMaterial* modelMaterial = &model->myMaterial;

		if ((!command.myMaterial.ShouldRenderDepth()) || (!modelMaterial->ShouldRenderDepth()))
		{
			continue;
		}

		if (command.myMaterial.GetVertexShader() != nullptr)
		{
			command.myMaterial.GetVertexShader()->Bind();
			command.myMaterial.GetVertexShader()->BindLayout();
		}
		else if (modelMaterial->GetVertexShader() != nullptr)
		{
			modelMaterial->GetVertexShader()->Bind();
			modelMaterial->GetVertexShader()->BindLayout();
		}
		else
		{
			myDepthVertexShader->Bind();
			myDepthVertexShader->BindLayout();
		}

		float distance = (command.myTransform.myPosition - CommonUtilities::Vector3f(aCameraPosition)).Length2();

		const SVertexDataWrapper& vertexData = model->GetVertexData(model->CalculateLodLevel(distance));
		STextureDataWrapper& textureData = model->GetTextureData();

		instanceData.myToWorld = command.myTransform;

		myInstanceBuffer.SetData(&instanceData);

		context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
		context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

		if (model->myModelData.mySceneAnimator && model->myModelData.mySceneAnimator->HasSkeleton())
		{
			model->myModelData.mySceneAnimator->SetAnimIndex(command.myAnimationID);

			auto& transforms = model->myModelData.mySceneAnimator->GetTransforms(command.myAnimationTime);

			myBoneBuffer.SetData(&transforms[0], static_cast<int>(transforms.size()) * sizeof(CommonUtilities::Matrix44f));
		}
		else
		{
			auto specialVec = CommonUtilities::Vector4f(0.f, 0.f, 0.f, -1.f);
			myBoneBuffer.SetData(&specialVec, sizeof(CommonUtilities::Vector4f));
		}

		myBoneBuffer.SetBuffer(2, EShaderType_Vertex);

		ID3D11ShaderResourceView* textures[STextureDataWrapper::maxTextures];
		textures[0] = IEngine::GetTextureManager().GetTexture(textureData.myTextures[0]);
		context->PSSetShaderResources(0, 1, textures);

		context->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);
	}

	myDepthVertexShader->Unbind();
	myDepthVertexShader->UnbindLayout();
	myDepthPixelShader->Unbind();
}


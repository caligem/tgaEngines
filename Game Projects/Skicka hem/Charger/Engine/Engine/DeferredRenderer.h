#pragma once

#include <array>

#include "GrowingArray.h"
#include "Matrix.h"
#include "Vector.h"
#include "Frustum.h"

#include "ConstantBuffer.h"
#include "GraphicsStateManager.h"

#include "Model.h"

#include "VertexDataWrapper.h"
#include "Transform.h"

#include "VFXBufferDataWrapper.h"
#include "FullscreenTexture.h"

class CDirectXFramework;
class CVertexShader;
class CPixelShader;

struct SModelRenderCommand;
struct ID3D11Buffer;
struct ID3D11ShaderResourceView;

struct SDirectionalLightRenderCommand;
struct SPointLightRenderCommand;
struct SSpotLightRenderCommand;

class CDeferredRenderer
{
public:
	CDeferredRenderer();
	~CDeferredRenderer();

	bool Init();
	void RenderDataPass(
		const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender,
		CConstantBuffer& aCameraBuffer,
		const CommonUtilities::Vector4f& aCameraPosition,
		const CommonUtilities::CFrustum& aFrustum
	);

	void RenderLightPass(
		CConstantBuffer& aCameraBuffer,
		const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender,
		const CommonUtilities::GrowingArray<SDirectionalLightRenderCommand>& aDirectionalLightRenderCommands,
		const CommonUtilities::GrowingArray<SPointLightRenderCommand>& aPointLightRenderCommands,
		const CommonUtilities::GrowingArray<SSpotLightRenderCommand>& aSpotLightRenderCommands,
		CFullscreenTexture& aGBuffer,
		const CommonUtilities::CFrustum& aFrustum,
		CFullscreenTexture& aLightTexture,
		CGraphicsStateManager& aStateManager,
		SRV aNoiseVolumeTexture
	);

private:
	struct SInstanceBufferData
	{
		CommonUtilities::Matrix44f myToWorld;
	};
	struct SBoneBufferData
	{
		CommonUtilities::Matrix44f myBones[64];
	};
	struct SLightBufferData
	{
		CommonUtilities::Vector4f myDirectionalLight;
		CommonUtilities::Vector4f myDirectionalLightColor;
	};
	struct SPointLightBufferData
	{
		CommonUtilities::Vector4f position;
		CommonUtilities::Vector4f color;
		float range;
		CommonUtilities::Vector3f padding;
	};
	struct SSpotLightBufferData
	{
		CommonUtilities::Vector4f position;
		CommonUtilities::Vector4f direction;
		CommonUtilities::Vector4f color;
		float range;
		float angle;
		CommonUtilities::Vector2f padding;
	};
	struct SScreenData
	{
		CommonUtilities::Vector2f myScreenSize;
		CommonUtilities::Vector2f myTrash;
	};

	bool CreateVertexBuffer();

	void RenderAmbientLight();
	void RenderDirectionalLights(
		CConstantBuffer& aCameraBuffer,
		const CommonUtilities::GrowingArray<SDirectionalLightRenderCommand>& aLightRenderCommands,
		const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender,
		const CommonUtilities::CFrustum& aFrustum,
		CFullscreenTexture& aGBuffer,
		CFullscreenTexture& aLightTexture,
		CGraphicsStateManager& aStateManager
	);
	void RenderPointLights(
		CConstantBuffer& aCameraBuffer,
		const CommonUtilities::GrowingArray<SPointLightRenderCommand>& aLightRenderCommands,
		const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender,
		const CommonUtilities::CFrustum& aFrustum,
		CFullscreenTexture& aGBuffer,
		CFullscreenTexture& aLightTexture,
		CGraphicsStateManager& aStateManager
	);
	void RenderSpotLights(
		CConstantBuffer& aCameraBuffer,
		const CommonUtilities::GrowingArray<SSpotLightRenderCommand>& aLightRenderCommands,
		const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender,
		const CommonUtilities::CFrustum& aFrustum,
		CFullscreenTexture& aGBuffer,
		CFullscreenTexture& aLightTexture,
		CGraphicsStateManager& aStateManager,
		SRV aNoiseVolumeTexture
	);
	void RenderDepthToTexture(
		const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender,
		const CommonUtilities::CFrustum& aFrustum,
		const CommonUtilities::Vector3f& aCameraPosition
	);
	CConstantBuffer mySpotLightBuffer;
	CConstantBuffer myPointLightBuffer;
	CConstantBuffer myInstanceBuffer;
	CConstantBuffer myBoneBuffer;
	CConstantBuffer myVFXBuffer;
	CConstantBuffer myDirectionalLightBuffer;
	CConstantBuffer myScreenBuffer;

	CVertexShader* myDataPassVertexShader;
	CPixelShader* myDataPassPixelShader;
	CVertexShader* myDepthVertexShader;
	CPixelShader* myDepthPixelShader;

	enum ELightType
	{
		ELightType_Ambient,
		ELightType_Direct,
		ELightType_Point,
		ELightType_Spot,
		ELightType_Count
	};
	CVertexShader* myLightPassScreenVertexShader;
	CVertexShader* myLightPassWorldVertexShader;
	std::array<CPixelShader*, ELightType_Count> myLightPassPixelShaders;

	CConstantBuffer myCameraBuffer;
	CConstantBuffer myLightCameraBuffer;
	struct SLightCameraBufferData
	{
		CommonUtilities::Matrix44f myViewProjection[6];
	};

	SVertexDataWrapper myVertexData;

	CModel::SModelData myIcosphere;

	CFullscreenTexture mySquareShadowMap;
	CFullscreenTexture myBigSquareShadowMap;
	CFullscreenTexture myCubeShadowMap;
};


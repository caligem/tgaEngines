#include "stdafx.h"
#ifndef _RETAIL
#include "DeveloperConsole.h"
#include "imgui.h"
#include "imgui_impl_dx11.h"

#include "IEngine.h"
#include "ScriptManager.h"
#include "LuaUtility.h"

#include "IWorld.h"
#include <iostream>
#include <string>
#include <deque>

static CommonUtilities::GrowingArray<std::string, int> mySuggestions(8);
static std::deque<std::string> myExecutedCommands;

int InputCallback(ImGuiTextEditCallbackData * data);

CDeveloperConsole::CDeveloperConsole()
	: myShowConsole(false)
	, myShouldScroll(false)
{
}

CDeveloperConsole::~CDeveloperConsole()
{
}

bool CDeveloperConsole::Init()
{
	myCommands.Init(16);
	myCommandHelpTexts.Init(8);
	for (CScriptManager::SRegisteredGameFunction& it : IEngine::GetScriptManager().GetRegisteredFunctions())
	{
		myCommands.Add(it.myFunctionNameInLua);
		myCommandHelpTexts.Add(it.myHelpText);
	}
	myCommands.Add("help"); //Hard coded command since it's not a lua function. It only exists here in the console
	myCommandHelpTexts.Add("The command you just used =).");

	myInputBuffer.reserve(2048);
	myTempInputBuffer.reserve(2048);
	myLines.Init(8);
	myCommandLogActivated = false;

	myBackground = IEngine::GetTextureManager().CreateTextureFromFile("Assets/Sprites/ffgLogo.dds");
	myLuaScript = IWorld::Script().AquireLuaScriptForNonGameObjects("Developer Consol");
	return true;
}

void CDeveloperConsole::Update()
{
	if (IEngine::GetInputManager().IsKeyPressed(Input::Key_OEM_5, 100))
	{
		myShowConsole = !myShowConsole;
		IWorld::Input().SetPriority(myShowConsole ? 100 : 0);
	}

	if (!myShowConsole)
	{
		return;
	}

	if (IWorld::Input().IsKeyPressed(Input::Key_Down), 1)
	{
		myCommandLogActivated = true;
	}


	if (myCurrentCommandToExecute.length() > 0)
	{
		IEngine::GetScriptManager().RunString(myLuaScript, myCurrentCommandToExecute.c_str());
		myCurrentCommandToExecute = "";
	}
}

void CDeveloperConsole::Render()
{
	if (myShowConsole)
	{
		//ImGui::ShowStyleEditor();
		ShowConsole();
	}
}

void CDeveloperConsole::ShowConsole()
{
	ImVec2 pos, size;

	SetPersonalStyle();
	RenderConsoleWindow(pos, size);
	RenderSuggestionsPopup(pos, size);
	RenderCommandLogPopup(pos, size);
	ImGui::PopStyleColor(13);
}

void CDeveloperConsole::RenderConsoleWindow(ImVec2& aPos, ImVec2& aSize)
{
	ImGui::GetIO().MouseDrawCursor = true;
	ImGui::Begin("Console", &myShowConsole, ImVec2(520, 600), -1.f, ImGuiWindowFlags_NoCollapse);

	ImGui::BeginChild("ScrollingRegion", ImVec2(0, -ImGui::GetItemsLineHeightWithSpacing()), false, ImGuiWindowFlags_HorizontalScrollbar);

	ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 1));
	for (std::string& line : myLines)
	{
		ImVec4 col = ImVec4(1.0f, 1.0f, 1.0f, 1.0f);
		ImGui::PushStyleColor(ImGuiCol_Text, col);
		ImGui::TextUnformatted(line.c_str());
		ImGui::PopStyleColor();
	}

	if (myShouldScroll)
	{
		ImGui::SetScrollHere();
		myShouldScroll = false;
	}

	ImGui::PopStyleVar();
	ImGui::EndChild();
	ImGui::Separator();

	ImGuiInputTextFlags flags =
		ImGuiInputTextFlags_EnterReturnsTrue |
		ImGuiInputTextFlags_CallbackCharFilter |
		ImGuiInputTextFlags_CallbackCompletion |
		ImGuiInputTextFlags_CallbackAlways |
		ImGuiInputTextFlags_CallbackHistory;
	// Command-line

	myTempInputBuffer = myInputBuffer;
	bool pressedEnter = ImGui::InputText(
		"Input",
		&myTempInputBuffer[0],
		myTempInputBuffer.capacity(),
		flags,
		&InputCallback,
		&myInputState
	);

	myInputBuffer = myTempInputBuffer.c_str();

	if (pressedEnter)
	{
		if (myInputBuffer.size() > 0)
		{
			ExecuteCommand();
			myCommandLogActivated = false;
		}
		else
		{

		}

		myInputBuffer = "";
	}

	if (myInputState.myClickedIndex != -1)
	{
		ImGui::SetKeyboardFocusHere(-1);
	}

	// Demonstrate keeping auto focus on the input box
	if (ImGui::IsItemHovered() || (ImGui::IsRootWindowOrAnyChildFocused() && !ImGui::IsAnyItemActive() && !ImGui::IsMouseClicked(0)))
		ImGui::SetKeyboardFocusHere(-1); // Auto focus previous widget

	ImVec2 consolePos = ImGui::GetWindowPos();
	ImVec2 consoleSize = ImGui::GetWindowSize();

	float bgSize = CommonUtilities::Min(consoleSize.x, consoleSize.y);
	ImVec2 bgPos = { consoleSize.x / 2.f, consoleSize.y / 2.f };
	ImGui::SetNextWindowPos(ImVec2(consolePos.x + bgPos.x - bgSize / 2.f, consolePos.y + bgPos.y - bgSize / 2.f));
	ImGui::Begin("logo", (bool*)1, consoleSize, false, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings | ImGuiWindowFlags_NoBringToFrontOnFocus);
	ImGui::Image(IEngine::GetTextureManager().GetTexture(myBackground), ImVec2(bgSize, bgSize), ImVec2(0, 0), ImVec2(1, 1), ImVec4(1.f, 1.f, 1.f, 0.3f));
	ImGui::End();

	aPos = ImGui::GetItemRectMin();
	aPos.y += ImGui::GetItemRectSize().y;
	aSize = ImVec2(ImGui::GetItemRectSize().x - 60, ImGui::GetItemsLineHeightWithSpacing() * CommonUtilities::Min(3, myInputState.myEntryCount));

	ImGui::End();
}

void CDeveloperConsole::RenderSuggestionsPopup(ImVec2& aPos, ImVec2& aSize)
{
	if (myInputBuffer.size() <= 0)
	{
		mySuggestions.RemoveAll();
		return;
	}

	GetSuggestions(myInputBuffer);
	myInputState.myEntryCount = mySuggestions.Size();
	if (myInputState.myEntryCount <= 0)
	{
		return;
	}

	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0);

	ImGuiWindowFlags flags =
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_HorizontalScrollbar |
		ImGuiWindowFlags_NoSavedSettings |
		ImGuiWindowFlags_ShowBorders;


	ImGui::SetNextWindowPos(aPos);
	ImGui::SetNextWindowSize(aSize);
	ImGui::Begin("input_popup", nullptr, flags);
	ImGui::PushAllowKeyboardFocus(false);

	for (int i = 0; i < mySuggestions.Size(); ++i)
	{
		bool isIndexActive = myInputState.myActiveIndex == i;

		if (isIndexActive)
		{
			ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(1, 0, 0, 1));
		}

		ImGui::PushID(i);
		if (ImGui::Selectable(mySuggestions[i].c_str(), isIndexActive))
		{
			myInputState.myClickedIndex = i;
		}
		ImGui::PopID();

		if (isIndexActive)
		{
			if (myInputState.mySelectionChanged)
			{
				ImGui::SetScrollHere();
				myInputState.mySelectionChanged = false;
			}

			ImGui::PopStyleColor(1);
		}
	}

	ImGui::PopAllowKeyboardFocus();
	ImGui::End();
	ImGui::PopStyleVar(1);
}

void CDeveloperConsole::RenderCommandLogPopup(ImVec2 & aPos, ImVec2 & aSize)
{
	if (!myCommandLogActivated)
	{
		return;
	}
	if (mySuggestions.Size() > 0)
	{
		myCommandLogActivated = false;
		return;
	}

	myInputState.myEntryCount = static_cast<int>(myExecutedCommands.size());
	if (myInputState.myEntryCount <= 0)
	{
		return;
	}

	ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0);

	ImGuiWindowFlags flags =
		ImGuiWindowFlags_NoTitleBar |
		ImGuiWindowFlags_NoResize |
		ImGuiWindowFlags_NoMove |
		ImGuiWindowFlags_HorizontalScrollbar |
		ImGuiWindowFlags_NoSavedSettings |
		ImGuiWindowFlags_ShowBorders;


	ImGui::SetNextWindowPos(aPos);
	ImGui::SetNextWindowSize(aSize);
	ImGui::Begin("command_log_popup", nullptr, flags);
	ImGui::PushAllowKeyboardFocus(false);

	//for (int i = myExecutedCommands.Size() -1; i > -1; --i)
	for (int i = 0; i < myExecutedCommands.size(); ++i)
	{
		bool isIndexActive = myInputState.myActiveIndex == i;

		if (isIndexActive)
		{
			ImGui::PushStyleColor(ImGuiCol_Border, ImVec4(1, 0, 0, 1));
		}

		ImGui::PushID(i);
		if (ImGui::Selectable(myExecutedCommands[i].c_str(), isIndexActive))
		{
			myInputState.myClickedIndex = i;
		}
		ImGui::PopID();

		if (isIndexActive)
		{
			if (myInputState.mySelectionChanged)
			{
				ImGui::SetScrollHere();
				myInputState.mySelectionChanged = false;
			}

			ImGui::PopStyleColor(1);
		}
	}

	ImGui::PopAllowKeyboardFocus();
	ImGui::End();
	ImGui::PopStyleVar(1);
}

void SetInputFromActiveIndex(ImGuiTextEditCallbackData* data, int entryIndex)
{
	CommonUtilities::GrowingArray<std::string, int> arrayForInput(static_cast<int>(myExecutedCommands.size()));
	if (entryIndex < mySuggestions.Size() && mySuggestions.Size() != 0)
	{
		arrayForInput = mySuggestions;
	}
	else
	{
		for (std::string& command : myExecutedCommands)
		{
			arrayForInput.Add(command);
		}
	}

	memcpy(data->Buf, arrayForInput[entryIndex].c_str(), arrayForInput[entryIndex].length() + 1);
	data->BufTextLen = (int)arrayForInput[entryIndex].length();
	data->BufDirty = true;
	data->CursorPos = data->BufTextLen;
}
int InputCallback(ImGuiTextEditCallbackData * data)
{
	CDeveloperConsole::InputState& state = *reinterpret_cast<CDeveloperConsole::InputState*>(data->UserData);

	switch (data->EventFlag)
	{
	case ImGuiInputTextFlags_CallbackCompletion:
		if (state.myActiveIndex != -1)
		{
			SetInputFromActiveIndex(data, state.myActiveIndex);
			state.myClickedIndex = -1;
			state.myActiveIndex = -1;
		}
		break;
	case ImGuiInputTextFlags_CallbackAlways:
		if (state.myClickedIndex != -1)
		{
			SetInputFromActiveIndex(data, state.myClickedIndex);
			state.myClickedIndex = -1;
			state.myActiveIndex = -1;
		}
		break;
	case ImGuiInputTextFlags_CallbackHistory:
		if (data->EventKey == ImGuiKey_UpArrow && state.myActiveIndex > 0)
		{
			state.myActiveIndex--;
			state.mySelectionChanged = true;
		}
		else if (data->EventKey == ImGuiKey_DownArrow && state.myActiveIndex < (state.myEntryCount - 1))
		{
			state.myActiveIndex++;
			state.mySelectionChanged = true;
		}
		break;
	case ImGuiInputTextFlags_CallbackCharFilter:
		break;
	}

	return 0;
}

void CDeveloperConsole::GetSuggestions(const std::string & aInput)
{
	mySuggestions.RemoveAll();

	std::string input = aInput;
	std::transform(input.begin(), input.end(), input.begin(), ::tolower);

	for (auto& key : myCommands)
	{
		std::string lowerKey = key;
		std::transform(lowerKey.begin(), lowerKey.end(), lowerKey.begin(), ::tolower);

		if (lowerKey.find(input) == 0)
		{
			mySuggestions.Add(key);
		}
	}

	std::sort(mySuggestions.begin(), mySuggestions.end(), [](auto& a, auto& b)
	{
		return a < b;
	});
}

void CDeveloperConsole::SetPersonalStyle()
{
	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.f, 0.f, 0.f, 0.78f));
	ImGui::PushStyleColor(ImGuiCol_TitleBg, ImVec4(0.00f, 0.00f, 0.00f, 0.83f));
	ImGui::PushStyleColor(ImGuiCol_TitleBgActive, ImVec4(0.00f, 0.00f, 0.00f, 0.87f));
	ImGui::PushStyleColor(ImGuiCol_TitleBgCollapsed, ImVec4(0.00f, 0.00f, 0.00f, 0.20f));

	ImGui::PushStyleColor(ImGuiCol_ScrollbarBg, ImVec4(0.05f, 0.05f, 0.05f, 0.60f));
	ImGui::PushStyleColor(ImGuiCol_ScrollbarGrab, ImVec4(0.26f, 0.26f, 0.26f, 0.30f));
	ImGui::PushStyleColor(ImGuiCol_ScrollbarGrabHovered, ImVec4(0.86f, 0.86f, 0.87f, 0.40f));
	ImGui::PushStyleColor(ImGuiCol_ScrollbarGrabActive, ImVec4(0.55f, 0.55f, 0.55f, 0.40f));

	ImGui::PushStyleColor(ImGuiCol_CloseButton, ImVec4(0.26f, 0.26f, 0.26f, 0.30f));
	ImGui::PushStyleColor(ImGuiCol_CloseButtonHovered, ImVec4(0.75f, 0.75f, 0.75f, 0.60f));

	ImGui::PushStyleColor(ImGuiCol_Header, ImVec4(0.68f, 0.00f, 0.00f, 0.45f));
	ImGui::PushStyleColor(ImGuiCol_HeaderHovered, ImVec4(0.76f, 0.00f, 0.00f, 0.80f));
	ImGui::PushStyleColor(ImGuiCol_HeaderActive, ImVec4(0.67f, 0.18f, 0.18f, 0.80f));
}

void CDeveloperConsole::AddToLog(const std::string& aInput)
{
	myLines.Add(aInput);
	myShouldScroll = true;
}

void CDeveloperConsole::AddToCommandLogPopup(const std::string & aInput)
{
	myExecutedCommands.push_front(aInput);
}

void CDeveloperConsole::ExecuteCommand()
{
	AddToLog(myInputBuffer);

	myCurrentCommandToExecute = myInputBuffer;

	if (myInputBuffer == "help")
	{
		ExecuteHelpCommand();
	}

	myExecutedCommands.push_front(myInputBuffer);
}

void CDeveloperConsole::ExecuteHelpCommand()
{
	for (int i = 0; i < myCommands.Size(); ++i)
	{
		AddToLog("   " + myCommands[i] + "():  " + myCommandHelpTexts[i]);
	}
}

#endif


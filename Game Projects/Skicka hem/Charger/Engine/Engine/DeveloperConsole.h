#pragma once

#include "GrowingArray.h"

struct ImVec2;

#ifndef _RETAIL

class CDeveloperConsole
{
public:
	CDeveloperConsole();
	~CDeveloperConsole();

	bool Init();
	void Update();

	void Render();

	struct InputState
	{
		int myActiveIndex = -1;
		int myClickedIndex = -1;
		bool mySelectionChanged = false;
		int myEntryCount = 0;
	};

private:
	void ShowConsole();
	void RenderConsoleWindow(ImVec2& aPos, ImVec2& aSize);
	void RenderSuggestionsPopup(ImVec2& aPos, ImVec2& aSize);
	void RenderCommandLogPopup(ImVec2& aPos, ImVec2& aSize);

	void ExecuteCommand();
	void ExecuteHelpCommand();
	void AddToLog(const std::string& aInput);
	void AddToCommandLogPopup(const std::string& aInput);
	void GetSuggestions(const std::string& aInput);

	void SetPersonalStyle();

	bool myShowConsole;

	CommonUtilities::GrowingArray<std::string, int> myCommands;
	CommonUtilities::GrowingArray<std::string, int> myCommandHelpTexts;
	CommonUtilities::GrowingArray<std::string, int> myLines;

	std::string myCurrentCommandToExecute = "";

	std::string myTempInputBuffer;
	std::string myInputBuffer;
	bool myShouldScroll;
	bool myCommandLogActivated;

	InputState myInputState;

	SRV myBackground;

	ID_T(CLuaScript) myLuaScript;
};

#else

#include <string>

class CDeveloperConsole
{
public:
	CDeveloperConsole(){}
	~CDeveloperConsole(){}

	bool Init() { return true; }
	void Update(){}

	void Render(){}

private:
	void ShowConsole();
	void RenderConsoleWindow(ImVec2&, ImVec2&) {}
	void RenderSuggestionsPopup(ImVec2&, ImVec2&){}
	void RenderCommandLogPopup(ImVec2&, ImVec2&){}

	void ExecuteCommand(){}
	void ExecuteHelpCommand(){}
	void AddToLog(const std::string&){}
	void AddToCommandLogPopup(const std::string&){}
	void GetSuggestions(const std::string&){}

	void SetPersonalStyle(){}
};

#endif

#include "stdafx.h"
#include "Engine.h"

#include "IEngine.h"

#include "ComponentSystem.h"
#include "GameObject.h"

#include <DirectXMath.h>

#include "Random.h"
#include "CommandLineManager.h"
#include "SystemStats.h"

#include <iomanip>

#include "imgui.h"
#include "imgui_impl_dx11.h"
#include "IClientNetwork.h"

#define MULTITHREADED

CEngine::CEngine()
	: myXBoxInput(0)
	, myXBoxInput2(1)
	, myNetworkSystem(myTimer)
{
}

CEngine::~CEngine()
{
	myWorkerPool.Destroy();
#ifndef _RETAIL
	ImGui_ImplDX11_Shutdown();
#endif
}

bool CEngine::Init(const SCreateParameters& aCreateParameters)
{
	START_TIMER(startupTime);
	CommonUtilities::InitRand();

	ENGINE_LOG(CONCOL_DEFAULT, "===============================");
	ENGINE_LOG(CONCOL_DEFAULT, "=== Starting The Engine... ===");
	ENGINE_LOG(CONCOL_DEFAULT, "===============================");
	myCreateParameters = aCreateParameters;

	IEngine::ourEngine = this;

	if (!myWindowHandler.Init(&aCreateParameters))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise WindowHandler!");
		return false;
	}

	if (!myFramework.Init(myWindowHandler, &aCreateParameters))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise DirectXFramework!");
		return false;
	}

	myTextureManager.Init();

	//Managers Init
	if (!mySceneManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise SceneManager!");
		return false;
	}
	if (!myModelManager.Init(myFramework))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise ModelManager.");
		return false;
	}
	if (!myCameraManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise CameraManager.");
		return false;
	}
	if (!myParticleManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise ParticleManager.");
		return false;
	}
	if (!mySpriteManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise SpriteManager.");
		return false;
	}
	if (!myTextManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise SpriteManager.");
		return false;
	}
	if (!myScriptManager.Init(aCreateParameters.myScriptRegisterCallback))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise ScriptManager");
		return false;
	}
	if (!myComponentSystem.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise Component System.");
		return false;
	}
	if (!myPathFinder.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise PathFinder.");
		return false;
	}

	ManagerInjection();

	// Graphics
	if (!myGraphicsPipeline.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise GraphicsPipeline.");
		return false;
	}

	unsigned int maxThreads = 1;
#ifndef MULTITHREADED
	myWorkerPool.Init(1);
#else
	maxThreads = std::thread::hardware_concurrency();
	if (maxThreads == 0)
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: No cores found!");
		return false;
	}
	else
	{
		ENGINE_LOG(CONCOL_VALID, "Created WorkerPool using: %d threads!", maxThreads);
	}
	myWorkerPool.Init(maxThreads);
#endif

	if (!myReportManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise ReportManager.");
		return false;
	};


	//DevTools
#ifndef _RETAIL
	if (!ImGui_ImplDX11_Init(IEngine::GetWindowHandler().GetWindowHandle(), IEngine::GetDevice(), IEngine::GetContext()))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to initialise ImGui in ReportManager");
		return false;
	}
	if (!myDeveloperConsole.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise Developer Console.");
		return false;
	}
#endif
	
	if (!myNetworkSystem.Init(static_cast<char>(maxThreads+1)))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise NetworkSystem.");
		return false;
	}

	myShouldTakeScreenshot = false;
	myScreenshotPath = L"";

	ENGINE_LOG(CONCOL_VALID, "Engine startup time: %f ms", GET_TIME(startupTime));
	return true;
}

void CEngine::Shutdown()
{
	myIsRunning = false;
	myWindowHandler.CloseWindow();
}

void CEngine::StartEngine()
{
	if (myCreateParameters.myInitFunctionToCall)
	{
		myCreateParameters.myInitFunctionToCall();
	}

	myUpdateFunctionToCall = myCreateParameters.myUpdateFunctionToCall;

	myIsRunning = true;

	if (myCreateParameters.myUpdateFunctionToCall)
	{
		Run();
	}
}

void CEngine::Run()
{
	MSG windowsMessage = { 0 };

	while (myIsRunning)
	{
		START_TIMER(frameMS);
		while (PeekMessage(&windowsMessage, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&windowsMessage);
			DispatchMessage(&windowsMessage);

			if (windowsMessage.message == WM_QUIT)
			{
				myIsRunning = false;
			}
		}

		if (myShouldTakeScreenshot)
		{
			myFramework.SaveScreenShot(myScreenshotPath);
			myShouldTakeScreenshot = false;
		}

		myWindowHandler.Update();

		myTimer.Update();
		myNetworkSystem.Update();
		myWorkerPool.DoWork(std::bind(&CEngine::MainJob, this));
		myWorkerPool.DoWork(std::bind(&CEngine::RenderJob, this));

		myWorkerPool.Wait();

		myWorkerPool.DoWork(std::bind(&CEngine::SyncJob, this));

		myWorkerPool.Wait();

		myInputManager.Update();
		myXBoxInput.Update();
		myXBoxInput2.Update();

		myTotalFrameMS = GET_TIME(frameMS);
	}

	ENGINE_LOG(CONCOL_DEFAULT, "===============================");
	ENGINE_LOG(CONCOL_DEFAULT, "=== Engine Shutting Down... ===");
	ENGINE_LOG(CONCOL_DEFAULT, "===============================");
}

void CEngine::UpdateDevTools()
{
	myReportManager.Update();
	myDeveloperConsole.Update();
#ifndef _RETAIL
	DebugDrawSystemStats();
#endif
}

void CEngine::MainJob()
{
	myComponentSystem.RunDestroys();
	myComponentSystem.UpdateComponents();
	UpdateDevTools();

	START_TIMER(gameLogicMS);
	myUpdateFunctionToCall();
	myGameLogicMS = GET_TIME(gameLogicMS);

	myGraphicsPipeline.SetRenderBuffers();
}

void CEngine::RenderJob()
{
	START_TIMER(renderMS);
	myGraphicsPipeline.BeginFrame();

	myGraphicsPipeline.Render();

#ifndef _RETAIL
	ImGui_ImplDX11_NewFrame(IEngine::GetCanvasSize().x, IEngine::GetCanvasSize().y);
	myReportManager.Render();
	myDeveloperConsole.Render();
	ImGui::Render();
#endif

	myFramework.Present();
	myRenderMS = GET_TIME(renderMS);
}

void CEngine::SyncJob()
{
	myGraphicsPipeline.SwapBuffers();
	myFileWatcher.FlushChanges();

	myGraphicsPipeline.SetCameraBuffer();

	mySceneManager.DestroyScenesInQueue();
}

void CEngine::DebugDrawSystemStats()
{
	static CommonUtilities::GrowingArray<float, int> fpsStack(60);
	if (fpsStack.Size() >= 60) fpsStack.Shift();

	fpsStack.Add(myTotalFrameMS);
	float fps = 0.f;
	for (float t : fpsStack) fps += t;
	fps /= static_cast<float>(fpsStack.Size());
	std::string fpsStr = ("fps: " + std::to_string(floorf(1000.f / fps)));
	fpsStr.resize(fpsStr.length() - 4);
	myGraphicsPipeline.GetDebugRenderer().DrawDebugText(fpsStr);

	std::string cpuUsage = "cpu usage: " + std::to_string(CSystemStats::CPUUsage());
	cpuUsage.resize(cpuUsage.length() - 4);
	cpuUsage.append("%");

	myGraphicsPipeline.GetDebugRenderer().DrawDebugText(cpuUsage);
	myGraphicsPipeline.GetDebugRenderer().DrawDebugText(("mem usage: " + std::to_string(CSystemStats::MemUsage()) + "MB"));

	std::string frameMS = ("total frame ms: " + std::to_string(roundf(myTotalFrameMS)));
	frameMS.resize(frameMS.length() - 4);
	myGraphicsPipeline.GetDebugRenderer().DrawDebugText(frameMS);

	if (CommonUtilities::CCommandLineManager::HasArgument("-activateLog", "engine"))
	{
		myEngineMS = myTotalFrameMS - (myRenderMS + myGameLogicMS);
		std::string engineMS = ("engine ms : " + std::to_string(roundf(myEngineMS)));
		engineMS.resize(engineMS.length() - 4);
		myGraphicsPipeline.GetDebugRenderer().DrawDebugText(engineMS);
	}

	if (CommonUtilities::CCommandLineManager::HasArgument("-activateLog", "gameplay"))
	{
		std::string gameLogicMS = ("gameLogic ms: " + std::to_string(roundf(myGameLogicMS)));
		gameLogicMS.resize(gameLogicMS.length() - 4);
		myGraphicsPipeline.GetDebugRenderer().DrawDebugText(gameLogicMS);
	}

	std::string renderMS = ("render ms: " + std::to_string(roundf(myRenderMS)));
	renderMS.resize(renderMS.length() - 4);
	myGraphicsPipeline.GetDebugRenderer().DrawDebugText(renderMS);

	if (myNetworkSystem.IsConnected())
	{
		std::string ping = "ping: " + std::to_string(myNetworkSystem.GetPing()) + " ms";
		myGraphicsPipeline.GetDebugRenderer().DrawDebugText(ping);
	}

	//spacing to seperate system stats
	myGraphicsPipeline.GetDebugRenderer().DrawDebugText("");

	myGraphicsPipeline.DrawDebugTexts();
}

void CEngine::SaveScreenShot(const std::wstring & aSavePath)
{
	myShouldTakeScreenshot = true;
	myScreenshotPath = aSavePath;
}

void CEngine::ManagerInjection()
{
	CModelComponent::ourModelManager = &myModelManager;
	CCameraComponent::ourCameraManager = &myCameraManager;
	CParticleSystemComponent::ourParticleManager = &myParticleManager;
	CStreakComponent::ourParticleManager = &myParticleManager;
	CSpriteComponent::ourSpriteManager = &mySpriteManager;
	CTextComponent::ourTextManager = &myTextManager;
	CGameObject::ourComponentSystem = &myComponentSystem;
	CGameObjectData::ourComponentSystem = &myComponentSystem;
	CTransform::ourComponentSystem = &myComponentSystem;
}


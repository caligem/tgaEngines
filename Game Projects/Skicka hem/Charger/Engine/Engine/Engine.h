#pragma once

#include "DirectXFramework.h"

#include "GraphicsPipeline.h"

#include "NetworkSystem.h"

#include "ComponentSystem.h"
#include "ModelManager.h"
#include "SceneManager.h"
#include "CameraManager.h"
#include "ShaderManager.h"
#include "ParticleManager.h"
#include "SpriteManager.h"
#include "TextManager.h"
#include "TextureManager.h"
#include "ScriptManager.h"
#include "PathFinder.h"

#include "DeveloperConsole.h"

#include "FileWatcher.h"
#include "WorkerPool.h"

#include "Timer.h"
#include "InputManager.h"
#include "XBOXController.h"

#include "ReportManager.h"

#include <GrowingArray.h>
#include <thread>
#include <atomic>

class IEngine;

using callback_function = std::function<void()>;
using callback_function_update = std::function<void()>;
using callback_function_wndProc = std::function<LRESULT(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam)>;

struct SCreateParameters
{
	callback_function myInitFunctionToCall;
	callback_function_update myUpdateFunctionToCall;
	callback_function_wndProc myWndProcCallback;
	callback_function myScriptRegisterCallback;

	unsigned short myWindowWidth = 800;
	unsigned short myWindowHeight = 600;
	unsigned short myCanvasWidth = 800;
	unsigned short myCanvasHeight = 600;

	bool myEnableVSync = true;
	bool myIsFullscreen = false;
	bool myIsBorderless = false;

	std::wstring myGameName = L"NO NAME GAME!";

	HWND myHWND = NULL;
};

class CEngine
{
public:
	CEngine();
	~CEngine();

	virtual bool Init(const SCreateParameters& aCreateParameters);
	void Shutdown();
	void StartEngine();

	const bool IsRunning() const { return myIsRunning; }

	void SaveScreenShot(const std::wstring& aSavePath);

protected:
	virtual void MainJob();
	virtual void RenderJob();
	virtual void SyncJob();

	void DebugDrawSystemStats();

	CWindowHandler myWindowHandler;

	CParticleManager myParticleManager;
	CGraphicsPipeline myGraphicsPipeline;

protected:
	friend IEngine;
	void Run();
	void UpdateDevTools();

	void ManagerInjection();

	bool myIsRunning = false;

	bool myShouldTakeScreenshot;
	std::wstring myScreenshotPath;

	CDirectXFramework myFramework;

	CNetworkSystem myNetworkSystem;

	CComponentSystem myComponentSystem;
	CModelManager myModelManager;
	CCameraManager myCameraManager;
	CSceneManager mySceneManager;
	CSpriteManager mySpriteManager;
	CTextManager myTextManager;
	CTextureManager myTextureManager;
	CScriptManager myScriptManager;
	CPathFinder myPathFinder;
	CDeveloperConsole myDeveloperConsole;

	SCreateParameters myCreateParameters;

	callback_function myInitFunctionToCall;
	callback_function_update myUpdateFunctionToCall;

	CFileWatcher myFileWatcher;
	CShaderManager myShaderManager;

	CWorkerPool myWorkerPool;

	CReportManager myReportManager;

	CommonUtilities::Timer myTimer;
	Input::CInputManager myInputManager;
	CommonUtilities::XBOXController myXBoxInput;
	CommonUtilities::XBOXController myXBoxInput2;

	float myEngineMS;
	float myGameLogicMS;
	float myRenderMS;
	float myTotalFrameMS;
};


#pragma once

#include <functional>
#include <vector>
#include <thread>
#include <mutex>

class CEngine;

typedef std::function<void(const std::wstring)> callback_function_file;

#ifndef _RETAIL

class CFileWatcher
{
public:
	CFileWatcher();
	~CFileWatcher();

	int WatchFile(const std::wstring& aFile, callback_function_file aCallback);
	void StopWatchingFile(const std::wstring& aFile, int aIndex);

private:
	friend CEngine;

	void FlushChanges();

	void ThreadLoop();
	void CompareFileTimes(const std::wstring& aFile);
	__int64 GetFileWriteTime(const std::wstring& aFile);
	bool IsValid(const std::wstring& aFile);
	std::thread myThread;
	std::vector<std::wstring> myChangedFiles;
	std::map<std::wstring, std::vector<callback_function_file>> myCallbacks;
	std::map<std::wstring, __int64> myFileTimes;

	std::mutex myMutex;
	volatile bool myShouldEndThread;
	volatile bool myThreadIsDone;
};

#else

class CFileWatcher
{
public:
	CFileWatcher(){}
	~CFileWatcher(){}

	int WatchFile(const std::wstring&, callback_function_file) { return 0; }
	void StopWatchingFile(const std::wstring&, int){}

private:
	friend CEngine;

	void FlushChanges(){}

	void ThreadLoop(){}
	void CompareFileTimes(const std::wstring& ){}
	__int64 GetFileWriteTime(const std::wstring& ){}
	bool IsValid(const std::wstring& ){}
};

#endif
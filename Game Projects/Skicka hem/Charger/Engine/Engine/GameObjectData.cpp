#include "stdafx.h"
#include "GameObjectData.h"

CComponentSystem* CGameObjectData::ourComponentSystem = nullptr;

CGameObjectData::CGameObjectData()
	: myTransform(this)
	, myHasBeenInitialized(false)
{
}


CGameObjectData::~CGameObjectData()
{
}

void CGameObjectData::Init(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID, int aAccessID)
{
	mySceneID = aSceneID;
	myID = aGameObjectDataID;
	myAccessID = aAccessID;

	myComponents.Init(8);
	myHasBeenInitialized = true;
	myIsActive = true;
	myTransform = CTransform(this);
}

void CGameObjectData::AddSComponent(int aComponentID, _UUID_T aUUID)
{
#ifndef _RETAIL
	if (!myHasBeenInitialized)
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying To Add Component to GameObjectData(ID: %d, Scene: %d) but it's uninitialized, forget to run Init on GameObject?", myID.val, mySceneID.val);
		return;
	}
#endif

	myComponents.Add(SComponentData(aComponentID, aUUID));
}

bool CGameObjectData::RemoveSComponent(_UUID_T aComponentType, int aComponentID)
{
	for (unsigned short i = myComponents.Size(); i > 0; --i)
	{
		if (aComponentType != myComponents[i - 1].componentType)
		{
			//TODO: Multiple Component Functionality
			aComponentID;
			continue;
		}
		myComponents.RemoveCyclicAtIndex(i - 1);
		return true;
	}

	ENGINE_LOG(CONCOL_ERROR, "Trying to remove component from GameObjectData but component doesnt exist.");
	return false;
}

const bool CGameObjectData::IsActive() const
{
	if (myTransform.myParent != nullptr)
	{
		return myIsActive && myTransform.myParent->myGameObjectData->IsActive();
	}

	return myIsActive;
}

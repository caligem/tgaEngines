#pragma once
#include "GrowingArray.h"
#include "UniqueIdentifier.h" //fix later
#include "Transform.h"

class CComponent;
class CScene;
class CComponentSystem;
class CTransform;

class CGameObjectData
{
public:
	CGameObjectData();
	~CGameObjectData();

	struct SComponentData
	{
		SComponentData()
		{
			componentID = -1;
			componentType = -1;
		}

		SComponentData(int aComponentID, _UUID_T aComponentType)
		{
			componentID = aComponentID;
			componentType = aComponentType;
		}

		int componentID;
		_UUID_T componentType;
	};

	void Init(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID, int aAccessID);

	void AddSComponent(int aComponentID, _UUID_T aUUID);

	bool RemoveSComponent(_UUID_T aComponentType, int aComponentID);

	inline CTransform& GetTransform() { return myTransform; }
	const int GetAccessID() { return myAccessID; }
	void SetActive(bool aIsActive) { myIsActive = aIsActive; }
	const bool IsActive() const;

private:
	friend CComponentSystem;
	friend CTransform;
	friend class CEngine;

	static CComponentSystem* ourComponentSystem;

	ID_T(CScene) mySceneID;
	ID_T(CGameObjectData) myID;
	int myAccessID = -1;

	template<typename T>
	int GetComponentID();
	template<typename T>
	void FillComponentIDBuffer(CommonUtilities::GrowingArray<int>& aIDBuffer);

	bool myIsActive;
	bool myHasBeenInitialized;
	CommonUtilities::GrowingArray<SComponentData> myComponents;
	CTransform myTransform;
};

template<typename T>
inline int CGameObjectData::GetComponentID()
{
	for (unsigned short i = 0; i < myComponents.Size(); ++i)
	{
		if (_UUID(T) != myComponents[i].componentType)
		{
			continue;
		}

		return myComponents[i].componentID;
	}
	return -1;
}

template<typename T>
inline void CGameObjectData::FillComponentIDBuffer(CommonUtilities::GrowingArray<int>& aIDBuffer)
{
	for (unsigned short i = 0; i < myComponents.Size(); ++i)
	{
		if (_UUID(T) != myComponents[i].componentType)
		{
			continue;
		}

		aIDBuffer.Add(myComponents[i].componentID);
	}
}

#pragma once

#include "ForwardRenderer.h"
#include "DeferredRenderer.h"
#include "FullscreenRenderer.h"
#include "FullscreenTexture.h"
#include "DebugRenderer.h"
#include "C2DRenderer.h"
#include "ParticleRenderer.h"
#include "SkyboxRenderer.h"
#include "VolumetricRenderer.h"

#include "DoubleBuffer.h"
#include "RenderCommand.h"
#include "GrowingArray.h"
#include "ConstantBuffer.h"
#include "Frustum.h"

#include "GraphicsStateManager.h"

#include "DoubleBuffer.h"
#include "RenderCommand.h"
#include "OrbitCamera.h"

#include "TextureManager.h"

#include "CameraDataWrapper.h"

class CModelComponent;
class CLightComponent;
class CParticleEditor;
class CEngine;
class CEditorEngine;

class CGraphicsPipeline
{
public:
	CGraphicsPipeline();
	~CGraphicsPipeline();

	bool Init();

	void BeginFrame();
	void Render();

	void SwapBuffers();
	void SetCameraBuffer();
	void SetRenderBuffers();

	CDebugRenderer& GetDebugRenderer() { return myDebugRenderer; }
	CForwardRenderer& GetForwardRenderer() { return myForwardRenderer; }
	CSkyboxRenderer& GetSkyboxRenderer() { return mySkyboxRenderer; }
	CDeferredRenderer& GetDeferredRenderer() { return myDeferredRenderer; }
	C2DRenderer& Get2DRenderer() { return my2DRenderer; }

	const SCameraBufferData& GetSavedCameraBuffer() const { return mySavedCameraBuffer; }
	const CommonUtilities::CFrustum& GetSavedFrustum() const { return myFrustum; }

	const CommonUtilities::Vector4f& GetFadeColor() const { return myFadeColor; }
	void SetFadeColor(const CommonUtilities::Vector4f& aColor) { myFadeColor = aColor; }
	const CommonUtilities::Vector3f& GetSavedPlayerPosition() const { return mySavedPlayerPosition; }
	void SetPosition(const CommonUtilities::Vector3f& aPosition) { myPlayerPosition = aPosition; }

	SRV GetSkybox() { return mySkybox; }
	SRV GetCubemap() { return myCubemap; }

private:
	friend CParticleEditor;
	friend CEngine;
	friend CEditorEngine;

	CDoubleBuffer<SModelRenderCommand> myModelRenderCommands;
	CDoubleBuffer<SParticleSystemRenderCommand> myParticleSystemRenderCommands;
	CDoubleBuffer<SStreakRenderCommand> myStreakRenderCommands;
	CDoubleBuffer<SVolumetricFogRenderCommand> myVolumetricFogRenderCommands;
	CDoubleBuffer<SDirectionalLightRenderCommand> myDirectionalLightRenderCommands;
	CDoubleBuffer<SPointLightRenderCommand> myPointLightRenderCommands;
	CDoubleBuffer<SSpotLightRenderCommand> mySpotLightRenderCommands;
	CDoubleBuffer<SCanvasRenderCommand> myCanvasRenderCommands;

	CommonUtilities::GrowingArray<int> myModelsToRender;
	CommonUtilities::GrowingArray<int> myParticleSystemsToRender;
	CommonUtilities::GrowingArray<int> myStreaksToRender;
	CommonUtilities::GrowingArray<int> myVolumetricFogToRender;
	CommonUtilities::GrowingArray<int> myLightsToRender;
	CommonUtilities::GrowingArray<int> myCanvasToRender;

	CConstantBuffer myCameraBuffer;
	SCameraBufferData mySavedCameraBuffer;
	CommonUtilities::Vector4f myCameraPosition;

	CConstantBuffer myFadeBuffer;
	CommonUtilities::Vector4f myFadeColor;
	CommonUtilities::Vector3f myPlayerPosition;
	CommonUtilities::Vector4f mySavedFadeColor;
	CommonUtilities::Vector3f mySavedPlayerPosition;

	enum ERenderer
	{
		ERenderer_Forward,
		ERenderer_Fullscreen,
		ERenderer_Debug,
		ERenderer_2D,
		ERenderer_Particle,
		ERenderer_FXAA,
		ERenderer_ColorGrading,
		ERenderer_SSAO,
		ERenderer_Count
	};
	enum EGBufferState
	{
		EGBuffer_NormalDepth,
		EGBuffer_Albedo,
		EGBuffer_RMAO,
		EGBuffer_Emissive,
		EGBuffer_None
	};
	EGBufferState myGBufferState;

	bool IsActive(ERenderer aRenderer) { return myActiveRenderers[aRenderer]; }

	CForwardRenderer myForwardRenderer;
	CDeferredRenderer myDeferredRenderer;
	CFullscreenRenderer myFullscreenRenderer;
	CDebugRenderer myDebugRenderer;
	C2DRenderer my2DRenderer;
	CParticleRenderer myParticleRenderer;
	CSkyboxRenderer mySkyboxRenderer;
	CVolumetricRenderer myVolumetricRenderer;

	CFullscreenTexture myFullscreenTexture;
	CFullscreenTexture myIntermediateTexture1;
	CFullscreenTexture myIntermediateTexture2;
	CFullscreenTexture* myWritePingpong;
	CFullscreenTexture* myReadPingpong;
	CFullscreenTexture myHalfTexture;
	CFullscreenTexture myQuarterTexture1;
	CFullscreenTexture myQuarterTexture2;
	CFullscreenTexture mySSAODepthTexture;

	CFullscreenTexture myGBuffer;
	CFullscreenTexture myLightTexture;

	CGraphicsStateManager myStateManager;

	std::array<bool, ERenderer_Count> myActiveRenderers;

	CommonUtilities::CFrustum myFrustum;

	SRV myColorGradingLUT;
	SRV myNoiseTexture;
	SRV myNoiseNormalTexture;
	SRV myNoiseVolumeTexture;
	SRV myCubemap;
	SRV mySkybox;

	COrbitCamera myOrbitCamera;
	bool myUseOrbitCamera;

	bool myDrawWireframe;

	void SwapPingPong();

	void FillModelBuffer();
	void FillParticleSystemBuffer();
	void FillStreakBuffer();
	void FillVolumetricFogBuffer();
	void FillLightBuffers();
	void FillCanvasBuffer();
	void ClearResourceSlots();
	void SetCameraBufferData(const SCameraBufferData& data);
	void DrawDebugTexts();
	void UpdateDebugState();
};


#include "stdafx.h"
#include "IClientNetwork.h"

CAddressWrapper IClientNetwork::myDestinationAddress;

IClientNetwork::IClientNetwork()
{
}

IClientNetwork::~IClientNetwork()
{
}

void IClientNetwork::SetDestinationAddress(CAddressWrapper & aDestinationAddress)
{
	myDestinationAddress = aDestinationAddress;
}

void IClientNetwork::Send(CNetMessage * aMessage)
{
	ourNetMessageManager->PackAndSendMessage(aMessage, myDestinationAddress);
}

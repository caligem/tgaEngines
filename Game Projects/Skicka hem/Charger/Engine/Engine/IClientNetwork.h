#pragma once
#include "INetwork.h"
#include "AddressWrapper.h"

class IClientNetwork : public INetwork
{
public:
	IClientNetwork();
	~IClientNetwork();

	static void SetDestinationAddress(CAddressWrapper& aDestinationAddress);
	static void Send(CNetMessage* aMessage);

private:
	friend class CNetworkSystem;
	static CAddressWrapper myDestinationAddress;
};


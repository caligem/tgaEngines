#pragma once

#ifdef INTERFACE_FRIEND
#undef INTERFACE_FRIEND
#endif
#define INTERFACE_FRIEND(T) class T;
#include "IEngineFriends.h"

class CDirectXFramework;
class CWindowHandler;
class CDebugRenderer;
class CForwardRenderer;
class CSkyboxRenderer;
class CDeferredRenderer;
class C2DRenderer;
class CGraphicsPipeline;
class CComponentSystem;
class CSceneManager;
class CCameraManager;
class CModelManager;
class CParticleManager;
class CSpriteManager;
class CTextManager;
class CTextureManager;
class CScriptManager;
class CPathFinder;

class CFileWatcher;
class CShaderManager;
class CWorkerPool;

namespace Input
{
	class CInputManager;
}
namespace CommonUtilities
{
	class XBOXController;
	class Timer;
}

struct ID3D11Device;
struct ID3D11DeviceContext;

class IEngine
{
private:
	//my friends can touch
	#ifdef INTERFACE_FRIEND
	#undef INTERFACE_FRIEND
	#endif
	#define INTERFACE_FRIEND(T) friend T;
	#include "IEngineFriends.h"

	//my private parts
	static CEngine& GetEngine();
	static CDirectXFramework& GetDXFramework();
	static CWindowHandler& GetWindowHandler();
	static CDebugRenderer& GetDebugRenderer();
	static CForwardRenderer& GetForwardRenderer();
	static CSkyboxRenderer& GetSkyboxRenderer();
	static CDeferredRenderer& GetDeferredRenderer();
	static C2DRenderer& Get2DRenderer();
	static CGraphicsPipeline& GetGraphicsPipeline();

	static CComponentSystem& GetComponentSystem();

	static CSceneManager& GetSceneManager();
	static CCameraManager& GetCameraManager();
	static CModelManager& GetModelManager();
	static CParticleManager& GetParticleManager();
	static CSpriteManager& GetSpriteManager();
	static CTextManager& GetTextManager();
	static CTextureManager& GetTextureManager();
	static CScriptManager& GetScriptManager();
	static CPathFinder& GetPathFinder();

	static const CommonUtilities::Vector2f GetWindowSize();
	static const CommonUtilities::Vector2f GetCanvasSize();
	static float GetWindowRatio();
	static float GetCanvasRatio();
	static void SetWindowSize(const CommonUtilities::Vector2<unsigned short>& aWindowSize);

	static ID3D11Device* GetDevice();
	static ID3D11DeviceContext* GetContext();

	static CFileWatcher& GetFileWatcher();
	static CShaderManager& GetShaderManager();

	static CommonUtilities::Timer& Time();
	static Input::CInputManager& GetInputManager();
	static CommonUtilities::XBOXController& GetXBoxController();

	static CWorkerPool& GetWorkerPool();

	static void ShowCursor();
	static void HideCursor();

	IEngine() = delete;
	~IEngine() = delete;
	
	static CEngine *ourEngine;
};


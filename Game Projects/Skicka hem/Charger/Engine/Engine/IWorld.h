#pragma once

#include "Vector.h"

class CSceneManager;
class CComponentSystem;
class CGameObjectData;
class CGameObject;
class CFileWatcher;
class CScriptManager;
class CPathFinder;

struct SCameraBufferData;

namespace CommonUtilities
{
	class CFrustum;
}

namespace Input
{
	class CInputManager;
}
namespace CommonUtilities
{
	class XBOXController;
}
namespace CommonUtilities
{
	class Timer;
}

class IWorld
{
public:
	static CFileWatcher& GetFileWatcher();
	static CSceneManager& GetSceneManager();
	static void GameQuit();

	static const CommonUtilities::Vector2f GetWindowSize();
	static const CommonUtilities::Vector2f GetCanvasSize();

	static void SetDebugColor(
		const CommonUtilities::Vector4f& aColor = { 1.f, 0.f, 0.f, 1.f }
	);
	static void DrawDebugLine(
		const CommonUtilities::Vector3f& aSource = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aDestination = { 1.f, 1.f, 1.f }
	);
	static void DrawDebugWireCube(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	static void DrawDebugWireSphere(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	static void DrawDebugWireCircle(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	static void DrawDebugCube(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	static void DrawDebugSphere(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	static void DrawDebugArrow(
		const CommonUtilities::Vector3f& aStartPosition = {0.f, 0.f, 0.f},
		const CommonUtilities::Vector3f& aEndPosition = {1.f, 1.f, 1.f}
	);

	static void DrawDebugText(const std::string& aDebugText);

	static CommonUtilities::Timer& Time();
	static CommonUtilities::XBOXController& XBOX();
	static Input::CInputManager& Input();
	static CScriptManager& Script();
	static CPathFinder& Path();

	static void ShowCursor();
	static void HideCursor();

	static void SetFadeColor(const CommonUtilities::Vector4f& aFadeColor);
	static const CommonUtilities::Vector4f& GetFadeColor();
	static void SetPlayerPosition(const CommonUtilities::Vector3f& aPosition);
	
	static const SCameraBufferData& GetSavedCameraBuffer();
	static const CommonUtilities::CFrustum& GetSavedFrustum();

private:
	IWorld() = delete;
	~IWorld() = delete;
};


#pragma once

class CPixelShader;
class CVertexShader;

class CMaterial
{
public:
	CMaterial() { Reset(); }
	~CMaterial() { Reset(); }
	const CPixelShader* GetPixelShader() const { return myPixelShader; }
	const CVertexShader* GetVertexShader() const { return myVertexShader; }
	void SetVertexShader(const std::wstring & aShaderFile);
	void SetPixelShader(const std::wstring & aShaderFile);

	void SetCustomData(const CommonUtilities::Vector4f& aCustomData) { myCustomData = aCustomData; myHasCustomData = true; }
	void SetShouldRenderDepth(bool aShouldRenderDepth) { myShouldRenderDepth = aShouldRenderDepth; }

	const CommonUtilities::Vector4f& GetCustomData() const { return myCustomData; }
	bool HasCustomData() const { return myHasCustomData; }
	bool ShouldRenderDepth() const { return myShouldRenderDepth; }

private:
	void Reset();

	CVertexShader* myVertexShader;
	CPixelShader* myPixelShader;

	bool myShouldRenderDepth;
	bool myHasCustomData;
	CommonUtilities::Vector4f myCustomData;
};
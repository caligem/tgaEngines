#include "stdafx.h"
#include "ModelComponent.h"
#include "ModelManager.h"

CModelManager* CModelComponent::ourModelManager = nullptr;

CModelComponent::CModelComponent()
{
}

CModelComponent::~CModelComponent()
{
}

void CModelComponent::Init(const SComponentData & aComponentData)
{
	myModelID = ourModelManager->AcquireModel(aComponentData.myFilePath);
	myRadius = ourModelManager->GetModel(myModelID)->myModelData.myRadius;
}

void CModelComponent::AssignMaterial(const std::wstring & aShaderFile)
{
	myMaterial.SetPixelShader(aShaderFile);
	myMaterial.SetVertexShader(aShaderFile);
}

bool CModelComponent::HasAnimations()
{
	return ourModelManager->GetModel(myModelID)->myModelData.mySceneAnimator != nullptr;
}

void CModelComponent::Release()
{
	ourModelManager->ReleaseModel(myModelID);
	myModelID = ID_T_INVALID(CModel);
	myMaterial = CMaterial();
}

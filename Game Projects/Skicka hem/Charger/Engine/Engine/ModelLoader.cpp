#include "stdafx.h"
#include "ModelLoader.h"

#include <d3d11.h>
#include <fstream>

#include "DDSTextureLoader.h"
#include "FBXLoaderCustom.h"

#include "IEngine.h"
#include "ShaderManager.h"
#include "DXMacros.h"

CModelLoader::CModelLoader()
{
}

CModelLoader::~CModelLoader()
{
}

bool CModelLoader::Init(ID3D11Device * aDevice)
{
	if (aDevice == nullptr)
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: DirectX Device was nullptr when loading Model.");
		return false;
	}

	myDevice = aDevice;
	myLoader = new CFBXLoaderCustom();

	return true;
}

CModel::SModelData CModelLoader::LoadModel(const char * aModelPath)
{
	START_TIMER(LOAD_MODEL);

	HRESULT result;
	CModel::SModelData modelData;

	//Loading model
	CLoaderModel* loaderModel = myLoader->LoadModel(aModelPath);

	if (loaderModel == nullptr)
	{
		RESOURCE_LOG(CONCOL_ERROR, "ERROR: Failed to load model: %s", aModelPath);
		delete loaderModel;
		return CreateCube();
	}

	if (loaderModel->myMeshes.empty())
	{
		RESOURCE_LOG(CONCOL_ERROR, "ERROR: Failed to find any meshes for: %s", aModelPath);
		delete loaderModel;
		return CreateCube();
	}

	modelData.myLodCount = static_cast<unsigned char>(loaderModel->myMeshes.size());
	if (modelData.myLodCount > CModel::SModelData::MaxLODs)modelData.myLodCount = CModel::SModelData::MaxLODs;
	for (int i = 0; i < modelData.myLodCount; ++i)
	{
		//Creating Vertex Buffer
		D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
		vertexBufferDesc.ByteWidth = loaderModel->myMeshes[i]->myVertexBufferSize * loaderModel->myMeshes[i]->myVertexCount;
		vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

		D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
		vertexBufferData.pSysMem = loaderModel->myMeshes[i]->myVerticies;

		result = myDevice->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &modelData.myVertexData[i].myVertexBuffer);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		}

		//Creating Index Buffer
		D3D11_BUFFER_DESC indexBufferDesc = { 0 };
		indexBufferDesc.ByteWidth = static_cast<unsigned int>(loaderModel->myMeshes[i]->myIndexes.size() * sizeof(unsigned int));
		indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

		D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
		indexBufferData.pSysMem = &loaderModel->myMeshes[i]->myIndexes[0];

		result = myDevice->CreateBuffer(&indexBufferDesc, &indexBufferData, &modelData.myVertexData[i].myIndexBuffer);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		}

		modelData.myVertexData[i].myNumberOfIndices = static_cast<unsigned int>(loaderModel->myMeshes[i]->myIndexes.size());
		modelData.myVertexData[i].myNumberOfVertices = loaderModel->myMeshes[i]->myVertexCount;
		modelData.myVertexData[i].myStride = loaderModel->myMeshes[i]->myVertexBufferSize;
		modelData.myVertexData[i].myOffset = 0;
		modelData.myVertexData[i].myLODDistance = loaderModel->myMeshes[i]->myLODDistance;
		modelData.myRadius = std::fmaxf(loaderModel->myMeshes[i]->myRadius, modelData.myRadius);
	}

	//Load Texture
	std::string filepath(aModelPath);
 
	size_t split = filepath.find_last_of("\\/");
	std::string path = filepath.substr(0, split + 1);
	std::string name = filepath.substr(split + 1);

	// Picks out the albedo, rough, ao, normal and metallic maps from the texture list
	for (int i = 0; i < loaderModel->myTextures.size() && i < ETextureTypes_Count; ++i)
	{
		if (loaderModel->myTextures[i].empty())
		{
			RESOURCE_LOG(CONCOL_WARNING, "Missing %s texture for model %s", myTextureTypeNames[i], aModelPath);

			if (i == 0)
			{
				modelData.myTextureData.myTextures[i] = LoadDefaultTexture(-1);
			}
			else
			{
				modelData.myTextureData.myTextures[i] = LoadDefaultTexture(i);
			}
		}
		else
		{
			modelData.myTextureData.myTextures[i] = IEngine::GetTextureManager().CreateTextureFromFile(path + loaderModel->myTextures[i].c_str());
		}
	}

	modelData.mySceneAnimator = new SceneAnimator();
	modelData.mySceneAnimator->Init(loaderModel->myScene);
	if (modelData.mySceneAnimator->Animations.empty())
	{
		SAFE_DELETE(modelData.mySceneAnimator);
	}

	delete loaderModel;

	float time = GET_TIME(LOAD_MODEL);
	if (time <= 20)
	{
		RESOURCE_LOG(CONCOL_VALID, "Model %s took %fms to load!", aModelPath, time)
	}
	else if (time < 100)
	{
		RESOURCE_LOG(CONCOL_WARNING, "Model %s took %fms to load!", aModelPath, time)
	}
	else
	{
		RESOURCE_LOG(CONCOL_ERROR, "Model %s took %fms to load!", aModelPath, time)
	}

	return modelData;
}

CModel::SModelData CModelLoader::CreateCube()
{
	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
		float nX, nY, nZ, nW; //normal
		float tX, tY, tZ, tW; //tangent
		float bX, bY, bZ, bW; //binormal
		float u, v;
	} vertices[24] =
	{
		{0.5, -0.5, 0.5, 1.f,   -0, 0, 1, 1.f,  1, -0, -0, 1.f, 0, 1, -0, 1.f,  0.f, 0.f},
		{-0.5, -0.5, 0.5, 1.f,  -0, 0, 1, 1.f,  1, -0, 0, 1.f,  0, 1, -0, 1.f,  1.f, 0.f},
		{0.5, 0.5, 0.5, 1.f,    -0, 0, 1, 1.f,  1, -0, 0, 1.f,  0, 1, -0, 1.f,  0.f, 1.f},
		{-0.5, 0.5, 0.5, 1.f,   -0, 0, 1, 1.f,  1, -0, 0, 1.f,  0, 1, -0, 1.f,  1.f, 1.f},
		{0.5, 0.5, 0.5, 1.f,    -0, 1, 0, 1.f,  1, -0, 0, 1.f,  0, 0, -1, 1.f,  0.f, 0.f},
		{-0.5, 0.5, 0.5, 1.f,   -0, 1, 0, 1.f,  1, 0, 0, 1.f,   0, 0, -1, 1.f,  1.f, 0.f},
		{0.5, 0.5, -0.5, 1.f,   -0, 1, 0, 1.f,  1, 0, 0, 1.f,   0, 0, -1, 1.f,  0.f, 1.f},
		{-0.5, 0.5, -0.5, 1.f,  -0, 1, 0, 1.f,  1, 0, 0, 1.f,   0, 0, -1, 1.f,  1.f, 1.f},
		{0.5, 0.5, -0.5, 1.f,   -0, 0, -1, 1.f, 1, 0, 0, 1.f,   0, -1, 0, 1.f,  0.f, 0.f},
		{-0.5, 0.5, -0.5, 1.f,  -0, 0, -1, 1.f, 1, 0, 0, 1.f,   0, -1, 0, 1.f,  1.f, 0.f},
		{0.5, -0.5, -0.5, 1.f,  -0, 0, -1, 1.f, 1, 0, 0, 1.f,   0, -1, 0, 1.f,  0.f, 1.f},
		{-0.5, -0.5, -0.5, 1.f, -0, 0, -1, 1.f, 1, 0, 0, 1.f,   0, -1, 0, 1.f,  1.f, 1.f},
		{0.5, -0.5, -0.5, 1.f,  -0, -1, 0, 1.f, 1, 0, -0, 1.f,  0, 0, 1, 1.f,   0.f, 0.f},
		{-0.5, -0.5, -0.5, 1.f, -0, -1, 0, 1.f, 1, 0, -0, 1.f,  0, 0, 1, 1.f,   1.f, 0.f},
		{0.5, -0.5, 0.5, 1.f,   -0, -1, 0, 1.f, 1, 0, -0, 1.f,  0, 0, 1, 1.f,   0.f, 1.f},
		{-0.5, -0.5, 0.5, 1.f,  -0, -1, 0, 1.f, 1, 0, -0, 1.f,  0, 0, 1, 1.f,   1.f, 1.f},
		{-0.5, -0.5, 0.5, 1.f,  -1, 0, 0, 1.f,  0, 0, -1, 1.f,  -0, 1, 0, 1.f,  0.f, 0.f},
		{-0.5, -0.5, -0.5, 1.f, -1, 0, 0, 1.f,  0, 0, -1, 1.f,  -0, 1, 0, 1.f,  1.f, 0.f},
		{-0.5, 0.5, 0.5, 1.f,   -1, 0, 0, 1.f,  0, 0, -1, 1.f,  -0, 1, 0, 1.f,  0.f, 1.f},
		{-0.5, 0.5, -0.5, 1.f,  -1, 0, 0, 1.f,  0, -0, -1, 1.f, 0, 1, -0, 1.f,  1.f, 1.f},
		{0.5, -0.5, -0.5, 1.f,  1, 0, 0, 1.f,   0, -0, 1, 1.f,  0, 1, 0, 1.f,   0.f, 0.f},
		{0.5, -0.5, 0.5, 1.f,   1, 0, 0, 1.f,   0, -0, 1, 1.f,  0, 1, 0, 1.f,   1.f, 0.f},
		{0.5, 0.5, -0.5, 1.f,   1, 0, 0, 1.f,   0, -0, 1, 1.f,  0, 1, 0, 1.f,   0.f, 1.f},
		{0.5, 0.5, 0.5, 1.f,    1, 0, 0, 1.f,   0, -0, 1, 1.f,  0, 1, 0, 1.f,   1.f, 1.f}
	};
	unsigned int indices[36] =
	{
		2,1,0,
		3,1,2,
		6,5,4,
		7,5,6,
		10,9,8,
		11,9,10,
		14,13,12,
		15,13,14,
		18,17,16,
		19,17,18,
		22,21,20,
		23,21,22
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = myDevice->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = myDevice->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
	}

	//Load shaders
	CModel::SModelData modelData;

	for (int i = 0; i < ETextureTypes_Count; ++i)
	{
		modelData.myTextureData.myTextures[i] = LoadDefaultTexture(i);
	}

	//Setup ModelData
	modelData.myVertexData[0].myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	modelData.myVertexData[0].myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	modelData.myVertexData[0].myStride = sizeof(Vertex);
	modelData.myVertexData[0].myOffset = 0;
	modelData.myVertexData[0].myVertexBuffer = vertexBuffer;
	modelData.myVertexData[0].myIndexBuffer = indexBuffer;
	modelData.myLodCount = 1;
	modelData.myRadius = CommonUtilities::Vector3f(0.5f, 0.5f, 0.5f).Length();

	return modelData;
}

struct Vertex
{
	#pragma warning(disable: 4201)
	union
	{
		struct
		{
			float x, y, z, w;
			float nX, nY, nZ, nW; //normal
			float tX, tY, tZ, tW; //tangent
			float bX, bY, bZ, bW; //binormal
		};
		struct
		{
			CommonUtilities::Vector4f position;
			CommonUtilities::Vector4f normal;
			CommonUtilities::Vector4f tangent;
			CommonUtilities::Vector4f binormal;
		};
	};
	#pragma warning(default: 4201)
};
struct Triangle
{
	unsigned int vertex[3];
};
using TriangleList = std::vector<Triangle>;
using VertexList = std::vector<Vertex>;

using Lookup = std::map<std::pair<unsigned, unsigned>, unsigned>;

unsigned vertex_for_edge(Lookup& lookup, VertexList& vertices, unsigned first, unsigned second)
{
	Lookup::key_type key(first, second);
	if (key.first > key.second)
		std::swap(key.first, key.second);

	auto inserted = lookup.insert({ key, static_cast<unsigned>(vertices.size()) });
	if (inserted.second)
	{
		auto& edge0 = vertices[first];
		auto& edge1 = vertices[second];
		auto point = (CommonUtilities::Vector3f(&edge0.x) + CommonUtilities::Vector3f(&edge1.x)).GetNormalized();
		vertices.push_back({ point.x, point.y, point.z, 1.f });
	}

	return inserted.first->second;
}

TriangleList Subdivide(VertexList& vertices, TriangleList triangles)
{
	Lookup lookup;
	TriangleList result;

	for (auto&& each : triangles)
	{
		std::array<unsigned, 3> mid;
		for (int edge = 0; edge < 3; ++edge)
		{
			mid[edge] = vertex_for_edge(lookup, vertices,
				each.vertex[edge], each.vertex[(edge + 1) % 3]);
		}

		result.push_back({ each.vertex[0], mid[0], mid[2] });
		result.push_back({ each.vertex[1], mid[1], mid[0] });
		result.push_back({ each.vertex[2], mid[2], mid[1] });
		result.push_back({ mid[0], mid[1], mid[2] });
	}

	return result;
}

CModel::SModelData CModelLoader::CreateIcosphere(int aSubdivisions, bool aIsForLight)
{
	const float X = .525731112119133606f;
	const float Z = .850650808352039932f;
	const float N = 0.f;

	static const VertexList verticesTemplate =
	{
		{-X,N,Z}, {X,N,Z}, {-X,N,-Z}, {X,N,-Z},
		{N,Z,X}, {N,Z,-X}, {N,-Z,X}, {N,-Z,-X},
		{Z,X,N}, {-Z,X, N}, {Z,-X,N}, {-Z,-X, N}
	};

	static const TriangleList trianglesTemplate =
	{
		{0,1,4},{0,4,9},{9,4,5},{4,8,5},{4,1,8},
		{8,1,10},{8,10,3},{5,8,3},{5,3,2},{2,3,7},
		{7,3,10},{7,10,6},{7,6,11},{11,6,0},{0,6,1},
		{6,10,1},{9,11,0},{9,2,11},{9,5,2},{7,11,2}
	};

	VertexList vertices = verticesTemplate;
	TriangleList triangles = trianglesTemplate;

	for (int i = 0; i < aSubdivisions; ++i)
	{
		triangles = Subdivide(vertices, triangles);
	}

	float expandingFactor = 1.f /
		((CommonUtilities::Vector3f(&vertices[triangles[0].vertex[0]].x) +
		CommonUtilities::Vector3f(&vertices[triangles[0].vertex[1]].x) +
		CommonUtilities::Vector3f(&vertices[triangles[0].vertex[2]].x))/3.f).Length();
	if (!aIsForLight)
	{
		expandingFactor = 0.5f;
	}

	for (Vertex& vert : vertices)
	{
		vert.nX = vert.x;
		vert.nY = vert.y;
		vert.nZ = vert.z;

		vert.x *= expandingFactor;
		vert.y *= expandingFactor;
		vert.z *= expandingFactor;
	}
	for (Triangle& triangle : triangles)
	{
		Vertex& vert0 = vertices[triangle.vertex[0]];
		Vertex& vert1 = vertices[triangle.vertex[1]];
		Vertex& vert2 = vertices[triangle.vertex[2]];

		vert0.tangent = (vert1.position - vert0.position).GetNormalized();
		vert1.tangent = (vert2.position - vert1.position).GetNormalized();
		vert2.tangent = (vert0.position - vert2.position).GetNormalized();

		vert0.binormal = CommonUtilities::Vector3f(vert0.normal).Cross(vert0.tangent);
		vert1.binormal = CommonUtilities::Vector3f(vert1.normal).Cross(vert1.tangent);
		vert2.binormal = CommonUtilities::Vector3f(vert2.normal).Cross(vert2.tangent);
	}

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices[0])*static_cast<unsigned int>(vertices.size());
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = &vertices[0];

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = myDevice->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(triangles[0]) * static_cast<unsigned int>(triangles.size());
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = &triangles[0];

	ID3D11Buffer* indexBuffer;

	result = myDevice->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
	}

	CModel::SModelData modelData;

	for (int i = 0; i < ETextureTypes_Count; ++i)
	{
		modelData.myTextureData.myTextures[i] = LoadDefaultTexture(i);
	}

	//Setup ModelData
	modelData.myVertexData[0].myNumberOfVertices = (sizeof(vertices[0])*static_cast<unsigned int>(vertices.size())) / sizeof(Vertex);
	modelData.myVertexData[0].myNumberOfIndices = (sizeof(triangles[0])*static_cast<unsigned int>(triangles.size())) / sizeof(unsigned int);
	modelData.myVertexData[0].myStride = sizeof(Vertex);
	modelData.myVertexData[0].myOffset = 0;
	modelData.myVertexData[0].myVertexBuffer = vertexBuffer;
	modelData.myVertexData[0].myIndexBuffer = indexBuffer;
	modelData.myLodCount = 1;
	modelData.myRadius = 1.0f;

	return modelData;
}

SRV CModelLoader::LoadDefaultTexture(int i)
{
	switch (i)
	{
	case ETextureTypes_Albedo:
		return IEngine::GetTextureManager().CreateGrayCheckerBoardTexture();
		break;
	case ETextureTypes_RMA:
		return IEngine::GetTextureManager().CreateRMATexture();
		break;
	case ETextureTypes_Normal:
		return IEngine::GetTextureManager().CreateNormalTexture();
		break;
	case ETextureTypes_Emissive:
		return IEngine::GetTextureManager().CreateBlackTexture();
		break;
	default:
		return IEngine::GetTextureManager().CreatePinkCheckerBoardTexture();
		break;
	}
}

#pragma once

#include "Model.h"

#include <map>

#include "TextureManager.h"

struct ID3D11Device;
class CFBXLoaderCustom;

class CModelLoader
{
public:
	CModelLoader();
	~CModelLoader();

	bool Init(ID3D11Device* aDevice);
	CModel::SModelData LoadModel(const char* aModelPath);
	CModel::SModelData CreateCube();
	CModel::SModelData CreateIcosphere(int aSubdivisions = 0, bool aIsForLight = true);
	SRV LoadDefaultTexture(int i);

private:
	enum ETextureTypes
	{
		ETextureTypes_Albedo,
		ETextureTypes_RMA,
		ETextureTypes_Normal,
		ETextureTypes_Emissive,
		ETextureTypes_Count
	};
	const char* myTextureTypeNames[ETextureTypes_Count] = {
		"albedo",
		"ambientOcclusion",
		"normal",
		"emissive"
	};

	ID3D11Device* myDevice;
	CFBXLoaderCustom* myLoader;

};


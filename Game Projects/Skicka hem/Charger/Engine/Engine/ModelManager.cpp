#include "stdafx.h"
#include "ModelManager.h"
#include "DirectXFramework.h"

#include "JsonDocument.h"

CModelManager::CModelManager()
{
}


CModelManager::~CModelManager()
{
}

bool CModelManager::Init(CDirectXFramework& aDirectXFramework)
{
	if (!myModelLoader.Init(aDirectXFramework.GetDevice()))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: ModelLoader failed to initialize in ComponentSystem Init.");
		return false;
	}


	{
		ID_T(CModel) modelID = myModels.Acquire();
		myModelCache["cube"] = modelID;
		myModels.GetObj(modelID)->IncrementCounter();
		CModel::SModelData modelData;
		modelData = myModelLoader.CreateCube();
		myModels.GetObj(modelID)->Init(modelData);
		myModels.GetObj(modelID)->myMaterial.SetVertexShader(L"Assets/Shaders/Debug/Grid");
		myModels.GetObj(modelID)->myMaterial.SetPixelShader(L"Assets/Shaders/Debug/Grid");
	}

	{
		ID_T(CModel) modelID = myModels.Acquire();
		myModelCache["sphere"] = modelID;
		myModels.GetObj(modelID)->IncrementCounter();
		CModel::SModelData modelData;
		modelData = myModelLoader.CreateIcosphere(3, false);
		myModels.GetObj(modelID)->Init(modelData);
		myModels.GetObj(modelID)->myMaterial.SetVertexShader(L"Assets/Shaders/Debug/Grid");
		myModels.GetObj(modelID)->myMaterial.SetPixelShader(L"Assets/Shaders/Debug/Grid");
	}

	return true;
}

ID_T(CModel) CModelManager::AcquireModel(const char * aModelPath)
{
	std::string modelPath;

	if (aModelPath == nullptr)
	{
		modelPath = "";
	}
	else
	{
		modelPath = aModelPath;
	}

	if (myModelCache.find(modelPath) != myModelCache.end())
	{
		ID_T(CModel) id = myModelCache[modelPath];
		myModels.GetObj(id)->IncrementCounter();
		return id;
	}
	else
	{
		ID_T(CModel) modelID = myModels.Acquire();
		myModelCache[modelPath] = modelID;
		myModels.GetObj(modelID)->IncrementCounter();

		CModel::SModelData modelData;
		if (aModelPath == nullptr)
		{
			modelData = myModelLoader.CreateCube();
		}
		else
		{
			modelData = myModelLoader.LoadModel(modelPath.c_str());
		}
		myModels.GetObj(modelID)->Init(modelData);

		if (modelPath == "")
		{
			myModels.GetObj(modelID)->myMaterial.SetVertexShader(L"Assets/Shaders/Debug/Grid");
			myModels.GetObj(modelID)->myMaterial.SetPixelShader(L"Assets/Shaders/Debug/Grid");
		}
		else
		{
			SearchForModelMaterials(modelPath, modelID);
		}

		return std::move(modelID);
	}
}

void CModelManager::ReleaseModel(ID_T(CModel) aModelID)
{
	for (auto it = myModelCache.begin(); it != myModelCache.end(); ++it)
	{
		if (it->second == aModelID)
		{
			myModels.GetObj(aModelID)->DecrementCounter();

			if (myModels.GetObj(aModelID)->GetRefCount() <= 0)
			{
				CModel& model = *myModels.GetObj(aModelID);
				model.~CModel();

				myModels.Release(aModelID);
				myModelCache.erase(it);
			}

			break;
		}
	}
}

CModel * CModelManager::GetModel(ID_T(CModel) aModelID)
{
	return std::move(myModels.GetObj(aModelID));
}

void CModelManager::SearchForModelMaterials(const std::string& aModelPath, ID_T(CModel) aModelID)
{
	std::string metaDataFile = aModelPath.substr(0, aModelPath.find_last_of('.')) + ".json";
	if ((INVALID_FILE_ATTRIBUTES == GetFileAttributesA(metaDataFile.c_str()) && GetLastError() == ERROR_FILE_NOT_FOUND))
	{
		return;
	}

	JsonDocument doc(metaDataFile.c_str());
	
	if (doc.Find("vs"))
	{
		std::string filePath = std::string("Assets/Shaders/VFX/") + doc["vs"].GetString();
		std::wstring wFilePath(filePath.begin(), filePath.end());
		myModels.GetObj(aModelID)->myMaterial.SetVertexShader(wFilePath);
	}
	if (doc.Find("ps"))
	{
		std::string filePath = std::string("Assets/Shaders/VFX/") + doc["ps"].GetString();
		std::wstring wFilePath(filePath.begin(), filePath.end());
		myModels.GetObj(aModelID)->myMaterial.SetPixelShader(wFilePath);
	}
	
}

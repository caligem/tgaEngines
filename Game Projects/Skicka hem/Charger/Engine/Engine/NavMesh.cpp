#include "stdafx.h"
#include "NavMesh.h"

#include "FBXLoaderCustom.h"
#include "IWorld.h"

#include <filesystem>

#include <limits>
#include <algorithm>

#include "Intersection.h"

CNavMesh::CNavMesh()
{
}

CNavMesh::~CNavMesh()
{
}

void CNavMesh::Init(const char * aNavMeshFbxFile)
{
	myVertices.clear();
	myTriangles.clear();
	myEdges.clear();

	namespace fs = std::experimental::filesystem;

	fs::path fbxPath(aNavMeshFbxFile);
	fs::path navmeshPath = fbxPath;
	navmeshPath.replace_extension("navmesh");

	if (fs::exists(fbxPath))
	{
		if (!fs::exists(navmeshPath) || (fs::last_write_time(navmeshPath).time_since_epoch().count() < fs::last_write_time(fbxPath).time_since_epoch().count()))
		{
			GENERAL_LOG(CONCOL_WARNING, "navmesh(%s) is out of date, generating new nav mesh...", navmeshPath.string().c_str());
			InternalInitFromFBX(aNavMeshFbxFile);
		}
		else
		{
			InternalInitFromBinaryFile(navmeshPath.string().c_str());
		}
	}
	else
	{
		if (fs::exists(navmeshPath))
		{
			InternalInitFromBinaryFile(navmeshPath.string().c_str());
		}
		else
		{
			GENERAL_LOG(CONCOL_ERROR, "ERROR: Couldn't load navmesh %s!", aNavMeshFbxFile);
			return;
		}
	}

	CreateNodeGraph();
}
CommonUtilities::Vector3f CNavMesh::GetCenterOfTriangle(int aTriangleIndex)
{
	return (
		myVertices[myTriangles[aTriangleIndex].v0] +
		myVertices[myTriangles[aTriangleIndex].v1] +
		myVertices[myTriangles[aTriangleIndex].v2]
	) / 3.f;
}

void CNavMesh::InternalInitFromFBX(const char * aNavMeshFbxFile)
{
	CFBXLoaderCustom modelLoader;
	CLoaderModel* loaderModel = modelLoader.LoadModel(aNavMeshFbxFile);

	if (loaderModel == nullptr || loaderModel->myMeshes.empty())
	{
		GENERAL_LOG(CONCOL_ERROR, "ERROR: Failed to find a mesh in navmesh file: %s", aNavMeshFbxFile);
		return;
	}

	auto* mesh = loaderModel->myMeshes[0];

	float* vertexCollection = new float[(mesh->myVertexBufferSize / sizeof(float)) * mesh->myVertexCount];
	memcpy(vertexCollection, mesh->myVerticies, mesh->myVertexBufferSize * mesh->myVertexCount);

	std::vector<int> indexRemap;
	myVertices.reserve(mesh->myVertexCount);
	for (int i = 0; i < mesh->myVertexCount; ++i)
	{
		int index = AddVertexAndGetIndex({
			-vertexCollection[i*mesh->myVertexBufferSize / sizeof(float) + 0],
			vertexCollection[i*mesh->myVertexBufferSize / sizeof(float) + 1],
			-vertexCollection[i*mesh->myVertexBufferSize / sizeof(float) + 2]
		});
		indexRemap.push_back(index);
	}

	myTriangles.reserve(mesh->myIndexes.size()/3);
	myEdges.reserve(mesh->myVertexCount);

	using Lookup = std::map<std::pair<int, int>, int>;
	Lookup edgeLUT;

	for (int i = 0; i < mesh->myIndexes.size(); i += 3)
	{
		myTriangles.emplace_back();
		myTriangles.back().v0 = indexRemap[mesh->myIndexes[i + 0]];
		myTriangles.back().v1 = indexRemap[mesh->myIndexes[i + 1]];
		myTriangles.back().v2 = indexRemap[mesh->myIndexes[i + 2]];

		for (int e = 0; e < 3; ++e)
		{
			Lookup::key_type key(myTriangles.back().myVertices[(e + 0) % 3], myTriangles.back().myVertices[(e + 1) % 3]);

			if (key.first > key.second)std::swap(key.first, key.second);

			if (edgeLUT.find(key) == edgeLUT.end())
			{
				myEdges.emplace_back();
				edgeLUT[key] = static_cast<int>(myEdges.size() - 1);

				myEdges.back().v0 = key.first;
				myEdges.back().v1 = key.second;
			}

			int edgeIndex = edgeLUT[key];

			auto& edge = myEdges[edgeIndex];
			if (edge.t0 == -1)
			{
				edge.t0 = static_cast<int>(myTriangles.size() - 1);
			}
			else
			{
				edge.t1 = static_cast<int>(myTriangles.size() - 1);
			}

			myTriangles.back().myEdges[e] = edgeIndex;
		}
	}

	InternalSaveToBinaryFile(aNavMeshFbxFile);
}

void CNavMesh::InternalSaveToBinaryFile(const char * aNavMeshFbxFile)
{
	std::experimental::filesystem::path path(aNavMeshFbxFile);
	path.replace_extension("navmesh");

	FILE* fp;
	fopen_s(&fp, path.string().c_str(), "wb");

	SNavmeshBinaryHeader header;
	header.numTriangles = static_cast<int>(myTriangles.size());
	header.numEdges = static_cast<int>(myEdges.size());
	header.numVertices = static_cast<int>(myVertices.size());
	fwrite(&header, sizeof(SNavmeshBinaryHeader), 1, fp);
	fwrite(&myTriangles[0], sizeof(myTriangles[0]), header.numTriangles, fp);
	fwrite(&myEdges[0], sizeof(myEdges[0]), header.numEdges, fp);
	fwrite(&myVertices[0], sizeof(myVertices[0]), header.numVertices, fp);

	fclose(fp);
}

void CNavMesh::InternalInitFromBinaryFile(const char* aBinaryFile)
{
	FILE* fp;
	fopen_s(&fp, aBinaryFile, "rb");

	SNavmeshBinaryHeader header;
	fread_s(&header, sizeof(header), sizeof(header), 1, fp);

	myTriangles.resize(header.numTriangles);
	myEdges.resize(header.numEdges);
	myVertices.resize(header.numVertices);

	fread_s(&myTriangles[0], sizeof(myTriangles[0])*header.numTriangles, sizeof(myTriangles[0]), header.numTriangles, fp);
	fread_s(&myEdges[0], sizeof(myEdges[0])*header.numEdges, sizeof(myEdges[0]), header.numEdges, fp);
	fread_s(&myVertices[0], sizeof(myVertices[0])*header.numVertices, sizeof(myVertices[0]), header.numVertices, fp);

	fclose(fp);
}

void CNavMesh::CreateNodeGraph()
{
	myNodes.clear();

	for (int i = 0; i < myTriangles.size(); ++i)
	{
		myNodes.emplace_back();

		for (int e = 0; e < 3; ++e)
		{
			auto& edge = myEdges[myTriangles[i].myEdges[e]];
			int otherTriangle = edge.t0;
			if (otherTriangle == i)
			{
				otherTriangle = edge.t1;
			}

			if (otherTriangle == -1)
			{
				continue;
			}

			SNode& node = myNodes.back();
			node.myRoutes[node.myNumRoutes].myTarget = otherTriangle;
			node.myRoutes[node.myNumRoutes].myCost = (GetCenterOfTriangle(i) - GetCenterOfTriangle(otherTriangle)).Length();
			++node.myNumRoutes;
		}
	}
}

int CNavMesh::AddVertexAndGetIndex(const CommonUtilities::Vector3f& aVertex)
{
	for (int i = 0; i < myVertices.size(); ++i)
	{
		if((myVertices[i]==aVertex))
		{
			return i;
		}
	}

	myVertices.push_back(aVertex);
	return static_cast<int>(myVertices.size()) - 1;
}

bool CNavMesh::RayHitLocation(const CommonUtilities::Vector3f & aRayOrigin, const CommonUtilities::Vector3f & aRayDirection, CommonUtilities::Vector3f & aContactPoint)
{
	CommonUtilities::Vector3f closestPoint;
	CommonUtilities::Vector3f contactPoint;

	CommonUtilities::Vector3f minClosestPoint;
	float minDistance = FLT_MAX;
	float minT = FLT_MAX;
	for (int i = 0; i < myTriangles.size(); ++i)
	{
		if (!myNodes[i].myIsActive)
		{
			continue;
		}

		const STriangle& t = myTriangles[i];
		float newT = CommonUtilities::RayVsTriangle(aRayOrigin, aRayDirection, myVertices[t.v0], myVertices[t.v1], myVertices[t.v2], contactPoint, closestPoint, minT);
		if (newT < minT)
		{
			minT = newT;
		}
		else
		{
			float distance = (contactPoint - closestPoint).Length();
			if (distance < minDistance)
			{
				minDistance = distance;
				minClosestPoint = closestPoint;
			}
		}
	}

	if (minT < FLT_MAX)
	{
		aContactPoint = aRayOrigin + aRayDirection * minT;
	}
	else
	{
		aContactPoint = minClosestPoint;
	}

	return (minT < FLT_MAX) || (minDistance < FLT_MAX);
}

bool CNavMesh::RayHitLocation(const CommonUtilities::Vector3f & aRayOrigin, const CommonUtilities::Vector3f & aRayDirection, CommonUtilities::Vector3f & aContactPoint, int & aIndex)
{
	CommonUtilities::Vector3f closestPoint;
	CommonUtilities::Vector3f contactPoint;

	CommonUtilities::Vector3f minClosestPoint;
	float minDistance = FLT_MAX;
	float minT = FLT_MAX;
	int minClosestIndex = -1;
	for (int i = 0; i < myTriangles.size(); ++i)
	{
		if (!myNodes[i].myIsActive)
		{
			continue;
		}

		const STriangle& t = myTriangles[i];
		float newT = CommonUtilities::RayVsTriangle(aRayOrigin, aRayDirection, myVertices[t.v0], myVertices[t.v1], myVertices[t.v2], contactPoint, closestPoint, minT);
		if (newT < minT)
		{
			minT = newT;
			aIndex = i;
		}
		else
		{
			float distance = (contactPoint - closestPoint).Length();
			if (distance < minDistance)
			{
				minDistance = distance;
				minClosestPoint = closestPoint;
				minClosestIndex = i;
			}
		}
	}

	if (minT < FLT_MAX)
	{
		aContactPoint = aRayOrigin + aRayDirection * minT;
	}
	else
	{
		aContactPoint = minClosestPoint;
		aIndex = minClosestIndex;
	}

	return (minT < FLT_MAX) || (minDistance < FLT_MAX);
}

bool CNavMesh::FindPath(int aStartIndex, int aEndIndex, CommonUtilities::GrowingArray<int, int>& aPath, volatile int & aRequestCount)
{
	CommonUtilities::GrowingArray<int, int> queue(128);

	for (int i = 0; i < myNodes.size(); ++i)
	{
		myNodes[i].myDistance = std::numeric_limits<float>::max();
		myNodes[i].myPriority = std::numeric_limits<float>::max();
		myNodes[i].myPredecessor = -1;
		myNodes[i].myState = CNavMesh::ENodeState_Unvisited;
	}

	myNodes[aStartIndex].myPredecessor = -1;
	myNodes[aStartIndex].myDistance = 0.f;
	myNodes[aStartIndex].myPriority = 0.f;

	queue.Add(aStartIndex);
	
	while (!queue.Empty())
	{
		if (aRequestCount != 1)
		{
			GENERAL_LOG(CONCOL_DEFAULT, "Canceled path!");
			return false;
		}

		std::sort(queue.begin(), queue.end(), [&](auto& a, auto& b)
		{
			return myNodes[a].myPriority > myNodes[b].myPriority;
		});

		int current = queue.Pop();

		if (current == aEndIndex)break;

		myNodes[current].myState = CNavMesh::ENodeState_Closed;

		for (int r = 0; r < myNodes[current].myNumRoutes; ++r)
		{
			auto& route = myNodes[current].myRoutes[r];
			auto& next = myNodes[route.myTarget];

			if (!next.myIsActive)
			{
				continue;
			}

			if (next.myState == CNavMesh::ENodeState_Closed)
			{
				continue;
			}

			if (next.myState == CNavMesh::ENodeState_Unvisited)
			{
				queue.Add(route.myTarget);
				next.myState = CNavMesh::ENodeState_Open;
			}

			float newCost = myNodes[current].myDistance + route.myCost;

			if (newCost < next.myDistance)
			{
				next.myDistance = newCost;
				next.myPredecessor = current;
				next.myPriority = Heuristic(route.myTarget, aEndIndex) + newCost;
			}
		}
	}

	if (myNodes[aEndIndex].myPredecessor == -1)
	{
		return false;
	}

	int cur = aEndIndex;
	aPath.Add(cur);
	while (cur != -1 && cur != aStartIndex)
	{
		cur = myNodes[cur].myPredecessor;
		aPath.Add(cur);
	}

	return true;
}

float CNavMesh::Heuristic(int aFirst, int aSecond)
{
	return (GetCenterOfTriangle(aFirst) - GetCenterOfTriangle(aSecond)).Length() * CommonUtilities::Pif;
}

void CNavMesh::SimpleStupidFunnelAlgorithm(CommonUtilities::GrowingArray<SPortal, int>& aPortals, PathPoints& aPath)
{
	CommonUtilities::Vector3f portalApex;
	CommonUtilities::Vector3f portalLeft;
	CommonUtilities::Vector3f portalRight;
	int apexIndex = 0;
	int leftIndex = 0;
	int rightIndex = 0;

	portalApex = aPortals[0].myLeftPoint;
	portalLeft = aPortals[0].myLeftPoint;
	portalRight = aPortals[0].myRightPoint;

	aPath.Add(portalApex);

	for (int i = 1; i < aPortals.Size(); ++i)
	{
		SPortal& portal = aPortals[i];

		/*
		if (std::fabsf(portal.myLeftPoint.y - portalApex.y) > 0.25f || std::fabsf(portal.myRightPoint.y - portalApex.y) > 0.25f)
		{
			portalApex = (portal.myLeftPoint + portal.myRightPoint) / 2.f;
			apexIndex = CommonUtilities::Max(leftIndex + 1, rightIndex + 1);

			aPath.push_back(portalApex);

			portalLeft = portalApex;
			portalRight = portalApex;
			leftIndex = apexIndex;
			rightIndex = apexIndex;

			i = apexIndex;
			continue;
		}
		*/

		if (TriArea2(portalApex, portalRight, portal.myRightPoint) <= 0.f)
		{
			if (VEqual(portalApex, portalRight) || TriArea2(portalApex, portalLeft, portal.myRightPoint) > 0.f)
			{
				portalRight = portal.myRightPoint;
				rightIndex = i;
			}
			else
			{
				aPath.Add(portalLeft);

				portalApex = portalLeft;
				apexIndex = leftIndex;

				portalLeft = portalApex;
				portalRight = portalApex;
				leftIndex = apexIndex;
				rightIndex = apexIndex;

				i = apexIndex;
				continue;
			}
		}

		if (TriArea2(portalApex, portalLeft, portal.myLeftPoint) >= 0.f)
		{
			if (VEqual(portalApex, portalLeft) || TriArea2(portalApex, portalRight, portal.myLeftPoint) < 0.f)
			{
				portalLeft = portal.myLeftPoint;
				leftIndex = i;
			}
			else
			{
				aPath.Add(portalRight);

				portalApex = portalRight;
				apexIndex = rightIndex;

				portalLeft = portalApex;
				portalRight = portalApex;
				leftIndex = apexIndex;
				rightIndex = apexIndex;

				i = apexIndex;
				continue;
			}
		}
	}

	//aPath.Add(aPortals.GetLast().myLeftPoint);
}

void CNavMesh::FindPortals(const CommonUtilities::GrowingArray<int, int>& aPath, CommonUtilities::GrowingArray<SPortal, int>& aPortals)
{
	for (int i = 0; i < aPath.Size()-1; ++i)
	{
		CommonUtilities::Vector3f cur = GetCenterOfTriangle(aPath[i]);
		CommonUtilities::Vector3f next = GetCenterOfTriangle(aPath[i+1]);

		STriangle& triangle = myTriangles[aPath[i]];
		for (int e : triangle.myEdges)
		{
			SEdge& edge = myEdges[e];
			if ((edge.t0 == aPath[i] && edge.t1 == aPath[i + 1]) || (edge.t1 == aPath[i] && edge.t0 == aPath[i + 1]))
			{
				if (TriArea2(cur, myVertices[edge.v0], next) < 0.f)
				{
					aPortals.Add({ myVertices[edge.v1], myVertices[edge.v0] });
				}
				else
				{
					aPortals.Add({ myVertices[edge.v0], myVertices[edge.v1] });
				}
			}
		}
	}
}

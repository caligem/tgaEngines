#pragma once

#include <vector>

#include "Vector.h"

enum EPathState
{
	EPathState_None,
	EPathState_Searching,
	EPathState_NotFound,
	EPathState_Found
};

using PathPoints = CommonUtilities::GrowingArray<CommonUtilities::Vector3f, int>;
struct SPath
{
	SPath():myPathPoints(128){}
	PathPoints myPathPoints;
	float myPathLength;
	volatile EPathState myState = EPathState_None;
	volatile int myRequestCount = 0;
};

class CNavMesh
{
public:
	CNavMesh();
	~CNavMesh();

private:
	friend class CPathFinder;

	#pragma warning(disable: 4201)
	struct STriangle
	{
		STriangle()
		{
			v0 = v1 = v2 = -1;
			e0 = e1 = e2 = -1;
		}
		union
		{
			struct
			{
				int myVertices[3];
				int myEdges[3];
			};
			struct
			{
				int v0, v1, v2;
				int e0, e1, e2;
			};
		};
	};
	struct SEdge
	{
		SEdge()
		{
			myVertices[0] = myVertices[1] = -1;
			myTriangles[0] = myTriangles[1] = -1;
		}
		union
		{
			struct  
			{
				int myVertices[2];
				int myTriangles[2];
			};
			struct  
			{
				int v0, v1;
				int t0, t1;
			};
		};
	};
	#pragma warning(default: 4201)

	struct SPortal
	{
		CommonUtilities::Vector3f myLeftPoint;
		CommonUtilities::Vector3f myRightPoint;
	};
	struct SRoute
	{
		int myTarget;
		float myCost;
	};
	enum ENodeState
	{
		ENodeState_Unvisited,
		ENodeState_Open,
		ENodeState_Closed
	};
	struct SNode
	{
		SRoute myRoutes[3];
		float myDistance;
		float myPriority;
		int myPredecessor = -1;
		unsigned char myNumRoutes;
		ENodeState myState;
		bool myIsActive = true;
	};
	struct SNavmeshBinaryHeader
	{
		int numTriangles;
		int numEdges;
		int numVertices;
	};

	CommonUtilities::Vector3f GetCenterOfTriangle(int aTriangleIndex);

	void Init(const char* aNavMeshFbxFile);

	void InternalInitFromFBX(const char* aNavMeshFbxFile);

	void InternalSaveToBinaryFile(const char* aNavMeshFbxFile);
	void InternalInitFromBinaryFile(const char* aBinaryFile);

	void CreateNodeGraph();
	int AddVertexAndGetIndex(const CommonUtilities::Vector3f& aVertex);

	bool RayHitLocation(const CommonUtilities::Vector3f& aRayOrigin, const CommonUtilities::Vector3f& aRayDirection, CommonUtilities::Vector3f& aContactPoint);
	bool RayHitLocation(const CommonUtilities::Vector3f & aRayOrigin, const CommonUtilities::Vector3f & aRayDirection, CommonUtilities::Vector3f & aContactPoint, int& aIndex);

	inline float TriArea2(const CommonUtilities::Vector3f& a, const CommonUtilities::Vector3f& b, const CommonUtilities::Vector3f& c)
	{
		const float ax = b.x - a.x;
		const float az = b.z - a.z;
		const float bx = c.x - a.x;
		const float bz = c.z - a.z;
		return bx*az - ax*bz;
	}
	inline bool VEqual(const CommonUtilities::Vector3f& a, const CommonUtilities::Vector3f& b)
	{
		static const float eq = 1e-6f;
		const float dx = a.x - b.x;
		const float dy = a.y - b.y;
		const float dz = a.z - b.z;
		return (dx*dx + dy*dy + dz*dz) < eq;
	}
	bool FindPath(int aStartIndex, int aEndIndex, CommonUtilities::GrowingArray<int, int>& aPath, volatile int& aRequestCount);
	float Heuristic(int aFirst, int aSecond);
	void SimpleStupidFunnelAlgorithm(CommonUtilities::GrowingArray<SPortal, int>& aPortals, PathPoints& aPath);
	void FindPortals(const CommonUtilities::GrowingArray<int, int>& aPath, CommonUtilities::GrowingArray<SPortal, int>& aPortals);

	std::vector<SNode> myNodes;

	std::vector<STriangle> myTriangles;
	std::vector<SEdge> myEdges;
	std::vector<CommonUtilities::Vector3f> myVertices;
};


#include "stdafx.h"
#include "NetworkSystem.h"
#include "Lmcons.h"
#include "ScriptManager.h"
#include "IClientNetwork.h"

#define DEFAULT_PORT 0
#define MAINSERVER_PORT 8888
#define SERVER_ADDR "10.155.94.158"

CNetworkSystem::CNetworkSystem(CommonUtilities::Timer& aTimer)
	: myNetMessageManager(aTimer)
{
	myIsConnected = false;
}

CNetworkSystem::~CNetworkSystem()
{
	CNetMessageConnect* disconnectMessage = myNetMessageManager.CreateMessage<CNetMessageConnect>();
	disconnectMessage->SetConnectionState(CNetMessageConnect::EConnectionState_Disconnect);
	myNetMessageManager.PackAndSendMessage(disconnectMessage, myMainServerAddress);
	
	
	myNetMessageManager.CloseSocket();
}

bool CNetworkSystem::Init(char aThreadCount)
{
	if (!myNetMessageManager.Init(aThreadCount))
	{
		NETWORK_LOG(CONCOL_ERROR, "Failed to init NetMessageManager in NetworkSystem");
		return false;
	}

	myMainServerAddress.Init(SERVER_ADDR, MAINSERVER_PORT);
	IClientNetwork::SetDestinationAddress(myMainServerAddress);

	//get ip
	char szHostName[255];
	gethostname(szHostName, sizeof(szHostName));
	struct hostent *host_entry;
	host_entry = gethostbyname(szHostName);
	char * szLocalIP;
	szLocalIP = inet_ntoa(*(struct in_addr *)*host_entry->h_addr_list);

	myNetMessageManager.OpenSocket(DEFAULT_PORT, szLocalIP);

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessageLuaCommand>(
		std::bind(&CNetworkSystem::HandleLuaCommandMessage, this, std::placeholders::_1, std::placeholders::_2));

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessageConnect>(
		std::bind(&CNetworkSystem::HandleConnectionMessage, this, std::placeholders::_1, std::placeholders::_2));

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessagePing>(
		std::bind(&CNetworkSystem::RecievePings, this, std::placeholders::_1, std::placeholders::_2));

	ConnectToMainServer();

	myLuaScript = IEngine::GetScriptManager().AquireLuaScriptForNonGameObjects("Developer Consol");

	IClientNetwork::ourNetMessageManager = &myNetMessageManager;

	return true;
}

void CNetworkSystem::Update()
{
	myNetMessageManager.RecieveMessage();
	UpdatePingTimers();
}

void CNetworkSystem::ConnectToMainServer()
{
	if (myIsConnected)
	{
		NETWORK_LOG(CONCOL_WARNING, "Trying to Connect but allready connected to main Server");
		return;
	}
	NETWORK_LOG(CONCOL_VALID, "Trying to connect to server...");

	CNetMessageConnect* connectMessage = myNetMessageManager.CreateMessage<CNetMessageConnect>();
	connectMessage->SetConnectionState(CNetMessageConnect::EConnectionState_TryingToConnect);
	myNetMessageManager.PackAndSendMessage(connectMessage, myMainServerAddress);
}

void CNetworkSystem::PingMainServer()
{
	if (!myIsConnected)
	{
		return;
	}
	
	CNetMessagePing* ping = myNetMessageManager.CreateMessage<CNetMessagePing>();
	ping->SetPingState(CNetMessagePing::EPingState_Ping);
	myNetMessageManager.PackAndSendMessage(ping, myMainServerAddress);
}

void CNetworkSystem::HandleConnectionMessage(CNetMessage* aMessage, CAddressWrapper& aSenderAddress)
{
	CNetMessageConnect* connectAnswer = static_cast<CNetMessageConnect*>(aMessage);

	if (connectAnswer->GetConnectionState() == CNetMessageConnect::EConnectionState_Ok)
	{
		myNetMessageManager.SetSenderID(connectAnswer->GetTargetID());
		myIsConnected = true;
		NETWORK_LOG(CONCOL_VALID, "Connected to Main Server, myID is: %d.", connectAnswer->GetTargetID());
		SendClientName(aSenderAddress);
	}
	else if (connectAnswer->GetConnectionState() == CNetMessageConnect::EConnectionState_Full)
	{
		NETWORK_LOG(CONCOL_ERROR, "Failed to connect to main server, server is full.");
	}
}

void CNetworkSystem::RecievePings(CNetMessage* aMessage, CAddressWrapper& aSenderAddress)
{
	CNetMessagePing* ping = static_cast<CNetMessagePing*>(aMessage);

	if (ping->GetPingState() == CNetMessagePing::EPingState_Pong)
	{
		myLatestRecievedPingTimer = 0.f;
		myRTT = static_cast<unsigned int>((IEngine::Time().GetTotalTime() - ping->GetPingTimestamp()));
		myPing = myRTT / 2;
	}
	else if (ping->GetPingState() == CNetMessagePing::EPingState_Ping)
	{
		CNetMessagePing* pong = myNetMessageManager.CreateMessage<CNetMessagePing>();
		pong->SetPingState(CNetMessagePing::EPingState_Pong);
		pong->SetPingTimestamp(ping->GetTimeStamp());
		myNetMessageManager.PackAndSendMessage(pong, aSenderAddress);
	}
}

void CNetworkSystem::SendClientName(const CAddressWrapper& aSenderAddress)
{
	TCHAR userName[UNLEN + 1];
	DWORD userNameLen = UNLEN + 1;
	GetUserName((TCHAR*)userName, &userNameLen);
	std::wstring userNameWStr(userName);
	std::string userNameStr(userNameWStr.begin(), userNameWStr.end());

	CNetMessageClientName* clientName = myNetMessageManager.CreateMessage<CNetMessageClientName>();
	clientName->SetClientName(userNameStr);
	myNetMessageManager.PackAndSendMessage(clientName, aSenderAddress);
}

void CNetworkSystem::UpdatePingTimers()
{
	float dt = IEngine::Time().GetRealDeltaTime();
	myLatestRecievedPingTimer += dt;
	myPingTimer += dt;

	if (myLatestRecievedPingTimer >= 10.f)
	{
		if (myIsConnected)
		{
			NETWORK_LOG(CONCOL_WARNING, "Lost Connection to server");
			myIsConnected = false;
		}
		ConnectToMainServer();
		myLatestRecievedPingTimer = 0;
	}

	if (myPingTimer >= myPingFrequency)
	{
		PingMainServer();
		myPingTimer = 0.f;
	}
}

void CNetworkSystem::HandleLuaCommandMessage(CNetMessage * aMessage, CAddressWrapper &)
{
	CNetMessageLuaCommand* luaCommandMessage = static_cast<CNetMessageLuaCommand*>(aMessage);
	IEngine::GetScriptManager().RunString(myLuaScript, luaCommandMessage->GetCommand().c_str());
}

#pragma once
#include "SocketWrapper.h"
#include "AddressWrapper.h"
#include "NetMessageManager.h"

class CNetworkSystem
{
public:
	CNetworkSystem(CommonUtilities::Timer& aTimer);
	~CNetworkSystem();

	bool Init(char aThreadCount);
	void Update();
	void ConnectToMainServer();

	inline const bool IsConnected() { return myIsConnected; }
	inline const uint32_t GetPing() { return myPing; }
	inline const uint32_t GetRTT() { return myRTT; }

private:
	void HandleConnectionMessage(CNetMessage* aMessage, CAddressWrapper& aSenderAddress);
	void SendClientName(const CAddressWrapper& aSenderAddress);

	void RecievePings(CNetMessage* aMessage, CAddressWrapper& aSenderAddress);
	void PingMainServer();
	void UpdatePingTimers();
	void HandleLuaCommandMessage(CNetMessage* aMessage, CAddressWrapper&);

	CAddressWrapper myMainServerAddress;
	CNetMessageManager myNetMessageManager;

	float myLatestRecievedPingTimer = 0.f;
	float myPingFrequency = 1.f;
	float myPingTimer = 0.f;

	float myTryingToConnectTimer = 0.f;
	float myTryingToConnectMaxTimer = 3.f;

	unsigned int myPing = 0;
	unsigned int myRTT = 0;

	ID_T(CLuaScript) myLuaScript;

	bool myIsConnected;
};
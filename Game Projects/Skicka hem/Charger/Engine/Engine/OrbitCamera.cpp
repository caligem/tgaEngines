#include "stdafx.h"
#include "OrbitCamera.h"

#include "IWorld.h"

COrbitCamera::COrbitCamera()
{
	myPivot = { 0.f, 0.f, 0.f };
	myRotation = { 0.f, 0.f };
	myZoom = 20.f;
}

COrbitCamera::~COrbitCamera()
{
}

void COrbitCamera::Update()
{
	Input::CInputManager& input = IWorld::Input();

	if (input.IsKeyDown(Input::Key_Alt, 1))
	{
		HandleOrbitControls();
	}
	else if(input.IsButtonDown(Input::Button_Right, 1))
	{
		HandleFpsControls();
	}
}

void COrbitCamera::SetPosDir(const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector3f & aDirection)
{
	myZoom = 20.f;
	myPivot = aPosition + aDirection * myZoom;

	CommonUtilities::Vector3f flatDirection = { aDirection.x, 0.f, aDirection.z };
	flatDirection.Normalize();

	float yAngle = std::acosf(flatDirection.Dot({ 0.f, 0.f, 1.f }));
	if (flatDirection.Cross({ 0.f, 0.f, 1.f }).y > 0.f)
	{
		yAngle *= -1.f;
	}

	myRotation = { std::asinf(-aDirection.y), yAngle };

	if (myRotation.x >= CommonUtilities::Pif / 2.f)
	{
		myRotation.x = CommonUtilities::Pif / 2.f - 0.001f;
	}
	if (myRotation.x <= -CommonUtilities::Pif / 2.f)
	{
		myRotation.x = -CommonUtilities::Pif / 2.f + 0.001f;
	}

	CommonUtilities::Vector3f newPos(0.f, 0.f, -myZoom);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundX(myRotation.x);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundY(myRotation.y);
	newPos += myPivot;

	myTransform.SetPosition(newPos);
	myTransform.LookAt(myPivot);
}

void COrbitCamera::HandleOrbitControls()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Vector2f movement = input.GetMouseMovement(1);
	movement.x *= (2.f*CommonUtilities::Pif) / IWorld::GetWindowSize().x;
	movement.y *= (CommonUtilities::Pif) / IWorld::GetWindowSize().y;

	if (input.IsButtonDown(Input::Button_Left, 1))
	{
		myRotation.x += movement.y;
		myRotation.y += movement.x;

		if (myRotation.x >= CommonUtilities::Pif / 2.f)
		{
			myRotation.x = CommonUtilities::Pif / 2.f - 0.001f;
		}
		if (myRotation.x <= -CommonUtilities::Pif / 2.f)
		{
			myRotation.x = -CommonUtilities::Pif / 2.f + 0.001f;
		}
	}
	else if (input.IsButtonDown(Input::Button_Middle, 1))
	{
		myPivot -= myTransform.GetRight() * movement.x * myZoom;
		myPivot += myTransform.GetUp() * movement.y * myZoom;
	}
	else if (input.IsButtonDown(Input::Button_Right, 1))
	{
		myZoom -= (movement.x + movement.y) * myZoom;
		if (myZoom < 0.1f)
		{
			myZoom = 0.1f;
			myPivot += myTransform.GetForward() * (movement.x + movement.y);
		}
	}

	CommonUtilities::Vector3f newPos(0.f, 0.f, -myZoom);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundX(myRotation.x);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundY(myRotation.y);
	newPos += myPivot;

	myTransform.SetPosition(newPos);
	myTransform.LookAt(myPivot);
}

void COrbitCamera::HandleFpsControls()
{
	static float normalSpeed = 20.f;
	static float fastSpeed = 50.f;

	float dt = IWorld::Time().GetRealDeltaTime();

	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Vector2f movement = input.GetMouseMovement(1);
	movement.x *= (2.f*CommonUtilities::Pif) / IWorld::GetWindowSize().x;
	movement.y *= (CommonUtilities::Pif) / IWorld::GetWindowSize().y;

	myRotation.x += movement.y;
	myRotation.y += movement.x;

	float speed = normalSpeed;
	if (input.IsKeyDown(Input::Key_Shift, 1))speed = fastSpeed;
	if (input.IsKeyDown(Input::Key_W, 1))
	{
		myTransform.Move(myTransform.GetForward() * dt * speed);
	}
	if (input.IsKeyDown(Input::Key_S, 1))
	{
		myTransform.Move(-myTransform.GetForward() * dt * speed);
	}
	if (input.IsKeyDown(Input::Key_D, 1))
	{
		myTransform.Move(myTransform.GetRight() * dt * speed);
	}
	if (input.IsKeyDown(Input::Key_A, 1))
	{
		myTransform.Move(-myTransform.GetRight() * dt * speed);
	}
	if (input.IsKeyDown(Input::Key_E, 1))
	{
		myTransform.Move(myTransform.GetUp() * dt * speed);
	}
	if (input.IsKeyDown(Input::Key_Q, 1))
	{
		myTransform.Move(-myTransform.GetUp() * dt * speed);
	}

	if (myRotation.x >= CommonUtilities::Pif / 2.f)
	{
		myRotation.x = CommonUtilities::Pif / 2.f - 0.001f;
	}
	if (myRotation.x <= -CommonUtilities::Pif / 2.f)
	{
		myRotation.x = -CommonUtilities::Pif / 2.f + 0.001f;
	}

	CommonUtilities::Vector3f direction(0.f, 0.f, 1.f);
	direction *= CommonUtilities::Matrix33f::CreateRotateAroundX(myRotation.x);
	direction *= CommonUtilities::Matrix33f::CreateRotateAroundY(myRotation.y);

	SetPosDir(myTransform.GetPosition(), direction);
}


#include "stdafx.h"
#include "ParticleRenderer.h"

#include "IEngine.h"
#include "ShaderManager.h"
#include "ParticleManager.h"
#include "ConstantBuffer.h"

#include "TextureManager.h"

#include "RenderCommand.h"

#include "StreakComponent.h"

CParticleRenderer::CParticleRenderer()
{
	myVertexShader = nullptr;
	myPixelShader = nullptr;
	myParticleGeometryShader = nullptr;
}

CParticleRenderer::~CParticleRenderer()
{
}

bool CParticleRenderer::Init()
{
	myVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/Particle/Particle.vs", EShaderInputLayoutType_Particle);
	myPixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/Particle/Particle.ps");
	myParticleGeometryShader = &IEngine::GetShaderManager().GetGeometryShader(L"Assets/Shaders/Particle/Particle.gs");
	myStreakGeometryShader = &IEngine::GetShaderManager().GetGeometryShader(L"Assets/Shaders/Particle/Streak.gs");

	if (!InitVertexBuffer())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init StreakBuffer in CParticleRenderer!");
		return false;
	}

	return true;
}

void CParticleRenderer::RenderParticles(CGraphicsStateManager & aStateManager, const CommonUtilities::GrowingArray<SParticleSystemRenderCommand>& aParticleSystemRenderCommands, CConstantBuffer & aCameraBuffer, const CommonUtilities::CFrustum & aFrustum)
{
	ID3D11DeviceContext* context = IEngine::GetContext();
	CTextureManager& textureManager = IEngine::GetTextureManager();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);
	aCameraBuffer.SetBuffer(0, EShaderType_Geometry);

	myVertexShader->Bind();
	myVertexShader->BindLayout();
	myParticleGeometryShader->Bind();
	myPixelShader->Bind();

	D3D11_MAPPED_SUBRESOURCE data;
	ZeroMemory(&data, sizeof(D3D11_MAPPED_SUBRESOURCE));

	CGraphicsStateManager::EBlendState currentBlendState = CGraphicsStateManager::EBlendState_Disabled;

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
	context->IASetVertexBuffers(0, 1, &myVertexData.myVertexBuffer, &myVertexData.myStride, &myVertexData.myOffset);

	for (auto& command : aParticleSystemRenderCommands)
	{
		if (aFrustum.CullAABB(command.myMin, command.myMax))
		{
			continue;
		}

		CParticleEmitter* particleEmitter = IEngine::GetParticleManager().GetParticleEmitter(command.myParticleEmitterID);
		
		if (currentBlendState != particleEmitter->GetBlendState())
		{
			currentBlendState = particleEmitter->GetBlendState();
			aStateManager.SetBlendState(currentBlendState);
		}

		int particles = command.myParticles.Size();
		if (particles <= 0)
		{
			continue;
		}

		int hasRendered = 0;
		unsigned int leftToRender = particles;

		ID3D11ShaderResourceView* texture = textureManager.GetTexture(particleEmitter->myTexture);
		context->PSSetShaderResources(0, 1, &texture);

		auto dataPtr = command.myParticles.begin();

		while (hasRendered < particles)
		{
			HRESULT result = context->Map(myVertexData.myVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &data);
			if (FAILED(result))
			{
				ENGINE_LOG(CONCOL_ERROR, "Failed to Map Constant Buffer");
			}

			int pointsToRender = CommonUtilities::Min(leftToRender, CParticleRenderer::MaxBufferSize);

			unsigned int copySize = pointsToRender * myVertexData.myStride;
			memcpy_s(data.pData, copySize, dataPtr + hasRendered, copySize);
			
			context->Unmap(myVertexData.myVertexBuffer, 0);

			context->Draw(pointsToRender, 0);

			hasRendered += pointsToRender;
			leftToRender -= pointsToRender;
		}
	}

	myVertexShader->Unbind();
	myVertexShader->UnbindLayout();
	myParticleGeometryShader->Unbind();
	myPixelShader->Unbind();
}

void CParticleRenderer::RenderStreaks(CGraphicsStateManager & aStateManager, const CommonUtilities::GrowingArray<SStreakRenderCommand>& aStreakRenderCommands, CConstantBuffer & aCameraBuffer, const CommonUtilities::CFrustum & aFrustum)
{
	ID3D11DeviceContext* context = IEngine::GetContext();
	CTextureManager& textureManager = IEngine::GetTextureManager();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);
	aCameraBuffer.SetBuffer(0, EShaderType_Geometry);

	myVertexShader->Bind();
	myVertexShader->BindLayout();
	myStreakGeometryShader->Bind();
	myPixelShader->Bind();

	D3D11_MAPPED_SUBRESOURCE data;
	ZeroMemory(&data, sizeof(D3D11_MAPPED_SUBRESOURCE));

	//CGraphicsStateManager::EBlendState currentBlendState = CGraphicsStateManager::EBlendState_Disabled;
	aStateManager;

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP_ADJ);
	context->IASetVertexBuffers(0, 1, &myVertexData.myVertexBuffer, &myVertexData.myStride, &myVertexData.myOffset);

	for (auto& command : aStreakRenderCommands)
	{
		if (aFrustum.CullAABB(command.myMin, command.myMax))
		{
			continue;
		}

		CStreak* streak = IEngine::GetParticleManager().GetStreak(command.myStreakID);

		int streakSize = command.myPoints.Size();
		if (streakSize <= 0)
		{
			continue;
		}

		int hasRendered = 0;
		unsigned int leftToRender = streakSize;

		ID3D11ShaderResourceView* texture = textureManager.GetTexture(streak->myTexture);
		context->PSSetShaderResources(0, 1, &texture);

		auto dataPtr = command.myPoints.begin();

		while (hasRendered < streakSize)
		{
			HRESULT result = context->Map(myVertexData.myVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &data);
			if (FAILED(result))
			{
				ENGINE_LOG(CONCOL_ERROR, "Failed to Map Constant Buffer");
			}

			int pointsToRender = CommonUtilities::Min(leftToRender, CParticleRenderer::MaxBufferSize);

			unsigned int offset = 0;

			unsigned int copySize = myVertexData.myStride;
			memcpy_s(data.pData, copySize, dataPtr + CommonUtilities::Max(0, hasRendered-1), copySize);
			offset += copySize;

			copySize = pointsToRender * myVertexData.myStride;
			memcpy_s(reinterpret_cast<void*>(reinterpret_cast<char*>(data.pData) + offset), copySize, dataPtr + hasRendered, copySize);
			offset += copySize;

			int extra = 1;
			copySize = myVertexData.myStride;
			if (hasRendered + pointsToRender == streakSize)
			{
				memcpy_s(reinterpret_cast<void*>(reinterpret_cast<char*>(data.pData) + offset), copySize, &command.myLeadingPoint, copySize);
				offset += copySize;

				memcpy_s(reinterpret_cast<void*>(reinterpret_cast<char*>(data.pData) + offset), copySize, &command.myLeadingPoint, copySize);
				extra = 3;
			}
			else
			{
				memcpy_s(reinterpret_cast<void*>(reinterpret_cast<char*>(data.pData) + offset), copySize, dataPtr+hasRendered+pointsToRender, copySize);
				offset += copySize;

				memcpy_s(reinterpret_cast<void*>(reinterpret_cast<char*>(data.pData) + offset), copySize, dataPtr+hasRendered+pointsToRender+1, copySize);
				extra = 3;
			}
			
			context->Unmap(myVertexData.myVertexBuffer, 0);

			context->Draw(pointsToRender + extra, 0);

			hasRendered += pointsToRender;
			leftToRender -= pointsToRender;
		}
	}

	myVertexShader->Unbind();
	myVertexShader->UnbindLayout();
	myStreakGeometryShader->Unbind();
	myPixelShader->Unbind();
}

bool CParticleRenderer::InitVertexBuffer()
{
	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = (MaxBufferSize+8) * sizeof(CParticleSystemComponent::SParticleBufferData);
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	HRESULT result = IEngine::GetDevice()->CreateBuffer(&bufferDesc, nullptr, &myVertexData.myVertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer in ParticleEmitter!");
		return false;
	}

	myVertexData.myStride = sizeof(CParticleSystemComponent::SParticleBufferData);
	myVertexData.myOffset = 0;

	return true;
}

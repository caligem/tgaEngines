#include "stdafx.h"
#include "ParticleSystemComponent.h"
#include "ParticleManager.h"
#include "IEngine.h"
#include "Random.h"

#include "Mathf.h"
#include "IWorld.h"

CParticleManager* CParticleSystemComponent::ourParticleManager = nullptr;

CParticleSystemComponent::CParticleSystemComponent()
{
}

CParticleSystemComponent::~CParticleSystemComponent()
{
}

void CParticleSystemComponent::Init()
{
	Init({});
}

void CParticleSystemComponent::Init(const char * aFilePath)
{
	myState = EParticleState_Play;

	myTotalPlayed = 0.f;
	mySpawnTimer = 0.f;
	
	myParticleEmitterID = ourParticleManager->AcquireParticleEmitter(aFilePath);

	myParticles.Init(128);
	myParticleProperties.Init(128);

	myFirstUpdate = true;
}

void CParticleSystemComponent::Init(SComponentData aComponentData)
{
	myState = EParticleState_Play;

	myTotalPlayed = 0.f;
	mySpawnTimer = 0.f;
	
	myParticleEmitterID = ourParticleManager->AcquireParticleEmitter(aComponentData.myFilePath);

	myParticles.Init(128);
	myParticleProperties.Init(128);
}

void CParticleSystemComponent::PlayInstant()
{
	Play();
	float invSpawnRate = 1.f / ourParticleManager->myParticleEmitters.GetObj(myParticleEmitterID)->GetSpawnRate();
	mySpawnTimer = invSpawnRate;
}

void CParticleSystemComponent::Play()
{
	myState = EParticleState_Play;
}

void CParticleSystemComponent::Pause()
{
	myState = EParticleState_Paused;
}

void CParticleSystemComponent::Stop()
{
	myState = EParticleState_Stopped;
	Reset();
}

bool CParticleSystemComponent::IsPlaying()
{
	return myState == EParticleState_Play;
}

void CParticleSystemComponent::Update(const CTransform& aTransform)
{
	if (myState == EParticleState_Stopped)
	{
		return;
	}

	if (!myFirstUpdate && IWorld::GetSavedFrustum().CullAABB(myBounds.GetMin(), myBounds.GetMax()))
	{
		return;
	}

	float dt = IEngine::Time().GetDeltaTime();

	myBounds.SetMinMax(aTransform.GetPosition(), aTransform.GetPosition());
	UpdateParticles(dt);

	if (myState == EParticleState_Paused)
	{
		return;
	}

	CParticleEmitter& emitter = *ourParticleManager->myParticleEmitters.GetObj(myParticleEmitterID);
	float invSpawnRate = 1.f / static_cast<float>(emitter.GetSpawnRate());
	mySpawnTimer += dt;
	myTotalPlayed += dt;

	if (myTotalPlayed >= emitter.GetDuration())
	{
		if (emitter.GetIsLoopable())
		{
			myTotalPlayed = 0.0f;
		}
	}

	int numParticlesToSpawn = static_cast<int>(mySpawnTimer / invSpawnRate);
	mySpawnTimer -= numParticlesToSpawn * invSpawnRate;
	float currentParticle = numParticlesToSpawn-1.f;

	while (currentParticle >= 0)
	{
		if (!SpawnParticle(emitter, aTransform))
		{
			break;
		}

		--currentParticle;
	}

	RemovedDeadParticles(emitter);
}

void CParticleSystemComponent::UpdateParticles(float aDeltaTime)
{
	CommonUtilities::Vector3f velocityBuffer;

	CParticleEmitter& emitter = *ourParticleManager->myParticleEmitters.GetObj(myParticleEmitterID);

	for (unsigned short i = 0; i < myParticles.Size(); ++i)
	{
		SParticleBufferData& particle = myParticles[i];
		SParticlePropertyData& props = myParticleProperties[i];

		if (emitter.GetShapeData().myShapeType == CParticleEmitter::ShapeType::ESphere)
		{
			particle.myPosition += CommonUtilities::Vector4f(props.myVelocity + props.myGravityVelocity) * aDeltaTime;
		}
		else
		{
			particle.myPosition += CommonUtilities::Vector4f(props.myVelocity * props.myOrientation + props.myGravityVelocity) * aDeltaTime;
		}
		particle.myColor = CommonUtilities::Lerp(emitter.GetStartColor(), emitter.GetEndColor(), props.myLifetime / static_cast<float>(emitter.GetLifetime()));
		particle.mySize = CommonUtilities::Lerp(emitter.GetStartSize(), emitter.GetEndSize(), props.myLifetime / static_cast<float>(emitter.GetLifetime()));
		particle.myRotation += props.myRotationVelocity * aDeltaTime;

		myBounds.EncapsulateWithoutRecalculating(particle.myPosition);

		props.myVelocity += emitter.GetAcceleration() * aDeltaTime;
		props.myGravityVelocity += (CommonUtilities::Vector3f(0.f, -ourGravityValue, 0.f) * emitter.GetGravityModifier()) * aDeltaTime;
		props.myLifetime += aDeltaTime;
	}

	float inflation = CommonUtilities::Max(
		emitter.GetStartSize().Length(),
		emitter.GetEndSize().Length()
	);
	myBounds.Inflate(inflation);
}

void CParticleSystemComponent::Reset()
{
	mySpawnTimer = 0.f;
	myTotalPlayed = 0.f;

	myParticles.RemoveAll();
	myParticleProperties.RemoveAll();
}

bool CParticleSystemComponent::SpawnParticle(CParticleEmitter& aParticleEmitter, const CTransform& aTransform)
{
	if (myTotalPlayed > aParticleEmitter.GetDuration())
	{
		if (myParticles.Empty())
		{
			Stop();
		}
		return false;
	}

	SParticleBufferData particle;
	SParticlePropertyData props;

	particle.myRotation = CommonUtilities::RandomRange(aParticleEmitter.GetStartRotation().x, aParticleEmitter.GetStartRotation().y);

	props.myVelocity = aParticleEmitter.GetStartVelocity();
	props.myLifetime = 0.f;
	props.myOrientation = aTransform.GetOrientation();
	props.myRotationVelocity = CommonUtilities::RandomRange(aParticleEmitter.GetRotationVelocity().x, aParticleEmitter.GetRotationVelocity().y);
	props.myGravityVelocity = { 0.f, 0.f, 0.f };


	if (aParticleEmitter.GetShapeData().myShapeType == CParticleEmitter::ShapeType::ESphere)
	{
		float angleA = CommonUtilities::Random() * 2.f * CommonUtilities::Pif;
		float angleB = CommonUtilities::Random() * CommonUtilities::Pif;
		float randomValue = CommonUtilities::Random();

		CommonUtilities::Vector3f newPosition = { std::sinf(angleB) * std::cosf(angleA), std::sinf(angleB) * std::sinf(angleA), std::cosf(angleB) };
		newPosition *= aParticleEmitter.GetShapeData().myRadius;
		newPosition *= (1.f - (1.f - aParticleEmitter.GetShapeData().myRadiusThickness) * randomValue);

		particle.myPosition = aTransform.GetPosition() + newPosition;
		props.myVelocity = newPosition.GetNormalized() * aParticleEmitter.GetStartVelocity().x;
	}
	else if (aParticleEmitter.GetShapeData().myShapeType == CParticleEmitter::ShapeType::EBox)
	{
		float xRange = aParticleEmitter.GetShapeData().myBoxSize.x;
		float yRange = aParticleEmitter.GetShapeData().myBoxSize.y;
		float zRange = aParticleEmitter.GetShapeData().myBoxSize.z;
		float thickness = aParticleEmitter.GetShapeData().myBoxThickness;

		float xMin = xRange * thickness;
		float yMin = yRange * thickness;
		float zMin = zRange * thickness;

		float xMax = 2.f * xRange - xMin;
		float yMax = 2.f * yRange - yMin;
		float zMax = 2.f * zRange - zMin;

		CommonUtilities::Vector3f range = {
			CommonUtilities::RandomRange(0, 2.f * xRange),
			CommonUtilities::RandomRange(0, 2.f * yRange),
			CommonUtilities::RandomRange(0, 2.f * zRange)
		};
		
		int axis = -1;
		float minDist = FLT_MAX;
		float currDist;

		if (range.x <= xMin)
		{
			currDist = xMin - range.x;
			if (currDist < minDist)
			{
				minDist = currDist;
				axis = 0;	
			}
		}
		if (range.y <= yMin)
		{
			currDist = yMin - range.y;
			if (currDist < minDist)
			{
				minDist = currDist;
				axis = 1;
			}
		}
		if (range.z <= zMin)
		{
			currDist = zMin - range.z;
			if (currDist < minDist)
			{
				minDist = currDist;
				axis = 2;
			}
		}

		if (range.x >= xMax)
		{
			currDist = range.x - xMax;
			if (currDist < minDist)
			{
				minDist = currDist;
				axis = 0;
			}
		}
		if (range.y >= yMax)
		{
			currDist = range.y - yMax;
			if (currDist < minDist)
			{
				minDist = currDist;
				axis = 1;
			}
		}
		if (range.z >= zMax)
		{
			currDist = range.z - zMax;
			if (currDist < minDist)
			{
				minDist = currDist;
				axis = 2;
			}
		}

		if (axis == 0)
		{
			range.x = CommonUtilities::RandomRange(xMin, xMax);
		}
		else if (axis == 1)
		{
			range.y = CommonUtilities::RandomRange(yMin, yMax);
		}
		else if (axis == 2)
		{
			range.z = CommonUtilities::RandomRange(zMin, zMax);
		}
			

		if (range.x > xRange) range.x -= 2.f * xRange;
		if (range.y > yRange) range.y -= 2.f * yRange;
		if (range.z > zRange) range.z -= 2.f * zRange;

		CommonUtilities::Vector3f randomSign = { CommonUtilities::Random(), CommonUtilities::Random(), CommonUtilities::Random() };
		if (randomSign.x <= 0.5f) range.x *= -1.f;
		if (randomSign.y <= 0.5f) range.y *= -1.f;
		if (randomSign.z <= 0.5f) range.z *= -1.f;

		particle.myPosition = aTransform.GetPosition() + range;
	}
	else if(aParticleEmitter.GetShapeData().myShapeType == CParticleEmitter::ShapeType::EPoint)
	{
		particle.myPosition = aTransform.GetPosition();
	}


	myParticles.Add(particle);
	myParticleProperties.Add(props);

	return true;
}

void CParticleSystemComponent::Release()
{
	ourParticleManager->ReleaseParticleEmitter(myParticleEmitterID);
	myParticleEmitterID = ID_T_INVALID(CParticleEmitter);
}

void CParticleSystemComponent::RemovedDeadParticles(const CParticleEmitter& aParticleEmitter)
{
	for (unsigned short i = myParticles.Size(); i > 0; --i)
	{
		if (myParticleProperties[i - 1].myLifetime > aParticleEmitter.GetLifetime())
		{
			myParticles.RemoveCyclicAtIndex(i - 1);
			myParticleProperties.RemoveCyclicAtIndex(i - 1);
		}
	}
}

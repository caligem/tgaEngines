#include "stdafx.h"
#include "PathFinder.h"

#include "NavMesh.h"

#include "Intersection.h"

#include "IWorld.h"

CPathFinder::CPathFinder()
	: myPaths(256)
{
}

CPathFinder::~CPathFinder()
{
	myWorkerPool.Destroy();
}

bool CPathFinder::Init()
{
	myWorkerPool.Init(1);

	return true;
}

bool CPathFinder::IsThereAPath(const CommonUtilities::Vector3f& aSource, const CommonUtilities::Vector3f& aDestination, float& aOutLength)
{
	PathPoints pathPoints(128);
	int startIndex = -1;
	int endIndex = -1;

	CommonUtilities::Vector3f contactPoint;
	myNavmesh.RayHitLocation(
		aSource + CommonUtilities::Vector3f::Up,
		{ 0.f, -1.f, 0.f },
		contactPoint,
		startIndex
	);
	myNavmesh.RayHitLocation(
		aDestination + CommonUtilities::Vector3f::Up,
		{ 0.f, -1.f, 0.f },
		contactPoint,
		endIndex
	);

	if (startIndex == -1 || endIndex == -1)
	{
		return false;
	}

	if (startIndex == endIndex)
	{
		return true;
	}

	int requestCount = 1;
	CommonUtilities::GrowingArray<int, int> pathIndices(512);
	myMutex.lock();
	if (!myNavmesh.FindPath(startIndex, endIndex, pathIndices, requestCount))
	{
		myMutex.unlock();
		return false;
	}
	myMutex.unlock();

	CommonUtilities::GrowingArray<CNavMesh::SPortal, int> portals(128);
	portals.Add({ aDestination, aDestination });
	myNavmesh.FindPortals(pathIndices, portals);
	portals.Add({ aSource, aSource });

	myNavmesh.SimpleStupidFunnelAlgorithm(portals, pathPoints);

	float length = 0.f;
	for (int i = 0; i < pathPoints.Size()-1; ++i)
	{
		length += (pathPoints[i] - pathPoints[i + 1]).Length();
	}

	aOutLength = length;

	return true;
}

void CPathFinder::ClearRequests()
{
	myWorkerPool.RemoveAllJobs();
	for (auto& it : myPaths)
	{
		it.myState = EPathState_None;
	}
}

void CPathFinder::LoadNavmesh(const char * aNavmeshFile)
{
	myNavmesh.Init(aNavmeshFile);
}

bool CPathFinder::RayHitLocation(const CommonUtilities::Vector3f & aRayOrigin, const CommonUtilities::Vector3f & aRayDirection, CommonUtilities::Vector3f & aContactPoint)
{
	return myNavmesh.RayHitLocation(aRayOrigin, aRayDirection, aContactPoint);
}

void CPathFinder::DebugRender()
{
	for (auto& edge : myNavmesh.myEdges)
	{
		bool isActive = true;
		for (int i = 0; i < 2; ++i)
		{
			if (edge.myTriangles[i] == -1)continue;

			if (!myNavmesh.myNodes[edge.myTriangles[i]].myIsActive)
			{
				isActive = false;
			}
		}
		if (isActive)
		{
			IWorld::SetDebugColor({ 0.f, 1.f, 0.f, 1.f });
		}
		else
		{
			IWorld::SetDebugColor({ 1.f, 0.f, 0.f, 1.f });
		}
		IWorld::DrawDebugLine(myNavmesh.myVertices[edge.v0] + CommonUtilities::Vector3f::Up*0.5f, myNavmesh.myVertices[edge.v1] + CommonUtilities::Vector3f::Up*0.5f);
	}
	for (int i = 0; i < myNavmesh.myNodes.size(); ++i)
	{
		int predecessor = myNavmesh.myNodes[i].myPredecessor;
		if (predecessor < 0)
		{
			continue;
		}

		IWorld::SetDebugColor({ 0.f, 1.f, 1.f, 1.f });
		IWorld::DrawDebugLine(
			myNavmesh.GetCenterOfTriangle(i) + CommonUtilities::Vector3f::Up*2.0f,
			myNavmesh.GetCenterOfTriangle(predecessor) + CommonUtilities::Vector3f::Up*2.0f
		);
	}
	/*
	for (int n = 0; n < myNavmesh.myNodes.size(); ++n)
	{
		auto& node = myNavmesh.myNodes[n];
		for (int i = 0; i < node.myNumRoutes; ++i)
		{
			IWorld::SetDebugColor({ 1.f, 1.f, 0.f, 1.f });
			IWorld::DrawDebugLine(myNavmesh.GetCenterOfTriangle(node.myRoutes[i].myTarget) + CommonUtilities::Vector3f::Up*2.f, myNavmesh.GetCenterOfTriangle(n) + CommonUtilities::Vector3f::Up*2.f);
		}
	}
	*/
}

void CPathFinder::DebugRenderPath(PathTicket & aTicket)
{
	SPath* path = myPaths.GetObj(aTicket);
	if (path == nullptr)
	{
		return;
	}

	if (path->myState != EPathState_Found)
	{
		return;
	}

	DebugRenderPath(path->myPathPoints);
}

void CPathFinder::DebugRenderPath(PathPoints & aPath)
{
	for (int i = 0; i < aPath.Size(); ++i)
	{
		auto& pos = aPath[i];

		IWorld::SetDebugColor({ 1.0f, 0.f, 1.f, 1.f });
		IWorld::DrawDebugWireSphere(pos);

		if (i < aPath.Size() - 1)
		{
			auto& nextPos = aPath[i + 1];

			IWorld::SetDebugColor({ 0.0f, 1.f, 1.f, 1.f });
			IWorld::DrawDebugLine(pos, nextPos);
		}
	}
}

void CPathFinder::RequestPath(const CommonUtilities::Vector3f & aStartPosition, const CommonUtilities::Vector3f & aEndPosition, PathTicket & aTicket)
{
	if (aTicket == ID_T_INVALID(SPath))
	{
		aTicket = myPaths.Acquire();
	}

	SPath& path = *myPaths.GetObj(aTicket);
	path.myState = EPathState_Searching;
	++path.myRequestCount;

	SPathRequest request;
	request.myStartPosition = aStartPosition;
	request.myEndPosition = aEndPosition;
	request.myTicket = aTicket;

	myWorkerPool.DoWork([request, this]()
	{
		START_TIMER(pathfindingTime);
		SPath& path = *myPaths.GetObj(request.myTicket);

		path.myState = EPathState_Searching;
		path.myPathPoints.RemoveAll();

		PathPoints pathPoints(128);
		int startIndex = -1;
		int endIndex = -1;

		CommonUtilities::Vector3f contactPoint;
		myNavmesh.RayHitLocation(
			request.myStartPosition + CommonUtilities::Vector3f::Up,
			{ 0.f, -1.f, 0.f },
			contactPoint,
			startIndex
		);
		myNavmesh.RayHitLocation(
			request.myEndPosition + CommonUtilities::Vector3f::Up,
			{ 0.f, -1.f, 0.f },
			contactPoint,
			endIndex
		);

		if (startIndex == -1 || endIndex == -1)
		{
			path.myState = EPathState_NotFound;
			--path.myRequestCount;
			return;
		}

		if (startIndex == endIndex)
		{
			path.myPathPoints.Add(request.myEndPosition);
			path.myState = EPathState_Found;
			--path.myRequestCount;
			return;
		}

		CommonUtilities::GrowingArray<int, int> pathIndices(512);
		myMutex.lock();
		if (!myNavmesh.FindPath(startIndex, endIndex, pathIndices, path.myRequestCount))
		{
			path.myState = EPathState_NotFound;
			--path.myRequestCount;
			myMutex.unlock();
			return;
		}
		myMutex.unlock();

		CommonUtilities::GrowingArray<CNavMesh::SPortal, int> portals(128);
		portals.Add({ request.myEndPosition, request.myEndPosition });
		myNavmesh.FindPortals(pathIndices, portals);
		portals.Add({ request.myStartPosition, request.myStartPosition });

		myNavmesh.SimpleStupidFunnelAlgorithm(portals, pathPoints);

		path.myPathPoints = pathPoints;

		float length = 0.f;
		for (int i = 0; i < pathPoints.Size()-1; ++i)
		{
			length += (pathPoints[i] - pathPoints[i + 1]).Length();
		}
		path.myPathLength = length;
		//GENERAL_LOG(CONCOL_DEFAULT, "found path | time %f ms | approximate length %f units", GET_TIME(pathfindingTime), length);

		path.myState = EPathState_Found;
		--path.myRequestCount;
	});
}

void CPathFinder::ReturnTicket(PathTicket& aTicket)
{
	myPaths.Release(aTicket);
	aTicket = ID_T_INVALID(SPath);
}

void CPathFinder::FinishedUsingPath(PathTicket & aTicket)
{
	if (aTicket == ID_T_INVALID(SPath))
	{
		return;
	}
	SPath* path = myPaths.GetObj(aTicket);
	if (path)
	{
		path->myState = EPathState_None;
	}
}

EPathState CPathFinder::GetPathState(PathTicket aTicket)
{
	if (aTicket == ID_T_INVALID(SPath))
	{
		return EPathState_None;
	}
	SPath* path = myPaths.GetObj(aTicket);
	if (path == nullptr)
	{
		return EPathState_None;
	}

	return path->myState;
}

PathPoints & CPathFinder::GetPath(PathTicket aTicket)
{
	return myPaths.GetObj(aTicket)->myPathPoints;
}

float CPathFinder::GetPathLength(PathTicket aTicket)
{
	return myPaths.GetObj(aTicket)->myPathLength;
}

void CPathFinder::DeactivateTriangles(const CommonUtilities::Vector3f & aPosition)
{
	constexpr float r = 3.f;
	constexpr float r2 = r*r;

	for (int i = 0; i < myNavmesh.myTriangles.size(); ++i)
	{
		if ((myNavmesh.GetCenterOfTriangle(i) - aPosition).Length2() < r2)
		{
			myNavmesh.myNodes[i].myIsActive = false;
		}
	}
}

void CPathFinder::ActivateTriangles(const CommonUtilities::Vector3f & aPosition)
{
	constexpr float r = 3.f;
	constexpr float r2 = r*r;

	for (int i = 0; i < myNavmesh.myTriangles.size(); ++i)
	{
		if ((myNavmesh.GetCenterOfTriangle(i) - aPosition).Length2() < r2)
		{
			myNavmesh.myNodes[i].myIsActive = true;
		}
	}
}

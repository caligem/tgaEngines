#pragma once

#include "NavMesh.h"

#include "ObjectPool.h"
#include "WorkerPool.h"

using PathTicket = ID_T(SPath);

class CPathFinder
{
public:
	CPathFinder();
	~CPathFinder();

	void LoadNavmesh(const char* aNavmeshFile);

	bool RayHitLocation(const CommonUtilities::Vector3f& aRayOrigin, const CommonUtilities::Vector3f& aRayDirection, CommonUtilities::Vector3f& aContactPoint);
	void DebugRender();
	void DebugRenderPath(PathTicket& aTicket);
	void DebugRenderPath(PathPoints& aPath);

	void ClearRequests();

	void RequestPath(const CommonUtilities::Vector3f& aStartPosition, const CommonUtilities::Vector3f& aEndPosition, PathTicket& aTicket);
	void ReturnTicket(PathTicket& aTicket);
	void FinishedUsingPath(PathTicket& aTicket);

	EPathState GetPathState(PathTicket aTicket);
	PathPoints& GetPath(PathTicket aTicket);
	float GetPathLength(PathTicket aTicket);

	void DeactivateTriangles(const CommonUtilities::Vector3f& aPosition);
	void ActivateTriangles(const CommonUtilities::Vector3f& aPosition);

	bool IsThereAPath(const CommonUtilities::Vector3f& aSource, const CommonUtilities::Vector3f& aDestination, float& aOutLength);

private:
	friend class CEngine;

	struct SPathRequest
	{
		CommonUtilities::Vector3f myStartPosition;
		CommonUtilities::Vector3f myEndPosition;
		PathTicket myTicket;
	};

	bool Init();
	CNavMesh myNavmesh;

	ObjectPool<SPath> myPaths;

	CWorkerPool myWorkerPool;

	std::mutex myMutex;
};


#pragma once

constexpr int PoolSizeComponentSystemComponentStorages = 8;

//Component storage pools
constexpr int PoolSizeModelComponent = 4096;
constexpr int PoolSizeCameraComponent = 16;
constexpr int PoolSizeAnimationControllerComponent = 512;
constexpr int PoolSizeAudioSourceComponent = 128;
constexpr int PoolSizeAudioListenerComponent = 16;
constexpr int PoolSizeLightComponent = 1024;
constexpr int PoolSizeParticleSystemComponent = 1024;
constexpr int PoolSizeStreakComponent = 512;
constexpr int PoolSizeSpriteComponent = 512;
constexpr int PoolSizeTextComponent = 128;
constexpr int PoolSizeCanvasComponent = 64;
constexpr int PoolSizeButtonComponent = 64;
constexpr int PoolSizeSliderComponent = 64;
constexpr int PoolSizeScriptComponent = 64;
constexpr int PoolSizeVolumetricFogComponent = 4; // 64

constexpr int PoolSizeComponentStorageGameObjectData = 8192;

//Managers
constexpr int PoolSizeParticleManagerParticleEmitters = PoolSizeParticleSystemComponent;
constexpr int PoolSizeParticleManagerStreaks = PoolSizeStreakComponent;
constexpr int PoolSizeTextureManagerTextures = 8192;
constexpr int PoolSizeCameraManagerCameras = PoolSizeCameraComponent;
constexpr int PoolSizeSceneManagerScenes = PoolSizeComponentSystemComponentStorages;
constexpr int PoolSizeSpriteManagerSprites = PoolSizeSpriteComponent;
constexpr int PoolSizeTextManagerFonts = 2;
//constexpr int PoolSizeScriptManagerScripts 1024 it's in ScriptManager's Constructor becuase Script Project can't find Engine
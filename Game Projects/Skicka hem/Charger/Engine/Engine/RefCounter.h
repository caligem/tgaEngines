#pragma once
class RefCounter
{
public:
	RefCounter()
	{
		myCounter = 0;
	}

	~RefCounter() = default;

	unsigned short GetRefCount() { return myCounter; }

	void IncrementCounter() { myCounter++; }
	void DecrementCounter() { myCounter--; }

private:
	unsigned short myCounter;
};


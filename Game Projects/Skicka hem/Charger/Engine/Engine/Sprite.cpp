#include "stdafx.h"
#include "Sprite.h"

#include "IEngine.h"

#include "DDSTextureLoader.h"

#include "DL_Debug.h"
#include "DXMacros.h"

CSprite::CSprite()
{
}

CSprite::~CSprite()
{
}

void CSprite::Init(const std::string & aFilename)
{
	myTexture = IEngine::GetTextureManager().CreateTextureFromFile(aFilename);
	myOriginalTextureSize = CTextureBuilder::GetTextureSize(IEngine::GetTextureManager().GetTexture(myTexture));
}

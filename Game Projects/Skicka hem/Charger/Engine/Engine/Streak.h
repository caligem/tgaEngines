#pragma once

class CParticleRenderer;

class CStreak
{
public:
	CStreak();
	~CStreak();

	bool Init(const char* aTexture);

private:
	friend CParticleRenderer;

	SRV myTexture;
};


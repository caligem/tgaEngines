#include "stdafx.h"
#include "StreakComponent.h"

#include "ParticleManager.h"

#include "IEngine.h"
#include "Mathf.h"

#include "IWorld.h"

CParticleManager* CStreakComponent::ourParticleManager = nullptr;

CStreakComponent::CStreakComponent()
{
}

CStreakComponent::~CStreakComponent()
{
}

void CStreakComponent::Init(const char * aTexture, const CommonUtilities::Vector4f & aStartColor, const CommonUtilities::Vector4f & aEndColor, float aStartSize, float aEndSize, float aTime, float aMinVertexDistance)
{
	myStreakID = ourParticleManager->AcquireStreak(aTexture);

	myStartColor = aStartColor;
	myEndColor = aEndColor;

	myStartSize = aStartSize;
	myEndSize = aEndSize;

	myTime = aTime;
	myMinVertexDistance = aMinVertexDistance;

	myPoints.Init(512);
	myProperties.Init(512);
}


void CStreakComponent::Init(const SComponentData & aComponentData)
{
	myStreakID = ourParticleManager->AcquireStreak(aComponentData.myTexture);

	myPoints.Init(512);
	myProperties.Init(512);
}

void CStreakComponent::Update(CommonUtilities::Vector3f aPosition)
{
	myLeadingPoint.myPosition = { aPosition.x, aPosition.y, aPosition.z, 0.f };

	if ( myPoints.Empty() || (myLeadingPoint.myPosition - myLastSolidifiedPosition).Length() > myMinVertexDistance )
	{
		EmitParticle();
	}

	UpdatePointData();
	RemoveDeadPoints();
}

void CStreakComponent::ClearStreak()
{
	myPoints.RemoveAll();
	myProperties.RemoveAll();
}

void CStreakComponent::EmitParticle()
{
	myPoints.Add({ myLeadingPoint.myPosition, {1.f, 1.f, 1.f, 1.f}, {1.f, 1.f} });
	myLastSolidifiedPosition = myLeadingPoint.myPosition;

	myProperties.EmplaceBack();
	myProperties.GetLast().myLifetime = 0.f;
	myProperties.GetLast().myMaxTime = myTime;
}

void CStreakComponent::Release()
{
	ourParticleManager->ReleaseStreak(myStreakID);
	myStreakID = ID_T_INVALID(CStreak);
}

void CStreakComponent::UpdatePointData()
{
	if (myPoints.Empty())return;

	float dt = IEngine::Time().GetDeltaTime();

	float totalLength = 0.f;
	for (unsigned short i = 0; i < myPoints.Size() - 1; ++i)
	{
		totalLength += (myPoints[i].myPosition - myPoints[i + 1].myPosition).Length();
	}
	totalLength += (myPoints.GetLast().myPosition - myLeadingPoint.myPosition).Length();

	myBounds.SetMinMax(myLeadingPoint.myPosition, myLeadingPoint.myPosition);

	float currentLength = 0.f;
	for (unsigned short i = 0; i < myPoints.Size(); ++i)
	{
		myProperties[i].myLifetime += dt;

		float alpha = myProperties[i].myLifetime / myProperties[i].myMaxTime;

		myPoints[i].mySize.x = currentLength / totalLength;
		myPoints[i].mySize.y = CommonUtilities::Lerp(myStartSize, myEndSize, alpha);
		myPoints[i].myColor = CommonUtilities::Lerp<CommonUtilities::Vector4f>(myStartColor, myEndColor, alpha);

		if (i < myPoints.Size() - 1)
		{
			currentLength += (myPoints[i].myPosition - myPoints[i + 1].myPosition).Length();
		}

		myBounds.EncapsulateWithoutRecalculating(myPoints[i].myPosition);
	}

	myBounds.Inflate(myEndSize*1.41421356f); //sqrt(2)

	myLeadingPoint.mySize.x = 1.f;
	myLeadingPoint.mySize.y = myStartSize;
	myLeadingPoint.myColor = myStartColor;
}

void CStreakComponent::RemoveDeadPoints()
{
	for (unsigned short i = myPoints.Size(); i > 0; --i)
	{
		if (myProperties[i - 1].myLifetime > myProperties[i - 1].myMaxTime)
		{
			myPoints.RemoveAtIndex(i - 1);
			myProperties.RemoveAtIndex(i - 1);
		}
	}
}

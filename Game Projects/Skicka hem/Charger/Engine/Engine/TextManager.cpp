#include "stdafx.h"
#include "TextManager.h"


CTextManager::CTextManager()
	: myFonts(PoolSizeTextManagerFonts)
{
}

CTextManager::~CTextManager()
{
}

bool CTextManager::Init()
{
	return true;
}

ID_T(CFont) CTextManager::AcquireFont(const char * aFontPath)
{
	std::string fontPath;

	if (aFontPath == nullptr)
	{
		fontPath = "";
	}
	else
	{
		fontPath = aFontPath;
	}

	if (myFontCache.find(fontPath) != myFontCache.end())
	{
		return myFontCache[fontPath];
	}
	else
	{
		ID_T(CFont) fontID = myFonts.Acquire();
		myFontCache[fontPath] = fontID;

		myFonts.GetObj(fontID)->Init(fontPath);

		return std::move(fontID);
	}
}

void CTextManager::ReleaseFont(ID_T(CFont) aFontID)
{
	aFontID;
}

#include "stdafx.h"
#include "TextureManager.h"

#include "TextureBuilder.h"

#include "IEngine.h"
#include "FileWatcher.h"

#include "DXMacros.h"

CTextureManager::CTextureManager()
	: myTextures(PoolSizeTextureManagerTextures)
{
}

CTextureManager::~CTextureManager()
{
	for (auto texture : myTextures)
	{
		SafeRelease(texture.ptr);
	}
}

void CTextureManager::Init()
{
	myCache["0*"] = myTextures.Acquire();
	(*(myTextures.GetObj(myCache["0*"]))) = CTextureBuilder::CreateGrayCheckerBoardTexture();
	myCache["1*"] = myTextures.Acquire();
	(*(myTextures.GetObj(myCache["1*"]))) = CTextureBuilder::CreateWhiteTexture();
	myCache["2*"] = myTextures.Acquire();
	(*(myTextures.GetObj(myCache["2*"]))) = CTextureBuilder::CreateNormalTexture();
	myCache["3*"] = myTextures.Acquire();
	(*(myTextures.GetObj(myCache["3*"]))) = CTextureBuilder::CreateBlackTexture();
	myCache["4*"] = myTextures.Acquire();
	(*(myTextures.GetObj(myCache["4*"]))) = CTextureBuilder::CreatePinkCheckerBoardTexture();
	myCache["5*"] = myTextures.Acquire();
	(*(myTextures.GetObj(myCache["5*"]))) = CTextureBuilder::CreateRMATexture();
}

SRV CTextureManager::CreateTextureFromFile(const std::string & aFilepath)
{
	if (myCache.find(aFilepath) != myCache.end())
	{
		return myCache[aFilepath];
	}

	SRV id = myTextures.Acquire();

	auto callback = [=](const std::wstring& aFilename)
	{
		std::string fileToReload(aFilename.begin(), aFilename.end());
		(*myTextures.GetObj(id)) = CTextureBuilder::CreateTextureFromFile(fileToReload);
	};

	std::wstring file(aFilepath.begin(), aFilepath.end());
	callback(file);
	IEngine::GetFileWatcher().WatchFile(file, callback);

	myCache[aFilepath] = id;

	return id;
}

SRV CTextureManager::CreateGrayCheckerBoardTexture()
{
	return CreateTextureFromFile("0*");
}
SRV CTextureManager::CreateRMATexture()
{
	return CreateTextureFromFile("5*");
}
SRV CTextureManager::CreateWhiteTexture()
{
	return CreateTextureFromFile("1*");
}
SRV CTextureManager::CreateNormalTexture()
{
	return CreateTextureFromFile("2*");
}
SRV CTextureManager::CreateBlackTexture()
{
	return CreateTextureFromFile("3*");
}
SRV CTextureManager::CreatePinkCheckerBoardTexture()
{
	return CreateTextureFromFile("4*");
}

ID3D11ShaderResourceView* CTextureManager::GetTexture(SRV aID)
{
	if (aID == ID_T_INVALID(SSRV))
	{
		return NULL;
	}
	return (*myTextures.GetObj(aID)).ptr;
}

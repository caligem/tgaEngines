#pragma once
#include "DoubleBuffer.h"
#include "Bounds.h"
struct SSpriteRenderCommand;
struct STextRenderCommand;

class CLuaScript;

class CUIElement
{
public:
	struct SButtonTints
	{
		CommonUtilities::Vector4f myNormalTint;
		CommonUtilities::Vector4f myHighlightedTint;
		CommonUtilities::Vector4f myPressedTint;
		CommonUtilities::Vector4f myDisabledTint;
	};

	CUIElement();
	virtual ~CUIElement();

	virtual void FillRenderCommands(CDoubleBuffer<SSpriteRenderCommand>& aSpriteRenderCommandsBuffer, CDoubleBuffer<STextRenderCommand>& aTextRenderCommandsBuffer) = 0;
	virtual CUIElement* GetFocus(const CommonUtilities::Vector2f& /*aMousePosition*/);
	const CommonUtilities::CBounds& GetBounds() const { return myBounds; }

	virtual void OnMouseMove(const CommonUtilities::Vector2f& /*aMousePosition*/) {}
	virtual void OnLeave() {}
	virtual void OnEnter() {}
	virtual void OnMouseDown(const CommonUtilities::Vector2f& /*aMousePosition*/) {}
	virtual bool OnMouseUp(const CommonUtilities::Vector2f& /*aMousePosition*/) { return true; }

	void SetShouldBeRendered(bool aShouldRender) { myShouldBeRendered = aShouldRender; }

	const std::string& GetName() const { return myName; }
	void SetName(const std::string& aName) { myName = aName; }
	virtual void SetCanvasLuaScript(ID_T(CLuaScript)* aLuaScriptID) { myCanvasLuaScript = aLuaScriptID; }

protected:
	CommonUtilities::CBounds myBounds;
	std::string myName;
	ID_T(CLuaScript)* myCanvasLuaScript = nullptr;
	bool myShouldBeRendered;
};


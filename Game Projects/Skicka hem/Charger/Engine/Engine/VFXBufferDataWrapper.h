#pragma once

#include "Vector.h"

struct SVFXBufferData
{
	float myTotalTime;
	CommonUtilities::Vector3f myTrash;
	CommonUtilities::Vector3f myPlayerPosition;
	float myOtherTrash;
	CommonUtilities::Vector4f myCustomData;
};

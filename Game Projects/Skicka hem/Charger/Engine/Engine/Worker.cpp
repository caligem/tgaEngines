#include "stdafx.h"
#include "Worker.h"

#include "ThreadNamer.h"
#include <string>
#include "WorkerPool.h"
#include <random>

CWorker::CWorker()
	: myIsRunning(false)
	, myWorkerPool(nullptr)
{
}

CWorker::CWorker(const CWorker&)
	: myIsRunning(false)
	, myWorkerPool(nullptr)
{
}

CWorker::~CWorker()
{
}

void CWorker::Init(CWorkerPool * aWorkerPool)
{
	myWorkerPool = aWorkerPool;
	myIsRunning = true;
	myFunctionPtr = nullptr;
	myThread = std::thread(std::bind(&CWorker::RunThread, this));
}

void CWorker::Stop()
{
	myIsRunning = false;
	if (myThread.joinable())
	{
		myThread.join();
	}
}

void CWorker::RunThread()
{
	static int ourIndex = 0;

	SetThreadName((std::string("Worker Thread ") + std::to_string(ourIndex++)).c_str());

	while (myIsRunning)
	{
		if (myWorkerPool->GetWork(myFunctionPtr))
		{
			myFunctionPtr();
			--myWorkerPool->myNumJobsInProgress;
		}
		std::this_thread::yield();
	}
}

#pragma once

#include <mutex>
#include <functional>
#include <deque>
#include <atomic>

#include "GrowingArray.h"

#include "Worker.h"

class CWorkerPool
{
public:
	CWorkerPool();
	~CWorkerPool();

	void Init(int aNumberOfThreads);
	void Destroy();

	void DoWork(const std::function<void()>& aJob);
	void Wait();

	void RemoveAllJobs();

private:
	friend CWorker;

	bool GetWork(std::function<void()>& aFunctionPtr);

	std::deque<std::function<void()>> myJobs;

	std::mutex myMutex;
	std::atomic_int myNumJobsInProgress;

	std::vector<CWorker> myWorkers;
};


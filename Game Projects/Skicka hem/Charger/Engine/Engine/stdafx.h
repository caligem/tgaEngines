#pragma once

#define NOMINMAX

#include <Mathf.h>

#include <d3d11.h>						
#include <d3d11_1.h>

#include <algorithm>
#include <exception>
#include <malloc.h>
#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>
#include <array>

#include <DL_Debug.h>

#include <UniqueIdentifier.h>
#include <ObjectPool.h>
#include <Timer.h>

#include "ShaderManager.h"				// 17 times 
#include "ModelManager.h"				// 13 times
#include "ParticleManager.h"			// 11 times
#include "CameraManager.h"				// 10 times
#include "SpriteManager.h"				// 10 times
#include "TextManager.h"				// 10 times
#include "InputManager.h"				// 9 times

#include "ComponentSystem.h"			// 14 times
#include "DirectXFramework.h"			// 13 times
#include "TextureBuilder.h"				// 12 times
#include "WorkerPool.h"					// 11 times
#include "GraphicsPipeline.h"			// 8 times (but compiles all 7 renderers this many times, so is pretty heavy)

#include "TextureManager.h"

#include "PoolSizes.h"

////(dessa g�r ingen st�rre skillnad i nul�get, men kanske l�ngre fram..)

//#include <math.h>				// >= 6 times
//#include <iostream>			// >= 9 times
//#include <fstream>			// >= 6 times
//#include <stdio.h>			// >= 5 times
//#include <stdint.h>			// >= 7 times
//#include <atomic>				// >= 4 times
//#include <mutex>				// >= 4 times
//#include <thread>				// >= 3 times
//#include <functional>			// >= 6 times
//#include <stdlib.h>			// >= 6 times
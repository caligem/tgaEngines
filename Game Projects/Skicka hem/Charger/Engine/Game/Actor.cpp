#include "stdafx.h"
#include "Actor.h"
#include "CameraDataWrapper.h"


CMetrics* CActor::ourMetrics = nullptr;
CProjectileManager* CActor::ourProjectileManager = nullptr;

CActor::CActor()
{
	myController = nullptr;
	myState = EIdle;
	myShouldBeRemoved = false;
	myHealthBar = nullptr;
	myScreenHealthBarForgivness = 0.1f;
}


CActor::~CActor()
{
	delete myController;
	myController = nullptr;
}

void CActor::BindMetricsPtr(CMetrics& aMetrics)
{
	ourMetrics = &aMetrics;

	CController::BindMetricsPtr(aMetrics);
}

void CActor::BindProjectileManagerPtr(CProjectileManager & aProjectileManager)
{
	ourProjectileManager = &aProjectileManager;
}

CController * CActor::GetController()
{
	return myController;
}

void CActor::UpdateHealthBar()
{
	if (!myHealthBar)
	{
		return;
	}

	if (!CHealth::GetIsDead())
	{
		CommonUtilities::Matrix44f viewProjection = IWorld::GetSavedCameraBuffer().myViewProjection;
		CommonUtilities::Vector4f pos = GetTransform().GetPosition();
		pos.w = 1.f;
		pos.y += 2.5f;

		CommonUtilities::Vector4f viewPos = pos * viewProjection;
		viewPos /= viewPos.w;
		viewPos.x = (viewPos.x + 1.f) / 2.f;
		viewPos.y = (-viewPos.y + 1.f) / 2.f;
		
		if ((viewPos.x < 0.f - myHealthBarOffset || viewPos.x > 1.f + myHealthBarOffset) || (viewPos.y < 0.f - myHealthBarOffset || viewPos.y > 1.f + myHealthBarOffset))
		{
			myHealthBar->SetShouldBeRendered(false);
			myHealthBarBackground->SetShouldBeRendered(false);
		}
		else
		{
			myHealthBarBackground->SetShouldBeRendered(true);
			myHealthBar->SetShouldBeRendered(true);
			myHealthBar->SetPosition({ viewPos.x - myHealthBarOffset, viewPos.y });
			myHealthBarBackground->SetPosition({ viewPos.x, viewPos.y });
		}
		myHealthBar->SetScale({ CHealth::GetCurrentHealthInPercent() , 1.f });
	}
	else
	{
		myHealthBar->SetShouldBeRendered(false);
		myHealthBarBackground->SetShouldBeRendered(false);
	}
}


void CActor::AddHealthBar(CCanvasComponent * aCanvasComponent)
{
	if (aCanvasComponent == nullptr)
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "HealthBar Canvas was Null when trying to add Healthbar");
		return;
	}
	
	myHealthBarBackground = aCanvasComponent->AddUIElement<CSpriteComponent>({ "Assets/Sprites/InGame/enemyHealthBarBackground.dds" });
	myHealthBarBackground->SetPivot({ 0.5f, 0.5f });
	myHealthBar = aCanvasComponent->AddUIElement<CSpriteComponent>({ "Assets/Sprites/InGame/enemyHealthBar.dds" });
	myHealthBar->SetPivot({ 0.f, 0.5f });
	myHealthBar->SetUVScale({ 0.47f, 0.99f });
	myHealthBar->SetUVOffset({ 0.f, 0.f });
	myHealthBarOffset = 0.02275f;
}

void CActor::SetStartTransform(const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector3f & aRotation, const CommonUtilities::Vector3f & aScale)
{
	myStartPosition = aPosition;
	myStartRotation = aRotation;
	myStartScale = aScale;
	ResetTransform();
}

void CActor::SetPosition(const CommonUtilities::Vector3f & aPosition)
{
	GetTransform().SetPosition(aPosition);
}

void CActor::UpdateOrientationAndRotationData(const SSteeringOutput& aSteeringOutput)
{
	mySteeringData.myOrientation = aSteeringOutput.myAngularOutput;

	if (mySteeringData.myRotation > mySteeringData.myMaxRotation)
	{
		mySteeringData.myRotation = mySteeringData.myMaxRotation;
	}
	else if (mySteeringData.myRotation > -mySteeringData.myMaxRotation)
	{
		mySteeringData.myRotation = -mySteeringData.myMaxRotation;
	}
}

void CActor::UpdateMovementData(const SSteeringOutput& aSteeringOutput)
{
	mySteeringData.myVelocity = aSteeringOutput.myLinearOutput;

	if (mySteeringData.myVelocity.Length() > mySteeringData.myMaxSpeed)
	{
		mySteeringData.myVelocity = mySteeringData.myVelocity.GetNormalized() * mySteeringData.myMaxSpeed;
	}
}

const EState& CActor::GetCurrentState() const
{
	return myState;
}

void CActor::ResetTransform()
{
	GetTransform().SetPosition(myStartPosition);
	GetTransform().SetRotation(myStartRotation);
	GetTransform().SetScale(myStartScale);
	mySteeringData.myPosition = myStartPosition;
}

void CActor::SetWasHitProjectile()
{
	myProjectileDamageHandler.SetWasHitThisFrame();
}

void CActor::SetWasHitPlayerAoE()
{
	myPlayerAoEDamageHandler.SetWasHitThisFrame();
}

void CActor::SetWasHitCompanionAoE()
{
	myCompanionAoEDamageHandler.SetWasHitThisFrame();
}

void CActor::SetWasHitCompanionBasicAttack()
{
	myCompanionBasicAttackDamageHandler.SetWasHitThisFrame();
}

void CActor::SetWasHitPlayerDash()
{
	myPlayerDashDamageHandler.SetWasHitThisFrame();
}

void CActor::ResetDamageHandlers()
{
	myProjectileDamageHandler.Reset();
	myPlayerDashDamageHandler.Reset();
	myPlayerAoEDamageHandler.Reset();
	myCompanionAoEDamageHandler.Reset();
	myCompanionBasicAttackDamageHandler.Reset();
}

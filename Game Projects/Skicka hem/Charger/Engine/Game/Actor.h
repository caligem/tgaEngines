#pragma once
#include "GameObject.h"
#include "Controller.h"
#include "Vector.h"
#include "Health.h"
#include "ProjectileDamageHandler.h"
#include "AoEDamageHandler.h"
#include "OneHitDamageHandler.h"

class CProjectileManager;
class CPollingStation;
class CMetrics;

class CActor :	public CGameObject, public CHealth
{
public:
	CActor();
	~CActor();
	static void BindMetricsPtr(CMetrics& aMetrics);
	static void BindProjectileManagerPtr(CProjectileManager& aProjectileManager);

	virtual void Init(ID_T(CScene) aSceneID, CController* aController, int aAccessID, ID_T(CGameObjectData) aParentID = ID_T_INVALID(CGameObjectData)) = 0;
	//virtual void Update() = 0;
	CController* GetController();

	void UpdateHealthBar();
	void AddHealthBar(CCanvasComponent* aCanvasComponent);

	void SetStartTransform(const CommonUtilities::Vector3f& aPosition, const CommonUtilities::Vector3f& aRotation, const CommonUtilities::Vector3f& aScale);
	virtual void SetPosition(const CommonUtilities::Vector3f &aPosition);
	void UpdateOrientationAndRotationData(const SSteeringOutput& aSteeringOutput);
	void UpdateMovementData(const SSteeringOutput& aSteeringOutput);
	virtual void Respawn() = 0;
	const EState& GetCurrentState() const;

	inline const bool ShouldBeRemoved() const {	return myShouldBeRemoved; }

	void SetWasHitProjectile();
	void SetWasHitPlayerAoE();
	void SetWasHitCompanionAoE();
	void SetWasHitCompanionBasicAttack();
	void SetWasHitPlayerDash();

	void ResetDamageHandlers();

protected:
	void ResetTransform();
	CController* myController;
	SSteeringData mySteeringData;

	CommonUtilities::Vector3f myStartPosition;
	CommonUtilities::Vector3f myStartRotation;
	CommonUtilities::Vector3f myStartScale;

	EState myState;

	static CMetrics* ourMetrics;
	static CProjectileManager* ourProjectileManager;

	CSpriteComponent* myHealthBar;
	CSpriteComponent* myHealthBarBackground;
	float myHealthBarOffset;
	float myScreenHealthBarForgivness;

	CProjectileDamageHandler myProjectileDamageHandler;
	COneHitDamageHandler myPlayerDashDamageHandler;
	CAoEDamageHandler myPlayerAoEDamageHandler;
	CAoEDamageHandler myCompanionAoEDamageHandler;
	CAoEDamageHandler myCompanionBasicAttackDamageHandler;

	CAnimationControllerComponent* myAnimationController;

	float mySpeed;
	bool myShouldBeRemoved;
};


#include "stdafx.h"
#include "AoEDamageHandler.h"


CAoEDamageHandler::CAoEDamageHandler()
	: myMaxMsBetweenDamage(),
	myElapsedMsSinceLastDamage(myMaxMsBetweenDamage),
	myWasHitThisFrame(false)
{
}

CAoEDamageHandler::~CAoEDamageHandler()
{
}

CAoEDamageHandler& CAoEDamageHandler::operator=(const CAoEDamageHandler & aRhs)
{
	myMaxMsBetweenDamage = aRhs.myMaxMsBetweenDamage;
	myElapsedMsSinceLastDamage = aRhs.myElapsedMsSinceLastDamage;
	myWasHitThisFrame = aRhs.myWasHitThisFrame;

	return *this;
}

void CAoEDamageHandler::Update()
{
	if (myElapsedMsSinceLastDamage < myMaxMsBetweenDamage)
	{
		std::chrono::milliseconds elapsedMs =
			std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::system_clock::now() - myStartTime);

		myElapsedMsSinceLastDamage = elapsedMs;
	}

	myWasHitThisFrame = false;
}

void CAoEDamageHandler::SetMaxMs(const int aMaxMs)
{
	myMaxMsBetweenDamage = std::chrono::milliseconds(aMaxMs);
	myElapsedMsSinceLastDamage = myMaxMsBetweenDamage;
}

void CAoEDamageHandler::SetStartTime()
{
	myStartTime = std::chrono::system_clock::now();
}

void CAoEDamageHandler::ResetElapsedMsSinceLastDamage()
{
	myElapsedMsSinceLastDamage = std::chrono::milliseconds(0);
}

void CAoEDamageHandler::SetWasHitThisFrame()
{
	if (myElapsedMsSinceLastDamage < myMaxMsBetweenDamage)
	{
		return;
	}
	else
	{
		myWasHitThisFrame = true;

		if (myElapsedMsSinceLastDamage >= myMaxMsBetweenDamage)
		{
			ResetElapsedMsSinceLastDamage();
			SetStartTime();
		}

		//ResetElapsedMsSinceLastDamage();
		//SetStartTime();
		return;
	}
}

void CAoEDamageHandler::Reset()
{
	ResetElapsedMsSinceLastDamage();
	myWasHitThisFrame = false;
}

const std::chrono::milliseconds& CAoEDamageHandler::GetElapsedMsSinceLastDamage() const
{
	return myElapsedMsSinceLastDamage;
}

const std::chrono::milliseconds& CAoEDamageHandler::GetMaxMsBetweenDamage() const
{
	return myMaxMsBetweenDamage;
}


bool CAoEDamageHandler::GetWasHit() const
{
	return myWasHitThisFrame;
}


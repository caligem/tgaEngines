#pragma once

class CAoEDamageHandler
{
public:
	~CAoEDamageHandler();
	CAoEDamageHandler();

	CAoEDamageHandler& CAoEDamageHandler::operator=(const CAoEDamageHandler& aRhs);

	void Update();

	void SetMaxMs(const int aMaxMs);
	void SetStartTime();
	void ResetElapsedMsSinceLastDamage();
	void SetWasHitThisFrame();
	void Reset();

	const std::chrono::milliseconds& GetElapsedMsSinceLastDamage() const;
	const std::chrono::milliseconds& GetMaxMsBetweenDamage() const;
	bool GetWasHit() const;

private:
	std::chrono::time_point <std::chrono::system_clock> myStartTime;
	std::chrono::milliseconds myElapsedMsSinceLastDamage;
	std::chrono::milliseconds myMaxMsBetweenDamage;
	bool myWasHitThisFrame;
};


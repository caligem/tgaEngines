#include "stdafx.h"
#include "AreaOfEffect.h"
#include "PollingStation.h"

#include "Random.h"

CMetrics* CAreaOfEffect::ourMetrics = nullptr;

CAreaOfEffect::CAreaOfEffect()
	: myIsStarted(false), myHasTimer(false)
{
}


CAreaOfEffect::~CAreaOfEffect()
{
}

void CAreaOfEffect::BindMetricsPtr(CMetrics& aMetrics)
{
	ourMetrics = &aMetrics;
}

void CAreaOfEffect::Init(ID_T(CScene) aSceneID, const bool & aHasTimer, char * aModelPath, const std::function<void()>& aFunctionToCallWhenDone, const float & aMaxActiveTime, bool aShouldBeRemovedAfterTimer)
{
	CGameObject::Init(aSceneID);
	
	for (CGameObject& pool : myPools)
	{
		pool.Init(aSceneID);
		pool.AddComponent<CModelComponent>({ aModelPath })->GetMaterial().SetShouldRenderDepth(false);
		pool.GetTransform().SetParent(&GetTransform());
		/*
		pool.GetTransform().SetPosition({
			CommonUtilities::Random() * 5.f-2.5f,
			0.f,
			CommonUtilities::Random() * 5.f-2.5f
		});
		*/
		pool.GetTransform().SetScale({0.f, 0.f, 0.f });
	}

	SetActive(false);

	myMaxActiveTime = aMaxActiveTime;
	myHasTimer = aHasTimer;

	myIsPersistent = aShouldBeRemovedAfterTimer;

	auto lambda = [=]()
	{
		if (aShouldBeRemovedAfterTimer)
		{
			SetActive(false);
		}
		if (aFunctionToCallWhenDone != nullptr)
		{
			aFunctionToCallWhenDone();
		}
	};

	if (aHasTimer)
	{
		myCountDown.Set(myMaxActiveTime, Countdown::type::oneshot, lambda);
	}
}

void CAreaOfEffect::Update()
{
	if (myHasTimer && myIsStarted)
	{
		myCountDown.Update();

		for (CGameObject& pool : myPools)
		{
			if (!pool.IsValid())
			{
				continue;
			}
			float f = myCountDown.GetPercent();

			float a = 2.f;
			float b = 10.f;
			float c = 64.f;

			if (!myIsPersistent)
			{
				f = (1.f - std::powf(std::powf(1.f - f, a), b));
			}
			else
			{
				f = (1.f - std::powf(std::powf(1.f - f, a), b)) * (1.f - std::powf(f, c));
			}

			float s = ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myAoECollisionDistance;

			pool.GetTransform().SetScale({
				f * s,
				f,
				f * s
			});
		}

		if (myCountDown.GetIsFinished())
		{
			myIsStarted = false;
			myCountDown.Reset();
		}
	}
}

void CAreaOfEffect::Start()
{
	if (myHasTimer)
	{
		myCountDown.Start();
		myIsStarted = true;

		for (CGameObject& pool : myPools)
		{
			/*
			pool.GetTransform().SetPosition({
				CommonUtilities::Random() * 5.f-2.5f,
				0.f,
				CommonUtilities::Random() * 5.f-2.5f
			});
			*/
			pool.GetTransform().SetScale({0.f, 0.f, 0.f });
		}
	}
}

void CAreaOfEffect::Reset()
{
	SetActive(false);
	myIsStarted = false;

	if (myHasTimer)
	{
		myCountDown.Reset();
	}
}

bool CAreaOfEffect::GetIsActive() const
{
	return (myIsStarted == myHasTimer); //if we have a timer, myIsStarted has to be true, otherwise, both == false
}


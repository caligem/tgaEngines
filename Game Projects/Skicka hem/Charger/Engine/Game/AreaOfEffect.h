#pragma once
#include "GameObject.h"
#include "Countdown.h"
#include <iostream>

class CMetrics;

class CAreaOfEffect : public CGameObject
{
public:
	CAreaOfEffect();
	~CAreaOfEffect();

	CAreaOfEffect& CAreaOfEffect::operator=(const CAreaOfEffect& aRhs)
	{
		myCountDown = aRhs.myCountDown;
		myMaxActiveTime = aRhs.myMaxActiveTime;
		myIsStarted = aRhs.myIsStarted;
		myHasTimer = aRhs.myHasTimer;
		myPools = aRhs.myPools;
		PointToObject(aRhs);
		return *this;
	}

	static void BindMetricsPtr(CMetrics& aMetrics);

	void Init(ID_T(CScene) aSceneID, 
		const bool& aHasTimer, 
		char* aModelPath, 
		const std::function<void()>& aFunctionToCallWhenDone = nullptr, 
		const float& aMaxActiveTime = 0,
		bool aShouldBeRemovedAfterTimer = false
	);

	void Update();
	void Start();
	void Reset();

	bool GetIsActive() const;

private:
	Countdown myCountDown;
	float myMaxActiveTime;
	bool myIsStarted;
	bool myHasTimer;
	bool myIsPersistent = false;

	std::array<CGameObject, 1> myPools;

	static CMetrics* ourMetrics;
};


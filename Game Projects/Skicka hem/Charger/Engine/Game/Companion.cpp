#include "stdafx.h"
#include "Companion.h"
#include "IWorld.h"
#include "Mathf.h"
#include <iostream>
#include "PollingStation.h"
#include "AreaOfEffect.h"
#include "PollingStation.h"
#include "Random.h"
#include "Actor.h"
#include "Exploder.h"

#include "AudioManager.h"

CMetrics* CCompanion::ourMetrics = nullptr;

CCompanion::CCompanion()
{
}


CCompanion::~CCompanion()
{
}

void CCompanion::BindMetricsPtr(CMetrics& aMetrics)
{
	ourMetrics = &aMetrics;
}

void CCompanion::Init(ID_T(CScene) aSceneID, CController* aController, int aAccessID, ID_T(CGameObjectData) aParentID)
{
	CGameObject::Init(aSceneID, aAccessID, aParentID);
	CGameObject childModel;
	childModel.Init(aSceneID);
	childModel.GetTransform().SetParent(&GetTransform());
	CModelComponent* modelComponent = childModel.AddComponent<CModelComponent>({ "Assets/Models/companion/companion.fbx" });
	childModel.AddComponent<CStreakComponent>({ "Assets/Particles/Sprites/roundParticle.dds" });

	myAnimationController = childModel.AddComponent<CAnimationControllerComponent>(modelComponent);

	childModel.GetTransform().SetPosition({0.f, 1.f, 0.f});
	
	mySteeringData.myMaxAcceleration = ourMetrics->GetCompanionMetrics().myMaxAcceleration;
	mySteeringData.myMaxSpeed = ourMetrics->GetCompanionMetrics().myMaxSpeed;
	mySteeringData.myMaxAngularAccelleration = 0.7f;
	mySteeringData.myMaxRotation = 1.5f;
	mySteeringData.myAttackRange = ourMetrics->GetCompanionMetrics().myAttackRange;
	mySteeringData.myAggroRange = 9999.f;
	mySteeringData.myTargetRangeForArrive = ourMetrics->GetCompanionMetrics().myMinDistanceToPlayer;
	mySteeringData.mySlowDownRadius = ourMetrics->GetCompanionMetrics().mySlowDownRadius;

	myController = aController;

	mySteeringData.myPosition = GetTransform().GetPosition();
	myIsCleaningUp = false;
	myIsBasicAttacking = false;
	myHasUsedUltimate = false;
	myCanUltimate = true;

	auto lambda = [=]()
	{
		myCanUltimate = true;
	};

	myUltimateAoECountDown.Set(ourMetrics->GetCompanionMetrics().myUltimateMetrics.myCooldown, Countdown::type::oneshot, lambda);
	myUltimateAoECountDown.SetIsFinished();

	myUltiExplotion = AddComponent<CParticleSystemComponent>("Assets/Particles/companionExplotion.json");
	myUltiExplotion->Stop();
}

void CCompanion::Update()
{
	//for update if they change in doc
	//TODO: REMOVE
	mySteeringData.myAttackRange = ourMetrics->GetCompanionMetrics().myAttackRange;
	mySteeringData.myTargetRangeForArrive = ourMetrics->GetCompanionMetrics().myMinDistanceToPlayer;
	mySteeringData.mySlowDownRadius = ourMetrics->GetCompanionMetrics().mySlowDownRadius;
	mySteeringData.myMaxSpeed = ourMetrics->GetCompanionMetrics().myMaxSpeed;
	mySteeringData.myMaxAcceleration = ourMetrics->GetCompanionMetrics().myMaxAcceleration;
	//--------

	if (myController == nullptr)
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Companion-controller was nullptr");
		return;
	}

	if (myIsCleaningUp)
	{
		float animationTimeNextFrame = (myAnimationController->GetTime() + IWorld::Time().GetDeltaTime()) * myAnimationController->GetCurrentAnimationTicksPerSecond();
		float animationMaxTime = myAnimationController->GetCurrentAnimationDuration();
		if (animationTimeNextFrame >= animationMaxTime)
		{
			if (myAcidToCleanUp)
			{
				CPollingStation::GetInstance().RemoveExploderAoE(myAcidToCleanUp);
			}
			myIsCleaningUp = false;
		}
		else
		{
			return;
		}
	}

	if (myIsBasicAttacking)
	{
		float animationTimeNextFrame = (myAnimationController->GetTime() + IWorld::Time().GetDeltaTime()) * myAnimationController->GetCurrentAnimationTicksPerSecond();
		float animationMaxTime = myAnimationController->GetCurrentAnimationDuration();
		
		if (animationTimeNextFrame >= animationMaxTime)
		{
			myIsBasicAttacking = false;
		}
		else
		{
			return;
		}
	}

	auto steeringOutput = myController->Update(mySteeringData);

	UpdateMovement(steeringOutput);

	if (steeringOutput.myOutputState == EState::EUltimateAoE)
	{
		UltimateAoE();
	}
	else if (steeringOutput.myOutputState == EState::EAcidCleanup)
	{
		AcidCleanup();
	}
	else if (steeringOutput.myOutputState == EState::EAttacking)
	{
		Melee();
	}
	else if (steeringOutput.myOutputState == EState::EFollowPlayer)
	{
		if (myAnimationController->GetAnimation() != "companionWalk")
		{
			myAnimationController->SetAnimation("companionWalk");
		}
	}
	else if (steeringOutput.myOutputState == EState::EIdle)
	{
		if (myAnimationController->GetAnimation() != "companionWalk")
		{
			myAnimationController->SetAnimation("companionWalk");
		}
		mySteeringData.myVelocity = { 0.f, 0.f, 0.f };
	}

	myUltimateAoECountDown.Update();

	mySteeringData.myOrientation = steeringOutput.myAngularOutput;
	if (mySteeringData.myRotation > mySteeringData.myMaxRotation)
	{
		mySteeringData.myRotation = mySteeringData.myMaxRotation;
	}
	else if (mySteeringData.myRotation > -mySteeringData.myMaxRotation)
	{
		mySteeringData.myRotation = -mySteeringData.myMaxRotation;
	}
}

void CCompanion::SetOnLoadPosition(const CommonUtilities::Vector3f& aStartPosition)
{
	GetTransform().SetPosition(aStartPosition);
	myStartPosition = aStartPosition;
	mySteeringData.myPosition = myStartPosition;
	//myTargetPosition = myStartPosition;
}

void CCompanion::SetStartPosition(const CommonUtilities::Vector3f & aStartPosition)
{
	myStartPosition = aStartPosition;
}

void CCompanion::Respawn()
{
	//ResetTransform();

	GetTransform().SetPosition(myStartPosition);
	mySteeringData.myPosition = myStartPosition;
	myUltimateAoECountDown.Reset();

	myCanUltimate = true;
	myHasUsedUltimate = false;

	myController->Reset();

	myAnimationController->SetAnimation("companionWalk");
}

bool CCompanion::GetCanUltimate() const
{
	return myCanUltimate;
}

bool CCompanion::GetUltimateAoEIsActive() const
{
	return myHasUsedUltimate;
}

bool CCompanion::GetIsBasicAttacking() const
{
	return myIsBasicAttacking;
}

void CCompanion::SetIsFinishedUltimate()
{
	myHasUsedUltimate = false;
	CPollingStation::GetInstance().SetUltimateFinished();
}

void CCompanion::UpdateMovement(const SSteeringOutput& aSteeringOutput)
{
	float dt = IWorld::Time().GetDeltaTime();
	mySteeringData.myPosition += mySteeringData.myVelocity * dt;

	GetTransform().SetPosition(
		CommonUtilities::Lerp(
			GetTransform().GetPosition(),
			mySteeringData.myPosition,
			dt * 3.0f));

	const CommonUtilities::Vector3f direction = { cosf(mySteeringData.myOrientation), 0.0f, sinf(mySteeringData.myOrientation) };
	CommonUtilities::Vector3f lookDirection = CommonUtilities::Slerp(GetTransform().GetForward().GetNormalized(), direction.GetNormalized(), dt * 8.f);
	lookDirection.y = 0.f;
	GetTransform().SetLookDirection(lookDirection);

	mySteeringData.myVelocity = aSteeringOutput.myLinearOutput;
	if (mySteeringData.myVelocity.Length() > mySteeringData.myMaxSpeed)
	{
		mySteeringData.myVelocity = mySteeringData.myVelocity.GetNormalized() * mySteeringData.myMaxSpeed;
	}
}

void CCompanion::Melee()
{
	if (!myIsBasicAttacking)
	{
		const auto closestEnemyPos = CPollingStation::GetInstance().GetClosestEnemy(mySteeringData.myPosition)->GetTransform().GetPosition();

		if ((closestEnemyPos - GetTransform().GetPosition()).Length2() <= 2.f)
		{
			IWorld::SetDebugColor({ 0.f, 1.f, 0.f, 1.f });

			myIsBasicAttacking = true;
			myAnimationController->SetAnimationForced("companionAttack");
		}
	}
}

void CCompanion::AcidCleanup()
{
	auto acid = CPollingStation::GetInstance().GetClosestAcid(mySteeringData.myPosition);
	if (acid != nullptr)
	{
		//std::cout << "going towars acid.." << std::endl;

		if(myController->HasArrivedToTarget())
		{
			//IWorld::SetDebugColor({ 0.f, 1.f, 0.f, 1.f });
			//std::cout << "ACID CLEANED UP" << std::endl;
			myAnimationController->SetAnimationForced("companionClean");
			myIsCleaningUp = true;
			myAcidToCleanUp = acid;
		}
	}
}

void CCompanion::UltimateAoE()
{
	if (!myHasUsedUltimate)
	{
		const auto dropLocation = CPollingStation::GetInstance().GetCenterPositionOfGroupedUpEnemies();

		//IWorld::SetDebugColor({ 0.f, 1.f, 0.f, 1.f });
		//std::cout << "Going towards middle" << std::endl;

		if (myAnimationController->GetAnimation() != "companionWalk")
		{
			myAnimationController->SetAnimation("companionWalk");
		}

		if (myController->HasArrivedToTarget())
		{
			myAnimationController->SetAnimation("companionAbility");
			AM.PlayNewInstance("SFX/Companion/Ultimate");

			myUltiExplotion->Play();
			myUltimateAoECountDown.Reset();
			myUltimateAoECountDown.Start();
			myHasUsedUltimate = true;
			myCanUltimate = false;

			IWorld::SetDebugColor({ 0.f, 1.f, 0.f, 1.f });
			//std::cout << "Spawned AoE" << std::endl;
		}
	}
}

#pragma once
#include "Controller.h"
#include "AreaOfEffect.h"

class CAreaOfEffect;

class CCompanion : public CGameObject
{
public:
	CCompanion();
	~CCompanion();

	static void BindMetricsPtr(CMetrics& aMetrics);

	void Init(ID_T(CScene) aSceneID, CController* aController, int aAccessID, ID_T(CGameObjectData) aParentID = ID_T_INVALID(CGameObjectData));
	void Update();

	void SetOnLoadPosition(const CommonUtilities::Vector3f& aStartPosition);
	void SetStartPosition(const CommonUtilities::Vector3f& aStartPosition);
	void Respawn();

	bool GetCanUltimate() const;
	bool GetUltimateAoEIsActive() const;
	bool GetIsBasicAttacking() const;
	void SetIsFinishedUltimate();

private:
	void UpdateMovement(const SSteeringOutput& aSteeringOutput);
	void Melee();
	void AcidCleanup();
	void UltimateAoE();

	static CMetrics* ourMetrics;

	CommonUtilities::Vector3f myPlayerPositionLastFrame;

	CommonUtilities::Vector3f myPlayerPositionCurrentFrame;

	CommonUtilities::Vector3f myStartPosition;

	CParticleSystemComponent* myUltiExplotion;

	CAnimationControllerComponent* myAnimationController;

	CController* myController;
	SSteeringData mySteeringData;

	CAreaOfEffect* myAcidToCleanUp;
	Countdown myUltimateAoECountDown;

	float mySpeed;
	bool myIsCleaningUp;
	bool myHasUsedUltimate;
	bool myCanUltimate;
	bool myIsBasicAttacking;
};


#include "stdafx.h"
#include "CompanionDecisionTreeController.h"
#include "PollingStation.h"
#include "AreaOfEffect.h"
#include "Actor.h"

CCompanionDecisionTreeController::CCompanionDecisionTreeController(const int aBehaviourFlag)
	: CController::CController(aBehaviourFlag)
{
	
}

CCompanionDecisionTreeController::~CCompanionDecisionTreeController()
{
}


SSteeringOutput CCompanionDecisionTreeController::Update(SSteeringData& aActorData)
{
	SSteeringOutput output;
	int cachedBehavior = myBehaviourFlag & EBehaviours_Arrive;
	int behaviour = 0;
	behaviour = cachedBehavior;

	const auto closestAcid = CPollingStation::GetInstance().GetClosestAcid(aActorData.myPosition);
	const auto closestEnemy = CPollingStation::GetInstance().GetClosestEnemy(aActorData.myPosition);
	const auto centerPosEnemies = CPollingStation::GetInstance().GetCenterPositionOfGroupedUpEnemies();

	if (CPollingStation::GetInstance().GetCanCompanionUltimate() && CPollingStation::GetInstance().CalculateIfEnemiesGroupedUp() &&
		(!IWorld::GetSavedFrustum().CullSphere(centerPosEnemies, 0.75f)))
	{
		behaviour |= EBehaviours_UltimateAoE;
		behaviour &= (~EBehaviours_Arrive);
	}
	else if (closestAcid != nullptr 
		&& (!IWorld::GetSavedFrustum().CullSphere(closestAcid->GetTransform().GetPosition(), 1.f)))
	{
		behaviour |= EBehaviours_AcidCleanup;
		behaviour &= (~EBehaviours_Arrive);
	}
	else if (closestEnemy != nullptr && 
		!IWorld::GetSavedFrustum().CullSphere(closestEnemy->GetTransform().GetPosition(), 0.5f)
		&& (closestEnemy->GetTransform().GetPosition() - aActorData.myPosition).Length2() < ourMetrics->GetCompanionMetrics().mySeekAcidRange)
	{
		behaviour |= EBehaviours_AttackEnemy;
		behaviour &= (~EBehaviours_Arrive);
	}
	else
	{
		const CommonUtilities::Vector3f& playerPos = CPollingStation::GetInstance().GetPlayerTransform().GetPosition();
		float distance = abs(playerPos.x - aActorData.myPosition.x);
		distance += abs(playerPos.z - aActorData.myPosition.z);
		float outerRing = 6.f;
		float innerRing = 2.5f;
		
		if (distance > outerRing)
		{
			behaviour = behaviour | EBehaviours_Arrive; //FOLLOW PLAYER
		}
		else if(distance < innerRing)
		{
			behaviour &= (~EBehaviours_Arrive);
		}
	}

	//behaviour = behaviour | EBehaviours_Evade;
	behaviour = behaviour | EBehaviours_KinFace;

	myBehaviourFlag = behaviour;
	output = this->CController::Update(aActorData);
	return output;
}

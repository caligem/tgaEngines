#pragma once
#include "Controller.h"

class CCompanionDecisionTreeController : public CController
{
public:
	CCompanionDecisionTreeController(const int aBehaviourFlag);
	~CCompanionDecisionTreeController();

	virtual SSteeringOutput Update(SSteeringData& aActorData) override;
};


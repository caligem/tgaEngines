#include "stdafx.h"
#include "Controller.h"
#include "IWorld.h"
#include "CameraDataWrapper.h"
#include "PollingStation.h"
#include "AreaOfEffect.h"
#include "Actor.h"
#include "Exploder.h"
#include "EqualityCheck.h"

CMetrics* CController::ourMetrics = nullptr;

CController::CController(const int aBehaviourFlag)
{
	myBehaviourFlag = aBehaviourFlag;
	AddBehaviour(EBehaviours_Wander, 1.0f);
	AddBehaviour(EBehaviours_Arrive, 1.0f);
	AddBehaviour(EBehaviours_Evade, 5.0f);
	AddBehaviour(EBehaviours_KinFace, 1.0f);
	AddBehaviour(EBehaviours_AttackPlayer, 1.0f);
	AddBehaviour(EBehaviours_AttackEnemy, 1.0f);
	AddBehaviour(EBehaviours_AcidCleanup, 1.0f);
	AddBehaviour(EBehaviours_UltimateAoE, 1.0f);
}


CController::~CController()
{
}

void CController::BindMetricsPtr(CMetrics & aMetrics)
{
	ourMetrics = &aMetrics;
}

SSteeringOutput CController::Update(SSteeringData& aActorData)
{
	IWorld::Path().DebugRenderPath(myBehaviour.GetPathPoints());

	const CommonUtilities::Vector3f playerPosition = CPollingStation::GetInstance().GetPlayerTransform().GetPosition();
	const float playerDist = (playerPosition - aActorData.myPosition).Length2();

	SSteeringOutput output;

	SSteeringOutput arriveOutput;
	SSteeringOutput kinFaceOutput;
	SSteeringOutput wanderOutput;
	SSteeringOutput acidOutput;
	SSteeringOutput ultimateAoEOutput;
	SSteeringOutput evadeOutput;
	SSteeringOutput attackOutput;
	SSteeringOutput avoidAcidOutput;

	CommonUtilities::Vector3f targetKinFace = playerPosition;

	//////////
	output.myOutputState = EIdle;

	if (myBehaviourFlag & EBehaviours_Arrive)
	{
		output.myOutputState = EFollowPlayer;

		if (!PathFindToTarget(aActorData, playerPosition, arriveOutput, targetKinFace, EPathFindPlayer))
		{
			output.myOutputState = EIdle;
		}

		arriveOutput.myLinearOutput *= GetWeight(EBehaviours_Arrive);

		//IWorld::SetDebugColor({ 0.0f, 1.0f, 0.0f, 1.0f });
		//IWorld::DrawDebugArrow(aActorData.myPosition, aActorData.myPosition + seekOutput);
	}

	if (myBehaviourFlag & EBehaviours_Wander)
	{
		wanderOutput = myBehaviour.DoWander(aActorData);
		wanderOutput.myLinearOutput *= GetWeight(EBehaviours_Wander);
		targetKinFace = aActorData.myPosition + wanderOutput.myLinearOutput;

		IWorld::SetDebugColor({ 1.0f, 0.0f, 0.0f, 1.0f });
		IWorld::DrawDebugArrow(aActorData.myPosition, aActorData.myPosition + wanderOutput.myLinearOutput);
	}

	if (myBehaviourFlag & EBehaviours_AttackEnemy)
	{
		output.myOutputState = EAttacking;

		const auto closestEnemy = CPollingStation::GetInstance().GetClosestEnemy(aActorData.myPosition);

		const CExploder* exploderEnemy = dynamic_cast<const CExploder*>(closestEnemy);

		auto closestEnemyPos = closestEnemy->GetTransform().GetPosition();

		if (exploderEnemy != nullptr) //if this is exploder
		{
			closestEnemyPos += (exploderEnemy->GetTransform().GetForward() * 4.f); //+ (exploderEnemy->GetTransform().GetRight() * 0.95f);
		}


		PathFindToTarget(aActorData, closestEnemyPos, attackOutput, targetKinFace, EPathFindAttack);


		//IWorld::DrawDebugWireSphere(closestEnemyPos);
	}

	if (myBehaviourFlag & EBehaviours_AttackPlayer)
	{
		output.myOutputState = EAttacking;
		targetKinFace = CPollingStation::GetInstance().GetPlayerTransform().GetPosition();
	}

	if (myBehaviourFlag & EBehaviours_AcidCleanup)
	{
		output.myOutputState = EAcidCleanup;

		const auto acidPos = CPollingStation::GetInstance().GetClosestAcid(aActorData.myPosition)->GetTransform().GetPosition();
		PathFindToTarget(aActorData, acidPos, acidOutput, targetKinFace, EPathFindCleanup);
	}

	if (myBehaviourFlag & EBehaviours_UltimateAoE)
	{
		output.myOutputState = EUltimateAoE;

		const auto centerPos = CPollingStation::GetInstance().GetCenterPositionOfGroupedUpEnemies();
		//IWorld::DrawDebugSphere(centerPos, { 0.5f, 0.5f, 0.5f });

		PathFindToTarget(aActorData, centerPos, ultimateAoEOutput, targetKinFace, EPathFindUltimate);
	}

	if (myBehaviourFlag & EBehaviours_AvoidPlayerAcid)
	{
		const auto acid = CPollingStation::GetInstance().GetPlayerAcid();
		if (acid.GetIsActive())
		{
			avoidAcidOutput.myLinearOutput += myBehaviour.DoEvade(aActorData, acid.GetTransform().GetPosition(), 5.0f).myLinearOutput;
		}
		avoidAcidOutput.myLinearOutput *= GetWeight(EBehaviours_AvoidPlayerAcid);
	}



	//Evade
	const auto& enemies = CPollingStation::GetInstance().GetEnemies();
	for (int i = 0; i < enemies.size(); ++i)
	{
		if (enemies[i] == nullptr || !enemies[i]->IsValid())
		{
			continue;
		}

		const auto enemyToCheckPos = enemies[i]->GetTransform().GetPosition();

		if ((enemyToCheckPos - aActorData.myPosition).Length2() < 1e-6)
		{
			continue;
		}		
		evadeOutput.myLinearOutput += myBehaviour.DoEvade(aActorData, enemyToCheckPos).myLinearOutput;
	}

	evadeOutput.myLinearOutput *= GetWeight(EBehaviours_Evade);

	if (myBehaviourFlag & EBehaviours_KinFace)
	{
		kinFaceOutput = myBehaviour.DoKinematicFace(aActorData, targetKinFace);
	}

	output.myLinearOutput += arriveOutput.myLinearOutput + wanderOutput.myLinearOutput + acidOutput.myLinearOutput + ultimateAoEOutput.myLinearOutput + evadeOutput.myLinearOutput + attackOutput.myLinearOutput;
	output.myAngularOutput = kinFaceOutput.myAngularOutput;


	//IWorld::SetDebugColor({ 1.0f, 1.0f, 1.0f, 1.0f });
	//IWorld::DrawDebugArrow(aActorData.myPosition, aActorData.myPosition + output);
	return output;
}

bool CController::PathFindToTarget(const SSteeringData &aActorData, const CommonUtilities::Vector3f& aTarget, SSteeringOutput& arriveOutput, CommonUtilities::Vector3f& targetKinFace, EPathFindingState aPathState)
{
	if (IWorld::Path().GetPathState(myBehaviour.GetPathTicket()) == EPathState_None || myBehaviour.GetPathFindingState() != aPathState)
	{
		IWorld::Path().RequestPath(aActorData.myPosition, aTarget, myBehaviour.GetPathTicket());
		myBehaviour.SetPathFindingState(aPathState);
		myBehaviour.SetHasPath(false);
		return false;
	}
	else if (IWorld::Path().GetPathState(myBehaviour.GetPathTicket()) == EPathState::EPathState_Found)
	{
		ArriveToNextPathPoint(aTarget, arriveOutput, aActorData, targetKinFace);
	}
	else if (IWorld::Path().GetPathState(myBehaviour.GetPathTicket()) == EPathState_NotFound)
	{
		//arriveOutput = myBehaviour.DoArrive(aActorData, aTarget);
		IWorld::Path().FinishedUsingPath(myBehaviour.GetPathTicket());
		return false;
	}
	return true;
}

void CController::ArriveToNextPathPoint(const CommonUtilities::Vector3f playerPosition, SSteeringOutput &arriveOutput, const SSteeringData &aActorData, CommonUtilities::Vector3f &targetKinFace)
{
	if (!myBehaviour.HasPath())
	{
		myBehaviour.GetPathPoints() = IWorld::Path().GetPath(myBehaviour.GetPathTicket());
		myBehaviour.SetHasPath(true);

		if (!myBehaviour.GetPathPoints().Empty())
		{
			auto targetPosition = myBehaviour.GetPathPoints().GetLast();
			if ((targetPosition - aActorData.myPosition).Length2() < 1.0f)
			{
				myBehaviour.GetPathPoints().Pop();
			}
		}
	}
	else
	{
		if (!myBehaviour.GetPathPoints().Empty())
		{
			if ((myBehaviour.GetPathPoints()[0] - playerPosition).Length2() > 3.f * 3.f)
			{
				myBehaviour.SetHasPath(false);
				IWorld::Path().RequestPath(aActorData.myPosition, playerPosition, myBehaviour.GetPathTicket());
				return;
			}

			if (myBehaviour.GetPathPoints().Size() > 1)
			{
				auto& currentPoint = myBehaviour.GetPathPoints()[myBehaviour.GetPathPoints().Size() - 1];
				auto& nextPoint = myBehaviour.GetPathPoints()[myBehaviour.GetPathPoints().Size() - 2];

				auto toNext = nextPoint - currentPoint;
				toNext.Normalize();

				auto toPlayer = aActorData.myPosition - currentPoint;

				auto dot = toPlayer.Dot(toNext);

				if (dot > 0.f)
				{
					auto closestPoint = currentPoint + toNext * dot;
					if ((aActorData.myPosition - closestPoint).Length2() < 1e-2)
					{
						myBehaviour.GetPathPoints().Pop();
					}
				}
			}

			if (myBehaviour.GetPathPoints().Size() > 1)
			{
				arriveOutput = myBehaviour.DoArrive(aActorData, myBehaviour.GetPathPoints().GetLast(), 0.f, 0.f);
			}
			else
			{
				arriveOutput = myBehaviour.DoArrive(aActorData, myBehaviour.GetPathPoints().GetLast(), 0.f, aActorData.mySlowDownRadius);
			}

			targetKinFace = myBehaviour.GetPathPoints().GetLast();

			if ((aActorData.myPosition - myBehaviour.GetPathPoints().GetLast()).Length2() < 1.f)
			{
				myBehaviour.GetPathPoints().Pop();
			}
		}
		else
		{
			myBehaviour.SetHasPath(false);
			IWorld::Path().FinishedUsingPath(myBehaviour.GetPathTicket());
		}
	}
}

bool CController::HasArrivedToTarget()
{
	return myBehaviour.GetPathPoints().Empty() && IWorld::Path().GetPathState(myBehaviour.GetPathTicket()) != EPathState_Searching;
}

void CController::AddBehaviour(EBehaviours aBehaviour, const float aWeight)
{
	auto it = myWeights.find(aBehaviour);
	if (it == myWeights.end())
	{
		myWeights[aBehaviour] = aWeight;
	}
	else
	{
		// Error handle
	}
}

void CController::ChangeWeight(EBehaviours aBehaviour, const float aWeight)
{
	auto it = myWeights.find(aBehaviour);
	if (it != myWeights.end())
	{
		it->second = aWeight;
	}
	else
	{
		// Weigth did not excist
	}
}

const float CController::GetWeight(EBehaviours aBehaviour)
{
	auto it = myWeights.find(aBehaviour);
	if (it != myWeights.end())
	{
		return it->second;
	}
	else
	{
		return 0.0f;
	}
}

void CController::Reset()
{
	myBehaviour.SetHasPath(false);
	myBehaviour.SetPathFindingState(EPathFindNone);

	if (myBehaviour.GetPathTicket() != ID_T_INVALID(SPath))
	{
		IWorld::Path().ReturnTicket(myBehaviour.GetPathTicket());
	}
}

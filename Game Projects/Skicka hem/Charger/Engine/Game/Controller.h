#pragma once
#include "SteeringBehaviour.h"
#include <unordered_map>

struct SSteeringOutput;
struct SSteeringData;
class CMetrics;

class CController
{
public:
	CController(const int aBehaviourFlag);
	~CController();

	static void BindMetricsPtr(CMetrics& aMetrics);

	virtual SSteeringOutput Update(SSteeringData& aActorData);

	bool PathFindToTarget(const SSteeringData &aActorData, const CommonUtilities::Vector3f& aTarget, SSteeringOutput& arriveOutput, CommonUtilities::Vector3f& targetKinFace, EPathFindingState aPathState);

	void ArriveToNextPathPoint(const CommonUtilities::Vector3f playerPosition, SSteeringOutput &arriveOutput, const SSteeringData &aActorData, CommonUtilities::Vector3f &targetKinFace);

	bool HasArrivedToTarget();
	//virtual SSteeringOutput Update(const SSteeringData &aActorData) = 0;

	enum EBehaviours
	{
		EBehaviours_Wander = (1 << 0),
		EBehaviours_Arrive = (1 << 1),
		EBehaviours_Evade = (1 << 2),
		EBehaviours_KinFace = (1 << 3),
		EBehaviours_AttackPlayer = (1 << 4),
		EBehaviours_AttackEnemy = (1 << 5),
		EBehaviours_AcidCleanup = (1 << 6),
		EBehaviours_UltimateAoE = (1 << 7),
		EBehaviours_AvoidPlayerAcid = (1 << 8)
		//EBehaviours_Explode = (1 << 8),
		//EBehaviours_ShootProjectile = (1 << 9)
	};

	void AddBehaviour(EBehaviours aBehaviour, const float aWeight = 1.0f);
	void ChangeWeight(EBehaviours aBehaviour, const float aWeight = 1.0f);
	const float GetWeight(EBehaviours aBehaviour);
	void Reset();
protected:
	static CMetrics* ourMetrics;
	std::unordered_map<EBehaviours, float> myWeights;
	int myBehaviourFlag;

private:
	CSteeringBehaviour myBehaviour;
	CommonUtilities::Vector3f mySeekTarget;
};
	
#include "stdafx.h"
#include "CppScriptFunctions.h"
#include "ScriptManager.h"

#include "GameWorld.h"
#include <IWorld.h>
#include "Vector3.h"
#include <AudioManager.h>
#include "GameObject.h"
#include "GameState.h"
#include "ShowroomState.h"

#include "MainMenuState.h"

CGameWorld* CCppScriptFunctions::ourGameWorld = nullptr;
CGameState* CCppScriptFunctions::ourGameState = nullptr;
std::vector<CGameObject> CCppScriptFunctions::myGameobjectIDs = std::vector<CGameObject>(10001);

CCppScriptFunctions::CCppScriptFunctions()
{
}


CCppScriptFunctions::~CCppScriptFunctions()
{
}


void CCppScriptFunctions::RegisterScriptFunctions()
{
	IWorld::Script().RegisterFunction("SetSFXVolume", SetSFXVolume, "\n"\
		"		Sets the SFX Volume\n"\
		"		A value between 0 - 100\n"\
	);
	IWorld::Script().RegisterFunction("SetMusicVolume", SetMusicVolume, "\n"\
		"		Sets the Music Volume\n"\
		"		A value between 0 - 100\n"\
	);
	IWorld::Script().RegisterFunction("SetMasterVolume", SetMasterVolume, "\n"\
		"		Sets the Master Volume\n"\
		"		A value between 0 - 100\n"\
	);
	IWorld::Script().RegisterFunction("StartScene", StartScene, "\n"\
		"		Start Scene\n"\
		"		Takes levelName of scene/level as argument\n"\
	);
	IWorld::Script().RegisterFunction("StartShowroom", StartShowroom, "\n"\
		"		Start Showroom\n"\
		"		Takes no argument\n"\
	);
	IWorld::Script().RegisterFunction("SwapScene", SwapScene, "\n"\
		"		Swap Scene\n"\
		"		Takes levelName of scene/level as argument\n"\
	);
	IWorld::Script().RegisterFunction("ShowCanvas", ShowCanvas, "\n"\
		"		Show a canvas\n"\
		"		Takes name of canvas as argument\n"\
	);
	IWorld::Script().RegisterFunction("HideCanvas", HideCanvas, "\n"\
		"		Hide a canvas\n"\
		"		Takes name of canvas as argument\n"\
	);
	IWorld::Script().RegisterFunction("ForceUnpauseGame", ForceUnpauseGame, "\n"\
		"		Force unpause the game, sets time speed to 1.f\n"\
		"		No arguments\n"\
	);
	IWorld::Script().RegisterFunction("UnpauseGame", UnpauseGame, "\n"\
		"		Unpause the game\n"\
		"		No arguments\n"\
	);
	IWorld::Script().RegisterFunction("ExitGame", ExitGame, "\n"\
		"		Exit the game\n"\
		"		No arguments\n"\
	);
	IWorld::Script().RegisterFunction("ExitToDesktop", ExitToDesktop, "\n"\
		"		Exit To Desktop\n"\
		"		No arguments\n"\
	);
	IWorld::Script().RegisterFunction("Print", Print, "\n"\
		"		Print Text\n"\
		"		A String to print to consol\n"\
	);
	IWorld::Script().RegisterFunction("RegisterCallback", RegisterCallback, "\n"\
		"		Calls lua function when it is triggerd\n"\
		"		a Game Object ID\n"\
		"		Name Of Event\n"\
		"		Trigger ID\n"\
		"		Name Of LuaFunction\n"\
	);
	IWorld::Script().RegisterFunction("Notify", Notify, "\n"\
		"		Notify all Listeners to this Event \n"\
		"		Name Of Event\n"\
		"		Trigger ID\n"\
	);
	IWorld::Script().RegisterFunction("MoveGameObject", MoveGameObject, "\n"\
		"		Move Object\n"\
		"		A Game Object ID\n"\
		"		A Position Table or 3 floats\n"\
	);
	IWorld::Script().RegisterFunction("SetPosition", SetPosition, "\n"\
		"		Set Position for Object\n"\
		"		A Game Object ID\n"\
		"		A Position Table or 3 floats\n"\
	);
	IWorld::Script().RegisterFunction("DestroyGameObject", DestroyGameObject, "\n"\
		"		Destroy Object\n"\
		"		A Game Object ID\n"\
	);
	IWorld::Script().RegisterFunction("ParticlePlay", ParticlePlay, "\n"\
		"		Play Objects ParticleEffect\n"\
		"		A Game Object ID\n"\
	);
	IWorld::Script().RegisterFunction("ParticleStop", ParticleStop, "\n"\
		"		Stop Objects ParticleEffect\n"\
		"		A Game Object ID\n"\
	);
	IWorld::Script().RegisterFunction("RunDialogue", RunDialogue, "\n" \
		"		Run Dialogue \n" \
		"		A DialogueName \n"\
	);
	IWorld::Script().RegisterFunction("PushTextToDialogue", PushTextToDialogue, "\n" \
		"		Push Text To Dialogue Frame \n" \
		"		A Text you wanna push to frame \n"\
	);
	IWorld::Script().RegisterFunction("ShouldUpdate", ShouldUpdate, "\n" \
		"		Decide if Update Loop should run\n" \
		"		A Game Object ID\n"\
		"		A Bool\n"\
	);
	IWorld::Script().RegisterFunction("DistanceBetween", DistanceBetween, "\n" \
		"		Returns Distance between two objects\n" \
		"		A Game Object ID nr1\n"\
		"		A Game Object ID nr2\n"\
	);
	IWorld::Script().RegisterFunction("LookAt", LookAt, "\n" \
		"		Turns Object to Look at other object\n" \
		"		A Game Object ID nr1\n"\
		"		A Game Object ID nr2\n"\
	);
	IWorld::Script().RegisterFunction("CameraLookAt", CameraLookAt, "\n" \
		"		Turns Camera to look at position\n" \
		"		X value\n"\
		"		Y value\n"\
		"		Z value\n"\
	);
	IWorld::Script().RegisterFunction("CameraLookAtGameObject", CameraLookAtGameObject, "\n" \
		"		Turns Camera to look at GameObject\n" \
		"		A Game Object ID nr\n"\
	);
	IWorld::Script().RegisterFunction("DetachCamera", DetachCamera, "\n" \
		"		Stopps the camera from following the player\n" \
	);
	IWorld::Script().RegisterFunction("AttachCamera", AttachCamera, "\n" \
		"		Makes Camera Follow Player\n" \
	);
	IWorld::Script().RegisterFunction("SetStartPositionForPlayerWithGameObject", SetStartPositionForPlayerWithGameObject, "\n" \
		"		Set the spawn position for the Player\n" \
		"		A Game Object ID\n"\
	);
	IWorld::Script().RegisterFunction("SetStartPositionForPlayer", SetStartPositionForPlayer, "\n" \
		"		Set the spawn position for the Player\n" \
		"		X value\n"\
		"		Y value\n"\
		"		Z value\n"\
	);
	IWorld::Script().RegisterFunction("PauseDeltaTime", PauseDeltaTime, "\n" \
		"		Pause Time\n" \
	);
	IWorld::Script().RegisterFunction("UnPauseDeltaTime", UnPauseDeltaTime, "\n" \
		"		Un Pause Time\n" \
	);
	IWorld::Script().RegisterFunction("WonGame", WonGame, "\n" \
		"		Won Game\n" \
	);
}

int CCppScriptFunctions::Print(lua_State* aLuaState)
{
	if (lua_isnumber(aLuaState, -1))
	{
		float aNumber = static_cast<float>(lua_tonumber(aLuaState, -1));
		std::cout << aNumber << std::endl;
		char buffer[64];
		snprintf(buffer, sizeof buffer, "%f", aNumber);
		SCRIPT_LOG(CONCOL_DEFAULT, buffer);
	}
	else if (lua_isstring(aLuaState, -1))
	{
		std::cout << lua_tostring(aLuaState, -1) << std::endl;
		SCRIPT_LOG(CONCOL_DEFAULT, lua_tostring(aLuaState, -1));
	}
	return 0;
}

int CCppScriptFunctions::RegisterCallback(lua_State* aLuaState)
{
	if (lua_isnumber(aLuaState, 1))
	{
		const int luaNumber = static_cast<int>(lua_tonumber(aLuaState, 1));
		CGameObject gameObject = GetGameObject(luaNumber);

		if (gameObject.IsValid())
		{
			if (lua_isstring(aLuaState, 4))
			{
				auto scirpt = gameObject.GetComponent<CScriptComponent>();
				if (scirpt && scirpt->GetScriptID() != ID_T_INVALID(CLuaScript))
				{
					const unsigned short event = static_cast<unsigned short>(lua_tonumber(aLuaState, 2));
					if (CScriptEventManager::EScriptEvent_Count < event)
					{
						SCRIPT_LOG(CONCOL_ERROR, "Event type does not Exist")
						return 0;
					}
					const unsigned short triggerID = static_cast<unsigned short>(lua_tonumber(aLuaState, 3));
					if (triggerID > 512)
					{
						SCRIPT_LOG(CONCOL_ERROR, "Trigger ID is higher then 128 do you need that many?")
						return 0;
					}
					CScriptEventManager::SLuaCallback callback = { gameObject.GetComponent<CScriptComponent>()->GetScriptID(), triggerID, lua_tostring(aLuaState, 4) };
					IWorld::Script().AddListenerToEvent(static_cast<CScriptEventManager::EScriptEvent>(event), callback);
				}
				else
				{
					SCRIPT_LOG(CONCOL_ERROR, "ScriptID is InValid, when trying to register callback")
				}
			}
		}
 	}
	return 0;
}

int CCppScriptFunctions::Notify(lua_State * aLuaState)
{
#ifndef _RETAIL
	if (lua_isnumber(aLuaState, 1))
	{
		if (lua_isnumber(aLuaState, 2))
		{
#endif _RETAIL
			IWorld::Script().NotifyListeners(static_cast<CScriptEventManager::EScriptEvent>(static_cast<int>(lua_tonumber(aLuaState, 1))), static_cast<unsigned short>(lua_tonumber(aLuaState, 2)));
#ifndef _RETAIL
		}
		else
		{
			SCRIPT_LOG(CONCOL_WARNING, "Second value needs to be a number")
		}
	}
	else
	{
		SCRIPT_LOG(CONCOL_WARNING, "First value needs to be a number")
	}
#endif _RETAIL
	return 0;
}

int CCppScriptFunctions::MoveGameObject(lua_State* aLuaState)
{
#ifndef _RETAIL
	if (lua_isnumber(aLuaState, 1))
	{
#endif _RETAIL
		const int luaNumber = static_cast<int>(lua_tonumber(aLuaState, 1));
		CGameObject gameObject = GetGameObject(luaNumber);

		CommonUtilities::Vector3f aPosition;

#ifndef _RETAIL
		if (lua_isnumber(aLuaState, 2) && lua_isnumber(aLuaState, 3) && lua_isnumber(aLuaState, 4))
		{
#endif _RETAIL
			aPosition.x = static_cast<float>(lua_tonumber(aLuaState, 2));
			aPosition.y = static_cast<float>(lua_tonumber(aLuaState, 3));
			aPosition.z = static_cast<float>(lua_tonumber(aLuaState, 4));
#ifndef _RETAIL
		}
		else
		{
			SCRIPT_LOG(CONCOL_WARNING, "Could not read Position Data");
		}
#endif _RETAIL
		gameObject.GetTransform().Move(aPosition);
#ifndef _RETAIL
	}	
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "argument 1 is not a GameObjectID");
	}
#endif _RETAIL
	return 0;
}

int CCppScriptFunctions::SetPosition(lua_State * aLuaState)
{
	if (lua_isnumber(aLuaState, 1))
	{
		const int luaNumber = static_cast<int>(lua_tonumber(aLuaState, 1));
		CGameObject gameObject = GetGameObject(luaNumber);

		if (gameObject.IsValid())
		{
			CommonUtilities::Vector3f aPosition(0.0f, 0.0f, 0.0f);
			if (lua_isnumber(aLuaState, 2))
			{
				aPosition.x = static_cast<float>(lua_tonumber(aLuaState, 2));
			}
			else
			{
				SCRIPT_LOG(CONCOL_WARNING, "Could not find X value, default = 0");
			}
			if (lua_isnumber(aLuaState, 3))
			{
				aPosition.y = static_cast<float>(lua_tonumber(aLuaState, 3));
			}
			else
			{
				SCRIPT_LOG(CONCOL_WARNING, "Could not find Y value, default = 0");
			}
			if (lua_isnumber(aLuaState, 4))
			{
				aPosition.z = static_cast<float>(lua_tonumber(aLuaState, 4));
			}
			else
			{
				SCRIPT_LOG(CONCOL_WARNING, "Could not find Z value, default = 0");

			}
			gameObject.GetTransform().SetPosition(aPosition);
		}
		else
		{
			SCRIPT_LOG(CONCOL_ERROR, "GameObject are not Valid");
		}
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "return value ain't a GameObjectID");
	}

	return 0;
}

int CCppScriptFunctions::DestroyGameObject(lua_State * aLuaState)
{
	if (lua_isnumber(aLuaState, 1))
	{
		CGameObject gameObject;
		gameObject.InitFromAccessID(static_cast<int>(lua_tonumber(aLuaState, 1)), ourGameWorld->GetCurrentSceneID());

		if (gameObject.IsValid())
		{
			if (gameObject.IsValid())
			{
				gameObject.Destroy();
			}
		}
	}
	return 0;
}

int CCppScriptFunctions::ParticlePlay(lua_State * aLuaState)
{
	if (lua_isnumber(aLuaState, 1))
	{
		CGameObject gameObject;
		gameObject.InitFromAccessID(static_cast<int>(lua_tonumber(aLuaState, 1)), ourGameWorld->GetCurrentSceneID());

		if (gameObject.IsValid())
		{
			CParticleSystemComponent* particleSys = gameObject.GetComponent<CParticleSystemComponent>();
			if (particleSys != nullptr)
			{
				particleSys->Play();
			}
			else
			{
				SCRIPT_LOG(CONCOL_WARNING, "Tried calling particle play on object without particle system")
			}
		}
	}
	return 0;
}

int CCppScriptFunctions::ParticleStop(lua_State * aLuaState)
{
	if (lua_isnumber(aLuaState, 1))
	{
		CGameObject gameObject;
		gameObject.InitFromAccessID(static_cast<int>(lua_tonumber(aLuaState, 1)), ourGameWorld->GetCurrentSceneID());

		if (gameObject.IsValid())
		{
			CParticleSystemComponent* particleSys = gameObject.GetComponent<CParticleSystemComponent>();
			if (particleSys != nullptr)
			{
				particleSys->Stop();
			}
			else
			{
				SCRIPT_LOG(CONCOL_WARNING, "Tried calling particle stop on object without particle system")
			}
		}
	}
	return 0;
}

int CCppScriptFunctions::RunDialogue(lua_State * aLuaState)
{
	if (lua_isstring(aLuaState, -1))
	{
		const std::string dialogueName = lua_tostring(aLuaState, -1);
		ourGameWorld->GetStateStack().GetCurrentState()->RunDialogue(dialogueName);
	}
	else
	{
		SCRIPT_LOG(CONCOL_WARNING, "Cant run dialogue with this argument, string is required.");
	}

	return 0;
}

int CCppScriptFunctions::PushTextToDialogue(lua_State * aLuaState)
{
	if (lua_isstring(aLuaState, -1))
	{
		const std::string text = lua_tostring(aLuaState, -1);
		ourGameWorld->GetStateStack().GetCurrentState()->PushText(text);
	}
	else
	{
		SCRIPT_LOG(CONCOL_WARNING, "Cant push text to dialogue with this argument, string is required.");
	}

	return 0;
}

int CCppScriptFunctions::ShouldUpdate(lua_State * aLuaState)
{
	if (lua_isnumber(aLuaState, 1))
	{
		CGameObject gameObject;
		gameObject.InitFromAccessID(static_cast<int>(lua_tonumber(aLuaState, 1)), ourGameWorld->GetCurrentSceneID());

		if (gameObject.IsValid())
		{
			if (lua_isboolean(aLuaState, 2))
			{
				CScriptComponent* script = gameObject.GetComponent<CScriptComponent>();
				script->ShouldUpdate(lua_toboolean(aLuaState, 2) != 0);
			}
			else
			{
				SCRIPT_LOG(CONCOL_WARNING, "Second value must be a bool");
			}
		}
	}
	return 0;
}

int CCppScriptFunctions::DistanceBetween(lua_State * aLuaState)
{
#ifndef _RETAIL
	if (lua_isnumber(aLuaState, 1))
	{
		if (lua_isnumber(aLuaState, 2))
		{
#endif _RETAIL
			const int luaNumber1 = static_cast<int>(lua_tonumber(aLuaState, 1));
			const int luaNumber2 = static_cast<int>(lua_tonumber(aLuaState, 2));
			CGameObject gameObject1 = GetGameObject(luaNumber1);
			CGameObject gameObject2 = GetGameObject(luaNumber2);

			lua_pushnumber(aLuaState, (gameObject1.GetTransform().GetPosition().Length() - gameObject2.GetTransform().GetPosition().Length()));
			return 1;

#ifndef _RETAIL
		}
	}
#endif _RETAIL
	return 0;
}

int CCppScriptFunctions::LookAt(lua_State* aLuaState)
{
#ifndef _RETAIL
	if (lua_isnumber(aLuaState, 1))
	{
		if (lua_isnumber(aLuaState, 2))
		{
#endif _RETAIL
			const int luaNumber1 = static_cast<int>(lua_tonumber(aLuaState, 1));
			const int luaNumber2 = static_cast<int>(lua_tonumber(aLuaState, 2));
			CGameObject gameObject1 = GetGameObject(luaNumber1);
			CGameObject gameObject2 = GetGameObject(luaNumber2);

			gameObject1.GetTransform().LookAt(gameObject2.GetTransform().GetPosition());
#ifndef _RETAIL
		}
	}
#endif _RETAIL
	return 0;
}

int CCppScriptFunctions::SetRotation(lua_State* aLuaState)
{
#ifndef _RETAIL
	if (lua_isnumber(aLuaState, 1))
	{
#endif _RETAIL
			const int luaNumber = static_cast<int>(lua_tonumber(aLuaState, 1));
			CGameObject gameObject = GetGameObject(luaNumber);
#ifndef _RETAIL
			if (!lua_isnumber(aLuaState, 2))
			{
				SCRIPT_LOG(CONCOL_WARNING, "Could not find X value, default = 0");
				return 0;
			}
			if (!lua_isnumber(aLuaState, 3))
			{
				SCRIPT_LOG(CONCOL_WARNING, "Could not find Y value, default = 0");
				return 0;
			}
			if (!lua_isnumber(aLuaState, 4))
			{
				SCRIPT_LOG(CONCOL_WARNING, "Could not find Z value, default = 0");
				return 0;
			}
#endif _RETAIL
			gameObject.GetTransform().SetRotation({ static_cast<float>(lua_tonumber(aLuaState, 2)), static_cast<float>(lua_tonumber(aLuaState, 3)), static_cast<float>(lua_tonumber(aLuaState, 4)) });
#ifndef _RETAIL
	}
#endif _RETAIL
	return 0;
}

int CCppScriptFunctions::DetachCamera(lua_State* /*aLuaState*/)
{
	ourGameState->SetShouldCameraFollow(false);
	return 0;
}

int CCppScriptFunctions::AttachCamera(lua_State* /*aLuaState*/)
{
	ourGameState->SetShouldCameraFollow(true);
	return 0;
}

int CCppScriptFunctions::CameraLookAt(lua_State * aLuaState)
{
#ifndef _RETAIL
	if (!lua_isnumber(aLuaState, 1))
	{
		SCRIPT_LOG(CONCOL_WARNING, "Could not find X value");
		return 0;
	}
	if (!lua_isnumber(aLuaState, 2))
	{
		SCRIPT_LOG(CONCOL_WARNING, "Could not find Y value");
		return 0;
	}
	if (!lua_isnumber(aLuaState, 3))
	{
		SCRIPT_LOG(CONCOL_WARNING, "Could not find Z value");
		return 0;
	}
#endif _RETAIL

	const float x = static_cast<float>(lua_tonumber(aLuaState, 1));
	const float y = static_cast<float>(lua_tonumber(aLuaState, 2));
	const float z = static_cast<float>(lua_tonumber(aLuaState, 3));
	ourGameState->GetMainCamera().GetTransform().LookAt({ x, y, z });
	return 0;
}

int CCppScriptFunctions::CameraLookAtGameObject(lua_State * aLuaState)
{
#ifndef _RETAIL
	if (lua_isnumber(aLuaState, 1))
	{
#endif _RETAIL
		const int luaNumber = static_cast<int>(lua_tonumber(aLuaState, 1));
		CGameObject gameObject = GetGameObject(luaNumber);
		gameObject.GetTransform().SetLookDirection(gameObject.GetTransform().GetPosition());
#ifndef _RETAIL
	}
#endif _RETAIL
	return 0;
}

int CCppScriptFunctions::SetStartPositionForPlayer(lua_State * aLuaState)
{
#ifndef _RETAIL
	if (!lua_isnumber(aLuaState, 1))
	{
		SCRIPT_LOG(CONCOL_WARNING, "Could not find X value");
		return 0;
	}
	if (!lua_isnumber(aLuaState, 2))
	{
		SCRIPT_LOG(CONCOL_WARNING, "Could not find Y value");
		return 0;
	}
	if (!lua_isnumber(aLuaState, 3))
	{
		SCRIPT_LOG(CONCOL_WARNING, "Could not find Z value");
		return 0;
	}
#endif _RETAIL

	const float x = static_cast<float>(lua_tonumber(aLuaState, 1));
	float y = static_cast<float>(lua_tonumber(aLuaState, 2));
	if (y > 0.0f)
	{
		y = 0.0f;
	}
	const float z = static_cast<float>(lua_tonumber(aLuaState, 3));
	ourGameState->GetPlayer().SetStartPosition({ x, y, z });
	return 0;
}

int CCppScriptFunctions::SetStartPositionForPlayerWithGameObject(lua_State * aLuaState)
{
#ifndef _RETAIL
	if (lua_isnumber(aLuaState, 1))
	{
#endif _RETAIL
		const int luaNumber = static_cast<int>(lua_tonumber(aLuaState, 1));
		CGameObject gameObject = GetGameObject(luaNumber);
		ourGameState->GetPlayer().SetStartPosition(gameObject.GetTransform().GetPosition());
		ourGameState->GetCompanion().SetStartPosition(gameObject.GetTransform().GetPosition());
#ifndef _RETAIL
	}
#endif _RETAIL
	return 0;
}

int CCppScriptFunctions::PauseDeltaTime(lua_State* /*aLuaState*/)
{
	ourGameState->SetTargetDeltaSpeed(0.f);
	return 0;
}

int CCppScriptFunctions::UnPauseDeltaTime(lua_State* /*aLuaState*/)
{
	ourGameState->SetTargetDeltaSpeed(1.0f);
	return 0;
}

int CCppScriptFunctions::WonGame(lua_State * )
{
	CMainMenuState::ourWonGame = true;
	return 0;
}

CGameObject& CCppScriptFunctions::GetGameObject(const int & aAccesID)
{
	if (myGameobjectIDs.size() > aAccesID)
	{
		if (myGameobjectIDs[aAccesID].IsValid())
		{
			return myGameobjectIDs[aAccesID];
		}
		else
		{
			CGameObject gameObject;
			gameObject.InitFromAccessID(aAccesID, ourGameWorld->GetCurrentSceneID());
			myGameobjectIDs[aAccesID].PointToObject(gameObject);
			return myGameobjectIDs[aAccesID];
		} 
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "AccesID is too large");
		return myGameobjectIDs[0];
	}
}

int CCppScriptFunctions::SetSFXVolume(lua_State* aLuaState)
{
	if (lua_isnumber(aLuaState, -1))
	{
		float aVolume = static_cast<float>(lua_tonumber(aLuaState, -1));
		AM.SetVolume(aVolume, AudioChannel::SoundEffects);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for SetSFXVolume is not a number");
	}
	return 0;
}

int CCppScriptFunctions::SetMusicVolume(lua_State* aLuaState)
{
	if (lua_isnumber(aLuaState, -1))
	{
		float aVolume = static_cast<float>(lua_tonumber(aLuaState, -1));
		AM.SetVolume(aVolume, AudioChannel::Music);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for SetMusicVolume is not a number");
	}
	return 0;
}

int CCppScriptFunctions::SetMasterVolume(lua_State* aLuaState)
{
	if (lua_isnumber(aLuaState, -1))
	{
		float aVolume = static_cast<float>(lua_tonumber(aLuaState, -1));
		AM.SetVolume(aVolume, AudioChannel::Master);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for SetMasterVolume is not a number");
	}
	return 0;
}

int CCppScriptFunctions::StartScene(lua_State* aLuaState)
{
	if (lua_isstring(aLuaState, -1))
	{
		std::string levelName = "Assets/Levels/";
		levelName.append(lua_tostring(aLuaState, -1));

		levelName.append(".json");

		ourGameWorld->GetStateStack().LoadAsync([=]()
		{
			CGameState* gameState = new CGameState(levelName);
			ourGameWorld->GetStateStack().PushMainStateWithoutOnEnter(gameState);
			gameState->Init();
			gameState->OnEnter();
			ourGameWorld->GetStateStack().StartFadeOut();
		}, false);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for StartScene is not a string");
	}
	return 0;
}

int CCppScriptFunctions::SwapScene(lua_State * aLuaState)
{
	if (lua_isstring(aLuaState, -1))
	{
		std::string levelName = "Assets/Levels/";
		levelName.append(lua_tostring(aLuaState, -1));

		levelName.append(".json");
		ourGameWorld->GetStateStack().GetCurrentState()->SwapScene(levelName);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for StartScene is not a string");
	}
	return 0;
}

int CCppScriptFunctions::StartShowroom(lua_State*)
{
	ourGameWorld->GetStateStack().GetCurrentState()->SetShouldPushState(true);
	return 0;
}

int CCppScriptFunctions::ShowCanvas(lua_State * aLuaState)
{
	if (lua_isstring(aLuaState, -1))
	{
		std::string canvasName = lua_tostring(aLuaState, -1);
		ourGameWorld->GetStateStack().GetCurrentState()->ShowCanvas(canvasName);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for ShowCanvas is not a string");
	}
	return 0;
}

int CCppScriptFunctions::HideCanvas(lua_State * aLuaState)
{
	if (lua_isstring(aLuaState, -1))
	{
		std::string canvasName = lua_tostring(aLuaState, -1);
		ourGameWorld->GetStateStack().GetCurrentState()->HideCanvas(canvasName);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for HideCanvas is not a string");
	}
	return 0;
}

int CCppScriptFunctions::ForceUnpauseGame(lua_State *)
{
	ourGameWorld->GetStateStack().GetCurrentState()->ForceUnpauseGame();
	return 0;
}

int CCppScriptFunctions::UnpauseGame(lua_State*)
{
	ourGameWorld->GetStateStack().GetCurrentState()->UnpauseGame();
	return 0;
}

int CCppScriptFunctions::ExitToDesktop(lua_State*)
{
	IWorld::GameQuit();
	return 0;
}

int CCppScriptFunctions::ExitGame(lua_State*)
{
	ourGameWorld->GetStateStack().GetCurrentState()->SetShouldPop(true);
	return 0;
}
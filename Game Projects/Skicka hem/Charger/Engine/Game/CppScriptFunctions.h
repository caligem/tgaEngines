#pragma once
#include <vector>

class CGameWorld;
class CGameObject;
class CGameState;
struct lua_State;

class CCppScriptFunctions
{
public:
	CCppScriptFunctions();
	~CCppScriptFunctions();

	static void RegisterScriptFunctions();
private:
	friend CGameWorld;
	friend CGameState;
	static int SetSFXVolume(lua_State* aLuaState);
	static int SetMusicVolume(lua_State* aLuaState);
	static int SetMasterVolume(lua_State* aLuaState);
	static int StartScene(lua_State* aLuaState);
	static int SwapScene(lua_State* aLuaState);
	static int StartShowroom(lua_State*);
	static int ShowCanvas(lua_State* aLuaState);
	static int HideCanvas(lua_State* aLuaState);
	static int ForceUnpauseGame(lua_State*);
	static int UnpauseGame(lua_State*);
	static int ExitToDesktop(lua_State*);
	static int ExitGame(lua_State*);
	static int Print(lua_State*);
	static int RegisterCallback(lua_State*);
	static int Notify(lua_State* aLuaState);
	static int MoveGameObject(lua_State* aLuaState);
	static int SetPosition(lua_State* aLuaState);
	static int DestroyGameObject(lua_State* aLuaState);
	static int ParticlePlay(lua_State* aLuaState);
	static int ParticleStop(lua_State* aLuaState);
	static int RunDialogue(lua_State* aLuaState);
	static int PushTextToDialogue(lua_State* aLuaState);
	static int ShouldUpdate(lua_State* aLuaState);
	static int DistanceBetween(lua_State* aLuaState);
	static int LookAt(lua_State* aLuaState);
	static int SetRotation(lua_State* aLuaState);
	static int DetachCamera(lua_State* aLuaState);
	static int AttachCamera(lua_State* aLuaState);
	static int CameraLookAt(lua_State* aLuaState);
	static int CameraLookAtGameObject(lua_State* aLuaState);
	static int SetStartPositionForPlayer(lua_State* aLuaState);
	static int SetStartPositionForPlayerWithGameObject(lua_State* aLuaState);
	static int PauseDeltaTime(lua_State* aLuaState);
	static int UnPauseDeltaTime(lua_State* aLuaState);
	static int WonGame(lua_State* aLuaState);

	static CGameWorld* ourGameWorld;
	static CGameState* ourGameState;

	static CGameObject& GetGameObject(const int& AccesID);

	static std::vector<CGameObject> myGameobjectIDs;
};


#include "stdafx.h"
#include "Dash.h"
#include "Metrics.h"
#include "PollingStation.h"
#include "AudioManager.h"

CMetrics* CDash::ourMetrics = nullptr;

CDash::CDash()
{
}


CDash::~CDash()
{
}

void CDash::BindMetricsPtr(CMetrics& aMetrics)
{
	ourMetrics = &aMetrics;
}

void CDash::Init(ID_T(CScene) aSceneID)
{
	myIsAiming = false;
	myShouldDash = false;
	myDashImpactDelayActive = false;
	myHasDashed = false;

	if (ourMetrics != nullptr)
	{
		myCharges = ourMetrics->GetPlayerMetrics().myDashMetrics.myMaxCharges;
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_WARNING, "Dash Metrics not loaded, player init before metrics load?");
		assert(true);
	}
	myRechargeTimer = 0.f;
	myDashImpactDelayTimer = 0.f;

	myMaxCircle.Init(aSceneID);
	myMaxCircle.AddComponent<CModelComponent>({ "Assets/Models/abilityRange/abilityRange.fbx" })->GetMaterial().SetShouldRenderDepth(false);
	myMaxCircle.SetActive(false);

	myCircleAroundMouse.Init(aSceneID);
	myCircleAroundMouse.AddComponent<CModelComponent>({ "Assets/Models/dashIndicator/dashIndicator.fbx" })->GetMaterial().SetShouldRenderDepth(false);
	const auto size = ourMetrics->GetPlayerMetrics().myDashMetrics.myDashAoERadius;
	myCircleAroundMouse.GetTransform().SetScale({ size, size, size });
	myCircleAroundMouse.SetActive(false);
}

void CDash::Update(const CommonUtilities::Vector3f& aPlayerPosition, const CommonUtilities::Vector3f& aMousePosition)
{
	if (myIsAiming)
	{
		RenderIndicator(aPlayerPosition, aMousePosition);
	}

	if (!HasMaxCharges())
	{
		myRechargeTimer += IWorld::Time().GetDeltaTime();

		if (myRechargeTimer >= ourMetrics->GetPlayerMetrics().myDashMetrics.myRechargeTime)
		{
			myRechargeTimer = 0.f;
			myCharges++;
		}
	}

	if (myDashImpactDelayActive)
	{
		if (myDashImpactDelayTimer >= ourMetrics->GetPlayerMetrics().myDashMetrics.myImpactDelay)
		{
			myDashImpactDelayTimer = 0.f;
			myDashImpactDelayActive = false;
		}
		else
		{
			myDashImpactDelayTimer += IWorld::Time().GetDeltaTime();
		}
	}
}

void CDash::ShouldNotDash()
{
	myShouldDash = false;
}

void CDash::TryDash()
{
	if (CanDash())
	{
		myShouldDash = true;
	}
}

void CDash::UseDash()
{
	AM.PlayNewInstance("SFX/Player/Ability/Dash");

	myMaxCircle.SetActive(false);
	myCircleAroundMouse.SetActive(false);
	myCharges--;
	myShouldDash = false;
	myIsAiming = false;
	myHasDashed = true;
	myDashImpactDelayActive = true;
}

float CDash::GetCooldownInPercent()
{
	return myRechargeTimer / ourMetrics->GetPlayerMetrics().myDashMetrics.myRechargeTime;
}

void CDash::ValidateDashTarget(const CommonUtilities::Vector3f& aPlayerPosition, CommonUtilities::Vector3f& aTargetPosition)
{
	float range = ourMetrics->GetPlayerMetrics().myDashMetrics.myDashRange;
	if ((aTargetPosition - aPlayerPosition).Length2() > range * range)
	{
		auto dir = (aTargetPosition - aPlayerPosition).GetNormalized();
		aTargetPosition = aPlayerPosition + (dir * range);
	}
}

inline const bool CDash::HasMaxCharges()
{
	{ return myCharges >= ourMetrics->GetPlayerMetrics().myDashMetrics.myMaxCharges; }
}

void CDash::SetDoneDashing()
{
	myHasDashed = false;
}

void CDash::Reset()
{
	myCharges = ourMetrics->GetPlayerMetrics().myDashMetrics.myMaxCharges;
	myRechargeTimer = 0.f;
	myDashImpactDelayTimer = 0.f;
	myShouldDash = false;
	myDashImpactDelayActive = false;
	myHasDashed = false;

	CancelAim();
}

void CDash::RenderIndicator(const CommonUtilities::Vector3f& aPlayerPosition, const CommonUtilities::Vector3f& aMousePosition)
{
	myMaxCircle.SetActive(true);
	myCircleAroundMouse.SetActive(true);

	float range = ourMetrics->GetPlayerMetrics().myDashMetrics.myDashRange;
	myMaxCircle.GetTransform().SetScale({ range, range, range });
	myMaxCircle.GetTransform().SetPosition(aPlayerPosition);

	CommonUtilities::Vector3f dashTarget = aMousePosition;
	ValidateDashTarget(aPlayerPosition, dashTarget);

	float impactRange = ourMetrics->GetPlayerMetrics().myDashMetrics.myDashAoERadius;
	myCircleAroundMouse.GetTransform().SetScale({ impactRange, impactRange, impactRange });
	myCircleAroundMouse.GetTransform().SetPosition(dashTarget);
}

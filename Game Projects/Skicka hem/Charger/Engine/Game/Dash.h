#pragma once

class CMetrics;

class CDash
{
public:
	CDash();
	~CDash();
	
	static void BindMetricsPtr(CMetrics& aMetrics);

	void Init(ID_T(CScene) aSceneID);
	void Update(const CommonUtilities::Vector3f& aPlayerPosition, const CommonUtilities::Vector3f& aMousePosition);

	inline void CancelAim() { myIsAiming = false; myMaxCircle.SetActive(false); myCircleAroundMouse.SetActive(false); }
	inline void ToggleAim() { myIsAiming = !myIsAiming; myMaxCircle.SetActive(myIsAiming); myCircleAroundMouse.SetActive(myIsAiming); }
	inline const bool IsAiming() const { return myIsAiming; }

	inline const bool CanDash() const { return myCharges > 0; }
	inline const bool ShouldDash() const { return myShouldDash; }
	void ShouldNotDash();
	inline const bool IsImpactDelayActive() const { return myDashImpactDelayActive; }

	void TryDash();
	
	void ValidateDashTarget(const CommonUtilities::Vector3f& aPlayerPosition, CommonUtilities::Vector3f& aTargetPosition);
	void UseDash();

	float GetCooldownInPercent();
	inline const bool HasMaxCharges();
	inline const int GetCharges() const { return myCharges; }
	const bool GetHasDashed() const { return myHasDashed; }
	void SetDoneDashing();

	void Reset();

private:
	void RenderIndicator(const CommonUtilities::Vector3f& aPlayerPosition, const CommonUtilities::Vector3f& aMousePosition);
	static CMetrics* ourMetrics;

	CGameObject myMaxCircle;
	CGameObject myCircleAroundMouse;

	int myCharges;

	float myRechargeTimer;
	float myDashImpactDelayTimer;

	bool myIsAiming;
	bool myShouldDash;
	bool myDashImpactDelayActive;
	bool myHasDashed;
};


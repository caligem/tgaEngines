#include "stdafx.h"
#include "Door.h"

#include "IWorld.h"
#include "PathFinder.h"

#include "PollingStation.h"
#include "AudioManager.h"

CDoor::CDoor()
{
	myIsOpen = false;
	myHasOpenedNavmesh = false;
}

CDoor::~CDoor()
{
}

void CDoor::OpenDoor()
{
	myHasOpenedNavmesh = false;
	myIsOpen = true;
	myTimer = 0.f;
	AM.PlayNewInstance("SFX/Environment/Gate_deep", nullptr);

	myPositionToOpen = GetTransform().GetPosition();
	myLocalPosition = GetTransform().GetLocalPosition();
	CPollingStation::GetInstance().ScreenShake();
}

void CDoor::Update()
{
	if (!myIsOpen)
	{
		return;
	}

	float dt = IWorld::Time().GetDeltaTime();

	constexpr float MaxTime = 3.f;

	if (!myHasOpenedNavmesh)
	{
		myTimer += dt;

		float f = CommonUtilities::Clamp01(myTimer / MaxTime);

		float a = 0.5f;
		float y = std::fmaxf(-25.f*std::powf(std::fminf(f, 0.1f) - 0.1f, 2.f) + 0.25f, 0.f) * -a;
		y += std::powf(-f, 5.f) * (1.f - 0.25f*a);

		//GENERAL_LOG(CONCOL_DEFAULT, "%f", y);

		float height = 5.5f;

		GetTransform().SetPosition(myLocalPosition + CommonUtilities::Vector3f(
			0.f,
			y * height,
			0.f
		));

		if (myTimer > MaxTime)
		{
			myHasOpenedNavmesh = true;
			IWorld::Path().ActivateTriangles(myPositionToOpen);
		}
	}
}

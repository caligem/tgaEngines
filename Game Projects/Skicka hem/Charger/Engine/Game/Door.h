#pragma once
#include "GameObject.h"

class CDoor : public CGameObject
{
public:
	CDoor();
	~CDoor();

	void SetDoorID(int aID) { myDoorID = aID; }
	const int GetDoorID() const { return myDoorID; }

	void SetRequiredKills(int aRequiredKills) { myRequiredKills = aRequiredKills; }
	const int GetRequiredKills() const { return myRequiredKills; }

	void OpenDoor();

	void Update();

private:
	int myDoorID = 0;
	int myRequiredKills = 0;

	bool myIsOpen = false;
	bool myHasOpenedNavmesh = false;
	float myTimer = 0.f;

	CommonUtilities::Vector3f myPositionToOpen;
	CommonUtilities::Vector3f myLocalPosition;
};


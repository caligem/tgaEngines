#include "stdafx.h"
#include "EnemyDecisionTreeController.h"
#include "PollingStation.h"


CEnemyDecisionTreeController::CEnemyDecisionTreeController(const int aBehaviourFlag)
	:CController::CController(aBehaviourFlag)
{
}


CEnemyDecisionTreeController::~CEnemyDecisionTreeController()
{
}

SSteeringOutput CEnemyDecisionTreeController::Update(SSteeringData& aActorData)
{
	SSteeringOutput output;
	int behaviour = 0;
	//behaviour = behaviour | EBehaviours_Wander;

	const auto playerPos = CPollingStation::GetInstance().GetPlayerTransform().GetPosition();
	const float distToPlayer = (playerPos - aActorData.myPosition).Length2();

	if (!aActorData.myHasSeenPlayer)
	{
		if (distToPlayer < aActorData.myAggroRange * aActorData.myAggroRange)
		{
			aActorData.myHasSeenPlayer = true;
		}
	}
		
	if (distToPlayer < aActorData.myAttackRange * aActorData.myAttackRange)
	{
		behaviour = behaviour | EBehaviours_AttackPlayer;
	}
	else
	{
		if (aActorData.myHasSeenPlayer)
		{
			behaviour = behaviour | EBehaviours_Arrive;
		}
	}

	behaviour = behaviour | EBehaviours_KinFace;

	myBehaviourFlag = behaviour;
	output = this->CController::Update(aActorData);
	return output;
}

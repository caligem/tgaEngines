#pragma once
#include "Controller.h"

class CEnemyDecisionTreeController : public CController
{
public:
	CEnemyDecisionTreeController(const int aBehaviourFlag);
	~CEnemyDecisionTreeController();

	virtual SSteeringOutput Update(SSteeringData& aActorData) override;
};


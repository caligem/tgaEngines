#include "stdafx.h"
#include "Exploder.h"
#include "IWorld.h"
#include "PollingStation.h"
#include "Metrics.h"
#include "CameraDataWrapper.h"
#include "AudioManager.h"

CExploder::CExploder()
{
}


CExploder::~CExploder()
{
}

void CExploder::Init(ID_T(CScene) aSceneID, CController* aController, int aAccessID, ID_T(CGameObjectData) aParentID)
{
	CGameObject::Init(aSceneID, aAccessID, aParentID);

	CModelComponent* modelComponent = AddComponent<CModelComponent>({"Assets/Models/enemies/enemyPopcorn/enemyPopcorn.fbx"});
	myAnimationController = AddComponent<CAnimationControllerComponent>(modelComponent);
	myAnimationController->SetAnimation("enemyPopcornIdle");


	myStunParticleObject.Init(aSceneID);
	myStunParticleObject.GetTransform().SetParent(&GetTransform());
	myStunParticleObject.GetTransform().Move({ 0.0f, 2.0f, 0.0f });
	myStunParticle = myStunParticleObject.AddComponent<CParticleSystemComponent>("Assets/Particles/enemyStunned.json");
	myStunParticle->Stop();

	myHitParticleObject.Init(aSceneID);
	myHitParticle = myHitParticleObject.AddComponent<CParticleSystemComponent>("Assets/Particles/bulletHitParticle.json");
	myHitParticle->Stop();

	myDeath = AddComponent<CParticleSystemComponent>("Assets/Particles/exploderSparks.json");
	myDeath->Stop();

	mySteeringData.myMaxAcceleration = 1.0f;
	mySteeringData.myMaxSpeed = ourMetrics->GetExploderMetrics().myMovementSpeed;
	mySteeringData.myMaxAngularAccelleration = 0.5f;
	mySteeringData.myMaxRotation = 1.5f;
	mySteeringData.myAttackRange = ourMetrics->GetExploderMetrics().myAttackRange;
	mySteeringData.myAggroRange = ourMetrics->GetExploderMetrics().myAggroRange;
	mySteeringData.myTargetRangeForArrive = 0.05f;
	myController = aController;

	CHealth::Init(ourMetrics->GetExploderMetrics().myMaxHealth);
	mySteeringData.myPosition = myStartPosition;

	CActor::myPlayerAoEDamageHandler.SetMaxMs(ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myAoEMaxMsBetweenDamageEnemies);
	CActor::myCompanionAoEDamageHandler.SetMaxMs(ourMetrics->GetCompanionMetrics().myUltimateMetrics.myAoEMaxMsBetweenDamage);
	CActor::myCompanionBasicAttackDamageHandler.SetMaxMs(ourMetrics->GetCompanionMetrics().myMsBetweenBasicAttackDamage);
	CActor::myPlayerDashDamageHandler.SetActiveTime(ourMetrics->GetPlayerMetrics().myDashMetrics.myStunTime);
}

void CExploder::Update()
{
	//for update if they change in doc
	//TODO: REMOVE
	mySteeringData.myMaxSpeed = ourMetrics->GetExploderMetrics().myMovementSpeed;
	mySteeringData.myAttackRange = ourMetrics->GetExploderMetrics().myAttackRange;
	mySteeringData.myAggroRange = ourMetrics->GetExploderMetrics().myAggroRange;

	if (myController == nullptr)
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Exploder controller was nullptr");
		return;
	}

	bool gotHit = false;
	if (CActor::myProjectileDamageHandler.GetWasHit())
	{
		gotHit = true;
		CHealth::TakeDamage(ourMetrics->GetPlayerMetrics().myAttackDamage);
		if (CHealth::GetIsDead())
		{
			myAnimationController->SetAnimation("enemyPopcornDeath");
			myAnimationController->SetLooping(false);
		}
	}

	if (CActor::myPlayerAoEDamageHandler.GetWasHit())
	{
		gotHit = true;
		CHealth::TakeDamage(ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myDamage);

		if (CHealth::GetIsDead())
		{
			myAnimationController->SetAnimation("enemyPopcornDeath");
			myAnimationController->SetLooping(false);
		}
	}
	if (CActor::myCompanionAoEDamageHandler.GetWasHit())
	{
		gotHit = true;
		CHealth::TakeDamage(ourMetrics->GetCompanionMetrics().myUltimateMetrics.myDamage);

		if (CHealth::GetIsDead())
		{
			myAnimationController->SetAnimation("enemyPopcornDeath");
			myAnimationController->SetLooping(false);
		}
	}

	if (CActor::myCompanionBasicAttackDamageHandler.GetWasHit())
	{
		gotHit = true;
		CHealth::TakeDamage(ourMetrics->GetCompanionMetrics().myBasicAttackDamage);

		if (CHealth::GetIsDead())
		{
			myAnimationController->SetAnimation("enemyPopcornDeath");
			myAnimationController->SetLooping(false);
		}
	}
	
	if (mySoundTimer <= 0.0f && gotHit)
	{
		AM.PlayNewInstance("SFX/Enemy/Exploder/Hurt");
		mySoundTimer = 0.4f;
	}
	mySoundTimer -= IWorld::Time().GetDeltaTime();

	CActor::UpdateHealthBar();

	CActor::myProjectileDamageHandler.Update();
	CActor::myPlayerAoEDamageHandler.Update();
	CActor::myCompanionAoEDamageHandler.Update();
	CActor::myCompanionBasicAttackDamageHandler.Update();
	CActor::myPlayerDashDamageHandler.Update();

	float dt = IWorld::Time().GetDeltaTime();

	if (CHealth::GetIsDead())
	{
		float animationTimeNextFrame = (myAnimationController->GetTime() + dt) * myAnimationController->GetCurrentAnimationTicksPerSecond();
		float animationMaxTime = myAnimationController->GetCurrentAnimationDuration();
		myDeath->Play();
		if (animationTimeNextFrame >= animationMaxTime)
		{
			myController->Reset();
			myShouldBeRemoved = true;
		}
	}
	else if (CActor::myPlayerDashDamageHandler.GetWasHit())
	{
		myAnimationController->SetAnimation("enemyPopcornIdle");
		myIsStunnedThisFrame = true;
	}
	else
	{
		SSteeringOutput steeringOutput;
		SetPosition(mySteeringData.myPosition + mySteeringData.myVelocity * dt);

		const CommonUtilities::Vector3f direction = { cosf(mySteeringData.myOrientation), 0.0f, sinf(mySteeringData.myOrientation) };
		CommonUtilities::Vector3f lookDirection = CommonUtilities::Slerp(GetTransform().GetForward().GetNormalized(), direction.GetNormalized(), dt * 8.f);
		lookDirection.y = 0.f;
		GetTransform().SetLookDirection(lookDirection);

		steeringOutput = myController->Update(mySteeringData);
		myState = steeringOutput.myOutputState;

		if (myState == EFollowPlayer)
		{
			myAnimationController->SetAnimation("enemyPopcornWalk");
			myAnimationController->SetLooping(true);
			CActor::UpdateMovementData(steeringOutput);
		}
		else if (myState == EIdle)
		{
			myAnimationController->SetAnimation("enemyPopcornIdle");
			myAnimationController->SetLooping(true);
			mySteeringData.myVelocity = { 0.f, 0.f, 0.f };
		}
		else if (myState == EAttacking)
		{
			Attack();
		}

		CActor::UpdateOrientationAndRotationData(steeringOutput);
	}
	if (myStunnedLastFrame && !myIsStunnedThisFrame)
	{
		myStunParticle->Stop();
	}
	else if (!myStunnedLastFrame && myIsStunnedThisFrame)
	{
		myStunParticle->Play();
	}
	myStunnedLastFrame = myIsStunnedThisFrame;
	myIsStunnedThisFrame = false;
}

void CExploder::SetPosition(const CommonUtilities::Vector3f& aPosition)
{
	GetTransform().SetPosition(aPosition);
	mySteeringData.myPosition = aPosition;
}

void CExploder::Respawn()
{
	ResetTransform();
	SetIsAlive();

	CHealth::Init(ourMetrics->GetExploderMetrics().myMaxHealth);
	CActor::ResetDamageHandlers();

	myController->Reset();
	mySteeringData.myHasSeenPlayer = false;
}

void CExploder::PlayHitParticle(const CommonUtilities::Vector3f & aPosition)
{
	myHitParticleObject.GetTransform().SetPosition(aPosition);
	myHitParticle->Play();
}

void CExploder::Attack()
{
	myAnimationController->SetAnimation("enemyPopcornAbility");
	myAnimationController->SetLooping(false);

	mySteeringData.myVelocity = { 0.f, 0.f, 0.f };
	CPollingStation::GetInstance().AddExploderAoEAtPosition(GetTransform().GetPosition());
	SetIsDead();
	AM.PlayNewInstance("SFX/Enemy/Exploder/Explode");
}
#pragma once
#include "Actor.h"

class CExploder : public CActor
{
public:
	CExploder();
	~CExploder();

	void Init(ID_T(CScene) aSceneID, CController* aController, int aAccessID, ID_T(CGameObjectData) aParentID = ID_T_INVALID(CGameObjectData)) override;
	void Update();

	void SetPosition(const CommonUtilities::Vector3f& aPosition) override;
	void Respawn() override;
	void PlayHitParticle(const CommonUtilities::Vector3f& aPosition);
private:
	void Attack();

	CGameObject myStunParticleObject;
	CParticleSystemComponent* myStunParticle;
	CGameObject myHitParticleObject;
	CParticleSystemComponent* myHitParticle;
	CParticleSystemComponent* myDeath;
	bool myIsStunnedThisFrame;
	bool myStunnedLastFrame;
	float mySoundTimer;
};


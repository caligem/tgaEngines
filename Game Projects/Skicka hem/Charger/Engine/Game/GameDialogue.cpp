#include "stdafx.h"
#include "GameDialogue.h"
#include <filesystem>
#include <iostream>

CGameDialogue::CGameDialogue()
{
}


CGameDialogue::~CGameDialogue()
{
}

void CGameDialogue::Init(ID_T(CScene) aSceneID)
{
	CGameObject gameObject;
	gameObject.Init(aSceneID);
	myDialogueFrame = gameObject.AddComponent<CCanvasComponent>({ CCanvasComponent::ERenderMode_ScreenSpaceOverlay, IWorld::GetCanvasSize() });

	myTextPosition = { 0.05f, 0.75f };

	InitTextComponents();

	LoadDialogues();
}

void CGameDialogue::Update()
{
	HandleTextSlots();
	HandleDialogues();
}

void CGameDialogue::PushText(const std::string & aText)
{
	for (STextSlot& textSlot : myTextSlots)
	{
		if (textSlot.myIsActive)
		{
			textSlot.mySlotIndex++;
			textSlot.myTargetPosition = { myTextPosition.x, myTextPosition.y - (myTextSlotOffset * textSlot.mySlotIndex) };
		}
	}

	myTextSlots[myNextTextSlot].myDuration = 0.f;
	myTextSlots[myNextTextSlot].myIsActive = true;
	myTextSlots[myNextTextSlot].myTextComponent->SetText(aText);
	myTextSlots[myNextTextSlot].myTextComponent->SetTint({ 1.f, 1.f, 1.f, 1.f });
	myTextSlots[myNextTextSlot].myTextComponent->SetPosition(myTextPosition);
	myTextSlots[myNextTextSlot].myTargetPosition = myTextPosition;
	myTextSlots[myNextTextSlot].mySlotIndex = 0;

	myNextTextSlot = (++myNextTextSlot) % myMaxTextSlots;
}

void CGameDialogue::RunDialogue(const std::string & aDialogueName)
{
	std::string dialogueName = "Assets/Dialogues/" + aDialogueName;

	if (myDialogueMap.find(dialogueName) == myDialogueMap.end())
	{
		SCRIPT_LOG(CONCOL_ERROR, "Cant run dialogue: %s", dialogueName.c_str());
		return;
	}

	int dialogueIndex = myDialogueMap.at(dialogueName);
	QueueDialogue(dialogueIndex);
}

void CGameDialogue::InitTextComponents()
{
	CTextComponent* textComponent;

	for (char slotIndex = 0; slotIndex < myMaxTextSlots; slotIndex++)
	{
		textComponent = myDialogueFrame->AddUIElement<CTextComponent>({ "Assets/Fonts/Mono/Mono" });
		textComponent->SetPivot({ 0.f, 0.5f });
		textComponent->SetScale({ 0.75f, 0.75f });
		textComponent->SetOutline({ 0.f, 0.f, 0.f, 1.f });
		textComponent->SetPosition(myTextPosition);
		textComponent->SetText("");

		myTextSlots[slotIndex].myTextComponent = textComponent;
	}
}

void CGameDialogue::LoadDialogues()
{
	std::string rootDir = "Assets/Dialogues";
	std::experimental::filesystem::path rootPath = std::experimental::filesystem::current_path().append(rootDir);
	GetDialoguePathsInFolder(rootPath.c_str());

	int dialogueCount = static_cast<unsigned short>(myDialogueMap.size());
	myDialogues.Init(dialogueCount);

	for (int i = 0; i < dialogueCount; ++i)
	{
		myDialogues.EmplaceBack();
	}

	for (auto& dialoguePath : myDialogueMap)
	{
		myDialogues[dialoguePath.second].Init(16);

		std::ifstream file;
		file.open(dialoguePath.first);

		std::string line;

		while (std::getline(file, line))
		{
			myDialogues[dialoguePath.second].Add(line);
		}
	}

}

void CGameDialogue::GetDialoguePathsInFolder(const wchar_t* aFolderPath)
{
	std::experimental::filesystem::directory_iterator modelFolderIt(aFolderPath);
	for (auto& file : modelFolderIt)
	{
		if (file.path().has_extension())
		{
			if (file.path().extension() == ".dlg")
			{
				std::string dialoguePath = file.path().generic_string();
				dialoguePath = dialoguePath.substr(std::experimental::filesystem::current_path().generic_string().size() + 1);
				
				myDialogueMap.emplace(dialoguePath, static_cast<int>(myDialogueMap.size()));
			}
		}
		else
		{
			GetDialoguePathsInFolder(file.path().c_str());
		}
	}
}

void CGameDialogue::HandleTextSlots()
{
	for (STextSlot& textSlot : myTextSlots)
	{
		if (textSlot.myIsActive)
		{
			textSlot.myTextComponent->SetPosition(CommonUtilities::Lerp(textSlot.myTextComponent->GetPosition(), textSlot.myTargetPosition, IWorld::Time().GetDeltaTime() * 10.f));

			textSlot.myDuration += IWorld::Time().GetDeltaTime();

			if (textSlot.myDuration >= myMaxTextDuration)
			{
				float currentFadeDuration = textSlot.myDuration - myMaxTextDuration;
				float percent = currentFadeDuration / myTextFadeDuration;

				if (percent >= 1.f)
				{
					textSlot.myTextComponent->SetText("");
					textSlot.myIsActive = false;

					percent = CommonUtilities::Clamp(percent, 0.f, 1.f);
				}
				textSlot.myTextComponent->SetTint(CommonUtilities::Lerp<CommonUtilities::Vector4f>({ 1.f, 1.f, 1.f, 1.f }, { 1.f, 1.f, 1.f, 0.f }, percent));
			}
		}
	}
}

void CGameDialogue::QueueDialogue(int dialogueIndex)
{
	myDialogueTimer = myDialogueDelay;

	for (auto& line : myDialogues[dialogueIndex])
	{
		myDialogueQueue.push(line);
	}
}

void CGameDialogue::HandleDialogues()
{
	if (myDialogueQueue.empty())
	{
		return;
	}

	myDialogueTimer += IWorld::Time().GetDeltaTime();

	if (myDialogueTimer >= myDialogueDelay)
	{
		myDialogueTimer = 0.f;

		const std::string& text = myDialogueQueue.front();
		PushText(text);
		myDialogueQueue.pop(); 
		if (myDialogueQueue.empty())
		{
			return;
		}
		const std::string& second = myDialogueQueue.front();
		PushText(second);
		myDialogueQueue.pop();
		const std::string& enter = "";
		PushText(enter);
	}
}

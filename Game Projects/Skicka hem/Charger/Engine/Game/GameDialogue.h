#pragma once
#include <map>
#include <queue>

class CGameDialogue
{
	struct STextSlot
	{
		CTextComponent* myTextComponent = nullptr;
		float myDuration = 0.f;
		char mySlotIndex = 0;
		bool myIsActive = false;
		CommonUtilities::Vector2f myTargetPosition;
	};

public:
	CGameDialogue();
	~CGameDialogue();

	void Init(ID_T(CScene) aSceneID);
	void Update();
	void PushText(const std::string& aText);
	void RunDialogue(const std::string& aDialogueName);

private:
	void InitTextComponents();
	void LoadDialogues();
	void GetDialoguePathsInFolder(const wchar_t* aFolderPath);
	void HandleTextSlots();
	void QueueDialogue(int dialogueIndex);
	void HandleDialogues();

	CommonUtilities::GrowingArray<CommonUtilities::GrowingArray<std::string, int>, int> myDialogues;
	std::map<std::string, int> myDialogueMap;
	std::queue<std::string> myDialogueQueue;

	STextSlot myTextSlots[5];
	CommonUtilities::Vector2f myTextPosition;

	CCanvasComponent* myDialogueFrame;

	static constexpr float myTextSlotOffset = 0.025f;
	static constexpr float myMaxTextDuration = 7.f;
	static constexpr float myTextFadeDuration = 2.0f;
	static constexpr float myDialogueDelay = 3.5f;
	static constexpr char myMaxTextSlots = 5;
	
	float myDialogueTimer = 0.f;
	char myNextTextSlot = 0;
};


#include "stdafx.h"
#include "GameState.h"
#include "IWorld.h"
#include "Random.h"
#include "RangedEnemy.h"
#include "Exploder.h"
#include "PollingStation.h"
#include "FileWatcher.h"
#include "AudioManager.h"
#include "CompanionDecisionTreeController.h"
#include "EnemyDecisionTreeController.h"
#include "PathFinder.h"
#include "CppScriptFunctions.h"

#include "VolumetricFogComponent.h"
#include "StateStack.h"
#include "JsonUtility.h"

#include <filesystem>

CGameState::CGameState(const std::string& aLevelToLoad)
	: myLevelToLoad(aLevelToLoad)
	, myHasCenterPositionOfGroupedUpEnemies(false)
	, myDoors(4)
	, myEnemiesAsActorObjects(32)
	, myExploderEnemies(32)
	, myRangedEnemies(32)
	, myTriggers(32)
	, myExploderAOEs(32)
	, myShouldPlayEndDialogue(false)
{
	myLevelID = -1;
	myCameraDetached = false;
	CCppScriptFunctions::ourGameState = this;
}


CGameState::~CGameState()
{
}

bool CGameState::Init()
{
	std::size_t pos = myLevelToLoad.find_last_of("Level");
	std::string id = myLevelToLoad.substr(pos + 1, 1);
	myLevelID = std::stoi(id) - 1;

	AM.StopAll(false);
	if (myLevelID == 0)
	{
		AM.Play("Music/MUSIC_Forest", nullptr);
	}
	else if (myLevelID == 1)
	{
		AM.Play("Music/MUSIC_Road", nullptr);
	}
	else if (myLevelID == 2)
	{
		AM.Play("Music/MUSIC_Base", nullptr);
	}
	else
	{
		AM.Play("Music/Music_MainTheme", nullptr);
	}
	myIsFading = false;
	myRespawnFade = 0.f;

	myShouldSwapScene = false;
	myShowPauseMenu = false;
	myTargetTimeSpeed = 1.f;

	CPollingStation::GetInstance().BindGameStatePtr(*this);
	mySceneID = IWorld::GetSceneManager().CreateScene();
	//CState::Init();
	//makes gameObjectID missmatch by 1 coz we add main camera in init
	myTriggers.Init(64);

	IWorld::GetFileWatcher().WatchFile(L"Assets/Metrics/CharacterMetrics.json", std::bind(&CMetrics::LoadMetricsFromFile, &myMetrics, std::placeholders::_1));
	myMetrics.LoadMetricsFromFile(L"Assets/Metrics/CharacterMetrics.json");

	CActor::BindMetricsPtr(myMetrics);
	CPlayer::BindMetricsPtr(myMetrics);
	CCompanion::BindMetricsPtr(myMetrics);
	CPollingStation::GetInstance().BindMetricsPtr(myMetrics);

	IWorld::GetFileWatcher().WatchFile(L"Assets/Metrics/inGameUIPositions.json", std::bind(&CPlayer::RepositionInGameUI, &myPlayer, std::placeholders::_1));

	CGameObject inGameUIGameObject;
	inGameUIGameObject.Init(mySceneID);
	myInGameUICanvas = inGameUIGameObject.AddComponent<CCanvasComponent>({ CCanvasComponent::ERenderMode_ScreenSpaceOverlay, IWorld::GetCanvasSize() });

	namespace fs = std::experimental::filesystem;

	fs::path navMeshPath(myLevelToLoad);
	navMeshPath.replace_extension("fbx");
	IWorld::Path().LoadNavmesh(navMeshPath.generic_string().c_str());

	myGameDialogue.Init(mySceneID);
	LoadFromFile(myLevelToLoad);

	myPlayer.Init(mySceneID);
	myPlayer.AddInGameUIElements(myInGameUICanvas);
	myPlayer.RepositionInGameUI(L"Assets/Metrics/inGameUIPositions.json");

	myPlayer.SetOnLoadPosition(myPlayerStartPosition);

	myProjectileManager.Init(mySceneID, &myMetrics);
	CPlayer::BindProjectileManagerPtr(myProjectileManager);
	CActor::BindProjectileManagerPtr(myProjectileManager);

	myMainCamera.Init(mySceneID);
	myMainCamera.AddComponent<CCameraComponent>({
		29.18f,
		static_cast<float>(IWorld::GetWindowSize().x / IWorld::GetWindowSize().y),
		0.1f, 1000.f
	})->SetAsActiveCamera();

	myMainCamera.GetTransform().SetPosition(myPlayer.GetTransform().GetPosition());

	CCompanionDecisionTreeController* companionController = new CCompanionDecisionTreeController(CController::EBehaviours_KinFace | CController::EBehaviours_Arrive);
	companionController->BindMetricsPtr(myMetrics);
	myCompanion.Init(mySceneID, companionController, 10001);
	myCompanion.SetOnLoadPosition(myPlayer.GetTransform().GetPosition() - CommonUtilities::Vector3f::Forward * 2.f);

	//RenderDebugEnemiesAroundPlayer(0, 1);

	CPollingStation::GetInstance().AddEnemyList(myEnemiesAsActorObjects);
	//////////////////////////////////////////////////////////////////////////
	/*
	CGameObject dir1;
	dir1.Init(mySceneID);
	dir1.GetTransform().SetLookDirection({ -1.f, -0.75f, 0.f });
	CLightComponent* light1 = dir1.AddComponent<CLightComponent>();
	light1->SetType(CLightComponent::ELightType_Directional);
	light1->SetColor({ 1.f, 0.8f, 0.2f });
	light1->SetIntensity(1.0f);
	*/

	CGameObject volumetricFog;
	volumetricFog.Init(mySceneID);
	volumetricFog.AddComponent<CVolumetricFogComponent>();
	volumetricFog.GetTransform().SetScale({ 1000.f, 14.f, 1000.f });
	volumetricFog.GetTransform().SetPosition({ 0.f, -10.f, 0.f });

	UpdateCamera(false);

	IWorld::GetSceneManager().GetSceneAt(mySceneID)->SetSkybox("Assets/CubeMaps/blackSkybox.dds");

	myNumberOfKilledEnemies = 0;

	myIsInited = true;

	return true;
}

EStateUpdate CGameState::Update()
{
	HandlePauseMenu();

	if (!myIsInited)
	{
		return EDoNothing;
	}

#ifndef _RETAIL
	static bool renderNavmesh = false;
	if (IWorld::Input().IsKeyPressed(Input::Key_N, 99))
	{
		renderNavmesh = !renderNavmesh;
	}
	if (renderNavmesh)
	{
		IWorld::Path().DebugRender();
	}
#endif

	HandleLevelReset();
	if (myShowPauseMenu)
	{
		IWorld::SetFadeColor({ 0.f, 0.f, 0.f, 0.f });
	}

	if (!myPlayer.GetIsDead() && !myShowPauseMenu)
	{
		myGameDialogue.Update();

		myProjectileManager.Update(myRangedEnemies, myExploderEnemies, &myPlayer);

		myEnemiesAsActorObjects.clear();
		myEnemiesAsActorObjects.insert(myEnemiesAsActorObjects.end(), myRangedEnemies.begin(), myRangedEnemies.end());
		myEnemiesAsActorObjects.insert(myEnemiesAsActorObjects.end(), myExploderEnemies.begin(), myExploderEnemies.end());

		////////// has to be checked before Updates
		PlayerAoECollisionChecks();
		PlayerDashCollisionChecks();
		UpdateExploderAoE();
		ExploderAoECollisionChecks();
		CompanionCollisionChecks();
		//////////---------------------------------

		myPlayer.Update();

		CleanUpDeadEnemies();
		UpdateDoors();

		myCompanion.Update();
		CPollingStation::GetInstance().Update(); //must be below Actors

		UpdateCamera(true, myCameraDetached);


		for (unsigned short i = 0; i < myTriggers.Size(); ++i)
		{
			myTriggers[i].CheckCollition(myPlayer.GetCollider());
		}
	}
	myPlayer.UpdateInGameUI();

	if (myShouldPop)
	{
		CCppScriptFunctions::myGameobjectIDs.clear();
		CCppScriptFunctions::myGameobjectIDs = std::vector<CGameObject>(10001);
		return EPop_Main;
	}
	if (myShouldSwapScene)
	{
		myIsInited = false;
		ourStateStack->LoadAsync([=]()
		{
			myIsInited = false;
			IWorld::Path().ClearRequests();
			CCppScriptFunctions::myGameobjectIDs.clear();
			CCppScriptFunctions::myGameobjectIDs = std::vector<CGameObject>(10001);
			CGameState* gameState = new CGameState(myNextLevel);
			ourStateStack->PopMainState();
			ourStateStack->PushMainStateWithoutOnEnter(gameState);
			gameState->Init();
			gameState->OnEnter();
			ourStateStack->StartFadeOut();
		}, false);

		return EDoNothing;
	}

	return EStateUpdate::EDoNothing;
}

void CGameState::CleanUpDeadEnemies()
{
	int numKilledEnemies = 0;

	for (int i = 0; i < myRangedEnemies.Size(); ++i)
	{
		myRangedEnemies[i]->Update();

		if (myRangedEnemies[i]->ShouldBeRemoved())
		{
			myRangedEnemies[i]->Destroy();
			myRangedEnemies.RemoveAtIndex(i);
			++numKilledEnemies;
		}
	}

	for (int i = 0; i < myExploderEnemies.Size(); ++i)
	{
		myExploderEnemies[i]->Update();

		if (myExploderEnemies[i]->ShouldBeRemoved())
		{
			myExploderEnemies[i]->Destroy();
			myExploderEnemies.RemoveAtIndex(i);
			++numKilledEnemies;
		}
	}


	myNumberOfKilledEnemies += numKilledEnemies;

	if (myCurrentDoor > myDoors.Size() - 1)
	{
		return;
	}
	if (myNumberOfKilledEnemies >= myDoors[myCurrentDoor].GetRequiredKills())
	{
		if (myLevelToLoad == "Assets/Levels/Level3.json" && myCurrentDoor == 3 && !myShouldPlayEndDialogue)
		{
			RunDialogue("Level3EndGame.dlg");
			myShouldPlayEndDialogue = true;
		}

		myNumberOfKilledEnemies -= myDoors[myCurrentDoor].GetRequiredKills();
		myDoors[myCurrentDoor].OpenDoor();
		++myCurrentDoor;
	}
}

void CGameState::HandleLevelReset()
{
	constexpr float AnimationTime = 2.f;

	if (!myPlayer.GetIsDead())
	{
		if (myIsFading)
		{
			myRespawnFade = CommonUtilities::Clamp01(myRespawnFade + IWorld::Time().GetDeltaTime() / AnimationTime);

			auto oldColor = IWorld::GetFadeColor();
			oldColor.x = std::powf(1.f - myRespawnFade, 4.f);
			oldColor.w = 1.f - myRespawnFade;
			IWorld::SetFadeColor(oldColor);

			if (myRespawnFade >= 1.f)
			{
				myIsFading = false;
			}
		}

		return;
	}

	if (!myIsFading)
	{
		myRespawnFade = 0.f;
		myIsFading = true;

		myPlayer.PlayDeathAnimation();
	}

	myRespawnFade = CommonUtilities::Clamp01(myRespawnFade + IWorld::Time().GetDeltaTime() / AnimationTime);

	auto oldColor = IWorld::GetFadeColor();
	oldColor.x = std::powf(myRespawnFade, 4.f);
	oldColor.w = myRespawnFade;
	IWorld::SetFadeColor(oldColor);

	if (myRespawnFade < 1.f)
	{
		return;
	}

	myRespawnFade = 0.f;

	IWorld::Path().ClearRequests();

	CNetMessagePlayerDeath* message = IClientNetwork::CreateMessage<CNetMessagePlayerDeath>();
	message->SetLevelID(myLevelID);
	message->SetPosition(myPlayer.GetTransform().GetPosition());
	IClientNetwork::Send(message);

	myPlayer.Respawn();
	myCompanion.Respawn();
	RespawnEnemies();
	myProjectileManager.ClearAllBullets();
	UpdateCamera(false);

	myHasCenterPositionOfGroupedUpEnemies = false;
	myCenterPositionOfGroupedUpEnemies = { 0.0f, 0.0f, 0.0f };
}

void CGameState::OnEnter()
{
	SetActiveScene();

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessagePlayerDeath>(
		std::bind(&CGameState::PlayerDeathReceived, this, std::placeholders::_1, std::placeholders::_2));

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessagePlayerDeathLocationList>(
		std::bind(&CGameState::PlayerDeathLocationSync, this, std::placeholders::_1, std::placeholders::_2));

	CNetMessagePlayerDeathLocationList* message = IClientNetwork::CreateMessage<CNetMessagePlayerDeathLocationList>();
	message->SetLevelID(myLevelID);
	IClientNetwork::Send(message);

	CPollingStation::GetInstance().BindGameStatePtr(*this);
	myShouldPop = false;
}

void CGameState::OnLeave()
{
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessagePlayerDeath>();
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessagePlayerDeathLocationList>();

	auto oldColor = IWorld::GetFadeColor();
	oldColor.x = 0.0f;
	oldColor.y = 0.0f;
	IWorld::SetFadeColor(oldColor);
}

void CGameState::PushText(const std::string & aText)
{
	myGameDialogue.PushText(aText);
}

void CGameState::RunDialogue(const std::string & aDialogue)
{
	myGameDialogue.RunDialogue(aDialogue);
}

void CGameState::OnLoadFinished(JsonDocument& aDoc, std::map<int, CGameObject>& aGameObjects)
{
	myExploderAOEs.Init(1);
	myExploderEnemies.Init(128);
	myRangedEnemies.Init(128);

	for (int i = 0; i < aDoc["myGameObjects"].GetSize(); ++i)
	{
		auto obj = aDoc["myGameObjects"][i];

		std::string tag = obj["myTag"].GetString();
		CActor* actor;
		CEnemyDecisionTreeController* controller;

		if (tag.compare("ExploderEnemy") == 0)
		{
			myExploderEnemies.Add(new CExploder());
			actor = myExploderEnemies.GetLast();
		}
		else if (tag.compare("RangedEnemy") == 0)
		{
			myRangedEnemies.Add(new CRangedEnemy());
			actor = myRangedEnemies.GetLast();
		}
		else if (tag.compare("Player") == 0)
		{
			myPlayerStartPosition = JsonToVector3f(obj["myPosition"]);

			continue;
		}
		else
		{
			continue;
		}
		controller = new CEnemyDecisionTreeController(CController::EBehaviours_KinFace | CController::EBehaviours_Arrive | CController::EBehaviours_AttackPlayer);
		controller->BindMetricsPtr(myMetrics);


		int parentID = obj["myParent"].GetInt();
		actor->Init(mySceneID, controller, obj["myID"].GetInt());
		actor->SetStartTransform(JsonToVector3f(obj["myPosition"]), JsonToQuatf(obj["myRotation"]).GetEulerAngles(), JsonToVector3f(obj["myScale"]));

		if (parentID != -1)
		{
			actor->GetTransform().SetParent(&aGameObjects[parentID].GetTransform());
		}

		if (tag.compare("ExploderEnemy") == 0)
		{
			myExploderEnemies.GetLast()->AddHealthBar(myInGameUICanvas);
		}
		else if (tag.compare("RangedEnemy") == 0)
		{
			myRangedEnemies.GetLast()->AddHealthBar(myInGameUICanvas);
			actor->GetController()->AddBehaviour(CController::EBehaviours_AvoidPlayerAcid);
		}


		if (obj.Find("myIsActive"))
		{
			actor->SetActive(obj["myIsActive"].GetBool());
		}
	}
	myDoors.Init(1);
	if (aDoc.Find("myDoors"))
	{
		int doorAmount = aDoc["myDoors"].GetSize();
		for (int i = 0; i < doorAmount; ++i)
		{
			auto obj = aDoc["myDoors"][i];
			myDoors.EmplaceBack();

			CDoor& door = myDoors.GetLast();
			int parentID = obj["myParentID"].GetInt();
			if (parentID != -1)
			{
				door.PointToObject(aGameObjects[parentID]);
			}
			door.SetDoorID(obj["myDoorID"].GetInt());
			door.SetRequiredKills(obj["myRequiredKills"].GetInt());
			IWorld::Path().DeactivateTriangles(door.GetTransform().GetPosition());
		}

		std::sort(myDoors.begin(), myDoors.end(), [](auto& a, auto& b)
		{
			return a.GetDoorID() < b.GetDoorID();
		});
	}
	if (aDoc.Find("myTriggers"))
	{
		for (int i = 0; i < aDoc["myTriggers"].GetSize(); ++i)
		{
			auto obj = aDoc["myTriggers"][i];
			int parentID = obj["myParent"].GetInt();
			if (aGameObjects.find(parentID) != aGameObjects.end())
			{
				CTriggerBox triggerBox;
				triggerBox.PointToObject(aGameObjects[parentID]);

				if (obj.Find("myTriggerID"))
				{
					if (obj.Find("myPosition"))
					{
						triggerBox.Init(static_cast<short>(obj["myTriggerID"].GetInt()),
							JsonToVector3f(obj["myPosition"]),
							JsonToQuatf(obj["myRotation"]).GetEulerAngles(), //TODO: wrong rotation, doenst take parent rotation into account, add function to get eulerangles from transformMatrix
							JsonToVector3f(obj["myScale"]));
						myTriggers.Add(triggerBox);
					}
				}
			}
		}
	}
}

void CGameState::SwapScene(const std::string & aLevelName)
{
	myShouldSwapScene = true;
	myNextLevel = aLevelName;
}

void CGameState::ForceUnpauseGame()
{
	IWorld::Time().SetSpeed(1.f);
	myShowPauseMenu = false;
	HideCanvas("PauseMenu");
	HideCanvas("Options");
	IWorld::Input().SetInputLevel(0);
}

void CGameState::UnpauseGame()
{
	myShowPauseMenu = false;
	myTargetTimeSpeed = 1.f;
	HideCanvas("PauseMenu");
	HideCanvas("Options");
	HideCanvas("HowToPlay");
	HideCanvas("Background");
	IWorld::Input().SetInputLevel(0);
}

const CTransform& CGameState::GetPlayerTransform()
{
	return myPlayer.GetTransform();
}

const CActor* CGameState::GetClosestEnemy(const CommonUtilities::Vector3f& aPosition)
{
	float shortestDistance = FLT_MAX;
	float distance = 0.0f;
	CActor* closestEnemy = nullptr;

	for (int i = 0; i < myRangedEnemies.Size(); ++i)
	{
		if (!myRangedEnemies[i]->IsValid())
		{
			continue;
		}

		distance = (aPosition - myRangedEnemies[i]->GetTransform().GetPosition()).Length();
		if (distance < shortestDistance && distance > 0.01f)
		{
			shortestDistance = distance;
			closestEnemy = myRangedEnemies[i];
		}
	}

	for (int i = 0; i < myExploderEnemies.Size(); ++i)
	{
		if (!myExploderEnemies[i]->IsValid())
		{
			continue;
		}

		distance = (aPosition - myExploderEnemies[i]->GetTransform().GetPosition()).Length();
		if (distance < shortestDistance && distance > 0.01f)
		{
			shortestDistance = distance;
			closestEnemy = myExploderEnemies[i];
		}
	}

	return closestEnemy;
}

CAreaOfEffect* CGameState::GetClosestAcid(const CommonUtilities::Vector3f& aPosition)
{
	float shortestDistance = FLT_MAX;
	float distance = 0.0f;
	CAreaOfEffect* closestAcid = nullptr;

	for (int i = 0; i < myExploderAOEs.Size(); ++i)
	{
		if (!myExploderAOEs[i].IsValid())
		{
			continue;
		}

		distance = (aPosition - myExploderAOEs[i].GetTransform().GetPosition()).Length();
		if (distance < shortestDistance && distance > 0.01f)
		{
			shortestDistance = distance;
			closestAcid = &myExploderAOEs[i];
		}
	}

	return closestAcid;
}

const CAreaOfEffect& CGameState::GetPlayerAcid()
{
	return myPlayer.GetAreaOfEffectObject();
}

void CGameState::RemoveExploderAoE(const CAreaOfEffect* aAoE)
{
	for (int i = 0; i < myExploderAOEs.Size(); ++i)
	{
		if (&myExploderAOEs[i] == aAoE)
		{
			//myExploderAOEs[i].Reset();
			myExploderAOEs[i].Destroy();
			myExploderAOEs.RemoveAtIndex(i);
		}
	}
}

void CGameState::AddExploderAOEAtPosition(const CommonUtilities::Vector3f& aPosition)
{
	CAreaOfEffect aoe;
	aoe.Init(mySceneID, true, "Assets/Models/hazards/hazard01/hazard01.fbx", nullptr, 3.f);
	aoe.GetTransform().SetPosition(aPosition);
	aoe.SetActive(true);
	aoe.Start();
	myExploderAOEs.Add(aoe);
}

bool CGameState::CalculateIfEnemiesGroupedUp()
{
	if (myHasCenterPositionOfGroupedUpEnemies && myCompanion.GetCanUltimate())
	{
		return myHasCenterPositionOfGroupedUpEnemies;
	}

	const auto companionPos = myCompanion.GetTransform().GetPosition();

	if ((companionPos - myPlayer.GetTransform().GetPosition()).Length() > myMetrics.GetCompanionMetrics().myUltimateMetrics.myMaxDistanceToPlayer)
	{
		return false;
	}

	int amountOfEnemiesNearby = 0;
	for (int i = 0; i < myEnemiesAsActorObjects.size(); ++i)
	{
		amountOfEnemiesNearby = 1;
		myCenterPositionOfGroupedUpEnemies = { 0.0f, 0.0f, 0.0f };
		CActor* enemy = myEnemiesAsActorObjects[i];
		if (!enemy->IsValid())
		{
			continue;
		}
		const float predictionMultiplier = 4.f;

		const auto iEnemyPos = enemy->GetTransform().GetPosition() + enemy->GetTransform().GetForward() * predictionMultiplier;

		const auto len = (companionPos - iEnemyPos).Length();
		if (len > myMetrics.GetCompanionMetrics().myUltimateMetrics.myMaxDistanceToEnemy)
		{
			continue;
		}

		for (int j = i + 1; j < myEnemiesAsActorObjects.size(); ++j)
		{
			CActor* secondEnemy = myEnemiesAsActorObjects[j];
			if (!secondEnemy->IsValid())
			{
				continue;
			}
			const auto jEnemyPos = secondEnemy->GetTransform().GetPosition() + enemy->GetTransform().GetForward() * predictionMultiplier;
			const float dist = (iEnemyPos - jEnemyPos).Length();

			if (dist < myMetrics.GetCompanionMetrics().myUltimateMetrics.myMaxDistanceBetweenGroupedUpEnemies)
			{
				++amountOfEnemiesNearby;

				myCenterPositionOfGroupedUpEnemies += jEnemyPos;
			}

			if (amountOfEnemiesNearby == myMetrics.GetCompanionMetrics().myUltimateMetrics.myAmountOfEnemiesGroupedUp)
			{
				myCenterPositionOfGroupedUpEnemies += iEnemyPos;

				myCenterPositionOfGroupedUpEnemies.x /= amountOfEnemiesNearby;
				myCenterPositionOfGroupedUpEnemies.y = 0.f;
				myCenterPositionOfGroupedUpEnemies.z /= amountOfEnemiesNearby;
				myHasCenterPositionOfGroupedUpEnemies = true;

				if ((myCenterPositionOfGroupedUpEnemies - myPlayer.GetTransform().GetPosition()).Length() > myMetrics.GetCompanionMetrics().myUltimateMetrics.myMaxDistanceToPlayer)
				{
					myCenterPositionOfGroupedUpEnemies = { 0.0f, 0.0f, 0.0f };
					myHasCenterPositionOfGroupedUpEnemies = false;
					return false;
				}

				return true;
			}
		}
	}

	myHasCenterPositionOfGroupedUpEnemies = false;
	return false;
}

void CGameState::SetUltimateFinished()
{
	myHasCenterPositionOfGroupedUpEnemies = false;
}

const CommonUtilities::Vector3f& CGameState::GetCenterPositionOfGroupedUpEnemies()
{
	return myCenterPositionOfGroupedUpEnemies;
}

bool CGameState::GetCanCompanionUltimate() const
{
	return myCompanion.GetCanUltimate();
}

bool CGameState::GetHasCenterPositionOfGroupedUpEnemies() const
{
	return myHasCenterPositionOfGroupedUpEnemies;
}

void CGameState::SetShouldCameraFollow(const bool aBool)
{
	myCameraDetached = aBool;
}

const bool CGameState::GetShouldCameraFollow() const
{
	return myCameraDetached;
}

void CGameState::RespawnEnemies()
{
	for (int i = 0; i < myExploderEnemies.Size(); ++i)
	{
		myExploderEnemies[i]->Respawn();
	}

	for (int i = 0; i < myRangedEnemies.Size(); ++i)
	{
		myRangedEnemies[i]->Respawn();
	}

	for (int i = 0; i < myExploderAOEs.Size(); ++i)
	{
		myExploderAOEs[i].Reset();
		myExploderAOEs[i].Destroy();
		myExploderAOEs.RemoveAtIndex(i);
		--i;
	}
}

void CGameState::ExploderAoECollisionChecks()
{
	for (auto& aoe : myExploderAOEs)
	{
		if ((CPollingStation::GetInstance().GetPlayerTransform().GetPosition() - aoe.GetTransform().GetPosition()).Length() < myMetrics.GetExploderMetrics().myAoECollisionDistance)
		{
			myPlayer.SetWasHitEnemyAoE();
		}
	}
}

void CGameState::PlayerAoECollisionChecks()
{
	const auto aoe = myPlayer.GetAreaOfEffectObject();
	if (!aoe.IsValid() || !aoe.GetIsActive())
	{
		return;
	}

	const auto AOEPos = aoe.GetTransform().GetPosition();

	for (unsigned short j = 0; j < myRangedEnemies.Size(); ++j)
	{
		CommonUtilities::Vector3f rangedEnemyPosition = myRangedEnemies[j]->GetTransform().GetPosition();
		if ((rangedEnemyPosition - AOEPos).Length2() < myMetrics.GetPlayerMetrics().myGrenadeMetrics.myAoECollisionDistance * myMetrics.GetPlayerMetrics().myGrenadeMetrics.myAoECollisionDistance)
		{
			myRangedEnemies[j]->SetWasHitPlayerAoE();
		}
	}

	for (unsigned short j = 0; j < myExploderEnemies.Size(); ++j)
	{
		CommonUtilities::Vector3f exploderPosition = myExploderEnemies[j]->GetTransform().GetPosition();
		if ((exploderPosition - AOEPos).Length2() < myMetrics.GetPlayerMetrics().myGrenadeMetrics.myAoECollisionDistance * myMetrics.GetPlayerMetrics().myGrenadeMetrics.myAoECollisionDistance)
		{
			myExploderEnemies[j]->SetWasHitPlayerAoE();
		}
	}

	if ((myPlayer.GetTransform().GetPosition() - AOEPos).Length2() < myMetrics.GetPlayerMetrics().myGrenadeMetrics.myAoECollisionDistance * myMetrics.GetPlayerMetrics().myGrenadeMetrics.myAoECollisionDistance)
	{
		myPlayer.SetWasHitHealAoE();
	}
}

void CGameState::PlayerDashCollisionChecks()
{
	if (!myPlayer.GetHasDashed())
	{
		return;
	}

	float range = myMetrics.GetPlayerMetrics().myDashMetrics.myDashAoERadius;

	for (unsigned short j = 0; j < myRangedEnemies.Size(); ++j)
	{
		CommonUtilities::Vector3f rangedEnemyPosition = myRangedEnemies[j]->GetTransform().GetPosition();
		if ((rangedEnemyPosition - myPlayer.GetTransform().GetPosition()).Length2() < range * range)
		{
			myRangedEnemies[j]->SetWasHitPlayerDash();
		}
	}

	for (unsigned short j = 0; j < myExploderEnemies.Size(); ++j)
	{
		CommonUtilities::Vector3f exploderPosition = myExploderEnemies[j]->GetTransform().GetPosition();
		if ((exploderPosition - myPlayer.GetTransform().GetPosition()).Length2() < range * range)
		{
			myExploderEnemies[j]->SetWasHitPlayerDash();
		}
	}

	myPlayer.SetDoneDashing();
}

void CGameState::CompanionCollisionChecks()
{
	if (myCompanion.GetUltimateAoEIsActive())
	{
		const auto AOEPos = myCompanion.GetTransform().GetPosition();

		for (unsigned short j = 0; j < myRangedEnemies.Size(); ++j)
		{
			CommonUtilities::Vector3f rangedEnemyPosition = myRangedEnemies[j]->GetTransform().GetPosition();
			if ((rangedEnemyPosition - AOEPos).Length() < myMetrics.GetCompanionMetrics().myUltimateMetrics.myCollisionDistanceForRanged)
			{
				myRangedEnemies[j]->SetWasHitCompanionAoE();
			}
		}

		for (unsigned short j = 0; j < myExploderEnemies.Size(); ++j)
		{
			CommonUtilities::Vector3f exploderPosition = myExploderEnemies[j]->GetTransform().GetPosition();

			const auto len = (exploderPosition - AOEPos).Length();
			if (len < myMetrics.GetCompanionMetrics().myUltimateMetrics.myCollisionDistanceForExploder)
			{
				myExploderEnemies[j]->SetWasHitCompanionAoE();
			}
		}

		myCompanion.SetIsFinishedUltimate();
	}

	if (myCompanion.GetIsBasicAttacking())
	{
		const auto companionPos = myCompanion.GetTransform().GetPosition();
		for (unsigned short j = 0; j < myRangedEnemies.Size(); ++j)
		{
			if (!myRangedEnemies[j]->IsValid() || myRangedEnemies[j]->GetIsDead())
			{
				continue;
			}

			const auto enemyPos = myRangedEnemies[j]->GetTransform().GetPosition();

			if ((enemyPos - companionPos).Length2() < 8.f)
			{
				myRangedEnemies[j]->SetWasHitCompanionBasicAttack();
			}
		}

		for (unsigned short j = 0; j < myExploderEnemies.Size(); ++j)
		{
			if (!myExploderEnemies[j]->IsValid() || myExploderEnemies[j]->GetIsDead())
			{
				continue;
			}

			const auto enemyPos = myExploderEnemies[j]->GetTransform().GetPosition();
			if ((enemyPos - companionPos).Length2() < myMetrics.GetCompanionMetrics().myDistanceToEnemyWhenBasicAttacking + 1.f)
			{
				myExploderEnemies[j]->SetWasHitCompanionBasicAttack();
			}
		}
	}
}

void CGameState::RenderDebugEnemiesAroundPlayer(const int aAmountOfExploders, const int aAmountOfRanged)
{
	const float minX = myPlayer.GetTransform().GetPosition().x - 15.f;
	const float minZ = myPlayer.GetTransform().GetPosition().z - 15.f;
	const float maxX = myPlayer.GetTransform().GetPosition().x + 15.f;
	const float maxZ = myPlayer.GetTransform().GetPosition().z + 15.f;

	for (int i = 0; i < aAmountOfExploders; ++i)
	{
		myExploderEnemies.Add(new CExploder());
		CEnemyDecisionTreeController* aiController = new CEnemyDecisionTreeController(0);
		aiController->BindMetricsPtr(myMetrics);
		myExploderEnemies.GetLast()->Init(mySceneID, aiController, -1);

		myExploderEnemies.GetLast()->SetStartTransform({
			CommonUtilities::RandomRange(minX, maxX),
			myPlayer.GetTransform().GetPosition().y,
			CommonUtilities::RandomRange(minZ, maxZ) },
			{ 0.f, 0.f, 0.f }, { 1.f, 1.f, 1.f });

		myExploderEnemies.GetLast()->SetActive(true);
		myExploderEnemies.GetLast()->AddHealthBar(myInGameUICanvas);
	}

	for (int i = 0; i < aAmountOfRanged; ++i)
	{
		myRangedEnemies.Add(new CRangedEnemy());
		CEnemyDecisionTreeController* aiController = new CEnemyDecisionTreeController(CController::EBehaviours_KinFace | CController::EBehaviours_Arrive | CController::EBehaviours_AttackPlayer);
		aiController->BindMetricsPtr(myMetrics);
		myRangedEnemies.GetLast()->Init(mySceneID, aiController, -1);

		myRangedEnemies.GetLast()->SetStartTransform({
			CommonUtilities::RandomRange(minX, maxX),
			myPlayer.GetTransform().GetPosition().y,
			CommonUtilities::RandomRange(minZ, maxZ) },
			{ 0.f, 0.f, 0.f }, { 1.f, 1.f, 1.f });

		myRangedEnemies.GetLast()->SetActive(true);
		myRangedEnemies.GetLast()->AddHealthBar(myInGameUICanvas);
	}
}

void CGameState::UpdateExploderAoE()
{
	for (auto& aoe : myExploderAOEs)
	{
		aoe.Update();
	}
}

void CGameState::UpdateDoors()
{
	for (CDoor& door : myDoors)
	{
		door.Update();
	}
}

void CGameState::HandlePauseMenu()
{
	if (IWorld::Input().IsKeyPressed(Input::Key_Escape, 1))
	{
		myShowPauseMenu = !myShowPauseMenu;
		if (myShowPauseMenu)
		{
			PauseGame();
		}
		else
		{
			UnpauseGame();
		}
	}

	IWorld::Time().SetSpeed(CommonUtilities::Lerp(IWorld::Time().GetSpeed(), myTargetTimeSpeed, IWorld::Time().GetRealDeltaTime() * 10.f));
}

void CGameState::PauseGame()
{
	myTargetTimeSpeed = 0.f;
	ShowCanvas("PauseMenu");
	ShowCanvas("Background");
	IWorld::Input().SetInputLevel(1);
}

void CGameState::UpdateCamera(bool aLerp /*= true*/, bool aDetached /*= false*/)
{
	static CommonUtilities::Vector3f mousePos = myPlayer.GetWorldPoint();

	mousePos = CommonUtilities::Lerp(mousePos, myPlayer.GetWorldPoint(), IWorld::Time().GetDeltaTime()*2.f);

	float ratio = 1.f;
	CommonUtilities::Vector3f target = (myPlayer.GetTransform().GetPosition()*ratio + (1.f - ratio)*mousePos);

	CommonUtilities::Vector3f cameraOffset(23.971f, 28.048f, -27.038f);

	auto cameraPos = target + cameraOffset;

	float screenShake = CPollingStation::GetInstance().GetScreenShake();

	if (aDetached)
	{
		return;
	}
	if (aLerp)
	{
		myMainCamera.GetTransform().SetPosition(CommonUtilities::Lerp(myMainCamera.GetTransform().GetPosition(), cameraPos, IWorld::Time().GetDeltaTime() * 10.f));
	}
	else
	{
		myMainCamera.GetTransform().SetPosition(cameraPos);
	}

	myMainCamera.GetTransform().SetLookDirection(-cameraOffset + CommonUtilities::Vector3f(
		CommonUtilities::Random(),
		CommonUtilities::Random(),
		CommonUtilities::Random()
	) * screenShake * 0.1f);

}

void CGameState::PlayerDeathReceived(CNetMessage * aMessage, CAddressWrapper &)
{
	CNetMessagePlayerDeath* message = static_cast<CNetMessagePlayerDeath*>(aMessage);
	if (message->GetLevelID() == myLevelID)
	{
		CGameObject deathObject;
		deathObject.Init(mySceneID, -1);
		CModelComponent* modelComponent = deathObject.AddComponent<CModelComponent>({ "Assets/Models/networkDeath00/networkDeath00.fbx" });
		deathObject.AddComponent<CAnimationControllerComponent>(modelComponent)->SetLooping(false);
		deathObject.GetTransform().SetPosition(message->GetPosition());
	}
}

void CGameState::PlayerDeathLocationSync(CNetMessage * aMessage, CAddressWrapper &)
{
	CNetMessagePlayerDeathLocationList* message = static_cast<CNetMessagePlayerDeathLocationList*>(aMessage);
	if (message->GetLevelID() == myLevelID)
	{
		for (auto& location : message->GetDeathLocations())
		{
			CGameObject deathObject;
			deathObject.Init(mySceneID, -1);
			CModelComponent* modelComponent = deathObject.AddComponent<CModelComponent>({ "Assets/Models/networkDeath00/networkDeath00.fbx" });
			deathObject.AddComponent<CAnimationControllerComponent>(modelComponent)->SetLooping(false);
			deathObject.GetTransform().SetPosition(location);
		}
	}
}

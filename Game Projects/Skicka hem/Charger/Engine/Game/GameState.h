#pragma once
#include "State.h"
#include "Player.h"
#include "Companion.h"
#include "GameDialogue.h"
#include "TriggerBox.h"
#include "AreaOfEffect.h"
#include "Metrics.h"
#include "ProjectileManager.h"
#include "Door.h"
#include "IClientNetwork.h"

class CExploder;
class CRangedEnemy;
class CActor;

class CGameState : public CState
{
public:
	CGameState(const std::string& aLevelToLoad);
	~CGameState();
	
	bool Init() override;
	EStateUpdate Update() override;

	void CleanUpDeadEnemies();

	void HandleLevelReset();

	void OnEnter() override;
	void OnLeave() override;

	virtual void PushText(const std::string& aText) override;
	virtual void RunDialogue(const std::string& aDialogue) override;
	virtual void OnLoadFinished(JsonDocument& aDoc, std::map<int, CGameObject>& aGameObjects) override;

	void SwapScene(const std::string& aLevelName) override;

	void ForceUnpauseGame() override;
	void UnpauseGame() override;
	void SetTargetDeltaSpeed(const float aSpeed) { myTargetTimeSpeed = aSpeed; };

	const CTransform& GetPlayerTransform();
	CPlayer& GetPlayer() { return myPlayer; }
	CCompanion& GetCompanion() { return myCompanion; }
	const CActor* GetClosestEnemy(const CommonUtilities::Vector3f& aPosition);
	CAreaOfEffect* GetClosestAcid(const CommonUtilities::Vector3f& aPosition);

	const CAreaOfEffect& GetPlayerAcid();
	
	void RemoveExploderAoE(const CAreaOfEffect* aAoE);
	void AddExploderAOEAtPosition(const CommonUtilities::Vector3f& aPosition);
	bool CalculateIfEnemiesGroupedUp();
	void SetUltimateFinished();

	const CommonUtilities::Vector3f& GetCenterPositionOfGroupedUpEnemies();
	bool GetCanCompanionUltimate() const;
	bool GetHasCenterPositionOfGroupedUpEnemies() const;

	void SetShouldCameraFollow(const bool aBool);
	const bool GetShouldCameraFollow() const;
private:
	void RespawnEnemies();
	void UpdateCamera(bool aLerp = true, bool aDetached = false);

	void PlayerDeathReceived(CNetMessage* aMessage, CAddressWrapper&);
	void PlayerDeathLocationSync(CNetMessage* aMessage, CAddressWrapper&);

	void ExploderAoECollisionChecks();
	void PlayerAoECollisionChecks();
	void PlayerDashCollisionChecks();
	void CompanionCollisionChecks();

	void RenderDebugEnemiesAroundPlayer(const int aAmountOfExploders, const int aAmountOfRanged);
	void UpdateExploderAoE();
	void UpdateDoors();

	void HandlePauseMenu();
	void PauseGame();

	CMetrics myMetrics;

	CPlayer myPlayer;
	CCompanion myCompanion;

	CommonUtilities::GrowingArray<CExploder*, int> myExploderEnemies;
	CommonUtilities::GrowingArray<CRangedEnemy*, int> myRangedEnemies;
	CommonUtilities::GrowingArray<CTriggerBox> myTriggers;
	CommonUtilities::GrowingArray<CAreaOfEffect, int> myExploderAOEs;
	CommonUtilities::GrowingArray<CDoor, int> myDoors;

	std::vector<CActor*> myEnemiesAsActorObjects;
	std::string myLevelToLoad;

	CGameDialogue myGameDialogue;
	CCanvasComponent* myInGameUICanvas;

	CommonUtilities::Vector3f myCenterPositionOfGroupedUpEnemies;

	CProjectileManager myProjectileManager;

	CommonUtilities::Vector3f myPlayerStartPosition;

	CommonUtilities::Vector3f myCameraLookAtTarget;

	int myLevelID;
	bool myShowPauseMenu;
	bool myIsInited = false;
	bool myCameraDetached;
	bool myHasCenterPositionOfGroupedUpEnemies;
	bool myShouldSwapScene;
	bool myIsFading;

	float myTargetTimeSpeed;
	float myRespawnFade;

	int myNumberOfKilledEnemies;
	int myCurrentDoor = 0;

	std::string myNextLevel;


	bool myShouldPlayEndDialogue;
};


#pragma once

#include "GrowingArray.h"

#include "State.h"
#include "StateStack.h"
#include "CppScriptFunctions.h"

class CGameWorld
{
public:
	CGameWorld();
	~CGameWorld();

	void Init();
	void Update();
	CStateStack& GetStateStack();
	ID_T(CScene) GetCurrentSceneID();

private:
	void LoadAudioSettings();
	void TestMemoryLeaks();
	CStateStack myStateStack;
	static CGameWorld* ourGameWorld;

	CCppScriptFunctions myScriptFunctions;
};


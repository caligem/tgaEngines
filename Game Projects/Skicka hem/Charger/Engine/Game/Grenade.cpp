#include "stdafx.h"
#include "Grenade.h"
#include "PollingStation.h"
#include "Mathf.h"
#include "Random.h"
#include <iostream>
#include "Metrics.h"
#include "AudioManager.h"

#include "IWorld.h"
#include "PathFinder.h"

CMetrics* CGrenade::ourMetrics = nullptr;

CGrenade::CGrenade()
	: myGrenadeState(eGrenadeState::eNothing), myCurrentGrenadeThrowingTime(0.f)
{
}


CGrenade::~CGrenade()
{
}

void CGrenade::BindMetricsPtr(CMetrics& aMetrics)
{
	ourMetrics = &aMetrics;

	CAreaOfEffect::BindMetricsPtr(aMetrics);
}

void CGrenade::Init(ID_T(CScene) aSceneID)
{
	myShouldThrowGrenade = false;
	myIsThrowMovementDelayActive = false;
	myThrowMovementDelayTimer = 0.f;

	CGameObject::Init(aSceneID);
	AddComponent<CModelComponent>({ "Assets/Models/projectiles/grenade.fbx" });
	GetTransform().SetScale({ 0.3f, 0.3f, 0.3f }); // TEMP because fbx is weirdly big

	myGrenadeCooldownTimer = 0.f;
	myIsOnCooldown = false;

	SetActive(false);
	
	myAOE.Init(aSceneID, true, "Assets/Models/hazards/hazard00/hazard00.fbx", nullptr, 3.f, true);

	myMinCircle.Init(aSceneID);
	myMinCircle.AddComponent<CModelComponent>({ "Assets/Models/abilityRange/abilityRange.fbx" })->GetMaterial().SetShouldRenderDepth(false);
	myMinCircle.SetActive(false);

	myMaxCircle.Init(aSceneID);
	myMaxCircle.AddComponent<CModelComponent>({ "Assets/Models/abilityRange2/abilityRange2.fbx" })->GetMaterial().SetShouldRenderDepth(false);
	myMaxCircle.SetActive(false);

	myCircleAroundMouse.Init(aSceneID);
	myCircleAroundMouse.AddComponent<CModelComponent>({ "Assets/Models/grenadeIndicator/grenadeIndicator.fbx" })->GetMaterial().SetShouldRenderDepth(false);
	myCircleAroundMouse.GetTransform().SetScale({ 2.5f, 2.5f, 2.5f });
	myCircleAroundMouse.SetActive(false);

	myStreakComponent = AddComponent<CStreakComponent>("Assets/Particles/Sprites/playerStreak.dds",
		CommonUtilities::Vector4f(0.3f, 0.8f, 0.4f, 1.f),
		CommonUtilities::Vector4f(0.3f, 0.8f, 0.6f, 0.f),
		0.2f,
		0.1f,
		0.4f
	);
}

const float CGrenade::GetCooldownInPercent() const
{
	return myGrenadeCooldownTimer / ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myCooldown;
}

void CGrenade::ParabolicTrajectory()
{
	//n�r t = 0 �r vi p� startPos, n�r t = 1 �r p� target
	myCurrentGrenadeThrowingTime += IWorld::Time().GetDeltaTime();

	const float t = myCurrentGrenadeThrowingTime / ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myAirTime;

	GetTransform().SetPosition(CommonUtilities::Lerp(
		myStartGrenadePosition,
		myTargetGrenadePosition,
		t));

	const float maxHeight = 8.f / ((myStartGrenadePosition-myTargetGrenadePosition).Length() / 2.f);
	const auto height = (-std::pow(((2 * t) - 1), 2) + 1);

	GetTransform().Move({ 0.f, height * maxHeight, 0.f });
}

void CGrenade::Update(const CommonUtilities::Vector3f& aMousePos)
{
	if (myIsOnCooldown)
	{
		if (IWorld::Input().IsKeyPressed(Input::Key_Q))
		{
			AM.PlayNewInstance("SFX/Player/Ability/Denied");
		}

		myGrenadeCooldownTimer += IWorld::Time().GetDeltaTime();
		if (myGrenadeCooldownTimer >= ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myCooldown)
		{
			myGrenadeCooldownTimer = 0.f;
			myGrenadeState = eGrenadeState::eNothing;
			myIsOnCooldown = false;
		}
	}

	if (myIsThrowMovementDelayActive)
	{
		if (myThrowMovementDelayTimer >= ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myThrowMovementDelay)
		{
			myThrowMovementDelayTimer = 0.f;
			myIsThrowMovementDelayActive = false;
		}
		else
		{
			myThrowMovementDelayTimer += IWorld::Time().GetDeltaTime();
		}
	}

	switch (myGrenadeState)
	{
		case(eGrenadeState::eChooseTargetMode) :
		{
			myMaxCircle.SetActive(true);
			myMinCircle.SetActive(true);
			myCircleAroundMouse.SetActive(true);

			auto& grenadePos = DrawGrenadeChooseTargetModeCircles(aMousePos);

			CommonUtilities::Vector3f closestPointOnNavmesh;
			IWorld::Path().RayHitLocation(grenadePos + CommonUtilities::Vector3f::Up, { 0.f, -1.f, 0.f }, closestPointOnNavmesh);

			myCircleAroundMouse.GetTransform().SetPosition(closestPointOnNavmesh);

			if (myShouldThrowGrenade)
			{
				myGrenadeState = eGrenadeState::eGrenadeMoving;
				SetActive(true);
				myTargetGrenadePosition = closestPointOnNavmesh;
				myStartGrenadePosition = CPollingStation::GetInstance().GetPlayerTransform().GetPosition() + CommonUtilities::Vector3f({0.f, 1.3f, 0.f});
				GetTransform().SetPosition(myStartGrenadePosition);
				AM.PlayNewInstance("SFX/Player/Ability/Throw");
				myIsThrowMovementDelayActive = true;
				myIsOnCooldown = true;
				myShouldThrowGrenade = false;
				myStreakComponent->SetLifeTime(0.4f);
				myStreakComponent->ClearStreak();
			}
		}
		break;
		{
		case(eGrenadeState::eGrenadeMoving) :

			myMaxCircle.SetActive(false);
			myMinCircle.SetActive(false);
			myCircleAroundMouse.SetActive(false);

			ParabolicTrajectory();

			if (myCurrentGrenadeThrowingTime >= ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myAirTime)
			{
				myStreakComponent->SetLifeTime(0.f);
				SetActive(false);
				myAOE.SetActive(true);
				myAOE.GetTransform().SetPosition(myTargetGrenadePosition);
				
				float randomYRotation = CommonUtilities::RandomRange(0.0f, CommonUtilities::Pif * 2.f);
				myAOE.GetTransform().SetRotation({ 0.f, randomYRotation, 0.f });

				myGrenadeState = eGrenadeState::eAOE;
				myAOE.Start();
				myCurrentGrenadeThrowingTime = 0.f;

				AM.PlayNewInstance("SFX/Player/Ability/Grenade Explode");
			}
		}
		break;

		case (eGrenadeState::eAOE) :
		{
			myAOE.Update();
		}
		break;

	}
}

bool CGrenade::GetIsDoneThrowingGrenade() const
{
	if (myGrenadeState == eGrenadeState::eNothing || myGrenadeState == eGrenadeState::eAOE)
	{
		return false;
	}

	return true;
}

void CGrenade::Reset()
{
	CancelAim();
	SetActive(false);
	myAOE.SetActive(false);
	myAOE.Reset();
	myCurrentGrenadeThrowingTime = 0.f;
	myGrenadeCooldownTimer = 0.f;
	myGrenadeState = eGrenadeState::eNothing;
	myIsOnCooldown = false;
}

void CGrenade::ToggleAim()
{
	if (myGrenadeState == eGrenadeState::eChooseTargetMode)
	{
		myGrenadeState = eGrenadeState::eNothing;

		myMinCircle.SetActive(false);
		myMaxCircle.SetActive(false);
		myCircleAroundMouse.SetActive(false);
	}
	else if(myGrenadeState == eGrenadeState::eNothing)
	{
		myGrenadeState = eGrenadeState::eChooseTargetMode;
	}
}

void CGrenade::CancelAim()
{
	if (myGrenadeState == eGrenadeState::eChooseTargetMode)
	{
		myGrenadeState = eGrenadeState::eNothing;

		myMaxCircle.SetActive(false);
		myMinCircle.SetActive(false);
		myCircleAroundMouse.SetActive(false);
	}
}

const CAreaOfEffect& CGrenade::GetAreaOfEffectObject() const
{
	return myAOE;
}

const CommonUtilities::Vector3f CGrenade::DrawGrenadeChooseTargetModeCircles(const CommonUtilities::Vector3f& aMousePos)
{
	const auto playerPos = CPollingStation::GetInstance().GetPlayerTransform().GetPosition();
	const CommonUtilities::Vector3f direction = (aMousePos - playerPos);

	const float minRadius = ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myMinRadius;
	const float maxRadius = ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myMaxRadius;

	// ----------------------MINRADIUS ----------------------
	const CommonUtilities::Vector3f minPos = { playerPos.x + (minRadius * direction.GetNormalized().x), aMousePos.y, playerPos.z + (minRadius * direction.GetNormalized().z) };
	IWorld::SetDebugColor({ 0.0f, 0.0f, 1.0f, 1.0f });
	myMinCircle.GetTransform().SetScale({ minRadius, minRadius, minRadius });
	myMinCircle.GetTransform().SetPosition(playerPos);
	// ------------------------------------------------------

	// ----------------------MAXRADIUS ----------------------
	const CommonUtilities::Vector3f maxPos = { playerPos.x + (maxRadius * direction.GetNormalized().x), aMousePos.y, playerPos.z + (maxRadius * direction.GetNormalized().z) };
	IWorld::SetDebugColor({ 0.0f, 1.0f, 0.0f, 1.0f });
	myMaxCircle.GetTransform().SetScale({ maxRadius, maxRadius, maxRadius });
	myMaxCircle.GetTransform().SetPosition(playerPos);
	// ------------------------------------------------------

	if (direction.Length() < minRadius)
	{
		myCircleAroundMouse.GetTransform().SetPosition(minPos);
		//IWorld::DrawDebugWireSphere(minPos, { 0.2f, 0.2f, 0.2f });
		return minPos;
	}
	else if (direction.Length() > maxRadius)
	{
		myCircleAroundMouse.GetTransform().SetPosition(maxPos);
		//IWorld::DrawDebugWireSphere(maxPos, { 0.2f, 0.2f, 0.2f });
		return maxPos;
	}
	else
	{
		myCircleAroundMouse.GetTransform().SetPosition(aMousePos);
		return aMousePos;
	}
}
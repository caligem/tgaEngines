#pragma once
#include "GameObject.h"
#include "AreaOfEffect.h"

class CMetrics;

class CGrenade : public CGameObject
{
public:
	CGrenade();
	~CGrenade();

	enum class eGrenadeState
	{
		eChooseTargetMode,
		eGrenadeMoving,
		eAOE,
		eNothing,
		eCount
	};

	static void BindMetricsPtr(CMetrics& aMetrics);

	void Init(ID_T(CScene) aSceneID);
	void Update(const CommonUtilities::Vector3f& aMousePos);
	void Reset();

	void ToggleAim();
	void CancelAim();
	void SetGrenadeState(const eGrenadeState& aGrenadeState) { myGrenadeState = aGrenadeState; };
	
	const CAreaOfEffect& GetAreaOfEffectObject() const;
	const eGrenadeState GetGrenadeState() const { return myGrenadeState; }
	inline const bool IsOnCooldown() const { return myIsOnCooldown; }
	const float GetCooldownInPercent() const;
	bool GetIsDoneThrowingGrenade() const;

	void SetShouldThrowGrenade() { myShouldThrowGrenade = true; }
	inline const bool IsThrowDelayActive() const { return myIsThrowMovementDelayActive; }
private:
	void ParabolicTrajectory();
	const CommonUtilities::Vector3f DrawGrenadeChooseTargetModeCircles(const CommonUtilities::Vector3f& aMousePos);
	
	static CMetrics* ourMetrics;

	CGameObject myMinCircle;
	CGameObject myMaxCircle;
	CGameObject myCircleAroundMouse;

	CommonUtilities::Vector3f myStartGrenadePosition;
	CommonUtilities::Vector3f myTargetGrenadePosition;
	
	eGrenadeState myGrenadeState;
	CAreaOfEffect myAOE; 

	CStreakComponent* myStreakComponent;

	float myCurrentGrenadeThrowingTime;
	float myGrenadeCooldownTimer;
	bool myIsOnCooldown;
	bool myShouldThrowGrenade;
	bool myIsThrowMovementDelayActive;
	float myThrowMovementDelayTimer;
};


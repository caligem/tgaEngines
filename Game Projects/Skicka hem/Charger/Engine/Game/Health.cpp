#include "stdafx.h"
#include "Health.h"


CHealth::CHealth()
	: myIsDead(false)
{
}


CHealth::~CHealth()
{
}

void CHealth::Init(const int aMaxHealth)
{
	myMaxHealth = aMaxHealth;
	myCurrentHealth = myMaxHealth;
}

void CHealth::TakeDamage(const int& aDamage)
{
	myCurrentHealth -= aDamage;

	if (myCurrentHealth <= 0)
	{
		myIsDead = true;
		myCurrentHealth = 0;
	}
}

void CHealth::IncreaseHealth(const int& aIncrease)
{
	myCurrentHealth += aIncrease;

	if (myCurrentHealth > myMaxHealth)
	{
		myCurrentHealth = myMaxHealth;
	}
}

int CHealth::GetCurrentHealth() const
{
	return myCurrentHealth;
}

bool CHealth::GetIsDead() const
{
	return myIsDead;
}

void CHealth::SetIsAlive()
{
	myIsDead = false;
}

void CHealth::SetIsDead()
{
	myIsDead = true;
}

int CHealth::GetMaxHealth() const
{
	return myMaxHealth;
}

float CHealth::GetCurrentHealthInPercent()
{
	return static_cast<float>(myCurrentHealth) / static_cast<float>(myMaxHealth);
}

#pragma once

class CHealth
{
public:
	CHealth();
	~CHealth();

	void Init(const int aMaxHealth);
	void TakeDamage(const int& aDamage);
	void IncreaseHealth(const int& aIncrease);
	int GetCurrentHealth() const;
	bool GetIsDead() const;
	void SetIsAlive();
	void SetIsDead();
	int GetMaxHealth() const;
	float GetCurrentHealthInPercent();


private:
	int myCurrentHealth;
	bool myIsDead;
	int myMaxHealth;
};


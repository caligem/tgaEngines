#include "stdafx.h"
#include "HusseinState.h"

#include "CameraDataWrapper.h"

#include "PathFinder.h"

CHusseinState::CHusseinState()
{
}

CHusseinState::~CHusseinState()
{
}

bool CHusseinState::Init()
{
	CState::Init();

	LoadFromFile("Assets/Levels/hussein.json");

	myLight.Init(mySceneID);

	/*
	light = myLight.AddComponent<CLightComponent>();
	light->SetType(CLightComponent::ELightType_Point);
	light->CastShadows(true);
	light->SetColor({ 1.f, 0.6f, 0.2f });
	light->SetRange(15.f);
	light->SetIntensity(1.f);

	myLight.GetTransform().SetPosition({ 10.f, 2.f, 7.f });
	*/


	/*
	START_TIMER(navmeshload);
	IWorld::Path().LoadNavmesh("Assets/Level2.fbx");
	GENERAL_LOG(CONCOL_DEFAULT, "Time to load navmesh: %f", GET_TIME(navmeshload));
	*/

	return true;
}

EStateUpdate CHusseinState::Update()
{
	Input::CInputManager& input = IWorld::Input();

	input;

	return EDoNothing;
}

void CHusseinState::OnEnter()
{
	SetActiveScene();
}

void CHusseinState::OnLeave()
{
}

void CHusseinState::ClickOnNavmesh(CommonUtilities::Vector3f& aContactPoint)
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Matrix44f viewProjInv = IWorld::GetSavedCameraBuffer().myInvertedViewProjection;

	CommonUtilities::Vector2f mousePos = {
		input.GetMousePosition().x / IWorld::GetWindowSize().x,
		input.GetMousePosition().y / IWorld::GetWindowSize().y
	};
	mousePos.x = mousePos.x * 2.f - 1.f;
	mousePos.y = (1.f - mousePos.y) * 2.f - 1.f;

	CommonUtilities::Vector4f rayOrigin = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 0.f, 1.f) * viewProjInv;
	rayOrigin /= rayOrigin.w;
	CommonUtilities::Vector4f rayEnd = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 1.f, 1.f) * viewProjInv;
	rayEnd /= rayEnd.w;

	CommonUtilities::Vector3f rayDir = rayEnd - rayOrigin;
	rayDir.Normalize();

	if (IWorld::Path().RayHitLocation(rayOrigin, rayDir, aContactPoint))
	{
		GENERAL_LOG(CONCOL_DEFAULT, "Hit something");

		RecalculatePath();
		return;
	}
}

void CHusseinState::RecalculatePath()
{
	IWorld::Path().RequestPath(myFirstPoint, mySecondPoint, myPathTicket);
}

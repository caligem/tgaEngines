#pragma once

#include "State.h"

#include "PathFinder.h"

class CHusseinState : public CState
{
public:
	CHusseinState();
	~CHusseinState();

	virtual bool Init() override;

	virtual EStateUpdate Update() override;

	virtual void OnEnter() override;

	virtual void OnLeave() override;
	void ClickOnNavmesh(CommonUtilities::Vector3f& aContactPoint);
	void RecalculatePath();
private:
	CGameObject myLight;
	CLightComponent* light;

	CommonUtilities::Vector3f myFirstPoint;
	CommonUtilities::Vector3f mySecondPoint;

	PathTicket myPathTicket;
};


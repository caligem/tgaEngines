#include "stdafx.h"
#include "MainMenuState.h"

#include "IWorld.h"
#include "SceneManager.h"
#include "StateStack.h"
#include "ShowroomState.h"
#include "AudioManager.h"
//#include "PollingStation.h"
//#include "GameState.h"

bool CMainMenuState::ourWonGame = false;

CMainMenuState::CMainMenuState()
{
}


CMainMenuState::~CMainMenuState()
{
}

bool CMainMenuState::Init()
{
	CState::Init();
	LoadFromFile("Assets/Levels/MainMenu.json");

	IWorld::GetSceneManager().GetSceneAt(mySceneID)->SetSkybox("Assets/CubeMaps/blackSkybox.dds");

	return true;
}

EStateUpdate CMainMenuState::Update()
{
	if (myShouldPushNewState)
	{
		myShouldPushNewState = false;
		CShowroomState* showroom = new CShowroomState();
		ourStateStack->PushMainStateWithoutOnEnter(showroom);
		showroom->Init();
		showroom->OnEnter();
	}

	//no
	/*if (myCanvases["HowToPlayPopup"]->GetIsRendered() &&
		(IWorld::Input().IsKeyPressed(Input::Key_Space) || 
		IWorld::Input().IsKeyPressed(Input::Key_Return)))
	{
		HideCanvas("HowToPlayPopup");

		CGameState* gameState = new CGameState("Assets/Levels/Level1.json");
		ourStateStack->PushMainStateWithoutOnEnter(gameState);
		gameState->Init();
		gameState->OnEnter();
		ourStateStack->StartFadeOut();
	}*/

	return EStateUpdate::EDoNothing;
}

void CMainMenuState::OnEnter()
{
	SetActiveScene();
	AM.StopAll(false);
	AM.Play("Music/Music_MainTheme");
	if (ourWonGame)
	{
		ForceShowCanvas("Credits");
		ForceHideCanvas("MainMenu");
		ForceHideCanvas("LevelSelect");
	}
	else
	{
		ForceShowCanvas("MainMenu");
	}

	ourWonGame = false;
	myCanvases["Options"]->OnEnter();
}

void CMainMenuState::OnLeave()
{
	AM.Stop("Music/Music_MainTheme", false);
	ForceHideCanvas("LevelSelect");
	ForceHideCanvas("HowToPlayPopup");
}

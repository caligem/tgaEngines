#include "stdafx.h"
#include "MemoryLeakTestState.h"


CMemoryLeakTestState::CMemoryLeakTestState()
{
}


CMemoryLeakTestState::~CMemoryLeakTestState()
{
}

bool CMemoryLeakTestState::Init()
{
	CState::Init();
	
	for (int i = 0; i < 1000; ++i)
	{
		CGameObject testObject;
		testObject.Init(mySceneID);
		testObject.AddComponent<CAudioListenerComponent>();
		testObject.AddComponent<CAudioSourceComponent>({ CommonUtilities::Vector3f(1.f, 0.f, 0.f) });
		testObject.AddComponent<CCameraComponent>({ 80.f, 3.f, 2.f, 1.f });
		testObject.AddComponent<CModelComponent>({ "Assets/Models/statueBigHead/statueBigHead.fbx" });
		testObject.AddComponent<CParticleSystemComponent>({ });
		testObject.AddComponent<CLightComponent>();
		testObject.AddComponent<CStreakComponent>({ "Assets/Particles/Sprites/roundParticle.dds" });
		testObject.AddComponent<CSpriteComponent>({ "Assets/Sprites/ffgLogo.dds" });
		testObject.AddComponent<CTextComponent>({ "Assets/Fonts/Mono/Mono" });
		testObject.AddComponent<CCanvasComponent>({ CCanvasComponent::ERenderMode_ScreenSpaceCamera,{ 1920.f, 1080.f } });
		testObject.Destroy();
	}

	return true;
}

EStateUpdate CMemoryLeakTestState::Update()
{
	return EStateUpdate::EPop_Main;
}

void CMemoryLeakTestState::OnEnter()
{
	SetActiveScene();
}

void CMemoryLeakTestState::OnLeave()
{
}

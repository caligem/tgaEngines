#include "stdafx.h"
#include "Metrics.h"

CMetrics::CMetrics()
{
}

CMetrics::~CMetrics()
{
}

void CMetrics::LoadMetricsFromFile(const std::wstring& aMetricsDoc)
{
	JsonDocument characterMetrics;
	std::string file(aMetricsDoc.begin(), aMetricsDoc.end());
	characterMetrics.LoadFile(file.c_str());
	
	if (characterMetrics.Find("myPlayerMetrics"))
	{
		auto playerObj = characterMetrics["myPlayerMetrics"];

		myPlayerMetrics.myMovementSpeed = playerObj["myMovementSpeed"].GetFloat();
		myPlayerMetrics.myAttackCooldown = playerObj["myAttackCooldown"].GetFloat();
		myPlayerMetrics.myAttackDamage = playerObj["myAttackDamage"].GetInt();
		myPlayerMetrics.myProjectileSpeed = playerObj["myProjectileSpeed"].GetFloat();
		myPlayerMetrics.myMaxHealth = playerObj["myMaxHealth"].GetInt();
		myPlayerMetrics.myProjectileCollisionDistanceForRanged = playerObj["myProjectileCollisionDistanceForRanged"].GetFloat();
		myPlayerMetrics.myProjectileCollisionDistanceForExploder = playerObj["myProjectileCollisionDistanceForExploder"].GetFloat();
		myPlayerMetrics.myClickArrowActiveTime = playerObj["myClickArrowActiveTime"].GetFloat();

		if (playerObj.Find("myGrenadeMetrics"))
		{
			auto grenadeObj = playerObj["myGrenadeMetrics"];

			myPlayerMetrics.myGrenadeMetrics.myAirTime = grenadeObj["myAirTime"].GetFloat();
			myPlayerMetrics.myGrenadeMetrics.myDamage = grenadeObj["myDamage"].GetInt();
			myPlayerMetrics.myGrenadeMetrics.myMinRadius = grenadeObj["myMinRadius"].GetFloat();
			myPlayerMetrics.myGrenadeMetrics.myMaxRadius = grenadeObj["myMaxRadius"].GetFloat();
			myPlayerMetrics.myGrenadeMetrics.myCooldown = grenadeObj["myCooldown"].GetFloat();
			myPlayerMetrics.myGrenadeMetrics.myThrowMovementDelay = grenadeObj["myThrowMovementDelay"].GetFloat();

			myPlayerMetrics.myGrenadeMetrics.myAoEHealAmount = grenadeObj["myAoEHealAmount"].GetInt();
			myPlayerMetrics.myGrenadeMetrics.myAoEMaxMsBetweenDamageEnemies = grenadeObj["myAoEMaxMsBetweenDamageEnemies"].GetInt();
			myPlayerMetrics.myGrenadeMetrics.myAoEMaxMsBetweenHealPlayer = grenadeObj["myAoEMaxMsBetweenHealPlayer"].GetInt();
			myPlayerMetrics.myGrenadeMetrics.myAoECollisionDistance = grenadeObj["myAoECollisionDistance"].GetFloat();
		}
		if (playerObj.Find("myDashMetrics"))
		{
			auto dashObj = playerObj["myDashMetrics"];
			myPlayerMetrics.myDashMetrics.myMaxCharges = dashObj["myMaxCharges"].GetInt();
			myPlayerMetrics.myDashMetrics.myRechargeTime = dashObj["myRechargeTime"].GetFloat();
			myPlayerMetrics.myDashMetrics.myImpactDelay = dashObj["myImpactDelay"].GetFloat();
			myPlayerMetrics.myDashMetrics.myDashRange = dashObj["myDashRange"].GetFloat();
			myPlayerMetrics.myDashMetrics.myDashAoERadius = dashObj["myDashAoERadius"].GetFloat();
			myPlayerMetrics.myDashMetrics.myStunTime = dashObj["myStunTime"].GetFloat();
		}
	}
	if (characterMetrics.Find("myRangedMetrics"))
	{
		auto rangedObj = characterMetrics["myRangedMetrics"];

		myRangedMetrics.myAttackRange = rangedObj["myAttackRange"].GetFloat();
		myRangedMetrics.myAttackSpeed = rangedObj["myAttackSpeed"].GetFloat();
		myRangedMetrics.myProjectileSpeed = rangedObj["myProjectileSpeed"].GetFloat();
		myRangedMetrics.myDamage = rangedObj["myDamage"].GetInt();
		myRangedMetrics.myMovementSpeed = rangedObj["myMovementSpeed"].GetFloat();
		myRangedMetrics.myMaxHealth = rangedObj["myMaxHealth"].GetInt();
		myRangedMetrics.myAggroRange = rangedObj["myAggroRange"].GetFloat();
		myRangedMetrics.myProjectileCollisionDistance = rangedObj["myProjectileCollisionDistance"].GetFloat();
		myRangedMetrics.mySlowDownRadius = rangedObj["mySlowDownRadius"].GetFloat();
	}
	if (characterMetrics.Find("myExploderMetrics"))
	{
		auto exploderObj = characterMetrics["myExploderMetrics"];

		myExploderMetrics.myMovementSpeed = exploderObj["myMovementSpeed"].GetFloat();
		myExploderMetrics.myAttackRange = exploderObj["myAttackRange"].GetFloat();
		myExploderMetrics.myMaxHealth = exploderObj["myMaxHealth"].GetInt();
		myExploderMetrics.myAggroRange = exploderObj["myAggroRange"].GetFloat();
		myExploderMetrics.myAoEDamage = exploderObj["myAoEDamage"].GetInt();
		myExploderMetrics.myAoEMaxMsBetweenDamagePlayer = exploderObj["myAoEMaxMsBetweenDamagePlayer"].GetInt();
		myExploderMetrics.myAoECollisionDistance = exploderObj["myAoECollisionDistance"].GetFloat();
		myExploderMetrics.mySlowDownRadius = exploderObj["mySlowDownRadius"].GetFloat();
	}
	if (characterMetrics.Find("myCompanionMetrics"))
	{
		auto companionObj = characterMetrics["myCompanionMetrics"];

		myCompanionMetrics.myMaxAcceleration = companionObj["myMaxAcceleration"].GetFloat();
		myCompanionMetrics.myMaxSpeed = companionObj["myMaxSpeed"].GetFloat();
		myCompanionMetrics.myAttackRange = companionObj["myAttackRange"].GetFloat();
		myCompanionMetrics.myMinDistanceToPlayer = companionObj["myMinDistanceToPlayer"].GetFloat();
		myCompanionMetrics.mySeekAcidRange = companionObj["mySeekAcidRange"].GetFloat();
		myCompanionMetrics.mySlowDownRadius = companionObj["mySlowDownRadius"].GetFloat();
		myCompanionMetrics.myDistanceToEnemyWhenBasicAttacking = companionObj["myDistanceToEnemyWhenBasicAttacking"].GetFloat();
		myCompanionMetrics.myMsBetweenBasicAttackDamage = companionObj["myMsBetweenBasicAttackDamage"].GetInt();
		myCompanionMetrics.myBasicAttackDamage = companionObj["myBasicAttackDamage"].GetInt();

		if (companionObj.Find("myUltimateMetrics"))
		{
			auto ultiMetrics = companionObj["myUltimateMetrics"];

			myCompanionMetrics.myUltimateMetrics.myMinDistanceToTargetPos = ultiMetrics["myMinDistanceToTargetPos"].GetFloat();
			myCompanionMetrics.myUltimateMetrics.myCooldown = ultiMetrics["myCooldown"].GetFloat();
			myCompanionMetrics.myUltimateMetrics.myAoELifeTime = ultiMetrics["myAoELifeTime"].GetFloat();
			myCompanionMetrics.myUltimateMetrics.myAmountOfEnemiesGroupedUp = ultiMetrics["myAmountOfEnemiesGroupedUp"].GetFloat();
			myCompanionMetrics.myUltimateMetrics.myMaxDistanceBetweenGroupedUpEnemies = ultiMetrics["myMaxDistanceBetweenGroupedUpEnemies"].GetFloat();
			myCompanionMetrics.myUltimateMetrics.myMaxDistanceToPlayer = ultiMetrics["myMaxDistanceToPlayer"].GetFloat();
			myCompanionMetrics.myUltimateMetrics.myMaxDistanceToEnemy = ultiMetrics["myMaxDistanceToEnemy"].GetFloat();
			myCompanionMetrics.myUltimateMetrics.myCollisionDistanceForExploder = ultiMetrics["myCollisionDistanceForExploder"].GetFloat();
			myCompanionMetrics.myUltimateMetrics.myCollisionDistanceForRanged = ultiMetrics["myCollisionDistanceForRanged"].GetFloat();
			myCompanionMetrics.myUltimateMetrics.myAoEMaxMsBetweenDamage = ultiMetrics["myAoEMaxMsBetweenDamage"].GetInt();
			myCompanionMetrics.myUltimateMetrics.myDamage = ultiMetrics["myDamage"].GetInt();
		}
	}

	GAMEPLAY_LOG(CONCOL_ERROR, "TESTING");
}

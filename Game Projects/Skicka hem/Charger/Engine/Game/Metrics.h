#pragma once
#include "JsonUtility.h"

class CMetrics
{
	struct SPlayerMetrics
	{
		float myMovementSpeed;
		float myAttackCooldown;
		int myAttackDamage;
		float myProjectileSpeed;
		int myMaxHealth;
		float myProjectileCollisionDistanceForRanged;
		float myProjectileCollisionDistanceForExploder;
		float myClickArrowActiveTime;

		struct SGrenadeMetrics
		{
			float myAirTime;
			int myDamage;
			float myMinRadius;
			float myMaxRadius;
			float myCooldown;
			float myThrowMovementDelay;

			int myAoEHealAmount;
			int myAoEMaxMsBetweenDamageEnemies;
			int myAoEMaxMsBetweenHealPlayer;
			float myAoECollisionDistance;
		} myGrenadeMetrics;

		struct SDashMetrics
		{
			int myMaxCharges;
			float myRechargeTime;
			float myImpactDelay;
			float myDashRange;
			float myDashAoERadius;
			float myStunTime;
		} myDashMetrics;
	} myPlayerMetrics;

	struct SRangedMetrics
	{
		float myAttackRange;
		float myAttackSpeed;
		float myProjectileSpeed;
		int myDamage;
		float myMovementSpeed;
		int myMaxHealth;
		float myAggroRange;
		float myProjectileCollisionDistance;
		float mySlowDownRadius;
	} myRangedMetrics;

	struct SExploderMetrics
	{
		float myMovementSpeed;
		float myAttackRange;
		int myMaxHealth;
		float myAggroRange;
		int myAoEMaxMsBetweenDamagePlayer;
		int myAoEDamage;
		float myAoECollisionDistance;
		float mySlowDownRadius;
	} myExploderMetrics;

	struct SCompanionMetrics
	{
		float myMaxAcceleration;
		float myMaxSpeed;
		float myAttackRange;
		float myMinDistanceToPlayer;
		float mySeekAcidRange;
		float mySlowDownRadius;
		float myDistanceToEnemyWhenBasicAttacking;
		int myMsBetweenBasicAttackDamage;
		int myBasicAttackDamage;

		struct SUltimateMetrics
		{
			float myMinDistanceToTargetPos;
			float myCooldown;
			float myAoELifeTime;
			float myAmountOfEnemiesGroupedUp;
			float myMaxDistanceBetweenGroupedUpEnemies;
			float myMaxDistanceToPlayer;
			float myMaxDistanceToEnemy;
			float myCollisionDistanceForExploder;
			float myCollisionDistanceForRanged;
			int myAoEMaxMsBetweenDamage;
			int myDamage;
		} myUltimateMetrics;
	} myCompanionMetrics;

public:
	CMetrics();
	~CMetrics();

	void LoadMetricsFromFile(const std::wstring& aMetricsDoc);
	const SPlayerMetrics& GetPlayerMetrics() const { return myPlayerMetrics; }
	const SRangedMetrics& GetRangedMetrics() const { return myRangedMetrics; }
	const SExploderMetrics& GetExploderMetrics() const { return myExploderMetrics; }
	const SCompanionMetrics& GetCompanionMetrics() const { return myCompanionMetrics; }
};


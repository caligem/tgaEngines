#include "stdafx.h"
#include "OneHitDamageHandler.h"


COneHitDamageHandler::COneHitDamageHandler()
	: myWasHitThisFrame(false)
{
}


COneHitDamageHandler::~COneHitDamageHandler()
{
}

void COneHitDamageHandler::Update()
{
	myCounter.Update();
}

void COneHitDamageHandler::SetActiveTime(const float aActiveTime)
{
	auto lambda = [=]()
	{
		myWasHitThisFrame = false;
	};

	myCounter.Set(aActiveTime, Countdown::type::oneshot, lambda);
}

void COneHitDamageHandler::SetWasHitThisFrame()
{
	myWasHitThisFrame = true;
	myCounter.Reset();
	myCounter.Start();
}

bool COneHitDamageHandler::GetWasHit() const
{
	return myWasHitThisFrame;
}

void COneHitDamageHandler::Reset()
{
	myWasHitThisFrame = false;
	myCounter.Stop();
	myCounter.Reset();
}

#pragma once
#include "Countdown.h"

class COneHitDamageHandler
{
public:
	COneHitDamageHandler();
	~COneHitDamageHandler();

	void Update();
	void SetActiveTime(const float aActiveTime);
	void SetWasHitThisFrame();
	void Reset();
	
	bool GetWasHit() const;

private:
	bool myWasHitThisFrame;
	Countdown myCounter;
};


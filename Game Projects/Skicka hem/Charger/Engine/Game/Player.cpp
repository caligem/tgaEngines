#include "stdafx.h"
#include "Player.h"
#include "IWorld.h"
#include "CameraDataWrapper.h"
#include <iostream>
#include "Intersection.h"
#include "InputManager.h"
#include "PollingStation.h"
#include "Metrics.h"
#include "ProjectileManager.h"

//F�r inl�mmning--------
#include "AudioManager.h"
#include "Mathf.h"
//-----------------------

CMetrics* CPlayer::ourMetrics = nullptr;
CProjectileManager* CPlayer::ourProjectileManager = nullptr;

CPlayer::CPlayer()
	: myCurrentTimerSinceLastProjectile(0.f)
	, myJustShot(false)
	, myStartPosition({ 0.0f, 0.f, 0.f })
	, myChromaticAberrationValue(0.f)
{
}


CPlayer::~CPlayer()
{
}

void CPlayer::BindMetricsPtr(CMetrics& aMetrics)
{
	ourMetrics = &aMetrics;
	CGrenade::BindMetricsPtr(aMetrics);
	CDash::BindMetricsPtr(aMetrics);
}

void CPlayer::BindProjectileManagerPtr(CProjectileManager & aProjectileManager)
{
	ourProjectileManager = &aProjectileManager;
}

void CPlayer::Init(ID_T(CScene) aSceneID)
{
	myPathTicket = ID_T_INVALID(SPath);
	myPathIsReady = false;
	myDashIconOverlay = nullptr;
	myUIHasBeenInitialised = false;
	myIsPlayingAnimation = false;
	myHealedLastFrame = false;
	myHealedThisFrame = false;

	mySceneID = aSceneID;
	CGameObject::Init(mySceneID, 10000);
	CModelComponent* modelComponent = AddComponent<CModelComponent>({ "Assets/Models/playerCharacter/playerCharacter.fbx" });
	myAnimationController = AddComponent<CAnimationControllerComponent>(modelComponent);
	myAnimationController->SetAnimation("playerIdle");
	myAnimationController->SetLooping(true);

	CGameObject streak;
	streak.Init(aSceneID);
	streak.GetTransform().SetParent(&GetTransform());
	streak.GetTransform().SetPosition({ 0.f, 1.3f, 0.2f });
	myStreakComponent = streak.AddComponent<CStreakComponent>("Assets/Particles/Sprites/playerStreak.dds",
		CommonUtilities::Vector4f(0.3f, 0.9f, 1.0f, 1.f),
		CommonUtilities::Vector4f(0.3f, 0.9f, 1.0f, 0.f),
		0.7f,
		0.6f,
		0.0f
	);
	//AddComponent<CParticleSystemComponent>();
	AddComponent<CScriptComponent>({ "PlayerScript", 0 });

	myGrenade.Init(mySceneID);
	myDash.Init(mySceneID);

	Respawn();

	myCollider.SetPosition(GetTransform().GetPosition());
	myCollider.SetRadius(1.0f);

	myHealAoEHit.SetMaxMs(ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myAoEMaxMsBetweenHealPlayer);
	myEnemyAoEDamageHandler.SetMaxMs(ourMetrics->GetExploderMetrics().myAoEMaxMsBetweenDamagePlayer);

	///////////////////
	myClickArrows.Init(mySceneID);
	myClickArrowsModel = myClickArrows.AddComponent<CModelComponent>({ "Assets/Models/klickRespons/klickArrows/klickArrows.fbx" });
	myClickArrowsModel->GetMaterial().SetShouldRenderDepth(false);
	myClickArrows.SetActive(false);

	for (int i = 0; i < MyNumberOfDashes; ++i)
	{
		myStunParticleObject[i].Init(mySceneID);
		myStunParticle[i] = myStunParticleObject[i].AddComponent<CParticleSystemComponent>("Assets/Particles/dashStunCrack.json");
		myStunParticle[i]->Stop();
	}

	for (int i = 0; i < MyNumberOfDashes; ++i)
	{
		myStartDashParticleObject[i].Init(mySceneID);
		myStartDashParticle[i] = myStartDashParticleObject[i].AddComponent<CParticleSystemComponent>("Assets/Particles/startBlink.json");
		myStartDashParticle[i]->Stop();
	}

	auto lambda = [=]()
	{
		myClickArrows.SetActive(false);
	};

	myClickArrowCountdown.Set(ourMetrics->GetPlayerMetrics().myClickArrowActiveTime, Countdown::type::oneshot, lambda);
	myClickArrowCountdown.SetIsFinished();

	myHealParticleSystem = AddComponent<CParticleSystemComponent>("Assets/Particles/healParticle.json");
	myHealParticleSystem->Stop();

	myBloodParticleObject.Init(aSceneID);
	myBloodParticle = myBloodParticleObject.AddComponent<CParticleSystemComponent>("Assets/Particles/playerBlood.json");
	myBloodParticle->Stop();
}

void CPlayer::Update()
{
	IWorld::DrawDebugText("X: " + std::to_string(GetTransform().GetPosition().x));
	IWorld::DrawDebugText("Z: " + std::to_string(GetTransform().GetPosition().z));

	myPreviousMouseClickPosition = myCurrentMouseClickPosition;
	myCurrentMouseClickPosition = GetWorldPoint();

	UpdateActionInput(myCurrentMouseClickPosition);
	UpdatePlayerMovement(myCurrentMouseClickPosition);
	myCollider.SetPosition(GetTransform().GetPosition());

	bool wasHitThisFrame = false;

	for (int i = 0; i < myProjectileDamageHandler.GetNrOfProjectilesHitThisFrame(); ++i)
	{
		CHealth::TakeDamage(ourMetrics->GetRangedMetrics().myDamage);
		wasHitThisFrame = true;
	}

	if (myEnemyAoEDamageHandler.GetWasHit())
	{
		CHealth::TakeDamage(ourMetrics->GetExploderMetrics().myAoEDamage);
		wasHitThisFrame = true;
	}

	if (myHealAoEHit.GetWasHit())
	{
		CHealth::IncreaseHealth(ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myAoEHealAmount);
	}

	if (wasHitThisFrame)
	{
		AM.PlayNewInstance("SFX/Player/Hurt");
		myChromaticAberrationValue = 0.1f;
	}

	auto oldColor = IWorld::GetFadeColor();
	oldColor.x = myChromaticAberrationValue;
	oldColor.y = 1.f - CHealth::GetCurrentHealthInPercent();
	oldColor.z = IWorld::Time().GetTotalTime();
	IWorld::SetFadeColor(oldColor);

	myChromaticAberrationValue = CommonUtilities::Lerp(myChromaticAberrationValue, 0.f, IWorld::Time().GetDeltaTime()*5.f);

	if (myHealedLastFrame && !myHealedThisFrame)
	{
		if (myHealParticleSystem->IsPlaying())
		{
			myHealParticleSystem->Pause();
		}
		AM.Stop("SFX/Player/Heal", false, nullptr);
	}
	else if (!myHealedLastFrame && myHealedThisFrame)
	{
		AM.PlayNewInstance("SFX/Player/Heal", nullptr);
	}

	myGrenade.Update(myCurrentMouseClickPosition);
	myDash.Update(GetTransform().GetPosition(), myCurrentMouseClickPosition);

	UpdateProjectiles();

	UpdateClickArrows();

	myHealAoEHit.Update();
	myEnemyAoEDamageHandler.Update();
	myProjectileDamageHandler.Update();

	PlayAnimationUntillDone();

	myHealedLastFrame = myHealedThisFrame;
	myHealedThisFrame = false;

	IWorld::SetPlayerPosition(GetTransform().GetPosition());
}

void CPlayer::UpdateClickArrows()
{
	myClickArrowCountdown.SetMaxTime(ourMetrics->GetPlayerMetrics().myClickArrowActiveTime);
	myClickArrowsModel->GetMaterial().SetCustomData({ myClickArrowCountdown.GetPercent(), 0.f, 0.f, 0.f });
	myClickArrowCountdown.Update();
}

void CPlayer::UpdatePlayerMovement(CommonUtilities::Vector3f aWorldPosition)
{
	float dt = IWorld::Time().GetDeltaTime();
	float speed = ourMetrics->GetPlayerMetrics().myMovementSpeed;

	myStreakComponent->SetLifeTime(0.0f);

	if (myDash.ShouldDash())
	{
		CommonUtilities::Vector3f dashTarget = aWorldPosition;
		myDash.ValidateDashTarget(GetTransform().GetPosition(), dashTarget);

		CommonUtilities::Vector3f direction(dashTarget - GetTransform().GetPosition());
		direction.y = 0.f;
		SetPlayerLookDirection(direction);
		CommonUtilities::Vector3f closestPointOnNavmesh;
		IWorld::Path().RayHitLocation(dashTarget + CommonUtilities::Vector3f::Up, { 0.f, -1.f, 0.f }, closestPointOnNavmesh);
		float pathLength;
		bool foundPath = IWorld::Path().IsThereAPath(GetTransform().GetPosition(), closestPointOnNavmesh, pathLength);
		if (foundPath && pathLength < 100.f)
		{
			myDash.UseDash();
			myStreakComponent->SetLifeTime(0.2f);
			myStreakComponent->EmitParticle();
			IWorld::Path().FinishedUsingPath(myPathTicket);
			myPathIsReady = false;

			myAnimationController->SetAnimationForced("playerDash");
			myIsPlayingAnimation = true;

			CommonUtilities::Vector3f blinkStart;
			blinkStart = GetTransform().GetPosition();
			for (int i = 0; i < MyNumberOfDashes; ++i)
			{
				if (!myStartDashParticle[i]->IsPlaying())
				{
					myStartDashParticleObject[i].GetTransform().SetPosition(CommonUtilities::Vector3f(blinkStart.x, blinkStart.y, blinkStart.z));
					myStartDashParticle[i]->Play();
					break;
				}
			}

			GetTransform().SetPosition(closestPointOnNavmesh);

			CommonUtilities::Vector3f stunPosition;
			stunPosition = GetTransform().GetPosition();
			for (int i = 0; i < MyNumberOfDashes; ++i)
			{
				if (!myStunParticle[i]->IsPlaying())
				{
					myStunParticleObject[i].GetTransform().SetPosition(CommonUtilities::Vector3f(stunPosition.x , stunPosition.y, stunPosition.z));
					myStunParticle[i]->Play();
					break;
				}
			}
			
		}
		else
		{
			myDash.ShouldNotDash();
		}
	}	

	MovePlayer(speed, dt);
}

void CPlayer::MovePlayer(float speed, float dt)
{	
	if (myJustShot || myDash.IsImpactDelayActive() || myGrenade.IsThrowDelayActive())
	{
		return;
	}
	CachePathIfFound();

	if (myPathIsReady)
	{
		//DebugRenderPlayerPath();
		if (myPathPoints.Empty())
		{
			myPathIsReady = false;
			if (IsAnimationsDone())
			{
				myAnimationController->SetAnimation("playerIdle");
			}
			return;
		}

		auto playerPos = GetTransform().GetPosition();

		if (myPathPoints.Size() > 1)
		{
			auto& currentPoint = myPathPoints[myPathPoints.Size() - 1];
			auto& nextPoint = myPathPoints[myPathPoints.Size() - 2];

			auto toNext = nextPoint - currentPoint;
			toNext.Normalize();

			auto toPlayer = playerPos - currentPoint;

			auto dot = toPlayer.Dot(toNext);

			if (dot > 0.f)
			{
				auto closestPoint = currentPoint + toNext * dot;
				if ((playerPos - closestPoint).Length2() < 1e-2)
				{
					myPathPoints.Pop();
				}
			}
		}
		
		auto& targetPosition = myPathPoints.GetLast();

		CommonUtilities::Vector3f direction(targetPosition - playerPos);
		CommonUtilities::Vector3f lookDirection = CommonUtilities::Slerp(
			GetTransform().GetForward().GetNormalized(), direction.GetNormalized(), dt * 8.f);
		lookDirection.y = 0.f;
		SetPlayerLookDirection(lookDirection);

		CommonUtilities::Vector3f movement = targetPosition - playerPos;
		movement.Normalize();
		GetTransform().Move(movement * speed * dt);
		if (IsAnimationsDone())
		{
			myAnimationController->SetAnimation("playerRun");
		}

		if ((targetPosition - GetTransform().GetPosition()).Length2() < 1.0f)
		{
			myPathPoints.Pop();
		}
	}
	else
	{
		if (IsAnimationsDone())
		{
			myAnimationController->SetAnimation("playerIdle");
		}
	}

}

void CPlayer::UpdateProjectiles()
{
	if (myJustShot)
	{
		myCurrentTimerSinceLastProjectile += IWorld::Time().GetDeltaTime();

		if (myCurrentTimerSinceLastProjectile >= ourMetrics->GetPlayerMetrics().myAttackCooldown)
		{
			myJustShot = false;
			myCurrentTimerSinceLastProjectile = 0.f;
		}
	}
}

CommonUtilities::Vector3f CPlayer::GetWorldPoint()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Matrix44f viewProjInv = IWorld::GetSavedCameraBuffer().myInvertedViewProjection;

	CommonUtilities::Vector2f mousePos = {
		input.GetMousePosition().x / IWorld::GetWindowSize().x,
		input.GetMousePosition().y / IWorld::GetWindowSize().y
	};
	mousePos.x = mousePos.x * 2.f - 1.f;
	mousePos.y = (1.f - mousePos.y) * 2.f - 1.f;

	CommonUtilities::Vector4f rayOrigin = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 0.f, 1.f) * viewProjInv;
	rayOrigin /= rayOrigin.w;
	CommonUtilities::Vector4f rayEnd = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 1.f, 1.f) * viewProjInv;
	rayEnd /= rayEnd.w;

	CommonUtilities::Vector3f rayDir = rayEnd - rayOrigin;
	rayDir.Normalize();

	CommonUtilities::Vector3f intersection;

	IWorld::Path().RayHitLocation(rayOrigin, rayDir, intersection);

	return intersection;
}

CommonUtilities::Vector3f CPlayer::GetShootPoint(const CommonUtilities::Vector3f aPosition)
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Matrix44f viewProjInv = CommonUtilities::Matrix44f::Inverse(IWorld::GetSavedCameraBuffer().myViewProjection);

	CommonUtilities::Vector2f mousePos = {
		input.GetMousePosition().x / IWorld::GetWindowSize().x,
		input.GetMousePosition().y / IWorld::GetWindowSize().y
	};
	mousePos.x = mousePos.x * 2.f - 1.f;
	mousePos.y = (1.f - mousePos.y) * 2.f - 1.f;

	CommonUtilities::Vector4f rayOrigin = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 0.f, 1.f) * viewProjInv;
	rayOrigin /= rayOrigin.w;
	CommonUtilities::Vector4f rayEnd = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 1.f, 1.f) * viewProjInv;
	rayEnd /= rayEnd.w;

	CommonUtilities::Vector3f rayDir = rayEnd - rayOrigin;
	rayDir.Normalize();

	CommonUtilities::Vector3f normal = { 0.f, 1.f, 0.f };

	float denom = normal.Dot(rayDir);

	CommonUtilities::Vector3f p0l0 = aPosition-CommonUtilities::Vector3f(rayOrigin);

	float t = p0l0.Dot(normal) / denom;

	CommonUtilities::Vector3f intersection = CommonUtilities::Vector3f(rayOrigin) + rayDir*t;

	return intersection;
}



const bool CPlayer::GetHasDashed() const
{
	return myDash.GetHasDashed();
}

bool CPlayer::GetIsDead() const
{
	return CHealth::GetIsDead();
}

const CAreaOfEffect& CPlayer::GetAreaOfEffectObject() const
{
	return myGrenade.GetAreaOfEffectObject();
}

void CPlayer::Shoot()
{
	if (!myJustShot)
	{
		auto& transform = GetTransform();
		CommonUtilities::Vector3f nozzle = GetPlayerNozzle();

		auto direction = GetShootPoint(nozzle) - transform.GetPosition();
		direction.y = 0.f;

		SetPlayerLookDirection(direction);

		CommonUtilities::Vector3f bulletStartPoint = GetPlayerNozzle();
		ourProjectileManager->FirePlayerProjectile(bulletStartPoint, transform.GetPosition() + direction * 100.f);

		myJustShot = true;
		AM.PlayNewInstance("SFX/Player/Shoot");
		myAnimationController->SetAnimationForced("playerAttack");
		myIsPlayingAnimation = true;
	}
}

CommonUtilities::Vector3f CPlayer::GetPlayerNozzle()
{
	auto& transform = GetTransform();
	CommonUtilities::Vector3f startOffsetForward = transform.GetForward() * 1.3f;
	startOffsetForward.y = 0.f;
	CommonUtilities::Vector3f startOffsetRight = transform.GetRight() * 0.1f;
	CommonUtilities::Vector3f startOffsetUp(0.f, 1.4f, 0.f);
	CommonUtilities::Vector3f startPoint = transform.GetPosition() + startOffsetForward + startOffsetUp + startOffsetRight;
	return startPoint;
}

void CPlayer::UpdateActionInput(CommonUtilities::Vector3f& aWorldPosition)
{
	if (IWorld::Input().IsKeyPressed(Input::Key_E))
	{
		if (myDash.CanDash())
		{
			myDash.ToggleAim();
			myGrenade.CancelAim();
		}
		else
		{
			AM.PlayNewInstance("SFX/Player/Ability/Denied");
		}
	}
	if (IWorld::Input().IsKeyPressed(Input::Key_Q))
	{
		myGrenade.ToggleAim();
		myDash.CancelAim();
	}

	if (IWorld::Input().IsButtonPressed(Input::Button_Left))
	{
		CachePathIfFound();
		IWorld::Path().FinishedUsingPath(myPathTicket);
		IWorld::Path().RequestPath(GetTransform().GetPosition(), aWorldPosition, myPathTicket);

		myClickArrows.SetActive(true);
		myClickArrows.GetTransform().SetPosition(aWorldPosition);
		myClickArrowCountdown.Reset();
		myClickArrowCountdown.Start();

		myHoldMovementTimer = 0.f;
	}
	if (IWorld::Input().IsButtonDown(Input::Button_Left))
	{
		myHoldMovementTimer += IWorld::Time().GetDeltaTime();
	}
	if (myHoldMovementTimer > 0.5f)
	{
		if (IWorld::Input().IsButtonReleased(Input::Button_Left))
		{
			myHoldMovementTimer = 0.f;
			StopFollowingPath();
		}
		else
		{
			CachePathIfFound();
			IWorld::Path().FinishedUsingPath(myPathTicket);
			IWorld::Path().RequestPath(GetTransform().GetPosition(), aWorldPosition, myPathTicket);
		}
	}
	
	if (IWorld::Input().IsButtonDown(Input::Button_Right)) // basic attack
	{
		if (myDash.IsAiming())
		{
			myDash.TryDash();
			myJustDashedTimer = 0.3f;
		}
		else if (myGrenade.GetGrenadeState() == CGrenade::eGrenadeState::eChooseTargetMode)
		{
			myGrenade.SetShouldThrowGrenade();
			myAnimationController->SetAnimation("playerGrenade");
			IWorld::Path().FinishedUsingPath(myPathTicket);
			myPathIsReady = false;
			myIsPlayingAnimation = true;
			CommonUtilities::Vector3f direction(aWorldPosition - GetTransform().GetPosition());
			direction.y = 0.f;
			SetPlayerLookDirection(direction);
		}
		else if (!myGrenade.GetIsDoneThrowingGrenade() && myJustDashedTimer <= 0.0f)
		{
			Shoot();
			StopFollowingPath();
		}
		else
		{
			myJustDashedTimer -= IWorld::Time().GetDeltaTime();
		}
	}
	if (IWorld::Input().IsButtonReleased(Input::Button_Right))
	{
		myJustDashedTimer = 0.0f;
	}
}

void CPlayer::StopFollowingPath()
{
	IWorld::Path().FinishedUsingPath(myPathTicket);
	myPathIsReady = false;
}

void CPlayer::Respawn()
{
	CHealth::Init(ourMetrics->GetPlayerMetrics().myMaxHealth);

	CommonUtilities::Vector3f closestPointOnNavmesh;
	IWorld::Path().RayHitLocation(myStartPosition + CommonUtilities::Vector3f::Up, { 0.f, -1.f, 0.f }, closestPointOnNavmesh);
	GetTransform().SetPosition(closestPointOnNavmesh);

	SetIsAlive();

	myCurrentTimerSinceLastProjectile = 0;
	myJustShot = false;
	myGrenade.Reset();
	myDash.Reset();
	myClickArrows.SetActive(false);
	myClickArrowCountdown.Reset();

	myEnemyAoEDamageHandler.Reset();
	myHealAoEHit.Reset();
	myProjectileDamageHandler.Reset();

	///////
	myPathIsReady = false;
	myHoldMovementTimer = 0.f;
	StopFollowingPath();
	////////

	myIsPlayingAnimation = false;
	myAnimationController->SetAnimation("playerIdle");
	myAnimationController->SetLooping(true);
}

const CColliderSphere& CPlayer::GetCollider()
{
	return myCollider;
}

void CPlayer::SetOnLoadPosition(const CommonUtilities::Vector3f& aStartPosition)
{
	myStartPosition = aStartPosition;

	CommonUtilities::Vector3f closestPointOnNavmesh;
	IWorld::Path().RayHitLocation(myStartPosition + CommonUtilities::Vector3f::Up, { 0.f, -1.f, 0.f }, closestPointOnNavmesh);
	GetTransform().SetPosition(closestPointOnNavmesh);
}

void CPlayer::SetStartPosition(const CommonUtilities::Vector3f & aStartPosition)
{
	myStartPosition = aStartPosition;
}

void CPlayer::RepositionInGameUI(const std::wstring & aFilepath)
{
	if (!myUIHasBeenInitialised)
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Trying to adjust InGameUI when it hasnt been initialised");
		return;
	}

	JsonDocument inGameUIPositionDoc;
	const std::string filePath(aFilepath.begin(), aFilepath.end());
	inGameUIPositionDoc.LoadFile(filePath.c_str());

	if (inGameUIPositionDoc.Find("myDashUI"))
	{
		auto dashUIObject = inGameUIPositionDoc["myDashUI"];
		for (auto& icon : myDashIcons)
		{
			icon->SetPosition(JsonToVector2f(dashUIObject["myIconPosition"]));
		}
		myDashIconOverlay->SetPosition(JsonToVector2f(dashUIObject["myIconOverlayPosition"]));
	}
	if (inGameUIPositionDoc.Find("myGrenadeUI"))
	{
		auto grenadeUIObject = inGameUIPositionDoc["myGrenadeUI"];
		for (auto& icon : myGrenadeIcons)
		{
			icon->SetPosition(JsonToVector2f(grenadeUIObject["myIconPosition"]));
		}
		myGrenadeIconOverlay->SetPosition(JsonToVector2f(grenadeUIObject["myIconOverlayPosition"]));
	}
	if (inGameUIPositionDoc.Find("myPlayerHealthBar"))
	{
		auto playerHealthBar = inGameUIPositionDoc["myPlayerHealthBar"];
		myHealthBar->SetPosition(JsonToVector2f(playerHealthBar["myHealthBarPosition"]));
		myHealthBarBackground->SetPosition(JsonToVector2f(playerHealthBar["myHealthBarBackgroundPosition"]));
	}
}

void CPlayer::AddInGameUIElements(CCanvasComponent* aInGameUICanvas)
{
	int iconCounter = 0;
	std::string iconPath;
	for (auto& icon : myDashIcons)
	{
		iconPath = "Assets/Sprites/InGame/dashIcon" + std::to_string(iconCounter) + ".dds";
		icon = aInGameUICanvas->AddUIElement<CSpriteComponent>({ iconPath.c_str() });
		icon->SetPivot({ 0.5f, 0.0f });
		iconCounter++;
	}

	myDashIconOverlay = aInGameUICanvas->AddUIElement<CSpriteComponent>({ "Assets/Sprites/InGame/abilityOverlay.dds" });
	myDashIconOverlay->SetPivot({ 0.5f, 0.0f });
	
	iconCounter = 0;
	for (auto& icon : myGrenadeIcons)
	{
		iconPath = "Assets/Sprites/InGame/grenadeIcon" + std::to_string(iconCounter) + ".dds";
		icon = aInGameUICanvas->AddUIElement<CSpriteComponent>({ iconPath.c_str() });
		icon->SetPivot({ 0.5f, 0.0f });
		iconCounter++;
	}

	myGrenadeIconOverlay = aInGameUICanvas->AddUIElement<CSpriteComponent>({ "Assets/Sprites/InGame/abilityOverlay.dds" });
	myGrenadeIconOverlay->SetPivot({ 0.5f, 0.0f });

	myHealthBarBackground = aInGameUICanvas->AddUIElement<CSpriteComponent>({ "Assets/Sprites/InGame/playerHealthBarBackground.dds" });
	myHealthBarBackground->SetPivot({ 0.5f, 1.f });
	myHealthBarBackground->SetTint({ 1.f, 1.f, 1.f, 0.8f });

	myHealthBar = aInGameUICanvas->AddUIElement<CSpriteComponent>({ "Assets/Sprites/InGame/playerHealthBar.dds" });
	myHealthBar->SetPivot({ 0.5f, 1.f });

	myUIHasBeenInitialised = true;
}

void CPlayer::SetDoneDashing()
{
	myDash.SetDoneDashing();
}

void CPlayer::SetWasHitEnemyAoE()
{
	myEnemyAoEDamageHandler.SetWasHitThisFrame();
}

void CPlayer::SetWasHitHealAoE()
{
	myHealAoEHit.SetWasHitThisFrame();
	myHealedThisFrame = true;
	myHealParticleSystem->Play();
}

void CPlayer::SetWasHitProjectile()
{
	myProjectileDamageHandler.SetWasHitThisFrame();
}

void CPlayer::UpdateInGameUI()
{
	if (!myUIHasBeenInitialised)
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Trying to update InGameUI when it hasnt been initialised");
		return;
	}

	for (auto& icon : myDashIcons)
	{
		icon->SetShouldBeRendered(false);
	}

	myDashIcons[myDash.GetCharges()]->SetShouldBeRendered(true);

	{
		static float f = 0.f;
		float yValue = CHealth::GetCurrentHealthInPercent();
		f = CommonUtilities::Lerp(f, yValue, IWorld::Time().GetDeltaTime() * 10.f);
		myHealthBar->SetUVOffset({ 1.f, -f });
		myHealthBar->SetScale({ 1.f, f });
		myHealthBar->SetUVScale({ 1.f, f });
	}

	if (myGrenade.IsOnCooldown())
	{
		myGrenadeIcons[0]->SetShouldBeRendered(false);
		myGrenadeIcons[1]->SetShouldBeRendered(true);
		myGrenadeIconOverlay->SetShouldBeRendered(true);

		float yValue = 1.0f - myGrenade.GetCooldownInPercent();
		myGrenadeIconOverlay->SetScale({ 1.f , yValue });
		myGrenadeIconOverlay->SetUVScale({ 1.f, yValue });
	}
	else
	{
		myGrenadeIcons[0]->SetShouldBeRendered(true);
		myGrenadeIcons[1]->SetShouldBeRendered(false);
		myGrenadeIconOverlay->SetShouldBeRendered(false);
	}

	if (myDash.HasMaxCharges())
	{
		myDashIconOverlay->SetShouldBeRendered(false);
	}
	else
	{
		myDashIconOverlay->SetShouldBeRendered(true);
		float yValue = 1.0f - myDash.GetCooldownInPercent();
		myDashIconOverlay->SetScale({ 1.f , yValue });
		myDashIconOverlay->SetUVScale({ 1.f, yValue });
	}
}

void CPlayer::CachePathIfFound()
{
	if (IWorld::Path().GetPathState(myPathTicket) == EPathState::EPathState_Found)
	{
		myPathPoints = IWorld::Path().GetPath(myPathTicket);
		myPathIsReady = true;
		IWorld::Path().FinishedUsingPath(myPathTicket);

		if (!myPathPoints.Empty())
		{
			auto targetPosition = myPathPoints.GetLast();
			if ((targetPosition - GetTransform().GetPosition()).Length2() < 1.0f)
			{
				myPathPoints.Pop();
			}
		}
	}
}

void CPlayer::DebugRenderPlayerPath()
{
	for (int i = 0; i < myPathPoints.Size(); ++i)
	{
		auto& pos = myPathPoints[i];

		IWorld::SetDebugColor({ 1.0f, 0.f, 1.f, 1.f });
		IWorld::DrawDebugWireSphere(pos);

		if (i < myPathPoints.Size() - 1)
		{
			auto& nextPos = myPathPoints[i + 1];

			IWorld::SetDebugColor({ 0.0f, 1.f, 1.f, 1.f });
			IWorld::DrawDebugLine(pos, nextPos);
		}
	}
}

void CPlayer::PlayAnimationUntillDone()
{
	if (myIsPlayingAnimation)
	{
		float animationTimeNextFrame = (myAnimationController->GetTime() + IWorld::Time().GetDeltaTime()) * myAnimationController->GetCurrentAnimationTicksPerSecond();
		float animationMaxTime = myAnimationController->GetCurrentAnimationDuration();
		if (animationTimeNextFrame >= animationMaxTime)
		{
			myIsPlayingAnimation = false;
		}
	}
}

bool CPlayer::IsAnimationsDone()
{
	if (myIsPlayingAnimation)
	{
		return false;
	}
	return true;
}

void CPlayer::SetPlayerLookDirection(const CommonUtilities::Vector3f& aDirection)
{
	if (aDirection.Length2() > 0.f)
	{
		GetTransform().SetLookDirection(aDirection);
	}
}

void CPlayer::PlayDeathAnimation()
{
	myIsPlayingAnimation = false;
	myAnimationController->SetAnimation("playerDeath");
	myAnimationController->SetLooping(false);
}

void CPlayer::PlayBloodParticle(CommonUtilities::Vector3f & aPositionOfHit)
{
	myBloodParticleObject.GetTransform().SetPosition(aPositionOfHit);
	myBloodParticle->PlayInstant();
}
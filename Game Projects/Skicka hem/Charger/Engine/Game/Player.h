#pragma once
#include "GameObject.h"
#include "Health.h"
#include "Projectile.h"
#include "Grenade.h"
#include "ColliderSphere.h"
#include "Dash.h"
#include "AoEDamageHandler.h"
#include "ProjectileDamageHandler.h"
#include "PathFinder.h"

class CMetrics;
class CProjectileManager;

class CPlayer : public CGameObject, CHealth
{
public:
	CPlayer();
	~CPlayer();

	static void BindMetricsPtr(CMetrics& aMetrics);
	static void BindProjectileManagerPtr(CProjectileManager& aProjectileManager);

	void Init(ID_T(CScene) aSceneID);
	void Update();

	void UpdateClickArrows();

	void Respawn();
	void SetOnLoadPosition(const CommonUtilities::Vector3f& aStartPosition);
	void SetStartPosition(const CommonUtilities::Vector3f& aStartPosition);
	void RepositionInGameUI(const std::wstring& aFilepath);
	void AddInGameUIElements(CCanvasComponent* myInGameUICanvas);
	void SetDoneDashing();
	void SetWasHitEnemyAoE();
	void SetWasHitHealAoE();
	void SetWasHitProjectile();

	bool GetIsDead() const;
	const CAreaOfEffect& GetAreaOfEffectObject() const;
	const CColliderSphere& GetCollider();
	CommonUtilities::Vector3f GetWorldPoint();
	CommonUtilities::Vector3f GetShootPoint(const CommonUtilities::Vector3f aPosition);
	const bool GetHasDashed() const;

	void PlayDeathAnimation();
	void PlayBloodParticle(CommonUtilities::Vector3f& aPositionOfHit);

	void UpdateInGameUI();

private:
	void UpdatePlayerMovement(CommonUtilities::Vector3f aWorldPosition);

	void MovePlayer(float speed, float dt);

	void UpdateProjectiles();
	void Shoot();

	CommonUtilities::Vector3f GetPlayerNozzle();

	void UpdateActionInput(CommonUtilities::Vector3f& aWorldPosition);

	void StopFollowingPath();

	void CachePathIfFound();
	void DebugRenderPlayerPath();
	void PlayAnimationUntillDone();
	bool IsAnimationsDone();
	void SetPlayerLookDirection(const CommonUtilities::Vector3f& aDirection);
	CColliderSphere myCollider;
	CDash myDash;
	CGrenade myGrenade;
	CGameObject myClickArrows;
	static const char MyNumberOfDashes = 3; 
	CGameObject myStunParticleObject[MyNumberOfDashes];
	CParticleSystemComponent* myStunParticle[MyNumberOfDashes];
	CGameObject myStartDashParticleObject[MyNumberOfDashes];
	CParticleSystemComponent* myStartDashParticle[MyNumberOfDashes];
	CGameObject myBloodParticleObject;
	CParticleSystemComponent* myBloodParticle;
	CModelComponent* myClickArrowsModel;
	Countdown myClickArrowCountdown;

	CommonUtilities::Vector3f myPreviousMouseClickPosition;
	CommonUtilities::Vector3f myCurrentMouseClickPosition;
	CommonUtilities::Vector3f myStartPosition;

	static CMetrics* ourMetrics;
	static CProjectileManager* ourProjectileManager;

	CAnimationControllerComponent* myAnimationController;

	CAoEDamageHandler myEnemyAoEDamageHandler;
	CAoEDamageHandler myHealAoEHit;
	CProjectileDamageHandler myProjectileDamageHandler;

	CStreakComponent* myStreakComponent;

	CSpriteComponent* myDashIcons[4];
	CSpriteComponent* myDashIconOverlay;

	CSpriteComponent* myGrenadeIcons[2];
	CSpriteComponent* myGrenadeIconOverlay;

	CParticleSystemComponent* myHealParticleSystem;

	CSpriteComponent* myHealthBarBackground;
	CSpriteComponent* myHealthBar;

	ID_T(CScene) mySceneID;
	float myCurrentTimerSinceLastProjectile;
	float myJustDashedTimer;
	bool myJustShot;
	bool myUIHasBeenInitialised;

	bool myIsPlayingAnimation;
	bool myHealedLastFrame;
	bool myHealedThisFrame;

	PathTicket myPathTicket;
	PathPoints myPathPoints;
	bool myPathIsReady;

	float myHoldMovementTimer = 0.f;
	float myChromaticAberrationValue;
};


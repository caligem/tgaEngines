#include "stdafx.h"
#include "PollingStation.h"
#include "GameState.h"
#include "Actor.h"

CMetrics* CPollingStation::ourMetrics = nullptr;

CPollingStation::CPollingStation() 
	: myFrameCount(1)
{
	if (myGameState == nullptr)
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "The GameState* in CPollingStation is nullptr!!");
	}
}

void CPollingStation::ScreenShake()
{
	myScreenShake = 1.f;
}

void CPollingStation::Update()
{
	++myFrameCount;

	myScreenShake = CommonUtilities::Lerp(myScreenShake, 0.f, IWorld::Time().GetDeltaTime());
}

void CPollingStation::BindGameStatePtr(CGameState & aGameState)
{
	myGameState = &aGameState;
}

void CPollingStation::BindMetricsPtr(CMetrics& aMetrics)
{
	ourMetrics = &aMetrics;
}

void CPollingStation::AddEnemyList(std::vector<CActor*>& aEnemyList)
{
	ourEnemies = &aEnemyList;
}

const CTransform& CPollingStation::GetPlayerTransform()
{
	if (myPlayerPositionCache.myLastUpdateFrame < myFrameCount)
	{
		myPlayerPositionCache.myTransform = myGameState->GetPlayerTransform();
		myPlayerPositionCache.myLastUpdateFrame = myFrameCount;
	}

	return myPlayerPositionCache.myTransform;
}

CAreaOfEffect* CPollingStation::GetClosestAcid(const CommonUtilities::Vector3f& aPosition) const
{
	return myGameState->GetClosestAcid(aPosition);
}

const CAreaOfEffect& CPollingStation::GetPlayerAcid() const
{
	return myGameState->GetPlayerAcid();
}

const CActor* CPollingStation::GetClosestEnemy(const CommonUtilities::Vector3f& aPosition)
{
	return myGameState->GetClosestEnemy(aPosition);
}

void CPollingStation::AddExploderAoEAtPosition(const CommonUtilities::Vector3f& aPosition)
{
	myGameState->AddExploderAOEAtPosition(aPosition);
}

void CPollingStation::RemoveExploderAoE(const CAreaOfEffect* aAoE)
{
	myGameState->RemoveExploderAoE(aAoE);
}

bool CPollingStation::CalculateIfEnemiesGroupedUp()
{
	return myGameState->CalculateIfEnemiesGroupedUp();
}

void CPollingStation::SetUltimateFinished()
{
	myGameState->SetUltimateFinished();
}

const CommonUtilities::Vector3f& CPollingStation::GetCenterPositionOfGroupedUpEnemies()
{
	return myGameState->GetCenterPositionOfGroupedUpEnemies();
}

bool CPollingStation::GetCanCompanionUltimate() const
{
	return myGameState->GetCanCompanionUltimate();
}

const std::vector<CActor*>& CPollingStation::GetEnemies()
{
	return *ourEnemies;
}
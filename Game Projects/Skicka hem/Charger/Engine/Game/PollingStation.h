#pragma once
#include <bitset>
#include "Metrics.h"
#include "AoEDamageHandler.h"
//#include "ProjectileDamageHandler.h"

class CGameState;
class CAreaOfEffect;
class CActor;

struct SPositionCache
{
	uint_least64_t myLastUpdateFrame = 0;
	CTransform myTransform;
};

class CPollingStation 
{
public:
	static CPollingStation& GetInstance()
	{
		static CPollingStation instance;
		return instance;
	}

	void Update();

	void BindGameStatePtr(CGameState& aGameState);
	void BindMetricsPtr(CMetrics& aMetrics);
	void AddEnemyList(std::vector<CActor*>& aEnemyList);

	const CTransform& GetPlayerTransform();
	CAreaOfEffect* GetClosestAcid(const CommonUtilities::Vector3f& aPosition) const;
	const CAreaOfEffect& GetPlayerAcid() const;
	const CActor* GetClosestEnemy(const CommonUtilities::Vector3f& aPosition);
	
	void AddExploderAoEAtPosition(const CommonUtilities::Vector3f& aPosition);
	void RemoveExploderAoE(const CAreaOfEffect* aAoE);
	bool CalculateIfEnemiesGroupedUp();
	void SetUltimateFinished();

	const CommonUtilities::Vector3f& GetCenterPositionOfGroupedUpEnemies();
	bool GetCanCompanionUltimate() const;
	const std::vector<CActor*>& GetEnemies();

	void ScreenShake();
	float GetScreenShake() const { return myScreenShake; }

private:
	CPollingStation();
	CPollingStation(CPollingStation const&) = delete; //do not implement
	void operator=(CPollingStation const&) = delete; //do not implement
	uint_least64_t myFrameCount;
	CGameState* myGameState;
	SPositionCache myPlayerPositionCache;
	std::vector<CActor*>* ourEnemies;

	float myScreenShake;

	static CMetrics* ourMetrics;
};


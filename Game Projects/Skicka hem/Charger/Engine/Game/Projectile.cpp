#include "stdafx.h"
#include "Projectile.h"
#include "IWorld.h"
#include "PollingStation.h"
#include "StreakComponent.h"

CProjectile::CProjectile()
{
}


CProjectile::~CProjectile()
{
}

void CProjectile::Init(ID_T(CScene) aSceneID, float aSpeed, char* aModelPath, float aStreakLifeTime)
{
	CGameObject::Init(aSceneID);
	AddComponent<CModelComponent>({ aModelPath });

	CGameObject streak;
	streak.Init(aSceneID);
	streak.GetTransform().SetParent(&GetTransform());
	streak.GetTransform().SetPosition({ 0.f, 0.f, 0.f });
	myStreak = streak.AddComponent<CStreakComponent>(
		"Assets/Particles/Sprites/playerBulletStreak.dds",
		CommonUtilities::Vector4f(1.f, 1.8f, 1.f, 1.f),
		CommonUtilities::Vector4f(0.f, 1.0f, 0.6f, 0.f),
		0.2f,
		0.1f,
		aStreakLifeTime,
		0.2f
	);
	myIsDead = false;
	mySpeed = aSpeed;
}

void CProjectile::Reset()
{
	myIsDead = false;
	myStreak->ClearStreak();
}

void CProjectile::Update()
{
	if (myIsDead)
	{
		return;
	}

	float dt = IWorld::Time().GetDeltaTime();
	GetTransform().Move(GetTransform().GetForward() * mySpeed * dt);

	GetTransform().RotateAround(myDirection, dt*3.f);

	if (GetIsOutOfRange())
	{
		myIsDead = true;
	}
}

void CProjectile::FireBullet(const CommonUtilities::Vector3f& aStartPosition, const CommonUtilities::Vector3f& aTargetPosition)
{
	GetTransform().SetPosition(aStartPosition);
	myTargetPosition = aTargetPosition;

	myDirection = (myTargetPosition - aStartPosition);
	myDirection.y = 0.f;
	myDirection.Normalize();

	GetTransform().SetLookDirection(myDirection);
	myStreak->ClearStreak();
}

void CProjectile::FireBulletInDirection(const CommonUtilities::Vector3f & aStartPosition, const CommonUtilities::Vector3f & aDirection)
{
	GetTransform().SetPosition(aStartPosition);
	myDirection = aDirection;
	myDirection.y = 0.f;
	myDirection.Normalize();

	GetTransform().SetLookDirection(myDirection);
	myStreak->ClearStreak();
}

void CProjectile::SetStreakColor(const CommonUtilities::Vector4f & aStartColor, const CommonUtilities::Vector4f & aEndColor)
{
	myStreak->SetStartAndEndColor(aStartColor, aEndColor);
}

bool CProjectile::GetIsOutOfRange() const
{
	if (IWorld::GetSavedFrustum().CullSphere(GetTransform().GetPosition(), 0.5f))
	{
		return true;
	}

	return false;
}
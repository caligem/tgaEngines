#pragma once
#include "GameObject.h"

class CProjectile : public CGameObject
{
public:
	CProjectile();
	~CProjectile();

	void Init(ID_T(CScene) aSceneID, float aSpeed, char* aModelPath, float aStreakLifeTime);
	void Reset();
	void Update();
	
	void FireBullet(const CommonUtilities::Vector3f& aStartPosition, const CommonUtilities::Vector3f& aTargetPosition);
	void FireBulletInDirection(const CommonUtilities::Vector3f& aStartPosition, const CommonUtilities::Vector3f& aDirection);

	bool IsDead() const { return myIsDead; }
	void SetIsDead(const bool aIsDead) { myIsDead = aIsDead; }
	void SetStreakColor(const CommonUtilities::Vector4f& aStartColor, const CommonUtilities::Vector4f& aEndColor);

private:
	//void HandleCollision();
	bool GetIsOutOfRange() const;

	CommonUtilities::Vector3f myTargetPosition;
	CommonUtilities::Vector3f myDirection;
	float mySpeed;
	bool myIsDead;

	CStreakComponent* myStreak;
};




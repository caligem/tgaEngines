#include "stdafx.h"
#include "ProjectileDamageHandler.h"


CProjectileDamageHandler::CProjectileDamageHandler()
	: myNrOfProjectilesHitThisFrame(0)
{
}

CProjectileDamageHandler::~CProjectileDamageHandler()
{
}

void CProjectileDamageHandler::Update()
{
	myNrOfProjectilesHitThisFrame = 0;
}

void CProjectileDamageHandler::SetWasHitThisFrame()
{
	++myNrOfProjectilesHitThisFrame;
}

void CProjectileDamageHandler::Reset()
{
	myNrOfProjectilesHitThisFrame = 0;
}

bool CProjectileDamageHandler::GetWasHit() const
{
	return myNrOfProjectilesHitThisFrame > 0;
}

int CProjectileDamageHandler::GetNrOfProjectilesHitThisFrame() const
{
	return myNrOfProjectilesHitThisFrame;
}



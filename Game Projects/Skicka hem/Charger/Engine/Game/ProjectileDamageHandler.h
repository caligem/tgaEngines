#pragma once

class CProjectileDamageHandler
{
public:
	~CProjectileDamageHandler();
	CProjectileDamageHandler();

	void Update();

	void SetWasHitThisFrame();
	void Reset();

	bool GetWasHit() const;
	int GetNrOfProjectilesHitThisFrame() const;

private:
	int myNrOfProjectilesHitThisFrame;
};
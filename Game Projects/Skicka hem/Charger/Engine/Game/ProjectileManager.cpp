#include "stdafx.h"
#include "ProjectileManager.h"
#include "Metrics.h"
#include "RangedEnemy.h"
#include "Exploder.h"
#include "Player.h"

CProjectileManager::CProjectileManager()
	:myPlayerProjectiles(64),
	myEnemiesProjectiles(64)
{
}


CProjectileManager::~CProjectileManager()
{
}

void CProjectileManager::Init(ID_T(CScene) aSceneID, CMetrics* aMetrics)
{
	mySceneID = aSceneID;
	myMetrics = aMetrics;
	myPlayerProjectiles.Init(32);
	myEnemiesProjectiles.Init(128);

	for (int i = 0; i < 32; ++i)
	{
		myPlayerProjectiles.EmplaceBack();
		myPlayerProjectiles[i].Init(mySceneID, myMetrics->GetPlayerMetrics().myProjectileSpeed, "Assets/Models/projectiles/playerProjectile.fbx", 0.4f);
		myPlayerProjectiles[i].SetActive(false);
		myAvailablePlayerProjectiles.push(i);
	}

	for (int i = 0; i < 128; ++i)
	{
		myEnemiesProjectiles.EmplaceBack();
		myEnemiesProjectiles[i].Init(mySceneID, myMetrics->GetRangedMetrics().myProjectileSpeed, "Assets/Models/projectiles/rangedProjectile.fbx", 1.0f);
		myEnemiesProjectiles[i].SetActive(false);
		myAvailableEnemyProjectiles.push(i);
	}
}

void CProjectileManager::FirePlayerProjectile(const CommonUtilities::Vector3f& aStartPosition, const CommonUtilities::Vector3f& aTargetPosition)
{
	CProjectile* projectile = &myPlayerProjectiles[GetAvailablePlayerProjectileIndex()];
	projectile->Reset();
	projectile->SetActive(true);
	projectile->FireBullet(aStartPosition, aTargetPosition);
}

void CProjectileManager::FireEnemyProjectile(const CommonUtilities::Vector3f & aStartPosition, const CommonUtilities::Vector3f & aDirection)
{
	CProjectile* projectile = &myEnemiesProjectiles[GetAvailableEnemyProjectileIndex()];
	projectile->Reset();
	projectile->SetActive(true);
	projectile->FireBulletInDirection(aStartPosition, aDirection);
	projectile->SetStreakColor(CommonUtilities::Vector4f(1.f, 0.2f, 0.2f, 1.0f), CommonUtilities::Vector4f(0.5f, 0.0f, 0.0f, 0.f));
}

void CProjectileManager::Update(CommonUtilities::GrowingArray<CRangedEnemy*, int>& aRangedEnemies, CommonUtilities::GrowingArray<CExploder*, int>& aExploderEnemies, CPlayer* aPlayer)
{
	PlayerProjectileCollision(aRangedEnemies, aExploderEnemies);
	EnemyProjectileCollision(aPlayer);
	UpdatePlayerProjectiles();
	UpdateEnemyProjectiles();
}

void CProjectileManager::PlayerProjectileCollision(CommonUtilities::GrowingArray<CRangedEnemy*, int>& aRangedEnemies, CommonUtilities::GrowingArray<CExploder*, int>& aExploderEnemies)
{
	for (auto& playerProjectile : myPlayerProjectiles)
	{
		if (!playerProjectile.IsActive())
		{
			continue;
		}

		auto currentProjectilePos = playerProjectile.GetTransform().GetPosition();
		currentProjectilePos.y = 0.f;

		for (unsigned short j = 0; j < aRangedEnemies.Size(); ++j)
		{
			if (aRangedEnemies[j]->GetIsDead() || playerProjectile.IsDead())
			{
				continue;
			}
			CommonUtilities::Vector3f enemyPosition = aRangedEnemies[j]->GetTransform().GetPosition();
			enemyPosition.y = 0.f;
			if ((enemyPosition - currentProjectilePos).Length2() < myMetrics->GetPlayerMetrics().myProjectileCollisionDistanceForRanged)
			{
				aRangedEnemies[j]->SetWasHitProjectile();
				aRangedEnemies[j]->PlayHitParticle(playerProjectile.GetTransform().GetPosition());
				playerProjectile.SetIsDead(true);
			}
		}

		for (unsigned short j = 0; j < aExploderEnemies.Size(); ++j)
		{
			if (aExploderEnemies[j]->GetIsDead() || playerProjectile.IsDead())
			{
				continue;
			}
			CommonUtilities::Vector3f enemyPosition = aExploderEnemies[j]->GetTransform().GetPosition();
			enemyPosition.y = 0.f;
			if ((enemyPosition - currentProjectilePos).Length2() < myMetrics->GetPlayerMetrics().myProjectileCollisionDistanceForExploder)
			{
				aExploderEnemies[j]->SetWasHitProjectile();
				aExploderEnemies[j]->SetWasHitProjectile();
				aExploderEnemies[j]->PlayHitParticle(playerProjectile.GetTransform().GetPosition());
				playerProjectile.SetIsDead(true);
			}
		}
	}
}

void CProjectileManager::EnemyProjectileCollision(CPlayer* aPlayer)
{
	if (aPlayer->GetIsDead())
	{
		return;
	}
	for (auto& enemyProjectile : myEnemiesProjectiles)
	{
		if (!enemyProjectile.IsActive())
		{
			continue;
		}

		auto Pos = enemyProjectile.GetTransform().GetPosition();
		if ((Pos - aPlayer->GetTransform().GetPosition()).Length2() < myMetrics->GetRangedMetrics().myProjectileCollisionDistance)
		{
			aPlayer->SetWasHitProjectile();
			aPlayer->PlayBloodParticle(Pos);
			enemyProjectile.SetIsDead(true);
		}
	}
}

void CProjectileManager::UpdatePlayerProjectiles()
{
	for (int i = 0; i < myPlayerProjectiles.Size(); ++i)
	{
		if (!myPlayerProjectiles[i].IsActive())
		{
			continue;
		}

		if (myPlayerProjectiles[i].IsDead())
		{
			myAvailablePlayerProjectiles.push(i);
			myPlayerProjectiles[i].SetActive(false);
			continue;
		}
		else
		{
			myPlayerProjectiles[i].Update();
		}
	}
}

void CProjectileManager::UpdateEnemyProjectiles()
{
	for (int i = 0; i < myEnemiesProjectiles.Size(); ++i)
	{
		if (!myEnemiesProjectiles[i].IsActive())
		{
			continue;
		}

		if (myEnemiesProjectiles[i].IsDead())
		{
			myAvailableEnemyProjectiles.push(i);
			myEnemiesProjectiles[i].SetActive(false);
			continue;
		}
		else
		{
			myEnemiesProjectiles[i].Update();
		}
	}
}

int CProjectileManager::GetAvailablePlayerProjectileIndex()
{
	int returnIndex = myAvailablePlayerProjectiles.front();
	myAvailablePlayerProjectiles.pop();
	return returnIndex;
}

int CProjectileManager::GetAvailableEnemyProjectileIndex()
{
	int returnIndex = myAvailableEnemyProjectiles.front();
	myAvailableEnemyProjectiles.pop();
	return returnIndex;
}

void CProjectileManager::ClearAllBullets()
{
	for (auto& playerProjectile : myPlayerProjectiles)
	{
		playerProjectile.SetIsDead(true);
	}

	for (auto& enemyProjectile : myEnemiesProjectiles)
	{
		enemyProjectile.SetIsDead(true);
	}
}

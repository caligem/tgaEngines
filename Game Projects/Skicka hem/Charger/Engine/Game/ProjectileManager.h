#pragma once
#include "Projectile.h"
#include "ObjectPool.h"
#include <queue>

class CMetrics;
class CRangedEnemy;
class CExploder;
class CPlayer;

class CProjectileManager
{
public:
	CProjectileManager();
	~CProjectileManager();

	void Init(ID_T(CScene) aSceneID, CMetrics* aMetrics);
	void FirePlayerProjectile(const CommonUtilities::Vector3f& aStartPosition, const CommonUtilities::Vector3f& aTargetPosition);
	void FireEnemyProjectile(const CommonUtilities::Vector3f& aStartPosition, const CommonUtilities::Vector3f& aDirection);

	void Update(CommonUtilities::GrowingArray<CRangedEnemy*, int>& aRangedEnemies, CommonUtilities::GrowingArray<CExploder*, int>& aExploderEnemies, CPlayer* aPlayer);
	void ClearAllBullets();

private:
	void PlayerProjectileCollision(CommonUtilities::GrowingArray<CRangedEnemy*, int>& aRangedEnemies, CommonUtilities::GrowingArray<CExploder*, int>& aExploderEnemies);
	void UpdatePlayerProjectiles();
	void UpdateEnemyProjectiles();
	void EnemyProjectileCollision(CPlayer* aPlayer);

	int GetAvailablePlayerProjectileIndex();
	int GetAvailableEnemyProjectileIndex();
	
	CommonUtilities::GrowingArray<CProjectile, int> myPlayerProjectiles;
	CommonUtilities::GrowingArray<CProjectile, int> myEnemiesProjectiles;

	std::queue<int> myAvailablePlayerProjectiles;
	std::queue<int> myAvailableEnemyProjectiles;

	ID_T(CScene) mySceneID;
	CMetrics* myMetrics;
};


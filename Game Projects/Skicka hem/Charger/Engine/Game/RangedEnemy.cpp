#include "stdafx.h"
#include "RangedEnemy.h"
#include "IWorld.h"
#include "PollingStation.h"
#include "Metrics.h"
#include "CameraDataWrapper.h"
#include "AudioManager.h"
#include "ProjectileManager.h"

CRangedEnemy::CRangedEnemy()
{
}


CRangedEnemy::~CRangedEnemy()
{
}

void CRangedEnemy::Init(ID_T(CScene) aSceneID, CController* aController, int aAccessID, ID_T(CGameObjectData) aParentID)
{
	myIsPlayingAttackAnimation = false;
	myIsStunnedThisFrame = false;
	myStunnedLastFrame = false;
	myDied = false;

	CGameObject::Init(aSceneID, aAccessID, aParentID);
	mySceneID = aSceneID;

	CModelComponent* modelComponent = AddComponent<CModelComponent>({ "Assets/Models/enemies/enemyRanged/enemyRanged.fbx" });
	myAnimationController = AddComponent<CAnimationControllerComponent>(modelComponent);
	myAnimationController->SetAnimation("enemyRangedIdle");


	myStunParticleObject.Init(aSceneID);
	myStunParticleObject.GetTransform().SetParent(&GetTransform());
	myStunParticleObject.GetTransform().Move({ 0.0f, 3.0f, 0.0f });
	myStunParticle = myStunParticleObject.AddComponent<CParticleSystemComponent>("Assets/Particles/enemyStunned.json");

	myHitParticleObject.Init(aSceneID);
	myHitParticle = myHitParticleObject.AddComponent<CParticleSystemComponent>("Assets/Particles/bulletHitParticle.json");
	myHitParticle->Stop();

	mySteeringData.myMaxAcceleration = 1.0f;
	mySteeringData.myMaxSpeed = ourMetrics->GetRangedMetrics().myMovementSpeed;
	mySteeringData.myMaxAngularAccelleration = 0.5f;
	mySteeringData.myMaxRotation = 1.5f;
	mySteeringData.myAttackRange = ourMetrics->GetRangedMetrics().myAttackRange;
	mySteeringData.myAggroRange = ourMetrics->GetRangedMetrics().myAggroRange;
	mySteeringData.myTargetRangeForArrive = 0.05f;
	myController = aController;
	myAttackTimer = 0.f;

	CHealth::Init(ourMetrics->GetRangedMetrics().myMaxHealth);
	mySteeringData.myPosition = myStartPosition;

	CActor::myPlayerAoEDamageHandler.SetMaxMs(ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myAoEMaxMsBetweenDamageEnemies);
	CActor::myCompanionAoEDamageHandler.SetMaxMs(ourMetrics->GetCompanionMetrics().myUltimateMetrics.myAoEMaxMsBetweenDamage);
	CActor::myCompanionBasicAttackDamageHandler.SetMaxMs(ourMetrics->GetCompanionMetrics().myMsBetweenBasicAttackDamage);
	CActor::myPlayerDashDamageHandler.SetActiveTime(ourMetrics->GetPlayerMetrics().myDashMetrics.myStunTime);
}

void CRangedEnemy::Update()
{
	//for update if they change in doc
	//TODO: REMOVE
	mySteeringData.myAttackRange = ourMetrics->GetRangedMetrics().myAttackRange;
	mySteeringData.myMaxSpeed = ourMetrics->GetRangedMetrics().myMovementSpeed;
	mySteeringData.myAggroRange = ourMetrics->GetRangedMetrics().myAggroRange;
	mySteeringData.mySlowDownRadius = ourMetrics->GetRangedMetrics().mySlowDownRadius;

	if (myController == nullptr)
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "RangedEnemy controller was nullptr");
		return;
	}

	CActor::UpdateHealthBar();
	WasHitChecks();

	CActor::myPlayerAoEDamageHandler.Update();
	CActor::myCompanionAoEDamageHandler.Update();
	CActor::myCompanionBasicAttackDamageHandler.Update();
	CActor::myProjectileDamageHandler.Update();
	CActor::myPlayerDashDamageHandler.Update();

	if (myIsPlayingAttackAnimation)
	{
		float animationTimeNextFrame = (myAnimationController->GetTime() + IWorld::Time().GetDeltaTime()) * myAnimationController->GetCurrentAnimationTicksPerSecond();
		float animationMaxTime = myAnimationController->GetCurrentAnimationDuration();

		if (animationTimeNextFrame >= animationMaxTime)
		{
			myIsPlayingAttackAnimation = false;
		}
		return;
	}

	float dt = IWorld::Time().GetDeltaTime();

	if (CHealth::GetIsDead())
	{
		float animationTimeNextFrame = (myAnimationController->GetTime() + dt) * myAnimationController->GetCurrentAnimationTicksPerSecond();
		float animationMaxTime = myAnimationController->GetCurrentAnimationDuration();
		if (animationTimeNextFrame >= animationMaxTime)
		{
			myShouldBeRemoved = true;
		}
	}
	else if (!myIsStunnedThisFrame)
	{
		SSteeringOutput steeringOutput;
		SetPosition(mySteeringData.myPosition + mySteeringData.myVelocity * dt);

		const CommonUtilities::Vector3f direction = { cosf(mySteeringData.myOrientation), 0.0f, sinf(mySteeringData.myOrientation) };
		CommonUtilities::Vector3f lookDirection = CommonUtilities::Slerp(
			GetTransform().GetForward().GetNormalized(), direction.GetNormalized(), dt * 8.f);
		lookDirection.y = 0.f;
		GetTransform().SetLookDirection(lookDirection);

		steeringOutput = myController->Update(mySteeringData);

		myState = steeringOutput.myOutputState;

		if (myState == EFollowPlayer)
		{
			CActor::UpdateMovementData(steeringOutput);

			myAnimationController->SetAnimation("enemyRangedWalk");
			myAnimationController->SetLooping(true);
		}
		else if (myState == EIdle)
		{
			mySteeringData.myVelocity = { 0.f, 0.f, 0.f };

			myAnimationController->SetAnimation("enemyRangedIdle");
			myAnimationController->SetLooping(true);

			myAttackTimer += dt;
		}
		else if (myState == EAttacking)
		{
			Attack(direction);

			myAttackTimer += dt;
		}
		
		CActor::UpdateOrientationAndRotationData(steeringOutput);
	}

	if (myStunnedLastFrame && !myIsStunnedThisFrame)
	{
		myStunParticle->Stop();
	}
	else if (!myStunnedLastFrame && myIsStunnedThisFrame)
	{
		myStunParticle->Play();
	}
	myStunnedLastFrame = myIsStunnedThisFrame;
	myIsStunnedThisFrame = false;
}

void CRangedEnemy::SetPosition(const CommonUtilities::Vector3f & aPosition)
{
	GetTransform().SetPosition(aPosition);
	mySteeringData.myPosition = aPosition;
}

void CRangedEnemy::Respawn()
{
	myAttackTimer = 1.f;
	ResetTransform();
	SetIsAlive();
	CHealth::Init(ourMetrics->GetRangedMetrics().myMaxHealth);
	CActor::ResetDamageHandlers();

	myController->Reset();
	mySteeringData.myHasSeenPlayer = false;
}

void CRangedEnemy::Destroy()
{
	CGameObject::Destroy();
}

void CRangedEnemy::PlayHitParticle(const CommonUtilities::Vector3f & aPosition)
{
	myHitParticleObject.GetTransform().SetPosition(aPosition);
	myHitParticle->Play();
}

void CRangedEnemy::Attack(const CommonUtilities::Vector3f& aDirection)
{	
	mySteeringData.myVelocity = { 0.f, 0.f, 0.f };

	if (myAttackTimer >= ourMetrics->GetRangedMetrics().myAttackSpeed)
	{
		myAnimationController->SetAnimationForced("enemyRangedAttack");
		myAnimationController->SetLooping(false);
		myIsPlayingAttackAnimation = true;

		ourProjectileManager->FireEnemyProjectile(GetTransform().GetPosition() + CommonUtilities::Vector3f(0.f, 0.5f, 0.f), aDirection);

		myAttackTimer = 0.f;
		AM.PlayNewInstance("SFX/Enemy/Ranged/Ranged_Attack");
	}
	else
	{
		myAnimationController->SetAnimation("enemyRangedIdle");
		myAnimationController->SetLooping(true);
	}
}

void CRangedEnemy::WasHitChecks()
{
	bool myHit = false;
	if (CActor::myProjectileDamageHandler.GetWasHit())
	{
		myHit = true;
		CHealth::TakeDamage(ourMetrics->GetPlayerMetrics().myAttackDamage);
	}

	if (CActor::myPlayerAoEDamageHandler.GetWasHit())
	{
		myHit = true;
		CHealth::TakeDamage(ourMetrics->GetPlayerMetrics().myGrenadeMetrics.myDamage);
	}

	if (CActor::myCompanionAoEDamageHandler.GetWasHit())
	{
		myHit = true;
		CHealth::TakeDamage(ourMetrics->GetCompanionMetrics().myUltimateMetrics.myDamage);
	}

	if (CActor::myCompanionBasicAttackDamageHandler.GetWasHit())
	{
		myHit = true;
		CHealth::TakeDamage(ourMetrics->GetCompanionMetrics().myBasicAttackDamage);
	}

	if (mySoundTimer <= 0.f && myHit)
	{
		AM.PlayNewInstance("SFX/Enemy/Ranged/Hurt");
		mySoundTimer = 0.4f;
	}
	mySoundTimer -= IWorld::Time().GetDeltaTime();


	if (CHealth::GetIsDead())
	{
		if (!myDied)
		{
			AM.Stop("SFX/Enemy/Ranged/Hurt", true);
			AM.PlayNewInstance("SFX/Enemy/Ranged/Death");
			myDied = !myDied;
		}
		myAnimationController->SetAnimation("enemyRangedDeath");
		myAnimationController->SetLooping(false);
	}

	if (CActor::myPlayerDashDamageHandler.GetWasHit()) //stun
	{
		if (!CHealth::GetIsDead())
		{
			myAnimationController->SetAnimation("enemyRangedIdle");
			myIsStunnedThisFrame = true;
			myIsPlayingAttackAnimation = false;
		}
	}
}

#pragma once
#include "Actor.h"

class CRangedEnemy : public CActor
{
public:
	CRangedEnemy();
	~CRangedEnemy();

	void Init(ID_T(CScene) aSceneID, CController* aController, int aAccessID, ID_T(CGameObjectData) aParentID = ID_T_INVALID(CGameObjectData)) override;
	void Update();
	
	void SetPosition(const CommonUtilities::Vector3f& aPosition) override;
	void Respawn() override;
	void Destroy();

	void PlayHitParticle(const CommonUtilities::Vector3f& aPosition);
private:
	void Attack(const CommonUtilities::Vector3f& aDirection);
	void WasHitChecks();

	ID_T(CScene) mySceneID;
	CGameObject myStunParticleObject;
	CParticleSystemComponent* myStunParticle;
	CGameObject myHitParticleObject;
	CParticleSystemComponent* myHitParticle;
	bool myIsStunnedThisFrame;
	bool myStunnedLastFrame;
	bool myDied;
	float mySoundTimer;
	//maybe event based later
	float myAttackTimer;
	bool myIsPlayingAttackAnimation;
};


#include "stdafx.h"
#include "ShowroomState.h"

#include <InputManager.h>
#include <IWorld.h>

#include <Quaternion.h>
#include <XBOXController.h>

#include "Random.h"

#include <filesystem>

CShowroomState::CShowroomState()
{
}

CShowroomState::~CShowroomState()
{
}

bool CShowroomState::Init()
{
	CState::Init();

	myCycleIndex = 0;

	std::sort(myCyclePositions.begin(), myCyclePositions.end(), [](auto a, auto b)
	{
		if (std::fabsf(a.z - b.z) < 10.f)
		{
			return a.x > b.x;
		}
		return a.z > b.z;
	});

	myPivot = { 0.f, 0.f, 0.f };
	if(!myCyclePositions.empty()) myPivot = myCyclePositions[myCycleIndex];
	myRotation = { 0.f, CommonUtilities::Pif };
	myZoom = 20.f;

	LoadModels();

	CGameObject dir1;
	dir1.Init(mySceneID);
	dir1.GetTransform().SetLookDirection({ -1.f, 1.f, 1.f });
	CLightComponent* light1 = dir1.AddComponent<CLightComponent>();
	light1->SetType(CLightComponent::ELightType_Directional);
	light1->SetColor({ 1.f, 0.8f, 0.5f });

	CGameObject dir2;
	dir2.Init(mySceneID);
	dir2.GetTransform().SetLookDirection({ 1.f, 0.f, -1.f });
	CLightComponent* light2 = dir2.AddComponent<CLightComponent>();
	light2->SetType(CLightComponent::ELightType_Directional);
	light2->SetColor({ 0.5f, 0.8f, 1.f });

	CGameObject dir3;
	dir3.Init(mySceneID);
	dir3.GetTransform().SetLookDirection({ 0.f, -1.f, 0.f });
	CLightComponent* light3 = dir3.AddComponent<CLightComponent>();
	light3->SetType(CLightComponent::ELightType_Directional);
	light3->SetColor({ 0.9f, 0.9f, 0.9f });

	//IWorld::GetSceneManager().GetSceneAt(mySceneID)->SetSkybox("Assets/CubeMaps/skybox.dds");
	//IWorld::GetSceneManager().GetSceneAt(mySceneID)->SetCubemap("Assets/CubeMaps/cubemap.dds");

	return true;
}

EStateUpdate CShowroomState::Update()
{
	HandleControls();

	IWorld::SetDebugColor({ 1.f, 0.f, 0.f, 1.f });
	IWorld::DrawDebugArrow({ 0.f, 0.f, 0.f }, { 5.f, 0.f, 0.f });
	IWorld::SetDebugColor({ 0.f, 1.f, 0.f, 1.f });
	IWorld::DrawDebugArrow({ 0.f, 0.f, 0.f }, { 0.f, 5.f, 0.f });
	IWorld::SetDebugColor({ 0.f, 0.f, 1.f, 1.f });
	IWorld::DrawDebugArrow({ 0.f, 0.f, 0.f }, { 0.f, 0.f, 5.f });

	if (myShouldPop)
	{
		return EPop_Main;
	}
	return EDoNothing;
}

void CShowroomState::OnEnter()
{
	SetActiveScene();
	myShouldPop = false;
}

void CShowroomState::OnLeave()
{
}

void CShowroomState::HandleControls()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Vector2f movement = input.GetMouseMovement();
	movement.x *= (2.f*CommonUtilities::Pif) / 1280.f;
	movement.y *= (CommonUtilities::Pif) / 720.f;

	if (input.IsKeyDown(Input::Key_Alt))
	{
		if (input.IsButtonDown(Input::Button_Left))
		{
			myRotation.x += movement.y;
			myRotation.y += movement.x;

			if (myRotation.x >= CommonUtilities::Pif / 2.f)
			{
				myRotation.x = CommonUtilities::Pif / 2.f - 0.001f;
			}
			if (myRotation.x <= -CommonUtilities::Pif / 2.f)
			{
				myRotation.x = -CommonUtilities::Pif / 2.f + 0.001f;
			}
		}
		else if (input.IsButtonDown(Input::Button_Middle))
		{
			myPivot -= myMainCamera.GetTransform().GetRight() * movement.x * myZoom;
			myPivot += myMainCamera.GetTransform().GetUp() * movement.y * myZoom;
		}
		else if (input.IsButtonDown(Input::Button_Right))
		{
			myZoom -= (movement.x + movement.y) * myZoom;
			if (myZoom < 0.1f)
			{
				myZoom = 0.1f;
				myPivot += myMainCamera.GetTransform().GetForward() * (movement.x + movement.y);
			}
		}
	}

	if (input.IsKeyPressed(Input::Key_Left))
	{
		if (myCycleIndex == 0)
		{
			myCycleIndex = myCyclePositions.size() - 1;
		}
		else
		{
			--myCycleIndex;
		}
		myPivot = myCyclePositions[myCycleIndex];
	}
	if (input.IsKeyPressed(Input::Key_Right))
	{
		++myCycleIndex;
		if (myCycleIndex == myCyclePositions.size())
		{
			myCycleIndex = 0;
		}
		myPivot = myCyclePositions[myCycleIndex];
	}

	if (input.IsKeyPressed(Input::Key_Escape) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_Back))
	{
		myShouldPop = true;
	}

	CommonUtilities::Vector3f newPos(0.f, 0.f, -myZoom);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundX(myRotation.x);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundY(myRotation.y);
	newPos += myPivot;

	myMainCamera.GetTransform().SetPosition(newPos);
	myMainCamera.GetTransform().LookAt(myPivot);
}

void CShowroomState::GetModelPaths()
{
	std::string rootDir = "Assets/Models";
	std::experimental::filesystem::path rootPath = std::experimental::filesystem::current_path().append(rootDir);
	GetFBXPathInFolder(rootPath.c_str());
}

void CShowroomState::GetFBXPathInFolder(const wchar_t* aFolderPath)
{
	std::experimental::filesystem::directory_iterator modelFolderIt(aFolderPath);
	for (auto& file : modelFolderIt)
	{
		if (file.path().has_extension())
		{
			if (file.path().extension() == ".fbx")
			{
				SModelFileData data;

				std::string modelPath = file.path().generic_string();
				data.myPath = modelPath.substr(std::experimental::filesystem::current_path().generic_string().size() + 1);
				
				auto time = std::experimental::filesystem::last_write_time(file);
				data.myTimeSinceEpoch = time.time_since_epoch().count();
				
				myModelFileDatas.push_back(data);
			}
		}
		else
		{
			GetFBXPathInFolder(file.path().c_str());
		}
	}
}

void CShowroomState::LoadModels()
{
	GetModelPaths();

	float offset = 30.f;

	std::sort(myModelFileDatas.begin(), myModelFileDatas.end(), [&](const SModelFileData& a, const SModelFileData& b) { return a.myTimeSinceEpoch > b.myTimeSinceEpoch; });

	for (unsigned short i = 0; i < myModelFileDatas.size(); ++i)
	{
		CGameObject model;
		model.Init(mySceneID);
		auto* modelComponent = model.AddComponent<CModelComponent>({ myModelFileDatas[i].myPath.c_str() });
		if(modelComponent->HasAnimations())
		{
			model.AddComponent<CAnimationControllerComponent>(modelComponent);
		}
		CommonUtilities::Vector3f position(-(i * offset), 0.f, 0.f);
		model.GetTransform().SetPosition(position);
		myCyclePositions.push_back(position);
	}
}

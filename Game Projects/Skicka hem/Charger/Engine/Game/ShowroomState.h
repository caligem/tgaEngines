#pragma once
#include "State.h"
#include <GrowingArray.h>
#include <Vector.h>

class CShowroomState : public CState
{
	struct SModelFileData
	{
		std::string myPath;
		long long myTimeSinceEpoch;
	};

public:
	CShowroomState();
	~CShowroomState();

	bool Init();
	EStateUpdate Update() override;
	void OnEnter() override;
	void OnLeave() override;

private:
	void HandleControls();
	void GetModelPaths();
	void GetFBXPathInFolder(const wchar_t* aFolderPath);
	void LoadModels();

	CGameObject myControlsInfo;

	CommonUtilities::Vector3f myPivot;
	CommonUtilities::Vector2f myRotation;
	float myZoom;

	std::vector<CommonUtilities::Vector3f> myCyclePositions;
	size_t myCycleIndex;

	std::vector<SModelFileData> myModelFileDatas;
};


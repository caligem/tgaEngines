#include "stdafx.h"
#include "SteeringBehaviour.h"
#include "Random.h"
#include <iostream>


CSteeringBehaviour::CSteeringBehaviour()
{
	myPathTicket = ID_T_INVALID(SPath);
	myHasPath = false;
}


CSteeringBehaviour::~CSteeringBehaviour()
{
}

SSteeringOutput CSteeringBehaviour::DoSeek(const SSteeringData & aActorData, const CommonUtilities::Vector3f & aTargetPos)
{
	SSteeringOutput output;
	output.myLinearOutput = aTargetPos - aActorData.myPosition;
	output.myLinearOutput.Normalize();
	output.myLinearOutput *= aActorData.myMaxAcceleration;

	return output;
}

SSteeringOutput CSteeringBehaviour::DoArrive(const SSteeringData& aActorData, const CommonUtilities::Vector3f& aTargetPos, const float& aTargetRange /*= 0.f*/, float aSlowDownRadius /*= 1.f*/)
{
	CommonUtilities::Vector3f direction = aTargetPos - aActorData.myPosition;
	float distance = direction.Length2();
	const float slowDownRadius = aSlowDownRadius;
	float targetRadius = aTargetRange;
	 
	if (distance <= targetRadius*targetRadius)
	{
		return SSteeringOutput();
	}

	if (distance < slowDownRadius*slowDownRadius)
	{
		SSteeringOutput output;
		output.myLinearOutput = direction.GetNormalized() * aActorData.myMaxSpeed * (distance / (slowDownRadius*slowDownRadius));
		return output;
	}
	else
	{
		SSteeringOutput output;
		output.myLinearOutput = direction.GetNormalized() * aActorData.myMaxSpeed;
		return output;
	}
}

SSteeringOutput CSteeringBehaviour::DoKinematicFace(const SSteeringData& aActorData, const CommonUtilities::Vector3f& aTargetPos)
{
	CommonUtilities::Vector3f direction = (aTargetPos - aActorData.myPosition).GetNormalized();
	SSteeringOutput output;
	output.myAngularOutput = { atan2f(direction.z, direction.x) };
	return output;
}

SSteeringOutput CSteeringBehaviour::DoWander(const SSteeringData& aActorStats)
{
	SSteeringOutput output;
	const float CIRCLEDISTANCE = 0.6f;
	const float CIRCLERADIUS = 0.04f;
	const float ANGLECHANGE = 0.05f;

	CommonUtilities::Vector3f circleCenter = aActorStats.myVelocity.GetNormalized();
	circleCenter *= CIRCLEDISTANCE;

	CommonUtilities::Vector3f displacement(0.f, 0.f, -1.f);
	displacement *= CIRCLERADIUS;

	const float length = displacement.Length();
	displacement.x = static_cast<float>(std::cos(myWanderAngle) * length);
	displacement.z = static_cast<float>(std::sin(myWanderAngle) * length);
	
	myWanderAngle += CommonUtilities::RandomRange(ANGLECHANGE, -ANGLECHANGE);

	output.myLinearOutput = (circleCenter + displacement) * 2.0f;

	return output;
}

SSteeringOutput CSteeringBehaviour::DoEvade(const SSteeringData& aActorStats, const CommonUtilities::Vector3f& aPositionToEvade, const float aRadius)
{
	SSteeringOutput output;

	float dist = (aActorStats.myPosition - aPositionToEvade).Length();
	if (dist < aRadius)
	{
		output.myLinearOutput = (aActorStats.myPosition - aPositionToEvade).GetNormalized() * (1.f - dist/aRadius);
	}
	else
	{
		output.myLinearOutput = CommonUtilities::Vector3f();
	}

	output.myLinearOutput.y = 0.f;

	return output;
}

SSteeringOutput CSteeringBehaviour::DoAvoidAcid(const SSteeringData & aActorStats, const CommonUtilities::Vector3f & aPlayerPosition, const CommonUtilities::Vector3f & aPositionToAvoid, const float aRadius)
{
	SSteeringOutput output;

	const CommonUtilities::Vector3<float> directionToTarget = (aActorStats.myPosition - aPositionToAvoid);
	const CommonUtilities::Vector3<float> directionToRun = (aActorStats.myPosition - aPlayerPosition);
	const float distance = directionToTarget.Length();

	if (distance < aRadius && distance > 0.05f)
	{
		if (distance < 0.001f)
		{
			output.myLinearOutput = directionToRun * 100.0f;
		}
		output.myLinearOutput += directionToRun * ((0.75f / std::pow((distance / 2.2f) / 0.5f, 2) - 0.1f));
	}

	return output;
}

#pragma once
#include "Vector3.h"
#include "PathFinder.h"

enum ETarget
{
	EPlayer,
	EEnemies
};

struct SSteeringData
{
	CommonUtilities::Vector3f myPosition;
	CommonUtilities::Vector3f myVelocity;
	float myOrientation = 0.f;
	float myRotation = 0.f;

	float myMaxAcceleration = 0.f;
	float myMaxSpeed = 0.f;
	float myMaxAngularAccelleration = 0.f;
	float myMaxRotation = 0.f;
	float myAggroRange = 0.f;
	float myAttackRange = 0.f;
	float myTargetRangeForArrive = 0.f;
	float mySlowDownRadius = 0.f;
	bool myHasSeenPlayer = false;
};

enum EState
{
	EFollowPlayer,
	EAttacking,
	EIdle,

	//companion only
	EAcidCleanup,
	EUltimateAoE
};

enum EPathFindingState
{
	EPathFindPlayer,
	EPathFindCleanup,
	EPathFindUltimate,
	EPathFindAttack,
	EPathFindNone
};

struct SSteeringOutput
{
	CommonUtilities::Vector3f myLinearOutput;
	float myAngularOutput = 0;
	EState myOutputState;
};

class CSteeringBehaviour
{
public:
	CSteeringBehaviour();
	~CSteeringBehaviour();

	//virtual SSteeringOutput GetSteering(const SKinematic& aCharacter, const CommonUtilities::Vector3f &aTargetPosition) = 0;
	SSteeringOutput DoSeek(const SSteeringData & aActorData, const CommonUtilities::Vector3f& aTargetPos);
	SSteeringOutput DoArrive(const SSteeringData& aActorData, const CommonUtilities::Vector3f& aTargetPos, const float& aTargetRange = 0.f, float aSlowDownRadius = 1.f);
	SSteeringOutput DoKinematicFace(const SSteeringData& aActorData, const CommonUtilities::Vector3f& aTargetPos);
	SSteeringOutput DoWander(const SSteeringData& aActorStats);
	SSteeringOutput DoEvade(const SSteeringData& aActorStats, const CommonUtilities::Vector3f& aPositionToEvade, const float aRadius = 3.0f);
	SSteeringOutput DoAvoidAcid(const SSteeringData& aActorStats, const CommonUtilities::Vector3f& aPlayerPosition, const CommonUtilities::Vector3f& aPositionToAvoid, const float aRadius = 3.0f);

	inline PathTicket& GetPathTicket() { return myPathTicket; }
	inline PathPoints& GetPathPoints() { return myPathPoints; }
	inline EPathFindingState GetPathFindingState() const { return myPathFindingState; }
	inline void SetPathFindingState(EPathFindingState aPathFindingState) { myPathFindingState = aPathFindingState; }
	inline const bool HasPath() { return myHasPath; }
	void SetHasPath(bool aHasPath) { myHasPath = aHasPath; }

private:
	float myWanderAngle = 0.0f;
	PathTicket myPathTicket;
	PathPoints myPathPoints;
	EPathFindingState myPathFindingState;
	bool myHasPath;
};


#include "stdafx.h"
#include "TriggerBox.h"

#include "IWorld.h"
#include "ScriptManager.h"

#include "ICollision.h"

CTriggerBox::CTriggerBox()
{
	myIsColliding = false;
}

CTriggerBox::~CTriggerBox()
{
}

void CTriggerBox::Init(const short aTriggerID, const CommonUtilities::Vector3f& aPosition, const CommonUtilities::Vector3f& aRotation, const CommonUtilities::Vector3f& aScale)
{
	myTriggerID = aTriggerID; aPosition;
	myOBB.SetPosition(GetTransform().GetPosition());
	myOBB.SetOrientation(aRotation);
	myOBB.SetRadius(aScale);
}

void CTriggerBox::Update()
{

}

bool CTriggerBox::CheckCollition(const CColliderSphere & aColiderSphere)
{
	CommonUtilities::Vector3f intersection;
	const bool colliding = ICollision::SphereVsOBB(aColiderSphere, myOBB, &intersection);

	if (!myIsColliding && colliding)
	{
		myIsColliding = true;
		IWorld::Script().NotifyListeners(CScriptEventManager::EScriptEvent_OnEnter, myTriggerID);
		return true;
	}
	else if (myIsColliding && !colliding)
	{
		myIsColliding = false;
		IWorld::Script().NotifyListeners(CScriptEventManager::EScriptEvent_OnLeave, myTriggerID);
	}
	IWorld::DrawDebugWireCube(myOBB.GetPosition(), myOBB.GetRadius(), myOBB.GetRotation());
	return myIsColliding;
}



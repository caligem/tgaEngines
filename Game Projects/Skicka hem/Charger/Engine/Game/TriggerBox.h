#pragma once
#include "GameObject.h"

#include "ColliderOBB.h"
#include "ColliderSphere.h"

class CTriggerBox : public CGameObject
{
public:
	CTriggerBox();
	~CTriggerBox();

	void Init(const short aTriggerID, const CommonUtilities::Vector3f& aSize, const CommonUtilities::Vector3f& aRotation, const CommonUtilities::Vector3f& aScale);
	void Update();
	bool CheckCollition(const CColliderSphere& aColiderSphere);
private:
	short myTriggerID;
	CColliderOBB myOBB;
	bool myIsColliding;
};


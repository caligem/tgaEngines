﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace LevelEditor
{
	public partial class LevelEditorControl : UserControl
	{
		private EditorControls.EngineCanvas myEngineCanvas = null;

		public LevelEditorControl()
		{
			InitializeComponent();
		}

		EditorControls.TransformView tc = new EditorControls.TransformView();
		private void Init(object sender, EventArgs e)
		{
			myEngineCanvas = new EditorControls.EngineCanvas();
			myCanvas.Child = myEngineCanvas;
            myCanvas.LostFocus += MyCanvas_LostFocus;
            myCanvas.GotFocus += MyCanvas_GotFocus;

			Expander ex = new Expander();
			ex.Header = "Transform";
			ex.Content = tc;
			myComponents.Children.Add(ex);

			tc.OnPositionChanged += Tc_OnPositionChanged;
			tc.OnRotationChanged += Tc_OnRotationChanged;
			tc.OnScaleChanged += Tc_OnScaleChanged;

            myEngineCanvas.myBridge.Subscribe(PostMaster.EMessageType.EMessageType_Transform_SetPosition, (byte[] aData, PostMaster.EDataType aDataType) =>
            {
                if (aDataType != PostMaster.EDataType.EDataType_Vector3) return;

                EditorClasses.Vector3 vec = new EditorClasses.Vector3(aData);
                tc.SetPosition(vec);
            });
            myEngineCanvas.myBridge.Subscribe(PostMaster.EMessageType.EMessageType_Transform_SetRotation, (byte[] aData, PostMaster.EDataType aDataType) =>
            {
                if (aDataType != PostMaster.EDataType.EDataType_Vector3) return;

                EditorClasses.Vector3 vec = new EditorClasses.Vector3(aData);
                tc.SetRotation(vec);
            });
            myEngineCanvas.myBridge.Subscribe(PostMaster.EMessageType.EMessageType_Transform_SetScale, (byte[] aData, PostMaster.EDataType aDataType) =>
            {
                if (aDataType != PostMaster.EDataType.EDataType_Vector3) return;

                EditorClasses.Vector3 vec = new EditorClasses.Vector3(aData);
                tc.SetScale(vec);
            });
		}

        private void MyCanvas_GotFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            myEngineCanvas.GetFocus();
        }

        private void MyCanvas_LostFocus(object sender, System.Windows.RoutedEventArgs e)
        {
            myEngineCanvas.LoseFocus();
        }

        private void Tc_OnScaleChanged(object sender, EditorClasses.Vector3 aVector)
		{
			myEngineCanvas.myBridge.SendMessage(aVector, PostMaster.EMessageType.EMessageType_Transform_SetScale, PostMaster.EDataType.EDataType_Vector3);
		}
		private void Tc_OnRotationChanged(object sender, EditorClasses.Vector3 aVector)
		{
			myEngineCanvas.myBridge.SendMessage(aVector, PostMaster.EMessageType.EMessageType_Transform_SetRotation, PostMaster.EDataType.EDataType_Vector3);
		}
		private void Tc_OnPositionChanged(object sender, EditorClasses.Vector3 aVector)
		{
			myEngineCanvas.myBridge.SendMessage(aVector, PostMaster.EMessageType.EMessageType_Transform_SetPosition, PostMaster.EDataType.EDataType_Vector3);
		}

		private void Shutdown(object sender, EventArgs e)
		{
            myEngineCanvas.LoseFocus();
			myEngineCanvas.Shutdown();
		}

		private void Button_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			Microsoft.Win32.OpenFileDialog of = new Microsoft.Win32.OpenFileDialog();

			if(of.ShowDialog().Value == true)
			{
				if(of.FileName.Contains("Assets\\"))
				{
					myEngineCanvas.myBridge.SendMessageString(
						of.FileName.Substring(of.FileName.IndexOf("Assets\\")),
						PostMaster.EMessageType.EMessageType_MeshFilter_AddMesh, PostMaster.EDataType.EDataType_String
					);
				}
			}
		}
	}
}

#include "stdafx.h"
#include "GameServer.h"


CGameServer::CGameServer()
{
}


CGameServer::~CGameServer()
{
}

void CGameServer::AddClient(unsigned short aClientID)
{
	myClients.push_back(aClientID);
}

void CGameServer::RemoveClient(unsigned short aClientID)
{
	for (unsigned short i = 0; i < myClients.size(); ++i)
	{
		if (myClients[i] == aClientID)
		{
			myClients.erase(myClients.begin() + i);
			break;
		}
	}
}

void CGameServer::AddDeathLocation(const CommonUtilities::Vector3f & aDeathLocation)
{
	myDeathLocations.push_back(aDeathLocation);
	//create message, send to each client on GS;
}

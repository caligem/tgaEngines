#pragma once
#include <vector>
#include "Vector.h"

class CGameServer
{
public:
	CGameServer();
	~CGameServer();

	inline void SetServerID(char aID) { myServerID = aID; }
	inline const char GetServerID() const { return myServerID; }
	void AddClient(unsigned short aClientID);
	void RemoveClient(unsigned short aClientID);

	void AddDeathLocation(const CommonUtilities::Vector3f& aDeathLocation);

private:
	char myServerID;
	std::vector<unsigned short> myClients;
	//TODO: maybe add timer on death so it doesnt get overflowed;
	std::vector<CommonUtilities::Vector3f> myDeathLocations;
};


#include "stdafx.h"
#include "IServerNetwork.h"


IServerNetwork::IServerNetwork()
{
}


IServerNetwork::~IServerNetwork()
{
}

void IServerNetwork::SendToClient(CNetMessage * aMessage, CAddressWrapper & aClientAddress)
{
	ourNetMessageManager->PackAndSendMessage(aMessage, aClientAddress);
}

#include "stdafx.h"
#include "MainServer.h"
#include <iostream>

CMainServer::CMainServer()
	: myNetworkSystem(myTimer, myUIMediator)
	, myUI(myUIMediator)
{
}


CMainServer::~CMainServer()
{
}

bool CMainServer::Init()
{
	if (!myWindow.Init())
	{
		printf("Failed to init Main Server Window.");
		return false;
	}
	if (!myUI.Init(myWindow.GetHWND(), myWindow.GetDevice(), myWindow.GetContext(), myWindow.GetWindowSize()))
	{
		printf("Failed to init Main Server UI.");
		return false;
	}
	
	if (!myNetworkSystem.Init())
	{
		myUIMediator.AddToLog("Failed to init Main Server Network System.", CMainServerUIMediator::EMessageColor::Error);
		return false;
	}
	else
	{
		myNetworkSystem.GoOnline();
	}

	myUI.SetGoOnlineCallback(std::bind(&CMainServerNetworkSystem::GoOnline, &myNetworkSystem));
	myUI.SetGoOfflineCallback(std::bind(&CMainServerNetworkSystem::GoOffline, &myNetworkSystem));
	myUI.SetSendLuaCommandCallback(std::bind(&CMainServerNetworkSystem::SendLuaCommandToClient, &myNetworkSystem, std::placeholders::_1, std::placeholders::_2));
	myUI.SetBroadcastLuaCommandCallback(std::bind(&CMainServerNetworkSystem::BroadcastLuaCommand, &myNetworkSystem, std::placeholders::_1));
	myUIMediator.AddToLog("Server startup successful", CMainServerUIMediator::EMessageColor::Valid);
	return true;
}

void CMainServer::Update()
{
	myTimer.Update();
	myNetworkSystem.Update();
	myUI.Update();
}

void CMainServer::Render()
{
	myWindow.BeginFrame();
	myUI.Render();
	myWindow.EndFrame();
}

void CMainServer::Shutdown()
{
	myNetworkSystem.Shutdown();
	myWindow.Shutdown();
}

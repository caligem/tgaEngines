#pragma once
#include "MainServerWindow.h"
#include "MainServerUI.h"
#include "MainServerNetworkSystem.h"
#include "MainServerUIMediator.h"

class CMainServer
{
public:
	CMainServer();
	~CMainServer();

	bool Init();
	void Update();
	void Render();
	void Shutdown();

private:
	CMainServerWindow myWindow;
	CMainServerUI myUI;
	CMainServerUIMediator myUIMediator;
	CMainServerNetworkSystem myNetworkSystem;
	CommonUtilities::Timer myTimer;
};


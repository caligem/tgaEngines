#include "stdafx.h"
#include "MainServerClients.h"


CMainServerClients::CMainServerClients()
	: myClientCount(0)
{
	for (int i = 0; i < myMaxClients; ++i)
	{
		myFreeIndices.push_back(i);
	}
}


CMainServerClients::~CMainServerClients()
{
}

const CMainServerClients::SClientData* CMainServerClients::AddClient(CAddressWrapper& aClient)
{
	if (myClientCount >= myClients.size())
	{
		return nullptr;
	}

	SClientData data;
	data.myAddress = aClient;
	data.myID = static_cast<unsigned short>(myFreeIndices.front());
	myFreeIndices.pop_front();

	myClients[myClientCount] = data;
	myClientCount++;

	return &myClients[myClientCount-1];
}

void CMainServerClients::RemoveClientByIndex(unsigned short aClientID)
{
	myFreeIndices.push_front(myClients[aClientID].myID);
	myClients[aClientID] = std::move(myClients[myClientCount - 1]);
	myClients[myClientCount - 1] = SClientData();
	myClientCount--;
}

void CMainServerClients::RemoveClientByID(unsigned short aClientID)
{
	for (unsigned short i = 0; i < myClientCount; ++i)
	{
		if (myClients[i].myID == aClientID)
		{
			RemoveClientByIndex(i);
		}
	}
}

CMainServerClients::SClientData* CMainServerClients::GetClientByID(unsigned short aClientID)
{
	for (unsigned short i = 0; i < myClientCount; ++i)
	{
		if (myClients[i].myID == aClientID)
		{
			return &myClients[i];
		}
	}

	return nullptr;
}

CMainServerClients::SClientData * CMainServerClients::GetClientByIndex(unsigned short aIndex)
{
	if (aIndex >= myClientCount)
	{
		return nullptr;
	}
	return &myClients[aIndex];
}

#pragma once
#include "AddressWrapper.h"
#include <string>
#include <array>
#include <deque>

class CMainServerClients
{
public:
	struct SClientData
	{
		CAddressWrapper myAddress;
		unsigned short myID = 0;
		std::string myName = "";
		unsigned int myPing = 0;
		unsigned int myRTT = 0;
		float myLatestPingTimer = 0.f;
		char myGameServerID = -1;
	};

	CMainServerClients();
	~CMainServerClients();

	const CMainServerClients::SClientData* AddClient(CAddressWrapper& aClient);
	void RemoveClientByIndex(unsigned short aIndex);
	void RemoveClientByID(unsigned short aClientID);

	SClientData* GetClientByID(unsigned short aClientID);
	SClientData* GetClientByIndex(unsigned short aIndex);
	const unsigned short GetClientCount() const { return myClientCount; }

	const std::array<SClientData, 512>& GetClients() const { return myClients; }
	const unsigned short GetMaxClients() const { return myMaxClients; }

private:
	static constexpr unsigned short myMaxClients = 512;
	std::array<SClientData, 512> myClients;
	std::deque<int> myFreeIndices;
	unsigned short myClientCount;
};


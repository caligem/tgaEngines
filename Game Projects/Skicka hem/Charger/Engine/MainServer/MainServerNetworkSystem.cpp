#include "stdafx.h"
#include "MainServerNetworkSystem.h"

#include <iostream>

#define DEFAULT_PORT 8888

#include "CommonNetworkIncludes.h"
#include "NetMessageIncludes.h"
#include "IServerNetwork.h"

CMainServerNetworkSystem::CMainServerNetworkSystem(CommonUtilities::Timer& aTimer, CMainServerUIMediator& aUIMediator)
	: myTimer(aTimer)
	, myNetMessageManager(aTimer)
	, myUIMediator(aUIMediator)
{
}


CMainServerNetworkSystem::~CMainServerNetworkSystem()
{
}

bool CMainServerNetworkSystem::Init()
{
	if (!myNetMessageManager.Init(1))
	{
		myUIMediator.AddToLog("NetMessageManager failed to init", CMainServerUIMediator::EMessageColor::Error);
		return false;
	}
	myNetMessageManager.SetSenderID(myClients.GetMaxClients());

	myUptime = 0;
	myUIMediator.SetServerStatusText("OFFLINE", CMainServerUIMediator::EMessageColor::Error);

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessageChatMessage>(
		std::bind(&CMainServerNetworkSystem::HandleChatMessage, this, std::placeholders::_1, std::placeholders::_2));

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessageClientName>(
		std::bind(&CMainServerNetworkSystem::SaveClientName, this, std::placeholders::_1, std::placeholders::_2));

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessageConnect>(
		std::bind(&CMainServerNetworkSystem::HandleConnectionMessage, this, std::placeholders::_1, std::placeholders::_2));

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessagePing>(
		std::bind(&CMainServerNetworkSystem::RecievePings, this, std::placeholders::_1, std::placeholders::_2));

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessagePlayerDeath>(
		std::bind(&CMainServerNetworkSystem::PlayerDeathReceived, this, std::placeholders::_1, std::placeholders::_2));

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessagePlayerDeathLocationList>(
		std::bind(&CMainServerNetworkSystem::SendPlayerDeathLocations, this, std::placeholders::_1, std::placeholders::_2));

	IServerNetwork::ourNetMessageManager = &myNetMessageManager;

	myDeathLocations.resize(3);

	return true;
}

void CMainServerNetworkSystem::Shutdown()
{
	myNetMessageManager.CloseSocket();
}

void CMainServerNetworkSystem::Update()
{
	if(myNetMessageManager.IsSocketOpen())
	{
		myUptime += myTimer.GetRealDeltaTime();

		myNetMessageManager.RecieveMessage();
		UpdatePings();
	}

	myUIMediator.SetUptime(static_cast<unsigned int>(myUptime));
	myUIMediator.UpdateClientData(myClients.GetClientCount(), myClients.GetClients());
}

void CMainServerNetworkSystem::GoOnline()
{
	myNetMessageManager.OpenSocket(DEFAULT_PORT);
	myUIMediator.SetServerStatusText("ONLINE", CMainServerUIMediator::EMessageColor::Valid);
}

void CMainServerNetworkSystem::GoOffline()
{
	if (myNetMessageManager.CloseSocket())
	{
		KickAllClients();
		myUIMediator.SetServerStatusText("OFFLINE", CMainServerUIMediator::EMessageColor::Error);
		myUptime = 0;
	}
}

void CMainServerNetworkSystem::SendLuaCommandToClient(const std::string& aCommand, unsigned short aClientID)
{
	SendLuaCommandMessage(aCommand, aClientID);
}

void CMainServerNetworkSystem::BroadcastLuaCommand(const std::string & aCommand)
{
	for (unsigned short index = 0; index < myClients.GetClientCount(); index++)
	{
		unsigned short id = myClients.GetClientByIndex(index)->myID;
		SendLuaCommandMessage(aCommand, id);
	}
}

void CMainServerNetworkSystem::HandleChatMessage(CNetMessage* aMessage, CAddressWrapper&)
{
	CNetMessageChatMessage* message = static_cast<CNetMessageChatMessage*>(aMessage);
	CMainServerClients::SClientData* client = myClients.GetClientByID(aMessage->GetSenderID());
	if (client)
	{
		std::string printMessage = client->myName + ": " + message->GetChatMessage();
		myUIMediator.AddToLog(printMessage, CMainServerUIMediator::Normal);
	}
}

void CMainServerNetworkSystem::HandleConnectionMessage(CNetMessage* aMessage, CAddressWrapper & aSender)
{
	CNetMessageConnect* connectionMessageReceived = static_cast<CNetMessageConnect*>(aMessage);
	CNetMessageConnect::EConnectionState connectionState = connectionMessageReceived->GetConnectionState();

	if (connectionState == CNetMessageConnect::EConnectionState_TryingToConnect)
	{
		CNetMessageConnect* connectMessageAnswer;
		const CMainServerClients::SClientData* client = myClients.AddClient(aSender);

		if (client != nullptr)
		{
			connectMessageAnswer = myNetMessageManager.CreateMessage<CNetMessageConnect>(client->myID);
			connectMessageAnswer->SetConnectionState(CNetMessageConnect::EConnectionState_Ok);
		}
		else
		{
			connectMessageAnswer = myNetMessageManager.CreateMessage<CNetMessageConnect>();
			connectMessageAnswer->SetConnectionState(CNetMessageConnect::EConnectionState_Full);
		}

		myNetMessageManager.PackAndSendMessage(connectMessageAnswer, aSender);
	}
	else if (connectionState == CNetMessageConnect::EConnectionState_Disconnect)
	{
		const CMainServerClients::SClientData* client = myClients.GetClientByID(connectionMessageReceived->GetSenderID());
		if (client)
		{
			myUIMediator.AddToLog(client->myName + " disconnected from main server", CMainServerUIMediator::Error);
			myClients.RemoveClientByID(client->myID);
		}
	}
	else
	{
		myUIMediator.AddToLog("Client trying to connect to main server with wrong type of connection state", CMainServerUIMediator::EMessageColor::Warning);
	}
}

void CMainServerNetworkSystem::SaveClientName(CNetMessage* aMessage, CAddressWrapper&)
{
	CNetMessageClientName* clientName = static_cast<CNetMessageClientName*>(aMessage);
	unsigned short clientID = clientName->GetSenderID();

	myClients.GetClientByID(clientID)->myName = clientName->GetClientName();
	myUIMediator.AddToLog(clientName->GetClientName() + " connected to main server", CMainServerUIMediator::EMessageColor::Valid);
}

void CMainServerNetworkSystem::RecievePings(CNetMessage* aMessage, CAddressWrapper&)
{
	CNetMessagePing* ping = static_cast<CNetMessagePing*>(aMessage);

	CMainServerClients::SClientData* sender = myClients.GetClientByID(ping->GetSenderID());
	if (!sender)
	{
		myUIMediator.AddToLog("Recieved Ping from unkown ID: " + std::to_string(ping->GetSenderID()), CMainServerUIMediator::EMessageColor::Error);
		return;
	}

	if (ping->GetPingState() == CNetMessagePing::EPingState_Pong)
	{
		unsigned int rtt = static_cast<unsigned int>((myTimer.GetTotalTime() - ping->GetPingTimestamp()));
		sender->myLatestPingTimer = 0;
		sender->myRTT = rtt;
		sender->myPing = rtt / 2;
	}
	else if (ping->GetPingState() == CNetMessagePing::EPingState_Ping)
	{
	
		CNetMessagePing* pong = myNetMessageManager.CreateMessage<CNetMessagePing>(sender->myID);
		pong->SetPingState(CNetMessagePing::EPingState_Pong);
		pong->SetPingTimestamp(ping->GetTimeStamp());
		myNetMessageManager.PackAndSendMessage(pong, sender->myAddress);
	}
}

void CMainServerNetworkSystem::PlayerDeathReceived(CNetMessage * aMessage, CAddressWrapper &)
{
	CNetMessagePlayerDeath* message = static_cast<CNetMessagePlayerDeath*>(aMessage);
	const CommonUtilities::Vector3f deathLocation = message->GetPosition();
	int levelID = message->GetLevelID();
	myDeathLocations[levelID].push_back(deathLocation);

	CMainServerClients::SClientData* senderClient = myClients.GetClientByID(message->GetSenderID());
	if (senderClient)
	{
		std::string debugText = "[" + senderClient->myName + "] Died at Level:" + std::to_string(levelID + 1) + " ("+ std::to_string(deathLocation.x) + ", " + std::to_string(deathLocation.y) + ", " + std::to_string(deathLocation.z) + ")";
		myUIMediator.AddToLog(debugText, CMainServerUIMediator::Normal);
	}

	for (unsigned short clientIndex = 0; clientIndex < myClients.GetClientCount(); ++clientIndex)
	{
		CMainServerClients::SClientData* client = myClients.GetClientByIndex(clientIndex);
		if (client)
		{
			CNetMessagePlayerDeath* newMessage = myNetMessageManager.CreateMessage<CNetMessagePlayerDeath>();
			newMessage->SetPosition(deathLocation);
			newMessage->SetLevelID(levelID);
			myNetMessageManager.PackAndSendMessage(newMessage, client->myAddress);
		}
	}
}

void CMainServerNetworkSystem::SendPlayerDeathLocations(CNetMessage * aMessage, CAddressWrapper & aSender)
{
	CNetMessagePlayerDeathLocationList* message = static_cast<CNetMessagePlayerDeathLocationList*>(aMessage);
	int levelID = message->GetLevelID();

	CNetMessagePlayerDeathLocationList* newMessage = myNetMessageManager.CreateMessage<CNetMessagePlayerDeathLocationList>();
	newMessage->SetLevelID(levelID);
	int counter = 0;
	for (auto& location : myDeathLocations[levelID])
	{
		if (counter >= 15)
		{
			newMessage->myStream.size();
			myNetMessageManager.PackAndSendMessage(newMessage, aSender);
			newMessage = myNetMessageManager.CreateMessage<CNetMessagePlayerDeathLocationList>();
			newMessage->SetLevelID(levelID);
			counter = 0;
		}
		else
		{
			newMessage->AddDeathLocation(location);
			counter++;
		}
	}

	if (counter != 0)
	{
		myNetMessageManager.PackAndSendMessage(newMessage, aSender);
	}
}

void CMainServerNetworkSystem::SendLuaCommandMessage(const std::string& aText, unsigned short aClientID)
{
	CMainServerClients::SClientData* client = myClients.GetClientByID(aClientID);
	if (!client)
	{
		myUIMediator.AddToLog("Failed to Send Chat Message to Client ID: " + std::to_string(aClientID), CMainServerUIMediator::EMessageColor::Error);
		return;
	}

	CNetMessageLuaCommand* luaCommandMessage = myNetMessageManager.CreateMessage<CNetMessageLuaCommand>();
	luaCommandMessage->SetCommand(aText);
	myNetMessageManager.PackAndSendMessage(luaCommandMessage, client->myAddress);
}

void CMainServerNetworkSystem::KickAllClients()
{
	for (unsigned short i = myClients.GetClientCount(); i > 0; --i)
	{
		KickClientByIndex(i - 1);
	}
}

void CMainServerNetworkSystem::KickClientByIndex(unsigned short aIndex)
{
	CMainServerClients::SClientData* client = myClients.GetClientByIndex(aIndex);
	myUIMediator.AddToLog(client->myName + " kicked from main server", CMainServerUIMediator::Error);
	myClients.RemoveClientByIndex(aIndex);
}

void CMainServerNetworkSystem::PingClients()
{
	for (unsigned short i = 0; i < myClients.GetClientCount(); i++)
	{
		CMainServerClients::SClientData* client = myClients.GetClientByIndex(i);

		CNetMessagePing* ping;
		ping = myNetMessageManager.CreateMessage<CNetMessagePing>(client->myID);
		ping->SetPingState(CNetMessagePing::EPingState_Ping);
		myNetMessageManager.PackAndSendMessage(ping, client->myAddress);
	}
}

void CMainServerNetworkSystem::UpdatePings()
{
	myPingTimer += myTimer.GetRealDeltaTime();
	if (myPingTimer >= myPingFrequency)
	{
		PingClients();
		myPingTimer = 0;
	}

	UpdateClientsPingTimer();
}

void CMainServerNetworkSystem::UpdateClientsPingTimer()
{
	for (unsigned short i = myClients.GetClientCount(); i > 0; --i)
	{
		CMainServerClients::SClientData* client = myClients.GetClientByIndex(i - 1);
		client->myLatestPingTimer += myTimer.GetRealDeltaTime();

		if (client->myLatestPingTimer >= 10.f)
		{
#ifndef _DEBUG
			myUIMediator.AddToLog(client->myName + " timed out from main server", CMainServerUIMediator::EMessageColor::Error);
			myClients.RemoveClientByIndex(i - 1);
#endif
		}
	}
}

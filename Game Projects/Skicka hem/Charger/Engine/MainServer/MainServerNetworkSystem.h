#pragma once
#include "MainServerClients.h"
#include "NetMessageManager.h"
#include "MainServerUIMediator.h"
#include "Vector.h"

class CMainServerNetworkSystem
{
public:
	CMainServerNetworkSystem(CommonUtilities::Timer& aTimer, CMainServerUIMediator& aUIMediator);
	~CMainServerNetworkSystem();

	bool Init();
	void Shutdown();
	void Update();

	void GoOnline();
	void GoOffline();

	void SendLuaCommandToClient(const std::string& aCommand, unsigned short aClientID);
	void BroadcastLuaCommand(const std::string& aCommand);

public:
	void HandleChatMessage(CNetMessage* aMessage, CAddressWrapper&);
	void HandleConnectionMessage(CNetMessage* aMessage, CAddressWrapper& aSender);
	void SaveClientName(CNetMessage* aMessage, CAddressWrapper&);
	void RecievePings(CNetMessage* aMessage, CAddressWrapper&);
	void PlayerDeathReceived(CNetMessage* aMessage, CAddressWrapper&);
	void SendPlayerDeathLocations(CNetMessage* aMessage, CAddressWrapper& aSender);

	void PingClients();
	void UpdatePings();
	void UpdateClientsPingTimer();

	void SendLuaCommandMessage(const std::string& aText, unsigned short aClientID);

	void KickAllClients();
	void KickClientByIndex(unsigned short aIndex);

	CMainServerClients myClients;

	CNetMessageManager myNetMessageManager;
	char myBuffer[256];

	std::vector<std::vector<CommonUtilities::Vector3f>> myDeathLocations;

	float myPingFrequency = 1.f;
	float myPingTimer = 0.f;

	CommonUtilities::Timer& myTimer;
	CMainServerUIMediator& myUIMediator;

	float myUptime;
};


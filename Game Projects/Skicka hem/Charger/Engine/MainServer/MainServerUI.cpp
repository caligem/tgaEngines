#include "stdafx.h"
#include "MainServerUI.h"

#include "imgui.h"
#include "imgui_impl_dx11.h"

#include <iostream>
#include "MainServerNetworkSystem.h"
#include "windows.h"

CMainServerUI::CMainServerUI(CMainServerUIMediator& aUIMediator)
	: myUIMediator(aUIMediator)
{
	myShowServerStatus = true;
	myShowConsole = true;
	myShowClients = false;
	myClientCount = 0;
}


CMainServerUI::~CMainServerUI()
{
}

bool CMainServerUI::Init(HWND aHWND, ID3D11Device* aDevice, ID3D11DeviceContext* aContext, const CommonUtilities::Vector2i& aWindowSize)
{
	if (!ImGui_ImplDX11_Init(aHWND, aDevice, aContext))
	{
		printf("Failed to Init ImGui");
		return false;
	}

	myWindowSize = {static_cast<float>(aWindowSize.x), static_cast<float>(aWindowSize.y)};

	myInputBuffer.reserve(2048);
	myTempInputBuffer.reserve(2048);

	return true;
}

void CMainServerUI::Shutdown()
{
	ImGui_ImplDX11_Shutdown();
}

void CMainServerUI::Update()
{
	SetServerStatusText(myUIMediator.GetServerStatusText());
	myUIMediator.GetLogMessage(std::bind(&CMainServerUI::AddMessageToLog, this, std::placeholders::_1));
	myUIMediator.GetClientData(myClientCount, myClients);

	if (!myCurrentCommandToExecute.empty())
	{
		//cut command
		size_t found = myCurrentCommandToExecute.find("(");
		if (found != std::string::npos)
		{
			std::string command = myCurrentCommandToExecute.substr(0, found);
			if (command == "SendLuaCommandToClient")
			{
				//cut id
				unsigned short clientID = 0;

				if (found + 1 != std::string::npos)
				{
					clientID = static_cast<unsigned short>(std::stoi(&myCurrentCommandToExecute[found+1]));
				}

				//cut function
				std::string functionOffset = ", ";
				size_t found1 = myCurrentCommandToExecute.find(functionOffset);
				if (found1 != std::string::npos)
				{
					command = myCurrentCommandToExecute.substr(found1 + functionOffset.length()-1);
					//cut last parantheses
					size_t lastParentheses = myCurrentCommandToExecute.find_last_of(")");
					if (lastParentheses != std::string::npos)
					{
						command = myCurrentCommandToExecute.substr(found1 + functionOffset.length() - 1, lastParentheses-found1- 1);
						mySendLuaCommandCallback(command, clientID);
					}
				}
			}
			else if (command == "SendLuaCommandToAll")
			{
				size_t lastParentheses = myCurrentCommandToExecute.find_last_of(")");
				if (lastParentheses != std::string::npos)
				{
					command = myCurrentCommandToExecute.substr(command.length()+1, lastParentheses - command.length() - 1);
					myBroadcastLuaCommandCallback(command);
				}
			}
			myCurrentCommandToExecute = "";
		}
	}
}

void CMainServerUI::Render()
{
	ImGui_ImplDX11_NewFrame(myWindowSize.x, myWindowSize.y);

	//ImGui::ShowTestWindow();
	//ImGui::ShowStyleEditor();
	if (myShowServerStatus) ShowServerStatus(&myShowServerStatus);
	if (myShowClients) ShowClients(&myShowClients);
	if (myShowConsole) ShowConsole(&myShowConsole);

	ImGui::Render();
}

inline void CMainServerUI::SetServerStatusText(const CMainServerUIMediator::SMessage & aMessage)
{
	myServerStatusText = aMessage.message;
	myServerStatusColor = GetColor(aMessage.color);
}

void CMainServerUI::ShowServerStatus(bool* aShow)
{
	ImGui::SetNextWindowPos(ImVec2(0.f, 00.f), ImGuiCond_Always, ImVec2(0.0f, 0.0f));
	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.0f, 0.0f, 0.0f, 0.3f)); // Transparent background
	ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.2f, 0.2f, 0.2f, 255.f));
	ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.3f, 0.3f, 0.3f, 255.f));
	ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.4f, 0.4f, 0.4f, 255.f));
	ImGui::SetNextWindowSize(ImVec2(1024.f, 22.f));
	if (ImGui::Begin("Server Status", aShow, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_AlwaysAutoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings))
	{
		float baseLine = 5.0f;
		ImGui::SetCursorScreenPos(ImVec2(0.f, 0.f));
		
		if (ImGui::Button("Clients", ImVec2(64.f, 22.f)))
		{
			CloseWindows();
			myShowClients = true;
		}

		ImGui::SetCursorScreenPos(ImVec2(64.f + 6.f, 0.f));
		if (ImGui::Button("Console", ImVec2(64.f, 22.f)))
		{
			CloseWindows();
			myShowConsole = true;
		}

		float rightWall = 1024.f;
		float wallSpacing = 16.f;
		float spacing = + 8.f;

		rightWall -= ImGui::CalcTextSize(myServerStatusText.c_str()).x + wallSpacing;
		ImGui::SetCursorScreenPos(ImVec2(rightWall, baseLine));
		ImVec4 color(myServerStatusColor.x, myServerStatusColor.y, myServerStatusColor.z, myServerStatusColor.w);
		ImGui::TextColored(color, myServerStatusText.c_str()); ImGui::SameLine();

		rightWall -= ImGui::CalcTextSize("|").x + spacing;
		ImGui::SetCursorScreenPos(ImVec2(rightWall, baseLine));
		ImGui::Text("|");

		std::string connectedClients = "Connected Clients: " + std::to_string(myClientCount);
		rightWall -= ImGui::CalcTextSize(connectedClients.c_str()).x + spacing;
		ImGui::SetCursorScreenPos(ImVec2(rightWall, baseLine));
		ImGui::Text(connectedClients.c_str());

		rightWall -= ImGui::CalcTextSize("|").x + spacing;
		ImGui::SetCursorScreenPos(ImVec2(rightWall, baseLine));
		ImGui::Text("|");

		unsigned int uptimeSeconds = myUIMediator.GetUptime();
		unsigned int uptimeMinutes = uptimeSeconds / 60;
		unsigned int uptimeHours = uptimeMinutes / 60;
		uptimeSeconds = uptimeSeconds % 60;
		uptimeMinutes = uptimeMinutes % 60;

		std::string uptime = "Uptime: " + 
			std::string(uptimeHours<10?"0":"") + std::to_string(uptimeHours) + ":" + 
			std::string(uptimeMinutes<10 ? "0" : "") + std::to_string(uptimeMinutes) + ":" +
			std::string(uptimeSeconds<10 ? "0" : "") + std::to_string(uptimeSeconds);
		
		rightWall -= ImGui::CalcTextSize(uptime.c_str()).x + spacing;
		ImGui::SetCursorScreenPos(ImVec2(rightWall, baseLine));
		ImGui::Text(uptime.c_str());

		ImGui::End();
	}
	ImGui::PopStyleColor(4);
}

void CMainServerUI::ShowClients(bool * aShow)
{
	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.0f, 0.0f, 0.0f, 0.3f));
	ImGui::PushStyleColor(ImGuiCol_ScrollbarBg, ImVec4(0.03f, 0.03f, 0.03f, 255.f));
	ImGui::PushStyleColor(ImGuiCol_ScrollbarGrab, ImVec4(0.47f, 0.47f, 0.47f, 255.f));
	ImGui::PushStyleColor(ImGuiCol_ScrollbarGrabHovered, ImVec4(0.7f, 0.7f, 0.7f, 255.f));
	ImGui::PushStyleColor(ImGuiCol_ScrollbarGrabActive, ImVec4(0.3f, 0.3f, 0.3f, 255.f));
	
	ImGui::SetNextWindowPos(ImVec2(0.f, 22.f), ImGuiCond_Always, ImVec2(0.0f, 0.0f));
	ImGui::SetNextWindowSize(ImVec2(1024.f, 750.f));
	if (ImGui::Begin("ClientsData", aShow, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings))
	{
		ImGui::GetStyle().WindowRounding = 4.f;
		ImGui::BeginChild(1, ImVec2(1024.f, 20.f));
		ImGui::Text("Clients");
		ImGui::EndChild();


		float columnOffset = 50.f;
		ImGui::BeginChild(2, ImVec2(1024.f, 22.f));
		ImGui::Columns(6, "titleColumns");
		ImGui::SetColumnOffset(1, columnOffset); columnOffset += 300.f;
		ImGui::SetColumnOffset(2, columnOffset); columnOffset += 120.f;
		ImGui::SetColumnOffset(3, columnOffset); columnOffset += 70.f;
		ImGui::SetColumnOffset(4, columnOffset); columnOffset += 80.f;
		ImGui::SetColumnOffset(5, columnOffset); columnOffset += 80.f;
		ImGui::Separator();
		ImGui::Text("ID"); ImGui::NextColumn();
		ImGui::Text("Name"); ImGui::NextColumn();
		ImGui::Text("IP"); ImGui::NextColumn();
		ImGui::Text("Port"); ImGui::NextColumn();
		ImGui::Text("Ping"); ImGui::NextColumn();
		ImGui::Text("RTT"); ImGui::NextColumn();
		ImGui::Separator();
		ImGui::EndChild();

		columnOffset = 50.f;
		ImGui::BeginChild(3, ImVec2(1024.f - 12.f, 680.f));
		ImGui::GetStyle().ScrollbarRounding = 2.f;
		ImGui::GetStyle().ScrollbarSize = 17.f;
		ImGui::Columns(6, "dataColumns", false);
		ImGui::SetColumnOffset(1, columnOffset); columnOffset += 300.f;
		ImGui::SetColumnOffset(2, columnOffset); columnOffset += 120.f;
		ImGui::SetColumnOffset(3, columnOffset); columnOffset += 70.f;
		ImGui::SetColumnOffset(4, columnOffset); columnOffset += 80.f;
		ImGui::SetColumnOffset(5, columnOffset); columnOffset += 80.f;
		ImGui::SetColumnOffset(6, columnOffset);
		for (size_t i = 0; i < myClients.size(); ++i)
		{
			myClients[i].myAddress.GetAddress(myAddressBuffer);
			ImGui::Text(std::to_string(myClients[i].myID).c_str()); ImGui::NextColumn();
			ImGui::Text(myClients[i].myName.c_str()); ImGui::NextColumn();
			ImGui::Text(myAddressBuffer); ImGui::NextColumn();
			ImGui::Text(std::to_string(myClients[i].myAddress.GetPort()).c_str()); ImGui::NextColumn();
			ImGui::Text(std::to_string(static_cast<int>(myClients[i].myPing)).c_str()); ImGui::NextColumn();
			ImGui::Text(std::to_string(static_cast<int>(myClients[i].myRTT)).c_str()); ImGui::NextColumn();
			ZeroMemory(myAddressBuffer, ADDR_LEN_STR);
		}
		ImGui::Columns(1);
		ImGui::EndChild();
		ImGui::End();
	}
	ImGui::PopStyleColor(5);
}

void CMainServerUI::ShowConsole(bool * aShow)
{
	ImGui::PushStyleColor(ImGuiCol_WindowBg, ImVec4(0.0f, 0.0f, 0.0f, 0.3f));
	ImGui::PushStyleColor(ImGuiCol_ScrollbarBg, ImVec4(0.03f, 0.03f, 0.03f, 255.f));
	ImGui::PushStyleColor(ImGuiCol_ScrollbarGrab, ImVec4(0.47f, 0.47f, 0.47f, 255.f));
	ImGui::PushStyleColor(ImGuiCol_ScrollbarGrabHovered, ImVec4(0.7f, 0.7f, 0.7f, 255.f));
	ImGui::PushStyleColor(ImGuiCol_ScrollbarGrabActive, ImVec4(0.3f, 0.3f, 0.3f, 255.f));

	ImGui::SetNextWindowPos(ImVec2(0.f, 22.f), ImGuiCond_Always, ImVec2(0.0f, 0.0f));
	ImGui::SetNextWindowSize(ImVec2(1024.f, 768.f - 22.f), ImGuiCond_Once);

	if (ImGui::Begin("Console", aShow, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoSavedSettings))
	{
		ImGui::BeginChild("ScrollingRegion", ImVec2(0, -ImGui::GetItemsLineHeightWithSpacing()), false, ImGuiWindowFlags_HorizontalScrollbar);

		ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(4, 1));

		for (const CMainServerUIMediator::SMessage& message : myLogMessages)
		{
			CommonUtilities::Vector4f color = GetColor(message.color);
			ImGui::PushStyleColor(ImGuiCol_Text, ImVec4(color.x, color.y, color.z, color.w));
			ImGui::TextUnformatted(message.message.c_str());
			ImGui::PopStyleColor();
		}

		if (myShouldScroll)
		{
			ImGui::SetScrollHere();
			myShouldScroll = false;
		}

		ImGui::PopStyleVar();
		ImGui::EndChild();
		ImGui::Separator();

		// Command-line

		myTempInputBuffer = myInputBuffer;
		bool pressedEnter = ImGui::InputText(
			"Input",
			&myTempInputBuffer[0],
			myTempInputBuffer.capacity(),
			ImGuiInputTextFlags_EnterReturnsTrue
		);

		myInputBuffer = myTempInputBuffer.c_str();

		if (pressedEnter)
		{
			pressedEnter = false;
			if (myInputBuffer.size() > 0)
			{
				ExecuteCommand();
			}
			else
			{
				//myCommandLogActivated = true;
			}

			myInputBuffer = "";
		}

		// Demonstrate keeping auto focus on the input box
		if (ImGui::IsItemHovered() || (ImGui::IsRootWindowOrAnyChildFocused() && !ImGui::IsAnyItemActive() && !ImGui::IsMouseClicked(0)))
			ImGui::SetKeyboardFocusHere(-1); // Auto focus previous widget

		ImGui::End();
	}

	ImGui::PopStyleColor(5);
}

void CMainServerUI::CloseWindows()
{
	myShowClients = false;
	myShowConsole = false;
}

CommonUtilities::Vector4f CMainServerUI::GetColor(CMainServerUIMediator::EMessageColor aColor)
{
	CommonUtilities::Vector4f col;

	switch (aColor)
	{
	case CMainServerUIMediator::EMessageColor::Error:
		col = CommonUtilities::Vector4f(1.0f, 0.0f, 0.0f, 1.0f);
		break;

	case CMainServerUIMediator::EMessageColor::Normal:
		col = CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f);
		break;

	case CMainServerUIMediator::EMessageColor::Valid:
		col = CommonUtilities::Vector4f(0.0f, 1.0f, 0.0f, 1.0f);
		break;

	case CMainServerUIMediator::EMessageColor::Warning:
		col = CommonUtilities::Vector4f(1.0f, 1.0f, 0.0f, 1.0f);
		break;

	default:
		col = CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f);
		break;
	}

	return col;
}

void CMainServerUI::ExecuteCommand()
{
	AddToLog(myInputBuffer, CMainServerUIMediator::EMessageColor::Normal);

	myCurrentCommandToExecute = myInputBuffer;
}

void CMainServerUI::SetSendLuaCommandCallback(std::function<void(const std::string&, unsigned short) > aFunction)
{
	mySendLuaCommandCallback = aFunction;
}

void CMainServerUI::SetBroadcastLuaCommandCallback(std::function<void(const std::string&)> aFunction)
{
	myBroadcastLuaCommandCallback = aFunction;
}

void CMainServerUI::AddToLog(const std::string & aInput, CMainServerUIMediator::EMessageColor aColor)
{
	myLogMessages.push_back(CMainServerUIMediator::SMessage({ aColor, aInput }));
	myShouldScroll = true;
}

void CMainServerUI::AddMessageToLog(const CMainServerUIMediator::SMessage& aMessage)
{
	myLogMessages.push_back(aMessage);
	myShouldScroll = true;
}
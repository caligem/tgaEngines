#pragma once
#include "MainServerClients.h"

#include <string>
#include <vector>
#include <functional>

class CMainServerUIMediator
{
public:
	enum EMessageColor
	{
		Normal,
		Valid,
		Error,
		Warning
	};

	struct SMessage
	{
		EMessageColor color;
		std::string message;
	};

	CMainServerUIMediator();
	~CMainServerUIMediator();

	void SetServerStatusText(const std::string& aStatusText, EMessageColor aColor) { myServerStatusText = SMessage({ aColor, aStatusText }); }
	const SMessage& GetServerStatusText() { return myServerStatusText; }

	void UpdateClientData(unsigned short aClientCount, const std::array<CMainServerClients::SClientData, 512>& aClientData);
	void GetClientData(unsigned short& aClientCount, std::array<CMainServerClients::SClientData, 512>& aClientData);

	void AddToLog(const std::string& aString, EMessageColor aColor);
	void GetLogMessage(std::function<void(const SMessage&)> aCallback);
	
	void SetUptime(unsigned int aUptime) { myUptime = aUptime; }
	unsigned int GetUptime() { return myUptime; }

private:
	std::vector<SMessage> myLogMessageBuffer;
	std::array<CMainServerClients::SClientData, 512> myClientData;

	SMessage myServerStatusText;

	unsigned short myClientCount;
	unsigned int myUptime;
};


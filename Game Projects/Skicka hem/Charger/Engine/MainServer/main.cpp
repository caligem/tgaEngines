#include "stdafx.h"

#include <iostream>
#include "windows.h"
#include "MainServer.h"

INT WINAPI wWinMain(HINSTANCE, HINSTANCE, LPWSTR, int)
{
	
	CMainServer mainServer;
	bool result = mainServer.Init();

	if (!result)
	{
		printf("Failed to start Main Server");
	}

	MSG windowMessage = { 0 };
	bool running = result;

	while (running)
	{
		while (PeekMessage(&windowMessage, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&windowMessage);
			DispatchMessage(&windowMessage);

			if (windowMessage.message == WM_QUIT)
			{
				running = false;
			}
		}
		mainServer.Update();
		mainServer.Render();
	}

	mainServer.Shutdown();
	return 0;
}
#pragma once
#include <functional>
#include "AddressWrapper.h"
#include "SerializeHelper.h"
#define SERIALIZE(aStream, aType) serialize(aType, aStream)
#define DESERIALIZE(aStream, aType) aType = deserialize<decltype(aType)>(aStream);

#define DECLARE_NETMESSAGE_CLASS_INHERITANCE(a, b) class CNetMessageManager; \
class a  : public b {  \
protected: \
	friend CNetMessageManager; \
    virtual inline const char * class_id() override { return #a; } \
private:

class CNetMessageManager;

class CNetMessage
{
public:
	CNetMessage();
	virtual ~CNetMessage();

	inline const unsigned short GetSenderID() const { return mySenderID; }
	inline const unsigned short GetTargetID() const { return myTargetID; }
	inline const unsigned int GetTimeStamp() const { return myTimestamp; }

	StreamType myStream;

protected:
	virtual inline const char * class_id() = 0;
	
	virtual void DoSerialize(StreamType&) {}
	virtual void DoDeSerialize(StreamType&) {}
	
	std::function<void(CNetMessage*, CAddressWrapper&)> myDoUnPackWorkCallback;

	unsigned int myID;
	unsigned int myTimestamp;
	unsigned short mySenderID;
	unsigned short myTargetID;

private:
	friend CNetMessageManager;
	inline virtual void ClearMessage() { myStream.clear(); }
	inline void SetUnPackWorkCallback(std::function<void(CNetMessage*, CAddressWrapper&)> aCallback) { myDoUnPackWorkCallback = aCallback; }
	void SetGeneralData(unsigned int aMessageID, unsigned int aTimeStamp, unsigned short aSenderID, unsigned short aTargetID);
	void PackMessage();
	void UnPackMessage(char* aMessage, int aSize);
	inline void DoUnPackWork(CNetMessage* aMessage, CAddressWrapper& aSenderAddress) { if(myDoUnPackWorkCallback) myDoUnPackWorkCallback(aMessage, aSenderAddress); }
};
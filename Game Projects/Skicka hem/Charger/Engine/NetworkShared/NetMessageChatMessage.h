#pragma once
#include "NetMessage.h"


DECLARE_NETMESSAGE_CLASS_INHERITANCE(CNetMessageChatMessage, CNetMessage)
public:
	CNetMessageChatMessage();
	~CNetMessageChatMessage();

	inline void SetChatMessage(const std::string& aMessage) { myMessage = aMessage; }
	inline const std::string& GetChatMessage() { return myMessage; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;
	std::string myMessage;
};

#include "stdafx.h"
#include "NetMessageConnect.h"

CNetMessageConnect::CNetMessageConnect()
{
}


CNetMessageConnect::~CNetMessageConnect()
{
}

void CNetMessageConnect::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myState);
}

void CNetMessageConnect::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myState);
}

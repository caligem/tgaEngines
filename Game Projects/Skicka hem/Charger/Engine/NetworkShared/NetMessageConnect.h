#pragma once
#include "NetMessage.h"

DECLARE_NETMESSAGE_CLASS_INHERITANCE(CNetMessageConnect, CNetMessage)
public:
	enum EConnectionState
	{
		EConnectionState_Ok,
		EConnectionState_Full,
		EConnectionState_TryingToConnect,
		EConnectionState_Disconnect
	};

	CNetMessageConnect();
	~CNetMessageConnect();

	inline void SetConnectionState(EConnectionState aState) { myState = aState; }
	inline EConnectionState GetConnectionState() { return myState; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;
	EConnectionState myState;
};


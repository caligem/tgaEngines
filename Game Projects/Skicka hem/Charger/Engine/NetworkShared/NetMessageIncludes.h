#pragma once

#include "NetMessageChatMessage.h"
#include "NetMessageClientName.h"
#include "NetMessageConnect.h"
#include "NetMessagePing.h"
#include "NetMessageLuaCommand.h"
#include "NetMessagePlayerDeath.h"
#include "NetMessagePlayerDeathLocationList.h"
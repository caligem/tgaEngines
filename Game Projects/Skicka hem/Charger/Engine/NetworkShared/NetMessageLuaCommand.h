#pragma once
#include "NetMessage.h"

DECLARE_NETMESSAGE_CLASS_INHERITANCE(CNetMessageLuaCommand, CNetMessage)
public:
	CNetMessageLuaCommand();
	~CNetMessageLuaCommand();

	inline void SetCommand(const std::string& aMessage) { myCommand = aMessage; }
	inline const std::string& GetCommand() { return myCommand; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;
	std::string myCommand;
};


#include "stdafx.h"
#include "NetMessageManager.h"
#include "NetMessageIncludes.h"
#include "CommonNetworkIncludes.h"
#include "Encryption.h"

CNetMessageManager::CNetMessageManager(CommonUtilities::Timer& aTimer)
	: myTimer(aTimer)
{
}


CNetMessageManager::~CNetMessageManager()
{
}

bool CNetMessageManager::Init(char aThreadCount)
{
	WSADATA wsaData;

	if (WSAStartup(MAKEWORD(2, 1), &wsaData))
	{
		return false;
	}

	char safeThreadCount = aThreadCount;
	if (aThreadCount < 1)
	{
		safeThreadCount = 1;
	}
	
	for (char i = 0; i < safeThreadCount; ++i)
	{
		myOutMessageLUT.emplace_back();
	}
	
	RegisterMessages();

	myKey.push_back(1);
	myKey.push_back(3);
	myKey.push_back(3);
	myKey.push_back(7);

	return true;
}

void CNetMessageManager::PackAndSendMessage(CNetMessage* aMessage, const CAddressWrapper& aDestination)
{
	myEncryptedMessage.clear();
	aMessage->PackMessage();
	Aes256::encrypt(myKey, (unsigned char*)&aMessage->myStream[0], aMessage->myStream.size(), myEncryptedMessage);
	mySocket.Send(aDestination, (char*)&myEncryptedMessage[0], myEncryptedMessage.size());
}

void CNetMessageManager::RecieveMessage()
{
	CAddressWrapper senderAddress;
	int receivedBytes = mySocket.Receive(senderAddress, myBuffer, sizeof(myBuffer));
	if (receivedBytes > 16)
	{
		myDecryptedMessage.clear();
		size_t bytesDecrytped = Aes256::decrypt(myKey, (unsigned char*)myBuffer, receivedBytes, myDecryptedMessage);
		HandleMessage((char*)&myDecryptedMessage[0], static_cast<int>(bytesDecrytped), senderAddress);
		ZeroMemory(myBuffer, sizeof(myBuffer));
	}
}

bool CNetMessageManager::CloseSocket()
{
	if (!mySocket.IsOpen())
	{
		return false;
	}

	mySocket.Close();
	return true;
}


bool CNetMessageManager::OpenSocket(unsigned short aPort, const char* aAddress)
{
	if (mySocket.IsOpen())
	{
		return false;
	}

	return mySocket.Open(aPort, aAddress);
}

bool CNetMessageManager::IsSocketOpen()
{
	return mySocket.IsOpen();
}

void CNetMessageManager::HandleMessage(char * aBuffer, int aBufferSize, CAddressWrapper & aSender)
{
	if (aBufferSize < 4)
	{
		//TODO: Handle Error
		return;
	}

	unsigned int hashedMessageID = 0;
	myIDBuffer.resize(sizeof(unsigned int));
	memcpy_s(&myIDBuffer[0], sizeof(unsigned int), aBuffer, sizeof(unsigned int));
	DESERIALIZE(myIDBuffer, hashedMessageID);

	CNetMessage* message = myInMessageLUT[hashedMessageID];
	if (message)
	{
		message->UnPackMessage(aBuffer, aBufferSize);
		message->DoUnPackWork(message, aSender);
	}
	message->ClearMessage();
}

uint32_t CNetMessageManager::HashMessageID(const uint8_t* key, size_t length)
{
	size_t i = 0;
	uint32_t hash = 0;
	while (i != length) {
		hash += key[i++];
		hash += hash << 10;
		hash ^= hash >> 6;
	}
	hash += hash << 3;
	hash ^= hash >> 11;
	hash += hash << 15;
	return hash;
}

void CNetMessageManager::RegisterMessages()
{
	RegisterMessage<CNetMessageChatMessage>();
	RegisterMessage<CNetMessageClientName>();
	RegisterMessage<CNetMessageConnect>();
	RegisterMessage<CNetMessagePing>();
	RegisterMessage<CNetMessageLuaCommand>();
	RegisterMessage<CNetMessagePlayerDeath>();
	RegisterMessage<CNetMessagePlayerDeathLocationList>();
}

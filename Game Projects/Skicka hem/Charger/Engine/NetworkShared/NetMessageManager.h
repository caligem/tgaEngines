#pragma once
#include "NetMessage.h"
#include "Timer.h"
#include <unordered_map>
#include "AddressWrapper.h"
#include "SocketWrapper.h"
#include <functional>

class CNetMessageManager
{
public:
	CNetMessageManager(CommonUtilities::Timer& aTimer);
	~CNetMessageManager();

	bool Init(char aThreadCount = 1);

	template<typename MessageType>
	void RegisterDoUnPackWorkCallback(std::function<void(CNetMessage*, CAddressWrapper&)> aFunction);

	template<typename MessageType>
	MessageType* CreateMessage(unsigned short aTargetID = 0);
	
	void PackAndSendMessage(CNetMessage* aMessage, const CAddressWrapper& aDestination);

	void SetSenderID(unsigned short aSenderID) { mySenderID = aSenderID; }

	void RecieveMessage();

	bool CloseSocket();
	bool OpenSocket(unsigned short aPort, const char* aAddress = nullptr);
	bool IsSocketOpen();

private:
	template<typename MessageType>
	void RegisterMessage();

	void HandleMessage(char* aBuffer, int aBufferSize, CAddressWrapper& aSender);

	uint32_t HashMessageID(const uint8_t* key, size_t length);
	void RegisterMessages();
	CSocketWrapper mySocket;
	char myBuffer[256];

	CommonUtilities::Timer& myTimer;
	unsigned short mySenderID;
	std::vector<unsigned char> myKey;
	std::vector<unsigned char> myEncryptedMessage;
	std::vector<unsigned char> myDecryptedMessage;
	std::vector<std::unordered_map<unsigned int, CNetMessage*>> myOutMessageLUT;
	std::unordered_map<unsigned int, CNetMessage*> myInMessageLUT;
	std::vector<char> myIDBuffer;
};

template<typename MessageType>
inline void CNetMessageManager::RegisterDoUnPackWorkCallback(std::function<void(CNetMessage*, CAddressWrapper&)> aFunction)
{
	MessageType temp;
	std::string messageID = temp.class_id();
	unsigned int hashedMessageID = HashMessageID((uint8_t*)messageID.c_str(), messageID.size());

	MessageType* message = static_cast<MessageType*>(myInMessageLUT[hashedMessageID]);
	message->SetUnPackWorkCallback(aFunction);
}

template<typename MessageType>
inline MessageType* CNetMessageManager::CreateMessage(unsigned short aTargetID)
{
	static int ourSharedMessageLUTIndex = 0;
	thread_local int ourMessageLUTIndex = ourSharedMessageLUTIndex++;

	MessageType temp;
	std::string messageID = temp.class_id();
	unsigned int hashedMessageID = HashMessageID((uint8_t*)messageID.c_str(), messageID.size());

	CNetMessage* message = myOutMessageLUT[ourMessageLUTIndex][hashedMessageID];
	message->ClearMessage();

	unsigned int timestamp = static_cast<unsigned int>(myTimer.GetTotalTime());
	message->SetGeneralData(hashedMessageID, timestamp, mySenderID, aTargetID);

	return static_cast<MessageType*>(message);
}

template<typename MessageType>
inline void CNetMessageManager::RegisterMessage()
{
	for (auto& lut : myOutMessageLUT)
	{
		MessageType* message = new MessageType();
		std::string messageID = message->class_id();
		unsigned int hashedMessageID = HashMessageID((uint8_t*)messageID.c_str(), messageID.size());

		lut[hashedMessageID] = message;
	}

	MessageType* message = new MessageType();
	std::string messageID = message->class_id();
	unsigned int hashedMessageID = HashMessageID((uint8_t*)messageID.c_str(), messageID.size());

	myInMessageLUT[hashedMessageID] = message;
}

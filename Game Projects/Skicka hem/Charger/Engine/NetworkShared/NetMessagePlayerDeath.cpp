#include "stdafx.h"
#include "NetMessagePlayerDeath.h"


CNetMessagePlayerDeath::CNetMessagePlayerDeath()
{
}


CNetMessagePlayerDeath::~CNetMessagePlayerDeath()
{
}

void CNetMessagePlayerDeath::DoSerialize(StreamType& aStreamType)
{
	SERIALIZE(aStreamType, myLevelID);
	SERIALIZE(aStreamType, myDeathPosition);
}

void CNetMessagePlayerDeath::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myLevelID);
	DESERIALIZE(aStreamType, myDeathPosition);
}

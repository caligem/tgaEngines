#pragma once
#include "NetMessage.h"
#include "Vector.h"

DECLARE_NETMESSAGE_CLASS_INHERITANCE(CNetMessagePlayerDeath, CNetMessage)
public:
	CNetMessagePlayerDeath();
	~CNetMessagePlayerDeath();

	inline void SetLevelID(int aLevelID) { myLevelID = aLevelID; }
	inline int GetLevelID() const { return myLevelID; }

	inline void SetPosition(const CommonUtilities::Vector3f aDeathPosition) { myDeathPosition = aDeathPosition; }
	inline CommonUtilities::Vector3f GetPosition() const { return myDeathPosition; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	int myLevelID;
	CommonUtilities::Vector3f myDeathPosition;
};
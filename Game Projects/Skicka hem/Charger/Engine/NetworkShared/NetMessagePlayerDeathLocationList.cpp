#include "stdafx.h"
#include "NetMessagePlayerDeathLocationList.h"


CNetMessagePlayerDeathLocationList::CNetMessagePlayerDeathLocationList()
{
}


CNetMessagePlayerDeathLocationList::~CNetMessagePlayerDeathLocationList()
{
}

void CNetMessagePlayerDeathLocationList::DoSerialize(StreamType& aStreamType)
{
	SERIALIZE(aStreamType, myLevelID);
	myDeathLocationCount = myDeathLocations.size();
	SERIALIZE(aStreamType, myDeathLocationCount);

	for (int i = 0; i < myDeathLocationCount; ++i)
	{
		const CommonUtilities::Vector3f location = myDeathLocations[i];
		SERIALIZE(aStreamType, location);
	}
}

void CNetMessagePlayerDeathLocationList::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myLevelID);
	DESERIALIZE(aStreamType, myDeathLocationCount);

	for (size_t i = 0; i < myDeathLocationCount; ++i)
	{
		CommonUtilities::Vector3f location;
		DESERIALIZE(aStreamType, location);
		myDeathLocations.push_back(location);
	}
}

void CNetMessagePlayerDeathLocationList::ClearMessage()
{
	myLevelID = -1;
	myDeathLocationCount = 0;
	myDeathLocations.clear();
	myStream.clear();
}

#pragma once
#include "NetMessage.h"
#include "Vector.h"
#include <vector.h>

DECLARE_NETMESSAGE_CLASS_INHERITANCE(CNetMessagePlayerDeathLocationList, CNetMessage)
public:
	CNetMessagePlayerDeathLocationList();
	~CNetMessagePlayerDeathLocationList();
	
	void SetLevelID(int aLevelID) { myLevelID = aLevelID; }
	int GetLevelID() const { return myLevelID; }

	const std::vector<CommonUtilities::Vector3f>& GetDeathLocations() const { return myDeathLocations; }
	void AddDeathLocation(const CommonUtilities::Vector3f& aDeathLocation) { myDeathLocations.push_back(aDeathLocation); }

private:
	void DoSerialize(StreamType& aStreamType) override;
	void DoDeSerialize(StreamType& aStreamType) override;
	void ClearMessage() override;

	int myLevelID;
	size_t myDeathLocationCount;
	std::vector<CommonUtilities::Vector3f> myDeathLocations;
};


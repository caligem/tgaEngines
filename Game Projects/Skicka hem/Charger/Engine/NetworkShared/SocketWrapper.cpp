#include "stdafx.h"
#include "SocketWrapper.h"

#include "CommonNetworkIncludes.h"

CSocketWrapper::CSocketWrapper()
	: myIsOpen(false)
{
}


CSocketWrapper::~CSocketWrapper()
{
}

bool CSocketWrapper::Open(unsigned short aPort, const char* aAddress)
{
	mySocket = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	sockaddr_in addr;
	addr.sin_family = AF_INET;

	if (!aAddress)
	{
		addr.sin_addr.s_addr = INADDR_ANY;
	}
	else
	{
		int IP = 0;
		inet_pton(AF_INET, aAddress, &IP);
		addr.sin_addr.s_addr = IP;
	}

	addr.sin_port = htons(aPort);

	if ( bind(mySocket, (const sockaddr*)&addr, sizeof(sockaddr_in)) < 0)
	{
		printf("Failed to bind socket with error: %d \n", WSAGetLastError());
		return false;
	};

	DWORD nonBlocking = 1;
	if (ioctlsocket(mySocket, FIONBIO, &nonBlocking) != 0)
	{
		printf("Failed to set socket to non-blocking\n");
		return false;
	}

	if (mySocket == SOCKET_ERROR)
	{
		printf("Failed to create socket with error: %d \n", WSAGetLastError());
		return false;
	}

	myIsOpen = true;

	return true;
}

void CSocketWrapper::Close()
{
	closesocket(mySocket);
}

bool CSocketWrapper::IsOpen() const
{
	return myIsOpen;
}

bool CSocketWrapper::Send(const CAddressWrapper & aDestination, const char * aData, int aSize)
{
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(aDestination.GetAddress());
	addr.sin_port = htons(aDestination.GetPort());

	sendto(mySocket, aData, aSize, 0, (sockaddr*)&addr, sizeof(addr));
	return false;
}

bool CSocketWrapper::Send(const CAddressWrapper & aDestination, const char * aData, size_t aSize)
{
	sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(aDestination.GetAddress());
	addr.sin_port = htons(aDestination.GetPort());

	sendto(mySocket, aData, static_cast<int>(aSize), 0, (sockaddr*)&addr, sizeof(addr));
	return true;
}

int CSocketWrapper::Receive(CAddressWrapper & aSender, char * aData, int aSize)
{
	sockaddr_in addr;
	socklen_t addrLength = sizeof(addr);
	
	int bytes = recvfrom(mySocket, aData, aSize, 0, (sockaddr*)&addr, &addrLength);

	aSender.Init(ntohl(addr.sin_addr.s_addr), ntohs(addr.sin_port));
	return bytes;
}
#pragma once
#include "AddressWrapper.h"

typedef unsigned long long SOCKET;

class CSocketWrapper
{
public:
	CSocketWrapper();
	~CSocketWrapper();

	bool Open(unsigned short aPort, const char* aAddress = nullptr);

	void Close();

	bool IsOpen() const;

	bool Send(const CAddressWrapper& aDestination, const char* aData, int aSize);
	bool Send(const CAddressWrapper& aDestination, const char* aData, size_t aSize);

	int Receive(CAddressWrapper& aSender, char* aData, int aSize);


private:
	SOCKET mySocket;
	bool myIsOpen;
};


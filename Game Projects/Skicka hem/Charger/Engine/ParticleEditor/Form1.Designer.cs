﻿namespace ParticleEditor
{
    partial class ParticleEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			this.components = new System.ComponentModel.Container();
			this.label_Lifetime = new System.Windows.Forms.Label();
			this.label_SpawnRate = new System.Windows.Forms.Label();
			this.label_Loopable = new System.Windows.Forms.Label();
			this.label_Acceleration = new System.Windows.Forms.Label();
			this.label_StartColor = new System.Windows.Forms.Label();
			this.label_EndColor = new System.Windows.Forms.Label();
			this.label_StartSize = new System.Windows.Forms.Label();
			this.label_EndSize = new System.Windows.Forms.Label();
			this.checkBox_Loopable = new System.Windows.Forms.CheckBox();
			this.groupBox_Skybox = new System.Windows.Forms.GroupBox();
			this.label_SkyboxDirectory = new System.Windows.Forms.Label();
			this.comboBox_SkyBox = new System.Windows.Forms.ComboBox();
			this.groupBox_ParticleProperties = new System.Windows.Forms.GroupBox();
			this.comboBox_BlendState = new System.Windows.Forms.ComboBox();
			this.label_BlendState = new System.Windows.Forms.Label();
			this.nud_GravityModifier = new System.Windows.Forms.NumericUpDown();
			this.label_GravityModifier = new System.Windows.Forms.Label();
			this.nud_RotationVelocity2 = new System.Windows.Forms.NumericUpDown();
			this.nud_RotationVelocity1 = new System.Windows.Forms.NumericUpDown();
			this.nud_StartRotation2 = new System.Windows.Forms.NumericUpDown();
			this.nud_StartRotation1 = new System.Windows.Forms.NumericUpDown();
			this.label_RotationVelocity = new System.Windows.Forms.Label();
			this.label_StartRotation = new System.Windows.Forms.Label();
			this.groupBox_SphereData = new System.Windows.Forms.GroupBox();
			this.groupBox_BoxData = new System.Windows.Forms.GroupBox();
			this.nud_BoxThickness = new System.Windows.Forms.NumericUpDown();
			this.label_BoxThickness = new System.Windows.Forms.Label();
			this.nud_BoxSizeX = new System.Windows.Forms.NumericUpDown();
			this.nud_BoxSizeY = new System.Windows.Forms.NumericUpDown();
			this.nud_BoxSizeZ = new System.Windows.Forms.NumericUpDown();
			this.label_BoxSize = new System.Windows.Forms.Label();
			this.label_SphereRadius = new System.Windows.Forms.Label();
			this.nud_SphereRadiusThickness = new System.Windows.Forms.NumericUpDown();
			this.nud_SphereRadius = new System.Windows.Forms.NumericUpDown();
			this.label_RadiusThickness = new System.Windows.Forms.Label();
			this.comboBox_ShapeType = new System.Windows.Forms.ComboBox();
			this.label_ShapeType = new System.Windows.Forms.Label();
			this.nud_StartVelocityX = new System.Windows.Forms.NumericUpDown();
			this.nud_AccelerationX = new System.Windows.Forms.NumericUpDown();
			this.nud_StartVelocityY = new System.Windows.Forms.NumericUpDown();
			this.nud_AccelerationY = new System.Windows.Forms.NumericUpDown();
			this.nud_Duration = new System.Windows.Forms.NumericUpDown();
			this.label_Duration = new System.Windows.Forms.Label();
			this.button_ImportTexture = new System.Windows.Forms.Button();
			this.label_TexturePath = new System.Windows.Forms.Label();
			this.label_Texture = new System.Windows.Forms.Label();
			this.nud_StartVelocityZ = new System.Windows.Forms.NumericUpDown();
			this.nud_AccelerationZ = new System.Windows.Forms.NumericUpDown();
			this.nud_Lifetime = new System.Windows.Forms.NumericUpDown();
			this.nud_SpawnRate = new System.Windows.Forms.NumericUpDown();
			this.groupBox_ParticleSize = new System.Windows.Forms.GroupBox();
			this.nud_EndSizeY = new System.Windows.Forms.NumericUpDown();
			this.nud_EndSizeX = new System.Windows.Forms.NumericUpDown();
			this.nud_StartSizeY = new System.Windows.Forms.NumericUpDown();
			this.nud_StartSizeX = new System.Windows.Forms.NumericUpDown();
			this.groupBox_ParticleColor = new System.Windows.Forms.GroupBox();
			this.button_SelectEndColor = new System.Windows.Forms.Button();
			this.button_SelectStartColor = new System.Windows.Forms.Button();
			this.label_StartVelocity = new System.Windows.Forms.Label();
			this.folderBrowserDialog_Skybox = new System.Windows.Forms.FolderBrowserDialog();
			this.openFileDialog_Texture = new System.Windows.Forms.OpenFileDialog();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.newToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.resetCameraToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.resetPivotToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.resetRotationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.resetScaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.totalResetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.showDebugLinesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.showPlayerModelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.panel_Properties = new System.Windows.Forms.Panel();
			this.button_Stop = new System.Windows.Forms.Button();
			this.button_Play = new System.Windows.Forms.Button();
			this.panel_GamePanel = new System.Windows.Forms.Panel();
			this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
			this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
			this.timer = new System.Windows.Forms.Timer(this.components);
			this.groupBox_Skybox.SuspendLayout();
			this.groupBox_ParticleProperties.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nud_GravityModifier)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_RotationVelocity2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_RotationVelocity1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartRotation2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartRotation1)).BeginInit();
			this.groupBox_SphereData.SuspendLayout();
			this.groupBox_BoxData.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nud_BoxThickness)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_BoxSizeX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_BoxSizeY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_BoxSizeZ)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_SphereRadiusThickness)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_SphereRadius)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartVelocityX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_AccelerationX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartVelocityY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_AccelerationY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_Duration)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartVelocityZ)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_AccelerationZ)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_Lifetime)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_SpawnRate)).BeginInit();
			this.groupBox_ParticleSize.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.nud_EndSizeY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_EndSizeX)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartSizeY)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartSizeX)).BeginInit();
			this.groupBox_ParticleColor.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			this.panel_Properties.SuspendLayout();
			this.SuspendLayout();
			// 
			// label_Lifetime
			// 
			this.label_Lifetime.AutoSize = true;
			this.label_Lifetime.Location = new System.Drawing.Point(7, 104);
			this.label_Lifetime.Name = "label_Lifetime";
			this.label_Lifetime.Size = new System.Drawing.Size(43, 13);
			this.label_Lifetime.TabIndex = 4;
			this.label_Lifetime.Text = "Lifetime";
			// 
			// label_SpawnRate
			// 
			this.label_SpawnRate.AutoSize = true;
			this.label_SpawnRate.Location = new System.Drawing.Point(7, 77);
			this.label_SpawnRate.Name = "label_SpawnRate";
			this.label_SpawnRate.Size = new System.Drawing.Size(66, 13);
			this.label_SpawnRate.TabIndex = 5;
			this.label_SpawnRate.Text = "Spawn Rate";
			// 
			// label_Loopable
			// 
			this.label_Loopable.AutoSize = true;
			this.label_Loopable.Location = new System.Drawing.Point(7, 51);
			this.label_Loopable.Name = "label_Loopable";
			this.label_Loopable.Size = new System.Drawing.Size(51, 13);
			this.label_Loopable.TabIndex = 6;
			this.label_Loopable.Text = "Loopable";
			// 
			// label_Acceleration
			// 
			this.label_Acceleration.AutoSize = true;
			this.label_Acceleration.Location = new System.Drawing.Point(7, 130);
			this.label_Acceleration.Name = "label_Acceleration";
			this.label_Acceleration.Size = new System.Drawing.Size(66, 13);
			this.label_Acceleration.TabIndex = 7;
			this.label_Acceleration.Text = "Acceleration";
			// 
			// label_StartColor
			// 
			this.label_StartColor.AutoSize = true;
			this.label_StartColor.Location = new System.Drawing.Point(6, 25);
			this.label_StartColor.Name = "label_StartColor";
			this.label_StartColor.Size = new System.Drawing.Size(56, 13);
			this.label_StartColor.TabIndex = 8;
			this.label_StartColor.Text = "Start Color";
			// 
			// label_EndColor
			// 
			this.label_EndColor.AutoSize = true;
			this.label_EndColor.Location = new System.Drawing.Point(6, 56);
			this.label_EndColor.Name = "label_EndColor";
			this.label_EndColor.Size = new System.Drawing.Size(53, 13);
			this.label_EndColor.TabIndex = 9;
			this.label_EndColor.Text = "End Color";
			// 
			// label_StartSize
			// 
			this.label_StartSize.AutoSize = true;
			this.label_StartSize.Location = new System.Drawing.Point(6, 26);
			this.label_StartSize.Name = "label_StartSize";
			this.label_StartSize.Size = new System.Drawing.Size(52, 13);
			this.label_StartSize.TabIndex = 10;
			this.label_StartSize.Text = "Start Size";
			// 
			// label_EndSize
			// 
			this.label_EndSize.AutoSize = true;
			this.label_EndSize.Location = new System.Drawing.Point(6, 49);
			this.label_EndSize.Name = "label_EndSize";
			this.label_EndSize.Size = new System.Drawing.Size(49, 13);
			this.label_EndSize.TabIndex = 11;
			this.label_EndSize.Text = "End Size";
			// 
			// checkBox_Loopable
			// 
			this.checkBox_Loopable.AutoSize = true;
			this.checkBox_Loopable.Location = new System.Drawing.Point(276, 51);
			this.checkBox_Loopable.Name = "checkBox_Loopable";
			this.checkBox_Loopable.Size = new System.Drawing.Size(15, 14);
			this.checkBox_Loopable.TabIndex = 2;
			this.checkBox_Loopable.UseVisualStyleBackColor = true;
			this.checkBox_Loopable.CheckedChanged += new System.EventHandler(this.checkBox_Loopable_CheckedChanged);
			// 
			// groupBox_Skybox
			// 
			this.groupBox_Skybox.Controls.Add(this.label_SkyboxDirectory);
			this.groupBox_Skybox.Controls.Add(this.comboBox_SkyBox);
			this.groupBox_Skybox.Location = new System.Drawing.Point(4, 14);
			this.groupBox_Skybox.Name = "groupBox_Skybox";
			this.groupBox_Skybox.Size = new System.Drawing.Size(308, 81);
			this.groupBox_Skybox.TabIndex = 13;
			this.groupBox_Skybox.TabStop = false;
			this.groupBox_Skybox.Text = "Skybox:";
			// 
			// label_SkyboxDirectory
			// 
			this.label_SkyboxDirectory.Location = new System.Drawing.Point(6, 22);
			this.label_SkyboxDirectory.Name = "label_SkyboxDirectory";
			this.label_SkyboxDirectory.Size = new System.Drawing.Size(119, 18);
			this.label_SkyboxDirectory.TabIndex = 6;
			this.label_SkyboxDirectory.Text = "No Directory";
			// 
			// comboBox_SkyBox
			// 
			this.comboBox_SkyBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox_SkyBox.FormattingEnabled = true;
			this.comboBox_SkyBox.Location = new System.Drawing.Point(6, 48);
			this.comboBox_SkyBox.Name = "comboBox_SkyBox";
			this.comboBox_SkyBox.Size = new System.Drawing.Size(296, 21);
			this.comboBox_SkyBox.TabIndex = 0;
			this.comboBox_SkyBox.DropDown += new System.EventHandler(this.comboBox_SkyBox_DropDown);
			this.comboBox_SkyBox.SelectedIndexChanged += new System.EventHandler(this.comboBox_Skybox_SelectedIndexChanged);
			// 
			// groupBox_ParticleProperties
			// 
			this.groupBox_ParticleProperties.Controls.Add(this.comboBox_BlendState);
			this.groupBox_ParticleProperties.Controls.Add(this.label_BlendState);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_GravityModifier);
			this.groupBox_ParticleProperties.Controls.Add(this.label_GravityModifier);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_RotationVelocity2);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_RotationVelocity1);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_StartRotation2);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_StartRotation1);
			this.groupBox_ParticleProperties.Controls.Add(this.label_RotationVelocity);
			this.groupBox_ParticleProperties.Controls.Add(this.label_StartRotation);
			this.groupBox_ParticleProperties.Controls.Add(this.groupBox_SphereData);
			this.groupBox_ParticleProperties.Controls.Add(this.comboBox_ShapeType);
			this.groupBox_ParticleProperties.Controls.Add(this.label_ShapeType);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_StartVelocityX);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_AccelerationX);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_StartVelocityY);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_AccelerationY);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_Duration);
			this.groupBox_ParticleProperties.Controls.Add(this.label_Duration);
			this.groupBox_ParticleProperties.Controls.Add(this.button_ImportTexture);
			this.groupBox_ParticleProperties.Controls.Add(this.label_TexturePath);
			this.groupBox_ParticleProperties.Controls.Add(this.label_Texture);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_StartVelocityZ);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_AccelerationZ);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_Lifetime);
			this.groupBox_ParticleProperties.Controls.Add(this.nud_SpawnRate);
			this.groupBox_ParticleProperties.Controls.Add(this.groupBox_ParticleSize);
			this.groupBox_ParticleProperties.Controls.Add(this.label_SpawnRate);
			this.groupBox_ParticleProperties.Controls.Add(this.checkBox_Loopable);
			this.groupBox_ParticleProperties.Controls.Add(this.label_Loopable);
			this.groupBox_ParticleProperties.Controls.Add(this.groupBox_ParticleColor);
			this.groupBox_ParticleProperties.Controls.Add(this.label_Lifetime);
			this.groupBox_ParticleProperties.Controls.Add(this.label_StartVelocity);
			this.groupBox_ParticleProperties.Controls.Add(this.label_Acceleration);
			this.groupBox_ParticleProperties.Location = new System.Drawing.Point(3, 113);
			this.groupBox_ParticleProperties.Name = "groupBox_ParticleProperties";
			this.groupBox_ParticleProperties.Size = new System.Drawing.Size(308, 633);
			this.groupBox_ParticleProperties.TabIndex = 14;
			this.groupBox_ParticleProperties.TabStop = false;
			this.groupBox_ParticleProperties.Text = "Particle Properties";
			// 
			// comboBox_BlendState
			// 
			this.comboBox_BlendState.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox_BlendState.FormattingEnabled = true;
			this.comboBox_BlendState.Location = new System.Drawing.Point(212, 569);
			this.comboBox_BlendState.Name = "comboBox_BlendState";
			this.comboBox_BlendState.Size = new System.Drawing.Size(86, 21);
			this.comboBox_BlendState.TabIndex = 40;
			this.comboBox_BlendState.SelectedIndexChanged += new System.EventHandler(this.comboBox_BlendState_SelectedIndexChanged);
			// 
			// label_BlendState
			// 
			this.label_BlendState.AutoSize = true;
			this.label_BlendState.Location = new System.Drawing.Point(7, 572);
			this.label_BlendState.Name = "label_BlendState";
			this.label_BlendState.Size = new System.Drawing.Size(59, 13);
			this.label_BlendState.TabIndex = 39;
			this.label_BlendState.Text = "BlendState";
			// 
			// nud_GravityModifier
			// 
			this.nud_GravityModifier.DecimalPlaces = 3;
			this.nud_GravityModifier.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_GravityModifier.Location = new System.Drawing.Point(225, 237);
			this.nud_GravityModifier.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
			this.nud_GravityModifier.Name = "nud_GravityModifier";
			this.nud_GravityModifier.Size = new System.Drawing.Size(66, 20);
			this.nud_GravityModifier.TabIndex = 38;
			this.nud_GravityModifier.ValueChanged += new System.EventHandler(this.nud_GravityModifier_ValueChanged);
			// 
			// label_GravityModifier
			// 
			this.label_GravityModifier.AutoSize = true;
			this.label_GravityModifier.Location = new System.Drawing.Point(7, 239);
			this.label_GravityModifier.Name = "label_GravityModifier";
			this.label_GravityModifier.Size = new System.Drawing.Size(80, 13);
			this.label_GravityModifier.TabIndex = 37;
			this.label_GravityModifier.Text = "Gravity Modifier";
			// 
			// nud_RotationVelocity2
			// 
			this.nud_RotationVelocity2.DecimalPlaces = 3;
			this.nud_RotationVelocity2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_RotationVelocity2.Location = new System.Drawing.Point(225, 209);
			this.nud_RotationVelocity2.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
			this.nud_RotationVelocity2.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
			this.nud_RotationVelocity2.Name = "nud_RotationVelocity2";
			this.nud_RotationVelocity2.Size = new System.Drawing.Size(66, 20);
			this.nud_RotationVelocity2.TabIndex = 36;
			this.nud_RotationVelocity2.ValueChanged += new System.EventHandler(this.nud_RotationVelocity_ValueChanged);
			// 
			// nud_RotationVelocity1
			// 
			this.nud_RotationVelocity1.DecimalPlaces = 3;
			this.nud_RotationVelocity1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_RotationVelocity1.Location = new System.Drawing.Point(157, 209);
			this.nud_RotationVelocity1.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
			this.nud_RotationVelocity1.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
			this.nud_RotationVelocity1.Name = "nud_RotationVelocity1";
			this.nud_RotationVelocity1.Size = new System.Drawing.Size(62, 20);
			this.nud_RotationVelocity1.TabIndex = 35;
			this.nud_RotationVelocity1.ValueChanged += new System.EventHandler(this.nud_RotationVelocity_ValueChanged);
			// 
			// nud_StartRotation2
			// 
			this.nud_StartRotation2.DecimalPlaces = 3;
			this.nud_StartRotation2.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_StartRotation2.Location = new System.Drawing.Point(225, 181);
			this.nud_StartRotation2.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
			this.nud_StartRotation2.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
			this.nud_StartRotation2.Name = "nud_StartRotation2";
			this.nud_StartRotation2.Size = new System.Drawing.Size(66, 20);
			this.nud_StartRotation2.TabIndex = 34;
			this.nud_StartRotation2.ValueChanged += new System.EventHandler(this.nud_StartRotation_ValueChanged);
			// 
			// nud_StartRotation1
			// 
			this.nud_StartRotation1.DecimalPlaces = 3;
			this.nud_StartRotation1.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_StartRotation1.Location = new System.Drawing.Point(157, 181);
			this.nud_StartRotation1.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
			this.nud_StartRotation1.Minimum = new decimal(new int[] {
            360,
            0,
            0,
            -2147483648});
			this.nud_StartRotation1.Name = "nud_StartRotation1";
			this.nud_StartRotation1.Size = new System.Drawing.Size(62, 20);
			this.nud_StartRotation1.TabIndex = 33;
			this.nud_StartRotation1.ValueChanged += new System.EventHandler(this.nud_StartRotation_ValueChanged);
			// 
			// label_RotationVelocity
			// 
			this.label_RotationVelocity.AutoSize = true;
			this.label_RotationVelocity.Location = new System.Drawing.Point(7, 211);
			this.label_RotationVelocity.Name = "label_RotationVelocity";
			this.label_RotationVelocity.Size = new System.Drawing.Size(87, 13);
			this.label_RotationVelocity.TabIndex = 32;
			this.label_RotationVelocity.Text = "Rotation Velocity";
			// 
			// label_StartRotation
			// 
			this.label_StartRotation.AutoSize = true;
			this.label_StartRotation.Location = new System.Drawing.Point(7, 183);
			this.label_StartRotation.Name = "label_StartRotation";
			this.label_StartRotation.Size = new System.Drawing.Size(72, 13);
			this.label_StartRotation.TabIndex = 31;
			this.label_StartRotation.Text = "Start Rotation";
			// 
			// groupBox_SphereData
			// 
			this.groupBox_SphereData.Controls.Add(this.groupBox_BoxData);
			this.groupBox_SphereData.Controls.Add(this.label_SphereRadius);
			this.groupBox_SphereData.Controls.Add(this.nud_SphereRadiusThickness);
			this.groupBox_SphereData.Controls.Add(this.nud_SphereRadius);
			this.groupBox_SphereData.Controls.Add(this.label_RadiusThickness);
			this.groupBox_SphereData.Location = new System.Drawing.Point(9, 293);
			this.groupBox_SphereData.Name = "groupBox_SphereData";
			this.groupBox_SphereData.Size = new System.Drawing.Size(291, 79);
			this.groupBox_SphereData.TabIndex = 30;
			this.groupBox_SphereData.TabStop = false;
			this.groupBox_SphereData.Text = "SphereData";
			// 
			// groupBox_BoxData
			// 
			this.groupBox_BoxData.Controls.Add(this.nud_BoxThickness);
			this.groupBox_BoxData.Controls.Add(this.label_BoxThickness);
			this.groupBox_BoxData.Controls.Add(this.nud_BoxSizeX);
			this.groupBox_BoxData.Controls.Add(this.nud_BoxSizeY);
			this.groupBox_BoxData.Controls.Add(this.nud_BoxSizeZ);
			this.groupBox_BoxData.Controls.Add(this.label_BoxSize);
			this.groupBox_BoxData.Location = new System.Drawing.Point(1, 0);
			this.groupBox_BoxData.Name = "groupBox_BoxData";
			this.groupBox_BoxData.Size = new System.Drawing.Size(293, 79);
			this.groupBox_BoxData.TabIndex = 30;
			this.groupBox_BoxData.TabStop = false;
			this.groupBox_BoxData.Text = "BoxData";
			// 
			// nud_BoxThickness
			// 
			this.nud_BoxThickness.DecimalPlaces = 3;
			this.nud_BoxThickness.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_BoxThickness.Location = new System.Drawing.Point(216, 48);
			this.nud_BoxThickness.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.nud_BoxThickness.Name = "nud_BoxThickness";
			this.nud_BoxThickness.Size = new System.Drawing.Size(65, 20);
			this.nud_BoxThickness.TabIndex = 12;
			this.nud_BoxThickness.ValueChanged += new System.EventHandler(this.nud_BoxThickness_ValueChanged);
			// 
			// label_BoxThickness
			// 
			this.label_BoxThickness.AutoSize = true;
			this.label_BoxThickness.Location = new System.Drawing.Point(7, 50);
			this.label_BoxThickness.Name = "label_BoxThickness";
			this.label_BoxThickness.Size = new System.Drawing.Size(77, 13);
			this.label_BoxThickness.TabIndex = 11;
			this.label_BoxThickness.Text = "Box Thickness";
			// 
			// nud_BoxSizeX
			// 
			this.nud_BoxSizeX.DecimalPlaces = 3;
			this.nud_BoxSizeX.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_BoxSizeX.Location = new System.Drawing.Point(77, 22);
			this.nud_BoxSizeX.Name = "nud_BoxSizeX";
			this.nud_BoxSizeX.Size = new System.Drawing.Size(65, 20);
			this.nud_BoxSizeX.TabIndex = 8;
			this.nud_BoxSizeX.ValueChanged += new System.EventHandler(this.nud_BoxSize_ValueChanged);
			// 
			// nud_BoxSizeY
			// 
			this.nud_BoxSizeY.DecimalPlaces = 3;
			this.nud_BoxSizeY.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_BoxSizeY.Location = new System.Drawing.Point(148, 22);
			this.nud_BoxSizeY.Name = "nud_BoxSizeY";
			this.nud_BoxSizeY.Size = new System.Drawing.Size(62, 20);
			this.nud_BoxSizeY.TabIndex = 9;
			this.nud_BoxSizeY.ValueChanged += new System.EventHandler(this.nud_BoxSize_ValueChanged);
			// 
			// nud_BoxSizeZ
			// 
			this.nud_BoxSizeZ.DecimalPlaces = 3;
			this.nud_BoxSizeZ.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_BoxSizeZ.Location = new System.Drawing.Point(216, 22);
			this.nud_BoxSizeZ.Name = "nud_BoxSizeZ";
			this.nud_BoxSizeZ.Size = new System.Drawing.Size(66, 20);
			this.nud_BoxSizeZ.TabIndex = 10;
			this.nud_BoxSizeZ.ValueChanged += new System.EventHandler(this.nud_BoxSize_ValueChanged);
			// 
			// label_BoxSize
			// 
			this.label_BoxSize.AutoSize = true;
			this.label_BoxSize.Location = new System.Drawing.Point(6, 24);
			this.label_BoxSize.Name = "label_BoxSize";
			this.label_BoxSize.Size = new System.Drawing.Size(48, 13);
			this.label_BoxSize.TabIndex = 0;
			this.label_BoxSize.Text = "Box Size";
			// 
			// label_SphereRadius
			// 
			this.label_SphereRadius.AutoSize = true;
			this.label_SphereRadius.Location = new System.Drawing.Point(6, 24);
			this.label_SphereRadius.Name = "label_SphereRadius";
			this.label_SphereRadius.Size = new System.Drawing.Size(40, 13);
			this.label_SphereRadius.TabIndex = 26;
			this.label_SphereRadius.Text = "Radius";
			// 
			// nud_SphereRadiusThickness
			// 
			this.nud_SphereRadiusThickness.DecimalPlaces = 3;
			this.nud_SphereRadiusThickness.Location = new System.Drawing.Point(219, 48);
			this.nud_SphereRadiusThickness.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.nud_SphereRadiusThickness.Name = "nud_SphereRadiusThickness";
			this.nud_SphereRadiusThickness.Size = new System.Drawing.Size(66, 20);
			this.nud_SphereRadiusThickness.TabIndex = 29;
			this.nud_SphereRadiusThickness.ValueChanged += new System.EventHandler(this.nud_SphereRadiusThickness_ValueChanged);
			// 
			// nud_SphereRadius
			// 
			this.nud_SphereRadius.DecimalPlaces = 3;
			this.nud_SphereRadius.Location = new System.Drawing.Point(219, 22);
			this.nud_SphereRadius.Name = "nud_SphereRadius";
			this.nud_SphereRadius.Size = new System.Drawing.Size(66, 20);
			this.nud_SphereRadius.TabIndex = 28;
			this.nud_SphereRadius.ValueChanged += new System.EventHandler(this.nud_SphereRadius_ValueChanged);
			// 
			// label_RadiusThickness
			// 
			this.label_RadiusThickness.AutoSize = true;
			this.label_RadiusThickness.Location = new System.Drawing.Point(6, 50);
			this.label_RadiusThickness.Name = "label_RadiusThickness";
			this.label_RadiusThickness.Size = new System.Drawing.Size(92, 13);
			this.label_RadiusThickness.TabIndex = 27;
			this.label_RadiusThickness.Text = "Radius Thickness";
			// 
			// comboBox_ShapeType
			// 
			this.comboBox_ShapeType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox_ShapeType.FormattingEnabled = true;
			this.comboBox_ShapeType.Location = new System.Drawing.Point(212, 266);
			this.comboBox_ShapeType.Name = "comboBox_ShapeType";
			this.comboBox_ShapeType.Size = new System.Drawing.Size(78, 21);
			this.comboBox_ShapeType.TabIndex = 25;
			this.comboBox_ShapeType.SelectedIndexChanged += new System.EventHandler(this.comboBox_ShapeType_SelectedIndexChanged);
			// 
			// label_ShapeType
			// 
			this.label_ShapeType.AutoSize = true;
			this.label_ShapeType.Location = new System.Drawing.Point(7, 270);
			this.label_ShapeType.Name = "label_ShapeType";
			this.label_ShapeType.Size = new System.Drawing.Size(65, 13);
			this.label_ShapeType.TabIndex = 24;
			this.label_ShapeType.Text = "Shape Type";
			// 
			// nud_StartVelocityX
			// 
			this.nud_StartVelocityX.DecimalPlaces = 3;
			this.nud_StartVelocityX.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_StartVelocityX.Location = new System.Drawing.Point(86, 154);
			this.nud_StartVelocityX.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.nud_StartVelocityX.Name = "nud_StartVelocityX";
			this.nud_StartVelocityX.Size = new System.Drawing.Size(65, 20);
			this.nud_StartVelocityX.TabIndex = 8;
			this.nud_StartVelocityX.ValueChanged += new System.EventHandler(this.nud_StartVelocityX_ValueChanged);
			// 
			// nud_AccelerationX
			// 
			this.nud_AccelerationX.DecimalPlaces = 3;
			this.nud_AccelerationX.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_AccelerationX.Location = new System.Drawing.Point(86, 128);
			this.nud_AccelerationX.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.nud_AccelerationX.Name = "nud_AccelerationX";
			this.nud_AccelerationX.Size = new System.Drawing.Size(65, 20);
			this.nud_AccelerationX.TabIndex = 5;
			this.nud_AccelerationX.ValueChanged += new System.EventHandler(this.nud_AccelerationX_ValueChanged);
			// 
			// nud_StartVelocityY
			// 
			this.nud_StartVelocityY.DecimalPlaces = 3;
			this.nud_StartVelocityY.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_StartVelocityY.Location = new System.Drawing.Point(157, 154);
			this.nud_StartVelocityY.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.nud_StartVelocityY.Name = "nud_StartVelocityY";
			this.nud_StartVelocityY.Size = new System.Drawing.Size(62, 20);
			this.nud_StartVelocityY.TabIndex = 9;
			this.nud_StartVelocityY.ValueChanged += new System.EventHandler(this.nud_StartVelocityY_ValueChanged);
			// 
			// nud_AccelerationY
			// 
			this.nud_AccelerationY.DecimalPlaces = 3;
			this.nud_AccelerationY.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_AccelerationY.Location = new System.Drawing.Point(157, 128);
			this.nud_AccelerationY.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.nud_AccelerationY.Name = "nud_AccelerationY";
			this.nud_AccelerationY.Size = new System.Drawing.Size(62, 20);
			this.nud_AccelerationY.TabIndex = 6;
			this.nud_AccelerationY.ValueChanged += new System.EventHandler(this.nud_AccelerationY_ValueChanged);
			// 
			// nud_Duration
			// 
			this.nud_Duration.DecimalPlaces = 3;
			this.nud_Duration.Location = new System.Drawing.Point(225, 23);
			this.nud_Duration.Name = "nud_Duration";
			this.nud_Duration.Size = new System.Drawing.Size(66, 20);
			this.nud_Duration.TabIndex = 1;
			this.nud_Duration.ValueChanged += new System.EventHandler(this.nud_Duration_ValueChanged);
			// 
			// label_Duration
			// 
			this.label_Duration.AutoSize = true;
			this.label_Duration.Location = new System.Drawing.Point(7, 25);
			this.label_Duration.Name = "label_Duration";
			this.label_Duration.Size = new System.Drawing.Size(47, 13);
			this.label_Duration.TabIndex = 23;
			this.label_Duration.Text = "Duration";
			// 
			// button_ImportTexture
			// 
			this.button_ImportTexture.Location = new System.Drawing.Point(223, 598);
			this.button_ImportTexture.Name = "button_ImportTexture";
			this.button_ImportTexture.Size = new System.Drawing.Size(75, 23);
			this.button_ImportTexture.TabIndex = 17;
			this.button_ImportTexture.Text = "&Import";
			this.button_ImportTexture.UseVisualStyleBackColor = true;
			this.button_ImportTexture.Click += new System.EventHandler(this.button_ImportTexture_Click);
			// 
			// label_TexturePath
			// 
			this.label_TexturePath.Location = new System.Drawing.Point(108, 603);
			this.label_TexturePath.Name = "label_TexturePath";
			this.label_TexturePath.Size = new System.Drawing.Size(109, 13);
			this.label_TexturePath.TabIndex = 21;
			this.label_TexturePath.Text = "Default Texture";
			// 
			// label_Texture
			// 
			this.label_Texture.AutoSize = true;
			this.label_Texture.Location = new System.Drawing.Point(7, 603);
			this.label_Texture.Name = "label_Texture";
			this.label_Texture.Size = new System.Drawing.Size(43, 13);
			this.label_Texture.TabIndex = 20;
			this.label_Texture.Text = "Texture";
			// 
			// nud_StartVelocityZ
			// 
			this.nud_StartVelocityZ.DecimalPlaces = 3;
			this.nud_StartVelocityZ.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_StartVelocityZ.Location = new System.Drawing.Point(225, 154);
			this.nud_StartVelocityZ.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.nud_StartVelocityZ.Name = "nud_StartVelocityZ";
			this.nud_StartVelocityZ.Size = new System.Drawing.Size(66, 20);
			this.nud_StartVelocityZ.TabIndex = 10;
			this.nud_StartVelocityZ.ValueChanged += new System.EventHandler(this.nud_StartVelocityZ_ValueChanged);
			// 
			// nud_AccelerationZ
			// 
			this.nud_AccelerationZ.DecimalPlaces = 3;
			this.nud_AccelerationZ.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_AccelerationZ.Location = new System.Drawing.Point(225, 128);
			this.nud_AccelerationZ.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
			this.nud_AccelerationZ.Name = "nud_AccelerationZ";
			this.nud_AccelerationZ.Size = new System.Drawing.Size(66, 20);
			this.nud_AccelerationZ.TabIndex = 7;
			this.nud_AccelerationZ.ValueChanged += new System.EventHandler(this.nud_AccelerationZ_ValueChanged);
			// 
			// nud_Lifetime
			// 
			this.nud_Lifetime.DecimalPlaces = 3;
			this.nud_Lifetime.Increment = new decimal(new int[] {
            3,
            0,
            0,
            65536});
			this.nud_Lifetime.Location = new System.Drawing.Point(225, 102);
			this.nud_Lifetime.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
			this.nud_Lifetime.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_Lifetime.Name = "nud_Lifetime";
			this.nud_Lifetime.Size = new System.Drawing.Size(66, 20);
			this.nud_Lifetime.TabIndex = 4;
			this.nud_Lifetime.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_Lifetime.ValueChanged += new System.EventHandler(this.nud_Lifetime_ValueChanged);
			// 
			// nud_SpawnRate
			// 
			this.nud_SpawnRate.DecimalPlaces = 3;
			this.nud_SpawnRate.Increment = new decimal(new int[] {
            3,
            0,
            0,
            65536});
			this.nud_SpawnRate.Location = new System.Drawing.Point(225, 75);
			this.nud_SpawnRate.Maximum = new decimal(new int[] {
            200,
            0,
            0,
            0});
			this.nud_SpawnRate.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_SpawnRate.Name = "nud_SpawnRate";
			this.nud_SpawnRate.Size = new System.Drawing.Size(66, 20);
			this.nud_SpawnRate.TabIndex = 3;
			this.nud_SpawnRate.Value = new decimal(new int[] {
            1,
            0,
            0,
            65536});
			this.nud_SpawnRate.ValueChanged += new System.EventHandler(this.nud_SpawnRate_ValueChanged);
			// 
			// groupBox_ParticleSize
			// 
			this.groupBox_ParticleSize.Controls.Add(this.nud_EndSizeY);
			this.groupBox_ParticleSize.Controls.Add(this.nud_EndSizeX);
			this.groupBox_ParticleSize.Controls.Add(this.nud_StartSizeY);
			this.groupBox_ParticleSize.Controls.Add(this.nud_StartSizeX);
			this.groupBox_ParticleSize.Controls.Add(this.label_StartSize);
			this.groupBox_ParticleSize.Controls.Add(this.label_EndSize);
			this.groupBox_ParticleSize.Location = new System.Drawing.Point(9, 481);
			this.groupBox_ParticleSize.Name = "groupBox_ParticleSize";
			this.groupBox_ParticleSize.Size = new System.Drawing.Size(291, 76);
			this.groupBox_ParticleSize.TabIndex = 16;
			this.groupBox_ParticleSize.TabStop = false;
			this.groupBox_ParticleSize.Text = "Size";
			// 
			// nud_EndSizeY
			// 
			this.nud_EndSizeY.DecimalPlaces = 3;
			this.nud_EndSizeY.Location = new System.Drawing.Point(219, 47);
			this.nud_EndSizeY.Name = "nud_EndSizeY";
			this.nud_EndSizeY.Size = new System.Drawing.Size(62, 20);
			this.nud_EndSizeY.TabIndex = 16;
			this.nud_EndSizeY.ValueChanged += new System.EventHandler(this.nud_EndSizeY_ValueChanged);
			// 
			// nud_EndSizeX
			// 
			this.nud_EndSizeX.DecimalPlaces = 3;
			this.nud_EndSizeX.Location = new System.Drawing.Point(148, 47);
			this.nud_EndSizeX.Name = "nud_EndSizeX";
			this.nud_EndSizeX.Size = new System.Drawing.Size(65, 20);
			this.nud_EndSizeX.TabIndex = 15;
			this.nud_EndSizeX.ValueChanged += new System.EventHandler(this.nud_EndSizeX_ValueChanged);
			// 
			// nud_StartSizeY
			// 
			this.nud_StartSizeY.DecimalPlaces = 3;
			this.nud_StartSizeY.Location = new System.Drawing.Point(219, 19);
			this.nud_StartSizeY.Name = "nud_StartSizeY";
			this.nud_StartSizeY.Size = new System.Drawing.Size(62, 20);
			this.nud_StartSizeY.TabIndex = 14;
			this.nud_StartSizeY.ValueChanged += new System.EventHandler(this.nud_StartSizeY_ValueChanged);
			// 
			// nud_StartSizeX
			// 
			this.nud_StartSizeX.DecimalPlaces = 3;
			this.nud_StartSizeX.Location = new System.Drawing.Point(148, 19);
			this.nud_StartSizeX.Name = "nud_StartSizeX";
			this.nud_StartSizeX.Size = new System.Drawing.Size(65, 20);
			this.nud_StartSizeX.TabIndex = 13;
			this.nud_StartSizeX.ValueChanged += new System.EventHandler(this.nud_StartSizeX_ValueChanged);
			// 
			// groupBox_ParticleColor
			// 
			this.groupBox_ParticleColor.Controls.Add(this.button_SelectEndColor);
			this.groupBox_ParticleColor.Controls.Add(this.button_SelectStartColor);
			this.groupBox_ParticleColor.Controls.Add(this.label_EndColor);
			this.groupBox_ParticleColor.Controls.Add(this.label_StartColor);
			this.groupBox_ParticleColor.Location = new System.Drawing.Point(9, 378);
			this.groupBox_ParticleColor.Name = "groupBox_ParticleColor";
			this.groupBox_ParticleColor.Size = new System.Drawing.Size(291, 87);
			this.groupBox_ParticleColor.TabIndex = 15;
			this.groupBox_ParticleColor.TabStop = false;
			this.groupBox_ParticleColor.Text = "Color";
			// 
			// button_SelectEndColor
			// 
			this.button_SelectEndColor.Location = new System.Drawing.Point(210, 52);
			this.button_SelectEndColor.Name = "button_SelectEndColor";
			this.button_SelectEndColor.Size = new System.Drawing.Size(75, 20);
			this.button_SelectEndColor.TabIndex = 12;
			this.button_SelectEndColor.Text = "Select Color";
			this.button_SelectEndColor.UseVisualStyleBackColor = true;
			this.button_SelectEndColor.Click += new System.EventHandler(this.button_SelectEndColor_Click);
			// 
			// button_SelectStartColor
			// 
			this.button_SelectStartColor.Location = new System.Drawing.Point(210, 21);
			this.button_SelectStartColor.Name = "button_SelectStartColor";
			this.button_SelectStartColor.Size = new System.Drawing.Size(75, 20);
			this.button_SelectStartColor.TabIndex = 11;
			this.button_SelectStartColor.Text = "Select Color";
			this.button_SelectStartColor.UseVisualStyleBackColor = true;
			this.button_SelectStartColor.Click += new System.EventHandler(this.button_SelectStartColor_Click);
			// 
			// label_StartVelocity
			// 
			this.label_StartVelocity.AutoSize = true;
			this.label_StartVelocity.Location = new System.Drawing.Point(7, 156);
			this.label_StartVelocity.Name = "label_StartVelocity";
			this.label_StartVelocity.Size = new System.Drawing.Size(69, 13);
			this.label_StartVelocity.TabIndex = 7;
			this.label_StartVelocity.Text = "Start Velocity";
			// 
			// openFileDialog_Texture
			// 
			this.openFileDialog_Texture.DefaultExt = "*.dds";
			this.openFileDialog_Texture.FileName = "openFileDialog_Texure";
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.editToolStripMenuItem,
            this.viewToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(1406, 24);
			this.menuStrip1.TabIndex = 15;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// fileToolStripMenuItem
			// 
			this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.openToolStripMenuItem});
			this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
			this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
			this.fileToolStripMenuItem.Text = "&File";
			// 
			// newToolStripMenuItem
			// 
			this.newToolStripMenuItem.Name = "newToolStripMenuItem";
			this.newToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.N)));
			this.newToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.newToolStripMenuItem.Text = "&New";
			this.newToolStripMenuItem.Click += new System.EventHandler(this.newToolStripMenuItem_Click);
			// 
			// saveToolStripMenuItem
			// 
			this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
			this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
			this.saveToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.saveToolStripMenuItem.Text = "&Save";
			this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
			// 
			// openToolStripMenuItem
			// 
			this.openToolStripMenuItem.Name = "openToolStripMenuItem";
			this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
			this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.openToolStripMenuItem.Text = "&Open";
			this.openToolStripMenuItem.Click += new System.EventHandler(this.openToolStripMenuItem_Click);
			// 
			// editToolStripMenuItem
			// 
			this.editToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetCameraToolStripMenuItem});
			this.editToolStripMenuItem.Name = "editToolStripMenuItem";
			this.editToolStripMenuItem.Size = new System.Drawing.Size(39, 20);
			this.editToolStripMenuItem.Text = "&Edit";
			// 
			// resetCameraToolStripMenuItem
			// 
			this.resetCameraToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.resetPivotToolStripMenuItem,
            this.resetRotationToolStripMenuItem,
            this.resetScaleToolStripMenuItem,
            this.totalResetToolStripMenuItem});
			this.resetCameraToolStripMenuItem.Name = "resetCameraToolStripMenuItem";
			this.resetCameraToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
			this.resetCameraToolStripMenuItem.Text = "&Reset Camera";
			// 
			// resetPivotToolStripMenuItem
			// 
			this.resetPivotToolStripMenuItem.Name = "resetPivotToolStripMenuItem";
			this.resetPivotToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
			this.resetPivotToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
			this.resetPivotToolStripMenuItem.Text = "&Reset Pivot";
			this.resetPivotToolStripMenuItem.Click += new System.EventHandler(this.resetPivotToolStripMenuItem_Click);
			// 
			// resetRotationToolStripMenuItem
			// 
			this.resetRotationToolStripMenuItem.Name = "resetRotationToolStripMenuItem";
			this.resetRotationToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.W)));
			this.resetRotationToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
			this.resetRotationToolStripMenuItem.Text = "&Reset Rotation";
			this.resetRotationToolStripMenuItem.Click += new System.EventHandler(this.resetRotationToolStripMenuItem_Click);
			// 
			// resetScaleToolStripMenuItem
			// 
			this.resetScaleToolStripMenuItem.Name = "resetScaleToolStripMenuItem";
			this.resetScaleToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.E)));
			this.resetScaleToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
			this.resetScaleToolStripMenuItem.Text = "&Reset Scale";
			this.resetScaleToolStripMenuItem.Click += new System.EventHandler(this.resetScaleToolStripMenuItem_Click);
			// 
			// totalResetToolStripMenuItem
			// 
			this.totalResetToolStripMenuItem.Name = "totalResetToolStripMenuItem";
			this.totalResetToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
			this.totalResetToolStripMenuItem.Size = new System.Drawing.Size(195, 22);
			this.totalResetToolStripMenuItem.Text = "&Total Reset";
			this.totalResetToolStripMenuItem.Click += new System.EventHandler(this.totalResetToolStripMenuItem_Click);
			// 
			// viewToolStripMenuItem
			// 
			this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showDebugLinesToolStripMenuItem,
            this.showPlayerModelToolStripMenuItem});
			this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
			this.viewToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
			this.viewToolStripMenuItem.Text = "&View";
			// 
			// showDebugLinesToolStripMenuItem
			// 
			this.showDebugLinesToolStripMenuItem.Name = "showDebugLinesToolStripMenuItem";
			this.showDebugLinesToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.D)));
			this.showDebugLinesToolStripMenuItem.Size = new System.Drawing.Size(216, 22);
			this.showDebugLinesToolStripMenuItem.Text = "&Show Debug Lines";
			this.showDebugLinesToolStripMenuItem.Click += new System.EventHandler(this.showDebugLinesToolStripMenuItem_Click);
			// 
			// panel_Properties
			// 
			this.panel_Properties.Controls.Add(this.groupBox_ParticleProperties);
			this.panel_Properties.Controls.Add(this.groupBox_Skybox);
			this.panel_Properties.Location = new System.Drawing.Point(1085, 38);
			this.panel_Properties.Name = "panel_Properties";
			this.panel_Properties.Size = new System.Drawing.Size(319, 749);
			this.panel_Properties.TabIndex = 16;
			// 
			// button_Stop
			// 
			this.button_Stop.Location = new System.Drawing.Point(996, 751);
			this.button_Stop.Name = "button_Stop";
			this.button_Stop.Size = new System.Drawing.Size(72, 21);
			this.button_Stop.TabIndex = 19;
			this.button_Stop.Text = "&Stop";
			this.button_Stop.UseVisualStyleBackColor = true;
			this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
			// 
			// button_Play
			// 
			this.button_Play.Location = new System.Drawing.Point(996, 723);
			this.button_Play.Name = "button_Play";
			this.button_Play.Size = new System.Drawing.Size(72, 22);
			this.button_Play.TabIndex = 18;
			this.button_Play.Text = "&Play";
			this.button_Play.UseVisualStyleBackColor = true;
			this.button_Play.Click += new System.EventHandler(this.button_Play_Click);
			// 
			// panel_GamePanel
			// 
			this.panel_GamePanel.Enabled = false;
			this.panel_GamePanel.Location = new System.Drawing.Point(12, 38);
			this.panel_GamePanel.Name = "panel_GamePanel";
			this.panel_GamePanel.Size = new System.Drawing.Size(1067, 749);
			this.panel_GamePanel.TabIndex = 20;
			// 
			// timer
			// 
			this.timer.Interval = 1000;
			this.timer.Tick += new System.EventHandler(this.timer_Tick);
			// 
			// ParticleEditor
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1406, 791);
			this.Controls.Add(this.button_Play);
			this.Controls.Add(this.button_Stop);
			this.Controls.Add(this.panel_GamePanel);
			this.Controls.Add(this.panel_Properties);
			this.Controls.Add(this.menuStrip1);
			this.DoubleBuffered = true;
			this.Name = "ParticleEditor";
			this.Text = "Particle Editor";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Shown += new System.EventHandler(this.Form1_Load);
			this.groupBox_Skybox.ResumeLayout(false);
			this.groupBox_ParticleProperties.ResumeLayout(false);
			this.groupBox_ParticleProperties.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nud_GravityModifier)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_RotationVelocity2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_RotationVelocity1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartRotation2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartRotation1)).EndInit();
			this.groupBox_SphereData.ResumeLayout(false);
			this.groupBox_SphereData.PerformLayout();
			this.groupBox_BoxData.ResumeLayout(false);
			this.groupBox_BoxData.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nud_BoxThickness)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_BoxSizeX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_BoxSizeY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_BoxSizeZ)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_SphereRadiusThickness)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_SphereRadius)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartVelocityX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_AccelerationX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartVelocityY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_AccelerationY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_Duration)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartVelocityZ)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_AccelerationZ)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_Lifetime)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_SpawnRate)).EndInit();
			this.groupBox_ParticleSize.ResumeLayout(false);
			this.groupBox_ParticleSize.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.nud_EndSizeY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_EndSizeX)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartSizeY)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.nud_StartSizeX)).EndInit();
			this.groupBox_ParticleColor.ResumeLayout(false);
			this.groupBox_ParticleColor.PerformLayout();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.panel_Properties.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_Lifetime;
        private System.Windows.Forms.Label label_SpawnRate;
        private System.Windows.Forms.Label label_Loopable;
        private System.Windows.Forms.Label label_Acceleration;
        private System.Windows.Forms.Label label_StartColor;
        private System.Windows.Forms.Label label_EndColor;
        private System.Windows.Forms.Label label_StartSize;
        private System.Windows.Forms.Label label_EndSize;
        private System.Windows.Forms.CheckBox checkBox_Loopable;
        private System.Windows.Forms.GroupBox groupBox_Skybox;
        private System.Windows.Forms.GroupBox groupBox_ParticleProperties;
        private System.Windows.Forms.GroupBox groupBox_ParticleColor;
        private System.Windows.Forms.GroupBox groupBox_ParticleSize;
        private System.Windows.Forms.NumericUpDown nud_AccelerationZ;
        private System.Windows.Forms.NumericUpDown nud_Lifetime;
        private System.Windows.Forms.NumericUpDown nud_SpawnRate;
        private System.Windows.Forms.NumericUpDown nud_EndSizeY;
        private System.Windows.Forms.NumericUpDown nud_EndSizeX;
        private System.Windows.Forms.NumericUpDown nud_StartSizeY;
        private System.Windows.Forms.NumericUpDown nud_StartSizeX;
        private System.Windows.Forms.Button button_ImportTexture;
        private System.Windows.Forms.Label label_TexturePath;
        private System.Windows.Forms.Label label_Texture;
        private System.Windows.Forms.NumericUpDown nud_Duration;
        private System.Windows.Forms.Label label_Duration;
        private System.Windows.Forms.ComboBox comboBox_SkyBox;
        private System.Windows.Forms.Label label_SkyboxDirectory;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog_Skybox;
        private System.Windows.Forms.OpenFileDialog openFileDialog_Texture;
        private System.Windows.Forms.Button button_SelectEndColor;
        private System.Windows.Forms.Button button_SelectStartColor;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.Panel panel_Properties;
        private System.Windows.Forms.NumericUpDown nud_AccelerationX;
        private System.Windows.Forms.NumericUpDown nud_AccelerationY;
		private System.Windows.Forms.NumericUpDown nud_StartVelocityX;
		private System.Windows.Forms.NumericUpDown nud_StartVelocityY;
		private System.Windows.Forms.NumericUpDown nud_StartVelocityZ;
		private System.Windows.Forms.Label label_StartVelocity;
		private System.Windows.Forms.Button button_Stop;
		private System.Windows.Forms.Button button_Play;
		private System.Windows.Forms.Panel panel_GamePanel;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem showDebugLinesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetCameraToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetPivotToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetRotationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resetScaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem totalResetToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.ToolStripMenuItem showPlayerModelToolStripMenuItem;
        private System.Windows.Forms.Label label_ShapeType;
        private System.Windows.Forms.ComboBox comboBox_ShapeType;
        private System.Windows.Forms.GroupBox groupBox_SphereData;
        private System.Windows.Forms.Label label_SphereRadius;
        private System.Windows.Forms.NumericUpDown nud_SphereRadiusThickness;
        private System.Windows.Forms.NumericUpDown nud_SphereRadius;
        private System.Windows.Forms.Label label_RadiusThickness;
		private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.GroupBox groupBox_BoxData;
        private System.Windows.Forms.NumericUpDown nud_BoxThickness;
        private System.Windows.Forms.Label label_BoxThickness;
        private System.Windows.Forms.NumericUpDown nud_BoxSizeX;
        private System.Windows.Forms.NumericUpDown nud_BoxSizeY;
        private System.Windows.Forms.NumericUpDown nud_BoxSizeZ;
        private System.Windows.Forms.Label label_BoxSize;
        private System.Windows.Forms.Label label_RotationVelocity;
        private System.Windows.Forms.Label label_StartRotation;
        private System.Windows.Forms.NumericUpDown nud_RotationVelocity2;
        private System.Windows.Forms.NumericUpDown nud_RotationVelocity1;
        private System.Windows.Forms.NumericUpDown nud_StartRotation2;
        private System.Windows.Forms.NumericUpDown nud_StartRotation1;
        private System.Windows.Forms.NumericUpDown nud_GravityModifier;
        private System.Windows.Forms.Label label_GravityModifier;
        private System.Windows.Forms.Label label_BlendState;
        private System.Windows.Forms.ComboBox comboBox_BlendState;
    }
}


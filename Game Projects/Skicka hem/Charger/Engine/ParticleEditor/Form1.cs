﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Security.Permissions;

namespace ParticleEditor
{
    public partial class ParticleEditor : Form
    {
        [DllImport("kernel32.dll",
            EntryPoint = "GetStdHandle",
            SetLastError = true,
            CharSet = CharSet.Auto,
            CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr GetStdHandle(int nStdHandle);

        [DllImport("kernel32.dll",
            EntryPoint = "AllocConsole",
            SetLastError = true,
            CharSet = CharSet.Auto,
            CallingConvention = CallingConvention.StdCall)]
        private static extern int AllocConsole();
        
        CEditorBridgeParticleEditor myEditorBridge = null;
        private Thread myThread;
        struct SDefaultParticleData
        {
            public decimal myDuration;
            public bool myIsLoopable;
            public decimal mySpawnRate;
            public decimal myLifetime;
            public EditorTools.Vector3 myAcceleration;
            public EditorTools.Vector3 myStartVelocity;
            public EditorTools.Vector2 myStartRotation;
            public EditorTools.Vector2 myRotationVelocity;
            public decimal myGravityModifier;
            public EditorTools.Vector4 myStartColor;
            public EditorTools.Vector4 myEndColor;
            public EditorTools.Vector2 myStartSize;
            public EditorTools.Vector2 myEndSize;
            public string myTexture;
        }

        private enum EShapeTypes
        {
            Sphere,
            Box,
            Point,
            Count
        }

        private enum EBlendStates
        {
            AlphaBlend,
            Additive,
            Count
        }

        private bool myHasBeenInitialised = false;
        private SDefaultParticleData myDefaultParticleData;
        private string myLoadedFileName;
        private const float Pif = 3.1415926535898f;
        private const float RadToDeg = 180.0f / Pif;
        private const float DegToRad = Pif / 180.0f;

        private enum EState
        {
            Playing,
            Paused,
            Stopped,
            Count
        }

        private EState myState = EState.Stopped;

        public ParticleEditor()
        {
            InitializeComponent();
            this.KeyPreview = true;
        }

		private void StartEditor(object aHandle)
        {
            myEditorBridge = new CEditorBridgeParticleEditor();
            myEditorBridge.Init(1, (IntPtr)aHandle);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            myThread = new Thread(new ParameterizedThreadStart(StartEditor));
            myThread.Start(panel_GamePanel.Handle);
            panel_GamePanel.Visible = false;
            panel_Properties.Visible = false;
            button_Play.Visible = false;
            button_Stop.Visible = false;
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            myEditorBridge.Shutdown();
            myThread.Join();
        }

        static int[] myMessageFilter = new int[] {
            2,      //WM_DESTROY
            5,      //WM_SIZE
            16,     //WM_CLOSE
            256,
            257,
            260,
            261,
            512,
            513,
            514,
            516,
            517,
            519,
            520,
            522
        };

        protected override void WndProc(ref Message m)
        {
            bool validMessage = false;
            for (int i = 0; i < myMessageFilter.Length; ++i)
            {
                if (m.Msg == myMessageFilter[i])
                {
                    validMessage = true;
                    break;
                }
            }

            if (!validMessage)
            {
                base.WndProc(ref m);
                return;
            }
            
            if (myEditorBridge != null)
            {
                if(panel_GamePanel.Visible)
                {
                    myEditorBridge.WndProc(m);
                }
            }
            base.WndProc(ref m);
        }

        private void checkBox_Loopable_CheckedChanged(object sender, EventArgs e)
        {
            myEditorBridge.SetIsLoopable(checkBox_Loopable.Checked);
        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            myState = EState.Stopped;
            myEditorBridge.ParticleSystemStop();
            button_Play.Text = "Play";
        }

        private void button_Play_Click(object sender, EventArgs e)
        {
            if (myState == EState.Stopped || myState == EState.Paused)
            {
                myEditorBridge.ParticleSystemPlay();
                button_Play.Text = "&Pause";
                myState = EState.Playing;
                timer.Stop();
                timer.Start();
            }
            else if (myState == EState.Playing)
            {
                myEditorBridge.ParticleSystemPause();
                myState = EState.Paused;
                button_Play.Text = "&Play";
            }
        }

        private void LoadSkyboxTextures()
        {
            string path = Directory.GetCurrentDirectory() + "/Assets/CubeMaps/";
            label_SkyboxDirectory.Text = "Assets/CubeMaps/";
            string[] files = Directory.GetFiles(path, "*.dds");

            for (int i = 0; i < files.Length; ++i)
            {
                comboBox_SkyBox.Items.Add(Path.GetFileName(files[i]));
            }
        }

        private void comboBox_Skybox_SelectedIndexChanged(object sender, EventArgs e)
        {
            object selectedItem = comboBox_SkyBox.SelectedItem;
            if (selectedItem != null)
            {
                string item = "Assets/CubeMaps/" + selectedItem.ToString();
                myEditorBridge.SetCubemapTexture(item);
            }
        }

        private void comboBox_SkyBox_DropDown(object sender, EventArgs e)
        {
            comboBox_SkyBox.Items.Clear();
            LoadSkyboxTextures();
        }

        private void button_SkyboxDirectory_Click(object sender, EventArgs e)
        {
            DialogResult result = folderBrowserDialog_Skybox.ShowDialog();

            if (result == DialogResult.OK)
            {
                label_SkyboxDirectory.Text = folderBrowserDialog_Skybox.SelectedPath;
            }
        }
        
        private void SetDefaultParticleData()
        {
            nud_Duration.Value = myDefaultParticleData.myDuration;
            checkBox_Loopable.Checked = myDefaultParticleData.myIsLoopable;
            nud_SpawnRate.Value = myDefaultParticleData.mySpawnRate;
            nud_Lifetime.Value = myDefaultParticleData.myLifetime;

			nud_AccelerationX.Value = (decimal)myDefaultParticleData.myAcceleration.x;
			nud_AccelerationY.Value = (decimal)myDefaultParticleData.myAcceleration.y;
			nud_AccelerationZ.Value = (decimal)myDefaultParticleData.myAcceleration.z;

			nud_StartVelocityX.Value = (decimal)myDefaultParticleData.myStartVelocity.x;
			nud_StartVelocityY.Value = (decimal)myDefaultParticleData.myStartVelocity.y;
			nud_StartVelocityZ.Value = (decimal)myDefaultParticleData.myStartVelocity.z;
            
            nud_StartRotation1.Value = (decimal)myDefaultParticleData.myStartRotation.x;
            nud_StartRotation2.Value = (decimal)myDefaultParticleData.myStartRotation.y;

            nud_RotationVelocity1.Value = (decimal)myDefaultParticleData.myRotationVelocity.x;
            nud_RotationVelocity2.Value = (decimal)myDefaultParticleData.myRotationVelocity.y;

            nud_GravityModifier.Value = myDefaultParticleData.myGravityModifier;

            nud_StartSizeX.Value = (decimal)myDefaultParticleData.myStartSize.x;
			nud_StartSizeY.Value = (decimal)myDefaultParticleData.myStartSize.y;

			nud_EndSizeX.Value = (decimal)myDefaultParticleData.myEndSize.x;
			nud_EndSizeY.Value = (decimal)myDefaultParticleData.myEndSize.y;

            label_TexturePath.Text = "Default Texture";
        }

        private void LoadDefaultParticleData()
        {
            myDefaultParticleData.myDuration = (decimal)myEditorBridge.GetDuration();
            myDefaultParticleData.myIsLoopable = myEditorBridge.GetIsLoopable();
            myDefaultParticleData.mySpawnRate = (decimal)myEditorBridge.GetSpawnRate();
            myDefaultParticleData.myLifetime = (decimal)myEditorBridge.GetLifeTime();
            myDefaultParticleData.myAcceleration = myEditorBridge.GetAcceleration();
            myDefaultParticleData.myStartVelocity = myEditorBridge.GetStartVelocity();
            myDefaultParticleData.myStartRotation = myEditorBridge.GetStartRotation();
            myDefaultParticleData.myStartRotation.x *= RadToDeg;
            myDefaultParticleData.myStartRotation.y *= RadToDeg;
            myDefaultParticleData.myRotationVelocity = myEditorBridge.GetRotationVelocity();
            myDefaultParticleData.myRotationVelocity.x *= RadToDeg;
            myDefaultParticleData.myRotationVelocity.y *= RadToDeg;
            myDefaultParticleData.myGravityModifier = (decimal)myEditorBridge.GetGravityModifier();
            myDefaultParticleData.myStartColor = myEditorBridge.GetStartColor();
            myDefaultParticleData.myEndColor = myEditorBridge.GetEndColor();
            myDefaultParticleData.myStartSize = myEditorBridge.GetStartSize();
            myDefaultParticleData.myEndSize = myEditorBridge.GetEndSize();
            myDefaultParticleData.myTexture = "Default Texture";
        }

        private void button_ImportTexture_Click(object sender, EventArgs e)
        {
            openFileDialog_Texture.Title = "Import Particle Texture";
            openFileDialog_Texture.InitialDirectory = Directory.GetCurrentDirectory() + "\\Assets\\Particles\\Sprites";
            openFileDialog_Texture.DefaultExt = "*.dds";
            openFileDialog_Texture.FileName = "";
            DialogResult result = openFileDialog_Texture.ShowDialog();

            if (result == DialogResult.OK)
            {
                label_TexturePath.Text = Path.GetFileName(openFileDialog_Texture.FileName);
                string file = openFileDialog_Texture.FileName.Replace(Directory.GetCurrentDirectory() + "\\", "");
                myEditorBridge.SetTexture(file);
            }
        }

        private void button_SelectStartColor_Click(object sender, EventArgs e)
        {
			EditorTools.ColorPicker colorPicker = new EditorTools.ColorPicker(myEditorBridge.GetStartColor());
            DialogResult result = colorPicker.ShowDialog();

            if (result == DialogResult.OK)
            {
				EditorTools.Vector4 vec = new EditorTools.Vector4(
					colorPicker.myColor.myR,
					colorPicker.myColor.myG,
					colorPicker.myColor.myB,
					colorPicker.myColor.myA
				);
				myEditorBridge.SetStartColor(vec);
            }
        }
		private void button_SelectEndColor_Click(object sender, EventArgs e)
		{
			EditorTools.ColorPicker colorPicker = new EditorTools.ColorPicker(myEditorBridge.GetEndColor());
            DialogResult result = colorPicker.ShowDialog();

            if (result == DialogResult.OK)
            {
				EditorTools.Vector4 vec = new EditorTools.Vector4(
					colorPicker.myColor.myR,
					colorPicker.myColor.myG,
					colorPicker.myColor.myB,
					colorPicker.myColor.myA
				);
				myEditorBridge.SetEndColor(vec);
            }
		}

        private void newToolStripMenuItem_Click(object sender, EventArgs e)
        {
			if (!myEditorBridge.IsRunning()) return;

            if (!myHasBeenInitialised)
            {
                LoadDefaultParticleData();
                myHasBeenInitialised = true;
            }

            myEditorBridge.ResetParticleToDefault();
            SetDefaultParticleData();
            LoadSkyboxTextures();
            panel_GamePanel.Visible = true;
            panel_Properties.Visible = true;
            button_Play.Visible = true;
            button_Stop.Visible = true;
            FillShapeTypesList((int)EShapeTypes.Point);
            FillBlendStatesList((int)EBlendStates.AlphaBlend);
            FillShapeData();
            groupBox_SphereData.Visible = false;
            groupBox_BoxData.Visible = false;
            myLoadedFileName = "newParticle";

            if (comboBox_SkyBox.SelectedIndex == -1)
            {
                if (comboBox_SkyBox.Items.Count > 0)
                {
                    comboBox_SkyBox.SelectedIndex = 0;
                }
            }
        }

        private void nud_Duration_ValueChanged(object sender, EventArgs e)
        {
            myEditorBridge.SetDuration((float)nud_Duration.Value);
        }
        private void nud_SpawnRate_ValueChanged(object sender, EventArgs e)
        {
            float spawnRate = (float)nud_SpawnRate.Value;
            float lifeTime = Math.Min((float)nud_Lifetime.Value, 100f / spawnRate);

            myEditorBridge.SetSpawnRate(spawnRate);
            myEditorBridge.SetLifetime(lifeTime);

            nud_SpawnRate.Value = (decimal)spawnRate;
            nud_Lifetime.Value = (decimal)lifeTime;
        }
        private void nud_Lifetime_ValueChanged(object sender, EventArgs e)
        {
            float lifeTime = (float)nud_Lifetime.Value;
            float spawnRate = Math.Min((float)nud_SpawnRate.Value, 100f / lifeTime);

            myEditorBridge.SetSpawnRate(spawnRate);
            myEditorBridge.SetLifetime(lifeTime);

            nud_Lifetime.Value = (decimal)lifeTime;
            nud_SpawnRate.Value = (decimal)spawnRate;
        }

        private void SetAccelerationOnParticle()
        {
			EditorTools.Vector3 vec = new EditorTools.Vector3(
				(float)nud_AccelerationX.Value,
				(float)nud_AccelerationY.Value,
				(float)nud_AccelerationZ.Value
			);
            myEditorBridge.SetAcceleration(vec);
        }
		private void SetStartVelocityOnParticle()
		{
			EditorTools.Vector3 vec = new EditorTools.Vector3(
				(float)nud_StartVelocityX.Value,
				(float)nud_StartVelocityY.Value,
				(float)nud_StartVelocityZ.Value
			);
            myEditorBridge.SetStartVelocity(vec);
		}
        private void SetStartSizeOnParticle()
        {
			EditorTools.Vector2 vec = new EditorTools.Vector2(
				(float)nud_StartSizeX.Value,
				(float)nud_StartSizeY.Value
			);
            myEditorBridge.SetStartSize(vec);
        }
        private void SetEndSizeOnParticle()
        {
			EditorTools.Vector2 vec = new EditorTools.Vector2(
				(float)nud_EndSizeX.Value,
				(float)nud_EndSizeY.Value
			);
            myEditorBridge.SetEndSize(vec);
        }

        private void nud_AccelerationX_ValueChanged(object sender, EventArgs e)
        {
            SetAccelerationOnParticle();
        }
        private void nud_AccelerationY_ValueChanged(object sender, EventArgs e)
        {
            SetAccelerationOnParticle();
        }
        private void nud_AccelerationZ_ValueChanged(object sender, EventArgs e)
        {
            SetAccelerationOnParticle();
        }
		private void nud_StartVelocityX_ValueChanged(object sender, EventArgs e)
		{
            SetStartVelocityOnParticle();
		}
		private void nud_StartVelocityY_ValueChanged(object sender, EventArgs e)
		{
            SetStartVelocityOnParticle();
		}
		private void nud_StartVelocityZ_ValueChanged(object sender, EventArgs e)
		{
            SetStartVelocityOnParticle();
		}
        private void nud_StartSizeX_ValueChanged(object sender, EventArgs e)
        {
            SetStartSizeOnParticle();
        }
        private void nud_StartSizeY_ValueChanged(object sender, EventArgs e)
        {
            SetStartSizeOnParticle();
        }
        private void nud_EndSizeX_ValueChanged(object sender, EventArgs e)
        {
            SetEndSizeOnParticle();
        }
        private void nud_EndSizeY_ValueChanged(object sender, EventArgs e)
        {
            SetEndSizeOnParticle();
        }

        private void showDebugLinesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myEditorBridge.ToggleDebugLines();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if(!myEditorBridge.IsPlaying() && myState == EState.Playing)
            {
                myState = EState.Stopped;
                button_Play.Text = "&Play";
                timer.Stop();
            }
        }

        private void resetPivotToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myEditorBridge.ResetCameraPivot();
        }
        private void resetRotationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myEditorBridge.ResetCameraRotation();
        }
        private void totalResetToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myEditorBridge.ResetCameraPivot();
            myEditorBridge.ResetCameraRotation();
            myEditorBridge.ResetCameraScale();
        }
        private void resetScaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            myEditorBridge.ResetCameraScale();
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
			if (!myEditorBridge.IsRunning()) return;

            if(panel_GamePanel.Visible == false)
            {
                return;
            }
            saveFileDialog.Filter = "JSON file|*.json";
            saveFileDialog.Title = "Save JSON file";
            saveFileDialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\Assets\\Particles";
            saveFileDialog.FileName = myLoadedFileName;
            DialogResult result = saveFileDialog.ShowDialog();

            if(result == DialogResult.OK)
            {
                string file = saveFileDialog.FileName.Replace(Directory.GetCurrentDirectory() + "\\", "");
                if(saveFileDialog.FileName != "")
                {
                    myEditorBridge.SaveFile(file);
                }
            }
            
        }

        LoadFileCallback myCallback;

        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {
			if (!myEditorBridge.IsRunning()) return;

            if(!myHasBeenInitialised)
            {
                LoadDefaultParticleData();
                myHasBeenInitialised = true;
            }
            openFileDialog.InitialDirectory = Directory.GetCurrentDirectory() + "\\Assets\\Particles";
            openFileDialog.Filter = "JSON file|*.json";
            openFileDialog.Title = "Load JSON file";
            openFileDialog.FileName = "";
            DialogResult result = openFileDialog.ShowDialog();

            if (result == DialogResult.OK)
            {
                if (openFileDialog.FileName != "")
                {
                    string file = openFileDialog.FileName.Replace(Directory.GetCurrentDirectory() + "\\", "");
                    myLoadedFileName = Path.GetFileName(file);

                    myCallback = new LoadFileCallback(MyLoadFileCallback);

                    myEditorBridge.LoadFile(file, Marshal.GetFunctionPointerForDelegate(myCallback));
                }
            }
        }

        public delegate void LoadFileCallback();
        public void MyLoadFileCallback()
        {
            LoadSkyboxTextures();
            panel_GamePanel.Visible = true;
            panel_Properties.Visible = true;
            button_Play.Visible = true;
            button_Stop.Visible = true;
            LoadData();

            if(comboBox_SkyBox.SelectedIndex == -1)
            {
                if(comboBox_SkyBox.Items.Count > 0)
                {
                    comboBox_SkyBox.SelectedIndex = 0;
                }
            }
        }

        public void LoadData()
        {
            while (!myEditorBridge.GetHasFinishedLoading()) { };

            nud_Duration.Value = (decimal)myEditorBridge.GetDuration();
            checkBox_Loopable.Checked = myEditorBridge.GetIsLoopable();
            nud_SpawnRate.Value = (decimal)myEditorBridge.GetSpawnRate();
            nud_Lifetime.Value = (decimal)myEditorBridge.GetLifeTime();

            EditorTools.Vector3 acceleration = myEditorBridge.GetAcceleration();
            nud_AccelerationX.Value = (decimal)acceleration.x;
            nud_AccelerationY.Value = (decimal)acceleration.y;
            nud_AccelerationZ.Value = (decimal)acceleration.z;

            EditorTools.Vector3 startVelocity = myEditorBridge.GetStartVelocity();
            nud_StartVelocityX.Value = (decimal)startVelocity.x;
            nud_StartVelocityY.Value = (decimal)startVelocity.y;
            nud_StartVelocityZ.Value = (decimal)startVelocity.z;

            EditorTools.Vector2 startRotation = myEditorBridge.GetStartRotation();
            startRotation.x = startRotation.x * RadToDeg;
            startRotation.y = startRotation.y * RadToDeg;
            nud_StartRotation1.Value = (decimal)startRotation.x;
            nud_StartRotation2.Value = (decimal)startRotation.y;

            EditorTools.Vector2 rotationVelocity = myEditorBridge.GetRotationVelocity();
            rotationVelocity.x = rotationVelocity.x * RadToDeg;
            rotationVelocity.y = rotationVelocity.y * RadToDeg;
            nud_RotationVelocity1.Value = (decimal)rotationVelocity.x;
            nud_RotationVelocity2.Value = (decimal)rotationVelocity.y;

            nud_GravityModifier.Value = (decimal)myEditorBridge.GetGravityModifier();

            EditorTools.Vector2 startSize = myEditorBridge.GetStartSize();
            nud_StartSizeX.Value = (decimal)startSize.x;
            nud_StartSizeY.Value = (decimal)startSize.y;

            EditorTools.Vector2 endSize = myEditorBridge.GetEndSize();
            nud_EndSizeX.Value = (decimal)endSize.x;
            nud_EndSizeY.Value = (decimal)endSize.y;

            label_TexturePath.Text = Path.GetFileName(myEditorBridge.GetTexturePath());

            if(label_TexturePath.Text == "")
            {
                label_TexturePath.Text = "Default Texture";
            }

            FillShapeTypesList(myEditorBridge.GetShapeType());
            FillBlendStatesList(myEditorBridge.GetBlendState()-1);
            FillShapeData();
        }

        private void comboBox_ShapeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            myEditorBridge.SetShapeType(comboBox_ShapeType.SelectedIndex);

            if(comboBox_ShapeType.SelectedIndex == (int)EShapeTypes.Sphere)
            {
                groupBox_SphereData.Visible = true;
                groupBox_BoxData.Visible = false;
            }
            else if (comboBox_ShapeType.SelectedIndex == (int)EShapeTypes.Box)
            {
                groupBox_BoxData.Parent = groupBox_SphereData.Parent;
                groupBox_BoxData.Location = groupBox_SphereData.Location;
                groupBox_BoxData.Visible = true;
                groupBox_SphereData.Visible = false;
            }
            else
            {
                groupBox_BoxData.Visible = false;
                groupBox_SphereData.Visible = false;
            }
        }

        private void FillShapeTypesList(int aSelectionIndex)
        {
            comboBox_ShapeType.Items.Clear();

            for(int index = 0; index < (int)EShapeTypes.Count; ++index)
            {
                comboBox_ShapeType.Items.Add((EShapeTypes)index);
            }

            comboBox_ShapeType.SelectedIndex = aSelectionIndex;

        }

        private void nud_SphereRadius_ValueChanged(object sender, EventArgs e)
        {
            myEditorBridge.SetSphereRadius((float)nud_SphereRadius.Value);
        }

        private void nud_SphereRadiusThickness_ValueChanged(object sender, EventArgs e)
        {
            myEditorBridge.SetSphereRadiusThickness((float)nud_SphereRadiusThickness.Value);
        }

        private void FillShapeData()
        {
                nud_SphereRadius.Value = (decimal)myEditorBridge.GetSphereRadius();
                nud_SphereRadiusThickness.Value = (decimal)myEditorBridge.GetSphereRadiusThickness();
                EditorTools.Vector3 boxSize = myEditorBridge.GetBoxSize();
                nud_BoxSizeX.Value = (decimal)boxSize.x;
                nud_BoxSizeY.Value = (decimal)boxSize.y;
                nud_BoxSizeZ.Value = (decimal)boxSize.z;

                nud_BoxThickness.Value = (decimal)myEditorBridge.GetBoxThickness();
        }

        private void nud_BoxSize_ValueChanged(object sender, EventArgs e)
        {
            EditorTools.Vector3 boxSize = new EditorTools.Vector3(
                (float)nud_BoxSizeX.Value,
                (float)nud_BoxSizeY.Value,
                (float)nud_BoxSizeZ.Value);
            myEditorBridge.SetBoxSize(boxSize);
        }

        private void nud_BoxThickness_ValueChanged(object sender, EventArgs e)
        {
            myEditorBridge.SetBoxThickness((float)nud_BoxThickness.Value);
        }

        private void nud_StartRotation_ValueChanged(object sender, EventArgs e)
        {
            EditorTools.Vector2 vec = new EditorTools.Vector2(
                (float)nud_StartRotation1.Value * DegToRad,
                (float)nud_StartRotation2.Value * DegToRad);
            myEditorBridge.SetStartRotation(vec);
        }

        private void nud_RotationVelocity_ValueChanged(object sender, EventArgs e)
        {
            EditorTools.Vector2 vec = new EditorTools.Vector2(
                (float)nud_RotationVelocity1.Value * DegToRad,
                (float)nud_RotationVelocity2.Value * DegToRad);
            myEditorBridge.SetRotationVelocity(vec);
        }

        private void nud_GravityModifier_ValueChanged(object sender, EventArgs e)
        {
            myEditorBridge.SetGravityModifier((float)nud_GravityModifier.Value);
        }

        private void comboBox_BlendState_SelectedIndexChanged(object sender, EventArgs e)
        {
            myEditorBridge.SetBlendState(comboBox_BlendState.SelectedIndex+1);
        }

        private void FillBlendStatesList(int aSelectionIndex)
        {
            comboBox_BlendState.Items.Clear();

            for (int index = 0; index < (int)EBlendStates.Count; ++index)
            {
                comboBox_BlendState.Items.Add((EBlendStates)index);
            }

            comboBox_BlendState.SelectedIndex = aSelectionIndex;

        }
    }

}
#pragma once

#include "Vector.h"

class CColliderSphere
{
public:
	CColliderSphere(const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f }, float aRadius = 1.f);
	~CColliderSphere();

	void SetPosition(const CommonUtilities::Vector3f& aPostion) { myPosition = aPostion; }
	void SetRadius(const float& aRadius) { myRadius = aRadius; }


	inline const CommonUtilities::Vector3f& GetPosition() const { return myPosition; }
	inline float GetRadius() const { return myRadius; }

private:
	CommonUtilities::Vector3f myPosition;
	float myRadius;
};


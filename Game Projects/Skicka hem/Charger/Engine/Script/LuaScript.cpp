#include "stdafx.h"
#include "LuaScript.h"
#include "ScriptManager.h"

CLuaScript::CLuaScript()
{
	myOwner = nullptr;
	ourScriptManager = nullptr;
}

CLuaScript::~CLuaScript()
{
}

void CLuaScript::Init(CScriptComponent* aScriptComponent, CScriptManager* aScriptManager, lua_State* aLuaState, const char* aPath)
{
	myOwner = aScriptComponent;
	ourScriptManager = aScriptManager;
	myLuaState = aLuaState;
	myPath = aPath;
	myScriptStateIsValid = ourScriptManager->CompileScriptFile(this);
}

void CLuaScript::InitForNonGameObjects(CScriptManager * aScriptManager, lua_State * aLuaState, const char * aPath)
{
	ourScriptManager = aScriptManager;
	myLuaState = aLuaState;
	myPath = aPath;
	myScriptStateIsValid = ourScriptManager->CompileScriptFile(this);
}

const std::string& CLuaScript::GetPath() const
{
	return myPath;
}
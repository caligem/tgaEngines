#include "stdafx.h"
#include "ScriptEventManager.h"
#include "ScriptManager.h"


CScriptManager* CScriptEventManager::ourScriptManager = nullptr;

CScriptEventManager::CScriptEventManager()
{
	for (int i = 0; i < EScriptEvent_Count; i++)
	{
		CommonUtilities::GrowingArray<SLuaCallback>  temp;
		temp.Init(8);
		const int numberOfTriggerIDs = 512;
		for (int j = 0; j < numberOfTriggerIDs; j++)
		{
			myListeners[i].push_back(temp);
		}
	}
}

CScriptEventManager::~CScriptEventManager()
{
}

void CScriptEventManager::Notify(const EScriptEvent aEvent, const unsigned short aTriggerID)
{
	for (const auto it : myListeners[aEvent][aTriggerID])
	{
		CallFunction(it);
	}
}

void CScriptEventManager::AddListener(const EScriptEvent aEvent, const SLuaCallback aLuaCallback)
{
	myListeners[aEvent][aLuaCallback.triggerID].Add(aLuaCallback);
}

void CScriptEventManager::RemoveListener(const EScriptEvent aEvent, const SLuaCallback aLuaCallback)
{
	myListeners[aEvent][aLuaCallback.triggerID].RemoveCyclic(aLuaCallback);
}

void CScriptEventManager::CallFunction(SLuaCallback aCallback)
{
	ourScriptManager->CallFunction(aCallback.idt, aCallback.functionName);
}
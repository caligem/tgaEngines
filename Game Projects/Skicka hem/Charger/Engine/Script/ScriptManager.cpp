#include "stdafx.h"
#include "ScriptManager.h"

#include <fstream>
#include <string>

#include "DL_Debug.h"
#include <Windows.h>
#include <iostream>
#include "LuaUtility.h"

#include "ScriptEventManager.h"

CScriptManager* CScriptManager::ourScriptManager = nullptr;

CScriptManager::CScriptManager()
	: myLuaScripts(8192)
{
}

CScriptManager::~CScriptManager()
{
	for (auto it = myLuaScripts.begin(); it != myLuaScripts.end(); ++it)
	{
		lua_close(it->myLuaState);
	}
}

bool CScriptManager::Init(std::function<void()> aCallback)
{
	ourScriptManager = this;
	CScriptEventManager::ourScriptManager = this;
	myRegisteredFunctions.Init(16);

	if (aCallback)
	{
		aCallback();
		PrintHelpFile();
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "std::function<void()> aCallback CScriptManager");
	}

	return true;
}

void CScriptManager::RunString(ID_T(CLuaScript) aLuaScriptID, const std::string& aFunctionName)
{
	if (luaL_dostring(myLuaScripts.GetObj(aLuaScriptID)->myLuaState, aFunctionName.c_str()) != 0)
	{
		SCRIPT_LOG(CONCOL_WARNING, "Could not run string");
	}
}

bool CScriptManager::RegisterFunction(const std::string& aFunctionName, lua_CFunction aFunction, const std::string& aHelpText, const int aExposedType)
{
	for (int i = 0; i < myRegisteredFunctions.Size(); ++i)
	{
		if (myRegisteredFunctions[i].myFunctionNameInLua == aFunctionName)
		{
			SCRIPT_LOG(CONCOL_WARNING, "Trying to Register \"%s\" twice!", aFunctionName.c_str());
			return false;
		}
	}

	myRegisteredFunctions.Add({ aFunctionName, aFunction, aHelpText, aExposedType });
	for (auto it = myLuaScripts.begin(); it != myLuaScripts.end(); ++it)
	{
		lua_register(it->myLuaState, aFunctionName.c_str(), aFunction);
	}

	myHelpTexts[aFunctionName] = std::make_tuple(aHelpText, true);
	return true;
}

ID_T(CLuaScript) CScriptManager::AquireLuaScript(CScriptComponent* aScriptComponent, const char* aFilePath)
{
	if (aFilePath == nullptr)
	{
		SCRIPT_LOG(CONCOL_ERROR, "Filepath nullptr returning Invalid CLuaScript");
		return ID_T_INVALID(CLuaScript);
	}

	ID_T(CLuaScript) luaID = myLuaScripts.Acquire();
	myLuaScriptCash[aFilePath] = luaID;

	lua_State* state = luaL_newstate();
	luaL_openlibs(state);

	myLuaScripts.GetObj(luaID)->Init(aScriptComponent, ourScriptManager, state, aFilePath);

	return luaID;
}

ID_T(CLuaScript) CScriptManager::AquireLuaScriptForNonGameObjects(const char * aFilePath)
{
	if (aFilePath == nullptr)
	{
		SCRIPT_LOG(CONCOL_ERROR, "Filepath nullptr returning Invalid CLuaScript");
		return ID_T_INVALID(CLuaScript);
	}

	if (myLuaScriptCash.find(aFilePath) != myLuaScriptCash.end())
	{
		ID_T(CLuaScript) id = myLuaScriptCash[aFilePath];
		return id;
	}
	else
	{
		ID_T(CLuaScript) luaID = myLuaScripts.Acquire();
		myLuaScriptCash[aFilePath] = luaID;

		lua_State* state = luaL_newstate();
		luaL_openlibs(state);

		myLuaScripts.GetObj(luaID)->InitForNonGameObjects(ourScriptManager, state, aFilePath);

		return luaID;
	}
}

void CScriptManager::ReleaseLuaScript(ID_T(CLuaScript) aId)
{
	myLuaScripts.Release(aId);
}

bool CScriptManager::CompileScriptFile(CLuaScript* aLuaScript)
{
	std::string scriptPath = "Assets/Scripts/";
	scriptPath.append(aLuaScript->myPath);
	scriptPath.append(".lua");
	CheckLinesOfCode(scriptPath);

	if (luaL_dofile(aLuaScript->myLuaState, scriptPath.c_str()) == 0)
	{
		for (int i = 0; i < myRegisteredFunctions.Size(); ++i)
		{
			lua_register(aLuaScript->myLuaState, myRegisteredFunctions[i].myFunctionNameInLua.c_str(), myRegisteredFunctions[i].myFunction);

		}
		luaL_openlibs(aLuaScript->myLuaState);
		return true;
	}
	else
	{
		std::string error = lua_tostring(aLuaScript->myLuaState, -1);
		SCRIPT_LOG(CONCOL_ERROR, "Could not compile Script: %s.", error.c_str());
		return false;
	}
}

void CScriptManager::NotifyListeners(const CScriptEventManager::EScriptEvent aEvent, const unsigned short aTriggerID)
{
	myScriptEventManager.Notify(aEvent, aTriggerID);
}

void CScriptManager::PushArgs(lua_State* aState, int aValue)
{
	lua_pushinteger(aState, aValue);
}

void CScriptManager::PushArgs(lua_State* aState, double aValue)
{
	lua_pushnumber(aState, aValue);
}

void CScriptManager::PushArgs(lua_State* aState, bool aValue)
{
	lua_pushboolean(aState, aValue);
}

void CScriptManager::PushArgs(lua_State* aState, const char * aValue)
{
	lua_pushstring(aState, aValue);
}

void CScriptManager::PushArgs(lua_State * aState, void * aGameObject)
{
	lua_pushlightuserdata(aState, aGameObject);
}

void CScriptManager::PrintHelpFile()
{
	std::ofstream file;
	CreateDirectory(L"Assets//Scripts", NULL);
	file.open("Assets/Scripts/_ExposedScriptsFunctions.txt");

	if (file.good())
	{
		for (auto& it : myHelpTexts)
		{
			file << it.first << "(): " << std::get<0>(it.second) << std::endl;
		}
	}

	file.close();
}

void CScriptManager::ReloadFile(ID_T(CLuaScript) aLuaScript, const char* filePath)
{
	aLuaScript;
	filePath;
}

void CScriptManager::CheckLinesOfCode(const std::string & aScriptfile)
{
	std::fstream file(aScriptfile);
	size_t linesCount = 0;
	std::string line;

	while (std::getline(file, line))
	{
		linesCount++;
	}

	if (linesCount > 500)
	{
		ENGINE_LOG(CONCOL_ERROR, "File(%s) to big(%d lines), please divide it.", aScriptfile.c_str(), linesCount);
	}
}


std::string CScriptManager::FindClosestFunctionName(const char * aFunctionName)
{
	int minDistance = INT_MAX;
	std::string closestFunction = "";
	for (auto& key : myHelpTexts)
	{
		int distance = LevenshteinDistance(aFunctionName, static_cast<int>(strlen(aFunctionName)), key.first.c_str(), static_cast<int>(key.first.length()));
		if (distance < minDistance)
		{
			minDistance = distance;
			closestFunction = key.first;
		}
	}

	return closestFunction;
}


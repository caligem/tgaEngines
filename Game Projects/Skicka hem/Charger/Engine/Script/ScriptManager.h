#pragma once

#include "lua.hpp"
#include <string>
#include <functional>
#include <map>
#include "DL_Debug.h"
#include "GrowingArray.h"
#include "ObjectPool.h"
#include "LuaScript.h"
#include "ScriptEventManager.h"

class CLuaScript;
class CScriptComponent;

class CScriptManager
{
	friend CScriptComponent;
public:
	CScriptManager();
	~CScriptManager();

	bool Init(std::function<void()> aCallback);

	template<typename ...Args>
	void CallFunction(ID_T(CLuaScript) luaScriptID, const std::string& aFunctionName, Args ...aArgs);
	void RunString(ID_T(CLuaScript) aLuaScriptID, const std::string& aFunctionName);

	bool RegisterFunction(const std::string& aFunctionName, lua_CFunction aFunction, const std::string& aHelpText, const int aExposedType = 3); 	/* ExposedType: 
																																					1 = Only Lua,
																																					2 = Only Consol, 
																																					3 = Both */
	bool CompileScriptFile(CLuaScript* aLuaScript);
	void AddListenerToEvent(const CScriptEventManager::EScriptEvent aEvent, const CScriptEventManager::SLuaCallback aLuaCallback) {myScriptEventManager.AddListener(aEvent, aLuaCallback); }
	void NotifyListeners(const CScriptEventManager::EScriptEvent aEvent, const unsigned short aTriggerID);

	template<typename ...Args>
	void PushArgs(lua_State* aState, int aValue, Args... aArgs);
	void PushArgs(lua_State* aState, int aValue);

	template<typename ...Args>
	void PushArgs(lua_State* aState, double aValue, Args... aArgs);
	void PushArgs(lua_State* aState, double aValue);

	template<typename ...Args>
	void PushArgs(lua_State* aState, bool aValue, Args... aArgs);
	void PushArgs(lua_State* aState, bool aValue);

	template<typename ...Args>
	void PushArgs(lua_State* aState, const char * aValue, Args... aArgs);
	void PushArgs(lua_State* aState, const char* aValue);

	template<typename ...Args>
	void PushArgs(lua_State* aState, void* aGameObject, Args... aArgs);
	void PushArgs(lua_State* aState, void* aGameObject);


	void PushArgs(lua_State*) {}

	auto& GetRegisteredFunctions() { return myRegisteredFunctions; }
	ID_T(CLuaScript) AquireLuaScript(CScriptComponent* aScriptComponent, const char* aFilePath);
	ID_T(CLuaScript) AquireLuaScriptForNonGameObjects(const char* aFilePath);
	void ReleaseLuaScript(ID_T(CLuaScript) aId);

	struct SRegisteredGameFunction
	{
		std::string myFunctionNameInLua;
		lua_CFunction myFunction;
		std::string myHelpText;
		int myShouldBeExposedToConsol; // 1 = only Lua, 2 = Only Consol, 3 = Both
	};
private:
	void PrintHelpFile();
	void ReloadFile(ID_T(CLuaScript) aLuaScript, const char* filePath);
	void CheckLinesOfCode(const std::string& aScriptfile);
	std::string FindClosestFunctionName(const char* aFunctionName);

	CScriptEventManager myScriptEventManager;

	bool myScriptStateIsValid;
	CommonUtilities::GrowingArray<SRegisteredGameFunction, int> myRegisteredFunctions;
	ObjectPool<CLuaScript> myLuaScripts;
	std::map<std::string, ID_T(CLuaScript)> myLuaScriptCash;
	std::map<std::string, std::tuple<std::string, bool>> myHelpTexts;

	static CScriptManager* ourScriptManager;
};

template<typename ...Args>
inline void CScriptManager::CallFunction(ID_T(CLuaScript) luaScriptID, const std::string& aFunctionName, Args ...aArgs)
{
	if (luaScriptID == ID_T_INVALID(CLuaScript))
	{
		SCRIPT_LOG(CONCOL_WARNING, "ID_T_INVALID(CLuaScript)");
		return;
	}

	lua_State* state = myLuaScripts.GetObj(luaScriptID)->myLuaState;
	lua_getglobal(state, aFunctionName.c_str());

	PushArgs(state, aArgs...);
	START_TIMER(LuaFunctionTimer);

	int result = lua_pcall(state, sizeof...(Args), 0, 0);

	float timerInMS = GET_TIME(LuaFunctionTimer);
	if (timerInMS > 1)
	{
		ENGINE_LOG(CONCOL_ERROR, "LuaFunction %s is too slow %lfms.", aFunctionName.c_str(), timerInMS);
	}

	if (result != 0)
	{
		std::string error = lua_tostring(state, -1);
		std::string key = "attempt to call a nil value (global '";
		std::size_t found = error.find(key);
		if (found != std::string::npos)
		{
			std::string functionName = error.substr(found + key.length());
			functionName = functionName.substr(0, functionName.find("'"));

			std::string closestFunctionName = FindClosestFunctionName(functionName.c_str());

			ENGINE_LOG(CONCOL_ERROR, "Couldnt find function %s, did you mean %s?", functionName.c_str(), closestFunctionName.c_str());
		}
		else
		{
			ENGINE_LOG(CONCOL_ERROR, "Error running function: %s, %s. \n", aFunctionName.c_str(), error.c_str());
		}
	}
}

template<typename ...Args>
inline void CScriptManager::PushArgs(lua_State* aState, int aValue, Args ...aArgs)
{
	PushArgs(aState, aValue);
	PushArgs(aState, aArgs...);
}

template<typename ...Args>
inline void CScriptManager::PushArgs(lua_State* aState, double aValue, Args ...aArgs)
{
	PushArgs(aState, aValue);
	PushArgs(aState, aArgs...);
}

template<typename ...Args>
inline void CScriptManager::PushArgs(lua_State* aState, bool aValue, Args ...aArgs)
{
	PushArgs(aState, aValue);
	PushArgs(aState, aArgs...);
}

template<typename ...Args>
inline void CScriptManager::PushArgs(lua_State* aState, const char * aValue, Args ...aArgs)
{
	PushArgs(aState, aValue);
	PushArgs(aState, aArgs...);
}

template<typename ...Args>
inline void CScriptManager::PushArgs(lua_State * aState, void* aScriptComponentPointer, Args ...aArgs)
{
	PushArgs(aState, aValue);
	PushArgs(aState, aArgs...);
}

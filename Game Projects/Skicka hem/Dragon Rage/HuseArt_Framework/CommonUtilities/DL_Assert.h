#ifndef DL_ASSERT_HEADER
#define DL_ASSERT_HEADER

#include "DL_Debug.h"
#include <crtdefs.h>

extern "C" {
	_ACRTIMP void __cdecl _wassert(_In_z_ const wchar_t * _Message, _In_z_ const wchar_t *_File, _In_ unsigned _Line);
}

#ifdef assert
#undef assert
#endif
#define assert(expression) (void)(                                                       \
	(!!(expression)) ||                                                              \
	(_wassert(_CRT_WIDE(#expression), _CRT_WIDE(__FILE__), (unsigned)(__LINE__)), 0) \
)

#endif

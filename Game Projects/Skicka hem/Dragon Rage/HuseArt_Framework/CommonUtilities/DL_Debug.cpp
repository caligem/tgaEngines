#include "DL_Debug.h"
#include <string>
#include <stdio.h>
#include <stdarg.h>
#include "Macros.h"
#include "DL_StackWalker.h"

namespace DL_Debug
{
	Debug* Debug::ourInstance = nullptr;

	bool Debug::Create(const char* aFile)
	{
		assert(ourInstance == nullptr && "Debug object already created!");
		ourInstance = new Debug();
		if (ourInstance == nullptr)
		{
			return false;
		}
		
		fopen_s(&ourInstance->myDebugFile, aFile, "a");
		if (!ourInstance->myDebugFile)
		{
			return false;
		}

		return true;
	}
	bool Debug::Destroy()
	{
		if (ourInstance != nullptr)
		{
			fclose(ourInstance->myDebugFile);
			SAFE_DELETE(ourInstance);
			return true;
		}
		return false;
	}
	Debug* Debug::GetInstance()
	{
		return ourInstance;
	}
	void Debug::AssertMessage(const char * aFileName, const int aLine, const char * aFunctionName, const char * aString)
	{
		DL_PRINT("\n");

		BreakLine();
		DL_PRINT("================================= CALL STACK ===================================\n");
		BreakLine();
		StackWalker stackWalker;
		stackWalker.ShowCallstack();
		BreakLine();

		std::string output("ERROR in file: ");
		output += aFileName;
		output += " <";
		output += aFunctionName;
		output += ">(";
		output += std::to_string(aLine);
		output += "): ";
		output += aString;
		output += "\n";
		DL_PRINT(output.c_str());
		BreakLine();
	}
	void Debug::PrintMessage(const char* aString)
	{
		vfprintf_s(myDebugFile, aString, nullptr);

		fflush(myDebugFile);
	}
	void Debug::DebugMessage(const char * aFileName, const int aLine, const char * aFunctionName, const char * aFormattedString, ...)
	{
		BreakLine();
		std::string output(aFileName);
		output += " <";
		output += aFunctionName;
		output += ">(";
		output += std::to_string(aLine);
		output += "): ";
		DL_PRINT(output.c_str());

		va_list args;
		va_start(args, aFormattedString);
		vfprintf_s(myDebugFile, aFormattedString, args);
		vfprintf_s(myDebugFile, "\n", nullptr);
		va_end(args);

		fflush(myDebugFile);
	}
	void Debug::BreakLine()
	{
		std::string breakLine(80, '=');
		breakLine += "\n";
		DL_PRINT(breakLine.c_str());
	}
}

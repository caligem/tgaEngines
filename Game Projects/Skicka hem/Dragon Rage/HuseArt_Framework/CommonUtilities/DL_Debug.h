#pragma once

#include <fstream>
#include <string.h>
#include "DL_Assert.h"

// DL_ASSERT
#define DL_ASSERT(expression) DL_Debug::Debug::GetInstance()->AssertMessage(__FILE__, __LINE__, __FUNCTION__, #expression); assert(false);

// DL_PRINT
#define DL_PRINT(string) DL_Debug::Debug::GetInstance()->PrintMessage(string);

// DL_DEBUG
#define DL_DEBUG( ... ) DL_Debug::Debug::GetInstance()->DebugMessage(__FILE__, __LINE__, __FUNCTION__, __VA_ARGS__);

namespace DL_Debug
{
	class Debug
	{
	public:
#ifdef _DEBUG
		static bool Create(const char* aFile = "Log_Debug.txt");
#else
		static bool Create(const char* aFile = "Log_Release.txt");
#endif
		static bool Destroy();
		static Debug* GetInstance();

		void AssertMessage(const char* aFileName, const int aLine, const char* aFunctionName, const char* aString);
		void PrintMessage(const char* aString);
		void DebugMessage(const char* aFileName, const int aLine, const char* aFunctionName, const char* aFormattedString, ...);

	private:
		Debug() = default;
		~Debug() = default;
		static Debug* ourInstance;

		void BreakLine();

		FILE* myDebugFile;
	};
}
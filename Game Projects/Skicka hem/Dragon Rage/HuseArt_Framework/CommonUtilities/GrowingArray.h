#pragma once

#include <cstring>
#include <assert.h>
#include "Macros.h"

namespace CommonUtilities
{
	template<typename ObjectType, typename SizeType=unsigned short>
	class GrowingArray
	{
	public:
		GrowingArray();
		GrowingArray(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag = true);
		GrowingArray(const GrowingArray& aGrowingArray);
		GrowingArray(GrowingArray&& aGrowingArray);

		~GrowingArray();

		GrowingArray& operator=(const GrowingArray& aGrowingArray);
		GrowingArray& operator=(GrowingArray&& aGrowingArray);

		void Init(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag = true);
		void ReInit(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag = true);

		inline ObjectType& operator[](const SizeType& aIndex);
		inline const ObjectType& operator[](const SizeType& aIndex) const;

		inline void Add(const ObjectType& aObject);
		inline void Insert(SizeType aIndex, ObjectType& aObject);

		inline void DeleteCyclic(ObjectType& aObject);
		inline void RemoveCyclic(const ObjectType& aObject);
		inline void DeleteCyclicAtIndex(SizeType aItemNumber);
		inline void RemoveCyclicAtIndex(SizeType aItemNumber);

		inline void Delete(ObjectType& aObject);
		inline void Remove(const ObjectType& aObject);
		inline void DeleteAtIndex(SizeType aItemNumber);
		inline void RemoveAtIndex(SizeType aItemNumber);

		inline void DeleteAll();
		inline void RemoveAll();

		inline SizeType Find(const ObjectType& aObject);

		inline ObjectType Pop();
		inline ObjectType Shift();
		inline void RemoveFilter(bool (*condition)(ObjectType& aValue), bool aPreserveOrder=false);
		inline void DeleteFilter(bool (*condition)(const ObjectType aValue), bool aPreserveOrder=false);

		inline ObjectType& GetLast();
		inline const ObjectType& GetLast() const;

		static const SizeType FoundNone = static_cast<SizeType>(-1);

		void Optimize();
		__forceinline SizeType Size() const;
		inline void Resize(SizeType aNewSize);

		inline ObjectType* begin();
		inline const ObjectType* begin() const;
		inline ObjectType* end();
		inline const ObjectType* end() const;
		inline bool Empty();

	private:
		ObjectType* myData;
		SizeType mySize;
		SizeType myCapacity;
		bool myUseSafeMode;
	};

	template<typename ObjectType, typename SizeType>
	CommonUtilities::GrowingArray<ObjectType, SizeType>::GrowingArray()
	{
		myData = nullptr;
		myCapacity = static_cast<SizeType>(0);
		mySize = static_cast<SizeType>(0);
		myUseSafeMode = true;
	}

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<ObjectType, SizeType>::GrowingArray(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag)
	{
		myData = new ObjectType[aNrOfRecommendedItems];
		myCapacity = aNrOfRecommendedItems;
		mySize = static_cast<SizeType>(0);
		myUseSafeMode = aUseSafeModeFlag;
	}

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<ObjectType, SizeType>::GrowingArray(const GrowingArray & aGrowingArray)
	{
		this->operator=(aGrowingArray);
	}

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<ObjectType, SizeType>::GrowingArray(GrowingArray && aGrowingArray)
	{
		this->operator=(aGrowingArray);
	}

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<ObjectType, SizeType>::~GrowingArray()
	{
		if (myData != nullptr)
		{
			delete[] myData;
			myData = nullptr;
		}
		mySize = static_cast<SizeType>(0);
		myCapacity = static_cast<SizeType>(0);
		myUseSafeMode = true;
	}

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<ObjectType, SizeType>& GrowingArray<ObjectType, SizeType>::operator=(const GrowingArray& aGrowingArray)
	{
		if (myCapacity < aGrowingArray.myCapacity)
		{
			delete[] myData;
			myData = new ObjectType[aGrowingArray.myCapacity];
			myCapacity = aGrowingArray.myCapacity;
		}
		mySize = aGrowingArray.mySize;
		myUseSafeMode = aGrowingArray.myUseSafeMode;
		if (myUseSafeMode)
		{
			for (SizeType i = static_cast<SizeType>(0); i < mySize; ++i)
			{
				myData[i] = aGrowingArray.myData[i];
			}
		}
		else
		{
			memcpy(myData, aGrowingArray.myData, sizeof(ObjectType)*mySize);
		}
		return *this;
	}

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<ObjectType, SizeType>& GrowingArray<ObjectType, SizeType>::operator=(GrowingArray && aGrowingArray)
	{
		myData = aGrowingArray.myData;
		aGrowingArray.myData = nullptr;
		mySize = aGrowingArray.mySize;
		myCapacity = aGrowingArray.myCapacity;
		myUseSafeMode = aGrowingArray.myUseSafeMode;
		return *this;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::Init(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag)
	{
		if (myData != nullptr)
		{
			ReInit(aNrOfRecommendedItems, aUseSafeModeFlag);
		}
		else
		{
			myData = new ObjectType[aNrOfRecommendedItems];
			myCapacity = aNrOfRecommendedItems;
			mySize = static_cast<SizeType>(0);
			myUseSafeMode = aUseSafeModeFlag;
		}
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::ReInit(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag)
	{
		if (myData != nullptr)
		{
			delete[] myData;
		}
		myData = new ObjectType[aNrOfRecommendedItems];
		myCapacity = aNrOfRecommendedItems;
		mySize = static_cast<SizeType>(0);
		myUseSafeMode = aUseSafeModeFlag;
	}

	template<typename ObjectType, typename SizeType>
	inline ObjectType & GrowingArray<ObjectType, SizeType>::operator[](const SizeType & aIndex)
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		assert(aIndex >= static_cast<SizeType>(0) && aIndex < mySize && "Subscript index out of bounds");
		return myData[aIndex];
	}

	template<typename ObjectType, typename SizeType>
	inline const ObjectType & GrowingArray<ObjectType, SizeType>::operator[](const SizeType & aIndex) const
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		assert(aIndex >= static_cast<SizeType>(0) && aIndex < mySize && "Subscript index out of bounds");
		return myData[aIndex];
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::Add(const ObjectType & aObject)
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		if (mySize == myCapacity)
		{
			//Double the size of the array
			Resize(myCapacity << 1);
		}
		myData[mySize] = aObject;
		++mySize;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::Insert(SizeType aIndex, ObjectType & aObject)
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		assert(aIndex >= static_cast<SizeType>(0) && aIndex <= mySize && "Inserting out of bounds");
		if (mySize == myCapacity)
		{
			//Double the size of the array
			Resize(myCapacity << 1);
		}
		for (SizeType i = mySize; i > aIndex; --i)
		{
			myData[i] = myData[i - 1];
		}
		myData[aIndex] = aObject;
		++mySize;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::DeleteCyclic(ObjectType & aObject)
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		for (SizeType i = mySize; i > static_cast<SizeType>(0); --i)
		{
			if (myData[i - 1] == aObject)
			{
				SAFE_DELETE(myData[i - 1]);
				myData[i - 1] = myData[mySize - 1];
				--mySize;
				return;
			}
		}
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::RemoveCyclic(const ObjectType & aObject)
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		for (SizeType i = mySize; i > static_cast<SizeType>(0); --i)
		{
			if (myData[i - 1] == aObject)
			{
				myData[i - 1] = myData[mySize - 1];
				--mySize;
				return;
			}
		}
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::DeleteCyclicAtIndex(SizeType aItemNumber)
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		assert(aItemNumber >= static_cast<SizeType>(0) && aItemNumber < mySize && "Index out of bounds");
		SAFE_DELETE(myData[aItemNumber]);
		myData[aItemNumber] = myData[mySize - 1];
		--mySize;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::RemoveCyclicAtIndex(SizeType aItemNumber)
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		assert(aItemNumber >= static_cast<SizeType>(0) && aItemNumber < mySize && "Index out of bounds");
		myData[aItemNumber] = myData[mySize - 1];
		--mySize;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::Delete(ObjectType& aObject)
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		for (SizeType i = Size(); i > static_cast<SizeType>(0); --i)
		{
			if (myData[i - 1] == aObject)
			{
				SAFE_DELETE(myData[i - 1]);
				for (SizeType j = i; j < Size(); ++j)
				{
					myData[j - 1] = myData[j];
				}
				--mySize;
				return;
			}
		}
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::Remove(const ObjectType & aObject)
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		for (SizeType i = Size(); i > static_cast<SizeType>(0); --i)
		{
			if (myData[i - 1] == aObject)
			{
				for (SizeType j = i; j < Size(); ++j)
				{
					myData[j - 1] = myData[j];
				}
				--mySize;
				return;
			}
		}
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::DeleteAtIndex(SizeType aItemNumber)
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		assert(aItemNumber >= static_cast<SizeType>(0) && aItemNumber < mySize && "Index out of bounds");
		SAFE_DELETE(myData[aItemNumber]);
		for (SizeType i = aItemNumber; i < Size()-1; ++i)
		{
			myData[i] = myData[i + 1];
		}
		--mySize;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::RemoveAtIndex(SizeType aItemNumber)
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		assert(aItemNumber >= static_cast<SizeType>(0) && aItemNumber < mySize && "Index out of bounds");
		for (SizeType i = aItemNumber; i < Size()-1; ++i)
		{
			myData[i] = myData[i + 1];
		}
		--mySize;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::DeleteAll()
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		for (SizeType i = mySize; i > static_cast<SizeType>(0); --i)
		{
			SAFE_DELETE(myData[i-1]);
		}
		mySize = static_cast<SizeType>(0);
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::RemoveAll()
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		mySize = static_cast<SizeType>(0);
	}

	template<typename ObjectType, typename SizeType>
	inline SizeType GrowingArray<ObjectType, SizeType>::Find(const ObjectType & aObject)
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		for (SizeType i = static_cast<SizeType>(0); i < mySize; ++i)
		{
			if (myData[i] == aObject)
			{
				return i;
			}
		}
		return FoundNone;
	}

	template<typename ObjectType, typename SizeType>
	inline ObjectType GrowingArray<ObjectType, SizeType>::Pop()
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		assert(mySize > static_cast<SizeType>(0) && "Array is empty");
		mySize--;
		return myData[mySize];
	}

	template<typename ObjectType, typename SizeType>
	inline ObjectType GrowingArray<ObjectType, SizeType>::Shift()
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		assert(mySize > static_cast<SizeType>(0) && "Array is empty");
		ObjectType objectCopy = myData[0];
		for (unsigned i = 0; i < mySize - 1; i++)
		{
			myData[i] = myData[i + 1];
		}
		mySize--;
		return objectCopy;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::RemoveFilter(bool(*condition)(ObjectType& aValue), bool aPreserveOrder)
	{
		if (aPreserveOrder)
		{
			for (SizeType i = Size(); i > 0; --i)
			{
				if (condition(myData[i-1]))
				{
					RemoveAtIndex(i-1);
				}
			}
		}
		else
		{
			for (SizeType i = Size(); i > 0; --i)
			{
				if (condition(myData[i-1]))
				{
					RemoveCyclicAtIndex(i-1);
				}
			}
		}
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::DeleteFilter(bool(*condition)(const ObjectType aValue), bool aPreserveOrder)
	{
		if (aPreserveOrder)
		{
			for (SizeType i = Size(); i > 0; --i)
			{
				if (condition(myData[i-1]))
				{
					DeleteAtIndex(i-1);
				}
			}
		}
		else
		{
			for (SizeType i = Size(); i > 0; --i)
			{
				if (condition(myData[i-1]))
				{
					DeleteCyclicAtIndex(i-1);
				}
			}
		}
	}

	template<typename ObjectType, typename SizeType>
	inline ObjectType & GrowingArray<ObjectType, SizeType>::GetLast()
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		assert(mySize > static_cast<SizeType>(0) && "Array is empty");
		return myData[mySize - 1];
	}

	template<typename ObjectType, typename SizeType>
	inline const ObjectType & GrowingArray<ObjectType, SizeType>::GetLast() const
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		assert(mySize > static_cast<SizeType>(0) && "Array is empty");
		return myData[mySize - 1];
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::Optimize()
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		if (mySize < myCapacity)
		{
			ObjectType* newData = new ObjectType[mySize];
			if (myUseSafeMode)
			{
				for (SizeType i = static_cast<SizeType>(0); i < mySize; ++i)
				{
					newData[i] = myData[i];
				}
			}
			else
			{
				memcpy(newData, myData, sizeof(ObjectType)*mySize);
			}
			delete[] myData;
			myData = newData;
			myCapacity = mySize;
		}
	}

	template<typename ObjectType, typename SizeType>
	inline SizeType GrowingArray<ObjectType, SizeType>::Size() const
	{
		return mySize;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::Resize(SizeType aNewSize)
	{
		if (aNewSize > myCapacity)
		{
			ObjectType* newData = new ObjectType[aNewSize];
			if (myUseSafeMode)
			{
				for (SizeType i = static_cast<SizeType>(0); i < mySize; ++i)
				{
					newData[i] = myData[i];
				}
			}
			else
			{
				memcpy(newData, myData, sizeof(ObjectType)*mySize);
			}

			delete[] myData;
			myData = newData;
			myCapacity = aNewSize;
		}
		else
		{
			mySize = aNewSize;
		}
	}

	template<typename ObjectType, typename SizeType>
	inline ObjectType* GrowingArray<ObjectType, SizeType>::begin()
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		return &myData[0];
	}

	template<typename ObjectType, typename SizeType>
	inline const ObjectType * GrowingArray<ObjectType, SizeType>::begin() const
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		return &myData[0];
	}

	template<typename ObjectType, typename SizeType>
	inline ObjectType * GrowingArray<ObjectType, SizeType>::end()
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		return &myData[mySize];
	}

	template<typename ObjectType, typename SizeType>
	inline const ObjectType * GrowingArray<ObjectType, SizeType>::end() const
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		return &myData[mySize];
	}

	template<typename ObjectType, typename SizeType>
	inline bool GrowingArray<ObjectType, SizeType>::Empty()
	{
		assert(myData != nullptr && "GrowingArray is not initialised yet.");
		return mySize == static_cast<SizeType>(0);
	}

}

#include "InputManager.h"

#include <Windows.h>
#include <windowsx.h>

namespace CommonUtilities
{
	InputManager::InputManager()
	{
		for( bool& b : myCurrentState.myKeyboard.myKeys )
		{
			b = false;
		}
		for( bool& b : myCurrentState.myMouse.myButtons )
		{
			b = false;
		}
	}

	InputManager::~InputManager()
	{
	}

	void InputManager::Update()
	{
		myPreviousState = myCurrentState;

		myCurrentState.myMouse.myDeltaX = myCurrentState.myMouse.myX - myPreviousState.myMouse.myX;
		myCurrentState.myMouse.myDeltaY = myCurrentState.myMouse.myY - myPreviousState.myMouse.myY;

		myCurrentState.myMouse.myScroll = 0;
	}

	void InputManager::OnInput(unsigned int aMessage, unsigned __int64 wParam, __int64 lParam)
	{
		INPUT input;
		switch( aMessage )
		{
		case WM_KEYDOWN:
			input.type = INPUT_KEYBOARD;
			input.ki = KEYBDINPUT();
			input.ki.wVk = static_cast<WORD>(wParam);

			UpdateInput(input);
			break;
		case WM_KEYUP:
			input.type = INPUT_KEYBOARD;
			input.ki = KEYBDINPUT();
			input.ki.wVk = static_cast<WORD>(wParam);
			input.ki.dwFlags = KEYEVENTF_KEYUP;
			
			UpdateInput(input);
			break;
		case WM_LBUTTONDOWN: case WM_RBUTTONDOWN: case WM_MBUTTONDOWN:
			input.type = INPUT_MOUSE;
			input.mi = MOUSEINPUT();
			input.mi.mouseData = static_cast<DWORD>(wParam);
			input.mi.dwFlags = 0xDEAD;

			UpdateInput(input);
			break;
		case WM_LBUTTONUP: case WM_RBUTTONUP: case WM_MBUTTONUP:
			input.type = INPUT_MOUSE;
			input.mi = MOUSEINPUT();
			input.mi.mouseData = static_cast<DWORD>(wParam);
			input.mi.dwFlags = 0xFADE;

			UpdateInput(input);
			break;
		case WM_MOUSEMOVE:
			input.type = INPUT_MOUSE;
			input.mi = MOUSEINPUT();
			input.mi.dwFlags = MOUSEEVENTF_MOVE;
			input.mi.dx = GET_X_LPARAM(lParam);
			input.mi.dy = GET_Y_LPARAM(lParam);

			UpdateInput(input);
			break;
		case WM_MOUSEWHEEL:
			input.type = INPUT_MOUSE;
			input.mi = MOUSEINPUT();
			input.mi.mouseData = GET_WHEEL_DELTA_WPARAM(wParam);
			input.mi.dwFlags = MOUSEEVENTF_WHEEL;

			UpdateInput(input);
			break;
		}
	}

	void InputManager::UpdateInput(INPUT aInput)
	{
		if (aInput.type == INPUT_KEYBOARD)
		{
			HandleKeyboardInput(aInput);
		}
		else if (aInput.type == INPUT_MOUSE)
		{
			HandleMouseInput(aInput);
		}
	}

	bool InputManager::IsKeyDown(Key aKey) const
	{
		if (aKey < 0 || aKey >= static_cast<int>(myCurrentState.myKeyboard.myKeys.size()))
		{
			return false;
		}
		return myCurrentState.myKeyboard.myKeys[aKey];
	}

	bool InputManager::IsButtonDown(Button aButton) const
	{
		if (aButton < 0 || aButton >= static_cast<int>(myCurrentState.myMouse.myButtons.size()))
		{
			return false;
		}
		return myCurrentState.myMouse.myButtons[aButton];
	}

	bool InputManager::IsKeyPressed(Key aKey) const
	{
		if (aKey < 0 || aKey >= static_cast<int>(myCurrentState.myKeyboard.myKeys.size()))
		{
			return false;
		}
		return myCurrentState.myKeyboard.myKeys[aKey] && !myPreviousState.myKeyboard.myKeys[aKey];
	}

	bool InputManager::IsButtonPressed(Button aButton) const
	{
		if (aButton < 0 || aButton >= static_cast<int>(myCurrentState.myMouse.myButtons.size()))
		{
			return false;
		}
		return myCurrentState.myMouse.myButtons[aButton] && !myPreviousState.myMouse.myButtons[aButton];
	}

	bool InputManager::IsKeyReleased(Key aKey) const
	{
		if (aKey < 0 || aKey >= static_cast<int>(myCurrentState.myKeyboard.myKeys.size()))
		{
			return false;
		}
		return !myCurrentState.myKeyboard.myKeys[aKey] && myPreviousState.myKeyboard.myKeys[aKey];
	}

	bool InputManager::IsButtonReleased(Button aButton) const
	{
		if (aButton < 0 || aButton >= static_cast<int>(myCurrentState.myMouse.myButtons.size()))
		{
			return false;
		}
		return !myCurrentState.myMouse.myButtons[aButton] && myPreviousState.myMouse.myButtons[aButton];
	}

	float InputManager::GetScroll() const
	{
		return static_cast<float>(myCurrentState.myMouse.myScroll) / WHEEL_DELTA;
	}

	Vector2f InputManager::GetMouseMovement() const
	{
		Vector2f mousePosition;
		mousePosition.x = static_cast<float>(myCurrentState.myMouse.myDeltaX);
		mousePosition.y = static_cast<float>(myCurrentState.myMouse.myDeltaY);
		return mousePosition;
	}

	Vector2f InputManager::GetMousePosition() const
	{
		Vector2f mousePosition;
		mousePosition.x = static_cast<float>(myCurrentState.myMouse.myX);
		mousePosition.y = static_cast<float>(myCurrentState.myMouse.myY);
		return mousePosition;
	}

	void InputManager::SetMousePosition(int aX, int aY)
	{
		RECT windowRectangle;
		GetWindowRect(GetForegroundWindow(), &windowRectangle);

		int border = GetSystemMetrics(SM_CXSIZEFRAME)*2;
		int titlebar = GetSystemMetrics(SM_CYCAPTION);
		int menubar = GetSystemMetrics(SM_CYMENU);

		SetCursorPos(
			windowRectangle.left + border						+ aX + 2,
			windowRectangle.top + border + titlebar + menubar	+ aY
		);

		myCurrentState.myMouse.myX = aX;
		myCurrentState.myMouse.myY = aY;
	}

	bool InputManager::HasMouseMoved() const
	{
		if (myCurrentState.myMouse.myDeltaX != 0)
		{
			return true;
		}
		if (myCurrentState.myMouse.myDeltaY != 0)
		{
			return true;
		}
		return false;
	}

	bool InputManager::HasScrolled() const
	{
		return (static_cast<float>(myCurrentState.myMouse.myScroll) / WHEEL_DELTA) != 0.f;
	}

	void InputManager::HideCursor() const
	{
		ShowCursor(FALSE);
	}

	void InputManager::DisplayCursor() const
	{
		ShowCursor(TRUE);
	}

	void InputManager::HandleMouseInput(INPUT aInput)
	{
		MOUSEINPUT mi = aInput.mi;

		if (mi.dwFlags == MOUSEEVENTF_MOVE)
		{
			myCurrentState.myMouse.myDeltaX += mi.dx - myCurrentState.myMouse.myX;
			myCurrentState.myMouse.myDeltaY += mi.dy - myCurrentState.myMouse.myY;
			myCurrentState.myMouse.myX = mi.dx;
			myCurrentState.myMouse.myY = mi.dy;
		}
		if (mi.dwFlags == MOUSEEVENTF_WHEEL)
		{
			myCurrentState.myMouse.myScroll = mi.mouseData;
		}
		//Mouse Button Down
		if (mi.dwFlags == 0xDEAD)
		{
			if ((mi.mouseData & 1) && !myCurrentState.myMouse.myButtons[Button_Left])
			{
				myCurrentState.myMouse.myButtons[Button_Left] = true;
			}
			if ((mi.mouseData & 2) && !myCurrentState.myMouse.myButtons[Button_Right])
			{
				myCurrentState.myMouse.myButtons[Button_Right] = true;
			}
			if ((mi.mouseData & 16) && !myCurrentState.myMouse.myButtons[Button_Middle])
			{
				myCurrentState.myMouse.myButtons[Button_Middle] = true;
			}
		}
		//Mouse Button Up
		else if (mi.dwFlags == 0xFADE)
		{
			if (!(mi.mouseData & 1) && myCurrentState.myMouse.myButtons[Button_Left])
			{
				myCurrentState.myMouse.myButtons[Button_Left] = false;
			}
			if (!(mi.mouseData & 2) && myCurrentState.myMouse.myButtons[Button_Right])
			{
				myCurrentState.myMouse.myButtons[Button_Right] = false;
			}
			if (!(mi.mouseData & 16) && myCurrentState.myMouse.myButtons[Button_Middle])
			{
				myCurrentState.myMouse.myButtons[Button_Middle] = false;
			}
		}
	}

	void InputManager::HandleKeyboardInput(INPUT aInput)
	{
		KEYBDINPUT ki = aInput.ki;

		if (ki.wVk < 0 || ki.wVk >= myCurrentState.myKeyboard.myKeys.size())
		{
			return;
		}
		if (ki.dwFlags == KEYEVENTF_KEYUP)
		{
			myCurrentState.myKeyboard.myKeys[ki.wVk] = false;
		}
		else
		{
			myCurrentState.myKeyboard.myKeys[ki.wVk] = true;
		}
	}

}

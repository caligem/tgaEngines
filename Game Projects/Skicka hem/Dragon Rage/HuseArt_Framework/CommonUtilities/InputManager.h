#pragma once

#include <array>
#include "Vector.h"

struct tagINPUT;
typedef tagINPUT INPUT;

namespace CommonUtilities
{
	enum Key {
		Key_Backspace = 0x08,
		Key_Tab = 0x09,
		Key_Return = 0x0D,
		Key_Shift = 0x10,
		Key_Ctrl = 0x11,
		Key_Alt = 0x12,
		Key_Escape = 0x1B,
		Key_Space = 0x20,
		Key_Left = 0x25,
		Key_Up = 0x26,
		Key_Right = 0x27,
		Key_Down = 0x28,
		Key_Delete = 0x2E,
		Key_0 = 0x30,
		Key_1 = 0x31,
		Key_2 = 0x32,
		Key_3 = 0x33,
		Key_4 = 0x34,
		Key_5 = 0x35,
		Key_6 = 0x36,
		Key_7 = 0x37,
		Key_8 = 0x38,
		Key_9 = 0x39,
		Key_A = 0x41,
		Key_B = 0x42,
		Key_C = 0x43,
		Key_D = 0x44,
		Key_E = 0x45,
		Key_F = 0x46,
		Key_G = 0x47,
		Key_H = 0x48,
		Key_I = 0x49,
		Key_J = 0x4A,
		Key_K = 0x4B,
		Key_L = 0x4C,
		Key_M = 0x4D,
		Key_N = 0x4E,
		Key_O = 0x4F,
		Key_P = 0x50,
		Key_Q = 0x51,
		Key_R = 0x52,
		Key_S = 0x53,
		Key_T = 0x54,
		Key_U = 0x55,
		Key_V = 0x56,
		Key_W = 0x57,
		Key_X = 0x58,
		Key_Y = 0x59,
		Key_Z = 0x5A,
		Key_Num0 = 0x60,
		Key_Num1 = 0x61,
		Key_Num2 = 0x62,
		Key_Num3 = 0x63,
		Key_Num4 = 0x64,
		Key_Num5 = 0x65,
		Key_Num6 = 0x66,
		Key_Num7 = 0x67,
		Key_Num8 = 0x68,
		Key_Num9 = 0x69,
		Key_F1 = 0x70,
		Key_F2 = 0x71,
		Key_F3 = 0x72,
		Key_F4 = 0x73,
		Key_F5 = 0x74,
		Key_F6 = 0x75,
		Key_F7 = 0x76,
		Key_F8 = 0x77,
		Key_F9 = 0x78,
		Key_F10 = 0x79,
		Key_F11 = 0x7A,
		Key_OEM_5 = 0xDC
	};
	enum Button {
		Button_Left = 0,
		Button_Right = 1,
		Button_Middle = 2
	};

	struct Mouse {
		int myX;
		int myY;
		int myDeltaX;
		int myDeltaY;
		int myScroll = 0;
		std::array<bool, 3> myButtons = { false };
	};
	struct Keyboard {
		std::array<bool, 256> myKeys = { false };
	};
	struct InputState {
		Mouse myMouse;
		Keyboard myKeyboard;
	};

	class InputManager
	{
	public:
		InputManager();
		~InputManager();

		void Update();

		void OnInput(unsigned int aMessage, unsigned __int64 wParam, __int64 lParam);
		void UpdateInput(INPUT aInput);

		bool IsKeyDown(Key aKey) const;
		bool IsButtonDown(Button aButton) const;
		bool IsKeyPressed(Key aKey) const;
		bool IsButtonPressed(Button aButton) const;
		bool IsKeyReleased(Key aKey) const;
		bool IsButtonReleased(Button aButton) const;
		float GetScroll() const;
		Vector2f GetMouseMovement() const;
		Vector2f GetMousePosition() const;
		bool HasMouseMoved() const;
		bool HasScrolled() const;

		void HideCursor() const;
		void DisplayCursor() const;

		void SetMousePosition(int aX, int aY);

	private:
		void HandleMouseInput(INPUT aInput);
		void HandleKeyboardInput(INPUT aInput);

		InputState myCurrentState;
		InputState myPreviousState;
	};
}
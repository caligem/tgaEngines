#pragma once

#define MIN(A, B) (((A)<(B))?(A):(B))
#define MAX(A, B) (((A)>(B))?(A):(B))
#define SAFE_DELETE(P) {delete P; P = nullptr;}
#define CYCLIC_ERASE(VEC, I) {VEC[(I)] = VEC[VEC.size()-1]; VEC.pop_back();}

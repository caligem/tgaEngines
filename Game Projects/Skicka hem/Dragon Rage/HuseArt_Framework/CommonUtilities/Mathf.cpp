#include "Mathf.h"

namespace CommonUtilities
{
	int Clamp(int aNumber, int aMin, int aMax)
	{
		if( aNumber < aMin )
			return aMin;
		if( aNumber > aMax )
			return aMax;
		return aNumber;
	}
	float Clamp(float aNumber, float aMin, float aMax)
	{
		if( aNumber < aMin )
			return aMin;
		if( aNumber > aMax )
			return aMax;
		return aNumber;
	}
	float Lerp(float aStart, float aEnd, float aTime)
	{
		return aStart + (aEnd-aStart)*aTime;
	}
}
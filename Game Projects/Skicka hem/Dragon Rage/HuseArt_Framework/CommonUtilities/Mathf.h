#pragma once

namespace CommonUtilities
{
	int Clamp(int aNumber, int aMin, int aMax);
	float Clamp(float aNumber, float aMin, float aMax);

	float Lerp(float aStart, float aEnd, float aTime);
}

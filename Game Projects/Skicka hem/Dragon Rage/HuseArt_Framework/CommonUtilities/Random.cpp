#include "Random.h"
#include <ctime>

void CommonUtilities::InitRand()
{
	srand(static_cast<unsigned int>(time(0)));
}

void CommonUtilities::InitRand(unsigned int seed)
{
	srand(seed);
}

float CommonUtilities::Random()
{
	return static_cast<float>(rand())/RAND_MAX;
}

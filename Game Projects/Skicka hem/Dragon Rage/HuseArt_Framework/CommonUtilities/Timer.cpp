#include "Timer.h"

using namespace CommonUtilities;

Timer::Timer()
{
	myStartTime = GetCurrentTime();
	myCurrentTime = myStartTime;
	myPreviousTime = myStartTime;
	myTotalTime = myCurrentTime - myPreviousTime;
}

Timer::~Timer() {}

void Timer::Update()
{
	myTotalTime = GetCurrentTime() - myStartTime;

	myPreviousTime = myCurrentTime;
	myCurrentTime = GetCurrentTime();
}

float Timer::GetDeltaTime() const
{
	return static_cast<float>(myCurrentTime - myPreviousTime);
}

double Timer::GetTotalTime() const
{
	return myTotalTime;
}
double Timer::GetCurrentTime()
{
	return static_cast<double>(
		std::chrono::high_resolution_clock::now().time_since_epoch().count()
	) / std::chrono::high_resolution_clock::period::den;
}

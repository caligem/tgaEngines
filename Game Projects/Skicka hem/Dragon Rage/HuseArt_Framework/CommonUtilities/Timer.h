#pragma once

#include <chrono>

namespace CommonUtilities
{
	class Timer
	{
	public:
		Timer();
		Timer(const Timer& aTimer) = delete;
		Timer& operator=(const Timer& aTimer) = delete;
		~Timer();

		void Update();

		float GetDeltaTime() const;
		double GetTotalTime() const;

	private:
		double GetCurrentTime();

		double myTotalTime;
		double myStartTime;
		double myCurrentTime;
		double myPreviousTime;

	};
}

#include "stdafx.h"
#include "EditorAnimation.h"

#include <tga2d\engine.h>
#include <tga2d\sprite\sprite.h>
#include <tga2d\primitives\line_primitive.h>

#include <Macros.h>
#include <Camera.h>

EditorAnimation::EditorAnimation()
{}
EditorAnimation::EditorAnimation(const char * aPath, const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector2f & aScale, float aRotation)
{
	Init(aPath, aPosition, aScale, aRotation);
}
EditorAnimation::~EditorAnimation()
{
	if( myLine != nullptr )
	{
		SAFE_DELETE(myLine);
	}
}

void EditorAnimation::Init(const char * aPath, const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector2f & aScale, float aRotation)
{
	Animation::Init(aPath, aPosition, aScale, aRotation);
	myPath = aPath;

	myLine = new Tga2D::CLinePrimitive();
}

void EditorAnimation::Render()
{
	if( myPoint.z < 0.f )
	{
		return;
	}

	if( myIsOutlined )
	{
		DrawLine(
			{ -myScaledSize.x / 2.f, -myScaledSize.y / 2.f },
			{ myScaledSize.x / 2.f, -myScaledSize.y / 2.f }
		);
		DrawLine(
			{ myScaledSize.x / 2.f, -myScaledSize.y / 2.f },
			{ myScaledSize.x / 2.f, myScaledSize.y / 2.f }
		);
		DrawLine(
			{ myScaledSize.x / 2.f, myScaledSize.y / 2.f },
			{ -myScaledSize.x / 2.f, myScaledSize.y / 2.f }
		);
		DrawLine(
			{ -myScaledSize.x / 2.f, myScaledSize.y / 2.f },
			{ -myScaledSize.x / 2.f, -myScaledSize.y / 2.f }
		);
		for( int i = myOffset; i < myFrames+myOffset; ++i )
		{
			int x = i;
			int y = 0;
			while( x - myCols >= 0 )
			{
				x -= myCols;
				++y;
			}

			DrawLine({
				-myScaledSize.x / 2.f + (x+0)*(myScaledSize.x / myCols),
				-myScaledSize.y / 2.f + (y+0)*(myScaledSize.y / myRows)
			}, {
				-myScaledSize.x / 2.f + (x+1)*(myScaledSize.x / myCols),
				-myScaledSize.y / 2.f + (y+0)*(myScaledSize.y / myRows)
			});

			DrawLine({
				-myScaledSize.x / 2.f + (x+1)*(myScaledSize.x / myCols),
				-myScaledSize.y / 2.f + (y+0)*(myScaledSize.y / myRows)
			}, {
				-myScaledSize.x / 2.f + (x+1)*(myScaledSize.x / myCols),
				-myScaledSize.y / 2.f + (y+1)*(myScaledSize.y / myRows)
			});
			DrawLine({
				-myScaledSize.x / 2.f + (x+1)*(myScaledSize.x / myCols),
				-myScaledSize.y / 2.f + (y+1)*(myScaledSize.y / myRows)
			}, {
				-myScaledSize.x / 2.f + (x+0)*(myScaledSize.x / myCols),
				-myScaledSize.y / 2.f + (y+1)*(myScaledSize.y / myRows)
			});
			DrawLine({
				-myScaledSize.x / 2.f + (x+0)*(myScaledSize.x / myCols),
				-myScaledSize.y / 2.f + (y+1)*(myScaledSize.y / myRows)
			}, {
				-myScaledSize.x / 2.f + (x+0)*(myScaledSize.x / myCols),
				-myScaledSize.y / 2.f + (y+0)*(myScaledSize.y / myRows)
			});
		}

		mySprite->SetUVOffset({ 0.f, 0.f });
		mySprite->SetUVScale({ 1.f, 1.f });
		Sprite::Render();

		Setup(
			static_cast<unsigned short>(myRows),
			static_cast<unsigned short>(myCols),
			static_cast<unsigned short>(myFrames),
			static_cast<unsigned short>(myOffset)
		);

		myIsOutlined = false;
	}
	else
	{
		Animation::Render();
	}
}

void EditorAnimation::ChangeValues(int aDeltaRows, int aDeltaCols, int aDeltaFrames, int aDeltaOffset)
{
	Setup(myRows + aDeltaRows, myCols + aDeltaCols, myFrames + aDeltaFrames, myOffset + aDeltaOffset);
}

void EditorAnimation::DrawLine(const CommonUtilities::Vector2f & aFrom, const CommonUtilities::Vector2f & aTo)
{
	CommonUtilities::Vector3f from = { aFrom.x*Tga2D::CEngine::GetInstance()->GetWindowRatio(), aFrom.y, 1.f };
	CommonUtilities::Vector3f to = { aTo.x*Tga2D::CEngine::GetInstance()->GetWindowRatio(), aTo.y, 1.f };

	from *= CommonUtilities::Matrix33f::CreateRotateAroundZ(myRotation);
	to *= CommonUtilities::Matrix33f::CreateRotateAroundZ(myRotation);

	myLine->SetFrom(myPoint.x + from.x/Tga2D::CEngine::GetInstance()->GetWindowRatio(), myPoint.y + from.y);
	myLine->SetTo(myPoint.x + to.x/Tga2D::CEngine::GetInstance()->GetWindowRatio(), myPoint.y + to.y);
	myLine->Render();
}

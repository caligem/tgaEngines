#pragma once

#include <string>

namespace Tga2D
{
	class CLinePrimitive;
}

#include <Vector.h>
#include <Animation.h>

class EditorAnimation : public Animation
{
public:
	EditorAnimation();
	EditorAnimation(const char* aPath, const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f }, const CommonUtilities::Vector2f& aScale = { 1.f, 1.f }, float aRotation = 0.f);
	~EditorAnimation();

	void Init(const char* aPath, const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f }, const CommonUtilities::Vector2f& aScale = { 1.f, 1.f }, float aRotation = 0.f);
	void Render();

	void ChangeValues(int aDeltaRows = 0, int aDeltaCols = 0, int aDeltaFrames = 0, int aDeltaOffset = 0);

	inline void Outline() { myIsOutlined = true; }

	inline int GetRows() const { return myRows; }
	inline int GetCols() const { return myCols; }
	inline int GetFrames() const { return myFrames; }
	inline int GetOffset() const { return myOffset; }

	inline const std::string& GetPath() const { return myPath; }

private:
	void DrawLine(const CommonUtilities::Vector2f& aFrom, const CommonUtilities::Vector2f& aTo);

	bool myIsOutlined = false;
	Tga2D::CLinePrimitive* myLine;

	std::string myPath;
};


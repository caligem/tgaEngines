#include "stdafx.h"
#include "EditorCircleCollider.h"

#include <tga2d\primitives\line_primitive.h>
#include <tga2d\math\common_math.h>

#include <Camera.h>

namespace CU = CommonUtilities;

EditorCircleCollider::EditorCircleCollider(float aRadius)
{
	myGizmo.Init("Assets/Images/Editor/GizmoCircleCollider.dds", GetPosition());
	myLine = new Tga2D::CLinePrimitive();
	myLine->myColor.Set(0.3f, 1.f, 0.3f);
	SetRadius(aRadius);
}

EditorCircleCollider::~EditorCircleCollider()
{}

void EditorCircleCollider::Render(const CommonUtilities::Camera & aCamera)
{
	myGizmo.SetPosition(GetPosition());

	if( myIsGizmoHidden )
	{
		myIsGizmoHidden = false;
	}
	else
	{
		myGizmo.Render();
	}

	if( myIsOutlined )
	{
		myLine->myColor.Set(0.f, 1.f, 1.f);
		DrawCircle(aCamera);
		myLine->myColor.Set(0.3f, 1.f, 0.3f);
		myIsOutlined = false;
	}
	else
	{
		DrawCircle(aCamera);
	}
}

void EditorCircleCollider::DrawCircle(const CommonUtilities::Camera & aCamera)
{
	float step = Tga2D::Pif / 32.f;
	for( float i = 0.f; i < Tga2D::Pif*2.f; i += step )
	{
		CU::Vector3f point1 = aCamera.PostProjectionSpace(myGizmo.GetPosition() + CU::Vector3f(
			std::cosf(i)*myRadius,
			std::sinf(i)*myRadius,
			0.f
		));
		CU::Vector3f point2 = aCamera.PostProjectionSpace(myGizmo.GetPosition() + CU::Vector3f(
			std::cosf(i+step)*myRadius,
			std::sinf(i+step)*myRadius,
			0.f
		));
		
		myLine->myFromPosition = {
			point1.x, point1.y
		};
		myLine->myToPosition = {
			point2.x, point2.y
		};
		myLine->Render();
	}
}

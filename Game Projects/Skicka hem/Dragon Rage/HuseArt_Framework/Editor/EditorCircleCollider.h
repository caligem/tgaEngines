#pragma once

namespace Tga2D
{
	class CLinePrimitive;
}

namespace CommonUtilities
{
	class Camera;
}

#include <CircleCollider.h>
#include "Sprite.h"

class EditorCircleCollider : public CircleCollider
{
public:
	EditorCircleCollider(float aRadius = 1.f);
	~EditorCircleCollider();

	void Render(const CommonUtilities::Camera& aCamera);

	inline void Outline() { myIsOutlined = true; }
	inline void HideGizmo() { myIsGizmoHidden = true; }

	Sprite myGizmo;

private:
	void DrawCircle(const CommonUtilities::Camera& aCamera);

	bool myIsOutlined = false;
	bool myIsGizmoHidden = false;

	Tga2D::CLinePrimitive* myLine;
};

#include "stdafx.h"
#include "EditorCurve.h"

#include <tga2d/engine.h>
#include <tga2d/primitives/line_primitive.h>
#include <tga2d\text\text.h>

#include <Camera.h>

#include <algorithm>

EditorCurve::EditorCurve(const CommonUtilities::Vector3f & aPosition)
{
	myPosition = aPosition;
	myGizmo.Init("Assets/Images/Editor/GizmoCurve.dds", GetPosition());

	myIndexText = new Tga2D::CText("Text/BOMBARD_.ttf", Tga2D::EFontSize_24, 1);
	myIndexText->myColor.Set(1.f, 1.f, 1.f, 1.f);

	myPoints.Init(8);
	AddPoint({ -0.5f, 0.f, 0.f });
	AddPoint({ 0.5f, 0.f, 0.f });

	myLine = new Tga2D::CLinePrimitive();
	myLine->myColor.Set(0.f, 1.f, 0.f);

	myAmount = 10;
	mySpeed = 1.f;
	myInterval = 1.f;
}

EditorCurve::~EditorCurve()
{
	for (CurvePoint& point : myPoints)
	{
		SAFE_DELETE(point.mySprite);
	}
	myPoints.RemoveAll();

	SAFE_DELETE(myLine);
}

void EditorCurve::Update(float aDeltaTime)
{
	aDeltaTime;
	for (CurvePoint& point : myPoints)
	{
		point.mySprite->SetPosition(myPosition + point.myPosition);
	}
	if (myIsDirty)
	{
		myCurve.ClearPoints();
		for (unsigned short i = 0; i < myPoints.Size(); ++i)
		{
			if(i == 0 || i == myPoints.Size()-1)
			{
				myCurve.AddPoint(myPoints[i].myPosition);
			}
			else
			{
				CommonUtilities::Vector3f middle = (myPoints[i - 1].myPosition + myPoints[i + 1].myPosition)/2.f;
				float factor = 2.f;
				factor += 0.5f * static_cast<float>(myPoints.Size() - 3);
				myCurve.AddPoint(middle+(myPoints[i].myPosition-middle)*factor);
			}
		}
		myIsDirty = false;
	}
}

void EditorCurve::Render(const CommonUtilities::Camera & aCamera)
{
	myGizmo.SetPosition(GetPosition());
	myGizmo.Render();

	int index = 0;
	for (CurvePoint& point : myPoints)
	{
		point.mySprite->Render();

		if (myIsOutlined)
		{
			++index;
			myIndexText->myText = std::to_string(index);
			myIndexText->myPosition = {
				point.mySprite->GetPoint().x,
				point.mySprite->GetPoint().y
			};
			myIndexText->Render();
		}
	}

	float step = 1.f / 128.f;
	for (float t = 0.f; t < 1.0f; t += step)
	{
		if (myIsOutlined)
		{
			myLine->myColor.Set( 0.95f, 0.75f, 0.9f );
		}
		DrawLine(
			aCamera.PostProjectionSpace(myPosition + myCurve.GetPointAbsolute(t)),
			aCamera.PostProjectionSpace(myPosition + myCurve.GetPointAbsolute(t + step))
		);
		if (myIsOutlined)
		{
			myLine->myColor.Set( 0.f, 1.f, 0.f );
		}
	}

	if (myIsOutlined)
	{
		myIsOutlined = false;
	}
}

void EditorCurve::CalculatePoint(const CommonUtilities::Camera & aCamera)
{
	myGizmo.CalculatePoint(aCamera);
	myPoint = aCamera.PostProjectionSpace(myPosition);

	for (CurvePoint& point : myPoints)
	{
		point.mySprite->CalculatePoint(aCamera);
	}
}

void EditorCurve::Move(const CommonUtilities::Vector3f& aMovement, bool aSpecial)
{
	if (mySelectedPoint != nullptr)
	{
		mySelectedPoint->myPosition += aMovement;
		myIsDirty = true;
	}
	else
	{
		myPosition += aMovement;
		if (aSpecial)
		{
			for (CurvePoint& cp : myPoints)
			{
				cp.myPosition -= aMovement;
			}
			myIsDirty = true;
		}
	}
}

void EditorCurve::Scale(const CommonUtilities::Vector2f & aScale)
{
	aScale;
}

void EditorCurve::AddPoint(const CommonUtilities::Vector3f & aPosition)
{
	myPoints.Add(CurvePoint({ new EditorSprite("Assets/Images/Editor/GizmoCurvePoint.dds"), aPosition }));
	myIsDirty = true;
}

void EditorCurve::PopPoint()
{
	if (myPoints.Empty())
	{
		return;
	}
	SAFE_DELETE(myPoints[myPoints.Size() - 1].mySprite);
	myPoints.RemoveAtIndex(myPoints.Size() - 1);
	myIsDirty = true;
}

void EditorCurve::AddPoints(const CommonUtilities::GrowingArray<CurvePoint>& aPoints)
{
	for (const CurvePoint& cp : aPoints)
	{
		myPoints.Add(CurvePoint({ new EditorSprite("Assets/Images/Editor/GizmoCurvePoint.dds"), cp.myPosition }));
	}
	myIsDirty = true;
}

void EditorCurve::FlipPoints()
{
	std::reverse(myPoints.begin(), myPoints.end());
}

void EditorCurve::MirrorPoints()
{
	for (CurvePoint& cp : myPoints)
	{
		cp.myPosition.x += (myPosition.x - cp.myPosition.x)*2.f;
	}
	myIsDirty = true;
}

void EditorCurve::SelectPoint(const CommonUtilities::Vector3f & aMousePoint)
{
	for (unsigned short i = myPoints.Size(); i > 0; --i)
	{
		CurvePoint& point = myPoints[i - 1];
		if (point.mySprite->IsInside(aMousePoint))
		{
			mySelectedPoint = &point;
			return;
		}
	}
}

void EditorCurve::ReleasePoint()
{
	mySelectedPoint = nullptr;
}

void EditorCurve::SetEnemyClass(const std::string & aEnemyClass)
{
	myClass.SetEnemyClass(aEnemyClass);
}

const std::string & EditorCurve::GetEnemyClass()
{
	return myClass.GetClassPath();
}

void EditorCurve::LoadEnemyClass()
{
	myClass.LoadEnemyClass();
}

void EditorCurve::DrawLine(const CommonUtilities::Vector2f & aFrom, const CommonUtilities::Vector2f & aTo)
{
	myLine->SetFrom(aFrom.x, aFrom.y);
	myLine->SetTo(aTo.x, aTo.y);
	myLine->Render();
}

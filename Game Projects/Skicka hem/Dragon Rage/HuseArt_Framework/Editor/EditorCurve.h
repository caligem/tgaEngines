#pragma once

#include <string>

namespace Tga2D
{
	class CLinePrimitive;
	class CText;
}

namespace CommonUtilities
{
	class Camera;
}

#include <Curve.h>
#include <GrowingArray.h>

#include <Vector.h>
#include "EditorSprite.h"
#include "EditorEnemyClass.h"

struct CurvePoint
{
	EditorSprite* mySprite;
	CommonUtilities::Vector3f myPosition;
};

class EditorCurve : public Curve
{
public:
	EditorCurve(const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f });
	~EditorCurve();

	void Update(float aDeltaTime);
	void Render(const CommonUtilities::Camera& aCamera);

	void CalculatePoint(const CommonUtilities::Camera& aCamera);

	inline void Outline() { myGizmo.Outline(); myIsOutlined = true; }
	void Move(const CommonUtilities::Vector3f& aMovement, bool aSpecial);
	void Scale(const CommonUtilities::Vector2f& aScale);
	void AddPoint(const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f });
	void PopPoint();
	void AddPoints(const CommonUtilities::GrowingArray<CurvePoint>& aPoints);
	void FlipPoints();
	void MirrorPoints();
	inline const CommonUtilities::GrowingArray<CurvePoint>& GetPoints() const { return myPoints; }

	void SelectPoint(const CommonUtilities::Vector3f& aMousePoint);
	void ReleasePoint();

	inline const CommonUtilities::GrowingArray<CurvePoint>& GetPoints() { return myPoints; }

	EditorSprite myGizmo;

	inline void SetAmount(int aAmount) { myAmount = aAmount; }
	inline void SetSpeed(float aSpeed) { mySpeed = aSpeed; }
	inline void SetInterval(float aInterval) { myInterval = aInterval; }
	inline int GetAmount() { return myAmount; }
	inline float GetSpeed() { return mySpeed; }
	inline float GetInterval() { return myInterval; }

	void SetEnemyClass(const std::string& aEnemyClass);
	const std::string& GetEnemyClass();
	void LoadEnemyClass();
	EditorEnemyClass myClass;


private:
	float mySpeed;
	float myInterval;
	int myAmount;

	void DrawLine(const CommonUtilities::Vector2f& aFrom, const CommonUtilities::Vector2f& aTo);

	bool myIsOutlined = false;
	bool myIsDirty = true;

	CommonUtilities::GrowingArray<CurvePoint> myPoints;

	Tga2D::CLinePrimitive* myLine;
	Tga2D::CText* myIndexText;

	CurvePoint* mySelectedPoint = nullptr;
};


#include "stdafx.h"
#include "EditorEnemy.h"


EditorEnemy::EditorEnemy()
{
	myGizmo.Init("Assets/Images/Editor/GizmoEnemy.dds", GetPosition());
}

EditorEnemy::~EditorEnemy()
{}

void EditorEnemy::Render()
{
	myGizmo.SetPosition(GetPosition());

	if( myIsOutlined )
	{
		myGizmo.Outline();
		myIsOutlined = false;
	}

	if( myIsGizmoHidden )
	{
		myIsGizmoHidden = false;
	}
	else
	{
		myGizmo.Render();
	}

}

void EditorEnemy::LoadEnemyClass()
{
	if (myClass.LoadEnemyClass())
	{
		myGizmo.SetTint(myClass.GetTint());
	}
}

void EditorEnemy::SetEnemyClass(const std::string& aEnemyClass)
{
	if(myClass.SetEnemyClass(aEnemyClass))
	{
		myGizmo.SetTint(myClass.GetTint());
	}
}

const std::string& EditorEnemy::GetEnemyClass()
{
	return myClass.GetClassPath();
}

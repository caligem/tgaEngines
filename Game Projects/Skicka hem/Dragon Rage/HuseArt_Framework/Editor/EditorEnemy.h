#pragma once

namespace Tga2D
{
	class LinePrimitive;
}

namespace CommonUtilities
{
	class Camera;
}

#include "EditorSprite.h"
#include "EditorEnemyClass.h"

#include <Vector.h>

class EditorEnemy
{
public:
	EditorEnemy();
	~EditorEnemy();

	void Render();

	inline void Outline() { myIsOutlined = true; }
	inline void HideGizmo() { myIsGizmoHidden = true; }

	void LoadEnemyClass();

	EditorSprite myGizmo;

	inline void SetPosition(const CommonUtilities::Vector3f& aPosition) { myPosition = aPosition; }
	inline void Move(const CommonUtilities::Vector3f& aMovement) { myPosition += aMovement; }
	inline const CommonUtilities::Vector3f& GetPosition() const { return myPosition; }
	
	void SetEnemyClass(const std::string& aEnemyClass);
	const std::string& GetEnemyClass();

	EditorEnemyClass myClass;

private:
	bool myIsOutlined = false;
	bool myIsGizmoHidden = false;


	CommonUtilities::Vector3f myPosition;
};


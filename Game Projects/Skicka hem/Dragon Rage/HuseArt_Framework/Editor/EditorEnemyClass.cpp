#include "stdafx.h"
#include "EditorEnemyClass.h"

#include <fstream>
#include <algorithm>
#include <string>

#include <Windows.h>

#include <FileDialog.h>
#include <Parser\json.hpp>

namespace CU = CommonUtilities;

EditorEnemyClass::EditorEnemyClass()
{
	SetDMG(10.f);
	SetHP(10.f);
	SetROF(1.f);
	SetProjectileSpeed(1.f);
	SetMovementSpeed(1.f);
}

EditorEnemyClass::~EditorEnemyClass()
{}

bool EditorEnemyClass::SaveEnemyClass()
{
	std::string filePath(CU::GetSaveDialogFilePath("HAF EnemyClass\0*.hafe\0"));
	std::replace(filePath.begin(), filePath.end(), '\\', '/');
	const char* search = "/Bin/Assets/";
	if (filePath.find(search) == filePath.npos)
	{
		return false;
	}

	nlohmann::json j = {
		{"tint", nlohmann::json::array({
			myTint.x,
			myTint.y,
			myTint.z,
			myTint.w
		})},
		{"hp", myHP},
		{"damage", myDMG},
		{"type", GetType()},
		{"name", GetName()},
		{"rateOfFire", GetROF()},
		{"projectileSpeed", GetProjectileSpeed()},
		{"movementSpeed", GetMovementSpeed()}
	};

	std::ofstream o(filePath.c_str());
	o << std::setw(2) << j << std::endl;

	return true;
}

bool EditorEnemyClass::LoadEnemyClass()
{
	std::string filePath(CU::GetOpenDialogFilePath("HAF EnemyClass\0*.hafe\0"));
	std::replace(filePath.begin(), filePath.end(), '\\', '/');
	const char* search = "/Bin/Assets/";
	if (filePath.find(search) == filePath.npos)
	{
		return false;
	}

	return SetEnemyClass(filePath.c_str());
}

bool EditorEnemyClass::SetEnemyClass(const std::string& aClassPath)
{
	std::ifstream i(aClassPath.c_str());
	if(i.bad() || i.fail())
	{
		return false;
	}

	myClassPath = aClassPath;
	std::replace(myClassPath.begin(), myClassPath.end(), '\\', '/');
	const char* search = "Assets/";
	if (myClassPath.find(search) == myClassPath.npos)
	{
		return false;
	}
	myClassPath = myClassPath.substr(myClassPath.find(search));

	nlohmann::json j;
	i >> j;

	if( j.find("tint") != j.end() )
	{
		SetTint( {
			j["tint"][0],
			j["tint"][1],
			j["tint"][2],
			1.f
		});
	}
	if (j.find("hp") != j.end())
	{
		SetHP(j["hp"]);
	}
	if (j.find("damage") != j.end())
	{
		SetDMG(j["damage"]);
	}
	if (j.find("type") != j.end())
	{
		SetType(j["type"].get<std::string>().c_str());
	}
	if (j.find("name") != j.end())
	{
		SetName(j["name"].get<std::string>().c_str());
	}
	if (j.find("rateOfFire") != j.end())
	{
		SetROF(j["rateOfFire"].get<float>());
	}
	if (j.find("projectileSpeed") != j.end())
	{
		SetProjectileSpeed(j["projectileSpeed"].get<float>());
	}
	if (j.find("movementSpeed") != j.end())
	{
		SetMovementSpeed(j["movementSpeed"].get<float>());
	}

	return true;
}

#pragma once

#include "EnemyClass.h"

#include <Vector.h>
#include <iostream>

class EditorEnemyClass : public EnemyClass
{
public:
	EditorEnemyClass();
	~EditorEnemyClass();

	inline void SetTint(const CommonUtilities::Vector4f& aTint = { 1.f, 1.f, 1.f, 1.f }) { myTint = aTint; }
	inline const CommonUtilities::Vector4f& GetTint() const { return myTint; }

	bool SaveEnemyClass();
	bool LoadEnemyClass();
	bool SetEnemyClass(const std::string& aClassPath);
	inline const std::string& GetClassPath() { return myClassPath; }

private:
	CommonUtilities::Vector4f myTint;

	std::string myClassPath;

};


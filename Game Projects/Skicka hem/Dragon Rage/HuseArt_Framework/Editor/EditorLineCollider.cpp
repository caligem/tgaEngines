#include "stdafx.h"
#include "EditorLineCollider.h"

#include <tga2d\primitives\line_primitive.h>
#include <tga2d\math\common_math.h>

#include <Camera.h>

namespace CU = CommonUtilities;

EditorLineCollider::EditorLineCollider(float aLength, float aRotation)
{
	myGizmo.Init("Assets/Images/Editor/GizmoLineCollider.dds", GetPosition());
	myLine = new Tga2D::CLinePrimitive();
	myLine->myColor.Set(0.3f, 1.f, 0.3f);
	SetLength(aLength);
	SetRotation(aRotation);
}

EditorLineCollider::~EditorLineCollider()
{}

void EditorLineCollider::Render(const CommonUtilities::Camera & aCamera)
{
	myGizmo.SetPosition(GetPosition());

	if( myIsGizmoHidden )
	{
		myIsGizmoHidden = false;
	}
	else
	{
		myGizmo.Render();
	}

	if( myIsOutlined )
	{
		myLine->myColor.Set(0.f, 1.f, 1.f);
		DrawLine(aCamera);
		myLine->myColor.Set(0.3f, 1.f, 0.3f);
		myIsOutlined = false;
	}
	else
	{
		DrawLine(aCamera);
	}
}

void EditorLineCollider::DrawLine(const CommonUtilities::Camera & aCamera)
{
	CU::Vector3f point1 = aCamera.PostProjectionSpace(myGizmo.GetPosition() + CU::Vector3f(
		std::cosf(myRotation)*myLength,
		std::sinf(myRotation)*myLength,
		0.f
	));
	CU::Vector3f point2 = aCamera.PostProjectionSpace(myGizmo.GetPosition() + CU::Vector3f(
		std::cosf(myRotation + Tga2D::Pif)*myLength,
		std::sinf(myRotation + Tga2D::Pif)*myLength,
		0.f
	));
	
	myLine->myFromPosition = {
		point1.x, point1.y
	};
	myLine->myToPosition = {
		point2.x, point2.y
	};
	myLine->Render();
}

#pragma once

namespace Tga2D
{
	class CLinePrimitive;
}

namespace CommonUtilities
{
	class Camera;
}

#include <LineCollider.h>
#include "Sprite.h"

class EditorLineCollider : public LineCollider
{
public:
	EditorLineCollider(float aLength = 1.f, float aRotation = 0.f);
	~EditorLineCollider();

	void Render(const CommonUtilities::Camera& aCamera);

	inline void Outline() { myIsOutlined = true; }
	inline void HideGizmo() { myIsGizmoHidden = true; }

	Sprite myGizmo;

private:
	void DrawLine(const CommonUtilities::Camera& aCamera);

	bool myIsOutlined = false;
	bool myIsGizmoHidden = false;

	Tga2D::CLinePrimitive* myLine;
	
};


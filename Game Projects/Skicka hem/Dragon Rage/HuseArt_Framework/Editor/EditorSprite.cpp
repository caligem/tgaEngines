#include "stdafx.h"
#include "EditorSprite.h"

#include <tga2d\engine.h>
#include <tga2d\sprite\sprite.h>
#include <tga2d\primitives\line_primitive.h>

#include <Macros.h>
#include <Camera.h>
#include <Matrix.h>

namespace CU = CommonUtilities;

EditorSprite::EditorSprite()
{
}

EditorSprite::EditorSprite(const char * aPath, const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector2f & aScale, float aRotation)
{
	Init(aPath, aPosition, aScale, aRotation);
}

EditorSprite::~EditorSprite()
{
	if( myLine != nullptr )
	{
		SAFE_DELETE(myLine);
	}
}

void EditorSprite::Init(const char * aPath, const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector2f & aScale, float aRotation)
{
	Sprite::Init(aPath, aPosition, aScale, aRotation);
	myPath = aPath;

	myLine = new Tga2D::CLinePrimitive();
	myLine->myColor.Set(0.f, 1.f, 1.f);
}

void EditorSprite::Render()
{
	if( myPoint.z < 0.f )
	{
		return;
	}

	Sprite::Render();

	if( myIsOutlined )
	{
		DrawLine(
			{ -myScaledSize.x / 2.f, -myScaledSize.y / 2.f },
			{ myScaledSize.x / 2.f, -myScaledSize.y / 2.f }
		);
		DrawLine(
			{ myScaledSize.x / 2.f, -myScaledSize.y / 2.f },
			{ myScaledSize.x / 2.f, myScaledSize.y / 2.f }
		);
		DrawLine(
			{ myScaledSize.x / 2.f, myScaledSize.y / 2.f },
			{ -myScaledSize.x / 2.f, myScaledSize.y / 2.f }
		);
		DrawLine(
			{ -myScaledSize.x / 2.f, myScaledSize.y / 2.f },
			{ -myScaledSize.x / 2.f, -myScaledSize.y / 2.f }
		);

		myIsOutlined = false;
	}
}

void EditorSprite::DrawLine(const CommonUtilities::Vector2f & aFrom, const CommonUtilities::Vector2f & aTo)
{
	static float r = static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().x) / Tga2D::CEngine::GetInstance()->GetWindowSize().y;

	CommonUtilities::Vector3f from = { aFrom.x*r, aFrom.y, 1.f };
	CommonUtilities::Vector3f to = { aTo.x*r, aTo.y, 1.f };

	from *= CommonUtilities::Matrix33f::CreateRotateAroundZ(myRotation);
	to *= CommonUtilities::Matrix33f::CreateRotateAroundZ(myRotation);

	myLine->SetFrom(myPoint.x + from.x/r, myPoint.y + from.y);
	myLine->SetTo(myPoint.x + to.x/r, myPoint.y + to.y);
	myLine->Render();
}

#pragma once

#include <string>

namespace Tga2D
{
	class CLinePrimitive;
}

#include <Vector.h>
#include <Sprite.h>

class EditorSprite : public Sprite
{
public:
	EditorSprite();
	EditorSprite(const char* aPath, const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f }, const CommonUtilities::Vector2f& aScale = { 1.f, 1.f }, float aRotation = 0.f);
	~EditorSprite();

	void Init(const char* aPath, const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f }, const CommonUtilities::Vector2f& aScale = { 1.f, 1.f }, float aRotation = 0.f);
	void Render();

	inline void Outline() { myIsOutlined = true; }

	inline const std::string& GetPath() const { return myPath; }

private:
	void DrawLine(const CommonUtilities::Vector2f& aFrom, const CommonUtilities::Vector2f& aTo);
	
	bool myIsOutlined = false;
	Tga2D::CLinePrimitive* myLine;

	std::string myPath;
};


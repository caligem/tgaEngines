#include "stdafx.h"
#include <tga2d/Engine.h>
#include "Game.h"

#include <tga2d/error/error_manager.h>

using namespace std::placeholders;

#ifdef _DEBUG
#pragma comment(lib,"DX2DEngine_Debug.lib")
#endif // DEBUG
#ifdef _RELEASE
#pragma comment(lib,"DX2DEngine_Release.lib")
#endif // RELEASE
#ifdef _RETAIL
#pragma comment(lib,"DX2DEngine_Retail.lib")
#endif // RETAIL


#include <iostream>

CGame::CGame() :
	myGameWorld(myInputManager, myTimer)
{
}

CGame::~CGame()
{
}

bool CGame::Init(const std::wstring& aVersion, HWND aHWND)
{
	unsigned short windowWidth = 1600;
	unsigned short windowHeight = 900;
	std::cin >> windowWidth >> windowHeight;

    Tga2D::SEngineCreateParameters createParameters;
	createParameters.myActivateDebugSystems = Tga2D::eDebugFeature_Drawcalls | Tga2D::eDebugFeature_Fps;
    
    createParameters.myInitFunctionToCall = std::bind( &CGame::InitCallBack, this );
    createParameters.myUpdateFunctionToCall = std::bind( &CGame::UpdateCallBack, this );
	createParameters.myWinProcCallback = std::bind(&CGame::WinProc, this, _1, _2, _3, _4);
    createParameters.myLogFunction = std::bind( &CGame::LogCallback, this, _1 );
	createParameters.myWindowWidth = windowWidth;
    createParameters.myWindowHeight = windowHeight;
	createParameters.myRenderWidth = windowWidth;
	createParameters.myRenderHeight = windowHeight;
	createParameters.myTargetWidth = windowWidth;
	createParameters.myTargetHeight = windowHeight;
	createParameters.myWindowSetting = Tga2D::EWindowSetting_Overlapped;
	createParameters.myAutoUpdateViewportWithWindow = false;
    createParameters.myClearColor.Set(100.f/255.f, 149.f/255.f, 237.f/255.f, 1.f);
	createParameters.myStartInFullScreen = false;
	createParameters.myEnableVSync = false;

	if (aHWND != nullptr)
	{
		createParameters.myHwnd = new HWND(aHWND);
	}
	
	std::wstring architecture = L"32 Bit";
#ifdef _AMD64_
	architecture = L"64 Bit";
#endif
	std::wstring appname = L"== EDITOR == RELEASE [" + aVersion + L"] " + architecture;
#ifdef _DEBUG
	appname = L"== EDITOR DEBUG == [" + aVersion + L"] " + architecture;
#endif

    createParameters.myApplicationName = appname;

    Tga2D::CEngine::CreateInstance( createParameters );

    if( !Tga2D::CEngine::GetInstance()->Start() )
    {
        ERROR_PRINT( "Fatal error! Engine could not start!" );
		system("pause");
		return false;
    }

	return true;
}

LRESULT CGame::WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_KEYDOWN: case WM_KEYUP:
	case WM_LBUTTONDOWN: case WM_RBUTTONDOWN: case WM_MBUTTONDOWN:
	case WM_LBUTTONUP: case WM_RBUTTONUP: case WM_MBUTTONUP:
	case WM_MOUSEMOVE: case WM_MOUSEWHEEL:
		myInputManager.OnInput(message, wParam, lParam);
		break;
	case WM_DESTROY:
		// close the application entirely
		PostQuitMessage(0);
		return 0;
		break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);
}

void CGame::InitCallBack()
{
    myGameWorld.Init();
}

void CGame::UpdateCallBack()
{
	myTimer.Update();

	myGameWorld.Update();
	myGameWorld.Render();

	myInputManager.Update();
}


void CGame::LogCallback( std::string aText )
{
}

#include "stdafx.h"
#include "GameWorld.h"

#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>

#include <tga2d\engine.h>
#include <tga2d\sprite\sprite.h>
#include <tga2d\primitives\line_primitive.h>
#include <tga2d\text\text.h>
#include <tga2d\math\common_math.h>

#include <FileDialog.h>
#include <Macros.h>
#include <InputManager.h>
#include <Timer.h>
#include <Matrix.h>
#include <Parser\json.hpp>

namespace CU = CommonUtilities;

CGameWorld::CGameWorld(const CommonUtilities::InputManager &aInputManager, const CommonUtilities::Timer &aTimer)
	: myInputManager(aInputManager)
	, myTimer(aTimer)
{}

CGameWorld::~CGameWorld()
{
	EmptyArrays();
}

void CGameWorld::Init()
{
	Tga2D::CEngine::GetInstance()->SetSampler(ESamplerType::ESamplerType_Point);

	float r = (static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().x)/static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().y));
	float fovY = Tga2D::DegToRad(45.f);
	float yScale = 1.f / std::tanf(fovY);
	float xScale = 1.f / (r * std::tanf(fovY));

	myCamera.Init({
		xScale, 0.f, 0.f, 0.f,
		0.f, yScale, 0.f, 0.f,
		0.f, 0.f, 1.f, 1.f,
		0.f, 0.f, 0.f, 1.f
	});
	myCamera.SetPosition({ 0.f, 0.f, 0.f });

	myMode = EditorMode::Normal;

	myModeText = new Tga2D::CText("Text/BOMBARD_.ttf", Tga2D::EFontSize_14, 1);
	myModeText->myPosition = { 0.f, 48.f / Tga2D::CEngine::GetInstance()->GetWindowSize().y };
	myModeText->myText = "Mode: Normal";

	myObjects.Init(8);
	myButtons.Init(8);
	myObjectsLD.Init(8);
	myBuffer.Init(8);
	myMultiSelection.Init(32);

	myLine = new Tga2D::CLinePrimitive();
	myLine->myColor.Set(1.f, 0.7f, 1.0f);

	DeselectObject();
}

void CGameWorld::Update()
{ 
	SetCursorState(CursorState::Arrow);

	// Update objects
	CU::Vector2f mousePoint = {
		myInputManager.GetMousePosition().x / Tga2D::CEngine::GetInstance()->GetWindowSize().x,
		myInputManager.GetMousePosition().y / Tga2D::CEngine::GetInstance()->GetWindowSize().y
	};
	for( Object* object : myObjects )
	{
		object->Update(myTimer.GetDeltaTime());
	}
	for( Object* object : myObjectsLD )
	{
		object->Update(myTimer.GetDeltaTime());
	}

	// Handle Input
	HandleInput();
}

void CGameWorld::Render()
{
	if (mySelectedObject != nullptr)
	{
		mySelectedObject->Outline();
	}
	else
	{
		switch (myMode)
		{
		case EditorMode::Normal:
			myBuffer = myObjects;
			break;
		case EditorMode::LevelDesign:
			myBuffer = myObjectsLD;
			for (Object* object : myButtons)myBuffer.Add(object);
			break;
		case EditorMode::Graphics:
			myBuffer = myObjects;
			break;
		}
		CU::Vector2f mousePoint = {
			myInputManager.GetMousePosition().x / Tga2D::CEngine::GetInstance()->GetWindowSize().x,
			myInputManager.GetMousePosition().y / Tga2D::CEngine::GetInstance()->GetWindowSize().y
		};
		std::sort(myBuffer.begin(), myBuffer.end(), [](Object* a, Object* b)
		{
			return a->GetPoint().z > b->GetPoint().z;
		});
		for (unsigned short i = myBuffer.Size(); i > 0; --i)
		{
			Object* object = myBuffer[i - 1];
			if (object->IsInside(mousePoint) && object->GetPoint().z > 0.f)
			{
				object->Outline();
				break;
			}
		}
	}
	if (mySpecialMode == SpecialMode::MultiSelect)
	{
		for (Object* enemy : myMultiSelection)
		{
			enemy->Outline();
		}
	}

	switch( myMode )
	{
	case EditorMode::Normal:
		RenderNormalMode();
		break;
	case EditorMode::LevelDesign:
		RenderLevelDesignMode();
		break;
	case EditorMode::Graphics:
		RenderGraphicsMode();
		break;
	}

	if (myCamera.GetPosition().z < 1.f)
	{
		float camZ = fabsf(1.f - myCamera.GetPosition().z);

		myLine->myColor.Set(1.f, 0.7f, 1.0f);
		DrawLine(
			myCamera.PostProjectionSpace({ -1.4f, myCamera.GetPosition().y + camZ, 1.f }),
			myCamera.PostProjectionSpace({ -1.4f, myCamera.GetPosition().y - camZ, 1.f })
		);

		DrawLine(
			myCamera.PostProjectionSpace({ 1.4f, myCamera.GetPosition().y + camZ, 1.f }),
			myCamera.PostProjectionSpace({ 1.4f, myCamera.GetPosition().y - camZ, 1.f })
		);

		DrawLine(
			myCamera.PostProjectionSpace({ -16.f/9.f, myCamera.GetPosition().y + camZ, 1.f }),
			myCamera.PostProjectionSpace({ -16.f/9.f, myCamera.GetPosition().y - camZ, 1.f })
		);

		DrawLine(
			myCamera.PostProjectionSpace({ 16.f/9.f, myCamera.GetPosition().y + camZ, 1.f }),
			myCamera.PostProjectionSpace({ 16.f/9.f, myCamera.GetPosition().y - camZ, 1.f })
		);

		DrawLine(
			myCamera.PostProjectionSpace({ 0.f, myCamera.GetPosition().y + camZ, 1.f }),
			myCamera.PostProjectionSpace({ 0.f, myCamera.GetPosition().y - camZ, 1.f })
		);

		if (myIsMultiSelecting)
		{
			CU::Vector2f mousePoint = {
				myInputManager.GetMousePosition().x / Tga2D::CEngine::GetInstance()->GetWindowSize().x,
				myInputManager.GetMousePosition().y / Tga2D::CEngine::GetInstance()->GetWindowSize().y
			};

			myLine->myColor.Set(0.3f, 1.0f, 0.5f);
			DrawLine(
				{ myMultiSelectionStart.x, myMultiSelectionStart.y, 1.f },
				{ myMultiSelectionStart.x, mousePoint.y, 1.f }
			);
			DrawLine(
				{ myMultiSelectionStart.x, myMultiSelectionStart.y, 1.f },
				{ mousePoint.x, myMultiSelectionStart.y, 1.f }
			);
			DrawLine(
				{ myMultiSelectionStart.x, mousePoint.y, 1.f },
				{ mousePoint.x, mousePoint.y, 1.f }
			);
			DrawLine(
				{ mousePoint.x, myMultiSelectionStart.y, 1.f },
				{ mousePoint.x, mousePoint.y, 1.f }
			);
		}
		
	}

	myModeText->Render();
}

void CGameWorld::RenderBuffer()
{
	for( Object* object : myBuffer )
	{
		object->CalculatePoint(myCamera);
	}

	std::stable_sort(myBuffer.begin(), myBuffer.end(), [](Object* a, Object* b)
	{
		if (a->GetPosition().z == b->GetPosition().z)
		{
			return a->GetPosition().y > b->GetPosition().y;
		}
		return a->GetPosition().z > b->GetPosition().z;
	});

	for( Object* object : myBuffer )
	{
		object->Render(myCamera);
	}
}

void CGameWorld::RenderNormalMode()
{
	myBuffer = myObjects;
	RenderBuffer();
}

void CGameWorld::RenderLevelDesignMode()
{
	myBuffer = myObjects;
	for( Object* object : myObjectsLD ) myBuffer.Add(object);
	for( Object* object : myButtons ) myBuffer.Add(object);

	RenderBuffer();
}

void CGameWorld::RenderGraphicsMode()
{
	RenderLevelDesignMode();
}

void CGameWorld::HandleInput()
{
	if( myInputManager.IsKeyPressed(CU::Key_F1) )
	{
		myMode = EditorMode::Normal;
		myModeText->myText = "Mode: Normal";
		DeselectObject();
	}
	else if( myInputManager.IsKeyPressed(CU::Key_F2) )
	{
		myMode = EditorMode::LevelDesign;
		myModeText->myText = "Mode: Level Design";
		DeselectObject();
	}
	else if( myInputManager.IsKeyPressed(CU::Key_F3) )
	{
		myMode = EditorMode::Graphics;
		myModeText->myText = "Mode: Graphics";
		DeselectObject();
	}
	else if( mySelectedObject != nullptr && myInputManager.IsKeyPressed(CU::Key_Delete) )
	{
		DeleteSelectedObject();
	}
	else if( mySelectedObject != nullptr && myInputManager.IsKeyPressed(CU::Key_Escape) )
	{
		DeselectObject();
	}
	else if( myInputManager.IsKeyPressed(CU::Key_Escape) )
	{
		myCamera.SetPosition({
			myCamera.GetPosition().x,
			myCamera.GetPosition().y,
			0.f
		});
	}

	switch( myMode )
	{
	case EditorMode::Normal:
		HandleInputNormalMode();
		break;
	case EditorMode::LevelDesign:
		HandleInputLevelDesignMode();
		break;
	case EditorMode::Graphics:
		HandleInputGraphicsMode();
		break;
	}
}

void CGameWorld::HandleInputNormalMode()
{
	if( myInputManager.IsKeyDown(CU::Key_Ctrl) && myInputManager.IsKeyDown(CU::Key_Shift) )
	{ }
	else if( myInputManager.IsKeyDown(CU::Key_Ctrl) )
	{
		if( mySelectedObject != nullptr && myInputManager.HasScrolled() )
		{
			MoveSelectedObjectZ();
		}
		else if( myInputManager.IsKeyPressed(CU::Key_O) )
		{
			OpenLevel();
		}
		else if( myInputManager.IsKeyPressed(CU::Key_S) )
		{
			SaveLevel();
		}
		else if( myInputManager.IsKeyPressed(CU::Key_D) )
		{
			DuplicateSelectedObject();
		}
	}
	else if( myInputManager.IsKeyDown(CU::Key_Shift) )
	{
		if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Right) )
		{
			mySelectedObject->myAnimation->ChangeValues(0, 0, 1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Left) )
		{
			mySelectedObject->myAnimation->ChangeValues(0, 0, -1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Down) )
		{
			mySelectedObject->myAnimation->ChangeValues(0, 0, 0, 1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Up) )
		{
			mySelectedObject->myAnimation->ChangeValues(0, 0, 0, -1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr && myInputManager.IsButtonDown(CU::Button_Right) )
		{
			RotateSelectedObject();
		}
		else if( mySelectedObject != nullptr && myInputManager.IsButtonDown(CU::Button_Left) )
		{
			ScaleSelectedObject();
		}
	}
	else
	{
		if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Right) )
		{
			mySelectedObject->myAnimation->ChangeValues(0, 1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Left) )
		{
			mySelectedObject->myAnimation->ChangeValues(0, -1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Down) )
		{
			mySelectedObject->myAnimation->ChangeValues(1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Up) )
		{
			mySelectedObject->myAnimation->ChangeValues(-1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( myInputManager.IsButtonDown(CU::Button_Left) &&
				myInputManager.IsKeyDown(CU::Key_Space) )
		{
			PanCamera();
		}
		else if( mySelectedObject != nullptr && myInputManager.IsKeyPressed(CU::Key_F) )
		{
			FocusOnSelectedObject();
		}
		else if( mySelectedObject != nullptr && myInputManager.IsButtonDown(CU::Button_Left) )
		{
			MoveSelectedObjectXY();
		}
		else if( myInputManager.HasScrolled())
		{
			ZoomCamera();
		}
		else if( mySelectedObject == nullptr && myInputManager.IsButtonPressed(CU::Button_Left) )
		{
			SelectObject();
		}
		else if( myInputManager.IsKeyPressed(CU::Key_W) )
		{
			ImportAnimation();
		}
		else if( myInputManager.IsKeyPressed(CU::Key_Q) )
		{
			ImportSprite();
		}
	}
}

void CGameWorld::HandleInputLevelDesignMode()
{
	if( myInputManager.IsKeyDown(CU::Key_Ctrl) && myInputManager.IsKeyDown(CU::Key_Shift) )
	{
		if (myInputManager.IsButtonPressed(CU::Button_Left))
		{
			StartMultiSelect();
		}
		if (myInputManager.IsButtonReleased(CU::Button_Left) && myIsMultiSelecting)
		{
			EndMultiSelect();
		}
	}
	else if( myInputManager.IsKeyDown(CU::Key_Ctrl) )
	{
		if( mySelectedObject != nullptr && myInputManager.HasScrolled() )
		{
			MoveSelectedObjectZ();
		}
		else if( myInputManager.IsKeyPressed(CU::Key_D) )
		{
			DuplicateSelectedObject();
		}
		else if( myInputManager.IsKeyPressed(CU::Key_2) )
		{
			AddCircleCollider({ myCamera.GetPosition().x, myCamera.GetPosition().y, 1.f });
		}
		else if( myInputManager.IsKeyPressed(CU::Key_3) )
		{
			AddLineCollider({ myCamera.GetPosition().x, myCamera.GetPosition().y, 1.f });
		}
		else if( myInputManager.IsKeyPressed(CU::Key_1) )
		{
			AddEnemy({ myCamera.GetPosition().x, myCamera.GetPosition().y, 1.f });
		}
		else if( myInputManager.IsKeyPressed(CU::Key_B) )
		{
			ImportButton();
		}
		else if (myInputManager.IsKeyPressed(CU::Key_C))
		{
			AddCurve({ myCamera.GetPosition().x, myCamera.GetPosition().y, 1.f });
		}
	}
	else if( myInputManager.IsKeyDown(CU::Key_Shift) )
	{
		if( mySelectedObject != nullptr && myInputManager.IsButtonDown(CU::Button_Right) )
		{
			RotateSelectedObject();
		}
		else if( mySelectedObject != nullptr && myInputManager.IsButtonDown(CU::Button_Left) )
		{
			ScaleSelectedObject();
		}
		else if( myInputManager.IsKeyPressed(CU::Key_1) )
		{
			OpenEnemyClassEditor();
		}
	}
	else
	{
		if( myInputManager.IsKeyPressed(CU::Key_H) )
		{
			myHideGizmos = !myHideGizmos;
		}
		else if (myInputManager.IsKeyPressed(CU::Key_F))
		{
			FlipCurve();
		}
		else if (myInputManager.IsKeyPressed(CU::Key_G))
		{
			MirrorCurve();
		}
		else if( myInputManager.IsButtonDown(CU::Button_Left) &&
				myInputManager.IsKeyDown(CU::Key_Space) &&
				myInputManager.HasMouseMoved() )
		{
			PanCamera();
		}
		else if( mySelectedObject != nullptr && myInputManager.IsButtonPressed(CU::Button_Left) )
		{
			MousePressedSelectedObject();
		}
		else if( mySelectedObject != nullptr && myInputManager.IsButtonReleased(CU::Button_Left) )
		{
			MouseReleasedSelectedObject();
		}
		else if( mySelectedObject != nullptr && myInputManager.IsButtonDown(CU::Button_Left) )
		{
			MoveSelectedObjectXY();
		}
		else if( myInputManager.HasScrolled())
		{
			ZoomCamera();
		}
		else if( mySelectedObject == nullptr && myInputManager.IsButtonPressed(CU::Button_Left) )
		{
			SelectObject();
		}
		else if (mySelectedObject != nullptr && myInputManager.IsKeyPressed(CU::Key_A))
		{
			if (mySelectedObject->myType == ObjectType::EditorCurve)
			{
				mySelectedObject->myCurve->AddPoint();
			}
		}
		else if (mySelectedObject != nullptr && myInputManager.IsKeyPressed(CU::Key_D))
		{
			if (mySelectedObject->myType == ObjectType::EditorCurve)
			{
				mySelectedObject->myCurve->PopPoint();
			}
		}
	}
}

void CGameWorld::HandleInputGraphicsMode()
{
	if( myInputManager.IsKeyDown(CU::Key_Ctrl) && myInputManager.IsKeyDown(CU::Key_Shift) )
	{ }
	else if( myInputManager.IsKeyDown(CU::Key_Ctrl) )
	{
		if( mySelectedObject != nullptr && myInputManager.HasScrolled() )
		{
			MoveSelectedObjectZ();
		}
		else if( myInputManager.IsKeyPressed(CU::Key_D) )
		{
			DuplicateSelectedObject();
		}
	}
	else if( myInputManager.IsKeyDown(CU::Key_Shift) )
	{
		if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Right) )
		{
			mySelectedObject->myAnimation->ChangeValues(0, 0, 1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Left) )
		{
			mySelectedObject->myAnimation->ChangeValues(0, 0, -1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Down) )
		{
			mySelectedObject->myAnimation->ChangeValues(0, 0, 0, 1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Up) )
		{
			mySelectedObject->myAnimation->ChangeValues(0, 0, 0, -1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr && myInputManager.IsButtonDown(CU::Button_Right) )
		{
			RotateSelectedObject();
		}
		else if( mySelectedObject != nullptr && myInputManager.IsButtonDown(CU::Button_Left) )
		{
			ScaleSelectedObject();
		}
	}
	else
	{
		if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Right) )
		{
			mySelectedObject->myAnimation->ChangeValues(0, 1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Left) )
		{
			mySelectedObject->myAnimation->ChangeValues(0, -1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Down) )
		{
			mySelectedObject->myAnimation->ChangeValues(1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( mySelectedObject != nullptr &&
				mySelectedObject->myType == ObjectType::EditorAnimation &&
				myInputManager.IsKeyPressed(CU::Key_Up) )
		{
			mySelectedObject->myAnimation->ChangeValues(-1);
			myInspector.UpdateSelectedObjectValues();
		}
		else if( myInputManager.IsButtonDown(CU::Button_Left) &&
				myInputManager.IsKeyDown(CU::Key_Space) )
		{
			PanCamera();
		}
		else if( mySelectedObject != nullptr && myInputManager.IsKeyPressed(CU::Key_F) )
		{
			FocusOnSelectedObject();
		}
		else if( mySelectedObject != nullptr && myInputManager.IsButtonDown(CU::Button_Left) )
		{
			MoveSelectedObjectXY();
		}
		else if( myInputManager.HasScrolled())
		{
			ZoomCamera();
		}
		else if( mySelectedObject == nullptr && myInputManager.IsButtonPressed(CU::Button_Left) )
		{
			SelectObject();
		}
		else if( myInputManager.IsKeyPressed(CU::Key_W) )
		{
			ImportAnimation();
		}
		else if( myInputManager.IsKeyPressed(CU::Key_Q) )
		{
			ImportSprite();
		}
	}
}

Object* CGameWorld::AddSprite(const char * aPath, const CU::Vector3f & aPosition, const CU::Vector2f & aScale, float aRotation, const CU::Vector4f& aTint)
{
	Object* o = new Object();
	o->myType = ObjectType::EditorSprite;
	o->mySprite = new EditorSprite(aPath, aPosition, aScale, aRotation);
	o->mySprite->SetTint(aTint);
	myObjects.Add(o);
	return o;
}

Object* CGameWorld::AddAnimation(const char * aPath, const CU::Vector3f & aPosition, const CU::Vector2f & aScale, float aRotation, const CU::Vector4f & aTint)
{
	Object* o = new Object();
	o->myType = ObjectType::EditorAnimation;
	o->myAnimation = new EditorAnimation(aPath, aPosition, aScale, aRotation);
	o->myAnimation->SetTint(aTint);
	myObjects.Add(o);
	return o;
}

Object* CGameWorld::AddCircleCollider(const CommonUtilities::Vector3f & aPosition, float aRadius)
{
	Object* o = new Object();
	o->myType = ObjectType::EditorCircleCollider;
	o->myCircleCollider = new EditorCircleCollider(aRadius);
	o->myCircleCollider->SetPosition(aPosition);
	myObjectsLD.Add(o);
	return o;
}

Object* CGameWorld::AddLineCollider(const CommonUtilities::Vector3f & aPosition, float aLength, float aRotation)
{
	Object* o = new Object();
	o->myType = ObjectType::EditorLineCollider;
	o->myLineCollider = new EditorLineCollider(aLength, aRotation);
	o->myLineCollider->SetPosition(aPosition);
	myObjectsLD.Add(o);
	return o;
}

Object* CGameWorld::AddEnemy(const CommonUtilities::Vector3f & aPosition)
{
	Object* o = new Object();
	o->myType = ObjectType::EditorEnemy;
	o->myEnemy = new EditorEnemy();
	o->myEnemy->SetPosition(aPosition);
	myObjectsLD.Add(o);
	return o;
}

Object* CGameWorld::AddButton(const char * aPath, const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector2f & aScale, float aRotation, const CommonUtilities::Vector4f & aTint)
{
	Object* o = new Object();
	o->myType = ObjectType::EditorSprite;
	o->mySprite = new EditorSprite(aPath, aPosition, aScale, aRotation);
	o->mySprite->SetTint(aTint);
	myButtons.Add(o);
	return o;
}

Object * CGameWorld::AddCurve(const CommonUtilities::Vector3f & aPosition)
{
	Object* o = new Object();
	o->myType = ObjectType::EditorCurve;
	o->myCurve = new EditorCurve(aPosition);
	myObjectsLD.Add(o);
	return o;
}

void CGameWorld::OpenEnemyClassEditor()
{
	DeselectObject();
	myInspector.DeselectObject();
	myInspector.OpenEnemyClassEditor();
}

void CGameWorld::ImportSprite()
{
	std::string filePath(CU::GetOpenDialogFilePath("dds\0*.dds\0"));
	std::replace(filePath.begin(), filePath.end(), '\\', '/');
	const char* search = "/Assets/";
	if( filePath.find(search) != filePath.npos )
	{
		AddSprite(
			filePath.substr(filePath.find("Assets/")).c_str(),
			{
				myCamera.GetPosition().x,
				myCamera.GetPosition().y,
				1.f
			}
		);
	}
}

void CGameWorld::ImportAnimation()
{
	std::string filePath(CU::GetOpenDialogFilePath("dds\0*.dds\0"));
	std::replace(filePath.begin(), filePath.end(), '\\', '/');
	const char* search = "/Assets/";
	if( filePath.find(search) != filePath.npos )
	{
		AddAnimation(
			filePath.substr(filePath.find("Assets/")).c_str(),
			{
				myCamera.GetPosition().x,
				myCamera.GetPosition().y,
				1.f
			}
		);
	}
}

void CGameWorld::ImportButton()
{
	std::string filePath(CU::GetOpenDialogFilePath("dds\0*.dds\0"));
	std::replace(filePath.begin(), filePath.end(), '\\', '/');
	const char* search = "/Assets/";
	if( filePath.find(search) != filePath.npos )
	{
		AddButton(
			filePath.substr(filePath.find("Assets/")).c_str(),
			{
				myCamera.GetPosition().x,
				myCamera.GetPosition().y,
				1.f
			}
		);
	}
}

void CGameWorld::OpenLevel()
{
	if( MessageBoxA(
		nullptr,
		"Are you sure you want to load a level?\nMake sure you've saved your current work.", "Open level",
		MB_YESNO
	) == IDNO )
	{ return; }

	std::string filePath(CU::GetOpenDialogFilePath("HAF Level\0*.hafl\0"));
	std::replace(filePath.begin(), filePath.end(), '\\', '/');
	const char* search = "/Assets/";
	if( filePath.find(search) != filePath.npos )
	{
		std::ifstream i(filePath.c_str());
		nlohmann::json j;
		i >> j;

		EmptyArrays();
		if( j.find("sprites") != j.end() )
		{
			for( auto sprite : j["sprites"])
			{
				Object* o = AddSprite(
					sprite["texture"].get<std::string>().c_str(),
					{
						sprite["position"][0],
						sprite["position"][1],
						sprite["position"][2]
					},
					{
						sprite["scale"][0],
						sprite["scale"][1]
					},
					sprite["rotation"].get<float>()
				);
				if( sprite.find("tint") != sprite.end() )
				{
					o->mySprite->SetTint(
						{
							sprite["tint"][0],
							sprite["tint"][1],
							sprite["tint"][2],
							sprite["tint"][3]
						}
					);
				}
				if (sprite.find("UVScale") != sprite.end())
				{
					o->mySprite->SetUVScale({
						sprite["UVScale"][0],
						sprite["UVScale"][1]
					});
				}
				if (sprite.find("locked") != sprite.end())
				{
					o->myLocked = sprite["locked"].get<float>();
				}
				if (sprite.find("name") != sprite.end())
				{
					o->myName = sprite["name"].get<std::string>();
				}
				if (sprite.find("tag") != sprite.end())
				{
					o->myTag = sprite["tag"].get<std::string>();
				}
			}
		}
		if( j.find("buttons") != j.end() )
		{
			for( auto button : j["buttons"] )
			{
				Object* o = AddButton(
					button["texture"].get<std::string>().c_str(),
					{
						button["position"][0],
						button["position"][1],
						button["position"][2]
					},
					{
						button["scale"][0],
						button["scale"][1]
					},
					button["rotation"].get<float>()
				);
				if( button.find("tint") != button.end() )
				{
					o->mySprite->SetTint(
						{
							button["tint"][0],
							button["tint"][1],
							button["tint"][2],
							button["tint"][3]
						}
					);
				}
				if (button.find("UVScale") != button.end())
				{
					o->mySprite->SetUVScale({
						button["UVScale"][0],
						button["UVScale"][1]
					});
				}
				if (button.find("locked") != button.end())
				{
					o->myLocked = button["locked"].get<float>();
				}
				if (button.find("name") != button.end())
				{
					o->myName = button["name"].get<std::string>();
				}
				if (button.find("tag") != button.end())
				{
					o->myTag = button["tag"].get<std::string>();
				}
			}
		}
		if( j.find("animations") != j.end() )
		{
			for( auto animation : j["animations"])
			{
				Object* o = AddAnimation(
					animation["texture"].get<std::string>().c_str(),
					{
						animation["position"][0],
						animation["position"][1],
						animation["position"][2]
					},
					{
						animation["scale"][0],
						animation["scale"][1]
					},
					animation["rotation"].get<float>()
				);
				o->myAnimation->Setup(
					animation["rows"].get<int>(),
					animation["cols"].get<int>(),
					animation["frames"].get<int>(),
					animation["offset"].get<int>()
				);
				if( animation.find("tint") != animation.end() )
				{
					o->myAnimation->SetTint(
						{
							animation["tint"][0],
							animation["tint"][1],
							animation["tint"][2],
							animation["tint"][3]
						}
					);
				}
				if (animation.find("locked") != animation.end())
				{
					o->myLocked = animation["locked"].get<float>();
				}
				if (animation.find("name") != animation.end())
				{
					o->myName = animation["name"].get<std::string>();
				}
				if (animation.find("tag") != animation.end())
				{
					o->myTag = animation["tag"].get<std::string>();
				}
			}
		}
		if( j.find("colliders") != j.end() )
		{
			for( auto collider : j["colliders"])
			{
				if (collider["type"] == "circle")
				{
					Object* o = AddCircleCollider(
						{
							collider["position"][0],
							collider["position"][1],
							collider["position"][2]
						},
						collider["radius"].get<float>()
					);
					o->myTag = collider["tag"].get<std::string>();
					if (collider.find("name") != collider.end())
					{
						o->myName = collider["name"].get<std::string>();
					}
					if (collider.find("tag") != collider.end())
					{
						o->myTag = collider["tag"].get<std::string>();
					}
				}
				else if (collider["type"] == "line")
				{
					Object* o = AddLineCollider(
						{
							collider["position"][0],
							collider["position"][1],
							collider["position"][2]
						},
						collider["length"].get<float>(),
						collider["rotation"].get<float>()
					);
					if (collider.find("name") != collider.end())
					{
						o->myName = collider["name"].get<std::string>();
					}
					if (collider.find("tag") != collider.end())
					{
						o->myTag = collider["tag"].get<std::string>();
					}
				}
			}
		}
		if (j.find("enemies") != j.end())
		{
			for (auto enemy : j["enemies"])
			{
				Object* o = AddEnemy(
					{
						enemy["position"][0],
						enemy["position"][1],
						enemy["position"][2]
					}
				);
				o->myEnemy->SetEnemyClass(enemy["class"]);
				if (enemy.find("name") != enemy.end())
				{
					o->myName = enemy["name"].get<std::string>();
				}
				if (enemy.find("tag") != enemy.end())
				{
					o->myTag = enemy["tag"].get<std::string>();
				}
			}
		}
		if (j.find("curves") != j.end())
		{
			for (auto curve : j["curves"])
			{
				Object* o = AddCurve({
					curve["position"][0],
					curve["position"][1],
					curve["position"][2],
				});
				o->myCurve->SetEnemyClass(curve["class"].get<std::string>());
				o->myCurve->SetAmount(curve["amount"].get<int>());
				o->myCurve->SetSpeed(curve["speed"].get<float>());
				o->myCurve->SetInterval(curve["interval"].get<float>());
				if (curve.find("points") != curve.end())
				{
					o->myCurve->PopPoint();
					o->myCurve->PopPoint();
					for (auto point : curve["points"])
					{
						o->myCurve->AddPoint({
							point[0],
							point[1],
							point[2]
						});
					}
				}
				if (curve.find("name") != curve.end())
				{
					o->myName = curve["name"].get<std::string>();
				}
				if (curve.find("tag") != curve.end())
				{
					o->myTag = curve["tag"].get<std::string>();
				}
			}
		}
	}
}

void CGameWorld::SaveLevel()
{
	std::string filePath(CU::GetSaveDialogFilePath("HAF Level\0*.hafl\0"));
	std::replace(filePath.begin(), filePath.end(), '\\', '/');
	const char* search = "/Assets/";
	if( filePath.find(search) != filePath.npos )
	{
		nlohmann::json j = {
			{"sprites", nlohmann::json::array()},
			{"buttons", nlohmann::json::array()},
			{"animations", nlohmann::json::array()},
			{"colliders", nlohmann::json::array()},
			{"enemies", nlohmann::json::array()},
			{"curves", nlohmann::json::array()}
		};

		if( myButtons.Empty() )
		{
			j["type"] = "level";
		}
		else
		{
			j["type"] = "menu";
		}

		for( Object* object : myObjects )
		{
			if( object->myType == ObjectType::EditorAnimation )
			{
				EditorAnimation* animation = object->myAnimation;
				j["animations"].push_back({
					{"name", object->myName},
					{"tag", object->myTag},
					{"texture", animation->GetPath()},
					{"position", nlohmann::json::array({
						animation->GetPosition().x,
						animation->GetPosition().y,
						animation->GetPosition().z
					})},
					{"scale", nlohmann::json::array({
						animation->GetScale().x,
						animation->GetScale().y
					})},
					{"rotation", animation->GetRotation()},
					{"rows", animation->GetRows()},
					{"cols", animation->GetCols()},
					{"frames", animation->GetFrames()},
					{"offset", animation->GetOffset()},
					{"duration", animation->GetDuration()},
					{"tint", nlohmann::json::array({
						animation->GetTint().x,
						animation->GetTint().y,
						animation->GetTint().z,
						animation->GetTint().w
					})},
					{"locked", object->myLocked}
				});
			}
			else if( object->myType == ObjectType::EditorSprite )
			{
				EditorSprite* sprite = object->mySprite;
				j["sprites"].push_back({
					{"name", object->myName},
					{"tag", object->myTag},
					{"texture", sprite->GetPath()},
					{"position", nlohmann::json::array({
						sprite->GetPosition().x,
						sprite->GetPosition().y,
						sprite->GetPosition().z
					})},
					{"UVScale", nlohmann::json::array({
						sprite->GetUVScale().x,
						sprite->GetUVScale().y
					})},
					{"scale", nlohmann::json::array({
						sprite->GetScale().x,
						sprite->GetScale().y
					})},
					{"rotation", sprite->GetRotation()},
					{"tint", nlohmann::json::array({
						sprite->GetTint().x,
						sprite->GetTint().y,
						sprite->GetTint().z,
						sprite->GetTint().w
					})},
					{"locked", object->myLocked}
				});
			}
		}
		for( Object* object : myButtons )
		{
			if( object->myType == ObjectType::EditorSprite )
			{
				EditorSprite* button = object->mySprite;
				j["buttons"].push_back({
					{"name", object->myName},
					{"tag", object->myTag},
					{"texture", button->GetPath()},
					{"position", nlohmann::json::array({
						button->GetPosition().x,
						button->GetPosition().y,
						button->GetPosition().z
					})},
					{"UVScale", nlohmann::json::array({
						button->GetUVScale().x,
						button->GetUVScale().y
					})},
					{"scale", nlohmann::json::array({
						button->GetScale().x,
						button->GetScale().y
					})},
					{"rotation", button->GetRotation()},
					{"tint", nlohmann::json::array({
						button->GetTint().x,
						button->GetTint().y,
						button->GetTint().z,
						button->GetTint().w
					})},
					{"locked", object->myLocked}
				});
			}
		}
		for( Object* object : myObjectsLD )
		{
			if( object->myType == ObjectType::EditorCircleCollider )
			{
				EditorCircleCollider* collider = object->myCircleCollider;
				j["colliders"].push_back({
					{"type", "circle"},
					{"name", object->myName},
					{"tag", object->myTag},
					{"position", nlohmann::json::array({
						collider->GetPosition().x,
						collider->GetPosition().y,
						collider->GetPosition().z
					})},
					{"radius", collider->GetRadius()}
				});
			}
			else if (object->myType == ObjectType::EditorLineCollider)
			{
				EditorLineCollider* collider = object->myLineCollider;
				j["colliders"].push_back({
					{"type", "line"},
					{"name", object->myName},
					{"tag", object->myTag},
					{"position", nlohmann::json::array({
						collider->GetPosition().x,
						collider->GetPosition().y,
						collider->GetPosition().z
					})},
					{"length", collider->GetLength()},
					{"rotation", collider->GetRotation()}
				});
			}
			else if(object->myType == ObjectType::EditorEnemy)
			{
				EditorEnemy* enemy = object->myEnemy;
				j["enemies"].push_back({
					{"name", object->myName},
					{"tag", object->myTag},
					{"position", nlohmann::json::array({
						enemy->GetPosition().x,
						enemy->GetPosition().y,
						enemy->GetPosition().z
					})},
					{"class", enemy->GetEnemyClass().c_str()}
				});
			}
			else if (object->myType == ObjectType::EditorCurve)
			{
				EditorCurve* curve = object->myCurve;
				nlohmann::json::object_t obj = nlohmann::json::object({
					{"name", object->myName},
					{"tag", object->myTag},
					{"position", nlohmann::json::array({
						curve->GetPosition().x,
						curve->GetPosition().y,
						curve->GetPosition().z
					})},
					{"points", nlohmann::json::array()},
					{"amount", curve->GetAmount()},
					{"speed", curve->GetSpeed()},
					{"interval", curve->GetInterval()},
					{"class", curve->GetEnemyClass().c_str()}
				});
				for (const CurvePoint& point : curve->GetPoints())
				{
					obj["points"].push_back(
						nlohmann::json::array({
							point.myPosition.x,
							point.myPosition.y,
							point.myPosition.z
						})
					);
				}
				j["curves"].push_back(obj);
			}
		}

		std::ofstream o(filePath.c_str());
		o << std::setw(2) << j << std::endl;
	}
}

void CGameWorld::PanCamera()
{
	CU::Vector2f mouseMovement = {
		-myInputManager.GetMouseMovement().x / Tga2D::CEngine::GetInstance()->GetWindowSize().x,
		myInputManager.GetMouseMovement().y / Tga2D::CEngine::GetInstance()->GetWindowSize().y
	};
	myCamera.Move(CU::Vector3f(
		mouseMovement.x*0.f,
		mouseMovement.y,
		0.f
	) * 20.f);
}

void CGameWorld::ZoomCamera()
{
	myCamera.Move(CU::Vector3f(
		0.f,
		0.f,
		myInputManager.GetScroll()
	) * myTimer.GetDeltaTime() * 300.f);

	SetCursorState(CursorState::SizeNS);
}

void CGameWorld::MoveSelectedObjectZ()
{
	if (mySelectedObject->myType != ObjectType::EditorSprite &&
		mySelectedObject->myType != ObjectType::EditorAnimation)
	{
		return;
	}

	if (mySelectedObject->myLocked != 0.f)
	{
		return;
	}

	mySelectedObject->Move(
		CU::Vector3f(
			0.f,
			0.f,
			-myInputManager.GetScroll()
		) * myTimer.GetDeltaTime() * 200.f
	);


	SetCursorState(CursorState::SizeNS);
	myInspector.UpdateSelectedObjectValues();
}

void CGameWorld::MoveSelectedObjectXY()
{
	static float r = static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().x) / Tga2D::CEngine::GetInstance()->GetWindowSize().y;
	CU::Vector2f mouseMovement = {
		(myInputManager.GetMouseMovement().x*r) / Tga2D::CEngine::GetInstance()->GetWindowSize().x,
		-myInputManager.GetMouseMovement().y / Tga2D::CEngine::GetInstance()->GetWindowSize().y
	};
	mouseMovement *= 2.f;

	if (mySpecialMode == SpecialMode::MultiSelect)
	{
		for (Object* enemy : myMultiSelection)
		{
			enemy->Move(
				CU::Vector3f(
					0.f,
					mouseMovement.y,
					0.f
				) * mySelectedObject->GetPoint().z
			);
		}
	}
	else
	{
		if (mySelectedObject->myLocked != 0.f)
		{
			return;
		}

		if (myInputManager.IsKeyDown(CU::Key_V))
		{
			CU::Vector2f mousePoint = {
				myInputManager.GetMousePosition().x / Tga2D::CEngine::GetInstance()->GetWindowSize().x,
				myInputManager.GetMousePosition().y / Tga2D::CEngine::GetInstance()->GetWindowSize().y
			};
			mousePoint.x = (-1.f + 2.f*mousePoint.x) * r;
			mousePoint.y = -(-1.f + 2.f*mousePoint.y);
			mousePoint *= mySelectedObject->GetPoint().z;
			mousePoint += CU::Vector2f(myCamera.GetPosition());
			mySelectedObject->SetPosition(
				{
					std::roundf(mousePoint.x*10.f) / 10.f,
					std::roundf(mousePoint.y*10.f) / 10.f,
					mySelectedObject->GetPosition().z
				}
			);
		}
		else if(myInputManager.IsKeyDown(CU::Key_S))
		{
			mySelectedObject->Move(
				CU::Vector3f(
					mouseMovement.x,
					mouseMovement.y,
					0.f
				) * mySelectedObject->GetPoint().z, true
			);
		}
		else
		{
			mySelectedObject->Move(
				CU::Vector3f(
					mouseMovement.x,
					mouseMovement.y,
					0.f
				) * mySelectedObject->GetPoint().z
			);
		}

		myInspector.UpdateSelectedObjectValues();
	}

	SetCursorState(CursorState::SizeAll);
}

void CGameWorld::RotateSelectedObject()
{
	static float r = static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().x) / Tga2D::CEngine::GetInstance()->GetWindowSize().y;
	CU::Vector3f currentPosition = myInputManager.GetMousePosition();
	CU::Vector3f previousPosition = (myInputManager.GetMousePosition() - myInputManager.GetMouseMovement());

	currentPosition.x /= Tga2D::CEngine::GetInstance()->GetWindowSize().x;
	currentPosition.y /= Tga2D::CEngine::GetInstance()->GetWindowSize().y;
	previousPosition.x /= Tga2D::CEngine::GetInstance()->GetWindowSize().x;
	previousPosition.y /= Tga2D::CEngine::GetInstance()->GetWindowSize().y;
	
	CU::Vector2f ca1 = CU::Vector2f(currentPosition - mySelectedObject->GetPoint());
	ca1.x *= r;
	CU::Vector2f ca2 = CU::Vector2f(previousPosition - mySelectedObject->GetPoint());
	ca2.x *= r;

	float currentAngle = ca1.GetAngle();
	float previousAngle = ca2.GetAngle();
	mySelectedObject->Rotate(currentAngle - previousAngle);
	
	myInspector.UpdateSelectedObjectValues();
}

void CGameWorld::ScaleSelectedObject()
{
	CU::Vector2f mouseMovement = {
		myInputManager.GetMouseMovement().x / Tga2D::CEngine::GetInstance()->GetWindowSize().x,
		-myInputManager.GetMouseMovement().y / Tga2D::CEngine::GetInstance()->GetWindowSize().y
	};

	mySelectedObject->Scale(CU::Vector2f(
		mouseMovement.x * mySelectedObject->GetPoint().z,
		mouseMovement.y * mySelectedObject->GetPoint().z
	) * 2.f);

	SetCursorState(CursorState::SizeNESW);
	myInspector.UpdateSelectedObjectValues();
}

void CGameWorld::FocusOnSelectedObject()
{
	myCamera.SetPosition(mySelectedObject->GetPosition() +
		CU::Vector3f(0.f, 0.f, -1.f)/2.f
	);
}

void CGameWorld::MousePressedSelectedObject()
{
	CU::Vector2f mousePoint = {
		myInputManager.GetMousePosition().x / Tga2D::CEngine::GetInstance()->GetWindowSize().x,
		myInputManager.GetMousePosition().y / Tga2D::CEngine::GetInstance()->GetWindowSize().y
	};
	if (mySelectedObject->myType == ObjectType::EditorCurve)
	{
		mySelectedObject->myCurve->SelectPoint(mousePoint);
	}
}

void CGameWorld::MouseReleasedSelectedObject()
{
	if (mySelectedObject->myType == ObjectType::EditorCurve)
	{
		mySelectedObject->myCurve->ReleasePoint();
	}
}

void CGameWorld::StartMultiSelect()
{
	CU::Vector2f mousePoint = {
		myInputManager.GetMousePosition().x / Tga2D::CEngine::GetInstance()->GetWindowSize().x,
		myInputManager.GetMousePosition().y / Tga2D::CEngine::GetInstance()->GetWindowSize().y
	};
	myMultiSelectionStart = mousePoint;
	myIsMultiSelecting = true;
}

void CGameWorld::EndMultiSelect()
{
	CU::Vector2f mousePoint = {
		myInputManager.GetMousePosition().x / Tga2D::CEngine::GetInstance()->GetWindowSize().x,
		myInputManager.GetMousePosition().y / Tga2D::CEngine::GetInstance()->GetWindowSize().y
	};
	myIsMultiSelecting = false;
	
	CU::Vector2f endSelection = mousePoint;

	CU::Vector2f smin(
		std::fminf(myMultiSelectionStart.x, endSelection.x),
		std::fminf(myMultiSelectionStart.y, endSelection.y)
	);
	CU::Vector2f smax(
		std::fmaxf(myMultiSelectionStart.x, endSelection.x),
		std::fmaxf(myMultiSelectionStart.y, endSelection.y)
	);

	myMultiSelection.RemoveAll();

	for (Object* object : myObjectsLD)
	{
		if (object->myType != ObjectType::EditorEnemy)
		{
			continue;
		}

		const CU::Vector3f& p = object->GetPoint();

		if (p.x >= smin.x && p.x <= smax.x && p.y >= smin.y && p.y <= smax.y)
		{
			myMultiSelection.Add(object);
		}
	}

	mySpecialMode = SpecialMode::MultiSelect;
}

void CGameWorld::SelectObject()
{
	CU::Vector2f mousePoint = {
		myInputManager.GetMousePosition().x / Tga2D::CEngine::GetInstance()->GetWindowSize().x,
		myInputManager.GetMousePosition().y / Tga2D::CEngine::GetInstance()->GetWindowSize().y
	};

	switch( myMode )
	{
	case EditorMode::Normal:
	case EditorMode::Graphics:
		for( Object* object : myObjects )
		{
			object->CalculatePoint(myCamera);
		}
		std::sort(myObjects.begin(), myObjects.end(), [](Object* a, Object* b)
		{
			return a->GetPoint().z > b->GetPoint().z;
		});
		for( unsigned short i = myObjects.Size(); i > 0; --i )
		{
			Object* object = myObjects[i - 1];
			if (object->GetPoint().z > 0.f && object->IsInside(mousePoint))
			{
				SetSelectedObject(object);
				break;
			}
		}
		break;
	case EditorMode::LevelDesign:
		for( Object* object : myObjectsLD )
		{
			object->CalculatePoint(myCamera);
		}
		std::sort(myObjectsLD.begin(), myObjectsLD.end(), [](Object* a, Object* b)
		{
			return a->GetPoint().z > b->GetPoint().z;
		});
		for( unsigned short i = myObjectsLD.Size(); i > 0; --i )
		{
			Object* object = myObjectsLD[i - 1];
			if (object->GetPoint().z > 0.f && object->IsInside(mousePoint))
			{
				SetSelectedObject(object);
				break;
			}
		}
		for( Object* object : myButtons )
		{
			object->CalculatePoint(myCamera);
		}
		std::sort(myButtons.begin(), myButtons.end(), [](Object* a, Object* b)
		{
			return a->GetPoint().z > b->GetPoint().z;
		});
		for( unsigned short i = myButtons.Size(); i > 0; --i )
		{
			Object* object = myButtons[i - 1];
			if (object->GetPoint().z > 0.f && object->IsInside(mousePoint))
			{
				SetSelectedObject(object);
				break;
			}
		}
		break;
	}

	myInspector.UpdateSelectedObjectValues();
}

void CGameWorld::SetSelectedObject(Object * o)
{
	myInspector.DeselectObject();
	mySelectedObject = o;
	myInspector.SelectObject(o);
}

void CGameWorld::DeselectObject()
{
	myMultiSelection.RemoveAll();
	myIsMultiSelecting = false;
	mySpecialMode = SpecialMode::None;

	mySelectedObject = nullptr;
	myInspector.DeselectObject();
}

void CGameWorld::DeleteSelectedObject()
{
	if( mySelectedObject != nullptr )
	{
		myObjects.Delete(mySelectedObject);
		myObjectsLD.Delete(mySelectedObject);
		myButtons.Delete(mySelectedObject);
		DeselectObject();
	}
}

void CGameWorld::DuplicateSelectedObject()
{
	if( mySelectedObject == nullptr )
	{
		return;
	}

	Object* object = mySelectedObject;
	if( mySelectedObject->myType == ObjectType::EditorSprite )
	{
		EditorSprite& s = *mySelectedObject->mySprite;
		mySelectedObject = AddSprite(s.GetPath().c_str(), s.GetPosition(), s.GetScale(), s.GetRotation(), s.GetTint());
		mySelectedObject->mySprite->SetUVScale(s.GetUVScale());
	}
	else if( mySelectedObject->myType == ObjectType::EditorAnimation )
	{
		EditorAnimation& a = *mySelectedObject->myAnimation;
		mySelectedObject = AddAnimation(a.GetPath().c_str(), a.GetPosition(), a.GetScale(), a.GetRotation(), a.GetTint());
		mySelectedObject->myAnimation->Setup(a.GetRows(), a.GetCols(), a.GetFrames(), a.GetOffset());
		mySelectedObject->myAnimation->SetDuration(a.GetDuration());
	}
	else if (mySelectedObject->myType == ObjectType::EditorCircleCollider)
	{
		EditorCircleCollider& c = *mySelectedObject->myCircleCollider;
		mySelectedObject = AddCircleCollider(c.GetPosition(), c.GetRadius());
	}
	else if (mySelectedObject->myType == ObjectType::EditorLineCollider)
	{
		EditorLineCollider& l = *mySelectedObject->myLineCollider;
		mySelectedObject = AddLineCollider(l.GetPosition(), l.GetLength(), l.GetRotation());
	}
	else if( mySelectedObject->myType == ObjectType::EditorEnemy )
	{
		EditorEnemy& e = *mySelectedObject->myEnemy;
		mySelectedObject = AddEnemy(e.GetPosition());
		mySelectedObject->myEnemy->SetEnemyClass(e.GetEnemyClass());
	}
	else if (mySelectedObject->myType == ObjectType::EditorCurve)
	{
		EditorCurve& c = *mySelectedObject->myCurve;
		mySelectedObject = AddCurve(c.GetPosition());
		mySelectedObject->myCurve->PopPoint();
		mySelectedObject->myCurve->PopPoint();
		mySelectedObject->myCurve->AddPoints(c.GetPoints());
		mySelectedObject->myCurve->SetEnemyClass(c.GetEnemyClass());
		mySelectedObject->myCurve->SetAmount(c.GetAmount());
		mySelectedObject->myCurve->SetInterval(c.GetInterval());
		mySelectedObject->myCurve->SetSpeed(c.GetSpeed());
	}

	mySelectedObject->myName = object->myName;
	mySelectedObject->myTag = object->myTag;
	myInspector.DeselectObject();
	myInspector.SelectObject(mySelectedObject);
}

void CGameWorld::FlipCurve()
{
	if( mySelectedObject == nullptr )
	{
		return;
	}

	if (mySelectedObject->myType == ObjectType::EditorCurve)
	{
		mySelectedObject->myCurve->FlipPoints();
	}
}

void CGameWorld::MirrorCurve()
{
	if( mySelectedObject == nullptr )
	{
		return;
	}

	if (mySelectedObject->myType == ObjectType::EditorCurve)
	{
		mySelectedObject->myCurve->MirrorPoints();
	}
}

void CGameWorld::SetCursorState(CursorState aCursorState)
{
	switch( aCursorState )
	{
	case CursorState::Arrow:
		myCursorState = IDC_ARROW;
		break;
	case CursorState::Hand:
		myCursorState = IDC_HAND;
		break;
	case CursorState::SizeAll:
		myCursorState = IDC_SIZEALL;
		break;
	case CursorState::SizeNESW:
		myCursorState = IDC_SIZENESW;
		break;
	case CursorState::SizeNS:
		myCursorState = IDC_SIZENS;
		break;
	case CursorState::Cross:
		myCursorState = IDC_CROSS;
		break;
	case CursorState::No:
		myCursorState = IDC_NO;
		break;
	case CursorState::Wait:
		myCursorState = IDC_WAIT;
		break;
	default:
		myCursorState = IDC_ARROW;
		break;
	}
}

void CGameWorld::EmptyArrays()
{
	DeselectObject();
	myObjects.DeleteAll();
	myButtons.DeleteAll();
	myObjectsLD.DeleteAll();
	myBuffer.RemoveAll();
}

void CGameWorld::DrawLine(const CommonUtilities::Vector3f & aFrom, const CommonUtilities::Vector3f & aTo)
{
	myLine->SetFrom(aFrom.x, aFrom.y);
	myLine->SetTo(aTo.x, aTo.y);
	myLine->Render();
}

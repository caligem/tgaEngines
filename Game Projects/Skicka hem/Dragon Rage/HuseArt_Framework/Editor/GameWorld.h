#pragma once

#include <vector>

namespace Tga2D
{
	class CSprite;
	class CText;
	class CLinePrimitive;
}

namespace CommonUtilities
{
	class InputManager;
	class Timer;
}
#include <Camera.h>
#include <GrowingArray.h>

#include "Inspector.h"
#include "Object.h"

enum class EditorMode
{
	Normal,
	LevelDesign,
	Graphics
};

enum class SpecialMode
{
	MultiSelect,
	None
};

enum class CursorState
{
	Arrow,
	Hand,
	SizeAll,
	SizeNESW,
	SizeNS,
	Cross,
	No,
	Wait
};

class CGameWorld
{
public:
	CGameWorld(const CommonUtilities::InputManager&, const CommonUtilities::Timer&); 
	~CGameWorld();

	void Init();
	void Update(); 
	void Render(); 

	inline LPWSTR GetCursorState() { return myCursorState; }

private:
	void RenderBuffer();
	void RenderNormalMode();
	void RenderLevelDesignMode();
	void RenderGraphicsMode();

	void HandleInput();
	void HandleInputNormalMode();
	void HandleInputLevelDesignMode();
	void HandleInputGraphicsMode();


	Object* AddSprite(const char* aPath, const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f }, const CommonUtilities::Vector2f& aScale = { 1.f, 1.f }, float aRotation = 0.f, const CommonUtilities::Vector4f& aTint = { 1.f, 1.f, 1.f, 1.f });
	Object* AddAnimation(const char* aPath, const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f }, const CommonUtilities::Vector2f& aScale = { 1.f, 1.f }, float aRotation = 0.f, const CommonUtilities::Vector4f& aTint = { 1.f, 1.f, 1.f, 1.f });
	Object* AddCircleCollider(const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f }, float aRadius = 1.f);
	Object* AddLineCollider(const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f }, float aLength = 1.f, float aRotation = 0.f);
	Object* AddEnemy(const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f });
	Object* AddButton(const char* aPath, const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f }, const CommonUtilities::Vector2f& aScale = { 1.f, 1.f }, float aRotation = 0.f, const CommonUtilities::Vector4f& aTint = { 1.f, 1.f, 1.f, 1.f });
	Object* AddCurve(const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f });

	void OpenEnemyClassEditor();

	void ImportSprite();
	void ImportAnimation();
	void ImportButton();

	void OpenLevel();
	void SaveLevel();

	void PanCamera();
	void ZoomCamera();

	void MoveSelectedObjectZ();
	void MoveSelectedObjectXY();
	void RotateSelectedObject();
	void ScaleSelectedObject();
	void FocusOnSelectedObject();

	void MousePressedSelectedObject();
	void MouseReleasedSelectedObject();
	void StartMultiSelect();
	void EndMultiSelect();
	void SelectObject();
	void SetSelectedObject(Object* o);
	void DeselectObject();
	void DeleteSelectedObject();
	void DuplicateSelectedObject();
	void FlipCurve();
	void MirrorCurve();

	void SetCursorState(CursorState aCursorState);

	void EmptyArrays();

	void DrawLine(const CommonUtilities::Vector3f& aFrom, const CommonUtilities::Vector3f& aTo);

	EditorMode myMode;
	Tga2D::CText* myModeText;
	Tga2D::CLinePrimitive* myLine;

	Inspector myInspector;

	const CommonUtilities::InputManager& myInputManager;
	const CommonUtilities::Timer& myTimer;

	CommonUtilities::Camera myCamera;

	CommonUtilities::GrowingArray<Object*> myObjects;
	CommonUtilities::GrowingArray<Object*> myButtons;
	CommonUtilities::GrowingArray<Object*> myObjectsLD;
	CommonUtilities::GrowingArray<Object*> myBuffer;
	Object* mySelectedObject;

	bool myHideGizmos = false;

	LPWSTR myCursorState = IDC_ARROW;

	bool myIsMultiSelecting = false;
	CommonUtilities::Vector2f myMultiSelectionStart;
	CommonUtilities::GrowingArray<Object*> myMultiSelection;
	SpecialMode mySpecialMode = SpecialMode::None;
};

#include "stdafx.h"
#include "Inspector.h"

#include <iostream>
#include <CommCtrl.h>

#include "..\Launcher_Editor\resource.h"

#include <tga2d\engine.h>
#include <tga2d\windows\windows_window.h>
#include <tga2d\math\common_math.h>
#include <Sprite.h>

#include <GrowingArray.h>
#include <Vector.h>
#include <Mathf.h>
#include <FileDialog.h>

#include "Object.h"

namespace CU = CommonUtilities;

#define BUFFER_SIZE 256
#define BG DKGRAY_BRUSH
#define BGEDIT LTGRAY_BRUSH

static CU::GrowingArray<ControlConnection*> locConnections;

Inspector::Inspector()
{
	myDlg = NULL;
	locConnections.Init(10);
}

Inspector::~Inspector()
{
	locConnections.DeleteAll();
}

void Inspector::UpdateSelectedObjectValues()
{
	for( ControlConnection* conn : locConnections )
	{
		switch( conn->myIDC )
		{
		case IDC_TINT_R:
		case IDC_TINT_G:
		case IDC_TINT_B:
		case IDC_TINT_A:
			SendMessage(conn->myHwnd, TBM_SETPOS, TRUE, std::atoi(conn->myOnUpdate().c_str()));
			break;
		case IDC_LOAD_CLASS:
		case IDC_SAVE_CLASS:
			break;
		default:
			SetWindowTextA(
				conn->myHwnd,
				conn->myOnUpdate().c_str()
			);
			break;
		}
	}
}

INT_PTR CALLBACK DlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(hDlg);
	UNREFERENCED_PARAMETER(lParam);

	switch( message )
	{
	case WM_HSCROLL:
		switch( LOWORD(wParam) )
		{
		case TB_THUMBTRACK:
		case TB_PAGEUP:
		case TB_PAGEDOWN:
			for( ControlConnection* conn : locConnections )
			{
				if( conn->myHwnd == (HWND)lParam )
				{
					conn->myOnChange(std::to_string(static_cast<float>(SendMessage(conn->myHwnd, TBM_GETPOS, 0, 0)) / 255.f));
				}
			}
			break;
		}
		break;
	case WM_COMMAND:
		if( HIWORD(wParam) == EN_CHANGE )
		{
			for( ControlConnection* conn : locConnections )
			{
				if( conn->myIDC == LOWORD(wParam) )
				{
					char buf[BUFFER_SIZE];
					GetWindowTextA(conn->myHwnd, buf, BUFFER_SIZE);
					conn->myOnChange(buf);
				}
			}
		}
		else if( HIWORD(wParam) == 0 )
		{
			for( ControlConnection* conn : locConnections )
			{
				if( conn->myIDC == LOWORD(wParam) )
				{
					std::string filePath;
					switch( LOWORD(wParam) )
					{
					case IDC_LOAD_CLASS:
					case IDC_SAVE_CLASS:
						conn->myOnChange("");
						break;
					}
				}
			}
		}
		return FALSE;
		break;
	case WM_CTLCOLOREDIT:
		SetBkMode((HDC)wParam, TRANSPARENT);
        SetTextColor((HDC)wParam, RGB(0, 0, 0));
        return (INT_PTR)( (HBRUSH)GetStockObject(BGEDIT) );
	case WM_CTLCOLORSTATIC:
		SetBkMode((HDC)wParam, TRANSPARENT);
        SetTextColor((HDC)wParam, RGB(255, 255, 255));
        return (INT_PTR)( (HBRUSH)GetStockObject(BG) );
	case WM_CTLCOLORDLG:
		return (INT_PTR)GetStockObject(BG);
		break;
	case WM_CLOSE:
		DestroyWindow(hDlg);
		locConnections.DeleteAll();
		return FALSE;
		break;
	}

	return FALSE;
}

void Inspector::SelectObject(Object* aObject)
{
	mySelectedObject = aObject;

	if( mySelectedObject->myType == ObjectType::EditorSprite )
	{
		myDlg = CreateDialog(NULL, MAKEINTRESOURCE(IDD_SPRITE), NULL, DlgProc);
	}
	else if( mySelectedObject->myType == ObjectType::EditorAnimation )
	{
		myDlg = CreateDialog(NULL, MAKEINTRESOURCE(IDD_ANIMATION), NULL, DlgProc);
	}
	else if( mySelectedObject->myType == ObjectType::EditorCircleCollider )
	{
		myDlg = CreateDialog(NULL, MAKEINTRESOURCE(IDD_COLLISION_CIRCLE), NULL, DlgProc);
	}
	else if( mySelectedObject->myType == ObjectType::EditorLineCollider )
	{
		myDlg = CreateDialog(NULL, MAKEINTRESOURCE(IDD_COLLISION_LINE), NULL, DlgProc);
	}
	else if( mySelectedObject->myType == ObjectType::EditorEnemy )
	{
		myDlg = CreateDialog(NULL, MAKEINTRESOURCE(IDD_ENEMY), NULL, DlgProc);
	}
	else if (mySelectedObject->myType == ObjectType::EditorCurve)
	{
		myDlg = CreateDialog(NULL, MAKEINTRESOURCE(IDD_CURVE), NULL, DlgProc);
	}

	if( IsWindow(myDlg) )
	{
		ShowWindow(myDlg, SW_SHOW);
		SetFocus(Tga2D::CEngine::GetInstance()->GetWindow().GetWindowHandle());
		Tga2D::CEngine::GetInstance()->SetInspector(myDlg);

		SetWindowStyle();
	}

	ConnectControl(
		IDC_NAME,
		[=](std::string aValue) { mySelectedObject->myName = aValue; },
		[=]()->std::string { return mySelectedObject->myName; }
	);
	ConnectControl(
		IDC_TAG,
		[=](std::string aValue) { mySelectedObject->myTag = aValue; },
		[=]()->std::string{ return mySelectedObject->myTag; }
	);
	ConnectControl(
		IDC_LOCKED,
		[=](std::string aValue) { mySelectedObject->myLocked = static_cast<float>(std::atof(aValue.c_str())); },
		[=]()->std::string{ return std::to_string(mySelectedObject->myLocked); }
	);
	ConnectControl(
		IDC_LOCKED2,
		[=](std::string aValue) { mySelectedObject->myLocked = static_cast<float>(std::atof(aValue.c_str())); },
		[=]()->std::string{ return std::to_string(mySelectedObject->myLocked); }
	);

	if( mySelectedObject->myType == ObjectType::EditorSprite )
	{
		SelectedSprite();
	}
	else if( mySelectedObject->myType == ObjectType::EditorAnimation )
	{
		SelectedAnimation();
	}
	else if( mySelectedObject->myType == ObjectType::EditorCircleCollider )
	{
		SelectedCirleCollider();
	}
	else if( mySelectedObject->myType == ObjectType::EditorLineCollider )
	{
		SelectedLineCollider();
	}
	else if( mySelectedObject->myType == ObjectType::EditorEnemy )
	{
		SelectedEnemy();
	}
	else if (mySelectedObject->myType == ObjectType::EditorCurve)
	{
		SelectedCurve();
	}
}

void Inspector::DeselectObject()
{
	mySelectedObject = nullptr;

	DestroyWindow(myDlg);

	locConnections.DeleteAll();
}

void Inspector::SelectedSprite()
{
	ConnectControl(
		IDC_TRANSFORM_PX,
		[=](std::string aValue)
		{
			mySelectedObject->mySprite->SetPosition({
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->mySprite->GetPosition().y,
				mySelectedObject->mySprite->GetPosition().z
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->mySprite->GetPosition().x); }
	);
	ConnectControl(
		IDC_TRANSFORM_PY,
		[=](std::string aValue)
		{
			mySelectedObject->mySprite->SetPosition({
				mySelectedObject->mySprite->GetPosition().x,
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->mySprite->GetPosition().z
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->mySprite->GetPosition().y); }
	);
	ConnectControl(
		IDC_TRANSFORM_PZ,
		[=](std::string aValue)
		{
			mySelectedObject->mySprite->SetPosition({
				mySelectedObject->mySprite->GetPosition().x,
				mySelectedObject->mySprite->GetPosition().y,
				static_cast<float>(std::atof(aValue.c_str()))
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->mySprite->GetPosition().z); }
	);
	ConnectControl(
		IDC_TRANSFORM_SX,
		[=](std::string aValue)
		{
			mySelectedObject->mySprite->SetScale({
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->mySprite->GetScale().y
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->mySprite->GetScale().x); }
	);
	ConnectControl(
		IDC_TRANSFORM_SY,
		[=](std::string aValue)
		{
			mySelectedObject->mySprite->SetScale({
				mySelectedObject->mySprite->GetScale().x,
				static_cast<float>(std::atof(aValue.c_str()))
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->mySprite->GetScale().y); }
	);
	ConnectControl(
		IDC_TRANSFORM_R,
		[=](std::string aValue) { mySelectedObject->mySprite->SetRotation(Tga2D::DegToRad(static_cast<float>(std::atof(aValue.c_str())))); },
		[=]()->std::string{ return std::to_string(Tga2D::RadToDeg(mySelectedObject->mySprite->GetRotation())); }
	);
	ConnectControl(
		IDC_TINT_R,
		[=](std::string aValue) {
			mySelectedObject->mySprite->SetTint({
				CU::Clamp(static_cast<float>(std::atof(aValue.c_str())), 0.f, 1.f),
				mySelectedObject->mySprite->GetTint().y,
				mySelectedObject->mySprite->GetTint().z,
				mySelectedObject->mySprite->GetTint().w
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->mySprite->GetTint().x*255.f); }
	);
	ConnectControl(
		IDC_TINT_G,
		[=](std::string aValue) {
			mySelectedObject->mySprite->SetTint({
				mySelectedObject->mySprite->GetTint().x,
				CU::Clamp(static_cast<float>(std::atof(aValue.c_str())), 0.f, 1.f),
				mySelectedObject->mySprite->GetTint().z,
				mySelectedObject->mySprite->GetTint().w
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->mySprite->GetTint().y*255.f); }
	);
	ConnectControl(
		IDC_TINT_B,
		[=](std::string aValue) {
			mySelectedObject->mySprite->SetTint({
				mySelectedObject->mySprite->GetTint().x,
				mySelectedObject->mySprite->GetTint().y,
				CU::Clamp(static_cast<float>(std::atof(aValue.c_str())), 0.f, 1.f),
				mySelectedObject->mySprite->GetTint().w
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->mySprite->GetTint().z*255.f); }
	);
	ConnectControl(
		IDC_TINT_A,
		[=](std::string aValue) {
			mySelectedObject->mySprite->SetTint({
				mySelectedObject->mySprite->GetTint().x,
				mySelectedObject->mySprite->GetTint().y,
				mySelectedObject->mySprite->GetTint().z,
				CU::Clamp(static_cast<float>(std::atof(aValue.c_str())), 0.f, 1.f),
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->mySprite->GetTint().w*255.f); }
	);
	ConnectControl(
		IDC_TRANSFORM_UVX,
		[=](std::string aValue) {
			mySelectedObject->mySprite->SetUVScale({
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->mySprite->GetUVScale().y
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->mySprite->GetUVScale().x); }
	);
	ConnectControl(
		IDC_TRANSFORM_UVY,
		[=](std::string aValue) {
			mySelectedObject->mySprite->SetUVScale({
				mySelectedObject->mySprite->GetUVScale().x,
				static_cast<float>(std::atof(aValue.c_str()))
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->mySprite->GetUVScale().y); }
	);
}

void Inspector::SelectedAnimation()
{
	ConnectControl(
		IDC_TRANSFORM_PX,
		[=](std::string aValue)
		{
			mySelectedObject->myAnimation->SetPosition({
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->myAnimation->GetPosition().y,
				mySelectedObject->myAnimation->GetPosition().z
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetPosition().x); }
	);
	ConnectControl(
		IDC_TRANSFORM_PY,
		[=](std::string aValue)
		{
			mySelectedObject->myAnimation->SetPosition({
				mySelectedObject->myAnimation->GetPosition().x,
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->myAnimation->GetPosition().z
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetPosition().y); }
	);
	ConnectControl(
		IDC_TRANSFORM_PZ,
		[=](std::string aValue)
		{
			mySelectedObject->myAnimation->SetPosition({
				mySelectedObject->myAnimation->GetPosition().x,
				mySelectedObject->myAnimation->GetPosition().y,
				static_cast<float>(std::atof(aValue.c_str()))
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetPosition().z); }
	);
	ConnectControl(
		IDC_TRANSFORM_SX,
		[=](std::string aValue)
		{
			mySelectedObject->myAnimation->SetScale({
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->myAnimation->GetScale().y
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetScale().x); }
	);
	ConnectControl(
		IDC_TRANSFORM_SY,
		[=](std::string aValue)
		{
			mySelectedObject->myAnimation->SetScale({
				mySelectedObject->myAnimation->GetScale().x,
				static_cast<float>(std::atof(aValue.c_str()))
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetScale().y); }
	);
	ConnectControl(
		IDC_TRANSFORM_R,
		[=](std::string aValue) { mySelectedObject->myAnimation->SetRotation(Tga2D::DegToRad(static_cast<float>(std::atof(aValue.c_str())))); },
		[=]()->std::string{ return std::to_string(Tga2D::RadToDeg(mySelectedObject->myAnimation->GetRotation())); }
	);
	ConnectControl(
		IDC_TINT_R,
		[=](std::string aValue) {
			mySelectedObject->myAnimation->SetTint({
				CU::Clamp(static_cast<float>(std::atof(aValue.c_str())), 0.f, 1.f),
				mySelectedObject->myAnimation->GetTint().y,
				mySelectedObject->myAnimation->GetTint().z,
				mySelectedObject->myAnimation->GetTint().w
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetTint().x*255.f); }
	);
	ConnectControl(
		IDC_TINT_G,
		[=](std::string aValue) {
			mySelectedObject->myAnimation->SetTint({
				mySelectedObject->myAnimation->GetTint().x,
				CU::Clamp(static_cast<float>(std::atof(aValue.c_str())), 0.f, 1.f),
				mySelectedObject->myAnimation->GetTint().z,
				mySelectedObject->myAnimation->GetTint().w
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetTint().y*255.f); }
	);
	ConnectControl(
		IDC_TINT_B,
		[=](std::string aValue) {
			mySelectedObject->myAnimation->SetTint({
				mySelectedObject->myAnimation->GetTint().x,
				mySelectedObject->myAnimation->GetTint().y,
				CU::Clamp(static_cast<float>(std::atof(aValue.c_str())), 0.f, 1.f),
				mySelectedObject->myAnimation->GetTint().w
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetTint().z*255.f); }
	);
	ConnectControl(
		IDC_TINT_A,
		[=](std::string aValue) {
			mySelectedObject->myAnimation->SetTint({
				mySelectedObject->myAnimation->GetTint().x,
				mySelectedObject->myAnimation->GetTint().y,
				mySelectedObject->myAnimation->GetTint().z,
				CU::Clamp(static_cast<float>(std::atof(aValue.c_str())), 0.f, 1.f),
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetTint().w*255.f); }
	);
	ConnectControl(
		IDC_ANIMATION_COLS,
		[=](std::string aValue) {
			mySelectedObject->myAnimation->SetCols(std::atoi(aValue.c_str()));
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetCols()); }
	);
	ConnectControl(
		IDC_ANIMATION_ROWS,
		[=](std::string aValue) {
			mySelectedObject->myAnimation->SetRows(std::atoi(aValue.c_str()));
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetRows()); }
	);
	ConnectControl(
		IDC_ANIMATION_FRAMES,
		[=](std::string aValue) {
			mySelectedObject->myAnimation->SetFrames(std::atoi(aValue.c_str()));
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetFrames()); }
	);
	ConnectControl(
		IDC_ANIMATION_OFFSET,
		[=](std::string aValue) {
			mySelectedObject->myAnimation->SetOffset(std::atoi(aValue.c_str()));
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetOffset()); }
	);
	ConnectControl(
		IDC_ANIMATION_DURATION,
		[=](std::string aValue) {
			mySelectedObject->myAnimation->SetDuration(static_cast<float>(std::atof(aValue.c_str())));
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myAnimation->GetDuration()); }
	);
}

void Inspector::SelectedCirleCollider()
{
	ConnectControl(
		IDC_TRANSFORM_PX,
		[=](std::string aValue)
		{
			mySelectedObject->myCircleCollider->SetPosition({
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->myCircleCollider->GetPosition().y,
				mySelectedObject->myCircleCollider->GetPosition().z
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myCircleCollider->GetPosition().x); }
	);
	ConnectControl(
		IDC_TRANSFORM_PY,
		[=](std::string aValue)
		{
			mySelectedObject->myCircleCollider->SetPosition({
				mySelectedObject->myCircleCollider->GetPosition().x,
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->myCircleCollider->GetPosition().z
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myCircleCollider->GetPosition().y); }
	);
	ConnectControl(
		IDC_TRANSFORM_PZ,
		[=](std::string aValue)
		{
			mySelectedObject->myCircleCollider->SetPosition({
				mySelectedObject->myCircleCollider->GetPosition().x,
				mySelectedObject->myCircleCollider->GetPosition().y,
				static_cast<float>(std::atof(aValue.c_str()))
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myCircleCollider->GetPosition().z); }
	);
	ConnectControl(
		IDC_COLLIDER_CIRCLE_RADIUS,
		[=](std::string aValue)
		{
			mySelectedObject->myCircleCollider->SetRadius(static_cast<float>(std::atof(aValue.c_str())));
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myCircleCollider->GetRadius()); }
	);
}

void Inspector::SelectedLineCollider()
{
	ConnectControl(
		IDC_TRANSFORM_PX,
		[=](std::string aValue)
		{
			mySelectedObject->myLineCollider->SetPosition({
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->myLineCollider->GetPosition().y,
				mySelectedObject->myLineCollider->GetPosition().z
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myLineCollider->GetPosition().x); }
	);
	ConnectControl(
		IDC_TRANSFORM_PY,
		[=](std::string aValue)
		{
			mySelectedObject->myLineCollider->SetPosition({
				mySelectedObject->myLineCollider->GetPosition().x,
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->myLineCollider->GetPosition().z
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myLineCollider->GetPosition().y); }
	);
	ConnectControl(
		IDC_TRANSFORM_PZ,
		[=](std::string aValue)
		{
			mySelectedObject->myLineCollider->SetPosition({
				mySelectedObject->myLineCollider->GetPosition().x,
				mySelectedObject->myLineCollider->GetPosition().y,
				static_cast<float>(std::atof(aValue.c_str()))
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myLineCollider->GetPosition().z); }
	);
	ConnectControl(
		IDC_TRANSFORM_R,
		[=](std::string aValue) { mySelectedObject->myLineCollider->SetRotation(Tga2D::DegToRad(static_cast<float>(std::atof(aValue.c_str())))); },
		[=]()->std::string{ return std::to_string(Tga2D::RadToDeg(mySelectedObject->myLineCollider->GetRotation())); }
	);
	ConnectControl(
		IDC_COLLIDER_LINE_LENGTH,
		[=](std::string aValue)
		{
			mySelectedObject->myLineCollider->SetLength(static_cast<float>(std::atof(aValue.c_str())));
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myLineCollider->GetLength()); }
	);
}

void Inspector::SelectedEnemy()
{
	ConnectControl(
		IDC_TRANSFORM_PX,
		[=](std::string aValue)
		{
			mySelectedObject->myEnemy->SetPosition({
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->myEnemy->GetPosition().y,
				mySelectedObject->myEnemy->GetPosition().z
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myEnemy->GetPosition().x); }
	);
	ConnectControl(
		IDC_TRANSFORM_PY,
		[=](std::string aValue)
		{
			mySelectedObject->myEnemy->SetPosition({
				mySelectedObject->myEnemy->GetPosition().x,
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->myEnemy->GetPosition().z
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myEnemy->GetPosition().y); }
	);
	ConnectControl(
		IDC_TRANSFORM_PZ,
		[=](std::string aValue)
		{
			mySelectedObject->myEnemy->SetPosition({
				mySelectedObject->myEnemy->GetPosition().x,
				mySelectedObject->myEnemy->GetPosition().y,
				static_cast<float>(std::atof(aValue.c_str()))
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myEnemy->GetPosition().z); }
	);
	ConnectControl(
		IDC_LOAD_CLASS,
		[=](std::string aValue)
		{
			mySelectedObject->myEnemy->LoadEnemyClass();
		},
		[=]()->std::string{ return ""; }
	);
}

void Inspector::SelectedCurve()
{
	ConnectControl(
		IDC_TRANSFORM_PX,
		[=](std::string aValue)
		{
			mySelectedObject->myCurve->SetPosition({
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->myCurve->GetPosition().y,
				mySelectedObject->myCurve->GetPosition().z
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myCurve->GetPosition().x); }
	);
	ConnectControl(
		IDC_TRANSFORM_PY,
		[=](std::string aValue)
		{
			mySelectedObject->myCurve->SetPosition({
				mySelectedObject->myCurve->GetPosition().x,
				static_cast<float>(std::atof(aValue.c_str())),
				mySelectedObject->myCurve->GetPosition().z
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myCurve->GetPosition().y); }
	);
	ConnectControl(
		IDC_TRANSFORM_PZ,
		[=](std::string aValue)
		{
			mySelectedObject->myCurve->SetPosition({
				mySelectedObject->myCurve->GetPosition().x,
				mySelectedObject->myCurve->GetPosition().y,
				static_cast<float>(std::atof(aValue.c_str()))
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myCurve->GetPosition().z); }
	);
	ConnectControl(
		IDC_LOAD_CLASS,
		[=](std::string aValue)
		{
			mySelectedObject->myCurve->LoadEnemyClass();
		},
		[=]()->std::string{ return ""; }
	);
	ConnectControl(
		IDC_MOVEMENT_AMOUNT,
		[=](std::string aValue)
		{
			mySelectedObject->myCurve->SetAmount(std::atoi(aValue.c_str()));
		},
		[=]()->std::string { return std::to_string(mySelectedObject->myCurve->GetAmount()); }
	);
	ConnectControl(
		IDC_MOVEMENT_INTERVAL,
		[=](std::string aValue)
		{
			mySelectedObject->myCurve->SetInterval(static_cast<float>(std::atof(aValue.c_str())));
		},
		[=]()->std::string { return std::to_string(mySelectedObject->myCurve->GetInterval()); }
	);
	ConnectControl(
		IDC_MOVEMENT_SPEED,
		[=](std::string aValue)
		{
			mySelectedObject->myCurve->SetSpeed(static_cast<float>(std::atof(aValue.c_str())));
		},
		[=]()->std::string { return std::to_string(mySelectedObject->myCurve->GetSpeed()); }
	);
}

void Inspector::OpenEnemyClassEditor()
{
	mySelectedObject = new Object();
	mySelectedObject->myType = ObjectType::EditorEnemyClass;
	mySelectedObject->myEnemyClass = new EditorEnemyClass();

	myDlg = CreateDialog(NULL, MAKEINTRESOURCE(IDD_ENEMY_CLASS), NULL, DlgProc);

	if( IsWindow(myDlg) )
	{
		ShowWindow(myDlg, SW_SHOW);
		SetFocus(myDlg);
		Tga2D::CEngine::GetInstance()->SetInspector(myDlg);

		SetWindowStyle();
	}

	ConnectControl(
		IDC_CLASS_NAME,
		[=](std::string aValue)
		{
			mySelectedObject->myEnemyClass->SetName(aValue);
		},
		[=]()->std::string { return mySelectedObject->myEnemyClass->GetName(); }
	);
	ConnectControl(
		IDC_HP,
		[=](std::string aValue)
		{
			mySelectedObject->myEnemyClass->SetHP(static_cast<float>(std::atof(aValue.c_str())));
		},
		[=]()->std::string { return std::to_string(mySelectedObject->myEnemyClass->GetHP()); }
	);
	ConnectControl(
		IDC_ATTACK_DMG,
		[=](std::string aValue)
		{
			mySelectedObject->myEnemyClass->SetDMG(static_cast<float>(std::atof(aValue.c_str())));
		},
		[=]()->std::string { return std::to_string(mySelectedObject->myEnemyClass->GetDMG()); }
	);
	ConnectControl(
		IDC_ATTACK_TYPE,
		[=](std::string aValue)
		{
			mySelectedObject->myEnemyClass->SetType(aValue.c_str());
		},
		[=]()->std::string { return mySelectedObject->myEnemyClass->GetType(); }
	);
	ConnectControl(
		IDC_ATTACK_ROF,
		[=](std::string aValue)
		{
			mySelectedObject->myEnemyClass->SetROF(static_cast<float>(std::atof(aValue.c_str())));
		},
		[=]()->std::string { return std::to_string(mySelectedObject->myEnemyClass->GetROF()); }
	);
	ConnectControl(
		IDC_ATTACK_SPEED,
		[=](std::string aValue)
		{
			mySelectedObject->myEnemyClass->SetProjectileSpeed(static_cast<float>(std::atof(aValue.c_str())));
		},
		[=]()->std::string { return std::to_string(mySelectedObject->myEnemyClass->GetProjectileSpeed()); }
	);
	ConnectControl(
		IDC_MOVEMENT_SPEED,
		[=](std::string aValue)
		{
			mySelectedObject->myEnemyClass->SetMovementSpeed(static_cast<float>(std::atof(aValue.c_str())));
		},
		[=]()->std::string { return std::to_string(mySelectedObject->myEnemyClass->GetMovementSpeed()); }
	);
	ConnectControl(
		IDC_TINT_R,
		[=](std::string aValue) {
			mySelectedObject->myEnemyClass->SetTint({
				CU::Clamp(static_cast<float>(std::atof(aValue.c_str())), 0.f, 1.f),
				mySelectedObject->myEnemyClass->GetTint().y,
				mySelectedObject->myEnemyClass->GetTint().z,
				1.f
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myEnemyClass->GetTint().x*255.f); }
	);
	ConnectControl(
		IDC_TINT_G,
		[=](std::string aValue) {
			mySelectedObject->myEnemyClass->SetTint({
				mySelectedObject->myEnemyClass->GetTint().x,
				CU::Clamp(static_cast<float>(std::atof(aValue.c_str())), 0.f, 1.f),
				mySelectedObject->myEnemyClass->GetTint().z,
				1.f
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myEnemyClass->GetTint().y*255.f); }
	);
	ConnectControl(
		IDC_TINT_B,
		[=](std::string aValue) {
			mySelectedObject->myEnemyClass->SetTint({
				mySelectedObject->myEnemyClass->GetTint().x,
				mySelectedObject->myEnemyClass->GetTint().y,
				CU::Clamp(static_cast<float>(std::atof(aValue.c_str())), 0.f, 1.f),
				1.f
			});
		},
		[=]()->std::string{ return std::to_string(mySelectedObject->myEnemyClass->GetTint().z*255.f); }
	);
	ConnectControl(
		IDC_LOAD_CLASS,
		[=](std::string aValue)
		{
			if (mySelectedObject->myEnemyClass->LoadEnemyClass())
			{
				UpdateSelectedObjectValues();
			}
		},
		[=]()->std::string{ return "Load Class"; }
	);
	ConnectControl(
		IDC_SAVE_CLASS,
		[=](std::string aValue)
		{
			mySelectedObject->myEnemyClass->SaveEnemyClass();
		},
		[=]()->std::string{ return "Save Class"; }
	);

	UpdateSelectedObjectValues();
}

void Inspector::SetWindowStyle()
{
	EnumChildWindows(myDlg, [](HWND hCtl, LPARAM lParam) -> BOOL
	{
		UNREFERENCED_PARAMETER(lParam);
		
		LONG lStyle = GetWindowLong(hCtl, GWL_STYLE);
		lStyle &= ~(WS_CAPTION | WS_THICKFRAME | WS_MINIMIZE | WS_MAXIMIZE | WS_SYSMENU);
		SetWindowLong(hCtl, GWL_STYLE, lStyle);

		LONG lExStyle = GetWindowLong(hCtl, GWL_EXSTYLE);
		lExStyle &= ~(WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
		SetWindowLong(hCtl, GWL_EXSTYLE, lExStyle);

		SetWindowPos(hCtl, NULL, 0, 0, 0, 0, SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_NOOWNERZORDER);

		return TRUE;
	}, 0);
}

void Inspector::ConnectControl(short aIDC, std::function<void(std::string aValue)> aOnChange, std::function<std::string()> aOnUpdate)
{
	if( !IsWindow(myDlg) )return;
	
	HWND control = GetDlgItem(myDlg, aIDC);
	locConnections.Add(new ControlConnection({ control, aOnChange, aOnUpdate, aIDC }));

	switch( aIDC )
	{
	case IDC_TINT_R:
	case IDC_TINT_G:
	case IDC_TINT_B:
	case IDC_TINT_A:
		SendMessage(control, TBM_SETRANGEMAX, 255, 255);
		break;
	}
}

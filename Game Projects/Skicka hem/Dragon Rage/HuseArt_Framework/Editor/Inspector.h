#pragma once

#include <windows.h>
#include <functional>

struct Object;

struct ControlConnection
{
	HWND myHwnd;
	std::function<void(std::string aValue)> myOnChange;
	std::function<std::string()> myOnUpdate;
	short myIDC;
};

class Inspector
{
public:
	Inspector();
	~Inspector();

	void UpdateSelectedObjectValues();

	void SelectObject(Object* aObject);
	void DeselectObject();

	void OpenEnemyClassEditor();

private:
	void SelectedSprite();
	void SelectedAnimation();
	void SelectedCirleCollider();
	void SelectedLineCollider();
	void SelectedEnemy();
	void SelectedCurve();

	void SetWindowStyle();

	HWND myDlg;

	void ConnectControl(short aIDC, std::function<void(std::string aValue)> aOnChange, std::function<std::string()> aOnUpdate);

	Object* mySelectedObject;
};

#pragma once

#include <string>

#include "EditorSprite.h"
#include "EditorAnimation.h"
#include "EditorCircleCollider.h"
#include "EditorLineCollider.h"
#include "EditorEnemy.h"
#include "EditorEnemyClass.h"
#include "EditorCurve.h"

#include <Macros.h>
#include <Camera.h>
#include <Vector.h>

enum class ObjectType
{
	EditorSprite,
	EditorAnimation,
	EditorCircleCollider,
	EditorLineCollider,
	EditorEnemy,
	EditorEnemyClass,
	EditorCurve
};

struct Object
{
	std::string myName = "GameObject[NAME]";
	std::string myTag = "Tag";

	float myLocked = 0.f;

	ObjectType myType;
	union
	{
		EditorSprite* mySprite;
		EditorAnimation* myAnimation;
		EditorCircleCollider* myCircleCollider;
		EditorLineCollider* myLineCollider;
		EditorEnemy* myEnemy;
		EditorEnemyClass* myEnemyClass;
		EditorCurve* myCurve;
	};

	~Object()
	{
		if( myType == ObjectType::EditorSprite )
		{
			if( mySprite != nullptr )
			{
				SAFE_DELETE(mySprite);
			}
		}
		else if( myType == ObjectType::EditorAnimation )
		{
			if( myAnimation != nullptr )
			{
				SAFE_DELETE(myAnimation);
			}
		}
		else if( myType == ObjectType::EditorCircleCollider )
		{
			if( myCircleCollider != nullptr )
			{
				SAFE_DELETE(myCircleCollider);
			}
		}
		else if( myType == ObjectType::EditorLineCollider )
		{
			if( myLineCollider != nullptr )
			{
				SAFE_DELETE(myLineCollider);
			}
		}
		else if( myType == ObjectType::EditorEnemy )
		{
			if( myEnemy != nullptr )
			{
				SAFE_DELETE(myEnemy);
			}
		}
		else if( myType == ObjectType::EditorEnemyClass )
		{
			if( myEnemyClass != nullptr )
			{
				SAFE_DELETE(myEnemyClass);
			}
		}
		else if (myType == ObjectType::EditorCurve)
		{
			if (myCurve != nullptr)
			{
				SAFE_DELETE(myCurve);
			}
		}
	}

	void Outline()
	{
		if( myType == ObjectType::EditorSprite )
		{
			mySprite->Outline();
		}
		else if( myType == ObjectType::EditorAnimation )
		{
			myAnimation->Outline();
		}
		else if( myType == ObjectType::EditorCircleCollider )
		{
			myCircleCollider->Outline();
		}
		else if( myType == ObjectType::EditorLineCollider )
		{
			myLineCollider->Outline();
		}
		else if( myType == ObjectType::EditorEnemy )
		{
			myEnemy->Outline();
		}
		else if( myType == ObjectType::EditorCurve )
		{
			myCurve->Outline();
		}
	}

	void CalculatePoint(const CommonUtilities::Camera& aCamera)
	{
		if( myType == ObjectType::EditorSprite )
		{
			mySprite->CalculatePoint(aCamera);
		}
		else if( myType == ObjectType::EditorAnimation )
		{
			myAnimation->CalculatePoint(aCamera);
		}
		else if( myType == ObjectType::EditorCircleCollider )
		{
			myCircleCollider->myGizmo.CalculatePoint(aCamera);
		}
		else if( myType == ObjectType::EditorLineCollider )
		{
			myLineCollider->myGizmo.CalculatePoint(aCamera);
		}
		else if( myType == ObjectType::EditorEnemy )
		{
			myEnemy->myGizmo.CalculatePoint(aCamera);
		}
		else if( myType == ObjectType::EditorCurve )
		{
			myCurve->CalculatePoint(aCamera);
		}
	}

	const CommonUtilities::Vector3f GetPosition() const
	{
		if( myType == ObjectType::EditorSprite )
		{
			return mySprite->GetPosition();
		}
		else if( myType == ObjectType::EditorAnimation )
		{
			return myAnimation->GetPosition();
		}
		else if( myType == ObjectType::EditorCircleCollider )
		{
			return myCircleCollider->GetPosition();
		}
		else if( myType == ObjectType::EditorLineCollider )
		{
			return myLineCollider->GetPosition();
		}
		else if( myType == ObjectType::EditorEnemy )
		{
			return myEnemy->GetPosition();
		}
		else if( myType == ObjectType::EditorCurve )
		{
			return myCurve->GetPosition();
		}
		return { 0.f, 0.f, 0.f };
	}
	const CommonUtilities::Vector3f GetPoint() const
	{
		if( myType == ObjectType::EditorSprite )
		{
			return mySprite->GetPoint();
		}
		else if( myType == ObjectType::EditorAnimation )
		{
			return myAnimation->GetPoint();
		}
		else if( myType == ObjectType::EditorCircleCollider )
		{
			return myCircleCollider->myGizmo.GetPoint();
		}
		else if( myType == ObjectType::EditorLineCollider )
		{
			return myLineCollider->myGizmo.GetPoint();
		}
		else if( myType == ObjectType::EditorEnemy )
		{
			return myEnemy->myGizmo.GetPoint();
		}
		else if( myType == ObjectType::EditorCurve )
		{
			return myCurve->GetPoint();
		}
		return { 0.f, 0.f, 0.f };
	}

	void Update(const float aDeltaTime)
	{
		if (myType == ObjectType::EditorAnimation)
		{
			myAnimation->Update(aDeltaTime);
		}
		else if (myType == ObjectType::EditorCurve)
		{
			myCurve->Update(aDeltaTime);
		}
	}
	void Render(const CommonUtilities::Camera& aCamera)
	{
		if( myType == ObjectType::EditorSprite )
		{
			mySprite->Render();
		}
		else if( myType == ObjectType::EditorAnimation )
		{
			myAnimation->Render();
		}
		else if( myType == ObjectType::EditorCircleCollider )
		{
			myCircleCollider->Render(aCamera);
		}
		else if( myType == ObjectType::EditorLineCollider )
		{
			myLineCollider->Render(aCamera);
		}
		else if( myType == ObjectType::EditorEnemy )
		{
			myEnemy->Render();
		}
		else if( myType == ObjectType::EditorCurve )
		{
			myCurve->Render(aCamera);
		}
	}

	void HideGizmo()
	{
		if( myType == ObjectType::EditorCircleCollider )
		{
			myCircleCollider->HideGizmo();
		}
		else if( myType == ObjectType::EditorLineCollider )
		{
			myLineCollider->HideGizmo();
		}
		else if( myType == ObjectType::EditorEnemy )
		{
			myEnemy->HideGizmo();
		}
	}

	void SetPosition(const CommonUtilities::Vector3f& aPosition)
	{
		if( myType == ObjectType::EditorSprite )
		{
			mySprite->SetPosition(aPosition);
		}
		else if( myType == ObjectType::EditorAnimation )
		{
			myAnimation->SetPosition(aPosition);
		}
		else if( myType == ObjectType::EditorCircleCollider )
		{
			myCircleCollider->SetPosition(aPosition);
		}
		else if( myType == ObjectType::EditorLineCollider )
		{
			myLineCollider->SetPosition(aPosition);
		}
		else if( myType == ObjectType::EditorEnemy )
		{
			myEnemy->SetPosition(aPosition);
		}
		else if (myType == ObjectType::EditorCurve)
		{
			myCurve->SetPosition(aPosition);
		}
	}
	void Move(const CommonUtilities::Vector3f& aMovement, bool aSpecial = false)
	{
		if( myType == ObjectType::EditorSprite )
		{
			mySprite->Move(aMovement);
		}
		else if( myType == ObjectType::EditorAnimation )
		{
			myAnimation->Move(aMovement);
		}
		else if( myType == ObjectType::EditorCircleCollider )
		{
			myCircleCollider->SetPosition(myCircleCollider->GetPosition()+aMovement);
		}
		else if( myType == ObjectType::EditorLineCollider )
		{
			myLineCollider->SetPosition(myLineCollider->GetPosition()+aMovement);
		}
		else if( myType == ObjectType::EditorEnemy )
		{
			myEnemy->Move(aMovement);
		}
		else if (myType == ObjectType::EditorCurve)
		{
			myCurve->Move(aMovement, aSpecial);
		}
	}
	void Rotate(const float aRotation)
	{
		if( myType == ObjectType::EditorSprite )
		{
			mySprite->Rotate(aRotation);
		}
		else if( myType == ObjectType::EditorAnimation )
		{
			myAnimation->Rotate(aRotation);
		}
		else if( myType == ObjectType::EditorLineCollider )
		{
			myLineCollider->SetRotation(myLineCollider->GetRotation()-(aRotation));
		}
	}
	void Scale(const CommonUtilities::Vector2f& aScale)
	{
		if( myType == ObjectType::EditorSprite )
		{
			mySprite->Scale(aScale);
		}
		else if( myType == ObjectType::EditorAnimation )
		{
			myAnimation->Scale(aScale);
		}
		else if( myType == ObjectType::EditorCircleCollider )
		{
			myCircleCollider->SetRadius(myCircleCollider->GetRadius() + aScale.x + aScale.y);
		}
		else if( myType == ObjectType::EditorLineCollider )
		{
			myLineCollider->SetLength(myLineCollider->GetLength() + aScale.x + aScale.y);
		}
		else if (myType == ObjectType::EditorCurve)
		{
			myCurve->Scale(aScale);
		}
	}
	bool IsInside(const CommonUtilities::Vector2f& aMousePoint){
		if( myType == ObjectType::EditorSprite )
		{
			return mySprite->IsInside(aMousePoint);
		}
		else if( myType == ObjectType::EditorAnimation )
		{
			return myAnimation->IsInside(aMousePoint);
		}
		else if( myType == ObjectType::EditorCircleCollider )
		{
			return myCircleCollider->myGizmo.IsInside(aMousePoint);
		}
		else if( myType == ObjectType::EditorLineCollider )
		{
			return myLineCollider->myGizmo.IsInside(aMousePoint);
		}
		else if( myType == ObjectType::EditorEnemy )
		{
			return myEnemy->myGizmo.IsInside(aMousePoint);
		}
		else if (myType == ObjectType::EditorCurve)
		{
			return myCurve->myGizmo.IsInside(aMousePoint);
		}
		return false;
	}
};
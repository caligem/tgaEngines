#include "stdafx.h"

#include <iostream>

#include <Base64.h>
#include <FileDialog.h>
#include <fstream>

#include <Parser\json.hpp>

int main()
{
	std::string filePath = CommonUtilities::GetOpenDialogFilePath("");
	nlohmann::json j = nlohmann::json::parse(CommonUtilities::OpenFile(filePath).c_str());

	std::string saveFilePath = CommonUtilities::GetSaveDialogFilePath("");
	CommonUtilities::SaveFile(saveFilePath, j.dump(), true);

    return 0;
}

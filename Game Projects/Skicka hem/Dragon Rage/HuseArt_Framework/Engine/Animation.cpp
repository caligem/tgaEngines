#include "stdafx.h"
#include "Animation.h"

#include <cmath>
#include <iostream>

#include <tga2d\engine.h>
#include <tga2d\sprite\sprite.h>

#include <Camera.h>
#include <Mathf.h>

#include "SpriteData.h"

Animation::Animation()
{
	mySprite = nullptr;
}

Animation::Animation(SpriteData aData)
{
	nlohmann::json::object_t& data = aData.data;
	Init(
		data["texture"].get<std::string>().c_str(),
		{
			data["position"][0].get<float>(),
			data["position"][1].get<float>(),
			data["position"][2].get<float>()
		},
		{
			data["scale"][0].get<float>(),
			data["scale"][1].get<float>()
		},
		data["rotation"].get<float>()
	);
	if (data.find("tint") != data.end())
	{
		SetTint(
		{
			data["tint"][0].get<float>(),
			data["tint"][1].get<float>(),
			data["tint"][2].get<float>(),
			data["tint"][3].get<float>()
		}
		);
	}
	Setup(
		data["rows"].get<int>(),
		data["cols"].get<int>(),
		data["frames"].get<int>(),
		data["offset"].get<int>()
	);
	if (data.find("duration") != data.end())
	{
		SetDuration(data["duration"].get<float>());
	}
}

Animation::Animation(const char * aPath, const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector2f & aScale, float aRotation)
{
	Init(aPath, aPosition, aScale, aRotation);
}

Animation::~Animation()
{}

void Animation::Init(const char * aPath, const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector2f & aScale, float aRotation)
{
	Sprite::Init(aPath, aPosition, aScale, aRotation);

	myDuration = 1.f;
	myTimer = 0.f;
	myLoop = true;
	myIsReversed = false;
	Setup();
}

void Animation::Update(float aDeltaTime)
{
	if (!myLoop && IsFinished())
	{
		return;
	}

	myTimer += aDeltaTime;
}

void Animation::Render()
{
	int index = (static_cast<int>((myTimer / myDuration) * (myFrames + 1)) % myFrames) + myOffset;
	if (myIsReversed)
	{
		index = (myFrames - (static_cast<int>((myTimer / myDuration) * (myFrames + 1)) % myFrames) - 1) + myOffset;
	}
	int x = index;
	int y = 0;
	while( x - myCols >= 0 )
	{
		x -= myCols;
		++y;
	}

	mySprite->SetUVScale({ myInvCols, myInvRows });
	mySprite->SetUVOffset({
		myInvCols * x,
		myInvRows * y,
	});

	myScaledSize.x *= myInvCols;
	myScaledSize.y *= myInvRows;
	mySprite->SetSize({
		Drawable::ourScreenScale * myInvCols * myScale.x*(myOriginalSize.x / myPoint.z),
		Drawable::ourScreenScale * myInvRows * myScale.y*(myOriginalSize.y / myPoint.z)
	});

	Sprite::Render();
}

void Animation::Setup(int aRows, int aCols, int aFrames, int aOffset)
{
	myTimer = 0.f;
	SetRows(aRows);
	SetCols(aCols);
	SetFrames(aFrames);
	SetOffset(aOffset);
}

void Animation::SetRows(int aRows)
{
	myRows = CommonUtilities::Clamp(aRows, 1, 64);
	myInvRows = 1.f / static_cast<float>(myRows);
	mySprite->SetUVScale({ myInvCols, myInvRows });
}
void Animation::SetCols(int aCols)
{
	myCols = CommonUtilities::Clamp(aCols, 1, 64);
	myInvCols = 1.f / static_cast<float>(myCols);
	mySprite->SetUVScale({ myInvCols, myInvRows });
}
void Animation::SetFrames(int aFrames)
{
	myFrames = CommonUtilities::Clamp(aFrames, 1, myRows*myCols);
}
void Animation::SetOffset(int aOffset)
{
	myOffset = CommonUtilities::Clamp(aOffset, 0, myRows*myCols-myFrames);
}
void Animation::SetDuration(float aDuration)
{
	myDuration = std::fabsf(aDuration);
}


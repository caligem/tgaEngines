#pragma once

#include "Sprite.h"

struct SpriteData;

class Animation : public Sprite
{
public:
	Animation();
	Animation(SpriteData aData);
	Animation(const char* aPath, const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f }, const CommonUtilities::Vector2f& aScale = { 1.f, 1.f }, float aRotation = 0.f);
	~Animation();

	void Init(const char* aPath, const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f }, const CommonUtilities::Vector2f& aScale = { 1.f, 1.f }, float aRotation = 0.f);

	void Update(float aDeltaTime) override;
	void Render() override;

	inline bool IsFinished() { return myTimer >= myDuration - (myDuration/static_cast<float>(myFrames)); }

	void Setup(int aRows = 1, int aCols = 1, int aFrames = 1, int aOffset = 0);

	void SetRows(int aRows);
	void SetCols(int aCols);
	void SetFrames(int aFrames);
	void SetOffset(int aOffset);
	void SetDuration(float aDuration);
	void SetLoop(bool aLoop) { myLoop = aLoop; }
	void SetReversed(bool aReversed) { myIsReversed = aReversed; }
	inline int GetRows() const { return myRows; }
	inline int GetCols() const { return myCols; }
	inline int GetFrames() const { return myFrames; }
	inline int GetOffset() const { return myOffset; }
	inline float GetDuration() const { return myDuration; }
	inline bool GetLoop() const { return myLoop; }
	inline bool IsReversed() const { return myIsReversed; }

protected:
	int myRows;
	int myCols;
	float myInvRows;
	float myInvCols;
	int myFrames;
	int myOffset;

	bool myLoop;

	float myDuration;

	float myTimer;
	bool myIsReversed;
};


#include "stdafx.h"
#include "AudioSource.h"

AudioSource::AudioSource()
{
	mySoundSystem = nullptr;
	mySound = nullptr;
	myChannel = nullptr;
	myLoop = false;
	myVolume = 1.f;
}

AudioSource::AudioSource(const AudioSource & aAudioSource)
{
	mySoundSystem = aAudioSource.mySoundSystem;
	mySound = aAudioSource.mySound;
	myChannel = aAudioSource.myChannel;
	myLoop = aAudioSource.myLoop;
	myVolume = aAudioSource.myVolume;
}

AudioSource::~AudioSource()
{}

void AudioSource::Init(const char * aFilepath, SoundSystemClass * aSoundSystem)
{
	mySoundSystem = aSoundSystem;
	mySoundSystem->CreateSound(&mySound, aFilepath);

	unsigned int length;
	mySound->getLength(&length, FMOD_TIMEUNIT_MS);
	myLength = static_cast<float>(length) / 1000.f;
}

void AudioSource::Play()
{
	if (mySoundSystem == nullptr)
	{
		return;
	}

	if (myChannel != nullptr && IsPlaying())
	{
		myChannel->setPosition(0, FMOD_TIMEUNIT_MS);
		Resume();
		return;
	}

	mySoundSystem->PlaySound(mySound, &myChannel, myLoop);

	if (myChannel != nullptr)
	{
		myChannel->setVolume(myVolume);
	}
}

void AudioSource::Resume()
{
	if (myChannel == nullptr)
	{
		return;
	}

	myChannel->setPaused(false);
}

void AudioSource::Pause()
{
	if (myChannel == nullptr)
	{
		return;
	}
	
	myChannel->setPaused(true);
}

void AudioSource::Stop()
{
	if (mySoundSystem == nullptr)
	{
		return;
	}

	if (myChannel == nullptr)
	{
		return;
	}

	myChannel->stop();
}

void AudioSource::SetVolume(float aVolume)
{
	myVolume = aVolume;

	if (myChannel == nullptr)
	{
		return;
	}
	
	myChannel->setVolume(myVolume);
}

void AudioSource::SetPitch(float aPitch)
{
	if (myChannel == nullptr)
	{
		return;
	}
	
	myChannel->setPitch(aPitch);
}

void AudioSource::Fade(float aFadeValue)
{
	if (myChannel == nullptr)
	{
		return;
	}
	
	myChannel->setVolume(myVolume * aFadeValue);
}

void AudioSource::SetLoop(bool aLoop)
{
	myLoop = aLoop;

	if (mySound == nullptr)
	{
		return;
	}

	if( !myLoop )
	{
		mySound->setMode(FMOD_LOOP_OFF);
	}
	else
	{
		mySound->setMode(FMOD_LOOP_NORMAL);
		mySound->setLoopCount(-1);
	}
}

bool AudioSource::IsPlaying() const
{
	if (myChannel == nullptr)
	{
		return false;
	}
	
	bool playing = false;
	if (myChannel->isPlaying(&playing) == FMOD_OK)
	{
		return playing;
	}

	return false;
}

float AudioSource::GetProgress() const
{
	if (myChannel == nullptr)
	{
		return 0.0f;
	}
	unsigned int position;
	myChannel->getPosition(&position, FMOD_TIMEUNIT_MS);
	float pos  = static_cast<float>(position) / 1000.f;
	return pos / myLength;
}

void AudioSource::Release()
{
	if (mySoundSystem == nullptr)
	{
		return;
	}

	mySoundSystem->ReleaseSound(mySound);
}

#pragma once

#include "SoundSystemClass.h"

class AudioSource
{
public:
	AudioSource();
	AudioSource(const AudioSource& aAudioSource);
	~AudioSource();

	void Init(const char* aFilepath, SoundSystemClass* aSoundSystem);

	void Play();
	void Resume();
	void Pause();
	void Stop();
	void Release();

	void Fade(float aFadeValue);
	void SetVolume(float aVolume);
	void SetPitch(float aPitch);
	void SetLoop(bool aLoop);

	inline float GetVolume() const { return myVolume; }
	inline bool IsLooping() const { return myLoop; }
	bool IsPlaying() const;
	float GetProgress() const;
	inline float GetLength() const { return myLength; }

private:
	SoundSystemClass* mySoundSystem;

	SoundClass mySound;
	ChannelClass myChannel;

	bool myLoop;
	float myVolume;

	float myLength;
};


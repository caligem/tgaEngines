#include "stdafx.h"
#include "Button.h"

#include <tga2d\engine.h>
#include <tga2d\sprite\sprite.h>

#include <Macros.h>
#include <Camera.h>

#include <tga2d/math/common_math.h>

namespace CU = CommonUtilities;

Button::Button()
{}

Button::Button(const char * aPath, const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector2f & aScale, float aRotation, const CommonUtilities::Vector4f& aTint)
{
	Init(aPath, aPosition, aScale, aRotation, aTint);
}

Button::~Button()
{
}

void Button::Render()
{
	Sprite::Render();
}

#pragma once

#include "Sprite.h"

class Button : public Sprite
{
public:
	Button();
	Button(
		const char* aPath,
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector2f& aScale = { 1.f, 1.f },
		float aRotation = 0.f,
		const CommonUtilities::Vector4f& aTint = { 1.f, 1.f, 1.f, 1.f }
	);
	virtual ~Button();

	void Render();

protected:

};


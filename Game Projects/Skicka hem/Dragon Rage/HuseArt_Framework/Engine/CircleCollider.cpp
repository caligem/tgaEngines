#include "stdafx.h"
#include "CircleCollider.h"
#include "LineCollider.h"

#include <tga2d\primitives\line_primitive.h>
#include <tga2d\math\common_math.h>

CircleCollider::CircleCollider(float aRadius)
	: myRadius(aRadius)
{
	myLine = new Tga2D::CLinePrimitive();
}

CircleCollider::~CircleCollider()
{}

bool CircleCollider::IsTouching(const Collider* aCollider)
{
	if( aCollider->GetType() == ColliderType::Circle )
	{
		const CircleCollider* other = static_cast<const CircleCollider*>(aCollider);
		float r = other->GetRadius() + myRadius;
		return (aCollider->GetPosition() - myPosition).Length2() < (r*r);
	}
	else if( aCollider->GetType() == ColliderType::Line )
	{
		return false;
	}

	return false;
}

void CircleCollider::RenderDebug(const CommonUtilities::Camera& aCamera)
{
	float step = Tga2D::Pif / 6.f;
	for( float i = 0.f; i < Tga2D::Pif*2.f; i += step )
	{
		CommonUtilities::Vector3f point1 = aCamera.PostProjectionSpace(GetPosition() + CommonUtilities::Vector3f(
			std::cosf(i)*myRadius,
			std::sinf(i)*myRadius,
			0.f
		));
		CommonUtilities::Vector3f point2 = aCamera.PostProjectionSpace(GetPosition() + CommonUtilities::Vector3f(
			std::cosf(i+step)*myRadius,
			std::sinf(i+step)*myRadius,
			0.f
		));
		
		myLine->myFromPosition = {
			point1.x, point1.y
		};
		myLine->myToPosition = {
			point2.x, point2.y
		};
		myLine->Render();
	}
}
#pragma once

#include "Collider.h"

#include <tga2d/math/common_math.h>
#include <tga2d/render/render_common.h>
#include <tga2d/primitives/line_primitive.h>
#include "Camera.h"

class CircleCollider : public Collider
{
public:
	CircleCollider(float aRadius = 1.0f);
	~CircleCollider();

	bool IsTouching(const Collider* aCollider) override;

	inline void SetRadius(float aRadius = 1.0f) { myRadius = aRadius; }
	inline float GetRadius() const { return myRadius; }
	ColliderType GetType() const override { return ColliderType::Circle; }

	void RenderDebug(const CommonUtilities::Camera& aCamera);

protected:
	float myRadius;

	Tga2D::CLinePrimitive* myLine;

};


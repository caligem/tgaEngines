#pragma once

enum class ColliderType
{
	None,
	Circle,
	Line,
	Point
};

#include <Vector.h>

class Collider
{
public:
	//Tells you if two colliders are touching
	virtual bool IsTouching(const Collider* aCollider) = 0;

	//TODO: update void to return some kind of CollisionData
	//virtual void Collide();

	inline void SetPosition(const CommonUtilities::Vector3f& aPosition) { myPosition = aPosition; }
	inline const CommonUtilities::Vector3f& GetPosition() const { return myPosition; }

	virtual ColliderType GetType() const = 0;

protected:
	Collider() = default;
	virtual ~Collider() = default;

	CommonUtilities::Vector3f myPosition;
};

#pragma once

#include <BezierCurve.h>

#include <Vector.h>

class Curve
{
public:
	Curve();
	virtual ~Curve();

	inline void SetPosition(const CommonUtilities::Vector3f& aMovement) { myPosition = aMovement; }

	inline const CommonUtilities::Vector3f& GetPosition() const { return myPosition; }
	inline const CommonUtilities::Vector3f& GetPoint() const { return myPoint; }

protected:
	CommonUtilities::BezierCurve myCurve;
	
	CommonUtilities::Vector3f myPosition;
	CommonUtilities::Vector3f myPoint;

};


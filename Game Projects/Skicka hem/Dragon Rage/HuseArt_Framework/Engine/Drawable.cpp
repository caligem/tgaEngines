#include "stdafx.h"
#include "Drawable.h"

float Drawable::ourScreenRatio = 16.f / 9.f;
float Drawable::ourScreenScale = 1.f;

const CommonUtilities::Vector2f Drawable::GetUnitSize()
{
	return {
		myPixelSize.x / 540.f,
		myPixelSize.y / 540.f
	};
}

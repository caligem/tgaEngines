#pragma once

namespace CommonUtilities
{
	class Camera;
}

struct SpriteData;

class CircleCollider;

#include <Vector.h>
#include <tga2d\math\vector2.h>

class Drawable
{
public:
	Drawable() {}
	virtual ~Drawable() {}

	virtual void Render() = 0;
	virtual void Update(float) = 0;

	virtual void CalculatePoint(const CommonUtilities::Camera& aCamera) = 0;
	virtual bool IsInside(const CommonUtilities::Vector2f& aPoint) = 0;
	virtual bool IsOutsideScreen(float aRadius=0.f) = 0;

	inline void SetPosition(const CommonUtilities::Vector3f& aMovement) { myPosition = aMovement; }
	inline void SetScale(const CommonUtilities::Vector2f& aScale) { myScale = aScale; }
	inline void SetRotation(const float& aRotation) { myRotation = aRotation; }
	inline void SetTint(const CommonUtilities::Vector4f& aTint) { myTint = aTint; }
	inline void Move(const CommonUtilities::Vector3f& aMovement) { myPosition += aMovement; }
	inline void Scale(const CommonUtilities::Vector2f& aScale) { myScale += aScale; }
	inline void Rotate(const float& aRotation) { myRotation += aRotation; }

	inline const CommonUtilities::Vector3f& GetPosition() const { return myPosition; }
	inline const CommonUtilities::Vector3f& GetPoint() const { return myPoint; }
	inline const CommonUtilities::Vector2f& GetScale() const { return myScale; }
	inline const Tga2D::Vector2f& GetScaledSize() const { return myScaledSize; }
	inline const float& GetRotation() const { return myRotation; }
	inline const CommonUtilities::Vector4f& GetTint() const { return myTint; }

	const CommonUtilities::Vector2f GetUnitSize();

	virtual bool IsTouching(const CircleCollider* aCollider) = 0;

	static float ourScreenScale;
	static float ourScreenRatio;

protected:
	
	CommonUtilities::Vector3f myPosition;
	CommonUtilities::Vector2f myScale;
	float myRotation;
	CommonUtilities::Vector4f myTint;

	CommonUtilities::Vector3f myPoint;

	Tga2D::Vector2f myScaledSize;
	Tga2D::Vector2f myPixelSize;
};


#include "stdafx.h"
#include "EnemyClass.h"

EnemyClass::EnemyClass(float aHP, float aDMG)
{
	SetHP(aHP);
	SetDMG(aDMG);
	SetType("default");
	SetName("generic");
}

EnemyClass::~EnemyClass()
{}

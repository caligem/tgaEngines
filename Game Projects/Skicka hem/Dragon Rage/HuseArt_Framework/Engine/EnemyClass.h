#pragma once

#include <string>

class EnemyClass
{
public:
	EnemyClass(float aHP = 100.f, float aDMG = 50.f);
	~EnemyClass();

	inline void SetHP(const float aHP) { myHP = aHP; }
	inline void SetDMG(const float aDMG) { myDMG = aDMG; }
	inline void SetROF(const float aROF) { myROF = aROF; }
	inline void SetProjectileSpeed(const float aProjectileSpeed) { myProjectileSpeed = aProjectileSpeed; }
	inline void SetMovementSpeed(const float aSpeed) { mySpeed = aSpeed; }
	inline void SetType(const std::string& aTypeName) { myType = aTypeName; }
	inline void SetName(const std::string& aName) { myName = aName; }

	inline float GetHP() const { return myHP; }
	inline float GetDMG() const { return myDMG; }
	inline float GetROF() const { return myROF; }
	inline float GetProjectileSpeed() const { return myProjectileSpeed; }
	inline float GetMovementSpeed() const { return mySpeed; }
	inline const std::string& GetType() const { return myType; }
	inline const std::string& GetName() const { return myName; }

protected:
	float myHP;
	float myDMG;
	float myROF;
	float myProjectileSpeed;
	float mySpeed;

	std::string myType;
	std::string myName;
};


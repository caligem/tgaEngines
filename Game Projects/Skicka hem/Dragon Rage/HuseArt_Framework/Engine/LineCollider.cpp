#include "stdafx.h"
#include "LineCollider.h"
#include "CircleCollider.h"

LineCollider::LineCollider(float aLength, float aRotation)
	: myLength(aLength)
	, myRotation(aRotation)
{ }

LineCollider::~LineCollider()
{}

bool LineCollider::IsTouching(const Collider * aCollider)
{
	if( aCollider->GetType() == ColliderType::Circle )
	{
		return false;
	}
	else if( aCollider->GetType() == ColliderType::Line )
	{
		return false;
	}

	return false;
}

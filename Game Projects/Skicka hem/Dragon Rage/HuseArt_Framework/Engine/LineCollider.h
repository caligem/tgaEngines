#pragma once

#include "Collider.h"

class LineCollider : public Collider
{
public:
	LineCollider(float aLength = 1.0f, float aRotation = 0.f);
	~LineCollider();

	bool IsTouching(const Collider* aCollider) override;

	inline void SetLength(float aLength = 1.0f) { myLength = aLength; }
	inline void SetRotation(float aRotation = 0.f) { myRotation = aRotation; }
	inline float GetLength() const { return myLength; }
	inline float GetRotation() const { return myRotation; }
	ColliderType GetType() const override { return ColliderType::Line; }

protected:
	float myLength;
	float myRotation;
};


#include "stdafx.h"
#include "SoundSystemClass.h"

#include <iostream>

SoundSystemClass::SoundSystemClass()
{
	if( FMOD::System_Create(&mySystem) != FMOD_OK )
	{
		return;
	}

	int driverCount = 0;
	mySystem->getNumDrivers(&driverCount);

	if( driverCount == 0 )
	{
		return;
	}

	mySystem->init(256, FMOD_INIT_NORMAL, nullptr);

	mySystem->createChannelGroup("master", &myChannelGroup);

	SetVolume(1.f);
}

SoundSystemClass::~SoundSystemClass()
{}

void SoundSystemClass::CreateSound(SoundClass* aSound, const char* aFilePath)
{
	if (mySystem->createSound(aFilePath, FMOD_DEFAULT, 0, aSound) != FMOD_OK)
	{
		std::cout << "Couldn't load audio: " << aFilePath << std::endl;
	}
}

void SoundSystemClass::PlaySound(SoundClass aSound, ChannelClass* aChannel, bool aLoop)
{
	if( !aLoop )
	{
		aSound->setMode(FMOD_LOOP_OFF);
	}
	else
	{
		aSound->setMode(FMOD_LOOP_NORMAL);
		aSound->setLoopCount(-1);
	}

	mySystem->playSound(aSound, nullptr, false, aChannel);

	(*aChannel)->setChannelGroup(myChannelGroup);
}

void SoundSystemClass::ReleaseSound(SoundClass aSound)
{
	aSound->release();
}

void SoundSystemClass::Update()
{
	mySystem->update();
}

void SoundSystemClass::SetVolume(float aVolume)
{
	myChannelGroup->setVolume(aVolume);
}

float SoundSystemClass::GetVolume()
{
	float volume;
	myChannelGroup->getVolume(&volume);
	return volume;
}

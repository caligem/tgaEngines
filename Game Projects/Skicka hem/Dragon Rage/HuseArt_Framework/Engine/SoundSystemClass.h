#pragma once

#include "fmod\fmod.hpp"

typedef FMOD::Sound* SoundClass;
typedef FMOD::Channel* ChannelClass;
typedef FMOD::ChannelGroup* ChannelGroupClass;

class SoundSystemClass
{
public:
	SoundSystemClass();
	~SoundSystemClass();

	void CreateSound(SoundClass* aSound, const char* aFilePath);
	void PlaySound(SoundClass aSound, ChannelClass* aChannel, bool aLoop = false);
	void ReleaseSound(SoundClass aSound);
	void Update();

	void SetVolume(float aVolume);
	float GetVolume();

private:
	FMOD::System* mySystem;

	ChannelGroupClass myChannelGroup;
};


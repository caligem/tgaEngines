#include "stdafx.h"
#include "Sprite.h"

#include <tga2d\engine.h>
#include <tga2d\sprite\sprite.h>
#include <tga2d\primitives\line_primitive.h>

#include <Macros.h>
#include <Camera.h>
#include <Matrix.h>

#include <tga2d/math/common_math.h>

#include "SpriteData.h"

#include "CircleCollider.h"

namespace CU = CommonUtilities;

Sprite::Sprite()
{
	mySprite = nullptr;
	myLine = nullptr;
}

Sprite::Sprite(SpriteData aData)
{
	nlohmann::json::object_t& data = aData.data;
	Init(
		data["texture"].get<std::string>().c_str(),
		{
			data["position"][0].get<float>(),
			data["position"][1].get<float>(),
			data["position"][2].get<float>()
		},
		{
			data["scale"][0].get<float>(),
			data["scale"][1].get<float>()
		},
		data["rotation"].get<float>()
	);
	if (data.find("tint") != data.end())
	{
		SetTint(
		{
			data["tint"][0].get<float>(),
			data["tint"][1].get<float>(),
			data["tint"][2].get<float>(),
			data["tint"][3].get<float>()
		}
		);
	}
	if (data.find("UVScale") != data.end())
	{
		SetUVScale({
			data["UVScale"][0].get<float>(),
			data["UVScale"][1].get<float>()
		});
	}
}

Sprite::Sprite(const char * aPath, const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector2f & aScale, float aRotation, const CommonUtilities::Vector4f & aTint)
{
	Init(aPath, aPosition, aScale, aRotation, aTint);
}

Sprite::~Sprite()
{
	if( mySprite != nullptr )
	{
		SAFE_DELETE(mySprite);
	}
	SAFE_DELETE(myLine);
}

void Sprite::Init(const char * aPath, const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector2f & aScale, float aRotation, const CommonUtilities::Vector4f & aTint)
{
	mySprite = new Tga2D::CSprite(aPath);
	mySprite->SetPivot({ .5f, .5f });

	myOriginalSize = mySprite->GetSize();

	myPosition = aPosition;
	myScale = aScale;
	myRotation = aRotation;

	mySprite->SetRotation(myRotation);

	myPixelSize = {
		static_cast<float>(mySprite->GetImageSize().x),
		static_cast<float>(mySprite->GetImageSize().y)
	};

	myTint = aTint;

	myLineBuffer.Init(8);
	myLine = new Tga2D::CLinePrimitive();
}

void Sprite::Render()
{
	if(mySprite == nullptr)
	{
		return;
	}

	if( myPoint.z <= 0.f || IsOutsideScreen() )
	{
		return;
	}

	if( myPoint.z <= 0.25f )
	{
		mySprite->SetColor({ myTint.x, myTint.y, myTint.z, myTint.w * (4.f*myPoint.z) });
	}
	else
	{
		mySprite->SetColor({ myTint.x, myTint.y, myTint.z, myTint.w });
	}
	mySprite->Render();
}

void Sprite::CalculatePoint(const CommonUtilities::Camera & aCamera)
{
	if(mySprite == nullptr)
	{
		return;
	}

	myPoint = aCamera.PostProjectionSpace(myPosition);
	myScaledSize = {
		Drawable::ourScreenScale * myScale.x*(myOriginalSize.x / Drawable::ourScreenRatio / myPoint.z),
		Drawable::ourScreenScale * myScale.y*(myOriginalSize.y / myPoint.z)
	};

	mySprite->SetPosition({ myPoint.x, myPoint.y });

	mySprite->SetSize({
		Drawable::ourScreenScale * myScale.x*(myOriginalSize.x / myPoint.z),
		Drawable::ourScreenScale * myScale.y*(myOriginalSize.y / myPoint.z)
	});
	mySprite->SetRotation(myRotation);
}

bool Sprite::IsInside(const CommonUtilities::Vector2f & aPoint)
{
	if(mySprite == nullptr)
	{
		return false;
	}

	CommonUtilities::Vector3f deltaPoint =
		CommonUtilities::Vector3f(aPoint.x, aPoint.y, 0.f) - myPoint;

	deltaPoint.x *= Drawable::ourScreenRatio;
	deltaPoint *= CommonUtilities::Matrix33f::CreateRotateAroundZ(-myRotation);
	deltaPoint.x /= Drawable::ourScreenRatio;

	float xScale = 1.f;
	float yScale = 1.f;
	if( myScaledSize.x < 0.f )
	{
		xScale = -1.f;
	}
	if( myScaledSize.y < 0.f )
	{
		yScale = -1.f;
	}

	if( deltaPoint.x > xScale*myScaledSize.x/2.f )
	{
		return false;
	}
	if( deltaPoint.x < xScale*-myScaledSize.x/2.f )
	{
		return false;
	}
	if( deltaPoint.y > yScale*myScaledSize.y/2.f )
	{
		return false;
	}
	if( deltaPoint.y < yScale*-myScaledSize.y/2.f )
	{
		return false;
	}
	return true;
}

bool Sprite::IsOutsideScreen(float aRadius)
{
	if(mySprite == nullptr)
	{
		return true;
	}

	CU::Vector3f point0 = (CU::Vector3f(myScaledSize.x*Drawable::ourScreenRatio, myScaledSize.y, 0.f) / 2.f) * CU::Matrix33f::CreateRotateAroundZ(myRotation);
	CU::Vector3f point1 = (CU::Vector3f(myScaledSize.x*Drawable::ourScreenRatio, -myScaledSize.y, 0.f) / 2.f) * CU::Matrix33f::CreateRotateAroundZ(myRotation);
	CU::Vector3f point2 = (CU::Vector3f(-myScaledSize.x*Drawable::ourScreenRatio, myScaledSize.y, 0.f) / 2.f) * CU::Matrix33f::CreateRotateAroundZ(myRotation);
	CU::Vector3f point3 = (CU::Vector3f(-myScaledSize.x*Drawable::ourScreenRatio, -myScaledSize.y, 0.f) / 2.f) * CU::Matrix33f::CreateRotateAroundZ(myRotation);
	point0.x /= Drawable::ourScreenRatio;
	point1.x /= Drawable::ourScreenRatio;
	point2.x /= Drawable::ourScreenRatio;
	point3.x /= Drawable::ourScreenRatio;
	point0 += myPoint;
	point1 += myPoint;
	point2 += myPoint;
	point3 += myPoint;

	float aRadius1 = aRadius + 1.f;
	if (
		(point0.x < -aRadius && point1.x < -aRadius && point2.x < -aRadius && point3.x < -aRadius) ||
		(point0.y < -aRadius && point1.y < -aRadius && point2.y < -aRadius && point3.y < -aRadius) ||
		(point0.x > aRadius1 && point1.x > aRadius1 && point2.x > aRadius1 && point3.x > aRadius1) ||
		(point0.y > aRadius1 && point1.y > aRadius1 && point2.y > aRadius1 && point3.y > aRadius1)
		)
	{
		return true;
	}
	return false;
}

void Sprite::SetUVScale(const Tga2D::Vector2f & aUVScale)
{
	if(mySprite == nullptr)
	{
		return;
	}
	mySprite->SetUVScale(aUVScale);
}

void Sprite::SetUVOffset(const Tga2D::Vector2f & aUVOffset)
{
	if(mySprite == nullptr)
	{
		return;
	}
	mySprite->SetUVOffset(aUVOffset);
}

const Tga2D::Vector2f& Sprite::GetUVScale() const
{
	return mySprite->GetUVScale();
}

const Tga2D::Vector2f & Sprite::GetUVOffset() const
{
	return mySprite->GetUVOffset();
}

bool Sprite::IsTouching(const CircleCollider * aCollider)
{
	CommonUtilities::Vector3f delta = aCollider->GetPosition() - myPosition;

	myLineBuffer.Add({ 0.f, 0.f, 0.f });
	myLineBuffer.Add(delta);

	CommonUtilities::Vector3f closestPoint = delta * CommonUtilities::Matrix33f::CreateRotateAroundZ(myRotation);

	float left = -(myScaledSize.x*Drawable::ourScreenRatio);
	float right = (myScaledSize.x*Drawable::ourScreenRatio);
	float top = (myScaledSize.y);
	float bottom = -(myScaledSize.y);

	if (closestPoint.x < left)
	{
		closestPoint.x = left;
	}
	else if (closestPoint.x > right)
	{
		closestPoint.x = right;
	}

	if (closestPoint.y < bottom)
	{
		closestPoint.y = bottom;
	}
	else if (closestPoint.y > top)
	{
		closestPoint.y = top;
	}

	closestPoint *= CommonUtilities::Matrix33f::CreateRotateAroundZ(-myRotation);

	myLineBuffer.Add(closestPoint);
	myLineBuffer.Add(delta);


	return (closestPoint-(aCollider->GetPosition()-myPosition)).Length2() <= aCollider->GetRadius()*aCollider->GetRadius();
}

void Sprite::RenderDebug(const CommonUtilities::Camera & aCamera)
{
	DrawLine(
		{ -myScaledSize.x / 2.f, -myScaledSize.y / 2.f },
		{ myScaledSize.x / 2.f, -myScaledSize.y / 2.f }
	);
	DrawLine(
		{ myScaledSize.x / 2.f, -myScaledSize.y / 2.f },
		{ myScaledSize.x / 2.f, myScaledSize.y / 2.f }
	);
	DrawLine(
		{ myScaledSize.x / 2.f, myScaledSize.y / 2.f },
		{ -myScaledSize.x / 2.f, myScaledSize.y / 2.f }
	);
	DrawLine(
		{ -myScaledSize.x / 2.f, myScaledSize.y / 2.f },
		{ -myScaledSize.x / 2.f, -myScaledSize.y / 2.f }
	);
	aCamera;

	myLine->myColor.Set(0.f, 1.f, 1.f, 1.f);
	for (unsigned short i = 0; i < myLineBuffer.Size(); i += 2)
	{
		DrawLineA(
			aCamera.PostProjectionSpace(myPosition + myLineBuffer[i]),
			aCamera.PostProjectionSpace(myPosition + myLineBuffer[i + 1])
		);
	}
	myLine->myColor.Set(1.f, 0.f, 0.f, 1.f);
	myLineBuffer.RemoveAll();
}

void Sprite::DrawLine(const CommonUtilities::Vector2f & aFrom, const CommonUtilities::Vector2f & aTo)
{
	CommonUtilities::Vector3f from = { aFrom.x*Drawable::ourScreenRatio, aFrom.y, 1.f };
	CommonUtilities::Vector3f to = { aTo.x*Drawable::ourScreenRatio, aTo.y, 1.f };

	from *= CommonUtilities::Matrix33f::CreateRotateAroundZ(myRotation);
	to *= CommonUtilities::Matrix33f::CreateRotateAroundZ(myRotation);

	myLine->SetFrom(myPoint.x + from.x/Drawable::ourScreenRatio, myPoint.y + from.y);
	myLine->SetTo(myPoint.x + to.x/Drawable::ourScreenRatio, myPoint.y + to.y);
	myLine->Render();
}

void Sprite::DrawLineA(const CommonUtilities::Vector2f & aFrom, const CommonUtilities::Vector2f & aTo)
{
	myLine->SetFrom(aFrom.x, aFrom.y);
	myLine->SetTo(aTo.x, aTo.y);
	myLine->Render();
}

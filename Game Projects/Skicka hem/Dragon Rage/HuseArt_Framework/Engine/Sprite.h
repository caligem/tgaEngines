#pragma once

namespace Tga2D
{
	class CSprite;
	class CLinePrimitive;
}

namespace CommonUtilities
{
	class Camera;
}

#include "Drawable.h"
#include <GrowingArray.h>

class Sprite : public Drawable
{
public:
	Sprite();
	Sprite(SpriteData aData);
	Sprite(
		const char* aPath,
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector2f& aScale = { 1.f, 1.f },
		float aRotation = 0.f,
		const CommonUtilities::Vector4f& aTint = { 1.f, 1.f, 1.f, 1.f }
	);
	virtual ~Sprite();

	virtual void Init(
		const char* aPath,
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector2f& aScale = { 1.f, 1.f },
		float aRotation = 0.f,
		const CommonUtilities::Vector4f& aTint = { 1.f, 1.f, 1.f, 1.f }
	);

	virtual void Render() override;
	virtual void Update(float) override {}

	void CalculatePoint(const CommonUtilities::Camera& aCamera) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsOutsideScreen(float aRadius=0.f) override;

	void SetUVScale(const Tga2D::Vector2f& aUVScale);
	void SetUVOffset(const Tga2D::Vector2f& aUVOffset);
	const Tga2D::Vector2f& GetUVScale() const;
	const Tga2D::Vector2f& GetUVOffset() const;

	bool IsTouching(const CircleCollider* aCollider) override;

	void RenderDebug(const CommonUtilities::Camera& aCamera);

protected:
	void DrawLine(const CommonUtilities::Vector2f & aFrom, const CommonUtilities::Vector2f & aTo);
	void DrawLineA(const CommonUtilities::Vector2f & aFrom, const CommonUtilities::Vector2f & aTo);

	Tga2D::CSprite* mySprite;

	Tga2D::Vector2f myOriginalSize;

	Tga2D::CLinePrimitive* myLine;

	CommonUtilities::GrowingArray<CommonUtilities::Vector3f> myLineBuffer;
};


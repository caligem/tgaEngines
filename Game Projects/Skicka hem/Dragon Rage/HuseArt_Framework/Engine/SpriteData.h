#pragma once

#include <Parser/json.hpp>

struct SpriteData
{
	SpriteData(nlohmann::json::object_t& aData) :data(aData){}
	nlohmann::json::object_t& data;
};

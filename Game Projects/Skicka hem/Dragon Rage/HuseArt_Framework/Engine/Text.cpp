#include "stdafx.h"
#include "Text.h"

#include <tga2d\engine.h>

#include <Macros.h>
#include <Camera.h>
#include <Matrix.h>

#include <tga2d/math/common_math.h>

#include "SpriteData.h"

#include "CircleCollider.h"

Text::Text()
{
	myText = nullptr;
}

Text::Text(const std::string & aText, Tga2D::EFontSize aFontSize)
{
	Init(aText, aFontSize);
}

Text::~Text()
{}

void Text::Init(const std::string & aText, Tga2D::EFontSize aFontSize)
{
	myText = new Tga2D::CText("Assets/Fonts/square.ttf", aFontSize, 4);
	myText->myText = aText;
	
	myScale = CommonUtilities::Vector2f(1.f, 1.f);
	myTint = CommonUtilities::Vector4f(1.f, 1.f, 1.f, 1.f);

	SetText(aText);
}

void Text::Render()
{
	if(myText == nullptr)
	{
		return;
	}

	if( myPoint.z <= 0.f || IsOutsideScreen() )
	{
		return;
	}

	if( myPoint.z <= 0.25f )
	{
		myText->myColor.Set(myTint.x, myTint.y, myTint.z, myTint.w * (4.f*myPoint.z));
	}
	else
	{
		myText->myColor.Set(myTint.x, myTint.y, myTint.z, myTint.w);
	}
	myText->Render();
}

void Text::Update(float)
{
	
}

void Text::CalculatePoint(const CommonUtilities::Camera & aCamera)
{
	float sScale = Tga2D::CEngine::GetInstance()->GetWindowSize().y / 1080.f;
	if(myText == nullptr)
	{
		return;
	}

	myPoint = aCamera.PostProjectionSpace(myPosition);
	myScaledSize = {
		Drawable::ourScreenScale * myScale.x*(Drawable::ourScreenRatio / myPoint.z),
		Drawable::ourScreenScale * myScale.y*(myPoint.z)
	};

	myText->myPosition.Set( myPoint.x, myPoint.y );

	myText->myScale = (sScale * myScale.y) / myPoint.z;
}

bool Text::IsInside(const CommonUtilities::Vector2f & aPoint)
{
	aPoint;
	return false;
}

bool Text::IsOutsideScreen(float aRadius)
{
	aRadius;
	return false;
}

void Text::SetText(const std::string & aText)
{
	if (myText == nullptr)
	{
		return;
	}

	myText->myText = aText;

	myPixelSize = {
		Drawable::ourScreenScale * (myText->GetWidth() * Tga2D::CEngine::GetInstance()->GetWindowSize().x) * Drawable::ourScreenRatio,
		1.f
	};
}

bool Text::IsTouching(const CircleCollider*) 
{
	return false;
}

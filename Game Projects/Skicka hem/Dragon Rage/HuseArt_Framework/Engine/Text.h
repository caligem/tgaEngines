#pragma once

namespace Tga2D
{
	class CSprite;
}

#include "Drawable.h"

#include <string>
#include <tga2d/text/text.h>

class Text : public Drawable
{
public:
	Text();
	Text(const std::string& aText, Tga2D::EFontSize aFontSize = Tga2D::EFontSize_14);
	~Text();

	void Init(const std::string& aText, Tga2D::EFontSize aFontSize = Tga2D::EFontSize_14);

	void Render() override;
	void Update(float) override;

	void CalculatePoint(const CommonUtilities::Camera& aCamera) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsOutsideScreen(float aRadius=0.f) override;

	void SetText(const std::string& aText);

	bool IsTouching(const CircleCollider* aCollider) override;

private:
	Tga2D::CText* myText;

};


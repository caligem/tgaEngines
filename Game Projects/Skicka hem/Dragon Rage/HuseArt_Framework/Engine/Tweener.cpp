#include "stdafx.h"
#include "Tweener.h"

#include <iostream>

#include <Macros.h>

Tweener* Tweener::ourInstance = nullptr;

void Tweener::CheckValidity()
{
	if( ourInstance == nullptr )
	{
		Create();
	}
}

void Tweener::Create()
{
	if( ourInstance != nullptr )
	{
		Destroy();
	}
	ourInstance = new Tweener();
}

void Tweener::Destroy()
{
	if( ourInstance == nullptr )
	{
		SAFE_DELETE(ourInstance);
	}
	else
	{
		std::cout << "Tweener not initialised" << std::endl;
	}
}

TweenObject& Tweener::Tween(float aDuration, TweenType aTween, TweenMod aMod)
{
	CheckValidity();

	TweenObject* newTweenObject = new TweenObject(aDuration, aTween, aMod);

	ourInstance->myTweens.Add(newTweenObject);

	return *newTweenObject;
}

void Tweener::Update(float aDeltaTime)
{
	CheckValidity();

	for( TweenObject* to : ourInstance->myTweens )
	{
		to->myTween.Update(aDeltaTime);
	}
	for( unsigned short i = ourInstance->myTweens.Size(); i > 0; --i )
	{
		TweenObject* to = ourInstance->myTweens[i-1];
		if( to->myTween.IsFinished() )
		{
			to->myOnFinish(to->myTo);
			ourInstance->myTweens.DeleteAtIndex(i - 1);
		}
		else
		{
			to->myOnUpdate(to->myFrom + to->myTween.GetValue() * (to->myTo-to->myFrom));
		}
	}
}

Tweener::Tweener()
{
	myTweens.Init(10);
}

Tweener::~Tweener()
{
	myTweens.DeleteAll();
}

/*TWEEN OBJECT*/
TweenObject::TweenObject(float aDuration, TweenType aTween, TweenMod aMod)
	: myTween(aTween, aMod, 0.f, 1.f, aDuration)
{}

TweenObject & TweenObject::OnUpdate(std::function<void(float aValue)> aOnUpdate)
{
	myOnUpdate = aOnUpdate;
	return *this;
}

TweenObject & TweenObject::OnFinish(std::function<void(float aValue)> aOnFinish)
{
	myOnFinish = aOnFinish;
	return *this;
}

TweenObject & TweenObject::From(float aFrom)
{
	myFrom = aFrom;
	return *this;
}

TweenObject & TweenObject::To(float aTo)
{
	myTo = aTo;
	return *this;
}

#pragma once

#include "Tweening\Tween.h"
#include <GrowingArray.h>

#include <functional>

class Tweener;

class TweenObject
{
	friend Tweener;
public:
	TweenObject(float aDuration = 1.f, TweenType aTween = TweenType::Quadratic, TweenMod aMod = TweenMod::EaseInOut);

	TweenObject& OnUpdate(std::function<void(float aValue)> aOnUpdate);
	TweenObject& OnFinish(std::function<void(float aValue)> aOnFinish);
	TweenObject& From(float aFrom);
	TweenObject& To(float aTo);

private:
	std::function<void(float aValue)> myOnUpdate = [](float){};
	std::function<void(float aValue)> myOnFinish = [](float){};
	Tween myTween;
	float myFrom = 0.f;
	float myTo = 1.f;
};

class Tweener
{
public:
	static void Destroy();
	
	static TweenObject& Tween(float aDuration = 1.f, TweenType aTween = TweenType::Quadratic, TweenMod aMod = TweenMod::EaseInOut);
	static void Update(float aDeltaTime);

private:
	Tweener();
	~Tweener();

	static void CheckValidity();
	static void Create();

	static Tweener* ourInstance;

	CommonUtilities::GrowingArray<TweenObject*> myTweens;
	
};


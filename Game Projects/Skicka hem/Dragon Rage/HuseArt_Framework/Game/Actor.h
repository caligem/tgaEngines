#pragma once

#include <Vector.h>

class Actor
{
public:
	Actor();
	virtual ~Actor();

	inline void SetPosition(const CommonUtilities::Vector3f& aPosition) { myPosition = aPosition; }
	inline void Move(const CommonUtilities::Vector3f& aMovement) { myPosition += aMovement; }
	inline const CommonUtilities::Vector3f& GetPosition() const { return myPosition; }

protected:
	CommonUtilities::Vector3f myPosition;

};


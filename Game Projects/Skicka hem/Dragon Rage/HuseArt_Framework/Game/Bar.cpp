#include "stdafx.h"
#include "Bar.h"

void Bar::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer, float aFlashTimer)
{
	if (myTimer > 0.f)
	{
		aRenderBuffer.Add(&myFrame);
		if (static_cast<int>(aFlashTimer * 10.f) % 2 == 0)
		{
			mySubstance.SetTint({ 1.0f, 1.0f, 1.0f, 1.0f });
		}
		else
		{
			mySubstance.SetTint({ 100.0f, 100.0f, 100.0f, 1.0f });
		}
		
		aRenderBuffer.Add(&mySubstance);
	}
}

void Bar::SetPosition(CommonUtilities::Vector3f aPosition)
{
	myFramePosition = aPosition;
	mySubstancePosition = myFramePosition;
	mySubstancePosition.z = myFramePosition.z + 0.001f;
	mySubstancePosition.x -= (1.f - myCurrentValue/myMaxValue) * (myFrame.GetUnitSize().x / 2.f);

	myFrame.SetPosition(myFramePosition);
	mySubstance.SetPosition(mySubstancePosition);
}
#pragma once
#include "Sprite.h"
#include <GrowingArray.h>
#include <cfloat>

class Bar
{
public:
	Bar() = default;
	virtual ~Bar() = default;

	virtual void Update(float aDeltaTime, float aCurrentValue, const CommonUtilities::Vector3f& aPosition) = 0;

	virtual void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer, float aFlashTimer = 0.0f);

	virtual void SetPosition(CommonUtilities::Vector3f aPosition);
	void SetMaxValue(float aValue) { myMaxValue = aValue; }
	void SetCurrentValue(float aValue) { myCurrentValue = aValue; }

	void SetShowTimer(float aTimer) { myShowTimer = aTimer; }
	void Show() { myTimer = myShowTimer; }

protected:
	Sprite myFrame;
	CommonUtilities::Vector3f myFramePosition;

	Sprite mySubstance;
	CommonUtilities::Vector3f mySubstancePosition;

	CommonUtilities::Vector2f myScale;

	float myMaxValue;
	float myCurrentValue;

	float myTimer;
	float myShowTimer;
};


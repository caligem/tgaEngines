#include "stdafx.h"
#include "Beam.h"

#include "Enemy.h"
#include "Player.h"

#include <Camera.h>
#include <Macros.h>

#include <CircleCollider.h>

namespace CU = CommonUtilities;

Beam::Beam()
	: myIsActive(false)
	, myHaveCollided(false)
	, myDamage(0.0f)
{}

Beam::~Beam()
{}

void Beam::Init(eBeamType aBeamType)
{
	myBeamType = aBeamType;

	myGlowAnimation.Init("Assets/Images/GUI/powerShine.dds");
	myGlow = false;

	if (aBeamType == eBeamType::Lightning)
	{
		mySprite.Init("Assets/Images/Projectiles/projectile_lighningbeamTileable.dds");

		myHead.Init("Assets/Images/Projectiles/Lightningbeam_Head.dds");
		myHead.Setup(2, 2, 4);

		myTail.Init("Assets/Images/Projectiles/Lightningbeam_Tail.dds");
		myTail.Setup(2, 2, 4);
	}
	else if (aBeamType == eBeamType::PlayerBeam)
	{
		mySprite.Init("Assets/Images/Projectiles/projectile_BeamYellow.dds");

		myHead.Init("Assets/Images/Projectiles/Lightningbeam_HeadYellow.dds");
		myHead.Setup(2, 2, 4);

		myTail.Init("Assets/Images/Projectiles/Lightningbeam_TailYellow.dds");
		myTail.Setup(2, 2, 4);
	}
	else if (aBeamType == eBeamType::Chain)
	{
		mySprite.Init("Assets/Images/Projectiles/projectile_magicalChain.dds");
		mySprite.SetScale({ 0.2f, 0.2f });
	}
	else if (aBeamType == eBeamType::WizardFire)
	{
		mySprite.Init("Assets/Images/Projectiles/projectile_BeamRed.dds");

		myHead.Init("Assets/Images/Projectiles/Lightningbeam_HeadRed.dds");
		myHead.Setup(2, 2, 4);

		myTail.Init("Assets/Images/Projectiles/Lightningbeam_TailRed.dds");
		myTail.Setup(2, 2, 4);
	}
	else if (aBeamType == eBeamType::WizardIce)
	{
		mySprite.Init("Assets/Images/Projectiles/projectile_BeamBlue.dds");

		myHead.Init("Assets/Images/Projectiles/Lightningbeam_HeadBlue.dds");
		myHead.Setup(2, 2, 4);

		myTail.Init("Assets/Images/Projectiles/Lightningbeam_TailBlue.dds");
		myTail.Setup(2, 2, 4);
	}
	else if (aBeamType == eBeamType::WizardLightning)
	{
		mySprite.Init("Assets/Images/Projectiles/projectile_BeamYellow.dds");

		myHead.Init("Assets/Images/Projectiles/Lightningbeam_HeadYellow.dds");
		myHead.Setup(2, 2, 4);

		myTail.Init("Assets/Images/Projectiles/Lightningbeam_TailYellow.dds");
		myTail.Setup(2, 2, 4);
	}
	else if (aBeamType == eBeamType::Beholder)
	{
		mySprite.Init("Assets/Images/Projectiles/projectile_BeamPurple.dds");

		myHead.Init("Assets/Images/Projectiles/Lightningbeam_HeadPurple.dds");
		myHead.Setup(2, 2, 4);

		myTail.Init("Assets/Images/Projectiles/Lightningbeam_TailPurple.dds");
		myTail.Setup(2, 2, 4);
	}
	else if (aBeamType == eBeamType::Lich)
	{
		mySprite.Init("Assets/Images/Projectiles/projectile_naturebeamTileable.dds");

		myHead.Init("Assets/Images/Projectiles/Naturebeam_Head.dds");
		myHead.Setup(2, 2, 4);

		myTail.Init("Assets/Images/Projectiles/Naturebeam_Tail.dds");
		myTail.Setup(2, 2, 4);
	}
	else if (aBeamType == eBeamType::BoneDragon)
	{
		mySprite.Init("Assets/Images/Projectiles/projectile_netherbeamTileable.dds");

		myHead.Init("Assets/Images/Projectiles/Netherbeam_Head.dds");
		myHead.Setup(2, 2, 4);

		myTail.Init("Assets/Images/Projectiles/Netherbeam_Tail.dds");
		myTail.Setup(2, 2, 4);
	}
	else if (aBeamType == eBeamType::LaserSight)
	{
		mySprite.Init("Assets/Images/Projectiles/projectile_laserSight.dds");
		mySprite.SetTint({ 1.0f, 1.0f, 1.0f, 0.0f });

		myTail.Init("Assets/Images/Projectiles/lazerSight_Head.dds");
		myTail.Setup(1, 1, 1);
		myTail.SetScale({ 2.0f, 2.0f });
	}


	myHead.SetDuration(0.5f);
	myTail.SetDuration(0.5f);

}

void Beam::Update(float aDeltaTime, bool aCheckCollided)
{
	if (!myIsActive)
	{
		if (myGlow)
		{
			if (myGlowTint.w > 0.0f)
			{
				myGlowTint.w -= aDeltaTime * 2.0f;
			}

			myGlowAnimation.SetTint({ myGlowTint.x, myGlowTint.y, myGlowTint.z, myGlowTint.w });
		}
		return;
	}
	
	CU::Vector3f direction = myEndPosition - myStartPosition;
	mySprite.SetRotation(-std::atan2f(direction.y, direction.x));

	CU::Vector3f midPoint;

	if (myHaveCollided && aCheckCollided)
	{
		CU::Vector3f distance = myCollidedPosition - myStartPosition;
		float dot = distance.Dot(direction.GetNormalized());

		if (dot < 0.f)
		{
			myLength = (myStartPosition - myEndPosition).Length();
			midPoint = (myStartPosition + myEndPosition) / 2.f;
		}
		else
		{
			CU::Vector3f endPos = myStartPosition + direction.GetNormalized()*dot;

			myLength = (myStartPosition - endPos).Length();
			midPoint = (myStartPosition + endPos) / 2.f;

			CU::Vector3f offset = direction.GetNormalized() * 0.05f;
			
			myHead.SetPosition({ endPos.x - offset.x, endPos.y - offset.y, endPos.z - 0.001f });
		}
	}
	else
	{
		myLength = (myStartPosition - myEndPosition).Length();
		midPoint = (myStartPosition + myEndPosition) / 2.f;

		myHead.SetPosition({ myEndPosition.x, myEndPosition.y, myEndPosition.z });
	}

	mySprite.SetPosition(midPoint);

	if (myBeamType == eBeamType::Chain)
	{
		mySprite.SetScale({ myLength / mySprite.GetUnitSize().x, 1.f * 0.5f });
		mySprite.SetUVScale({ myLength / mySprite.GetUnitSize().x * 2.0f , 1.f });
		mySprite.SetUVOffset({
			mySprite.GetUVOffset().x - 2.f*aDeltaTime,
			0.f
		});
	}
	else if (myBeamType == eBeamType::WizardFire || myBeamType == eBeamType::WizardLightning || myBeamType == eBeamType::WizardIce)
	{
		mySprite.SetScale({ myLength / mySprite.GetUnitSize().x, 0.5f });
		mySprite.SetUVScale({ myLength / mySprite.GetUnitSize().x , 1.f });
		mySprite.SetUVOffset({
			mySprite.GetUVOffset().x - 5.f*aDeltaTime,
			0.f
		});

		myHead.SetScale({ 0.5f, 0.5f });
		myTail.SetScale({ 0.5f, 0.5f });
	}
	else
	{
		mySprite.SetScale({ myLength / mySprite.GetUnitSize().x, 1.f });
		mySprite.SetUVScale({ myLength / mySprite.GetUnitSize().x , 1.f });
		mySprite.SetUVOffset({
			mySprite.GetUVOffset().x - 5.f*aDeltaTime,
			0.f
		});		
	}	

	myHead.SetRotation(-std::atan2f(direction.y, direction.x));
	myHead.Update(aDeltaTime);
	
	CU::Vector3f offset;

	if (myBeamType != eBeamType::LaserSight)
	{
		offset = direction.GetNormalized() * 0.02f;
	}
	else
	{
		offset = direction.GetNormalized() * 0.48f;
	}

	myTail.SetPosition({ myStartPosition.x + offset.x, myStartPosition.y + offset.y, myStartPosition.z - 0.001f});
	myTail.SetRotation(-std::atan2f(direction.y, direction.x));
	myTail.Update(aDeltaTime);

	myHaveCollided = false;

	if (myGlow)
	{

		if (myGlowTint.w < 0.8f)
		{
			myGlowTint.w += aDeltaTime * 2.0f;
		}

		myGlowAnimation.SetPosition(myStartPosition);
		myGlowAnimation.SetTint({ myGlowTint.x, myGlowTint.y, myGlowTint.z, myGlowTint.w });
	}
}

void Beam::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	if (myIsActive)
	{
		aRenderBuffer.Add(&mySprite);

		if (myBeamType != eBeamType::Chain && myBeamType != eBeamType::LaserSight)
		{
			aRenderBuffer.Add(&myHead);
		}
		if (myBeamType != eBeamType::Chain)
		{
			aRenderBuffer.Add(&myTail);
		}
	}

	if (myGlow)
	{
		aRenderBuffer.Add(&myGlowAnimation);
	}
}

bool Beam::IsTouching(const CircleCollider* aCollider)
{
	if (!myIsActive)
	{
		return false;
	}

	if (mySprite.IsTouching(aCollider))
	{
		if (myHaveCollided)
		{
			if ((myCollidedPosition - myStartPosition).Length2() > (aCollider->GetPosition() - myStartPosition).Length2())
			{
				myCollidedPosition = aCollider->GetPosition();
			}
		}
		else
		{
			myCollidedPosition = aCollider->GetPosition();
		}
		myHaveCollided = true;
		return true;
	}

	return false;
}

bool Beam::IsTouching(Player & aPlayer)
{
	if (!myIsActive)
	{
		myHaveCollided = false;
		return false;
	}

	if (mySprite.IsTouching(aPlayer.GetCollider()))
	{
		myCollidedPosition = aPlayer.GetPosition();
		myHaveCollided = true;
	}
	else
	{
		myHaveCollided = false;
	}

	return false;
}

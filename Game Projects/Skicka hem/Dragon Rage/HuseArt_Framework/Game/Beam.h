#pragma once

#include <Vector.h>

#include <Animation.h>
#include <Sprite.h>
#include <GrowingArray.h>

#include "Element.h"

class Enemy;
class Player;

class CircleCollider;

enum class eBeamType
{
	Lightning,
	PlayerBeam,
	Chain,
	WizardFire,
	WizardIce,
	WizardLightning,
	Beholder,
	Lich,
	BoneDragon,
	LaserSight
};

class Beam
{
public:
	Beam();
	~Beam();
	
	void Init(eBeamType aBeamType);
	void Update(float aDeltaTime, bool aCheckCollided = true);

	void SetStartPosition(CommonUtilities::Vector3f aPosition) { myStartPosition = aPosition; }
	void SetEndPosition(CommonUtilities::Vector3f aPosition) { myEndPosition = aPosition; }
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer);
	void SetGlow(bool aGlow, CommonUtilities::Vector4f aGlowTint) { myGlow = aGlow; myGlowTint = aGlowTint; }

	inline void SetElement(const eElement& aElement) { myElement = aElement; }
	inline const eElement GetElement() const { return myElement; }

	void SetDamage(float aDamage) { myDamage = aDamage; }
	inline const float GetDamage() const { return myDamage; }
	inline const bool IsActive() { return myIsActive; }
	inline void Activate() { myIsActive = true; }
	inline void Dectivate() { myIsActive = false; }

	bool IsTouching(const CircleCollider* aCollider);
	bool IsTouching(Player& aPlayer);

	virtual void RenderDebug(const CommonUtilities::Camera& aCamera) { mySprite.RenderDebug(aCamera); }

private:
	float myDamage;
	bool myIsActive;
	bool myHaveCollided;
	float myLength;
	CommonUtilities::Vector3f myCollidedPosition;

	eBeamType myBeamType;

	Sprite mySprite;
	CommonUtilities::Vector3f myStartPosition;
	CommonUtilities::Vector3f myEndPosition;
	Animation myHead;
	Animation myTail;

	eElement myElement;

	Animation myGlowAnimation;
	CommonUtilities::Vector4f myGlowTint;
	bool myGlow;
};
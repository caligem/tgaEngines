#include "stdafx.h"
#include "Blackboard.h"

#include <Macros.h>

Blackboard* Blackboard::ourInstance = nullptr;

void Blackboard::Create()
{
	if (ourInstance == nullptr)
	{
		ourInstance = new Blackboard();
	}
}

void Blackboard::Destroy()
{
	if (ourInstance == nullptr)
	{
		return;
	}
	SAFE_DELETE(ourInstance);
}

const std::string Blackboard::GetNote(const std::string & aKey)
{
	if (myData.find(aKey) == myData.end())
	{
		return "";
	}

	return myData[aKey];
}

#pragma once

#include <string>
#include <map>

class Blackboard
{
public:
	static Blackboard* GetInstance() { return ourInstance; }

	static void Create();
	static void Destroy();

	inline void SetNote(const std::string& aKey, const std::string& aValue) { myData[aKey] = aValue; }
	const std::string GetNote(const std::string& aKey);

private:
	Blackboard() = default;
	~Blackboard() = default;
	
	static Blackboard* ourInstance;

	std::map<std::string, std::string> myData;

};


#include "stdafx.h"
#include "BonusAnimation.h"
#include "Player.h"


void BonusAnimation::InitBonusAnimation()
{
	myShowBonusAnimation = false;
	myAllowBonusHeartFadeOut = false;
	increaseY = 0.f;
	alpha = 1.0f;

	myBonusGlow.Init("Assets/Images/Player/bonus_glow.dds");
	myBonusGlow.Setup(2, 2, 4);
	myBonusGlow.SetDuration(1.2f);
	CommonUtilities::Vector4f tint = { 1.f, 1.f, 1.f, 1.0f };
	myBonusGlow.SetTint(tint);

	myBonusHeart.Init("Assets/Images/Player/bonus_heart.dds");
}

void BonusAnimation::UpdateBonusAnimation(float aDeltaTime, Player& aPlayer)
{
	myBonusAnimationCountdown.Update(aDeltaTime);
	myBonusHeartFadeOutCountdown.Update(aDeltaTime);

	if (myShowBonusAnimation)
	{
		myBonusGlow.SetPosition({ aPlayer.GetPosition().x, aPlayer.GetPosition().y, aPlayer.GetPosition().z + 0.001f });
		myBonusGlow.Update(aDeltaTime);

		increaseY += aDeltaTime/7;
		CommonUtilities::Vector3f offset = { 0.1f, 0.15f + increaseY, 0.f };

		if (offset.y >= 0.25f)
		{
			offset.y = 0.25f;

			if (!myAllowBonusHeartFadeOut)
			{
				myBonusHeartFadeOutCountdown.Set([&] { myAllowBonusHeartFadeOut = true; }, 0.4f);
			}

			if (myAllowBonusHeartFadeOut)
			{
				alpha -= aDeltaTime*2;
			}
		}
		myBonusHeart.SetTint({ 1.f, 1.f, 1.f, alpha });
		myBonusGlow.SetTint({ 1.f, 1.f, 1.f, alpha });
		myBonusHeart.SetPosition(aPlayer.GetPosition() + offset);
	}
}

void BonusAnimation::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	if (myShowBonusAnimation)
	{
		aRenderBuffer.Add(&myBonusGlow);
		aRenderBuffer.Add(&myBonusHeart);
	}
}

void BonusAnimation::PlayBonusAnimation()
{
	if (myShowBonusAnimation)
	{
		return;
	}

	myShowBonusAnimation = true;

	if (myShowBonusAnimation)
	{
		myBonusAnimationCountdown.Set([&]
		{
			myShowBonusAnimation = false;
			myAllowBonusHeartFadeOut = false;
			increaseY = 0.f;
			alpha = 1.0f;
		}, 2.0f);
	}
}
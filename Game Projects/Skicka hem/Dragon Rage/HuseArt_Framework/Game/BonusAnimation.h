#pragma once
#include <Animation.h>
#include "CountDown.h"

class Player;


class BonusAnimation
{
public:
	BonusAnimation() = default;
	~BonusAnimation() = default;

	void InitBonusAnimation();
	void UpdateBonusAnimation(float aDeltaTime, Player& aPlayer);
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer);
	void PlayBonusAnimation();


private:
	Animation myBonusGlow;
	Animation myBonusHeart;
	CountDown myBonusAnimationCountdown;
	CountDown myBonusHeartFadeOutCountdown;
	bool myAllowBonusHeartFadeOut;
	bool myShowBonusAnimation;
	float increaseY;
	float alpha;
};


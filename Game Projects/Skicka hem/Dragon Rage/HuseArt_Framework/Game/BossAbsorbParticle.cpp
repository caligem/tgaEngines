#include "stdafx.h"
#include "BossAbsorbParticle.h"

#include <Random.h>
#include "ExplosionManager.h"
#include "Player.h"

BossAbsorbParticle::BossAbsorbParticle(const float aAngle)
	: myAngle(aAngle)
{
}

BossAbsorbParticle::~BossAbsorbParticle()
{
}

void BossAbsorbParticle::Init(const EnemyClass & aEnemyClass, const CommonUtilities::Vector3f aPosition)
{
	aEnemyClass;
	myPosition = aPosition;
	myHealth = 1.f;
	myScoreFactor = 1.0f;

	myAnimation.Init("Assets/Images/Projectiles/bossParticle.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myShineAnimation.Init("Assets/Images/GUI/powerShine.dds", { myPosition.x, myPosition.y, myPosition.z + 0.1f });
	myShineAnimation.SetTint({ 1.0f, 1.0f, 1.0f, 0.0f });
	myScale = { 0.2f, 0.2f };
	myAnimation.SetScale(myScale);

	myOriginalDir.x = std::cosf(myAngle) * 4.0f;
	myOriginalDir.y = std::sinf(myAngle) * 4.0f;
	myOriginalDir.z = 0.f;

	myKillLength = 0.1f;
	mySpeed = 2.0f;
	myScaleDown = false;
	myDirection = { 0.0f, 0.0f, 0.0f };
	myPlayerTint = 0.0f;
}

void BossAbsorbParticle::Update(float aDeltaTime, Player& aPlayer)
{	
	myPlayerPos = aPlayer.GetPosition();

	myDirection = aPlayer.GetPosition() - myPosition;
	
	
	myOriginalDir += (myDirection - myOriginalDir)  * aDeltaTime;


	if (!myScaleDown)
	{
		myScale.x += aDeltaTime * 3.0f;
		myScale.y += aDeltaTime * 3.0f;
		if (myScale.x > 0.7f || myScale.y > 0.7f)
		{
			myScaleDown = true;
		}
	}
	else if (myScale.x > 0.2f || myScale.y > 0.2f)
	{
		myScale.x -= aDeltaTime / 2.0f;
		myScale.y -= aDeltaTime / 2.0f;
	}
	else
	{
		Kill();
	}


	myPosition += myOriginalDir * aDeltaTime;
	myPosition += myDirection * (mySpeed * mySpeed) * aDeltaTime;

	mySpeed += aDeltaTime;

	myAnimation.SetScale(myScale);
	myAnimation.SetPosition(myPosition);
	myAnimation.Update(aDeltaTime);

	myShineAnimation.SetScale({ 3.5f, 3.5f });
	myShineAnimation.SetTint({ myTint.x, myTint.y, myTint.z, 0.009f });
	myShineAnimation.SetPosition({ myPlayerPos.x, myPlayerPos.y, myPlayerPos.z - 0.001f, });


	myPlayerTint += aDeltaTime / 100.0f;
	aPlayer.GetCurrentStance()->GetCurrentAnimation()->SetTint({
		aPlayer.GetCurrentStance()->GetCurrentAnimation()->GetTint().x + myPlayerTint,
		aPlayer.GetCurrentStance()->GetCurrentAnimation()->GetTint().x + myPlayerTint,
		aPlayer.GetCurrentStance()->GetCurrentAnimation()->GetTint().x + myPlayerTint,
		1.0f });
}

void BossAbsorbParticle::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
	aRenderBuffer.Add(&myShineAnimation);
}

void BossAbsorbParticle::SetTint(const CommonUtilities::Vector4f& aTint)
{
	myTint = aTint;
	myAnimation.SetTint(myTint);
}

void BossAbsorbParticle::DeathAnimation()
{
	Animation* newSpark = new Animation("Assets/Images/Sprites/spark.dds");
	newSpark->Setup(6, 8, 48);
	newSpark->SetDuration(0.5f);
	newSpark->SetLoop(true);
	newSpark->SetPosition({ myPlayerPos.x, myPlayerPos.y, myPlayerPos.z - 0.01f });
	newSpark->SetTint({ 1.0f, 1.0f, 1.0f, 0.5f });
	newSpark->SetScale({ 1.f, 1.f });
	ExplosionManager::GetInstance()->AddExplosion(newSpark);
}

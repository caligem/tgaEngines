#pragma once
#include "Enemy.h"

#include <Animation.h>
#include "CountDown.h"


class BossAbsorbParticle : public Enemy
{
public:
	BossAbsorbParticle(const float aAngle);
	~BossAbsorbParticle();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;

	void Update(float, Player&) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void SetTint(const CommonUtilities::Vector4f& aTint);

	void DeathAnimation() override;

	bool IsInside(const CommonUtilities::Vector2f& aPoint) override { aPoint; return false; }
	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override { aDamage; aDeltaTime; }
	bool IsBoss() { return false; }

	inline bool IsTouching(const CircleCollider* aCircleCollider) override { aCircleCollider; return false; }
	inline bool IsTouching(Projectile* aProjectile) override { aProjectile; return false; }
	inline bool IsTouching(Beam& aBeam) override { aBeam; return false; }

private:
	Animation myAnimation;
	Animation myShineAnimation;

	float myAngle;
	float myKillLength;
	float mySpeed;
	float myPlayerTint;
	bool myScaleDown;

	CommonUtilities::Vector2f myScale;
	CommonUtilities::Vector3f myOriginalDir;
	CommonUtilities::Vector3f myDirection;
	CommonUtilities::Vector3f myPlayerPos;
	CommonUtilities::Vector4f myTint;
};


#include "stdafx.h"
#include "BossBeholder.h"

#include "ProjectileManager.h"
#include "Player.h"


#include "ProjectileFactory.h"
#include "EnemyFactory.h"
#include "ExplosionManager.h"
#include "EnemyFactory.h"
#include "Blackboard.h"

#include <Random.h>

namespace CU = CommonUtilities;

BossBeholder::BossBeholder()
	: myRotation(0.0f)
	, myBeamSpinnRotation(0.0f)
	, myRotationSpeed(0.0f)
	, myMovementSpeed(1.2f)
	, myPrimaryAttackProjectileSpeed(0.7f)
	, myPrimaryAttackFireRate(1.0f)
	, myCenterBeamFireRate(1.0f)
	, mySideBeamFireRate(1.5f)
	, myUltimateFireRate(2.0f)
	, myBeamDuration(2.0f)
	, myPrimaryAttackAngel(0.0f)
	, mySpeedUp(true)
	, myNbrOfShoots(0)
	, myNbrOfNormalAttacks(0)
	, myWaitBeforeAndAfterUltimate(1.5f)
	, myCenterBeamStartPos(0.0f, 0.0f, 0.0f)
	, myCenterBeamEndPos(0.0f, 0.0f, 0.0f)
	, myLeftTopBeamStartPos(0.0f, 0.0f, 0.0f)
	, myLeftTopBeamEndPos(0.0f, 0.0f, 0.0f)
	, myLeftCenterBeamStartPos(0.0f, 0.0f, 0.0f)
	, myLeftCenterBeamEndPos(0.0f, 0.0f, 0.0f)
	, myLeftBottomBeamStartPos(0.0f, 0.0f, 0.0f)
	, myLeftBottomBeamEndPos(0.0f, 0.0f, 0.0f)
	, myRightTopBeamStartPos(0.0f, 0.0f, 0.0f)
	, myRightTopBeamEndPos(0.0f, 0.0f, 0.0f)
	, myRightCenterBeamStartPos(0.0f, 0.0f, 0.0f)
	, myRightCenterBeamEndPos(0.0f, 0.0f, 0.0f)
	, myRightBottomBeamStartPos(0.0f, 0.0f, 0.0f)
	, myRightBottomBeamEndPos(0.0f, 0.0f, 0.0f)
	, myShootDirection(0.0f, 0.0f, 0.0f)
	, myIncreaseTint(true)
	, myDeathTint(0.0f)
	, myCurrentTint(1.0f)
	, myPlayBeamSound(true)
	, myLeftBoundary(-1.4f)
	, myRightBoundary(1.4f)
	, myTopBoundary(0.0f)
	, myBottomBoundary(-1.8f)
	, myCenterY(0)
	, myIsSlowed(false)
	, myHoverDistance(0.05f)
	, myBoppingSpeed(0.5f)
	, myHoverTime(0.0f)
{}



BossBeholder::~BossBeholder()
{
}

void BossBeholder::Init(const EnemyClass& aEnemyClass, const CU::Vector3f aPosition)
{
	myPosition = aPosition;

	myAnimationIdle.Init("Assets/Images/Enemies/Bosses/enemy_boss_Watcher.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myAnimationIdle.Setup(2, 2, 4);
	myAnimationCenterBeam.Init("Assets/Images/Enemies/Bosses/enemy_boss_Watcher_animation_bigbeam.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myAnimationCenterBeam.Setup(2, 2, 4);
	myAnimationCenterBeam.SetLoop(false);
	myAnimationSideBeams.Init("Assets/Images/Enemies/Bosses/enemy_boss_Watcher_animation_crossbeams.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myAnimationSideBeams.Setup(2, 2, 4);
	myAnimationSideBeams.SetLoop(false);
	myAnimationUltimate.Init("Assets/Images/Enemies/Bosses/enemy_boss_Watcher_animation_spinbeam.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myAnimationUltimate.Setup(2, 3, 3);
	myAnimationUltimate.SetLoop(false);
	myAnimationBody.Init("Assets/Images/Enemies/Bosses/enemy_boss_Watcher_body.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myCurrentAnimation = &myAnimationIdle;

	myRightTopEye.Init("Assets/Images/Enemies/Bosses/enemy_boss_Watcher_animation_eyes.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myRightTopEye.Setup(2, 4, 1, 1);
	myRightCenterEye.Init("Assets/Images/Enemies/Bosses/enemy_boss_Watcher_animation_eyes.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myRightCenterEye.Setup(2, 4, 1, 0);
	myRightBottomEye.Init("Assets/Images/Enemies/Bosses/enemy_boss_Watcher_animation_eyes.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myRightBottomEye.Setup(2, 4, 1, 7);
	myLeftTopEye.Init("Assets/Images/Enemies/Bosses/enemy_boss_Watcher_animation_eyes.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myLeftTopEye.Setup(2, 4, 1, 3);
	myLeftCenterEye.Init("Assets/Images/Enemies/Bosses/enemy_boss_Watcher_animation_eyes.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myLeftCenterEye.Setup(2, 4, 1, 4);
	myLeftBottomEye.Init("Assets/Images/Enemies/Bosses/enemy_boss_Watcher_animation_eyes.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myLeftBottomEye.Setup(2, 4, 1, 5);
		
	myEnemyClass = aEnemyClass;
	myHealth = myEnemyClass.GetHP();
	myPrimaryAttackCooldown = myPrimaryAttackFireRate;
	myCenterBeamCooldown = myCenterBeamFireRate;
	mySideBeamCooldown = mySideBeamFireRate;
	myUltimateCooldown = myUltimateFireRate;
	myTopBoundary += aPosition.y;
	myBottomBoundary += aPosition.y;
	myCenterY = (myTopBoundary + myBottomBoundary) / 2.0f;

	myCenterBeam.Init(eBeamType::Beholder);
	myCenterBeam.SetGlow(true, { 1.0f, 0.6f, 0.6f, 0.2f });
	myLeftTopBeam.Init(eBeamType::Beholder);
	myLeftTopBeam.SetGlow(true, { 1.0f, 0.6f, 0.6f, 0.2f });
	myLeftCenterBeam.Init(eBeamType::Beholder);
	myLeftCenterBeam.SetGlow(true, { 1.0f, 0.6f, 0.6f, 0.2f });
	myLeftBottomBeam.Init(eBeamType::Beholder);
	myLeftBottomBeam.SetGlow(true, { 1.0f, 0.6f, 0.6f, 0.2f });
	myRightTopBeam.Init(eBeamType::Beholder);
	myRightTopBeam.SetGlow(true, { 1.0f, 0.6f, 0.6f, 0.2f });
	myRightCenterBeam.Init(eBeamType::Beholder);
	myRightCenterBeam.SetGlow(true, { 1.0f, 0.6f, 0.6f, 0.2f });
	myRightBottomBeam.Init(eBeamType::Beholder);
	myRightBottomBeam.SetGlow(true, { 1.0f, 0.6f, 0.6f, 0.2f });

	myCenterBeamStartPos = { myPosition.x, myPosition.y, myPosition.z + 0.0001f };
	myLeftTopBeamStartPos = { myPosition.x, myPosition.y, myPosition.z + 0.0001f };
	myLeftCenterBeamStartPos = { myPosition.x, myPosition.y, myPosition.z + 0.0001f };
	myLeftBottomBeamStartPos = { myPosition.x, myPosition.y, myPosition.z + 0.0001f };
	myRightTopBeamStartPos = { myPosition.x, myPosition.y, myPosition.z + 0.0001f };
	myRightCenterBeamStartPos = { myPosition.x, myPosition.y, myPosition.z + 0.0001f };
	myRightBottomBeamStartPos = { myPosition.x, myPosition.y, myPosition.z + 0.0001f };

	myMoveSet = eBeholderMoveSet::attackPrimary;
	myCollider.SetRadius(0.25f);

	myHealthBar.Init(myEnemyClass.GetHP(), { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f }, FLT_MAX, { 8.0f, 1.0f });

	myBleed = true;
	mySpawnExplosion = true;
	myPlayBossDeathAnimation = false;
	myHasPlayedFinalBossExplosion = false;

	if (myEnemyClass.GetType().find(".hafd") != std::string::npos)
	{
		myDialogue.Init(myEnemyClass.GetType());
	}
}

void BossBeholder::Update(float aDeltaTime, Player& aPlayer)
{
	aPlayer;

	if (myHealth == 1)
	{
		DeathAnimation();
		myDeathTint += aDeltaTime / 20.0f;

		if (myCurrentAnimation->GetTint().x > 3.0f)
		{
			myIncreaseTint = false;
		}
		else if (myCurrentAnimation->GetTint().x < 0.0f)
		{
			myIncreaseTint = true;
		}

		if (myIncreaseTint)
		{
			myCurrentTint += myDeathTint;
			myCurrentAnimation->SetTint({ myCurrentTint, myCurrentTint, myCurrentTint, 1.0f });
		}
		else if (!myIncreaseTint)
		{
			myCurrentTint -= myDeathTint;
			myCurrentAnimation->SetTint({ myCurrentTint, myCurrentTint, myCurrentTint, 1.0f });
		}


		myCenterBeam.Dectivate();
		myLeftTopBeam.Dectivate();
		myLeftCenterBeam.Dectivate();
		myLeftBottomBeam.Dectivate();
		myRightTopBeam.Dectivate();
		myRightCenterBeam.Dectivate();
		myRightBottomBeam.Dectivate();

		myHealthBar.Update(aDeltaTime, myHealth, { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f });
		myHealthBar.Show();
		myCountDown.Update(aDeltaTime);

		return;
	}

	myPosition += CommonUtilities::Vector3f(0.f, myHoverDistance * std::sinf(myHoverTime * myBoppingSpeed), 0.f) * aDeltaTime;
	myHoverTime += aDeltaTime * 2.f * Tga2D::Pif;

	if (myMoveSet == eBeholderMoveSet::attackPrimary)
	{
		myPrimaryAttackCooldown -= aDeltaTime;

		if (myPrimaryAttackCooldown < 0)
		{
			if (myNbrOfShoots < 8)
			{
				ShootAttack(myPrimaryAttackAngel);
				myPrimaryAttackAngel += Tga2D::Pif / 12;
				myPrimaryAttackCooldown = 0.1f;
				++myNbrOfShoots;
			}
			else if (myNbrOfShoots < 17)
			{
				ShootAttack(myPrimaryAttackAngel);
				myPrimaryAttackAngel -= Tga2D::Pif / 14;
				myPrimaryAttackCooldown = 0.1f;
				++myNbrOfShoots;
			}
			else
			{
				myNbrOfShoots = 0;
				myPrimaryAttackAngel = 0.0f;
				myPrimaryAttackCooldown = myPrimaryAttackFireRate;
				++myNbrOfNormalAttacks;

				if (myNbrOfNormalAttacks == 2)
				{
					myNbrOfNormalAttacks = 0;
					myMoveSet = eBeholderMoveSet::attackCenterBeam;
				}
			}
		}
	}


	if (myMoveSet == eBeholderMoveSet::attackCenterBeam)
	{
		myCenterBeamCooldown -= aDeltaTime;

		if (myCurrentAnimation != &myAnimationCenterBeam)
		{
			myCountDown.Set(std::bind(&BossBeholder::PlayCenterEyeOpen, this), 0.3f);
		}

		myCurrentAnimation = &myAnimationCenterBeam;

		

		if (myAnimationCenterBeam.IsFinished() &&
			myAnimationCenterBeam.GetLoop() == false &&
			myAnimationCenterBeam.IsReversed() == false)
		{
			myAnimationCenterBeam.SetLoop(true);
			myAnimationCenterBeam.Setup(2, 2, 1, 3);
			myCountDown.Set(std::bind(&BossBeholder::PlayEyeBeam, this), 0.3f);
		}

		if (myCenterBeamCooldown < 0.0f && myBeamDuration > 0.0f && myAnimationCenterBeam.GetLoop() == true)
		{				
			myCenterBeamStartPos = { myPosition.x, myPosition.y - 0.3f, myPosition.z + 0.0001f };
			myCenterBeamEndPos = { myPosition.x, myBottomBoundary - 1.0f, myPosition.z + 0.0001f };

			myLeftCenterBeamStartPos = { myPosition.x - 0.05f, myPosition.y - 0.3f, myPosition.z + 0.0002f };
			myLeftCenterBeamEndPos = { myPosition.x - 1.0f, myBottomBoundary - 1.0f, myPosition.z + 0.0002f };

			myRightCenterBeamStartPos = { myPosition.x + 0.05f, myPosition.y - 0.3f, myPosition.z + 0.0002f };
			myRightCenterBeamEndPos = { myPosition.x + 1.0f, myBottomBoundary - 1.0f, myPosition.z + 0.0002f };

			myCenterBeam.SetStartPosition(myCenterBeamStartPos);
			myCenterBeam.SetEndPosition(myCenterBeamEndPos);
			myCenterBeam.Activate();

			myLeftCenterBeam.SetStartPosition(myLeftCenterBeamStartPos);
			myLeftCenterBeam.SetEndPosition(myLeftCenterBeamEndPos);
			myLeftCenterBeam.Activate();

			myRightCenterBeam.SetStartPosition(myRightCenterBeamStartPos);
			myRightCenterBeam.SetEndPosition(myRightCenterBeamEndPos);
			myRightCenterBeam.Activate();
		}

		if (myCenterBeam.IsActive())
		{
			myBeamDuration -= aDeltaTime;

			if (myBeamDuration < 0.0f)
			{
				myAnimationCenterBeam.SetReversed(true);
				myAnimationCenterBeam.Setup(2, 2, 4);
				myAnimationCenterBeam.SetLoop(false);
				myCenterBeam.Dectivate();
				myLeftCenterBeam.Dectivate();
				myRightCenterBeam.Dectivate();				
			}
		}
		else if (!myCenterBeam.IsActive() && myBeamDuration < 0.0f)
		{
			if (myAnimationCenterBeam.IsFinished() &&
				myAnimationCenterBeam.GetLoop() == false &&
				myAnimationCenterBeam.IsReversed() == true)
			{
				myBeamDuration = 2.0f;
				myCenterBeamCooldown = myCenterBeamFireRate;
				myCurrentAnimation = &myAnimationIdle;
				myAnimationCenterBeam.SetReversed(false);
				myAnimationCenterBeam.Setup(2, 2, 4);
				myAnimationCenterBeam.SetLoop(false);

				if (myPosition.x < 0.1f && myPosition.x > -0.1f)
				{
					myMoveSet = eBeholderMoveSet::moveToLeftX;
				}
				else if (myPosition.x < myLeftBoundary + 0.6f)
				{
					myMoveSet = eBeholderMoveSet::moveToRightX;
				}
				else if (myPosition.x > myRightBoundary - 0.6f)
				{
					myMoveSet = eBeholderMoveSet::moveToCenterX;
				}
			}
		}

		myCenterBeam.Update(aDeltaTime, false);
		myLeftCenterBeam.Update(aDeltaTime, false);
		myRightCenterBeam.Update(aDeltaTime, false);
	}


	if (myMoveSet == eBeholderMoveSet::attackSideBeam)
	{
		mySideBeamCooldown -= aDeltaTime;
		myCurrentAnimation = &myAnimationSideBeams;

		if (myAnimationSideBeams.IsFinished() &&
			myAnimationSideBeams.GetLoop() == false &&
			myAnimationSideBeams.IsReversed() == false)
		{
			myAnimationSideBeams.SetLoop(true);
			myAnimationSideBeams.Setup(2, 2, 1, 3);
		}

		if (mySideBeamCooldown < 0 &&
			!myLeftCenterBeam.IsActive() &&
			!myRightCenterBeam.IsActive() &&
			myAnimationSideBeams.GetLoop() == true)
		{
			myLeftCenterBeamStartPos = { myPosition.x - 0.72f, myPosition.y - 0.2f, myPosition.z + 0.0001f };
			myLeftCenterBeamEndPos = { myPosition.x - 4.0f, myBottomBoundary - 1.0f, myPosition.z + 0.0001f };

			myRightCenterBeamStartPos = { myPosition.x + 0.72f, myPosition.y - 0.2f, myPosition.z + 0.0001f };
			myRightCenterBeamEndPos = { myPosition.x + 4.0f, myBottomBoundary - 1.0f, myPosition.z + 0.0001f };

			myLeftCenterBeam.Activate();
			myRightCenterBeam.Activate();

		}

		if (myLeftCenterBeam.IsActive() && myRightCenterBeam.IsActive())
		{			
			SideBeamAttack(aDeltaTime);

			if (myPlayBeamSound)
			{
				PlayEyeBeam();
				myPlayBeamSound = false;
				myCountDown.Set([=] { myPlayBeamSound = true; }, 1.9f);
			}
		}
		else if (!myLeftCenterBeam.IsActive() &&
				 !myRightCenterBeam.IsActive() &&
				 myLeftCenterBeamEndPos.x > myRightBoundary ||
				 myRightCenterBeamEndPos.x < myLeftBoundary)
		{
			if (myAnimationSideBeams.IsFinished() &&
				myAnimationSideBeams.GetLoop() == false &&
				myAnimationSideBeams.IsReversed() == true)
			{
				mySideBeamCooldown = mySideBeamFireRate;
				myCurrentAnimation = &myAnimationIdle;
				myAnimationSideBeams.SetReversed(false);
				myAnimationSideBeams.Setup(2, 2, 4);
				myMoveSet = eBeholderMoveSet::ultimateGettingReady;
			}
		}

		myLeftCenterBeam.Update(aDeltaTime, false);
		myRightCenterBeam.Update(aDeltaTime, false);
	}

	if (myMoveSet == eBeholderMoveSet::attackUltimate)
	{
		myUltimateCooldown -= aDeltaTime;
		myCurrentAnimation = &myAnimationUltimate;
		UpdateBeamRotation();

		if (myAnimationUltimate.IsFinished() &&
			myAnimationUltimate.GetLoop() == false &&
			myAnimationUltimate.IsReversed() == false)
		{
			myCurrentAnimation = &myAnimationBody;
		}

		if (myUltimateCooldown < 0.0f &&
			!myLeftTopBeam.IsActive() &&
			!myLeftCenterBeam.IsActive() &&
			!myLeftBottomBeam.IsActive() &&
			!myRightTopBeam.IsActive() &&
			!myRightCenterBeam.IsActive() &&
			!myRightBottomBeam.IsActive() &&
			myCurrentAnimation == &myAnimationBody)
		{
			myLeftTopBeam.Activate();
			myLeftCenterBeam.Activate();
			myLeftBottomBeam.Activate();
			myRightTopBeam.Activate();
			myRightCenterBeam.Activate();
			myRightBottomBeam.Activate();
		}

		if (myLeftTopBeam.IsActive() &&
			myLeftCenterBeam.IsActive() &&
			myLeftBottomBeam.IsActive() &&
			myRightTopBeam.IsActive() &&
			myRightCenterBeam.IsActive() &&
			myRightBottomBeam.IsActive())
		{
			UltimateAttack(aDeltaTime);
		}
		else if (!myLeftTopBeam.IsActive() &&
				 !myLeftCenterBeam.IsActive() &&
				 !myLeftBottomBeam.IsActive() &&
				 !myRightTopBeam.IsActive() &&
				 !myRightCenterBeam.IsActive() &&
				 !myRightBottomBeam.IsActive() &&
				 myAnimationUltimate.IsReversed() == true)
		{
			myWaitBeforeAndAfterUltimate -= aDeltaTime;

			if (myWaitBeforeAndAfterUltimate < 0.0f)
			{
				if (myAnimationUltimate.IsFinished() &&
					myAnimationUltimate.GetLoop() == false &&
					myAnimationUltimate.IsReversed() == true)
				{
					myRotation = 0.0f;
					myRotationSpeed = 0.0f;
					mySpeedUp = true;
					myUltimateCooldown = myUltimateFireRate;
					myCurrentAnimation = &myAnimationIdle;
					myAnimationUltimate.SetReversed(false);
					myAnimationUltimate.Setup(2, 3, 3);
					myMoveSet = eBeholderMoveSet::returnToStartingPos;
					myWaitBeforeAndAfterUltimate = 1.5f;
				}
			}
		}

		if (myRotation >= 4 * Tga2D::Pif && myAnimationUltimate.IsReversed() == false)
		{
			myLeftTopBeam.Dectivate();
			myLeftCenterBeam.Dectivate();
			myLeftBottomBeam.Dectivate();
			myRightTopBeam.Dectivate();
			myRightCenterBeam.Dectivate();
			myRightBottomBeam.Dectivate();
			myCurrentAnimation = &myAnimationUltimate;
			myAnimationUltimate.SetReversed(true);
			myAnimationUltimate.Setup(2, 3, 3);
			myAnimationUltimate.SetLoop(false);			
		}

	}

	if (myMoveSet == eBeholderMoveSet::attackUltimate ||
		myMoveSet == eBeholderMoveSet::returnToStartingPos)
	{
		myLeftTopBeam.Update(aDeltaTime, false);
		myLeftCenterBeam.Update(aDeltaTime, false);
		myLeftBottomBeam.Update(aDeltaTime, false);

		myRightTopBeam.Update(aDeltaTime, false);
		myRightCenterBeam.Update(aDeltaTime, false);
		myRightBottomBeam.Update(aDeltaTime, false);
	}



	if (myMoveSet == eBeholderMoveSet::moveToCenterX ||
		myMoveSet == eBeholderMoveSet::moveToLeftX ||
		myMoveSet == eBeholderMoveSet::moveToRightX ||
		myMoveSet == eBeholderMoveSet::ultimateGettingReady ||
		myMoveSet == eBeholderMoveSet::returnToStartingPos)
	{
		Move(aDeltaTime);
	}	

	myCurrentAnimation->Update(aDeltaTime);
	myCurrentAnimation->SetPosition({ myPosition.x, myPosition.y - 0.2f, myPosition.z + 0.001f });

		
	DamagedFlash(*myCurrentAnimation);
	if (myCurrentAnimation == &myAnimationBody)
	{
		DamagedFlash(myLeftTopEye);
		DamagedFlash(myLeftCenterEye);
		DamagedFlash(myLeftBottomEye);
		DamagedFlash(myRightTopEye);
		DamagedFlash(myRightCenterEye);
		DamagedFlash(myRightBottomEye);
	}
	if (myFlashTimer > 0.0f)
	{
		myFlashTimer -= aDeltaTime;
	}

	myHealthBar.Update(aDeltaTime, myHealth, { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f });
	myHealthBar.Show();
	myCountDown.Update(aDeltaTime);
}

void BossBeholder::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{

	aRenderBuffer.Add(myCurrentAnimation);

	myCenterBeam.FillRenderBuffer(aRenderBuffer);
	myLeftTopBeam.FillRenderBuffer(aRenderBuffer);
	myLeftCenterBeam.FillRenderBuffer(aRenderBuffer);
	myLeftBottomBeam.FillRenderBuffer(aRenderBuffer);
	myRightTopBeam.FillRenderBuffer(aRenderBuffer);
	myRightCenterBeam.FillRenderBuffer(aRenderBuffer);
	myRightBottomBeam.FillRenderBuffer(aRenderBuffer);
	myHealthBar.FillRenderBuffer(aRenderBuffer);

	if (myCurrentAnimation == &myAnimationBody)
	{
		aRenderBuffer.Add(&myLeftTopEye);
		aRenderBuffer.Add(&myLeftCenterEye);
		aRenderBuffer.Add(&myLeftBottomEye);
		aRenderBuffer.Add(&myRightTopEye);
		aRenderBuffer.Add(&myRightCenterEye);
		aRenderBuffer.Add(&myRightBottomEye);
	}
}

void BossBeholder::TakeDamage(const float aDamage, const float aDeltaTime)
{
	if (!myPlayBossDeathAnimation)
	{
		myHealth -= aDamage * aDeltaTime;
		if (myBleed)
		{
			SpawnBlood(3, 1.5f);
			if (myCanPlayTakeDamageSound)
			{
				myCanPlayTakeDamageSound = false;
				SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/BossHurt.wav", 0.5f);
				myCountDown.Set(std::bind(&BossBeholder::CanPlayTakeDamageSound, this), 0.5f);
			}
		}

		if (myFlashTimer < 0.8f)
		{
			myFlashTimer = 1.0f;
		}

		if (myHealth < 1.0f)
		{
			myHealth = 1.0f;
			myFlashTimer = 0.0f;
			DamagedFlash(*myCurrentAnimation);			
			myBleed = true;
			myCountDown.Set(std::bind(&BossBeholder::FinalBossExplosion, this), 3.0f);
			myCountDown.Set(std::bind([=] { Blackboard::GetInstance()->SetNote("deathslowmotion", "true"); }), 2.0f);
			myPlayBossDeathAnimation = true;
		}
	}
}

bool BossBeholder::IsInside(const CU::Vector2f & aPoint)
{
	aPoint;
	return myCurrentAnimation->IsInside(aPoint);
}

void BossBeholder::ShootAttack(float aAngel)
{
	float a = aAngel;

	ProjectileBeholderShot* projectileOne = ProjectileFactory::CreateBeholderShot({ myPosition.x - 0.4f, myPosition.y - 0.45f, myPosition.z },
		myPrimaryAttackProjectileSpeed,
		CU::Vector3f(-std::cos(a), -std::sin(a), 0.f));
	projectileOne->SetDamage(myEnemyClass.GetDMG());
	projectileOne->SetGlow(true, { 0.8f, 0.8f, 1.0f, 0.0f });
	ProjectileManager::GetInstance()->EnemyShoot(projectileOne);

	ProjectileBeholderShot* projectileTwo = ProjectileFactory::CreateBeholderShot({ myPosition.x + 0.4f, myPosition.y - 0.45f, myPosition.z },
		myPrimaryAttackProjectileSpeed,
		CU::Vector3f(std::cos(a), -std::sin(a), 0.f));
	projectileTwo->SetDamage(myEnemyClass.GetDMG());
	projectileTwo->SetGlow(true, { 0.8f, 0.8f, 1.0f, 0.0f });
	ProjectileManager::GetInstance()->EnemyShoot(projectileTwo);
}


void BossBeholder::SideBeamAttack(float aDeltaTime)
{
	myLeftCenterBeamStartPos = { myPosition.x - 0.72f, myPosition.y - 0.2f, myPosition.z + 0.0001f };
	myRightCenterBeamStartPos = { myPosition.x + 0.72f, myPosition.y - 0.2f, myPosition.z + 0.0001f };

	myLeftCenterBeam.SetStartPosition(myLeftCenterBeamStartPos);
	myLeftCenterBeam.SetEndPosition(myLeftCenterBeamEndPos);

	myRightCenterBeam.SetStartPosition(myRightCenterBeamStartPos);
	myRightCenterBeam.SetEndPosition(myRightCenterBeamEndPos);

	if (myLeftCenterBeamEndPos.x < myRightBoundary && myRightCenterBeamEndPos.x > myLeftBoundary)
	{
		myLeftCenterBeamEndPos.x += aDeltaTime * 1.5f;
		myRightCenterBeamEndPos.x -= aDeltaTime * 1.5f;
	}
	else if (myAnimationSideBeams.IsReversed() == false)
	{
		myAnimationSideBeams.SetReversed(true);
		myAnimationSideBeams.Setup(2, 2, 4);
		myAnimationSideBeams.SetLoop(false);
		myLeftCenterBeam.Dectivate();
		myRightCenterBeam.Dectivate();
		
	}
}

void BossBeholder::UltimateAttack(float aDeltaTime)
{
	if ((myRotation < (2 * Tga2D::Pif)) && mySpeedUp)
	{
		if (myRotationSpeed < 1.5f)
		{
			myRotationSpeed += aDeltaTime * 0.2f;
		}
		myRotation += myRotationSpeed * aDeltaTime;
	}
	else
	{
		mySpeedUp = false;
	}

	if (!mySpeedUp && (myRotation < (4 * Tga2D::Pif)))
	{
		if (myRotationSpeed > 0.25f)
		{
			myRotationSpeed -= aDeltaTime * 0.18f;
		}
		myRotation += myRotationSpeed * aDeltaTime;
	}


	myBeamSpinnRotation = myRotation;
	myBeamSpinnRotation += Tga2D::Pif / 8.f;
	while (myBeamSpinnRotation < 0.f)myBeamSpinnRotation += Tga2D::Pif*2.f;
	while (myBeamSpinnRotation > Tga2D::Pif*2.f)myBeamSpinnRotation -= Tga2D::Pif*2.f;


	int rightTop = static_cast<int>(myBeamSpinnRotation / ((Tga2D::Pif*2.f) / 8.f)) + 1;
	if (rightTop >= 8)
	{
		rightTop = rightTop - 8;
	}

	int rightCenter = static_cast<int>(myBeamSpinnRotation / ((Tga2D::Pif*2.f) / 8.f)) + 0;
	if (rightCenter >= 8)
	{
		rightCenter = rightCenter - 8;
	}

	int RightBottom = static_cast<int>(myBeamSpinnRotation / ((Tga2D::Pif*2.f) / 8.f)) + 7;
	if (RightBottom >= 8)
	{
		RightBottom = RightBottom - 8;
	}

	int leftTop = static_cast<int>(myBeamSpinnRotation / ((Tga2D::Pif*2.f) / 8.f)) + 3;
	if (leftTop >= 8)
	{
		leftTop = leftTop - 8;
	}

	int leftCenter = static_cast<int>(myBeamSpinnRotation / ((Tga2D::Pif*2.f) / 8.f)) + 4;
	if (leftCenter >= 8)
	{
		leftCenter = leftCenter - 8;
	}

	int leftBottom = static_cast<int>(myBeamSpinnRotation / ((Tga2D::Pif*2.f) / 8.f)) + 5;
	if (leftBottom >= 8)
	{
		leftBottom = leftBottom - 8;
	}
	
	myRightTopEye.Setup(2, 4, 1, rightTop);
	myRightCenterEye.Setup(2, 4, 1, rightCenter);
	myRightBottomEye.Setup(2, 4, 1, RightBottom);
	myLeftTopEye.Setup(2, 4, 1, leftTop);
	myLeftCenterEye.Setup(2, 4, 1, leftCenter);
	myLeftBottomEye.Setup(2, 4, 1, leftBottom);
}

void BossBeholder::Move(float aDeltaTime)
{
	if (myMoveSet == eBeholderMoveSet::moveToCenterX)
	{
		if (myPosition.x > 0.0f)
		{
			myPosition.x -= myMovementSpeed * aDeltaTime;
		}
		else
		{
			myMoveSet = eBeholderMoveSet::attackSideBeam;
		}
	}
	else if (myMoveSet == eBeholderMoveSet::moveToLeftX)
	{
		if (myPosition.x > myLeftBoundary + 0.5f)
		{
			myPosition.x -= myMovementSpeed * aDeltaTime;
		}
		else
		{
			myMoveSet = eBeholderMoveSet::attackCenterBeam;
		}
	}
	else if (myMoveSet == eBeholderMoveSet::moveToRightX)
	{
		if (myPosition.x < myRightBoundary - 0.5f)
		{
			myPosition.x += myMovementSpeed * aDeltaTime;
		}
		else
		{
			myMoveSet = eBeholderMoveSet::attackCenterBeam;
		}
	}
	else if (myMoveSet == eBeholderMoveSet::ultimateGettingReady)
	{
		myWaitBeforeAndAfterUltimate -= aDeltaTime;

		if (myWaitBeforeAndAfterUltimate < 0.0f)
		{
			if (myPosition.y > myCenterY)
			{
				myPosition.y -= myMovementSpeed * aDeltaTime;
			}
			else
			{
				myWaitBeforeAndAfterUltimate = 0.5f;
				myMoveSet = eBeholderMoveSet::attackUltimate;
			}
		}
	}
	else if (myMoveSet == eBeholderMoveSet::returnToStartingPos)
	{		
		if (myPosition.y < myTopBoundary)
		{
			myPosition.y += myMovementSpeed * aDeltaTime;
		}
		else
		{			
			myMoveSet = eBeholderMoveSet::attackPrimary;
		}
	}
}

CommonUtilities::Vector3f BossBeholder::CreateBeamVector(const CommonUtilities::Vector3f& aOffset, float aRotation, const CommonUtilities::Vector2f& aRadii)
{
	return{
		myPosition.x + aOffset.x + std::cosf(aRotation)*aRadii.x,
		myPosition.y + aOffset.y + std::sinf(aRotation)*aRadii.y,
		myPosition.z + aOffset.z + 0.001f
	};
}

void BossBeholder::UpdateBeamRotation()
{
	float offsetY = 0.15f;
	float rx = 1.f / 2.75f;
	float ry = 1.f / 3.f;

	myLeftTopBeamStartPos = CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation - Tga2D::Pif / 4.f + Tga2D::Pif, { rx, ry });
	myLeftTopBeamEndPos = CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation - Tga2D::Pif / 4.f + Tga2D::Pif);

	myLeftCenterBeamStartPos = CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation + Tga2D::Pif, { rx, ry });
	myLeftCenterBeamEndPos = CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation + Tga2D::Pif);

	myLeftBottomBeamStartPos = CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation + Tga2D::Pif / 4.f + Tga2D::Pif, { rx, ry });
	myLeftBottomBeamEndPos = CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation + Tga2D::Pif / 4.f + Tga2D::Pif);

	myRightTopBeamStartPos = CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation + Tga2D::Pif / 4.f, { rx, ry });
	myRightTopBeamEndPos = CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation + Tga2D::Pif / 4.f);

	myRightCenterBeamStartPos = CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation, { rx, ry });
	myRightCenterBeamEndPos = CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation);

	myRightBottomBeamStartPos = CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation - Tga2D::Pif / 4.f, { rx, ry });
	myRightBottomBeamEndPos = CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation - Tga2D::Pif / 4.f);


	myLeftTopBeam.SetStartPosition(myLeftTopBeamStartPos);
	myLeftTopBeam.SetEndPosition(myLeftTopBeamEndPos);
	myLeftTopEye.SetPosition(CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation - Tga2D::Pif / 4.f + Tga2D::Pif, { rx*0.75f, ry*0.75f }));

	myLeftCenterBeam.SetStartPosition(myLeftCenterBeamStartPos);
	myLeftCenterBeam.SetEndPosition(myLeftCenterBeamEndPos);
	myLeftCenterEye.SetPosition(CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation + Tga2D::Pif, { rx*0.75f, ry*0.75f }));

	myLeftBottomBeam.SetStartPosition(myLeftBottomBeamStartPos);
	myLeftBottomBeam.SetEndPosition(myLeftBottomBeamEndPos);
	myLeftBottomEye.SetPosition(CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation + Tga2D::Pif / 4.f + Tga2D::Pif, { rx*0.75f, ry*0.75f }));

	myRightTopBeam.SetStartPosition(myRightTopBeamStartPos);
	myRightTopBeam.SetEndPosition(myRightTopBeamEndPos);
	myRightTopEye.SetPosition(CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation + Tga2D::Pif / 4.f, { rx*0.75f, ry*0.75f }));
	  
	myRightCenterBeam.SetStartPosition(myRightCenterBeamStartPos);
	myRightCenterBeam.SetEndPosition(myRightCenterBeamEndPos);
	myRightCenterEye.SetPosition(CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation, { rx*0.75f, ry*0.75f }));
	  
	myRightBottomBeam.SetStartPosition(myRightBottomBeamStartPos);
	myRightBottomBeam.SetEndPosition(myRightBottomBeamEndPos);
	myRightBottomEye.SetPosition(CreateBeamVector({ 0.f, -offsetY, 0.f }, myRotation - Tga2D::Pif / 4.f, { rx*0.75f, ry*0.75f }));
}

void BossBeholder::RenderDebug(const CommonUtilities::Camera & aCamera)
{
	Enemy::RenderDebug(aCamera);
	if (myCenterBeam.IsActive())myCenterBeam.RenderDebug(aCamera);

	Enemy::RenderDebug(aCamera);
	if (myLeftTopBeam.IsActive())myLeftTopBeam.RenderDebug(aCamera);
	Enemy::RenderDebug(aCamera);
	if (myLeftCenterBeam.IsActive())myLeftCenterBeam.RenderDebug(aCamera);
	Enemy::RenderDebug(aCamera);
	if (myLeftBottomBeam.IsActive())myLeftBottomBeam.RenderDebug(aCamera);

	Enemy::RenderDebug(aCamera);
	if (myRightTopBeam.IsActive())myRightTopBeam.RenderDebug(aCamera);
	Enemy::RenderDebug(aCamera);
	if (myRightCenterBeam.IsActive())myRightCenterBeam.RenderDebug(aCamera);
	Enemy::RenderDebug(aCamera);
	if (myRightBottomBeam.IsActive())myRightBottomBeam.RenderDebug(aCamera);
}

inline bool BossBeholder::IsTouching(const CircleCollider * aCircleCollider)
{
	if (myHealth > 1)
	{
		return myCollider.IsTouching(aCircleCollider);
	}

	return false;
}

inline bool BossBeholder::BeamCollision(const CircleCollider * aCircleCollider)
{
	if (myCenterBeam.IsTouching(aCircleCollider))
	{
		return true;
	}
	else if (myLeftTopBeam.IsTouching(aCircleCollider))
	{
		return true;
	}
	else if (myLeftCenterBeam.IsTouching(aCircleCollider))
	{
		return true;
	}
	else if (myLeftBottomBeam.IsTouching(aCircleCollider))
	{
		return true;
	}
	else if (myRightTopBeam.IsTouching(aCircleCollider))
	{
		return true;
	}
	else if (myRightCenterBeam.IsTouching(aCircleCollider))
	{
		return true;
	}
	else if (myRightBottomBeam.IsTouching(aCircleCollider))
	{
		return true;
	}

	return false;
}

void BossBeholder::DeathAnimation()
{
	if (mySpawnExplosion)
	{
		SpawnExplosion(0.15f);
	}
	
	if (myBleed)
	{
		SpawnBlood(12, 0.2f);
	}

	if (myHasPlayedFinalBossExplosion)
	{
		myHealth = 0.0f;
	}
}

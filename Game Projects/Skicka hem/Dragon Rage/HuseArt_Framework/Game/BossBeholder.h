#pragma once
#include "Enemy.h"
#include "Beam.h"
#include "SFXManager.h"
#include <Animation.h>

class Player;

enum class eBeholderMoveSet
{
	attackPrimary,
	attackCenterBeam,
	attackSideBeam,
	attackUltimate,
	moveToLeftX,
	moveToRightX,
	moveToCenterX,
	ultimateGettingReady,
	returnToStartingPos
};

class BossBeholder : public Enemy
{
public:
	BossBeholder();
	~BossBeholder();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float aDeltaTime, Player& aPlayer) override;
	void UpdateCollider() override { myCollider.SetPosition(myPosition - CommonUtilities::Vector3f(0.f, 0.125f, 0.f)); }
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsBoss() override { return true; }

	inline bool IsTouching(const CircleCollider* aCircleCollider) override;
	inline bool BeamCollision(const CircleCollider* aCircleCollider) override;
	inline bool IsTouching(Projectile* aProjectile) override { return aProjectile->IsTouching(GetCollider()); }
	void DeathAnimation() override;

	void ShootAttack(float aAngel);
	void SideBeamAttack(float aDeltaTime);
	void UltimateAttack(float aDeltaTime);
	
	void Move(float aDeltaTime);
	void UpdateBeamRotation();

	void PlayCenterEyeOpen() { SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/BeholderEyebeamChargeUp.wav", 0.5f); }
	void PlayEyeBeam() { SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/BeholderEyeBeam.wav", 0.5f); }

	void RenderDebug(const CommonUtilities::Camera& aCamera) override;

private:
	CommonUtilities::Vector3f CreateBeamVector(const CommonUtilities::Vector3f& aOffset, float aRotation, const CommonUtilities::Vector2f& aRadii = { 2.f, 2.f });
	

	bool myIsSlowed;
	float mySlowTimer;

	Animation* myCurrentAnimation;
	Animation myAnimationIdle;
	Animation myAnimationCenterBeam;
	Animation myAnimationSideBeams;
	Animation myAnimationUltimate;
	Animation myAnimationBody;	

	Animation myLeftTopEye;
	Animation myLeftCenterEye;
	Animation myLeftBottomEye;
	Animation myRightTopEye;
	Animation myRightCenterEye;
	Animation myRightBottomEye;

	bool myIncreaseTint;
	float myCurrentTint;
	float myDeathTint;

	Beam myCenterBeam;
	bool myPlayBeamSound;

	Beam myLeftTopBeam;
	Beam myLeftCenterBeam;
	Beam myLeftBottomBeam;
	Beam myRightTopBeam;
	Beam myRightCenterBeam;
	Beam myRightBottomBeam;

	float myBeamSpinnRotation;

	CommonUtilities::Vector3f myCenterBeamStartPos;
	CommonUtilities::Vector3f myCenterBeamEndPos;
	CommonUtilities::Vector3f myLeftTopBeamStartPos;
	CommonUtilities::Vector3f myLeftTopBeamEndPos;
	CommonUtilities::Vector3f myLeftCenterBeamStartPos;
	CommonUtilities::Vector3f myLeftCenterBeamEndPos;
	CommonUtilities::Vector3f myLeftBottomBeamStartPos;
	CommonUtilities::Vector3f myLeftBottomBeamEndPos;
	CommonUtilities::Vector3f myRightTopBeamStartPos;
	CommonUtilities::Vector3f myRightTopBeamEndPos;								
	CommonUtilities::Vector3f myRightCenterBeamStartPos;
	CommonUtilities::Vector3f myRightCenterBeamEndPos;								
	CommonUtilities::Vector3f myRightBottomBeamStartPos;
	CommonUtilities::Vector3f myRightBottomBeamEndPos;

	float myMovementSpeed;
	float myRotation;
	float myRotationSpeed;

	eBeholderMoveSet myMoveSet;

	float myPrimaryAttackProjectileSpeed;

	float myPrimaryAttackFireRate;
	float myCenterBeamFireRate;
	float mySideBeamFireRate;
	float myUltimateFireRate;

	float myPrimaryAttackCooldown;
	float myCenterBeamCooldown;
	float mySideBeamCooldown;
	float myUltimateCooldown;

	bool mySpeedUp;
	float myBeamDuration;
	float myPrimaryAttackAngel;
	int myNbrOfShoots;
	int myNbrOfNormalAttacks;
	float myWaitBeforeAndAfterUltimate;

	float myLeftBoundary;
	float myRightBoundary;
	float myTopBoundary;
	float myBottomBoundary;
	float myCenterY;

	CommonUtilities::Vector3f myShootDirection;

	//Bopping Movement
	float myHoverDistance;
	float myBoppingSpeed;
	float myHoverTime;

};



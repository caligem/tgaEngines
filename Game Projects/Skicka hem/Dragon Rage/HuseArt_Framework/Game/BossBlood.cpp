#include "stdafx.h"
#include "BossBlood.h"

#include <Random.h>
#include "ExplosionManager.h"
#include "Player.h"

BossBlood::BossBlood(const float aMinAngle, const float aMaxAngle, const float aSpeed, const float aScale)
	: myMinAngle(aMinAngle)
	, myMaxAngle(aMaxAngle)
	, mySpeed(aSpeed)
	, myScale({ aScale, aScale })
{
}

BossBlood::~BossBlood()
{
}

void BossBlood::Init(const EnemyClass & aEnemyClass, const CommonUtilities::Vector3f aPosition)
{
	aEnemyClass;
	myHealth = 1.f;
	myScoreFactor = 0.0f;

	myAnimation.Init("Assets/Images/Projectiles/bossBlood.dds", aPosition);
	myAnimation.SetScale(myScale);

	float angle = myMinAngle + CommonUtilities::Random() * (myMaxAngle - myMinAngle);
	myDirection.x = std::cosf(angle);
	myDirection.y = std::sinf(angle);
	myDirection.z = 0.f;

	myLifeSpann = 3.0f;
	myGravity = 1.0f;
	mySpeed *= CommonUtilities::Random();
	myPosition = aPosition;
	myScaleDown = false;
}

void BossBlood::Update(float aDeltaTime, Player& aPlayer)
{
	aPlayer;
	if (mySpeed > 0)
	{
		mySpeed -= aDeltaTime / 2;
	}
	else
	{
		mySpeed = 0.0f;
	}

	if (!myScaleDown)
	{
		myScale.x += aDeltaTime;
		myScale.y += aDeltaTime;
		if (myScale.x > 0.5f && myScale.y > 0.5f)
		{
			myScaleDown = true;
		}
	}
	else
	{
		myScale.x -= aDeltaTime / 2.0f;
		myScale.y -= aDeltaTime / 2.0f;
	}

	myPosition.y += myGravity * aDeltaTime;
	myGravity -= aDeltaTime * 2.0f;
	myPosition += myDirection * mySpeed * aDeltaTime;

	myAnimation.SetScale(myScale);
	myAnimation.SetPosition(myPosition);
	myAnimation.Update(aDeltaTime);

	myLifeSpann -= aDeltaTime;
	if (myLifeSpann < 0 || myScale.x < 0.1f && myScale.y < 0.1f)
	{
		Kill();
	}
}

void BossBlood::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void BossBlood::SetTint(const CommonUtilities::Vector4f& aTint)
{
	myTint = aTint;
	myAnimation.SetTint(myTint);
}

//void BossBlood::DeathAnimation()
//{
//	Animation* newSpark = new Animation("Assets/Images/Sprites/spark.dds");
//	newSpark->Setup(6, 8, 48);
//	newSpark->SetDuration(0.5f);
//	newSpark->SetLoop(true);
//	newSpark->SetPosition(GetPosition());
//	newSpark->SetTint(myTint);
//	newSpark->SetScale({ 1.f, 1.f });
//	ExplosionManager::GetInstance()->AddExplosion(newSpark);
//}

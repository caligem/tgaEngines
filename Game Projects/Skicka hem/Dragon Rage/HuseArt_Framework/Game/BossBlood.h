#pragma once
#include "Enemy.h"

#include <Animation.h>
#include "CountDown.h"


class BossBlood : public Enemy
{
public:
	BossBlood(const float aMinAngle, const float aMaxAngle, const float aSpeed, const float aScale);
	~BossBlood();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;

	void Update(float, Player&) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void SetTint(const CommonUtilities::Vector4f& aTint);

	//void DeathAnimation() override;

	bool IsInside(const CommonUtilities::Vector2f& aPoint) override { aPoint; return false; }
	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override { aDamage; aDeltaTime; }
	bool IsBoss() { return false; }

	inline bool IsTouching(const CircleCollider* aCircleCollider) override { aCircleCollider; return false; }
	inline bool IsTouching(Projectile* aProjectile) override { aProjectile; return false; }
	inline bool IsTouching(Beam& aBeam) override { aBeam; return false; }

private:
	Animation myAnimation;

	float myMinAngle;
	float myMaxAngle;
	float mySpeed;
	float myGravity;
	float myLifeSpann;
	bool myScaleDown;

	CommonUtilities::Vector2f myScale;
	CommonUtilities::Vector3f myDirection;
	CommonUtilities::Vector4f myTint;
};


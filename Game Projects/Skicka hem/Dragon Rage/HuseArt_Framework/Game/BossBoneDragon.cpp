#include "stdafx.h"
#include "BossBoneDragon.h"

#include "ProjectileManager.h"
#include "Player.h"
#include "SFXManager.h"

#include "ProjectileFactory.h"
#include "EnemyFactory.h"
#include "ExplosionManager.h"
#include "EnemyFactory.h"
#include "Blackboard.h"

#include <Random.h>

namespace CU = CommonUtilities;

BossBoneDragon::BossBoneDragon()
	: myRotation(0)
	, myMovementSpeed(0.5f)
	, myPrimaryAttackProjectileSpeed(1.5f)
	, myEnergyWaveProjectileSpeed(2.0f)
	, myGranadeProjectileSpeed(0.9f)
	, myPrimaryAttackFireRate(1.0f)
	, myEnergyWaveFireRate(2.0f)
	, myGranadeFireRate(1.0f)
	, myBeamFireRate(1.0f)
	, myExplosionFireRate(1.0f)
	, myPrimaryAttackAngel(Tga2D::Pif / 8)
	, myNbrOfNormalAttacks(0)
	, myNbrOfGranadeAttacks(0)
	, myBeamStartPos(0.0f, 0.0f, 0.0f)
	, myBeamEndPos(0.0f, 0.0f, 0.0f)
	, myShootDirection(0.0f, 0.0f, 0.0f)
	, myIncreaseTint(true)
	, myDeathTint(0.0f)
	, myCurrentTint(1.0f)
	, myNbrOfBombs(0)
	, myRightGranadeThrow(false)
	, myBeamRight(true)
	, myBeamDone(false)
	, myLeftBoundary(-1.4f)
	, myRightBoundary(1.4f)
	, myTopBoundary(0.0f)
	, myBottomBoundary(-1.8f)
	, myCenterY(0)
	, myIsSlowed(false)
	, myIgnoreElement(true)
	, myPlayWingAnimation(true)
	, myChangeStance(false)
	, myShineAlfa(0.0f)
	, myTintAmount(1.0f)
	, myHoverDistance(0.05f)
	, myBoppingSpeed(0.5f)
	, myHoverTime(0.0f)
{}



BossBoneDragon::~BossBoneDragon()
{
}

void BossBoneDragon::Init(const EnemyClass& aEnemyClass, const CU::Vector3f aPosition)
{
	myPosition = aPosition;

	myAnimationIdle.Init("Assets/Images/Enemies/Bosses/enemy_boss_bonedragon.dds", { myPosition.x, myPosition.y - 0.3f, myPosition.z + 0.001f });
	myAnimationIdle.Setup(2, 2, 4);
	myAnimationLightning.Init("Assets/Images/Enemies/Bosses/enemy_boss_bonedragon_lightningShot.dds", { myPosition.x, myPosition.y - 0.3f, myPosition.z + 0.001f });
	myAnimationLightning.Setup(2, 4, 4, 4);
	myAnimationLightning.SetLoop(false);
	myAnimationIce.Init("Assets/Images/Enemies/Bosses/enemy_boss_bonedragon_granade.dds", { myPosition.x, myPosition.y - 0.3f, myPosition.z + 0.001f });
	myAnimationIce.Setup(2, 4, 4);
	myAnimationIce.SetLoop(false);
	myAnimationFire.Init("Assets/Images/Enemies/Bosses/enemy_boss_bonedragon_explosions.dds", { myPosition.x, myPosition.y - 0.3f, myPosition.z + 0.001f });
	myAnimationFire.Setup(3, 4, 4, 8);
	myAnimationFire.SetLoop(false);
	myAnimationWingClouds.Init("Assets/Images/Enemies/Bosses/enemy_boss_bonedragon_wingClouds.dds", { myPosition.x, myPosition.y - 0.3f, myPosition.z + 0.001f });
	myAnimationWingClouds.Setup(2, 2, 2);
	myAnimationWingClouds.SetLoop(false);
	myAnimationBeam.Init("Assets/Images/Enemies/Bosses/enemy_boss_bonedragon_Beam.dds", { myPosition.x, myPosition.y - 0.3f, myPosition.z + 0.001f });
	myAnimationBeam.Setup(3, 4, 4, 8);
	myAnimationBeam.SetLoop(true);
	myCurrentAnimation = &myAnimationLightning;


	myShine.Init("Assets/Images/GUI/powerShine.dds", { myPosition.x, myPosition.y, myPosition.z + 0.0001f });
	myShine.SetScale({ 5.0f, 5.0f });
	myShine.SetPosition({ myPosition.x, myPosition.y - 0.3f, myPosition.z + 0.002f });
	myShine.SetTint({ 1.0f, 1.0f, 1.0f, myShineAlfa });

	myEnemyClass = aEnemyClass;
	myHealth = myEnemyClass.GetHP();
	myPrimaryAttackCooldown = myPrimaryAttackFireRate;
	myEnegryWaveCooldown = myEnergyWaveFireRate;
	myGranadeCooldown = myGranadeFireRate;
	myBeamCooldown = myBeamFireRate;
	myExplosionCooldown = myExplosionFireRate;
	myTopBoundary += myPosition.y;
	myBottomBoundary += myPosition.y;
	myCenterY = (myTopBoundary + myBottomBoundary) / 2.0f;

	myBeam.Init(eBeamType::BoneDragon);
	myBeam.Dectivate();

	myMoveSet = eBoneDragonMoveSet::attackPrimary;
	myCollider.SetRadius(0.25f);

	myHealthBar.Init(myEnemyClass.GetHP(), { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f }, FLT_MAX, { 8.0f, 1.0f });

	myCanPlayTakeDamageSound = true;

	myBleed = true;
	mySpawnExplosion = true;
	myPlayBossDeathAnimation = false;
	myHasPlayedFinalBossExplosion = false;

	if (myEnemyClass.GetType().find(".hafd") != std::string::npos)
	{
		myDialogue.Init(myEnemyClass.GetType());
	}
}

void BossBoneDragon::Update(float aDeltaTime, Player& aPlayer)
{
	if (myHealth == 1)
	{
		DeathAnimation();
		myDeathTint += aDeltaTime / 20.0f;

		if (myCurrentAnimation->GetTint().x > 3.0f)
		{
			myIncreaseTint = false;
		}
		else if (myCurrentAnimation->GetTint().x < 0.0f)
		{
			myIncreaseTint = true;
		}

		if (myIncreaseTint)
		{
			myCurrentTint += myDeathTint;
			myCurrentAnimation->SetTint({ myCurrentTint, myCurrentTint, myCurrentTint, 1.0f });
		}
		else if (!myIncreaseTint)
		{
			myCurrentTint -= myDeathTint;
			myCurrentAnimation->SetTint({ myCurrentTint, myCurrentTint, myCurrentTint, 1.0f });
		}
		
		myBeam.Dectivate();

		myHealthBar.Update(aDeltaTime, myHealth, { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f });
		myHealthBar.Show();
		myCountDown.Update(aDeltaTime);

		return;
	}
	
	myPosition += CommonUtilities::Vector3f(0.f, myHoverDistance * std::sinf(myHoverTime * myBoppingSpeed), 0.f) * aDeltaTime;
	myHoverTime += aDeltaTime * 2.f * Tga2D::Pif;

	if (myMoveSet == eBoneDragonMoveSet::attackPrimary)
	{
		myIgnoreElement = false;
		myElement = eElement::Lightning;

		myPrimaryAttackCooldown -= aDeltaTime;

		if (myPrimaryAttackCooldown < 0)
		{
			if (myAnimationLightning.IsFinished() &&
				myAnimationLightning.GetLoop() == false)
			{
				myAnimationLightning.Setup(2, 4, 4);
				myAnimationLightning.SetLoop(true);
			}
			else if (myAnimationLightning.GetLoop() == true)
			{
				if (myNbrOfNormalAttacks < 5)
				{
					ShootAttack(myPrimaryAttackAngel);
					myPrimaryAttackAngel += Tga2D::Pif / 11;
					myPrimaryAttackCooldown = 0.3f;
					++myNbrOfNormalAttacks;
				}
				else if (myNbrOfNormalAttacks < 10)
				{
					ShootAttack(myPrimaryAttackAngel);
					myPrimaryAttackAngel -= Tga2D::Pif / 11;
					myPrimaryAttackCooldown = 0.3f;
					++myNbrOfNormalAttacks;
				}
				else if (myNbrOfNormalAttacks < 15)
				{
					ShootAttack(myPrimaryAttackAngel);
					myPrimaryAttackAngel += Tga2D::Pif / 12;
					myPrimaryAttackCooldown = 0.3f;
					++myNbrOfNormalAttacks;
				}
				else if (myNbrOfNormalAttacks < 20)
				{
					ShootAttack(myPrimaryAttackAngel);
					myPrimaryAttackAngel -= Tga2D::Pif / 12;
					myPrimaryAttackCooldown = 0.3f;
					++myNbrOfNormalAttacks;
				}
				else
				{
					if (myAnimationLightning.IsFinished() &&
						myAnimationLightning.GetOffset() == 0)
					{
						myAnimationLightning.Setup(2, 4, 4, 4);
					}
					else if (myAnimationLightning.IsFinished() &&
							 myAnimationLightning.GetOffset() == 4)
					{
						myChangeStance = true;

						if (myTintAmount > 8.0f)
						{
							if (myAnimationLightning.IsFinished())
							{
								myNbrOfNormalAttacks = 0;
								myPrimaryAttackAngel = Tga2D::Pif / 8;
								myPrimaryAttackCooldown = myPrimaryAttackFireRate;

								if (myHealth < myEnemyClass.GetHP() / 3.0f)
								{
									myAnimationLightning.Setup(2, 4, 4, 4);
									myCurrentAnimation = &myAnimationIce;
									myMoveSet = eBoneDragonMoveSet::attackGranades;
								}
								else
								{
									myAnimationLightning.Setup(2, 4, 4, 4);
									myCurrentAnimation = &myAnimationIdle;
									myMoveSet = eBoneDragonMoveSet::attackWings;
								}
							}
						}
					}
				}
			}
		}
	}

	if (myMoveSet == eBoneDragonMoveSet::attackWings || myHealth < myEnemyClass.GetHP() / 3.0f)
	{
		if (myMoveSet == eBoneDragonMoveSet::attackWings)
		{
			myIgnoreElement = true;
			myEnegryWaveCooldown -= aDeltaTime;

			if (myEnegryWaveCooldown < 0)
			{
				if (myAnimationIdle.IsFinished() &&
					myPlayWingAnimation)
				{
					myAnimationIdle.Setup(2, 2, 4);
					myAnimationWingClouds.Setup(2, 2, 3);
					myPlayWingAnimation = false;
				}
				else if (myAnimationWingClouds.IsFinished())
				{
					if (!myChangeStance)
					{
						WingAttack();
						myAnimationWingClouds.Setup(2, 2, 2, 2);
					}

					myChangeStance = true;

					if (myTintAmount > 8.0f)
					{
						if (myCurrentAnimation->IsFinished())
						{
							myCurrentAnimation = &myAnimationIce;
							myEnegryWaveCooldown = myEnergyWaveFireRate;
							myPlayWingAnimation = true;
							myMoveSet = eBoneDragonMoveSet::attackGranades;
						}
					}
				}
			}
		}
		else if (myHealth < myEnemyClass.GetHP() / 3.0f)
		{
			myEnegryWaveCooldown -= aDeltaTime;

			if (myEnegryWaveCooldown < 0)
			{
				if (myCurrentAnimation->IsFinished() &&
					myCurrentAnimation->GetFrames() == 4 &&
					myPlayWingAnimation)
				{
					myAnimationWingClouds.Setup(2, 2, 3);
					myPlayWingAnimation = false;
				}
				else if (myAnimationWingClouds.IsFinished() &&
						 myAnimationWingClouds.GetOffset() == 0)
				{
					WingAttack();
					myAnimationWingClouds.Setup(2, 2, 2, 2);
					myPlayWingAnimation = true;
					myEnegryWaveCooldown = myEnergyWaveFireRate * 2.0f;
				}
			}
		}
	}

	if (myMoveSet == eBoneDragonMoveSet::attackGranades)
	{
		myIgnoreElement = false;
		myElement = eElement::Ice;

		myGranadeCooldown -= aDeltaTime;

		if (myGranadeCooldown < 0)
		{
			if (myNbrOfGranadeAttacks < 6)
			{
				if (myAnimationIce.IsFinished())
				{
					if (!myRightGranadeThrow)
					{
						myAnimationIce.Setup(2, 4, 4, 3);
					}
					else if (myRightGranadeThrow)
					{
						myAnimationIce.Setup(2, 4, 4);
					}

					GranadeAttack();
					++myNbrOfGranadeAttacks;
				}				
			}
			else
			{	
				myChangeStance = true;

				if (myTintAmount > 8.0f)
				{
					if (myCurrentAnimation->IsFinished())
					{
						myAnimationIce.Setup(2, 4, 4);
						myCurrentAnimation = &myAnimationFire;
						myMoveSet = eBoneDragonMoveSet::attackBombs;
						myGranadeCooldown = myGranadeFireRate;
						myNbrOfGranadeAttacks = 0;
					}
				}
			}
		}
	}

	if (myMoveSet == eBoneDragonMoveSet::attackBombs)
	{
		myIgnoreElement = false;
		myElement = eElement::Fire;

		myExplosionCooldown -= aDeltaTime;

		if (myExplosionCooldown < 0)
		{
			if (myAnimationFire.IsFinished() &&
				myAnimationFire.GetLoop() == false &&
				myAnimationFire.GetOffset() == 8 &&
				myNbrOfBombs == 0)
			{
				myAnimationFire.Setup(3, 4, 6);
			}

			if (myAnimationFire.IsFinished() &&
				myAnimationFire.GetLoop() == false &&
				myAnimationFire.GetFrames() == 6 &&
				myNbrOfBombs == 0)
			{
				myAnimationFire.SetLoop(true);
				myAnimationFire.Setup(3, 4, 4, 2);
			}

			if (myNbrOfBombs < 10 &&
				myAnimationFire.GetLoop() == true)
			{
				ExplosionAttack(aPlayer);
				myExplosionCooldown = 0.8f;
				++myNbrOfBombs;
			}
			else
			{
				if (myAnimationFire.GetLoop() == true)
				{
					myAnimationFire.SetLoop(false);
				}
				else if (myAnimationFire.IsFinished() &&
						 myAnimationFire.GetFrames() == 4 &&
						 myAnimationFire.GetOffset() == 2)
				{
					myAnimationFire.Setup(3, 4, 6, 2);
				}
				else if (myAnimationFire.IsFinished())
				{
					if (myAnimationFire.GetFrames() == 6 &&
						myAnimationFire.GetOffset() == 2)
					{
						myAnimationFire.Setup(3, 4, 4, 8);
					}


					myChangeStance = true;

					if (myTintAmount > 8.0f)
					{
						if (myAnimationFire.IsFinished())
						{
							myAnimationFire.Setup(3, 4, 4, 8);
							myCurrentAnimation = &myAnimationBeam;
							myMoveSet = eBoneDragonMoveSet::attackBeam;
							myExplosionCooldown = myExplosionFireRate;
							myNbrOfBombs = 0;
						}
					}
				}
			}
		}
	}

	if (myMoveSet == eBoneDragonMoveSet::attackBeam)
	{
		myIgnoreElement = true;

		myBeamCooldown -= aDeltaTime;

		if (myBeamCooldown < 0 && !myBeam.IsActive() && !myBeamDone)
		{
			if (myPosition.x > myLeftBoundary && myBeamRight)
			{
				myPosition.x -= myMovementSpeed * aDeltaTime;
			}
			else if (myPosition.x < myRightBoundary && !myBeamRight)
			{
				myPosition.x += myMovementSpeed * aDeltaTime;
			}
			else
			{

				if (myAnimationBeam.GetLoop() == true)
				{
					myAnimationBeam.SetLoop(false);
				}

				if (myAnimationBeam.IsFinished() &&
					myAnimationBeam.GetLoop() == false &&
					myAnimationBeam.GetFrames() == 4)
				{
					myAnimationBeam.SetLoop(false);
					myAnimationBeam.Setup(3, 4, 2);									
				}

				if (myAnimationBeam.IsFinished() &&
					myAnimationBeam.GetFrames() == 2)
				{
					myBeamStartPos = { myPosition.x, myPosition.y - 0.3f, myPosition.z + 0.0001f };
					myBeamEndPos = { myPosition.x, myBottomBoundary - 1.0f, myPosition.z + 0.0001f };
					myBeam.SetStartPosition(myBeamStartPos);
					myBeam.SetEndPosition(myBeamEndPos);

					myBeam.Activate();
					myAnimationBeam.SetLoop(true);
					myAnimationBeam.Setup(3, 4, 4, 2);
				}
			}
		}
		else if (myBeam.IsActive() && !myBeamDone)
		{		

		if (myAnimationBeam.GetLoop() == true)
		{
			BeamAttack(aDeltaTime);
		}

		}
		else if (myBeamDone)
		{
			if (myPosition.x > 0.0f && myBeamRight)
			{
				myPosition.x -= myMovementSpeed * aDeltaTime;
			}
			else if (myPosition.x < 0.0f && !myBeamRight)
			{
				myPosition.x += myMovementSpeed * aDeltaTime;
			}
			else
			{
				myChangeStance = true;

				if (myTintAmount > 8.0f)
				{
					if (myCurrentAnimation->IsFinished())
					{
						myAnimationBeam.Setup(3, 4, 4, 8);
						myCurrentAnimation = &myAnimationLightning;
						myBeamCooldown = myBeamFireRate;
						if (myBeamRight)
						{
							myBeamRight = false;
						}
						else if (!myBeamRight)
						{
							myBeamRight = true;
						}

						myBeamDone = false;
						myMoveSet = eBoneDragonMoveSet::attackPrimary;
					}
				}
			}
		}

		myBeam.Update(aDeltaTime, false);
	}

	myCurrentAnimation->Update(aDeltaTime);
	myCurrentAnimation->SetPosition({ myPosition.x, myPosition.y - 0.3f, myPosition.z + 0.001f });

	myAnimationWingClouds.Update(aDeltaTime);
	myAnimationWingClouds.SetPosition({ myPosition.x, myPosition.y - 0.3f, myPosition.z + 0.0001f });

	if (myChangeStance)
	{
		ChangeStance(aDeltaTime);
	}
	else if (myTintAmount == 1.0f)
	{
		DamagedFlash(*myCurrentAnimation);
	}
	
	if (myFlashTimer > 0.0f)
	{
		myFlashTimer -= aDeltaTime;
	}

	myHealthBar.Update(aDeltaTime, myHealth, { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f });
	myHealthBar.Show();
	myCountDown.Update(aDeltaTime);
}

void BossBoneDragon::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	if ((myMoveSet == eBoneDragonMoveSet::attackWings || myHealth < myEnemyClass.GetHP() / 3.0f) && myHealth > 1.0f)
	{
		if (!myAnimationWingClouds.IsFinished())
		{
			aRenderBuffer.Add(&myAnimationWingClouds);
		}
	}

	aRenderBuffer.Add(myCurrentAnimation);
	aRenderBuffer.Add(&myShine);

	myBeam.FillRenderBuffer(aRenderBuffer);
	myHealthBar.FillRenderBuffer(aRenderBuffer);
}

void BossBoneDragon::TakeDamage(const float aDamage, const float aDeltaTime)
{
	if (!myPlayBossDeathAnimation)
	{
		float damageFactor = 1.f;
		
		if (!myIgnoreElement)
		{
			if (myLastHitElement == myElement)
			{
				damageFactor = 1.f;
			}
			else
			{
				damageFactor = 0.25f;
			}
		}

		myHealth -= aDamage * aDeltaTime * damageFactor;

		if (myBleed)
		{
			SpawnBlood(3, 1.5f);

			if (myCanPlayTakeDamageSound)
			{
				myCanPlayTakeDamageSound = false;
				SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/BossHurt.wav", 0.5f);
				myCountDown.Set(std::bind(&BossBoneDragon::CanPlayTakeDamageSound, this), 0.5f);
			}
		}

		if (myFlashTimer < 0.8f)
		{
			myFlashTimer = 1.0f;
		}

		if (myHealth < 1.0f)
		{
			myHealth = 1.0f;
			myFlashTimer = 0.0f;
			DamagedFlash(*myCurrentAnimation);
			myBleed = true;
			myCountDown.Set(std::bind(&BossBoneDragon::FinalBossExplosion, this), 3.0f);
			myCountDown.Set(std::bind([=] { Blackboard::GetInstance()->SetNote("deathslowmotion", "true"); }), 2.0f);
			myPlayBossDeathAnimation = true;
		}
	}

	if (myCanPlayTakeDamageSound)
	{
		myCanPlayTakeDamageSound = false;
		SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/BossHurt.wav", 0.5f);
		myCountDown.Set(std::bind(&BossBoneDragon::CanPlayTakeDamageSound, this), 0.5f);
	}
}

bool BossBoneDragon::IsInside(const CU::Vector2f & aPoint)
{
	aPoint;
	return myAnimationIdle.IsInside(aPoint);
}

void BossBoneDragon::ShootAttack(float aAngel)
{
	if (aAngel < Tga2D::Pif / 2)
	{
		for (float a = -aAngel; a >= -Tga2D::Pif + aAngel; a += (2 * aAngel - Tga2D::Pif))
		{
			ProjectileLightningNova* projectile = ProjectileFactory::CreateLightningNova({ myPosition.x, myPosition.y - 0.5f, myPosition.z },
				myPrimaryAttackProjectileSpeed,
				CU::Vector3f(std::cos(a), std::sin(a), 0.f));
			projectile->SetDamage(myEnemyClass.GetDMG());
			ProjectileManager::GetInstance()->EnemyShoot(projectile);
		}
	}
}

void BossBoneDragon::WingAttack()
{
	for (float pos = -0.5f; pos <= 0.5f; pos += 1.0f)
	{
		ProjectileEnergyWave* projectile = ProjectileFactory::CreateEnergyWave(CU::Vector3f(myPosition.x + pos, myPosition.y - 0.3f, myPosition.z),
			myEnergyWaveProjectileSpeed,
			CU::Vector3f(0.0f, -1.0f, 0.0f));
		projectile->SetDamage(myEnemyClass.GetDMG() * 3.f);
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
	}
}

void BossBoneDragon::GranadeAttack()
{
		if (myRightGranadeThrow)
		{
			ProjectileGranade* projectile = ProjectileFactory::CreateGranade(CU::Vector3f(myPosition.x, myPosition.y - 0.1f, myPosition.z),
				myGranadeProjectileSpeed,
				CU::Vector3f(0.7f, -0.5f, 0.0f));
			projectile->SetDamage(myEnemyClass.GetDMG());
			ProjectileManager::GetInstance()->EnemyShoot(projectile);
			myRightGranadeThrow = false;
		}
		else if (!myRightGranadeThrow)
		{
			ProjectileGranade* projectile = ProjectileFactory::CreateGranade(CU::Vector3f(myPosition.x, myPosition.y - 0.1f, myPosition.z),
				myGranadeProjectileSpeed,
				CU::Vector3f(-0.7f, -0.5f, 0.0f));
			projectile->SetDamage(myEnemyClass.GetDMG());
			ProjectileManager::GetInstance()->EnemyShoot(projectile);
			myRightGranadeThrow = true;
		}	
}

void BossBoneDragon::BeamAttack(float aDeltaTime)
{
	myBeam.SetStartPosition(myBeamStartPos);
	myBeam.SetEndPosition(myBeamEndPos);

	if (myPosition.x < 0.5f && myBeam.IsActive() && myBeamRight)
	{
		myPosition.x += myMovementSpeed * aDeltaTime * 1.5f;
		myBeamStartPos.x += myMovementSpeed * aDeltaTime * 1.5f;
		myBeamEndPos.x += myMovementSpeed * aDeltaTime * 1.5f;
	}
	else if (myPosition.x > -0.5f && myBeam.IsActive() && !myBeamRight)
	{
		myPosition.x -= myMovementSpeed * aDeltaTime * 1.5f;
		myBeamStartPos.x -= myMovementSpeed * aDeltaTime * 1.5f;
		myBeamEndPos.x -= myMovementSpeed * aDeltaTime * 1.5f;
	}
	else
	{
		if (myAnimationBeam.IsFinished() &&
			myAnimationBeam.GetFrames() == 4)
		{
			myAnimationBeam.Setup(3, 4, 6, 2);
		}
		else if (myAnimationBeam.IsFinished())
		{
			myBeam.Dectivate();
			myBeamDone = true;

			myAnimationBeam.Setup(3, 4, 4, 8);
		}
	}
}

void BossBoneDragon::ExplosionAttack(Player& aPlayer)
{
	float xPos = aPlayer.GetPosition().x;
	float yPos = aPlayer.GetPosition().y;

	if (!((xPos < (myPosition.x + 0.4f)) &&
		(xPos > (myPosition.x - 0.4f)) &&
		(yPos > (myPosition.y - 0.4f))))
	{
		ProjectileDelayedBomb* projectileOne = ProjectileFactory::CreateDelayedBomb(CU::Vector3f(xPos, yPos, aPlayer.GetPosition().z - 0.001f),
			1.0f,
			CU::Vector3f(0.0f, -1.0f, 0.0f),
			0.5f);
		projectileOne->SetDamage(myEnemyClass.GetDMG() * 5);
		ProjectileManager::GetInstance()->EnemyShoot(projectileOne);
	}

	for (float index = 0.0f; index < Tga2D::Pif * 2.0f; index += Tga2D::Pif / 3.0f)
	{
		if (!(((xPos + std::cos(index) / 7.0f) < (myPosition.x + 0.4f)) &&
			((xPos + std::cos(index) / 7.0f) > (myPosition.x - 0.4f)) &&
			((yPos + std::sin(index) / 7.0f) >(myPosition.y - 0.4f))))
		{
			ProjectileDelayedBomb* projectileTwo = ProjectileFactory::CreateDelayedBomb(CU::Vector3f(xPos + std::cos(index) / 7.0f, yPos + std::sin(index) / 7.0f, aPlayer.GetPosition().z - 0.001f),
				0.0f,
				CU::Vector3f(0.0f, 0.0f, 0.0f),
				0.5f);
			projectileTwo->SetDamage(myEnemyClass.GetDMG());
			ProjectileManager::GetInstance()->EnemyShoot(projectileTwo);
		}
	}
}

void BossBoneDragon::ChangeStance(float aDeltaTime)
{
	if (myTintAmount < 10.0f && myIncreaseTint)
	{
		myTintAmount += aDeltaTime * 10.0f;
	}
	else
	{
		myIncreaseTint = false;
	}

	if (myIncreaseTint && myShineAlfa < 1.0f)
	{
		myShineAlfa += aDeltaTime * 10.0f;
	}
	else if (!myIncreaseTint)
	{
		myTintAmount -= aDeltaTime * 10.0f;
		myShineAlfa -= aDeltaTime * 10.0f;

		if (myTintAmount <= 1.0f)
		{
			myChangeStance = false;
			myIncreaseTint = true;
			myShineAlfa = 0.0f;
			myTintAmount = 1.0f;
		}
	}

	myAnimationIdle.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f });
	myAnimationLightning.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f });
	myAnimationIce.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f });
	myAnimationFire.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f });
	myAnimationBeam.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f });
	myShine.SetTint({ 1.0f, 1.0f, 1.0f, myShineAlfa });
	myShine.SetPosition({ myPosition.x, myPosition.y, myPosition.z + 0.001f });
}

void BossBoneDragon::RenderDebug(const CommonUtilities::Camera & aCamera)
{
	Enemy::RenderDebug(aCamera);
	if (myBeam.IsActive())myBeam.RenderDebug(aCamera);
}

inline bool BossBoneDragon::IsTouching(const CircleCollider * aCircleCollider)
{
	if (myBeam.IsTouching(aCircleCollider))
	{
		return true;
	}

	if (myHealth > 1)
	{
		return myCollider.IsTouching(aCircleCollider);
	}

	return false;
}

inline bool BossBoneDragon::BeamCollision(const CircleCollider * aCircleCollider)
{
	if (myBeam.IsTouching(aCircleCollider))
	{
		return true;
	}

	return false;
}

void BossBoneDragon::DeathAnimation()
{
	if (mySpawnExplosion)
	{
		SpawnExplosion(0.15f);
	}

	if (myBleed)
	{
		SpawnBlood(12, 0.2f);
	}

	if (myHasPlayedFinalBossExplosion)
	{
		myHealth = 0.0f;
	}
}
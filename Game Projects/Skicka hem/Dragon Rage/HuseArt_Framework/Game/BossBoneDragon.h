#pragma once
#include "Enemy.h"
#include "Beam.h"

#include <Animation.h>

class Player;

enum class eBoneDragonMoveSet
{
	attackPrimary,
	attackWings,
	attackGranades,
	attackBombs,
	attackBeam
};

class BossBoneDragon : public Enemy
{
public:
	BossBoneDragon();
	~BossBoneDragon();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float aDeltaTime, Player& aPlayer) override;
	void UpdateCollider() override { myCollider.SetPosition(myPosition - CommonUtilities::Vector3f(0.f, 0.1f, 0.f)); }
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsBoss() override { return true; }

	inline bool IsTouching(const CircleCollider* aCircleCollider) override;
	inline bool BeamCollision(const CircleCollider* aCircleCollider) override;
	inline bool IsTouching(Projectile* aProjectile) override { return aProjectile->IsTouching(GetCollider()); }
	void DeathAnimation() override;

	void ShootAttack(float aAngel);
	void WingAttack();
	void GranadeAttack();
	void BeamAttack(float aDeltaTime);
	void ExplosionAttack(Player& aPlayer);
	void ChangeStance(float aDeltaTime);

	void RenderDebug(const CommonUtilities::Camera& aCamera) override;

private:
	bool myIgnoreElement;

	bool myIsSlowed;
	float mySlowTimer;

	Animation* myCurrentAnimation;
	Animation myAnimationIdle;
	Animation myAnimationLightning;
	Animation myAnimationIce;
	Animation myAnimationFire;
	Animation myAnimationBeam;

	Animation myAnimationWingClouds;
	bool myPlayWingAnimation;

	bool myIncreaseTint;
	float myCurrentTint;
	float myDeathTint;

	Beam myBeam;
	float myRotation;
	float myMovementSpeed;
	bool myBeamDone;

	eBoneDragonMoveSet myMoveSet;

	float myPrimaryAttackProjectileSpeed;
	float myEnergyWaveProjectileSpeed;
	float myGranadeProjectileSpeed;

	float myPrimaryAttackFireRate;
	float myEnergyWaveFireRate;
	float myGranadeFireRate;
	float myBeamFireRate;
	float myExplosionFireRate;

	float myPrimaryAttackCooldown;
	float myEnegryWaveCooldown;
	float myGranadeCooldown;
	float myBeamCooldown;
	float myExplosionCooldown;

	float myPrimaryAttackAngel;
	int myNbrOfNormalAttacks;
	int myNbrOfGranadeAttacks;
	CommonUtilities::Vector3f myBeamStartPos;
	CommonUtilities::Vector3f myBeamEndPos;

	int myNbrOfBombs;
	bool myRightGranadeThrow;
	bool myBeamRight;

	float myLeftBoundary;
	float myRightBoundary;
	float myTopBoundary;
	float myBottomBoundary;
	float myCenterY;

	CommonUtilities::Vector3f myShootDirection;

	//Change Stance
	Animation myShine;
	float myTintAmount;
	float myShineAlfa;
	bool myChangeStance;

	//Bopping Movement
	float myHoverDistance;
	float myBoppingSpeed;
	float myHoverTime;
};



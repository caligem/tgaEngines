#include "stdafx.h"
#include "BossCastle.h"

#include "ProjectileManager.h"
#include "ExplosionManager.h"
#include "Player.h"

#include "SFXManager.h"

#include "ProjectileFactory.h"
#include "EnemyFactory.h"
#include "EnemyClassCache.h"
#include "Blackboard.h"

#include "EnemyGiantOwl.h"

#include <Random.h>

namespace CU = CommonUtilities;


BossCastle::BossCastle(EnemyClassCache & aEnemyClassCache)
	: myRotation(0)
	, myCatapultProjectileSpeed(0.5f)
	, myIcicleProjectileSpeed(1.5f)
	, myArrowProjectileSpeed(1.0f)
	, myCatapultAttackFireRate(3.0f)
	, myWizardFireRate(4.0f)
	, myCastleTowerFireRate(1.0f)
	, myBombFireRate(1.0f)
	, myArrowFireRate(0.8f)
	, myBeamLockOn(1.0f)
	, myBeamDuration(2.0f)
	, myRightWizardShootDir(0.0f, 0.0f, 0.0f)
	, myLeftWizardShootDir(0.0f, 0.0f, 0.0f)
	, myIncreaseTint(true)
	, myDeathTint(0.0f)
	, myCurrentTint(1.0f)
	, myLeftBoundary(-1.4f)
	, myRightBoundary(1.4f)
	, myTopBoundary(0.0f)
	, myBottomBoundary(-1.8f)
	, myCenterY(0)
	, myTrackPlayerLocation(0.0f, 0.0f, 0.0f)
	, myEnemyClassCache(aEnemyClassCache)
	, myAllTowersDead(false)
	, myTowerHit(nullptr)
	, myIsSlowed(false)
	, myBombCircleFilled(false)
	, myNbrOfWizardAttacks(3)
	, myRightCatapultFlashTimer(0.0f)
	, myLeftCatapultFlashTimer(0.0f)
	, myRightWizardFlashTimer(0.0f)
	, myLeftWizardFlashTimer(0.0f)
	, myLockLaserSight(true)
	, myAnimationFinished(true)
{
}

BossCastle::~BossCastle()
{
}

void BossCastle::Init(const EnemyClass& aEnemyClass, const CU::Vector3f aPosition)
{
	myPosition = aPosition;

	myRightCatapultTowerPos = { myPosition.x + 0.8f, myPosition.y - 0.4f, myPosition.z + 0.002f };
	myLeftCatapultTowerPos = { myPosition.x - 0.8f, myPosition.y - 0.4f, myPosition.z + 0.002f };
	myRightWizardTowerPos = { myPosition.x + 1.0f, myPosition.y - 0.3f, myPosition.z + 0.002f };
	myLeftWizardTowerPos = { myPosition.x - 1.0f, myPosition.y - 0.3f, myPosition.z + 0.002f };

	myAnimationIdle.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle.dds", { myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.002f });
	myAnimationCastleAim.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_aim.dds", { myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.002f });
	myAnimationCastleAim.Setup(2, 2, 4);
	myAnimationCastleAim.SetDuration(0.5f);
	myAnimationCastleAim.SetLoop(false);
	myAnimationCastleWizards.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_wizardShot.dds", { myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.002f });
	myAnimationCastleWizards.Setup(2, 2, 4);
	myAnimationCastleWizards.SetDuration(0.5f);
	myAnimationCastleWizards.SetLoop(false);
	myCurrentCastleAnimation = &myAnimationIdle;

	myAnimationCastleShield.Init("Assets/Images/Projectiles/projectile_castleShield.dds", { myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.001f });

	myAnimationRightCataputTowerCrystal.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_crystals.dds", { myRightCatapultTowerPos.x, myRightCatapultTowerPos.y - 0.12f, myRightCatapultTowerPos.z - 0.001f });
	myAnimationRightCataputTowerCrystal.Setup(2, 2, 4);
	myAnimationLeftCatapultTowerCrystal.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_crystals.dds", { myLeftCatapultTowerPos.x, myLeftCatapultTowerPos.y - 0.12f, myLeftCatapultTowerPos.z - 0.001f });
	myAnimationLeftCatapultTowerCrystal.Setup(2, 2, 4);
	myAnimationRightWizardTowerCrystal.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_crystals.dds", { myRightWizardTowerPos.x - 0.02f, myRightWizardTowerPos.y - 0.05f, myRightWizardTowerPos.z - 0.001f });
	myAnimationRightWizardTowerCrystal.Setup(2, 2, 4);
	myAnimationLeftWizardTowerCrystal.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_crystals.dds", { myLeftWizardTowerPos.x - 0.02f, myLeftWizardTowerPos.y - 0.05f, myLeftWizardTowerPos.z - 0.001f });
	myAnimationLeftWizardTowerCrystal.Setup(2, 2, 4);

	myAnimationRightCatapultIdle.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_tower_catapult.dds", myRightCatapultTowerPos);
	myAnimationRightCatapultAttack.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_tower_catapult_throw.dds", myRightCatapultTowerPos);
	myAnimationRightCatapultAttack.Setup(2, 3, 3);
	myAnimationRightCatapultAttack.SetDuration(0.5f);
	myAnimationRightCatapultAttack.SetLoop(false);
	myCurrentRightCatapultAnimation = &myAnimationRightCatapultIdle;

	myAnimationLeftCatapultIdle.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_tower_catapult.dds", myLeftCatapultTowerPos);
	myAnimationLeftCatapultAttack.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_tower_catapult_throw.dds", myRightCatapultTowerPos);
	myAnimationLeftCatapultAttack.Setup(2, 3, 3);
	myAnimationLeftCatapultAttack.SetDuration(0.5f);
	myAnimationLeftCatapultAttack.SetLoop(false);
	myCurrentLeftCatapultAnimation = &myAnimationLeftCatapultIdle;

	myAnimationRightWizardIdle.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_tower_wizard.dds", myRightWizardTowerPos);
	myAnimationRightWizardAim.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_tower_wizard_aim.dds", myRightWizardTowerPos);
	myAnimationRightWizardAim.Setup(1, 4, 4);
	myAnimationRightWizardAim.SetDuration(0.5f);
	myCurrentRightWizardAnimation = &myAnimationRightWizardIdle;

	myAnimationLeftWizardIdle.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_tower_wizard.dds", myLeftWizardTowerPos);
	myAnimationLeftWizardAim.Init("Assets/Images/Enemies/Bosses/enemy_boss_castle_tower_wizard_aim.dds", myLeftWizardTowerPos);
	myAnimationLeftWizardAim.Setup(1, 4, 4);
	myAnimationLeftWizardAim.SetDuration(0.5f);
	myCurrentLeftWizardAnimation = &myAnimationLeftWizardIdle;

	myEnemyClass = aEnemyClass;
	myHealth = myEnemyClass.GetHP();
	myRightCatapultTowerHealth = myEnemyClass.GetHP() / 4.0f;
	myLeftCatapultTowerHealth = myEnemyClass.GetHP() / 4.0f;
	myRightWizardTowerHealth = myEnemyClass.GetHP() / 4.0f;
	myLeftWizardTowerHealth = myEnemyClass.GetHP() / 4.0f;

	myCatapultAttackCooldown = myCatapultAttackFireRate;
	myWizardCooldown = myWizardFireRate;
	myCastleTowerCooldown = myCastleTowerFireRate;
	myBombCooldown = myBombFireRate;
	myArrowCooldown = myArrowFireRate;
	myTopBoundary += aPosition.y;
	myBottomBoundary += aPosition.y;
	myCenterY = (myTopBoundary + myBottomBoundary) / 2.0f;

	myCollider.SetRadius(0.5f);
	myColliderRightCatapultTower.SetRadius(0.10f);
	myColliderLeftCatapultTower.SetRadius(0.10f);
	myColliderRightWizardTower.SetRadius(0.10f);
	myColliderLeftWizardTower.SetRadius(0.10f);

	myColliderRightCatapultTower.SetPosition(myRightCatapultTowerPos);
	myColliderLeftCatapultTower.SetPosition(myLeftCatapultTowerPos);
	myColliderRightWizardTower.SetPosition(myRightWizardTowerPos);
	myColliderLeftWizardTower.SetPosition(myLeftWizardTowerPos);

	myHealthBar.Init(myEnemyClass.GetHP(), { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f }, FLT_MAX, { 8.0f, 1.0f });
	myRightCatapultTowerHealthBar.Init(myRightCatapultTowerHealth, { myRightCatapultTowerPos.x, myRightCatapultTowerPos.y + 0.1f, myRightCatapultTowerPos.z - 0.01f });
	myLeftCatapultTowerHealthBar.Init(myLeftCatapultTowerHealth, { myLeftCatapultTowerPos.x, myLeftCatapultTowerPos.y + 0.1f, myLeftCatapultTowerPos.z - 0.01f });
	myRightWizardTowerHealthBar.Init(myRightWizardTowerHealth, { myRightWizardTowerPos.x, myRightWizardTowerPos.y + 0.1f, myRightWizardTowerPos.z - 0.01f });
	myLeftWizardTowerHealthBar.Init(myLeftWizardTowerHealth, { myLeftWizardTowerPos.x, myLeftWizardTowerPos.y + 0.1f, myLeftWizardTowerPos.z - 0.01f });
	
	myLeftBeam.Init(eBeamType::Lightning);
	myLeftBeam.SetGlow(true, { 0.6f, 0.6f, 1.0f, 0.4f });

	myRightBeam.Init(eBeamType::Lightning);
	myRightBeam.SetGlow(true, { 0.6f, 0.6f, 1.0f, 0.4f });

	myRightLaserSight.Init(eBeamType::LaserSight);
	myLeftLaserSight.Init(eBeamType::LaserSight);

	myMoveSet = eCastleMoveSet::attackWizards;

	mySpawnExplosion = true;
	myPlayBossDeathAnimation = false;
	myHasPlayedFinalBossExplosion = false;

	if (myEnemyClass.GetType().find(".hafd") != std::string::npos)
	{
		myDialogue.Init(myEnemyClass.GetType());
	}
}

void BossCastle::Update(float aDeltaTime, Player& aPlayer)
{
	if (myHealth == 1)
	{
		DeathAnimation();
		myDeathTint += aDeltaTime / 100.0f;

		if (myCurrentCastleAnimation->GetTint().x > 3.0f)
		{
			myIncreaseTint = false;
		}
		else if (myCurrentCastleAnimation->GetTint().x < 0.0f)
		{
			myIncreaseTint = true;
		}

		if (myIncreaseTint)
		{
			myCurrentTint += myDeathTint;
			myCurrentCastleAnimation->SetTint({ myCurrentTint, myCurrentTint, myCurrentTint, 1.0f });
		}
		else if (!myIncreaseTint)
		{
			myCurrentTint -= myDeathTint;
			myCurrentCastleAnimation->SetTint({ myCurrentTint, myCurrentTint, myCurrentTint, 1.0f });
		}

		UpdateHealthBars(aDeltaTime);
		myCountDown.Update(aDeltaTime);

		return;
	}

	CheckTowerStatus();

	myCatapultAttackCooldown -= aDeltaTime;

	if (myCatapultAttackCooldown < 0)
	{
		if (myCurrentRightCatapultAnimation != &myAnimationRightCatapultAttack &&
			myCurrentLeftCatapultAnimation != &myAnimationLeftCatapultAttack)
		{
			myCurrentRightCatapultAnimation = &myAnimationRightCatapultAttack;
			myCurrentLeftCatapultAnimation = &myAnimationLeftCatapultAttack;

		}
		else if (myAnimationRightCatapultAttack.IsFinished() &&
				 myAnimationLeftCatapultAttack.IsFinished() &&
				 myAnimationRightCatapultAttack.GetOffset() == 0 &&
				 myAnimationLeftCatapultAttack.GetOffset() == 0)
		{
			if (myRightCatapultTowerHealth > 0)
			{
				RightCatapultAttack();
			}
			if (myLeftCatapultTowerHealth > 0)
			{
				LeftCatapultAttack();
			}

			myAnimationRightCatapultAttack.Setup(2, 3, 3, 3);
			myAnimationLeftCatapultAttack.Setup(2, 3, 3, 3);

			
		}
		else if (myAnimationRightCatapultAttack.IsFinished() &&
				 myAnimationLeftCatapultAttack.IsFinished() &&
				 myAnimationRightCatapultAttack.GetOffset() == 3 &&
				 myAnimationLeftCatapultAttack.GetOffset() == 3)
		{
			myAnimationRightCatapultAttack.Setup(2, 3, 3);
			myAnimationLeftCatapultAttack.Setup(2, 3, 3);

			myCurrentRightCatapultAnimation = &myAnimationRightCatapultIdle;
			myCurrentLeftCatapultAnimation = &myAnimationLeftCatapultIdle;

			myCatapultAttackCooldown = myCatapultAttackFireRate;
		}
	}

	myWizardCooldown -= aDeltaTime;

	if (myWizardCooldown < 0)
	{
		myLockLaserSight = true;

		if (myCurrentRightWizardAnimation != &myAnimationRightWizardAim &&
			myCurrentLeftWizardAnimation != &myAnimationLeftWizardAim)
		{
			myCurrentRightWizardAnimation = &myAnimationRightWizardAim;
			myCurrentLeftWizardAnimation = &myAnimationLeftWizardAim;

			myAnimationRightWizardAim.SetLoop(false);
			myAnimationLeftWizardAim.SetLoop(false);

			myAnimationRightWizardAim.Setup(1, 4, 3);
			myAnimationLeftWizardAim.Setup(1, 4, 3);			
		}
		else if (myAnimationRightWizardAim.IsFinished() &&
				 myAnimationLeftWizardAim.IsFinished() &&
				 myAnimationRightWizardAim.GetLoop() == false &&
				 myAnimationLeftWizardAim.GetLoop() == false &&
				 myBeamLockOn > 0.0f)
		{
			myAnimationRightWizardAim.SetLoop(true);
			myAnimationLeftWizardAim.SetLoop(true);

			myAnimationRightWizardAim.Setup(1, 4, 1, 2);
			myAnimationLeftWizardAim.Setup(1, 4, 1, 2);
		}		

		if (myAnimationRightWizardAim.GetLoop() == true &&
			myAnimationLeftWizardAim.GetLoop() == true)
		{
			myBeamLockOn -= aDeltaTime;
		}


		if (myBeamLockOn < 0.0f)
		{
			if (myBeamDuration >= 0.0f)
			{
				myAnimationRightWizardAim.Setup(1, 4, 1, 3);
				myAnimationLeftWizardAim.Setup(1, 4, 1, 3);
			}

			myBeamDuration -= aDeltaTime;

			if (myBeamDuration < 0.0f)
			{
				myLeftBeam.Dectivate();
				myRightBeam.Dectivate();
				if (myAnimationRightWizardAim.IsReversed() == false &&
					myAnimationLeftWizardAim.IsReversed() == false)
				{
					myAnimationRightWizardAim.Setup(1, 4, 3);
					myAnimationLeftWizardAim.Setup(1, 4, 3);

					myAnimationRightWizardAim.SetReversed(true);
					myAnimationLeftWizardAim.SetReversed(true);

					myAnimationRightWizardAim.SetLoop(false);
					myAnimationLeftWizardAim.SetLoop(false);
				}

				if (myAnimationRightWizardAim.IsFinished() &&
					myAnimationLeftWizardAim.IsFinished() &&
					myAnimationRightWizardAim.IsReversed() == true &&
					myAnimationLeftWizardAim.IsReversed() == true)
				{
					myAnimationRightWizardAim.SetReversed(false);
					myAnimationLeftWizardAim.SetReversed(false);
					myAnimationRightWizardAim.Setup(1, 4, 3);
					myAnimationLeftWizardAim.Setup(1, 4, 3);
					myCurrentRightWizardAnimation = &myAnimationRightWizardIdle;
					myCurrentLeftWizardAnimation = &myAnimationLeftWizardIdle;
					myAnimationFinished = true;
					myWizardCooldown = myWizardFireRate;
					myBeamDuration = 2.0f;
					myBeamLockOn = 1.0f;
				}
				else
				{
					myAnimationFinished = false;
				}
			}
			else
			{
				if (myLeftWizardTowerHealth > 0)
				{
					myLeftBeam.Activate();
				}
				else
				{
					myLeftBeam.Dectivate();
				}

				if (myRightWizardTowerHealth > 0)
				{
					myRightBeam.Activate();
				}
				else
				{
					myRightBeam.Dectivate();
				}
			}
		}
	}
	else
	{
		myLeftWizardShootDir = aPlayer.GetPosition() - CU::Vector3f({ myLeftWizardTowerPos.x, myLeftWizardTowerPos.y + 0.2f, myLeftWizardTowerPos.z });
		myRightWizardShootDir = aPlayer.GetPosition() - CU::Vector3f({ myRightWizardTowerPos.x, myRightWizardTowerPos.y + 0.2f, myRightWizardTowerPos.z });
		myLeftWizardShootDir.z = 0.f;
		myRightWizardShootDir.z = 0.f;

		myLeftBeam.SetStartPosition({ myLeftWizardTowerPos.x, myLeftWizardTowerPos.y + 0.2f, myLeftWizardTowerPos.z - 0.002f });
		myLeftBeam.SetEndPosition(myLeftWizardTowerPos + myLeftWizardShootDir.GetNormalized() * 5.f);

		myRightBeam.SetStartPosition({ myRightWizardTowerPos.x, myRightWizardTowerPos.y + 0.2f, myRightWizardTowerPos.z - 0.002f });
		myRightBeam.SetEndPosition(myRightWizardTowerPos + myRightWizardShootDir.GetNormalized() * 5.f);
		
		myLockLaserSight = false;
	}	

	if (!myLockLaserSight)
	{
		CommonUtilities::Vector3f offset(0.f, 0.2f, -0.002f);
		myLeftLaserSight.SetStartPosition({ myLeftWizardTowerPos.x, myLeftWizardTowerPos.y + 0.2f, myLeftWizardTowerPos.z - 0.002f });
		myLeftLaserSight.SetEndPosition(myLeftWizardTowerPos + offset + myLeftWizardShootDir.GetNormalized() * 10.5f);

		myRightLaserSight.SetStartPosition({ myRightWizardTowerPos.x, myRightWizardTowerPos.y + 0.2f, myRightWizardTowerPos.z - 0.002f });
		myRightLaserSight.SetEndPosition(myRightWizardTowerPos + offset + myRightWizardShootDir.GetNormalized() * 10.5f);
	}


	if ((!myRightBeam.IsActive() || !myLeftBeam.IsActive()) && myAnimationFinished)
	{
		myLeftLaserSight.Activate();
		myRightLaserSight.Activate();
	}
	else
	{
		myLeftLaserSight.Dectivate();
		myRightLaserSight.Dectivate();
	}

	if (myAllTowersDead)
	{
		/*myEnemiesSpawnCooldown -= aDeltaTime;

		if (myEnemiesSpawnCooldown < 0)
		{
			SpawnEnemies(aPlayer);
			myEnemiesSpawnCooldown = myEnemiesSpawnRate;
		}*/

		myArrowCooldown -= aDeltaTime;

		if (myArrowCooldown < 0)
		{
			ArrowAttack();
			myArrowCooldown = myArrowFireRate;
		}


		if (myMoveSet == eCastleMoveSet::attackWizards)
		{
			myCastleTowerCooldown -= aDeltaTime;

			if (myCastleTowerCooldown < 0)
			{
				myCurrentCastleAnimation = &myAnimationCastleWizards;

				if (myAnimationCastleWizards.IsFinished() &&
					myAnimationCastleWizards.IsReversed() == false)
				{
					myAnimationCastleWizards.SetReversed(true);
					myAnimationCastleWizards.Setup(2, 2, 4);

					CastleTowerAttack();
					
				}
				else if (myAnimationCastleWizards.IsFinished() &&
						 myAnimationCastleWizards.IsReversed() == true)
				{
					myAnimationCastleWizards.SetReversed(false);
					myAnimationCastleWizards.Setup(2, 2, 4);
					myCurrentCastleAnimation = &myAnimationIdle;

					myCastleTowerCooldown = myCastleTowerFireRate;
					--myNbrOfWizardAttacks;

					if (myNbrOfWizardAttacks == 0)
					{
						myNbrOfWizardAttacks = 3;
						myMoveSet = eCastleMoveSet::attackBomb;
					}
				}
			}			
		}

		if (myMoveSet == eCastleMoveSet::attackBomb)
		{
			myBombCooldown -= aDeltaTime;

			if (myBombCooldown < 0)
			{
				myCurrentCastleAnimation = &myAnimationCastleAim;

				if (myAnimationCastleAim.IsFinished() &&
					myAnimationCastleAim.GetLoop() == false &&
					myAnimationCastleAim.IsReversed() == false)
				{
					myAnimationCastleAim.SetLoop(true);
					myAnimationCastleAim.Setup(2, 2, 1, 4);

					BombAttack();

				}
				else if (myBombCooldown < -2.5f &&
						 myAnimationCastleAim.GetLoop() == true && 
						 myAnimationCastleAim.IsReversed() == false)
				{
					myAnimationCastleAim.SetLoop(false);
					myAnimationCastleAim.SetReversed(true);
					myAnimationCastleAim.Setup(2, 2, 4);

					
				}
				else if (myAnimationCastleAim.IsFinished() && 
						 myAnimationCastleAim.GetLoop() == false &&
						 myAnimationCastleAim.IsReversed() == true)
				{
					myAnimationCastleAim.SetLoop(false);
					myAnimationCastleAim.SetReversed(false);
					myAnimationCastleAim.Setup(2, 2, 4);
					myCurrentCastleAnimation = &myAnimationIdle;

					myBombCooldown = myBombFireRate;
					myMoveSet = eCastleMoveSet::attackWizards;
				}				
			}
			else
			{
				myTrackPlayerLocation = { aPlayer.GetPosition() };
			}
		}
	}

	myLeftBeam.Update(aDeltaTime, false);
	myRightBeam.Update(aDeltaTime, false);

	myLeftLaserSight.Update(aDeltaTime, false);
	myRightLaserSight.Update(aDeltaTime, false);
	
	myRightWizardShootDir = aPlayer.GetPosition() - myRightWizardTowerPos;
	myLeftWizardShootDir = aPlayer.GetPosition() - myLeftWizardTowerPos;

	myCurrentCastleAnimation->Update(aDeltaTime);
	myCurrentCastleAnimation->SetPosition({ myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.002f });

	myAnimationCastleShield.Update(aDeltaTime);
	myAnimationCastleShield.SetPosition({ myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.001f });

	myCurrentRightCatapultAnimation->Update(aDeltaTime);
	myCurrentRightCatapultAnimation->SetPosition(myRightCatapultTowerPos);

	myCurrentLeftCatapultAnimation->Update(aDeltaTime);
	myCurrentLeftCatapultAnimation->SetPosition(myLeftCatapultTowerPos);

	myCurrentRightWizardAnimation->Update(aDeltaTime);
	myCurrentRightWizardAnimation->SetPosition(myRightWizardTowerPos);

	myCurrentLeftWizardAnimation->Update(aDeltaTime);
	myCurrentLeftWizardAnimation->SetPosition(myLeftWizardTowerPos);

	myAnimationRightCataputTowerCrystal.Update(aDeltaTime);
	myAnimationRightCataputTowerCrystal.SetPosition({ myRightCatapultTowerPos.x, myRightCatapultTowerPos.y - 0.12f, myRightCatapultTowerPos.z - 0.001f });
	myAnimationLeftCatapultTowerCrystal.Update(aDeltaTime);
	myAnimationLeftCatapultTowerCrystal.SetPosition({ myLeftCatapultTowerPos.x, myLeftCatapultTowerPos.y - 0.12f, myLeftCatapultTowerPos.z - 0.001f });
	myAnimationRightWizardTowerCrystal.Update(aDeltaTime);
	myAnimationRightWizardTowerCrystal.SetPosition({ myRightWizardTowerPos.x - 0.02f, myRightWizardTowerPos.y - 0.05f, myRightWizardTowerPos.z - 0.001f });
	myAnimationLeftWizardTowerCrystal.Update(aDeltaTime);
	myAnimationLeftWizardTowerCrystal.SetPosition({ myLeftWizardTowerPos.x - 0.02f, myLeftWizardTowerPos.y - 0.05f, myLeftWizardTowerPos.z - 0.001f });

	DamagedFlash(*myCurrentCastleAnimation, myFlashTimer);
	if (myFlashTimer > 0.0f)
	{
		myFlashTimer -= aDeltaTime;
	}

	DamagedFlash(*myCurrentRightCatapultAnimation, myRightCatapultFlashTimer);
	if (myRightCatapultFlashTimer > 0.0f)
	{
		myRightCatapultFlashTimer -= aDeltaTime;
	}

	DamagedFlash(*myCurrentLeftCatapultAnimation, myLeftCatapultFlashTimer);
	if (myLeftCatapultFlashTimer > 0.0f)
	{
		myLeftCatapultFlashTimer -= aDeltaTime;
	}

	DamagedFlash(*myCurrentRightWizardAnimation, myRightWizardFlashTimer);
	if (myRightWizardFlashTimer > 0.0f)
	{
		myRightWizardFlashTimer -= aDeltaTime;
	}

	DamagedFlash(*myCurrentLeftWizardAnimation, myLeftWizardFlashTimer);
	if (myLeftWizardFlashTimer > 0.0f)
	{
		myLeftWizardFlashTimer -= aDeltaTime;
	}

	UpdateHealthBars(aDeltaTime);
}

void BossCastle::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(myCurrentCastleAnimation);	

	if (!myAllTowersDead)
	{
		aRenderBuffer.Add(&myAnimationCastleShield);
	}
	else if (myHealth > 0)
	{
		myHealthBar.FillRenderBuffer(aRenderBuffer);
	}	


	if (myRightCatapultTowerHealth > 0)
	{
		aRenderBuffer.Add(myCurrentRightCatapultAnimation);
		aRenderBuffer.Add(&myAnimationRightCataputTowerCrystal);
		myRightCatapultTowerHealthBar.FillRenderBuffer(aRenderBuffer);
	}
	if (myLeftCatapultTowerHealth > 0)
	{
		aRenderBuffer.Add(myCurrentLeftCatapultAnimation);
		aRenderBuffer.Add(&myAnimationLeftCatapultTowerCrystal);
		myLeftCatapultTowerHealthBar.FillRenderBuffer(aRenderBuffer);
	}


	if (myRightWizardTowerHealth > 0)
	{
		aRenderBuffer.Add(myCurrentRightWizardAnimation);
		aRenderBuffer.Add(&myAnimationRightWizardTowerCrystal);
		myRightWizardTowerHealthBar.FillRenderBuffer(aRenderBuffer);
		if (!myRightBeam.IsActive())
		{
			myRightLaserSight.FillRenderBuffer(aRenderBuffer);
		}
		
	}
	if (myLeftWizardTowerHealth > 0)
	{
		aRenderBuffer.Add(myCurrentLeftWizardAnimation);
		aRenderBuffer.Add(&myAnimationLeftWizardTowerCrystal);
		myLeftWizardTowerHealthBar.FillRenderBuffer(aRenderBuffer);
		if (!myLeftBeam.IsActive())
		{
			myLeftLaserSight.FillRenderBuffer(aRenderBuffer);
		}
	}
	

	myLeftBeam.FillRenderBuffer(aRenderBuffer);
	myRightBeam.FillRenderBuffer(aRenderBuffer);
}

void BossCastle::TakeDamage(const float aDamage, const float aDeltaTime)
{
	if (myAllTowersDead)
	{
		if (!myPlayBossDeathAnimation)
		{
			myHealth -= aDamage * aDeltaTime;

			if (myFlashTimer < 0.8f)
			{
				myFlashTimer = 1.0f;
			}

			if (myHealth < 1.0f)
			{
				myHealth = 1.0f;
				myFlashTimer = 0.0f;
				DamagedFlash(*myCurrentCastleAnimation, myFlashTimer);
				myCountDown.Set(std::bind(&BossCastle::FinalBossExplosion, this), 3.0f);
				myCountDown.Set(std::bind([=] { Blackboard::GetInstance()->SetNote("deathslowmotion", "true"); }), 2.0f);
				myPlayBossDeathAnimation = true;
			}
		}
	}
	else
	{
		if (myTowerHit == myCurrentRightCatapultAnimation)
		{
			myRightCatapultTowerHealth -= aDamage * aDeltaTime;

			if (myRightCatapultFlashTimer < 0.8f)
			{
				myRightCatapultFlashTimer = 1.0f;
			}
			myTowerHit = nullptr;

			if (myRightCatapultTowerHealth <= 0)
			{
				TowerDeathAnimation(myRightCatapultTowerPos);
			}
		}
		else if (myTowerHit == myCurrentLeftCatapultAnimation)
		{
			myLeftCatapultTowerHealth -= aDamage * aDeltaTime;

			if (myLeftCatapultFlashTimer < 0.8f)
			{
				myLeftCatapultFlashTimer = 1.0f;
			}
			myTowerHit = nullptr;

			if (myLeftCatapultTowerHealth <= 0)
			{
				TowerDeathAnimation(myLeftCatapultTowerPos);
			}
		}
		else if (myTowerHit == myCurrentRightWizardAnimation)
		{
			myRightWizardTowerHealth -= aDamage * aDeltaTime;

			if (myRightWizardFlashTimer < 0.8f)
			{
				myRightWizardFlashTimer = 1.0f;
			}
			myTowerHit = nullptr;

			if (myRightWizardTowerHealth <= 0)
			{
				TowerDeathAnimation(myRightWizardTowerPos);
			}
		}
		else if (myTowerHit == myCurrentLeftWizardAnimation)
		{
			myLeftWizardTowerHealth -= aDamage * aDeltaTime;

			if (myLeftWizardFlashTimer < 0.8f)
			{
				myLeftWizardFlashTimer = 1.0f;
			}
			myTowerHit = nullptr;

			if (myLeftWizardTowerHealth <= 0)
			{
				TowerDeathAnimation(myLeftWizardTowerPos);
			}
		}
	}	 
}

bool BossCastle::IsInside(const CU::Vector2f & aPoint)
{
	
	if (myAllTowersDead)
	{
		return myCurrentCastleAnimation->IsInside(aPoint);
	}
	else
 	{
		if (myCurrentRightCatapultAnimation->IsInside(aPoint))
		{
			myTowerHit = myCurrentRightCatapultAnimation;
			return true;
		}
		else if (myCurrentLeftCatapultAnimation->IsInside(aPoint))
		{
			myTowerHit = myCurrentLeftCatapultAnimation;
			return true;
		}
		else if (myCurrentRightWizardAnimation->IsInside(aPoint))
		{
			myTowerHit = myCurrentRightWizardAnimation;
			return true;
		}
		else if (myCurrentRightWizardAnimation->IsInside(aPoint))
		{
			myTowerHit = myCurrentLeftWizardAnimation;
			return true;
		}
	}
	
	return myCurrentCastleAnimation->IsInside(aPoint);
}


void BossCastle::RightCatapultAttack()
{
	float angel = -0.8f;
	for (int nbrOfBoulders = 0; nbrOfBoulders < 5; ++nbrOfBoulders)
	{
		ProjectileBoulder* projectile = ProjectileFactory::CreateBoulder({ myRightCatapultTowerPos.x, myRightCatapultTowerPos.y + 0.2f, myRightCatapultTowerPos.z - 0.003f },
			myCatapultProjectileSpeed,
			CU::Vector3f(angel, -1.0f, 0.0f));
		projectile->SetDamage(myEnemyClass.GetDMG());
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
		angel += 0.4f;
	}
}

void BossCastle::LeftCatapultAttack()
{
	float angel = -0.8f;
	for (int nbrOfBoulders = 0; nbrOfBoulders < 5; ++nbrOfBoulders)
	{
		ProjectileBoulder* projectile = ProjectileFactory::CreateBoulder({ myLeftCatapultTowerPos.x, myLeftCatapultTowerPos.y + 0.2f, myLeftCatapultTowerPos.z - 0.003f },
			myCatapultProjectileSpeed,
			CU::Vector3f(angel, -1.0f, 0.0f));
		projectile->SetDamage(myEnemyClass.GetDMG());
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
		angel += 0.4f;
	}
}


void BossCastle::CastleTowerAttack()
{
	for (float a = -Tga2D::Pif / 5; a > -Tga2D::Pif; a -= Tga2D::Pif / 5)
	{
		ProjectileIcicle* projectile = ProjectileFactory::CreateIcicles(CU::Vector3f(myPosition.x + 0.2f, myPosition.y - 0.2f, myPosition.z),
			myIcicleProjectileSpeed * 0.5f,
			CU::Vector3f(std::cosf(a), std::sinf(a), 0.f));
		projectile->SetDamage(myEnemyClass.GetDMG());
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
	}

	for (float a = -Tga2D::Pif / 5; a > -Tga2D::Pif; a -= Tga2D::Pif / 5)
	{
		ProjectileIcicle* projectile = ProjectileFactory::CreateIcicles(CU::Vector3f(myPosition.x - 0.2f, myPosition.y - 0.2f, myPosition.z),
			myIcicleProjectileSpeed * 0.5f,
			CU::Vector3f(std::cosf(a), std::sinf(a), 0.f));
		projectile->SetDamage(myEnemyClass.GetDMG());
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
	}
}

void BossCastle::BombAttack()
{
	if (!myBombCircleFilled)
	{
		for (float index = 0.0f; index < Tga2D::Pif * 2.0f; index += Tga2D::Pif / 10.0f)
		{

			if (!(((myTrackPlayerLocation.x + std::cos(index) / 2.0f) < (myPosition.x + 0.55f)) &&
				((myTrackPlayerLocation.x + std::cos(index) / 2.0f) > (myPosition.x - 0.55f)) &&
				((myTrackPlayerLocation.y + std::sin(index) / 2.0f) > (myPosition.y - 1.1f))))
			{
				ProjectileDelayedBomb* projectile = ProjectileFactory::CreateDelayedBomb(CU::Vector3f(myTrackPlayerLocation.x + std::cos(index) / 2.0f, myTrackPlayerLocation.y + std::sin(index) / 2.0f, myPosition.z + 0.002f),
					0.0f,
					CU::Vector3f(0.0f, 0.0f, 0.0f),
					1.5f);
				projectile->SetDamage(myEnemyClass.GetDMG());
				ProjectileManager::GetInstance()->EnemyShoot(projectile);
			}
		}

		for (float index = 0.0f; index < Tga2D::Pif * 2.0f; index += Tga2D::Pif / 14.0f)
		{
			if (!(((myTrackPlayerLocation.x + std::cos(index) / 1.6f) < (myPosition.x + 0.55f)) &&
				((myTrackPlayerLocation.x + std::cos(index) / 1.6f) > (myPosition.x - 0.55f)) &&
				((myTrackPlayerLocation.y + std::sin(index) / 1.6f) > (myPosition.y - 1.1f))))
			{
				ProjectileDelayedBomb* projectile = ProjectileFactory::CreateDelayedBomb(CU::Vector3f(myTrackPlayerLocation.x + std::cos(index) / 1.6f, myTrackPlayerLocation.y + std::sin(index) / 1.6f, myPosition.z + 0.002f),
					0.0f,
					CU::Vector3f(0.0f, 0.0f, 0.0f),
					1.0f);
				projectile->SetDamage(myEnemyClass.GetDMG());
				ProjectileManager::GetInstance()->EnemyShoot(projectile);
			}
		}

		for (float index = 0.0f; index < Tga2D::Pif * 2.0f; index += Tga2D::Pif / 18.0f)
		{
			if (!(((myTrackPlayerLocation.x + std::cos(index) / 1.35f) < (myPosition.x + 0.55f)) &&
				((myTrackPlayerLocation.x + std::cos(index) / 1.35f) > (myPosition.x - 0.55f)) &&
				((myTrackPlayerLocation.y + std::sin(index) / 1.35f) > (myPosition.y - 1.1f))))
			{
				ProjectileDelayedBomb* projectile = ProjectileFactory::CreateDelayedBomb(CU::Vector3f(myTrackPlayerLocation.x + std::cos(index) / 1.35f, myTrackPlayerLocation.y + std::sin(index) / 1.35f, myPosition.z + 0.002f),
					0.0f,
					CU::Vector3f(0.0f, 0.0f, 0.0f),
					0.5f);
				projectile->SetDamage(myEnemyClass.GetDMG());
				ProjectileManager::GetInstance()->EnemyShoot(projectile);
			}
		}

		myBombCircleFilled = true;
	}
	else
	{
		if (!(myTrackPlayerLocation.x < (myPosition.x + 0.55f) &&
			myTrackPlayerLocation.x > (myPosition.x - 0.55f) &&
			myTrackPlayerLocation.y >(myPosition.y - 1.1f)))
		{
			ProjectileDelayedBomb* projectile = ProjectileFactory::CreateDelayedBomb(CU::Vector3f(myTrackPlayerLocation.x, myTrackPlayerLocation.y, myPosition.z + 0.002f),
				0.0f,
				CU::Vector3f(0.0f, 0.0f, 0.0f),
				1.5f);
			projectile->SetDamage(myEnemyClass.GetDMG());
			ProjectileManager::GetInstance()->EnemyShoot(projectile);
		}

		for (float index = 0.0f; index < Tga2D::Pif * 2.0f; index += Tga2D::Pif / 3.0f)
		{
			if (!(((myTrackPlayerLocation.x + std::cos(index) / 7.0f) < (myPosition.x + 0.55f)) &&
				((myTrackPlayerLocation.x + std::cos(index) / 7.0f) > (myPosition.x - 0.55f)) &&
				((myTrackPlayerLocation.y + std::sin(index) / 7.0f) >(myPosition.y - 1.1f))))
			{
				ProjectileDelayedBomb* projectile = ProjectileFactory::CreateDelayedBomb(CU::Vector3f(myTrackPlayerLocation.x + std::cos(index) / 7.0f, myTrackPlayerLocation.y + std::sin(index) / 7.0f, myPosition.z + 0.002f),
					0.0f,
					CU::Vector3f(0.0f, 0.0f, 0.0f),
					1.0f);
				projectile->SetDamage(myEnemyClass.GetDMG());
				ProjectileManager::GetInstance()->EnemyShoot(projectile);
			}
		}

		for (float index = 0.0f; index < Tga2D::Pif * 2.0f; index += Tga2D::Pif / 6.0f)
		{
			if (!(((myTrackPlayerLocation.x + std::cos(index) / 3.8f) < (myPosition.x + 0.55f)) &&
				((myTrackPlayerLocation.x + std::cos(index) / 3.8f) > (myPosition.x - 0.55f)) &&
				((myTrackPlayerLocation.y + std::sin(index) / 3.8f) >(myPosition.y - 1.1f))))
			{
				ProjectileDelayedBomb* projectile = ProjectileFactory::CreateDelayedBomb(CU::Vector3f(myTrackPlayerLocation.x + std::cos(index) / 3.8f, myTrackPlayerLocation.y + std::sin(index) / 3.8f, myPosition.z + 0.002f),
					0.0f,
					CU::Vector3f(0.0f, 0.0f, 0.0f),
					0.5f);
				projectile->SetDamage(myEnemyClass.GetDMG());
				ProjectileManager::GetInstance()->EnemyShoot(projectile);
			}
		}

		for (float index = 0.0f; index < Tga2D::Pif * 2.0f; index += Tga2D::Pif / 9.0f)
		{
			if (!(((myTrackPlayerLocation.x + std::cos(index) / 2.5f) < (myPosition.x + 0.55f)) &&
				((myTrackPlayerLocation.x + std::cos(index) / 2.5f) > (myPosition.x - 0.55f)) &&
				((myTrackPlayerLocation.y + std::sin(index) / 2.5f) >(myPosition.y - 1.1f))))
			{
				ProjectileDelayedBomb* projectile = ProjectileFactory::CreateDelayedBomb(CU::Vector3f(myTrackPlayerLocation.x + std::cos(index) / 2.5f, myTrackPlayerLocation.y + std::sin(index) / 2.5f, myPosition.z + 0.002f),
					0.0f,
					CU::Vector3f(0.0f, 0.0f, 0.0f),
					0.1f);
				projectile->SetDamage(myEnemyClass.GetDMG());
				ProjectileManager::GetInstance()->EnemyShoot(projectile);
			}
		}

		myBombCircleFilled = false;
	}
}

void BossCastle::ArrowAttack()
{
	//float xPos = (myPosition.x - 0.5f) + CU::Random() * ((myPosition.x + 0.5f) - (myPosition.x - 0.5f));

	float yPosMiddle = myPosition.y - 0.68f;
	CommonUtilities::Vector3f myArrowPos;

	float random = CU::Random();

	if (random < 1.f / 7.f)
	{
		CommonUtilities::Vector3f LeftFirstArrowPos = { myPosition.x - 0.37f, yPosMiddle + 0.2f, myPosition.z };
		myArrowPos = LeftFirstArrowPos;
	}
	else if (random < 2.f / 7.f)
	{
		CommonUtilities::Vector3f LeftSecondArrowPos = { myPosition.x - 0.27f, yPosMiddle + 0.1f, myPosition.z };
		myArrowPos = LeftSecondArrowPos;
	}
	else if (random < 3.f / 7.f)
	{
		CommonUtilities::Vector3f LeftThirdArrowPos = { myPosition.x - 0.14f, yPosMiddle + 0.03f, myPosition.z };
		myArrowPos = LeftThirdArrowPos;
	}
	else if (random < 4.f / 7.f)
	{
		CommonUtilities::Vector3f middleArrowPos = { myPosition.x, yPosMiddle, myPosition.z };
		myArrowPos = middleArrowPos;
	}
	else if (random < 5.f / 7.f)
	{
		CommonUtilities::Vector3f RightFirstArrowPos = { myPosition.x + 0.13f, yPosMiddle + 0.02f, myPosition.z };
		myArrowPos = RightFirstArrowPos;
	}
	else if (random < 6.f / 7.f)
	{
		CommonUtilities::Vector3f RightSecondArrowPos = { myPosition.x + 0.25f, yPosMiddle + 0.08f, myPosition.z };
		myArrowPos = RightSecondArrowPos;
	}
	else
	{
		CommonUtilities::Vector3f RightThirdArrowPos = { myPosition.x + 0.37f, yPosMiddle + 0.17f, myPosition.z };
		myArrowPos = RightThirdArrowPos;
	}

	ProjectileArrow* projectile = ProjectileFactory::CreateArrow(myArrowPos,
		myArrowProjectileSpeed,
		{ 0.0f, -1.0f, 0.0f });
	projectile->SetDamage(myEnemyClass.GetDMG());
	ProjectileManager::GetInstance()->EnemyShoot(projectile);
}

void BossCastle::CheckTowerStatus()
{
	if (myRightCatapultTowerHealth <= 0 
		&& myLeftCatapultTowerHealth <= 0 
		&& myRightWizardTowerHealth <= 0 
		&& myLeftWizardTowerHealth <= 0)
	{
		myAllTowersDead = true;
	}
}

void BossCastle::UpdateHealthBars(float aDeltaTime)
{
	myHealthBar.Update(aDeltaTime, myHealth, { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f });
	myRightCatapultTowerHealthBar.Update(aDeltaTime, myRightCatapultTowerHealth, { myRightCatapultTowerPos.x, myRightCatapultTowerPos.y + 0.1f, myRightCatapultTowerPos.z - 0.01f });
	myLeftCatapultTowerHealthBar.Update(aDeltaTime, myLeftCatapultTowerHealth, { myLeftCatapultTowerPos.x, myLeftCatapultTowerPos.y + 0.1f, myLeftCatapultTowerPos.z - 0.01f });
	myRightWizardTowerHealthBar.Update(aDeltaTime, myRightWizardTowerHealth, { myRightWizardTowerPos.x, myRightWizardTowerPos.y + 0.1f, myRightWizardTowerPos.z - 0.01f });
	myLeftWizardTowerHealthBar.Update(aDeltaTime, myLeftWizardTowerHealth, { myLeftWizardTowerPos.x, myLeftWizardTowerPos.y + 0.1f, myLeftWizardTowerPos.z - 0.01f });

	myHealthBar.Show();
	myRightCatapultTowerHealthBar.Show();
	myLeftCatapultTowerHealthBar.Show();
	myRightWizardTowerHealthBar.Show();
	myLeftWizardTowerHealthBar.Show();
}

inline bool BossCastle::IsTouching(Beam& aBeam)
{
	if (myAllTowersDead)
	{
		return aBeam.IsTouching(GetCollider());
	}
	else
	{
		if (aBeam.IsTouching(&myColliderRightCatapultTower) && myRightCatapultTowerHealth > 0)
		{
			myTowerHit = myCurrentRightCatapultAnimation;
			return true;
		}
		else if (aBeam.IsTouching(&myColliderLeftCatapultTower) && myLeftCatapultTowerHealth > 0)
		{
			myTowerHit = myCurrentLeftCatapultAnimation;
			return true;
		}
		else if (aBeam.IsTouching(&myColliderRightWizardTower) && myRightWizardTowerHealth > 0)
		{
			myTowerHit = myCurrentRightWizardAnimation;
			return true;
		}
		else if (aBeam.IsTouching(&myColliderLeftWizardTower) && myLeftWizardTowerHealth > 0)
		{
			myTowerHit = myCurrentLeftWizardAnimation;
			return true;
		}
	}

	return aBeam.IsTouching(GetCollider());
}

inline bool BossCastle::IsTouching(Projectile* aProjectile)
{
	if (myAllTowersDead)
	{
		return aProjectile->IsTouching(GetCollider());
	}
	else
	{
		if (aProjectile->IsTouching(&myColliderRightCatapultTower) && myRightCatapultTowerHealth > 0)
		{	
			myTowerHit = myCurrentRightCatapultAnimation;
			return true;
		}
		else if (aProjectile->IsTouching(&myColliderLeftCatapultTower) && myLeftCatapultTowerHealth > 0)
		{
			myTowerHit = myCurrentLeftCatapultAnimation;
			return true;
		}
		else if (aProjectile->IsTouching(&myColliderRightWizardTower) && myRightWizardTowerHealth > 0)
		{
			myTowerHit = myCurrentRightWizardAnimation;
			return true;
		}
		else if (aProjectile->IsTouching(&myColliderLeftWizardTower) && myLeftWizardTowerHealth > 0)
		{
			myTowerHit = myCurrentLeftWizardAnimation;
			return true;
		}
	}

	return aProjectile->IsTouching(GetCollider());
}

void BossCastle::RenderDebug(const CommonUtilities::Camera & aCamera)
{
	myCollider.RenderDebug(aCamera);
	myColliderRightCatapultTower.RenderDebug(aCamera);
	myColliderLeftCatapultTower.RenderDebug(aCamera);
	myColliderRightWizardTower.RenderDebug(aCamera);
	myColliderLeftWizardTower.RenderDebug(aCamera);
}

void BossCastle::DamagedFlash(Animation & aAnimation, float aFlashTimer)
{
	if (aFlashTimer >= 0.9f)
	{
		aAnimation.SetTint({ 255.0f, 255.0f, 255.0f, 255.0f });
	}
	else
	{
		aAnimation.SetTint({ 1.0f, 1.0f, 1.0f, 1.0f });
	}
}

inline bool BossCastle::IsTouching(const CircleCollider * aCircleCollider)
{
	if (myHealth > 1)
	{
		if (myCollider.IsTouching(aCircleCollider))
		{
			return true;
		}
	}

	if (myColliderRightCatapultTower.IsTouching(aCircleCollider) && myRightCatapultTowerHealth > 0)
	{
		return true;
	}

	if (myColliderLeftCatapultTower.IsTouching(aCircleCollider) && myLeftCatapultTowerHealth > 0)
	{
		return true;
	}

	if (myColliderRightWizardTower.IsTouching(aCircleCollider) && myRightWizardTowerHealth > 0)
	{
		return true;
	}

	if (myColliderLeftWizardTower.IsTouching(aCircleCollider) && myLeftWizardTowerHealth > 0)
	{
		return true;
	}

	return false;
}

inline bool BossCastle::BeamCollision(const CircleCollider * aCircleCollider)
{
	if (myLeftBeam.IsTouching(aCircleCollider))
	{
		return true;
	}

	if (myRightBeam.IsTouching(aCircleCollider))
	{
		return true;
	}

	return false;
}

void BossCastle::TowerDeathAnimation(const CommonUtilities::Vector3f aPosition)
{
	for (int index = 0; index < 50; ++index)
	{
		Animation* newExplosion = new Animation("Assets/Images/Projectiles/explosion.dds");
		newExplosion->Setup(2, 3, 6);
		newExplosion->SetDuration(0.3f + CU::Random() * (1.0f - 0.3f));
		float scale = 1.0f + CU::Random() * (3.0f - 1.0f);
		newExplosion->SetScale({ scale, scale });
		float xPos = (aPosition.x - 0.1f) + CU::Random() * ((aPosition.x + 0.1f) - (aPosition.x - 0.1f));
		float yPos = (aPosition.y - 0.2f) + CU::Random() * ((aPosition.y + 0.2f) - (aPosition.y - 0.2f));
		newExplosion->SetPosition({ xPos, yPos, aPosition.z });
		ExplosionManager::GetInstance()->AddExplosion(newExplosion);
	}
	SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/Explosion.wav", 0.4f);
}

void BossCastle::FinalBossExplosion()
{
	if (myHasPlayedFinalBossExplosion)
	{
		return;
	}
	myHasPlayedFinalBossExplosion = true;

	for (int index = 0; index < 10; ++index)
	{
		Animation* newExplosion = new Animation("Assets/Images/Projectiles/explosion.dds");
		newExplosion->Setup(2, 3, 6);
		newExplosion->SetDuration(0.3f + CU::Random() * (1.5f - 0.3f));
		float scale = 1.0f + CU::Random() * (3.0f - 1.0f);
		newExplosion->SetScale({ scale, scale });
		float xPos = (myPosition.x - 0.3f) + CU::Random() * ((myPosition.x + 0.3f) - (myPosition.x - 0.3f));
		float yPos = (myPosition.y - 0.3f) + CU::Random() * ((myPosition.y + 0.3f) - (myPosition.y - 0.3f));
		newExplosion->SetPosition({ xPos, yPos, myPosition.z - 0.01f });
		ExplosionManager::GetInstance()->AddExplosion(newExplosion);
	}

	for (int index = 0; index < 200; ++index)
	{
		EnemyFactory::CreateEnemyParticle(myEnemyClass, myPosition, myElement, true);
	}
}

void BossCastle::DeathAnimation()
{
	if (mySpawnExplosion)
	{
		SpawnExplosion(0.1f);
	}

	if (myHasPlayedFinalBossExplosion)
	{
		myHealth = 0.0f;
	}
}
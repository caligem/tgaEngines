#pragma once
#include "Enemy.h"
#include "Beam.h"

#include <Animation.h>

class Player;
class EnemyClassCache;

enum class eCastleMoveSet
{
	attackWizards,
	attackBomb
};

class BossCastle : public Enemy
{
public:
	BossCastle(EnemyClassCache& aEnemyClassCache);
	~BossCastle();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float aDeltaTime, Player& aPlayer) override;
	void UpdateCollider() override { myCollider.SetPosition({ myPosition.x, myPosition.y - 0.4f, myPosition.z }); }
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsBoss() override { return true; }

	inline bool IsTouching(Beam& aBeam) override;
	inline bool IsTouching(Projectile* aProjectile) override;
	void DeathAnimation() override;
	void TowerDeathAnimation(const CommonUtilities::Vector3f aPosition);
	void FinalBossExplosion() override;
	void DamagedFlash(Animation& aAnimation, float aFlashTimer);
	inline bool IsTouching(const CircleCollider* aCircleCollider) override;
	inline bool BeamCollision(const CircleCollider* aCircleCollider) override;
	void RenderDebug(const CommonUtilities::Camera& aCamera) override;

	void RightCatapultAttack();
	void LeftCatapultAttack();
	void CastleTowerAttack();
	void BombAttack();
	void ArrowAttack();
	void CheckTowerStatus();
	void UpdateHealthBars(float aDeltaTime);

private:
	bool myIsSlowed;
	float mySlowTimer;

	Animation* myCurrentCastleAnimation;
	Animation myAnimationIdle;
	Animation myAnimationCastleAim;
	Animation myAnimationCastleWizards;

	Animation* myCurrentRightCatapultAnimation;
	Animation myAnimationRightCatapultIdle;
	Animation myAnimationRightCatapultAttack;

	Animation* myCurrentLeftCatapultAnimation;
	Animation myAnimationLeftCatapultIdle;
	Animation myAnimationLeftCatapultAttack;

	Animation* myCurrentRightWizardAnimation;
	Animation myAnimationRightWizardIdle;
	Animation myAnimationRightWizardAim;

	Animation* myCurrentLeftWizardAnimation;
	Animation myAnimationLeftWizardIdle;
	Animation myAnimationLeftWizardAim;

	Animation myAnimationRightCataputTowerCrystal;
	Animation myAnimationLeftCatapultTowerCrystal;
	Animation myAnimationRightWizardTowerCrystal;
	Animation myAnimationLeftWizardTowerCrystal;
	Animation myAnimationCastleShield;
	Animation* myTowerHit;

	bool myIncreaseTint;
	float myCurrentTint;
	float myDeathTint;

	eCastleMoveSet myMoveSet;

	Beam myLeftBeam;
	Beam myRightBeam;
	Beam myLeftLaserSight;
	Beam myRightLaserSight;

	bool myLockLaserSight;
	bool myAnimationFinished;

	CircleCollider myColliderRightCatapultTower;
	CircleCollider myColliderLeftCatapultTower;
	CircleCollider myColliderRightWizardTower;
	CircleCollider myColliderLeftWizardTower;

	HealthBar myRightCatapultTowerHealthBar;
	HealthBar myLeftCatapultTowerHealthBar;
	HealthBar myRightWizardTowerHealthBar;
	HealthBar myLeftWizardTowerHealthBar;

	float myRotation;
	float myRightCatapultTowerHealth;
	float myLeftCatapultTowerHealth;
	float myRightWizardTowerHealth;
	float myLeftWizardTowerHealth;

	float myCatapultProjectileSpeed;
	float myIcicleProjectileSpeed;
	float myArrowProjectileSpeed;

	float myCatapultAttackFireRate;
	float myWizardFireRate;
	float myCastleTowerFireRate;
	float myBombFireRate;
	float myArrowFireRate;

	float myCatapultAttackCooldown;
	float myWizardCooldown;
	float myCastleTowerCooldown;
	float myBombCooldown;
	float myArrowCooldown;

	float myBeamLockOn;
	float myBeamDuration;
	bool myAllTowersDead;

	int myNbrOfWizardAttacks;
	bool myBombCircleFilled;

	float myRightCatapultFlashTimer;
	float myLeftCatapultFlashTimer;
	float myRightWizardFlashTimer;
	float myLeftWizardFlashTimer;

	float myLeftBoundary;
	float myRightBoundary;
	float myTopBoundary;
	float myBottomBoundary;
	float myCenterY;

	CommonUtilities::Vector3f myRightCatapultTowerPos;
	CommonUtilities::Vector3f myLeftCatapultTowerPos;
	CommonUtilities::Vector3f myRightWizardTowerPos;
	CommonUtilities::Vector3f myLeftWizardTowerPos;

	CommonUtilities::Vector3f myRightWizardShootDir;
	CommonUtilities::Vector3f myLeftWizardShootDir;

	CommonUtilities::Vector3f myTrackPlayerLocation;
	EnemyClassCache& myEnemyClassCache;

};



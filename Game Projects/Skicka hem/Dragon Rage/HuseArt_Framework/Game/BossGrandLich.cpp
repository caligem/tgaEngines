#include "stdafx.h"
#include "BossGrandLich.h"

#include "ProjectileManager.h"
#include "ExplosionManager.h"
#include "Player.h"
#include "SFXManager.h"

#include "ProjectileFactory.h"
#include "EnemyFactory.h"
#include "EnemyClassCache.h"

#include "EnemyOwlRider.h"
#include "EnemyGiantOwl.h"
#include "EnemyNatureWisp.h"
#include "Blackboard.h"

#include <Random.h>

namespace CU = CommonUtilities;

BossGrandLich::BossGrandLich(EnemyClassCache & aEnemyClassCache)
	: myRotation(0)
	, myBeamFireRate(2.0f)
	, mySpawnEnemiesFireRate(2.0f)
	, myExplosionFireRate(2.0f)
	, myPrimaryAttackAngel(Tga2D::Pif / 8)
	, myBeamStartPos(0.0f, 0.0f, 0.0f)
	, myBeamEndPos(0.0f, 0.0f, 0.0f)
	, myShootDirection(0.0f, 0.0f, 0.0f)
	, myLeftToRight(true)
	, myIncreaseTint(true)
	, myDeathTint(0.0f)
	, myCurrentTint(1.0f)
	, myNbrOfBombs(10)
	, myNbrOfEnemies(0)
	, myEnemyType(0)
	, myLeftBoundary(-1.4f)
	, myRightBoundary(1.4f)
	, myTopBoundary(0.0f)
	, myBottomBoundary(-1.8f)
	, myCenterY(0)
	, myIsSlowed(false)
	, myEnemyClassCache(aEnemyClassCache)
	, myHoverDistance(0.05f)
	, myBoppingSpeed(0.5f)
	, myHoverTime(0.0f)
{}



BossGrandLich::~BossGrandLich()
{
}

void BossGrandLich::Init(const EnemyClass& aEnemyClass, const CU::Vector3f aPosition)
{
	myPosition = aPosition;

	myAnimationIdle.Init("Assets/Images/Enemies/Bosses/enemy_boss_lich.dds", { myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.001f });
	myAnimationIdle.Setup(2, 2, 4);
	myAnimationBeam.Init("Assets/Images/Enemies/Bosses/enemy_boss_lich_beam.dds", { myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.001f });
	myAnimationBeam.Setup(2, 4, 3);
	myAnimationBeam.SetLoop(false);
	myAnimationSpawning.Init("Assets/Images/Enemies/Bosses/enemy_boss_lich_spawning.dds", { myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.001f });
	myAnimationSpawning.Setup(2, 4, 8);
	myAnimationSpawning.SetDuration(1.5f);
	myAnimationSpawning.SetLoop(false);
	myAnimationSeeds.Init("Assets/Images/Enemies/Bosses/enemy_boss_lich_seeds.dds", { myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.001f });
	myAnimationSeeds.Setup(2, 2, 4);
	myAnimationSeeds.SetLoop(false);
	myCurrentAnimation = &myAnimationIdle;

	myEnemyClass = aEnemyClass;
	myHealth = myEnemyClass.GetHP();
	myBeamCooldown = myBeamFireRate;
	mySpawnEnemiesCooldown = mySpawnEnemiesFireRate;
	myExplosionCooldown = myExplosionFireRate;
	myTopBoundary += aPosition.y;
	myBottomBoundary += aPosition.y;
	myCenterY = (myTopBoundary + myBottomBoundary) / 2.0f;

	myBeam.Init(eBeamType::Lich);
	myBeam.Dectivate();
	myBeamStartPos = { myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.0001f };
	myBeamEndPos = { myLeftBoundary * 2, myBottomBoundary - 1.0f, myPosition.z + 0.0001f };

	myMoveSet = eGrandLichMoveSet::attackBeam;
	myCollider.SetRadius(0.25f);

	myHealthBar.Init(myEnemyClass.GetHP(), { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f }, FLT_MAX, { 8.0f, 1.0f });

	myBleed = true;
	mySpawnExplosion = true;
	myPlayBossDeathAnimation = false;
	myHasPlayedFinalBossExplosion = false;

	if (myEnemyClass.GetType().find(".hafd") != std::string::npos)
	{
		myDialogue.Init(myEnemyClass.GetType());
	}
}

void BossGrandLich::Update(float aDeltaTime, Player& aPlayer)
{
	aPlayer;

	if (myHealth == 1)
	{
		DeathAnimation();
		myDeathTint += aDeltaTime / 20.0f;

		if (myCurrentAnimation->GetTint().x > 3.0f)
		{
			myIncreaseTint = false;
		}
		else if (myCurrentAnimation->GetTint().x < 0.0f)
		{
			myIncreaseTint = true;
		}

		if (myIncreaseTint)
		{
			myCurrentTint += myDeathTint;
			myCurrentAnimation->SetTint({ myCurrentTint, myCurrentTint, myCurrentTint, 1.0f });
		}
		else if (!myIncreaseTint)
		{
			myCurrentTint -= myDeathTint;
			myCurrentAnimation->SetTint({ myCurrentTint, myCurrentTint, myCurrentTint, 1.0f });
		}
		
		myBeam.Dectivate();
		
		myHealthBar.Update(aDeltaTime, myHealth, { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f });
		myHealthBar.Show();
		myCountDown.Update(aDeltaTime);

		return;
	}

	myPosition += CommonUtilities::Vector3f(0.f, myHoverDistance * std::sinf(myHoverTime * myBoppingSpeed), 0.f) * aDeltaTime;
	myHoverTime += aDeltaTime * 2.f * Tga2D::Pif;

	if (myMoveSet == eGrandLichMoveSet::attackBeam)
	{
		myBeamCooldown -= aDeltaTime;		

		if (myBeamCooldown < 0.0f && !myBeam.IsActive())
		{	
			if (myCurrentAnimation != &myAnimationBeam)
			{
				myCurrentAnimation = &myAnimationBeam;
			}
			else if (myAnimationBeam.IsFinished())
			{
				myBeam.Activate();

				myAnimationBeam.Setup(2, 4, 4, 3);
				myAnimationBeam.SetLoop(true);
			}			
		}

		if (myBeam.IsActive())
		{
			BeamAttack(aDeltaTime);
		}

		myBeam.Update(aDeltaTime, false);
	}

	if (myMoveSet == eGrandLichMoveSet::attackSpawnEnemies)
	{
		mySpawnEnemiesCooldown -= aDeltaTime;		

		if (mySpawnEnemiesCooldown < 0.0f)
		{
			if (myCurrentAnimation != &myAnimationSpawning)
			{
				myCurrentAnimation = &myAnimationSpawning;
			}
			else if (myAnimationSpawning.IsFinished())
			{
				SpawnEnemiesAttack(myEnemyType);
				myAnimationSpawning.Setup(2, 4, 8);
				myCurrentAnimation = &myAnimationIdle;
				myMoveSet = eGrandLichMoveSet::attackBombs;
				mySpawnEnemiesCooldown = mySpawnEnemiesFireRate;
				if (myEnemyType == 2)
				{
					myEnemyType = 0;
				}
				else
				{
					++myEnemyType;
				}
			}			
		}
	}

	if (myMoveSet == eGrandLichMoveSet::attackBombs)
	{
		myExplosionCooldown -= aDeltaTime;		

		if (myExplosionCooldown < 0.0f)
		{
			if (myCurrentAnimation != &myAnimationSeeds &&
				myNbrOfBombs == 10)
			{
				myCurrentAnimation = &myAnimationSeeds;
			}
			else if (myAnimationSeeds.IsFinished())
			{
				if (myNbrOfBombs > 0)
				{						
					myCurrentAnimation = &myAnimationIdle;
					ExplosionAttack();
					myExplosionCooldown = 0.1f;
					--myNbrOfBombs;
				}
				else
				{	
					myAnimationSeeds.Setup(2, 2, 4);
					myMoveSet = eGrandLichMoveSet::attackBeam;
					myExplosionCooldown = myExplosionFireRate;
					myNbrOfBombs = 10;
				}
			}			
		}
	}

	myCurrentAnimation->Update(aDeltaTime);
	myCurrentAnimation->SetPosition({ myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.001f });

	DamagedFlash(*myCurrentAnimation);
	if (myFlashTimer > 0.0f)
	{
		myFlashTimer -= aDeltaTime;
	}

	myHealthBar.Update(aDeltaTime, myHealth, { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f });
	myHealthBar.Show();
	myCountDown.Update(aDeltaTime);
}

void BossGrandLich::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(myCurrentAnimation);
	myBeam.FillRenderBuffer(aRenderBuffer);
	myHealthBar.FillRenderBuffer(aRenderBuffer);
}

void BossGrandLich::TakeDamage(const float aDamage, const float aDeltaTime)
{
	if (!myPlayBossDeathAnimation)
	{
		myHealth -= aDamage * aDeltaTime;
		if (myBleed)
		{
			SpawnBlood(3, 1.5f);

			if (myCanPlayTakeDamageSound)
			{
				myCanPlayTakeDamageSound = false;
				SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/BossHurt.wav", 0.5f);
				myCountDown.Set(std::bind(&BossGrandLich::CanPlayTakeDamageSound, this), 0.5f);
			}
		}

		if (myFlashTimer < 0.8f)
		{
			myFlashTimer = 1.0f;
		}

		if (myHealth < 1.0f)
		{
			myHealth = 1.0f;
			myFlashTimer = 0.0f;
			DamagedFlash(*myCurrentAnimation);
			myBleed = true;
			myCountDown.Set(std::bind(&BossGrandLich::FinalBossExplosion, this), 3.0f);
			myCountDown.Set(std::bind([=] { Blackboard::GetInstance()->SetNote("deathslowmotion", "true"); }), 2.0f);
			myPlayBossDeathAnimation = true;
		}
	}
}

bool BossGrandLich::IsInside(const CU::Vector2f & aPoint)
{
	aPoint;
	return myCurrentAnimation->IsInside(aPoint);
}

void BossGrandLich::BeamAttack(float aDeltaTime)
{
	myBeam.SetStartPosition(myBeamStartPos);
	myBeam.SetEndPosition(myBeamEndPos);

	if (myLeftToRight)
	{
		if (myBeamEndPos.x < myRightBoundary * 2)
		{
			myBeamEndPos.x += aDeltaTime;
		}
		else
		{
			if (myAnimationBeam.GetLoop() == true)
			{
				myAnimationBeam.SetLoop(false);
			}
			else if (myAnimationBeam.IsFinished() &&
					 myAnimationBeam.GetFrames() != 5)
			{
				myAnimationBeam.Setup(2, 4, 5, 3);
			}
			else if (myAnimationBeam.IsFinished() &&
					 myAnimationBeam.GetFrames() == 5)
			{				
				myLeftToRight = false;
				myBeam.Dectivate();
				myBeamCooldown = myBeamFireRate;
				myAnimationBeam.Setup(2, 4, 3);
				myCurrentAnimation = &myAnimationIdle;
				myMoveSet = eGrandLichMoveSet::attackSpawnEnemies;
			}		
		}
	}
	else if (!myLeftToRight)
	{
		if (myBeamEndPos.x > myLeftBoundary * 2)
		{
			myBeamEndPos.x -= aDeltaTime;
		}
		else
		{
			if (myAnimationBeam.GetLoop() == true)
			{
				myAnimationBeam.SetLoop(false);
			}
			else if (myAnimationBeam.IsFinished() &&
				myAnimationBeam.GetFrames() != 5)
			{
				myAnimationBeam.Setup(2, 4, 5, 3);
			}
			else if (myAnimationBeam.IsFinished() &&
				myAnimationBeam.GetFrames() == 5)
			{
				myLeftToRight = true;
				myBeam.Dectivate();
				myBeamCooldown = myBeamFireRate;
				myAnimationBeam.Setup(2, 4, 3);
				myCurrentAnimation = &myAnimationIdle;
				myMoveSet = eGrandLichMoveSet::attackSpawnEnemies;
			}
		}
	}
}

void BossGrandLich::SpawnEnemiesAttack(const int aType)
{
	

	if (aType == 0)
	{
		float xPos = 0.0f;
		for (int i = 0; i < 4; ++i)
		{
			xPos = (static_cast<float>(i) * 0.5f) - 0.7f;
			EnemyOwlRider* mySpawnEnemy = dynamic_cast<EnemyOwlRider*>(EnemyFactory::CreateEnemy(myEnemyClassCache.GetClass("Assets/Data/enemies/owlrider.hafe"),
										CU::Vector3f(myPosition.x + xPos, myPosition.y - 0.5f, myPosition.z - 0.001f),
										myEnemyClassCache, eElement::Lightning));

			if (mySpawnEnemy != nullptr)
			{
				mySpawnEnemy->AllowStrafe();
				mySpawnEnemy->IsSpawned();
			}
		}
	}

	else if (aType == 1)
	{
		EnemyGiantOwl* mySpawnEnemyMid = dynamic_cast<EnemyGiantOwl*>(EnemyFactory::CreateEnemy(myEnemyClassCache.GetClass("Assets/Data/enemies/giantowl.hafe"),
									     CU::Vector3f(myPosition.x, myPosition.y - 0.5f, myPosition.z - 0.001f),
										 myEnemyClassCache, eElement::Ice));

		if (mySpawnEnemyMid != nullptr)
		{
			mySpawnEnemyMid->SetMoveDown({ 0.0f, -1.0f, 0.0f }, 0.5f);
			mySpawnEnemyMid->IsSpawned();
		}

		float xPos = 0.0f;
		for (int i = 1; i < 4; ++i)
		{
			xPos = (static_cast<float>(i) * 0.2f);

			EnemyGiantOwl* mySpawnEnemyLeft = dynamic_cast<EnemyGiantOwl*>(EnemyFactory::CreateEnemy(myEnemyClassCache.GetClass("Assets/Data/enemies/giantowl.hafe"),
											  CU::Vector3f(myPosition.x - xPos, myPosition.y - 0.5f + (xPos / 2), myPosition.z - 0.001f),
											  myEnemyClassCache, eElement::Ice));

			if (mySpawnEnemyLeft != nullptr)
			{
				mySpawnEnemyLeft->SetMoveDown({ 0.0f, -1.0f, 0.0f }, 0.5f);
				mySpawnEnemyLeft->IsSpawned();
			}

			EnemyGiantOwl* mySpawnEnemyRight = dynamic_cast<EnemyGiantOwl*>(EnemyFactory::CreateEnemy(myEnemyClassCache.GetClass("Assets/Data/enemies/giantowl.hafe"),
		   									   CU::Vector3f(myPosition.x + xPos, myPosition.y - 0.5f + (xPos / 2), myPosition.z - 0.001f),
											   myEnemyClassCache, eElement::Ice));

			if (mySpawnEnemyRight != nullptr)
			{
				mySpawnEnemyRight->SetMoveDown({ 0.0f, -1.0f, 0.0f }, 0.5f);
				mySpawnEnemyRight->IsSpawned();
			}
		}
	}

	else if (aType == 2)
	{
		float xPos = -1.2f;
		for (int i = 0; i < 2; ++i)
		{
			xPos += (static_cast<float>(i) * 2.4f);
			EnemyNatureWisp* mySpawnEnemy = dynamic_cast<EnemyNatureWisp*>(EnemyFactory::CreateEnemy(myEnemyClassCache.GetClass("Assets/Data/enemies/naturewisp.hafe"),
				CU::Vector3f(myPosition.x + xPos, myPosition.y - 0.1f, myPosition.z - 0.001f),
				myEnemyClassCache, eElement::Fire));
										  
			mySpawnEnemy->MoveDown(2.0f);
			mySpawnEnemy->IsSpawned();
		}
	}
}

void BossGrandLich::ExplosionAttack()
{
	float randXPos = myLeftBoundary + CU::Random() * (myRightBoundary - myLeftBoundary);
	float randYPos = myBottomBoundary + CU::Random() * (myTopBoundary - myBottomBoundary);

	while (((randXPos < (myPosition.x + 0.85f)) &&
		(randXPos > (myPosition.x - 0.85f)) &&
		(randYPos > (myPosition.y - 0.95f))))
	{
		randXPos = (myLeftBoundary + 0.6f) + CU::Random() * ((myRightBoundary - 0.6f) - (myLeftBoundary + 0.6f));
		randYPos = myBottomBoundary + CU::Random() * ((myTopBoundary - 0.5f) - myBottomBoundary);
	}
	
	ProjectileDelayedBomb* projectile = ProjectileFactory::CreateDelayedBomb(CU::Vector3f(randXPos, randYPos, myPosition.z + 0.001f),
		1.0f,
		CU::Vector3f(0.0f, -1.0f, 0.0f),
		4.0f,
		true,
		2);
	projectile->SetDamage(myEnemyClass.GetDMG());
	ProjectileManager::GetInstance()->EnemyShoot(projectile);
}

void BossGrandLich::RenderDebug(const CommonUtilities::Camera & aCamera)
{
	Enemy::RenderDebug(aCamera);
	if (myBeam.IsActive())myBeam.RenderDebug(aCamera);
}

inline bool BossGrandLich::IsTouching(const CircleCollider * aCircleCollider)
{
	if (myHealth > 1)
	{
		return myCollider.IsTouching(aCircleCollider);
	}

	return false;
}

inline bool BossGrandLich::BeamCollision(const CircleCollider * aCircleCollider)
{
	if (myBeam.IsTouching(aCircleCollider))
	{
		return true;
	}

	return false;
}

void BossGrandLich::DeathAnimation()
{
	if (mySpawnExplosion)
	{
		SpawnExplosion(0.15f);
	}

	if (myBleed)
	{
		SpawnBlood(12, 0.2f);
	}

	if (myHasPlayedFinalBossExplosion)
	{
		myHealth = 0.0f;
	}
}

eElement BossGrandLich::RandomElement()
{
	float random = CU::Random();

	if (random < 1.f / 3.f)
	{
		return eElement::Fire;
	}
	else if (random < 2.f / 3.f)
	{
		return eElement::Ice;
	}
	else
	{
		return eElement::Lightning;
	}
}
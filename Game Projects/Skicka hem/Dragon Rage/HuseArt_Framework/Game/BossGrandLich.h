#pragma once
#include "Enemy.h"
#include "Beam.h"

#include <Animation.h>

class Player;
class EnemyClassCache;

enum class eGrandLichMoveSet
{
	attackBeam,
	attackSpawnEnemies,
	attackBombs
};

class BossGrandLich : public Enemy
{
public:
	BossGrandLich(EnemyClassCache& aEnemyClassCache);
	~BossGrandLich();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float aDeltaTime, Player& aPlayer) override;
	void UpdateCollider() override { myCollider.SetPosition({ myPosition.x, myPosition.y - 0.25f, myPosition.z }); }
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsBoss() override { return true; }

	inline bool IsTouching(const CircleCollider* aCircleCollider) override;
	inline bool BeamCollision(const CircleCollider* aCircleCollider) override;
	inline bool IsTouching(Projectile* aProjectile) override { return aProjectile->IsTouching(GetCollider()); }
	void DeathAnimation() override;

	void BeamAttack(float aDeltaTime);
	void SpawnEnemiesAttack(const int aType);
	void ExplosionAttack();

	void RenderDebug(const CommonUtilities::Camera& aCamera) override;

private:
	eElement RandomElement();

	bool myIsSlowed;
	float mySlowTimer;

	Animation* myCurrentAnimation;
	Animation myAnimationIdle;
	Animation myAnimationBeam;
	Animation myAnimationSpawning;
	Animation myAnimationSeeds;

	bool myIncreaseTint;
	float myCurrentTint;
	float myDeathTint;

	Beam myBeam;
	bool myLeftToRight;
	float myRotation;

	eGrandLichMoveSet myMoveSet;

	float myBeamFireRate;
	float mySpawnEnemiesFireRate;
	float myExplosionFireRate;

	float myBeamCooldown;
	float mySpawnEnemiesCooldown;
	float myExplosionCooldown;

	float myPrimaryAttackAngel;

	CommonUtilities::Vector3f myBeamStartPos;
	CommonUtilities::Vector3f myBeamEndPos;

	int myNbrOfBombs;
	int myNbrOfEnemies;
	int myEnemyType;

	float myLeftBoundary;
	float myRightBoundary;
	float myTopBoundary;
	float myBottomBoundary;
	float myCenterY;

	CommonUtilities::Vector3f myShootDirection;

	EnemyClassCache& myEnemyClassCache;

	//Bopping Movement
	float myHoverDistance;
	float myBoppingSpeed;
	float myHoverTime;

};



#include "stdafx.h"
#include "BossIceDragon.h"

#include "EnemyFactory.h"
#include "ProjectileManager.h"
#include "ExplosionManager.h"
#include "Player.h"
#include "SFXManager.h"
#include "Blackboard.h"
#include <tga2d/math/common_math.h>
#include <Random.h>

#include "ProjectileFactory.h"

namespace CU = CommonUtilities;

BossIceDragon::BossIceDragon()
	: myRotation(0)
	, myMovementSpeed(0.05f)
	, myMoveRight(true)
	, myMoveUp(false)
	, myIcicleProjectileSpeed(1.0f)
	, myOrbProjectileSpeed(0.4f)
	, myFrostBreathProjectileSpeed(1.0f)
	, myPrimaryAttackFireRate(1.0f)
	, myFrozenOrbFireRate(0.8f)
	, myFrostBreathFireRate(0.1f)
	, myBlizzardFireRate(1.0f)
	, myFrostBreathDuration(2.0f)
	, myBlizzardDuration(8.0f)
	, myShootDirection(0.0f, 0.0f, 0.0f)
	, myPrimaryVollyOne(0)
	, myPrimaryVollyTwo(0)
	, myNbrOfNormalAttacks(0)
	, myIncreaseTint(true)
	, myDeathTint(0.0f)
	, myCurrentTint(1.0f)
	, myLeftBoundary(-1.4f)
	, myRightBoundary(1.4f)
	, myTopBoundary(0.0f)
	, myBottomBoundary(-1.8f)
	, myCenterY(0)
	, myIsSlowed(false)
	, myHoverDistance(0.08f)
	, myBoppingSpeed(0.8f)
	, myHoverTime(0.0f)
{}


BossIceDragon::~BossIceDragon()
{
}

void BossIceDragon::Init(const EnemyClass& aEnemyClass, const CU::Vector3f aPosition)
{
	myPosition = aPosition;

	myAnimationIdle.Init("Assets/Images/Enemies/Bosses/enemy_boss_icedragon.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myAnimationShooting.Init("Assets/Images/Enemies/Bosses/enemy_boss_icedragon_animation_shooting.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myAnimationShooting.Setup(1, 3, 3);
	myAnimationFrostBreath.Init("Assets/Images/Enemies/Bosses/enemy_boss_icedragon_animation_icebreath.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myAnimationFrostBreath.Setup(2, 3, 6);
	myAnimationFrostBreath.SetLoop(false);
	myAnimationFrostBreath.SetDuration(2.0f);
	myAnimationBlizzard.Init("Assets/Images/Enemies/Bosses/enemy_boss_icedragon_animation_icicles.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myAnimationBlizzard.Setup(2, 4, 7);
	myCurrentAnimation = &myAnimationIdle;
	myEnemyClass = aEnemyClass;
	myHealth = myEnemyClass.GetHP();
	myPrimaryAttackCooldown = myPrimaryAttackFireRate;
	myFrozenOrbCooldown = myFrozenOrbFireRate;
	myFrostBreathCooldown = myFrostBreathFireRate;
	myBlizzardCooldown = myBlizzardFireRate;
	myTopBoundary += myPosition.y;
	myBottomBoundary += myPosition.y;
	myCenterY = (myTopBoundary + myBottomBoundary) / 2.0f;

	myElement = eElement::Ice;
	myMoveSet = eIceDragonMoveSet::attackNomral;

	myCollider.SetRadius(0.25f);

	myHealthBar.Init(myEnemyClass.GetHP(), { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f }, FLT_MAX, { 8.0f, 1.0f });

	myBleed = true;
	mySpawnExplosion = true;
	mySpawnPowerParticle = true;
	myPlayBossDeathAnimation = false;
	myHasPlayedFinalBossExplosion = false;

	if (myEnemyClass.GetType().find(".hafd") != std::string::npos)
	{
		myDialogue.Init(myEnemyClass.GetType());
	}
}

void BossIceDragon::Update(float aDeltaTime, Player& aPlayer)
{
	aPlayer;

	if (myHealth == 1)
	{
		DeathAnimation();
		myDeathTint += aDeltaTime / 20.0f;

		if (myCurrentAnimation->GetTint().x > 3.0f)
		{
			myIncreaseTint = false;
		}
		else if (myCurrentAnimation->GetTint().x < 0.0f)
		{
			myIncreaseTint = true;
		}

		if (myIncreaseTint)
		{
			myCurrentTint += myDeathTint;
			myCurrentAnimation->SetTint({ myCurrentTint, myCurrentTint, myCurrentTint, 1.0f });
		}
		else if (!myIncreaseTint)
		{
			myCurrentTint -= myDeathTint;
			myCurrentAnimation->SetTint({ myCurrentTint, myCurrentTint, myCurrentTint, 1.0f });
		}

		myHealthBar.Update(aDeltaTime, myHealth, { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f });
		myHealthBar.Show();
		myCountDown.Update(aDeltaTime);

		return;
	}

	//Movement(aDeltaTime);

	myPosition += CommonUtilities::Vector3f(1.5f * myHoverDistance * std::cosf(myHoverTime * myBoppingSpeed / 10.0f), 
											myHoverDistance * std::sinf(myHoverTime * myBoppingSpeed),
											0.f) * aDeltaTime;
	myHoverTime += aDeltaTime * 2.f * Tga2D::Pif;
	
	if (myMoveSet == eIceDragonMoveSet::attackNomral)
	{
		myPrimaryAttackCooldown -= aDeltaTime;
	}

	if (myPrimaryAttackCooldown < 0)
	{
		if (myPrimaryVollyOne < 5)
		{
			myCurrentAnimation = &myAnimationShooting;
			ShootAttack(1);
			++myPrimaryVollyOne;
			if (myPrimaryVollyOne == 5)
			{
				myPrimaryAttackCooldown = 1.2f;
				myCurrentAnimation = &myAnimationIdle;
			}
			else
			{
				myPrimaryAttackCooldown = 0.4f;				
			}
		}
		else
		{
			if (myPrimaryVollyTwo < 5)
			{
				myCurrentAnimation = &myAnimationShooting;
				myPrimaryAttackCooldown = 0.4f;
				ShootAttack(2);
				++myPrimaryVollyTwo;				
			}
			else
			{
				myPrimaryVollyOne = 0;
				myPrimaryVollyTwo = 0;
				++myNbrOfNormalAttacks;
				myPrimaryAttackCooldown = myPrimaryAttackFireRate;
				myCurrentAnimation = &myAnimationIdle;
				if (myNbrOfNormalAttacks == 1)
				{
					myMoveSet = eIceDragonMoveSet::attackOrb;
				}
				else if (myNbrOfNormalAttacks == 2)
				{
					myMoveSet = eIceDragonMoveSet::attackFrostBreath;
				}
				else if (myNbrOfNormalAttacks == 3)
				{
					myMoveSet = eIceDragonMoveSet::attackBlizzard;
					myNbrOfNormalAttacks = 0;
				}
			}
		}
	}

	if (myMoveSet == eIceDragonMoveSet::attackOrb)
	{
		myFrozenOrbCooldown -= aDeltaTime;
	}

	if (myFrozenOrbCooldown < 0)
	{
		myCurrentAnimation = &myAnimationShooting;
		if (myFrozenOrbCooldown < -0.5f)
		{
			FrozenOrbAttack();
			myFrozenOrbCooldown = myFrozenOrbFireRate;
			myPrimaryAttackCooldown += 1;
			myCurrentAnimation = &myAnimationIdle;
			myMoveSet = eIceDragonMoveSet::attackNomral;
		}
	}

	if (myMoveSet == eIceDragonMoveSet::attackFrostBreath)
	{
		myFrostBreathCooldown -= aDeltaTime;

		if (myFrostBreathCooldown < 0)
		{
			if (myCurrentAnimation != &myAnimationFrostBreath)
			{
				myCurrentAnimation = &myAnimationFrostBreath;
			}


			if (myCurrentAnimation == &myAnimationFrostBreath && 
				myAnimationFrostBreath.IsFinished() && 
				myAnimationFrostBreath.GetLoop() == false)
			{
				myAnimationFrostBreath.SetLoop(true);
				myAnimationFrostBreath.Setup(2, 3, 1, 5);
			}

			if (myCurrentAnimation == &myAnimationFrostBreath &&
				myAnimationFrostBreath.GetLoop() == true &&
				myFrostBreathDuration >= 0.0f)
			{
				FrostBreathAttack();
				myFrostBreathCooldown = 0.001f;
				myFrostBreathDuration -= aDeltaTime;
			}
			else if (myFrostBreathDuration < 0.0f)
			{
				myFrostBreathCooldown = myFrostBreathFireRate;
				myFrostBreathDuration = 2.0f;
				myAnimationFrostBreath.SetLoop(false);
				myAnimationFrostBreath.Setup(2, 3, 6);
				myCurrentAnimation = &myAnimationIdle;
				myMoveSet = eIceDragonMoveSet::attackNomral;
			}
		}
	}

	if (myMoveSet == eIceDragonMoveSet::attackBlizzard)
	{
		myBlizzardCooldown -= aDeltaTime;
		myBlizzardDuration -= aDeltaTime;
		myCurrentAnimation = &myAnimationBlizzard;

		if (myBlizzardCooldown < 0)
		{
			BlizzardAttack();
			myBlizzardCooldown = 0.3f;			
		}

		if (myBlizzardDuration < 0)
		{
			myBlizzardCooldown = myBlizzardFireRate;
			myBlizzardDuration = 6.0f;
			myCurrentAnimation = &myAnimationIdle;
			myMoveSet = eIceDragonMoveSet::attackNomral;
		}
	}

	
	myCurrentAnimation->Update(aDeltaTime);
	myCurrentAnimation->SetPosition({ myPosition.x, myPosition.y - 0.4f, myPosition.z + 0.001f });
	
	DamagedFlash(*myCurrentAnimation);
	if (myFlashTimer > 0.0f)
	{
		myFlashTimer -= aDeltaTime;
	}

	myHealthBar.Update(aDeltaTime, myHealth, { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f });
	myHealthBar.Show();
	myCountDown.Update(aDeltaTime);
}

void BossIceDragon::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(myCurrentAnimation);
	myHealthBar.FillRenderBuffer(aRenderBuffer);
}

void BossIceDragon::TakeDamage(const float aDamage, const float aDeltaTime)
{
	if (!myPlayBossDeathAnimation)
	{
		myHealth -= aDamage * aDeltaTime;
		if (myBleed)
		{
			SpawnBlood(3, 1.5f);

			if (myCanPlayTakeDamageSound)
			{
				myCanPlayTakeDamageSound = false;
				SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/BossHurt.wav", 0.5f);
				myCountDown.Set(std::bind(&BossIceDragon::CanPlayTakeDamageSound, this), 0.5f);
			}
		}

		if (myFlashTimer < 0.8f)
		{
			myFlashTimer = 1.0f;
		}

		if (myHealth < 1.0f)
		{
			myHealth = 1.0f;
			myFlashTimer = 0.0f;
			DamagedFlash(*myCurrentAnimation);
			myBleed = true;
			myCountDown.Set(std::bind(&BossIceDragon::FinalBossExplosion, this), 3.0f);
			myCountDown.Set(std::bind([=] { Blackboard::GetInstance()->SetNote("deathslowmotion", "true"); }), 2.0f);
			myPlayBossDeathAnimation = true;
		}
	}
}

bool BossIceDragon::IsInside(const CU::Vector2f & aPoint)
{
	aPoint;
	return myCurrentAnimation->IsInside(aPoint);
}

inline bool BossIceDragon::IsTouching(const CircleCollider * aCircleCollider)
{
	if (myHealth > 1)
	{
		return myCollider.IsTouching(aCircleCollider);
	}

	return false;
}

void BossIceDragon::ShootAttack(int aAttackPattern)
{
	if (aAttackPattern == 1)
	{
		for (float a = -Tga2D::Pif / 5; a > -Tga2D::Pif; a -= Tga2D::Pif / 5)
		{
			ProjectileIcicle* projectile = ProjectileFactory::CreateIcicles({ myPosition.x - 0.05f, myPosition.y - 0.5f, myPosition.z },
				myIcicleProjectileSpeed * 0.5f,
				CU::Vector3f(std::cosf(a), std::sinf(a), 0.f));
			projectile->SetDamage(myEnemyClass.GetDMG());
			ProjectileManager::GetInstance()->EnemyShoot(projectile);
		}
	}
	else if (aAttackPattern == 2)
	{
		for (float a = -Tga2D::Pif / 10; a >= (9 * -Tga2D::Pif / 10); a -= Tga2D::Pif / 5)
		{
			ProjectileIcicle* projectile = ProjectileFactory::CreateIcicles({ myPosition.x - 0.05f, myPosition.y - 0.5f, myPosition.z },
				myIcicleProjectileSpeed,
				CU::Vector3f(std::cosf(a), std::sinf(a), 0.f));
			projectile->SetDamage(myEnemyClass.GetDMG());
			ProjectileManager::GetInstance()->EnemyShoot(projectile);
		}
	}
}

void BossIceDragon::FrozenOrbAttack()
{
	ProjectileFrozenOrb* projectile = ProjectileFactory::CreateFrozenOrb({ myPosition.x - 0.05f, myPosition.y - 0.5f, myPosition.z + 0.001f },
		myOrbProjectileSpeed,
		CU::Vector3f(0.0f, -1.0f, 0.0f));
	projectile->SetDamage(myEnemyClass.GetDMG());
	ProjectileManager::GetInstance()->EnemyShoot(projectile);
}

void BossIceDragon::FrostBreathAttack()
{
	float low = (-3.14f * 3) / 4;
	float high = -3.14f / 4;
	for (int volly = 0; volly < 3; ++volly)
	{
		float randDir = low + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high - low)));
		ProjectileFrostBreath* projectile = ProjectileFactory::CreateFrostBreath({ myPosition.x - 0.05f, myPosition.y - 0.55f, myPosition.z },
			myFrostBreathProjectileSpeed,
			CU::Vector3f(std::cos(randDir), std::sin(randDir), 0.0f));
		projectile->SetDamage(myEnemyClass.GetDMG());
		projectile->SetGlow(true, { 0.6f, 0.6f, 1.0f, 0.0f });
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
	}
}

void BossIceDragon::BlizzardAttack()
{
	float randSide = static_cast <float> (rand()) / (static_cast <float> (RAND_MAX ));
	float randHeight = myBottomBoundary + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (myTopBoundary - myBottomBoundary)));

	if(randSide < 0.5)
	{
		ProjectileIcicle* projectile = ProjectileFactory::CreateIcicles(CU::Vector3f(myRightBoundary, randHeight, 1),
			myIcicleProjectileSpeed / 1.5f,
			CU::Vector3f(-1.0f, 0.0f, 0.0f));
		projectile->SetDamage(myEnemyClass.GetDMG());
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
	}
	else
	{
		ProjectileIcicle* projectile = ProjectileFactory::CreateIcicles(CU::Vector3f(myLeftBoundary, randHeight, 1),
			myIcicleProjectileSpeed / 1.5f,
			CU::Vector3f(1.0f, 0.0f, 0.0f));
		projectile->SetDamage(myEnemyClass.GetDMG());
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
	}
}

void BossIceDragon::Movement(float aDeltaTime)
{
	if (myMoveRight)
	{
		if (myPosition.x < 0.3f)
		{
			myPosition.x += myMovementSpeed * aDeltaTime;
		}
		else
		{
			myMoveRight = false;
		}
	}
	else if (!myMoveRight)
	{
		if (myPosition.x > -0.3f)
		{
			myPosition.x -= myMovementSpeed * aDeltaTime;
		}
		else
		{
			myMoveRight = true;
		}
	}

	if (myMoveUp)
	{
		if (myPosition.y < myTopBoundary)
		{
			myPosition.y += myMovementSpeed * 2.0f * aDeltaTime;
		}
		else
		{
			myMoveUp = false;
		}
	}
	else if (!myMoveUp)
	{
		if (myPosition.y > myTopBoundary - 0.2f)
		{
			myPosition.y -= myMovementSpeed * 2.0f * aDeltaTime;
		}
		else
		{
			myMoveUp = true;
		}
	}

}

void BossIceDragon::DeathAnimation()
{	
	if (mySpawnPowerParticle)
	{
		CommonUtilities::Vector4f tint(0.3f, 0.3f, 1.0f, 0.7f);

		SpawnPowerParticle(0.02f, 0.0f, tint);
		SpawnPowerParticle(0.02f, Tga2D::Pif / 4.0f, tint);
		SpawnPowerParticle(0.02f, Tga2D::Pif / 2.0f, tint);
		SpawnPowerParticle(0.02f, 3 * Tga2D::Pif / 4.0f, tint);
		SpawnPowerParticle(0.02f, Tga2D::Pif, tint);
		SpawnPowerParticle(0.02f, -Tga2D::Pif / 4.0f, tint);
		SpawnPowerParticle(0.02f, -Tga2D::Pif / 2.0f, tint);
		SpawnPowerParticle(0.02f, -3 * Tga2D::Pif / 4.0f, tint);
	}

	if (mySpawnExplosion)
	{
		SpawnExplosion(0.15f);
	}

	if (myBleed)
	{
		SpawnBlood(12, 0.2f);
	}

	if (myHasPlayedFinalBossExplosion)
	{
		myHealth = 0.0f;
		Blackboard::GetInstance()->SetNote("icestance", "unlocked");
		Blackboard::GetInstance()->SetNote("newstance", "show");
		Blackboard::GetInstance()->SetNote("icestancefirsttime", "true");
	}	
}

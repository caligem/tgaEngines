#pragma once
#include "Enemy.h"

#include <Animation.h>

class Player;

enum class eIceDragonMoveSet
{
	attackNomral,
	attackOrb,
	attackFrostBreath,
	attackBlizzard,
};

class BossIceDragon : public Enemy
{
public:
	BossIceDragon();
	~BossIceDragon();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float aDeltaTime, Player& aPlayer) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsBoss() override { return true; }
	inline bool IsTouching(const CircleCollider* aCircleCollider) override;
	inline bool IsTouching(Projectile* aProjectile) override { return aProjectile->IsTouching(GetCollider()); }
	void DeathAnimation() override;

	void ShootAttack(int aAttackPattern);
	void FrozenOrbAttack();
	void FrostBreathAttack();
	void BlizzardAttack();
	void Movement(float aDeltaTime);

private:
	bool myIsSlowed;
	float mySlowTimer;

	Animation* myCurrentAnimation;
	Animation myAnimationIdle;
	Animation myAnimationShooting;
	Animation myAnimationFrostBreath;
	Animation myAnimationBlizzard;

	bool myIncreaseTint;
	float myCurrentTint;
	float myDeathTint;

	float myRotation;
	float myMovementSpeed;
	bool myMoveRight;
	bool myMoveUp;

	eIceDragonMoveSet myMoveSet;

	float myIcicleProjectileSpeed;
	float myOrbProjectileSpeed;
	float myFrostBreathProjectileSpeed;

	float myPrimaryAttackFireRate;
	float myFrozenOrbFireRate;
	float myFrostBreathFireRate;
	float myBlizzardFireRate;

	float myPrimaryAttackCooldown;
	float myFrozenOrbCooldown;
	float myFrostBreathCooldown;
	float myBlizzardCooldown;

	float myFrostBreathDuration;
	float myBlizzardDuration;

	int myPrimaryVollyOne;
	int myPrimaryVollyTwo;
	int myNbrOfNormalAttacks;

	float myLeftBoundary;
	float myRightBoundary;
	float myTopBoundary;
	float myBottomBoundary;
	float myCenterY;

	CommonUtilities::Vector3f myShootDirection;

	//Bopping Movement
	float myHoverDistance;
	float myBoppingSpeed;
	float myHoverTime;
};



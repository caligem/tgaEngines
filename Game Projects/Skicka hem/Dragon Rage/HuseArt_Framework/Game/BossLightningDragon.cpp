#include "stdafx.h"
#include "BossLightningDragon.h"

#include "EnemyFactory.h"
#include "ProjectileManager.h"
#include "Player.h"
#include "SFXManager.h"
#include "Blackboard.h"
#include <tga2d/math/common_math.h>
#include <Random.h>

#include "ProjectileFactory.h"
#include "ExplosionManager.h"


namespace CU = CommonUtilities;

BossLightningDragon::BossLightningDragon()
	: myRotation(Tga2D::Pif / 2)
	, myMovementSpeed(0.4f)
	, myLightningRodProjectileSpeed(0.5f)
	, myNovaProjectileSpeed(0.8f)
	, myPrimaryAttackFireRate(2.0f)
	, myVollyAttackFireRate(1.5f)
	, myNovaAttackFireRate(10.0f)
	, myArrowAttackFireRate(1.0f)
	, myPrimaryAttackVolly(0)
	, myWaitTimerAfterPrimaryAttack(3.0f)
	, myWaitTimerAfterVollyAttack(2.0f)
	, myWaitTimerBeforeNovaAttack(1.0f)
	, myFireVolly(false)
	, myNormalPattern(true)
	, myEnergyBallMove(false)
	, myShootDirectionOne(0.0f, 0.0f, 0.0f)
	, myShootDirectionTwo(0.0f, 0.0f, 0.0f)
	, myShootEnergyBall(0.0f, 0.0f, 0.0f)
	, myDragonOneDestination(0.0f, 0.0f, 0.0f)
	, myDragonTwoDestination(0.0f, 0.0f, 0.0f)
	, myDragonOneMovemntSpeed(0.0f)
	, myDragonTwoMovemntSpeed(0.0f)
	, mySetDragonOneMidPoint(true)
	, mySetDragonTwoMidPoint(true)
	, myDragonOneOriginalDistance(0.0f)
	, myDragonTwoOriginalDistance(0.0f)
	, myIncreaseTint(true)
	, myDeathTint(0.0f)
	, myCurrentTint(1.0f)
	, myLeftBoundary(-1.4f)
	, myRightBoundary(1.4f)
	, myTopBoundary(0.0f)
	, myBottomBoundary(-1.8f)
	, myCenterY(0)
	, myIsSlowed(false)
{}


BossLightningDragon::~BossLightningDragon()
{
}

void BossLightningDragon::Init(const EnemyClass& aEnemyClass, const CU::Vector3f aPosition)
{
	myPosition = aPosition;
	myPositionDragonOne = CU::Vector3f(myPosition.x - 1.0f, myPosition.y, myPosition.z + 0.001f);
	myPositionDragonTwo = CU::Vector3f(myPosition.x + 1.0f, myPosition.y, myPosition.z + 0.001f);

	myAnimationDragonOneIdle.Init("Assets/Images/Enemies/Bosses/enemy_boss_lightningdragon.dds", myPositionDragonOne);
	myAnimationDragonOneIdle.Setup(2, 2, 4);
	myColliderDragonOne.SetRadius(0.3f);
	myColliderDragonOne.SetPosition(myPositionDragonOne);

	myAnimationDragonTwoIdle.Init("Assets/Images/Enemies/Bosses/enemy_boss_lightningdragon.dds", myPositionDragonTwo);
	myAnimationDragonTwoIdle.Setup(2, 2, 4);
	myColliderDragonTwo.SetRadius(0.3f);
	myColliderDragonTwo.SetPosition(myPositionDragonTwo);

	myAnimationIdle.Init("Assets/Images/Enemies/Bosses/enemy_boss_lightningdragonrider.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myAnimationIdle.Setup(2, 2, 4);
	myAnimationShot.Init("Assets/Images/Enemies/Bosses/enemy_boss_lightningdragonrider_shot.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myAnimationShot.Setup(1, 3, 3);
	myAnimationShot.SetLoop(false);
	myAnimationShot.SetDuration(0.5f);
	myAnimationNova.Init("Assets/Images/Enemies/Bosses/enemy_boss_lightningdragonrider_boom.dds", { myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myAnimationNova.Setup(2, 3, 5);
	myAnimationNova.SetLoop(false);
	myCurrentAnimation = &myAnimationIdle;

	myEnemyClass = aEnemyClass;
	myHealth = myEnemyClass.GetHP();

	myPrimaryAttackCooldown = myPrimaryAttackFireRate;
	myVollyAttackCooldown = myVollyAttackFireRate;
	myNovaAttackCooldown = myNovaAttackFireRate;
	myArrowAttackCooldown = myVollyAttackFireRate;

	myTopBoundary += aPosition.y;
	myBottomBoundary += aPosition.y;
	myCenterY = (myTopBoundary + myBottomBoundary) / 2.0f;

	myDragonOneBeam.Init(eBeamType::Chain);	
	myDragonTwoBeam.Init(eBeamType::Chain);

	myMoveSet = eLightningDragonMoveSet::startingPosition;

	myCollider.SetRadius(0.11f);

	myHealthBar.Init(myEnemyClass.GetHP(), { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f }, FLT_MAX, { 8.0f, 1.0f });

	myBleed = true;
	mySpawnExplosion = true;
	mySpawnPowerParticle = true;
	myPlayBossDeathAnimation = false;
	myHasPlayedFinalBossExplosion = false;

	if (myEnemyClass.GetType().find(".hafd") != std::string::npos)
	{
		myDialogue.Init(myEnemyClass.GetType());
	}
}

void BossLightningDragon::Update(float aDeltaTime, Player& aPlayer)
{

	if (myHealth == 1)
	{
		DeathAnimation();
		myDeathTint += aDeltaTime / 20.0f;

		if (myCurrentAnimation->GetTint().x > 3.0f)
		{
			myIncreaseTint = false;
		}
		else if (myCurrentAnimation->GetTint().x < 0.0f)
		{
			myIncreaseTint = true;
		}

		if (myIncreaseTint)
		{
			myCurrentTint += myDeathTint;
			myCurrentAnimation->SetTint({ myCurrentTint, myCurrentTint, myCurrentTint, 1.0f });
		}
		else if (!myIncreaseTint)
		{
			myCurrentTint -= myDeathTint;
			myCurrentAnimation->SetTint({ myCurrentTint, myCurrentTint, myCurrentTint, 1.0f });
		}

		myAnimationDragonOneIdle.SetTint({ 1.0f, 1.0f, 1.0f, 1.0f - (myDeathTint * 8.0f) });
		myAnimationDragonTwoIdle.SetTint({ 1.0f, 1.0f, 1.0f, 1.0f - (myDeathTint * 8.0f) });

		myDragonOneBeam.Dectivate();
		myDragonTwoBeam.Dectivate();

		myHealthBar.Update(aDeltaTime, myHealth, { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f });
		myHealthBar.Show();
		myCountDown.Update(aDeltaTime);

		return;
	}


	myShootDirectionOne = aPlayer.GetPosition() - myPositionDragonOne;
	myShootDirectionTwo = aPlayer.GetPosition() - myPositionDragonTwo;
	myShootEnergyBall = aPlayer.GetPosition() - myPosition;

	LightningChainAttack();
	UpdateEnergyBallPos(aDeltaTime);

	myArrowAttackCooldown -= aDeltaTime;

	if (myArrowAttackCooldown < 0.0f && myMoveSet != eLightningDragonMoveSet::attackNova)
	{
		if (myCurrentAnimation != &myAnimationShot)
		{
			myCurrentAnimation = &myAnimationShot;
		}

		if (myAnimationShot.IsFinished() == true)
		{
			RiderAttack();
			myArrowAttackCooldown = myArrowAttackFireRate;
			myAnimationShot.Setup(1, 3, 3);
			myCurrentAnimation = &myAnimationIdle;
		}
	}

	if (myMoveSet == eLightningDragonMoveSet::moveSetOne
		|| myMoveSet == eLightningDragonMoveSet::moveSetTwo
		|| myMoveSet == eLightningDragonMoveSet::moveSetThree
		|| myMoveSet == eLightningDragonMoveSet::moveSetFour
		|| myMoveSet == eLightningDragonMoveSet::moveSetFive)
	{
		FlyAttack(aDeltaTime);
	}
	else if (myMoveSet == eLightningDragonMoveSet::moveTogeter || myMoveSet == eLightningDragonMoveSet::moveAway)
	{
		Movement(aDeltaTime);
	}
	else if (myMoveSet == eLightningDragonMoveSet::resetPosition)
	{
		ResetPos(aDeltaTime);
	}
	else if (myMoveSet == eLightningDragonMoveSet::spinnGettingReady || myMoveSet == eLightningDragonMoveSet::attackSpinn)
	{
		SpinnAttack(aDeltaTime);
	}

	if (myMoveSet == eLightningDragonMoveSet::startingPosition)
	{
		myPrimaryAttackCooldown -= aDeltaTime;

		if (myPrimaryAttackCooldown < 0)
		{
			if (myPrimaryAttackVolly < 3)
			{
				ShootAttack();
				myPrimaryAttackCooldown = 0.5f;
				++myPrimaryAttackVolly;
			}
			else
			{
				myWaitTimerAfterPrimaryAttack -= aDeltaTime;
				if (myWaitTimerAfterPrimaryAttack < 0)
				{
					myWaitTimerAfterPrimaryAttack = 3.0f;
					myPrimaryAttackVolly = 0;
					myPrimaryAttackCooldown = myPrimaryAttackFireRate;
					if (myNormalPattern)
					{
						myMoveSet = eLightningDragonMoveSet::moveTogeter;
						myNormalPattern = false;
					}
					else if (!myNormalPattern)
					{
						myMoveSet = eLightningDragonMoveSet::spinnGettingReady;
						myNormalPattern = true;
					}
				}
			}
		}
	}
	
	if (myFireVolly)
	{
		myVollyAttackCooldown -= aDeltaTime;

		if (myVollyAttackCooldown < 0)
		{			
			VollyAttack();
			myVollyAttackCooldown = myVollyAttackFireRate;
			myFireVolly = false;
			myMoveSet = eLightningDragonMoveSet::moveAway;
		}
	}

	if (myMoveSet == eLightningDragonMoveSet::attackNova)
	{
		myWaitTimerBeforeNovaAttack -= aDeltaTime;

		if (myWaitTimerBeforeNovaAttack < 0)
		{
			if (myCurrentAnimation != &myAnimationNova)
			{
				myCurrentAnimation = &myAnimationNova;
			}
		
			if (myAnimationNova.IsFinished() == true)
			{
				NovaAttack();
				myMoveSet = eLightningDragonMoveSet::resetPosition;
				myArrowAttackCooldown = myArrowAttackFireRate + 1.0f;
				myWaitTimerBeforeNovaAttack = 1.0f;
				myAnimationNova.Setup(2, 3, 5);
				myCurrentAnimation = &myAnimationIdle;
			}
		}		
	}
	
	DragonsMovement(aDeltaTime);

	myCurrentAnimation->Update(aDeltaTime);
	myCurrentAnimation->SetPosition({ myPosition.x, myPosition.y, myPosition.z + 0.001f });

	myDragonOneBeam.Update(aDeltaTime, false);
	myDragonTwoBeam.Update(aDeltaTime, false);
	
	DamagedFlash(*myCurrentAnimation);
	if (myFlashTimer > 0.0f)
	{
		myFlashTimer -= aDeltaTime;
	}

	myHealthBar.Update(aDeltaTime, myHealth, { 0.0f, myCenterY + 0.95f, myPosition.z - 0.02f });
	myHealthBar.Show();
	myCountDown.Update(aDeltaTime);

	myColliderDragonOne.SetPosition(myPositionDragonOne);
	myColliderDragonTwo.SetPosition(myPositionDragonTwo);
}

void BossLightningDragon::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimationDragonOneIdle);
	aRenderBuffer.Add(&myAnimationDragonTwoIdle);
	aRenderBuffer.Add(myCurrentAnimation);

	myDragonOneBeam.FillRenderBuffer(aRenderBuffer);
	myDragonTwoBeam.FillRenderBuffer(aRenderBuffer);

	myHealthBar.FillRenderBuffer(aRenderBuffer);
}

void BossLightningDragon::TakeDamage(const float aDamage, const float aDeltaTime)
{
	if (!myPlayBossDeathAnimation)
	{
		myHealth -= aDamage * aDeltaTime;
		if (myBleed)
		{
			SpawnBlood(3, 1.5f);

			if (myCanPlayTakeDamageSound)
			{
				myCanPlayTakeDamageSound = false;
				SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/BossHurt.wav", 0.5f);
				myCountDown.Set(std::bind(&BossLightningDragon::CanPlayTakeDamageSound, this), 0.5f);
			}
		}

		if (myFlashTimer < 0.8f)
		{
			myFlashTimer = 1.0f;
		}

		if (myHealth < 1.0f)
		{
			myHealth = 1.0f;
			myFlashTimer = 0.0f;
			DamagedFlash(*myCurrentAnimation);
			myBleed = true;
			myCountDown.Set(std::bind(&BossLightningDragon::FinalBossExplosion, this), 3.0f);
			myCountDown.Set(std::bind([=] { Blackboard::GetInstance()->SetNote("deathslowmotion", "true"); }), 2.0f);
			myPlayBossDeathAnimation = true;
		}
	}
}

bool BossLightningDragon::IsInside(const CU::Vector2f & aPoint)
{
	aPoint;
	return myAnimationDragonOneIdle.IsInside(aPoint) || myAnimationDragonTwoIdle.IsInside(aPoint);
}

inline bool BossLightningDragon::IsTouching(const CircleCollider * aCircleCollider)
{
	if (myHealth > 1)
	{
		if (myColliderDragonTwo.IsTouching(aCircleCollider))
		{
			return true;
		}
		else if (myColliderDragonOne.IsTouching(aCircleCollider))
		{
			return true;
		}
		else if (myCollider.IsTouching(aCircleCollider))
		{
			return true;
		}
	}
	
	return false;
}

inline bool BossLightningDragon::BeamCollision(const CircleCollider * aCircleCollider)
{
	if (myDragonOneBeam.IsTouching(aCircleCollider))
	{
		return true;
	}
	else if (myDragonTwoBeam.IsTouching(aCircleCollider))
	{
		return true;
	}

	return false;
}

void BossLightningDragon::ShootAttack()
{
	ProjectileLightningRod* projectileOne = ProjectileFactory::CreateLightningRod(myPositionDragonOne,
		myLightningRodProjectileSpeed,
		myShootDirectionOne);
	projectileOne->SetDamage(myEnemyClass.GetDMG());
	ProjectileManager::GetInstance()->EnemyShoot(projectileOne);

	ProjectileLightningRod* projectileTwo = ProjectileFactory::CreateLightningRod(myPositionDragonTwo,
		myLightningRodProjectileSpeed,
		myShootDirectionTwo);
	projectileTwo->SetDamage(myEnemyClass.GetDMG());
	ProjectileManager::GetInstance()->EnemyShoot(projectileTwo);
}

void BossLightningDragon::VollyAttack()
{
	float angel = -0.8f;
	for (int nbrOfRods = 0; nbrOfRods < 5; ++nbrOfRods)
	{
		ProjectileLightningRod* projectile = ProjectileFactory::CreateLightningRod(myPositionDragonOne,
			myLightningRodProjectileSpeed,
			CU::Vector3f(angel, -1.0f, 0.0f));
		projectile->SetDamage(myEnemyClass.GetDMG());
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
		angel += 0.4f;
	}
}

void BossLightningDragon::LightningChainAttack()
{
	myDragonOneBeam.SetStartPosition({ myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myDragonOneBeam.SetEndPosition(myPositionDragonOne);

	myDragonTwoBeam.SetStartPosition({ myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myDragonTwoBeam.SetEndPosition(myPositionDragonTwo);

	if (myDragonOneBeam.IsActive() == false &&
		myDragonTwoBeam.IsActive() == false)
	{
		myDragonOneBeam.Activate();
		myDragonTwoBeam.Activate();
	}
}

void BossLightningDragon::NovaAttack()
{
	for (float a = 0.f; a < 2.f * Tga2D::Pif; a += Tga2D::Pif /	10)
	{
		ProjectileLightningNova* projectile = ProjectileFactory::CreateLightningNova(myPosition,
			myNovaProjectileSpeed,
			CU::Vector3f(std::cos(a), std::sin(a), 0.f));
		projectile->SetDamage(myEnemyClass.GetDMG());
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
	}
}

void BossLightningDragon::RiderAttack()
{
	/*

	Why dis no work?

	for (float a = Tga2D::Pif / 8.0f; a >= -Tga2D::Pif / 8.0f; a -= Tga2D::Pif / 8.0f)
	{
	ProjectileArrow* projectile = ProjectileFactory::CreateArrow(myPosition,
	myLightningRodProjectileSpeed * 2,
	CU::Vector3f(myShootEnergyBall.x + std::cosf(a), myShootEnergyBall.y + std::sinf(a), 0.0f));
	projectile->SetDamage(myEnemyClass.GetDMG());
	ProjectileManager::GetInstance()->EnemyShoot(projectile);
	}*/

	ProjectileLightningNova* projectile = ProjectileFactory::CreateLightningNova(myPosition,
		myLightningRodProjectileSpeed * 2,
		CU::Vector3f(myShootEnergyBall.x, myShootEnergyBall.y, 0.0f));
	projectile->SetDamage(myEnemyClass.GetDMG());
	ProjectileManager::GetInstance()->EnemyShoot(projectile);

}

void BossLightningDragon::Movement(float aDeltaTime)
{
	if (myMoveSet == eLightningDragonMoveSet::moveTogeter)
	{
		if (myPositionDragonOne.x < 0 && myPositionDragonTwo.x > 0)
		{
			myPositionDragonOne.x += myMovementSpeed * aDeltaTime;
			myPositionDragonTwo.x -= myMovementSpeed * aDeltaTime;
		}
		else
		{
			myFireVolly = true;
		}
	}
	else if (myMoveSet == eLightningDragonMoveSet::moveAway)
	{
		myWaitTimerAfterVollyAttack -= aDeltaTime;

		if (myWaitTimerAfterVollyAttack < 0)
		{
			if (myPositionDragonOne.x > -1.0f && myPositionDragonTwo.x < 1.0f)
			{
				myPositionDragonOne.x -= myMovementSpeed * aDeltaTime;
				myPositionDragonTwo.x += myMovementSpeed * aDeltaTime;
			}
			else
			{
				myWaitTimerAfterVollyAttack = 2.0f;
				myMoveSet = eLightningDragonMoveSet::moveSetOne;
			}
		}
	}
}
#include <iostream>
void BossLightningDragon::FlyAttack(float aDeltaTime)
{
	if (myMoveSet == eLightningDragonMoveSet::moveSetOne ||
		myMoveSet == eLightningDragonMoveSet::moveSetTwo)
	{
		myDragonOneDestination = CU::Vector3f({ myRightBoundary - 0.4f, myBottomBoundary + 0.2f, myPositionDragonOne.z }) - myPositionDragonOne;

		if (mySetDragonOneMidPoint &&
			myMoveSet == eLightningDragonMoveSet::moveSetOne)
		{			
			myDragonOneOriginalDistance = myDragonOneDestination.Length();
			myDragonOneDestinationMidPoint = myPositionDragonOne + (myDragonOneDestination.Length() / 2.0f) * myDragonOneDestination.GetNormalized();
			mySetDragonOneMidPoint = false;
			std::cout << "x: " << myDragonOneDestinationMidPoint.x <<
						" y: " << myDragonOneDestinationMidPoint.y <<
						" z: " << myDragonOneDestinationMidPoint.z << std::endl;

		}
		else if ((myDragonOneDestinationMidPoint - myPositionDragonOne).Length() < 0.1f)
		{
			myMoveSet = eLightningDragonMoveSet::moveSetTwo;
		}

		//if (myPositionDragonOne.x < 1.0f)
		//{
		//	myPositionDragonOne.x += myMovementSpeed * aDeltaTime;
		//}

		//if (myPositionDragonOne.y > myBottomBoundary)
		//{
		//	myPositionDragonOne.y -= myMovementSpeed * aDeltaTime * 1.5f;
		//}

		//if (myPositionDragonOne.x >= 1.0f && myPositionDragonOne.y <= myBottomBoundary)
		//{
		//	myMoveSet = eLightningDragonMoveSet::moveSetTwo;
		//}
	}

	if (myMoveSet == eLightningDragonMoveSet::moveSetTwo ||
		myMoveSet == eLightningDragonMoveSet::moveSetThree)
	{
		myDragonTwoDestination = CU::Vector3f({ myLeftBoundary + 0.4f, myBottomBoundary + 0.2f, myPositionDragonTwo.z }) - myPositionDragonTwo;

		if (mySetDragonTwoMidPoint &&
			myMoveSet == eLightningDragonMoveSet::moveSetTwo)
		{
			myDragonTwoDestinationMidPoint = myPositionDragonTwo + (myDragonTwoDestination.Length() / 2.0f) * myDragonTwoDestination.GetNormalized();
			mySetDragonTwoMidPoint = false;
			std::cout << "x: " << myDragonTwoDestinationMidPoint.x <<
						" y: " << myDragonTwoDestinationMidPoint.y <<
						" z: " << myDragonTwoDestinationMidPoint.z << std::endl;
		}
		else if (myDragonTwoDestination.Length() < 0.5f)
		{
			myMoveSet = eLightningDragonMoveSet::moveSetThree;
		}




		//if (myPositionDragonTwo.x > -1.0f)
		//{
		//	myPositionDragonTwo.x -= myMovementSpeed * aDeltaTime * 1.5f;
		//}

		//if (myPositionDragonTwo.y > myBottomBoundary)
		//{
		//	myPositionDragonTwo.y -= myMovementSpeed * aDeltaTime;
		//}

		//if (myPositionDragonTwo.x <= -1.0f && myPositionDragonTwo.y <= myBottomBoundary)
		//{
		//	myMoveSet = eLightningDragonMoveSet::moveSetThree;
		//}
	}

	if (myMoveSet == eLightningDragonMoveSet::moveSetThree ||
		myMoveSet == eLightningDragonMoveSet::moveSetFour)
	{
		myDragonOneDestination = CU::Vector3f({ myLeftBoundary + 0.4f, myCenterY, myPositionDragonOne.z }) - myPositionDragonOne;

		if (mySetDragonOneMidPoint &&
			myMoveSet == eLightningDragonMoveSet::moveSetThree)
		{
			myDragonOneDestinationMidPoint = myPositionDragonOne + (myDragonOneDestination.Length() / 2.0f) * myDragonOneDestination.GetNormalized();
			mySetDragonOneMidPoint = false;
			std::cout << "x: " << myDragonOneDestinationMidPoint.x <<
						" y: " << myDragonOneDestinationMidPoint.y <<
						" z: " << myDragonOneDestinationMidPoint.z << std::endl;

		}
		else if (myDragonOneDestination.Length() < 1.5f)
		{
			myMoveSet = eLightningDragonMoveSet::moveSetFour;
		}



		//if (myPositionDragonOne.x > -1.0f)
		//{
		//	myPositionDragonOne.x -= myMovementSpeed * aDeltaTime;
		//}

		//if (myPositionDragonOne.y < myCenterY)
		//{
		//	myPositionDragonOne.y += myMovementSpeed * aDeltaTime * 1.5f;
		//}

		//if (myPositionDragonOne.x <= -1.0f && myPositionDragonOne.y >= myCenterY)
		//{
		//	myMoveSet = eLightningDragonMoveSet::moveSetFour;
		//}
	}

	if (myMoveSet == eLightningDragonMoveSet::moveSetFour)
	{
		myDragonTwoDestination = CU::Vector3f({ myRightBoundary - 0.4f, myCenterY, myPositionDragonTwo.z }) - myPositionDragonTwo;

		if (mySetDragonTwoMidPoint &&
			myMoveSet == eLightningDragonMoveSet::moveSetFour)
		{
			myDragonTwoDestinationMidPoint = myPositionDragonTwo + (myDragonTwoDestination.Length() / 2.0f) * myDragonTwoDestination.GetNormalized();
			mySetDragonTwoMidPoint = false;
			std::cout << "x: " << myDragonTwoDestinationMidPoint.x <<
						" y: " << myDragonTwoDestinationMidPoint.y <<
						" z: " << myDragonTwoDestinationMidPoint.z << std::endl;
		}
		else if (myDragonTwoDestination.Length() <= 0.1f)
		{
			myMoveSet = eLightningDragonMoveSet::moveSetFive;
		}






		//if (myPositionDragonTwo.x < 1.0f)
		//{
		//	myPositionDragonTwo.x += myMovementSpeed * aDeltaTime * 1.5f;
		//}

		//if (myPositionDragonTwo.y < myCenterY)
		//{
		//	myPositionDragonTwo.y += myMovementSpeed * aDeltaTime;
		//}

		//if (myPositionDragonTwo.x >= 1.0f && myPositionDragonTwo.y >= myCenterY)
		//{
		//	myMoveSet = eLightningDragonMoveSet::moveSetFive;
		//}
	}
	else if (myMoveSet == eLightningDragonMoveSet::moveSetFive)
	{
		myWaitTimerBeforeNovaAttack -= aDeltaTime;

		if (myWaitTimerBeforeNovaAttack < 0)
		{
			if (myPositionDragonOne.x < 0.0f)
			{
				myPositionDragonOne.x += myMovementSpeed * aDeltaTime;
			}

			if (myPositionDragonTwo.x > 0.0f)
			{
				myPositionDragonTwo.x -= myMovementSpeed * aDeltaTime;
			}

			if (myPositionDragonOne.x >= 0.0f && myPositionDragonTwo.x <= 0.0f)
			{
				myMoveSet = eLightningDragonMoveSet::attackNova;
				myWaitTimerBeforeNovaAttack = 0.5f;
			}
		}
	}
}

void BossLightningDragon::ResetPos(float aDeltaTime)
{
	if (myPositionDragonOne.x > -1.0f)
	{
		myPositionDragonOne.x -= myMovementSpeed * aDeltaTime;
	}

	if (myPositionDragonTwo.x < 1.0f)
	{
		myPositionDragonTwo.x += myMovementSpeed * aDeltaTime;
	}
	if (myPositionDragonOne.y < myTopBoundary)
	{
		myPositionDragonOne.y += myMovementSpeed * aDeltaTime;
	}

	if (myPositionDragonTwo.y < myTopBoundary)
	{
		myPositionDragonTwo.y += myMovementSpeed * aDeltaTime;
	}

	if (myPositionDragonOne.x <= -1.0f && myPositionDragonTwo.x >= 1.0f && myPositionDragonOne.y >= myTopBoundary && myPositionDragonTwo.y >= myTopBoundary)
	{
		myMoveSet = eLightningDragonMoveSet::startingPosition;
	}
}

void BossLightningDragon::SpinnAttack(float aDeltaTime)
{
	if (myMoveSet == eLightningDragonMoveSet::spinnGettingReady)
	{

		if (myPositionDragonOne.x > myLeftBoundary)
		{
			myPositionDragonOne.x -= myMovementSpeed * aDeltaTime;
		}

		if (myPositionDragonOne.y > myCenterY)
		{
			myPositionDragonOne.y -= myMovementSpeed * aDeltaTime;
		}

		if (myPositionDragonTwo.x < myRightBoundary)
		{
			myPositionDragonTwo.x += myMovementSpeed * aDeltaTime;
		}

		if (myPositionDragonTwo.y > myCenterY)
		{
			myPositionDragonTwo.y -= myMovementSpeed * aDeltaTime;
		}

		if (myPositionDragonOne.x <= myLeftBoundary
			&& myPositionDragonTwo.x >= myRightBoundary
			&& myPositionDragonOne.y <= myCenterY
			&& myPositionDragonTwo.y <= myCenterY)
		{
			myMoveSet = eLightningDragonMoveSet::attackSpinn;
		}
	}
	else if (myMoveSet == eLightningDragonMoveSet::attackSpinn)
	{
		myRotation += aDeltaTime;
		myPositionDragonOne.x -= std::cos(myRotation) * aDeltaTime * 1.5f;
		myPositionDragonOne.y -= std::sin(myRotation) * aDeltaTime;
																   	
		myPositionDragonTwo.x += std::cos(myRotation) * aDeltaTime * 1.5f;
		myPositionDragonTwo.y += std::sin(myRotation) * aDeltaTime;
				

		if (myRotation > ((4 * Tga2D::Pif) + (Tga2D::Pif / 2)))
		{
			myRotation = Tga2D::Pif / 2;
			myMoveSet = eLightningDragonMoveSet::resetPosition;
			myPrimaryAttackCooldown = myPrimaryAttackFireRate;
		}		
	}
}

void BossLightningDragon::UpdateEnergyBallPos(float aDeltaTime)
{
	float length = (myPositionDragonOne - myPositionDragonTwo).Length();
	CU::Vector3f dir = { myPositionDragonTwo.x - myPositionDragonOne.x, myPositionDragonTwo.y - myPositionDragonOne.y , 0.0f };
	dir.Normalize();

	CU::Vector3f centerPos = myPositionDragonOne + ((length / 2) * dir);

	if (((myPosition - centerPos).Length() > 0.2f) && !myEnergyBallMove)
	{
		myEnergyBallMove = true;
	}

	if (myEnergyBallMove)
	{
		CU::Vector3f moveBallDir = { centerPos.x - myPosition.x, centerPos.y - myPosition.y , 0.0f };
		myPosition += moveBallDir * aDeltaTime;

		if ((myPosition - centerPos).Length() < 0.01f)
		{
			myEnergyBallMove = false;
		}
	}
}

void BossLightningDragon::DragonsMovement(float aDeltaTime)
{
	/*std::cout << (myDragonOneDestinationMidPoint - myPositionDragonOne).Length() << std::endl;
	std::cout << (myDragonTwoDestinationMidPoint - myPositionDragonTwo).Length() << std::endl;*/

	//std::cout << "X-speed: " << std::abs(std::cosf(myDragonOneMovemntSpeed)) << std::endl;
	//std::cout << "Y-speed: " << std::abs(std::sinf(myDragonOneMovemntSpeed)) << std::endl;

	std::cout << "speed: " << myDragonOneMovemntSpeed << std::endl;

	if ((myDragonOneDestinationMidPoint - myPositionDragonOne).Length() > 0.3f)
	{
		myDragonOneMovemntSpeed = myMovementSpeed / ((myDragonOneDestinationMidPoint - myPositionDragonOne).Length());
		//myDragonOneMovemntSpeed = (2.0f * Tga2D::Pif) * (myDragonOneDestination.Length() / myDragonOneOriginalDistance);
	}

	if (myDragonOneDestination.Length() > 0.1f)
	{
		myPositionDragonOne += aDeltaTime * myDragonOneDestination.GetNormalized() * myDragonOneMovemntSpeed;

		/*myPositionDragonOne += aDeltaTime * CU::Vector3f(myDragonOneDestination.GetNormalized().x * std::abs(std::cosf(myDragonOneMovemntSpeed)),
														 myDragonOneDestination.GetNormalized().y * std::abs(std::sinf(myDragonOneMovemntSpeed)),
														 myDragonOneDestination.GetNormalized().z);*/
	}
	else if (!mySetDragonOneMidPoint)
	{
		myDragonOneDestination = { 0.0f, 0.0f, 0.0f };
		myDragonOneDestinationMidPoint = { 0.0f, 0.0f, 0.0f };
		mySetDragonOneMidPoint = true;
	}



	if ((myDragonTwoDestinationMidPoint - myPositionDragonTwo).Length() > 0.3f)
	{
		myDragonTwoMovemntSpeed = myMovementSpeed / ((myDragonTwoDestinationMidPoint - myPositionDragonTwo).Length());
	}
	
	if (myDragonTwoDestination.Length() > 0.1f)
	{
		myPositionDragonTwo += aDeltaTime * myDragonTwoDestination.GetNormalized() * myDragonTwoMovemntSpeed;
	}
	else if(!mySetDragonTwoMidPoint)
	{
		myDragonTwoDestination = { 0.0f, 0.0f, 0.0f };
		myDragonTwoDestinationMidPoint = { 0.0f, 0.0f, 0.0f };
		mySetDragonTwoMidPoint = true;
	}




	
	
	myAnimationDragonOneIdle.Update(aDeltaTime);
	myAnimationDragonOneIdle.SetPosition(myPositionDragonOne);

	myAnimationDragonTwoIdle.Update(aDeltaTime);
	myAnimationDragonTwoIdle.SetPosition(myPositionDragonTwo);
}

void BossLightningDragon::DeathAnimation()
{

	if (mySpawnPowerParticle)
	{
		CommonUtilities::Vector4f tint(1.f, 1.f, 0.f, 1.f);
		SpawnPowerParticle(0.02f, 0.0f, tint);
		SpawnPowerParticle(0.02f, Tga2D::Pif / 4.0f, tint);
		SpawnPowerParticle(0.02f, Tga2D::Pif / 2.0f, tint);
		SpawnPowerParticle(0.02f, 3 * Tga2D::Pif / 4.0f, tint);
		SpawnPowerParticle(0.02f, Tga2D::Pif, tint);
		SpawnPowerParticle(0.02f, -Tga2D::Pif / 4.0f, tint);
		SpawnPowerParticle(0.02f, -Tga2D::Pif / 2.0f, tint);
		SpawnPowerParticle(0.02f, -3 * Tga2D::Pif / 4.0f, tint);
	}

	if (mySpawnExplosion)
	{
		SpawnExplosion(0.15f);
	}

	if (myBleed)
	{
		SpawnBlood(12, 0.2f);
	}

	if (myHasPlayedFinalBossExplosion)
	{
		myHealth = 0.0f;
		Blackboard::GetInstance()->SetNote("lightningstance", "unlocked");
		Blackboard::GetInstance()->SetNote("newstance", "show");
		Blackboard::GetInstance()->SetNote("lightningstancefirsttime", "true");
	}	


}


void BossLightningDragon::RenderDebug(const CommonUtilities::Camera & aCamera)
{
	myCollider.RenderDebug(aCamera);
	myColliderDragonOne.RenderDebug(aCamera);
	myColliderDragonTwo.RenderDebug(aCamera);
}
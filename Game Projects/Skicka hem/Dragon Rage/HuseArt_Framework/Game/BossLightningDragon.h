#pragma once
#include "Enemy.h"
#include "Beam.h"
#include <Animation.h>

class Player;

enum class eLightningDragonMoveSet
{
	startingPosition,
	moveTogeter,
	moveAway,
	moveSetOne,
	moveSetTwo,
	moveSetThree,
	moveSetFour,
	moveSetFive,
	attackNova,
	resetPosition,
	spinnGettingReady,
	attackSpinn,
};

class BossLightningDragon : public Enemy
{
public:
	BossLightningDragon();
	~BossLightningDragon();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float aDeltaTime, Player& aPlayer) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsBoss() override { return true; }

	inline bool IsTouching(const CircleCollider* aCircleCollider) override;
	inline bool BeamCollision(const CircleCollider* aCircleCollider) override;
	inline bool IsTouching(Projectile* aProjectile) override { return aProjectile->IsTouching(GetCollider()); }
	void DeathAnimation() override;
	void RenderDebug(const CommonUtilities::Camera& aCamera) override;

	void ShootAttack();
	void VollyAttack();
	void LightningChainAttack();
	void NovaAttack();
	void RiderAttack();

	void Movement(float aDeltaTime);
	void FlyAttack(float aDeltaTime);
	void ResetPos(float aDeltaTime);

	void SpinnAttack(float aDeltaTime);

	void UpdateEnergyBallPos(float aDeltaTime);

	void DragonsMovement(float aDeltaTime);

private:
	bool myIsSlowed;
	float mySlowTimer;

	Animation* myCurrentAnimation;
	Animation myAnimationIdle;
	Animation myAnimationShot;
	Animation myAnimationNova;
	Animation myAnimationDragonOneIdle;
	Animation myAnimationDragonTwoIdle;

	bool myIncreaseTint;
	float myCurrentTint;
	float myDeathTint;

	CommonUtilities::Vector3f myPositionDragonOne;
	CommonUtilities::Vector3f myPositionDragonTwo;

	Beam myDragonOneBeam;
	Beam myDragonTwoBeam;

	CircleCollider myColliderDragonOne;
	CircleCollider myColliderDragonTwo;

	eLightningDragonMoveSet myMoveSet;

	float myRotation;
	float myMovementSpeed;

	float myLightningRodProjectileSpeed;
	float myNovaProjectileSpeed;

	float myPrimaryAttackFireRate;
	float myVollyAttackFireRate;
	float myNovaAttackFireRate;
	float myArrowAttackFireRate;

	float myPrimaryAttackCooldown;
	float myVollyAttackCooldown;
	float myNovaAttackCooldown;
	float myArrowAttackCooldown;

	float myWaitTimerAfterPrimaryAttack;
	float myWaitTimerAfterVollyAttack;
	float myWaitTimerBeforeNovaAttack;
	int myPrimaryAttackVolly;

	bool myFireVolly;
	bool myNormalPattern;
	bool myEnergyBallMove;

	float myLeftBoundary;
	float myRightBoundary;
	float myTopBoundary;
	float myBottomBoundary;
	float myCenterY;

	CommonUtilities::Vector3f myShootDirectionOne;
	CommonUtilities::Vector3f myShootDirectionTwo;
	CommonUtilities::Vector3f myShootEnergyBall;

	float myDragonOneMovemntSpeed;
	float myDragonTwoMovemntSpeed;

	CommonUtilities::Vector3f myDragonOneDestination;
	CommonUtilities::Vector3f myDragonOneDestinationMidPoint;
	float myDragonOneOriginalDistance;
	bool mySetDragonOneMidPoint;

	CommonUtilities::Vector3f myDragonTwoDestination;
	CommonUtilities::Vector3f myDragonTwoDestinationMidPoint;
	float myDragonTwoOriginalDistance;
	bool mySetDragonTwoMidPoint;
};



#include "stdafx.h"
#include "Controls.h"

namespace CU = CommonUtilities;


Controls::Controls()
{
	myKeyboardMoveUp = CU::Key_Up;
	myKeyboardMoveLeft = CU::Key_Left;
	myKeyboardMoveDown = CU::Key_Down;
	myKeyboardMoveRight = CU::Key_Right;

	myKeyboardPrimaryFire = CU::Key_Space;
	myKeyboardSecondaryFire = CU::Key_R;

	myKeyboardFirstDragonForm = CU::Key_Q;
	myKeyboardSecondDragonForm = CU::Key_W;
	myKeyboardThirdDragonForm = CU::Key_E;

	myXboxMoveStick = CU::XStick_Left;

	myXboxPrimaryFire = CU::XAxis_RT;
	myXboxSecondaryFire = CU::XAxis_LT;

	myXboxFirstDragonForm = CU::XButton_B;
	myXboxSecondDragonForm = CU::XButton_X;
	myXboxThirdDragonForm = CU::XButton_Y;
}


Controls::~Controls()
{
}

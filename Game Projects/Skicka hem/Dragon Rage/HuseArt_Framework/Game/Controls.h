#pragma once
#include "InputManager.h"
#include "XBOXController.h"

class Controls
{

public:
	Controls();
	~Controls();
	
	//XInput
	CommonUtilities::XStick GetXboxMoveStick() { return myXboxMoveStick; }

	CommonUtilities::XAxis GetXboxPrimaryFire() { return myXboxPrimaryFire; }
	void SetXboxPrimaryFire(CommonUtilities::XAxis aXAxis) { myXboxPrimaryFire = aXAxis; }
	CommonUtilities::XAxis GetXboxSecondaryFire() { return myXboxSecondaryFire; }
	void SetXboxSecondaryFire(CommonUtilities::XAxis aXAxis) { myXboxSecondaryFire = aXAxis; }

	CommonUtilities::XButton GetXboxFirstDragonForm() { return myXboxFirstDragonForm; }
	CommonUtilities::XButton GetXboxSecondDragonForm() { return myXboxSecondDragonForm; }
	CommonUtilities::XButton GetXboxThirdDragonForm() { return myXboxThirdDragonForm; }
	void SetXboxFirstDragonForm(CommonUtilities::XButton aButton) { myXboxFirstDragonForm = aButton; }
	void SetXboxSecondDragonForm(CommonUtilities::XButton aButton) { myXboxSecondDragonForm = aButton; }
	void SetXboxThirdDragonForm(CommonUtilities::XButton aButton) { myXboxThirdDragonForm = aButton; }

	//Keyboard
	CommonUtilities::Key GetKeyboardMoveUp() { return myKeyboardMoveUp; }
	CommonUtilities::Key GetKeyboardMoveLeft() { return myKeyboardMoveLeft; }
	CommonUtilities::Key GetKeyboardMoveDown() { return myKeyboardMoveDown; }
	CommonUtilities::Key GetKeyboardMoveRight() { return myKeyboardMoveRight; }
	void SetKeyboardMoveUp(CommonUtilities::Key aKey) { myKeyboardMoveUp = aKey; }
	void SetKeyboardMoveLeft(CommonUtilities::Key aKey) { myKeyboardMoveLeft = aKey; }
	void SetKeyboardMoveDown(CommonUtilities::Key aKey) { myKeyboardMoveDown = aKey; }
	void SetKeyboardMoveRight(CommonUtilities::Key aKey) { myKeyboardMoveRight = aKey; }

	CommonUtilities::Key GetKeyboardPrimaryFire() { return myKeyboardPrimaryFire; }
	void SetKeyboardPrimaryFire(CommonUtilities::Key aKey) { myKeyboardPrimaryFire = aKey; }
	CommonUtilities::Key GetKeyboardSecondaryFire() { return myKeyboardSecondaryFire; }
	void SetKeyboardSecondaryFire(CommonUtilities::Key aKey) { myKeyboardSecondaryFire = aKey; }

	CommonUtilities::Key GetKeyboardFirstDragonForm() { return myKeyboardFirstDragonForm; }
	CommonUtilities::Key GetKeyboardSecondDragonForm() { return myKeyboardSecondDragonForm; }
	CommonUtilities::Key GetKeyboardThirdDragonForm() { return myKeyboardThirdDragonForm; }
	void SetKeyboardFirstDragonForm(CommonUtilities::Key aKey) { myKeyboardFirstDragonForm = aKey; }
	void SetKeyboardSecondDragonForm(CommonUtilities::Key aKey) { myKeyboardSecondDragonForm = aKey; }
	void SetKeyboardThirdDragonForm(CommonUtilities::Key aKey) { myKeyboardThirdDragonForm = aKey; }

private:
	//XInput
	CommonUtilities::XStick myXboxMoveStick;

	CommonUtilities::XAxis myXboxPrimaryFire;
	CommonUtilities::XAxis myXboxSecondaryFire;

	CommonUtilities::XButton myXboxFirstDragonForm;
	CommonUtilities::XButton myXboxSecondDragonForm;
	CommonUtilities::XButton myXboxThirdDragonForm;

	//Keyboard
	CommonUtilities::Key myKeyboardMoveUp;
	CommonUtilities::Key myKeyboardMoveLeft;
	CommonUtilities::Key myKeyboardMoveDown;
	CommonUtilities::Key myKeyboardMoveRight;

	CommonUtilities::Key myKeyboardPrimaryFire;
	CommonUtilities::Key myKeyboardSecondaryFire;

	CommonUtilities::Key myKeyboardFirstDragonForm;
	CommonUtilities::Key myKeyboardSecondDragonForm;
	CommonUtilities::Key myKeyboardThirdDragonForm;
};
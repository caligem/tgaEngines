#include "stdafx.h"
#include "CountDown.h"

CountDown::CountDown()
{
	myFunctions.Init(2);
}

CountDown::~CountDown()
{}

void CountDown::Update(float aDeltaTime)
{
	for (unsigned short i = myFunctions.Size(); i > 0; --i)
	{
		FunctionTimerCombo& ftc = myFunctions[i-1];

		ftc.myTimer -= aDeltaTime;

		if (ftc.myTimer <= 0.f && ftc.myFunction)
		{
			ftc.myFunction();

			myFunctions.RemoveCyclicAtIndex(i - 1);
		}
	}
}

void CountDown::Set(std::function<void()> aFunction, float aInterval)
{
	FunctionTimerCombo ftc;
	ftc.myFunction = aFunction;
	ftc.myTimer = aInterval;
	myFunctions.Add(ftc);
}

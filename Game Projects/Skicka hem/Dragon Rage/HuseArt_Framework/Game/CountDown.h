#pragma once

#include <functional>

#include <GrowingArray.h>

class CountDown
{
public:
	CountDown();
	~CountDown();

	void Update(float aDeltaTime);

	void Set(std::function<void()> aFunction, float aInterval);

private:
	struct FunctionTimerCombo
	{
		float myTimer;
		std::function<void()> myFunction;
	};

	CommonUtilities::GrowingArray<FunctionTimerCombo> myFunctions;

};


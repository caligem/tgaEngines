#include "stdafx.h"
#include "Dialogue.h"

#include <Parser/json.hpp>
#include <FileDialog.h>

Dialogue::Dialogue()
{
	myIsValid = false;
	myCurrentLine = 0;
}

Dialogue::~Dialogue()
{}

void Dialogue::Init(const std::string & aPath)
{
	std::string data = CommonUtilities::OpenFile(aPath);
	nlohmann::json dialogueData = nlohmann::json::parse(data.c_str());

	if (dialogueData.find("lines") == dialogueData.end())
	{
		myIsValid = false;
		return;
	}

	myLines.Init(8);
	for (auto line : dialogueData["lines"])
	{
		DialogueLine dl;
		dl.myIsPlayer = line["player"].get<bool>();
		for (auto l : line["lines"])
		{
			dl.myText += l.get<std::string>() + "\n";
		}
		myLines.Add(dl);
	}

	myIsValid = true;
}

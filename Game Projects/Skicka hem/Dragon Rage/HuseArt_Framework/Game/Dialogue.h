#pragma once

#include <string>
#include <GrowingArray.h>

class Dialogue;

struct DialogueLine
{
public:
	inline bool IsPlayer() const { return myIsPlayer; }
	const std::string& GetText() const { return myText; }
private:
	friend Dialogue;
	bool myIsPlayer;
	std::string myText;
};

class Dialogue
{
public:
	Dialogue();
	~Dialogue();

	void Init(const std::string& aPath);

	inline bool IsValid() const { return myIsValid; }
	inline bool IsEnded() const { return myCurrentLine == myLines.Size(); }

	inline const DialogueLine& GetNext() { return myLines[myCurrentLine++]; }

private:
	bool myIsValid;
	unsigned short myCurrentLine;

	CommonUtilities::GrowingArray<DialogueLine> myLines;

};


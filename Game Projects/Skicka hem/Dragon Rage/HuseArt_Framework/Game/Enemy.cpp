#include "stdafx.h"
#include "Enemy.h"

#include "ExplosionManager.h"
#include "EnemyFactory.h"
#include "SFXManager.h"
#include "Blackboard.h"

#include <Random.h>
#include <Mathf.h>

namespace CU = CommonUtilities;

Enemy::Enemy()
{
	myAwake = false;
	myScoreFactor = 1.f;
	myCollider.SetRadius(0.1f);
	myFlashTimer = 0.0f;
	myHasPlayedFinalBossExplosion = false;
}

void Enemy::DeathAnimation()
{
	if (myElement == myLastHitElement)
	{
		int max = static_cast<int>(10.f * myScoreFactor);
		for (int i = 0; i < max; ++i)
		{
			EnemyFactory::CreateEnemyParticle(myEnemyClass, myPosition, myElement);
		}
	}

	

	Animation* newExplosion = new Animation("Assets/Images/Projectiles/explosionTint.dds");
	newExplosion->Setup(2, 3, 6);
	newExplosion->SetDuration(0.75f);
	newExplosion->SetLoop(true);
	newExplosion->SetPosition(GetPosition() - CommonUtilities::Vector3f(0.f, 0.f, 0.025f) );
	newExplosion->SetScale({ 1.f, 1.f });

	if (myLastHitElement == eElement::Fire)
	{
		newExplosion->SetTint({ 1.f, 0.f, 0.f, 1.f });
	}
	else if (myLastHitElement == eElement::Ice)
	{
		newExplosion->SetTint({ 0.f, 0.5f, 1.f, 1.f });
	}
	else if (myLastHitElement == eElement::Lightning)
	{
		newExplosion->SetTint({ 1.0f, 0.9f, 0.f, 1.f });

	}

	ExplosionManager::GetInstance()->AddExplosion(newExplosion);

	SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/Splat.wav", 0.4f).SetPitch(0.8f+0.4f*CommonUtilities::Random());
}

void Enemy::DamagedFlash(Animation& aAnimation)
{
	if (myFlashTimer >= 0.9f)
	{
		aAnimation.SetTint({ 255.0f, 255.0f, 255.0f, 1.0f });
	}
	else
	{
		aAnimation.SetTint({ 1.0f, 1.0f, 1.0f, 1.0f });
	}
}
void Enemy::SpawnBlood(int aSection, float aCooldown)
{
	float minAngle = 0.0f;
	float maxAngle = (2.0f * Tga2D::Pif) / aSection;
	float sprayDir = CU::Random() * (2.0f * Tga2D::Pif);

	minAngle += sprayDir;
	maxAngle += sprayDir;

	float xOffset = std::cosf(minAngle) + CU::Random() * (std::cosf(maxAngle) - std::cosf(minAngle));
	float yOffset = std::sinf(minAngle) + CU::Random() * (std::sinf(maxAngle) - std::sinf(minAngle));

	for (int index = 0; index < 50; ++index)
	{
		CommonUtilities::Vector3f spawnPos(myPosition.x + (xOffset / 5.0f), myPosition.y - 0.2f + (yOffset / 5.0f), myPosition.z - 0.001f);
		CommonUtilities::Vector4f tint(1.f, 1.f, 1.f, 1.f);
		EnemyFactory::CreateBossBlood(myEnemyClass, spawnPos, tint, minAngle, maxAngle, 1.2f);
	}

	myBleed = false;
	myCountDown.Set([=] { myBleed = true; }, aCooldown);
}

void Enemy::SpawnExplosion(float aCooldown)
{
	Animation* newExplosion = new Animation("Assets/Images/Projectiles/explosion.dds");
	newExplosion->Setup(2, 3, 6);
	newExplosion->SetDuration(0.3f + CU::Random() * (1.5f - 0.3f));
	float scale = 1.0f + CU::Random() * (3.0f - 1.0f);
	newExplosion->SetScale({ scale, scale });
	float xPos = (myPosition.x - 0.3f) + CU::Random() * ((myPosition.x + 0.3f) - (myPosition.x - 0.3f));
	float yPos = (myPosition.y - 0.3f) + CU::Random() * ((myPosition.y + 0.3f) - (myPosition.y - 0.3f));
	newExplosion->SetPosition({ xPos, yPos, myPosition.z - 0.01f });
	ExplosionManager::GetInstance()->AddExplosion(newExplosion);
	SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/Explosion.wav", 0.4f);
	mySpawnExplosion = false;
	myCountDown.Set([=] { mySpawnExplosion = true; }, aCooldown);
}

void Enemy::SpawnPowerParticle(float aCooldown, float aAngle, CommonUtilities::Vector4f& aTint)
{
	CommonUtilities::Vector3f spawnPos(myPosition.x, myPosition.y, myPosition.z - 0.001f);
	EnemyFactory::CreateBossAbsorbParticle(myEnemyClass, spawnPos, aTint, aAngle);

	mySpawnPowerParticle = false;
	myCountDown.Set([=] { mySpawnPowerParticle = true; }, aCooldown);
}

void Enemy::FinalBossExplosion()
{
	if (myHasPlayedFinalBossExplosion)
	{
		return;
	}
	myHasPlayedFinalBossExplosion = true;

	for (int index = 0; index < 10; ++index)
	{
		Animation* newExplosion = new Animation("Assets/Images/Projectiles/explosion.dds");
		newExplosion->Setup(2, 3, 6);
		newExplosion->SetDuration(0.3f + CU::Random() * (1.5f - 0.3f));
		float scale = 1.0f + CU::Random() * (3.0f - 1.0f);
		newExplosion->SetScale({ scale, scale });
		float xPos = (myPosition.x - 0.3f) + CU::Random() * ((myPosition.x + 0.3f) - (myPosition.x - 0.3f));
		float yPos = (myPosition.y - 0.3f) + CU::Random() * ((myPosition.y + 0.3f) - (myPosition.y - 0.3f));
		newExplosion->SetPosition({ xPos, yPos, myPosition.z - 0.01f });
		ExplosionManager::GetInstance()->AddExplosion(newExplosion);
	}

	for (int index = 0; index < 250; ++index)
	{
		CommonUtilities::Vector3f spawnPos(myPosition.x, myPosition.y, myPosition.z - 0.001f);
		CommonUtilities::Vector4f tint(1.f, 0.f, 0.f, 1.f);
		if (index % 15 == 0)
		{
			EnemyFactory::CreateBossBlood(myEnemyClass, spawnPos, tint, 0.0f, 2.0f * Tga2D::Pif, 2.0f, 2.0f);
		}
		else
		{
			EnemyFactory::CreateBossBlood(myEnemyClass, spawnPos, tint, 0.0f, 2.0f * Tga2D::Pif, 2.0f);
		}
	}

	for (int index = 0; index < 200; ++index)
	{
		EnemyFactory::CreateEnemyParticle(myEnemyClass, myPosition, myElement, true);
	}
}

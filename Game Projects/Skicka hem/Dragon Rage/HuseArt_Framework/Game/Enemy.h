#pragma once

namespace CommonUtilities
{
	class Camera;
}
#include <Camera.h>

class Drawable;
class Player;
class Highscore;

#include "Beam.h"

#include "Actor.h"

#include "Sprite.h"
#include "EnemyClass.h"
#include "HealthBar.h"

#include <Vector.h>
#include <GrowingArray.h>
#include <CircleCollider.h>

#include "Projectile.h"
#include "CountDown.h"

#include "Element.h"

#include "Dialogue.h"

class Enemy : public Actor
{
public:
	Enemy();
	~Enemy() = default;
		
	virtual void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) = 0;
	virtual void Update(float, Player&) = 0;
	virtual void UpdateCollider() { myCollider.SetPosition(myPosition); }
	virtual void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) = 0;
	virtual void FillGuiBuffer(CommonUtilities::GrowingArray<Drawable*>&) {};

	virtual void TakeDamage(const float aDamage, const float aDeltaTime = 1) = 0;
	virtual bool IsInside(const CommonUtilities::Vector2f& aPoint) = 0;
	virtual bool IsBoss() = 0;
	virtual inline bool IsDead(Highscore&) { return myHealth <= 0.f; }
	virtual inline void Kill() { myHealth = 0.f; }
	virtual void DeathAnimation();
	virtual void DamagedFlash(Animation& aAnimation);
	virtual void SpawnBlood(int aSection, float aCooldown);
	virtual void SpawnExplosion(float aCooldown);
	virtual void SpawnPowerParticle(float aCooldown, float aAngle, CommonUtilities::Vector4f& aTint);
	virtual void FinalBossExplosion();
	virtual inline const float GetDamage() { return myEnemyClass.GetDMG(); }
	virtual inline const float GetHP() const { return myEnemyClass.GetHP(); }
	inline bool IsAwake() { return myAwake; }
	inline void SetScoreFactor(float aScoreFactor) { myScoreFactor = aScoreFactor; }
	inline const float GetScoreFactor() const { return myScoreFactor; }
	inline void Wake() { myAwake = true; }

	inline void SetElement(const eElement& aElement) { myElement = aElement; }
	inline void SetTakenElement(const eElement& aElement) { myLastHitElement = aElement; }
	
	virtual inline bool IsTouching(const CircleCollider* aCircleCollider) { return myCollider.IsTouching(aCircleCollider); }
	virtual inline bool IsTouching(Projectile* aProjectile) { return aProjectile->IsTouching(GetCollider()); }
	virtual inline bool IsTouching(Beam& aBeam) { return aBeam.IsTouching(GetCollider()); }
	inline const CircleCollider* GetCollider() { return &myCollider; }

	virtual inline bool BeamCollision(const CircleCollider* aCircleCollider) { aCircleCollider; return false; }

	virtual void RenderDebug(const CommonUtilities::Camera& aCamera) { myCollider.RenderDebug(aCamera); }

	const std::string& GetTypeName() const { return myEnemyClass.GetName(); }

	inline Dialogue& GetDialogue() { return myDialogue; }

	inline eElement GetElement() const { return myElement; }

protected:

	void CanPlayTakeDamageSound() { myCanPlayTakeDamageSound = true; }

	CircleCollider myCollider;

	bool myAwake;
	float myScoreFactor;

	float myHealth;
	EnemyClass myEnemyClass;
	HealthBar myHealthBar;
	
	float myFlashTimer;

	eElement myElement;
	eElement myLastHitElement;

	//CountDown myTakeDamageSoundCountDown;
	bool myCanPlayTakeDamageSound;

	Dialogue myDialogue;

	CountDown myCountDown;
	bool myBleed;
	bool mySpawnExplosion;
	bool mySpawnPowerParticle;

	bool myPlayBossDeathAnimation;
	bool myHasPlayedFinalBossExplosion;
};


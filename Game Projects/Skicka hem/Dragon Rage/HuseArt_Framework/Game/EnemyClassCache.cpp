#include "stdafx.h"
#include "EnemyClassCache.h"

#include <fstream>
#include <algorithm>
#include <string>

#include <Parser/json.hpp>

#include <DL_Debug.h>

EnemyClassCache::EnemyClassCache()
{}

EnemyClassCache::~EnemyClassCache()
{}

const EnemyClass& EnemyClassCache::GetClass(const std::string & aClassPath)
{
	if (myMap.find(aClassPath) == myMap.end())
	{
		LoadClass(aClassPath);
	}

	return myMap[aClassPath];
}

void EnemyClassCache::LoadClass(const std::string & aClassPath)
{
	std::ifstream i(aClassPath);
	if (i.fail() || i.bad())
	{
		DL_DEBUG("Can't open filestream!");
		return;
	}

	nlohmann::json j;
	i >> j;

	EnemyClass ec;

	if (j.find("damage") != j.end())
	{
		ec.SetDMG(j["damage"].get<float>());
	}
	if (j.find("hp") != j.end())
	{
		ec.SetHP(j["hp"].get<float>());
	}
	if (j.find("name") != j.end())
	{
		ec.SetName(j["name"].get<std::string>());
	}
	if (j.find("type") != j.end())
	{
		ec.SetType(j["type"].get<std::string>());
	}
	if (j.find("rateOfFire") != j.end())
	{
		ec.SetROF(j["rateOfFire"].get<float>());
	}
	if (j.find("projectileSpeed") != j.end())
	{
		ec.SetProjectileSpeed(j["projectileSpeed"].get<float>());
	}
	if (j.find("movementSpeed") != j.end())
	{
		ec.SetMovementSpeed(j["movementSpeed"].get<float>());
	}

	myMap[aClassPath] = ec;
}

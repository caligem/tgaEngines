#pragma once

#include <map>
#include <string>
#include "EnemyClass.h"

class EnemyClassCache
{
public:
	EnemyClassCache();
	~EnemyClassCache();

	const EnemyClass& GetClass(const std::string& aClassPath);

private:
	void LoadClass(const std::string& aClassPath);

	std::map<std::string, EnemyClass> myMap;

};


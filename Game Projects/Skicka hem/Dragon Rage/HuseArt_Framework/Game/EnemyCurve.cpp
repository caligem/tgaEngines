#include "stdafx.h"
#include "EnemyCurve.h"

#include "EnemyFactory.h"
#include <Macros.h>

#include "Highscore.h"

EnemyCurve::EnemyCurve(const int aAmount, const float aInterval, const float aSpeed, EnemyClassCache & aEnemyClassCache)
	: myEnemyClassCache(aEnemyClassCache)
	, myAmountLeft(aAmount)
	, myAmountStart(aAmount)
	, myInterval(aInterval)
	, mySpeed(aSpeed)
	, myTimer(0.f)
	, myLastHitEnemy(nullptr)
{}

EnemyCurve::~EnemyCurve()
{
	for (EnemyOnCurve& eoc : myEnemies)
	{
		SAFE_DELETE(eoc.myEnemy);
	}
}

void EnemyCurve::Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition)
{
	myEnemies.Init(16);
	myPoints.Init(8);
	myEnemyClass = aEnemyClass;
	myPosition = aPosition;
}

void EnemyCurve::Update(float aDeltaTime, Player& aPlayer)
{
	if (myTimer <= 0.f && myAmountLeft > 0)
	{
		SpawnEnemy();
	}
	else
	{
		myTimer -= aDeltaTime;
	}


	for (unsigned short i = myEnemies.Size(); i > 0; --i)
	{
		EnemyOnCurve& eoc = myEnemies[i - 1];

		eoc.myEnemy->Update(aDeltaTime, aPlayer);

		eoc.myTime += (mySpeed / myCurve.GetLength()) * aDeltaTime;
		eoc.myEnemy->SetPosition(myCurve.GetPoint(eoc.myTime));

		if (eoc.myTime >= 1.f)
		{
			eoc.myEnemy->Kill();
			eoc.myEnemy->SetScoreFactor(0.f);
		}
	}
}

void EnemyCurve::UpdateCollider()
{
	for (EnemyOnCurve& eoc : myEnemies)
	{
		eoc.myEnemy->UpdateCollider();
	}
}

void EnemyCurve::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	for (EnemyOnCurve& eoc : myEnemies)
	{
		eoc.myEnemy->FillRenderBuffer(aRenderBuffer);
	}
}

bool EnemyCurve::IsTouching(Projectile * aProjectile)
{
	for (EnemyOnCurve& eoc : myEnemies)
	{
		Enemy* enemy = eoc.myEnemy;
		if (enemy->IsTouching(aProjectile))
		{
			enemy->SetTakenElement(aProjectile->GetElement());
			enemy->TakeDamage(aProjectile->GetDamage());
			return true;
		}
	}
	return false;
}

bool EnemyCurve::IsTouching(const CircleCollider * aCircleCollider)
{
	myLastHitEnemy = nullptr;

	for (EnemyOnCurve& eoc : myEnemies)
	{
		Enemy* enemy = eoc.myEnemy;
		if (enemy->IsTouching(aCircleCollider))
		{
			myLastHitEnemy = enemy;
			return true;
		}
	}
	return false;
}

bool EnemyCurve::IsTouching(Beam & aBeam)
{
	myLastHitEnemy = nullptr;

	for (EnemyOnCurve& eoc : myEnemies)
	{
		Enemy* enemy = eoc.myEnemy;
		if (enemy->IsTouching(aBeam))
		{
			enemy->SetTakenElement(aBeam.GetElement());
			myLastHitEnemy = enemy;
			return true;
		}
	}
	return false;
}

void EnemyCurve::AddPoint(const CommonUtilities::Vector3f & aPoint)
{
	myPoints.Add(myPosition + aPoint);
}
void EnemyCurve::PopPoint()
{
	myCurve.PopPoint();
}
void EnemyCurve::BuildCurve()
{
	myCurve.ClearPoints();
	for (unsigned short i = 0; i < myPoints.Size(); ++i)
	{
		if(i == 0 || i == myPoints.Size()-1)
		{
			myCurve.AddPoint(myPoints[i]);
		}
		else
		{
			CommonUtilities::Vector3f middle = (myPoints[i - 1] + myPoints[i + 1])/2.f;
			float factor = 2.f;
			factor += 0.5f * static_cast<float>(myPoints.Size() - 3);
			myCurve.AddPoint(middle+(myPoints[i]-middle)*factor);
		}
	}
	myCurve.BuildCurve();
}

void EnemyCurve::RenderDebug(const CommonUtilities::Camera & aCamera)
{
	for (EnemyOnCurve& eoc : myEnemies)
	{
		eoc.myEnemy->RenderDebug(aCamera);
	}
}

void EnemyCurve::SpawnEnemy()
{
	EnemyOnCurve eoc;
	eoc.myTime = 0.f;
	eoc.myEnemy = EnemyFactory::CreateEnemy(myEnemyClass, myCurve.GetPoint(0.f), myEnemyClassCache, myElement, false);

	myTimer = myInterval;
	--myAmountLeft;

	if (eoc.myEnemy == nullptr)return;
	myEnemies.Add(eoc);
}

void EnemyCurve::TakeDamage(const float aDamage, const float aDeltaTime)
{
	if (myLastHitEnemy == nullptr)
	{
		return;
	}

	myLastHitEnemy->TakeDamage(aDamage, aDeltaTime);
	myLastHitEnemy = nullptr;
}

inline bool EnemyCurve::IsDead(Highscore& aHighscore)
{
	for (unsigned short i = myEnemies.Size(); i > 0; --i)
	{
		EnemyOnCurve& eoc = myEnemies[i - 1];
		if (eoc.myEnemy->IsDead(aHighscore))
		{
			if (eoc.myEnemy->GetScoreFactor() > 0.f)
			{
				eoc.myEnemy->DeathAnimation();
			}

			SAFE_DELETE(eoc.myEnemy);

			myEnemies.RemoveCyclicAtIndex(i - 1);
		}
	}

	return myAmountLeft <= 0 && myEnemies.Size() <= 0;
}

inline void EnemyCurve::Kill()
{
	if (myLastHitEnemy == nullptr)
	{
		return;
	}

	myLastHitEnemy->Kill();
	myLastHitEnemy = nullptr;
}

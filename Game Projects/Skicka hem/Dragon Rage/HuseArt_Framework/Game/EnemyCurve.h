#pragma once

#include "Enemy.h"
#include "EnemyClassCache.h"

#include <BezierCurve.h>

struct EnemyOnCurve
{
	float myTime;
	Enemy* myEnemy;
};

class EnemyCurve : public Enemy
{
public:
	EnemyCurve(const int aAmount, const float aInterval, const float aSpeed, EnemyClassCache& aEnemyClassCache);
	~EnemyCurve();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float, Player&) override;
	void UpdateCollider() override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f&) override { return false; }
	bool IsBoss() override { return false; }
	inline bool IsDead(Highscore& aHighscore) override;
	inline void Kill() override;
	void DeathAnimation() override {};
	inline const float GetDamage() { return myEnemyClass.GetDMG(); }
	inline const float GetHP() const { return myEnemyClass.GetHP()*(myAmountStart); }

	bool IsTouching(Projectile* aProjectile) override;
	bool IsTouching(const CircleCollider* aCircleCollider) override;
	bool IsTouching(Beam& aBeam) override;

	void AddPoint(const CommonUtilities::Vector3f& aPoint);
	void PopPoint();
	void BuildCurve();

	void RenderDebug(const CommonUtilities::Camera& aCamera) override;

private:
	void SpawnEnemy();

	EnemyClassCache& myEnemyClassCache;
	CommonUtilities::BezierCurve myCurve;
	CommonUtilities::GrowingArray<EnemyOnCurve> myEnemies;
	CommonUtilities::GrowingArray<CommonUtilities::Vector3f> myPoints;

	int myAmountStart;
	int myAmountLeft;
	float myInterval;
	float mySpeed;
	float myTimer;

	Enemy* myLastHitEnemy;
};


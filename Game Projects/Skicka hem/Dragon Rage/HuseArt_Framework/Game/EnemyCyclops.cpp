#include "stdafx.h"
#include "EnemyCyclops.h"

#include "ProjectileManager.h"
#include "ProjectileFactory.h"
#include "Player.h"

#include <tga2d/error/error_manager.h>

namespace CU = CommonUtilities;


EnemyCyclops::EnemyCyclops()
	: myProjectileSpeed(0.f)
	, myRotation(0)
	, myFireRate(0.f)
	, myCooldown(0.f)
	, myProjectileDirection(0.0f, 0.0f, 0.0f)
	, myIsSlowed(false)
	, myHoverDistance(0.04f)
	, myBoppingSpeed(0.5f)
{}


EnemyCyclops::~EnemyCyclops()
{}

void EnemyCyclops::Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition)
{
	myPosition = aPosition; 
	InitAnimation();
	myEnemyClass = aEnemyClass;

	myHealth = myEnemyClass.GetHP();
	myProjectileSpeed = myEnemyClass.GetProjectileSpeed();
	myFireRate = myEnemyClass.GetROF();

	myCooldown = myFireRate;

	myHoverTime = 0.f;
	myOriginalPosition = myPosition;

	myHealthBar.Init(myEnemyClass.GetHP(), { myPosition.x, myPosition.y + 0.3f, myPosition.z - 0.01f }, 2.f);
}

void EnemyCyclops::Update(float aDeltaTime, Player& aPlayer)
{
	CU::Vector3f delta = aPlayer.GetPosition() - myPosition;
	myAnimation.Update(aDeltaTime);
	myAnimation.SetPosition(myPosition);
	myCooldown -= aDeltaTime;
	if (myCooldown <= 0.0f) 
	{
		myProjectileDirection = delta;
		Shoot();
		myCooldown = myFireRate;
	}


	myHoverTime += aDeltaTime * 2.f * Tga2D::Pif;
	myPosition = myOriginalPosition + CommonUtilities::Vector3f(0.f, myHoverDistance * std::sinf(myHoverTime * myBoppingSpeed), 0.f);

	myHealthBar.Update(aDeltaTime, myHealth, { myPosition.x, myPosition.y + 0.3f, myPosition.z - 0.01f });
	myAnimation.SetPosition(myPosition);
	DamagedFlash(myAnimation);
	myFlashTimer -= aDeltaTime;
}

void EnemyCyclops::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
	myHealthBar.FillRenderBuffer(aRenderBuffer);
}

void EnemyCyclops::TakeDamage(const float aDamage, const float aDeltaTime)
{
	myHealth -= aDamage * aDeltaTime;
	myHealthBar.Show();
	if (myFlashTimer < 0.8f)
	{
		myFlashTimer = 1.0f;
	}
}

bool EnemyCyclops::IsInside(const CommonUtilities::Vector2f & aPoint)
{
	return myAnimation.IsInside(aPoint);
}

void EnemyCyclops::Shoot()
{
	ProjectileBoulder* projectile = ProjectileFactory::CreateBoulder(myPosition, myProjectileSpeed, myProjectileDirection);
	projectile->SetDamage(myEnemyClass.GetDMG());
	ProjectileManager::GetInstance()->EnemyShoot(projectile);
}

void EnemyCyclops::InitAnimation()
{
	switch (myElement)
	{
	case eElement::Fire:
		myAnimation.Init("Assets/Images/Enemies/enemy_cyclops_fire.dds", myPosition);
		break;
	case eElement::Ice:
		myAnimation.Init("Assets/Images/Enemies/enemy_cyclops_ice.dds", myPosition);
		break;
	case eElement::Lightning:
		myAnimation.Init("Assets/Images/Enemies/enemy_cyclops_lightning.dds", myPosition);
		break;
	}

	myAnimation.SetPosition(myPosition);
	myAnimation.Update(0.f);
}

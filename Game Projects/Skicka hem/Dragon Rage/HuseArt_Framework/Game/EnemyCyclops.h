#pragma once

#include "Enemy.h"

#include <Animation.h>

class Player;

class EnemyCyclops : public Enemy
{
public:
	EnemyCyclops();
	~EnemyCyclops();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float aDeltaTime, Player& aPlayer) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsBoss() override { return false; }

	void Shoot();

private:
	void InitAnimation();

	bool myIsSlowed;
	float mySlowTimer;
	float myProjectileSpeed;
	float myRotation;
	float myFireRate;
	float myCooldown;
	Animation myAnimation;
	CommonUtilities::Vector3f myProjectileDirection;

	float myHoverDistance;
	float myBoppingSpeed;
	float myHoverTime;
	CommonUtilities::Vector3f myOriginalPosition;
};


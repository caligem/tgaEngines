#include "stdafx.h"
#include "EnemyFactory.h"

#include "EnemyManager.h"

#include "EnemyParticle.h"
#include "BossBlood.h"
#include "BossAbsorbParticle.h"

#include "EnemyStormCloud.h"
#include "EnemyCyclops.h"
#include "EnemyGiantOwl.h"
#include "EnemyOwlRider.h"
#include "EnemyNatureWisp.h"
#include "EnemyWizard.h"

#include "BossIceDragon.h"
#include "BossBoneDragon.h"
#include "BossLightningDragon.h"
#include "BossCastle.h"
#include "BossGrandLich.h"
#include "BossBeholder.h"

#include "EnemyClass.h"
#include "EnemyClassCache.h"

#include "Element.h"

Enemy * EnemyFactory::CreateEnemy(const EnemyClass & aEnemyClass, const CommonUtilities::Vector3f & aPosition, EnemyClassCache & aEnemyClassCache, eElement aElement, bool aAddToEnemyManager)
{
	Enemy* newEnemy = nullptr;

	if (aEnemyClass.GetName() == "stormcloud")
	{
		newEnemy = new EnemyStormCloud();
		newEnemy->Init(aEnemyClass, aPosition);
	}
	else if (aEnemyClass.GetName() == "cyclops")
	{
		newEnemy = new EnemyCyclops();
		newEnemy->SetScoreFactor(0.8f);
	}
	else if (aEnemyClass.GetName() == "giantowl")
	{
		newEnemy = new EnemyGiantOwl();
		newEnemy->SetScoreFactor(0.4f);
	}
	else if (aEnemyClass.GetName() == "owlrider")
	{
		newEnemy = new EnemyOwlRider();
		newEnemy->SetScoreFactor(0.5f);
	}
	else if (aEnemyClass.GetName() == "naturewisp" || aEnemyClass.GetName() == "icewisp" ||
		aEnemyClass.GetName() == "firewisp" || aEnemyClass.GetName() == "lightningwisp" )
	{
		newEnemy = new EnemyNatureWisp();
		newEnemy->SetScoreFactor(0.6f);
	}
	else if (aEnemyClass.GetName() == "wizard")
	{
		newEnemy = new EnemyWizard();
		newEnemy->SetScoreFactor(0.7f);
	}
	else if (aEnemyClass.GetName() == "bossicedragon")
	{
		newEnemy = new BossIceDragon();
	}
	else if (aEnemyClass.GetName() == "bossbonedragon")
	{
		newEnemy = new BossBoneDragon();
	}
	else if (aEnemyClass.GetName() == "bosscastle")
	{
		newEnemy = new BossCastle(aEnemyClassCache);
	}
	else if (aEnemyClass.GetName() == "bosslightningdragon")
	{
		newEnemy = new BossLightningDragon();
	}
	else if (aEnemyClass.GetName() == "bossbeholder")
	{
		newEnemy = new BossBeholder();
	}
	else if (aEnemyClass.GetName() == "bossgrandlich")
	{
		newEnemy = new BossGrandLich(aEnemyClassCache);
	}

	if (newEnemy != nullptr)
	{
		newEnemy->SetElement(aElement);
		newEnemy->Init(aEnemyClass, aPosition);
	}

	if (aAddToEnemyManager && newEnemy != nullptr)
	{
		EnemyManager::GetInstance()->AddEnemy(newEnemy);
	}

	return newEnemy;
}

BossAbsorbParticle* EnemyFactory::CreateBossAbsorbParticle(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f& aPosition, const CommonUtilities::Vector4f& aTint, const float aAngle)
{
	BossAbsorbParticle* newParticle = nullptr;

	newParticle = new BossAbsorbParticle(aAngle);
	newParticle->Init(aEnemyClass, aPosition);
	newParticle->SetTint(aTint);

	if (newParticle != nullptr)
	{
		EnemyManager::GetInstance()->AddEnemy(newParticle);
	}

	return newParticle;
}

EnemyParticle* EnemyFactory::CreateEnemyParticle(const EnemyClass & aEnemyClass, const CommonUtilities::Vector3f & aPosition, const eElement& aElement, const bool aIsBoss)
{
	EnemyParticle* newParticle = nullptr;

	newParticle = new EnemyParticle();
	newParticle->Init(aEnemyClass, aPosition);
	newParticle->SetElement(aElement, aIsBoss);



	if (newParticle != nullptr)
	{
		EnemyManager::GetInstance()->AddEnemy(newParticle);
	}

	return newParticle;
}

BossBlood* EnemyFactory::CreateBossBlood(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f& aPosition, const CommonUtilities::Vector4f& aTint, const float aMinAngle, const float aMaxAngle, const float aSpeed, const float aScale)
{
	BossBlood* newParticle = nullptr;

	newParticle = new BossBlood(aMinAngle, aMaxAngle, aSpeed, aScale);
	newParticle->Init(aEnemyClass, aPosition);
	newParticle->SetTint(aTint);

	if (newParticle != nullptr)
	{
		EnemyManager::GetInstance()->AddEnemy(newParticle);
	}

	return newParticle;
}
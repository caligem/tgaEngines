#pragma once

#include <Vector.h>

class Enemy;
class EnemyClass;
class EnemyClassCache;

class EnemyParticle;
class BossBlood;
class BossAbsorbParticle;

#include "Element.h"

namespace EnemyFactory
{
	Enemy* CreateEnemy(
		const EnemyClass& aEnemyClass,
		const CommonUtilities::Vector3f & aPosition,
		EnemyClassCache& aEnemyClassCache,
		eElement aElement = eElement::Fire,
		bool aAddToEnemyManager = true
	);

	BossBlood* CreateBossBlood(const EnemyClass& aEnemyClass,
		const CommonUtilities::Vector3f& aPosition,
		const CommonUtilities::Vector4f& aTint,
		const float aMinAngle,
		const float aMaxAngle,
		const float aSpeed = 1.0f,
		const float aScale = 0.5f

	);

	BossAbsorbParticle* CreateBossAbsorbParticle(const EnemyClass& aEnemyClass,
		const CommonUtilities::Vector3f& aPosition,
		const CommonUtilities::Vector4f& aTint,
		const float aAngle
	);

	EnemyParticle* CreateEnemyParticle(
		const EnemyClass& aEnemyClass,
		const CommonUtilities::Vector3f& aPosition,
		const eElement& aElement,
		const bool aIsBoss = false
	);

	
}


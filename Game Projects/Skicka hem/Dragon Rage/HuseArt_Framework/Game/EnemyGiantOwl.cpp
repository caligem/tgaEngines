#include "stdafx.h"
#include "EnemyGiantOwl.h"
#include "ProjectileManager.h"
#include "ProjectileFactory.h"
#include "Player.h"

#include <tga2d/error/error_manager.h>

namespace CU = CommonUtilities;


EnemyGiantOwl::EnemyGiantOwl()
	: myIsSlowed(false)
{}


EnemyGiantOwl::~EnemyGiantOwl()
{}

void EnemyGiantOwl::Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition)
{
	myPosition = aPosition;
	myEnemyClass = aEnemyClass;
	myHealth = myEnemyClass.GetHP();

	InitAnimation();

	myShine.Init("Assets/Images/GUI/powerShine.dds", { myPosition.x, myPosition.y, myPosition.z + 0.0001f });
	myShine.SetScale({ 1.0f, 1.0f });
	myShine.SetPosition({ myPosition.x, myPosition.y, myPosition.z + 0.001f });
	myShineAlfa = 0.0f;
	myTintAmount = 12.0f;
	myAnimation.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f - (myTintAmount / 12.0f) });
	myIsSpawned = false;

	myHealthBar.Init(myEnemyClass.GetHP(), { myPosition.x, myPosition.y + 0.1f, myPosition.z - 0.01f }, 2.f);

	myMovement = 0.0f;
}

void EnemyGiantOwl::Update(float aDeltaTime, Player& aPlayer)
{
	if (myIsSpawned)
	{
		if (myTintAmount > 1.0f)
		{
			myTintAmount -= aDeltaTime * 8.0f;
		}

		if (1.0f - (myTintAmount / 12.0f) < 0.5f)
		{
			myShineAlfa += aDeltaTime * 8.0f;
		}
		else
		{
			myShineAlfa -= aDeltaTime * 8.0f;
		}

		myAnimation.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f - (myTintAmount / 12.0f) });
		myShine.SetTint({ 1.0f, 1.0f, 1.0f, myShineAlfa });
		myShine.SetPosition({ myPosition.x, myPosition.y, myPosition.z + 0.001f });
	}
	else
	{
		myShineAlfa = 0.0f;
		myAnimation.SetTint({ 1.0f, 1.0f, 1.0f, 1.0f });
	}

	if (myShineAlfa <= 0.0f)
	{
		aPlayer;
		myAnimation.Update(aDeltaTime);
		myAnimation.SetPosition(myPosition);

		if (myMovement > 0.0f)
		{
			myPosition += myMoveDirection * myMovement * aDeltaTime;
		}

		myHealthBar.Update(aDeltaTime, myHealth, { myPosition.x, myPosition.y + 0.1f, myPosition.z - 0.01f });

		DamagedFlash(myAnimation);
		myFlashTimer -= aDeltaTime;
	}
}

void EnemyGiantOwl::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
	myHealthBar.FillRenderBuffer(aRenderBuffer);

	if (myIsSpawned)
	{
		aRenderBuffer.Add(&myShine);
	}
}

void EnemyGiantOwl::TakeDamage(const float aDamage, const float aDeltaTime)
{
	if (myShineAlfa <= 0.0f)
	{
		myHealth -= aDamage * aDeltaTime;
		myHealthBar.Show();
		if (myFlashTimer < 0.8f)
		{
			myFlashTimer = 1.0f;
		}
	}
}

bool EnemyGiantOwl::IsInside(const CommonUtilities::Vector2f & aPoint)
{
	return myAnimation.IsInside(aPoint);
}

void EnemyGiantOwl::SetMoveDown(const CommonUtilities::Vector3f& aDir, float aSpeed, float aRotatoin)
{
	myMovement = aSpeed;
	myMoveDirection = aDir;
	myAnimation.SetRotation(aRotatoin);
}

void EnemyGiantOwl::InitAnimation()
{
	switch (myElement)
	{
	case eElement::Fire:
		myAnimation.Init("Assets/Images/Enemies/enemy_bird_fire.dds", myPosition);
		break;
	case eElement::Ice:
		myAnimation.Init("Assets/Images/Enemies/enemy_bird_ice.dds", myPosition);
		break;
	case eElement::Lightning:
		myAnimation.Init("Assets/Images/Enemies/enemy_bird_lightning.dds", myPosition);
		break;
	}

	myAnimation.SetPosition(myPosition);
	myAnimation.Update(0.f);
}

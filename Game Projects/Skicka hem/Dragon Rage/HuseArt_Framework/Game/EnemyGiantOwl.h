#pragma once

#include "Enemy.h"

#include <Animation.h>

class Player;

class EnemyGiantOwl : public Enemy
{
public:
	EnemyGiantOwl();
	~EnemyGiantOwl();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float aDeltaTime, Player& aPlayer) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsBoss() override { return false; }

	void SetMoveDown(const CommonUtilities::Vector3f& aDir, float aSpeed, float aRotatoin = 0.0f);
	void IsSpawned() { myIsSpawned = true; }

private:
	void InitAnimation();	

	Animation myShine;
	bool myIsSpawned;
	float myTintAmount;
	float myShineAlfa;

	bool myIsSlowed;
	float mySlowTimer;
	float myMovement;
	Animation myAnimation;
	CommonUtilities::Vector3f myProjectileDirection;
	CommonUtilities::Vector3f myMoveDirection;

};


#include "stdafx.h"
#include "EnemyManager.h"

#include <algorithm>

#include "ExplosionManager.h"

#include "Player.h"
#include "Enemy.h"
#include "Projectile.h"
#include "Highscore.h"

#include <Animation.h>

namespace CU = CommonUtilities;

EnemyManager* EnemyManager::ourInstance = nullptr;

void EnemyManager::Create()
{
	if (ourInstance == nullptr)
	{
		ourInstance = new EnemyManager();
		ourInstance->myEnemies.Init(32);
		ourInstance->myCurrentBoss = nullptr;
	}
	else
	{
		ourInstance->myEnemies.DeleteAll();
	}
}

void EnemyManager::Destroy()
{
	if (ourInstance == nullptr)
	{
		return;
	}
	ourInstance->myEnemies.DeleteAll();
	SAFE_DELETE(ourInstance);
}

void EnemyManager::AddEnemy(Enemy* aEnemy)
{
	myEnemies.Add(aEnemy);
}

void EnemyManager::Update(const float aDeltaTime, Player & aPlayer)
{
	if (myEnemies.Empty())
	{
		return;
	}

	for(unsigned short i = 0; i < myEnemies.Size(); ++i)
	{
		Enemy* enemy = myEnemies[i];
		if (!enemy->IsAwake())
		{
			continue;
		}
		enemy->Update(aDeltaTime, aPlayer);
		enemy->UpdateCollider();
	}


	/*
	std::sort(myEnemies.begin(), myEnemies.end(), [&](Enemy* a, Enemy* b){
		return a->GetPosition().y < b->GetPosition().y;
	});
	*/
}

void EnemyManager::CollideWithPlayer(Player & aPlayer, const float aDeltaTime)
{
	for(unsigned short i = 0; i < myEnemies.Size(); ++i)
	{
		Enemy* enemy = myEnemies[i];

		if (!enemy->IsAwake())
		{
			continue;
		}
		if(enemy->IsTouching(aPlayer.GetCollider()))
		{
			aPlayer.TakeDamage(enemy->GetDamage());

			if (!enemy->IsBoss())
			{
				enemy->TakeDamage(enemy->GetDamage() * aDeltaTime * 0.5f);
			}
		}

		if (enemy->BeamCollision(aPlayer.GetCollider()))
		{
			aPlayer.TakeDamage(enemy->GetDamage() * 2.0f);
		}


		float scoreFactor = ((enemy->GetElement() == aPlayer.GetBeam().GetElement()) || enemy->IsBoss() ) ? 1.0f : 0.25f;
		if (enemy->IsTouching(aPlayer.GetBeam()))
		{
			enemy->SetTakenElement(aPlayer.GetBeam().GetElement());
			enemy->TakeDamage(aPlayer.GetBeam().GetDamage() * scoreFactor, aDeltaTime);
		}
	}
}

void EnemyManager::CleanUp(Highscore& aHighscore)
{
	if (myCurrentBoss != nullptr)
	{
		if (myCurrentBoss->IsDead(aHighscore))
		{
			myCurrentBoss = nullptr;
		}
	}
	for (unsigned short i = myEnemies.Size(); i > 0; --i)
	{
		Enemy* enemy = myEnemies[i - 1];
		if (!enemy->IsAwake())
		{
			continue;
		}
		if (enemy->IsDead(aHighscore))
		{
			if (enemy->GetScoreFactor() > 0.f)
			{
				enemy->DeathAnimation();
			}

			if (enemy->IsBoss())
			{
				ProjectileManager::GetInstance()->ClearProjectiles();
			}
			
			myEnemies.DeleteCyclicAtIndex(i - 1);
		}
	}
}

void EnemyManager::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	for (Enemy* enemy : myEnemies)
	{
		if (!enemy->IsAwake())
		{
			//continue;
		}
		enemy->FillRenderBuffer(aRenderBuffer);
	}
}

void EnemyManager::FillGuiBuffer(CommonUtilities::GrowingArray<Drawable*>& aGuiBuffer)
{
	for (Enemy* enemy : myEnemies)
	{
		if (!enemy->IsAwake())
		{
			continue;
		}
		enemy->FillGuiBuffer(aGuiBuffer);
	}
}

bool EnemyManager::IsEnemyHit(Projectile* aProjectile, const float aDeltaTime)
{
	for (unsigned short i = myEnemies.Size(); i > 0; --i)
	{
		Enemy* enemy = myEnemies[i - 1];
		if (!enemy->IsAwake())
		{
			continue;
		}
		if (enemy->IsTouching(aProjectile))
		{
			enemy->SetTakenElement(aProjectile->GetElement());

			float scoreFactor = ((enemy->GetElement() == aProjectile->GetElement()) || enemy->IsBoss() )? 1.0f : 0.25f;

			if (strcmp(aProjectile->GetType(), "firebreath") == 0)
			{
				enemy->TakeDamage(aProjectile->GetDamage()*scoreFactor, aDeltaTime);
			}
			else
			{
				enemy->TakeDamage(aProjectile->GetDamage()*scoreFactor);
			}
			return true;
		}
	}

	return false;
}

void EnemyManager::WakeEnemies(const CommonUtilities::Camera& aCamera)
{
	for (Enemy* enemy : myEnemies)
	{
		if (enemy->GetPosition().y - aCamera.GetPosition().y < -1.1f && !enemy->IsBoss())
		{
			enemy->SetScoreFactor(0.f);
			enemy->Kill();
		}

		if (enemy->IsAwake())
		{
			continue;
		}

		if (enemy->GetPosition().y - aCamera.GetPosition().y < 1.f)
		{
			enemy->Wake();
			if (enemy->IsBoss())
			{
				myCurrentBoss = enemy;
				myCameraTargetPosition = enemy->GetPosition() - CommonUtilities::Vector3f(0.f, 0.9f, 0.f);
			}
		}
	}
}

float EnemyManager::GetLevelEnd() const
{
	float levelEndY = 0.f;

	for (Enemy* enemy : myEnemies)
	{
		if (enemy->GetPosition().y > levelEndY)
		{
			levelEndY = enemy->GetPosition().y;
		}
	}
	return levelEndY + 0.1f;
}

void EnemyManager::RenderDebug(const CommonUtilities::Camera & aCamera)
{
	for (Enemy* enemy : myEnemies)
	{
		if (!enemy->IsAwake())
		{
			continue;
		}
		enemy->RenderDebug(aCamera);
	}
}

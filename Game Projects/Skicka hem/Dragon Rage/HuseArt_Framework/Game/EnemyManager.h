#pragma once

#include <GrowingArray.h>
#include <Camera.h>
#include <Vector.h>

class Enemy;
class Drawable;
class Player;
class Projectile;
class Highscore;

class EnemyManager
{
public:
	static EnemyManager* GetInstance() { return ourInstance; }

	static void Create();
	static void Destroy();

	void AddEnemy(Enemy* aEnemy);

	void Update(const float aDeltaTime, Player& aPlayer);
	void CollideWithPlayer(Player& aPlayer, const float aDeltaTime);
	void CleanUp(Highscore& aHighscore);
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer);
	void FillGuiBuffer(CommonUtilities::GrowingArray<Drawable*>& aGuiBuffer);
	bool IsEnemyHit(Projectile* aProjectile, const float aDeltaTime);
	void WakeEnemies(const CommonUtilities::Camera& aCamera);

	inline bool IsBossFight() { return myCurrentBoss != nullptr; }
	inline Enemy* GetCurrentBoss() { return myCurrentBoss; }
	inline const CommonUtilities::Vector3f& GetTargetPosition() { return myCameraTargetPosition; }

	float GetLevelEnd() const;

	void RenderDebug(const CommonUtilities::Camera& aCamera);

private:
	EnemyManager() = default;
	~EnemyManager() = default;

	static EnemyManager* ourInstance;

	CommonUtilities::GrowingArray<Enemy*> myEnemies;

	Enemy* myCurrentBoss;
	CommonUtilities::Vector3f myCameraTargetPosition;

	int myEnemyCount;
	int myEnemiesKilled;

};


#include "stdafx.h"
#include "EnemyNatureWisp.h"

#include "ProjectileManager.h"
#include "ProjectileFactory.h"
#include "Player.h"

#include <tga2d/math/common_math.h>

namespace CU = CommonUtilities;


EnemyNatureWisp::EnemyNatureWisp()
	: myProjectileSpeed(0.0f)
	, myRotation(0.f)
	, myFireRate(0.0f)
	, myProjectileDirection(0.f, 1.f, 0.f)
	, myIsSlowed(false)
	, myMoveDown(0.0f)
{}

void EnemyNatureWisp::Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition)
{
	myPosition = aPosition;
	InitAnimation();
	myAnimation.Setup(1, 1, 1);
	myAnimation.SetScale({ 1.f, 1.f });
	myEnemyClass = aEnemyClass;

	myShine.Init("Assets/Images/GUI/powerShine.dds", { aPosition.x, aPosition.y, aPosition.z + 0.0001f });
	myShine.SetScale({ 1.0f, 1.0f });
	myShineAlfa = 0.0f;
	myTintAmount = 12.0f;
	myAnimation.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f - (myTintAmount / 12.0f) });
	myIsSpawned = false;

	myHealth = myEnemyClass.GetHP();
	myProjectileSpeed = myEnemyClass.GetProjectileSpeed();
	myFireRate = myEnemyClass.GetROF();

	myCooldown = myFireRate;

	myHealthBar.Init(myEnemyClass.GetHP(), { myPosition.x, myPosition.y + 0.1f, myPosition.z - 0.01f }, 2.f);
}

void EnemyNatureWisp::Update(float aDeltaTime, Player& aPlayer)
{
	if (myIsSpawned)
	{
		if (myTintAmount > 1.0f)
		{
			myTintAmount -= aDeltaTime * 8.0f;
		}

		if (1.0f - (myTintAmount / 12.0f) < 0.5f)
		{
			myShineAlfa += aDeltaTime * 8.0f;
		}
		else
		{
			myShineAlfa -= aDeltaTime * 8.0f;
		}

		myAnimation.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f - (myTintAmount / 12.0f) });
		myShine.SetTint({ 1.0f, 1.0f, 1.0f, myShineAlfa });
	}
	else
	{
		myShineAlfa = 0.0f;
		myAnimation.SetTint({ 1.0f, 1.0f, 1.0f, 1.0f });
	}

	if (myShineAlfa <= 0.0f)
	{
		aPlayer;
		myProjectileDirection = { sinf(myRotation), cosf(myRotation), 0.f };
		CU::Vector3f secondDirection = { -myProjectileDirection.y, myProjectileDirection.x, 0.0f };
		CU::Vector3f thirdDirection = -myProjectileDirection;
		CU::Vector3f fourthDirection = -secondDirection;

		myCooldown -= aDeltaTime;


		if (myCooldown <= 0)
		{
			Shoot(myProjectileDirection);
			Shoot(secondDirection);
			Shoot(thirdDirection);
			Shoot(fourthDirection);
			myRotation += Tga2D::Pif / 4.f;
			myCooldown = myFireRate;
		}

		myAnimation.Update(aDeltaTime);
		myAnimation.SetPosition(GetPosition());

		myPosition.y -= myMoveDown * aDeltaTime / 10.0f;

		myHealthBar.Update(aDeltaTime, myHealth, { myPosition.x, myPosition.y + 0.1f, myPosition.z - 0.01f });

		DamagedFlash(myAnimation);
		myFlashTimer -= aDeltaTime;
	}
}

void EnemyNatureWisp::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);;
	myHealthBar.FillRenderBuffer(aRenderBuffer);

	if (myIsSpawned)
	{
		aRenderBuffer.Add(&myShine);
	}
}


void EnemyNatureWisp::TakeDamage(const float aDamage, const float aDeltaTime)
{
	if (myShineAlfa <= 0.0f)
	{
		myHealth -= aDamage * aDeltaTime;
		myHealthBar.Show();
		if (myFlashTimer < 0.8f)
		{
			myFlashTimer = 1.0f;
		}
	}
}

bool EnemyNatureWisp::IsInside(const CommonUtilities::Vector2f & aPoint)
{
	return myAnimation.IsInside(aPoint);
}

void EnemyNatureWisp::Shoot(CU::Vector3f& aProjectileDirection)
{
	if (myElement == eElement::Fire)
	{
		ProjectileWispFireShot* projectile = ProjectileFactory::CreateWispFireShot(myPosition, myProjectileSpeed, aProjectileDirection);
		projectile->SetDamage(myEnemyClass.GetDMG());
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
	}
	else if (myElement == eElement::Ice)
	{
		ProjectileWispIceShot* projectile = ProjectileFactory::CreateWispIceShot(myPosition, myProjectileSpeed, aProjectileDirection);
		projectile->SetDamage(myEnemyClass.GetDMG());
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
	}
	else if (myElement == eElement::Lightning)
	{
		ProjectileWispLightningShot* projectile = ProjectileFactory::CreateWispLightningShot(myPosition, myProjectileSpeed, aProjectileDirection);
		projectile->SetDamage(myEnemyClass.GetDMG());
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
	}
}

void EnemyNatureWisp::MoveDown(float aSpeed)
{
	myMoveDown = aSpeed;
}

void EnemyNatureWisp::InitAnimation()
{
	switch (myElement)
	{
	case eElement::Fire:
		myAnimation.Init("Assets/Images/Enemies/enemy_wisp_fire.dds", myPosition);
		break;
	case eElement::Ice:
		myAnimation.Init("Assets/Images/Enemies/enemy_wisp_ice.dds", myPosition);
		break;
	case eElement::Lightning:
		myAnimation.Init("Assets/Images/Enemies/enemy_wisp_lightning.dds", myPosition);
		break;
	}

	myAnimation.SetPosition(myPosition);
	myAnimation.Update(0.f);
}

#pragma once

#include "Enemy.h"

#include <Animation.h>

class Player;

class EnemyNatureWisp : public Enemy
{
public:
	EnemyNatureWisp();
	~EnemyNatureWisp() = default;

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float aDeltaTime, Player& aPlayer) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsBoss() override { return false; }

	void Shoot(CommonUtilities::Vector3f& aDirection);

	void MoveDown(float aSpeed);
	void IsSpawned() { myIsSpawned = true; }

private:
	void InitAnimation();
	
	Animation myShine;
	bool myIsSpawned;
	float myTintAmount;
	float myShineAlfa;

	bool myIsSlowed;
	float mySlowTimer;
	float myProjectileSpeed;
	float myRotation;
	float myFireRate;
	float myCooldown;
	float myMoveDown;

	Animation myAnimation;
	CommonUtilities::Vector3f myProjectileDirection;
	CommonUtilities::Vector3f myProjectileDirectionFive;
	CommonUtilities::Vector3f myProjectileDirectionSix;
	CommonUtilities::Vector3f myProjectileDirectionSeven;
	CommonUtilities::Vector3f myProjectileDirectionEight;
	CommonUtilities::Vector3f myRotateVector;

};
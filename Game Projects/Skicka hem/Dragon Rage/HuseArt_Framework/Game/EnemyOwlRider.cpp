#include "stdafx.h"
#include "EnemyOwlRider.h"

#include "ProjectileManager.h"
#include "ProjectileFactory.h"
#include "Player.h"

#include <Random.h>
#include <tga2d/error/error_manager.h>

namespace CU = CommonUtilities;


EnemyOwlRider::EnemyOwlRider()
	: myProjectileSpeed(0.f)
	, myFireRate(0.0f)
	, myProjectileDirection(0.f, -1.f, 0.f)
	, myIsSlowed(false)
{}


EnemyOwlRider::~EnemyOwlRider()
{}

void EnemyOwlRider::Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition)
{
	myPosition = aPosition;
	myAllowStrafe = false;
	
	InitAnimation();

	myEnemyClass = aEnemyClass;

	myShine.Init("Assets/Images/GUI/powerShine.dds", { myPosition.x, myPosition.y, myPosition.z + 0.0001f });
	myShine.SetScale({ 1.0f, 1.0f });
	myShineAlfa = 0.0f;
	myTintAmount = 12.0f;
	myAnimation.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f - (myTintAmount / 12.0f) });
	myIsSpawned = false;	

	myHealth = myEnemyClass.GetHP();
	myProjectileSpeed = myEnemyClass.GetProjectileSpeed();
	myFireRate = myEnemyClass.GetROF();
	myMovementSpeed = myEnemyClass.GetMovementSpeed();

	myShootCooldown = myFireRate * CommonUtilities::Random();
	
	float rand = CommonUtilities::Random();
	if (rand < 0.5f)
	{
		myIsMovingLeft = true;
	}
	else
	{
		myIsMovingLeft = false;
	}

	myLeftStrafeValue = -1.f;
	myRightStrafeValue = 1.f;

	myHealthBar.Init(myEnemyClass.GetHP(), { myPosition.x, myPosition.y + 0.12f, myPosition.z - 0.01f }, 2.f);
}

void EnemyOwlRider::Update(float aDeltaTime, Player& aPlayer)
{
	if (myIsSpawned)
	{
		if (myTintAmount > 1.0f)
		{
			myTintAmount -= aDeltaTime * 8.0f;
		}

		if (1.0f - (myTintAmount / 12.0f) < 0.5f)
		{
			myShineAlfa += aDeltaTime * 8.0f;
		}
		else
		{
			myShineAlfa -= aDeltaTime * 8.0f;
		}

		myAnimation.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f - (myTintAmount / 12.0f) });
		myShine.SetTint({ 1.0f, 1.0f, 1.0f, myShineAlfa });
		myShine.SetPosition({ myPosition.x, myPosition.y, myPosition.z + 0.001f });
	}
	else
	{
		myShineAlfa = 0.0f;
		myAnimation.SetTint({ 1.0f, 1.0f, 1.0f, 1.0f });
	}

	if (myShineAlfa <= 0.0f)
	{

		aPlayer;
		myAnimation.Update(aDeltaTime);
		myAnimation.SetPosition(myPosition);
		myShootCooldown -= aDeltaTime;

		if (myShootCooldown <= 0)
		{
			Shoot();
			myShootCooldown = myFireRate;
		}

		if (myAllowStrafe == true)
		{
			StrafeMove(aDeltaTime);
		}

		myHealthBar.Update(aDeltaTime, myHealth, { myPosition.x, myPosition.y + 0.12f, myPosition.z - 0.01f });

		DamagedFlash(myAnimation);
		myFlashTimer -= aDeltaTime;
	}
}

void EnemyOwlRider::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
	myHealthBar.FillRenderBuffer(aRenderBuffer);

	if (myIsSpawned)
	{
		aRenderBuffer.Add(&myShine);
	}
}

void EnemyOwlRider::TakeDamage(const float aDamage, const float aDeltaTime)
{
	if (myShineAlfa <= 0.0f)
	{
		myHealth -= aDamage * aDeltaTime;
		myHealthBar.Show();
		if (myFlashTimer < 0.8f)
		{
			myFlashTimer = 1.0f;
		}
	}
}

void EnemyOwlRider::StrafeMove(float aDeltaTime)
{
	if (myIsMovingLeft == true)
	{
		if (myPosition.x <= myLeftStrafeValue)
		{
			myIsMovingLeft = false;
		}
		else
		{
			Move(CommonUtilities::Vector3f(-myMovementSpeed, -0.2f, 0.f)*aDeltaTime);
		}
	}
	else if (myIsMovingLeft == false)
	{
		if (myPosition.x >= myRightStrafeValue)
		{
			myIsMovingLeft = true;
		}
		else
		{
			Move(CommonUtilities::Vector3f(myMovementSpeed, -0.2f, 0.f)*aDeltaTime);
		}
	}
	else
	{
		ERROR_PRINT("ERROR IN ENEMYARCHER MOVEMENT FUNCTION");
	}
}


bool EnemyOwlRider::IsInside(const CommonUtilities::Vector2f & aPoint)
{
	return myAnimation.IsInside(aPoint);
}

void EnemyOwlRider::Shoot()
{
	ProjectileArrow* projectile = ProjectileFactory::CreateArrow(myPosition, myProjectileSpeed, myProjectileDirection);
	projectile->SetDamage(myEnemyClass.GetDMG());
	ProjectileManager::GetInstance()->EnemyShoot(projectile);
}


void EnemyOwlRider::InitAnimation()
{
	switch (myElement)
	{
	case eElement::Fire:
		myAnimation.Init("Assets/Images/Enemies/enemy_owlrider_fire.dds", myPosition);
		break;
	case eElement::Ice:
		myAnimation.Init("Assets/Images/Enemies/enemy_owlrider_ice.dds", myPosition);
		break;
	case eElement::Lightning:
		myAnimation.Init("Assets/Images/Enemies/enemy_owlrider_lightning.dds", myPosition);
		break;
	}

	myAnimation.SetPosition(myPosition);
	myAnimation.Update(0.f);
}

#pragma once

#include "Enemy.h"

#include <Animation.h>

class Player;

class EnemyOwlRider : public Enemy
{
public:
	EnemyOwlRider();
	~EnemyOwlRider();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float aDeltaTime, Player& aPlayer) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsBoss() override { return false; }

	void StrafeMove(float aDeltaTime);
	void AllowStrafe() { myAllowStrafe = true; }
	void IsSpawned() { myIsSpawned = true; }

	void Shoot();

private:
	void InitAnimation();


	Animation myShine;
	bool myIsSpawned;
	float myTintAmount;
	float myShineAlfa;

	bool myIsSlowed;
	float mySlowTimer;
	float myProjectileSpeed;
	float myFireRate;
	float myShootCooldown;

	bool myAllowStrafe;
	bool myIsMovingLeft;
	float myLeftStrafeValue;
	float myRightStrafeValue;
	float myMovementSpeed;

	Animation myAnimation;
	CommonUtilities::Vector3f myProjectileDirection;

};


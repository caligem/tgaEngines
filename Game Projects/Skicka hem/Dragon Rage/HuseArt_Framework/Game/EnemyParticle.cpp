#include "stdafx.h"
#include "EnemyParticle.h"

#include <Random.h>

#include <Mathf.h>

#include "SFXManager.h"
#include "ExplosionManager.h"
#include "Player.h"
#include "Highscore.h"

float EnemyParticle::myCoinTimer = 0.f;
float EnemyParticle::myCoinPitch = 0.f;
float EnemyParticle::myLastSound = 0.f;
int EnemyParticle::myCoinCount = 0;
int EnemyParticle::mySoulsCaught = 0;
int EnemyParticle::mySoulCombos = 0;

EnemyParticle::EnemyParticle()
{
}

EnemyParticle::~EnemyParticle()
{
}

void EnemyParticle::Init(const EnemyClass & aEnemyClass, const CommonUtilities::Vector3f aPosition)
{
	aEnemyClass;
	myHealth = 1.f;
	myScoreFactor = 132.5f;

	myAnimation.Init("Assets/Images/Projectiles/souls.dds", aPosition);
	myGlowAnimation.Init("Assets/Images/GUI/powerShine.dds", aPosition);
	myAnimation.SetScale({ 1.f, 1.f });
	myGlowAnimation.SetScale({ 0.5f, 0.5f });
	myIncreaseTint = true;

	float angle = CommonUtilities::Random() * 2.f * Tga2D::Pif;
	float dist = CommonUtilities::Random() * 0.3f;
	myDirection.x = std::cosf(angle) * dist;
	myDirection.y = std::sinf(angle) * dist;
	myDirection.z = 0.f;

	myCountDown.Set([=] {myStop = true; }, 0.6f);
	myStop = false;
	myAbsorbRange = 0.55f;
	myKillRange = 0.1f;
	mySpeed = 2.f;
	myPosition = aPosition;

	myIsBoss = false;
	myIncreaseRG = true;
	myIncreaseGB = false;
	myIncreaseBR = false;
}

void EnemyParticle::Update(float aDeltaTime, Player& aPlayer)
{
	myCountDown.Update(aDeltaTime);

	if (myStop)
	{
		mySpeed -= aDeltaTime * 2.f;

		if (mySpeed < 0.4f)
		{
			mySpeed = 0.4f;
		}

		MoveTowardsPlayer(aDeltaTime, aPlayer);
	}
	else
	{
		mySpeed -= aDeltaTime / 2.f;
		myPosition += myDirection * mySpeed * aDeltaTime;
	}
	
	myAnimation.SetPosition(myPosition);
	myAnimation.Update(aDeltaTime);

	if (myIncreaseTint)
	{
		if (myGlowTint < 0.6f)
		{
			myGlowTint += aDeltaTime * 5.0f;
		}
		else
		{
			myIncreaseTint = false;
		}
	}
	else if (myGlowTint > 0.0f)
	{
		myGlowTint -= aDeltaTime * 5.0f;
	}
	else
	{
		myIncreaseTint = true;
	}

	myGlowAnimation.SetPosition(myPosition);
	myGlowAnimation.SetTint({ myTint.x, myTint.y, myTint.z, myGlowTint });


	if (myIsBoss)
	{		
		UpdateBossColor(aDeltaTime);
		myAnimation.SetTint(myTint);
	}

	if (EnemyParticle::myCoinCount >= 20)
	{
		//TODO: PowerUp Sound
		aPlayer.IncreaseSecondaryPower(3.f);
		aPlayer.PlayBonusAnimation();
		EnemyParticle::myCoinCount = 0;
	}
}

void EnemyParticle::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
	aRenderBuffer.Add(&myGlowAnimation);
}

void EnemyParticle::MoveTowardsPlayer(float aDeltaTime, Player & aPlayer)
{
	CommonUtilities::Vector3f directionToPlayer = aPlayer.GetPosition() - myPosition;

	if (directionToPlayer.Length() < myAbsorbRange)
	{
		mySpeed = 2.f;
		myPosition += directionToPlayer.GetNormalized() * mySpeed * aDeltaTime;

		if (directionToPlayer.Length() < myKillRange)
		{
			aPlayer.AddPlayerHealth(2.0f);
			Kill();
		}
	}
	else
	{
		myDirection += (directionToPlayer - myDirection) * aDeltaTime*0.1f;
		myPosition += myDirection * mySpeed * aDeltaTime;
	}
}

void EnemyParticle::SetTint(const CommonUtilities::Vector4f & aTint)
{
	myTint = aTint;
	myGlowTint = myTint.w;
	myAnimation.SetTint(myTint);
}

void EnemyParticle::SetElement(const eElement & aElement, bool aIsBoss)
{
	myElement = aElement;
	myIsBoss = aIsBoss;

	switch (myElement)
	{
	case eElement::Fire:
		SetTint({ 1.f, 0.f, 0.f, 1.f });
		break;
	case eElement::Ice:
		SetTint({ 0.f, 0.5f, 1.f, 1.f });
		break;
	case eElement::Lightning:
		SetTint({ 1.0f, 0.9f, 0.f, 1.f });
		break;
	}
}

#include <iostream>
void EnemyParticle::DeathAnimation()
{
	Animation* newSpark = new Animation("Assets/Images/Sprites/spark.dds");
	newSpark->Setup(6, 8, 48);
	newSpark->SetDuration(0.5f);
	newSpark->SetLoop(true);
	newSpark->SetPosition(GetPosition());
	newSpark->SetTint(myTint);
	newSpark->SetScale({ 1.f, 1.f });
	ExplosionManager::GetInstance()->AddExplosion(newSpark);

	
	++EnemyParticle::myCoinCount;
	++EnemyParticle::mySoulsCaught;

	if (EnemyParticle::myCoinTimer > 1.f)
	{
		if (EnemyParticle::myCoinPitch >= 1.5f)
		{
			++EnemyParticle::mySoulCombos;
		}
		EnemyParticle::myCoinPitch = 1.f;
		EnemyParticle::myCoinCount = 0;
	}
	EnemyParticle::myCoinTimer = 0.f;
	if (EnemyParticle::myLastSound < 0.f)
	{
		SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/Pickup.wav", 0.25f).SetPitch(EnemyParticle::myCoinPitch);
		EnemyParticle::myLastSound = 0.05f;
		EnemyParticle::myCoinPitch = CommonUtilities::Clamp(EnemyParticle::myCoinPitch + 0.05f, 1.f, 1.5f);
	}
}

void EnemyParticle::UpdateBossColor(float aDeltaTime)
{
	if (myIncreaseRG)
	{
		if (myTint.x < 1.0f)
		{
			myTint.x += aDeltaTime * 2.0f;
		}

		if (myTint.y < 1.0f)
		{
			myTint.y += aDeltaTime * 2.0f;
		}

		if (myTint.z > 0.0f)
		{
			myTint.z -= aDeltaTime * 2.0f;
		}

		if (myTint.x >= 1.0f && myTint.y >= 1.0f && myTint.z <= 0.0f)
		{
			myIncreaseRG = false;
			myIncreaseGB = true;
		}
	}
	else if (myIncreaseGB)
	{
		if (myTint.x > 0.0f)
		{
			myTint.x -= aDeltaTime * 2.0f;
		}

		if (myTint.y < 1.0f)
		{
			myTint.y += aDeltaTime * 2.0f;
		}

		if (myTint.z < 1.0f)
		{
			myTint.z += aDeltaTime * 2.0f;
		}

		if (myTint.x <= 0.0f && myTint.y >= 1.0f && myTint.z >= 1.0f)
		{
			myIncreaseGB = false;
			myIncreaseBR = true;
		}
	}
	else if (myIncreaseBR)
	{
		if (myTint.x < 1.0f)
		{
			myTint.x += aDeltaTime * 2.0f;
		}

		if (myTint.y > 0.0f)
		{
			myTint.y -= aDeltaTime * 2.0f;
		}

		if (myTint.z < 1.0f)
		{
			myTint.z += aDeltaTime * 2.0f;
		}

		if (myTint.x >= 1.0f && myTint.y <= 0.0f && myTint.z >= 1.0f)
		{
			myIncreaseBR = false;
			myIncreaseRG = true;
		}
	}
}

inline bool EnemyParticle::IsDead(Highscore& aHighscore)
{
	if (myHealth <= 0.f)
	{
		aHighscore.AddScore(myScoreFactor * EnemyParticle::myCoinPitch);
		return true;
	}
	return false;
}

#pragma once
#include "Enemy.h"

#include <Animation.h>
#include "CountDown.h"

class EnemyParticle : public Enemy
{
public:
	EnemyParticle();
	~EnemyParticle();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;

	void Update(float, Player&) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void MoveTowardsPlayer(float aDeltaTime, Player& aPlayer);
	void SetTint(const CommonUtilities::Vector4f& aTint);
	void SetElement(const eElement& aElement, bool aIsBoss);

	void DeathAnimation() override;
	void UpdateBossColor(float aDeltaTime);

	inline bool IsDead(Highscore& aHighscore) override;

	bool IsInside(const CommonUtilities::Vector2f& aPoint) override { aPoint; return false; }
	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override { aDamage; aDeltaTime; }
	bool IsBoss() { return false; }

	inline bool IsTouching(const CircleCollider* aCircleCollider) override { aCircleCollider; return false; }
	inline bool IsTouching(Projectile* aProjectile) override { aProjectile; return false; }
	inline bool IsTouching(Beam& aBeam) override { aBeam; return false; }

	inline static void UpdateCoinTimer(float aDeltaTime) { myCoinTimer += aDeltaTime; myLastSound -= aDeltaTime; }

	static int mySoulsCaught;
	static int mySoulCombos;

private:
	Animation myAnimation;
	Animation myGlowAnimation;

	float mySpeed;
	float myAbsorbRange;
	float myKillRange;

	bool myIsBoss;
	bool myIncreaseRG;
	bool myIncreaseGB;
	bool myIncreaseBR;

	CountDown myCountDown;

	bool myStop;

	CommonUtilities::Vector3f myDirection;
	CommonUtilities::Vector4f myTint;
	float myGlowTint;
	bool myIncreaseTint;

	eElement myElement;

	static float myCoinTimer;
	static float myCoinPitch;
	static float myLastSound;
	static int myCoinCount;
};

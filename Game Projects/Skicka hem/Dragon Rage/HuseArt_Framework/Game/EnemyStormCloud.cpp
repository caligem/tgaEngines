#include "stdafx.h"
#include "EnemyStormCloud.h"

#include "ProjectileManager.h"
#include "ProjectileFactory.h"
#include "Player.h"

namespace CU = CommonUtilities;

EnemyStormCloud::EnemyStormCloud()
	: myIsSlowed(false)
{

}

EnemyStormCloud::~EnemyStormCloud()
{

}

void EnemyStormCloud::Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition)
{
	myAnimation.Init("Assets/Images/Enemies/enemy_stormCloud.dds", aPosition);
	myAnimation.Setup(2, 3, 6);
	myAnimation.Update(0.f);
	myPosition = aPosition;
	myEnemyClass = aEnemyClass;
	myHealth = myEnemyClass.GetHP();
	myDamageTimer = 0.f;
}

void EnemyStormCloud::Update(float aDeltaTime, Player& aPlayer)
{
	aPlayer;
	myAnimation.Update(aDeltaTime);
	myDamageTimer -= aDeltaTime;
}

void EnemyStormCloud::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void EnemyStormCloud::TakeDamage(const float aDamage, const float aDeltaTime)
{
	aDamage;
	aDeltaTime;
}
 
bool EnemyStormCloud::IsInside(const CU::Vector2f& aPoint)
{
	return myAnimation.IsInside(aPoint);
}

inline const float EnemyStormCloud::GetDamage()
{
	return myEnemyClass.GetDMG();
}
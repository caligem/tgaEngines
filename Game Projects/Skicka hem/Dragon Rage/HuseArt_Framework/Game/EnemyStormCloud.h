#pragma once
#include "Enemy.h"

#include <Animation.h>

class Player;

class EnemyStormCloud : public Enemy
{
public:
	EnemyStormCloud();
	~EnemyStormCloud();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float aDeltaTime, Player& aPlayer) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsBoss() override { return false; }
	inline const float GetDamage() override;

	inline bool IsTouching(Projectile*) override { return false; }
	virtual inline bool IsTouching(Beam&) { return false; }

private:
	bool myIsSlowed;
	float mySlowTimer;
	float myDamageTimer;
	Animation myAnimation;
};


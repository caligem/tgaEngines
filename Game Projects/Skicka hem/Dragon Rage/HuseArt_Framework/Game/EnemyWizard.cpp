#include "stdafx.h"
#include "EnemyWizard.h"

#include "ProjectileManager.h"
#include "ProjectileFactory.h"
#include "Player.h"

#include <tga2d/error/error_manager.h>

namespace CU = CommonUtilities;

EnemyWizard ::EnemyWizard ()
	: myProjectileSpeed(0.0f)
	, myRotation(0)
	, myProjectileDirection(0.0f, 0.0f, 0.0f)
	, myFireRate(2.0f)
	, myXOffsetOne(0.0f)
	, myDuration(2.0f)
	, myIsSlowed(false)
	, myHoverDistance(0.03f)
	, myBoppingSpeed(1.0f)
{}


EnemyWizard ::~EnemyWizard ()
{
}

void EnemyWizard ::Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition)
{
	myPosition = aPosition;

	InitAnimation();
	myAnimationCharge.Init("Assets/Images/Projectiles/wizard_charge.dds", aPosition);
	myAnimationCharge.Setup(1, 4, 4);
	myAnimationCharge.SetDuration(0.8f);
	myAnimationCharge.SetLoop(false);
	myEnemyClass = aEnemyClass;

	myHealth = myEnemyClass.GetHP();
	myProjectileSpeed = myEnemyClass.GetProjectileSpeed();
	myFireRate = myEnemyClass.GetROF();
	myFireRate = 2.0f;
	myCooldown = myFireRate;
	myLockOn = 0.8f;

	myHoverTime = 0.f;
	myOriginalPosition = myPosition;

	myHealthBar.Init(myEnemyClass.GetHP(), { myPosition.x, myPosition.y + 0.14f, myPosition.z - 0.01f }, 2.f);
}

void EnemyWizard::Update(float aDeltaTime, Player& aPlayer)
{
	CU::Vector3f offset = myProjectileDirection.GetNormalized() * 0.01f;

	myCooldown -= aDeltaTime;

	if (myCooldown < 0.0f)
	{		

		myLockOn -= aDeltaTime;
		myAnimationCharge.Update(aDeltaTime);

		if (myLockOn < 0.0f && myAnimationCharge.IsFinished())
		{
			myDuration -= aDeltaTime;

			if (myDuration < 0.f)
			{
				myBeam.Dectivate();
				myCooldown = myFireRate;
				myDuration = 2.0f;
				myLockOn = 0.8f;
				myAnimationCharge.Setup(1, 4, 4);
			}
			else
			{
				myBeam.Activate();
			}
		}
	}
	else
	{
		myProjectileDirection = aPlayer.GetPosition() - myPosition;

		myBeam.SetEndPosition(myPosition + myProjectileDirection.GetNormalized() * 5.0f);
		myLaserSight.SetEndPosition(myPosition + myProjectileDirection.GetNormalized() * 5.0f);
	}

	myHoverTime += aDeltaTime * 2.f * Tga2D::Pif;
	myPosition = myOriginalPosition + CommonUtilities::Vector3f(0.f, myHoverDistance * std::sinf(myHoverTime * myBoppingSpeed), 0.f);

	myAnimation.Update(aDeltaTime);	
	myAnimation.SetPosition(myPosition);
	myAnimationCharge.SetPosition({ myPosition.x - 0.06f, myPosition.y + 0.06f, myPosition.z - 0.0001f });

	myLaserSight.SetStartPosition(CU::Vector3f({ myPosition.x - 0.06f, myPosition.y + 0.06f, myPosition.z }) + offset);
	myBeam.SetStartPosition(CU::Vector3f({ myPosition.x - 0.06f, myPosition.y + 0.06f, myPosition.z }) + offset);

	myBeam.Update(aDeltaTime, false);
	myLaserSight.Update(aDeltaTime, false);
	myHealthBar.Update(aDeltaTime, myHealth, { myPosition.x, myPosition.y + 0.14f, myPosition.z - 0.01f });

	DamagedFlash(myAnimation);
	myFlashTimer -= aDeltaTime;
}

void EnemyWizard::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);

	if (myCooldown < 0.0f && myDuration == 2.0f)
	{
		aRenderBuffer.Add(&myAnimationCharge);
	}

	if (myBeam.IsActive())
	{
		myBeam.FillRenderBuffer(aRenderBuffer);
	}
	else
	{
		myLaserSight.FillRenderBuffer(aRenderBuffer);
	}
	myHealthBar.FillRenderBuffer(aRenderBuffer);
}

void EnemyWizard::TakeDamage(const float aDamage, const float aDeltaTime)
{
	myHealth -= (aDamage * aDeltaTime);
	myHealthBar.Show();

	if (myFlashTimer < 0.8f)
	{
		myFlashTimer = 1.0f;
	}
}

bool EnemyWizard ::IsInside(const CU::Vector2f & aPoint)
{
	return myAnimation.IsInside(aPoint);
}

inline bool EnemyWizard::IsTouching(const CircleCollider * aCircleCollider)
{

	if (myCollider.IsTouching(aCircleCollider))
	{
		return true;
	}

	return false;
}

inline bool EnemyWizard::BeamCollision(const CircleCollider * aCircleCollider)
{
	if (myBeam.IsTouching(aCircleCollider))
	{
		return true;
	}

	return false;
}

void EnemyWizard::InitAnimation()
{
	switch (myElement)
	{
	case eElement::Fire:
		myAnimation.Init("Assets/Images/Enemies/enemy_wizard_fire.dds");
		myBeam.Init(eBeamType::WizardFire);
		break;
	case eElement::Ice:
		myAnimation.Init("Assets/Images/Enemies/enemy_wizard_ice.dds");
		myBeam.Init(eBeamType::WizardIce);
		break;
	case eElement::Lightning:
		myAnimation.Init("Assets/Images/Enemies/enemy_wizard_lightning.dds");
		myBeam.Init(eBeamType::WizardLightning);
		break;
	}
	myLaserSight.Init(eBeamType::LaserSight);
	myLaserSight.Activate();

	myAnimation.SetPosition(myPosition);
	myAnimation.Update(0.f);
}

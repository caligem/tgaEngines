#pragma once
#include "Enemy.h"
#include "Beam.h"

#include <Animation.h>

class Player;

class EnemyWizard : public Enemy
{
public:
	EnemyWizard();
	~EnemyWizard();

	void Init(const EnemyClass& aEnemyClass, const CommonUtilities::Vector3f aPosition) override;
	void Update(float aDeltaTime, Player& aPlayer) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void TakeDamage(const float aDamage, const float aDeltaTime = 1) override;
	bool IsInside(const CommonUtilities::Vector2f& aPoint) override;
	bool IsBoss() override { return false; }

	inline bool IsTouching(const CircleCollider* aCircleCollider) override;

	inline bool BeamCollision(const CircleCollider* aCircleCollider) override;

private:
	void InitAnimation();

	bool myIsSlowed;
	float mySlowTimer;
	float myProjectileSpeed;
	float myRotation;

	float myXOffsetOne;
	float myFireRate;
	float myCooldown;
	float myLockOn;
	float myDuration;

	float myHoverDistance;
	float myBoppingSpeed;
	float myHoverTime;
	CommonUtilities::Vector3f myOriginalPosition;

	Beam myBeam;
	Beam myLaserSight;
	Animation myAnimation;
	Animation myAnimationCharge;
	CommonUtilities::Vector3f myProjectileDirection;
};
#include "stdafx.h"
#include "ExplosionManager.h"

#include <Animation.h>

ExplosionManager* ExplosionManager::ourInstance = nullptr;

ExplosionManager::ExplosionManager()
{}

ExplosionManager::~ExplosionManager()
{}

void ExplosionManager::Create()
{
	if (ourInstance == nullptr)
	{
		ourInstance = new ExplosionManager();
		ourInstance->myExplosions.Init(32);
	}
	else
	{
		ourInstance->myExplosions.DeleteAll();
	}
}

void ExplosionManager::Destroy()
{
	if (ourInstance == nullptr)
	{
		return;
	}
	ourInstance->myExplosions.DeleteAll();
	SAFE_DELETE(ourInstance);
}

void ExplosionManager::Update(float aDeltaTime)
{
	for (Animation* explosion : myExplosions)
	{
		explosion->Update(aDeltaTime);
	}
}

void ExplosionManager::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	for (Animation* explosion : myExplosions)
	{
		if (explosion->IsFinished())continue;

		aRenderBuffer.Add(explosion);
	}
}

void ExplosionManager::CleanUp()
{
	for (unsigned short i = myExplosions.Size(); i > 0; --i)
	{
		if (myExplosions[i - 1]->IsFinished())
		{
			myExplosions.DeleteCyclicAtIndex(i - 1);
		}
	}
}

void ExplosionManager::AddExplosion(Animation * aAnimation)
{
	myExplosions.Add(aAnimation);
}

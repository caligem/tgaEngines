#pragma once

#include "Vector.h"
#include <GrowingArray.h>

class Drawable;
class Animation;

class ExplosionManager
{
public:
	ExplosionManager();
	~ExplosionManager();

	static ExplosionManager* GetInstance() { return ourInstance; }

	static void Create();
	static void Destroy();

	void Update(float aDeltaTime);
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer);
	void CleanUp();

	void AddExplosion(Animation* aAnimation);

private:
	static ExplosionManager* ourInstance;

	CommonUtilities::GrowingArray<Animation*> myExplosions;

};


#include "stdafx.h"
#include "FireStance.h"

#include "ProjectileFactory.h"
#include "ProjectileManager.h"
#include "SFXManager.h"

#include "Player.h"

namespace CU = CommonUtilities;

FireStance::FireStance(Player & aPlayer, Screenshake & aScreenshake)
	: myPlayer(aPlayer)
	, Stance(aScreenshake)
{}

FireStance::~FireStance()
{
}

void FireStance::Init()
{
	myElement = eElement::Fire;

	myFlyingAnimation.Init("Assets/Images/Player/playerDragonFireFlying.dds");
	myFlyingAnimation.Setup(2, 2, 4);
	myGlideAnimation.Init("Assets/Images/Player/playerDragonFire.dds");
	//myFlyingAnimation.Setup(1, 1, 1);
	//myFlyingAnimation.SetDuration(.5f);

	/*
	myShootingAnimation.Init("Assets/Images/Sprites/dragon_shoot_red.dds");
	myShootingAnimation.Setup(1, 3, 3);
	myShootingAnimation.SetDuration(.2f);
	*/
	myCurrentAnimation = &myFlyingAnimation;
}

void FireStance::Update(float aDeltaTime)
{
	Stance::Update(aDeltaTime);
	ManageShootingAnimation();
}

void FireStance::Shoot(eShootType aShootType)
{
	if (aShootType == eShootType::Primary)
	{
		if (myPrimaryShootCooldown <= 1.f / myStats.primaryRateOfFire)
		{
			return;
		}

		myProjectileSpeed = 2.f;

		ProjectileFireBall* projectile = ProjectileFactory::CreateFireBall(myPosition, myProjectileSpeed, CU::Vector3f(0.f, 1.f, 0.f));
		projectile->SetDamage(myStats.primaryDamage);
		projectile->SetElement(eElement::Fire);
		ProjectileManager::GetInstance()->PlayerShoot(projectile);
	
		SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/Fireball.wav", 0.3f);

		myPrimaryShootCooldown = 0.f;
	}
	else if (aShootType == eShootType::Secondary)
	{
		if (mySecondaryShootCooldown <= 1.f / myStats.secondaryRateOfFire)
		{
			return;
		}

		myProjectileSpeed = 1.5f;

		for (int i = 0; i < 5; i++)
		{
			ProjectileFireBreath* projectile = ProjectileFactory::CreateFireBreath(myPosition, myProjectileSpeed, CU::Vector3f(-0.1f + 0.05f*static_cast<float>(i), 1.f, 0.f));
			projectile->SetDamage(myStats.secondaryDamage);
			projectile->SetElement(eElement::Fire);
			ProjectileManager::GetInstance()->PlayerShoot(projectile);
		}

		SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/Firebreath.wav", 0.7f);

		mySecondaryShootCooldown = 0.f;
		myScreenshake.Shake(0.3f, { 0.025f, 0.025f, 0.0f });
	}
}

void FireStance::ManageShootingAnimation()
{
	if (myPrimaryShootCooldown < 0.1f || mySecondaryShootCooldown < 0.1f)
	{
		SetState(eState::Shooting);
	}
	else
	{
		SetState(eState::Flying);
	}
}
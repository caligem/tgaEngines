#pragma once
#include "Stance.h"

class FireStance : public Stance
{
public:
	FireStance(Player& aPlayer, Screenshake& aScreenshake);
	~FireStance();

	void Init() override;
	void Update(float aDeltaTime) override;

	void Shoot(eShootType aShootType) override;

	void ManageShootingAnimation();

	eStance GetStance() override { return eStance::Fire; }

private:
	Player& myPlayer;
};


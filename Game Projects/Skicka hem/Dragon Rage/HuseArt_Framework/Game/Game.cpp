#include "stdafx.h"
#include <tga2d/Engine.h>
#include "Game.h"

#include <tga2d/error/error_manager.h>
#include <DL_Debug.h>

using namespace std::placeholders;

#ifdef _DEBUG
#pragma comment(lib,"DX2DEngine_Debug.lib")
#endif // DEBUG
#ifdef _RELEASE
#pragma comment(lib,"DX2DEngine_Release.lib")
#endif // RELEASE
#ifdef _RETAIL
#pragma comment(lib,"DX2DEngine_Retail.lib")
#endif // RETAIL


CGame::CGame() :
	myGameWorld(myInputManager, myTimer)
{
#ifdef _DEBUG
	DL_Debug::Debug::Create("Log_Game_DEBUG.txt");
#endif // DEBUG
#ifdef _RELEASE
	DL_Debug::Debug::Create("Log_Game_RELEASE.txt");
#endif
#ifdef _RETAIL
	DL_Debug::Debug::Create("Log_Game_RETAIL.txt");
#endif
}

CGame::~CGame()
{
	DL_Debug::Debug::Destroy();
}

void GetResolution();
void SetResolution(unsigned short aWidth, unsigned short aHeight, bool aFullscreen);
static unsigned short windowWidth = 1440;
static unsigned short windowHeight = 900;
static bool windowFullscreen = false;

bool CGame::Init(const std::wstring& aVersion, HWND aHWND)
{
	GetResolution();

    Tga2D::SEngineCreateParameters createParameters;
	createParameters.myActivateDebugSystems = Tga2D::eDebugFeature_Drawcalls | Tga2D::eDebugFeature_Fps | Tga2D::eDebugFeature_Mem;
    
    createParameters.myInitFunctionToCall = std::bind( &CGame::InitCallBack, this );
    createParameters.myUpdateFunctionToCall = std::bind( &CGame::UpdateCallBack, this );
	createParameters.myWinProcCallback = std::bind(&CGame::WinProc, this, _1, _2, _3, _4);
    createParameters.myLogFunction = std::bind( &CGame::LogCallback, this, _1 );
    createParameters.myWindowHeight = windowHeight;
    createParameters.myWindowWidth = windowWidth;
	createParameters.myRenderHeight = windowHeight;
	createParameters.myRenderWidth = windowWidth;
	createParameters.myTargetWidth = windowWidth;
	createParameters.myTargetHeight = windowHeight;
	createParameters.myWindowSetting = Tga2D::EWindowSetting_Overlapped;
	createParameters.myAutoUpdateViewportWithWindow = true;
    createParameters.myClearColor.Set(0.f, 0.f, 0.f, 1.f);
	createParameters.myStartInFullScreen = windowFullscreen;
	createParameters.myEnableVSync = false;

	if (aHWND != nullptr)
	{
		createParameters.myHwnd = new HWND(aHWND);
	}

	#ifdef _DEBUG
		//std::wstring appname = L"TGA 2D DEBUG  [" + aVersion + L"] ";
		std::wstring appname = L"Dragon Rage DEBUG [" + aVersion + L"]";
	#endif
	#ifdef _RELEASE
		//std::wstring appname = L"TGA 2D RELEASE [" + aVersion + L"] ";
		std::wstring appname = L"Dragon Rage RELEASE [" + aVersion + L"]";
	#endif
	#ifdef _RETAIL
		//std::wstring appname = L"TGA 2D RETAIL  [" + aVersion + L"] ";
		aVersion;
		std::wstring appname = L"Dragon Rage";
	#endif // _RETAIL

    createParameters.myApplicationName = appname;

    Tga2D::CEngine::CreateInstance( createParameters );

    if( !Tga2D::CEngine::GetInstance()->Start() )
    {
        ERROR_PRINT( "Fatal error! Engine could not start!" );
		system("pause");
		return false;
    }

	return true;
}

LRESULT CGame::WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_KEYDOWN: case WM_KEYUP:
	case WM_LBUTTONDOWN: case WM_RBUTTONDOWN: case WM_MBUTTONDOWN:
	case WM_LBUTTONUP: case WM_RBUTTONUP: case WM_MBUTTONUP:
	case WM_MOUSEMOVE: case WM_MOUSEWHEEL:
		myInputManager.OnInput(message, wParam, lParam);
		break;
		// this message is read when the window is closed
	case WM_DESTROY:
		// close the application entirely
		PostQuitMessage(0);
		return 0;
		break;
	}

	hWnd;
	return 0;
}

void CGame::InitCallBack()
{
    myGameWorld.Init();
	myTimer.Update();
}

void CGame::UpdateCallBack()
{
	myTimer.Update();

	myGameWorld.Update();
	myGameWorld.Render();

	myInputManager.Update();

	if (myGameWorld.HasQuit())
	{
		Tga2D::CEngine::GetInstance()->SetFullScreen(false);
		Tga2D::CEngine::GetInstance()->Shutdown();
	}
}

void CGame::LogCallback( std::string aText )
{
}






// DIALOG ON STARTUP

#include "../Launcher_Game/resource.h"

#define BG DKGRAY_BRUSH
#define BGEDIT LTGRAY_BRUSH

#include <string>
#include <algorithm>
#include <regex>
#include <iostream>

INT_PTR CALLBACK DlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(hDlg);
	UNREFERENCED_PARAMETER(lParam);

	char buffer[80];

	switch( message )
	{
	case WM_INITDIALOG: 
	{
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_ADDSTRING, 0, (LPARAM)L"01.3840 x 2160");
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_ADDSTRING, 0, (LPARAM)L"02.2560 x 1600");
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_ADDSTRING, 0, (LPARAM)L"03.2560 x 1440");
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_ADDSTRING, 0, (LPARAM)L"04.1920 x 1200");
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_ADDSTRING, 0, (LPARAM)L"05.1920 x 1080");
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_ADDSTRING, 0, (LPARAM)L"06.1680 x 1050");
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_ADDSTRING, 0, (LPARAM)L"07.1440 x 900");
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_ADDSTRING, 0, (LPARAM)L"08.1280 x 800");
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_ADDSTRING, 0, (LPARAM)L"09.1280 x 720");
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_ADDSTRING, 0, (LPARAM)L"10.1024 x 576");
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_ADDSTRING, 0, (LPARAM)L"11.768 x 432");
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_ADDSTRING, 0, (LPARAM)L"12.640 x 360");
		SendMessage(GetDlgItem(hDlg, IDC_RESOLUTION), CB_SELECTSTRING, 0, (LPARAM)L"05.1920 x 1080");

		break;
	}
	case WM_COMMAND:
		switch( LOWORD(wParam) )
		{
		case IDC_RESOLUTION:
			break;
		case IDC_PLAY:
			bool fullscreen = IsDlgButtonChecked(hDlg, IDC_FULLSCREEN) == 1;
			GetWindowTextA(GetDlgItem(hDlg, IDC_RESOLUTION), buffer, 80);

			std::string res(buffer);

			std::regex resMatcher("(?:[0-9]*).([0-9]+) x ([0-9]+)");
			std::smatch matches;
			if (std::regex_match(res, matches, resMatcher))
			{
				SetResolution(
					static_cast<unsigned short>(std::atoi(matches[1].str().c_str())),
					static_cast<unsigned short>(std::atoi(matches[2].str().c_str())),
					fullscreen
				);
			}

			EndDialog(hDlg, TRUE);

			break;
		}
		break;
	case WM_CTLCOLOREDIT:
		SetBkMode((HDC)wParam, TRANSPARENT);
        SetTextColor((HDC)wParam, RGB(0, 0, 0));
        return (INT_PTR)( (HBRUSH)GetStockObject(BGEDIT) );
	case WM_CTLCOLORSTATIC:
		SetBkMode((HDC)wParam, TRANSPARENT);
        SetTextColor((HDC)wParam, RGB(255, 255, 255));
        return (INT_PTR)( (HBRUSH)GetStockObject(BG) );
	case WM_CTLCOLORDLG:
		return (INT_PTR)GetStockObject(BG);
		break;
	case WM_CLOSE:
		DestroyWindow(hDlg);
		return FALSE;
		break;
	}

	return FALSE;
}

void GetResolution()
{
	DialogBox(NULL, MAKEINTRESOURCE(IDD_GET_RESOLUTION), NULL, DlgProc);
}
void SetResolution(unsigned short aWidth, unsigned short aHeight, bool aFullscreen)
{
	windowWidth = aWidth;
	windowHeight = aHeight;
	windowFullscreen = aFullscreen;
}

#pragma once
#include <tga2d/Engine.h>
#include "GameWorld.h"

#include "../CommonUtilities/InputManager.h"
#include "../CommonUtilities/Timer.h"

class CGame
{
public:
	CGame();
	~CGame();
	bool Init(const std::wstring& aVersion = L"", HWND aHWND = nullptr);
private:
	LRESULT WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void InitCallBack();
	void UpdateCallBack();
	void LogCallback(std::string aText);

	CGameWorld myGameWorld;

	CommonUtilities::InputManager myInputManager;
	CommonUtilities::Timer myTimer;
};

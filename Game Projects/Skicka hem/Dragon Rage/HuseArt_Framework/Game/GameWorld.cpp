#include "stdafx.h"
#include "GameWorld.h"

#include "tga2d/engine.h"
#include <tga2d/sprite/sprite.h>
#include <tga2d/math/common_math.h>

#include <InputManager.h>
#include <Timer.h>

#include <Tweener.h>

#include <fstream>
#include <Parser\json.hpp>

#include "Blackboard.h"

#include <Mathf.h>

#include <Random.h>

#include "WidgetFactory.h"
#include "SpriteData.h"

namespace CU = CommonUtilities;

CGameWorld::CGameWorld(const CU::InputManager &aInputManager, const CU::Timer &aTimer)
	: myInputManager(aInputManager)
	, myTimer(aTimer)
	, myXboxController(0)
	, myCurrentScene(nullptr)
	, myFadeTimer(0.f)
	, myFadeDuration(0.5f)
	, myFadeDistance(0.30f)
	, myPauseFadeDistance(0.2f)
	, myCanPause(false)
	, myIsPaused(false)
	, myFadeMenuMusic(true)
	, myHasQuit(false)
	, myPauseWidgetSystem(aInputManager, myXboxController)
{
}

CGameWorld::~CGameWorld()
{
	Blackboard::Destroy();

	myMenuMusic.Release();
	myCutsceneMusic.Release();
}

void CGameWorld::Init()
{
	Drawable::ourScreenRatio = static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().x) / Tga2D::CEngine::GetInstance()->GetWindowSize().y;
	Drawable::ourScreenScale = static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().y) / 1080.f;

	Tga2D::CEngine::GetInstance()->SetSampler(ESamplerType::ESamplerType_Point);
	CommonUtilities::InitRand();
	ShowCursor(FALSE);

	InitCamera();

	myOverlay.Init("Assets/Images/GUI/overlay.dds");
	myOverlay.SetScale({ 1000.f, 1000.f });
	myOverlay.SetUVScale({ 1000.f, 1000.f });

	InitPauseMenu();

	Blackboard::Create();

	std::ifstream i("Assets/Data/main.haf");
	nlohmann::json j;
	i >> j;

	RequestScene(j["startScene"].get<std::string>());

	myMenuMusic.Init("Assets/Audio/music/dragonrageMenu.wav", &mySoundSystem);
	myMenuMusic.SetVolume(0.2f);
	myMenuMusic.SetLoop(true);
	myMenuMusic.Play();
	myMenuMusic.Fade(0.f);

	myCutsceneMusic.Init("Assets/Audio/music/dragonrageChill.wav", &mySoundSystem);
	myCutsceneMusic.SetVolume(0.2f);
	myCutsceneMusic.SetLoop(true);
	myCutsceneMusic.Play();
	myCutsceneMusic.Fade(0.f);
}

void CGameWorld::Update()
{ 
	float dt = CommonUtilities::Clamp(myTimer.GetDeltaTime(), 0.f, 1.f/60.f);

	if (myState == eFadeState::None)
	{
		if (myCurrentScene != nullptr)
		{
			if (myCanPause && !myCurrentScene->IsMenu())
			{
				if (myInputManager.IsKeyPressed(CU::Key_Escape) || (myXboxController.IsConnected() && myXboxController.IsButtonPressed(CommonUtilities::XButton_Start)))
				{
					myIsPaused = !myIsPaused;
				}
			}

			if (myCurrentScene->RequestingQuitGame())
			{
				SwitchState(eFadeState::Out);
			}
			if (myCurrentScene->RequestingSceneLoad())
			{
				RequestScene(myCurrentScene->GetSceneToLoad().c_str());
			}
			else
			{
				if(myIsPaused && !myCurrentScene->IsMenu())
				{
					UpdatePauseMenu(dt);
				}
				else
				{
					UpdateScene(dt);
				}
			}
		}
	}
	else
	{
		FadeScene(dt);
	}
	
	if (!myIsPaused)
	{
		Tweener::Update(dt);
	}

	mySoundSystem.Update();
}

void CGameWorld::Render()
{ 
	if (myCurrentScene != nullptr)
	{
		myCurrentScene->Render();

		if(myIsPaused && !myCurrentScene->IsMenu() && myState == eFadeState::None)
		{
			RenderPauseMenu();
		}
	}

	if (myState != eFadeState::None)
	{
		myOverlay.CalculatePoint(myCamera);
		myOverlay.Render();
	}
}

void CGameWorld::InitCamera()
{
	float r = (static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().x) / static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().y));
	float fovY = Tga2D::DegToRad(45.f);
	float yScale = 1.f / std::tanf(fovY);
	float xScale = 1.f / (r * std::tanf(fovY));

	myCamera.Init({
		xScale, 0.f, 0.f, 0.f,
		0.f, yScale, 0.f, 0.f,
		0.f, 0.f, 1.f, 1.f,
		0.f, 0.f, 0.f, 1.f
	});
	myCamera.SetPosition({ 0.f, 0.f, 0.f });

	myGuiCamera.Init({
		xScale, 0.f, 0.f, 0.f,
		0.f, yScale, 0.f, 0.f,
		0.f, 0.f, 1.f, 1.f,
		0.f, 0.f, 0.f, 1.f
	});
	myGuiCamera.SetPosition({ 0.f, 0.f, 0.f });
}

void CGameWorld::LoadScene(const std::string & aPath)
{
	Blackboard::GetInstance()->SetNote("previousScene", Blackboard::GetInstance()->GetNote("currentScene"));
	Blackboard::GetInstance()->SetNote("currentScene", aPath);

	if (myCurrentScene != nullptr)
	{
		myPreviousSceneType = GetSceneType(myCurrentScene);
		SAFE_DELETE(myCurrentScene);
	}
	myCurrentScene = SceneLoader::LoadScene(aPath, myInputManager, myCamera, myGuiCamera, myXboxController, mySoundSystem);
	myCurrentSceneType = GetSceneType(myCurrentScene);
}

void CGameWorld::RequestScene(const std::string & aPath)
{
	mySceneToLoad = aPath;
	if (myCurrentScene == nullptr)
	{
		SwitchState(eFadeState::Load);
	}
	else
	{
		SwitchState(eFadeState::Out);
	}
}

void CGameWorld::UpdateScene(float aDeltaTime)
{
	myCurrentScene->Update(aDeltaTime);
	myXboxController.Update();
}

void CGameWorld::FadeScene(float aDeltaTime)
{
	myFadeTimer += aDeltaTime;

	float alpha = myFadeTimer / myFadeDuration;

	alpha = alpha*alpha*(3.0f - 2.0f*alpha);

	switch (myState)
	{
	case eFadeState::Out:
		if (myIsPaused)
		{
			myOverlay.SetPosition(myCamera.GetPosition() + CU::Vector3f(0.f, 0.f, myPauseFadeDistance + alpha*(myFadeDistance - myPauseFadeDistance)));
		}
		else
		{
			myOverlay.SetPosition(myCamera.GetPosition() + CU::Vector3f(0.f, 0.f, alpha*myFadeDistance));
		}
		myCurrentScene->Fade(1.f - alpha);
		myCanPause = false;
		if (myCurrentScene->RequestingQuitGame())
		{
			myMenuMusic.Fade(1.f - alpha);
		}
		if (myFadeTimer >= myFadeDuration)
		{
			if (myCurrentScene->RequestingQuitGame())
			{
				SwitchState(eFadeState::Quit);
			}
			else
			{
				myFadeMenuMusic = !myCurrentScene->IsMenu();
				SwitchState(eFadeState::Load);
			}
		}
		break;
	case eFadeState::Load:
		myIsPaused = false;
		LoadScene(mySceneToLoad);
		SwitchState(eFadeState::In);
		break;
	case eFadeState::In:
		myOverlay.SetPosition(myCamera.GetPosition() + CU::Vector3f(0.f, 0.f, myFadeDistance-alpha*myFadeDistance));
		if (myCurrentScene != nullptr)
		{
			FadeMusic(alpha);
			myCurrentScene->Fade(alpha);
			myCanPause = false;
		}
		if (myFadeTimer >= myFadeDuration)
		{
			SwitchState(eFadeState::None);
			myCanPause = true;
		}
		break;
	case eFadeState::Quit:
		myHasQuit = true;
		break;
	}
}

void CGameWorld::FadeMusic(float aAlpha)
{
	switch (myCurrentSceneType)
	{
	case eSceneType::Menu:
		switch (myPreviousSceneType)
		{
		case eSceneType::Cutscene:
			myCutsceneMusic.Fade(1.f - aAlpha);
			myMenuMusic.Fade(aAlpha);
			break;
		case eSceneType::Level:
			myCutsceneMusic.Fade(0.f);
			myMenuMusic.Fade(aAlpha);
			break;
		case eSceneType::Menu:
			break;
		default:
			myMenuMusic.Fade(aAlpha);
			break;
		}
		break;
	case eSceneType::Cutscene:
		switch (myPreviousSceneType)
		{
		case eSceneType::Menu:
			myCutsceneMusic.Fade(aAlpha);
			myMenuMusic.Fade(1.f - aAlpha);
			break;
		case eSceneType::Level:
			myCutsceneMusic.Fade(aAlpha);
			myMenuMusic.Fade(0.f);
			break;
		case eSceneType::Cutscene:
			break;
		case eSceneType::None:
			myCutsceneMusic.Fade(aAlpha);
			break;
		}
		break;
	case eSceneType::Level:
		switch (myPreviousSceneType)
		{
		case eSceneType::Menu:
			myMenuMusic.Fade(1.f - aAlpha);
			break;
		case eSceneType::Cutscene:
			myCutsceneMusic.Fade(1.f - aAlpha);
			break;
		case eSceneType::Level:
			break;
		case eSceneType::None:
			break;
		}
		break;
	}
}

void CGameWorld::InitPauseMenu()
{
	myPauseBuffer.Init(4);
	myPauseWidgetSystem.Init();

	//Resume Game
	{
		nlohmann::json::object_t data = nlohmann::json::object({
			{"texture", "Assets/Images/GUI/button_resume_game.dds"},
			{"position", nlohmann::json::array({0.f, 0.65f, 1.f})},
			{"scale", nlohmann::json::array({1.f, 1.f})},
			{"rotation", 0.f}
		});
		WidgetButton* button = WidgetFactory::CreateButton(data);
		button->SetCallback([=]()
		{
			myIsPaused = false;
		});
		myPauseWidgetSystem.AddWidget(button);
	}

	//Master Volume Slider
	{
		nlohmann::json::object_t data = nlohmann::json::object({
			{"texture", "Assets/Images/GUI/Options Menu/slider.dds"},
			{"position", nlohmann::json::array({0.f, 0.4f, 1.f})},
			{"scale", nlohmann::json::array({1.f, 1.f})},
			{"rotation", 0.f}
		});
		WidgetSlider* slider = WidgetFactory::CreateSlider(data);
		slider->SetCallback([=](float aValue){
			mySoundSystem.SetVolume(aValue);

			std::string label = "Volume: ";
			label += std::to_string(static_cast<int>(std::roundf(mySoundSystem.GetVolume()*100.f)));
			label += "%";
			slider->SetLabel(label);
		});
		slider->SetValue(mySoundSystem.GetVolume());
		std::string label = "Volume: ";
		label += std::to_string(static_cast<int>(std::roundf(mySoundSystem.GetVolume()*100.f)));
		label += "%";
		slider->SetLabel(label);
		myPauseWidgetSystem.AddWidget(slider);
	}

	//Level Select
	{
		nlohmann::json::object_t data = nlohmann::json::object({
			{"texture", "Assets/Images/GUI/button_level_select.dds"},
			{"position", nlohmann::json::array({0.f, 0.25f, 1.f})},
			{"scale", nlohmann::json::array({1.f, 1.f})},
			{"rotation", 0.f}
		});
		WidgetButton* button = WidgetFactory::CreateButton(data);
		button->SetCallback([=]()
		{
			RequestScene("Assets/Data/menus/menu.hafl");
		});
		myPauseWidgetSystem.AddWidget(button);
	}
	
	//Quit to main menu
	{
		nlohmann::json::object_t data = nlohmann::json::object({
			{"texture", "Assets/Images/GUI/button_quit_to_main_menu.dds"},
			{"position", nlohmann::json::array({0.f, 0.1f, 1.f})},
			{"scale", nlohmann::json::array({1.f, 1.f})},
			{"rotation", 0.f}
		});
		WidgetButton* button = WidgetFactory::CreateButton(data);
		button->SetCallback([=]()
		{
			RequestScene("Assets/Data/menus/mainmenu.hafl");
		});
		myPauseWidgetSystem.AddWidget(button);
	}

	//controller image
	{
		nlohmann::json::object_t data = nlohmann::json::object({
			{"texture", "Assets/Images/GUI/Options Menu/controller.dds"},
			{"position", nlohmann::json::array({-0.6f, -0.3f, 1.f})},
			{"scale", nlohmann::json::array({1.f, 1.f})},
			{"rotation", 0.f}
		});
		WidgetImage* controllerImage = WidgetFactory::CreateImage(data);
		myPauseWidgetSystem.AddWidget(controllerImage);
	}

	//keyboard image
	{
		nlohmann::json::object_t data = nlohmann::json::object({
			{"texture", "Assets/Images/GUI/Options Menu/keyboard.dds"},
			{"position", nlohmann::json::array({0.5f, -0.3f, 1.f})},
			{"scale", nlohmann::json::array({1.f, 1.f})},
			{"rotation", 0.f}
		});
		WidgetImage* keyboardImage = WidgetFactory::CreateImage(data);
		myPauseWidgetSystem.AddWidget(keyboardImage);
	}
}

void CGameWorld::UpdatePauseMenu(float aDeltaTime)
{
	myOverlay.SetPosition(myCamera.GetPosition() + CU::Vector3f(0.f, 0.f, myPauseFadeDistance));

	myPauseWidgetSystem.Update(aDeltaTime);

	myXboxController.Update();
}

void CGameWorld::RenderPauseMenu()
{
	myOverlay.CalculatePoint(myCamera);
	myOverlay.Render();

	myPauseBuffer.RemoveAll();
	myPauseWidgetSystem.FillRenderBuffer(myPauseBuffer);

	for (Drawable* drawable : myPauseBuffer)
	{
		drawable->CalculatePoint(myGuiCamera);
	}

	std::stable_sort(myPauseBuffer.begin(), myPauseBuffer.end(), [](Drawable* a, Drawable* b)
	{
		if (a->GetPosition().z == b->GetPosition().z)
		{
			return a->GetPosition().y > b->GetPosition().y;
		}
		return a->GetPosition().z > b->GetPosition().z;
	});

	for	(Drawable* drawable : myPauseBuffer)
	{
		drawable->Render();
	}
}

eSceneType CGameWorld::GetSceneType(Scene * aScene)
{
	if (aScene->IsMenu())
	{
		if (aScene->IsCutscene())
		{
			return eSceneType::Cutscene;
		}
		else
		{
			return eSceneType::Menu;
		}
	}
	else
	{
		return eSceneType::Level;
	}
}

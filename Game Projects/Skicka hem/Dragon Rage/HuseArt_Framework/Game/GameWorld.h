#pragma once

#include <string>

namespace CommonUtilities
{
	class InputManager;
	class Timer;
}

#include <XBOXController.h>
#include <Camera.h>
#include <GrowingArray.h>

#include "SceneLoader.h"
#include "Sprite.h"
#include "Text.h"

#include "SoundSystemClass.h"
#include "AudioSource.h"

#include "CountDown.h"

#include "WidgetSystem.h"

enum class eFadeState
{
	None,
	Out,
	Load,
	In,
	Quit
};

enum class eSceneType
{
	None,
	Menu,
	Cutscene,
	Level
};

class CGameWorld
{
public:
	CGameWorld(const CommonUtilities::InputManager&, const CommonUtilities::Timer&); 
	~CGameWorld();

	void Init();
	void Update(); 
	void Render(); 

	inline bool HasQuit() const { return myHasQuit; }

private:
	void InitCamera();
	void LoadScene(const std::string& aPath);
	void RequestScene(const std::string& aPath);

	void UpdateScene(float aDeltaTime);
	void FadeScene(float aDeltaTime);
	void FadeMusic(float aAlpha);
	inline void SwitchState(eFadeState aState) { myState = aState; myFadeTimer = 0.f; }

	void InitPauseMenu();
	void UpdatePauseMenu(float aDeltaTime);
	void RenderPauseMenu();

	eSceneType GetSceneType(Scene* aScene);

	const CommonUtilities::InputManager& myInputManager;
	const CommonUtilities::Timer& myTimer;

	CommonUtilities::XBOXController myXboxController;
	SoundSystemClass mySoundSystem;

	CommonUtilities::Camera myCamera;
	CommonUtilities::Camera myGuiCamera;

	Sprite myOverlay;
	eFadeState myState;
	Scene* myCurrentScene;
	std::string mySceneToLoad;
	float myFadeTimer;
	float myFadeDuration;
	float myFadeDistance;
	float myPauseFadeDistance;

	CommonUtilities::GrowingArray<Drawable*> myPauseBuffer;
	WidgetSystem myPauseWidgetSystem;
	bool myCanPause;
	bool myIsPaused;

	bool myFadeMenuMusic;
	AudioSource myMenuMusic;
	AudioSource myCutsceneMusic;

	bool myHasQuit;
	
	eSceneType myCurrentSceneType;
	eSceneType myPreviousSceneType;
};
#include "stdafx.h"
#include "HealthBar.h"


void HealthBar::Init(float aMaxValue, const CommonUtilities::Vector3f & aPosition, float aShowTimer, const CommonUtilities::Vector2f & aScale, bool aIsPlayer, const char * aFramePath, const char * aSubstancePath)
{
	myIsPlayer = aIsPlayer;
	myScale = aScale;
	mySubstance.Init(aFramePath);
	myFrame.Init(aSubstancePath);
	SetMaxValue(aMaxValue);
	SetCurrentValue(aMaxValue);
	SetPosition(aPosition);
	SetShowTimer(aShowTimer);
	myFrame.SetScale(myScale);
	mySubstance.SetScale(myScale);

	myTimer = 0.f;

	if (!myIsPlayer)
	{
		myBackground.Init("Assets/Images/Sprites/healthbar_back.dds");
	}
}

void HealthBar::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer, float aFlashTimer)
{
	if (myTimer > 0.f)
	{
		aRenderBuffer.Add(&myFrame);
		if (static_cast<int>(aFlashTimer * 10.f) % 2 == 0)
		{
			mySubstance.SetTint({ 1.0f, 1.0f, 1.0f, 1.0f });
		}
		else
		{
			mySubstance.SetTint({ 100.0f, 100.0f, 100.0f, 1.0f });
		}

		aRenderBuffer.Add(&mySubstance);

		if (!myIsPlayer)
		{
			aRenderBuffer.Add(&myBackground);
		}
	}
}

void HealthBar::Update(float aDeltaTime, float aCurrentValue, const CommonUtilities::Vector3f& aPosition)
{
	if (myTimer < 0.f)
	{
		return;
	}
	else
	{
		myTimer -= aDeltaTime;
	}

	SetCurrentValue(aCurrentValue);
	SetPosition(aPosition);

	float alpha = (myCurrentValue / myMaxValue);

	if (myScaleDirection == eScaleDirection::X)
	{
		mySubstance.SetScale({ myScale.x * alpha, myScale.y });
		mySubstance.SetUVScale({ myScale.x * alpha, myScale.y });

	}
	else if (myScaleDirection == eScaleDirection::Y)
	{
		mySubstance.SetScale({ myScale.x, myScale.y * alpha });
		mySubstance.SetUVScale({ myScale.x, myScale.y * alpha });
		mySubstance.SetUVOffset({ 0.f, myScale.y - alpha });
	}
}

void HealthBar::SetPosition(CommonUtilities::Vector3f aPosition)
{
	myFramePosition = aPosition;
	mySubstancePosition = myFramePosition;
	mySubstancePosition.z = myFramePosition.z + 0.001f;

	if (!myIsPlayer)
	{
		myBackgroundPosition = myFramePosition;
		myBackgroundPosition.z = myFramePosition.z + 0.002f;
		myBackground.SetScale({ myScale.x, myScale.y });
		myBackground.SetPosition(myBackgroundPosition);
	}

	if (myScaleDirection == eScaleDirection::X)
	{
		mySubstancePosition.x -= (myScale.x * (1.f - myCurrentValue / myMaxValue)) * (mySubstance.GetUnitSize().x / 2.f);
	}
	else if (myScaleDirection == eScaleDirection::Y)
	{
		mySubstancePosition.y -= (myScale.y * (1.f - myCurrentValue / myMaxValue)) * (mySubstance.GetUnitSize().y / 2.f);
	}

	myFrame.SetPosition(myFramePosition);
	mySubstance.SetPosition(mySubstancePosition);
}

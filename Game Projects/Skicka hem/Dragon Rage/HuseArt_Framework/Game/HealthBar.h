#pragma once
#include "Bar.h"
#include <Animation.h>

enum class eScaleDirection
{
	X,
	Y
};

class HealthBar: public Bar
{
public:
	HealthBar() = default;
	~HealthBar() = default;

	void Init(float aMaxValue, const CommonUtilities::Vector3f& aPosition, float aShowTimer = FLT_MAX, const CommonUtilities::Vector2f& aScale = { 1.0f, 1.0f }, bool aIsPlayer = false, const char* aFramePath = "Assets/Images/Sprites/healthbar.dds", const char* aSubstancePath = "Assets/Images/Sprites/emptybar.dds");

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer, float aFlashTimer = 0.0f) override;

	void Update(float aDeltaTime, float aCurrentValue, const CommonUtilities::Vector3f& aPosition) override;
	void SetPosition(CommonUtilities::Vector3f aPosition) override;

	void SetScaleDirection(eScaleDirection aScaleDirection) { myScaleDirection = aScaleDirection; }
	
private:
	eScaleDirection myScaleDirection = eScaleDirection::X;
	Animation myBackground;
	CommonUtilities::Vector3f myBackgroundPosition;

	bool myIsPlayer;
};


#include "stdafx.h"
#include "HighScore.h"

#include <tga2d/engine.h>
#include <tga2d/math/vector2.h>

#include <Base64.h>
#include <Parser\json.hpp>
#include <fstream>
#include <Vector.h>
#include <Mathf.h>

#include "Blackboard.h"

Highscore::Highscore()
{
	myScore = 0.f;
	myDisplayScore = 0.f;
	myPopZ = 1.f;
}

Highscore::~Highscore()
{
}

void Highscore::Init()
{
	myHighscore.Init("", Tga2D::EFontSize_36);
	myHighscore.SetPosition({
		-Tga2D::CEngine::GetInstance()->GetWindowRatio() + 0.05f,
		0.9f,
		1.0f
	});
}

void Highscore::Update(float aDeltaTime)
{
	myDisplayScore = CommonUtilities::Lerp(myDisplayScore, myScore, aDeltaTime * 15.f);
	myPopZ = CommonUtilities::Lerp(myPopZ, 1.f, aDeltaTime * 5.f);
}

void Highscore::AddScore(float aScore)
{
	myScore += aScore;
	myPopZ = 1.1f;
}

void Highscore::FillGuiBuffer(CommonUtilities::GrowingArray<Drawable*>& aGuiBuffer)
{
	myHighscore.SetText(std::string("Highscore: ") + std::to_string(static_cast<int>(std::ceilf(myDisplayScore))));
	myHighscore.SetScale({ myPopZ, myPopZ });
	aGuiBuffer.Add(&myHighscore);
}

inline void Highscore::ResetScore()
{
	myScore = 0.f;
}

void Highscore::SaveScore()
{
	nlohmann::json j = {
		{"score", myScore}
	};

	const std::string scoreData = j.dump();

	std::ofstream o("Assets/Data/score.hafd");
	o << CommonUtilities::Base64Encode(reinterpret_cast<unsigned char const*>(scoreData.c_str()), scoreData.length()) << std::endl;

	Blackboard::GetInstance()->SetNote("score", j.dump());
}



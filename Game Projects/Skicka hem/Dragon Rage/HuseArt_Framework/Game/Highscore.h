#pragma once

#include <GrowingArray.h>
#include <Text.h>

class Drawable;
class Highscore
{
public:
	Highscore();
	~Highscore();

	void Init();
	void Update(float aDeltaTime);
	void FillGuiBuffer(CommonUtilities::GrowingArray<Drawable*>& aGuiBuffer);

	void AddScore(float aScore);
	inline void ResetScore();
	void SaveScore();

private:
	float myScore;
	float myDisplayScore;
	float myPopZ;
	Text myHighscore;
};


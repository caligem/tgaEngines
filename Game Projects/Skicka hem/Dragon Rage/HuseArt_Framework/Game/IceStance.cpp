#include "stdafx.h"
#include "IceStance.h"

#include "ProjectileFactory.h"
#include "ProjectileManager.h"
#include "SFXManager.h"

#include "Player.h"

namespace CU = CommonUtilities;

IceStance::IceStance(const CommonUtilities::Camera & aCamera, Player & aPlayer, Screenshake & aScreenshake)
	: myBlizzardTimer(0.f)
	, myBlizzardValueTarget(1.f)
	, myBlizzardValue(1.f)
	, myCamera(aCamera)
	, myPlayer(aPlayer)
	, Stance(aScreenshake)
{}

IceStance::~IceStance()
{
}

void IceStance::Init()
{
	myElement = eElement::Ice;

	myFlyingAnimation.Init("Assets/Images/Player/playerDragonIceFlying.dds");
	myFlyingAnimation.Setup(2, 2, 4);
	myGlideAnimation.Init("Assets/Images/Player/playerDragonIce.dds");
	//myFlyingAnimation.Setup(1, 3, 3);
	//myFlyingAnimation.SetDuration(.5f);

	/*
	myShootingAnimation.Init("Assets/Images/Sprites/dragon_shoot_blue.dds");
	myShootingAnimation.Setup(1, 3, 3);
	myShootingAnimation.SetDuration(.2f);
	*/

	myBlizzardSprite.Init("Assets/Images/Projectiles/blizzard.dds");
	myBlizzardSprite.SetScale({ 1.f, 1.f });
	myBlizzardSprite.SetUVScale({ 4.0f, 4.0f });

	myCurrentAnimation = &myFlyingAnimation;
}

void IceStance::Update(float aDeltaTime)
{
	Stance::Update(aDeltaTime);
	ManageBlizzard(aDeltaTime);
	ManageShootingAnimation();
}

void IceStance::Shoot(eShootType aShootType)
{
	if (aShootType == eShootType::Primary)
	{
		if (myPrimaryShootCooldown <= 1.f / myStats.primaryRateOfFire)
		{
			return;
		}

		myProjectileSpeed = 2.f;

		for (int i = 0; i < 5; i++)
		{
			ProjectileIcicle* projectile = ProjectileFactory::CreateIcicles(myPosition, myProjectileSpeed, CU::Vector3f(-0.2f + 0.1f*static_cast<float>(i), 1.f, 0.f));
			projectile->SetDamage(myStats.primaryDamage);
			projectile->SetElement(eElement::Ice);
			ProjectileManager::GetInstance()->PlayerShoot(projectile);
		}

		SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/Iciclex3.wav", 0.2f);

		myPrimaryShootCooldown = 0.f;
	}
	else if (aShootType == eShootType::Secondary)
	{
		if (mySecondaryShootCooldown <= 1.f / myStats.secondaryRateOfFire)
		{
			return;
		}
		
		Blizzard(0.5f, 5.f);

		SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/Blizzard.wav");

		mySecondaryShootCooldown = 0.f;

		myScreenshake.Shake(0.5f, { 0.01f, 0.01f, 0.0f });
	}
}

void IceStance::Blizzard(float aSlowValue, float aTime)
{
	myBlizzardTimer = aTime;
	myBlizzardValueTarget = aSlowValue;
}

void IceStance::ManageBlizzard(float aDeltaTime)
{
	myBlizzardValue += (myBlizzardValueTarget - myBlizzardValue) * aDeltaTime * 2.f;

	myBlizzardSprite.SetPosition({ myCamera.GetPosition() + CU::Vector3f(0.f, 0.f, 0.25f) });
	myBlizzardSprite.SetTint({ 1.0f, 1.0f, 1.0f, 1.0f - myBlizzardValue });

	if (myBlizzardTimer <= 0.f)
	{
		myBlizzardValueTarget = 1.f;
	}
	else
	{
		myBlizzardTimer -= aDeltaTime;
	}
}

void IceStance::ManageShootingAnimation()
{
	if (myPrimaryShootCooldown < 0.1f || mySecondaryShootCooldown < 0.1f)
	{
		SetState(eState::Shooting);
	}
	else
	{
		SetState(eState::Flying);
	}
}
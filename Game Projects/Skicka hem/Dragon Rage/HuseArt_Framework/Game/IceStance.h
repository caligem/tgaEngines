#pragma once
#include "Stance.h"

namespace CommonUtilities
{
	class Camera;
}

class IceStance : public Stance
{
public:
	IceStance(const CommonUtilities::Camera& aCamera, Player& aPlayer, Screenshake& aScreenshake);
	~IceStance();

	void Init() override;
	void Update(float aDeltaTime) override;

	void Shoot(eShootType aShootType) override;

	void ManageShootingAnimation();

	void Blizzard(float aSlowValue, float aTime);
	void ManageBlizzard(float aDeltaTime);

	inline const float GetBlizzardValue() const { return myBlizzardValue; }

	Sprite* GetBlizzardSprite() { return &myBlizzardSprite; }
	eStance GetStance() override { return eStance::Ice; }

private:
	float myBlizzardValue;
	float myBlizzardValueTarget;
	float myBlizzardTimer;
	Sprite myBlizzardSprite;
	const CommonUtilities::Camera& myCamera;
	Player& myPlayer;
};
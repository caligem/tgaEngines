#include "stdafx.h"
#include "KeyboardControls.h"

namespace CU = CommonUtilities;

KeyboardControls::KeyboardControls()
{
	myMoveUp = CU::Key_W;
	myMoveLeft = CU::Key_A;
	myMoveDown = CU::Key_S;
	myMoveRight = CU::Key_D;

	myDodgeLeft = CU::Key_Q;
	myDodgeRight = CU::Key_E;

	myPrimaryFire = CU::Key_Space;
	//mySecondaryFire =

	myFirstDragonForm = CU::Key_1;
	mySecondDragonForm = CU::Key_2;
	myThirdDragonForm = CU::Key_3;
	myForthDragonForm = CU::Key_4;
}


KeyboardControls::~KeyboardControls()
{
}

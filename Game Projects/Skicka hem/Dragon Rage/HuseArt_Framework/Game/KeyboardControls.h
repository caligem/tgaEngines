#pragma once

#include "InputManager.h"

class KeyboardControls
{
public:
	KeyboardControls();
	~KeyboardControls();

	CommonUtilities::Key GetMoveUp() { return myMoveUp; }
	CommonUtilities::Key GetMoveLeft() { return myMoveLeft; }
	CommonUtilities::Key GetMoveDown() { return myMoveDown; }
	CommonUtilities::Key GetMoveRight() { return myMoveRight; }
	void SetMoveUp(CommonUtilities::Key aKey) { myMoveUp = aKey; }
	void SetMoveLeft(CommonUtilities::Key aKey) { myMoveLeft = aKey; }
	void SetMoveDown(CommonUtilities::Key aKey) { myMoveDown = aKey; }
	void SetMoveRight(CommonUtilities::Key aKey) { myMoveRight = aKey; }

	CommonUtilities::Key GetPrimaryFire() { return myPrimaryFire; }
	void SetPrimaryFire(CommonUtilities::Key aKey) { myMoveUp = aKey; }
	//CommonUtilities::Key GetSecondaryFire() { return mySecondaryFire; }
	//void SetSecondaryFire(CommonUtilities::Key aKey) { myMoveUp = aKey; }
	
	CommonUtilities::Key GetFirstDragonForm() { return myFirstDragonForm; }
	CommonUtilities::Key GetSecondDragonForm() { return mySecondDragonForm; }
	CommonUtilities::Key GetThirdDragonForm() { return myThirdDragonForm; }
	CommonUtilities::Key GetForthDragonForm() { return myForthDragonForm; }
	void SetFirstDragonForm(CommonUtilities::Key aKey) { myFirstDragonForm = aKey; }
	void SetSecondDragonForm(CommonUtilities::Key aKey) { mySecondDragonForm = aKey; }
	void SetThirdDragonForm(CommonUtilities::Key aKey) { myThirdDragonForm = aKey; }
	void SetForthDragonForm(CommonUtilities::Key aKey) { myForthDragonForm = aKey; }

	CommonUtilities::Key GetDodgeLeft() { return myDodgeLeft; }
	CommonUtilities::Key GetDodgeRight() { return myDodgeRight; }
	void SetDodgeLeft(CommonUtilities::Key aKey) { myDodgeLeft = aKey; }
	void SetDodgeRight(CommonUtilities::Key aKey) { myDodgeRight = aKey; }
private:
	CommonUtilities::Key myMoveUp;
	CommonUtilities::Key myMoveLeft;
	CommonUtilities::Key myMoveDown;
	CommonUtilities::Key myMoveRight;

	CommonUtilities::Key myDodgeLeft;
	CommonUtilities::Key myDodgeRight;

	CommonUtilities::Key myPrimaryFire;
	//CommonUtilities::Key mySecondaryFire;

	CommonUtilities::Key myFirstDragonForm;
	CommonUtilities::Key mySecondDragonForm;
	CommonUtilities::Key myThirdDragonForm;
	CommonUtilities::Key myForthDragonForm;
};


#include "stdafx.h"
#include "LevelScene.h"

#include <Animation.h>
#include <SpriteData.h>

#include <Camera.h>

#include "EnemyManager.h"
#include "ProjectileManager.h"
#include "ExplosionManager.h"
#include "SFXManager.h"
#include "EnemyParticle.h"

#include "EnemyFactory.h"
#include "EnemyCurve.h"
#include "Blackboard.h"
#include <EnemyClass.h>

#include <InputManager.h>
#include <Tweener.h>

#include <fstream>
#include <DL_Debug.h>
#include <Mathf.h>

LevelScene::LevelScene(
	const CommonUtilities::InputManager & aInputManager,
	CommonUtilities::Camera & aCamera,
	CommonUtilities::Camera & aGuiCamera,
	CommonUtilities::XBOXController & aXboxController,
	SoundSystemClass & aSoundSystem
)
	: myCamera(aCamera)
	, myGuiCamera(aGuiCamera)
	, myInputManager(aInputManager)
	, myXboxController(aXboxController)
	, myPlayer(aInputManager, aXboxController, aSoundSystem, aCamera, myScreenshake)
	, mySoundFactory(aSoundSystem)
	, myScreenshake(aXboxController)
{
	mySprites.Init(128);
	myBuffer.Init(128);
	myFloaters.Init(2);

	myRequestSceneLoad = false;
	myShowDebug = true;
	myBossState = eBossState::Dialogue;
	myLevelState = eLevelState::Glide;
}

LevelScene::~LevelScene()
{
	EnemyManager::Destroy();
	ProjectileManager::Destroy();
	ExplosionManager::Destroy();
	SFXManager::Destroy();

	myLevelMusic.Stop();
	myBossMusic.Stop();
	myDialogueMusic.Stop();
	myLevelMusic.Release();
	myBossMusic.Release();
	myDialogueMusic.Release();
}

void LevelScene::Init(nlohmann::json aJSON)
{
	myCamera.SetPosition({ 0.f, 0.f, 0.f });
	myGuiCamera.SetPosition({ 0.f, 0.f, 0.f });

	EnemyManager::Create();
	ProjectileManager::Create();
	ExplosionManager::Create();
	SFXManager::Create(&mySoundFactory);

	LoadLevel(aJSON);

	myLevelEndY = EnemyManager::GetInstance()->GetLevelEnd();

	myHighscore.Init();

	myPlayer.Init();
	myPlayer.SetPosition({ 0.f, 0.f, 1.f });
	ReadSettings();

	InitMusic();
	InitDialogue();

	myDeathSlowTime = 5.0f;
	mySlowAmount = 1.0f;
	EnemyParticle::mySoulsCaught = 0;
	EnemyParticle::mySoulCombos = 0;
}

void LevelScene::LoadLevel(nlohmann::json aJSON)
{
	if (aJSON.find("sprites") != aJSON.end())
	{
		for (nlohmann::json::object_t spriteData : aJSON["sprites"])
		{
			LoadSprite(spriteData);
		}
	}
	if (aJSON.find("animations") != aJSON.end())
	{
		for (nlohmann::json::object_t animationData : aJSON["animations"])
		{
			LoadAnimation(animationData);
		}
	}
	if (aJSON.find("enemies") != aJSON.end())
	{
		for (nlohmann::json::object_t enemyData : aJSON["enemies"])
		{
			LoadEnemy(enemyData);
		}
	}
	if (aJSON.find("curves") != aJSON.end())
	{
		for (nlohmann::json::object_t curveData : aJSON["curves"])
		{
			LoadCurve(curveData);
		}
	}
}
void LevelScene::LoadSprite(nlohmann::json::object_t aSpriteObject)
{
	Sprite* newSprite = new Sprite(aSpriteObject);
	mySprites.Add(newSprite);

	if (aSpriteObject["name"].get<std::string>() == "floater")
	{
		myFloaters.Add({
			newSprite->GetPosition(),
			newSprite
		});
	}
}
void LevelScene::LoadAnimation(nlohmann::json::object_t aAnimationObject)
{
	mySprites.Add(new Animation(aAnimationObject));
}
void LevelScene::LoadEnemy(nlohmann::json::object_t aEnemyObject)
{
	if (
		aEnemyObject.find("position") == aEnemyObject.end() ||
		aEnemyObject.find("class") == aEnemyObject.end() ||
		aEnemyObject.find("tag") == aEnemyObject.end()
	)
	{
		return;
	}

	const EnemyClass& enemyClass = myEnemyClassCache.GetClass(aEnemyObject["class"].get<std::string>());

	const std::string& tag = aEnemyObject["tag"].get<std::string>();
	eElement enemyElement = eElement::Fire;
	if (tolower(tag[0]) == 'f')
	{
		enemyElement = eElement::Fire;
	}
	else if (tolower(tag[0]) == 'i')
	{
		enemyElement = eElement::Ice;
	}
	else if (tolower(tag[0]) == 'l')
	{
		enemyElement = eElement::Lightning;
	}

	EnemyFactory::CreateEnemy(
		enemyClass,
		{
			aEnemyObject["position"][0].get<float>(),
			aEnemyObject["position"][1].get<float>(),
			aEnemyObject["position"][2].get<float>()
		},
		myEnemyClassCache,
		enemyElement
	);
}
void LevelScene::LoadCurve(nlohmann::json::object_t aCurveObject)
{
	if (
		aCurveObject.find("position") == aCurveObject.end() ||
		aCurveObject.find("class") == aCurveObject.end() ||
		aCurveObject.find("points") == aCurveObject.end() ||
		aCurveObject.find("speed") == aCurveObject.end() ||
		aCurveObject.find("interval") == aCurveObject.end() ||
		aCurveObject.find("amount") == aCurveObject.end() ||
		aCurveObject.find("tag") == aCurveObject.end()
	)
	{
		return;
	}

	const EnemyClass& enemyClass = myEnemyClassCache.GetClass(aCurveObject["class"].get<std::string>());

	const std::string& tag = aCurveObject["tag"].get<std::string>();
	eElement element = eElement::Fire;

	if (tolower(tag[0]) == 'f')
	{
		element = eElement::Fire;
	}
	else if (tolower(tag[0]) == 'i')
	{
		element = eElement::Ice;
	}
	else if (tolower(tag[0]) == 'l')
	{
		element = eElement::Lightning;
	}


	EnemyCurve* enemyCurve = new EnemyCurve(
		aCurveObject["amount"].get<int>(),
		aCurveObject["interval"].get<float>(),
		aCurveObject["speed"].get<float>(),
		myEnemyClassCache
	);
	enemyCurve->SetElement(element);
	enemyCurve->Init(
		enemyClass,
		{
			aCurveObject["position"][0].get<float>(),
			aCurveObject["position"][1].get<float>(),
			aCurveObject["position"][2].get<float>()
		}
	);

	for (auto point : aCurveObject["points"])
	{
		enemyCurve->AddPoint({
			point[0].get<float>(),
			point[1].get<float>(),
			point[2].get<float>(),
		});
	}
	enemyCurve->BuildCurve();

	EnemyManager::GetInstance()->AddEnemy(enemyCurve);
}

void LevelScene::HandleStates(float aDeltaTime)
{
	switch (myLevelState)
	{
	case eLevelState::Glide:
		UpdateGlideFight(aDeltaTime);
		myBossState = eBossState::Dialogue;
		break;
	case eLevelState::Boss:
		if (myBossState == eBossState::Dialogue)
		{
			myLevelMusicAlphaTarget = 0.f;
			myBossMusicAlphaTarget = 0.f;
			myDialogueMusicAlphaTarget = 1.f;
			UpdateBossDialogue(aDeltaTime);
		}
		if (myBossState == eBossState::Fight)
		{
			myLevelMusicAlphaTarget = 0.f;
			myBossMusicAlphaTarget = 1.f;
			myDialogueMusicAlphaTarget = 0.f;
			UpdateBossFight(aDeltaTime);
		}
		break;
	case eLevelState::End:
		myHighscore.SaveScore();
		Blackboard::GetInstance()->SetNote("postgame", "true");

		Blackboard::GetInstance()->SetNote("postgame_damage_taken", std::to_string(static_cast<int>(myPlayer.GetDamageTaken())));
		Blackboard::GetInstance()->SetNote("postgame_souls_collected", std::to_string(EnemyParticle::mySoulsCaught));
		Blackboard::GetInstance()->SetNote("postgame_full_combos", std::to_string(EnemyParticle::mySoulCombos));

		UpdateGamePlay(aDeltaTime);
		if (!myPlayer.IsDead())
		{
			if (Blackboard::GetInstance()->GetNote("currentScene") == "Assets/Data/levels/tutorial.hafl")
			{
				Blackboard::GetInstance()->SetNote("level2", "unlocked");
				RequestScene("Assets/Data/menus/postgame.hafl");
			}
			if (Blackboard::GetInstance()->GetNote("currentScene") == "Assets/Data/levels/level1.hafl")
			{
				Blackboard::GetInstance()->SetNote("level3", "unlocked");
				RequestScene("Assets/Data/menus/postgame.hafl");
			}
			if (Blackboard::GetInstance()->GetNote("currentScene") == "Assets/Data/levels/level2.hafl")
			{
				RequestScene("Assets/Data/cutscenes/OutroCutscene/scene1.hafl");
			}
		}
		else
		{
			RequestScene("Assets/Data/menus/postgame.hafl");
		}
		break;
	}

	myLevelMusicAlpha = CommonUtilities::Lerp(myLevelMusicAlpha, myLevelMusicAlphaTarget, aDeltaTime*3.0f);
	myBossMusicAlpha = CommonUtilities::Lerp(myBossMusicAlpha, myBossMusicAlphaTarget, aDeltaTime*3.0f);
	myDialogueMusicAlpha = CommonUtilities::Lerp(myDialogueMusicAlpha, myDialogueMusicAlphaTarget, aDeltaTime*3.0f);
	myLevelMusic.Fade(myLevelMusicAlpha);
	myBossMusic.Fade(myBossMusicAlpha);
	myDialogueMusic.Fade(myDialogueMusicAlpha);
}

void LevelScene::UpdateGamePlay(float aDeltaTime)
{
	UpdatePlayer(aDeltaTime);

	UpdateManagers(aDeltaTime);
	HandleCollisions(aDeltaTime);
	CleanUpManagers();

	//Wake up enemies that come inside the screen
	EnemyManager::GetInstance()->WakeEnemies(myCamera);
}

void LevelScene::UpdateBossDialogue(float aDeltaTime)
{
	//Camera and music stuff
	float dy = EnemyManager::GetInstance()->GetTargetPosition().y - myCamera.GetPosition().y;
	dy *= 5.0f;
	myCamera.Move({ 0.f, dy * aDeltaTime * myCameraScrollSpeed, 0.f });
	myPlayer.Move({ 0.f, dy * aDeltaTime * myCameraScrollSpeed, 0.f });

	if (!myDialogueMusic.IsPlaying())myDialogueMusic.Play();

	BoundPlayer();
	myPlayer.Update(aDeltaTime);

	//Update dialogue stuff
	Enemy* boss = EnemyManager::GetInstance()->GetCurrentBoss();

	Dialogue& dialogue = boss->GetDialogue();

	if (!dialogue.IsValid() || (dialogue.IsEnded() && DialogueStep()))
	{
		myBossState = eBossState::Fight;
		return;
	}

	if (DialogueStep())
	{
		const DialogueLine& dl = dialogue.GetNext();
		if (dl.IsPlayer())
		{
			SetDialogueText(dl.GetText(), true);
		}
		else
		{
			SetDialogueText(dl.GetText(), false);
		}
	}

	ExplosionManager::GetInstance()->Update(aDeltaTime * mySlowAmount);	
	CleanUpManagers();
}

void LevelScene::UpdateBossFight(float aDeltaTime)
{
	float dy = EnemyManager::GetInstance()->GetTargetPosition().y - myCamera.GetPosition().y;
	dy *= 5.0f;
	myCamera.Move({ 0.f, dy * aDeltaTime * myCameraScrollSpeed, 0.f });
	myPlayer.Move({ 0.f, dy * aDeltaTime * myCameraScrollSpeed, 0.f });

	myLevelMusicAlpha = CommonUtilities::Lerp(myLevelMusicAlpha, 0.0f, aDeltaTime*3.0f);
	myBossMusicAlpha = CommonUtilities::Lerp(myBossMusicAlpha, 1.0f, aDeltaTime*3.0f);
	if (!myBossMusic.IsPlaying())myBossMusic.Play();
	UpdateGamePlay(aDeltaTime);
}

void LevelScene::UpdateGlideFight(float aDeltaTime)
{
	myCamera.Move({ 0.f, aDeltaTime * myCameraScrollSpeed * myPlayer.GetBlizzardValue(), 0.f });
	myPlayer.Move({ 0.f, aDeltaTime * myCameraScrollSpeed * myPlayer.GetBlizzardValue(), 0.f });

	myLevelMusicAlpha = CommonUtilities::Lerp(myLevelMusicAlpha, 1.0f, aDeltaTime*3.0f);
	myBossMusicAlpha = CommonUtilities::Lerp(myBossMusicAlpha, 0.0f, aDeltaTime*3.0f);
	UpdateGamePlay(aDeltaTime);
}

void LevelScene::Update(float aDeltaTime)
{
	CleanUpSFXManager();
	EnemyParticle::UpdateCoinTimer(aDeltaTime);

	UpdateScenery(aDeltaTime);

	UpdateState();
	HandleStates(aDeltaTime);

	myScreenshake.Update(aDeltaTime);
	myHighscore.Update(aDeltaTime);

	if (Blackboard::GetInstance()->GetNote("deathslowmotion") == "true")
	{
		myDeathSlowTime -= aDeltaTime;
		float a = (1.f - myDeathSlowTime / 5.f);
		a = a * a * (3.f - 2.f*a);
		mySlowAmount = 0.4f + (0.6f * a);
		if (myDeathSlowTime < 0)
		{
			mySlowAmount = 1.0f;
			myDeathSlowTime = 5.0f;
			Blackboard::GetInstance()->SetNote("deathslowmotion", "false");
		}
	}
}

void LevelScene::ReadSettings()
{
	std::ifstream settingsFile("Assets/Data/settings.haf");
	if( settingsFile.bad() || settingsFile.fail() )
	{
		DL_DEBUG("CAN'T OPEN SETTINGS FILE");
	}
	nlohmann::json settingsJSON;
	settingsFile >> settingsJSON;
	if (settingsJSON.find("cameraScrollSpeed") != settingsJSON.end())
	{
		myCameraScrollSpeed = settingsJSON["cameraScrollSpeed"].get<float>();
	}
	if (settingsJSON.find("movementSpeed") != settingsJSON.end())
	{
		myPlayer.SetMovementSpeed({
			settingsJSON["movementSpeed"]["x"].get<float>(),
			settingsJSON["movementSpeed"]["y"].get<float>()
		});
	}
	if (settingsJSON.find("boundaries") != settingsJSON.end())
	{
		myBoundaries.x = settingsJSON["boundaries"]["left"].get<float>();
		myBoundaries.y = settingsJSON["boundaries"]["top"].get<float>();
		myBoundaries.z = settingsJSON["boundaries"]["right"].get<float>();
		myBoundaries.w = settingsJSON["boundaries"]["bottom"].get<float>();
	}
	if (settingsJSON.find("fire") != settingsJSON.end())
	{
		myPlayer.SetStatsFire({
			settingsJSON["fire"]["primaryDamage"].get<float>(),
			settingsJSON["fire"]["primaryRateOfFire"].get<float>(),
			settingsJSON["fire"]["secondaryDamage"].get<float>(),
			settingsJSON["fire"]["secondaryRateOfFire"].get<float>()
		});
	}
	if (settingsJSON.find("ice") != settingsJSON.end())
	{
		myPlayer.SetStatsIce({
			settingsJSON["ice"]["primaryDamage"].get<float>(),
			settingsJSON["ice"]["primaryRateOfFire"].get<float>(),
			settingsJSON["ice"]["secondaryDamage"].get<float>(),
			settingsJSON["ice"]["secondaryRateOfFire"].get<float>()
		});
	}
	if (settingsJSON.find("lightning") != settingsJSON.end())
	{
		myPlayer.SetStatsLightning({
			settingsJSON["lightning"]["primaryDamage"].get<float>(),
			settingsJSON["lightning"]["primaryRateOfFire"].get<float>(),
			settingsJSON["lightning"]["secondaryDamage"].get<float>(),
			settingsJSON["lightning"]["secondaryRateOfFire"].get<float>()
		});
	}
	if (settingsJSON.find("focusFactor") != settingsJSON.end())
	{
		myPlayer.SetFocusFactor(settingsJSON["focusFactor"].get<float>());
	}
}

void LevelScene::BoundPlayer()
{
	if (myPlayer.GetPosition().x < myBoundaries.x)
	{
		myPlayer.SetPosition({ myBoundaries.x, myPlayer.GetPosition().y, myPlayer.GetPosition().z });
	}
	if (myPlayer.GetPosition().x > myBoundaries.z)
	{
		myPlayer.SetPosition({ myBoundaries.z, myPlayer.GetPosition().y, myPlayer.GetPosition().z });
	}
	if (myPlayer.GetPosition().y - myCamera.GetPosition().y > myBoundaries.y)
	{
		myPlayer.SetPosition({ myPlayer.GetPosition().x, myCamera.GetPosition().y + myBoundaries.y, myPlayer.GetPosition().z });
	}
	if (myPlayer.GetPosition().y - myCamera.GetPosition().y < myBoundaries.w)
	{
		myPlayer.SetPosition({ myPlayer.GetPosition().x, myCamera.GetPosition().y + myBoundaries.w, myPlayer.GetPosition().z });
	}
}

void LevelScene::InitMusic()
{
	myLevelMusic = mySoundFactory.CreateSound("Assets/Audio/music/dragonrageLevel.wav");
	myLevelMusic.SetVolume(0.65f);
	myLevelMusic.SetLoop(true);

	myBossMusic = mySoundFactory.CreateSound("Assets/Audio/music/dragonrageBoss.wav");
	myBossMusic.SetVolume(0.65f);
	myBossMusic.SetLoop(true);

	myDialogueMusic = mySoundFactory.CreateSound("Assets/Audio/music/dragonrageDialogue.wav");
	myDialogueMusic.SetVolume(0.65f);
	myDialogueMusic.SetLoop(true);

	myBossMusic.Fade(0.f);
	myDialogueMusic.Fade(0.f);

	myLevelMusicAlpha = 1.f;
	myBossMusicAlpha = 0.f;
	myDialogueMusicAlpha = 0.f;
	myLevelMusicAlphaTarget = 1.f;
	myBossMusicAlphaTarget = 0.f;
	myDialogueMusicAlphaTarget = 0.f;
}

void LevelScene::InitDialogue()
{
	myDialogueTextBox.Init("Assets/Images/Dialogues/background.dds");

	myDialogueText.Init("PLS PUT TEXT HERE", Tga2D::EFontSize_48);
	myDialogueText.SetPosition(myDialogueTextBox.GetPosition());
}

void LevelScene::SetDialogueText(const std::string & aText, bool aIsPlayer)
{
	if (aIsPlayer)
	{
		myDialogueTextBox.SetPosition(CommonUtilities::Vector3f(
			0.f,
			-1.f + myDialogueTextBox.GetUnitSize().y/2.f,
			0.99f
		));
	}
	else
	{
		myDialogueTextBox.SetPosition(CommonUtilities::Vector3f(
			0.f,
			1.f - myDialogueTextBox.GetUnitSize().y/2.f,
			0.99f
		));
	}

	myDialogueText.SetText(aText);
	myDialogueText.SetPosition(myDialogueTextBox.GetPosition() + CommonUtilities::Vector3f(
		-myDialogueTextBox.GetUnitSize().x*0.425f,
		myDialogueTextBox.GetUnitSize().y*0.2f,
		-0.001f
	));
}

bool LevelScene::DialogueStep()
{
	return
		myInputManager.IsKeyPressed(CU::Key_Space) ||
		(
			myXboxController.IsConnected() &&
			myXboxController.IsButtonPressed(CommonUtilities::XButton_A)
		) ||
		myJustChangedState;
}

void LevelScene::SetState(eLevelState aLevelState)
{
	myLevelState = aLevelState;
	myJustChangedState = true;
}

void LevelScene::UpdateState()
{
	myJustChangedState = false;

	if (myCamera.GetPosition().y >= myLevelEndY || myPlayer.IsDead())
	{
		SetState(eLevelState::End);
		std::cout << "changed to end state" << std::endl;
	}

	switch (myLevelState)
	{
	case eLevelState::Glide:
		if (EnemyManager::GetInstance()->IsBossFight())
		{
			ProjectileManager::GetInstance()->ClearProjectiles();
			SetState(eLevelState::Boss);
			std::cout << "changed to boss state" << std::endl;
		}
		break;
	case eLevelState::Boss:
		if (!EnemyManager::GetInstance()->IsBossFight())
		{
			SetState(eLevelState::Glide);
			std::cout << "changed to glide state" << std::endl;
			myScreenshake.Shake(1.0f, { 0.5f, 0.5f, 0.0f });

			myLevelMusicAlphaTarget = 1.f;
			myBossMusicAlphaTarget = 0.f;
			myDialogueMusicAlphaTarget = 0.f;
		}
		break;
	}
}

void LevelScene::UpdateScenery(float aDeltaTime)
{
	//Runs update on all drawables, it's for updating the animations
	for (Drawable* drawable : mySprites)
	{
		drawable->Update(aDeltaTime);
	}

	myAnimTimer += aDeltaTime;
	for (FloaterDrawable& fd : myFloaters)
	{
		fd.myDrawable->SetPosition(fd.myOriginalPosition + CommonUtilities::Vector3f(
			0.f, 0.0625f*std::sinf(myAnimTimer), 0.f
		));
	}
}

void LevelScene::UpdatePlayer(float aDeltaTime)
{
	myPlayer.PlayerControls(aDeltaTime * mySlowAmount);
	BoundPlayer();
	myPlayer.Update(aDeltaTime);
}

void LevelScene::UpdateManagers(float aDeltaTime)
{
	EnemyManager::GetInstance()->Update(aDeltaTime * myPlayer.GetBlizzardValue() * mySlowAmount, myPlayer);
	ProjectileManager::GetInstance()->UpdateEnemieProjectiles(aDeltaTime * myPlayer.GetBlizzardValue() * mySlowAmount, myPlayer);
	ProjectileManager::GetInstance()->UpdatePlayerProjectiles(aDeltaTime * mySlowAmount, myPlayer);
	ExplosionManager::GetInstance()->Update(aDeltaTime * mySlowAmount);	
}

void LevelScene::HandleCollisions(float aDeltaTime)
{
	EnemyManager::GetInstance()->CollideWithPlayer(myPlayer, aDeltaTime);
	ProjectileManager::GetInstance()->CollideWithPlayerAndEnemies(myPlayer, aDeltaTime);
}

void LevelScene::CleanUpSFXManager()
{
	/*
	* Clean up SFX that has stopped playing, has to be at the top so
	* that the sound system updates the isPlaying state before cleaning up
	*/
	SFXManager::GetInstance()->CleanUp();
}

void LevelScene::CleanUpManagers()
{
	EnemyManager::GetInstance()->CleanUp(myHighscore);
	ProjectileManager::GetInstance()->CleanUp();
	ExplosionManager::GetInstance()->CleanUp();
}

void LevelScene::Render()
{
	myCamera.Move(myScreenshake.GetShakeOffset());
	FillBufferAndCalculatePoints();
	SortAndRenderBuffer();
	myCamera.Move(-myScreenshake.GetShakeOffset());

	FillGuiBufferAndCalculatePoints();
	SortAndRenderBuffer();
}

void LevelScene::Fade(float aAlpha)
{
	if (!myLevelMusic.IsPlaying())
	{
		myLevelMusic.Play();
	}
	myLevelMusic.Fade(aAlpha);
	myBossMusic.Fade(aAlpha);
}

void LevelScene::SortAndRenderBuffer()
{
	if (myBuffer.Empty())
	{
		return;
	}

	std::stable_sort(myBuffer.begin(), myBuffer.end(), [](Drawable* a, Drawable* b)
	{
		if (a->GetPosition().z == b->GetPosition().z)
		{
			return a->GetPosition().y > b->GetPosition().y;
		}
		return a->GetPosition().z > b->GetPosition().z;
	});

	for	(Drawable* drawable : myBuffer)
	{
		drawable->Render();
	}
}

void LevelScene::FillBufferAndCalculatePoints()
{
	myBuffer.RemoveAll();
	for (Drawable* drawable : mySprites)
	{
		myBuffer.Add(drawable);
	}

	EnemyManager::GetInstance()->FillRenderBuffer(myBuffer);
	ProjectileManager::GetInstance()->FillRenderBuffer(myBuffer);
	ExplosionManager::GetInstance()->FillRenderBuffer(myBuffer);
	myPlayer.FillRenderBuffer(myBuffer);

	for (Drawable* drawable : myBuffer)
	{
		drawable->CalculatePoint(myCamera);
	}
}

void LevelScene::FillGuiBufferAndCalculatePoints()
{
	myBuffer.RemoveAll();
	
	if (myLevelState == eLevelState::Boss && myBossState == eBossState::Dialogue)
	{
		myBuffer.Add(&myDialogueTextBox);
		myBuffer.Add(&myDialogueText);
	}
	else
	{
		EnemyManager::GetInstance()->FillGuiBuffer(myBuffer);
		myPlayer.FillGuiBuffer(myBuffer);
		myHighscore.FillGuiBuffer(myBuffer);
	}

	for (Drawable* drawable : myBuffer)
	{
		drawable->CalculatePoint(myGuiCamera);
	}
}

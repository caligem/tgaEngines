#pragma once

#include "Scene.h"

namespace CommonUtilities
{
	class Camera;
	class InputManager;
	class XBOXController;
}

class SoundSystemClass;

#include "Player.h"
#include "EnemyClassCache.h"
#include "Highscore.h"
#include "Screenshake.h"

#include "SoundFactory.h"

#include <Parser\json.hpp>

#include "AudioSource.h"

#include "Text.h"

enum class eLevelState
{
	Glide,
	Boss,
	End
};

enum class eBossState
{
	Dialogue,
	Fight
};

class LevelScene : public Scene
{
public:
	LevelScene(
		const CommonUtilities::InputManager& aInputManager,
		CommonUtilities::Camera& aCamera,
		CommonUtilities::Camera& aGuiCamera,
		CommonUtilities::XBOXController& aXboxController,
		SoundSystemClass& aSoundSystem
	);
	~LevelScene();

	void Update(float aDeltaTime) override;
	void Render() override;
	void Fade(float aAlpha) override;

	void SortAndRenderBuffer() override;
	void FillBufferAndCalculatePoints() override;
	void FillGuiBufferAndCalculatePoints() override;

	void Init(nlohmann::json aJSON);

	bool IsMenu() override { return false; }
	bool IsCutscene() override { return false; }

private:
	void LoadLevel(nlohmann::json aJSON);
	void LoadSprite(nlohmann::json::object_t aSpriteObject);
	void LoadAnimation(nlohmann::json::object_t aAnimationObject);
	void LoadEnemy(nlohmann::json::object_t aEnemyObject);
	void LoadCurve(nlohmann::json::object_t aCurveObject);

	void ReadSettings();
	void BoundPlayer();
	void InitMusic();
	void InitDialogue();

	void HandleStates(float aDeltaTime);
	void UpdateGamePlay(float aDeltaTime);

	void SetState(eLevelState aLevelState);
	void UpdateState();
	void UpdateScenery(float aDeltaTime);
	void UpdatePlayer(float aDeltaTime);
	void UpdateManagers(float aDeltaTime);
	void HandleCollisions(float aDeltaTime);
	void CleanUpSFXManager();
	void CleanUpManagers();

	void SetDialogueText(const std::string& aText, bool aIsPlayer);
	bool DialogueStep();
	void UpdateBossDialogue(float aDeltaTime);
	void UpdateBossFight(float aDeltaTime);
	void UpdateGlideFight(float aDeltaTime);

	const CommonUtilities::InputManager& myInputManager;
	CommonUtilities::XBOXController& myXboxController;
	CommonUtilities::Camera& myCamera;
	CommonUtilities::Camera& myGuiCamera;

	Player myPlayer;

	Screenshake myScreenshake;

	Highscore myHighscore;
	EnemyClassCache myEnemyClassCache;

	float myLevelEndY;
	float myCameraScrollSpeed;
	CommonUtilities::Vector4f myBoundaries;

	SoundFactory mySoundFactory;

	AudioSource myLevelMusic;
	AudioSource myBossMusic;
	AudioSource myDialogueMusic;
	float myBossMusicAlphaTarget;
	float myDialogueMusicAlphaTarget;
	float myLevelMusicAlphaTarget;
	float myBossMusicAlpha;
	float myDialogueMusicAlpha;
	float myLevelMusicAlpha;

	bool myJustChangedState;
	eLevelState myLevelState;
	eBossState myBossState;
	Sprite myDialogueTextBox;
	Text myDialogueText;

	bool myShowDebug;

	float myDeathSlowTime;
	float mySlowAmount;
};


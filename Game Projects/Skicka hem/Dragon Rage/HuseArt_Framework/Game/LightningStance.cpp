#include "stdafx.h"
#include "LightningStance.h"

#include "SFXManager.h"

#include "Player.h"

LightningStance::LightningStance(Player & aPlayer, Screenshake & aScreenshake, SoundSystemClass & aSoundSystem)
	: myPlayer(aPlayer)
	, Stance(aScreenshake)
	, mySoundSystem(aSoundSystem)
{}

LightningStance::~LightningStance()
{
	myBeamStartup.Stop();
	myBeamLoop.Stop();
	myBeamEnd.Stop();
	myBeamStartup.Release();
	myBeamLoop.Release();
	myBeamEnd.Release();
}

void LightningStance::Init()
{
	myElement = eElement::Lightning;

	myFlyingAnimation.Init("Assets/Images/Player/playerDragonLightningFlying.dds");
	myFlyingAnimation.Setup(2, 2, 4);
	myGlideAnimation.Init("Assets/Images/Player/playerDragonLightning.dds");
	//myFlyingAnimation.Setup(1, 3, 3);
	//myFlyingAnimation.SetDuration(.5f);

	/*
	myShootingAnimation.Init("Assets/Images/Sprites/dragon_shoot_yellow.dds");
	myShootingAnimation.Setup(1, 3, 3);
	myShootingAnimation.SetDuration(.2f);
	*/

	myBeamStartup.Init("Assets/Audio/sfx/beamStartup.wav", &mySoundSystem);
	myBeamStartup.SetLoop(false);
	myBeamStartup.SetVolume(0.15f);
	myBeamLoop.Init("Assets/Audio/sfx/beamLoop.wav", &mySoundSystem);
	myBeamLoop.SetLoop(true);
	myBeamLoop.SetVolume(0.15f);
	myBeamEnd.Init("Assets/Audio/sfx/beamEnd.wav", &mySoundSystem);
	myBeamEnd.SetLoop(false);
	myBeamEnd.SetVolume(0.15f);
	myPreviousBeamState = false;

	myBeam.Init(eBeamType::PlayerBeam);

	myCurrentAnimation = &myFlyingAnimation;
}

void LightningStance::Update(float aDeltaTime)
{
	Stance::Update(aDeltaTime);
	myBeam.Update(aDeltaTime);
	ManageShootingAnimation();

	if (!myPreviousBeamState && myBeam.IsActive())
	{
		myBeamStartup.Play();
	}
	else if (myPreviousBeamState && myBeam.IsActive())
	{
		if (!myBeamLoop.IsPlaying() && myBeamStartup.GetProgress() > 0.9f)
		{
			myBeamLoop.Play();
		}
	}
	else if (myPreviousBeamState && !myBeam.IsActive())
	{
		myBeamLoop.Stop();
		myBeamStartup.Stop();
		myBeamEnd.Play();
	}

	myPreviousBeamState = myBeam.IsActive();
}

void LightningStance::Shoot(eShootType aShootType)
{
	if (aShootType == eShootType::Primary)
	{
		myBeam.SetDamage(myStats.primaryDamage);
		myBeam.Activate();
		myBeam.SetElement(eElement::Lightning);

		
		CU::Vector3f endPos = myPosition + CU::Vector3f(0.f, 2.f, 0.f);
		CU::Vector3f offset = (endPos - myPosition).GetNormalized() * 0.18f;

		myBeam.SetStartPosition(myPosition + offset);
		myBeam.SetEndPosition(endPos);
	}
	else if (aShootType == eShootType::Secondary)
	{
		if (mySecondaryShootCooldown <= 1.f / myStats.secondaryRateOfFire)
		{
			return;
		}

		myProjectileSpeed = 3.f;

		for (float a = 0.f; a < 2.f * Tga2D::Pif; a += Tga2D::Pif / 30)
		{
			ProjectileLightningNova* projectile = ProjectileFactory::CreateLightningNova(myPosition, myProjectileSpeed, CU::Vector3f(std::cos(a), std::sin(a), 0.f));
			projectile->SetDamage(myStats.secondaryDamage);
			projectile->SetElement(eElement::Lightning);
			ProjectileManager::GetInstance()->PlayerShoot(projectile);
		}

		mySecondaryShootCooldown = 0.f;
		myScreenshake.Shake(0.5f, { 0.045f, 0.045f, 0.0f });
	}
}

void LightningStance::ManageShootingAnimation()
{
	if (myPrimaryShootCooldown < 0.1f || mySecondaryShootCooldown < 0.1f)
	{
		SetState(eState::Shooting);
	}
	else 
	{
		SetState(eState::Flying);
	}
}
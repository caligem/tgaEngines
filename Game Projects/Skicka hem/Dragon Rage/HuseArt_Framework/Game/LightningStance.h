#pragma once
#include "Stance.h"

#include "ProjectileFactory.h"
#include "ProjectileManager.h"
#include "Beam.h"

#include <SoundSystemClass.h>
#include <AudioSource.h>

namespace CU = CommonUtilities;

class LightningStance : public Stance
{
public:
	LightningStance(Player& aPlayer, Screenshake& aScreenshake, SoundSystemClass& aSoundSystem);
	~LightningStance();

	void Init() override;
	void Update(float aDeltaTime) override;

	void ManageShootingAnimation();

	void Shoot(eShootType aShootType) override;

	inline Beam& GetBeam() { return myBeam; }

	eStance GetStance() override { return eStance::Lightning; }

private:
	Beam myBeam;
	Player& myPlayer;

	SoundSystemClass& mySoundSystem;

	bool myPreviousBeamState;
	AudioSource myBeamStartup;
	AudioSource myBeamLoop;
	AudioSource myBeamEnd;
};


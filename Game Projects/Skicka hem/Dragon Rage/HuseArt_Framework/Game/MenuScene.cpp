#include "stdafx.h"
#include "MenuScene.h"

#include <sstream>

#include "SFXManager.h"

#include <InputManager.h>
#include <XBOXController.h>
#include <Camera.h>
#include <Base64.h>
#include <Vector.h>
#include <DL_Debug.h>
#include <Parser/json.hpp>

#include <Animation.h>
#include <SpriteData.h>

#include <tga2d/engine.h>
#include <tga2d/text/text.h>

#include "Blackboard.h"

#include <Parser/json.hpp>

#include <SoundSystemClass.h>

std::map<std::string, std::string> MenuScene::ourLevelMap = {
	{"level1", "Assets/Data/cutscenes/level1.hafl"},
	{"level2", "Assets/Data/cutscenes/level2.hafl"},
	{"level3", "Assets/Data/cutscenes/level3.hafl"}
};

MenuScene::MenuScene(
	const CommonUtilities::InputManager & aInputManager,
	CommonUtilities::Camera & aCamera,
	CommonUtilities::Camera & aGuiCamera,
	CommonUtilities::XBOXController & aXboxController,
	SoundSystemClass & aSoundSystem
)
	: myInputManager(aInputManager)
	, myCamera(aCamera)
	, myGuiCamera(aGuiCamera)
	, myXboxController(aXboxController)
	, mySoundSystem(aSoundSystem)
	, myWidgetSystem(aInputManager, aXboxController)
	, mySoundFactory(aSoundSystem)
{
	myRequestSceneLoad = false;
	mySceneType = eMenuSceneType::Menu;
}

MenuScene::~MenuScene()
{
	SFXManager::Destroy();
}

void MenuScene::Init(nlohmann::json aJSON)
{
	myCamera.SetPosition({ 0.f, 0.f, 0.f });

	SFXManager::Create(&mySoundFactory);

	mySprites.Init(128);
	myBuffer.Init(128);
	myWidgetSystem.Init();
	myFloaters.Init(2);
	myBlinkers.Init(2);

	if (aJSON.find("sprites") != aJSON.end())
	{
		for (nlohmann::json::object_t spriteData : aJSON["sprites"])
		{
			LoadSprite(spriteData);
		}
	}
	if (aJSON.find("animations") != aJSON.end())
	{
		for (nlohmann::json::object_t animationData : aJSON["animations"])
		{
			LoadAnimation(animationData);
		}
	}
	if (aJSON.find("buttons") != aJSON.end())
	{
		for (nlohmann::json::object_t buttonData : aJSON["buttons"])
		{
			LoadButton(buttonData);
		}
	}

	ReadScore();
}

void MenuScene::TriggerButton(WidgetButton* aButton)
{
	if (aButton->GetEventType() == "loadscene")
	{
		if (Blackboard::GetInstance()->GetNote("currentScene") == "Assets/Data/menus/mainmenu.hafl")
		{
			SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/Menu/MenuSelect.wav", 0.25f); }
		if (Blackboard::GetInstance()->GetNote("currentScene") == "Assets/Data/menus/menu.hafl")
		{
			SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/Menu/MenuEnterLevel.mp3", 0.25f);
		}

		if (Blackboard::GetInstance()->GetNote("currentScene") == "Assets/Data/menus/mainmenu.hafl" &&
			aButton->GetEventData() == "Assets/Data/menus/menu.hafl" &&
			Blackboard::GetInstance()->GetNote("IntroCutscene") != "viewed" )
		{
			RequestScene("Assets/Data/cutscenes/IntroCutscene/scene1.hafl");
			Blackboard::GetInstance()->SetNote("IntroCutscene", "viewed");
		}
		else
		{
			RequestScene(aButton->GetEventData());
		}
	}
	else if (aButton->GetEventType() == "loadcutscene")
	{
		RequestScene(aButton->GetEventData());
	}
	else if (aButton->GetEventType() == "quitgame")
	{
		RequestQuitGame();
	}
}

void MenuScene::Update(float aDeltaTime)
{
	myAnimTimer += aDeltaTime;
	for (FloaterDrawable& fd : myFloaters)
	{
		fd.myDrawable->SetPosition(fd.myOriginalPosition + CommonUtilities::Vector3f(
			0.f, 0.0125f*std::sinf(myAnimTimer), 0.f
		));
	}
	for (BlinkerDrawable& bd : myBlinkers)
	{
		bd.myDrawable->SetTint({
			bd.myDrawable->GetTint().x,
			bd.myDrawable->GetTint().y,
			bd.myDrawable->GetTint().z,
			0.75f + 0.25f * ((std::roundf((std::cosf(myAnimTimer))*3.f))/3.f)
		});
	}

	for (Drawable* drawable : mySprites)
	{
		drawable->Update(aDeltaTime);
	}

	myWidgetSystem.Update(aDeltaTime);

	if (mySceneType == eMenuSceneType::Splash || mySceneType == eMenuSceneType::Cutscene)
	{
		myCountdown.Update(aDeltaTime);
	}
}

void MenuScene::Render()
{
	FillBufferAndCalculatePoints();
	SortAndRenderBuffer();

	FillGuiBufferAndCalculatePoints();
	SortAndRenderBuffer();
}

void MenuScene::Fade(float aAlpha)
{
	aAlpha;
}

void MenuScene::SortAndRenderBuffer()
{
	if (myBuffer.Empty())
	{
		return;
	}

	std::stable_sort(myBuffer.begin(), myBuffer.end(), [](Drawable* a, Drawable* b)
	{
		if (a->GetPosition().z == b->GetPosition().z)
		{
			return a->GetPosition().y > b->GetPosition().y;
		}
		return a->GetPosition().z > b->GetPosition().z;
	});

	for	(Drawable* drawable : myBuffer)
	{
		drawable->Render();
	}
}

void MenuScene::FillBufferAndCalculatePoints()
{
	//Clear buffer
	myBuffer.RemoveAll();
	
	//Fill buffer
	for (Drawable* drawable : mySprites)
	{
		myBuffer.Add(drawable);
	}

	if (mySceneType == eMenuSceneType::PostGame)
	{
		myBuffer.Add(&myHighscore);
	}

	//Calculate points
	for (Drawable* drawable : myBuffer)
	{
		drawable->CalculatePoint(myCamera);
	}
}

void MenuScene::FillGuiBufferAndCalculatePoints()
{
	myBuffer.RemoveAll();

	myWidgetSystem.FillRenderBuffer(myBuffer);

	for (Drawable* drawable : myBuffer)
	{
		drawable->CalculatePoint(myGuiCamera);
	}
}

void MenuScene::PrintScoreFromFile(const CommonUtilities::Vector2f& aPosition)
{
	std::ifstream i("Assets/Data/score.hafd");

	if (i.bad() || i.fail())
	{
		DL_DEBUG("SCORE LOADING FAILED");
	}

	std::string encodedData((std::istreambuf_iterator<char>(i)), std::istreambuf_iterator<char>());
	std::string decodedData = CommonUtilities::Base64Decode(encodedData);

	nlohmann::json j = nlohmann::json::parse(decodedData.c_str());
	std::cout << j.dump(4) << std::endl;

	int score = 0;

	if (j.find("score") != j.end())
	{
		score = j["score"].get<int>();
	}

	aPosition;
	/*
	myHighscore->myPosition = { aPosition.x, aPosition.y };
	myHighscore->myText = "Highscore: " + std::to_string(score);
	myHighscore->Render();
	*/
}

void MenuScene::LoadSprite(nlohmann::json::object_t aSpriteObject)
{
	Sprite* newSprite = new Sprite(aSpriteObject);
	mySprites.Add(newSprite);
	if (aSpriteObject.find("name") != aSpriteObject.end())
	{
		if (aSpriteObject["name"].get<std::string>() == "splash")
		{
			mySceneType = eMenuSceneType::Splash;
			mySplashTarget = aSpriteObject["tag"].get<std::string>();
			myCountdown.Set(std::bind(&MenuScene::GoToSplashTarget, this), 2.f);
		}
		else if (aSpriteObject["name"].get<std::string>() == "cutscene")
		{
			mySceneType = eMenuSceneType::Cutscene;
			mySplashTarget = aSpriteObject["tag"].get<std::string>();
			myCountdown.Set(std::bind(&MenuScene::GoToSplashTarget, this), 30.f);
		}
		else if (aSpriteObject["name"].get<std::string>() == "floater")
		{
			myFloaters.Add({
				newSprite->GetPosition(),
				newSprite
			});
		}
		else if (aSpriteObject["name"].get<std::string>() == "blinker")
		{
			myBlinkers.Add({
				newSprite
			});
		}
	}
	if (aSpriteObject.find("tag") != aSpriteObject.end())
	{
		if (aSpriteObject["tag"].get<std::string>() == "level2")
		{
			if (Blackboard::GetInstance()->GetNote("level2") != "unlocked")
			{
				newSprite->SetTint({ .25f, .25f, .25f, 1.f });
			}
		}
		else if (aSpriteObject["tag"].get<std::string>() == "level3")
		{
			if (Blackboard::GetInstance()->GetNote("level3") != "unlocked")
			{
				newSprite->SetTint({ .25f, .25f, .25f, 1.f });
			}
		}
	}
}

void MenuScene::LoadAnimation(nlohmann::json::object_t aAnimationObject)
{
	Animation* newAnimation = new Animation(aAnimationObject);
	mySprites.Add(newAnimation);
	if (aAnimationObject.find("tag") != aAnimationObject.end())
	{
		if (aAnimationObject["tag"].get<std::string>() == "level2")
		{
			if (Blackboard::GetInstance()->GetNote("level2") != "unlocked")
			{
				newAnimation->SetTint({ .25f, .25f, .25f, 1.f });
			}
		}
		else if (aAnimationObject["tag"].get<std::string>() == "level3")
		{
			if (Blackboard::GetInstance()->GetNote("level3") != "unlocked")
			{
				newAnimation->SetTint({ .25f, .25f, .25f, 1.f });
			}
		}
	}
}

void MenuScene::ReadScore()
{
	if (Blackboard::GetInstance()->GetNote("postgame") != "true" ||
		Blackboard::GetInstance()->GetNote("currentScene") != "Assets/Data/menus/postgame.hafl" ) 
	{
		return;
	}

	mySceneType = eMenuSceneType::PostGame;

	myHighscore.Init("", Tga2D::EFontSize_48);

	nlohmann::json j = nlohmann::json::parse(Blackboard::GetInstance()->GetNote("score").c_str());
	int score = 0;
	if (j.find("score") != j.end())
	{
		score = j["score"].get<int>();
	}

	std::stringstream ss;
	ss << "Highscore: " << std::to_string(score) << '\n'
		<< "Souls collected: " << Blackboard::GetInstance()->GetNote("postgame_souls_collected") << '\n'
		<< "Soul combos: " << Blackboard::GetInstance()->GetNote("postgame_full_combos");
	

	myHighscore.SetText(ss.str());
	myHighscore.SetPosition( { -0.5f, 0.0f, 1.0f } );

	SaveScoreOnline(score);

	Blackboard::GetInstance()->SetNote("postgame", "false");
}

void MenuScene::SaveScoreOnline(int aScore)
{
	aScore;
	//OnlineLeaderboard::SaveScore(Blackboard::GetInstance()->GetNote("previousScene"), aScore);
}

void MenuScene::GoToSplashTarget()
{
	RequestScene(mySplashTarget);
}

void MenuScene::LoadButton(nlohmann::json::object_t aData)
{
	if (aData.find("name") != aData.end())
	{
		if (aData["name"].get<std::string>() == "slider")
		{
			if (aData["tag"].get<std::string>() == "masterVolume")
			{
				WidgetSlider* slider = WidgetFactory::CreateSlider(aData);
				slider->SetValue(mySoundSystem.GetVolume());
				slider->SetCallback([=](float aValue){
					mySoundSystem.SetVolume(aValue);

					std::string label = "Volume: ";
					label += std::to_string(static_cast<int>(std::roundf(mySoundSystem.GetVolume()*100.f)));
					label += "%";
					slider->SetLabel(label);
				});

				std::string label = "Volume: ";
				label += std::to_string(static_cast<int>(std::roundf(mySoundSystem.GetVolume()*100.f)));
				label += "%";
				slider->SetLabel(label);

				myWidgetSystem.AddWidget(slider);
			}
		}
		else if (aData["name"].get<std::string>() == "checkbox")
		{
			if (aData["tag"].get<std::string>() == "vibration")
			{
				WidgetCheckbox* checkbox = WidgetFactory::CreateCheckbox(aData);
				checkbox->SetCallback([=](bool aValue){
					myXboxController.SetVibration(aValue);
				});
				checkbox->SetValue(myXboxController.GetVibration());

				checkbox->SetLabel("Vibration");

				myWidgetSystem.AddWidget(checkbox);
			}
		}
		else
		{
			std::string datatag = aData["tag"].get<std::string>();
			if (datatag == "level2" ||
				datatag == "level3")
			{
				if (Blackboard::GetInstance()->GetNote(datatag) == "unlocked")
				{
					aData["tag"] = ourLevelMap[datatag];
					WidgetButton* button = WidgetFactory::CreateButton(aData);
					button->SetCallback([=](){
						TriggerButton(button);
					});
					myWidgetSystem.AddWidget(button);
				}
			}
			else
			{
				if (datatag == "level1")
				{
					aData["tag"] = ourLevelMap[datatag];
				}
				WidgetButton* button = WidgetFactory::CreateButton(aData);
				button->SetCallback([=](){
					TriggerButton(button);
				});
				myWidgetSystem.AddWidget(button);
			}
		}
	}
}

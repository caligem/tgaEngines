#pragma once

#include "Scene.h"

#include <map>

namespace CommonUtilities
{
	class Camera;
	class InputManager;
	class XBOXController;
}

class SoundSystemClass;

#include "WidgetSystem.h"
#include "WidgetFactory.h"

#include <Parser\json.hpp>

#include "SoundFactory.h"

#include "CountDown.h"
#include <Text.h>

enum class eMenuSceneType
{
	Menu,
	PostGame,
	Splash,
	Cutscene
};

struct BlinkerDrawable
{
	Drawable* myDrawable;
};

class MenuScene : public Scene
{
public:
	MenuScene(
		const CommonUtilities::InputManager& aInputManager,
		CommonUtilities::Camera& aCamera,
		CommonUtilities::Camera& aGuiCamera,
		CommonUtilities::XBOXController& aXboxController,
		SoundSystemClass& aSoundSystem
	);
	~MenuScene();

	void Update(float aDeltaTime) override;
	void Render() override;
	void Fade(float aAlpha) override;

	void SortAndRenderBuffer() override;
	void FillBufferAndCalculatePoints() override;
	void FillGuiBufferAndCalculatePoints() override;

	void Init(nlohmann::json aJSON);

	void TriggerButton(WidgetButton* aButton);
	void PrintScoreFromFile(const CommonUtilities::Vector2f& aPosition);

	bool IsMenu() override { return true; }
	bool IsCutscene() override { return mySceneType == eMenuSceneType::Cutscene; }

private:
	void ReadScore();
	void SaveScoreOnline(int aScore);
	void GoToSplashTarget();

	void LoadSprite(nlohmann::json::object_t aSpriteObject);
	void LoadAnimation(nlohmann::json::object_t aAnimationObject);
	void LoadButton(nlohmann::json::object_t aData);

	const CommonUtilities::InputManager& myInputManager;
	CommonUtilities::XBOXController& myXboxController;
	CommonUtilities::Camera& myCamera;
	CommonUtilities::Camera& myGuiCamera;
	SoundSystemClass& mySoundSystem;

	WidgetSystem myWidgetSystem;
	SoundFactory mySoundFactory;

	Text myHighscore;

	std::string mySplashTarget;
	CountDown myCountdown;

	eMenuSceneType mySceneType;

	static std::map<std::string, std::string> ourLevelMap;

	CommonUtilities::GrowingArray<BlinkerDrawable> myBlinkers;

};


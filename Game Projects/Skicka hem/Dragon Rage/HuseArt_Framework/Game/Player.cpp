#include "stdafx.h"
#include "Player.h"

#include "ExplosionManager.h"
#include "SFXManager.h"
#include "Blackboard.h"

#include <InputManager.h>
#include <Tweener.h>
#include <XBOXController.h>
#include <Camera.h>
#include <tga2d/math/common_math.h>
#include <Mathf.h>

#include <iostream>
#include <Tweener.h>

#include <tga2d\engine.h>

namespace CU = CommonUtilities;

Player::Player(const CU::InputManager& aInputManager, CU::XBOXController& aXboxController, SoundSystemClass& aSoundSystem, const CommonUtilities::Camera& aCamera, Screenshake& aScreenshake)
	: myInputManager(aInputManager)
	, myXboxController(aXboxController)
	, mySoundSystem(aSoundSystem)
	, myCamera(aCamera)
	, myIsChanging(false)
	, myCurrentHealth(500.0f)
	, myIsSlowed(false)
	, myFireStance(*this, aScreenshake)
	, myIceStance(aCamera, *this, aScreenshake)
	, myLightningStance(*this, aScreenshake, aSoundSystem)
	, myFocusFactor(0.75f)
	, myIceUnlocked(false)
	, myLightningUnlocked(false)
	, myFlashTimer(0.0f)
	, mySlowTimer(0.0f)
	, myScreenshake(aScreenshake)
	, myBeamSlowdown(1.f)
{
}

Player::~Player()
{
	if (myXboxController.IsConnected())
	{
		myXboxController.Vibrate(0.0f, 0.0f);
	}
}

void Player::Init()
{
	myCanChangeStance = true;
	myCanPlayTakeDamageSound = true;

	myMaxHealth = myCurrentHealth;

	myHealthBarPosition = { -Tga2D::CEngine::GetInstance()->GetWindowRatio() + 0.125f, -0.5f, 1.f };
	myPowerBarPosition = { -Tga2D::CEngine::GetInstance()->GetWindowRatio() + 0.125f, -0.85f, 1.0f };

	myHealthBar.SetScaleDirection(eScaleDirection::Y);

	myHealthBar.Init(myMaxHealth, myHealthBarPosition, FLT_MAX, { 1.0f, 1.0f }, true, "Assets/Images/GUI/player_healthBlood.dds", "Assets/Images/GUI/player_healthBar.dds");
	myPowerBar.Init(10.f, myPowerBarPosition, FLT_MAX, { 1.0f, 1.0f }, "Assets/Images/GUI/player_powerBall.dds", "Assets/Images/GUI/player_powerBar.dds");

	myCollider.SetRadius(0.025f);

	myFireStance.Init();
	myIceStance.Init();
	myLightningStance.Init();
	myCurrentStance = &myFireStance;

	myPortraits.Init({ Tga2D::CEngine::GetInstance()->GetWindowRatio() , -1.f, 1.0f });

	myBonusAnimation.InitBonusAnimation();

	myShowNewTimer = 5.0f;
}

void Player::Update(float aDeltaTime)
{
	FinishChange(aDeltaTime);

	myBonusAnimation.UpdateBonusAnimation(aDeltaTime, *this);

	myCurrentStance->SetPosition(myPosition);

	myFireStance.Update(aDeltaTime);
	myIceStance.Update(aDeltaTime);
	myLightningStance.Update(aDeltaTime);
	Stance::UpdateSecondaryCooldown(aDeltaTime);

	myCollider.SetPosition(myPosition);

	myStanceCountDown.Update(aDeltaTime);
	myTakeDamageSoundCountDown.Update(aDeltaTime);

	myHealthBar.Update(aDeltaTime, myCurrentHealth, myHealthBarPosition);
	myPowerBar.Update(aDeltaTime, Stance::GetSecondaryShootCooldown(), myPowerBarPosition);
	myHealthBar.Show();
	myPowerBar.Show();	

	if (myFlashTimer > 0.0f)
	{
		myFlashTimer -= aDeltaTime;
	}

	myCurrentStance->GetCurrentAnimation()->SetTint({
		CommonUtilities::Lerp(myCurrentStance->GetCurrentAnimation()->GetTint().x, 1.0f, aDeltaTime * 10.f),
		CommonUtilities::Lerp(myCurrentStance->GetCurrentAnimation()->GetTint().y, 1.0f, aDeltaTime * 10.f),
		CommonUtilities::Lerp(myCurrentStance->GetCurrentAnimation()->GetTint().z, 1.0f, aDeltaTime * 10.f),
		myCurrentStance->GetCurrentAnimation()->GetTint().w }
	);

	if (!myIceUnlocked && Blackboard::GetInstance()->GetNote("icestance") == "unlocked")
	{
		myPortraits.UnlockStance(ePortraitSelected::ice);
		myIceUnlocked = true;
	}

	if (!myLightningUnlocked && Blackboard::GetInstance()->GetNote("lightningstance") == "unlocked")
	{
		myPortraits.UnlockStance(ePortraitSelected::lightning);
		myLightningUnlocked = true;
	}


	if (Blackboard::GetInstance()->GetNote("icestancefirsttime") == "true")
	{
		ChangeStance(eStance::Ice);
		Blackboard::GetInstance()->SetNote("icestancefirsttime", "false");
	}

	if (Blackboard::GetInstance()->GetNote("lightningstancefirsttime") == "true")
	{
		ChangeStance(eStance::Lightning);
		Blackboard::GetInstance()->SetNote("lightningstancefirsttime", "false");
	}

	if (Blackboard::GetInstance()->GetNote("newstance") == "show")
	{
		myShowNewTimer -= aDeltaTime;

		if (myShowNewTimer < 0)
		{
			Blackboard::GetInstance()->SetNote("newstance", "dontshow");
			myShowNewTimer = 5.0f;
		}
	}

	if (myLightningStance.GetBeam().IsActive())
	{
		myBeamSlowdown = 0.3f;
	}
	else
	{
		myBeamSlowdown = 1.0f;
	}
}

void Player::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	if (static_cast<int>(myFlashTimer * 10.f) % 2 == 0)
	{
		aRenderBuffer.Add(myCurrentStance->GetCurrentAnimation());
	}

	aRenderBuffer.Add(myIceStance.GetBlizzardSprite());

	myBonusAnimation.FillRenderBuffer(aRenderBuffer);

	if (myLightningStance.GetBeam().IsActive())
	{
		myLightningStance.GetBeam().FillRenderBuffer(aRenderBuffer);
	}
}

void Player::FillGuiBuffer(CommonUtilities::GrowingArray<Drawable*>& aGuiBuffer)
{
	myHealthBar.FillRenderBuffer(aGuiBuffer, myFlashTimer);
	myPowerBar.FillRenderBuffer(aGuiBuffer);
	myPortraits.FillRenderBuffer(aGuiBuffer);
}

void Player::PlayerControls(float aDeltaTime)
{
	myLightningStance.GetBeam().Dectivate();

	bool hasUsedKeyboard = false;
	
	KeyboardControls(hasUsedKeyboard, aDeltaTime);

	if (!hasUsedKeyboard)
	{
		XboxControls(aDeltaTime);
	}
}

void Player::KeyboardControls(bool& aHasUsedKeyboard, float aDeltaTime)
{
	//move
	float focusFactor = 1.f;
	if (myInputManager.IsKeyDown(CU::Key_Shift))
	{
		focusFactor = myFocusFactor;
	}
	focusFactor *= myBeamSlowdown;

	if (myInputManager.IsKeyDown(myControls.GetKeyboardMoveUp()))
	{
		Move(CU::Vector3f(0.f, 1.f, 0.f)*aDeltaTime*mySpeed.y*focusFactor);
		aHasUsedKeyboard = true;
	}
	if (myInputManager.IsKeyDown(myControls.GetKeyboardMoveDown()))
	{
		Move(CU::Vector3f(0.f, -1.f, 0.f)*aDeltaTime*mySpeed.y*focusFactor);
		aHasUsedKeyboard = true;
	}
	if (myInputManager.IsKeyDown(myControls.GetKeyboardMoveRight()))
	{
		Move(CU::Vector3f(1.f, 0.f, 0.f)*aDeltaTime*mySpeed.x*focusFactor);
		aHasUsedKeyboard = true;
	}
	if (myInputManager.IsKeyDown(myControls.GetKeyboardMoveLeft()))
	{
		Move(CU::Vector3f(-1.f, 0.f, 0.f)*aDeltaTime*mySpeed.x*focusFactor);
		aHasUsedKeyboard = true;
	}

	//shoot
	if (myInputManager.IsKeyDown(myControls.GetKeyboardPrimaryFire()))
	{
		Shoot(eShootType::Primary);
		aHasUsedKeyboard = true;
	}
	if (myInputManager.IsKeyDown(myControls.GetKeyboardSecondaryFire()))
	{
		Shoot(eShootType::Secondary);
		aHasUsedKeyboard = true;
	}
	//stance 
	if (myInputManager.IsKeyPressed(myControls.GetKeyboardFirstDragonForm()))
	{
		ChangeStance(eStance::Fire);
	}
	if (myInputManager.IsKeyPressed(myControls.GetKeyboardSecondDragonForm()))
	{
		ChangeStance(eStance::Ice);
	}
	if (myInputManager.IsKeyPressed(myControls.GetKeyboardThirdDragonForm()))
	{
		ChangeStance(eStance::Lightning);
	}
}

void Player::XboxControls(float aDeltaTime)
{
	if (!myXboxController.IsConnected())
	{
		return;
	}
	
	//move
	float focusFactor = 1.f;
	if (myXboxController.IsButtonDown(CU::XButton_LB))
	{
		focusFactor = myFocusFactor;
	}
	focusFactor *= myBeamSlowdown;

	Move(
		CU::Vector3f(
			myXboxController.GetStick(myControls.GetXboxMoveStick()).x*mySpeed.x,
			myXboxController.GetStick(myControls.GetXboxMoveStick()).y*mySpeed.y,
			0.f
		)*aDeltaTime*focusFactor
	);

	//shoot
	if (myXboxController.GetAxis(myControls.GetXboxPrimaryFire()) > 0.0f || myXboxController.IsButtonDown(CommonUtilities::XButton_A))
	{
		Shoot(eShootType::Primary);
	}
	if (myXboxController.GetAxis(myControls.GetXboxSecondaryFire()) > 0.0f || myXboxController.IsButtonDown(CommonUtilities::XButton_RB))
	{
		Shoot(eShootType::Secondary);
	}

	//stance
	if (myXboxController.IsButtonPressed(myControls.GetXboxFirstDragonForm()))
	{
		ChangeStance(eStance::Fire);
	}
	if (myXboxController.IsButtonPressed(myControls.GetXboxSecondDragonForm()))
	{
		ChangeStance(eStance::Ice);
	}
	if (myXboxController.IsButtonPressed(myControls.GetXboxThirdDragonForm()))
	{
		ChangeStance(eStance::Lightning);
	}

	if (myFlashTimer > 1.5f)
	{
		myXboxController.Vibrate(0.7f, 0.7f);
	}
	else
	{
		myXboxController.Vibrate(0.0f, 0.0f);
	}
}

void Player::Shoot(eShootType aShootType)
{
	if (myIsChanging)
	{
		return;
	}

	myCurrentStance->Shoot(aShootType);

}

void Player::TakeDamage(float aDamage, const float aDeltaTime)
{
	if (myFlashTimer <= 0.0f && aDamage > 0.0f)
	{
		aDeltaTime;
		if (Blackboard::GetInstance()->GetNote("invincibility") != "on")
		{
			myCurrentHealth -= aDamage;
			myDamageTaken += aDamage;
			myFlashTimer = 2.0f;
		}

		if (myCanPlayTakeDamageSound)
		{
			myCanPlayTakeDamageSound = false;
			SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/Playerhurt.wav", 0.5f);
			myTakeDamageSoundCountDown.Set(std::bind(&Player::CanPlayTakeDamageSound, this), 2.0f);
		}	
	}

	if (myCurrentHealth < 0.f)
	{
		myCurrentHealth = 0.f;

	}	
}

void Player::FinishChange(float aDeltaTime)
{
	if (myTimeSinceChange > 0.2f)
	{
		myIsChanging = false;
	}
	else
	{
		myTimeSinceChange += aDeltaTime;
	}
}

void Player::AddPlayerHealth(float aAmount)
{
	if ((aAmount + myCurrentHealth) < myMaxHealth)
	{
		myCurrentHealth += aAmount;
	}
}

bool Player::IsInside(const CommonUtilities::Vector2f& aPosition)
{
	return myCurrentStance->GetCurrentAnimation()->IsInside(aPosition);
}

void Player::RenderDebug(const CommonUtilities::Camera & aCamera)
{
	myCollider.RenderDebug(aCamera);
	if (myLightningStance.GetBeam().IsActive())
	{
		myLightningStance.GetBeam().RenderDebug(aCamera);
	}
}

void Player::ChangeStance(eStance aStance)
{
	if (aStance == myCurrentStance->GetStance() || !myCanChangeStance)
	{
		return;
	}

	switch (aStance)
	{
	case eStance::Fire:
		myCurrentStance = &myFireStance;
		myPortraits.SetSelected(ePortraitSelected::fire);
		break;
	case eStance::Ice:
		if (Blackboard::GetInstance()->GetNote("icestance") != "unlocked")
		{
			return;
		}
		
		myCurrentStance = &myIceStance;
		myPortraits.SetSelected(ePortraitSelected::ice);
		myPortraits.UnlockStance(ePortraitSelected::ice);
		break;
	case eStance::Lightning:
		if (Blackboard::GetInstance()->GetNote("lightningstance") != "unlocked")
		{
			return;
		}
		
		myCurrentStance = &myLightningStance;
		myPortraits.SetSelected(ePortraitSelected::lightning);
		myPortraits.UnlockStance(ePortraitSelected::lightning);
		break;
	}

	myCanChangeStance = false;
	myIsChanging = true;
	myTimeSinceChange = 0.f;
	myStanceCountDown.Set(std::bind(&Player::CanChangeStance, this), 0.2f);

	SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/ChangeElement.wav", 0.4f);
	PlayPoof();
}

void Player::PlayPoof()
{
	Animation* newExplosion = new Animation("Assets/Images/Player/expl.dds");
	newExplosion->Setup(2, 2, 4);
	newExplosion->SetDuration(0.5f);
	newExplosion->SetLoop(true);
	newExplosion->SetPosition(myPosition + CU::Vector3f(0.f, 0.1f, -0.01f));
	ExplosionManager::GetInstance()->AddExplosion(newExplosion);
}
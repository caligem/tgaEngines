#pragma once

#include "Controls.h"
#include "Camera.h"
#include "Beam.h"
#include "Stance.h"
#include "FireStance.h"
#include "IceStance.h"
#include "LightningStance.h"
#include "CountDown.h"

#include "BonusAnimation.h"

#include "HealthBar.h"
#include "PowerBar.h"

#include "Portraits.h"

#include <GrowingArray.h>
#include <Animation.h>
#include <Vector.h>
#include <SoundSystemClass.h>
#include <CircleCollider.h>

namespace CommonUtilities
{
	class InputManager;
	class XBOXController;
	class Camera;
}


#include <Vector.h>
#include <tga2d/text/text.h>

#include "Actor.h"

#include "Screenshake.h"

class Player : public Actor
{
public:
	Player(const CommonUtilities::InputManager& aInputManager, CommonUtilities::XBOXController& aXboxController, SoundSystemClass& aSoundSystem, const CommonUtilities::Camera& aCamera, Screenshake& aScreenshake);
	~Player();

	void Init();
	void Update(float aDeltaTime);
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer);
	void FillGuiBuffer(CommonUtilities::GrowingArray<Drawable*>& aGuiBuffer);

	inline void SetMovementSpeed(const CommonUtilities::Vector2f& aMovementSpeed) { mySpeed = aMovementSpeed; }
	void PlayerControls(float aDeltaTime);
	void Shoot(eShootType aShootType);

	inline void PlayBonusAnimation() { myBonusAnimation.PlayBonusAnimation(); }

	void TakeDamage(float aDamage, const float aDeltaTime = 1);
	inline const CommonUtilities::Vector3f& GetPoint() const { return myCurrentStance->GetCurrentAnimation()->GetPoint(); }
	inline float GetDamageTaken()const { return myDamageTaken; }

	inline const float GetBlizzardValue() const { return myIceStance.GetBlizzardValue(); }
	inline Beam& GetBeam() { return myLightningStance.GetBeam(); }
		
	inline eElement const GetCurrentElement() const { return myCurrentStance->GetElement(); }
	inline Stance* GetCurrentStance() { return myCurrentStance; }

	inline void SetStatsFire(const Stats& aStats) { myFireStance.SetStats(aStats); }
	inline void SetStatsIce(const Stats& aStats) { myIceStance.SetStats(aStats); }
	inline void SetStatsLightning(const Stats& aStats) { myLightningStance.SetStats(aStats); }
	void AddPlayerHealth(float aAmount);

	inline void IncreaseSecondaryPower(float aAmount) { myCurrentStance->IncreaseSecondaryPower(aAmount); }

	inline void SetFocusFactor(const float& aFocusFactor) { myFocusFactor = aFocusFactor; }

	bool IsInside(const CommonUtilities::Vector2f& aPosition);
	inline bool IsTouching(const CircleCollider* aCollider) { return myCollider.IsTouching(aCollider); }
	inline const CircleCollider* GetCollider() { return &myCollider; }
	inline void SetColliderRadius(float aRadius) { myCollider.SetRadius(aRadius); }

	inline void SetHealth(float aHealth) { myCurrentHealth = aHealth; myMaxHealth = aHealth; }

	inline bool IsDead()const { return myCurrentHealth <= 0.f; }

	void RenderDebug(const CommonUtilities::Camera& aCamera);

private:
	bool myIsChanging;

	bool myIsSlowed;
	float mySlowTimer;
	float myFocusFactor;

	float myCurrentHealth;
	float myMaxHealth;
	CommonUtilities::Vector2f mySpeed;

	//Take Damage
	CountDown myTakeDamageSoundCountDown;
	bool myCanPlayTakeDamageSound;
	void CanPlayTakeDamageSound() { myCanPlayTakeDamageSound = true; }

	float myFlashTimer;

	//Stances
	Stance* myCurrentStance;
	FireStance myFireStance;
	IceStance myIceStance;
	LightningStance myLightningStance;

	void ChangeStance(eStance aStance);
	void PlayPoof();
	void CanChangeStance() { myCanChangeStance = true; }
	CountDown myStanceCountDown;
	bool myCanChangeStance;
	
	//Health Bars
	HealthBar myHealthBar;
	CommonUtilities::Vector3f myHealthBarPosition;
	PowerBar myPowerBar;
	CommonUtilities::Vector3f myPowerBarPosition;

	//Portraits
	Portraits myPortraits;
	bool myIceUnlocked;
	bool myLightningUnlocked;
	float myShowNewTimer;	

	//Controls
	void KeyboardControls(bool& aHasUsedKeyboard, float aDeltaTime);
	void XboxControls(float aDeltaTime);

	Controls myControls;
	const CommonUtilities::InputManager& myInputManager;
	CommonUtilities::XBOXController& myXboxController;

	const CommonUtilities::Camera& myCamera;

	void FinishChange(float aDeltaTime);
	float myTimeSinceChange;

	//Sound
	SoundSystemClass& mySoundSystem;

	CircleCollider myCollider;

	Screenshake& myScreenshake;

	BonusAnimation myBonusAnimation;

	float myBeamSlowdown;
	float myDamageTaken;
};

#include "stdafx.h"
#include "Portraits.h"
#include "Blackboard.h"

void Portraits::Init(const CommonUtilities::Vector3f & aPosition)
{
	myPosition = aPosition;
	float portraitScale = 0.8f;
	myFirePortrait.Init("Assets/Images/GUI/portraitFire.dds");
	myFirePortrait.SetScale({ portraitScale, portraitScale });
	myIcePortrait.Init("Assets/Images/GUI/portraitIce.dds");
	myIcePortrait.SetScale({ portraitScale, portraitScale });
	myLightningPortrait.Init("Assets/Images/GUI/portraitLightning.dds");
	myLightningPortrait.SetScale({ portraitScale, portraitScale });
	myUnkownIcePortrait.Init("Assets/Images/GUI/portraitUnkown.dds");
	myUnkownIcePortrait.SetScale({ portraitScale, portraitScale });
	myUnkownLightningPortrait.Init("Assets/Images/GUI/portraitUnkown.dds");
	myUnkownLightningPortrait.SetScale({ portraitScale, portraitScale });
	mySelected.Init("Assets/Images/GUI/portraitSelected.dds");
	myNew.Init("Assets/Images/GUI/new.dds");
	myNew.SetScale({ portraitScale, portraitScale });

	mySelected.SetScale({ 1.0f, 1.0f });

	myCurrentIce = &myUnkownIcePortrait;
	myCurrentLightning = &myUnkownLightningPortrait;

	myPosition.x -= myFirePortrait.GetUnitSize().x;
	myPosition.y += myFirePortrait.GetUnitSize().y;
	myFirePosition = { myPosition.x, myPosition.y, myPosition.z };
	myIcePosition = { myFirePosition.x - myIcePortrait.GetUnitSize().x, myFirePosition.y, myPosition.z };
	myLightningPosition = { (myFirePosition.x+myIcePosition.x)/2.f, myFirePosition.y + myLightningPortrait.GetUnitSize().y, myPosition.z };
	mySelectedPosition = { myFirePosition.x, myFirePosition.y, myFirePosition.z + 0.001f };

	myFirePortrait.SetPosition(myFirePosition);
	myCurrentIce->SetPosition(myIcePosition);
	myIcePortrait.SetPosition(myIcePosition);
	myCurrentLightning->SetPosition(myLightningPosition);
	myLightningPortrait.SetPosition(myLightningPosition);
	mySelected.SetPosition(mySelectedPosition);

}

void Portraits::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myFirePortrait);
	aRenderBuffer.Add(myCurrentIce);
	aRenderBuffer.Add(myCurrentLightning);
	aRenderBuffer.Add(&mySelected);
	if (Blackboard::GetInstance()->GetNote("newstance") == "show")
	{
		aRenderBuffer.Add(&myNew);
	}
}

void Portraits::SetSelected(ePortraitSelected aSelected)
{
	if (aSelected == ePortraitSelected::fire)
	{
		mySelectedPosition = { myFirePosition.x, myFirePosition.y, myFirePosition.z + 0.001f, };
	}
	else if (aSelected == ePortraitSelected::ice)
	{
		mySelectedPosition = { myIcePosition.x, myIcePosition.y, myIcePosition.z + 0.001f, };
	}
	else if (aSelected == ePortraitSelected::lightning)
	{
		mySelectedPosition = { myLightningPosition.x, myLightningPosition.y, myLightningPosition.z + 0.001f, };
	}

	mySelected.SetPosition(mySelectedPosition);
}

void Portraits::UnlockStance(ePortraitSelected aSelected)
{
	if (aSelected == ePortraitSelected::ice)
	{
		myCurrentIce = &myIcePortrait;
		myNew.SetPosition({ myIcePosition.x - 0.25f, myIcePosition.y, myIcePosition.z });
	}
	else if (aSelected == ePortraitSelected::lightning)
	{
		myCurrentLightning = &myLightningPortrait;
		myNew.SetPosition({ myLightningPosition.x - 0.25f, myLightningPosition.y, myLightningPosition.z });
	}
}

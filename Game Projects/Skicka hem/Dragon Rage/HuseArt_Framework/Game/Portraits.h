#pragma once
#include "Sprite.h"
#include <GrowingArray.h>
#include <cfloat>

enum class ePortraitSelected
{
	fire,
	ice,
	lightning
};

class Portraits
{
public:
	Portraits() = default;
	~Portraits() = default;

	void Init(const CommonUtilities::Vector3f& aPosition);

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer);

	void SetSelected(ePortraitSelected aSelected);
	void UnlockStance(ePortraitSelected aSelected);

protected:
	Sprite* myCurrentIce;
	Sprite* myCurrentLightning;

	Sprite myFirePortrait;
	Sprite myIcePortrait;
	Sprite myLightningPortrait;
	Sprite myUnkownIcePortrait;
	Sprite myUnkownLightningPortrait;
	Sprite mySelected;
	Sprite myNew;

	CommonUtilities::Vector3f myFirePosition;
	CommonUtilities::Vector3f myIcePosition;
	CommonUtilities::Vector3f myLightningPosition;
	CommonUtilities::Vector3f mySelectedPosition;

	CommonUtilities::Vector3f myPosition;
};


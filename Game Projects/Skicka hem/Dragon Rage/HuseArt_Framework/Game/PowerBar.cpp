#include "stdafx.h"
#include "PowerBar.h"
#include "SFXManager.h"

#include <tga2d/math/common_math.h>


void PowerBar::Init(float aMaxValue, const CommonUtilities::Vector3f & aPosition, float aShowTimer, const CommonUtilities::Vector2f & aScale, const char * aFramePath, const char * aSubstancePath)
{
	myScale = aScale;
	mySubstance.Init(aFramePath);
	myFrame.Init(aSubstancePath);
	SetMaxValue(aMaxValue);
	SetCurrentValue(aMaxValue);
	SetPosition(aPosition);
	SetShowTimer(aShowTimer);
	myFrame.SetScale(myScale);
	mySubstance.SetScale(myScale);

	myTimer = 0.f;

	myReadyAnimation.Init("Assets/Images/Projectiles/powerBall_charge.dds");
	myReadyAnimation.Setup(2, 2, 4);
	myReadyAnimation.SetPosition({ mySubstancePosition.x, mySubstancePosition.y, mySubstancePosition.z + 0.001f });
}

void PowerBar::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer, float aFlashTimer)
{
	if (myTimer > 0.f)
	{
		aRenderBuffer.Add(&myFrame);
		if (static_cast<int>(aFlashTimer * 10.f) % 2 == 0)
		{
			mySubstance.SetTint({ 1.0f, 1.0f, 1.0f, 1.0f });
		}
		else
		{
			mySubstance.SetTint({ 100.0f, 100.0f, 100.0f, 1.0f });
		}

		aRenderBuffer.Add(&mySubstance);
	}

	if (myRenderAnimation)
	{
		aRenderBuffer.Add(&myReadyAnimation);
	}

}

void PowerBar::Update(float aDeltaTime, float aCurrentValue, const CommonUtilities::Vector3f & aPosition)
{
	myReadyAnimation.Update(aDeltaTime);

	if (myTimer < 0.f)
	{
		return;
	}
	else
	{
		myTimer -= aDeltaTime;
	}

	if (aCurrentValue > 10.f)
	{
		myRenderAnimation = true;

		SetCurrentValue(10.f);
		if (myReadyUpSound)
		{
			SFXManager::GetInstance()->PlaySound("Assets/Audio/sfx/PowerCharged.wav", 2.0f);
			myReadyUpSound = false;
		}
	}
	else
	{
		myRenderAnimation = false;
		myReadyUpSound = true;
		SetCurrentValue(aCurrentValue);
	}
	SetPosition(aPosition);

	float alpha = (myCurrentValue / myMaxValue);

	mySubstance.SetScale({ myScale.x , myScale.y * alpha });
	mySubstance.SetUVScale({ myScale.x , myScale.y * alpha });
	mySubstance.SetUVOffset({ 0.f, myScale.y - alpha });
}

void PowerBar::SetPosition(CommonUtilities::Vector3f aPosition)
{
	myFramePosition = aPosition;
	mySubstancePosition = myFramePosition;
	mySubstancePosition.z = myFramePosition.z + 0.001f;
	mySubstancePosition.y -= (myScale.y * (1.f - myCurrentValue / myMaxValue)) * (mySubstance.GetUnitSize().y / 2.f);

	myFrame.SetPosition(myFramePosition);
	mySubstance.SetPosition(mySubstancePosition);
}

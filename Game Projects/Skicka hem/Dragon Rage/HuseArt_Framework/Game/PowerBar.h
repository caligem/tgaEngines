#pragma once
#include "Bar.h"
#include <Animation.h>

class PowerBar : public Bar
{
public:
	PowerBar() = default;
	~PowerBar() = default;

	void Init(float aMaxValue, const CommonUtilities::Vector3f& aPosition, float aShowTimer = FLT_MAX, const CommonUtilities::Vector2f& aScale = { 1.0f, 1.0f }, const char* aFramePath = "Assets/Images/Sprites/healthbar.dds", const char* aSubstancePath = "Assets/Images/Sprites/emptybar.dds");

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer, float aFlashTimer = 0.0f) override;

	void Update(float aDeltaTime, float aCurrentValue, const CommonUtilities::Vector3f& aPosition) override;
	void SetPosition(CommonUtilities::Vector3f aPosition) override;

private:
	bool myReadyUpSound = false;
	bool myRenderAnimation = false;
	Animation myReadyAnimation;
};


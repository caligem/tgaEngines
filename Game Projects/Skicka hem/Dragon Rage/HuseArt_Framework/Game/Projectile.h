#pragma once
#include "Actor.h"

#include "Element.h"

namespace CommonUtilities
{
	class Camera;
}

#include <Vector.h>
#include <GrowingArray.h>

#include <Animation.h>

class Player;
class Enemy;

#include <CircleCollider.h>

class Projectile : public Actor
{
public:
	Projectile() = default;
	~Projectile() = default;

	virtual void Init(const char* aProjectileType, const CommonUtilities::Vector3f& aPosition, float aSpeed) = 0;
	virtual void Update(float) = 0;
	virtual void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) = 0;
	virtual void SetGlow(bool aGlow, CommonUtilities::Vector4f aGlowTint) { myGlow = aGlow; myGlowTint = aGlowTint; }

	virtual inline const float GetDamage() = 0;
	virtual void SetDamage(const float aDamage) = 0;
	virtual void Hit(Player& aPlayer, const float aDeltaTime = 1) = 0;
	virtual void Collided() { myIsDead = true; }
	void Kill() { myIsDead = true; }
	const char* GetType() const { return myProjectileType; }
	virtual const CommonUtilities::Vector3f& GetPoint() const = 0;
	virtual bool IsDead() = 0;

	inline void SetElement(const eElement& aElement) { myElement = aElement; }
	inline const eElement GetElement() const { return myElement; }

	inline const float GetSpeed() const { return mySpeed; }
	inline const float GetRotation() const { return myRotation; }

	virtual inline bool IsTouching(const CircleCollider* aCollider) { return myAnimation.IsTouching(aCollider); }

	virtual void RenderDebug(const CommonUtilities::Camera& aCamera) { myAnimation.RenderDebug(aCamera); }

protected:
	Animation myAnimation;
	Animation myGlowAnimation;
	const char* myProjectileType = "";
	float myDamage;
	float mySpeed;
	float myRotation;
	bool myIsDead = false;

	eElement myElement;

	CommonUtilities::Vector4f myGlowTint;
	bool myGlow = false;

};


#include "stdafx.h"
#include "ProjectileArrow.h"

#include "Player.h"

namespace CU = CommonUtilities;


void ProjectileArrow::Init(const char * aProjectileType, const CU::Vector3f & aPosition, float aSpeed)
{
	aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_arrow.dds", aPosition);
	myPosition = aPosition;
	mySpeed = aSpeed;
}

void ProjectileArrow::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;

	myAnimation.SetPosition(myPosition);
}

void ProjectileArrow::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileArrow::SetDirection(const CU::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileArrow::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileArrow::Hit(Player & aPlayer, const float aDeltaTime)
{
	aDeltaTime;
	aPlayer.TakeDamage(myDamage);
	myIsDead = true;
}

const CU::Vector3f & ProjectileArrow::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileArrow::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

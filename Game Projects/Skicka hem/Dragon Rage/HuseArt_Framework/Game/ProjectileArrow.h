#pragma once

#include "Projectile.h"

class ProjectileArrow : public Projectile
{
public:
	ProjectileArrow() = default;
	~ProjectileArrow() = default;

	void Init(const char* aProjectileType, const CommonUtilities::Vector3f& aPosition, float aSpeed) override;
	void Update(float aDeltaTime) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;
	void SetDirection(const CommonUtilities::Vector3f& aDirection);

	inline const float GetDamage() override { return myDamage; }
	void SetDamage(const float aDamage) override;
	void Hit(Player& aPlayer, const float aDeltaTime = 1) override;
	const CommonUtilities::Vector3f& GetPoint() const override;
	bool IsDead() override;

private:
	CommonUtilities::Vector3f myDirection;
};


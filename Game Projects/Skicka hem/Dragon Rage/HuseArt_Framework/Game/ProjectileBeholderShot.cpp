#include "stdafx.h"
#include "ProjectileBeholderShot.h"

#include "Player.h"

namespace CU = CommonUtilities;

void ProjectileBeholderShot::Init(const char* aProjectileType, const CU::Vector3f& aPosition, float aSpeed)
{
	aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_novePurple.dds", aPosition);
	myGlowAnimation.Init("Assets/Images/GUI/powerShine.dds", aPosition);
	mySpeed = aSpeed;
	myPosition = aPosition;
	myStartPos = { aPosition.x, aPosition.y + 0.08f, aPosition.z };
	myIncreaseTint = true;
}

void ProjectileBeholderShot::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;
	myAnimation.SetPosition(myPosition);

	if (myGlow)
	{
		if (myIncreaseTint)
		{
			if (myGlowTint.w < 0.6f)
			{
				myGlowTint.w += aDeltaTime * 5.0f;
			}
			else
			{
				myIncreaseTint = false;
			}
		}
		else if (myGlowTint.w > 0.0f)
		{
			myGlowTint.w -= aDeltaTime * 5.0f;
		}

		myGlowAnimation.SetPosition(myStartPos);
		myGlowAnimation.SetTint({ myGlowTint.x, myGlowTint.y, myGlowTint.z, myGlowTint.w });
	}
}

void ProjectileBeholderShot::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);

	if (myGlow)
	{
		aRenderBuffer.Add(&myGlowAnimation);
	}
}

void ProjectileBeholderShot::SetDirection(const CU::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileBeholderShot::Hit(Player& aPlayer, const float aDeltaTime)
{
	aDeltaTime;
	aPlayer.TakeDamage(myDamage);
	myIsDead = true;
}

void ProjectileBeholderShot::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

const CU::Vector3f & ProjectileBeholderShot::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileBeholderShot::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

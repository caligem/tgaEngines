#include "stdafx.h"
#include "ProjectileBoulder.h"

#include "Player.h"

namespace CU = CommonUtilities;


void ProjectileBoulder::Init(const char * aProjectileType, const CU::Vector3f& aPosition, float aSpeed)
{
	aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_boulder.dds", aPosition);
	mySpeed = aSpeed;
	myPosition = aPosition;
}

void ProjectileBoulder::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;

	myAnimation.SetPosition(myPosition);
}

void ProjectileBoulder::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileBoulder::SetDirection(const CU::Vector3f & aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileBoulder::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileBoulder::Hit(Player & aPlayer, const float aDeltaTime)
{
	aDeltaTime;
	aPlayer.TakeDamage(myDamage);
	myIsDead = true;
}

const CU::Vector3f & ProjectileBoulder::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileBoulder::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

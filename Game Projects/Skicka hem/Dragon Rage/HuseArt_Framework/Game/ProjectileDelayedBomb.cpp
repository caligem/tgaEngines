#include "stdafx.h"
#include "ProjectileDelayedBomb.h"

#include "ProjectileFactory.h"
#include "ProjectileManager.h"
#include "ExplosionManager.h"

#include "Player.h"
#include "Random.h"

#include <tga2d/math/common_math.h>

namespace CU = CommonUtilities;

ProjectileDelayedBomb::ProjectileDelayedBomb(float aDetonationTime, bool aCluster,const int aBombType)
	: myDetonationTime(aDetonationTime)
	, myCluster(aCluster)
	, myBombType(aBombType)
{}

void ProjectileDelayedBomb::Init(const char* aProjectileType, const CommonUtilities::Vector3f& aPosition, float aSpeed)
{
	if (myBombType == 1)
	{
		myAnimation.Init("Assets/Images/Projectiles/bonedragon_explosion_crosshair.dds", aPosition);
		myAnimation.Setup(2, 2, 4);
	}
	else if (myBombType == 2)
	{
		myAnimation.Init("Assets/Images/Projectiles/destrictiveSeed.dds", aPosition);
		myAnimation.Setup(2, 2, 4);
		myAnimation.SetDuration(myDetonationTime);
		myAnimation.SetLoop(false);
	}

	myShine.Init("Assets/Images/GUI/powerShine.dds", { aPosition.x, aPosition.y, aPosition.z + 0.001f });
	myShine.SetScale({ 0.5f, 0.5f });
	myShineAlfa = 0.0f;
	myTintAmount = 12.0f;
	myAnimation.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f - (myTintAmount / 12.0f) });
	//myAnimation.Setup(6, 4, 24);
	aProjectileType;
	aSpeed;
	myPosition = aPosition;
	mySpeed = 0.0f;
	myRotation = 0;
	myDamageFactor = 0.0f;

	myCountdown.Set(std::bind(&ProjectileDelayedBomb::Explode, this), myDetonationTime);
}

void ProjectileDelayedBomb::Update(float aDeltaTime)
{
	if (myTintAmount > 1.0f)
	{
		myTintAmount -= aDeltaTime * 10.0f;
	}
	else
	{
		myAnimation.Update(aDeltaTime);
		myCountdown.Update(aDeltaTime);
	}

	if (1.0f - (myTintAmount / 12.0f) < 0.5f)
	{
		myShineAlfa += aDeltaTime * 10.0f;
	}
	else
	{
		myShineAlfa -= aDeltaTime * 10.0f;
	}

	myAnimation.SetTint({ myTintAmount, myTintAmount, myTintAmount, 1.0f - (myTintAmount / 12.0f) });
	myShine.SetTint({ 1.0f, 1.0f, 1.0f, myShineAlfa });

	if (myBombType == 1)
	{
		myAnimation.Update(aDeltaTime);
	}
	myAnimation.SetPosition(myPosition);
}

void ProjectileDelayedBomb::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	if (myDamageFactor == 0.0f)
	{
		aRenderBuffer.Add(&myAnimation);
	}

	aRenderBuffer.Add(&myShine);
}

void ProjectileDelayedBomb::SetDirection(const CommonUtilities::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileDelayedBomb::Hit(Player& aPlayer, const float aDeltaTime)
{

	aPlayer.TakeDamage(myDamage * myDamageFactor, aDeltaTime);

	//myIsDead = true;
}

void ProjectileDelayedBomb::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

const CommonUtilities::Vector3f & ProjectileDelayedBomb::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileDelayedBomb::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

void ProjectileDelayedBomb::Explode()
{
	Animation* newExplosion = new Animation("Assets/Images/Projectiles/explosion.dds");
	newExplosion->Setup(2, 3, 6);
	newExplosion->SetDuration(0.5f);
	newExplosion->SetScale({ 1.0f, 1.0f });
	newExplosion->SetPosition(myPosition);
	ExplosionManager::GetInstance()->AddExplosion(newExplosion);
	myDamageFactor = 1.0f;
	
	if (myCluster)
	{
		SpawnSplinter();
	}

	myCountdown.Set(std::bind(&ProjectileDelayedBomb::Kill, this), 0.5f);
}

void ProjectileDelayedBomb::SpawnSplinter()
{
	float random = CommonUtilities::Random();

	if (random < 1.f / 3.f)
	{
		for (float index = 0; index < 4; ++index)
		{
			ProjectileWispFireShot* projectile = ProjectileFactory::CreateWispFireShot({ myPosition.x, myPosition.y, myPosition.z - 0.001f },
				0.5f,
				CU::Vector3f(std::cos((Tga2D::Pif / 2.0f) * index), std::sin((Tga2D::Pif / 2.0f) * index), 0.0f));
			projectile->SetDamage(myDamage);
			ProjectileManager::GetInstance()->EnemyShoot(projectile);
		}
	}
	else if (random < 2.f/3.f)
	{
		for (float index = 0; index < 4; ++index)
		{
			ProjectileWispIceShot* projectile = ProjectileFactory::CreateWispIceShot({ myPosition.x, myPosition.y, myPosition.z - 0.001f },
				0.5f,
				CU::Vector3f(std::cos((Tga2D::Pif / 2.0f) * index), std::sin((Tga2D::Pif / 2.0f) * index), 0.0f));
			projectile->SetDamage(myDamage);
			ProjectileManager::GetInstance()->EnemyShoot(projectile);
		}
	}
	else
	{
		for (float index = 0; index < 4; ++index)
		{
			ProjectileWispLightningShot* projectile = ProjectileFactory::CreateWispLightningShot({ myPosition.x, myPosition.y, myPosition.z - 0.001f },
				0.5f,
				CU::Vector3f(std::cos((Tga2D::Pif / 2.0f) * index), std::sin((Tga2D::Pif / 2.0f) * index), 0.0f));
			projectile->SetDamage(myDamage);
			ProjectileManager::GetInstance()->EnemyShoot(projectile);
		}
	}
}


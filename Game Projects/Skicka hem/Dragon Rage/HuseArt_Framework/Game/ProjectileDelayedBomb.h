#pragma once
#include "Projectile.h"
#include "CountDown.h"

#include <Animation.h>

class ProjectileDelayedBomb : public Projectile
{
public:
	ProjectileDelayedBomb(float aDetonationTime, bool aCluster,const int aBombType);
	~ProjectileDelayedBomb() = default;

	void Init(const char* aProjectileType, const CommonUtilities::Vector3f& aPosition, float aSpeed) override;
	void Update(float aDeltaTime) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;
	void SetDirection(const CommonUtilities::Vector3f& aDirection);

	const float GetDamage() override { return myDamage; }
	void SetDamage(const float aDamage) override;
	const CommonUtilities::Vector3f& GetPoint() const override;
	void Hit(Player& aPlayer, const float aDeltaTime = 1) override;
	bool IsDead() override;

private:
	void Explode();
	void SpawnSplinter();

	Animation myShine;

	CountDown myCountdown;

	CommonUtilities::Vector3f myDirection;
	float myRotation;
	float myDamageFactor;
	float myDetonationTime;
	float myTintAmount;
	float myShineAlfa;
	bool myCluster;

	const int myBombType;
};



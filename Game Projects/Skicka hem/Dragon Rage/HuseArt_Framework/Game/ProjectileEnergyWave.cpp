#include "stdafx.h"
#include "ProjectileEnergyWave.h"

#include "Player.h"

#include <tga2d/math/common_math.h>

namespace CU = CommonUtilities;

ProjectileEnergyWave::ProjectileEnergyWave()
{}


ProjectileEnergyWave::~ProjectileEnergyWave()
{}

void ProjectileEnergyWave::Init(const char * aProjectileType, const CU::Vector3f & aPosition, float aSpeed)
{
	myAnimation.Init("Assets/Images/Projectiles/projectile_natureShield.dds", aPosition);
	//myAnimation.Setup(6, 4, 24);
	aProjectileType;
	myPosition = aPosition;
	mySpeed = aSpeed;
	myIsDead = false;
}

void ProjectileEnergyWave::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;

	myAnimation.SetPosition(myPosition);
}

void ProjectileEnergyWave::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileEnergyWave::SetDirection(const CU::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileEnergyWave::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileEnergyWave::Hit(Player & aPlayer, const float aDeltaTime)
{
	aPlayer.TakeDamage(myDamage, aDeltaTime);
	//myIsDead = true;
}

const CommonUtilities::Vector3f & ProjectileEnergyWave::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileEnergyWave::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

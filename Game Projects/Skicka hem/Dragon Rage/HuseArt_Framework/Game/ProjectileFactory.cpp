#include "stdafx.h"
#include "ProjectileFactory.h"

namespace CU = CommonUtilities;

namespace ProjectileFactory
{
	ProjectileFireBall* CreateFireBall(const CU::Vector3f & aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileFireBall* newProjectileFireBall = new ProjectileFireBall();
		newProjectileFireBall->Init("", aPosition, aSpeed);
		newProjectileFireBall->SetDirection(aDirection);
		return newProjectileFireBall;
	}

	ProjectileIcicle* CreateIcicles(const CU::Vector3f & aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileIcicle* newProjectileIcicles = new ProjectileIcicle();
		newProjectileIcicles->Init("", aPosition, aSpeed);
		newProjectileIcicles->SetDirection(aDirection);
		return newProjectileIcicles;
	}

	ProjectileArrow* CreateArrow(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileArrow* newProjectileArrow = new ProjectileArrow();
		newProjectileArrow->Init("", aPosition, aSpeed);
		newProjectileArrow->SetDirection(aDirection);
		return newProjectileArrow;
	}

	ProjectileWispFireShot* CreateWispFireShot(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileWispFireShot* newProjectileArrow = new ProjectileWispFireShot();
		newProjectileArrow->Init("", aPosition, aSpeed);
		newProjectileArrow->SetDirection(aDirection);
		return newProjectileArrow;
	}

	ProjectileWispIceShot* CreateWispIceShot(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileWispIceShot* newProjectileArrow = new ProjectileWispIceShot();
		newProjectileArrow->Init("", aPosition, aSpeed);
		newProjectileArrow->SetDirection(aDirection);
		return newProjectileArrow;
	}

	ProjectileWispLightningShot* CreateWispLightningShot(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileWispLightningShot* newProjectileArrow = new ProjectileWispLightningShot();
		newProjectileArrow->Init("", aPosition, aSpeed);
		newProjectileArrow->SetDirection(aDirection);
		return newProjectileArrow;
	}

	ProjectileWispNatureShot* CreateWispNatureShot(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileWispNatureShot* newProjectileArrow = new ProjectileWispNatureShot();
		newProjectileArrow->Init("", aPosition, aSpeed);
		newProjectileArrow->SetDirection(aDirection);
		return newProjectileArrow;
	}

	ProjectileBoulder* CreateBoulder(const CommonUtilities::Vector3f & aPosition, const float aSpeed, const CommonUtilities::Vector3f & aDirection)
	{
		ProjectileBoulder* newBoulder = new ProjectileBoulder();
		newBoulder->Init("", aPosition, aSpeed);
		newBoulder->SetDirection(aDirection);
		return newBoulder;
	}

	ProjectileMelee* CreateMelee(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection, Enemy& aEnemy)
	{
		ProjectileMelee* newProjectileMelee = new ProjectileMelee(aEnemy);
		newProjectileMelee->Init("", aPosition, aSpeed);
		newProjectileMelee->SetDirection(aDirection);
		return newProjectileMelee;
	}

	ProjectileNatureMelee* CreateNatureMelee(const CommonUtilities::Vector3f & aPosition, const float aSpeed, eAttackType aAttackType, Player & aPlayer)
	{
		ProjectileNatureMelee* newProjectileNatureMelee = new ProjectileNatureMelee(aPlayer);
		newProjectileNatureMelee->Init("naturemelee", aPosition, aSpeed);
		newProjectileNatureMelee->SetAttackType(aAttackType);
		return newProjectileNatureMelee;
	}

	ProjectileNatureShot* CreateNatureShot(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileNatureShot* newProjectileNatureShot = new ProjectileNatureShot();
		newProjectileNatureShot->Init("", aPosition, aSpeed);
		newProjectileNatureShot->SetDirection(aDirection);
		return newProjectileNatureShot;
	}
	ProjectileFrozenOrb* CreateFrozenOrb(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileFrozenOrb* newProjectileFrozenOrb = new ProjectileFrozenOrb();
		newProjectileFrozenOrb->Init("", aPosition, aSpeed);
		newProjectileFrozenOrb->SetDirection(aDirection);
		return newProjectileFrozenOrb;
	}
	ProjectileFrostBreath* CreateFrostBreath(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileFrostBreath* newProjectileFrostBreath = new ProjectileFrostBreath();
		newProjectileFrostBreath->Init("", aPosition, aSpeed);
		newProjectileFrostBreath->SetDirection(aDirection);
		return newProjectileFrostBreath;
	}
	ProjectileFireBreath* CreateFireBreath(const CommonUtilities::Vector3f & aPosition, const float aSpeed, const CommonUtilities::Vector3f & aDirection)
	{
		ProjectileFireBreath* newProjectileFireBreath = new ProjectileFireBreath();
		newProjectileFireBreath->Init("firebreath", aPosition, aSpeed);
		newProjectileFireBreath->SetDirection(aDirection);
		return newProjectileFireBreath;
	}
	ProjectileEnergyWave* CreateEnergyWave(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileEnergyWave* newProjectileEnergyWave = new ProjectileEnergyWave();
		newProjectileEnergyWave->Init("", aPosition, aSpeed);
		newProjectileEnergyWave->SetDirection(aDirection);
		return newProjectileEnergyWave;
	}
	ProjectileGranade* CreateGranade(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileGranade* newProjectileGranade = new ProjectileGranade();
		newProjectileGranade->Init("", aPosition, aSpeed);
		newProjectileGranade->SetDirection(aDirection);
		return newProjectileGranade;
	}
	ProjectileLightningRod* CreateLightningRod(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection)
	{
		ProjectileLightningRod* newProjectileLightningRod = new ProjectileLightningRod();
		newProjectileLightningRod->Init("", aPosition, aSpeed);
		newProjectileLightningRod->SetDirection(aDirection);
		return newProjectileLightningRod;
	}
	ProjectileLightningNova * CreateLightningNova(const CommonUtilities::Vector3f & aPosition, const float aSpeed, const CommonUtilities::Vector3f & aDirection)
	{
		ProjectileLightningNova* newProjectileLightningNova = new ProjectileLightningNova();
		newProjectileLightningNova->Init("", aPosition, aSpeed);
		newProjectileLightningNova->SetDirection(aDirection);
		return newProjectileLightningNova;
	}
	ProjectileDelayedBomb* CreateDelayedBomb(const CommonUtilities::Vector3f & aPosition, const float aSpeed, const CommonUtilities::Vector3f & aDirection, float aDetonationTime, bool aCluster, const int aBombType)
	{
		ProjectileDelayedBomb* newProjectileDelayedBomb = new ProjectileDelayedBomb(aDetonationTime, aCluster, aBombType);
		newProjectileDelayedBomb->Init("", aPosition, aSpeed);
		newProjectileDelayedBomb->SetDirection(aDirection);
		return newProjectileDelayedBomb;
	}
	ProjectileIceFist* CreateIceFist(const CommonUtilities::Vector3f & aPosition, const float aSpeed, const CommonUtilities::Vector3f & aDirection)
	{
		ProjectileIceFist* newProjectileIceFist = new ProjectileIceFist();
		newProjectileIceFist->Init("", aPosition, aSpeed);
		newProjectileIceFist->SetDirection(aDirection);
		return newProjectileIceFist;
	}
	ProjectileBeholderShot* CreateBeholderShot(const CommonUtilities::Vector3f & aPosition, const float aSpeed, const CommonUtilities::Vector3f & aDirection)
	{
		ProjectileBeholderShot* newProjectileIceFist = new ProjectileBeholderShot();
		newProjectileIceFist->Init("", aPosition, aSpeed);
		newProjectileIceFist->SetDirection(aDirection);
		return newProjectileIceFist;
	}
}
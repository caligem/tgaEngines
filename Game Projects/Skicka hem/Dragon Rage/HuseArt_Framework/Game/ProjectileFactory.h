#pragma once

#include <Vector.h>

#include "ProjectileFireBall.h"
#include "ProjectileIcicle.h"
#include "ProjectileArrow.h"
#include "ProjectileWispFireShot.h"
#include "ProjectileWispIceShot.h"
#include "ProjectileWispLightningShot.h"
#include "ProjectileWispNatureShot.h"
#include "ProjectileMelee.h"
#include "ProjectileNatureShot.h"
#include "ProjectileFrozenOrb.h"
#include "ProjectileFireBreath.h"
#include "ProjectileBoulder.h"
#include "ProjectileFrostBreath.h"
#include "ProjectileEnergyWave.h"
#include "ProjectileGranade.h"
#include "ProjectileLightningRod.h"
#include "ProjectileLightningNova.h"
#include "ProjectileNatureMelee.h"
#include "ProjectileDelayedBomb.h"
#include "ProjectileIceFist.h"
#include "ProjectileBeholderShot.h"

namespace ProjectileFactory
{
	ProjectileFireBall* CreateFireBall(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileIcicle* CreateIcicles(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileArrow* CreateArrow(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileWispFireShot* CreateWispFireShot(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileWispIceShot* CreateWispIceShot(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileWispLightningShot* CreateWispLightningShot(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileMelee* CreateMelee(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection, Enemy& aEnemy);
	ProjectileNatureMelee* CreateNatureMelee(const CommonUtilities::Vector3f& aPosition, const float aSpeed, eAttackType aAttackType, Player& aPlayer);
	ProjectileNatureShot* CreateNatureShot(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileWispNatureShot* CreateWispNatureShot(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileFrozenOrb* CreateFrozenOrb(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileBoulder* CreateBoulder(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileFrostBreath* CreateFrostBreath(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileFireBreath* CreateFireBreath(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileEnergyWave* CreateEnergyWave(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileGranade* CreateGranade(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileLightningRod* CreateLightningRod(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileLightningNova* CreateLightningNova(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileDelayedBomb* CreateDelayedBomb(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection, float aDetonationTime, bool aCluster = false, const int aBombType = 1);
	ProjectileIceFist* CreateIceFist(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
	ProjectileBeholderShot* CreateBeholderShot(const CommonUtilities::Vector3f& aPosition, const float aSpeed, const CommonUtilities::Vector3f& aDirection);
}


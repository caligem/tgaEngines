#include "stdafx.h"
#include "ProjectileFireBall.h"

#include "Player.h"

namespace CU = CommonUtilities;


void ProjectileFireBall::Init(const char * aProjectileType, const CU::Vector3f& aPosition, float aSpeed)
{
	aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_fireball.dds", aPosition);
	myAnimation.Setup(2, 2, 4);
	mySpeed = aSpeed;
	myPosition = aPosition;
}

void ProjectileFireBall::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;

	myAnimation.Update(aDeltaTime);
	myAnimation.SetPosition(myPosition);
}

void ProjectileFireBall::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileFireBall::SetDirection(const CU::Vector3f & aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileFireBall::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileFireBall::Hit(Player & aPlayer, const float aDeltaTime)
{
	aDeltaTime;
	aPlayer.TakeDamage(myDamage);
	myIsDead = true;
}

const CU::Vector3f & ProjectileFireBall::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileFireBall::IsDead()
{
 	return myIsDead || myAnimation.IsOutsideScreen();
}

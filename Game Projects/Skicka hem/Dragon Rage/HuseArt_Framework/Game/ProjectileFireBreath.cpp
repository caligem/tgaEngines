#include "stdafx.h"
#include "ProjectileFireBreath.h"

#include "Player.h"

#include <tga2d/math/common_math.h>

namespace CU = CommonUtilities;

ProjectileFireBreath::ProjectileFireBreath()
	: myDamageTimer(0.f)
{}


ProjectileFireBreath::~ProjectileFireBreath()
{}

void ProjectileFireBreath::Init(const char * aProjectileType, const CU::Vector3f & aPosition, float aSpeed)
{
	myAnimation.Init("Assets/Images/Projectiles/projectile_firebreath.dds", aPosition);
	//myAnimation.Setup(6, 4, 24);
	myProjectileType = aProjectileType;
	myPosition = aPosition;
	mySpeed = aSpeed;
	myIsDead = false;
	myScale = 0.5f;
}

void ProjectileFireBreath::Update(float aDeltaTime)
{
	myScale += aDeltaTime * 1.5f;
	myPosition += myDirection * mySpeed * aDeltaTime;
	myAnimation.SetScale(CU::Vector2f(myScale, myScale));
	myAnimation.SetPosition(myPosition);
}

void ProjectileFireBreath::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileFireBreath::SetDirection(const CU::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileFireBreath::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileFireBreath::Hit(Player & aPlayer, const float aDeltaTime)
{
	if (myDamageTimer <= 0.f)
	{
		myDamageTimer = 0.5f;
		aPlayer.TakeDamage(myDamage, aDeltaTime);
	}
}

const CommonUtilities::Vector3f & ProjectileFireBreath::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileFireBreath::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

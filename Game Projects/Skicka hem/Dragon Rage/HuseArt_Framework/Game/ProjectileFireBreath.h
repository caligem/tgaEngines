#pragma once

#include "Projectile.h"

class ProjectileFireBreath : public Projectile
{
public:
	ProjectileFireBreath();
	~ProjectileFireBreath();

	void Init(const char* aProjectileType, const CommonUtilities::Vector3f& aPos, float aSpeed) override;
	void Update(float aDeltaTime) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;
	void SetDirection(const CommonUtilities::Vector3f& aDirection);

	inline const float GetDamage() override { return myDamage; }
	void SetDamage(const float aDamage) override;
	void Hit(Player& aPlayer, const float aDeltaTime = 1) override;
	const CommonUtilities::Vector3f& GetPoint() const override;
	inline void Collided() override {}
	bool IsDead() override;

private:
	float myDamageTimer;
	float myScale;

	CommonUtilities::Vector3f myDirection;
};

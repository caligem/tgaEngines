#include "stdafx.h"
#include "ProjectileFrostBreath.h"

#include "Player.h"
#include <Random.h>

#include <tga2d/math/common_math.h>

namespace CU = CommonUtilities;

ProjectileFrostBreath::ProjectileFrostBreath()
	: myDamageTimer(0.f)
{}


ProjectileFrostBreath::~ProjectileFrostBreath()
{}

void ProjectileFrostBreath::Init(const char * aProjectileType, const CU::Vector3f & aPosition, float aSpeed)
{
	myAnimation.Init("Assets/Images/Projectiles/projectile_frostbreath.dds", aPosition);
	myGlowAnimation.Init("Assets/Images/GUI/powerShine.dds", aPosition);
	//myAnimation.Setup(6, 4, 24);
	aProjectileType;
	myPosition = aPosition;
	mySpeed = aSpeed;
	myIsDead = false;
	myScale = 0.5f;
	myGlowAnimation.SetScale({ myScale, myScale });
	myStartPos = { aPosition.x, aPosition.y, aPosition.z };
	myIncreaseTint = true;

	if (CU::Random() < 0.5f)
	{
		mySmokeDirection = { 8.0f, 1.0f, 0.0f };
	}
	else
	{
		mySmokeDirection = { -8.0f, 1.0f, 0.0f };
	}
}

void ProjectileFrostBreath::Update(float aDeltaTime)
{
	myScale += aDeltaTime * 1.5f;
	myPosition += myDirection * mySpeed * aDeltaTime;
	myAnimation.SetScale(CU::Vector2f(myScale, myScale));
	myAnimation.SetPosition(myPosition);

	if (myGlow)
	{
		if (myIncreaseTint)
		{
			if (myGlowTint.w < 0.3f)
			{
				myGlowTint.w += aDeltaTime * 2.0f;
			}
			else
			{
				myIncreaseTint = false;
			}
		}
		else if (myGlowTint.w > 0.0f)
		{
			myGlowTint.w -= aDeltaTime * 3.0f;
		}

		myGlowAnimation.SetTint({ myGlowTint.x, myGlowTint.y, myGlowTint.z, myGlowTint.w });
		myStartPos.y += aDeltaTime;
		myStartPos.x += aDeltaTime * (CU::Random() - 0.5f) * 2.0f;
		myStartPos += mySmokeDirection * aDeltaTime / 3.0f;
		myGlowAnimation.SetPosition({ myStartPos.x, myStartPos.y, myStartPos.z - 0.0001f });
		mySmokeDirection.x *= 0.85f;
		myGlowAnimation.SetScale({ myScale, myScale });
	}
}

void ProjectileFrostBreath::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);

	if (myGlow)
	{
		aRenderBuffer.Add(&myGlowAnimation);
	}
}

void ProjectileFrostBreath::SetDirection(const CU::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileFrostBreath::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileFrostBreath::Hit(Player & aPlayer, const float aDeltaTime)
{

	aPlayer.TakeDamage(myDamage, aDeltaTime);

}

const CommonUtilities::Vector3f & ProjectileFrostBreath::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileFrostBreath::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

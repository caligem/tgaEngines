#include "stdafx.h"
#include "ProjectileFrozenOrb.h"

#include "ProjectileFactory.h"
#include "ProjectileManager.h"

#include "Player.h"

#include <tga2d/math/common_math.h>

namespace CU = CommonUtilities;

void ProjectileFrozenOrb::Init(const char* aProjectileType, const CommonUtilities::Vector3f& aPosition, float aSpeed)
{
	myAnimation.Init("Assets/Images/Projectiles/projectile_iceOrb.dds", aPosition);
	//myAnimation.Setup(6, 4, 24);
	myIcicleFireRate = 0.5f;
	myIcicleCooldown = myIcicleFireRate;
	aProjectileType;
	myPosition = aPosition;
	mySpeed = aSpeed;
	myRotation = 0;
	myLifetime = 5.5f;
}

void ProjectileFrozenOrb::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;
	myAnimation.SetPosition(myPosition);

	myRotation += aDeltaTime;
	myAnimation.SetRotation(myRotation * 3);

	myIcicleDirection = { std::cos(myRotation * 10) ,  std::sin(myRotation * 10) , 0};

	myIcicleCooldown -= aDeltaTime;
	if (myIcicleCooldown <= 0)
	{
		SpawnIcicle(myIcicleDirection);
		myIcicleCooldown = myIcicleFireRate;
	}

	myLifetime -= aDeltaTime;

	if (myLifetime < 0)
	{
		myIsDead = true;
	}
}

void ProjectileFrozenOrb::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileFrozenOrb::SetDirection(const CommonUtilities::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myIcicleDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileFrozenOrb::Hit(Player& aPlayer, const float aDeltaTime)
{
	aPlayer.TakeDamage(myDamage, aDeltaTime);
	//myIsDead = true;
}

void ProjectileFrozenOrb::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

const CommonUtilities::Vector3f & ProjectileFrozenOrb::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileFrozenOrb::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

void ProjectileFrozenOrb::SpawnIcicle(CommonUtilities::Vector3f aDirection)
{
	float index = 0.0f;
	for (float i = 0; i < 4; ++i)
	{
		ProjectileIcicle* projectileOne = ProjectileFactory::CreateIcicles(myPosition,
			1.0f,
			CU::Vector3f(aDirection.x + std::cos(index), aDirection.y + std::sin(index), 0.0f));
		projectileOne->SetDamage(myDamage);
		ProjectileManager::GetInstance()->EnemyShoot(projectileOne);

		ProjectileIcicle* projectileTwo = ProjectileFactory::CreateIcicles(myPosition,
			1.0f,
			CU::Vector3f(-aDirection.x + std::cos(index), -aDirection.y + std::sin(index), 0.0f));
		projectileTwo->SetDamage(myDamage);
		ProjectileManager::GetInstance()->EnemyShoot(projectileTwo);

		index += (3.14f / 2.0f);
	}
}

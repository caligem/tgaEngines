#include "stdafx.h"
#include "ProjectileGranade.h"

#include "ProjectileFactory.h"
#include "ProjectileManager.h"

#include "Player.h"

#include <tga2d/math/common_math.h>

namespace CU = CommonUtilities;

void ProjectileGranade::Init(const char* aProjectileType, const CommonUtilities::Vector3f& aPosition, float aSpeed)
{
	myAnimation.Init("Assets/Images/Projectiles/projectile_iceOrb.dds", aPosition);
	//myAnimation.Setup(6, 4, 24);
	aProjectileType;
	myPosition = aPosition;
	mySpeed = aSpeed;
	myRotation = 0;
	myLifetime = 1.5f;
	myDropDir = 0.0f;
}

void ProjectileGranade::Update(float aDeltaTime)
{
	myDirection.x += myDropDir * aDeltaTime;
	myPosition.x += myDirection.x * mySpeed * 1.5f * aDeltaTime;
	myPosition.y += myDirection.y * mySpeed * aDeltaTime;
	myAnimation.SetPosition(myPosition);

	myRotation += aDeltaTime;
	myAnimation.SetRotation(myRotation * 3);
	
	
	myLifetime -= aDeltaTime;

	if (myLifetime < 0)
	{
		SpawnShards();
		myIsDead = true;
	}
}

void ProjectileGranade::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileGranade::SetDirection(const CommonUtilities::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));

	if (aDirection.x > 0)
	{
		myDropDir = -0.7f;
	}
	else
	{
		myDropDir = 0.7f;
	}

}

void ProjectileGranade::Hit(Player& aPlayer, const float aDeltaTime)
{
	aPlayer.TakeDamage(myDamage, aDeltaTime);
	//myIsDead = true;
}

void ProjectileGranade::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

const CommonUtilities::Vector3f & ProjectileGranade::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileGranade::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

void ProjectileGranade::SpawnShards()
{
	for (float a = 0.f; a < 2.f * Tga2D::Pif; a += Tga2D::Pif / 10)
	{
		ProjectileIcicle* projectile = ProjectileFactory::CreateIcicles(myPosition,
			1.0f,
			CU::Vector3f(std::cos(a), std::sin(a), 0.f));
		projectile->SetDamage(myDamage);
		ProjectileManager::GetInstance()->EnemyShoot(projectile);
	}

	/*float low = -1.0f;
	float high = 1.0f;
	for (float i = 0; i < 20; ++i)
	{
		float randXDir = low + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high - low)));
		float randYDir = low + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (high - low)));

		CU::Vector3f dir = { randXDir, randYDir, 0 };

		ProjectileIcicle* projectile = ProjectileFactory::CreateIcicles(myPosition,
			2.0f,
			dir);
		projectile->SetDamage(myDamage);
		ProjectileManager::GetInstance()->EnemyShoot(projectile);

	}*/
}

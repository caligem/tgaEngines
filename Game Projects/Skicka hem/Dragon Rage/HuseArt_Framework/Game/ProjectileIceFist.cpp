#include "stdafx.h"
#include "ProjectileIceFist.h"

#include "Player.h"

namespace CU = CommonUtilities;


void ProjectileIceFist::Init(const char * aProjectileType, const CU::Vector3f & aPosition, float aSpeed)
{
	aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_iceFist.dds", aPosition);
	myPosition = aPosition;
	mySpeed = aSpeed;
	myAnimation.SetScale({ 1.0f, 1.0f });
}

void ProjectileIceFist::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;

	myAnimation.SetPosition(myPosition);
}

void ProjectileIceFist::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileIceFist::SetDirection(const CU::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileIceFist::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileIceFist::Hit(Player & aPlayer, const float aDeltaTime)
{
	aDeltaTime;
	aPlayer.TakeDamage(myDamage);
	myIsDead = true;
}

const CU::Vector3f & ProjectileIceFist::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileIceFist::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

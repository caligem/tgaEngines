#include "stdafx.h"
#include "ProjectileIcicle.h"

#include "Player.h"


namespace CU = CommonUtilities;

void ProjectileIcicle::Init(const char* aProjectileType, const CU::Vector3f& aPosition, float aSpeed)
{
	aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_icicle.dds", aPosition);
	mySpeed = aSpeed;
	myPosition = aPosition;
}

void ProjectileIcicle::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;
	myAnimation.SetPosition(myPosition);
}

void ProjectileIcicle::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileIcicle::SetDirection(const CU::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileIcicle::Hit(Player& aPlayer, const float aDeltaTime)
{
	aDeltaTime;
	aPlayer.TakeDamage(myDamage);
	myIsDead = true;
}

void ProjectileIcicle::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

const CU::Vector3f & ProjectileIcicle::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileIcicle::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

#include "stdafx.h"
#include "ProjectileLightningNova.h"

#include "Player.h"

#include "Mathf.h"

namespace CU = CommonUtilities;

void ProjectileLightningNova::Init(const char* aProjectileType, const CU::Vector3f& aPosition, float aSpeed)
{
	aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_lighningNova.dds", aPosition);
	myGlowAnimation.Init("Assets/Images/GUI/powerShine.dds", aPosition);
	mySpeed = aSpeed;
	myPosition = aPosition;
	myStartPos = { aPosition.x, aPosition.y + 0.08f, aPosition.z };
	myIncreaseTint = true;
	myDamageDealFactor = 0.1f;
}

void ProjectileLightningNova::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;
	myAnimation.SetPosition(myPosition);

	if (myGlow)
	{
		if (myIncreaseTint)
		{
			if (myGlowTint.w < 0.6f)
			{
				myGlowTint.w += aDeltaTime * 5.0f;
			}
			else
			{
				myIncreaseTint = false;
			}
		}
		else if (myGlowTint.w > 0.0f)
		{
			myGlowTint.w -= aDeltaTime * 5.0f;
		}

		myGlowAnimation.SetPosition(myStartPos);
		myGlowAnimation.SetTint({ myGlowTint.x, myGlowTint.y, myGlowTint.z, myGlowTint.w });
	}

	myDamageDealFactor = CommonUtilities::Lerp(myDamageDealFactor, 1.f, aDeltaTime*25.f);
}

void ProjectileLightningNova::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);

	if (myGlow)
	{
		aRenderBuffer.Add(&myGlowAnimation);
	}
}

void ProjectileLightningNova::SetDirection(const CU::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileLightningNova::Hit(Player& aPlayer, const float aDeltaTime)
{
	aDeltaTime;
	aPlayer.TakeDamage(myDamage);
	myIsDead = true;
}

void ProjectileLightningNova::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

const CU::Vector3f & ProjectileLightningNova::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileLightningNova::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

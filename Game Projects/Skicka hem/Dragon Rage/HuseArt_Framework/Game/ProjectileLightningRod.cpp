#include "stdafx.h"
#include "ProjectileLightningRod.h"

#include "ProjectileFactory.h"
#include "ProjectileManager.h"

#include "Player.h"

#include <tga2d/math/common_math.h>

namespace CU = CommonUtilities;

void ProjectileLightningRod::Init(const char* aProjectileType, const CommonUtilities::Vector3f& aPosition, float aSpeed)
{
	myAnimation.Init("Assets/Images/Projectiles/blueCrystal.dds", aPosition);
	myAnimation.SetScale({ 1.0f, 1.0f});
	aProjectileType;
	myPosition = aPosition;
	mySpeed = aSpeed;
	myRotation = 0;

	myBeam.Init(eBeamType::Lightning);
	myBeam.Activate();
}

void ProjectileLightningRod::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;
	myAnimation.SetPosition(myPosition);

	myRotation += (aDeltaTime * 2.0f);
	myAnimation.SetRotation(myRotation);

	myBeam.SetStartPosition(myPosition);
	myBeam.SetEndPosition({ myPosition.x + (std::cos(-myRotation) / 2.0f),
							myPosition.y + (std::sin(-myRotation) / 2.0f),
							myPosition.z });

	myBeam.Update(aDeltaTime, false);
}

void ProjectileLightningRod::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
	myBeam.FillRenderBuffer(aRenderBuffer);
}

void ProjectileLightningRod::SetDirection(const CommonUtilities::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileLightningRod::Hit(Player& aPlayer, const float aDeltaTime)
{
	aPlayer.TakeDamage(myDamage, aDeltaTime);
}

void ProjectileLightningRod::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

const CommonUtilities::Vector3f & ProjectileLightningRod::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileLightningRod::IsDead()
{
	return myAnimation.IsOutsideScreen(0.25f);
}

bool ProjectileLightningRod::IsTouching(const CircleCollider * aCollider)
{
	if (myBeam.IsTouching(aCollider))
	{
		return true;
	}

	return myAnimation.IsTouching(aCollider);
}

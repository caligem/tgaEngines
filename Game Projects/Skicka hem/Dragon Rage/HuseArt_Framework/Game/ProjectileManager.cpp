#include "stdafx.h"

#include "ExplosionManager.h"
#include "ProjectileManager.h"
#include "Projectile.h"
#include "Player.h"
#include "EnemyManager.h"

#include <Animation.h>
#include <iostream>
#include <algorithm>

ProjectileManager* ProjectileManager::ourInstance = nullptr;

namespace CU = CommonUtilities;

void ProjectileManager::Create()
{
	if (ourInstance == nullptr)
	{
		ourInstance = new ProjectileManager();
		ourInstance->myEnemyProjectiles.Init(64);
		ourInstance->myPlayerProjectiles.Init(64);
	}
	else
	{
		ourInstance->myEnemyProjectiles.DeleteAll();
		ourInstance->myPlayerProjectiles.DeleteAll();
	}
}

void ProjectileManager::Destroy()
{
	if (ourInstance == nullptr)
	{
		return;
	}
	ourInstance->myPlayerProjectiles.DeleteAll();
	ourInstance->myEnemyProjectiles.DeleteAll();
	SAFE_DELETE(ourInstance);
}

void ProjectileManager::UpdateEnemieProjectiles(float aDeltaTime, Player & aPlayer)
{
	if (myEnemyProjectiles.Empty())
	{
		return;
	}

	for (unsigned short i = 0; i < myEnemyProjectiles.Size(); ++i)
	{
		myEnemyProjectiles[i]->Update(aDeltaTime);
	}
	aPlayer;
}

void ProjectileManager::UpdatePlayerProjectiles(float aDeltaTime, Player & aPlayer)
{
	if (myPlayerProjectiles.Empty())
	{
		return;
	}

	for (unsigned short i = 0; i < myPlayerProjectiles.Size(); ++i)
	{
		myPlayerProjectiles[i]->Update(aDeltaTime);
	}
	aPlayer;
}

void ProjectileManager::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	for (Projectile* projectile : myEnemyProjectiles)
	{
		projectile->FillRenderBuffer(aRenderBuffer);
	}

	for (Projectile* projectile : myPlayerProjectiles)
	{
		projectile->FillRenderBuffer(aRenderBuffer);
	}
}

void ProjectileManager::CollideWithPlayerAndEnemies(Player& aPlayer, const float aDeltaTime)
{
	for(Projectile* projectile : myEnemyProjectiles)
	{
		if (projectile->IsTouching(aPlayer.GetCollider()))
		{
			projectile->Hit(aPlayer, aDeltaTime);
		}
	}

	for (unsigned short i = myPlayerProjectiles.Size(); i > 0; --i)
	{
		Projectile* projectile = myPlayerProjectiles[i - 1];
		if (EnemyManager::GetInstance()->IsEnemyHit(projectile, aDeltaTime))
		{
			projectile->Collided();
		}
	}
}

void ProjectileManager::PlayerShoot(Projectile* aProjectile)
{
	myPlayerProjectiles.Add(aProjectile);
}

void ProjectileManager::EnemyShoot(Projectile* aProjectile)
{
	myEnemyProjectiles.Add(aProjectile);
}

void ProjectileManager::RenderDebug(const CommonUtilities::Camera & aCamera)
{
	for (Projectile* projectile : myEnemyProjectiles)
	{
		projectile->RenderDebug(aCamera);
	}
	for (Projectile* projectile : myPlayerProjectiles)
	{
		projectile->RenderDebug(aCamera);
	}
}

void ProjectileManager::CleanUp()
{
	for (unsigned short i = myPlayerProjectiles.Size(); i > 0; --i)
	{
		Projectile* projectile = myPlayerProjectiles[i - 1];
		if (projectile->IsDead())
		{
			myPlayerProjectiles.DeleteCyclicAtIndex(i - 1);
		}
	}
	for (unsigned short i = myEnemyProjectiles.Size(); i > 0; --i)
	{
		Projectile* projectile = myEnemyProjectiles[i - 1];
		if (projectile->IsDead())
		{
			myEnemyProjectiles.DeleteCyclicAtIndex(i - 1);
		}
	}
}

void ProjectileManager::ClearProjectiles()
{
	for (Projectile* projectile : myEnemyProjectiles)
	{
		Animation* newExplosion = new Animation("Assets/Images/Projectiles/explosion.dds");
		newExplosion->Setup(2, 3, 6);
		newExplosion->SetDuration(0.75f);
		newExplosion->SetLoop(true);
		newExplosion->SetPosition(projectile->GetPosition() - CommonUtilities::Vector3f(0.f, 0.f, 0.025f) );
		newExplosion->SetScale({ 1.f, 1.f });
		ExplosionManager::GetInstance()->AddExplosion(newExplosion);
		projectile->Kill();
	}
	for (Projectile* projectile : myPlayerProjectiles)
	{
		Animation* newExplosion = new Animation("Assets/Images/Projectiles/explosion.dds");
		newExplosion->Setup(2, 3, 6);
		newExplosion->SetDuration(0.75f);
		newExplosion->SetLoop(true);
		newExplosion->SetPosition(projectile->GetPosition() - CommonUtilities::Vector3f(0.f, 0.f, 0.025f));
		newExplosion->SetScale({ 1.f, 1.f });
		ExplosionManager::GetInstance()->AddExplosion(newExplosion);
		projectile->Kill();
	}
}

#pragma once
#include <GrowingArray.h>
#include <vector.h>
  
class Player;
class Projectile;
class Drawable;

#include <Camera.h>

class ProjectileManager
{
public:
	static ProjectileManager* GetInstance() { return ourInstance; }

	static void Create();
	static void Destroy();

	void UpdateEnemieProjectiles(float aDeltaTime, Player & aPlayer);
	void UpdatePlayerProjectiles(float aDeltaTime, Player & aPlayer);
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer);
	void CollideWithPlayerAndEnemies(Player& aPlayer, const float aDeltaTime);
	void CleanUp();
	void ClearProjectiles();

	void PlayerShoot(Projectile* aProjectile);
	void EnemyShoot(Projectile* aProjectile);

	void RenderDebug(const CommonUtilities::Camera& aCamera);

private:
	ProjectileManager() = default;
	~ProjectileManager() = default;

	static ProjectileManager* ourInstance;
	CommonUtilities::GrowingArray<Projectile*> myPlayerProjectiles;
	CommonUtilities::GrowingArray<Projectile*> myEnemyProjectiles;

};

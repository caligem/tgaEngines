#include "stdafx.h"
#include "ProjectileMelee.h"

#include "Player.h"
#include "Enemy.h"

namespace CU = CommonUtilities;


ProjectileMelee::ProjectileMelee(Enemy& aEnemy) : myEnemy(aEnemy)
{
}

void ProjectileMelee::Init(const char * aProjectileType, const CU::Vector3f& aPosition, float aSpeed)
{
	aProjectileType;
	aSpeed;
	myAnimation.Init("Assets/Images/Projectiles/projectile_natureMelee.dds", aPosition);
	//myAnimation.Setup(6, 4, 24);
	myAnimation.SetScale({ 1.0f, 1.0f });
	myPosition = aPosition;
	myPosDifference = myPosition - myEnemy.GetPosition();
	myLifeSpann = false;
	myLifeSpann2 = 0.2f;
}

void ProjectileMelee::Update(float aDeltaTime)
{
	myLifeSpann2 -= aDeltaTime;
	myPosition = myEnemy.GetPosition() + myPosDifference;
	myAnimation.SetPosition(myPosition);

	if (myLifeSpann2 <= 0)
	{
		myIsDead = true;
	}

	if (myLifeSpann)
	{
		//myIsDead = true;
	}
	myLifeSpann = true;
}

void ProjectileMelee::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileMelee::SetDirection(const CommonUtilities::Vector3f & aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileMelee::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileMelee::Hit(Player & aPlayer, const float aDeltaTime)
{
	aPlayer.TakeDamage(myDamage, aDeltaTime);
	//myIsDead = true;
}

const CU::Vector3f & ProjectileMelee::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileMelee::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}



#include "stdafx.h"
#include "ProjectileNatureMelee.h"

#include "Player.h"

ProjectileNatureMelee::ProjectileNatureMelee(Player& aPlayer)
	: myPlayer(aPlayer)
	, myDamageTimer(0.f)
	, myLifeTime(0.2f)
{
}


ProjectileNatureMelee::~ProjectileNatureMelee()
{
}

void ProjectileNatureMelee::Init(const char * aProjectileType, const CommonUtilities::Vector3f & aPosition, float aSpeed)
{
	myProjectileType = aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_natureMelee.dds", aPosition);
	myPosition = aPosition + myOffset;
	mySpeed = aSpeed;
	
}

void ProjectileNatureMelee::SetAttackType(eAttackType aAttackType)
{
	if (aAttackType == eAttackType::Front)
	{
		myOffset = { 0.f, 0.25f, 0.f };
		myRotation = myAnimation.GetRotation() - Tga2D::Pif / 2;
	}
	else if (aAttackType == eAttackType::Behind)
	{
		myOffset = { 0.f, -0.25f, 0.f };
		myRotation = myAnimation.GetRotation() + Tga2D::Pif / 2;
	}
}

void ProjectileNatureMelee::Update(float aDeltaTime)
{
	myPosition = myPlayer.GetPosition() + myOffset;
	myAnimation.SetPosition(myPosition);
	myAnimation.SetRotation(myRotation);

	myLifeTime -= aDeltaTime;

	if (myLifeTime <= 0.f)
	{
		Kill();
	}
}

void ProjectileNatureMelee::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

inline const float ProjectileNatureMelee::GetDamage()
{
	return myDamage;
}

void ProjectileNatureMelee::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileNatureMelee::Hit(Player & aPlayer, const float aDeltaTime)
{
	aPlayer.TakeDamage(myDamage, aDeltaTime);
}

const CommonUtilities::Vector3f & ProjectileNatureMelee::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileNatureMelee::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

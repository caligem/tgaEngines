#pragma once

#include "Projectile.h"

enum class eAttackType
{
	Front,
	Behind
};

class ProjectileNatureMelee : public Projectile
{
public:
	ProjectileNatureMelee(Player& aPlayer);
	~ProjectileNatureMelee();
	void Init(const char* aProjectileType, const CommonUtilities::Vector3f& aPosition, float aSpeed) override;
	void SetAttackType(eAttackType aAttackType);
	void Update(float aDeltaTime) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	inline const float GetDamage() override;
	void SetDamage(const float aDamage) override;
	void Hit(Player& aPlayer, const float aDeltaTime = 1) override;
	const CommonUtilities::Vector3f& GetPoint() const override;
	inline void Collided() override {}
	bool IsDead() override;

private:
	CommonUtilities::Vector3f myDirection;
	float myDamageTimer;
	float myLifeTime;
	Player& myPlayer;
	CommonUtilities::Vector3f myOffset;
};


#include "stdafx.h"
#include "ProjectileNatureShot.h"

#include "Player.h"

namespace CU = CommonUtilities;

void ProjectileNatureShot::Init(const char* aProjectileType, const CU::Vector3f& aPosition, float aSpeed)
{
	aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_wispNatureShot.dds", aPosition);
	mySpeed = aSpeed;
	myPosition = aPosition;
}

void ProjectileNatureShot::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;
	myAnimation.SetPosition(myPosition);
}

void ProjectileNatureShot::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileNatureShot::SetDirection(const CU::Vector3f& aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileNatureShot::Hit(Player & aPlayer, const float aDeltaTime)
{
	aDeltaTime;
	aPlayer.TakeDamage(myDamage);
	myIsDead = true;
}

void ProjectileNatureShot::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

const CU::Vector3f & ProjectileNatureShot::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileNatureShot::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

#include "stdafx.h"
#include "ProjectileWispFireShot.h"

#include "Player.h"

namespace CU = CommonUtilities;

void ProjectileWispFireShot::Init(const char * aProjectileType, const CU::Vector3f & aPosition, float aSpeed)
{
	aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_wispFireShot.dds", aPosition);
	mySpeed = aSpeed;
	myPosition = aPosition;
}

void ProjectileWispFireShot::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;
	myAnimation.SetPosition(myPosition);
}

void ProjectileWispFireShot::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileWispFireShot::SetDirection(const CU::Vector3f & aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileWispFireShot::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileWispFireShot::Hit(Player & aPlayer, const float aDeltaTime)
{
	aDeltaTime;
	aPlayer.TakeDamage(myDamage);
	myIsDead = true;
}

const CU::Vector3f & ProjectileWispFireShot::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileWispFireShot::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}

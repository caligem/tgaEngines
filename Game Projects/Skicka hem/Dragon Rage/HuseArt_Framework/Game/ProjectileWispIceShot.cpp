#include "stdafx.h"
#include "ProjectileWispIceShot.h"

#include "Player.h"


namespace CU = CommonUtilities;

void ProjectileWispIceShot::Init(const char * aProjectileType, const CU::Vector3f & aPosition, float aSpeed)
{
	aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_wispIceShot.dds", aPosition);
	mySpeed = aSpeed;
	myPosition = aPosition;
}

void ProjectileWispIceShot::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;
	myAnimation.SetPosition(myPosition);
}

void ProjectileWispIceShot::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileWispIceShot::SetDirection(const CU::Vector3f & aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileWispIceShot::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileWispIceShot::Hit(Player & aPlayer, const float aDeltaTime)
{
	aDeltaTime;
	aPlayer.TakeDamage(myDamage);
	myIsDead = true;
}

const CU::Vector3f & ProjectileWispIceShot::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileWispIceShot::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}
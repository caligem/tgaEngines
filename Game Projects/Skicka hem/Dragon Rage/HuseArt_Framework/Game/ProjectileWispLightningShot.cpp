#include "stdafx.h"
#include "ProjectileWispLightningShot.h"

#include "Player.h"

namespace CU = CommonUtilities;

void ProjectileWispLightningShot::Init(const char * aProjectileType, const CU::Vector3f & aPosition, float aSpeed)
{
	aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_wispLightningShot.dds", aPosition);
	mySpeed = aSpeed;
	myPosition = aPosition;
}

void ProjectileWispLightningShot::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;
	myAnimation.SetPosition(myPosition);
}

void ProjectileWispLightningShot::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileWispLightningShot::SetDirection(const CU::Vector3f & aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileWispLightningShot::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileWispLightningShot::Hit(Player & aPlayer, const float aDeltaTime)
{
	aDeltaTime;
	aPlayer.TakeDamage(myDamage);
	myIsDead = true;
}

const CU::Vector3f & ProjectileWispLightningShot::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileWispLightningShot::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}
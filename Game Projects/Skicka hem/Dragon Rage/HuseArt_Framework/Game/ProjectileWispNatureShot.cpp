#include "stdafx.h"
#include "ProjectileWispNatureShot.h"

#include "Player.h"

namespace CU = CommonUtilities;

void ProjectileWispNatureShot::Init(const char * aProjectileType, const CU::Vector3f & aPosition, float aSpeed)
{
	aProjectileType;
	myAnimation.Init("Assets/Images/Projectiles/projectile_wispNatureShot.dds", aPosition);
	mySpeed = aSpeed;
	myPosition = aPosition;
}

void ProjectileWispNatureShot::Update(float aDeltaTime)
{
	myPosition += myDirection * mySpeed * aDeltaTime;
	myAnimation.SetPosition(myPosition);
}

void ProjectileWispNatureShot::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myAnimation);
}

void ProjectileWispNatureShot::SetDirection(const CU::Vector3f & aDirection)
{
	myDirection = aDirection.GetNormalized();
	myAnimation.SetRotation(-std::atan2f(myDirection.y, myDirection.x));
}

void ProjectileWispNatureShot::SetDamage(const float aDamage)
{
	myDamage = aDamage;
}

void ProjectileWispNatureShot::Hit(Player & aPlayer, const float aDeltaTime)
{
	aDeltaTime;
	aPlayer.TakeDamage(myDamage);
	myIsDead = true;
}

const CU::Vector3f & ProjectileWispNatureShot::GetPoint() const
{
	return myAnimation.GetPoint();
}

bool ProjectileWispNatureShot::IsDead()
{
	return myIsDead || myAnimation.IsOutsideScreen();
}
#include "stdafx.h"
#include "SFXManager.h"
#include "SoundFactory.h"
#include <Macros.h>

SFXManager* SFXManager::ourInstance = nullptr;

void SFXManager::Create(SoundFactory* aSoundFactory)
{
	if (ourInstance == nullptr)
	{
		ourInstance = new SFXManager();
		ourInstance->mySoundFactory = aSoundFactory;
		ourInstance->mySFXs.Init(64);
	}
	else
	{
		for (AudioSource& sfx : ourInstance->mySFXs)
		{
			sfx.Release();
		}
		ourInstance->mySFXs.RemoveAll();
	}
}

void SFXManager::Destroy()
{
	if (ourInstance == nullptr)
	{
		return;
	}

	for (AudioSource& sfx : ourInstance->mySFXs)
	{
		sfx.Release();
	}

	SAFE_DELETE(ourInstance);
}

AudioSource SFXManager::PlaySound(const char * aFilePath, float aVolume)
{
	AudioSource newSFX = mySoundFactory->CreateSound(aFilePath);
	newSFX.SetVolume(aVolume);
	newSFX.Play();
	mySFXs.Add(newSFX);
	return newSFX;
}

void SFXManager::CleanUp()
{
	if (mySFXs.Empty())
	{
		return;
	}

	for (unsigned short index = mySFXs.Size(); index > 0; index--)
	{
		if (!mySFXs[index - 1].IsPlaying())
		{
			mySFXs[index - 1].Release();
			mySFXs.RemoveAtIndex(index - 1);
		}
	}
}


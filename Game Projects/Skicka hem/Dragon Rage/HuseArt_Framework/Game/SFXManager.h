#pragma once

#include <GrowingArray.h>
#include <AudioSource.h>

class SoundFactory;

class SFXManager
{
public:
	static SFXManager* GetInstance() { return ourInstance; }

	static void Create(SoundFactory* aSoundFactory);
	static void Destroy();

	AudioSource PlaySound(const char* aFilePath, float aVolume = 1.0f);
	void CleanUp();

private:
	SFXManager() = default;
	~SFXManager() = default;

	static SFXManager* ourInstance;

	SoundFactory* mySoundFactory;
	CommonUtilities::GrowingArray<AudioSource> mySFXs;
};


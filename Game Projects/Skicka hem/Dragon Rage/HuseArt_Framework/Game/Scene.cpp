#include "stdafx.h"
#include "Scene.h"

#include <Sprite.h>

Scene::Scene()
{
	myRequestSceneLoad = false;
	myRequestQuitGame = false;
	myAnimTimer = 0.f;
}

Scene::~Scene()
{
	mySprites.DeleteAll();
	myBuffer.RemoveAll();
}

#pragma once

#include <string>

namespace CommonUtilities
{
	class Camera;
}
#include <GrowingArray.h>
#include <Vector.h>

class Drawable;

struct FloaterDrawable
{
	CommonUtilities::Vector3f myOriginalPosition;
	Drawable* myDrawable;
};

class Scene
{
public:
	Scene();
	virtual ~Scene();

	virtual void Update(float aDeltaTime) = 0;
	virtual void Render() = 0;
	virtual void Fade(float aAlpha) = 0;

	virtual void SortAndRenderBuffer() = 0;
	virtual void FillBufferAndCalculatePoints() = 0;
	virtual void FillGuiBufferAndCalculatePoints() = 0;

	inline void RequestScene(const std::string& aPath) { mySceneToLoad = aPath; myRequestSceneLoad = true; };
	inline bool RequestingSceneLoad() { return myRequestSceneLoad; }
	inline const std::string& GetSceneToLoad() { return mySceneToLoad; }

	inline bool RequestingQuitGame() { return myRequestQuitGame; }
	inline void RequestQuitGame() { myRequestQuitGame = true; }

	virtual bool IsMenu() = 0;
	virtual bool IsCutscene() = 0;

protected:
	CommonUtilities::GrowingArray<Drawable*> mySprites;
	CommonUtilities::GrowingArray<Drawable*> myBuffer;

	CommonUtilities::GrowingArray<FloaterDrawable> myFloaters;
	float myAnimTimer;

	bool myRequestSceneLoad;
	std::string mySceneToLoad;

	bool myRequestQuitGame;
};


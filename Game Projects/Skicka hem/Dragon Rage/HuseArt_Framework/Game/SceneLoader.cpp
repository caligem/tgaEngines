#include "stdafx.h"
#include "SceneLoader.h"

#include "LevelScene.h"
#include "MenuScene.h"

#include <FileDialog.h>
#include <fstream>
#include <iostream>
#include <string>

#include <DL_Debug.h>
#include <Parser\json.hpp>

namespace CU = CommonUtilities;

SceneLoader::SceneLoader()
{
}

SceneLoader::~SceneLoader()
{
}

Scene * SceneLoader::LoadScene(
	const std::string& aPath,
	const CommonUtilities::InputManager & aInputManager,
	CommonUtilities::Camera & aCamera,
	CommonUtilities::Camera & aGuiCamera,
	CommonUtilities::XBOXController & aXboxController,
	SoundSystemClass & aSoundSystem )
{
	std::ifstream i(aPath);
	if( i.bad() || i.fail() )
	{
		DL_DEBUG("FAILED TO LOAD LEVEL");
	}
	nlohmann::json j;
	i >> j;

	if (j.find("type") != j.end())
	{
		if (j["type"] == "level")
		{
			LevelScene* newScene = new LevelScene(aInputManager, aCamera, aGuiCamera, aXboxController, aSoundSystem);
			newScene->Init(j);
			return newScene;
		}
		else if (j["type"] == "menu")
		{
			MenuScene* newScene = new MenuScene(aInputManager, aCamera, aGuiCamera, aXboxController, aSoundSystem);
			newScene->Init(j);
			return newScene;
		}
	}

	return nullptr;
}

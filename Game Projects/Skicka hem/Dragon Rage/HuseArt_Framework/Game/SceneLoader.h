#pragma once

namespace CommonUtilities
{
	class Camera;
	class InputManager;
	class XBOXController;
}

class SoundSystemClass;

#include "Scene.h"

#include <string>

class SceneLoader
{
public:
	static Scene* LoadScene(
		const std::string& aPath,
		const CommonUtilities::InputManager& aInputManager,
		CommonUtilities::Camera& aCamera,
		CommonUtilities::Camera& aGuiCamera,
		CommonUtilities::XBOXController& aXboxController,
		SoundSystemClass& aSoundSystem
	);

protected:
	SceneLoader();
	~SceneLoader();

};


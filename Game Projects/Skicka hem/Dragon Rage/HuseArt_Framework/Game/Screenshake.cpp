#include "stdafx.h"
#include "Screenshake.h"
#include <Random.h>

#include <XBOXController.h>

namespace CU = CommonUtilities;

Screenshake::Screenshake(CommonUtilities::XBOXController & aXboxController)
	: myXboxController(aXboxController)
{
	myShakeOffset = { 0.0f, 0.0f, 0.0f };
	myCurrentOffset = { 0.0f, 0.0f, 0.0f };
	myDuration = 0.0f;
	myCurrentTime = 0.0f;
}

void Screenshake::Update(float aDeltaTime)
{
	myCurrentTime -= aDeltaTime;

	if (myCurrentTime <= 0.0f)
	{
		myCurrentTime = 0.0f;
		myDuration = 0.0f;
		myShakeOffset = { 0.0f, 0.0f, 0.0f, };
		myCurrentOffset = { 0.0f, 0.0f, 0.0f };
	}
	else
	{	
		float randX = -myShakeOffset.x + CU::Random() * (myShakeOffset.x - (-myShakeOffset.x));
		float randY = -myShakeOffset.y + CU::Random() * (myShakeOffset.y - (-myShakeOffset.y));
		float randZ = -myShakeOffset.z + CU::Random() * (myShakeOffset.z - (-myShakeOffset.z));

		float alpha = myCurrentTime / myDuration;

		alpha = alpha * alpha * (3.0f - 2.0f * alpha);

		myCurrentOffset.x = randX * alpha;
		myCurrentOffset.y = randY * alpha;
		myCurrentOffset.z = randZ * alpha;
	}

	if (myXboxController.IsConnected())
	{
		myXboxController.Vibrate(GetShakeOffset().Length()*100.f/myDuration, GetShakeOffset().Length()*100.f/myDuration);
	}
}

void Screenshake::Shake(float aDuration, CU::Vector3f aShakeOffset)
{
	if (myShakeOffset.Length() > aShakeOffset.Length())
	{
		return;
	}
	myShakeOffset = aShakeOffset;
	myCurrentOffset = aShakeOffset;
	myDuration = aDuration;
	myCurrentTime = aDuration;
}

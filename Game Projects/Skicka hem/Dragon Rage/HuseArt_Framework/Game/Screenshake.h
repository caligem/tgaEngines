#pragma once
#include <Vector.h>

namespace CommonUtilities
{
	class XBOXController;
}

class Screenshake
{
public:
	Screenshake(CommonUtilities::XBOXController& aXboxController);
	~Screenshake() = default;

	void Update(float aDeltaTime);
	void Shake(float aDuration, CommonUtilities::Vector3f aShakeOffset);
	inline const CommonUtilities::Vector3f& GetShakeOffset() const { return myCurrentOffset; }

private:
	CommonUtilities::XBOXController& myXboxController;

	CommonUtilities::Vector3f myShakeOffset;
	CommonUtilities::Vector3f myCurrentOffset;
	float myDuration;
	float myCurrentTime;
};


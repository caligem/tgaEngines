#include "stdafx.h"
#include "SoundFactory.h"
#include "AudioSource.h"

#include <string>

SoundFactory::SoundFactory(SoundSystemClass& aSoundSystem)
	: mySoundSystem(aSoundSystem)
{
}

AudioSource SoundFactory::CreateSound(const char* aFilePath)
{

	AudioSource newAudio;

	newAudio.Init(aFilePath, &mySoundSystem);

	return newAudio;
}

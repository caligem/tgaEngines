#pragma once
#include <SoundSystemClass.h>

class AudioSource;

class SoundFactory
{
public:
	SoundFactory(SoundSystemClass& aSoundSystem);
	~SoundFactory() = default;

	AudioSource CreateSound(const char* aFilePath);

private:
	SoundSystemClass& mySoundSystem;
};
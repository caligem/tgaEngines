#include "stdafx.h"
#include "Stance.h"

float Stance::mySecondaryShootCooldown = 10.f;

Stance::Stance(Screenshake & aScreenshake)
	: myScreenshake(aScreenshake)
{
	mySwitch = true;
}

Stance::~Stance()
{
}

void Stance::Update(float aDeltaTime)
{
	myCurrentAnimation->SetPosition(myPosition);
	myCurrentAnimation->Update(aDeltaTime);
	myFlyingAnimation.SetPosition(myPosition);
	myGlideAnimation.SetPosition(myPosition);
	myShootingAnimation.SetPosition(myPosition);

	myPrimaryShootCooldown += aDeltaTime;

	if (mySwitch)
	{
		mySwitch = false;

		if (myCurrentAnimation == &myGlideAnimation)
		{
			myCurrentAnimation = &myFlyingAnimation;
			myCountDown.Set([=] { mySwitch = true; }, 2.5f);
		}
		else if (myCurrentAnimation == &myFlyingAnimation)
		{
			myCurrentAnimation = &myGlideAnimation;
			myCountDown.Set([=] { mySwitch = true; }, 1.5f);
		}		
	}

	myCountDown.Update(aDeltaTime);
}

void Stance::SetState(eState aState)
{
	myState = aState;

	if (myState == eState::Flying)
	{
		/*if (mySwitch)
		{
			mySwitch = false;

			if (myCurrentAnimation == &myGlideAnimation)
			{
				myCurrentAnimation = &myFlyingAnimation;
			}
			else if (myCurrentAnimation == &myFlyingAnimation)
			{
				myCurrentAnimation = &myGlideAnimation;
			}

			myCountDown.Set([=] { mySwitch = true; }, 2.0f);
		}*/

		myFlyingAnimation.SetPosition(myPosition);
	}
	else if (myState == eState::Shooting)
	{
		//TODO: Implement ShootingStance to all stances.
		//myShootingAnimation.SetPosition(myPosition);
		//myCurrentAnimation = &myShootingAnimation;
	}
}

void Stance::IncreaseSecondaryPower(float aAmount)
{
	mySecondaryShootCooldown += aAmount;
	if (mySecondaryShootCooldown > 10.f)
	{
		mySecondaryShootCooldown = 10.f;
	}
}

void Stance::UpdateSecondaryCooldown(float aDeltaTime)
{
	Stance::mySecondaryShootCooldown += aDeltaTime;
}

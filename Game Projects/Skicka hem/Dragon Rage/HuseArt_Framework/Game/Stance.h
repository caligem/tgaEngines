#pragma once
#include <Animation.h>

class Player;

#include "Element.h"
#include "CountDown.h"

#include "Screenshake.h"

struct Stats
{
	float primaryDamage;
	float primaryRateOfFire;
	float secondaryDamage;
	float secondaryRateOfFire;
};

enum class eShootType
{
	Primary,
	Secondary
};

enum class eState
{
	Flying,
	Shooting
};

enum class eStance
{
	Fire,
	Ice,
	Lightning
};


class Stance
{
public:
	Stance(Screenshake& aScreenshake);
	~Stance();

	virtual void Init() = 0;
	virtual void Update(float aDeltaTime);

	virtual void Shoot(eShootType aShootType) = 0;

	void SetState(eState aState);
	void SetPosition(CommonUtilities::Vector3f& aPosition) { myPosition = aPosition; }
	inline void SetStats(const Stats& aStats) { myStats = aStats; }
	
	virtual inline void IncreaseSecondaryPower(float aAmount);

	inline eElement const GetElement() const { return myElement; }

	const float GetPrimaryShootCooldown() const { return myPrimaryShootCooldown; }
	static const float GetSecondaryShootCooldown() { return Stance::mySecondaryShootCooldown; }
	static void UpdateSecondaryCooldown(float aDeltaTime);


	Animation* GetCurrentAnimation() { return myCurrentAnimation; }
	virtual eStance GetStance() = 0;

protected:

	Stats myStats;
	eState myState;
	
	eElement myElement;

	Animation* myCurrentAnimation;
	Animation myFlyingAnimation;
	Animation myGlideAnimation;
	Animation myShootingAnimation;

	CountDown myCountDown;
	bool mySwitch;

	float myPrimaryShootCooldown = 0.15f;
	static float mySecondaryShootCooldown;

	float myProjectileSpeed = 2.f;

	CommonUtilities::Vector3f myPosition;

	Screenshake& myScreenshake;
};


#pragma once

#include "Sprite.h"
#include <GrowingArray.h>
#include <Vector.h>

class Widget
{
public:
	Widget();
	~Widget();

	virtual void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) = 0;
	virtual void Update(float) {};

	virtual void OnMousePressed(const CommonUtilities::Vector2f&) {};
	virtual void OnMouseDown(const CommonUtilities::Vector2f&) {};
	virtual void OnMouseReleased(const CommonUtilities::Vector2f&) {};
	virtual void OnMouseMoved(const CommonUtilities::Vector2f&) {};
};


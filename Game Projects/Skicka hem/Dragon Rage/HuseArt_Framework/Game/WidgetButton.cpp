#include "stdafx.h"
#include "WidgetButton.h"

#include "SpriteData.h"

#include <Mathf.h>

WidgetButton::WidgetButton()
{
	myScaleFactor = 1.f;
	myIsHovered = false;
}

WidgetButton::WidgetButton(SpriteData aSpriteData)
{
	nlohmann::json::object_t& data = aSpriteData.data;
	mySprite.Init(
		data["texture"].get<std::string>().c_str(),
		{
			data["position"][0].get<float>(),
			data["position"][1].get<float>(),
			data["position"][2].get<float>()
		},
		{
			data["scale"][0].get<float>(),
			data["scale"][1].get<float>()
		},
		data["rotation"].get<float>()
	);
	if (data.find("tint") != data.end())
	{
		mySprite.SetTint(
		{
			data["tint"][0].get<float>(),
			data["tint"][1].get<float>(),
			data["tint"][2].get<float>(),
			data["tint"][3].get<float>()
		}
		);
	}
	if (data.find("UVScale") != data.end())
	{
		mySprite.SetUVScale({
			data["UVScale"][0].get<float>(),
			data["UVScale"][1].get<float>()
		});
	}
	if (data.find("name") != data.end())
	{
		myEventType = data["name"].get<std::string>();
	}
	if (data.find("tag") != data.end())
	{
		myEventData = data["tag"].get<std::string>();
	}

	myOriginalScale = mySprite.GetScale();
	myScaleFactor = 1.f;
	myIsHovered = false;
}

WidgetButton::~WidgetButton()
{}

void WidgetButton::Update(float aDeltaTime)
{
	if (myIsHovered)
	{
		myScaleFactor = CommonUtilities::Lerp(myScaleFactor, 1.1f, aDeltaTime*25.f);
	}
	else
	{
		myScaleFactor = CommonUtilities::Lerp(myScaleFactor, 1.0f, aDeltaTime*25.f);
	}
	mySprite.SetScale(myOriginalScale * myScaleFactor);
}

void WidgetButton::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&mySprite);
}

void WidgetButton::OnMousePressed(const CommonUtilities::Vector2f& aMousePoint)
{
	if (mySprite.IsInside(aMousePoint) && myCallback != nullptr)
	{
		myCallback();
	}
}

void WidgetButton::OnMouseMoved(const CommonUtilities::Vector2f& aMousePoint)
{
	if (mySprite.IsInside(aMousePoint))
	{
		myIsHovered = true;
	}
	else
	{
		myIsHovered = false;
	}
}


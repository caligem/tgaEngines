#pragma once

#include "Widget.h"

#include <functional>
#include <string>

struct SpriteData;

class WidgetButton : public Widget
{
public:
	WidgetButton();
	WidgetButton(SpriteData aSpriteData);
	~WidgetButton();

	void Update(float aDeltaTime) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void OnMousePressed(const CommonUtilities::Vector2f& aMousePoint) override;
	void OnMouseMoved(const CommonUtilities::Vector2f& aMousePoint) override;

	void SetCallback(std::function<void()> aCallback) { myCallback = aCallback; }

	inline const std::string& GetEventType() { return myEventType; }
	inline const std::string& GetEventData() { return myEventData; }

private:
	Sprite mySprite;
	bool myIsHovered = false;
	float myScaleFactor;
	CommonUtilities::Vector2f myOriginalScale;

	std::string myEventType;
	std::string myEventData;

	std::function<void()> myCallback;
};


#include "stdafx.h"
#include "WidgetCheckbox.h"

#include "SpriteData.h"

#include <Mathf.h>

WidgetCheckbox::WidgetCheckbox()
{
	myCheck = false;
}

WidgetCheckbox::WidgetCheckbox(SpriteData aSpriteData)
{
	nlohmann::json::object_t& data = aSpriteData.data;
	myPosition = {
		data["position"][0].get<float>(),
		data["position"][1].get<float>(),
		data["position"][2].get<float>()
	};

	myCheckbox.Init(
		data["texture"].get<std::string>().c_str(),
		myPosition,
		{
			data["scale"][0].get<float>(),
			data["scale"][1].get<float>()
		},
		data["rotation"].get<float>()
	);
	myCheckmark.Init(
		data["texture"].get<std::string>().c_str(),
		myPosition,
		{
			data["scale"][0].get<float>(),
			data["scale"][1].get<float>()
		},
		data["rotation"].get<float>()
	);

	myCheckbox.SetScale({ 1.f, 0.5f });
	myCheckbox.SetUVScale({ 1.f, 0.5f });
	myCheckmark.SetScale({ 1.f, 0.5f });
	myCheckmark.SetUVScale({ 1.f, 0.5f });
	myCheckmark.SetUVOffset({ 0.f, 0.5f });

	myLabel.Init("", Tga2D::EFontSize_48);
}

WidgetCheckbox::~WidgetCheckbox()
{}

void WidgetCheckbox::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myCheckbox);
	if (myCheck)
	{
		aRenderBuffer.Add(&myCheckmark);
	}
	aRenderBuffer.Add(&myLabel);
}

void WidgetCheckbox::OnMouseReleased(const CommonUtilities::Vector2f& aMousePoint)
{
	if (myCheckbox.IsInside(aMousePoint))
	{
		SetValue(!myCheck);
		FireCallback(myCheck);
	}
}

void WidgetCheckbox::SetValue(bool aValue)
{
	myCheck = aValue;
}

void WidgetCheckbox::SetLabel(const std::string & aLabel)
{
	myLabel.SetText(aLabel);

	myLabel.SetPosition(myPosition + CommonUtilities::Vector3f(
		myCheckbox.GetUnitSize().x/2.f,
		-myCheckbox.GetUnitSize().y/12.f,
		0.001f
	));
}

void WidgetCheckbox::FireCallback(bool aValue)
{
	if (myCallback == nullptr)
	{
		return;
	}

	myCallback(aValue);
}

#pragma once

#include "Widget.h"

#include <functional>
#include <Vector.h>

#include <Text.h>

struct SpriteData;

class WidgetCheckbox : public Widget
{
public:
	WidgetCheckbox();
	WidgetCheckbox(SpriteData aSpriteData);
	~WidgetCheckbox();

	inline void SetCallback(std::function<void(bool)> aCallback) { myCallback = aCallback; }

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void OnMouseReleased(const CommonUtilities::Vector2f&) override;

	void SetValue(bool aValue);
	void SetLabel(const std::string& aLabel);

private:
	void FireCallback(bool aValue);

	Sprite myCheckbox;
	Sprite myCheckmark;

	Text myLabel;

	CommonUtilities::Vector3f myPosition;

	std::function<void(bool)> myCallback;

	bool myCheck;

};


#include "stdafx.h"
#include "WidgetFactory.h"

#include "SpriteData.h"

WidgetButton* WidgetFactory::CreateButton(SpriteData aSpriteData)
{
	return new WidgetButton(aSpriteData);
}

WidgetSlider* WidgetFactory::CreateSlider(SpriteData aSpriteData)
{
	return new WidgetSlider(aSpriteData);
}

WidgetCheckbox * WidgetFactory::CreateCheckbox(SpriteData aSpriteData)
{
	return new WidgetCheckbox(aSpriteData);
}

WidgetImage * WidgetFactory::CreateImage(SpriteData aSpriteData)
{
	return new WidgetImage(aSpriteData);
}

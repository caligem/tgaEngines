#pragma once

#include "WidgetButton.h"
#include "WidgetSlider.h"
#include "WidgetCheckbox.h"
#include "WidgetImage.h"

struct SpriteData;

namespace WidgetFactory
{
	WidgetButton* CreateButton(SpriteData aSpriteData);
	WidgetSlider* CreateSlider(SpriteData aSpriteData);
	WidgetCheckbox* CreateCheckbox(SpriteData aSpriteData);
	WidgetImage* CreateImage(SpriteData aSpriteData);
}

#include "stdafx.h"
#include "WidgetImage.h"

#include "SpriteData.h"

WidgetImage::WidgetImage(SpriteData aSpriteData)
{
	nlohmann::json::object_t& data = aSpriteData.data;
	mySprite.Init(
		data["texture"].get<std::string>().c_str(),
		{
			data["position"][0].get<float>(),
			data["position"][1].get<float>(),
			data["position"][2].get<float>()
		},
		{
			data["scale"][0].get<float>(),
			data["scale"][1].get<float>()
		},
		data["rotation"].get<float>()
	);
	if (data.find("tint") != data.end())
	{
		mySprite.SetTint(
		{
			data["tint"][0].get<float>(),
			data["tint"][1].get<float>(),
			data["tint"][2].get<float>(),
			data["tint"][3].get<float>()
		}
		);
	}
	if (data.find("UVScale") != data.end())
	{
		mySprite.SetUVScale({
			data["UVScale"][0].get<float>(),
			data["UVScale"][1].get<float>()
		});
	}
}

WidgetImage::~WidgetImage()
{}

void WidgetImage::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable *>& aRenderBuffer)
{
	aRenderBuffer.Add(&mySprite);
}

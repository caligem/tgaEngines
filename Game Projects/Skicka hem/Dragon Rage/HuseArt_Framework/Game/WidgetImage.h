#pragma once

#include "Widget.h"

#include "Sprite.h"
struct SpriteData;

class WidgetImage : public Widget
{
public:
	WidgetImage(SpriteData aSpriteData);
	~WidgetImage();

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable *>& aRenderBuffer) override;

private:
	Sprite mySprite;
};


#include "stdafx.h"
#include "WidgetSlider.h"

#include "SpriteData.h"

#include <Mathf.h>

WidgetSlider::WidgetSlider()
{
	myIsSliding = false;
}

WidgetSlider::WidgetSlider(SpriteData aSpriteData)
{
	nlohmann::json::object_t& data = aSpriteData.data;
	myPosition = {
		data["position"][0].get<float>(),
		data["position"][1].get<float>(),
		data["position"][2].get<float>()
	};

	mySliderBody.Init(
		data["texture"].get<std::string>().c_str(),
		myPosition,
		{
			data["scale"][0].get<float>(),
			data["scale"][1].get<float>()
		},
		data["rotation"].get<float>()
	);
	mySliderHead.Init(
		data["texture"].get<std::string>().c_str(),
		myPosition,
		{
			data["scale"][0].get<float>(),
			data["scale"][1].get<float>()
		},
		data["rotation"].get<float>()
	);

	mySliderBody.SetScale({ 1.f, 0.5f });
	mySliderBody.SetUVScale({ 1.f, 0.5f });
	mySliderHead.SetScale({ 1.f, 0.5f });
	mySliderHead.SetUVScale({ 1.f, 0.5f });
	mySliderHead.SetUVOffset({ 0.f, 0.5f });

	myLabel.Init("", Tga2D::EFontSize_48);

	myIsSliding = false;
}

WidgetSlider::~WidgetSlider()
{}

void WidgetSlider::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&mySliderBody);
	aRenderBuffer.Add(&mySliderHead);
	aRenderBuffer.Add(&myLabel);
}

void WidgetSlider::OnMousePressed(const CommonUtilities::Vector2f& aMousePoint)
{
	myIsSliding = mySliderBody.IsInside(aMousePoint);
}

void WidgetSlider::OnMouseDown(const CommonUtilities::Vector2f& aMousePoint)
{
	if (!myIsSliding)
	{
		return;
	}

	float halfX = mySliderBody.GetScaledSize().x / 2.f;
	float pointX = mySliderBody.GetPoint().x;
	float mouseX = CommonUtilities::Clamp(aMousePoint.x, pointX - halfX, pointX + halfX) - (pointX - halfX);

	float value = mouseX / (2.f*halfX);
	SetValue(value);
	FireCallback(value);
}

void WidgetSlider::OnMouseReleased(const CommonUtilities::Vector2f&)
{
	myIsSliding = false;
}

void WidgetSlider::SetValue(float aValue)
{
	mySliderHead.SetPosition(mySliderBody.GetPosition() + CommonUtilities::Vector3f(
		mySliderBody.GetUnitSize().x * (aValue-0.5f),
		0.f,
		0.f
	));
}

void WidgetSlider::SetLabel(const std::string & aLabel)
{
	myLabel.SetText(aLabel);

	myLabel.SetPosition(myPosition - CommonUtilities::Vector3f(
		mySliderBody.GetUnitSize().x/2.f,
		-mySliderBody.GetUnitSize().y/4.f,
		0.001f
	));
}

void WidgetSlider::FireCallback(float aValue)
{
	if (myCallback == nullptr)
	{
		return;
	}

	myCallback(aValue);
}

#pragma once

#include "Widget.h"

#include <functional>
#include <Vector.h>

#include <Text.h>

struct SpriteData;

class WidgetSlider : public Widget
{
public:
	WidgetSlider();
	WidgetSlider(SpriteData aSpriteData);
	~WidgetSlider();

	inline void SetCallback(std::function<void(float)> aCallback) { myCallback = aCallback; }

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void OnMousePressed(const CommonUtilities::Vector2f&) override;
	void OnMouseDown(const CommonUtilities::Vector2f&) override;
	void OnMouseReleased(const CommonUtilities::Vector2f&) override;

	void SetValue(float aValue);
	void SetLabel(const std::string& aLabel);

private:
	void FireCallback(float aValue);

	Sprite mySliderBody;
	Sprite mySliderHead;

	Text myLabel;

	CommonUtilities::Vector3f myPosition;

	std::function<void(float)> myCallback;

	bool myIsSliding;
};


#include "stdafx.h"
#include "WidgetSystem.h"

#include <InputManager.h>
#include <XBOXController.h>
#include <Camera.h>
#include <Mathf.h>

#include <tga2d/engine.h>

WidgetSystem::WidgetSystem(const CommonUtilities::InputManager & aInputManager, CommonUtilities::XBOXController & aXboxController)
	: myInputManager(aInputManager)
	, myXboxController(aXboxController)
{
}

WidgetSystem::~WidgetSystem()
{
	myWidgets.DeleteAll();
}

void WidgetSystem::Init()
{
	myWidgets.Init(16);

	myMousePosition = { 0.f, 0.f, 0.99f };

	myCursor.SetPosition(myMousePosition);
	myCursor.Init("Assets/Images/GUI/cursor.dds");
}

void WidgetSystem::Update(float aDeltaTime)
{
	HandleCursorInput(aDeltaTime);

	bool hasUsedMouse = HandleMouseEvents();
	if (!hasUsedMouse)
	{
		HandleControllerEvents();
	}

	for (Widget* widget : myWidgets)
	{
		widget->Update(aDeltaTime);
	}
}

void WidgetSystem::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	for (Widget* widget : myWidgets)
	{
		widget->FillRenderBuffer(aRenderBuffer);
	}

	HandleCursorInput();

	myCursor.SetPosition(myMousePosition);
	aRenderBuffer.Add(&myCursor);
}

void WidgetSystem::HandleCursorInput(float aDeltaTime)
{
	bool hasUsedMouse = HandleMouseCursorInput();
	if (!hasUsedMouse)
	{
		HandleControllerCursorInput(aDeltaTime);
	}

	myMousePosition.x = CommonUtilities::Clamp(myMousePosition.x, -Tga2D::CEngine::GetInstance()->GetWindowRatio(), Tga2D::CEngine::GetInstance()->GetWindowRatio());
	myMousePosition.y = CommonUtilities::Clamp(myMousePosition.y, -1.f, 1.f);

	myCursor.SetPosition(myMousePosition);
}

bool WidgetSystem::HandleMouseCursorInput()
{
	if (myInputManager.HasMouseMoved())
	{
		myMousePosition = {
			CommonUtilities::Clamp(myInputManager.GetMousePosition().x / Tga2D::CEngine::GetInstance()->GetWindowSize().x, 0.0f, 1.0f),
			CommonUtilities::Clamp(myInputManager.GetMousePosition().y / Tga2D::CEngine::GetInstance()->GetWindowSize().y, 0.0f, 1.0f),
			0.99f
		};
		myMousePosition.x = myMousePosition.x * 2.0f - 1.0f;
		myMousePosition.x *= Tga2D::CEngine::GetInstance()->GetWindowRatio();
		myMousePosition.y = ((-2.f*myMousePosition.y + 1.f));
		return true;
	}

	return false;
}

void WidgetSystem::HandleControllerCursorInput(float aDeltaTime)
{
	if (myXboxController.IsConnected())
	{
		myMousePosition += CommonUtilities::Vector3f(
			myXboxController.GetStick(CommonUtilities::XStick_Left).x,
			myXboxController.GetStick(CommonUtilities::XStick_Left).y,
			0.f
		) * aDeltaTime * 2.f;
	}
}

bool WidgetSystem::HandleMouseEvents()
{
	bool mouseUsed = true;

	if (myInputManager.IsButtonPressed(CommonUtilities::Button_Left))
	{
		TriggerOnPressedEvents();
	}
	else if (myInputManager.IsButtonDown(CommonUtilities::Button_Left))
	{
		TriggerOnDownEvents();
	}
	else if (myInputManager.IsButtonReleased(CommonUtilities::Button_Left))
	{
		TriggerOnReleasedEvents();
	}
	else if (myInputManager.HasMouseMoved())
	{
		TriggerOnMoved();
	}
	else
	{
		mouseUsed = false;
	}

	return mouseUsed;
}

void WidgetSystem::HandleControllerEvents()
{
	if (!myXboxController.IsConnected())
	{
		return;
	}

	if (myXboxController.IsButtonPressed(CommonUtilities::XButton_A))
	{
		TriggerOnPressedEvents();
	}
	else if (myXboxController.IsButtonDown(CommonUtilities::XButton_A))
	{
		TriggerOnDownEvents();
	}
	else if (myXboxController.IsButtonReleased(CommonUtilities::XButton_A))
	{
		TriggerOnReleasedEvents();
	}
	else if (myXboxController.GetStick(CommonUtilities::XStick_Left).Length2() > 0.0f)
	{
		TriggerOnMoved();
	}
}

void WidgetSystem::TriggerOnPressedEvents()
{
	for (Widget* widget : myWidgets)
	{
		widget->OnMousePressed(myCursor.GetPoint());
	}
}

void WidgetSystem::TriggerOnDownEvents()
{
	for (Widget* widget : myWidgets)
	{
		widget->OnMouseDown(myCursor.GetPoint());
	}
}

void WidgetSystem::TriggerOnReleasedEvents()
{
	for (Widget* widget : myWidgets)
	{
		widget->OnMouseReleased(myCursor.GetPoint());
	}
}

void WidgetSystem::TriggerOnMoved()
{
	for (Widget* widget : myWidgets)
	{
		widget->OnMouseMoved(myCursor.GetPoint());
	}
}

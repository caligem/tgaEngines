#pragma once

#include "Widget.h"
#include <GrowingArray.h>

#include <Sprite.h>

namespace CommonUtilities
{
	class InputManager;
	class XBOXController;
	class Camera;
}

class WidgetSystem
{
public:
	WidgetSystem(const CommonUtilities::InputManager& aInputManager, CommonUtilities::XBOXController& aXboxController);
	~WidgetSystem();

	void Init();
	void Update(float aDeltaTime);

	inline void AddWidget(Widget* aWidget) { myWidgets.Add(aWidget); }

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer);

private:
	void HandleCursorInput(float aDeltaTime = 0.0f);
	bool HandleMouseCursorInput();
	void HandleControllerCursorInput(float aDeltaTime);

	bool HandleMouseEvents();
	void HandleControllerEvents();

	void TriggerOnPressedEvents();
	void TriggerOnDownEvents();
	void TriggerOnReleasedEvents();
	void TriggerOnMoved();

	const CommonUtilities::InputManager& myInputManager;
	CommonUtilities::XBOXController& myXboxController;

	CommonUtilities::GrowingArray<Widget*> myWidgets;

	CommonUtilities::Vector3f myMousePosition;

	Sprite myCursor;
};


//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Launcher.rc
//
#define IDI_ICON1                       101
#define IDC_NAME                        1003
#define IDC_TAG                         1004
#define IDD_COLLISION_CIRCLE            1005
#define IDC_HP                          1006
#define IDC_TRANSFORM_PX                1007
#define IDC_HP2                         1007
#define IDD_SPRITE                      1008
#define IDC_MOVEMENT_AMOUNT             1008
#define IDC_TRANSFORM_PY                1009
#define IDD_ANIMATION                   1010
#define IDC_MOVEMENT_INTERVAL           1010
#define IDC_TRANSFORM_PZ                1011
#define IDD_COLLISION_LINE              1012
#define IDC_MOVEMENT_SPEED              1012
#define IDC_TRANSFORM_SX                1013
#define IDD_ENEMY                       1014
#define IDC_TRANSFORM_SY                1015
#define IDD_ENEMY_CLASS                 1016
#define IDC_TRANSFORM_R                 1017
#define IDD_BUTTON                      1018
#define IDC_TINT_R                      1019
#define IDD_CURVE                       1019
#define IDC_TINT_G                      1020
#define IDC_TINT_B                      1021
#define IDC_ANIMATION_COLS              1022
#define IDC_TRANSFORM_UVX               1023
#define IDC_TRANSFORM_UVY               1024
#define IDC_TAG2                        1025
#define IDC_LOCKED2                     1026
#define IDC_ANIMATION_ROWS              1027
#define IDC_LOCKED                      1028
#define IDC_ANIMATION_FRAMES            1029
#define IDC_ANIMATION_OFFSET            1030
#define IDC_ANIMATION_DURATION          1031
#define IDC_TINT_A                      1032
#define IDC_COLLIDER_CIRCLE_RADIUS      1033
#define IDC_COLLIDER_LINE_LENGTH        1034
#define IDC_LOAD_CLASS                  1035
#define IDC_SAVE_CLASS                  1036
#define IDC_ATTACK_DMG                  1037
#define IDC_ATTACK_TYPE                 1038
#define IDC_CLASS_NAME                  1039
#define IDC_ATTACK_ROF                  1040
#define IDC_ATTACK_SPEED                1041

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        112
#define _APS_NEXT_COMMAND_VALUE         40002
#define _APS_NEXT_CONTROL_VALUE         1037
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif

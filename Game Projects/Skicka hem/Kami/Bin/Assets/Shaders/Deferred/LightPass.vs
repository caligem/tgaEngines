#include "LightPass.si"
#include "../CameraBuffer.si"

cbuffer InstanceData : register(b1)
{
	float4x4 toWorld;
}

PixelInput main(VertexInput input)
{
	PixelInput output;

	input.myPosition.w = 1.f;
	output.myPosition = mul(toWorld, input.myPosition);
	output.myWorldPosition = output.myPosition;

	output.myUV = input.myUV;

	output.myViewPosition = float4(cameraOrientation._14, cameraOrientation._24, cameraOrientation._34, 1.f);

	return output;
}
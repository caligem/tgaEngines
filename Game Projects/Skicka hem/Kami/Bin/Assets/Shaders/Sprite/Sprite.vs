#include "Sprite.si"

cbuffer QuadData : register(b0)
{
	float2 screenSize;
	float2 spriteSize;

	float2 position;
	float2 scale;
	float2 pivot;
	float2 uvOffset;
	float2 uvScale;
	float rotation;
	float _padding;
	float4 tint;
}

PixelInput main(VertexInput input)
{
	PixelInput output;

	input.myPosition.x -= (2.f*pivot.x) - 1.f;
	input.myPosition.y += (2.f*pivot.y) - 1.f;

	float2 ratio = spriteSize.xy / screenSize.xy;

	float relativeScale = screenSize.y / 1080.f;
	
	input.myPosition.xy *= scale * relativeScale;

	float rot = rotation;
	float c = cos(rot);
	float s = sin(rot);
	float2x2 rotMat = float2x2(
		c, s,
		-s, c
	);
	input.myPosition.xy = mul(rotMat, input.myPosition.xy);

	input.myPosition.xy *= ratio;

	input.myPosition.x += (2.f*position.x)-1.f;
	input.myPosition.y -= (2.f*position.y)-1.f;

	output.myPosition = input.myPosition;
	output.myUV = (uvOffset) + input.myUV.xy * uvScale.xy;

	output.myTint = tint;
	
	return output;
}
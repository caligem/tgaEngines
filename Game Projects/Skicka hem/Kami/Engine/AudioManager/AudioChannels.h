#pragma once
enum class AudioChannel
{
	Master,
	Music,
	SoundEffects
};
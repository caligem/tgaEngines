#include "AudioListener.h"


#pragma comment (lib, "../lib/fmodstudio64_vc.lib")
AudioListener::AudioListener()
{
	FMOD_VECTOR fmodVector;
	fmodVector.x = 0.0f;
	fmodVector.y = 0.0f;
	fmodVector.z = 0.0f;
	my3DAttributes.velocity = fmodVector;
	fmodVector.y = 1.0f;
	my3DAttributes.up = fmodVector;
	fmodVector.y = 0.0f;
	fmodVector.z = 1.0f;
	my3DAttributes.forward = fmodVector;
}


AudioListener::~AudioListener()
{
}

void AudioListener::SetPosition(CommonUtilities::Vector3f aPosition)
{
	myPosition = aPosition;
	FMOD_VECTOR fmodVector;
	fmodVector.x = myPosition.x;
	fmodVector.y = myPosition.y;
	fmodVector.z = myPosition.z;
	my3DAttributes.position = fmodVector;
}

#include "AudioSource.h"
#include "AudioManager.h"

#pragma comment (lib, "../lib/fmodstudio64_vc.lib")
AudioSource::AudioSource()
{
	FMOD_VECTOR fmodVector;
	fmodVector.x = 0.0f;
	fmodVector.y = 0.0f;
	fmodVector.z = 0.0f;
	my3DAttributes.velocity = fmodVector;
	fmodVector.y = 1.0f;
	my3DAttributes.up = fmodVector;
}


AudioSource::~AudioSource()
{
	AM.RemoveConnectedAudioEvents(this);
}

void AudioSource::SetPosition(CommonUtilities::Vector3f aPosition)
{
	myPosition = aPosition;
	FMOD_VECTOR fmodVector;
	fmodVector.x = 1.0f;
	fmodVector.y = 0.0f;
	fmodVector.z = 0.0f;
	if (myPosition.x > aPosition.x)
	{
		fmodVector.x = -1.0f;
	}
	my3DAttributes.forward = fmodVector;
	fmodVector.x = myPosition.x;
	fmodVector.y = myPosition.y;
	fmodVector.z = myPosition.z;
	my3DAttributes.position = fmodVector;
}

void AudioSource::LoadAudioEvent(const char * aAudioName, bool aRemoveAfterPlayback, AudioChannel aAudioChannel)
{
	AM.LoadAudioEvent(aAudioName, aRemoveAfterPlayback, this, aAudioChannel);
}

void AudioSource::UnLoadAudioSource()
{
	AM.RemoveConnectedAudioEvents(this);
	AM.ReturnAudioSource(this);
}

void AudioSource::Stop(const char * aAudioName, bool aShouldBeImmediate)
{
	AM.Stop(aAudioName, aShouldBeImmediate, this);
}

void AudioSource::Play(const char * aAudioName)
{
	AM.Play(aAudioName, this);
}

void AudioSource::PlayNewInstance(const char * aAudioName, AudioChannel aAudioChannel)
{
	AM.PlayNewInstance(aAudioName, this, aAudioChannel);
}


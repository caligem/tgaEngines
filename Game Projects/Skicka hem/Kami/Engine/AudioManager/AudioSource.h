#pragma once
#include "../CommonUtilities/Vector.h"
#include "FMOD/fmod_studio.hpp"
#include "AudioChannels.h"
class AudioSource
{
public:
	AudioSource();
	~AudioSource();
	CommonUtilities::Vector3f GetPosition() { return myPosition; }
	void SetPosition(CommonUtilities::Vector3f aPosition);
	FMOD_3D_ATTRIBUTES* Get3DAttributes() { return &my3DAttributes; }

	void LoadAudioEvent(const char* aAudioName, bool aRemoveAfterPlayback = false, AudioChannel aAudioChannel = AudioChannel::SoundEffects);
	void UnLoadAudioSource();

	void Stop(const char * aAudioName, bool aShouldBeImmediate = true);
	void Play(const char * aAudioName);
	void PlayNewInstance(const char * aAudioName, AudioChannel aAudioChannel = AudioChannel::SoundEffects);
private:
	CommonUtilities::Vector3f myPosition;
	FMOD_3D_ATTRIBUTES my3DAttributes;
};


#include "AudioSourcePool.h"
#include <assert.h>
#pragma comment (lib, "../lib/fmodstudio64_vc.lib")
AudioSourcePool::AudioSourcePool()
{
	for (int i = 0; i < myFreeIndexes.size(); ++i)
	{
		myFreeIndexes[i] = true;
	}
}


AudioSourcePool::~AudioSourcePool()
{
}

AudioSource* AudioSourcePool::GetAudioSource()
{
	for (int i = 0; i < myFreeIndexes.size(); ++i)
	{
		if (myFreeIndexes[i])
		{
			myFreeIndexes[i] = false;
			return &myAudioSourcePool[i];
		}
	}
	return nullptr;
}

int AudioSourcePool::GetAmmountOfFreeAudioSources()
{
	int freeSpaces = 0;
	for (int i = 0; i < myFreeIndexes.size(); ++i)
	{
		if (myFreeIndexes[i] == true)
		{
			++freeSpaces;
		}
	}
	return freeSpaces;
}

void AudioSourcePool::ReturnAudioSource(AudioSource * aAudioSource)
{
	for (int i = 0; i < myAudioSourcePool.size(); ++i)
	{
		if (&myAudioSourcePool[i] == aAudioSource)
		{
			myFreeIndexes[i] = true;
		}
	}
}

#pragma once

class CEditorProxy;

#include "PostMasterMessageType.cs"

typedef void(*EditorCallbackMessageHandler)(EMessageType aMessageType, EDataType aDataType);

public ref class CEditorBridge
{
public:
	CEditorBridge();

	void Init(unsigned char aEditorType, System::IntPtr aHandle);
	void WndProc(System::Windows::Forms::Message aMessage);

	void Shutdown();

	bool IsRunning();

	void SendMessage(array<System::Byte>^ aData, int aMessageType, int aDataType);
	void SetEditorMessageCallback(System::IntPtr aCallback);

	System::Collections::Generic::List<System::Byte>^ GetByteList() { return ourByteList; }

	static System::Collections::Generic::List<System::Byte>^ ourByteList = nullptr;
	static EditorCallbackMessageHandler ourEditorMessageHandler;

	void GotFocus();
	void LostFocus();

private:
	CEditorProxy* myEditorProxy;

};


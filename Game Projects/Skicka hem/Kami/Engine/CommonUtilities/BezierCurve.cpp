#include "BezierCurve.h"

namespace CommonUtilities
{
	BezierCurve::BezierCurve()
	{
		myIsBuilt = false;
		myPoints.Init(8);
		myBuffer.Init(8);
		myLength = 0.f;
	}

	BezierCurve::~BezierCurve()
	{}

	void BezierCurve::AddPoint(const Vector3f & aPoint)
	{
		myPoints.Add(aPoint);
	}
	void BezierCurve::PopPoint()
	{
		myPoints.Pop();
	}
	void BezierCurve::ClearPoints()
	{
		myPoints.RemoveAll();
	}
	const Vector3f BezierCurve::GetPoint(const float t)
	{
		if( myPoints.Empty() || !myIsBuilt )
		{
			return Vector3f();
		}
		if( t <= 0.f )
		{
			return myPoints[0];
		}
		if( t >= 1.f )
		{
			return myPoints.GetLast();
		}

		for( int i = 0; i < LUTSize-1; ++i )
		{
			if( myLUT[i].myTime < t && myLUT[i+1].myTime > t || i == LUTSize-2 )
			{
				float t1 = (t - myLUT[i].myTime) / (myLUT[i+1].myTime - myLUT[i].myTime);
				return myLUT[i].myPoint * (1.f - t1) + myLUT[i + 1].myPoint * t1;
			}
		}

		return myPoints.GetLast();
	}
	void BezierCurve::BuildCurve()
	{
		CacheLength();
		CacheLUT();
		myIsBuilt = true;
	}
	const Vector3f BezierCurve::GetPointAbsolute(const float t)
	{
		if( myPoints.Empty() )
		{
			return Vector3f();
		}

		myBuffer = myPoints;

		int n = myPoints.Size();
		
		while( n > 1 )
		{
			--n;
			for( unsigned short i = 0; i < n; ++i )
			{
				myBuffer[i] = myBuffer[i] * (1.f - t) + myBuffer[i + 1] * t;
			}
		}

		return myBuffer[0];
	}
	float BezierCurve::CalcLength(float end)
	{
		if( myPoints.Empty() )
		{
			return 0.f;
		}
		float length = 0.f;
		float step = end / 128.f;
		for( float i = 0.f; i < end; i += step )
		{
			length += (GetPointAbsolute(i+step)-GetPointAbsolute(i)).Length();
		}
		return length;
	}
	void BezierCurve::CacheLength()
	{
		myLength = CalcLength();
	}
	void BezierCurve::CacheLUT()
	{
		float step = 1.f / static_cast<float>(LUTSize);
		for( int i = 0; i < LUTSize; ++i )
		{
			float t = static_cast<float>(i) * step;
			myLUT[i] = {
				GetPointAbsolute(t),
				CalcLength(t) / myLength
			};
		}
	}
}

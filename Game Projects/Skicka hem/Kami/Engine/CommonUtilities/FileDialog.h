#pragma once

#include <string>

namespace CommonUtilities
{
	bool SaveFile(const std::string& aFilePath, const std::string& aData, bool aEncode = false);
	std::string OpenFile(const std::string& aFilePath, bool aEncoded = false);

	std::string GetMyDocumentsPath();
	bool CreateFilePath(const std::string& aPath);
	std::string GetMyGamePath();
}

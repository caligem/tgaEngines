#include "Intersection.h"

namespace CommonUtilities
{
	bool CircleVsCircle(const Vector2f & aPoint0, float aRadius0, const Vector2f & aPoint1, float aRadius1, CommonUtilities::Vector2f & aContactPoint)
	{
		float d2 = (aPoint0 - aPoint1).Length2();
		float r2 = aRadius0 + aRadius1;
		r2 *= r2;

		if( d2 <= r2 )
		{
			aContactPoint = (aPoint0 + aPoint1) / 2.f;
			return true;
		}
		return false;
	}

	bool CircleVsLine(const Vector2f & aPointCircle, float aRadiusCircle, const Vector2f & aStartPointLine, const Vector2f & aEndPointLine, CommonUtilities::Vector2f & aContactPoint)
	{
		CommonUtilities::Vector2f dir = aEndPointLine - aStartPointLine;
		CommonUtilities::Vector2f normal = dir.GetNormal().GetNormalized();

		CommonUtilities::Vector2f diff = aPointCircle - aStartPointLine;

		float t = diff.Dot(dir) / dir.Length2();
		CommonUtilities::Vector2f closestPoint = aStartPointLine + dir * t;
		if( t <= 0.f )
		{
			closestPoint = aStartPointLine;
		}
		else if( t >= 1.f )
		{
			closestPoint = aEndPointLine;
		}

		float d2 = (aPointCircle - closestPoint).Length2();

		float r2 = aRadiusCircle*aRadiusCircle;

		if( d2 <= r2 )
		{
			aContactPoint = closestPoint;
			return true;
		}
		return false;
	}
}
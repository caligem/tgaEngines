#pragma once

#include "EditorEngine.h"
#include "DoubleBuffer.h"

#include "PostMasterMessageType.cs"

struct SMessage
{
	EMessageType myMessageType;
	EDataType myDataType;
	const unsigned char* myData;
};

class CEditor
{
public:
	CEditor();
	~CEditor();

	bool Init(HWND aHWND = NULL);
	void Shutdown();
	void StartEngine();

	void SendWindowMessage(unsigned int Msg, void* wParam, void* lParam);
	void GotFocus();
	void LostFocus();
	
	bool IsRunning();

	void RecieveMessage(const unsigned char* newData, EMessageType aMessageType, EDataType aDataType);
	void Subscribe(EMessageType aMessageType, std::function<void(const SMessage&)> aCallback);
	void SetEditorMessageCallback(std::function<void(const unsigned char*, EMessageType, EDataType)> aCallback) { myEditorMessageCallback = aCallback; }

	static void SendMessage(const unsigned char* newData, EMessageType aMessageType, EDataType aDataType);

protected:
	LRESULT WndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam);
	virtual void InitCallback();
	virtual void UpdateCallback();
	void UpdateInput();
	void HandleMessages();


	struct SWindowMessage
	{
		unsigned int myMessage;
		unsigned long long myWParam;
		long long myLParam;
	};

	std::array<std::function<void(const SMessage&)>, EMessageType_Count> myEngineMessageCallbacks;
	std::function<void(const unsigned char*, EMessageType, EDataType)> myEditorMessageCallback;
	CDoubleBuffer<SMessage> myEngineMessageDoubleBuffer;
	CDoubleBuffer<SMessage> myEditorMessageDoubleBuffer;
	std::mutex myOutgoingMessageLock;
	std::mutex myIncomingMessageLock;
	CDoubleBuffer<SWindowMessage> myWindowMessageDoubleBuffer;
	std::mutex myInputLock;

	CEditorEngine myEngine;
	std::wstring myApplicationName;

	volatile bool myHasFocus;

private:
	static CEditor* ourEditor;
};


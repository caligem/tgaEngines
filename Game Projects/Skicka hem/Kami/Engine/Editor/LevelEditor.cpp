#include "stdafx.h"
#include "LevelEditor.h"

#include "Mathf.h"
#include "IEditor.h"
#include "IWorld.h"

CLevelEditor::CLevelEditor()
{
	myPivot = { 0.0f, 0.0f, 0.0f };
	myRotation = { 0.f, 0.f };
	myZoom = 20.f;
}

CLevelEditor::~CLevelEditor()
{
}

void CLevelEditor::InitCallback()
{
	CState::Init();
	SetActiveScene();

	myMainCamera.SetPosition({ 0.0f, 0.0f, -10.f });

	Subscribe(EMessageType_Transform_SetPosition, [=](const SMessage& aMessage)
	{
		CommonUtilities::Vector3f vec;
		memcpy(vec.pData, aMessage.myData, sizeof(CommonUtilities::Vector3f));
		if(myFocus.IsValid())
			myFocus.SetPosition(vec);
	});
	Subscribe(EMessageType_Transform_SetRotation, [=](const SMessage& aMessage)
	{
		CommonUtilities::Vector3f vec;
		memcpy(vec.pData, aMessage.myData, sizeof(CommonUtilities::Vector3f));
		vec *= CommonUtilities::Deg2Rad;
		if(myFocus.IsValid())
			myFocus.GetTransform().SetRotation(vec);
	});
	Subscribe(EMessageType_Transform_SetScale, [=](const SMessage& aMessage)
	{
		CommonUtilities::Vector3f vec;
		memcpy(vec.pData, aMessage.myData, sizeof(CommonUtilities::Vector3f));
		if(myFocus.IsValid())
			myFocus.GetTransform().SetScale(vec);
	});
	Subscribe(EMessageType_MeshFilter_AddMesh, [=](const SMessage& aMessage)
	{
		if (myFocus.IsValid())
		{
			GENERAL_LOG(CONCOL_DEFAULT, "Entered");
			myFocus.AddComponent<CModelComponent>(reinterpret_cast<const char*>(aMessage.myData));
		}
	});

	myEngine.SetManipulationToolHandler(std::bind(&CLevelEditor::ManipulationToolHandler, this));
}

void CLevelEditor::UpdateCallback()
{
	CEditor::UpdateCallback();

	UpdateCameraControls();
	DrawGrid();

	Input::CInputManager input = IEditor::Input();
	if (input.IsKeyPressed(Input::Key_A))
	{
		myFocus.Init(mySceneID);
		myFocus.SetPosition(myPivot);
	}
}

void CLevelEditor::UpdateCameraControls()
{
	Input::CInputManager input = IEditor::Input();

	CommonUtilities::Vector2f movement = input.GetMouseMovement();
	movement.x *= (2.f*CommonUtilities::Pif) / IEditor::GetWindowSize().x;
	movement.y *= (CommonUtilities::Pif) / IEditor::GetWindowSize().y;

	if (input.IsKeyDown(Input::Key_Alt))
	{
		if (input.IsButtonDown(Input::Button_Left))
		{
			myRotation.x += movement.y;
			myRotation.y += movement.x;

			if (myRotation.x >= CommonUtilities::Pif / 2.f)
			{
				myRotation.x = CommonUtilities::Pif / 2.f - 0.001f;
			}
			if (myRotation.x <= -CommonUtilities::Pif / 2.f)
			{
				myRotation.x = -CommonUtilities::Pif / 2.f + 0.001f;
			}
		}

		if (input.IsButtonDown(Input::Button_Middle))
		{
			myPivot -= myMainCamera.GetTransform().GetRight() * movement.x * myZoom;
			myPivot += myMainCamera.GetTransform().GetUp() * movement.y * myZoom;
		}

		if (input.IsButtonDown(Input::Button_Right))
		{
			myZoom -= (movement.x + movement.y) * myZoom;
			if (myZoom < 0.1f)
			{
				myZoom = 0.1f;
				myPivot += myMainCamera.GetTransform().GetForward() * (movement.x + movement.y);
			}
		}
	}

	CommonUtilities::Vector3f newPos(0.f, 0.f, -myZoom);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundX(myRotation.x);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundY(myRotation.y);
	newPos += myPivot;

	myMainCamera.GetTransform().SetPosition(newPos);
	myMainCamera.GetTransform().LookAt(myPivot);
}

void CLevelEditor::ManipulationToolHandler()
{
	Input::CInputManager& input = IEditor::Input();
	if (input.IsKeyPressed(Input::Key_Q))
	{
		myManipulationTool.SetMode(CManipulationTool::EMode_None);
	}
	if (input.IsKeyPressed(Input::Key_W))
	{
		myManipulationTool.SetMode(CManipulationTool::EMode_Translate);
	}
	if (input.IsKeyPressed(Input::Key_E))
	{
		myManipulationTool.SetMode(CManipulationTool::EMode_Rotate);
	}
	if (input.IsKeyPressed(Input::Key_R))
	{
		myManipulationTool.SetMode(CManipulationTool::EMode_Scale);
	}
	if (input.IsKeyPressed(Input::Key_1))
	{
		myManipulationTool.SetSpace(CManipulationTool::ESpace_Local);
	}
	if (input.IsKeyPressed(Input::Key_2))
	{
		myManipulationTool.SetSpace(CManipulationTool::ESpace_World);
	}

	if (myFocus.IsValid())
	{
		myManipulationTool.UpdateGameObject(myFocus);
	}
}

void CLevelEditor::DrawGrid()
{
	CommonUtilities::Vector2f xzPos = {
		std::floorf(myPivot.x),
		std::floorf(myPivot.z)
	};

	float gridRes = 25.f;
	float gridSize = 1.f;
	gridSize = std::ceilf(myZoom / 20.f);
	float gridDist = gridRes * gridSize;
	for (float i = -gridRes; i <= gridRes; ++i)
	{
		IWorld::SetDebugColor({ 0.3f, 0.3f, 0.3f, 0.5f });
		if ((int)(xzPos.x + i) % 5 == 0)
		{
			IWorld::SetDebugColor({ 0.2f, 0.2f, 0.2f, 0.75f });
		}
		IWorld::DrawLine({ xzPos.x + i*gridSize, 0.f, xzPos.y - gridDist }, { xzPos.x + i*gridSize, 0.f, xzPos.y + gridDist });

		IWorld::SetDebugColor({ 0.3f, 0.3f, 0.3f, 0.5f });
		if ((int)(xzPos.y + i) % 5 == 0)
		{
			IWorld::SetDebugColor({ 0.2f, 0.2f, 0.2f, 0.75f });
		}
		IWorld::DrawLine({ xzPos.x - gridDist, 0.f, xzPos.y + i*gridSize }, { xzPos.x + gridDist, 0.f, xzPos.y + i*gridSize });
	}
}

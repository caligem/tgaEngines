﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditorClasses
{
	public abstract class ISerialize
	{
		public abstract byte[] Serialize();
	}
}

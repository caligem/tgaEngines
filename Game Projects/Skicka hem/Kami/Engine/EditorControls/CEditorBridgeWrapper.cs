﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.InteropServices;

namespace EditorControls
{
	public delegate void MessageCallback(PostMaster.EMessageType aMessageType, PostMaster.EDataType aDataType);
	public delegate void SubscribeCallback(byte[] aData, PostMaster.EDataType aDataType);

	public class CEditorBridgeWrapper
	{
		private CEditorBridge myEditorBridge;
		private Thread myThread;
        private volatile bool myIsRunning = true;

        private Dictionary<PostMaster.EMessageType, List<SubscribeCallback>> mySubscriptions;

        #region Init/Shutdown
        public CEditorBridgeWrapper()
		{
            mySubscriptions = new Dictionary<PostMaster.EMessageType, List<SubscribeCallback>>();
		}
		public void Shutdown()
		{
            myIsRunning = false;
            if(myEditorBridge != null)myEditorBridge.Shutdown();
			myThread.Abort();
		}
		public void Init(object aHandle)
		{
            myThread = new Thread(new ParameterizedThreadStart(StartEditor));
            myThread.Start(aHandle);
		}
        private void StartEditor(object aHandle)
        {
            if (!myIsRunning) return;
            myEditorBridge = new CEditorBridge();

            myEditorCallbackHandler = new MessageCallback(MessageCallbackHandler);
            myEditorBridge.SetEditorMessageCallback(Marshal.GetFunctionPointerForDelegate(myEditorCallbackHandler));

            myEditorBridge.Init((byte)PostMaster.EEditorType.EEditorType_LevelEditor, (IntPtr)aHandle);
        }
        #endregion

        #region EditorMessageCallbackHandler

        private MessageCallback myEditorCallbackHandler;
		private void MessageCallbackHandler(PostMaster.EMessageType aMessageType, PostMaster.EDataType aDataType)
		{
			List<byte> data = myEditorBridge.GetByteList();

			if (data == null) return;

            if (!mySubscriptions.ContainsKey(aMessageType)) return;

            foreach(SubscribeCallback callback in mySubscriptions[aMessageType])
            {
                callback(data.ToArray(), aDataType);
            }
		}
        public void Subscribe(PostMaster.EMessageType aMessageType, SubscribeCallback aCallback)
        {
            if(!mySubscriptions.ContainsKey(aMessageType))
            {
                mySubscriptions[aMessageType] = new List<SubscribeCallback>();
            }

            mySubscriptions[aMessageType].Add(aCallback);
        }

        #endregion

        #region MessageCommunication
        public void SendMessage<T>(T aSerializable, PostMaster.EMessageType aMessageType, PostMaster.EDataType aDataType) where T : EditorClasses.ISerialize
		{
			if (aMessageType == PostMaster.EMessageType.EMessageType_Invalid) return;
			if (aDataType == PostMaster.EDataType.EDataType_Invalid) return;

			myEditorBridge.SendMessage(aSerializable.Serialize(), (int)aMessageType, (int)aDataType);
		}
		public void SendMessageObject(object aObject, PostMaster.EMessageType aMessageType, PostMaster.EDataType aDataType)
		{
			if (aObject == null)
				return;
			if (aMessageType == PostMaster.EMessageType.EMessageType_Invalid) return;
			if (aDataType == PostMaster.EDataType.EDataType_Invalid) return;

			using (MemoryStream m = new MemoryStream())
			{
				BinaryFormatter formatter = new BinaryFormatter();
				formatter.Serialize(m, aObject);
				myEditorBridge.SendMessage(m.ToArray(), (int)aMessageType, (int)aDataType);
			}
		}
		public void SendMessageString(string aString, PostMaster.EMessageType aMessageType, PostMaster.EDataType aDataType)
		{
			using (MemoryStream m = new MemoryStream())
			{
				using (BinaryWriter writer = new BinaryWriter(m))
				{
					writer.Write((int)aString.Length);
					foreach(byte c in aString)
					{
						writer.Write(c);
					}
				}
				myEditorBridge.SendMessage(m.ToArray(), (int)aMessageType, (int)aDataType);
			}
		}
        #endregion

        #region WndProc
        static int[] myMessageFilter = new int[] {
            2,      //WM_DESTROY
            5,      //WM_SIZE
            16,     //WM_CLOSE
            256,
            257,
            260,
            261,
            512,
            513,
            514,
            516,
            517,
            519,
            520,
            522
        };
        public void WndProc(ref Message m)
        {
            if(!myIsRunning)
            {
                return;
            }

            bool validMessage = false;
            for (int i = 0; i < myMessageFilter.Length; ++i)
            {
                if (m.Msg == myMessageFilter[i])
                {
                    validMessage = true;
                    break;
                }
            }

            if (!validMessage)
            {
                return;
            }

            if (myEditorBridge != null)
            {
                if (myEditorBridge.IsRunning())
                    myEditorBridge.WndProc(m);
            }
        }
        public void GotFocus()
        {
            if (myEditorBridge != null)
                myEditorBridge.GotFocus();
        }
        public void LostFocus()
        {
            if (myEditorBridge != null)
                myEditorBridge.LostFocus();
        }
        #endregion
    }
}

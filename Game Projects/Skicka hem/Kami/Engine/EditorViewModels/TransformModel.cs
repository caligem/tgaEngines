﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditorViewModels
{
	public class TransformModel : TransformViewModel
	{
		public static TransformModel Instance => new TransformModel();
		public TransformModel()
		{
			myPosition = new Vector3Model("Position");
			myRotation = new Vector3Model("Rotation");
			myScale = new Vector3Model("Scale", 1f, 1f, 1f);
		}
	}
}

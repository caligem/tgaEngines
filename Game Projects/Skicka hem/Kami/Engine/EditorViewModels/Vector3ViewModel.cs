﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditorViewModels
{
	public class Vector3ViewModel
	{
		public string myLabel { get; set; }
		public float myX { get; set; }
		public float myY { get; set; }
		public float myZ { get; set; }
	}
}

#include "stdafx.h"
#include "C2DRenderer.h"

#include "IEngine.h"
#include "ShaderManager.h"
#include "SpriteManager.h"
#include "RenderCommand.h"
#include "TextManager.h"

#include "DDSTextureLoader.h"
#include "TextureBuilder.h"


C2DRenderer::C2DRenderer()
{
}

C2DRenderer::~C2DRenderer()
{
}

bool C2DRenderer::Init()
{
	if (!InitBuffers())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to load buffers for C2DRenderer!");
		return false;
	}

	myVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/Sprite/Sprite.vs", EShaderInputLayoutType_Sprite);
	mySpritePixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/Sprite/Sprite.ps");
	myTextPixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/Sprite/Text.ps");
	return true;
}

void C2DRenderer::Render(const CommonUtilities::GrowingArray<SSpriteRenderCommand>& aSpritesToRender, const CommonUtilities::GrowingArray<STextRenderCommand>& aTextToRender)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	myVertexShader->Bind();
	myVertexShader->BindLayout();
	mySpritePixelShader->Bind();
	
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetVertexBuffers(0, 1, &myQuad.myVertexBuffer, &myQuad.myStride, &myQuad.myOffset);
	context->IASetIndexBuffer(myQuad.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	for (const SSpriteRenderCommand& command : aSpritesToRender)
	{
		CSprite* sprite = IEngine::GetSpriteManager().GetSprite(command.mySpriteID);
		context->PSSetShaderResources(0, 1, &sprite->myTexture);
		SQuadBufferData data;
		data.myScreenSize = IEngine::GetCanvasSize();
		data.mySpriteSize = command.mySpriteData.myOriginalTextureSize;

		data.myPosition = command.mySpriteData.myPosition;
		data.myScale = command.mySpriteData.myScale;
		data.myPivot = command.mySpriteData.myPivot;
		data.myRotation = command.mySpriteData.myRotation;
		data.myUVOffset = command.mySpriteData.myUVOffset;
		data.myUVScale = command.mySpriteData.myUVScale;
		data.myTint = command.mySpriteData.myTint;

		myQuadBuffer.SetData(&data);
		myQuadBuffer.SetBuffer(0, EShaderType_Vertex);

		context->DrawIndexed(myQuad.myNumberOfIndices, 0, 0);
	}

	RenderText(aTextToRender);

	myVertexShader->Unbind();
	myVertexShader->UnbindLayout();
	mySpritePixelShader->Unbind();
}

void C2DRenderer::RenderText(const CommonUtilities::GrowingArray<STextRenderCommand>& aTextToRender)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	myTextPixelShader->Bind();

	for (const STextRenderCommand& command : aTextToRender)
	{
		context->PSSetShaderResources(0, 1, &IEngine::GetTextManager().GetFont(command.myFontID)->myTexture);

		SQuadBufferData data;
		data.myScreenSize = IEngine::GetCanvasSize();
		data.mySpriteSize = command.mySpriteData.myOriginalTextureSize;

		data.myPosition = command.mySpriteData.myPosition + command.myPosition;
		data.myScale = command.mySpriteData.myScale;
		data.myPivot = command.mySpriteData.myPivot;
		data.myRotation = command.mySpriteData.myRotation;
		data.myUVOffset = command.mySpriteData.myUVOffset;
		data.myUVScale = command.mySpriteData.myUVScale;
		data.myTint = command.mySpriteData.myTint;

		myQuadBuffer.SetData(&data);
		myQuadBuffer.SetBuffer(0, EShaderType_Vertex);

		context->DrawIndexed(myQuad.myNumberOfIndices, 0, 0);
	}

}

bool C2DRenderer::InitBuffers()
{
	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
		float u, v;
	} vertices[4] =
	{
		{-1.f, +1.f, 0.f, 1.f,	0.f, 0.f},
		{+1.f, +1.f, 0.f, 1.f,	1.f, 0.f},
		{-1.f, -1.f, 0.f, 1.f,	0.f, 1.f},
		{+1.f, -1.f, 0.f, 1.f,	1.f, 1.f}
	};
	unsigned int indices[6] =
	{
		0,1,2,
		1,3,2
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = IEngine::GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		return false;
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = IEngine::GetDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		return false;
	}

	//Setup VertexData
	myQuad.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	myQuad.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	myQuad.myStride = sizeof(Vertex);
	myQuad.myOffset = 0;
	myQuad.myVertexBuffer = vertexBuffer;
	myQuad.myIndexBuffer = indexBuffer;

	//Setup constant buffer
	if (!myQuadBuffer.Init(sizeof(SQuadBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create Constant buffer: %s!", "SQuadBufferData");
		return false;
	}

	return true;
}


#pragma once

#include "ConstantBuffer.h"

#include "VertexDataWrapper.h"
#include "Vector.h"
#include "GrowingArray.h"

#include "Sprite.h"

#include "Font.h"

class CVertexShader;
class CPixelShader;
struct SSpriteRenderCommand;
struct STextRenderCommand;

struct ID3D11ShaderResourceView;

class C2DRenderer
{
public:
	C2DRenderer();
	~C2DRenderer();

	bool Init();
	void Render(
		const CommonUtilities::GrowingArray<SSpriteRenderCommand>& aSpritesToRender,
		const CommonUtilities::GrowingArray<STextRenderCommand>& aTextToRender
	);

private:
	bool InitBuffers();
	void RenderText(const CommonUtilities::GrowingArray<STextRenderCommand>& aTextToRender);

	struct SQuadBufferData
	{
		CommonUtilities::Vector2f myScreenSize;
		CommonUtilities::Vector2f mySpriteSize;
		CommonUtilities::Vector2f myPosition;
		CommonUtilities::Vector2f myScale;
		CommonUtilities::Vector2f myPivot;
		CommonUtilities::Vector2f myUVOffset;
		CommonUtilities::Vector2f myUVScale;
		float myRotation;
		float myPadding;
		CommonUtilities::Vector4f myTint;
	};

	SVertexDataWrapper myQuad;

	CConstantBuffer myQuadBuffer;
	CVertexShader* myVertexShader;
	CPixelShader* mySpritePixelShader;
	CPixelShader* myTextPixelShader;

};


#pragma once

#include "Matrix.h"

class CDirectXFramework;
class CCameraComponent;
struct ID3D11Buffer;

class CCamera
{
public:
	CCamera();
	~CCamera();

	bool Init(const CommonUtilities::Matrix44f& aProjectionMatrix, float aProjectionDepth);

	const CommonUtilities::Matrix44f& GetProjection() const { return myProjection; }
	float GetProjectionDepth() const { return myProjectionDepth; }

private:
	CommonUtilities::Matrix44f myProjection;
	float myProjectionDepth;
};


#include "stdafx.h"

#include "CameraComponent.h"
#include "CameraManager.h"
#include "Camera.h"
#include "Matrix.h"
#include <cmath>
#include "IEngine.h"
#include "SceneManager.h"

CCameraManager* CCameraComponent::ourCameraManager = nullptr;

CCameraComponent::CCameraComponent()
	: myCameraID(ID_T_INVALID(CCamera))
{
}

CCameraComponent::~CCameraComponent()
{
}

void CCameraComponent::Init(SCameraComponentData aCameraComponentData)
{
	if (aCameraComponentData.cameraFovInDegrees < 60.f)
	{
		ENGINE_LOG(CONCOL_ERROR, "You're camera fov is very small: %f degrees!", aCameraComponentData.cameraFovInDegrees);
		aCameraComponentData.cameraFovInDegrees = 90.f;
	}
	else if (aCameraComponentData.cameraFovInDegrees >= 180.f)
	{
		ENGINE_LOG(CONCOL_ERROR, "You're camera fov is invalid: %f degrees!", aCameraComponentData.cameraFovInDegrees);
		aCameraComponentData.cameraFovInDegrees = 90.f;
	}

	float B = 1.f / std::tan(aCameraComponentData.cameraFovInDegrees * CommonUtilities::Deg2Rad * 0.5f);
	float A = B / aCameraComponentData.aspectRatio;
	float C = aCameraComponentData.farPlane / (aCameraComponentData.farPlane - aCameraComponentData.nearPlane);
	float D = 1.f;
	float E = -aCameraComponentData.nearPlane * aCameraComponentData.farPlane / (aCameraComponentData.farPlane - aCameraComponentData.nearPlane);

	CommonUtilities::Matrix44<float> projectionMatrix(
		A, 0, 0, 0,
		0, B, 0, 0,
		0, 0, C, D,
		0, 0, E, 0);

	myCameraID = ourCameraManager->AcquireCamera(projectionMatrix, aCameraComponentData.farPlane - aCameraComponentData.nearPlane);
}

void CCameraComponent::SetFov(float aFov)
{
	if (aFov < 60.f)
	{
		ENGINE_LOG(CONCOL_ERROR, "You're camera fov is very small: %f degrees!", aFov);
	}
	else if (aFov >= 180.f)
	{
		ENGINE_LOG(CONCOL_ERROR, "You're camera fov is invalid: %f degrees!", aFov);
	}

	float aspectRatio = IEngine::GetCanvasSize().x / IEngine::GetCanvasSize().y;
	float n = 0.1f;
	float f = 1000.f;

	float B = 1.f / std::tan(aFov * CommonUtilities::Deg2Rad * 0.5f);
	float A = B / aspectRatio;
	float C = f / (f - n);
	float D = 1.f;
	float E = -n * f / (f - n);

	CommonUtilities::Matrix44<float> projectionMatrix(
		A, 0, 0, 0,
		0, B, 0, 0,
		0, 0, C, D,
		0, 0, E, 0);

	ourCameraManager->GetCamera(myCameraID)->Init(projectionMatrix, f-n);
}

void CCameraComponent::SetAsActiveCamera()
{
	IEngine::GetSceneManager().GetSceneAt(mySceneID)->SetActiveCamera(myID);
}

void CCameraComponent::Release()
{
	ourCameraManager->myCameras.Release(myCameraID);
	myCameraID = ID_T_INVALID(CCamera);
}

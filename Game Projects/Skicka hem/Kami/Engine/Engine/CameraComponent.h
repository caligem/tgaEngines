#pragma once

#include "Matrix.h"
#include "Vector.h"
#include "ObjectPool.h"
#include "Component.h"

class CCamera;
class CDirectXFramework;
class CCameraManager;
class CGraphicsPipline;
class CComponentSystem;

class CCameraComponent : public CComponent
{
public:
	CCameraComponent();
	~CCameraComponent();

	struct SCameraComponentData
	{

		SCameraComponentData(float aFovInDegrees, float aAspectRatio, float aNearPlane, float aFarPlane)
		{
			cameraFovInDegrees = aFovInDegrees;
			aspectRatio = aAspectRatio;
			nearPlane = aNearPlane;
			farPlane = aFarPlane;
		}

		float nearPlane;
		float aspectRatio;
		float farPlane;
		float cameraFovInDegrees;
	};

	void Init(SCameraComponentData aCameraComponentData);

	void SetFov(float aFov);
	void SetAsActiveCamera();

private:
	friend CCamera;
	friend CEngine;
	friend CGraphicsPipeline;
	friend CComponentSystem;

	void Release() override;
	static CCameraManager* ourCameraManager;

	ID_T(CCamera) myCameraID;
};


#include "stdafx.h"
#include "CameraManager.h"


CCameraManager::CCameraManager()
	: myCameras(8)
{
}


CCameraManager::~CCameraManager()
{
}

bool CCameraManager::Init()
{
	return true;
}

ID_T(CCamera) CCameraManager::AcquireCamera(const CommonUtilities::Matrix44f& aProjectionMatrix, float aProjectionDepth)
{
	ID_T(CCamera) id = myCameras.Acquire();
	myCameras.GetObj(id)->Init(aProjectionMatrix, aProjectionDepth);
	return id;
}

void CCameraManager::ReleaseCamera(ID_T(CCamera) aCameraID)
{
	myCameras.Release(aCameraID);
}

CCamera * CCameraManager::GetCamera(ID_T(CCamera) aCameraID)
{
	return std::move(myCameras.GetObj(aCameraID));
}

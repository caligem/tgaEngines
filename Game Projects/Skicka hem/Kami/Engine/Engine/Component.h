#pragma once

class CGameObjectData;
class CForwardRenderer;
class CEngine;
class CComponentSystem;
class CGraphicsPipeline;
class CScene;

class CComponent
{
public:
	CComponent();
	~CComponent();

	inline void SetParent(ID_T(CGameObjectData) aParentID, ID_T(CScene) aSceneID, int aID) { myGameObjectDataID = aParentID; mySceneID = aSceneID; myID = aID; }

protected:
	virtual void Release(){}

	ID_T(CGameObjectData) myGameObjectDataID;
	ID_T(CScene) mySceneID;
	int myID;

private:
	friend CForwardRenderer;
	friend CComponentSystem;
	friend CGraphicsPipeline;

};
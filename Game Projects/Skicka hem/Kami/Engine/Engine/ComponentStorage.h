#pragma once
class CComponentSystem;

#include "ObjectPool.h"
#include "GameObjectData.h"
#include "ModelComponent.h"
#include "CameraComponent.h"
#include "AudioSourceComponent.h"
#include "AudioListenerComponent.h"
#include "PointLightComponent.h"
#include "ParticleSystemComponent.h"
#include "SpriteComponent.h"
#include "TextComponent.h"

class CComponentStorage
{
public:
	CComponentStorage();
	~CComponentStorage();

	void PrintStorageInfo();

private:
	friend CComponentSystem;

	ObjectPool<CGameObjectData> myGameObjectData;
	
#include "RedefineComponentRegistrer.h"
#define ComponentRegister(Type, Container, Size) \
ObjectPool<##Type> ##Container;
#include "RegisteredComponents.h"

};
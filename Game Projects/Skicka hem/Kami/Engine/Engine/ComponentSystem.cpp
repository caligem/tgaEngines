#include "stdafx.h"
#include "ComponentSystem.h"
#include "DirectXFramework.h"
#include "Scene.h"
#include "GameObject.h"
#include "ModelManager.h"
#include "SceneManager.h"
#include "InputManager.h"

#define PRINT_STORAGE_INFO

CComponentSystem::CComponentSystem()
	: myComponentStorages(8)
{
}

CComponentSystem::~CComponentSystem()
{
}

bool CComponentSystem::Init()
{
	myGameObjectsToBeDestroyed.Init(512);

	for (int i = 0; i < 8; ++i)
	{
		myComponentStorageConnections[i] = -1;
	}

	return true;
}

void CComponentSystem::UpdateComponents()
{
#ifdef PRINT_STORAGE_INFO
	if (IEngine::GetInputManager().IsKeyPressed(Input::Key_OEM_5))
	{
		PrintStorageInfos();
	}
#endif

	//TODO:_CMS
	CScene* scene = IEngine::GetSceneManager().GetActiveScene();
	if (scene == nullptr) return;
	ID_T(CScene) sceneID = scene->mySceneID;
	UpdateAudioComponents(sceneID);
	UpdateParticleComponents(sceneID);
}

void CComponentSystem::DestroyGameObject(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID)
{
	CGameObjectData* gameObjectData = GetComponentStorageBasedOnSceneID(aSceneID)->myGameObjectData.GetObj(aGameObjectDataID);
	gameObjectData->SetActive(false);

	myGameObjectsToBeDestroyed.Add({ gameObjectData->myID, gameObjectData->mySceneID });
}

void CComponentSystem::PrintStorageInfos()
{
#ifdef PRINT_STORAGE_INFO
	for(int i = 0; i < myComponentStorageConnections.size(); ++i)
	{
		if (myComponentStorageConnections[i].val == -1)continue;
		ENGINE_LOG(CONCOL_DEFAULT, "StorageInfo for SceneID<%d>", i);
		myComponentStorages.GetObj(myComponentStorageConnections[i])->PrintStorageInfo();
	}
#endif
}

void CComponentSystem::DestroyGameObjectsInQueue()
{
#include "RedefineComponentRegistrer.h"
#define ComponentRegister(Type, Container, Size) \
else if (componentType == _UUID(##Type)) \
{ \
	componentStorage->##Container.GetObj(component.componentID)->Release(); \
	componentStorage->##Container.Release(component.componentID); \
	IEngine::GetSceneManager().GetSceneAt(gameObject.sceneID)->RemoveComponentID(_UUID(##Type), component.componentID); \
}

	//TODO:_CMS
	for (const auto& gameObject : myGameObjectsToBeDestroyed)
	{
		CGameObjectData* gameObjectData = GetComponentStorageBasedOnSceneID(gameObject.sceneID)->myGameObjectData.GetObj(gameObject.gameObjectDataID);
		CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(gameObject.sceneID);

		for (const auto& component : gameObjectData->myComponents)
		{
			_UUID_T componentType = component.componentType;

			if(false) { }
				#include "RegisteredComponents.h"
			else
			{
				ENGINE_LOG(CONCOL_ERROR, "ERROR: Trying to release unregistered component type");
			}
		}

		componentStorage->myGameObjectData.Release(gameObject.gameObjectDataID);
	}

	myGameObjectsToBeDestroyed.RemoveAll();
}

void CComponentSystem::UpdateAudioComponents(ID_T(CScene) aSceneID)
{
	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aSceneID);
	for (auto it = componentStorage->myAudioSourceComponents.begin(); it != componentStorage->myAudioSourceComponents.end(); ++it)
	{
		CGameObjectData* gameObjectData = componentStorage->myGameObjectData.GetObj(it->myGameObjectDataID);
		if (gameObjectData->IsActive())
		{
			it->Update();
			it->SetPosition(gameObjectData->GetTransform().GetPosition());
		}
	}

	for (auto it = componentStorage->myAudioListenerComponents.begin(); it != componentStorage->myAudioListenerComponents.end(); ++it)
	{
		CGameObjectData* gameObjectData = componentStorage->myGameObjectData.GetObj(it->myGameObjectDataID);
		if (gameObjectData->myIsActive)
		{
			it->Update();
			it->SetPosition(gameObjectData->GetTransform().GetPosition());
		}
	}
}

void CComponentSystem::UpdateParticleComponents(ID_T(CScene) aSceneID)
{
	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aSceneID);
	for (auto it = componentStorage->myParticleSystemComponents.begin(); it != componentStorage->myParticleSystemComponents.end(); ++it)
	{
		CGameObjectData* gameObjectData = componentStorage->myGameObjectData.GetObj(it->myGameObjectDataID);
		if (gameObjectData->myIsActive)
		{
			it->Update(gameObjectData->GetTransform());
		}
	}
}

void CComponentSystem::AcquireComponentStorage(ID_T(CScene) aSceneID)
{
	ID_T(CComponentStorage) storageID = myComponentStorages.Acquire();
	myComponentStorageConnections[aSceneID.val] = storageID;
}

void CComponentSystem::ReleaseSceneObjectsAndComponentStorage(ID_T(CScene) aSceneID)
{
	CComponentStorage* cStorage = myComponentStorages.GetObj(myComponentStorageConnections[aSceneID.val]);

	if (cStorage == nullptr)
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to release Invalid ComponentStorage!");
		return;
	}

	for (auto& it : cStorage->myGameObjectData)
	{
		DestroyGameObject(it.myID, aSceneID);
	}

	DestroyGameObjectsInQueue();

	myComponentStorages.Release(myComponentStorageConnections[aSceneID.val]);
	myComponentStorageConnections[aSceneID.val] = -1;
}

ID_T(CComponentStorage) CComponentSystem::GetComponentStorageIDBasedOnSceneID(ID_T(CScene) aSceneID)
{
	if (myComponentStorageConnections[aSceneID.val].val != -1)
	{
		ID_T(CComponentStorage) storageID = myComponentStorageConnections[aSceneID.val];
		return storageID;
	}

	ENGINE_LOG(CONCOL_ERROR, "Trying to GetComponentStorageID but cant find connection with the SceneID: %d", aSceneID.val);
	return ID_T_INVALID(CComponentStorage);
}

CComponentStorage* CComponentSystem::GetComponentStorageBasedOnSceneID(ID_T(CScene) aSceneID)
{
	ID_T(CComponentStorage) storageID = GetComponentStorageIDBasedOnSceneID(aSceneID);
	if (storageID == ID_T_INVALID(CComponentStorage))
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to GetComponentStorage but cant find connection with the SceneID: %d", aSceneID.val);
		return nullptr;
	}
	return myComponentStorages.GetObj(storageID);
}

ID_T(CGameObjectData) CComponentSystem::CreateGameObjectData(ID_T(CScene) aSceneID)
{
	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aSceneID);

	ID_T(CGameObjectData) gameObjectData = componentStorage->myGameObjectData.Acquire();
	ID_T(CComponentStorage) storageID = GetComponentStorageIDBasedOnSceneID(aSceneID);

	componentStorage->myGameObjectData.GetObj(gameObjectData)->Init(gameObjectData, aSceneID);
	return gameObjectData;
}

CGameObjectData * CComponentSystem::GetGameObjectData(ID_T(CGameObjectData) aGameObjectDataIDptr, ID_T(CScene) aSceneID)
{
	return GetComponentStorageBasedOnSceneID(aSceneID)->myGameObjectData.GetObj(aGameObjectDataIDptr);
}

#pragma once

class CSceneManager;
class CDirectXFramework;
class GameObjectData;
class CModel;
class CModelManager;
class CGameObject;
class CTransform;
class CScene;
class CGraphicsPipeline;

#include "ObjectPool.h"
#include "ComponentStorage.h"
#include "IEngine.h"
#include "Scene.h"
#include <array>
#include <map>
#include <vector>
#include "SceneManager.h"

class CComponentSystem
{
public:
	CComponentSystem();
	~CComponentSystem();

	bool Init();
	void UpdateComponents();
	void DestroyGameObjectsInQueue();
	void PrintStorageInfos();

private:
	friend CGameObjectData;
	friend CGameObject;
	friend CTransform;
	friend CSceneManager;
	friend CScene;
	friend CGraphicsPipeline;

	struct SDestroyData
	{
		SDestroyData() {};

		SDestroyData(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID)
		{
			gameObjectDataID = aGameObjectDataID;
			sceneID = aSceneID;
		}

		ID_T(CGameObjectData) gameObjectDataID;
		ID_T(CScene) sceneID;
	};

	void UpdateAudioComponents(ID_T(CScene) aSceneID);
	void UpdateParticleComponents(ID_T(CScene) aSceneID);

	void AcquireComponentStorage(ID_T(CScene) aSceneID);
	void ReleaseSceneObjectsAndComponentStorage(ID_T(CScene) aSceneID);

	ID_T(CComponentStorage) GetComponentStorageIDBasedOnSceneID(ID_T(CScene) aSceneID);
	CComponentStorage* GetComponentStorageBasedOnSceneID(ID_T(CScene) aSceneID);

	template<typename T, typename ParameterPack>
	T* AddComponent(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID, ParameterPack aParameterPack);

	template<typename T>
	T* GetComponent(ID_T(CGameObjectData) aGameObjectData, ID_T(CScene) aSceneID);

	template<typename T>
	T* GetComponent(int aComponentID, ID_T(CScene) aSceneID);

	CGameObjectData* GetGameObjectData(ID_T(CGameObjectData) aGameObjectDataIDptr, ID_T(CScene) aSceneID);
	ID_T(CGameObjectData) CreateGameObjectData(ID_T(CScene) aSceneID);


	void DestroyGameObject(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID);

	ObjectPool<CComponentStorage> myComponentStorages;
	std::array<ID_T(CComponentStorage), 8> myComponentStorageConnections;

	CommonUtilities::GrowingArray<SDestroyData> myGameObjectsToBeDestroyed;
};

template<typename T, typename ParameterPack>
inline T* CComponentSystem::AddComponent(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID, ParameterPack aParameterPack)
{
	int componentID = -1;

	CGameObjectData* gameObjectData = GetGameObjectData(aGameObjectDataID, aSceneID);
	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aSceneID);
	CScene* scene = IEngine::GetSceneManager().GetSceneAt(gameObjectData->mySceneID);

	CComponent* component = nullptr;

#include "RedefineComponentRegistrer.h"
#define ComponentRegister(Type, Container, Size) \
else if (_UUID(T) == _UUID(##Type)) \
{ \
	ID_T(##Type) theComponentID = componentStorage->##Container.Acquire(); \
	component = componentStorage->##Container.GetObj(theComponentID); \
	componentID = theComponentID.val; \
	scene->AddComponentID(_UUID(T), componentID); \
}

	//TODO:_CMS
	if (false) { }
	#include "RegisteredComponents.h"
	else
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Trying to add an unregistered component type (%s)!", typeid(T).name());
	}

	T* tPtr = static_cast<T*>(component);
	tPtr->Init(aParameterPack);
	tPtr->SetParent(aGameObjectDataID, aSceneID, componentID);
	GetGameObjectData(aGameObjectDataID, aSceneID)->AddSComponent(ID_T(T)(componentID), _UUID(T));

	return tPtr;
}

template<typename T>
inline T* CComponentSystem::GetComponent(ID_T(CGameObjectData) aGameObjectData, ID_T(CScene) aSceneID)
{
	CComponent* component = nullptr;
	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aSceneID);

	int id = -1;

#include "RedefineComponentRegistrer.h"
#define ComponentRegister(Type, Container, Size) \
else if (_UUID(T) == _UUID(##Type)) \
{ \
	id = componentStorage->myGameObjectData.GetObj(aGameObjectData)->GetComponentID<T>(); \
	ID_T(##Type) componentID = ID_T(##Type)(id); \
	component = componentStorage->##Container.GetObj(componentID); \
}

	//TODO:_CMS
	if (false) { } // ugly hack
	#include "RegisteredComponents.h"
	else
	{
		ENGINE_LOG(CONCOL_WARNING, "type T does not match any Component // CComponentSystem::GetComponent()");
		return nullptr;
	}

	return static_cast<T*>(component);
}

template<typename T>
inline T * CComponentSystem::GetComponent(int aComponentID, ID_T(CScene) aSceneID)
{
	CComponent* component = nullptr;
	if (aSceneID == ID_T_INVALID(CScene))
	{
		return nullptr;
	}
	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aSceneID);

#include "RedefineComponentRegistrer.h"
#define ComponentRegister(Type, Container, Size) \
else if (_UUID(T) == _UUID(##Type)) \
{ \
	component = componentStorage->##Container.GetObj(aComponentID); \
}

	if(false){}
	#include "RegisteredComponents.h"
	else
	{
		ENGINE_LOG(CONCOL_WARNING, "type T does not match any Component // CComponentSystem::GetComponent<%s>()", typeid(T).name());
		return nullptr;
	}

	return static_cast<T*>(component);
}

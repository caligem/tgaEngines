#include "stdafx.h"
#include "ConstantBuffer.h"

#include "IEngine.h"

CConstantBuffer::CConstantBuffer()
{
}

CConstantBuffer::~CConstantBuffer()
{
}

bool CConstantBuffer::Init(int aBufferSize)
{
	myBufferSize = aBufferSize;

	ID3D11Device* device = IEngine::GetDevice();

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = aBufferSize;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	HRESULT result;
	result = device->CreateBuffer(&bufferDesc, nullptr, &myBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create constant buffer with size: %d!", aBufferSize);
		return false;
	}

	return true;
}

void CConstantBuffer::SetData(const void* aDataPtr)
{
	HRESULT result;
	ID3D11DeviceContext* context = IEngine::GetContext();
	D3D11_MAPPED_SUBRESOURCE data;
	ZeroMemory(&data, sizeof(D3D11_MAPPED_SUBRESOURCE));
	result = context->Map(myBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &data);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to Map Constant Buffer");
	}
	memcpy_s(data.pData, myBufferSize, aDataPtr, myBufferSize);
	context->Unmap(myBuffer, 0);
}

void CConstantBuffer::SetBuffer(int aSlot, EShaderType aShaderType)
{
	switch (aShaderType)
	{
	case EShaderType_Vertex:
		IEngine::GetContext()->VSSetConstantBuffers(aSlot, 1, &myBuffer);
		break;
	case EShaderType_Pixel:
		IEngine::GetContext()->PSSetConstantBuffers(aSlot, 1, &myBuffer);
		break;
	case EShaderType_Geometry:
		IEngine::GetContext()->GSSetConstantBuffers(aSlot, 1, &myBuffer);
		break;
	}
}

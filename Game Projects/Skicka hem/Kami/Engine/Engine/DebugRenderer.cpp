#include "stdafx.h"
#ifndef _RETAIL

#include "DebugRenderer.h"

#include <DL_Debug.h>
#include "IEngine.h"

#include "Transform.h"
#include "ShaderManager.h"

#include <d3d11.h>

CDebugRenderer::CDebugRenderer()
{
	myVertexShader = nullptr;
	myPixelShader = nullptr;
	myColor = { 1.f, 0.f, 0.f, 1.f };
}

CDebugRenderer::~CDebugRenderer()
{
}

bool CDebugRenderer::Init()
{
	myLineBuffer.Init();
	myBoxBuffer.Init();
	mySphereBuffer.Init();

	if (!myInstanceBuffer.Init(sizeof(SInstanceBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create InstanceBuffer for DebugRenderer");
		return false;
	}

	if (!InitLineModel())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create CDebugRenderer::myLineModel");
		return false;
	}
	if (!InitBoxModel())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create CDebugRenderer::myBoxModel");
		return false;
	}
	if (!InitSphereModel())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create CDebugRenderer::mySphereModel");
		return false;
	}

	myVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/Debug/Debug.vs", EShaderInputLayoutType_Debug);
	myPixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/Debug/Debug.ps");

	return true;
}

void CDebugRenderer::Render(CConstantBuffer & aCameraBufferData)
{
	aCameraBufferData.SetBuffer(0, EShaderType_Vertex);

	ID3D11DeviceContext* context = IEngine::GetContext();
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	myVertexShader->Bind();
	myVertexShader->BindLayout();
	myPixelShader->Bind();

	for (SLineData& line : myLineBuffer.GetReadBuffer())
	{
		CTransform transform;
		transform.SetPosition(line.mySource);
		transform.LookAt(line.myDestination);
		transform.SetScale({ 1.f, 1.f, (line.myDestination - line.mySource).Length() });

		Render(myLineModel, transform.GetMatrix(), line.myColor);
	}
	for (SBoxData& box : myBoxBuffer.GetReadBuffer())
	{
		CTransform transform;
		transform.SetPosition(box.myPosition);
		transform.SetRotation(box.myRotation);
		transform.SetScale(box.myScale);
		Render(myBoxModel, transform.GetMatrix(), box.myColor);
	}
	for (SSphereData& sphere : mySphereBuffer.GetReadBuffer())
	{
		CTransform transform;
		transform.SetPosition(sphere.myPosition);
		transform.SetRotation(sphere.myRotation);
		transform.SetScale(sphere.myScale);
		Render(mySphereModel, transform.GetMatrix(), sphere.myColor);
	}

	myVertexShader->Unbind();
	myVertexShader->UnbindLayout();
	myPixelShader->Unbind();
}

void CDebugRenderer::Render(SVertexDataWrapper & aModel, const CommonUtilities::Matrix44f & aMatrix, const CommonUtilities::Vector4f& aColor)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	SInstanceBufferData data;
	data.myToWorld = aMatrix;
	data.myColor = aColor;
	myInstanceBuffer.SetData(&data);
	myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);
	myInstanceBuffer.SetBuffer(1, EShaderType_Pixel);

	context->IASetVertexBuffers(0, 1, &aModel.myVertexBuffer, &aModel.myStride, &aModel.myOffset);
	context->IASetIndexBuffer(aModel.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	context->DrawIndexed(aModel.myNumberOfIndices, 0, 0);
}

void CDebugRenderer::SwapBuffers()
{
	myLineBuffer.SwapBuffers();
	myBoxBuffer.SwapBuffers();
	mySphereBuffer.SwapBuffers();
}

void CDebugRenderer::DrawLine(const CommonUtilities::Vector3f & aSource, const CommonUtilities::Vector3f & aDestination)
{
	myLineMutex.lock();
	myLineBuffer.Write({ aSource, aDestination, myColor });
	myLineMutex.unlock();
}

void CDebugRenderer::DrawBox(const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector3f & aScale, const CommonUtilities::Vector3f & aRotation)
{
	myBoxMutex.lock();
	myBoxBuffer.Write({ aPosition, aScale, aRotation, myColor });
	myBoxMutex.unlock();
}

void CDebugRenderer::DrawSphere(const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector3f & aScale, const CommonUtilities::Vector3f & aRotation)
{
	mySphereMutex.lock();
	mySphereBuffer.Write({ aPosition, aScale, aRotation, myColor });
	mySphereMutex.unlock();
}

void CDebugRenderer::SetColor(const CommonUtilities::Vector4f & aColor)
{
	myColor = aColor;
}

bool CDebugRenderer::InitLineModel()
{
	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
	} vertices[2] =
	{
		{0.0f, 0.0f, 0.0f, 1.f},
		{0.0f, 0.0f, 1.0f, 1.f}
	};
	unsigned int indices[2] =
	{
		0,1
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = IEngine::GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		return false;
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = IEngine::GetDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		return false;
	}

	//Setup VertexData
	myLineModel.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	myLineModel.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	myLineModel.myStride = sizeof(Vertex);
	myLineModel.myOffset = 0;
	myLineModel.myVertexBuffer = vertexBuffer;
	myLineModel.myIndexBuffer = indexBuffer;

	return true;
}

bool CDebugRenderer::InitBoxModel()
{
	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
	} vertices[8] =
	{
		{-0.5, 0.5, 0.5, 1.f },
		{0.5, 0.5, 0.5, 1.f  },
		{0.5, 0.5, -0.5, 1.f },
		{-0.5, 0.5, -0.5, 1.f},
		{-0.5, -0.5, 0.5, 1.f},
		{0.5, -0.5, 0.5, 1.f },
		{0.5, -0.5, -0.5, 1.f},
		{-0.5, -0.5, -0.5, 1.f}
	};
	unsigned int indices[24] =
	{
		0,1,
		1,2,
		2,3,
		3,0,

		0,4,
		1,5,
		2,6,
		3,7,

		4,5,
		5,6,
		6,7,
		7,4
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = IEngine::GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		return false;
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = IEngine::GetDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		return false;
	}

	//Setup VertexData
	myBoxModel.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	myBoxModel.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	myBoxModel.myStride = sizeof(Vertex);
	myBoxModel.myOffset = 0;
	myBoxModel.myVertexBuffer = vertexBuffer;
	myBoxModel.myIndexBuffer = indexBuffer;

	return true;
}

bool CDebugRenderer::InitSphereModel()
{
	const int resolution = 24;

	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
	} vertices[resolution * 3];
	unsigned int indices[resolution * 6];

	for (int i = 0; i < resolution; ++i)
	{
		float a = ((CommonUtilities::Pif*2.f) / resolution)*i;
		vertices[i + resolution * 0] = { 0.5f*std::sinf(a), 0.5f*std::cosf(a), 0.f, 1.f };
		vertices[i + resolution * 1] = { 0.f, 0.5f*std::cosf(a), 0.5f*std::sinf(a), 1.f };
		vertices[i + resolution * 2] = { 0.5f*std::cosf(a), 0.f, 0.5f*std::sinf(a), 1.f };

		indices[(i + resolution * 0) * 2 + 0] = (i + 0) % resolution + resolution * 0;
		indices[(i + resolution * 0) * 2 + 1] = (i + 1) % resolution + resolution * 0;
		indices[(i + resolution * 1) * 2 + 0] = (i + 0) % resolution + resolution * 1;
		indices[(i + resolution * 1) * 2 + 1] = (i + 1) % resolution + resolution * 1;
		indices[(i + resolution * 2) * 2 + 0] = (i + 0) % resolution + resolution * 2;
		indices[(i + resolution * 2) * 2 + 1] = (i + 1) % resolution + resolution * 2;
	}

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = IEngine::GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		return false;
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = IEngine::GetDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		return false;
	}

	//Setup VertexData
	mySphereModel.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	mySphereModel.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	mySphereModel.myStride = sizeof(Vertex);
	mySphereModel.myOffset = 0;
	mySphereModel.myVertexBuffer = vertexBuffer;
	mySphereModel.myIndexBuffer = indexBuffer;

	return true;
}

#endif

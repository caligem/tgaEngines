#pragma once

#include "Vector.h"
#include "DoubleBuffer.h"
#include <mutex>

#include "VertexDataWrapper.h"
#include "ConstantBuffer.h"

class CVertexShader;
class CPixelShader;

#ifndef _RETAIL

class CDebugRenderer
{
public:
	CDebugRenderer();
	~CDebugRenderer();

	bool Init();

	void Render(CConstantBuffer & aCameraBufferData);
	void SwapBuffers();

	void DrawLine(
		const CommonUtilities::Vector3f& aSource = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aDestination = { 1.f, 1.f, 1.f }
	);
	void DrawBox(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	void DrawSphere(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	void SetColor(const CommonUtilities::Vector4f & aColor);

private:
	bool InitLineModel();
	bool InitBoxModel();
	bool InitSphereModel();

	void Render(SVertexDataWrapper& aModel, const CommonUtilities::Matrix44f& aMatrix, const CommonUtilities::Vector4f& aColor);

	struct SLineData{
		CommonUtilities::Vector3f mySource;
		CommonUtilities::Vector3f myDestination;
		CommonUtilities::Vector4f myColor;
	};
	struct SBoxData{
		CommonUtilities::Vector3f myPosition;
		CommonUtilities::Vector3f myScale;
		CommonUtilities::Vector3f myRotation;
		CommonUtilities::Vector4f myColor;
	};
	struct SSphereData{
		CommonUtilities::Vector3f myPosition;
		CommonUtilities::Vector3f myScale;
		CommonUtilities::Vector3f myRotation;
		CommonUtilities::Vector4f myColor;
	};
	struct SInstanceBufferData
	{
		CommonUtilities::Matrix44f myToWorld;
		CommonUtilities::Vector4f myColor;
	};

	CVertexShader* myVertexShader;
	CPixelShader* myPixelShader;

	CConstantBuffer myInstanceBuffer;

	CDoubleBuffer<SLineData> myLineBuffer;
	CDoubleBuffer<SBoxData> myBoxBuffer;
	CDoubleBuffer<SSphereData> mySphereBuffer;

	SVertexDataWrapper myLineModel;
	SVertexDataWrapper myBoxModel;
	SVertexDataWrapper mySphereModel;

	std::mutex myLineMutex;
	std::mutex myBoxMutex;
	std::mutex mySphereMutex;

	CommonUtilities::Vector4f myColor;
};

#else

class CDebugRenderer
{
public:
	CDebugRenderer(){}
	~CDebugRenderer() {}

	bool Init() { return true; }

	void Render(CConstantBuffer & ) {}
	void SwapBuffers(){}

	void DrawLine(
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&
	){}
	void DrawBox(
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&
	){}
	void DrawSphere(
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&,
		const CommonUtilities::Vector3f&
	){}
	void SetColor(const CommonUtilities::Vector4f &) {}
};

#endif
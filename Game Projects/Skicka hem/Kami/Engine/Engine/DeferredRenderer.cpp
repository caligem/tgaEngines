#include "stdafx.h"
#include "DeferredRenderer.h"
#include "DirectXFramework.h"
#include "RenderCommand.h"
#include "ModelManager.h"
#include "IEngine.h"
#include "Scene.h"
#include "SceneManager.h"
#include "ShaderManager.h"

#include "Shader.h"

#include <d3d11.h>

#include "TextureBuilder.h"

CDeferredRenderer::CDeferredRenderer()
{
	myDataPassVertexShader = nullptr;
	myDataPassPixelShader = nullptr;
	myLightPassVertexShader = nullptr;
	myLightPassPixelShaders = { nullptr };
}

CDeferredRenderer::~CDeferredRenderer()
{
}

bool CDeferredRenderer::Init()
{
	if (!myInstanceBuffer.Init(sizeof(SInstanceBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myInstanceBuffer");
		return false;
	}

	if (!myDirectionalLightBuffer.Init(sizeof(SLightBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myDirectionalLightBuffer");
		return false;
	}
	if (!myPointLightBuffer.Init(sizeof(SPointLightBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myPointLightBuffer");
		return false;
	}
	if (!myScreenBuffer.Init(sizeof(SScreenData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myScreenBuffer");
		return false;
	}

	CShaderManager& shaderManager = IEngine::GetShaderManager();

	myDataPassVertexShader = &shaderManager.GetVertexShader(L"Assets/Shaders/Deferred/DataPass.vs", EShaderInputLayoutType_PBR);
	myDataPassPixelShader = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/DataPass.ps");

	myLightPassVertexShader = &shaderManager.GetVertexShader(L"Assets/Shaders/Deferred/LightPass.vs", EShaderInputLayoutType_PPFX);
	myLightPassPixelShaders[ELightType_Ambient] = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/AmbientLightPass.ps");
	myLightPassPixelShaders[ELightType_Direct] = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/DirectLightPass.ps");
	myLightPassPixelShaders[ELightType_Point] = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/PointLightPass.ps");

	myCubemap = CTextureBuilder::CreateTextureFromFile("Assets/Cubemaps/cubemap.dds");

	if (!CreateVertexBuffer())
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myVertexBuffer");
		return false;
	}

	return true;
}

void CDeferredRenderer::RenderDataPass(const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender, CConstantBuffer& aCameraBuffer, const CommonUtilities::Vector4f& aCameraPosition, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);

	SInstanceBufferData instanceData;

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	myDataPassVertexShader->Bind();
	myDataPassVertexShader->BindLayout();
	myDataPassPixelShader->Bind();

	unsigned short numModels = aModelsToRender.Size();
	unsigned short numModelsRendered = 0;

	for (const SModelRenderCommand& command : aModelsToRender)
	{
		CModel* model = IEngine::GetModelManager().GetModel(command.myModelToRender);

		if (!model)
		{
			continue;
		}

		if (Cull(command, aFrustum, model->myModelData.myRadius))
		{
			continue;
		}

		float distance = (command.myTransform.GetPosition() - CommonUtilities::Vector3f(aCameraPosition)).Length2();

		const SVertexDataWrapper& vertexData = model->GetVertexData(model->CalculateLodLevel(distance));
		STextureDataWrapper& textureData = model->GetTextureData();
		
		instanceData.myToWorld = command.myTransform.GetMatrix();

		myInstanceBuffer.SetData(&instanceData);

		context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
		context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

		context->PSSetShaderResources(0, sizeof(textureData.myTextures)/sizeof(textureData.myTextures[0]), textureData.myTextures);
		context->PSSetShaderResources(sizeof(textureData.myTextures)/sizeof(textureData.myTextures[0]), 1, &myCubemap);

		context->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);
		++numModelsRendered;
	}

	myDataPassVertexShader->Unbind();
	myDataPassVertexShader->UnbindLayout();
	myDataPassPixelShader->Unbind();

	numModels;
	//ENGINE_LOG(CONCOL_DEFAULT, "Culling: %d / %d", numModelsRendered, numModels);
}

void CDeferredRenderer::RenderLightPass(CConstantBuffer aCameraBuffer, const CommonUtilities::GrowingArray<SLightRenderCommand>& aLightRenderCommands, CFullscreenTexture& aGBuffer)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	aCameraBuffer.SetBuffer(0, EShaderType_Pixel);
	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);

	SScreenData screenData;
	screenData.myScreenSize = IEngine::GetCanvasSize();
	myScreenBuffer.SetData(&screenData);
	myScreenBuffer.SetBuffer(1, EShaderType_Pixel);

	context->IASetVertexBuffers(0, 1, &myVertexData.myVertexBuffer, &myVertexData.myStride, &myVertexData.myOffset);
	context->IASetIndexBuffer(myVertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	aGBuffer.SetAsResourceOnSlot(0);
	context->PSSetShaderResources(aGBuffer.GetNumTargets(), 1, &myCubemap);

	SInstanceBufferData instanceData;
	instanceData.myToWorld = CommonUtilities::Matrix44f();
	myInstanceBuffer.SetData(&instanceData);
	myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

	myLightPassVertexShader->Bind();
	myLightPassVertexShader->BindLayout();
	myLightPassPixelShaders[ELightType_Ambient]->Bind();
	RenderAmbientLight();

	myLightPassPixelShaders[ELightType_Direct]->Bind();
	RenderDirectLights();

	myLightPassPixelShaders[ELightType_Point]->Bind();
	RenderPointLights(aLightRenderCommands);

	myLightPassVertexShader->Unbind();
	myLightPassVertexShader->UnbindLayout();
	myLightPassPixelShaders[ELightType_Ambient]->Unbind();
}

void CDeferredRenderer::SetCubemapTexture(const char * aCubemapTexture)
{
	myCubemap->Release();
	myCubemap = CTextureBuilder::CreateTextureFromFile(aCubemapTexture);
}

bool CDeferredRenderer::CreateVertexBuffer()
{
	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
		float u, v;
	} vertices[4] =
	{
		{-1.f, +1.f, 0.f, 1.f,	0.f, 0.f},
		{+1.f, +1.f, 0.f, 1.f,	1.f, 0.f},
		{-1.f, -1.f, 0.f, 1.f,	0.f, 1.f},
		{+1.f, -1.f, 0.f, 1.f,	1.f, 1.f}
	};
	unsigned int indices[6] =
	{
		0,1,2,
		1,3,2
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = IEngine::GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		return false;
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = IEngine::GetDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		return false;
	}

	//Setup VertexData
	myVertexData.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	myVertexData.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	myVertexData.myStride = sizeof(Vertex);
	myVertexData.myOffset = 0;
	myVertexData.myVertexBuffer = vertexBuffer;
	myVertexData.myIndexBuffer = indexBuffer;

	return true;
}

void CDeferredRenderer::RenderAmbientLight()
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	context->DrawIndexed(myVertexData.myNumberOfIndices, 0, 0);
}

void CDeferredRenderer::RenderDirectLights()
{
	SLightBufferData lightData;

	CScene* activeScene = IEngine::GetSceneManager().GetActiveScene();
	if (!activeScene)
	{
		return;
	}

	lightData.myDirectionalLight = activeScene->GetDirectionalLight().GetNormalized();
	lightData.myDirectionalLightColor = { 1.0f, 0.9f, 0.8f, 1.f };
	myDirectionalLightBuffer.SetData(&lightData);
	myDirectionalLightBuffer.SetBuffer(2, EShaderType_Pixel);

	IEngine::GetContext()->DrawIndexed(myVertexData.myNumberOfIndices, 0, 0);
}

void CDeferredRenderer::RenderPointLights(const CommonUtilities::GrowingArray<SLightRenderCommand>& aLightRenderCommands)
{
	SPointLightBufferData pointLightData;
	for (const SLightRenderCommand& lightRenderCommand : aLightRenderCommands)
	{
		pointLightData.color = lightRenderCommand.color;
		pointLightData.position = lightRenderCommand.position;
		pointLightData.range = lightRenderCommand.range;

		myPointLightBuffer.SetData(&pointLightData);
		myPointLightBuffer.SetBuffer(2, EShaderType_Pixel);

		IEngine::GetContext()->DrawIndexed(myVertexData.myNumberOfIndices, 0, 0);
	}
}

bool CDeferredRenderer::Cull(const SModelRenderCommand& aCommand, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum, const float& aRadius)
{
	const CommonUtilities::Vector3f point = aCommand.myTransform.GetPosition();
	float radius = aRadius * aCommand.myTransform.GetScale().Length();

	for (unsigned short i = 0; i < 6; ++i)
	{
		const auto& plane = aFrustum[i];
		if (plane.Distance(point) > radius)
		{
			return true;
		}
	}

	return false;
}

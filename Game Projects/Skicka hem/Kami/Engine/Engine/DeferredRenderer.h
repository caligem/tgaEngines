#pragma once

#include <array>

#include "GrowingArray.h"
#include "Matrix.h"
#include "Vector.h"
#include "Plane.h"

#include "ConstantBuffer.h"

#include "Model.h"

#include "VertexDataWrapper.h"


class CDirectXFramework;
class CVertexShader;
class CPixelShader;
class CFullscreenTexture;

struct SModelRenderCommand;
struct ID3D11Buffer;
struct ID3D11ShaderResourceView;
struct SLightRenderCommand;

class CDeferredRenderer
{
public:
	CDeferredRenderer();
	~CDeferredRenderer();

	bool Init();
	void RenderDataPass(const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender, CConstantBuffer& aCameraBuffer, const CommonUtilities::Vector4f& aCameraPosition, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum);
	void RenderLightPass(CConstantBuffer aCameraBuffer, const CommonUtilities::GrowingArray<SLightRenderCommand>& aLightRenderCommands, CFullscreenTexture& aGBuffer);

	void SetCubemapTexture(const char* aCubemapTexture);

private:
	struct SInstanceBufferData
	{
		CommonUtilities::Matrix44f myToWorld;
	};
	struct SLightBufferData
	{
		CommonUtilities::Vector4f myDirectionalLight;
		CommonUtilities::Vector4f myDirectionalLightColor;
	};
	struct SPointLightBufferData
	{
		CommonUtilities::Vector4f position;
		CommonUtilities::Vector3f color;
		float range;
	};
	struct SScreenData
	{
		CommonUtilities::Vector2f myScreenSize;
		CommonUtilities::Vector2f myTrash;
	};

	bool CreateVertexBuffer();

	void RenderAmbientLight();
	void RenderDirectLights();
	void RenderPointLights(const CommonUtilities::GrowingArray<SLightRenderCommand>& aLightRenderCommands);
	
	CConstantBuffer myPointLightBuffer;
	CConstantBuffer myInstanceBuffer;
	CConstantBuffer myDirectionalLightBuffer;
	CConstantBuffer myScreenBuffer;
	ID3D11ShaderResourceView* myCubemap;

	CVertexShader* myDataPassVertexShader;
	CPixelShader* myDataPassPixelShader;

	enum ELightType
	{
		ELightType_Ambient,
		ELightType_Direct,
		ELightType_Point,
		ELightType_Count
	};
	CVertexShader* myLightPassVertexShader;
	std::array<CPixelShader*, ELightType_Count> myLightPassPixelShaders;

	SVertexDataWrapper myVertexData;
	bool Cull(const SModelRenderCommand& aCommand, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum, const float& aRadius);
};


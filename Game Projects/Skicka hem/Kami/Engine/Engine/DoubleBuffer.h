#pragma once

#include <GrowingArray.h>

template<typename T>
class CDoubleBuffer
{
public:
	CDoubleBuffer();
	~CDoubleBuffer();

	inline void Init();

	inline CommonUtilities::GrowingArray<T>& GetReadBuffer() { return *myReadBuffer; }
	inline const CommonUtilities::GrowingArray<T>& GetReadBuffer() const { return *myReadBuffer; }

	inline void Write(const T& aObject) { myWriteBuffer->Add(aObject); }

	inline void SwapBuffers();

private:
	CommonUtilities::GrowingArray<T> myBuffer1;
	CommonUtilities::GrowingArray<T> myBuffer2;

	CommonUtilities::GrowingArray<T>* myWriteBuffer;
	CommonUtilities::GrowingArray<T>* myReadBuffer;
};

template<typename T>
inline CDoubleBuffer<T>::CDoubleBuffer()
{
	myWriteBuffer = nullptr;
	myReadBuffer = nullptr;
}

template<typename T>
inline CDoubleBuffer<T>::~CDoubleBuffer()
{
}

template<typename T>
inline void CDoubleBuffer<T>::Init()
{
	myBuffer1.Init(4);
	myBuffer2.Init(4);
	myWriteBuffer = &myBuffer1;
	myReadBuffer = &myBuffer2;
}

template<typename T>
inline void CDoubleBuffer<T>::SwapBuffers()
{
	if (myReadBuffer == &myBuffer1)
	{
		myReadBuffer = &myBuffer2;
		myWriteBuffer = &myBuffer1;
	}
	else
	{
		myReadBuffer = &myBuffer1;
		myWriteBuffer = &myBuffer2;
	}
	myWriteBuffer->RemoveAll();
}

#include "stdafx.h"
#include "Engine.h"

#include "IEngine.h"

#include "ComponentSystem.h"
#include "GameObject.h"

#include <DirectXMath.h>

#include "SystemStats.h"

#include "Random.h"

#define MULTITHREADED

CEngine::CEngine()
	: myXBoxInput(0)
	, myXBoxInput2(1)
{
}

CEngine::~CEngine()
{
	myWorkerPool.Destroy();
	myReportManager.Shutdown();
}

bool CEngine::Init(const SCreateParameters& aCreateParameters)
{
	CommonUtilities::InitRand();

	ENGINE_LOG(CONCOL_VALID, "Starting the engine...");
	myCreateParameters = aCreateParameters;

	IEngine::ourEngine = this;

	if (!myWindowHandler.Init(&aCreateParameters))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise WindowHandler!");
		return false;
	}

	if (!myFramework.Init(myWindowHandler, &aCreateParameters))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise DirectXFramework!");
		return false;
	}

	//Managers Init
	if (!mySceneManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise SceneManager!");
		return false;
	}
	if (!myModelManager.Init(myFramework))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise ModelManager.");
		return false;
	}
	if (!myCameraManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise CameraManager.");
		return false;
	}
	if (!myLightManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise LightManager.");
		return false;
	}
	if (!myParticleManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise ParticleManager.");
		return false;
	}
	if (!mySpriteManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise SpriteManager.");
		return false;
	}
	if (!myTextManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise SpriteManager.");
		return false;
	}
	if (!myComponentSystem.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise Component System.");
		return false;
	}

	// Graphics
	if (!myGraphicsPipeline.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise GraphicsPipeline.");
		return false;
	}

	
	ManagerInjection();

#ifndef MULTITHREADED
	myWorkerPool.Init(1);
#else
	unsigned int maxThreads = std::thread::hardware_concurrency();
	if (maxThreads == 0)
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: No cores found!");
		return false;
	}
	else
	{
		ENGINE_LOG(CONCOL_VALID, "Created WorkerPool using: %d threads!", maxThreads);
	}
	myWorkerPool.Init(maxThreads);
#endif

	if (!myReportManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise ReportManager.");
		return false;
	};

	myShouldTakeScreenshot = false;
	myScreenshotPath = L"";

	return true;
}

void CEngine::Shutdown()
{
	myIsRunning = false;
	myWindowHandler.CloseWindow();
}

void CEngine::StartEngine()
{
	if (myCreateParameters.myInitFunctionToCall)
	{
		myCreateParameters.myInitFunctionToCall();
	}

	myUpdateFunctionToCall = myCreateParameters.myUpdateFunctionToCall;

	myIsRunning = true;

	if (myCreateParameters.myUpdateFunctionToCall)
	{
		Run();
	}
}

void CEngine::Run()
{
	MSG windowsMessage = { 0 };

	while (myIsRunning)
	{
		while (PeekMessage(&windowsMessage, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&windowsMessage);
			DispatchMessage(&windowsMessage);

			if (windowsMessage.message == WM_QUIT)
			{
				myIsRunning = false;
			}
		}

		if (myShouldTakeScreenshot)
		{
			myFramework.SaveScreenShot(myScreenshotPath);
			myShouldTakeScreenshot = false;
		}

		myReportManager.Update();
		myWindowHandler.Update();

		myTimer.Update();

		myWorkerPool.DoWork(std::bind(&CEngine::MainJob, this));
		myWorkerPool.DoWork(std::bind(&CEngine::RenderJob, this));

		myWorkerPool.Wait();

		myWorkerPool.DoWork(std::bind(&CEngine::SyncJob, this));

		myWorkerPool.Wait();

		myInputManager.Update();
		myXBoxInput.Update();
		myXBoxInput2.Update();
	}
}

void CEngine::MainJob()
{
	myComponentSystem.DestroyGameObjectsInQueue();
	myComponentSystem.UpdateComponents();

	myUpdateFunctionToCall();

	myGraphicsPipeline.SetRenderBuffers();
}

void CEngine::RenderJob()
{
	myGraphicsPipeline.BeginFrame();
	myReportManager.NewFrame();
	myGraphicsPipeline.Render();
	myReportManager.Render();
	myFramework.Present();
}

void CEngine::SaveScreenShot(const std::wstring & aSavePath)
{
	myShouldTakeScreenshot = true;
	myScreenshotPath = aSavePath;
}

void CEngine::SyncJob()
{
	myGraphicsPipeline.SwapBuffers();
	myFileWatcher.FlushChanges();

	mySceneManager.DestroyScenesInQueue();

	myGraphicsPipeline.SetCameraBuffer();
}

void CEngine::ManagerInjection()
{
	CModelComponent::ourModelManager = &myModelManager;
	CCameraComponent::ourCameraManager = &myCameraManager;
	CPointLightComponent::ourLightLoader = &myLightManager;
	CParticleSystemComponent::ourParticleManager = &myParticleManager;
	CSpriteComponent::ourSpriteManager = &mySpriteManager;
	CTextComponent::ourTextManager = &myTextManager;
	CGameObject::ourComponentSystem = &myComponentSystem;
	CTransform::ourComponentSystem = &myComponentSystem;
}


#pragma once

#include "DirectXFramework.h"

#include "GraphicsPipeline.h"

#include <GrowingArray.h>
#include "ComponentSystem.h"
#include "ModelManager.h"
#include "SceneManager.h"
#include "CameraManager.h"
#include "LightManager.h"
#include "ShaderManager.h"
#include "ParticleManager.h"
#include "SpriteManager.h"
#include "TextManager.h"

#include "FileWatcher.h"
#include "WorkerPool.h"

#include "Timer.h"
#include "InputManager.h"
#include "XBOXController.h"

#include "ReportManager.h"

#include <thread>
#include <atomic>

class IEngine;

using callback_function = std::function<void()>;
using callback_function_update = std::function<void()>;
using callback_function_wndProc = std::function<LRESULT(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam)>;

struct SCreateParameters
{
	callback_function myInitFunctionToCall;
	callback_function_update myUpdateFunctionToCall;
	callback_function_wndProc myWndProcCallback;

	unsigned short myWindowWidth = 800;
	unsigned short myWindowHeight = 600;
	unsigned short myCanvasWidth = 800;
	unsigned short myCanvasHeight = 600;

	bool myEnableVSync = true;
	bool myIsFullscreen = false;
	bool myIsBorderless = false;

	std::wstring myGameName = L"NO NAME GAME!";

	HWND myHWND = NULL;
};

class CEngine
{
public:
	CEngine();
	~CEngine();

	virtual bool Init(const SCreateParameters& aCreateParameters);
	void Shutdown();
	void StartEngine();

	const bool IsRunning() const { return myIsRunning; }

	void SaveScreenShot(const std::wstring& aSavePath);

protected:
	virtual void SyncJob();
	virtual void MainJob();
	virtual void RenderJob();
	CWindowHandler myWindowHandler;

	CParticleManager myParticleManager;
	CGraphicsPipeline myGraphicsPipeline;

protected:
	friend IEngine;
	void Run();

	void ManagerInjection();

	bool myIsRunning = false;

	bool myShouldTakeScreenshot;
	std::wstring myScreenshotPath;

	CDirectXFramework myFramework;

	CComponentSystem myComponentSystem;
	CModelManager myModelManager;
	CCameraManager myCameraManager;
	CSceneManager mySceneManager;
	CSpriteManager mySpriteManager;
	CTextManager myTextManager;

	CLightManager myLightManager;
	SCreateParameters myCreateParameters;

	callback_function myInitFunctionToCall;
	callback_function_update myUpdateFunctionToCall;

	CFileWatcher myFileWatcher;
	CShaderManager myShaderManager;

	CWorkerPool myWorkerPool;

	CReportManager myReportManager;

	CommonUtilities::Timer myTimer;
	Input::CInputManager myInputManager;
	CommonUtilities::XBOXController myXBoxInput;
	CommonUtilities::XBOXController myXBoxInput2;
};


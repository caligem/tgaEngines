#include "stdafx.h"
#ifndef _RETAIL

#include "FileWatcher.h"

#include <Windows.h>

CFileWatcher::CFileWatcher()
{
	myShouldEndThread = false;
	myThreadIsDone = true;
	myThread = std::thread(std::bind(&CFileWatcher::ThreadLoop, this));
}

CFileWatcher::~CFileWatcher()
{
	myShouldEndThread = true;
	while (!myThreadIsDone) { }

	if (myThread.joinable())
	{
		myThread.join();
	}
}

int CFileWatcher::WatchFile(const std::wstring & aFile, callback_function_file aCallback)
{
	myMutex.lock();
	if (myCallbacks.find(aFile) == myCallbacks.end())
	{
		myCallbacks[aFile] = std::vector<callback_function_file>();
	}
	myCallbacks[aFile].push_back(aCallback);
	myFileTimes[aFile] = GetFileWriteTime(aFile);
	myMutex.unlock();

	return static_cast<int>(myCallbacks[aFile].size() - 1);
}

void CFileWatcher::StopWatchingFile(const std::wstring& aFile, int aIndex)
{
	if (myCallbacks.find(aFile) == myCallbacks.end())
	{
		return;
	}
	myCallbacks[aFile][aIndex] = nullptr;
}

void CFileWatcher::FlushChanges()
{
	myMutex.lock();

	for (auto file : myChangedFiles)
	{
		if (IsValid(file))
		{
			for (callback_function_file& callback : myCallbacks[file])
			{
				if (callback)
				{
					callback(file);
				}
			}
		}
	}

	myChangedFiles.clear();

	myMutex.unlock();
}

void CFileWatcher::ThreadLoop()
{
	myShouldEndThread = false;
	while (!myShouldEndThread)
	{
		myThreadIsDone = false;
		myMutex.lock();

		for (auto file : myCallbacks)
		{
			CompareFileTimes(file.first);
		}

		myMutex.unlock();
		myThreadIsDone = true;
		Sleep(1000);
	}
}

void CFileWatcher::CompareFileTimes(const std::wstring& aFile)
{
	__int64 newTime = GetFileWriteTime(aFile);

	if (myFileTimes[aFile] != newTime)
	{
		myFileTimes[aFile] = newTime;
		myChangedFiles.push_back(aFile);
	}
}

__int64 CFileWatcher::GetFileWriteTime(const std::wstring & aFile)
{
	WIN32_FILE_ATTRIBUTE_DATA attr;
	FILETIME creation;
	GetFileAttributesEx(aFile.c_str(), GetFileExInfoStandard, &attr);
	FileTimeToLocalFileTime(&attr.ftLastWriteTime, &creation);

	ULARGE_INTEGER fileTime;
	fileTime.LowPart = creation.dwLowDateTime;
	fileTime.HighPart = creation.dwHighDateTime;

	return fileTime.QuadPart;
}

bool CFileWatcher::IsValid(const std::wstring& aFile)
{
	return !(INVALID_FILE_ATTRIBUTES == GetFileAttributesW(aFile.c_str()) && GetLastError() == ERROR_FILE_NOT_FOUND);
}

#endif

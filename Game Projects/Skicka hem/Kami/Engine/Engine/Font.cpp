#include "stdafx.h"
#include "Font.h"

#include <string>
#include <fstream>
#include <iostream>

#include <DL_Debug.h>

#include "TextureBuilder.h"

CFont::CFont()
{
}

CFont::~CFont()
{
}

bool CFont::Init(const std::string& aFilepath)
{
	if (!InitTextData(aFilepath))
	{
		RESOURCE_LOG(CONCOL_ERROR, "Failed to load font %s", aFilepath.c_str());
		return false;
	}

	myTexture = CTextureBuilder::CreateTextureFromFile(aFilepath + ".dds");

	return true;
}

bool CFont::InitTextData(const std::string & aFilepath)
{
	std::ifstream file(aFilepath + ".fnt");

	if (file.fail() || file.bad())
	{
		RESOURCE_LOG(CONCOL_ERROR, "ERROR: Failed to load font file: %s", (aFilepath + ".fnt").c_str());
		return false;
	}

	std::string trash;

	std::string buffer;
	std::string separator("=");

	std::getline(file, trash);
	file >> trash; // Throw away trash

	file >> buffer;
	myLineHeight = static_cast<float>(std::atoi(buffer.substr(buffer.find(separator) + 1).c_str()));
	file >> buffer;
	myBaseLine = static_cast<float>(std::atoi(buffer.substr(buffer.find(separator) + 1).c_str()));

	file >> buffer;
	float textureWidth = static_cast<float>(std::atof(buffer.substr(buffer.find(separator) + 1).c_str()));
	file >> buffer;
	float textureHeight = static_cast<float>(std::atof(buffer.substr(buffer.find(separator) + 1).c_str()));

	myOriginalTextureSize = { textureWidth, textureHeight };

	std::getline(file, trash);
	std::getline(file, trash);

	file >> trash >> buffer;

	int numChars = std::atoi(buffer.substr(buffer.find(separator) + 1).c_str());

	for (int i = 0; i <= numChars; ++i)
	{
		std::getline(file, trash);

		//Example Line
		//char id=32 x=0 y=0 width=0 height=0 xoffset=1 yoffset=8 xadvance=23 page=0 chnl=15

		file >> trash >> buffer;
		char letter = static_cast<char>(std::atoi(buffer.substr(buffer.find(separator) + 1).c_str()));

		file >> buffer;
		int x = std::atoi(buffer.substr(buffer.find(separator) + 1).c_str());
		file >> buffer;
		int y = std::atoi(buffer.substr(buffer.find(separator) + 1).c_str());
		file >> buffer;
		int w = std::atoi(buffer.substr(buffer.find(separator) + 1).c_str());
		file >> buffer;
		int h = std::atoi(buffer.substr(buffer.find(separator) + 1).c_str());
		file >> buffer;
		float xo = static_cast<float>(std::atof(buffer.substr(buffer.find(separator) + 1).c_str())); // xOffset
		file >> buffer;
		float yo = static_cast<float>(std::atof(buffer.substr(buffer.find(separator) + 1).c_str())); // yOffset
		file >> buffer;
		float xa = static_cast<float>(std::atof(buffer.substr(buffer.find(separator) + 1).c_str())); // xAdvance

		float u0 = static_cast<float>(x) / myOriginalTextureSize.x;
		float v0 = static_cast<float>(y) / myOriginalTextureSize.y;

		float u1 = static_cast<float>(x + w) / myOriginalTextureSize.x;
		float v1 = static_cast<float>(y + h) / myOriginalTextureSize.y;

		SCharData data;
		data.myUVOffset = { u0, v0 };
		data.myUVScale = { u1 - u0, v1 - v0 };
		data.myOffset = { xo, yo };
		data.myXAdvance = xa;

		myMap[letter] = data;
	}

	return true;
}

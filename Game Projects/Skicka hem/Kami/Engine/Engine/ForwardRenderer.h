#pragma once

#include "GrowingArray.h"
#include "Matrix.h"
#include "Vector.h"

#include "ConstantBuffer.h"

class CDirectXFramework;
class CVertexShader;
class CPixelShader;

struct SModelRenderCommand;
struct ID3D11Buffer;
struct ID3D11ShaderResourceView;
struct SLightRenderCommand;

class CForwardRenderer
{
public:
	CForwardRenderer();
	~CForwardRenderer();

	bool Init();
	void Render(const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender, CConstantBuffer& aCameraBuffer, const CommonUtilities::GrowingArray<SLightRenderCommand>& aLightRenderCommands);

	void SetCubemapTexture(const char* aCubemapTexturePath);

private:

	struct SInstanceBufferData
	{
		CommonUtilities::Matrix44f myToWorld;
	};

	struct SLightBufferData
	{
		CommonUtilities::Vector4f myDirectionalLight;
		CommonUtilities::Vector4f myDirectionalLightColor;
	};

	struct SPointLightBufferData
	{
		unsigned int numberOfUsedPointLights;
		unsigned int padding[3];
		struct
		{
			CommonUtilities::Vector4f position;
			CommonUtilities::Vector3f color;
			float range;
		} pointLights[8];
	};
	
	CConstantBuffer myPointLightBuffer;
	CConstantBuffer myInstanceBuffer;
	CConstantBuffer myDirectionalLightBuffer;
	ID3D11ShaderResourceView* myCubemap;

	CVertexShader* myVertexShader;
	CPixelShader* myPixelShader;
};


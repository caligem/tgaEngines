#include "stdafx.h"
#include "FullscreenRenderer.h"

#include "IEngine.h"
#include "FullscreenTexture.h"
#include "ShaderManager.h"

CFullscreenRenderer::CFullscreenRenderer()
{
	myVertexShader = nullptr;
}

CFullscreenRenderer::~CFullscreenRenderer()
{
}

bool CFullscreenRenderer::Init()
{
	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
		float u, v;
	} vertices[4] =
	{
		{-1.f, +1.f, 0.f, 1.f,	0.f, 0.f},
		{+1.f, +1.f, 0.f, 1.f,	1.f, 0.f},
		{-1.f, -1.f, 0.f, 1.f,	0.f, 1.f},
		{+1.f, -1.f, 0.f, 1.f,	1.f, 1.f}
	};
	unsigned int indices[6] =
	{
		0,1,2,
		1,3,2
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = IEngine::GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		return false;
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = IEngine::GetDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		return false;
	}

	//Load shaders
	myVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/PPFX/PPFX.vs", EShaderInputLayoutType_PPFX);

	myPixelShaders[EEffect_Copy] = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/PPFX/Copy.ps");
	myPixelShaders[EEffect_BloomLuminance] = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/PPFX/BloomLuminance.ps");
	myPixelShaders[EEffect_GaussianBlurHorizontal] = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/PPFX/GaussianBlurHorizontal.ps");
	myPixelShaders[EEffect_GaussianBlurVertical] = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/PPFX/GaussianBlurVertical.ps");
	myPixelShaders[EEffect_BloomAdd] = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/PPFX/BloomAdd.ps");
	myPixelShaders[EEffect_FXAA] = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/PPFX/FXAA.ps");
	myPixelShaders[EEffect_ColorGrading] = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/PPFX/ColorGrading.ps");
	myPixelShaders[EEffect_SSAO] = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/PPFX/SSAO.ps");
	myPixelShaders[EEffect_Fog] = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/PPFX/Fog.ps");
	myPixelShaders[EEffect_Fade] = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/PPFX/Fade.ps");

	//Setup VertexData
	myVertexData.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	myVertexData.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	myVertexData.myStride = sizeof(Vertex);
	myVertexData.myOffset = 0;
	myVertexData.myVertexBuffer = vertexBuffer;
	myVertexData.myIndexBuffer = indexBuffer;

	//Setup constant buffer
	if (!myFullscreenTextureBuffer.Init(sizeof(SFullscreenTextureBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create Constant buffer: %s!", "FullscreenTextureBufferData");
		return false;
	}

	return true;
}

void CFullscreenRenderer::Render(EEffect aEffect, std::array<CFullscreenTexture*, 2> aFullscreenTextures)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	for (unsigned int i = 0; i < aFullscreenTextures.size(); ++i)
	{
		ID3D11ShaderResourceView* nullResource = NULL;
		if (aFullscreenTextures[i] == nullptr)
		{
			context->PSSetShaderResources(i, 1, &nullResource);
			continue;
		}

		aFullscreenTextures[i]->SetAsResourceOnSlot(i);
	}

	if (aFullscreenTextures[0] != nullptr)
	{
		SFullscreenTextureBufferData data;
		data.myTexelSize = {
			1.f / aFullscreenTextures[0]->GetSize().x,
			1.f / aFullscreenTextures[0]->GetSize().y
		};
		myFullscreenTextureBuffer.SetData(&data);
		myFullscreenTextureBuffer.SetBuffer(1, EShaderType_Pixel);
	}

	Render(aEffect);
}

void CFullscreenRenderer::Render(EEffect aEffect)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetVertexBuffers(0, 1, &myVertexData.myVertexBuffer, &myVertexData.myStride, &myVertexData.myOffset);
	context->IASetIndexBuffer(myVertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	myVertexShader->Bind();
	myVertexShader->BindLayout();
	myPixelShaders[aEffect]->Bind();

	context->DrawIndexed(myVertexData.myNumberOfIndices, 0, 0);

	myVertexShader->Unbind();
	myVertexShader->UnbindLayout();
	myPixelShaders[aEffect]->Unbind();
}

void CFullscreenRenderer::SetResources(ID3D11ShaderResourceView** aShaderResourceViews, unsigned int aCount, unsigned int aSlot)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	context->PSSetShaderResources(aSlot, aCount, aShaderResourceViews);
}

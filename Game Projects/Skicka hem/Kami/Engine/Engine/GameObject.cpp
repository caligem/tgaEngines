#include "stdafx.h"
#include "GameObject.h"

#include "SceneManager.h"

CComponentSystem* CGameObject::ourComponentSystem = nullptr;

CGameObject::CGameObject()
	: myIsInitialized(false)
{
}
CGameObject::~CGameObject()
{

}

void CGameObject::Init(ID_T(CScene) aSceneID)
{
	mySceneID = aSceneID;
	myGameObjectDataID = ourComponentSystem->CreateGameObjectData(aSceneID);
	myIsInitialized = true;

	AssignDebugPtr(CGameObjectData, ourComponentSystem->GetGameObjectData(myGameObjectDataID, aSceneID));
	AssignDebugPtr(CScene, IEngine::GetSceneManager().GetSceneAt(aSceneID));
}

void CGameObject::PointToObject(const CGameObject & aGameObject)
{
	myGameObjectDataID = aGameObject.myGameObjectDataID;
	mySceneID = aGameObject.mySceneID;
	myIsInitialized = aGameObject.myIsInitialized;
}

void CGameObject::Move(const CommonUtilities::Vector3f& aMovement)
{
	if (!IsInitializedCheck())
	{
		return;
	}
	GetTransform().Move(aMovement);
}

void CGameObject::Rotate(const CommonUtilities::Vector3f& aRotation)
{
	if (!IsInitializedCheck())
	{
		return;
	}
	GetTransform().Rotate(aRotation);
}

void CGameObject::SetPosition(const CommonUtilities::Vector3f& aPosition)
{
	if (!IsInitializedCheck())
	{
		return;
	}
	GetTransform().SetPosition(aPosition);
}

const CommonUtilities::Vector3f CGameObject::GetForward()
{
	return (CommonUtilities::Vector3f(0.f, 0.f, 1.f) * CommonUtilities::Matrix33f(GetTransform().GetMatrix())).GetNormalized();
}

const CommonUtilities::Vector3f CGameObject::GetRight()
{
	return (CommonUtilities::Vector3f(1.f, 0.f, 0.f) * CommonUtilities::Matrix33f(GetTransform().GetMatrix())).GetNormalized();
}

const CommonUtilities::Vector3f CGameObject::GetUp()
{
	return (CommonUtilities::Vector3f(0.f, 1.f, 0.f) * CommonUtilities::Matrix33f(GetTransform().GetMatrix())).GetNormalized();
}

void CGameObject::SetActive(bool aIsActive)
{
	ourComponentSystem->GetGameObjectData(myGameObjectDataID, mySceneID)->SetActive(aIsActive);
}

const bool CGameObject::IsActive() const
{
	return ourComponentSystem->GetGameObjectData(myGameObjectDataID, mySceneID)->IsActive();
}


void CGameObject::Destroy()
{
	if (!IsInitializedCheck())
	{
		return;
	}
	GetTransform().Destroy();

	myIsInitialized = false;
}

bool CGameObject::IsInitializedCheck()
{
	if (!myIsInitialized)
	{
		ENGINE_LOG(CONCOL_WARNING, "Trying to do Operations on a UnInitialized GameObject");
	}

	return myIsInitialized;
}

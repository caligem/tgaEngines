#pragma once
#include "Vector.h"
#include "ObjectPool.h"
#include "Transform.h"
#include "ComponentSystem.h"

#include <Macros.h>

class CScene;
class CEngine;

class CGameObject 
{
public:
	CGameObject();
	~CGameObject();

	virtual void Init(ID_T(CScene) aSceneID);
	void PointToObject(const CGameObject& aGameObject);

	template<typename T>
	T* GetComponent();

	template<typename T, typename ParameterPack>
	T* AddComponent(ParameterPack aParameterPack);

	virtual void Move(const CommonUtilities::Vector3f& aMovement);
	virtual void Rotate(const CommonUtilities::Vector3f& aRotation);
	virtual void SetPosition(const CommonUtilities::Vector3f& aPosition);

	///// TEMP
	const CommonUtilities::Vector3f GetForward();
	const CommonUtilities::Vector3f GetRight();
	const CommonUtilities::Vector3f GetUp();
	inline CTransform& GetTransform() { return ourComponentSystem->GetGameObjectData(myGameObjectDataID, mySceneID)->GetTransform(); }

	void SetActive(bool aIsActive);
	const bool IsActive() const;

	void Destroy();

	const bool IsValid() const { return myGameObjectDataID != ID_T_INVALID(CGameObjectData); }

private:
	friend CComponentSystem;
	friend CEngine;

	static CComponentSystem* ourComponentSystem;

	ID_T(CGameObjectData) myGameObjectDataID;
	ID_T(CScene) mySceneID;

	DefineDebugPtr(CGameObjectData);
	DefineDebugPtr(CScene);

	bool myIsInitialized;
	bool IsInitializedCheck();
};

template<typename T>
inline T* CGameObject::GetComponent()
{
	return ourComponentSystem->GetComponent<T>(myGameObjectDataID, mySceneID);
}

template<typename T, typename ParameterPack>
inline T* CGameObject::AddComponent(ParameterPack aParameterPack)
{
	return ourComponentSystem->AddComponent<T>(myGameObjectDataID, mySceneID, aParameterPack);
}

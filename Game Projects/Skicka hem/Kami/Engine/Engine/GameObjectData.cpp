#include "stdafx.h"
#include "GameObjectData.h"

CGameObjectData::CGameObjectData()
	: myTransform(this)
	, myHasBeenInitialized(false)
{
}


CGameObjectData::~CGameObjectData()
{
}

void CGameObjectData::Init(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID)
{
	mySceneID = aSceneID;
	myID = aGameObjectDataID;

	myComponents.Init(4);
	myHasBeenInitialized = true;
	myIsActive = true;
	myTransform = CTransform(this);
}
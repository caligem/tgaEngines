#pragma once
#include "GrowingArray.h"
#include "UniqueIdentifier.h" //fix later
#include "Transform.h"

class CComponent;
class CScene;
class CComponentSystem;
class CTransform;

class CGameObjectData
{
public:
	CGameObjectData();
	~CGameObjectData();

	struct SComponentData
	{
		SComponentData()
		{
			componentID = -1;
			componentType = -1;
		}

		SComponentData(int aComponentID, _UUID_T aComponentType)
		{
			componentID = aComponentID;
			componentType = aComponentType;
		}

		int componentID;
		_UUID_T componentType;
	};

	void Init(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID);


	template<typename T>
	void AddSComponent(ID_T(T) componentID, _UUID_T aUUID);

	inline CTransform& GetTransform() { return myTransform; }
	void SetActive(bool aIsActive) { myIsActive = aIsActive; }
	const bool IsActive() const { return myIsActive; }

private:
	friend CComponentSystem;
	friend CTransform;

	ID_T(CScene) mySceneID;
	ID_T(CGameObjectData) myID;

	template<typename T>
	int GetComponentID();

	bool myIsActive;
	bool myHasBeenInitialized;
	CommonUtilities::GrowingArray<SComponentData> myComponents;
	CTransform myTransform;
};

template<typename T>
inline int CGameObjectData::GetComponentID()
{
	for (unsigned short i = 0; i < myComponents.Size(); ++i)
	{
		if (_UUID(T) != myComponents[i].componentType)
		{
			continue;
		}

		return myComponents[i].componentID;
	}
	ENGINE_LOG(CONCOL_ERROR, "Trying to get ComponentID from GameObjectData but it doesnt exist.");
	return -1;
}

template<typename T>
inline void CGameObjectData::AddSComponent(ID_T(T) aComponentIDptr, _UUID_T aUUID)
{
#ifndef _RETAIL
	if (!myHasBeenInitialized)
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying To Add Component Type %s to GameObjectData but it's uninitialized, forget to run Init on GameObject?", typeid(T).name());
		return;
	}

	for (unsigned short i = 0; i < myComponents.Size(); ++i)
	{
		if (myComponents[i].componentType == aUUID)
		{
			ENGINE_LOG(CONCOL_WARNING, "Trying to add %s to GameObject(ID: %d) that allready exists.", typeid(T).name(), myID);
		}
	}
#endif

	myComponents.Add(SComponentData(aComponentIDptr.val, aUUID));
}

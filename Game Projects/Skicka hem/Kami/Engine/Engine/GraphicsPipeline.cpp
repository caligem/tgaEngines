#include "stdafx.h"
#include "GraphicsPipeline.h"

#include "IEngine.h"
#include "DirectXFramework.h"

#include "ComponentSystem.h"
#include "SceneManager.h"
#include "CameraManager.h"
#include "LightManager.h"

#include "WorkerPool.h"
#include "TextureBuilder.h"

#include <DL_Debug.h>

#include <InputManager.h>

CGraphicsPipeline::CGraphicsPipeline()
{
}

CGraphicsPipeline::~CGraphicsPipeline()
{
}

bool CGraphicsPipeline::Init()
{
	if (!myStateManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init StateManager!");
		return false;
	}

	if (!myCameraBuffer.Init(sizeof(SCameraBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create ForwardRenderer::myCameraBuffer");
	}

	if (!myFadeBuffer.Init(sizeof(CommonUtilities::Vector4f)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create ForwardRenderer::FadeBuffer");
	}

	if (!myForwardRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init ForwardRenderer!");
		return false;
	}
	if (!myDeferredRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init DeferredRenderer!");
		return false;
	}
	if (!myFullscreenRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init FullscreenRenderer!");
		return false;
	}
	if (!myDebugRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init DebugRenderer!");
		return false;
	}
	if (!my2DRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init 2DRenderer!");
		return false;
	}
	if (!myParticleRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init ParticleRenderer!");
		return false;
	}
	if (!mySkyboxRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init SkyboxRenderer!");
		return false;
	}

	if (!myFullscreenTexture.Init(IEngine::GetCanvasSize(), DXGI_FORMAT_R8G8B8A8_UNORM, true))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myFullscreenTexture!");
		return false;
	}
	if (!myIntermediateTexture1.Init(IEngine::GetCanvasSize()))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myIntermediateTexture!");
		return false;
	}
	myReadPingpong = &myIntermediateTexture1;
	if (!myIntermediateTexture2.Init(IEngine::GetCanvasSize()))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myIntermediateTexture!");
		return false;
	}
	myWritePingpong = &myIntermediateTexture2;
	if (!myHalfTexture.Init(IEngine::GetCanvasSize() / 2.f))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myHalfTexture!");
		return false;
	}
	if (!myQuarterTexture1.Init(IEngine::GetCanvasSize() / 4.f))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myQuarterTexture1!");
		return false;
	}
	if (!myQuarterTexture2.Init(IEngine::GetCanvasSize() / 4.f))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myQuarterTexture2!");
		return false;
	}

	DXGI_FORMAT formats[CFullscreenTexture::MaxSize] = {
		DXGI_FORMAT_R32G32B32A32_FLOAT,	// 0: Normal {n.x, n.y, n.z, depth}
		DXGI_FORMAT_R8G8B8A8_UNORM,		// 1: Albedo {a.r, a.g, a.b, a.a}
		DXGI_FORMAT_R8G8B8A8_UNORM,		// 2: RMAO   {roughness, metalness, AO, SSAO}
		DXGI_FORMAT_R8G8B8A8_UNORM		// 3: Emissive {e.r, e.g, e.b, e.a}
	};
	if (!myGBuffer.Init(IEngine::GetCanvasSize(), 4, formats, true))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myGBuffer!");
		return false;
	}
	if (!myLightTexture.Init(IEngine::GetCanvasSize()))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myLightTexture!");
		return false;
	}

	myColorGradingLUT = CTextureBuilder::CreateTextureFromFile("Assets/LUTS/RGBLUT_NoChange.dds");
	myNoiseTexture = CTextureBuilder::CreateTextureFromFile("Assets/Shaders/noise.dds");

	myModelsToRender.Init(16);
	myParticleSystemsToRender.Init(16);
	myPointLightsToRender.Init(16);
	mySpritesToRender.Init(16);
	myTextToRender.Init(16);

	myLightRenderCommands.Init();
	myModelRenderCommands.Init();
	myParticleSystemRenderCommands.Init();
	mySpriteRenderCommands.Init();
	myTextRenderCommands.Init();

	myActiveRenderers[ERenderer_Forward] = false;
	myActiveRenderers[ERenderer_Fullscreen] = true;
	myActiveRenderers[ERenderer_Debug] = true;
	myActiveRenderers[ERenderer_2D] = true;
	myActiveRenderers[ERenderer_Particle] = true;
	myActiveRenderers[ERenderer_FXAA] = true;
	myActiveRenderers[ERenderer_ColorGrading] = true;
	myActiveRenderers[ERenderer_SSAO] = true;

	myFrustumPlanes.Init(6);
	for (int i = 0; i < 6; ++i)myFrustumPlanes.EmplaceBack();

	return true;
}

void CGraphicsPipeline::Render()
{
#ifndef _RETAIL
	Input::CInputManager& input = IEngine::GetInputManager();
	for (int i = 0; i < ERenderer_Count; ++i)
	{
		if (input.IsKeyPressed(static_cast<Input::Key>(Input::Key_F1 + i)))
		{
			myActiveRenderers[i] = !myActiveRenderers[i];
		}
	}
#endif

	ID3D11DeviceContext* context = IEngine::GetContext();

	//Render objects in world
	if (IsActive(ERenderer_Forward))
	{
		myFullscreenTexture.SetAsActiveTarget();
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_Depth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
		myForwardRenderer.Render(myModelRenderCommands.GetReadBuffer(), myCameraBuffer, myLightRenderCommands.GetReadBuffer());
	}
	else
	{
		//Render skybox
		myFullscreenTexture.SetAsActiveTarget();
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
		mySkyboxRenderer.Render(myCameraBuffer, myCameraPosition);

		//Data Pass
		myGBuffer.SetAsActiveTarget();
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_Depth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
		myDeferredRenderer.RenderDataPass(myModelRenderCommands.GetReadBuffer(), myCameraBuffer, myCameraPosition, myFrustumPlanes);

		//SSAO Pass
		if (IsActive(ERenderer_SSAO))
		{
			ClearResourceSlots();
			myWritePingpong->SetAsActiveTarget();
			myGBuffer.SetTextureAsResourceOnSlot(0, 2);
			myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Copy);
			SwapPingPong();

			ClearResourceSlots();
			myGBuffer.SetTextureAsActiveTarget(2);
			myGBuffer.SetTextureAsResourceOnSlot(0, 0);
			myReadPingpong->SetTextureAsResourceOnSlot(1);
			context->PSSetShaderResources(2, 1, &myNoiseTexture);
			myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
			myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
			myStateManager.SetSamplerState(CGraphicsStateManager::ESamplerState_Linear_Wrap);
			myCameraBuffer.SetBuffer(0, EShaderType_Pixel);
			myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_SSAO);
		}

		//Light Passes
		myLightTexture.SetAsActiveTarget();
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Additive);
		myDeferredRenderer.RenderLightPass(myCameraBuffer, myLightRenderCommands.GetReadBuffer(), myGBuffer);

		myFullscreenTexture.SetAsActiveTarget();
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_AlphaBlend);
		myGBuffer.SetTextureAsResourceOnSlot(0, 0);
		myLightTexture.SetAsResourceOnSlot(1);
		context->PSSetShaderResources(2, 1, mySkyboxRenderer.GetSkyboxResource());
		myCameraBuffer.SetBuffer(0, EShaderType_Pixel);
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Fog);
	}

	//Render Particle emitters
	myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_Depth);
	myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
	if (IsActive(ERenderer_Forward))
	{
		myFullscreenTexture.SetAsActiveTarget();
	}
	else
	{
		myFullscreenTexture.SetAsActiveTarget(myGBuffer.GetDepthStencilView());
	}
	if (IsActive(ERenderer_Particle))
	{
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_AlphaBlend);
		myParticleRenderer.Render(myStateManager, myParticleSystemRenderCommands.GetReadBuffer(), myCameraBuffer);
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_Depth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
	}

	//Render debug lines
	if (IsActive(ERenderer_Debug))myDebugRenderer.Render(myCameraBuffer);

	// Fullscreen effects come here :) start ping-pong-ing
	myWritePingpong->SetAsActiveTarget();
	myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Copy, { &myFullscreenTexture, nullptr });
	SwapPingPong();


	if (IsActive(ERenderer_Fullscreen))
	{

		//Down sample scene
		myHalfTexture.SetAsActiveTarget();
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_BloomLuminance, { myReadPingpong, nullptr });

		//Down sample some more :D
		myQuarterTexture1.SetAsActiveTarget();
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Copy, { &myHalfTexture, nullptr });

		//BLUUUUUUUUUUUUR
		for (int i = 0; i < 15; ++i)
		{
			myQuarterTexture2.SetAsActiveTarget();
			myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_GaussianBlurHorizontal, { &myQuarterTexture1, nullptr });
			myQuarterTexture1.SetAsActiveTarget();
			myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_GaussianBlurVertical, { &myQuarterTexture2, nullptr });
		}

		//Scale up to fullscreen and present
		myWritePingpong->SetAsActiveTarget();
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_BloomAdd, { myReadPingpong, &myQuarterTexture1 });
		SwapPingPong();
	}

	if (IsActive(ERenderer_ColorGrading))
	{
		myWritePingpong->SetAsActiveTarget();
		myReadPingpong->SetAsResourceOnSlot(0);
		context->PSSetShaderResources(1, 1, &myColorGradingLUT);
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_ColorGrading);
		SwapPingPong();
	}

	myWritePingpong->SetAsActiveTarget();
	myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Copy, { myReadPingpong, nullptr });
	SwapPingPong();

	if (IsActive(ERenderer_FXAA))
	{
		myWritePingpong->SetAsActiveTarget();
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_FXAA, { myReadPingpong, nullptr });
		SwapPingPong();
	}

	myWritePingpong->SetAsActiveTarget();
	myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Copy, { myReadPingpong, nullptr });

	//Render 2D stuffs
	myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_AlphaBlend);
	myStateManager.SetSamplerState(CGraphicsStateManager::ESamplerState_Linear_Wrap);
	if (IsActive(ERenderer_2D)) my2DRenderer.Render(mySpriteRenderCommands.GetReadBuffer(), myTextRenderCommands.GetReadBuffer());
	myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
	myStateManager.SetSamplerState(CGraphicsStateManager::ESamplerState_Linear_Clamp);

	//FadeFX
	SwapPingPong();
	IEngine::GetDXFramework().GetBackBuffer()->SetAsActiveTarget();
	myFadeBuffer.SetData(&myFadeColor);
	myFadeBuffer.SetBuffer(2, EShaderType_Pixel);
	myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Fade, { myReadPingpong, nullptr });
}

void CGraphicsPipeline::SwapBuffers()
{
	myModelRenderCommands.SwapBuffers();
	myParticleSystemRenderCommands.SwapBuffers();
	mySpriteRenderCommands.SwapBuffers();
	myLightRenderCommands.SwapBuffers();
	myTextRenderCommands.SwapBuffers();

	myDebugRenderer.SwapBuffers();
}

void CGraphicsPipeline::SetRenderBuffers()
{
	IEngine::GetWorkerPool().DoWork(std::bind(&CGraphicsPipeline::FillModelBuffer, this));
	IEngine::GetWorkerPool().DoWork(std::bind(&CGraphicsPipeline::FillParticleSystemBuffer, this));
	IEngine::GetWorkerPool().DoWork(std::bind(&CGraphicsPipeline::FillPointLightBuffer, this));
	IEngine::GetWorkerPool().DoWork(std::bind(&CGraphicsPipeline::FillSpriteBuffer, this));
}

void CGraphicsPipeline::SwapPingPong()
{
	if (myWritePingpong == &myIntermediateTexture1)
	{
		myWritePingpong = &myIntermediateTexture2;
		myReadPingpong = &myIntermediateTexture1;
	}
	else
	{
		myWritePingpong = &myIntermediateTexture1;
		myReadPingpong = &myIntermediateTexture2;
	}
}

void CGraphicsPipeline::FillModelBuffer()
{
	CSceneManager& sceneManager = IEngine::GetSceneManager();
	CComponentSystem& componentSystem = IEngine::GetComponentSystem();

	CScene* activeScene = sceneManager.GetActiveScene();
	if (activeScene == nullptr) return;
	ID_T(CScene) activeSceneID = activeScene->mySceneID;

	myModelsToRender.RemoveAll();
	activeScene->FillRenderBuffer(_UUID(CModelComponent), myModelsToRender);
	for (auto modelComponentID : myModelsToRender)
	{
		CModelComponent* modelComponent = componentSystem.GetComponent<CModelComponent>(modelComponentID, activeSceneID);
		CGameObjectData* gameObjectData = componentSystem.GetGameObjectData(modelComponent->myGameObjectDataID, activeSceneID);

		if (gameObjectData->IsActive())
		{
			myModelRenderCommands.Write({
				gameObjectData->GetTransform().GetMatrix(),
				modelComponent->myModelID
			});
		}
	}
}

void CGraphicsPipeline::FillParticleSystemBuffer()
{
	CSceneManager& sceneManager = IEngine::GetSceneManager();
	CComponentSystem& componentSystem = IEngine::GetComponentSystem();

	CScene* activeScene = sceneManager.GetActiveScene();
	if (activeScene == nullptr) return;
	ID_T(CScene) activeSceneID = activeScene->mySceneID;

	myParticleSystemsToRender.RemoveAll();
	activeScene->FillRenderBuffer(_UUID(CParticleSystemComponent), myParticleSystemsToRender);
	for (auto particleSystemComponentID : myParticleSystemsToRender)
	{
		CParticleSystemComponent* particleSystemComponent = componentSystem.GetComponent<CParticleSystemComponent>(particleSystemComponentID, activeSceneID);
		CGameObjectData* gameObjectData = componentSystem.GetGameObjectData(particleSystemComponent->myGameObjectDataID, activeSceneID);

		if (gameObjectData->IsActive())
		{
			myParticleSystemRenderCommands.Write({
				particleSystemComponent->myParticles,
				particleSystemComponent->myParticleEmitterID
			});
		}
	}
}

void CGraphicsPipeline::FillPointLightBuffer()
{
	CSceneManager& sceneManager = IEngine::GetSceneManager();
	CComponentSystem& componentSystem = IEngine::GetComponentSystem();

	CScene* activeScene = sceneManager.GetActiveScene();
	if (activeScene == nullptr) return;
	ID_T(CScene) activeSceneID = activeScene->mySceneID;

	myPointLightsToRender.RemoveAll();
	activeScene->FillRenderBuffer(_UUID(CPointLightComponent), myPointLightsToRender);
	CLightManager& lightManagerPtr = IEngine::GetLightManager();
	for (auto pointLightComponentID : myPointLightsToRender)
	{
		CPointLightComponent* lightComponent = componentSystem.GetComponent<CPointLightComponent>(pointLightComponentID, activeSceneID);
		ID_T(CPointLight) pointLightID = lightComponent->myPointLightID;
		CPointLight* pointLight = lightManagerPtr.GetPointLight(pointLightID);
		CGameObjectData* gameObjectData = componentSystem.GetGameObjectData(lightComponent->myGameObjectDataID, activeSceneID);

		if (gameObjectData->IsActive())
		{
			myLightRenderCommands.Write({
				gameObjectData->GetTransform().GetPosition(),
				pointLight->myRange,
				pointLight->myColor
			});
		}
	}
}

void CGraphicsPipeline::FillSpriteBuffer()
{
	CSceneManager& sceneManager = IEngine::GetSceneManager();
	CComponentSystem& componentSystem = IEngine::GetComponentSystem();

	CScene* activeScene = sceneManager.GetActiveScene();
	if (activeScene == nullptr) return;
	ID_T(CScene) activeSceneID = activeScene->mySceneID;

	mySpritesToRender.RemoveAll();
	activeScene->FillRenderBuffer(_UUID(CSpriteComponent), mySpritesToRender);

	// Bubble Sort Fast For Priority
	unsigned short howManySorted = 0;
	while (howManySorted < mySpritesToRender.Size())
	{
		for (unsigned short i = 0; i < mySpritesToRender.Size() - (1 + howManySorted); ++i)
		{
			CSpriteComponent* spriteComponent = componentSystem.GetComponent<CSpriteComponent>(mySpritesToRender[i], activeSceneID);
			CSpriteComponent* spriteToCompareWith = componentSystem.GetComponent<CSpriteComponent>(mySpritesToRender[i + 1], activeSceneID);
			if (spriteComponent->GetPriority() > spriteToCompareWith->GetPriority())
			{
				int swap = mySpritesToRender[i];
				mySpritesToRender[i] = mySpritesToRender[i + 1];
				mySpritesToRender[i + 1] = swap;
			}
		}
		++howManySorted;
	}
	//-----
	for (auto sprite : mySpritesToRender)
	{
		CSpriteComponent* spriteComponent = componentSystem.GetComponent<CSpriteComponent>(sprite, activeSceneID);
		CGameObjectData* gameObjectData = componentSystem.GetGameObjectData(spriteComponent->myGameObjectDataID, activeSceneID);

		if (gameObjectData->IsActive())
		{
			mySpriteRenderCommands.Write({
				spriteComponent->myData,
				spriteComponent->mySpriteID
			});
		}
	}

	myTextToRender.RemoveAll();
	activeScene->FillRenderBuffer(_UUID(CTextComponent), myTextToRender);
	for (auto text : myTextToRender)
	{
		CTextComponent* textComponent = componentSystem.GetComponent<CTextComponent>(text, activeSceneID);
		CGameObjectData* gameObjectData = componentSystem.GetGameObjectData(textComponent->myGameObjectDataID, activeSceneID);

		if (gameObjectData->IsActive())
		{
			for (STextRenderCommand command : textComponent->GetRenderCommands())
			{
				command.myPosition = textComponent->GetPosition();
				myTextRenderCommands.Write(command);
			}
		}
	}
}

void CGraphicsPipeline::ClearResourceSlots()
{
	ID3D11ShaderResourceView* res[CFullscreenTexture::MaxSize] = { NULL };
	IEngine::GetContext()->PSSetShaderResources(0, CFullscreenTexture::MaxSize, res);
}

void CGraphicsPipeline::SetCameraBuffer()
{
	CSceneManager& sceneManager = IEngine::GetSceneManager();
	CCameraComponent* camera = sceneManager.GetActiveScene()->GetActiveCamera();
	if (camera == nullptr)
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to get active camera from scene!");
		return;
	}
	ID_T(CCamera) cameraID = camera->myCameraID;

	CCamera* cam = IEngine::GetCameraManager().GetCamera(cameraID);
	if (cam == nullptr)
	{
		return;
	}

	SCameraBufferData data;
	data.myCameraOrientation = IEngine::GetComponentSystem().GetGameObjectData(camera->myGameObjectDataID, sceneManager.GetActiveScene()->mySceneID)->GetTransform().GetMatrix();
	data.myToCamera = data.myCameraOrientation.GetFastInverse();
	data.myProjection = cam->GetProjection();
	data.myInvertedProjection = CommonUtilities::Matrix44f::Inverse(cam->GetProjection());
	data.myViewProjection = data.myToCamera * data.myProjection;
	data.myInvertedViewProjection = CommonUtilities::Matrix44f::Inverse(data.myViewProjection);

	myCameraBuffer.SetData(&data);
	mySavedCameraBuffer = data;
	myCameraPosition = {
		data.myCameraOrientation[12],
		data.myCameraOrientation[13],
		data.myCameraOrientation[14],
		cam->GetProjectionDepth()
	};

	CalculateFrustumPlanes(data, cam->GetProjectionDepth());
}

void CGraphicsPipeline::CalculateFrustumPlanes(SCameraBufferData aBuffer, float)
{
	const auto viewProjMat = aBuffer.myToCamera * aBuffer.myProjection;
	CommonUtilities::Vector4f planes[6];

	// Left clipping plane
	planes[0].x = viewProjMat[3] + viewProjMat[0];
	planes[0].y = viewProjMat[7] + viewProjMat[4];
	planes[0].z = viewProjMat[11] + viewProjMat[8];
	planes[0].w = viewProjMat[15] + viewProjMat[12];
	// Right clipping plane
	planes[1].x = viewProjMat[3] - viewProjMat[0];
	planes[1].y = viewProjMat[7] - viewProjMat[4];
	planes[1].z = viewProjMat[11] - viewProjMat[8];
	planes[1].w = viewProjMat[15] - viewProjMat[12];
	// Top clipping plane
	planes[2].x = viewProjMat[3] - viewProjMat[1];
	planes[2].y = viewProjMat[7] - viewProjMat[5];
	planes[2].z = viewProjMat[11] - viewProjMat[9];
	planes[2].w = viewProjMat[15] - viewProjMat[13];
	// Bottom clipping plane
	planes[3].x = viewProjMat[3] + viewProjMat[1];
	planes[3].y = viewProjMat[7] + viewProjMat[5];
	planes[3].z = viewProjMat[11] + viewProjMat[9];
	planes[3].w = viewProjMat[15] + viewProjMat[13];
	// Near clipping plane
	planes[4].x = viewProjMat[3];
	planes[4].y = viewProjMat[7];
	planes[4].z = viewProjMat[11];
	planes[4].w = viewProjMat[15];
	// Far clipping plane
	planes[5].x = viewProjMat[3] - viewProjMat[2];
	planes[5].y = viewProjMat[7] - viewProjMat[6];
	planes[5].z = viewProjMat[11] - viewProjMat[10];
	planes[5].w = viewProjMat[15] - viewProjMat[14];


	myFrustumPlanes[0].InitWithABCD(-planes[0].x, -planes[0].y, -planes[0].z, planes[0].w);
	myFrustumPlanes[1].InitWithABCD(-planes[1].x, -planes[1].y, -planes[1].z, planes[1].w);
	myFrustumPlanes[2].InitWithABCD(-planes[2].x, -planes[2].y, -planes[2].z, planes[2].w);
	myFrustumPlanes[3].InitWithABCD(-planes[3].x, -planes[3].y, -planes[3].z, planes[3].w);
	myFrustumPlanes[4].InitWithABCD(-planes[4].x, -planes[4].y, -planes[4].z, planes[4].w);
	myFrustumPlanes[5].InitWithABCD(-planes[5].x, -planes[5].y, -planes[5].z, planes[5].w);
}

void CGraphicsPipeline::BeginFrame()
{
	float clearColor[] = { 1.0f, 0.7f, 0.3f, 1.f };
	myFullscreenTexture.ClearTexture(clearColor);
	myLightTexture.ClearTexture({ 0.f, 0.f, 0.f, 0.f });
	myGBuffer.ClearTexture({ 0.f, 0.f, 0.f, 0.f });
	myGBuffer.ClearTextureAt(0, { 0.f, 0.f, 0.f, 1.f });
}
#pragma once

#include "ForwardRenderer.h"
#include "DeferredRenderer.h"
#include "FullscreenRenderer.h"
#include "FullscreenTexture.h"
#include "DebugRenderer.h"
#include "C2DRenderer.h"
#include "ParticleRenderer.h"
#include "SkyboxRenderer.h"

#include "DoubleBuffer.h"
#include "RenderCommand.h"
#include "GrowingArray.h"
#include "ConstantBuffer.h"
#include "Plane.h"

#include "GraphicsStateManager.h"

#include "DoubleBuffer.h"
#include "RenderCommand.h"

struct ID3D11ShaderResourceView;

class CModelComponent;
class CPointLightComponent;
class CParticleEditor;
class CEngine;
class CEditorEngine;

class CGraphicsPipeline
{
public:
	struct SCameraBufferData
	{
		CommonUtilities::Matrix44f myCameraOrientation;
		CommonUtilities::Matrix44f myToCamera;
		CommonUtilities::Matrix44f myProjection;
		CommonUtilities::Matrix44f myInvertedProjection;
		CommonUtilities::Matrix44f myViewProjection;
		CommonUtilities::Matrix44f myInvertedViewProjection;
	};

	CGraphicsPipeline();
	~CGraphicsPipeline();

	bool Init();

	void BeginFrame();
	void Render();

	void SwapBuffers();
	void SetCameraBuffer();
	void SetRenderBuffers();

	CDebugRenderer& GetDebugRenderer() { return myDebugRenderer; }
	CForwardRenderer& GetForwardRenderer() { return myForwardRenderer; }
	CSkyboxRenderer& GetSkyboxRenderer() { return mySkyboxRenderer; }
	CDeferredRenderer& GetDeferredRenderer() { return myDeferredRenderer; }

	const CGraphicsPipeline::SCameraBufferData& GetSavedCameraBuffer() const { return mySavedCameraBuffer; }

	const CommonUtilities::Vector4f& GetFadeColor() const { return myFadeColor; }
	void SetFadeColor(const CommonUtilities::Vector4f& aColor) { myFadeColor = aColor; }

private:
	friend CParticleEditor;
	friend CEngine;
	friend CEditorEngine;

	CDoubleBuffer<SModelRenderCommand> myModelRenderCommands;
	CDoubleBuffer<SParticleSystemRenderCommand> myParticleSystemRenderCommands;
	CDoubleBuffer<SSpriteRenderCommand> mySpriteRenderCommands;
	CDoubleBuffer<STextRenderCommand> myTextRenderCommands;
	CDoubleBuffer<SLightRenderCommand> myLightRenderCommands;
	
	CommonUtilities::GrowingArray<int> myModelsToRender;
	CommonUtilities::GrowingArray<int> myParticleSystemsToRender;
	CommonUtilities::GrowingArray<int> myPointLightsToRender;
	CommonUtilities::GrowingArray<int> mySpritesToRender;
	CommonUtilities::GrowingArray<int> myTextToRender;

	CConstantBuffer myCameraBuffer;
	SCameraBufferData mySavedCameraBuffer;
	CommonUtilities::Vector4f myCameraPosition;

	CConstantBuffer myFadeBuffer;
	CommonUtilities::Vector4f myFadeColor;

	enum ERenderer
	{
		ERenderer_Forward,
		ERenderer_Fullscreen,
		ERenderer_Debug,
		ERenderer_2D,
		ERenderer_Particle,
		ERenderer_FXAA,
		ERenderer_ColorGrading,
		ERenderer_SSAO,
		ERenderer_Count
	};

	bool IsActive(ERenderer aRenderer) { return myActiveRenderers[aRenderer]; }
	void CGraphicsPipeline::CalculateFrustumPlanes(SCameraBufferData aBuffer, float aFarPlane);

	CForwardRenderer myForwardRenderer;
	CDeferredRenderer myDeferredRenderer;
	CFullscreenRenderer myFullscreenRenderer;
	CDebugRenderer myDebugRenderer;
	C2DRenderer my2DRenderer;
	CParticleRenderer myParticleRenderer;
	CSkyboxRenderer mySkyboxRenderer;

	CFullscreenTexture myFullscreenTexture;
	CFullscreenTexture myIntermediateTexture1;
	CFullscreenTexture myIntermediateTexture2;
	CFullscreenTexture* myWritePingpong;
	CFullscreenTexture* myReadPingpong;
	CFullscreenTexture myHalfTexture;
	CFullscreenTexture myQuarterTexture1;
	CFullscreenTexture myQuarterTexture2;

	CFullscreenTexture myGBuffer;
	CFullscreenTexture myLightTexture;

	CGraphicsStateManager myStateManager;

	std::array<bool, ERenderer_Count> myActiveRenderers;

	CommonUtilities::GrowingArray<CommonUtilities::Plane<float>> myFrustumPlanes;

	ID3D11ShaderResourceView* myColorGradingLUT;
	ID3D11ShaderResourceView* myNoiseTexture;

	void SwapPingPong();

	void FillModelBuffer();
	void FillParticleSystemBuffer();
	void FillPointLightBuffer();
	void FillSpriteBuffer();
	void ClearResourceSlots();
};


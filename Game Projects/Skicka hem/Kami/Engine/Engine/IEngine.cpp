#include "stdafx.h"
#include "IEngine.h"
#include "Engine.h"

CEngine* IEngine::ourEngine = nullptr;

CEngine& IEngine::GetEngine()
{
	return *ourEngine;
}

CDirectXFramework & IEngine::GetDXFramework()
{
	return ourEngine->myFramework;
}

CWindowHandler & IEngine::GetWindowHandler()
{
	return ourEngine->myWindowHandler;
}

CDebugRenderer & IEngine::GetDebugRenderer()
{
	return ourEngine->myGraphicsPipeline.GetDebugRenderer();
}

CForwardRenderer& IEngine::GetForwardRenderer()
{
	return ourEngine->myGraphicsPipeline.GetForwardRenderer();
}

CSkyboxRenderer & IEngine::GetSkyboxRenderer()
{
	return ourEngine->myGraphicsPipeline.GetSkyboxRenderer();
}

CDeferredRenderer & IEngine::GetDeferredRenderer()
{
	return ourEngine->myGraphicsPipeline.GetDeferredRenderer();
}

CGraphicsPipeline & IEngine::GetGraphicsPipeline()
{
	return ourEngine->myGraphicsPipeline;
}

CComponentSystem & IEngine::GetComponentSystem()
{
	return ourEngine->myComponentSystem;
}

CSceneManager & IEngine::GetSceneManager()
{
	return ourEngine->mySceneManager;
}

CCameraManager & IEngine::GetCameraManager()
{
	return ourEngine->myCameraManager;
}

CLightManager & IEngine::GetLightManager()
{
	return ourEngine->myLightManager;
}

const CommonUtilities::Vector2f IEngine::GetWindowSize()
{
	return std::move(CommonUtilities::Vector2f(ourEngine->myCreateParameters.myWindowWidth, ourEngine->myCreateParameters.myWindowHeight));
}

void IEngine::SetWindowSize(const CommonUtilities::Vector2<unsigned short>& aWindowSize)
{
	ourEngine->myCreateParameters.myWindowWidth = aWindowSize.x;
	ourEngine->myCreateParameters.myWindowHeight = aWindowSize.y;
}

const CommonUtilities::Vector2f IEngine::GetCanvasSize()
{
	return std::move(CommonUtilities::Vector2f(ourEngine->myCreateParameters.myCanvasWidth, ourEngine->myCreateParameters.myCanvasHeight));
}

ID3D11Device * IEngine::GetDevice()
{
	return ourEngine->myFramework.GetDevice();
}

ID3D11DeviceContext * IEngine::GetContext()
{
	return ourEngine->myFramework.GetContext();
}

CModelManager & IEngine::GetModelManager()
{
	return ourEngine->myModelManager;
}

CParticleManager & IEngine::GetParticleManager()
{
	return ourEngine->myParticleManager;
}

CSpriteManager& IEngine::GetSpriteManager()
{
	return ourEngine->mySpriteManager;
}

CTextManager & IEngine::GetTextManager()
{
	return ourEngine->myTextManager;
}

CFileWatcher& IEngine::GetFileWatcher()
{
	return ourEngine->myFileWatcher;
}

CShaderManager & IEngine::GetShaderManager()
{
	return ourEngine->myShaderManager;
}

CommonUtilities::Timer & IEngine::Time()
{
	return ourEngine->myTimer;
}

Input::CInputManager& IEngine::GetInputManager()
{
	return ourEngine->myInputManager;
}

CommonUtilities::XBOXController& IEngine::GetXBoxController()
{
	if (ourEngine->myXBoxInput.IsConnected())
	{
		return ourEngine->myXBoxInput;
	}
	else if (ourEngine->myXBoxInput2.IsConnected())
	{
		return ourEngine->myXBoxInput2;
	}
	return ourEngine->myXBoxInput;
}

CWorkerPool& IEngine::GetWorkerPool()
{
	return ourEngine->myWorkerPool;
}

void IEngine::ShowCursor()
{
	ourEngine->myWindowHandler.ShowCursor();
}

void IEngine::HideCursor()
{
	ourEngine->myWindowHandler.HideCursor();
}

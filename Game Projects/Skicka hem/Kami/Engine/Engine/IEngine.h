#pragma once

class CEngine;
class CGUI;
class CDirectXFramework;
class CGraphicsPipeline;
class CGraphicsStateManager;

class CForwardRenderer;
class CDeferredRenderer;
class CDebugRenderer;
class CFullscreenRenderer;
class C2DRenderer;
class CParticleRenderer;
class CSkyboxRenderer;

class CParticleEmitter;
class CFullscreenTexture;
class CComponentSystem;
class CParticleSystemComponent;
class CSpriteComponent;
class CSpriteManager;
class CParticleManager;
class CSceneManager;
class CCameraManager;
class CTextManager;
class CScene;
class CTextureBuilder;
class CConstantBuffer;
class CFileWatcher;
class CVertexShader;
class CPixelShader;
class CGeometryShader;
class CModelLoader;
class CSprite;
class CCameraComponent;
class CGameObject;
class CWorkerPool;
class CWindowHandler;
class CLevelSelectState;

class CReportManager;

class CShaderManager;
class CModelManager;
class CLightManager;
class CSkyboxRenderer;

class CParticleEditor;

class IWorld;
class IEditor;

namespace CommonUtilities
{
	class Timer;
}

namespace Input
{
	class CInputManager;
}
namespace CommonUtilities
{
	class XBOXController;
}

struct ID3D11Device;
struct ID3D11DeviceContext;

class IEngine
{
private:
	//my friends can touch
	friend CEngine;
	friend CGUI;
	friend CGraphicsPipeline;
	friend CGraphicsStateManager;
	friend CForwardRenderer;
	friend CDeferredRenderer;
	friend CFullscreenRenderer;
	friend CFullscreenTexture;
	friend C2DRenderer;
	friend CParticleRenderer;
	friend CSkyboxRenderer;
	friend CParticleEmitter;
	friend CParticleSystemComponent;
	friend CSpriteComponent;
	friend CScene;
	friend CSceneManager;
	friend CTextureBuilder;
	friend CConstantBuffer;
	friend CDebugRenderer;
	friend CComponentSystem;
	friend CGameObject;
	friend CWindowHandler;
	friend CLevelSelectState;

	friend IWorld;
	friend IEditor;

	friend CVertexShader;
	friend CPixelShader;
	friend CGeometryShader;
	friend CModelLoader;
	friend CSprite;
	friend CCameraComponent;

	friend CParticleEditor;

	friend CReportManager;

	//my private parts
	static CEngine& GetEngine();
	static CDirectXFramework& GetDXFramework();
	static CWindowHandler& GetWindowHandler();
	static CDebugRenderer& GetDebugRenderer();
	static CForwardRenderer& GetForwardRenderer();
	static CSkyboxRenderer& GetSkyboxRenderer();
	static CDeferredRenderer& GetDeferredRenderer();
	static CGraphicsPipeline& GetGraphicsPipeline();

	static CComponentSystem& GetComponentSystem();

	static CSceneManager& GetSceneManager();
	static CCameraManager& GetCameraManager();
	static CLightManager& GetLightManager();
	static CModelManager& GetModelManager();
	static CParticleManager& GetParticleManager();
	static CSpriteManager& GetSpriteManager();
	static CTextManager& GetTextManager();

	static const CommonUtilities::Vector2f GetWindowSize();
	static const CommonUtilities::Vector2f GetCanvasSize();
	static void SetWindowSize(const CommonUtilities::Vector2<unsigned short>& aWindowSize);

	static ID3D11Device* GetDevice();
	static ID3D11DeviceContext* GetContext();

	static CFileWatcher& GetFileWatcher();
	static CShaderManager& GetShaderManager();

	static CommonUtilities::Timer& Time();
	static Input::CInputManager& GetInputManager();
	static CommonUtilities::XBOXController& GetXBoxController();

	static CWorkerPool& GetWorkerPool();

	static void ShowCursor();
	static void HideCursor();

	IEngine() = delete;
	~IEngine() = delete;
	
	static CEngine *ourEngine;
};


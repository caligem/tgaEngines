#include "stdafx.h"

#include "IWorld.h"
#include "IEngine.h"

#include "DebugRenderer.h"
#include "Engine.h"
#include "SceneManager.h"

CFileWatcher & IWorld::GetFileWatcher()
{
	return IEngine::GetFileWatcher();
}

CSceneManager & IWorld::GetSceneManager()
{
	return IEngine::GetSceneManager();
}

void IWorld::GameQuit()
{
	IEngine::GetEngine().Shutdown();
}

const CommonUtilities::Vector2f IWorld::GetWindowSize()
{
	return std::move(IEngine::GetWindowSize());
}

const CommonUtilities::Vector2f IWorld::GetCanvasSize()
{
	return std::move(IEngine::GetCanvasSize());
}

void IWorld::SetDebugColor(const CommonUtilities::Vector4f & aColor)
{
	IEngine::GetDebugRenderer().SetColor(aColor);
}

void IWorld::DrawLine(const CommonUtilities::Vector3f & aSource, const CommonUtilities::Vector3f & aDestination)
{
	IEngine::GetDebugRenderer().DrawLine(aSource, aDestination);
}

void IWorld::DrawBox(const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector3f & aScale, const CommonUtilities::Vector3f & aRotation)
{
	IEngine::GetDebugRenderer().DrawBox(aPosition, aScale, aRotation);
}

void IWorld::DrawSphere(const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector3f & aScale, const CommonUtilities::Vector3f & aRotation)
{
	IEngine::GetDebugRenderer().DrawSphere(aPosition, aScale, aRotation);
}

CommonUtilities::Timer & IWorld::Time()
{
	return IEngine::Time();
}

Input::CInputManager & IWorld::Input()
{
	return IEngine::GetInputManager();
}

void IWorld::ShowCursor()
{
	IEngine::ShowCursor();
}

void IWorld::HideCursor()
{
	IEngine::HideCursor();
}

void IWorld::SetFadeColor(const CommonUtilities::Vector4f & aFadeColor)
{
	IEngine::GetGraphicsPipeline().SetFadeColor(aFadeColor);
}

const CommonUtilities::Vector4f& IWorld::GetFadeColor()
{
	return IEngine::GetGraphicsPipeline().GetFadeColor();
}

CommonUtilities::XBOXController& IWorld::XBOX()
{
	return IEngine::GetXBoxController();
}
#pragma once

#include "Vector.h"

class CSceneManager;
class CComponentSystem;
class CGameObjectData;
class CGameObject;
class CFileWatcher;

namespace Input
{
	class CInputManager;
}
namespace CommonUtilities
{
	class XBOXController;
}
namespace CommonUtilities
{
	class Timer;
}

class IWorld
{
public:
	static CFileWatcher& GetFileWatcher();
	static CSceneManager& GetSceneManager();
	static void GameQuit();

	static const CommonUtilities::Vector2f GetWindowSize();
	static const CommonUtilities::Vector2f GetCanvasSize();

	static void SetDebugColor(
		const CommonUtilities::Vector4f& aColor = { 1.f, 0.f, 0.f, 1.f }
	);
	static void DrawLine(
		const CommonUtilities::Vector3f& aSource = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aDestination = { 1.f, 1.f, 1.f }
	);
	static void DrawBox(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);
	static void DrawSphere(
		const CommonUtilities::Vector3f& aPosition = { 0.f, 0.f, 0.f },
		const CommonUtilities::Vector3f& aScale = { 1.f, 1.f, 1.f },
		const CommonUtilities::Vector3f& aRotation = { 0.f, 0.f, 0.f }
	);

	static CommonUtilities::Timer& Time();
	static CommonUtilities::XBOXController& XBOX();
	static Input::CInputManager& Input();

	static void ShowCursor();
	static void HideCursor();

	static void SetFadeColor(const CommonUtilities::Vector4f& aFadeColor);
	static const CommonUtilities::Vector4f& GetFadeColor();
private:
	IWorld() = delete;
	~IWorld() = delete;
};


#include "stdafx.h"
#include "LightManager.h"


CLightManager::CLightManager()
{
}

CLightManager::~CLightManager()
{
}

bool CLightManager::Init()
{
	return true;
}

ID_T(CPointLight) CLightManager::AcquirePointLight(float aRange, CommonUtilities::Vector3f aColor)
{
	ID_T(CPointLight) id = myPointLights.Acquire();
	myPointLights.GetObj(id)->Init(aRange, aColor);
	return id;
}

void CLightManager::ReleasePointLight(ID_T(CPointLight) aPointLightID)
{
	myPointLights.Release(aPointLightID);
}

CPointLight * CLightManager::GetPointLight(ID_T(CPointLight) aPointLightID)
{
	return myPointLights.GetObj(aPointLightID);
}

#pragma once
#include "PointLight.h"
#include "ObjectPool.h"

class CGraphicsPipeline;

class CLightManager
{
public:
	CLightManager();
	~CLightManager();
	bool Init();
	ID_T(CPointLight) AcquirePointLight(float aRange, CommonUtilities::Vector3f aColor);
	void ReleasePointLight(ID_T(CPointLight));

private:
	friend CGraphicsPipeline;

	CPointLight* GetPointLight(ID_T(CPointLight) aPointLightID);
	ObjectPool<CPointLight> myPointLights;
};


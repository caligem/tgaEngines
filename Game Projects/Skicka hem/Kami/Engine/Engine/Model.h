#pragma once
#include "RefCounter.h"
#include "VertexDataWrapper.h"
#include "TextureDataWrapper.h"

class CModel : public RefCounter
{
public:
	CModel();
	~CModel();

	struct SModelData
	{
		constexpr static unsigned char MaxLODs = 8;
		SVertexDataWrapper myVertexData[MaxLODs];
		STextureDataWrapper myTextureData;
		unsigned char myLodCount = 0;
		float myRadius = 0.f;
	};

	void Init(const SModelData& aModelData);

	const SVertexDataWrapper& GetVertexData(unsigned char aIndex) const { return myModelData.myVertexData[aIndex]; }
	STextureDataWrapper& GetTextureData() { return myModelData.myTextureData; }

	unsigned char CalculateLodLevel(float aDistance);

	SModelData myModelData;
};


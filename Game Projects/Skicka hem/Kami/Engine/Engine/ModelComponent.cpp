#include "stdafx.h"
#include "ModelComponent.h"
#include "ModelManager.h"

CModelManager* CModelComponent::ourModelManager = nullptr;

CModelComponent::CModelComponent()
{
}

CModelComponent::~CModelComponent()
{
}

void CModelComponent::Init(const char* aFilePath)
{
	myModelID = ourModelManager->AcquireModel(aFilePath);
}

void CModelComponent::Release()
{
	ourModelManager->ReleaseModel(myModelID);
	myModelID = ID_T_INVALID(CModel);
}

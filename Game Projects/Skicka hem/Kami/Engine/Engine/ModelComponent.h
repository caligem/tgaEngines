#pragma once

#include "Matrix.h"
#include "Vector.h"
#include "Component.h"

class CModel;
class CForwardRenderer;
class CEngine;
class CModelManager;
class CGraphicsPipeline;
class CComponentSystem;

class CModelComponent : public CComponent
{
public:
	CModelComponent();
	~CModelComponent();

	void Init(const char* aFilePath);


private:
	friend CForwardRenderer;
	friend CEngine;
	friend CGraphicsPipeline;
	friend CComponentSystem;

	void Release() override;

	static CModelManager* ourModelManager;

	ID_T(CModel) myModelID;
};


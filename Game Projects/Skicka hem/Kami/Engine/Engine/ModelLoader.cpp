#include "stdafx.h"
#include "ModelLoader.h"

#include <d3d11.h>
#include <fstream>

#include "DDSTextureLoader.h"
#include "FBXLoaderCustom.h"
#include "TextureBuilder.h"

#include "IEngine.h"
#include "ShaderManager.h"
#include "DXMacros.h"

CModelLoader::CModelLoader()
{
}

CModelLoader::~CModelLoader()
{
}

bool CModelLoader::Init(ID3D11Device * aDevice)
{
	if (aDevice == nullptr)
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: DirectX Device was nullptr when loading Model.");
		return false;
	}

	myDevice = aDevice;
	myLoader = new CFBXLoaderCustom();

	return true;
}

CModel::SModelData CModelLoader::LoadModel(const char * aModelPath)
{
	START_TIMER(LOAD_MODEL);

	HRESULT result;
	CModel::SModelData modelData;

	//Loading model
	CLoaderModel* loaderModel = myLoader->LoadModel(aModelPath);

	if (loaderModel == nullptr)
	{
		RESOURCE_LOG(CONCOL_ERROR, "ERROR: Failed to load model: %s", aModelPath);
		return LoadCube();
	}

	if (loaderModel->myMeshes.empty())
	{
		RESOURCE_LOG(CONCOL_ERROR, "ERROR: Failed to find any meshes for: %s", aModelPath);
		return LoadCube();
	}

	modelData.myLodCount = static_cast<unsigned char>(loaderModel->myMeshes.size());
	if (modelData.myLodCount > CModel::SModelData::MaxLODs)modelData.myLodCount = CModel::SModelData::MaxLODs;
	for (int i = 0; i < modelData.myLodCount; ++i)
	{
		//Creating Vertex Buffer
		D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
		vertexBufferDesc.ByteWidth = loaderModel->myMeshes[i]->myVertexBufferSize * loaderModel->myMeshes[i]->myVertexCount;
		vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

		D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
		vertexBufferData.pSysMem = loaderModel->myMeshes[i]->myVerticies;

		result = myDevice->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &modelData.myVertexData[i].myVertexBuffer);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		}

		//Creating Index Buffer
		D3D11_BUFFER_DESC indexBufferDesc = { 0 };
		indexBufferDesc.ByteWidth = static_cast<unsigned int>(loaderModel->myMeshes[i]->myIndexes.size() * sizeof(unsigned int));
		indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
		indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

		D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
		indexBufferData.pSysMem = &loaderModel->myMeshes[i]->myIndexes[0];

		result = myDevice->CreateBuffer(&indexBufferDesc, &indexBufferData, &modelData.myVertexData[i].myIndexBuffer);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		}

		modelData.myVertexData[i].myNumberOfIndices = static_cast<unsigned int>(loaderModel->myMeshes[i]->myIndexes.size());
		modelData.myVertexData[i].myNumberOfVertices = loaderModel->myMeshes[i]->myVertexCount;
		modelData.myVertexData[i].myStride = loaderModel->myMeshes[i]->myVertexBufferSize;
		modelData.myVertexData[i].myOffset = 0;
		modelData.myVertexData[i].myLODDistance = loaderModel->myMeshes[i]->myLODDistance;
		modelData.myRadius = std::fmaxf(loaderModel->myMeshes[i]->myRadius, modelData.myRadius);
	}

	//Load Texture
	std::string filepath(aModelPath);
 
	size_t split = filepath.find_last_of("\\/");
	std::string path = filepath.substr(0, split + 1);
	std::string name = filepath.substr(split + 1);

	// Picks out the albedo, rough, ao, normal and metallic maps from the texture list
	for (int i = 0; i < loaderModel->myTextures.size(); ++i)
	{
		if (loaderModel->myTextures[i].empty())
		{
			RESOURCE_LOG(CONCOL_WARNING, "Missing %s texture for model %s", myTextureTypeNames[i], aModelPath);
			if (i == 0)
			{
				modelData.myTextureData.myTextures[i] = LoadDefaultTexture(-1);
			}
			else
			{
				modelData.myTextureData.myTextures[i] = LoadDefaultTexture(i);
			}
		}
		else
		{
			std::wstring textureFilePath(path.begin(), path.end());
			textureFilePath.append(loaderModel->myTextures[i].begin(), loaderModel->myTextures[i].end());

			//RESOURCE_LOG(CONCOL_VALID, "LOADING %s as %s", myTextureTypeNames[i], loaderModel->myTextures[i].c_str());

			ID3D11ShaderResourceView* shaderResourceView = NULL;
			result = DirectX::CreateDDSTextureFromFile(myDevice, textureFilePath.c_str(), nullptr, &shaderResourceView);
			if (FAILED(result))
			{
				RESOURCE_LOG(CONCOL_ERROR, "ERROR: Failed to load %s texture: %s | %s!", myTextureTypeNames[i], loaderModel->myTextures[i].c_str(), aModelPath);
				shaderResourceView = LoadDefaultTexture(i);
			}
			modelData.myTextureData.myTextures[i] = shaderResourceView;
		}
	}

	delete loaderModel;

	float time = GET_TIME(LOAD_MODEL);
	if (time <= 20)
	{
		RESOURCE_LOG(CONCOL_VALID, "Model %s took %fms to load!", aModelPath, time)
	}
	else if (time < 100)
	{
		RESOURCE_LOG(CONCOL_WARNING, "Model %s took %fms to load!", aModelPath, time)
	}
	else
	{
		RESOURCE_LOG(CONCOL_ERROR, "Model %s took %fms to load!", aModelPath, time)
	}

	return modelData;
}

CModel::SModelData CModelLoader::LoadCube()
{
	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
		float nX, nY, nZ, nW; //normal
		float tX, tY, tZ, tW; //tangent
		float bX, bY, bZ, bW; //binormal
		float u, v;
	} vertices[24] =
	{
		{0.5, -0.5, 0.5, 1.f,   -0, 0, 1, 1.f,  1, -0, -0, 1.f, 0, 1, -0, 1.f,  0.f, 0.f},
		{-0.5, -0.5, 0.5, 1.f,  -0, 0, 1, 1.f,  1, -0, 0, 1.f,  0, 1, -0, 1.f,  1.f, 0.f},
		{0.5, 0.5, 0.5, 1.f,    -0, 0, 1, 1.f,  1, -0, 0, 1.f,  0, 1, -0, 1.f,  0.f, 1.f},
		{-0.5, 0.5, 0.5, 1.f,   -0, 0, 1, 1.f,  1, -0, 0, 1.f,  0, 1, -0, 1.f,  1.f, 1.f},
		{0.5, 0.5, 0.5, 1.f,    -0, 1, 0, 1.f,  1, -0, 0, 1.f,  0, 0, -1, 1.f,  0.f, 0.f},
		{-0.5, 0.5, 0.5, 1.f,   -0, 1, 0, 1.f,  1, 0, 0, 1.f,   0, 0, -1, 1.f,  1.f, 0.f},
		{0.5, 0.5, -0.5, 1.f,   -0, 1, 0, 1.f,  1, 0, 0, 1.f,   0, 0, -1, 1.f,  0.f, 1.f},
		{-0.5, 0.5, -0.5, 1.f,  -0, 1, 0, 1.f,  1, 0, 0, 1.f,   0, 0, -1, 1.f,  1.f, 1.f},
		{0.5, 0.5, -0.5, 1.f,   -0, 0, -1, 1.f, 1, 0, 0, 1.f,   0, -1, 0, 1.f,  0.f, 0.f},
		{-0.5, 0.5, -0.5, 1.f,  -0, 0, -1, 1.f, 1, 0, 0, 1.f,   0, -1, 0, 1.f,  1.f, 0.f},
		{0.5, -0.5, -0.5, 1.f,  -0, 0, -1, 1.f, 1, 0, 0, 1.f,   0, -1, 0, 1.f,  0.f, 1.f},
		{-0.5, -0.5, -0.5, 1.f, -0, 0, -1, 1.f, 1, 0, 0, 1.f,   0, -1, 0, 1.f,  1.f, 1.f},
		{0.5, -0.5, -0.5, 1.f,  -0, -1, 0, 1.f, 1, 0, -0, 1.f,  0, 0, 1, 1.f,   0.f, 0.f},
		{-0.5, -0.5, -0.5, 1.f, -0, -1, 0, 1.f, 1, 0, -0, 1.f,  0, 0, 1, 1.f,   1.f, 0.f},
		{0.5, -0.5, 0.5, 1.f,   -0, -1, 0, 1.f, 1, 0, -0, 1.f,  0, 0, 1, 1.f,   0.f, 1.f},
		{-0.5, -0.5, 0.5, 1.f,  -0, -1, 0, 1.f, 1, 0, -0, 1.f,  0, 0, 1, 1.f,   1.f, 1.f},
		{-0.5, -0.5, 0.5, 1.f,  -1, 0, 0, 1.f,  0, 0, -1, 1.f,  -0, 1, 0, 1.f,  0.f, 0.f},
		{-0.5, -0.5, -0.5, 1.f, -1, 0, 0, 1.f,  0, 0, -1, 1.f,  -0, 1, 0, 1.f,  1.f, 0.f},
		{-0.5, 0.5, 0.5, 1.f,   -1, 0, 0, 1.f,  0, 0, -1, 1.f,  -0, 1, 0, 1.f,  0.f, 1.f},
		{-0.5, 0.5, -0.5, 1.f,  -1, 0, 0, 1.f,  0, -0, -1, 1.f, 0, 1, -0, 1.f,  1.f, 1.f},
		{0.5, -0.5, -0.5, 1.f,  1, 0, 0, 1.f,   0, -0, 1, 1.f,  0, 1, 0, 1.f,   0.f, 0.f},
		{0.5, -0.5, 0.5, 1.f,   1, 0, 0, 1.f,   0, -0, 1, 1.f,  0, 1, 0, 1.f,   1.f, 0.f},
		{0.5, 0.5, -0.5, 1.f,   1, 0, 0, 1.f,   0, -0, 1, 1.f,  0, 1, 0, 1.f,   0.f, 1.f},
		{0.5, 0.5, 0.5, 1.f,    1, 0, 0, 1.f,   0, -0, 1, 1.f,  0, 1, 0, 1.f,   1.f, 1.f}
	};
	unsigned int indices[36] =
	{
		2,1,0,
		3,1,2,
		6,5,4,
		7,5,6,
		10,9,8,
		11,9,10,
		14,13,12,
		15,13,14,
		18,17,16,
		19,17,18,
		22,21,20,
		23,21,22
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = myDevice->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = myDevice->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
	}

	//Load shaders
	CModel::SModelData modelData;

	for (int i = 0; i < ETextureTypes_Count; ++i)
	{
		modelData.myTextureData.myTextures[i] = LoadDefaultTexture(i);
	}

	//Setup ModelData
	modelData.myVertexData[0].myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	modelData.myVertexData[0].myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	modelData.myVertexData[0].myStride = sizeof(Vertex);
	modelData.myVertexData[0].myOffset = 0;
	modelData.myVertexData[0].myVertexBuffer = vertexBuffer;
	modelData.myVertexData[0].myIndexBuffer = indexBuffer;
	modelData.myLodCount = 1;
	modelData.myRadius = CommonUtilities::Vector3f(0.5f, 0.5f, 0.5f).Length();

	return modelData;
}

ID3D11ShaderResourceView* CModelLoader::LoadDefaultTexture(int i)
{
	switch (i)
	{
	case ETextureTypes_Albedo:
		return CTextureBuilder::CreateGrayCheckerBoardTexture();
		break;
	case ETextureTypes_Roughness:
		return CTextureBuilder::CreateWhiteTexture();
		break;
	case ETextureTypes_AmbientOcclusion:
		return CTextureBuilder::CreateWhiteTexture();
		break;
	case ETextureTypes_Normal:
		return CTextureBuilder::CreateNormalTexture();
		break;
	case ETextureTypes_Metalness:
		return CTextureBuilder::CreateBlackTexture();
		break;
	case ETextureTypes_Emissive:
		return CTextureBuilder::CreateBlackTexture();
		break;
	default:
		return CTextureBuilder::CreatePinkCheckerBoardTexture();
		break;
	}
}

#pragma once

#include "Model.h"

#include <map>

struct ID3D11Device;
class CFBXLoaderCustom;

class CModelLoader
{
public:
	CModelLoader();
	~CModelLoader();

	bool Init(ID3D11Device* aDevice);
	CModel::SModelData LoadModel(const char* aModelPath);
	CModel::SModelData LoadCube();
	ID3D11ShaderResourceView* LoadDefaultTexture(int i);

private:
	enum ETextureTypes
	{
		ETextureTypes_Albedo,
		ETextureTypes_Roughness,
		ETextureTypes_AmbientOcclusion,
		ETextureTypes_Normal,
		ETextureTypes_Metalness,
		ETextureTypes_Emissive,
		ETextureTypes_Count
	};
	const char* myTextureTypeNames[ETextureTypes_Count] = {
		"albedo",
		"roughness",
		"ambientOcclusion",
		"normal",
		"metalness",
		"emissive"
	};

	ID3D11Device* myDevice;
	CFBXLoaderCustom* myLoader;

};


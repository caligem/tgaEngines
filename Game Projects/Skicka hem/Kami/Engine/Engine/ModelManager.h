#pragma once
#include "ModelLoader.h"

class CDirectXFramework;
class CForwardRenderer;
class CDeferredRenderer;
class CSkyboxRenderer;

class CModelManager
{
public:
	CModelManager();
	~CModelManager();

	bool Init(CDirectXFramework& aDirectXFramework);
	ID_T(CModel) AcquireModel(const char* aModelPath = nullptr);
	void ReleaseModel(ID_T(CModel) aModelID);

private:
	friend CSkyboxRenderer;
	friend CForwardRenderer;
	friend CDeferredRenderer;

	CModel* GetModel(ID_T(CModel) aModelID);
	std::map<std::string, ID_T(CModel)> myModelCache;
	ObjectPool<CModel> myModels;
	CModelLoader myModelLoader;
};


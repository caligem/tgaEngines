#pragma once
#include "ObjectPool.h"
#include "ParticleEmitter.h"

class CParticleRenderer;
class CParticleSystemComponent;
class CParticleEditor;

class CParticleManager
{
public:
	CParticleManager();
	~CParticleManager();

	bool Init();
	ID_T(CParticleEmitter) AcquireParticleEmitter(const char* aFilePath, unsigned int aVertexBufferSize);
	ID_T(CParticleEmitter) AcquireParticleEmitter(CParticleEmitter::SParticleData aParticleData, unsigned int aVertexBufferSize);
	void ReleaseParticleEmitter(ID_T(CParticleEmitter) aParticleEmitterID);

private:
	friend CParticleRenderer;
	friend CParticleSystemComponent;
	friend CParticleEditor;

	ID_T(CParticleEmitter) CParticleManager::LoadParticleData(const char * aFilePath, unsigned int aVertexBufferSize);
	CParticleEmitter* GetParticleEmitter(ID_T(CParticleEmitter) aParticleEmitterID);
	void LoadParticleDataFromFile(CParticleEmitter::SParticleData& aParticleData, const char* aFilePath);
	std::map<std::string, ID_T(CParticleEmitter)> myParticleDataCache;
	ObjectPool<CParticleEmitter> myParticleEmitters;
};


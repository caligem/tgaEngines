#include "stdafx.h"
#include "ParticleRenderer.h"

#include "IEngine.h"
#include "ShaderManager.h"
#include "ParticleManager.h"
#include "ConstantBuffer.h"

#include "RenderCommand.h"

CParticleRenderer::CParticleRenderer()
{
	myVertexShader = nullptr;
	myPixelShader = nullptr;
	myGeometryShader = nullptr;
}

CParticleRenderer::~CParticleRenderer()
{
}

bool CParticleRenderer::Init()
{
	myVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/Particle/Particle.vs", EShaderInputLayoutType_Particle);
	myPixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/Particle/Particle.ps");
	myGeometryShader = &IEngine::GetShaderManager().GetGeometryShader(L"Assets/Shaders/Particle/Particle.gs");

	return true;
}

void CParticleRenderer::Render(CGraphicsStateManager& aStateManager, const CommonUtilities::GrowingArray<SParticleSystemRenderCommand>& aParticleSystemRenderCommands, CConstantBuffer& aCameraBuffer)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);
	aCameraBuffer.SetBuffer(0, EShaderType_Geometry);

	myVertexShader->Bind();
	myVertexShader->BindLayout();
	myGeometryShader->Bind();
	myPixelShader->Bind();

	D3D11_MAPPED_SUBRESOURCE data;
	ZeroMemory(&data, sizeof(D3D11_MAPPED_SUBRESOURCE));

	CGraphicsStateManager::EBlendState currentBlendState = CGraphicsStateManager::EBlendState_Disabled;

	for (unsigned short i = 0; i < aParticleSystemRenderCommands.Size(); ++i)
	{
		CParticleEmitter* particleEmitter = IEngine::GetParticleManager().GetParticleEmitter(aParticleSystemRenderCommands[i].myParticleEmitterID);
		
		if (currentBlendState != particleEmitter->GetBlendState())
		{
			currentBlendState = particleEmitter->GetBlendState();
			aStateManager.SetBlendState(currentBlendState);
		}

		unsigned short particlesSize = aParticleSystemRenderCommands[i].myParticles.Size();
		if (particlesSize <= 0)
		{
			continue;
		}

		HRESULT result = context->Map(particleEmitter->myVertexData.myVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &data);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "Failed to Map Constant Buffer");
		}


		int copySize = particlesSize * particleEmitter->myVertexData.myStride;
		memcpy_s(data.pData, copySize, aParticleSystemRenderCommands[i].myParticles.begin(), copySize);
		context->Unmap(particleEmitter->myVertexData.myVertexBuffer, 0);

		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
		context->IASetVertexBuffers(0, 1, &particleEmitter->myVertexData.myVertexBuffer, &particleEmitter->myVertexData.myStride, &particleEmitter->myVertexData.myOffset);
		context->PSSetShaderResources(0, 1, &particleEmitter->myTexture);

		context->Draw(particlesSize, 0);
	}

	myVertexShader->Unbind();
	myVertexShader->UnbindLayout();
	myGeometryShader->Unbind();
	myPixelShader->Unbind();
}

#pragma once

#include "GrowingArray.h"
#include "Vector.h"

class CVertexShader;
class CPixelShader;
class CGeometryShader;
class CConstantBuffer;
class CGraphicsStateManager;

struct SParticleSystemRenderCommand;

class CParticleRenderer
{
public:
	CParticleRenderer();
	~CParticleRenderer();

	bool Init();

	void Render(CGraphicsStateManager& aStateManager, const CommonUtilities::GrowingArray<SParticleSystemRenderCommand>& aParticleSystemRenderCommands, CConstantBuffer& aCameraBuffer);

private:
	CVertexShader* myVertexShader;
	CPixelShader* myPixelShader;
	CGeometryShader* myGeometryShader;
};


#pragma once

class CGraphicsPipeline;

class CPointLight
{
public:
	CPointLight();
	~CPointLight();

	void Init(float aRange, CommonUtilities::Vector3f aColor);

private:
	friend CGraphicsPipeline;

	float myRange;
	CommonUtilities::Vector3f myColor;
};


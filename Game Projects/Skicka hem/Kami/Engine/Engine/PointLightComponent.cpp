#include "stdafx.h"
#include "PointLightComponent.h"
#include "LightManager.h"

CLightManager* CPointLightComponent::ourLightLoader = nullptr;

CPointLightComponent::CPointLightComponent()
{
}


CPointLightComponent::~CPointLightComponent()
{
}

void CPointLightComponent::Init(SPointLightComponentData aPointLightData)
{
	myPointLightID = ourLightLoader->AcquirePointLight(aPointLightData.range, aPointLightData.color);
}

void CPointLightComponent::Release()
{
	ourLightLoader->ReleasePointLight(myPointLightID);
	myPointLightID = ID_T_INVALID(CPointLight);
}

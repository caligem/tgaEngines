#pragma once
#include "Component.h"

class CLightManager;
class CEngine;
class CPointLight;
class CGraphicsPipeline;
class CComponentSystem;

class CPointLightComponent : public CComponent
{
public:
	CPointLightComponent();
	~CPointLightComponent();

	struct SPointLightComponentData
	{

		SPointLightComponentData(float aRange, CommonUtilities::Vector3f aColor)
		{
			range = aRange;
			color = aColor;
		}

		float range;
		CommonUtilities::Vector3f color;
	};

	void Init(SPointLightComponentData aPointLightData);


private:
	friend CEngine;
	friend CGraphicsPipeline;
	friend CComponentSystem;

	static CLightManager* ourLightLoader;
	ID_T(CPointLight) myPointLightID;

	void Release() override;
};


#pragma once

#include "Transform.h"
#include "Matrix.h"
#include "ObjectPool.h"
#include "ParticleSystemComponent.h"
#include "SpriteComponent.h"

class CModel;
class CSprite;
class CFont;

struct SModelRenderCommand
{
	CTransform myTransform;
	ID_T(CModel) myModelToRender;
};

struct SLightRenderCommand
{
	CommonUtilities::Vector3f position;
	float range;
	CommonUtilities::Vector3f color;
};

struct SParticleSystemRenderCommand
{
	CommonUtilities::GrowingArray<CParticleSystemComponent::SParticleBufferData> myParticles;
	ID_T(CParticleEmitter) myParticleEmitterID;
};

struct SSpriteRenderCommand
{
	CSpriteComponent::SSpriteBufferData mySpriteData;
	ID_T(CSprite) mySpriteID;
};

struct STextRenderCommand
{
	CSpriteComponent::SSpriteBufferData mySpriteData;
	CommonUtilities::Vector2f myPosition;
	ID_T(CFont) myFontID;
};

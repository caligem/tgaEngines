#pragma once

#include <GrowingArray.h>
#include "ObjectPool.h"
#include "Vector.h"
#include "UniqueIdentifier.h"

class CModelComponent;
class CCameraComponent;
class CPointLightComponent;
class CParticleSystemComponent;
class CSpriteComponent;
class CComponentSystem;
class CGraphicsPipeline;

class CScene
{
public:
	CScene();
	~CScene();

	void Init(ID_T(CScene) aSceneID);

	void AddComponentID(_UUID_T aUUID, int aID);

	void FillRenderBuffer(_UUID_T aUUID_T, CommonUtilities::GrowingArray<int>& aBuffer);

	CCameraComponent* GetActiveCamera();
	void SetActiveCamera(ID_T(CCameraComponent) aCameraInstance);

	void SetDirectionalLight(const CommonUtilities::Vector3f& aDirection) { myDirectionalLight = aDirection; }
	const CommonUtilities::Vector3f& GetDirectionalLight() const { return myDirectionalLight; }

	void RemoveComponentID(_UUID_T aUUID, int aID);
	bool GrowingArrayContainsUUID(_UUID_T aUUID);
	void Unload();

private:
	friend CComponentSystem;
	friend CGraphicsPipeline;

	ID_T(CScene) mySceneID;

	ID_T(CCameraComponent) myActiveCamera;

	void AddComponentToGrowingArray(_UUID_T aUUID, unsigned short aStartSize);
	std::map<_UUID_T, CommonUtilities::GrowingArray<int>> myComponentIDs;

	CommonUtilities::Vector3f myDirectionalLight;
};

#include "stdafx.h"
#include "SceneManager.h"

#define MAX_SCENES 8

CSceneManager::CSceneManager()
	: mySceneCount(0)
	, myScenes(MAX_SCENES)
{
}


CSceneManager::~CSceneManager()
{
}

bool CSceneManager::Init()
{
	myScenesToBeDestroyed.Init(8);
	return true;
}

ID_T(CScene) CSceneManager::CreateScene()
{
	if (mySceneCount == MAX_SCENES - 1)
	{
		ENGINE_LOG(CONCOL_WARNING, "Used up the last scene in SceneManager.")
	}
	mySceneCount++;
	ID_T(CScene) sceneID = myScenes.Acquire();
	myScenes.GetObj(sceneID)->Init(sceneID);
	return sceneID;
}

CScene * CSceneManager::GetActiveScene()
{
	if (myActiveSceneID == ID_T_INVALID(CScene))
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to get ActiveScene, but it isnt set in SceneManager");
		return nullptr;
	}
	return myScenes.GetObj(myActiveSceneID);
}

CScene* CSceneManager::GetSceneAt(ID_T(CScene) aSceneID)
{
	return myScenes.GetObj(aSceneID);
}

void CSceneManager::SetActiveScene(ID_T(CScene) aSceneID)
{
	myActiveSceneID = aSceneID;
}

void CSceneManager::DestroyScene(ID_T(CScene) aSceneID)
{
	myScenesToBeDestroyed.Add(aSceneID);
}

void CSceneManager::DestroyScenesInQueue()
{
	for (unsigned short i = 0; i < myScenesToBeDestroyed.Size(); ++i)
	{
		myScenes.GetObj(myScenesToBeDestroyed[i])->Unload();
		myScenes.Release(myScenesToBeDestroyed[i]);
		mySceneCount--;
	}

	myScenesToBeDestroyed.RemoveAll();
}

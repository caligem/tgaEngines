#pragma once
#include "ObjectPool.h"
#include "Scene.h"

class CEngine;

class CSceneManager
{
public:
	CSceneManager();
	~CSceneManager();

	bool Init();

	ID_T(CScene) CreateScene();

	CScene* GetActiveScene();
	CScene* GetSceneAt(ID_T(CScene) aSceneID);
	void SetActiveScene(ID_T(CScene) aSceneID);

	void DestroyScene(ID_T(CScene) aSceneID);

private:
	friend CEngine;
	ID_T(CScene) myActiveSceneID;
	size_t mySceneCount;
	ObjectPool<CScene> myScenes;
	CommonUtilities::GrowingArray<ID_T(CScene)> myScenesToBeDestroyed;

	/*TODO: Implement events
		std::function<void()> callbackZzz;
		- activeSceneChanged
		- sceneLoaded
		- sceneUnloaded
	*/
public:
	void DestroyScenesInQueue();
};


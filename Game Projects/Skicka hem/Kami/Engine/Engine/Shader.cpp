#include "stdafx.h"
#include "Shader.h"

#include <d3d11.h>
#include <d3dcompiler.h>
#pragma comment(lib,"d3dcompiler.lib")

#include "IEngine.h"
#include "FileWatcher.h"
#include "DL_Debug.h"
#include "DXMacros.h"

enum EShaderProfile
{
	EShaderProfile_Vertex,
	EShaderProfile_Pixel,
	EShaderProfile_Geometry
};

std::string defaultVertexShader =
"struct VertexInput { float4 myPosition : POSITION; }; \
struct PixelInput { float4 myPosition : SV_POSITION; }; \
struct PixelOutput { float4 myColor : SV_TARGET; }; \
cbuffer CameraData : register(b0) { float4x4 cameraOrientation; float4x4 toCamera; float4x4 toProjection; } \
cbuffer InstanceData : register(b1) { float4x4 toWorld; } \
PixelInput main(VertexInput input) \
{ \
PixelInput output; \
input.myPosition.w = 1.f; \
output.myPosition = mul(toWorld, input.myPosition); \
output.myPosition = mul(toCamera, output.myPosition); \
output.myPosition = mul(toProjection, output.myPosition); \
return output; \
} ";

std::string defaultPixelShader =
"struct VertexInput { float4 myPosition : POSITION; }; \
struct PixelInput { float4 myPosition : SV_POSITION; }; \
struct PixelOutput { float4 myColor : SV_TARGET; }; \
 \
PixelOutput main(PixelInput input) \
{ \
PixelOutput output; \
output.myColor = float4(1.f, 0.3f, 0.5f, 1.f); \
return output; \
} ";

std::string defaultGeometryShader =
"\
struct Meep{float4 myPosition : SV_POSITION; }; \
[maxvertexcount(1)] \
void main(point Meep input[1], inout TriangleStream<Meep> output) \
{ \
output.Append(input[0]); \
output.RestartStrip(); \
} ";

ID3DBlob* _CompileDefaultShader(EShaderProfile aProfile)
{
	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifndef _RETAIL
    flags |= D3DCOMPILE_DEBUG;
#endif

	LPCSTR profile;
	switch (aProfile)
	{
	case EShaderProfile_Vertex:
		profile = "vs_5_0";
		break;
	case EShaderProfile_Pixel:
		profile = "ps_5_0";
		break;
	case EShaderProfile_Geometry:
		profile = "gs_5_0";
		break;
	default:
		profile = "cs_5_0";
		break;
	}
    ID3DBlob* shaderBlob = NULL;
    ID3DBlob* errorBlob = NULL;

	HRESULT result;

	std::string shader;
	if (aProfile == EShaderProfile_Vertex)
	{
		shader = defaultVertexShader;
	}
	else if (aProfile == EShaderProfile_Pixel)
	{
		shader = defaultPixelShader;
	}
	else if (aProfile == EShaderProfile_Geometry)
	{
		shader = defaultGeometryShader;
	}

	result = D3DCompile(shader.data(), shader.size(), NULL, NULL, NULL, "main", profile, flags, NULL, &shaderBlob, &errorBlob);

	if (FAILED(result))
	{
		if (errorBlob)
		{
			std::string errorText(static_cast<char*>(errorBlob->GetBufferPointer()));
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to load default shader!\n%s", errorText.c_str());
			errorBlob->Release();
		}

		if (shaderBlob)
		{
			shaderBlob->Release();
			shaderBlob = NULL;
		}
	}

	return shaderBlob;
}

ID3DBlob* _CompileShader(const std::wstring & aShaderFile, EShaderProfile aProfile)
{
	UINT flags = D3DCOMPILE_ENABLE_STRICTNESS;
#ifndef _RETAIL
    flags |= D3DCOMPILE_DEBUG;
#endif

	LPCSTR profile;
	switch (aProfile)
	{
	case EShaderProfile_Vertex:
		profile = "vs_5_0";
		break;
	case EShaderProfile_Pixel:
		profile = "ps_5_0";
		break;
	case EShaderProfile_Geometry:
		profile = "gs_5_0";
		break;
	default:
		profile = "cs_5_0";
		break;
	}
    ID3DBlob* shaderBlob = NULL;
    ID3DBlob* errorBlob = NULL;
	HRESULT result = D3DCompileFromFile(
		aShaderFile.c_str(), NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE,
		"main", profile,
		flags, 0, &shaderBlob, &errorBlob
	);

	if (FAILED(result))
	{
		std::string shaderFile(aShaderFile.begin(), aShaderFile.end());
		if (errorBlob)
		{
			std::string errorText(static_cast<char*>(errorBlob->GetBufferPointer()));
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to load shader: %s\n%s", shaderFile.c_str(), errorText.c_str());
			errorBlob->Release();
		}

		if (shaderBlob)
		{
			shaderBlob->Release();
			shaderBlob = NULL;
		}

		if (result == HRESULT_FROM_WIN32(ERROR_FILE_NOT_FOUND))
		{
			ENGINE_LOG(CONCOL_ERROR, "Can't find shader file: %s! Using default shader", shaderFile.c_str());
		}

		shaderBlob = _CompileDefaultShader(aProfile);
	}

	return shaderBlob;
}

void CVertexShader::CreateInputLayout(ID3D11InputLayout *& aInputLayout, void * aBlob, __int64 aBlobSize, EShaderInputLayoutType aLayoutType, const std::string & aFilename)
{
	aFilename;
	HRESULT result;
	if (aLayoutType == EShaderInputLayoutType_PBR)
	{
		D3D11_INPUT_ELEMENT_DESC layout[5] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "UV", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		result = IEngine::GetDevice()->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), aBlob, aBlobSize, &aInputLayout);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create input layout: %s!", aFilename.c_str());
		}
	}
	else if (aLayoutType == EShaderInputLayoutType_PPFX)
	{
		D3D11_INPUT_ELEMENT_DESC layout[2] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "UV", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		result = IEngine::GetDevice()->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), aBlob, aBlobSize, &aInputLayout);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create input layout: %s!", aFilename.c_str());
		}
	}
	else if(aLayoutType == EShaderInputLayoutType_Sprite)
	{
		D3D11_INPUT_ELEMENT_DESC layout[2] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "UV", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		result = IEngine::GetDevice()->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), aBlob, aBlobSize, &aInputLayout);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create input layout: %s!", aFilename.c_str());
		}
	}
	else if(aLayoutType == EShaderInputLayoutType_Particle)
	{
		//TODO: Fix dis
		D3D11_INPUT_ELEMENT_DESC layout[4] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "SIZE", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "ROTATION", 0, DXGI_FORMAT_R32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		result = IEngine::GetDevice()->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), aBlob, aBlobSize, &aInputLayout);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create input layout: %s!", aFilename.c_str());
		}
	}
	else if(aLayoutType == EShaderInputLayoutType_Debug)
	{
		D3D11_INPUT_ELEMENT_DESC layout[2] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		result = IEngine::GetDevice()->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), aBlob, aBlobSize, &aInputLayout);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create input layout: %s!", aFilename.c_str());
		}
	}
	else
	{
		D3D11_INPUT_ELEMENT_DESC layout[1] =
		{
			{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		result = IEngine::GetDevice()->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), aBlob, aBlobSize, &aInputLayout);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create input layout: %s!", aFilename.c_str());
		}
	}
}

CVertexShader::CVertexShader()
{
	myVertexShader = NULL;
	myInputLayout = NULL;
	myLayoutType = EShaderInputLayoutType_PBR;
}
CVertexShader::~CVertexShader()
{
	SafeRelease(myVertexShader);
	SafeRelease(myInputLayout);
}
void CVertexShader::Bind()
{
	IEngine::GetContext()->VSSetShader(myVertexShader, NULL, NULL);
}
void CVertexShader::BindLayout()
{
	IEngine::GetContext()->IASetInputLayout(myInputLayout);
}
void CVertexShader::Unbind()
{
	IEngine::GetContext()->VSSetShader(NULL, NULL, NULL);
}
void CVertexShader::UnbindLayout()
{
	IEngine::GetContext()->IASetInputLayout(NULL);
}
void CVertexShader::CompileShader(const std::wstring & aShaderFile, EShaderInputLayoutType aInputLayoutType)
{
	myLayoutType = aInputLayoutType;

	auto shaderCompile = [&](const std::wstring& aFilename)
	{
		if (myVertexShader)
		{
			SafeRelease(myVertexShader);
		}
		if (myInputLayout)
		{
			SafeRelease(myInputLayout);
		}

		std::string fileName(aFilename.begin(), aFilename.end());
		ENGINE_LOG(CONCOL_DEFAULT, "Compiling shader: %s", fileName.c_str());
		ID3DBlob* blob = _CompileShader(aFilename, EShaderProfile_Vertex);
		if (blob)
		{
			IEngine::GetDevice()->CreateVertexShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &myVertexShader);
			CreateInputLayout(myInputLayout, blob->GetBufferPointer(), blob->GetBufferSize(), myLayoutType, fileName);
		}
		else
		{
			myVertexShader = NULL;
			myInputLayout = NULL;
		}
	};

	shaderCompile(aShaderFile);
	IEngine::GetFileWatcher().WatchFile(aShaderFile, shaderCompile);
}

CPixelShader::CPixelShader()
{
	myPixelShader = NULL;
}
CPixelShader::~CPixelShader()
{
	SafeRelease(myPixelShader);
}
void CPixelShader::Bind()
{
	IEngine::GetContext()->PSSetShader(myPixelShader, NULL, NULL);
}
void CPixelShader::Unbind()
{
	IEngine::GetContext()->PSSetShader(NULL, NULL, NULL);
}
void CPixelShader::CompileShader(const std::wstring & aShaderFile)
{
	auto shaderCompile = [&](const std::wstring& aFilename)
	{
		std::string fileName(aFilename.begin(), aFilename.end());
		ENGINE_LOG(CONCOL_DEFAULT, "Compiling shader: %s", fileName.c_str());
		ID3DBlob* blob = _CompileShader(aFilename, EShaderProfile_Pixel);
		if (blob)
		{
			IEngine::GetDevice()->CreatePixelShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &myPixelShader);
			blob->Release();
		}
		else
		{
			myPixelShader = NULL;
		}
	};

	shaderCompile(aShaderFile);
	IEngine::GetFileWatcher().WatchFile(aShaderFile, shaderCompile);
}

CGeometryShader::CGeometryShader()
{
	myGeometryShader = NULL;
}
CGeometryShader::~CGeometryShader()
{
	SafeRelease(myGeometryShader);
}
void CGeometryShader::Bind()
{
	IEngine::GetContext()->GSSetShader(myGeometryShader, NULL, NULL);
}
void CGeometryShader::Unbind()
{
	IEngine::GetContext()->GSSetShader(NULL, NULL, NULL);
}
void CGeometryShader::CompileShader(const std::wstring & aShaderFile)
{
	auto shaderCompile = [&](const std::wstring& aFilename)
	{
		std::string fileName(aFilename.begin(), aFilename.end());
		ENGINE_LOG(CONCOL_DEFAULT, "Compiling shader: %s", fileName.c_str());
		ID3DBlob* blob = _CompileShader(aFilename, EShaderProfile_Geometry);
		if (blob)
		{
			IEngine::GetDevice()->CreateGeometryShader(blob->GetBufferPointer(), blob->GetBufferSize(), NULL, &myGeometryShader);
			blob->Release();
		}
		else
		{
			myGeometryShader = NULL;
		}
	};

	shaderCompile(aShaderFile);
	IEngine::GetFileWatcher().WatchFile(aShaderFile, shaderCompile);
}

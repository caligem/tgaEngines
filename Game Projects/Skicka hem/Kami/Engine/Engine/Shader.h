#pragma once

class CShaderManager;

struct ID3D11VertexShader;
struct ID3D11PixelShader;
struct ID3D11GeometryShader;
struct ID3D11InputLayout;

#include <string>

enum EShaderInputLayoutType
{
	EShaderInputLayoutType_PBR,
	EShaderInputLayoutType_PPFX,
	EShaderInputLayoutType_Sprite,
	EShaderInputLayoutType_Particle,
	EShaderInputLayoutType_Debug
};

class CVertexShader
{
public:
	CVertexShader();
	~CVertexShader();

	void Bind();
	void BindLayout();
	void Unbind();
	void UnbindLayout();

private:
	friend CShaderManager;

	void CompileShader(const std::wstring& aShaderFile, EShaderInputLayoutType aInputLayoutType);
	void CreateInputLayout(ID3D11InputLayout*& aInputLayout, void* aBlob, __int64 aBlobSize, EShaderInputLayoutType aLayoutType, const std::string& aFilename);

	ID3D11VertexShader* myVertexShader;
	ID3D11InputLayout* myInputLayout;
	EShaderInputLayoutType myLayoutType;

};

class CPixelShader
{
public:
	CPixelShader();
	~CPixelShader();

	void Bind();
	void Unbind();

private:
	friend CShaderManager;

	void CompileShader(const std::wstring& aShaderFile);

	ID3D11PixelShader* myPixelShader;

};

class CGeometryShader
{
public:
	CGeometryShader();
	~CGeometryShader();

	void Bind();
	void Unbind();

private:
	friend CShaderManager;

	void CompileShader(const std::wstring& aShaderFile);

	ID3D11GeometryShader* myGeometryShader;

};


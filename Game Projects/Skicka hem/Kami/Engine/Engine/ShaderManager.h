#pragma once

#include <string>
#include "Shader.h"

class CShaderManager
{
public:
	CShaderManager();
	~CShaderManager();

	CVertexShader& GetVertexShader(const std::wstring& aShaderFile, EShaderInputLayoutType aLayoutType);
	CPixelShader& GetPixelShader(const std::wstring& aShaderFile);
	CGeometryShader& GetGeometryShader(const std::wstring& aShaderFile);

private:
	std::map<std::wstring, CVertexShader> myVertexShaders;
	std::map<std::wstring, CPixelShader> myPixelShaders;
	std::map<std::wstring, CGeometryShader> myGeometryShaders;
	
};


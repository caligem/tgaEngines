#include "stdafx.h"
#include "SkyboxRenderer.h"

#include "IEngine.h"
#include "ConstantBuffer.h"

#include "ModelLoader.h"
#include "ModelManager.h"
#include "TextureBuilder.h"
#include "ShaderManager.h"
#include "Shader.h"

CSkyboxRenderer::CSkyboxRenderer()
{
}

CSkyboxRenderer::~CSkyboxRenderer()
{
}

bool CSkyboxRenderer::Init()
{
	myModel = IEngine::GetModelManager().myModelLoader.LoadCube();
	myModel.myTextureData.myTextures[0] = CTextureBuilder::CreateTextureFromFile("Assets/CubeMaps/skybox.dds");

	myVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/Model/Model.vs", EShaderInputLayoutType_PBR);
	myPixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/Model/Skybox.ps");

	myTransform[0] = -100.f;
	myTransform[5] = 100.f;
	myTransform[10] = 100.f;

	if (!myInstanceBuffer.Init(sizeof(SInstanceBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create SkyboxRenderer::myInstanceBuffer");
		return false;
	}

	return true;
}

void CSkyboxRenderer::Render(CConstantBuffer& aCameraBuffer, const CommonUtilities::Vector3f& aCameraPosition)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);

	SInstanceBufferData instanceData;

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	myVertexShader->Bind();
	myVertexShader->BindLayout();
	myPixelShader->Bind();

	const SVertexDataWrapper& vertexData = myModel.myVertexData[0];
	STextureDataWrapper& textureData = myModel.myTextureData;
	
	myTransform[12] = aCameraPosition.x;
	myTransform[13] = aCameraPosition.y;
	myTransform[14] = aCameraPosition.z;
	instanceData.myToWorld = myTransform;

	myInstanceBuffer.SetData(&instanceData);

	context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
	context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

	context->PSSetShaderResources(0, 1, textureData.myTextures);

	context->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);

	myVertexShader->Unbind();
	myVertexShader->UnbindLayout();
	myPixelShader->Unbind();
}

void CSkyboxRenderer::SetCubemapTexture(const char * aCubemapTexture)
{
	myModel.myTextureData.myTextures[0]->Release();
	myModel.myTextureData.myTextures[0] = CTextureBuilder::CreateTextureFromFile(aCubemapTexture);
}

#pragma once

#include "Model.h"
#include "ConstantBuffer.h"

class CVertexShader;
class CPixelShader;

class CSkyboxRenderer
{
public:
	CSkyboxRenderer();
	~CSkyboxRenderer();

	bool Init();
	void Render(CConstantBuffer& aCameraBuffer, const CommonUtilities::Vector3f& aCameraPosition);

	void SetCubemapTexture(const char* aCubemapTexture);

	ID3D11ShaderResourceView** GetSkyboxResource() { return myModel.myTextureData.myTextures; }

private:
	struct SInstanceBufferData
	{
		CommonUtilities::Matrix44f myToWorld;
	};

	CConstantBuffer myInstanceBuffer;

	CModel::SModelData myModel;
	CommonUtilities::Matrix44f myTransform;

	CVertexShader* myVertexShader;
	CPixelShader* myPixelShader;

};


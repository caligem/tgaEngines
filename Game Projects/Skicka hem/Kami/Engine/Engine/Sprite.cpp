#include "stdafx.h"
#include "Sprite.h"

#include "IEngine.h"

#include "DDSTextureLoader.h"
#include "TextureBuilder.h"

#include "DL_Debug.h"
#include "DXMacros.h"

CSprite::CSprite()
{
	myTexture = NULL;
}

CSprite::~CSprite()
{
	SafeRelease(myTexture);
}

void CSprite::Init(const std::string & aFilename)
{
	if (myTexture)
	{
		SafeRelease(myTexture);
	}

	myTexture = CTextureBuilder::CreateTextureFromFile(aFilename);
}

#pragma once

#include <string>
#include <Vector.h>

class C2DRenderer;
class CSpriteComponent;

struct ID3D11ShaderResourceView;

class CSprite
{
public:
	CSprite();
	~CSprite();

	void Init(const std::string& aFilename);

private:
	friend C2DRenderer;
	friend CSpriteComponent;

	ID3D11ShaderResourceView* myTexture;
	CommonUtilities::Vector2f myOriginalTextureSize;
};


#include "stdafx.h"
#include "SpriteComponent.h"
#include "SpriteManager.h"

#include "IEngine.h"
#include "TextureBuilder.h"

CSpriteManager* CSpriteComponent::ourSpriteManager = nullptr;

CSpriteComponent::CSpriteComponent()
{
}


CSpriteComponent::~CSpriteComponent()
{
}

bool CSpriteComponent::Init(const char * aFilePath)
{
	myData.myPosition = { 0.f, 0.f };
	myData.myScale = { 1.f, 1.f };
	myData.myPivot = { 0.0f, 0.0f };
	myData.myUVOffset = { 0.0f, 0.0f };
	myData.myUVScale = { 1.0f, 1.0f };
	myData.myTint = { 1.0f, 1.0f, 1.0f, 1.0f };
	myData.myRotation = 0.f;
	myData.myPriority = 0;

	mySpriteID = ourSpriteManager->AcquireSprite(aFilePath);
	myData.myOriginalTextureSize = CTextureBuilder::GetTextureSize(ourSpriteManager->GetSprite(mySpriteID)->myTexture);
	return true;
}

void CSpriteComponent::SetScaleRelativeToScreen(const CommonUtilities::Vector2f & aScale)
{
	myData.myScale = {
		(1920.f / myData.myOriginalTextureSize.x) * aScale.x,
		(1080.f / myData.myOriginalTextureSize.y) * aScale.y
	};
}

void CSpriteComponent::Release()
{
	ourSpriteManager->ReleaseSprite(mySpriteID);
	mySpriteID = ID_T_INVALID(CSprite);
}

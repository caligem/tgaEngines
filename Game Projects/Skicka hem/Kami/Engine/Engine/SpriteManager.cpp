#include "stdafx.h"
#include "SpriteManager.h"


CSpriteManager::CSpriteManager()
	: mySprites(128)
{
}


CSpriteManager::~CSpriteManager()
{
}

bool CSpriteManager::Init()
{
	return true;
}

ID_T(CSprite) CSpriteManager::AcquireSprite(const char * aTexturePath)
{
	ID_T(CSprite) id = mySprites.Acquire();
	mySprites.GetObj(id)->Init(aTexturePath);
	return id;
}

void CSpriteManager::ReleaseSprite(ID_T(CSprite) aSpriteID)
{
	mySprites.Release(aSpriteID);
}

CSprite * CSpriteManager::GetSprite(ID_T(CSprite) aSpriteID)
{
	return mySprites.GetObj(aSpriteID);
}

#include "stdafx.h"
#include "StateStack.h"
#include "../Game/State.h"
#include "IWorld.h"

CStateStack::CStateStack()
{    
	CState::ourStateStack = this;
	myStates.Init(4);
	myWorkerPool.Init(1);
	myFadeState = EFadeState_Done;
}

CStateStack::~CStateStack()
{
	myWorkerPool.Destroy();
	for (auto& ga : myStates)
	{
		ga.DeleteAll();
	}
}

bool CStateStack::Init(CState * aLoadingScreenState)
{
	myLoadingScreenState = aLoadingScreenState;
	if (!myLoadingScreenState->Init())
	{
		return false;
	}

	return true;
}

bool CStateStack::Update()
{
	if (GetCurrentState() != nullptr)
	{
		float dt = IWorld::Time().GetDeltaTime();

		switch (myFadeState)
		{
		case CStateStack::EFadeState_FadingIn:
			myFadeTimer += dt;

			IWorld::SetFadeColor({ 0.f, 0.f, 0.f, myFadeTimer / myFadeDuration });

			if (myFadeTimer > myFadeDuration)
			{
				myFadeTimer = 0.f;
				myFadeState = EFadeState_FadingOut;
				if (myFinishedFadingCallback)
				{
					myFinishedFadingCallback();
				}
			}
			break;

		case CStateStack::EFadeState_FadingOut:
			myFadeTimer += dt;
			IWorld::SetFadeColor({ 0.f, 0.f, 0.f, 1.f - myFadeTimer / myFadeDuration });

			if (myFadeTimer > myFadeDuration)
			{
				myFadeTimer = 0.f;
				myFadeState = EFadeState_Done;
			}

			break;

		default:
			break;
		}
		int stateStatus = EDoNothing;
		if (myShowLoadingScreen && myFadeState == EFadeState_Done)
		{
			myLoadingScreenState->Update();
		}
		else
		{
			stateStatus = GetCurrentState()->Update();
		}

		if (myIsLoading)
		{
			return true;
		}

		if (stateStatus == EStateUpdate::EPop_Main)
		{
			PopMainState();
		}
		else if (stateStatus == EStateUpdate::EPop_Sub)
		{
			PopSubState();
		}
		else if (stateStatus == EStateUpdate::EPop_Sub_Push_Sub)
		{
			CState* newStateToPush = GetCurrentState()->GetNewStateToPush();
			PopSubState();
			PushSubState(newStateToPush);
		}
		else if (stateStatus == EStateUpdate::EPop_Sub_Push_Main)
		{
			CState* newStateToPush = GetCurrentState()->GetNewStateToPush();
			PopSubState();
			PushMainState(newStateToPush);
		}
		else if (stateStatus == EStateUpdate::EPop_Main_Push_Sub)
		{
			CState* newStateToPush = GetCurrentState()->GetNewStateToPush();
			PopMainState();
			PushSubState(newStateToPush);
		}
		else if (stateStatus == EStateUpdate::EPop_Main_Push_Main)
		{
			CState* newStateToPush = GetCurrentState()->GetNewStateToPush();
			PopMainState();
			PushMainState(newStateToPush);
		}
		else if ( stateStatus == EStateUpdate::EQuitGame)
		{
			return false;
		}
		return true;
	}
	return false;
}

void CStateStack::PushMainState(CState* aMainState)
{
	myIsLoading = false;
	myShowLoadingScreen = false;
	if (!myStates.Empty())
	{
		GetCurrentState()->OnLeave();
	}
	myStates.Add(CommonUtilities::GrowingArray<CState*>(5));
	myStates.GetLast().Add(aMainState);
	GetCurrentState()->OnEnter();
}

void CStateStack::PushSubState(CState * aSubState)
{
	assert(myStates.Size() != 0);
	myIsLoading = false;
	myShowLoadingScreen = false;
	GetCurrentState()->OnLeave();
	myStates.GetLast().Add(aSubState);
	GetCurrentState()->OnEnter();
}

void CStateStack::PushAndPopState(CState * aMainState)
{
	myIsLoading = false;
	myShowLoadingScreen = false;
	PopMainState();
	PushMainState(aMainState);
}

void CStateStack::PopMainState()
{
	if (myFadeState == EFadeState_FadingIn)return;
	for (int i = myStates.GetLast().Size() - 1; i >= 0; --i)
	{
		myStates.GetLast().GetLast()->OnLeave();
		myStates.GetLast().GetLast()->DestoryScene();
		myStates.GetLast().DeleteCyclicAtIndex(static_cast<unsigned short>(i));
	}
	myStates.RemoveCyclicAtIndex(myStates.Size() - 1);

	if (GetCurrentState() != nullptr)
	{
		GetCurrentState()->OnEnter();
	}
}

void CStateStack::PopSubState()
{
	if (myFadeState == EFadeState_FadingIn)return;
	if (myStates.GetLast().Size() != 0)
	{
		GetCurrentState()->OnLeave();
		myStates.GetLast().GetLast()->DestoryScene();
		myStates.GetLast().DeleteCyclicAtIndex(myStates.GetLast().Size() - 1);
		if (myStates.GetLast().Size() != 0)
		{
			myStates.GetLast().GetLast()->OnEnter();
		}
		else
		{
			PopMainState();
		}
	}
}

CState* CStateStack::GetCurrentState()
{
	if (myStates.Empty())
	{
		return nullptr;
	}

	if (myStates.GetLast().Empty())
	{
		return nullptr;
	}

	return myStates.GetLast().GetLast();
}

const CState* CStateStack::GetCurrentState() const
{
	if (myStates.Empty())
	{
		return nullptr;
	}

	if (myStates.GetLast().Empty())
	{
		return nullptr;
	}

	return myStates.GetLast().GetLast();
}

bool CStateStack::LoadAsync(const std::function<void()>& aJob, bool aShowLoadingscreen /*= true*/)
{
	if (myFadeState != EFadeState_Done) return false;
	StartFadeIn([=] {
		myWorkerPool.DoWork(aJob);
		if (aShowLoadingscreen && myLoadingScreenState)
		{
			myLoadingScreenState->OnEnter();
			myShowLoadingScreen = aShowLoadingscreen;
		}
		myIsLoading = true;
	});
	return true;
}

void CStateStack::StartFadeIn(std::function<void()> aCallback)
{
	myFadeTimer = 0.f;
	myFadeState = EFadeState_FadingIn;
	myFinishedFadingCallback = aCallback;
}

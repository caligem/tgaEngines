#include "stdafx.h"
#include "TextComponent.h"

#include "TextManager.h"

#include "IWorld.h"

CTextManager* CTextComponent::ourTextManager = nullptr;

CTextComponent::CTextComponent()
{
}

CTextComponent::~CTextComponent()
{
}

void CTextComponent::Init(const char * aFontPath)
{
	myFontID = ourTextManager->AcquireFont(aFontPath);

	myRenderCommands.Init(16);

	myData.myPosition = { 0.f, 0.f };
	myData.myScale = { 1.f, 1.f };
	myData.myPivot = { 0.0f, 0.0f };
	myData.myTint = { 1.0f, 1.0f, 1.0f, 1.0f };
	myData.myRotation = 0.f;
}

void CTextComponent::SetText(const std::string & aString)
{
	if (myText == aString)return;
	myText = aString;
	myIsDirty = true;
}

void CTextComponent::Invalidate()
{
	//ENGINE_LOG(CONCOL_DEFAULT, "Text object invalidated!");
	mySize.x = 0.f;

	float currentWidth = 0.f;

	float numLines = 1.f;
	for (char c : myText)
	{
		if (c == '\n')
		{
			++numLines;
			currentWidth = 0.f;
			continue;
		}

		const CFont::SCharData& data = ourTextManager->GetFont(myFontID)->GetData(c);

		currentWidth += data.myXAdvance;

		mySize.x = std::fmaxf(currentWidth, mySize.x);
	}

	mySize.y = numLines * ourTextManager->GetFont(myFontID)->GetLineHeight();

	ResetRenderCommands();

	myIsDirty = false;
}

void CTextComponent::ResetRenderCommands()
{
	myRenderCommands.RemoveAll();

	const CommonUtilities::Vector2f& screenSize = IWorld::GetCanvasSize();

	float relativeScale = screenSize.y / 1080.f;

	CommonUtilities::Vector2f direction = { std::cosf(myData.myRotation), std::sinf(myData.myRotation) };
	CommonUtilities::Vector2f normal = { -direction.y, direction.x };

	CommonUtilities::Vector2f cursor = { 0.f, 0.f };

	cursor.x -= (mySize.x * direction.x * myData.myPivot.x * myData.myScale.x + mySize.y * normal.x * myData.myPivot.y * myData.myScale.y) / screenSize.x * relativeScale;
	cursor.y -= (mySize.x * direction.y * myData.myPivot.x * myData.myScale.x + mySize.y * normal.y * myData.myPivot.y * myData.myScale.y) / screenSize.y * relativeScale;

	float numLines = 0.f;

	CFont* font = ourTextManager->GetFont(myFontID);

	STextRenderCommand command;
	command.mySpriteData.myOriginalTextureSize = font->GetOriginalTextureSize();
	command.mySpriteData.myPivot = { 0.f, 0.f };
	command.mySpriteData.myRotation = myData.myRotation;
	command.mySpriteData.myTint = myData.myTint;
	command.myFontID = myFontID;
	command.myPosition = myData.myPosition;


	for (char c : myText)
	{
		if (c == '\n')
		{
			++numLines;
			cursor = { 0.f, 0.f };

			cursor.x -= (mySize.x * direction.x * myData.myPivot.x * myData.myScale.x + mySize.y * normal.x * myData.myPivot.y * myData.myScale.y) / screenSize.x * relativeScale;
			cursor.y -= (mySize.x * direction.y * myData.myPivot.x * myData.myScale.x + mySize.y * normal.y * myData.myPivot.y * myData.myScale.y) / screenSize.y * relativeScale;

			cursor.x += (numLines*font->GetLineHeight() / screenSize.x) * normal.x * myData.myScale.y * relativeScale;
			cursor.y += (numLines*font->GetLineHeight() / screenSize.y) * normal.y * myData.myScale.y * relativeScale;
		}

		const CFont::SCharData& data = font->GetData(c);

		command.mySpriteData.myPosition = cursor;
		command.mySpriteData.myPosition.x += ((data.myOffset.x / screenSize.x) * direction.x * myData.myScale.x + (data.myOffset.y / screenSize.x) * normal.x * myData.myScale.y) * relativeScale;
		command.mySpriteData.myPosition.y += ((data.myOffset.x / screenSize.y) * direction.y * myData.myScale.x + (data.myOffset.y / screenSize.y) * normal.y * myData.myScale.y) * relativeScale;

		command.mySpriteData.myScale.x = data.myUVScale.x * myData.myScale.x;
		command.mySpriteData.myScale.y = data.myUVScale.y * myData.myScale.y;
		command.mySpriteData.myUVScale = data.myUVScale;
		command.mySpriteData.myUVOffset = data.myUVOffset;

		myRenderCommands.Add(command);

		cursor.x += (data.myXAdvance / screenSize.x) * direction.x * myData.myScale.x * relativeScale;
		cursor.y += (data.myXAdvance / screenSize.y) * direction.y * myData.myScale.x * relativeScale;
	}
}

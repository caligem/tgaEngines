#pragma once
#include "Component.h"

#include <string>
#include "Vector.h"

#include "GrowingArray.h"

#include "Font.h"

#include "RenderCommand.h"

class CEngine;
class CTextManager;
class CGraphicsPipeline;

class CTextComponent : public CComponent
{
public:
	CTextComponent();
	~CTextComponent();

	struct STextBufferData
	{
		CommonUtilities::Vector2f myPosition;
		CommonUtilities::Vector2f myScale;
		CommonUtilities::Vector2f myPivot;
		CommonUtilities::Vector4f myTint;
		float myRotation = 0.f;
	};
	
	void Init(const char* aFontPath);

	void SetText(const std::string& aString);
	void SetPosition(const CommonUtilities::Vector2f& aPosition) { myData.myPosition = aPosition; }
	void SetScale(const CommonUtilities::Vector2f& aScale) { myData.myScale = aScale; myIsDirty = true; }
	void SetPivot(const CommonUtilities::Vector2f& aPivot) { myData.myPivot = aPivot; myIsDirty = true; }
	void SetTint(const CommonUtilities::Vector4f& aTint) { myData.myTint = aTint; myIsDirty = true; }
	void SetRotation(float aRotation) { myData.myRotation = aRotation; myIsDirty = true; }

	const std::string& GetText() const { return myText; }
	const CommonUtilities::Vector2f& GetPosition() const { return myData.myPosition; }
	const CommonUtilities::Vector2f& GetScale() const { return myData.myScale; }
	const CommonUtilities::Vector2f& GetPivot() const { return myData.myPivot; }
	const CommonUtilities::Vector4f& GetTint() const { return myData.myTint; }
	const float& GetRotation() const { return myData.myRotation; }

	const CommonUtilities::Vector2f& GetSize() const { return mySize; }
	const CommonUtilities::GrowingArray<STextRenderCommand>& GetRenderCommands() { if (myIsDirty) { Invalidate(); myIsDirty = false; } return myRenderCommands; }

private:
	friend CEngine;
	friend CGraphicsPipeline;

	static CTextManager* ourTextManager;

	void Invalidate();
	void ResetRenderCommands();

	ID_T(CFont) myFontID;

	std::string myText;

	STextBufferData myData;
	CommonUtilities::Vector2f mySize;

	CommonUtilities::GrowingArray<STextRenderCommand> myRenderCommands;

	bool myIsDirty;

};


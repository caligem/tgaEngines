#pragma once

#include "Font.h"
#include "ObjectPool.h"

class CTextManager
{
public:
	CTextManager();
	~CTextManager();

	bool Init();
	ID_T(CFont) AcquireFont(const char* aFontPath);
	void ReleaseFont(ID_T(CFont) aFontID);

	inline CFont* GetFont(ID_T(CFont) aFontID) { return myFonts.GetObj(aFontID); }
	
private:
	ObjectPool<CFont> myFonts;

	std::map<std::string, ID_T(CFont)> myFontCache;

};


#include "stdafx.h"
#include "TextureDataWrapper.h"

#include "DXMacros.h"

void STextureDataWrapper::ReleaseAllTextures()
{
	for (int i = 0; i < sizeof(myTextures) / sizeof(myTextures[0]); ++i)
	{
		SafeRelease(myTextures[i]);
	}
}


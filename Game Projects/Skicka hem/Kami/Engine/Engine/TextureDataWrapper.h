#pragma once

struct ID3D11ShaderResourceView;

struct STextureDataWrapper
{
public:
	STextureDataWrapper() {
		myTextures[0] = nullptr;
		myTextures[1] = nullptr;
		myTextures[2] = nullptr;
		myTextures[3] = nullptr;
		myTextures[4] = nullptr;
		myTextures[5] = nullptr;
	}
	~STextureDataWrapper() {}

	void ReleaseAllTextures();

	ID3D11ShaderResourceView* myTextures[6] = { nullptr, nullptr, nullptr, nullptr, nullptr, nullptr };
};
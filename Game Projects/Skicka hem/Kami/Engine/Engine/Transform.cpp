#include "stdafx.h"
#include "Transform.h"

#include "ComponentSystem.h"

CComponentSystem* CTransform::ourComponentSystem = nullptr;

CTransform::CTransform(CGameObjectData* aGameObject)
{
	myGameObjectData = aGameObject;
	myScale = { 1.f, 1.f, 1.f };

	myChildren.Init(2);
}
CTransform::CTransform(const CommonUtilities::Matrix44f & aMatrix)
{
	myTransform = aMatrix;

	myOrientation.myPosition = aMatrix.myPosition;

	myScale = { aMatrix.myRightAxis.Length(), aMatrix.myUpAxis.Length(), aMatrix.myForwardAxis.Length() };

	myOrientation.myRightAxis = aMatrix.myRightAxis.GetNormalized();
	myOrientation.myUpAxis = aMatrix.myUpAxis.GetNormalized();
	myOrientation.myForwardAxis = aMatrix.myForwardAxis.GetNormalized();
}
void CTransform::SetParent(CTransform * aParent)
{
	if (myParent)
	{
		myParent->RemoveChild(this);
	}
	myParent = aParent;
	myParent->AddChild(this);
}
void CTransform::AddChild(CTransform * aChild)
{
	myChildren.Add(aChild);
}
void CTransform::RemoveChild(CTransform * aChild)
{
	myChildren.Remove(aChild);
}
void CTransform::InternalSetPosition(const CommonUtilities::Vector3f & aPosition)
{
	myOrientation.myPosition = aPosition;
}
void CTransform::InternalSetRotation(const CommonUtilities::Vector3f & aRotation)
{
	CommonUtilities::Matrix33f buffer = CommonUtilities::Matrix33f::CreateYawPitchRoll(aRotation);

	myOrientation.myRightAxis = buffer.myRightAxis;
	myOrientation.myUpAxis = buffer.myUpAxis;
	myOrientation.myForwardAxis = buffer.myForwardAxis;
}
void CTransform::InternalSetLookAtRotation(const CommonUtilities::Vector3f & aSource, const CommonUtilities::Vector3f & aTarget, const CommonUtilities::Vector3f & aUpVector)
{
	CommonUtilities::Matrix44f buffer = CommonUtilities::LookAt(aTarget, aSource, aUpVector);

	myOrientation.myRightAxis = buffer.myRightAxis;
	myOrientation.myUpAxis = buffer.myUpAxis;
	myOrientation.myForwardAxis = buffer.myForwardAxis;
}
void CTransform::InternalMove(const CommonUtilities::Vector3f & aMovement)
{
	myOrientation.myPosition += aMovement;
}
void CTransform::InternalRotate(const CommonUtilities::Vector3f& aRotation)
{
	CommonUtilities::Matrix33f buffer = myOrientation;
	buffer *= CommonUtilities::Matrix33f::CreateYawPitchRoll(aRotation);

	myOrientation.myRightAxis = buffer.myRightAxis.GetNormalized();
	myOrientation.myUpAxis = buffer.myUpAxis.GetNormalized();
	myOrientation.myForwardAxis = buffer.myForwardAxis.GetNormalized();
}
void CTransform::InternalRotateAround(const CommonUtilities::Vector3f& aAxis, float aRotation)
{
	CommonUtilities::Matrix33f buffer = CommonUtilities::Matrix33f(myOrientation) * CommonUtilities::Matrix33f::CreateRotateAround(aAxis, aRotation);

	myOrientation.myRightAxis = buffer.myRightAxis.GetNormalized();
	myOrientation.myUpAxis = buffer.myUpAxis.GetNormalized();
	myOrientation.myForwardAxis = buffer.myForwardAxis.GetNormalized();
}
void CTransform::InternalLookAt(const CommonUtilities::Vector3f & aTarget, const CommonUtilities::Vector3f & aUpVector)
{
	CommonUtilities::Matrix44f buffer = CommonUtilities::LookAt(aTarget, myOrientation.myPosition, aUpVector);

	myOrientation.myRightAxis = buffer.myRightAxis;
	myOrientation.myUpAxis = buffer.myUpAxis;
	myOrientation.myForwardAxis = buffer.myForwardAxis;
}
void CTransform::Invalidate() const
{
	myLocalTransform = myOrientation;
	myLocalTransform.myRightAxis *= myScale.x;
	myLocalTransform.myUpAxis *= myScale.y;
	myLocalTransform.myForwardAxis *= myScale.z;

	if (myParent)
	{
		myTransform = myLocalTransform * myParent->GetMatrix();
	}
	else
	{
		myTransform = myLocalTransform;
	}

	myIsDirty = false;
}
void CTransform::FireDirtyChain()
{
	myIsDirty = true;
	for (CTransform* child : myChildren)
	{
		child->FireDirtyChain();
	}
}
void CTransform::Destroy()
{
	if (myParent)
	{
		myParent->RemoveChild(this);
	}
	for (unsigned short i = myChildren.Size(); i > 0; --i)
	{
		myChildren[i-1]->Destroy();
	}

	ourComponentSystem->DestroyGameObject(myGameObjectData->myID, myGameObjectData->mySceneID);
}

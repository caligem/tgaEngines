#include "stdafx.h"
#include "CreditState.h"

#include "IWorld.h"
#include "XBOXController.h"
#include "GButton.h"

#include "AudioManager.h"
#include "AudioChannels.h"

CCreditsState::CCreditsState()
{
	myShouldPop = false;
}


CCreditsState::~CCreditsState()
{
}

bool CCreditsState::Init()
{
	CState::Init();

	myBackground.Init(mySceneID);
	myBackground.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/creditsBack.dds");
	myBackground.GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myBackground.GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.5f, 0.5f));
	myBackground.SetActive(true);

	for (int i = 0; i < ECreditButtons_Count; ++i)
	{
		myCreditButtons[i].Init(mySceneID);
		myCreditButtons[i].SetActive(true);
	}
	myCreditButtons[ECreditButtons_Back].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/backButton.dds");
	myCreditButtons[ECreditButtons_Back].GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.92f, 0.92f));
	for (int i = 0; i < ECreditButtons_Count; ++i)
	{
		myCreditButtons[i].GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	}

	myGUI.Init(0.6f);
	AddButtons();
	return true;
}

EStateUpdate CCreditsState::Update()
{
	myGUI.Update();
	if (IWorld::Input().IsKeyPressed(Input::Key_Escape) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_Back) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_B) || myShouldPop)
	{
		return EPop_Sub;
	}
	return EDoNothing;
}

void CCreditsState::OnEnter()
{
	SetActiveScene();
}

void CCreditsState::OnLeave()
{
}

void CCreditsState::AddButtons()
{
	CGButton* back = new CGButton();
	back->SetOnMouseReleased(std::bind(&CCreditsState::Back, this));
	back->SetOnMouseOverStart(std::bind(&CCreditsState::OnHoverButton, this, ECreditButtons_Back));
	back->SetOnMouseOverEnd(std::bind(&CCreditsState::EndHoverButton, this, ECreditButtons_Back));
	back->SetPosition(myCreditButtons[ECreditButtons_Back].GetComponent<CSpriteComponent>()->GetPosition());
	back->SetSize(200.f, 140.f);
	myGUI.AddGUIObject(back);

	myGUI.SetDefaultFocusObject(back);
}

void CCreditsState::Back()
{
	AM.PlayNewInstance("SFX/Buttons/ButtonBack", nullptr, AudioChannel::SoundEffects);
	myShouldPop = true;
}

void CCreditsState::OnHoverButton(EHowToPlayButtons aButton)
{
	myCreditButtons[aButton].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.06f, 1.06f));
	myCreditButtons[aButton].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.1f, 1.1f, 1.1f, 1.0f));
}

void CCreditsState::EndHoverButton(EHowToPlayButtons aButton)
{
	myCreditButtons[aButton].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.f, 1.f));
	myCreditButtons[aButton].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
}

#pragma once
#include "State.h"
#include "GUI.h"
class CCreditsState : public CState
{
public:
	enum EHowToPlayButtons
	{
		ECreditButtons_Back,
		ECreditButtons_Count
	};
	CCreditsState();
	~CCreditsState();

	bool Init();
	EStateUpdate Update() override;
	void OnEnter() override;
	void OnLeave() override;
private:
	void AddButtons();
	void Back();
	void OnHoverButton(EHowToPlayButtons aButton);
	void EndHoverButton(EHowToPlayButtons aButton);
	CGUI myGUI;
	std::array<CGameObject, ECreditButtons_Count> myCreditButtons;
	CGameObject myBackground;
	bool myShouldPop;
};


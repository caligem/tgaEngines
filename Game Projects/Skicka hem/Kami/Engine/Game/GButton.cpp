#include "stdafx.h"
#include "GButton.h"
#include "AudioManager.h"


CGButton::CGButton()
{
	myOnMouseClicked = nullptr;
	myOnMouseDown = nullptr;
	myOnMouseReleased = nullptr;
	myOnMouseOverStart = nullptr;
	myOnMouseOver = nullptr;
	myOnMouseOverEnd = nullptr;
}


CGButton::~CGButton()
{
}

void CGButton::SetOnMouseClicked(std::function<void()> aClickFunction)
{
	myOnMouseClicked = aClickFunction;
}

void CGButton::SetOnMouseDown(std::function<void()> aMouseDownFunction)
{
	myOnMouseDown = aMouseDownFunction;
}

void CGButton::SetOnMouseReleased(std::function<void()> aMouseReleasedFunction)
{
	myOnMouseReleased = aMouseReleasedFunction;
}

void CGButton::SetOnMouseOverStart(std::function<void()> aOnMouseOverStartFunction)
{
	myOnMouseOverStart = aOnMouseOverStartFunction;
}

void CGButton::SetOnMouseOver(std::function<void()> aOnMouseOverFunction)
{
	myOnMouseOver = aOnMouseOverFunction;
}

void CGButton::SetOnMouseOverEnd(std::function<void()> aMouseOverEndFunction)
{
	myOnMouseOverEnd = aMouseOverEndFunction;
}

void CGButton::OnPressed()
{
	myIsClicked = true;
	if (myOnMouseClicked != nullptr)
	{
		myOnMouseClicked();
	}
}

void CGButton::OnHeldDown()
{
	if (myOnMouseDown != nullptr)
	{
		myOnMouseDown();
	}
}

void CGButton::OnReleased()
{
	if (myIsHovered == true)
	{
		if (myIsClicked == true)
		{
			if (myOnMouseReleased != nullptr)
			{
				myOnMouseReleased();
			}
		}
	}
	myIsClicked = false;
}

void CGButton::OnHoverStart()
{
	myIsHovered = true;
	if (myOnMouseOverStart != nullptr)
	{
		AM.PlayNewInstance("SFX/Buttons/ButtonHover");
		myOnMouseOverStart();
	}
}

void CGButton::OnHoverOver()
{
}

void CGButton::OnHoverEnd()
{
	myIsHovered = false;
	if (myOnMouseOverEnd != nullptr)
	{
		myOnMouseOverEnd();
	}
}

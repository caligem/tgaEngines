#pragma once
#include "GUIBase.h"
#include <functional>

class CGButton : public CGUIBase
{
public:
	CGButton();
	~CGButton();

	void SetOnMouseClicked(std::function<void()> aClickFunction);
	void SetOnMouseDown(std::function<void()> aMouseDownFunction);
	void SetOnMouseReleased(std::function<void()> aMouseReleasedFunction);
	void SetOnMouseOverStart(std::function<void()> aOnMouseOverStartFunction);
	void SetOnMouseOver(std::function<void()> aOnMouseOverFunction);
	void SetOnMouseOverEnd(std::function<void()> aMouseOverEndFunction);

	const bool GetIsClicked() const { return myIsClicked; }
private:
	void OnPressed() override;
	void OnHeldDown() override;
	void OnReleased() override;
	void OnHoverStart() override;
	void OnHoverOver() override;
	void OnHoverEnd() override;

	std::function<void()> myOnMouseClicked;
	std::function<void()> myOnMouseDown;
	std::function<void()> myOnMouseReleased;
	std::function<void()> myOnMouseOverStart;
	std::function<void()> myOnMouseOver;
	std::function<void()> myOnMouseOverEnd;

	bool myIsClicked;
};


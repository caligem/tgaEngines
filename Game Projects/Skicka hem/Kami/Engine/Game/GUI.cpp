#include "stdafx.h"
#include "GUI.h"
#include "GUIBase.h"


#include "IWorld.h"
#include "XBOXController.h"


CGUI::CGUI()
{
	myGUIObjects.Init(5);
	myLastPressedGUIObject = nullptr;
	myMouseOverGUIObject = nullptr;
	myDefaultFocusObject = nullptr;
	myOnlyMouseInput = false;
	myUsingMouse = true;
	myAllowXBoxInput = true;
	myDefaultXBoxInputDelay = 0.2f;
	myCurrentXBoxInputDelay = myDefaultXBoxInputDelay;
	myXBoxInputTimer = 0.f;
}


CGUI::~CGUI()
{
	myGUIObjects.DeleteAll();
}

void CGUI::Init(const float aTimeUntillGuiActivate)
{
	myTimeUntillActive = aTimeUntillGuiActivate;
	if (myDefaultFocusObject != nullptr)
	{
		SetFocusObject(myDefaultFocusObject);
	}
}

void CGUI::Update()
{
	float deltaTime = IWorld::Time().GetDeltaTime();
	if (myTimeUntillActive > 0)
	{
		myTimeUntillActive -= deltaTime;
	}
	else
	{
		if (IWorld::Input().HasMouseMoved()) { myUsingMouse = true; }
		HandleInput();
		myXBoxInputTimer += deltaTime;
		if (myXBoxInputTimer > (myCurrentXBoxInputDelay + 0.1f))
		{
			myAllowXBoxInput = true;
			myXBoxInputTimer = 0.0f;
		}
	}
}

void CGUI::AddGUIObject(CGUIBase * aGUIObject)
{
#ifndef DEBUG
	if (aGUIObject == nullptr)
	{
		GAMEPLAY_LOG(CONCOL_WARNING, "Invaild GUIObject");
		return;
	}
#endif // DEBUG
	myGUIObjects.Add(aGUIObject);
}

bool CGUI::SetDefaultFocusObject(CGUIBase* aGUIObject)
{
	bool setDefaultFocus(false);

	if (myGUIObjects.Find(aGUIObject) != myGUIObjects.FoundNone)
	{
		myDefaultFocusObject = aGUIObject;
		setDefaultFocus = true;
	}

	return setDefaultFocus;
}

void CGUI::SetFocusObject(CGUIBase * aGUIObject)
{
#ifndef DEBUG
	if (aGUIObject == nullptr)
	{
		GAMEPLAY_LOG(CONCOL_WARNING, "Invaild GUIObject");
		return;
	}
#endif // DEBUG
	if (myLastPressedGUIObject == nullptr)
	{
		myLastPressedGUIObject = myDefaultFocusObject;
	}
	SwitchFocusedObject(aGUIObject);
}

void CGUI::SetIfMouseOnlyInput(const bool aIfMouseOnly)
{
	myOnlyMouseInput = aIfMouseOnly;
}

void CGUI::AddTimeForDisableInput(const float aTime)
{
	myTimeUntillActive += aTime;
}

void CGUI::HandleKeyInput()
{
	Input::CInputManager* input = &IWorld::Input();
	CommonUtilities::XBOXController* xbox = &IWorld::XBOX();
	const float xboxStickSensetivity = 0.7f;
	const bool xBoxStickMovedRight(xbox->GetStick(CommonUtilities::XStick_Left).x > xboxStickSensetivity);
	const bool xBoxStickMovedLeft(xbox->GetStick(CommonUtilities::XStick_Left).x < -xboxStickSensetivity);
	const bool xBoxStickMovedUp(xbox->GetStick(CommonUtilities::XStick_Left).y > xboxStickSensetivity);
	const bool xBoxStickMovedDown(xbox->GetStick(CommonUtilities::XStick_Left).y < -xboxStickSensetivity);
	const bool leftArrowButtonPressed(input->IsKeyPressed(Input::Key_Left) || input->IsKeyPressed(Input::Key_A) || xbox->IsButtonPressed(CommonUtilities::XButton_Left));
	const bool rightArrowButtonPressed(input->IsKeyPressed(Input::Key_Right) || input->IsKeyPressed(Input::Key_D) || xbox->IsButtonPressed(CommonUtilities::XButton_Right));
	const bool upArrowButtonPressed(input->IsKeyPressed(Input::Key_Up) || input->IsKeyPressed(Input::Key_W) || xbox->IsButtonPressed(CommonUtilities::XButton_Up));
	const bool downArrowButtonPressed(input->IsKeyPressed(Input::Key_Down) || input->IsKeyPressed(Input::Key_S) || xbox->IsButtonPressed(CommonUtilities::XButton_Down));
	const bool enterButtonPressed(input->IsKeyPressed(Input::Key_Return) || input->IsKeyPressed(Input::Key_Space) || xbox->IsButtonPressed(CommonUtilities::XButton_A));
	const bool enterButtonDown(input->IsKeyDown(Input::Key_Return) || input->IsKeyDown(Input::Key_Space) || xbox->IsButtonDown(CommonUtilities::XButton_A));
	const bool enterButtonReleased(input->IsKeyReleased(Input::Key_Return) || input->IsKeyReleased(Input::Key_Space) || xbox->IsButtonReleased(CommonUtilities::XButton_A));

	const bool anyKeyStateChanged((leftArrowButtonPressed == true) || (rightArrowButtonPressed == true)
		|| (upArrowButtonPressed == true) || (downArrowButtonPressed == true)
		|| (enterButtonPressed == true) || (enterButtonDown == true) || (enterButtonReleased == true));
	const bool hasXboxStateChanged(xBoxStickMovedRight || xBoxStickMovedLeft || xBoxStickMovedUp || xBoxStickMovedDown);

	
	if (anyKeyStateChanged)
	{
		if (myUsingMouse)
		{
			if (myMouseOverGUIObject != nullptr)
			{
				myMouseOverGUIObject->OnHoverEnd();
				myMouseOverGUIObject->myIsHovered = false;
			}
		}
		myUsingMouse = false;

		const bool hasAnyGUIObjectFocus(myLastPressedGUIObject != nullptr);

			if (hasAnyGUIObjectFocus == true)
			{
				CGUIBase* beforeSwitchGUIObject(myLastPressedGUIObject);
				bool switchedFocus(false);

				if (leftArrowButtonPressed)
				{
					switchedFocus = SwitchFocusedObject(myLastPressedGUIObject->GetConnectedObject(EGUISide::LEFT));
				}
				else if (rightArrowButtonPressed)
				{
					switchedFocus = SwitchFocusedObject(myLastPressedGUIObject->GetConnectedObject(EGUISide::RIGHT));
				}
				else if (upArrowButtonPressed)
				{
					switchedFocus = SwitchFocusedObject(myLastPressedGUIObject->GetConnectedObject(EGUISide::TOP));
				}
				else if (downArrowButtonPressed)
				{
					switchedFocus = SwitchFocusedObject(myLastPressedGUIObject->GetConnectedObject(EGUISide::BOTTOM));
				}
				if (switchedFocus == false)
				{
					myLastPressedGUIObject->OnHoverOver();
				}
				else
				{
					beforeSwitchGUIObject->OnReleased();
				}

				if (enterButtonPressed == true)
				{
					myLastPressedGUIObject->OnPressed();
				}
				else if (enterButtonDown == true)
				{
					myLastPressedGUIObject->OnHeldDown();
				}
				else if (enterButtonReleased == true)
				{
					myLastPressedGUIObject->OnReleased();
				}
			}
			else
			{
#ifndef _RETAIL
				if (myDefaultFocusObject == nullptr)
				{
					GAMEPLAY_LOG(CONCOL_WARNING, "Tried to navigate GUI with keys without any default focus set. Won't work");
				}
				else
				{
#endif // _RETAIL
					myLastPressedGUIObject = myDefaultFocusObject;
					myLastPressedGUIObject->OnHoverStart();

					if (enterButtonPressed == true)
					{
						myLastPressedGUIObject->OnPressed();
					}
#ifndef _RETAIL
				}
#endif // _RETAIL
		}
	}
	else if (hasXboxStateChanged)
	{
		if (myUsingMouse)
		{
			if (myMouseOverGUIObject != nullptr)
			{
				myMouseOverGUIObject->OnHoverEnd();
				myMouseOverGUIObject->myIsHovered = false;
			}
		}
		myUsingMouse = false;
		if (myAllowXBoxInput)
		{

		const bool hasAnyGUIObjectFocus(myLastPressedGUIObject != nullptr);

			if (hasAnyGUIObjectFocus == true)
			{
				CGUIBase* beforeSwitchGUIObject(myLastPressedGUIObject);
				bool switchedFocus(false);

				if (xBoxStickMovedLeft)
				{
					switchedFocus = SwitchFocusedObject(myLastPressedGUIObject->GetConnectedObject(EGUISide::LEFT));
					myAllowXBoxInput = false;
					myCurrentXBoxInputDelay -= IWorld::Time().GetDeltaTime() * 10.f;
				}
				else if (xBoxStickMovedRight)
				{
					switchedFocus = SwitchFocusedObject(myLastPressedGUIObject->GetConnectedObject(EGUISide::RIGHT));
					myAllowXBoxInput = false;
					myCurrentXBoxInputDelay -= IWorld::Time().GetDeltaTime() * 10.f;
				}
				else if (xBoxStickMovedUp)
				{
					switchedFocus = SwitchFocusedObject(myLastPressedGUIObject->GetConnectedObject(EGUISide::TOP));
					myAllowXBoxInput = false;
					myCurrentXBoxInputDelay -= IWorld::Time().GetDeltaTime() * 10.f;
				}
				else if (xBoxStickMovedDown)
				{
					switchedFocus = SwitchFocusedObject(myLastPressedGUIObject->GetConnectedObject(EGUISide::BOTTOM));
					myAllowXBoxInput = false;
					myCurrentXBoxInputDelay -= IWorld::Time().GetDeltaTime() * 10.f;
				}

				if (switchedFocus == false)
				{
					myLastPressedGUIObject->OnHoverOver();
				}
				else
				{
					beforeSwitchGUIObject->OnReleased();
				}

				if (enterButtonPressed == true)
				{
					myLastPressedGUIObject->OnPressed();
				}
				else if (enterButtonDown == true)
				{
					myLastPressedGUIObject->OnHeldDown();
				}
				else if (enterButtonReleased == true)
				{
					myLastPressedGUIObject->OnReleased();
				}
		}
		else
		{
#ifndef _RETAIL
			if (myDefaultFocusObject == nullptr)
			{
				GAMEPLAY_LOG(CONCOL_WARNING, "Tried to navigate GUI with keys without any default focus set, won't work");
			}
			else
			{
#endif // _RETAIL
				myLastPressedGUIObject = myDefaultFocusObject;
				myLastPressedGUIObject->OnHoverStart();

				if (enterButtonPressed == true)
				{
					myLastPressedGUIObject->OnPressed();
				}
#ifndef _RETAIL
			}
#endif // _RETAIL
		}
		}
	}
	else
	{
		myCurrentXBoxInputDelay = myDefaultXBoxInputDelay;
	}
}

bool CGUI::SwitchFocusedObject(CGUIBase* aGUIObject)
{
	if (aGUIObject != nullptr)
	{
		myLastPressedGUIObject->OnHoverEnd();
		myLastPressedGUIObject->myIsHovered = false;
		myLastPressedGUIObject = aGUIObject;
		myLastPressedGUIObject->OnHoverStart();
		myLastPressedGUIObject->myIsHovered = true;

		return true;
	}

	return false;
}

bool CGUI::CheckIfMouseIsOver(const CommonUtilities::Vector2f & aTopLeftPosition, const CommonUtilities::Vector2f & aBottomRightPosition)
{
	const CommonUtilities::Vector2f& mousePosition(IWorld::Input().GetMousePosition());
	const CommonUtilities::Vector2f& windowSize(IWorld::GetWindowSize());
	CommonUtilities::Vector2f mousePosInZeroTooOne(mousePosition.x / windowSize.x, mousePosition.y / windowSize.y);
	if (((aTopLeftPosition.x < mousePosInZeroTooOne.x) && (aTopLeftPosition.y < mousePosInZeroTooOne.y))
		&& ((aBottomRightPosition.x > mousePosInZeroTooOne.x) && (aBottomRightPosition.y > mousePosInZeroTooOne.y)))
	{
		return true;
	}

	return false;
}

void CGUI::HandleInput()
{
	const bool leftMouseButtonPressed(IWorld::Input().IsButtonPressed(Input::Button_Left));
	const bool leftMouseButtonDown(IWorld::Input().IsButtonDown(Input::Button_Left));
	const bool leftMouseButtonReleased(IWorld::Input().IsButtonReleased(Input::Button_Left));

	if ((leftMouseButtonReleased == true) && (myLastPressedGUIObject != nullptr))
	{
		myLastPressedGUIObject->OnReleased();
		myLastPressedGUIObject = nullptr;
	}

	if (!myOnlyMouseInput)
	{
		HandleKeyInput();
	}
	for (unsigned short guiObjectIndex = 0; guiObjectIndex < myGUIObjects.Size(); ++guiObjectIndex)
	{
		CGUIBase*& currentGUIObject(myGUIObjects[guiObjectIndex]);

		if (myUsingMouse)
		{
			CommonUtilities::Vector2f windowSize = IWorld::GetWindowSize();
			float relativScale = windowSize.y / 1080.f;
			CommonUtilities::Vector2f topLeftPosition((currentGUIObject->myPosition.x) - ((currentGUIObject->mySize.x / windowSize.x) * relativScale / 2),
				(currentGUIObject->myPosition.y) - ((currentGUIObject->mySize.y / windowSize.x) * relativScale / 2));
			CommonUtilities::Vector2f bottomRightPosition(CommonUtilities::Vector2f((currentGUIObject->myPosition.x) + ((currentGUIObject->mySize.x / windowSize.x) * relativScale / 2),
				(currentGUIObject->myPosition.y) + ((currentGUIObject->mySize.y / windowSize.x) * relativScale / 2)));
			if (CheckIfMouseIsOver(topLeftPosition, bottomRightPosition))
			{
				myMouseOverGUIObject = currentGUIObject;
				if (currentGUIObject->myIsHovered == true)
				{
					currentGUIObject->OnHoverOver();
				}
				else
				{
					currentGUIObject->OnHoverStart();
					currentGUIObject->myIsHovered = true;
				}

				if (leftMouseButtonPressed == true)
				{
					currentGUIObject->OnPressed();
					myLastPressedGUIObject = currentGUIObject;
				}
				else if (leftMouseButtonDown == true)
				{
					currentGUIObject->OnHeldDown();
				}
				else if (leftMouseButtonReleased == true)
				{
					currentGUIObject->OnReleased();
				}
			}
			else if ((currentGUIObject->myIsHovered == true) && (IWorld::Input().HasMouseMoved() == true))
			{
				currentGUIObject->OnHoverEnd();
				currentGUIObject->myIsHovered = false;
			}
		}

		currentGUIObject->Update();
	}
}

#pragma once

class CGUIBase;

class CGUI
{
public:
	CGUI();
	~CGUI();

	void Init(const float aTimeUntillGuiActivate = 0.3f);
	void Update();

	void AddGUIObject(CGUIBase* aGUIObject);
	bool SetDefaultFocusObject(CGUIBase* aGUIObject);
	void SetFocusObject(CGUIBase* aGUIObject);
	void SetIfMouseOnlyInput(const bool aIfMouseOnly);
	void AddTimeForDisableInput(const float aTime);

private:
	void HandleInput();
	void HandleKeyInput();
	bool SwitchFocusedObject(CGUIBase* aGUIObject);
	bool CheckIfMouseIsOver(const CommonUtilities::Vector2f& aTopLeftPosition, const CommonUtilities::Vector2f& aBottomRightPosition);

	CommonUtilities::GrowingArray<CGUIBase*> myGUIObjects;
	CGUIBase* myLastPressedGUIObject;
	CGUIBase* myMouseOverGUIObject;
	CGUIBase* myDefaultFocusObject;
	bool myOnlyMouseInput;
	bool myAllowXBoxInput;
	bool myUsingMouse;
	float myDefaultXBoxInputDelay;
	float myCurrentXBoxInputDelay;
	float myXBoxInputTimer;
	float myTimeUntillActive;
};

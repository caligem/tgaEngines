#include "stdafx.h"
#include "GUIBase.h"


CGUIBase::CGUIBase()
{
	myIsHovered = false;
	myConnectedGUIObjects[0] = nullptr;
	myConnectedGUIObjects[1] = nullptr;
	myConnectedGUIObjects[2] = nullptr;
	myConnectedGUIObjects[3] = nullptr;
}


CGUIBase::~CGUIBase()
{
}

void CGUIBase::Init()
{

}

void CGUIBase::Update()
{

}

CGUIBase * CGUIBase::GetConnectedObject(const EGUISide & aSideToGet)
{
	return myConnectedGUIObjects[aSideToGet];
}

void CGUIBase::SetConnectedObject(CGUIBase* aGUIObject, const EGUISide & aSideToConnect)
{
	myConnectedGUIObjects[aSideToConnect] = aGUIObject;
}

void CGUIBase::SetPosition(float aX, float aY)
{
	myPosition.x = aX;
	myPosition.y = aY;
}

void CGUIBase::SetPosition(const CommonUtilities::Vector2f& aPosition)
{
	myPosition = aPosition;
}

void CGUIBase::SetSize(float aX, float aY)
{
	mySize.x = aX;
	mySize.y = aY;
}

void CGUIBase::OnPressed()
{
}

void CGUIBase::OnHeldDown()
{
}

void CGUIBase::OnReleased()
{
}

void CGUIBase::OnHoverStart()
{
}

void CGUIBase::OnHoverOver()
{
}

void CGUIBase::OnHoverEnd()
{
}

#pragma once

enum EGUISide
{
	RIGHT = 0,
	BOTTOM,
	LEFT,
	TOP
};

class CGUI;

class CGUIBase
{
	friend CGUI;
public:
	CGUIBase();
	virtual ~CGUIBase();

	void Init();
	virtual void Update();

	CGUIBase* GetConnectedObject(const EGUISide& aSideToGet);
	void SetConnectedObject(CGUIBase* aGUIObject, const EGUISide& aSideToConnect);

	void SetPosition(float aX, float aY);
	void SetPosition(const CommonUtilities::Vector2f& aPosition);
	void SetSize(float aX, float aY);
protected:
	virtual void OnPressed();
	virtual void OnHeldDown();
	virtual void OnReleased();
	virtual void OnHoverOver();
	virtual void OnHoverStart();
	virtual void OnHoverEnd();

	bool myIsHovered;

	std::array<CGUIBase*, 4> myConnectedGUIObjects;
	CommonUtilities::Vector2f myPosition;
	CommonUtilities::Vector2f mySize;
private:
};

#include "stdafx.h"
#include "GameState.h"

#include "StateStack.h"
#include "ScorescreenState.h"
#include "PauseMenuState.h"

#include "InputManager.h"
#include "XBOXController.h"
#include "JsonDocument.h"
#include "FileWatcher.h"
#include "AudioManager.h"

#include "iworld.h"
#include "gameobject.h"
#include "GButton.h"
#include "random.h"
#include "mathf.h"
#include "quaternion.h"

#include "icollision.h"
#include "colliderSphere.h"

#include "DL_Debug.h"

#include <Timer.h>
#include <iomanip>

#include <FileDialog.h>

CGameState::CGameState()
	: myRaceTime(0.0f),
	myHasPlayed(false),
	myBirdsAcquiredThisRace(0),
	myHasLevelBeenFinished(false),
	myNumberOfPassedHoops(0),
	myIsTimeAttackOn(false),
	myWasTimeAttackBeatenBefore(false),
	myTimeToBeat(180.0f)
{
	myHowToPlayTimer = 2.f;
}

CGameState::~CGameState()
{
	IWorld::GetFileWatcher().StopWatchingFile(L"settings.json", mySettingsFilewatcherID);
}

void CGameState::Init(const std::string& aFilepath)
{

	//CREATE SCENE
	CState::Init();

	//LOAD PLAYER SCORE
	myScoreDoc.LoadFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), true);

	//Parsing for lvlName. Used when saving score ( SaveScoreToProgressFile() )
	size_t split = aFilepath.find_last_of("\\/");
	std::string fileName = aFilepath.substr(split + 1);
	split = fileName.find_last_of(".");
	myLvlName = fileName.substr(0, split);

	if (myLvlName == "Tutorial")
	{
		myCurrentLevelIndex = 0;
	}
	else if (myLvlName == "Village")
	{
		myCurrentLevelIndex = 1;
	}
	else if (myLvlName == "Pillars")
	{
		myCurrentLevelIndex = 2;
	}
	else if (myLvlName == "Roots")
	{
		myCurrentLevelIndex = 3;
	}

	if (myScoreDoc.Find(myLvlName.c_str()))
	{
		if (myScoreDoc[myLvlName.c_str()].Find("played"))
		{
			myHasPlayed = myScoreDoc[myLvlName.c_str()]["played"].GetBool();
		}
		else
		{
			myHasPlayed = false;
		}

		if (myScoreDoc[myLvlName.c_str()].Find("timeAttackBeaten"))
		{
			myWasTimeAttackBeatenBefore = myScoreDoc[myLvlName.c_str()]["timeAttackBeaten"].GetBool();
		}
		else
		{
			myWasTimeAttackBeatenBefore = false;
		}
	}
	if (myHasPlayed)
	{
		myIsTimeAttackOn = true;
	}

	///////////// LEVEL
	JsonDocument doc;
	doc.LoadFile(aFilepath.c_str());

	LoadColliders(doc);
	LoadGameObjects(doc);
	LoadPointLights(doc);
	LoadTunnelPoints(doc);
	LoadHoops(doc);
	LoadSettings();
	LoadUI();

	auto callback = [&](const std::wstring& aFilepath)
	{
		std::string fileName(aFilepath.begin(), aFilepath.end());
		myUIPositionsDoc.LoadFile(fileName.c_str(), false); //TODO: Set encode to true here and in SaveScoreToFile when we don't need to add anything more

		UpdateUIPositions();
	};

	callback(L"inGameUIPositions.json");
	IWorld::GetFileWatcher().WatchFile(L"inGameUIPositions.json", callback);


	if (!myIsTimeAttackOn)
	{
		myUIObjects[EGameStateUI_Time].SetActive(false);
	}

	myPlayerCamera.PointToObject(myMainCamera);

	myPlayer.Init(mySceneID);
	SetPlayerStartPosition();

	//NEXT HOOP PARTICLE
	myNextHoopParticle.Init(mySceneID);
	myNextHoopParticle.AddComponent<CParticleSystemComponent>("Assets/Particles/nextHoop.json")->Play();
	myNextHoopParticle.GetTransform().SetParent(&myHoops.begin()->GetTransform());

	myHeartParticle.Init(mySceneID);
	myHeartParticle.AddComponent<CParticleSystemComponent>("Assets/Particles/HeartsBird.json")->Stop();
	myHeartParticle.GetTransform().SetParent(&myPlayer.GetTransform());

	myHowToPlayPopup.Init(mySceneID, "Assets/Sprites/Menu/howToPlayPopup.dds", 5.f, 1.f);

	myPlayer.SetCanBoost(true);
}

void CGameState::LoadColliders(JsonDocument& aDoc)
{
	myOBBs.Init(32);
	if (aDoc.Find("myBoxColliders"))
	{
		for (int i = 0; i < aDoc["myBoxColliders"].GetSize(); ++i)
		{
			auto box = aDoc["myBoxColliders"][i];

			CColliderOBB obb;

			obb.SetPosition({
				box["myPosition"]["myX"].GetFloat(),
				box["myPosition"]["myY"].GetFloat(),
				box["myPosition"]["myZ"].GetFloat()
			});
			obb.SetRadius({
				box["myScale"]["myX"].GetFloat() * 0.5f,
				box["myScale"]["myY"].GetFloat() * 0.5f,
				box["myScale"]["myZ"].GetFloat() * 0.5f
			});
			CommonUtilities::Quatf quat(
				box["myRotation"]["myW"].GetFloat(),
				box["myRotation"]["myX"].GetFloat(),
				box["myRotation"]["myY"].GetFloat(),
				box["myRotation"]["myZ"].GetFloat()
			);
			obb.SetOrientation(quat.GetEulerAngles());

			myOBBs.Add(obb);
		}
	}
	mySpheres.Init(32);
	if (aDoc.Find("mySphereColliders"))
	{
		for (int i = 0; i < aDoc["mySphereColliders"].GetSize(); ++i)
		{
			auto obj = aDoc["mySphereColliders"][i];

			CColliderSphere sphere(
			{
				obj["myPosition"]["myX"].GetFloat(),
				obj["myPosition"]["myY"].GetFloat(),
				obj["myPosition"]["myZ"].GetFloat()
			},
				obj["myRadius"].GetFloat()
			);

			mySpheres.Add(sphere);
		}
	}
}

void CGameState::LoadGameObjects(JsonDocument& aDoc)
{
	std::map<int, std::string> meshFilters;
	if (aDoc.Find("myMeshFilters"))
	{
		for (int i = 0; i < aDoc["myMeshFilters"].GetSize(); ++i)
		{
			meshFilters[aDoc["myMeshFilters"][i]["myParent"].GetInt()] = aDoc["myMeshFilters"][i]["myPath"].GetString();
		}
	}

	for (unsigned short i = 0; i < my3DBabyBirds.size(); ++i)
	{
		my3DBabyBirds[i].Init(mySceneID);

		std::string lvl = "";
		unsigned char particleNr = 0;

		switch (myCurrentLevelIndex)
		{
		case 0:  
			lvl = "Tutorial";
			break;

		case 1:
			lvl = "Village";
			break;

		case 2:
			lvl = "Pillars";
			break;

		case 3:
			lvl = "Roots";
			break;
		}

		particleNr = i % 4;

		std::string filePath;

		if (i == 2 || i == 3)
		{
			const short index = i - 2;
			myPoofParticles[index].Init(mySceneID);
			myPoofParticles[index].AddComponent<CParticleSystemComponent>("Assets/Particles/poofParticle.json")->Stop();
			myPoofParticles[index].GetTransform().SetParent(&my3DBabyBirds[i].GetTransform());
			myPoofParticles[index].GetTransform().SetPosition({ myPoofParticles[index].GetTransform().GetPosition().x, myPoofParticles[index].GetTransform().GetPosition().y - 1.0f, 2.5f });
		}

		if (i == 3)
		{
			filePath = "Assets/Models/babyBirdTimeAttack/babyBirdTimeAttack.fbx";
		}
		else
		{
			filePath = "Assets/Models/babyBird" + lvl + "/babyBird" + lvl + ".fbx";
		}

		my3DBabyBirds[i].AddComponent<CModelComponent>(filePath.c_str());

		my3DBabyBirds[i].AddComponent<CParticleSystemComponent>(std::string("Assets/Particles/Hearts" + std::to_string(particleNr) + ".json").c_str())->PlayInstant();

		my3DBabyBirds[i].SetActive(false);
	}

	if (aDoc.Find("myGameObjects"))
	{
		unsigned short birdIndex = 0;

		for (int i = 0; i < aDoc["myGameObjects"].GetSize(); ++i)
		{
			auto obj = aDoc["myGameObjects"][i];
			if (meshFilters.find(obj["myID"].GetInt()) != meshFilters.end())
			{
				CGameObject tempModel;
				tempModel.Init(mySceneID);
				tempModel.AddComponent<CModelComponent>(meshFilters[obj["myID"].GetInt()].c_str());

				tempModel.Move({
					obj["myPosition"]["myX"].GetFloat(),
					obj["myPosition"]["myY"].GetFloat(),
					obj["myPosition"]["myZ"].GetFloat()
				});
				tempModel.GetTransform().Scale({
					obj["myScale"]["myX"].GetFloat(),
					obj["myScale"]["myY"].GetFloat(),
					obj["myScale"]["myZ"].GetFloat()
				});
				CommonUtilities::Quatf quat(
					obj["myRotation"]["myW"].GetFloat(),
					obj["myRotation"]["myX"].GetFloat(),
					obj["myRotation"]["myY"].GetFloat(),
					obj["myRotation"]["myZ"].GetFloat()
				);

				tempModel.GetTransform().Rotate(quat.GetEulerAngles());
			}

			if (!obj.Find("myTag"))
			{
				continue;
			}

			if (std::string(obj["myTag"].GetString()) == "ParticleSystem")
			{
				CGameObject tempModel;
				tempModel.Init(mySceneID);

				std::string filePath = "Assets/Particles/";
				filePath += obj["myName"].GetString();
				filePath += ".json";

				tempModel.AddComponent<CParticleSystemComponent>(filePath.c_str());

				tempModel.Move({
					obj["myPosition"]["myX"].GetFloat(),
					obj["myPosition"]["myY"].GetFloat(),
					obj["myPosition"]["myZ"].GetFloat()
				});
				tempModel.GetTransform().Scale({
					obj["myScale"]["myX"].GetFloat(),
					obj["myScale"]["myY"].GetFloat(),
					obj["myScale"]["myZ"].GetFloat()
				});
				CommonUtilities::Quatf quat(
					obj["myRotation"]["myW"].GetFloat(),
					obj["myRotation"]["myX"].GetFloat(),
					obj["myRotation"]["myY"].GetFloat(),
					obj["myRotation"]["myZ"].GetFloat()
				);
				tempModel.GetTransform().Rotate(quat.GetEulerAngles());
				continue;
			}
			else if (std::string(obj["myTag"].GetString()) == "Bird")
			{
				my3DBabyBirds[birdIndex].Move({
					obj["myPosition"]["myX"].GetFloat(),
					obj["myPosition"]["myY"].GetFloat(),
					obj["myPosition"]["myZ"].GetFloat()
				});
				CommonUtilities::Quatf quat(
					obj["myRotation"]["myW"].GetFloat(),
					obj["myRotation"]["myX"].GetFloat(),
					obj["myRotation"]["myY"].GetFloat(),
					obj["myRotation"]["myZ"].GetFloat()
				);
				my3DBabyBirds[birdIndex].GetTransform().Rotate(quat.GetEulerAngles());

				++birdIndex;
				continue;
			}
		}
	}
}

void CGameState::LoadHoops(JsonDocument& aDoc)
{
	myHoops.Init(32);
	myPassedHoops.Init(32);

	for (int i = 0; i < aDoc["myHoops"].GetSize(); ++i)
	{
		auto obj = aDoc["myHoops"][i];

		myHoops.Add(CHoop());
		CHoop& hoop = myHoops.GetLast();

		if (obj.Find("myType"))
		{
			hoop.SetType(static_cast<CHoop::EHoopType>(obj["myType"].GetInt()));
		}

		hoop.Init(mySceneID);
		hoop.SetPosition({
			obj["myPosition"]["myX"].GetFloat(),
			obj["myPosition"]["myY"].GetFloat(),
			obj["myPosition"]["myZ"].GetFloat()
		});
		CommonUtilities::Quatf quat(
			obj["myRotation"]["myW"].GetFloat(),
			obj["myRotation"]["myX"].GetFloat(),
			obj["myRotation"]["myY"].GetFloat(),
			obj["myRotation"]["myZ"].GetFloat()
		);
		hoop.Rotate(quat.GetEulerAngles());


		hoop.GetTransform().SetScale({ 0.0f, 0.0f, 0.0f });
		hoop.SetTargetScale({ 0.0f, 0.0f, 0.0f });
		hoop.SetScale({ 0.0f, 0.0f, 0.0f });

		if (obj.Find("myTunnelIndex"))
		{
			hoop.SetTunnelIndex(obj["myTunnelIndex"].GetInt());
		}
	}

	myTotalNrOfHoops = myHoops.Size();

	myHoopsToTakeForFirstBird = static_cast<int>(std::round(myTotalNrOfHoops * 0.4f));	// 40%
	myHoopsToTakeForSecondBird = static_cast<int>(std::round(myTotalNrOfHoops * 0.8f));	// 80%
	myHoopsToTakeForThirdBird = static_cast<int>(std::round(myTotalNrOfHoops * 1.f));	// 100%
}

void CGameState::LoadPointLights(JsonDocument& aDoc)
{
	for (int i = 0; i < aDoc["myPointLights"].GetSize(); ++i)
	{
		auto obj = aDoc["myPointLights"][i];
		CGameObject gameObject;
		gameObject.Init(mySceneID);

		gameObject.AddComponent<CPointLightComponent>(CPointLightComponent::SPointLightComponentData(
			obj["myRange"].GetFloat(),
			{
				obj["myColor"]["myX"].GetFloat(),
				obj["myColor"]["myY"].GetFloat(),
				obj["myColor"]["myZ"].GetFloat()
			}
		));
		gameObject.SetPosition({
			obj["myPosition"]["myX"].GetFloat(),
			obj["myPosition"]["myY"].GetFloat(),
			obj["myPosition"]["myZ"].GetFloat()
		});
	}
}

void CGameState::LoadTunnelPoints(JsonDocument& aDoc)
{
	myTunnelPoints.Init(32);
	myCurrentTunnelIndex = 0;
	if (aDoc.Find("myTunnelPoints"))
	{
		for (int i = 0; i < aDoc["myTunnelPoints"].GetSize(); ++i)
		{
			auto obj = aDoc["myTunnelPoints"][i];

			CTransform tunnelPoint;

			tunnelPoint.SetPosition({
				obj["myPosition"]["myX"].GetFloat(),
				obj["myPosition"]["myY"].GetFloat(),
				obj["myPosition"]["myZ"].GetFloat()
			});
			CommonUtilities::Quatf quat(
				obj["myRotation"]["myW"].GetFloat(),
				obj["myRotation"]["myX"].GetFloat(),
				obj["myRotation"]["myY"].GetFloat(),
				obj["myRotation"]["myZ"].GetFloat()
			);
			tunnelPoint.Rotate(quat.GetEulerAngles());
			myTunnelPoints.Add(tunnelPoint);
		}
	}
}

void CGameState::LoadUI()
{
	for (int i = 0; i < EGameStateUI_Count; ++i)
	{
		myUIObjects[i].Init(mySceneID);
		myUIObjects[i].SetActive(true);
	}

	/////////////// Points UI
	CSpriteComponent* hoopIcon = myUIObjects[EGameStateUI_Hoop].AddComponent<CSpriteComponent>("Assets/Sprites/uiHoopIcon.dds");
	hoopIcon->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myHoopIconScale = hoopIcon->GetScale();

	///////////////HoopCountText
	CTextComponent* hoopCountText = myUIObjects[EGameStateUI_Hoop].AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");
	hoopCountText->SetText("0%");
	hoopCountText->SetTint({ 1.0f, 1.0f, 1.0f, 1.f });
	hoopCountText->SetScale({ 1.5f, 1.5f });

	/////////////// TIMER ICON
	CSpriteComponent* timerIcon = myUIObjects[EGameStateUI_Time].AddComponent<CSpriteComponent>("Assets/Sprites/uiTimerIcon.dds");
	timerIcon->SetPivot(CommonUtilities::Vector2f(1.0f, 0.0f));

	/////////////// TIMER TEXT
	CTextComponent* timerText = myUIObjects[EGameStateUI_Time].AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");
	timerText->SetText("0:00.00");
	timerText->SetTint({ 1.0f, 1.0f, 1.0f, 1.f });
	timerText->SetPivot({ 0.0f, 0.0f });
	timerText->SetScale({ 1.7f, 1.7f });

	////////////// BIRDS
	for (int i = 0; i < myLevelSelectBirds.size(); ++i)
	{
		////////////// DARK BIRD
		myLevelSelectBirds[i].myDarkBird.Init(mySceneID);
		myLevelSelectBirds[i].myDarkBird.SetActive(false);
		CSpriteComponent* bird = myLevelSelectBirds[i].myDarkBird.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/iconBirdDark.dds");
		bird->SetScale({ 0.43f, 0.43f });
		bird->SetPivot({ 0.5f, 0.5f });

		////////////// LIGHT BIRD
		myLevelSelectBirds[i].myLightBird.Init(mySceneID);
		myLevelSelectBirds[i].myLightBird.SetActive(false);
		bird = myLevelSelectBirds[i].myLightBird.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/iconBirdLight.dds");
		bird->SetScale({ 0.43f, 0.43f });
		bird->SetPivot({ 0.5f,0.5f });

		myLevelSelectBirds[i].myFlashBird.Init(mySceneID);
		myLevelSelectBirds[i].myFlashBird.SetActive(false);
		bird = myLevelSelectBirds[i].myFlashBird.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/iconBirdLight.dds");
		bird->SetScale({ 0.43f, 0.43f });
		bird->SetPivot({ 0.5f, 0.5f });
	}

	myBabyBirdIconScale = myLevelSelectBirds[0].myDarkBird.GetComponent<CSpriteComponent>()->GetScale();
	SetBirdIcons(0);

	///////////////BOOST UI
	myBoostCounterPlacementRange = 0.20f;
	myCurrentFlapCount = myPlayer.GetCurrentFlapCount();

	unsigned short maxBoostCount = static_cast<unsigned short>(myPlayer.GetMaxFlapCount());
	myBoostsIcons.Init(maxBoostCount);
	myGreyedOutBoostIcons.Init(maxBoostCount);
	myFlashBoostIcons.Init(maxBoostCount);
	for (unsigned short i = 0; i < maxBoostCount; ++i)
	{
		myBoostsIcons.Add(CGameObject());
		myFlashBoostIcons.Add(CGameObject());
		myGreyedOutBoostIcons.Add(CGameObject());
	}

	CommonUtilities::Vector2f iconScale = CommonUtilities::Vector2f(0.8f, 0.8f);

	for (short i = 0; i < maxBoostCount; ++i)
	{
		myBoostsIcons[i].Init(mySceneID);
		std::string boostIconPath = "Assets/Sprites/BoostIcon/uiBoostIcon" + std::to_string(maxBoostCount - i) + ".dds";
		CSpriteComponent* boostSpriteComponent = myBoostsIcons[i].AddComponent<CSpriteComponent>(boostIconPath.c_str());
		boostSpriteComponent->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
		boostSpriteComponent->SetScale(iconScale);

		myFlashBoostIcons[i].Init(mySceneID);
		CSpriteComponent* flashBoostSpriteComponent = myFlashBoostIcons[i].AddComponent<CSpriteComponent>(boostIconPath.c_str());
		flashBoostSpriteComponent->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
		flashBoostSpriteComponent->SetScale(iconScale);
		myFlashBoostIcons[i].SetActive(false);

		myGreyedOutBoostIcons[i].Init(mySceneID);
		boostIconPath = "Assets/Sprites/BoostIcon/uiBoostIconGrey" + std::to_string(maxBoostCount - i) + ".dds";
		CSpriteComponent* greyedBoostSpriteComponent = myGreyedOutBoostIcons[i].AddComponent<CSpriteComponent>(boostIconPath.c_str());
		greyedBoostSpriteComponent->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
		greyedBoostSpriteComponent->SetScale(iconScale);
		myGreyedOutBoostIcons[i].SetActive(false);

		CommonUtilities::Vector2f position;
		position.y = 0.90f;
		position.x = 0.50f;
		position.x = position.x - ((myBoostCounterPlacementRange) / myCurrentFlapCount) + ((myBoostCounterPlacementRange* i) / myCurrentFlapCount) - ((myBoostCounterPlacementRange * (i - i)) / myCurrentFlapCount);
		boostSpriteComponent->SetPosition(position);
		greyedBoostSpriteComponent->SetPosition(position);
		flashBoostSpriteComponent->SetPosition(position);

	}
	myFlashBoostTint = CommonUtilities::Vector4f(1.f, 1.f, 1.f, 1.f);
	myFlashBoostScale = iconScale;
	myCurrentFlashBoostTint = myFlashBoostTint;
	myCurrentFlashBoostScale = myFlashBoostScale;
}

void CGameState::UpdateUIPositions() // SETS ALL UI POSITIONS 
{
	myUIPositionsDoc.Find("hoopIcon") ?
		myUIObjects[EGameStateUI_Hoop].GetComponent<CSpriteComponent>()->SetPosition({ myUIPositionsDoc["hoopIcon"]["x"].GetFloat(), myUIPositionsDoc["hoopIcon"]["y"].GetFloat() }) //JSON
		: myUIObjects[EGameStateUI_Hoop].GetComponent<CSpriteComponent>()->SetPosition({ 0.005f, 0.005f }); //DEFAULT

	myUIPositionsDoc.Find("hoopPercentageText") ?
		myUIObjects[EGameStateUI_Hoop].GetComponent<CTextComponent>()->SetPosition({ myUIPositionsDoc["hoopPercentageText"]["x"].GetFloat(), myUIPositionsDoc["hoopPercentageText"]["y"].GetFloat() }) //JSON
		: myUIObjects[EGameStateUI_Hoop].GetComponent<CTextComponent>()->SetPosition({ 0.03f, 0.15f }); //DEFAULT

	myUIPositionsDoc.Find("timeIcon") ?
		myUIObjects[EGameStateUI_Time].GetComponent<CSpriteComponent>()->SetPosition({ myUIPositionsDoc["timeIcon"]["x"].GetFloat(), myUIPositionsDoc["timeIcon"]["y"].GetFloat() }) //JSON
		: myUIObjects[EGameStateUI_Time].GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(1.f, 0.0f)); //DEFAULT

	myUIPositionsDoc.Find("timeText") ?
		myUIObjects[EGameStateUI_Time].GetComponent<CTextComponent>()->SetPosition({ myUIPositionsDoc["timeText"]["x"].GetFloat(), myUIPositionsDoc["timeText"]["y"].GetFloat() }) //JSON
		: myUIObjects[EGameStateUI_Time].GetComponent<CTextComponent>()->SetPosition({ 0.825f, 0.05f }); //DEFAULT

	/////////// BIRDS
	for (int i = 0; i < 3; ++i)
	{
		CommonUtilities::Vector2f defaultPos = { 0.0f, 0.0f };
		char* birdName = "";
		switch (i)
		{
		case 0: { defaultPos = { 0.1f, 0.2f }; birdName = "firstBird"; break; }
		case 1: { defaultPos = { 0.2f, 0.2f }; birdName = "secondBird"; break; }
		case 2: { defaultPos = { 0.3f, 0.2f }; birdName = "thirdBird"; break; }
		}

		JsonValue currentJsonBird = myUIPositionsDoc[birdName];

		myUIPositionsDoc.Find(birdName) ?
			myLevelSelectBirds[i].myDarkBird.GetComponent<CSpriteComponent>()->SetPosition({ currentJsonBird["x"].GetFloat(), currentJsonBird["y"].GetFloat() }) //JSON
			: myLevelSelectBirds[i].myDarkBird.GetComponent<CSpriteComponent>()->SetPosition(defaultPos); //DEFAULT

		myUIPositionsDoc.Find(birdName) ?
			myLevelSelectBirds[i].myLightBird.GetComponent<CSpriteComponent>()->SetPosition({ currentJsonBird["x"].GetFloat(), currentJsonBird["y"].GetFloat() }) //JSON
			: myLevelSelectBirds[i].myLightBird.GetComponent<CSpriteComponent>()->SetPosition(defaultPos); //DEFAULT

		myUIPositionsDoc.Find(birdName) ?
			myLevelSelectBirds[i].myFlashBird.GetComponent<CSpriteComponent>()->SetPosition({ currentJsonBird["x"].GetFloat(), currentJsonBird["y"].GetFloat() }) //JSON
			: myLevelSelectBirds[i].myFlashBird.GetComponent<CSpriteComponent>()->SetPosition(defaultPos); //DEFAULT
	}
}

void CGameState::SetBirdIcons(const unsigned int& aBabyBirdCountHighscore)
{
	unsigned short currentBirdsSet = 0;

	CSpriteComponent* sc;

	for (int i = 0; i < 3; ++i)
	{
		myLevelSelectBirds[i].myLightBird.SetActive(false);
		myLevelSelectBirds[i].myDarkBird.SetActive(false);

		if (aBabyBirdCountHighscore > 0 && currentBirdsSet < aBabyBirdCountHighscore)
		{
			myLevelSelectBirds[i].myLightBird.SetActive(true);
			sc = myLevelSelectBirds[i].myLightBird.GetComponent<CSpriteComponent>();
		}
		else
		{
			myLevelSelectBirds[i].myDarkBird.SetActive(true);
			sc = myLevelSelectBirds[i].myDarkBird.GetComponent<CSpriteComponent>();
		}

		++currentBirdsSet;
	}
}

void CGameState::Set3DBirds(const unsigned int & aBabyBirdCountHighscore)
{
	unsigned short lastBird =  2;

	unsigned short currentBirdsSet = 0;
	bool beatTimeAttack = false;

	if (myHasPlayed && myRaceTime <= myTimeToBeat && myNumberOfPassedHoops == myTotalNrOfHoops)
	{
		beatTimeAttack = true;

		if (!my3DBabyBirds[3].IsActive())
		{
			lastBird = 3;

			// 3 - 2 ---> index 3 is timeAttackBird, -2 because we only have 2 poofParticles (index 0 and 1)
			myPoofParticles[3 - 2].GetComponent<CParticleSystemComponent>()->PlayInstant();
		}
	}

	for (unsigned short i = 0; i <= lastBird; ++i)
	{
		if (aBabyBirdCountHighscore > 0 || beatTimeAttack)
		{
			if (!my3DBabyBirds[i].IsActive() && (currentBirdsSet < aBabyBirdCountHighscore || beatTimeAttack))
			{
				if (beatTimeAttack && currentBirdsSet >= aBabyBirdCountHighscore && i <= lastBird - 1)
				{
					i += lastBird - i;
				}

				my3DBabyBirds[i].SetActive(true);
				
				if (i == 3)
				{
					my3DBabyBirds[i].GetTransform().SetScale({ 0.f, 0.f, 0.f });
				}
			}
		}
		else
		{

			my3DBabyBirds[i].SetActive(false);
		}

		++currentBirdsSet;
	}
}

EStateUpdate CGameState::Update()
{
	static bool shouldPauseDt = false;

	if (!myHasPlayed && myCurrentLevelIndex == 0 && myHowToPlayTimer > 0)
	{
		myHowToPlayTimer -= IWorld::Time().GetDeltaTime();
	}
	else if (myHowToPlayTimer <= 0 && !myHowToPlayPopup.GetIsPlaying() && !myHowToPlayPopup.GetIsDone())
	{
		myHowToPlayPopup.Play();
		myUIObjects[EGameStateUI_Hoop].GetComponent<CTextComponent>()->SetText("");
		shouldPauseDt = true;
	}
#ifndef _RETAIL
	else if (IWorld::Input().IsKeyPressed(Input::Key_P))
	{
		shouldPauseDt = !shouldPauseDt;
	}

	if (IWorld::Input().IsKeyPressed(Input::Key_9))
	{
		myHoopsToTakeForThirdBird = 3;
	}
#endif

	myHowToPlayPopup.Update();	
	if (shouldPauseDt)
	{
		IWorld::Time().SetSpeed(0);
	}
	else
	{
		IWorld::Time().SetSpeed(CommonUtilities::Lerp(IWorld::Time().GetSpeed(), 1.f, IWorld::Time().GetRealDeltaTime() * 10.f));
	}

	if (!shouldPauseDt)
	{
		UpdatePlayer();
		UpdateCamera();
		UpdateHoops();

		UpdateBoostCounter();

		if (myRaceTime <= myTimeToBeat && myNumberOfPassedHoops == myTotalNrOfHoops) // IF BEAT TIMEATTACK
		{
			if (!my3DBabyBirds[3].IsActive())
			{
				Set3DBirds(myBirdsAcquiredThisRace);
			}
		}

		if (myBirdsAcquiredThisRace > 0)
		{
			myLevelSelectBirds[myBirdsAcquiredThisRace - 1].myFlashBird.GetComponent<CSpriteComponent>()->SetScale(myCurrentBabyBirdFlashIconScale);
			myLevelSelectBirds[myBirdsAcquiredThisRace - 1].myFlashBird.GetComponent<CSpriteComponent>()->SetTint(myCurrentBabyBirdFlashIconTint);
		}

		myCurrentBabyBirdFlashIconScale = CommonUtilities::Lerp(myCurrentBabyBirdFlashIconScale, { 1.0f, 1.0f }, 5.f * IWorld::Time().GetDeltaTime());
		myCurrentBabyBirdFlashIconTint = CommonUtilities::Lerp(myCurrentBabyBirdFlashIconTint, { 999.f, 999.f, 999.f, 0.f }, 5.f * IWorld::Time().GetDeltaTime());

	#ifndef _RETAIL
		IWorld::SetDebugColor({ 1.f, 0.7f, 0.3f, 1.f });
		int count = 0;
		for (unsigned short transformIndex = static_cast<unsigned short>(myCurrentTunnelIndex); transformIndex < myTunnelPoints.Size() - 1; ++transformIndex)
		{
			CTransform& source = myTunnelPoints[transformIndex];
			CTransform& target = myTunnelPoints[transformIndex + 1];
			DrawTunnel(source, target);
			if (count++ > 5)break;
		}

		if (IWorld::Input().IsKeyPressed(Input::Key_Y))
		{
			DoOnWinStuff();
		}
	#endif

		if (myHasLevelBeenFinished)
		{
			myTimerUntillShowScore += IWorld::Time().GetDeltaTime();
			if (myTimerUntillShowScore > 2.f && !myHasDoneOnWinStuff)
			{
				DoOnWinStuff();
				myHasDoneOnWinStuff = true;
			}
		}
		else
		{
			UpdateTimer();
		}

		if (IWorld::Input().IsKeyPressed(Input::Key_Escape) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_Start))
		{
			CPauseMenuState* pauseMenu = new CPauseMenuState();
			pauseMenu->Init();
			ourStateStack->PushSubState(pauseMenu);
		}
	}
	else if (myHowToPlayPopup.GetIsDone())
	{
		UpdateHoopPercentageCounter();
		shouldPauseDt = false;
	}

	return EDoNothing;
}

void CGameState::OnEnter()
{
	if (myCurrentLevelIndex == 1 || myCurrentLevelIndex == 2)
	{
		AM.Stop("Music/MenuMusic", false);
		AM.Play("Music/GameStateMusic2", false);
	}
	else
	{
		AM.Stop("Music/MenuMusic", false);
		AM.Play("Music/GameStateMusic", false);
	}
	SetActiveScene();
	JsonDocument settingsDoc;
	settingsDoc.LoadFile("settings.json", false);

	IWorld::HideCursor();

	myPlayer.ReadControlsFromJson();
}

void CGameState::OnLeave()
{
	IWorld::ShowCursor();
}

void CGameState::SetPlayerStartPosition()
{
	if (!myTunnelPoints.Empty())
	{
		CTransform& tunnelPoint = myTunnelPoints[0];
		CommonUtilities::Vector3f forward = tunnelPoint.GetForward();
		myPlayer.SetPosition(tunnelPoint.GetPosition() - (forward * 10.f));
		myPlayer.GetTransform().SetLookDirection(forward);

		myTunnel.SetPosition(myPlayer.GetTransform().GetPosition());
		myTunnel.SetDirection(tunnelPoint.GetPosition() - myTunnel.GetPosition());
		myTunnel.SetHoopDirection(tunnelPoint.GetPosition() - myTunnel.GetPosition());
	}
	else
	{
		myPlayer.SetPosition(myPlayer.GetTransform().GetPosition() + (myPlayer.GetForward() * 2.f));

		myTunnel.SetPosition(myPlayer.GetTransform().GetPosition());
		myTunnel.SetDirection(myPlayer.GetForward());
	}

	myPlayer.SetTunnelDirection(myTunnel.GetDirection());
	myPlayer.SetCurrentTunnelDirection({ 0.f, 0.f, 0.f });

	myPlayerCamera.GetTransform().SetLookDirection(myTunnel.GetDirection());
	myPlayerCamera.SetPosition(myPlayer.GetTransform().GetPosition() - myPlayer.GetForward() * 2.f);
}

void CGameState::DrawTunnel(CTransform& source, CTransform& target)
{
	CommonUtilities::Vector3f up1 = source.GetUp();
	CommonUtilities::Vector3f up2 = target.GetUp();
	CommonUtilities::Vector3f right1 = source.GetRight();
	CommonUtilities::Vector3f right2 = target.GetRight();

	float tunnelWidth = myTunnel.GetRadius();
	int res = 16;
	float rep = 8.f;
	for (float r = 0.f; r < rep; ++r)
	{
		for (int i = 0; i < res; ++i)
		{
			float a = i * ((CommonUtilities::Pif * 2.f) / res);
			float b = (i + 1) * ((CommonUtilities::Pif * 2.f) / res);
			CommonUtilities::Vector3f right = CommonUtilities::Lerp(right1, right2, r / rep);
			CommonUtilities::Vector3f up = CommonUtilities::Lerp(up1, up2, r / rep);
			CommonUtilities::Vector3f pos = CommonUtilities::Lerp(source.GetPosition(), target.GetPosition(), r / rep);
			IWorld::DrawLine(
				pos + (right * std::cosf(a) + up*std::sinf(a)) * tunnelWidth,
				pos + (right * std::cosf(b) + up*std::sinf(b)) * tunnelWidth
			);
		}
	}
	for (float a = 0.f; a < CommonUtilities::Pif*2.f; a += (CommonUtilities::Pif*2.f) / res)
	{
		IWorld::DrawLine(
			source.GetPosition() + (right1 * std::cosf(a) + up1 * std::sinf(a)) * tunnelWidth,
			target.GetPosition() + (right2 * std::cosf(a) + up2 * std::sinf(a)) * tunnelWidth
		);
	}
}

void CGameState::UpdatePlayer()
{
	float dt = IWorld::Time().GetDeltaTime();

	my3DBabyBirds[3].GetTransform().SetScale(CommonUtilities::Lerp(my3DBabyBirds[3].GetTransform().GetScale(), { 1.f, 1.f, 1.f }, dt*3.f));

	myPlayer.Update(myHasLevelBeenFinished);

	// Tunnel bounds
	float inforce = myTunnel.CalculateInwardsForce(myPlayer.GetTransform().GetPosition());
	const CommonUtilities::Vector3f closestPoint = myTunnel.GetClosestPoint(myPlayer.GetTransform().GetPosition());
	const CommonUtilities::Vector3f inwardsForce = (closestPoint - myPlayer.GetTransform().GetPosition()).GetNormalized();

	myPlayer.Move(inwardsForce * dt * inforce * myPlayer.GetSpeed());

	// Aim assist
	if (!myHoops.Empty())
	{
		if (myCurrentTunnelIndex == std::fmaxf(myHoops[0].GetTunnelIndex() - 1.f, 0.f))
		{
			float force = myTunnel.CalculateAimAssistForce(myPlayer.GetTransform().GetPosition());
			const CommonUtilities::Vector3f hoopClosestPoint = myTunnel.GetHoopClosestPoint(myPlayer.GetTransform().GetPosition());
			const CommonUtilities::Vector3f aimAssistForce = (hoopClosestPoint - myPlayer.GetTransform().GetPosition()).GetNormalized();

			myPlayer.Move(aimAssistForce * dt * force * myPlayer.GetSpeed());

			IWorld::SetDebugColor({ 1.f, 0.5f, 0.5f, 1.f });
			IWorld::DrawLine(myTunnel.GetHoopPosition() + myTunnel.GetHoopDirection()*1000.f, myTunnel.GetHoopPosition() - myTunnel.GetHoopDirection()*1000.f);
			IWorld::DrawLine(myPlayer.GetTransform().GetPosition(), hoopClosestPoint);
			IWorld::DrawSphere(hoopClosestPoint);
		}
	}

	IWorld::SetDebugColor({ 0.f, 1.f, 1.f, 1.f });
	IWorld::DrawLine(myTunnel.GetPosition() + myTunnel.GetDirection()*1000.f, myTunnel.GetPosition() - myTunnel.GetDirection()*1000.f);
	IWorld::DrawLine(myPlayer.GetTransform().GetPosition(), closestPoint);
	IWorld::DrawSphere(closestPoint);

	CheckCollisions();

	myPlayer.UpdateWings();

	if (!myHasLevelBeenFinished)
	{
		auto dir = (closestPoint - myPlayer.GetTransform().GetPosition());
		float length = dir.Length()*2.f + 3.f;
		dir.Normalize();
		myPlayer.Move(dir * length * (1.f - myPlayer.GetSlowDown()) * dt);
	}
}

void CGameState::CheckCollisions()
{
	IWorld::SetDebugColor({ 0.f, 1.f, 0.f, 1.f });

	float r = 0.25f;
	CColliderSphere playerSphere(myPlayer.GetTransform().GetPosition(), r);
	IWorld::DrawSphere(playerSphere.GetPosition(), { r*2.f, r*2.f, r*2.f });

	CommonUtilities::Vector3f contactPoint;

	for (CColliderOBB& obb : myOBBs)
	{
		IWorld::SetDebugColor({ 0.f, 1.f, 0.f, 1.f });
		IWorld::DrawBox(obb.GetPosition(), obb.GetRadius()*2.f, obb.GetRotation());

		if (ICollision::SphereVsOBB(playerSphere, obb, &contactPoint))
		{
			CommonUtilities::Vector3f collisionNormal = myPlayer.GetTransform().GetPosition() - contactPoint;
			float penetration = r - (collisionNormal).Length();
			collisionNormal.Normalize();
			myPlayer.Move(collisionNormal * penetration * 0.01f);
			myPlayer.Collide();
		}
		IWorld::SetDebugColor({ 1.f, 0.f, 0.f, 1.f });
		IWorld::DrawBox(contactPoint, { 0.1f, 0.1f, 0.1f });
	}
	for (CColliderSphere& sphere : mySpheres)
	{
		IWorld::SetDebugColor({ 0.f, 1.f, 0.f, 1.f });
		IWorld::DrawSphere(sphere.GetPosition(), CommonUtilities::Vector3f(1.f, 1.f, 1.f)*sphere.GetRadius()*2.f);

		if (ICollision::SphereVsSphere(playerSphere, sphere, &contactPoint))
		{
			CommonUtilities::Vector3f collisionNormal = myPlayer.GetTransform().GetPosition() - contactPoint;
			float penetration = r - (collisionNormal).Length();
			collisionNormal.Normalize();
			myPlayer.Move(collisionNormal * penetration * 0.01f);
			myPlayer.Collide();
		}
		IWorld::SetDebugColor({ 1.f, 0.f, 0.f, 1.f });
		IWorld::DrawBox(contactPoint, { 0.1f, 0.1f, 0.1f });
	}
}

void CGameState::UpdateCamera()
{
	const CommonUtilities::Vector3f closestPoint = myTunnel.GetClosestPoint(myPlayer.GetTransform().GetPosition());

	myPlayerCamera.Update(
		myPlayer.GetTransform().GetPosition(),
		closestPoint,
		CommonUtilities::LookAt(myTunnel.GetDirection(), CommonUtilities::Vector3f(0.f, 0.f, 0.f)),
		myPlayer.GetSpeedBoost(),
		myPlayer.GetSlowDown(), myHasLevelBeenFinished
	);
}

void CGameState::UpdateHoopLists()
{
	for (unsigned short i = 0; i < myHoops.Size(); ++i)
	{
		CHoop& hoop = myHoops[i];
		hoop.Update();

		if (i == 0)
		{
			CTransform& hoopTranform = hoop.GetTransform();
			myNextHoopParticle.GetTransform().SetParent(&hoopTranform);
			myNextHoopParticle.GetTransform().RotateAround(hoopTranform.GetLocalForward(), CommonUtilities::Pif * IWorld::Time().GetDeltaTime());
			hoop.SetTargetScale({ 1.f, 1.f, 1.f });
		}
		else if (i < 3)
		{
			if (hoop.GetType() == CHoop::EHoopType_Boost)
			{
				hoop.SetTargetScale({ 1.f, 1.f, 1.f });
			}
			else
			{
				hoop.SetTargetScale({ 0.3f, 0.3f, 0.8f });
			}
		}

	}
}

void CGameState::PassCheckCurrentHoop()
{
	CHoop& hoop = myHoops[0];

	const CommonUtilities::Vector3f vecFromPlayerToHoop = hoop.GetTransform().GetPosition() - myPlayer.GetTransform().GetPosition();
	const float distBetweenPlayerAndHoop = vecFromPlayerToHoop.Length();

	CommonUtilities::Vector3f z = (CommonUtilities::Vector3f(0.0f, 0.0f, 1.0f) * CommonUtilities::Matrix33f(hoop.GetTransform().GetMatrix())).GetNormalized();

	CommonUtilities::Vector3f lastPos = hoop.GetTransform().GetPosition();

	if (z.Dot(vecFromPlayerToHoop) < 0.3f) // z-axis check
	{
		++myNumberOfPassedHoops;

		if (distBetweenPlayerAndHoop < 1.5f) // HIT
		{
			DoOnHoopHitStuff();

			if (hoop.GetType() == CHoop::EHoopType_Boost)
			{
				myPlayer.BoostHoop();
			}

			UpdateBirdUI();

			myPassedHoops.Add(hoop);
			myPassedHoops.GetLast().Hit(!myIsPlayingHeartParticles);
		}
		else
		{
			myPassedHoops.Add(hoop);
			myPassedHoops.GetLast().Miss();
		}

		UpdateHoopPercentageCounter();

		myHoops.RemoveAtIndex(0);

		myTunnel.SetAimAssist(static_cast<float>(myPlayer.GetPoints()) / static_cast<float>(myNumberOfPassedHoops));
	}
}

void CGameState::PlayHeartParticle()
{
	myHeartParticle.GetComponent<CParticleSystemComponent>()->Play();
	myHeartParticle.GetTransform().SetParent(&myPlayer.GetTransform());

	myIsPlayingHeartParticles = true;
}

void CGameState::UpdateBirdUI()
{
	const unsigned short currentHoopScore = static_cast<unsigned short>(myPlayer.GetPoints());
	unsigned short acquiredBirdsLastFrame = myBirdsAcquiredThisRace;

	/////////////////////// BABY BIRDSSSS
	if (currentHoopScore >= myHoopsToTakeForFirstBird) //40% --> get one bird
	{
		if (!(myBirdsAcquiredThisRace >= 1))
		{
			myBirdsAcquiredThisRace = 1;
			AM.PlayNewInstance("SFX/SoundEffects/BabyBirdobtained", nullptr, AudioChannel::SoundEffects);
			myCurrentBabyBirdFlashIconScale = myBabyBirdIconScale;
			myCurrentBabyBirdFlashIconTint = CommonUtilities::Vector4f(999.f, 999.f, 999.f, 1.f);
			myLevelSelectBirds[myBirdsAcquiredThisRace - 1].myFlashBird.SetActive(true);
			PlayHeartParticle();
		}

		if (currentHoopScore >= myHoopsToTakeForSecondBird) //80% --> get another bird
		{
			if (!(myBirdsAcquiredThisRace >= 2))
			{
				myBirdsAcquiredThisRace = 2;
				AM.PlayNewInstance("SFX/SoundEffects/BabyBirdobtained", nullptr, AudioChannel::SoundEffects);
				myCurrentBabyBirdFlashIconScale = myBabyBirdIconScale;
				myCurrentBabyBirdFlashIconTint = CommonUtilities::Vector4f(999.f, 999.f, 999.f, 1.f);
				myLevelSelectBirds[myBirdsAcquiredThisRace - 1].myFlashBird.SetActive(true);
				PlayHeartParticle();
			}

			if (currentHoopScore >= myHoopsToTakeForThirdBird) //100% --> get another bird
			{
				if (!(myBirdsAcquiredThisRace >= 3))
				{
					myBirdsAcquiredThisRace = 3;
					AM.PlayNewInstance("SFX/SoundEffects/BabyBirdobtained", nullptr, AudioChannel::SoundEffects);
					myCurrentBabyBirdFlashIconScale = myBabyBirdIconScale;
					myCurrentBabyBirdFlashIconTint = CommonUtilities::Vector4f(999.f, 999.f, 999.f, 1.f);
					myLevelSelectBirds[myBirdsAcquiredThisRace - 1].myFlashBird.SetActive(true);
					PlayHeartParticle();

					// 2 - 2 ---> index 2 is 100%-bird, -2 because we only have 2 poofParticles (index 0 and 1)
					myPoofParticles[2 - 2].GetComponent<CParticleSystemComponent>()->PlayInstant();
				}
			}
		}
	}

	if (acquiredBirdsLastFrame != myBirdsAcquiredThisRace)
	{
		SetBirdIcons(myBirdsAcquiredThisRace);
		Set3DBirds(myBirdsAcquiredThisRace);
	}
}

void CGameState::PassCheckCurrentTunnel()
{
	if (myCurrentTunnelIndex >= myTunnelPoints.Size() - 1)
	{
		return;
	}

	CTransform& tunnelPoint = myTunnelPoints[static_cast<unsigned short>(myCurrentTunnelIndex + 1)];

	const CommonUtilities::Vector3f vecFromPlayerToTunnelPoint = tunnelPoint.GetPosition() - myPlayer.GetTransform().GetPosition();
	const float distBetweenPlayerAndTunnelPoint = vecFromPlayerToTunnelPoint.Length();

	CommonUtilities::Vector3f z = tunnelPoint.GetForward();


	if (z.Dot(vecFromPlayerToTunnelPoint) < 0.0f) // z-axis check
	{
		++myCurrentTunnelIndex;

		myTunnel.SetPosition(tunnelPoint.GetPosition());
		if (myCurrentTunnelIndex + 1 >= myTunnelPoints.Size()) return;
		myTunnel.SetDirection(myTunnelPoints[CommonUtilities::Clamp<unsigned short>(static_cast<unsigned short>(myCurrentTunnelIndex + 1), 0, myTunnelPoints.Size()-1)].GetPosition() - tunnelPoint.GetPosition());

		myPlayer.SetTunnelDirection(myTunnel.GetDirection());

		myTunnel.SetHoopPosition(myPlayer.GetTransform().GetPosition());
		myTunnel.SetHoopDirection(myHoops[0].GetTransform().GetPosition() - myTunnel.GetHoopPosition());
	}
}

void CGameState::UpdateHoopPercentageCounter()
{
	if (myTotalNrOfHoops > 0)
	{
		float fraction = static_cast<float>(myPlayer.GetPoints()) / static_cast<float>(myTotalNrOfHoops);

		myUIObjects[EGameStateUI_Hoop].GetComponent<CTextComponent>()->SetText(std::to_string(static_cast<int>(round(fraction * 100))) + "%");
	}
	else
	{
		myUIObjects[EGameStateUI_Hoop].GetComponent<CTextComponent>()->SetText("0%");
	}
}

void CGameState::DoOnHoopHitStuff()
{
	myPlayer.AddPoints();
	myCurrentHoopIconScale = CommonUtilities::Vector2f(1.4f, 1.4f);
}

void CGameState::DoOnWinStuff()
{
	SaveScoreToProgressFile();

	CScorescreenState *winscreen = new CScorescreenState;
	ourStateStack->LoadAsync([=]()
	{
		winscreen->Init(myLvlName);
		ourStateStack->PushSubState(winscreen);
	}, false);
}

void CGameState::SaveScoreToProgressFile()
{
	myScoreDoc.LoadFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), true);
	const unsigned short currentHoopScore = static_cast<unsigned short>(myPlayer.GetPoints());

	if (myScoreDoc.Find(myLvlName.c_str()))
	{
		myScoreDoc[myLvlName.c_str()]["hoopsCapturedLastRace"].SetInt(currentHoopScore);
		myScoreDoc[myLvlName.c_str()]["totalNrOfHoops"].SetInt(myTotalNrOfHoops); //Added so it gets overwritten if the number of hoops in the level is changed

		unsigned short hoopHighscore = static_cast<unsigned short>(myScoreDoc[myLvlName.c_str()]["hoopHighscore"].GetInt());
		if (hoopHighscore < currentHoopScore)
		{
			hoopHighscore = currentHoopScore;
			myScoreDoc[myLvlName.c_str()]["hoopHighscore"].SetInt(currentHoopScore);
		}

		myScoreDoc[myLvlName.c_str()]["raceTime"].SetFloat(myRaceTime);
		if (myScoreDoc[myLvlName.c_str()].Find("bestTime"))
		{
			float bestTime = myScoreDoc[myLvlName.c_str()]["bestTime"].GetFloat();
			if (myRaceTime < bestTime)
			{
				myScoreDoc[myLvlName.c_str()]["bestTime"].SetFloat(myRaceTime);
			}
		}
		else
		{
			myScoreDoc[myLvlName.c_str()].AddMember("bestTime", myRaceTime);
		}

		if (myScoreDoc[myLvlName.c_str()].Find("played"))
		{
			myScoreDoc[myLvlName.c_str()]["played"].SetBool(true);
		}
		else
		{
			myScoreDoc[myLvlName.c_str()].AddMember("playedTimeAttack", true);
		}

		if (myScoreDoc[myLvlName.c_str()].Find("locked"))
		{
			myScoreDoc[myLvlName.c_str()]["locked"].SetBool(false);
		}
		else
		{
			myScoreDoc[myLvlName.c_str()].AddMember("locked", false);
		}

		if (myScoreDoc[myLvlName.c_str()].Find("babyBirdCountLastRace"))
		{
			myScoreDoc[myLvlName.c_str()]["babyBirdCountLastRace"].SetInt(myBirdsAcquiredThisRace);
		}
		else
		{
			myScoreDoc[myLvlName.c_str()].AddMember("babyBirdCountLastRace", myBirdsAcquiredThisRace);
		}

		unsigned short babyBirdCountHighscore = 0;

		if (myScoreDoc[myLvlName.c_str()].Find("babyBirdCountHighscore"))
		{
			babyBirdCountHighscore = static_cast<unsigned short>(myScoreDoc[myLvlName.c_str()]["babyBirdCountHighscore"].GetInt());
		}
		else
		{
			myScoreDoc[myLvlName.c_str()].AddMember("babyBirdCountHighscore", babyBirdCountHighscore);
		}

		if (babyBirdCountHighscore < myBirdsAcquiredThisRace)
		{
			babyBirdCountHighscore = myBirdsAcquiredThisRace;
			myScoreDoc[myLvlName.c_str()]["babyBirdCountHighscore"].SetInt(babyBirdCountHighscore);
		}

		if (myIsTimeAttackOn && !myWasTimeAttackBeatenBefore)
		{
			if (myScoreDoc[myLvlName.c_str()].Find("playedTimeAttack"))
			{
				myScoreDoc[myLvlName.c_str()]["playedTimeAttack"].SetBool(true);
			}
			else
			{
				myScoreDoc[myLvlName.c_str()].AddMember("played", true);
			}

			if (myRaceTime < myTimeToBeat)
			{
				if (myScoreDoc[myLvlName.c_str()].Find("timeAttackBeaten"))
				{
					myScoreDoc[myLvlName.c_str()]["timeAttackBeaten"].SetBool(true);
				}
				else
				{
					myScoreDoc[myLvlName.c_str()].AddMember("timeAttackBeaten", true);
				}
			}
			else
			{
				if (myScoreDoc[myLvlName.c_str()].Find("timeAttackBeaten"))
				{
					myScoreDoc[myLvlName.c_str()]["timeAttackBeaten"].SetBool(false);
				}
				else
				{
					myScoreDoc[myLvlName.c_str()].AddMember("timeAttackBeaten", false);
				}
			}
		}
	}
	else
	{
		JsonObject object;
		object.AddMember("hoopHighscore", currentHoopScore);
		object.AddMember("hoopsCapturedLastRace", currentHoopScore);
		object.AddMember("totalNrOfHoops", myTotalNrOfHoops);
		object.AddMember("raceTime", 999999.f);
		object.AddMember("bestTime", 999999.f);
		object.AddMember("babyBirdCountHighscore", myBirdsAcquiredThisRace);
		object.AddMember("babyBirdCountLastRace", myBirdsAcquiredThisRace);
		object.AddMember("locked", false);
		object.AddMember("played", true);
		object.AddMember("playedTimeAttack", myIsTimeAttackOn);
		object.AddMember("timeAttackBeaten", false);
		object.AddMember("timeAttackMsgWasShown", false);
		myScoreDoc.AddObject(myLvlName.c_str(), object);
	}

	myScoreDoc.SaveFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), false, true);
	GAMEPLAY_LOG(CONCOL_VALID, "Saved score to file");
}

void CGameState::UpdateHoops()
{
	myIsPlayingHeartParticles = false;

	myUIObjects[EGameStateUI_Hoop].GetComponent<CSpriteComponent>()->SetScale(myCurrentHoopIconScale);
	myCurrentHoopIconScale = CommonUtilities::Lerp(myCurrentHoopIconScale, myHoopIconScale, 5.f * IWorld::Time().GetDeltaTime());

	for (unsigned short i = myPassedHoops.Size(); i > 0; --i)
	{
		myPassedHoops[i - 1].Update();
		if (myPassedHoops[i - 1].IsDead())
		{
			myPassedHoops[i - 1].Destroy();
			myPassedHoops.RemoveCyclicAtIndex(i - 1);
		}
	}

	if (myHoops.Empty())
	{
		return;
	}

	UpdateHoopLists();

	if (myCurrentTunnelIndex >= std::fmaxf(myHoops[0].GetTunnelIndex() - 1.f, 0.f))
	{
		PassCheckCurrentHoop();
	}
	PassCheckCurrentTunnel();

	if (myHoops.Size() < 1)
	{
		myHasLevelBeenFinished = true;
		myPlayer.SetCanBoost(false);
	}
}

void CGameState::UpdateBoostCounter()
{
	unsigned short maxBoosts = myBoostsIcons.Size();

	float playerCurrentFlapCount = myPlayer.GetCurrentFlapCount();

	if (playerCurrentFlapCount != myCurrentFlapCount)
	{
		if (playerCurrentFlapCount < myCurrentFlapCount)
		{
			for (unsigned short i = 0; i < maxBoosts; ++i)
			{
				if (i == static_cast<unsigned short>(playerCurrentFlapCount))
				{
					myFlashBoostIcons[i].SetActive(true);
				}
				else
				{
					myFlashBoostIcons[i].SetActive(false);
				}
			}
			myCurrentFlashBoostTint = myFlashBoostTint;
			myCurrentFlashBoostScale = myFlashBoostScale;
		}

		myCurrentFlapCount = playerCurrentFlapCount;

		unsigned short flooredFlapCount = static_cast<unsigned short>(floorf(myCurrentFlapCount));

		for (unsigned short i = 0; i < maxBoosts; ++i)
		{
			if (i < (flooredFlapCount))
			{
				myBoostsIcons[i].SetActive(true);
				myGreyedOutBoostIcons[i].SetActive(false);
			}
			else
			{
				myGreyedOutBoostIcons[i].SetActive(true);
				myBoostsIcons[i].SetActive(false);
			}
		}
	}

	myCurrentFlashBoostTint = CommonUtilities::Lerp(myCurrentFlashBoostTint, { 999.f, 999.f, 999.f, 0.f }, 5.f * IWorld::Time().GetDeltaTime());
	myCurrentFlashBoostScale = CommonUtilities::Lerp(myCurrentFlashBoostScale, { 1.4f, 1.4f }, 5.f * IWorld::Time().GetDeltaTime());

	for (unsigned short i = 0; i < maxBoosts; ++i)
	{

		CSpriteComponent* flashBoostIconSpriteComponent = myFlashBoostIcons[i].GetComponent<CSpriteComponent>();
		flashBoostIconSpriteComponent->SetTint(myCurrentFlashBoostTint);
		flashBoostIconSpriteComponent->SetScale(myCurrentFlashBoostScale);
	}
}

void CGameState::UpdateTimer()
{
	myRaceTime += IWorld::Time().GetDeltaTime();
	unsigned short mins = static_cast<unsigned short>(myRaceTime) / 60;
	unsigned short secs = static_cast<unsigned short>(myRaceTime) % 60;
	unsigned short millisecs = static_cast<unsigned short>(myRaceTime * 100.f) % 100;
	myTimeToStringStream.str(std::string());
	myTimeToStringStream << std::setfill('0') << std::setw(1) << mins << ":" << std::setfill('0') << std::setw(2) << secs << "." << std::setfill('0') << std::setw(2) << millisecs;
	myUIObjects[EGameStateUI_Time].GetComponent<CTextComponent>()->SetText(myTimeToStringStream.str());
}

void CGameState::LoadSettings()
{
	auto callback = [=](const std::wstring& aFilepath)
	{
		std::string str(aFilepath.begin(), aFilepath.end());
		JsonDocument settingsDoc;
		settingsDoc.LoadFile(str.c_str());

		if (settingsDoc.Find("camera"))
		{
			myPlayerCamera.LoadFromJson(settingsDoc["camera"]);
		}
		else
		{
			GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member 'camera'! Default settings applied!");
			myPlayerCamera.SetDefaultValues();
		}

		if (settingsDoc.Find("player"))
		{
			myPlayer.LoadPlayerPropsFromJson(settingsDoc["player"]);
		}
		else
		{
			GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member 'player'! Default settings applied!");
			myPlayer.SetDefaultValues();
		}

		std::string timeToBeatMember = "timeToBeat" + myLvlName;

		if (settingsDoc.Find("misc") && settingsDoc["misc"].Find(timeToBeatMember.c_str()))
		{
			myTimeToBeat = settingsDoc["misc"][timeToBeatMember.c_str()].GetFloat();
		}
		else
		{
			GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member 'misc'! Default settings applied!");
			myTimeToBeat = 180;
		}
	};

	callback(L"settings.json");
	mySettingsFilewatcherID = IWorld::GetFileWatcher().WatchFile(L"settings.json", callback);
}


#pragma once
#include "State.h"

#include "Player.h"
#include "PlayerCamera.h"
#include "Tunnel.h"
#include "Hoop.h"

#include "ColliderOBB.h"
#include "ColliderSphere.h"

#include "StaticArray.h"
#include <JsonDocument.h>
#include <sstream>
#include "PopUp.h"

class CGameState : public CState
{
public:
	CGameState();
	~CGameState();

	void Init(const std::string& aFilepath);
	EStateUpdate Update() override;
	void OnEnter() override;
	void OnLeave() override;

private:
	void LoadSettings();

	void UpdatePlayer();
	void UpdateHoops();
	void UpdateBoostCounter();
	void UpdateTimer();

	void SetPlayerStartPosition();
	void DrawTunnel(CTransform& source, CTransform& target);
	void CheckCollisions();
	void UpdateCamera();
	void UpdateHoopLists();
	void PassCheckCurrentHoop();
	void PassCheckCurrentTunnel();

	void DoOnHoopHitStuff();
	void DoOnWinStuff();
	void SaveScoreToProgressFile();

	void UpdateHoopPercentageCounter();
	void SetBirdIcons(const unsigned int& aBabyBirdCountHighscore);
	void Set3DBirds(const unsigned int& aBabyBirdCountHighscore);
	void UpdateBirdUI();
	void UpdateUIPositions();

	void PlayHeartParticle();

	//// LOAD
	void LoadColliders(JsonDocument& aDoc);
	void LoadGameObjects(JsonDocument& aDoc);
	void LoadHoops(JsonDocument& aDoc);
	void LoadPointLights(JsonDocument& aDoc);
	void LoadTunnelPoints(JsonDocument& aDoc);
	void LoadUI();

	struct SBabyBird
	{
		CGameObject myLightBird;
		CGameObject myDarkBird;
		CGameObject myFlashBird;
	};

	std::array<SBabyBird, 3> myLevelSelectBirds;
	std::array<CGameObject, 4> my3DBabyBirds;

	int mySettingsFilewatcherID;
	bool myHasLevelBeenFinished;

	float myDefaultCameraFollowSpeed;

	CGameObject myCameraGO;
	float myCameraDistanceToObject;
	float myCameraSpeedWhileBreak; // Higher is Closer To Bird
	float myCameraSpeedWhileBoost; // Higher is Closer To Bird

	CPlayer myPlayer;
	CPlayerCamera myPlayerCamera;
	CTunnel myTunnel;
	CommonUtilities::GrowingArray<CTransform> myTunnelPoints;

	CommonUtilities::GrowingArray<CHoop> myHoops;
	CommonUtilities::GrowingArray<CHoop> myPassedHoops;
	CommonUtilities::GrowingArray<CColliderOBB> myOBBs;
	CommonUtilities::GrowingArray<CColliderSphere> mySpheres;

	CommonUtilities::Vector2f myHoopIconScale;
	CommonUtilities::Vector2f myCurrentHoopIconScale;

	CommonUtilities::Vector2f myBabyBirdIconScale;
	CommonUtilities::Vector2f myCurrentBabyBirdFlashIconScale;
	CommonUtilities::Vector4f myCurrentBabyBirdFlashIconTint;
	std::ostringstream myTimeToStringStream;

	CGameObject myNextHoopParticle;
	CGameObject myHeartParticle;
	std::array<CGameObject, 2> myPoofParticles;

	int myCurrentTunnelIndex;

	unsigned int myTotalNrOfHoops;
	unsigned int myNumberOfPassedHoops;

	JsonDocument myScoreDoc;
	JsonDocument myUIPositionsDoc;
	std::string myLvlName;

	float myTimerUntillShowScore = 0.f;
	float myRaceTime;
	float myTimeToBeat;
	float myBoostCounterPlacementRange;
	float myCurrentFlapCount;
	float myHowToPlayTimer;
	bool myHasPlayed;
	bool myIsTimeAttackOn;
	bool myWasTimeAttackBeatenBefore;
	bool myHasDoneOnWinStuff = false;
	bool myIsPlayingHeartParticles;

	int myHoopsToTakeForFirstBird;
	int myHoopsToTakeForSecondBird;
	int myHoopsToTakeForThirdBird;

	short myBirdsAcquiredThisRace;
	short myCurrentLevelIndex;

	enum EGameStateUI
	{
		EGameStateUI_Hoop,
		EGameStateUI_Time,
		EGameStateUI_FirstBird,
		EGameStateUI_SecondBird,
		EGameStateUI_ThirdBird,
		EGameStateUI_Count
	};

	std::array<CGameObject, EGameStateUI_Count> myUIObjects;

	CommonUtilities::GrowingArray<CGameObject> myBoostsIcons;
	CommonUtilities::GrowingArray<CGameObject> myGreyedOutBoostIcons;
	CommonUtilities::GrowingArray<CGameObject> myFlashBoostIcons;
	CommonUtilities::Vector2f myFlashBoostScale;
	CommonUtilities::Vector4f myFlashBoostTint;
	CommonUtilities::Vector2f myCurrentFlashBoostScale;
	CommonUtilities::Vector4f myCurrentFlashBoostTint;

	CPopUpSprite myHowToPlayPopup;
};

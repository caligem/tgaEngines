#include "stdafx.h"
#include "Hoop.h"

#include "IWorld.h"
#include "Mathf.h"
#include "AudioManager.h"
#include "AudioChannels.h"

CHoop::CHoop()
{
	myLocalScale = { 1.f, 1.f, 1.f };
	myTargetScale = { 1.f, 1.f, 1.f };
	myMiss = false;
	myHit = false;
	myDeathTimer = 0.f;
	myTunnelIndex = -1;
	myRotationSpeed = 1;
}

CHoop::~CHoop()
{
}

void CHoop::Init(ID_T(CScene) aSceneID)
{
	//First
	CGameObject::Init(aSceneID);
	//----

	const float distance = 1.4f;
	for (int i = 0; i < 8; ++i)
	{
		myParticleEmitters[i].Init(aSceneID);
		if (myType == EHoopType_Boost)
		{
			myParticleEmitters[i].AddComponent<CParticleSystemComponent>("Assets/Particles/BoostHoopGrab.json")->Stop();
		}
		else
		{
			myParticleEmitters[i].AddComponent<CParticleSystemComponent>("Assets/Particles/HoopGrab.json")->Stop();
		}
		myParticleEmitters[i].GetTransform().SetParent(&GetTransform());
		myParticleEmitters[i].GetTransform().RotateAround(GetTransform().GetLocalForward(), i / 4.f * CommonUtilities::Pif);
		myParticleEmitters[i].SetPosition(myParticleEmitters[i].GetTransform().GetPosition() + (myParticleEmitters[i].GetTransform().GetLocalUp() * 2.f));
	}

	myModel.Init(aSceneID);
	myModel.GetTransform().SetParent(&GetTransform());

	switch (myType)
	{
	case CHoop::EHoopType_Boost:
		myModel.AddComponent<CModelComponent>("Assets/Models/hoopBoost/hoopBoost.fbx");
		break;
	case CHoop::EHoopType_Normal:
	default:
		myModel.AddComponent<CModelComponent>("Assets/Models/hoop/hoop.fbx");
		break;
	}
	AddComponent<CAudioSourceComponent>(CAudioSourceComponent::SAudioComponentData(GetTransform().GetPosition()));
}

void CHoop::Update()
{
	float dt = IWorld::Time().GetDeltaTime();

	if (myType == EHoopType_Boost)
	{
		myModel.GetTransform().Rotate({ 0.f, 0.f, dt*2.f });
	}
	else
	{
		myModel.GetTransform().Rotate({ 0.f, 0.f, std::cosf(IWorld::Time().GetTotalTime()*2.f)*dt });
	}

	float lerpSpeed = 3.0f;


	GetTransform().SetScale(myLocalScale);

	if (myHit)
	{
		lerpSpeed = 15.f;
		myDeathTimer += dt;
		GetTransform().Move(CommonUtilities::Vector3f(-GetTransform().GetRight()*5.f + CommonUtilities::Vector3f(0.f, 1.f, 0.f)*4.1f) * dt);
	}
	else if (myMiss)
	{
		lerpSpeed = 8.f;
		myDeathTimer += dt;
		GetTransform().Move({ 0.f, -4.1f*dt, 0.f });
	}

	myLocalScale = CommonUtilities::Lerp(myLocalScale, myTargetScale, dt * lerpSpeed);

}

void CHoop::Hit(const bool& aShouldPlayParticle)
{
	if (myType == EHoopType_Boost)
	{
		AM.PlayNewInstance("SFX/SoundEffects/Boosthoop", nullptr, AudioChannel::SoundEffects);
	}
	else
	{
		AM.PlayNewInstance("SFX/SoundEffects/HoopHit", nullptr, AudioChannel::SoundEffects);
	}
	SetTargetScale({ 0.f, 0.f, 0.f });

	if (aShouldPlayParticle)
	{
		for (int i = 0; i < 8; ++i)
		{
			myParticleEmitters[i].GetComponent<CParticleSystemComponent>()->Play();
		}
	}

	myRotationSpeed = 5;
	myHit = true;
}

void CHoop::Miss()
{
	AM.PlayNewInstance("SFX/SoundEffects/HoopMiss", nullptr, AudioChannel::SoundEffects);
	SetTargetScale({ 0.f, 0.f, 0.f });
	myMiss = true;
}

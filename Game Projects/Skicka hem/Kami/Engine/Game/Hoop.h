#pragma once

#include "GameObject.h"

class CHoop : public CGameObject
{
public:
	CHoop();
	~CHoop();

	enum EHoopType
	{
		EHoopType_Normal,
		EHoopType_Boost,
		EHoopType_Spawn
	};

	void Init(ID_T(CScene) aSceneID) override;
	void Update();

	void SetType(EHoopType aType) { myType = aType; }
	void SetTunnelIndex(int aIndex) { myTunnelIndex = aIndex; }

	EHoopType GetType() const { return myType; }
	int GetTunnelIndex() const { return myTunnelIndex; }

	void SetScale(const CommonUtilities::Vector3f& aScale) { myLocalScale = aScale; }
	void SetTargetScale(const CommonUtilities::Vector3f& aScale) { myTargetScale = aScale; }

	void Hit(const bool& aShouldPlayParticle);
	void Miss();
	bool IsDead() { return myDeathTimer > 1.f; }

private:
	EHoopType myType;
	std::array<CGameObject, 8> myParticleEmitters;

	CommonUtilities::Vector3f myLocalScale;
	CommonUtilities::Vector3f myTargetScale;

	CGameObject myModel;

	float myDeathTimer;
	int myTunnelIndex;
	unsigned int myRotationSpeed;
	bool myHit;
	bool myMiss;
};


#include "stdafx.h"
#include "HowToPlayState.h"

#include "IWorld.h"
#include "XBOXController.h"
#include "SceneManager.h"
#include "SpriteComponent.h"
#include "GButton.h"

#include "AudioManager.h"
#include "AudioChannels.h"

CHowToPlayState::CHowToPlayState()
{
	myShouldPop = false;
}


CHowToPlayState::~CHowToPlayState()
{
}

bool CHowToPlayState::Init()
{
	CState::Init();

	myGUI.Init();

	myBackground.Init(mySceneID);
	myBackground.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/howToPlayBack.dds");
	myBackground.GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myBackground.GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.5f, 0.5f));
	myBackground.SetActive(true);

	for (int i = 0; i < EHowToPlayButtons_Count; ++i)
	{
		myHowToPlayButtons[i].Init(mySceneID);
		myHowToPlayButtons[i].SetActive(true);
	}
	myHowToPlayButtons[EHowToPlayButtons_Back].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/backButton.dds");
	myHowToPlayButtons[EHowToPlayButtons_Back].GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.92f, 0.92f));
	for (int i = 0; i < EHowToPlayButtons_Count; ++i)
	{
		myHowToPlayButtons[i].GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	}

	AddButtons();
	return true;
}

EStateUpdate CHowToPlayState::Update()
{
	myGUI.Update();

	if (IWorld::Input().IsKeyPressed(Input::Key_Escape) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_Back) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_B) || myShouldPop)
	{
		return EPop_Sub;
	}
	return EDoNothing;
}

void CHowToPlayState::OnEnter()
{
	SetActiveScene();
}

void CHowToPlayState::OnLeave()
{
}

void CHowToPlayState::AddButtons()
{
	CGButton* back = new CGButton();
	back->SetOnMouseReleased(std::bind(&CHowToPlayState::Back, this));
	back->SetOnMouseOverStart(std::bind(&CHowToPlayState::OnHoverButton, this, EHowToPlayButtons_Back));
	back->SetOnMouseOverEnd(std::bind(&CHowToPlayState::EndHoverButton, this, EHowToPlayButtons_Back));
	back->SetPosition(myHowToPlayButtons[EHowToPlayButtons_Back].GetComponent<CSpriteComponent>()->GetPosition());
	back->SetSize(200.f, 140.f);
	myGUI.AddGUIObject(back);

	myGUI.SetDefaultFocusObject(back);
}

void CHowToPlayState::Back()
{
	AM.PlayNewInstance("SFX/Buttons/ButtonBack", nullptr, AudioChannel::SoundEffects);
	myShouldPop = true;
}

void CHowToPlayState::OnHoverButton(EHowToPlayButtons aButton)
{
	myHowToPlayButtons[aButton].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.06f, 1.06f));
	myHowToPlayButtons[aButton].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.1f, 1.1f, 1.1f, 1.0f));
}

void CHowToPlayState::EndHoverButton(EHowToPlayButtons aButton)
{
	myHowToPlayButtons[aButton].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.0f, 1.0f));
	myHowToPlayButtons[aButton].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
}

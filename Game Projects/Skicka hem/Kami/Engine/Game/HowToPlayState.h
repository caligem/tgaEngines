#pragma once
#include "State.h"
#include "GUI.h"

class CHowToPlayState : public CState
{
public:
	enum EHowToPlayButtons
	{
		EHowToPlayButtons_Back,
		EHowToPlayButtons_Count
	};
	CHowToPlayState();
	~CHowToPlayState();

	bool Init();
	EStateUpdate Update() override;
	void OnEnter() override;
	void OnLeave() override;
private:
	CGUI myGUI;
	CGameObject myBackground;
	std::array<CGameObject, EHowToPlayButtons_Count> myHowToPlayButtons;
private:
	// Button functions
	void AddButtons();
	void Back();
	void OnHoverButton(EHowToPlayButtons aButton);
	void EndHoverButton(EHowToPlayButtons aButton);

	bool myShouldPop;
};


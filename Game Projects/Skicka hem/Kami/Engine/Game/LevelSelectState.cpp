#include "stdafx.h"
#include "LevelSelectState.h"

#include "JsonDocument.h"
#include "Quaternion.h"

#include "CameraComponent.h"

#include "IWorld.h"
#include <InputManager.h>
#include "XBOXController.h"
#include "Mathf.h"

#include "AudioManager.h"
#include "AudioChannels.h"

#include "GameState.h"
#include "StateStack.h"
#include "GButton.h"

#include <iomanip>
#include <sstream>

#include "IWorld.h"
#include "FileWatcher.h"
#include "CreditState.h"

#include "FileDialog.h"

CLevelSelectState::CLevelSelectState()
{
	myShouldPop = false;
	myLevelIsLocked = { false, true, true, true };
	myLevelIsPlayed = { false, false, false, false };
	myUnlockMsgWasShown = { false };
	myAllowXBoxInput = true;
	myXBoxInputTimer = 0.f;
	myPlayedEndStory = false;
	myShownEndCredits = false;
	myHasStartedLevel = false;
}

CLevelSelectState::~CLevelSelectState()
{
}

void CLevelSelectState::UpdateLevelName()
{
	switch (myCurrentLevel)
	{
	case 0: { myLevelName = "Tutorial"; break; }
	case 1: { myLevelName = "Village"; break; }
	case 2: { myLevelName = "Pillars"; break; }
	case 3: { myLevelName = "Roots"; break; }
	}
}

void CLevelSelectState::Init(const std::string& aFilepath)
{
	//Create Scene
	CState::Init();

	myGUI.Init();
	myUnlockGUI.Init();

	myMainCamera.AddComponent<CAudioListenerComponent>(0);
	////NOTE!
	// ScoreDoc is loaded in OnEnter so that it re-loads every time we go into levelSelect (in case stats has changed)

	// Load Level from JSON
	JsonDocument doc;
	doc.LoadFile(aFilepath.c_str());

	UpdateLevelName();

	LoadGameObjects(doc);
	LoadCameras(doc);
	LoadPointLights(doc);
	LoadGUISprites();

	/////Time attack and level unlocked message
	myUnlockMsgWindow.Init(mySceneID);
	myLastUnlockMsgWindow.Init(mySceneID);
	myUnlockMsgWindow.SetActive(false);
	myLastUnlockMsgWindow.SetActive(false);

	CSpriteComponent* unlockLastBackground = myLastUnlockMsgWindow.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/messageBackLastLevel.dds");
	unlockLastBackground->SetPosition({ 0.5f, 0.5f });
	unlockLastBackground->SetPivot({ 0.5f, 0.5f });

	CSpriteComponent* unlockBackground = myUnlockMsgWindow.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/messageBack.dds");
	unlockBackground->SetPosition({ 0.5f, 0.5f });
	unlockBackground->SetPivot({ 0.5f, 0.5f });

	LoadUI();

	UpdateUIPositions(); // NEEDS TO BE SET BEFORE BUTTONS
	LoadButtons();

	//POPUPS
	myFirstTimePlayerPopUp.Init(mySceneID, "Assets/Sprites/storyScreen.dds", 5.f, 1.f);
	myStoryEndPopUp.Init(mySceneID, "Assets/Sprites/storyScreenEnding.dds", 5.f, 1.f, false);
}

void CLevelSelectState::LoadGameObjects(JsonDocument& aDoc)
{
	std::map<int, std::string> meshFilters;
	if (aDoc.Find("myMeshFilters"))
	{
		for (int i = 0; i < aDoc["myMeshFilters"].GetSize(); ++i)
		{
			meshFilters[aDoc["myMeshFilters"][i]["myParent"].GetInt()] = aDoc["myMeshFilters"][i]["myPath"].GetString();
		}
	}

	for (unsigned short i = 0; i < my3DBabyBirds.size(); ++i)
	{
		my3DBabyBirds[i].Init(mySceneID);

		std::string lvl = "";
		unsigned char particleNr = 0;

		switch (i)
		{
		case 0: case 1: case 2: case 3:
			lvl = "Tutorial";
			break;

		case 4: case 5: case 6: case 7:
			lvl = "Village";
			break;

		case 8: case 9: case 10: case 11:
			lvl = "Pillars";
			break;

		case 12: case 13: case 14: case 15:
			lvl = "Roots";
			break;
		}

		particleNr = i % 4;

		std::string filePath;

		if (i == 3 || i == 7 || i == 11 || i == 15)
		{
			filePath = "Assets/Models/babyBirdTimeAttack/babyBirdTimeAttack.fbx";
		}
		else
		{
			filePath = "Assets/Models/babyBird" + lvl + "/babyBird" + lvl + ".fbx";
		}

		my3DBabyBirds[i].AddComponent<CModelComponent>(filePath.c_str());

		my3DBabyBirds[i].AddComponent<CParticleSystemComponent>(std::string("Assets/Particles/Hearts" + std::to_string(particleNr) + ".json").c_str())->Stop();
		my3DBabyBirds[i].SetActive(false);
	}

	if (aDoc.Find("myGameObjects"))
	{
		unsigned short birdIndex = 0;

		for (int i = 0; i < aDoc["myGameObjects"].GetSize(); ++i)
		{
			auto obj = aDoc["myGameObjects"][i];
			if (meshFilters.find(obj["myID"].GetInt()) != meshFilters.end())
			{
				CGameObject tempModel;
				tempModel.Init(mySceneID);
				tempModel.AddComponent<CModelComponent>(meshFilters[obj["myID"].GetInt()].c_str());

				tempModel.Move({
					obj["myPosition"]["myX"].GetFloat(),
					obj["myPosition"]["myY"].GetFloat(),
					obj["myPosition"]["myZ"].GetFloat()
				});
				tempModel.GetTransform().Scale({
					obj["myScale"]["myX"].GetFloat(),
					obj["myScale"]["myY"].GetFloat(),
					obj["myScale"]["myZ"].GetFloat()
				});
				CommonUtilities::Quatf quat(
					obj["myRotation"]["myW"].GetFloat(),
					obj["myRotation"]["myX"].GetFloat(),
					obj["myRotation"]["myY"].GetFloat(),
					obj["myRotation"]["myZ"].GetFloat()
				);
				tempModel.GetTransform().Rotate(quat.GetEulerAngles());
				continue;
			}

			if (!obj.Find("myTag"))
			{
				continue;
			}

			if (std::string(obj["myTag"].GetString()) == "ParticleSystem")
			{
				CGameObject tempModel;
				tempModel.Init(mySceneID);

				std::string filePath = "Assets/Particles/";
				filePath += obj["myName"].GetString();
				filePath += ".json";

				tempModel.AddComponent<CParticleSystemComponent>(filePath.c_str());

				tempModel.Move({
					obj["myPosition"]["myX"].GetFloat(),
					obj["myPosition"]["myY"].GetFloat(),
					obj["myPosition"]["myZ"].GetFloat()
				});
				tempModel.GetTransform().Scale({
					obj["myScale"]["myX"].GetFloat(),
					obj["myScale"]["myY"].GetFloat(),
					obj["myScale"]["myZ"].GetFloat()
				});
				CommonUtilities::Quatf quat(
					obj["myRotation"]["myW"].GetFloat(),
					obj["myRotation"]["myX"].GetFloat(),
					obj["myRotation"]["myY"].GetFloat(),
					obj["myRotation"]["myZ"].GetFloat()
				);
				tempModel.GetTransform().Rotate(quat.GetEulerAngles());
				continue;
			}
			else if (std::string(obj["myTag"].GetString()) == "Bird")
			{
				my3DBabyBirds[birdIndex].AddComponent<CAudioSourceComponent>(
					CAudioSourceComponent::SAudioComponentData(CommonUtilities::Vector3f(obj["myPosition"]["myX"].GetFloat(), obj["myPosition"]["myY"].GetFloat(), obj["myPosition"]["myZ"].GetFloat())));

				my3DBabyBirds[birdIndex].Move({
					obj["myPosition"]["myX"].GetFloat(),
					obj["myPosition"]["myY"].GetFloat(),
					obj["myPosition"]["myZ"].GetFloat()
				});
				CommonUtilities::Quatf quat(
					obj["myRotation"]["myW"].GetFloat(),
					obj["myRotation"]["myX"].GetFloat(),
					obj["myRotation"]["myY"].GetFloat(),
					obj["myRotation"]["myZ"].GetFloat()
				);
				my3DBabyBirds[birdIndex].GetTransform().Rotate(quat.GetEulerAngles());


				++birdIndex;
				continue;
			}
		}
	}
}

void CLevelSelectState::LoadCameras(JsonDocument& aDoc)
{
	for (int i = 0; i < aDoc["myCameras"].GetSize(); ++i)
	{
		auto obj = aDoc["myCameras"][i];
		CommonUtilities::Quatf quat(
			obj["myRotation"]["myW"].GetFloat(),
			obj["myRotation"]["myX"].GetFloat(),
			obj["myRotation"]["myY"].GetFloat(),
			obj["myRotation"]["myZ"].GetFloat()
		);
		myCameraTransforms[i].Rotate({
			quat.GetEulerAngles().x,
			quat.GetEulerAngles().y + CommonUtilities::Pif,
			quat.GetEulerAngles().z
		});
		myCameraTransforms[i].SetPosition({
			obj["myPosition"]["myX"].GetFloat(),
			obj["myPosition"]["myY"].GetFloat(),
			obj["myPosition"]["myZ"].GetFloat()
		});

		if (i >= EGate_Count)break;
	}

	myMainCamera.GetTransform().SetLookAtRotation({ 0.f, 0.f, 0.f }, myCameraTransforms[EGate_Tutorial].GetForward());
	myMainCamera.SetPosition(myCameraTransforms[EGate_Tutorial].GetPosition());
}

void CLevelSelectState::LoadPointLights(JsonDocument& aDoc)
{
	for (int i = 0; i < aDoc["myPointLights"].GetSize(); ++i)
	{
		auto obj = aDoc["myPointLights"][i];
		CGameObject gameObject;
		gameObject.Init(mySceneID);
		gameObject.AddComponent<CPointLightComponent>(CPointLightComponent::SPointLightComponentData(
			obj["myRange"].GetFloat(),
			{
				obj["myColor"]["myX"].GetFloat(),
				obj["myColor"]["myY"].GetFloat(),
				obj["myColor"]["myZ"].GetFloat()
			}
		));
		gameObject.SetPosition({
			obj["myPosition"]["myX"].GetFloat(),
			obj["myPosition"]["myY"].GetFloat(),
			obj["myPosition"]["myZ"].GetFloat()
		});
	}
}

void CLevelSelectState::LoadGUISprites()
{
	myGUISprites[ELevelSelectButtons_LeftArrow].Init(mySceneID);
	myGUISprites[ELevelSelectButtons_LeftArrow].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/leftButton.dds");
	myGUISprites[ELevelSelectButtons_LeftArrow].GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myGUISprites[ELevelSelectButtons_LeftArrow].SetActive(true);

	myGUISprites[ELevelSelectButtons_Play].Init(mySceneID);
	myGUISprites[ELevelSelectButtons_Play].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/startLevelBotton.dds");
	myGUISprites[ELevelSelectButtons_Play].GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myGUISprites[ELevelSelectButtons_Play].SetActive(true);

	myGUISprites[ELevelSelectButtons_Back].Init(mySceneID);
	myGUISprites[ELevelSelectButtons_Back].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/backButton.dds");
	myGUISprites[ELevelSelectButtons_Back].GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myGUISprites[ELevelSelectButtons_Back].SetActive(true);

	myGUISprites[ELevelSelectButtons_RightArrow].Init(mySceneID);
	myGUISprites[ELevelSelectButtons_RightArrow].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/rightButton.dds");
	myGUISprites[ELevelSelectButtons_RightArrow].GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myGUISprites[ELevelSelectButtons_RightArrow].SetActive(true);

	myGUISprites[ELevelSelectButtons_UnlockOK].Init(mySceneID);
	myGUISprites[ELevelSelectButtons_UnlockOK].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/okButton.dds");
	myGUISprites[ELevelSelectButtons_UnlockOK].GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myGUISprites[ELevelSelectButtons_UnlockOK].GetComponent<CSpriteComponent>()->SetPriority(1);
	myGUISprites[ELevelSelectButtons_UnlockOK].SetActive(false);
}

void CLevelSelectState::LoadButtons()
{
	CGButton* leftArrow = new CGButton();
	leftArrow->SetOnMouseReleased(std::bind(&CLevelSelectState::GoLeft, this));
	leftArrow->SetOnMouseOverStart(std::bind(&CLevelSelectState::OnHoverButton, this, ELevelSelectButtons_LeftArrow));
	leftArrow->SetOnMouseOverEnd(std::bind(&CLevelSelectState::EndHoverButton, this, ELevelSelectButtons_LeftArrow));
	leftArrow->SetPosition(myGUISprites[ELevelSelectButtons_LeftArrow].GetComponent<CSpriteComponent>()->GetPosition());
	leftArrow->SetSize(200.f, 140.f);
	myGUI.AddGUIObject(leftArrow);

	CGButton* rightArrow = new CGButton();
	rightArrow->SetOnMouseReleased(std::bind(&CLevelSelectState::GoRight, this));
	rightArrow->SetOnMouseOverStart(std::bind(&CLevelSelectState::OnHoverButton, this, ELevelSelectButtons_RightArrow));
	rightArrow->SetOnMouseOverEnd(std::bind(&CLevelSelectState::EndHoverButton, this, ELevelSelectButtons_RightArrow));
	rightArrow->SetPosition(myGUISprites[ELevelSelectButtons_RightArrow].GetComponent<CSpriteComponent>()->GetPosition());
	rightArrow->SetSize(200.f, 140.f);
	myGUI.AddGUIObject(rightArrow);

	CGButton* back = new CGButton();
	back->SetOnMouseReleased(std::bind(&CLevelSelectState::Back, this));
	back->SetOnMouseOverStart(std::bind(&CLevelSelectState::OnHoverButton, this, ELevelSelectButtons_Back));
	back->SetOnMouseOverEnd(std::bind(&CLevelSelectState::EndHoverButton, this, ELevelSelectButtons_Back));
	back->SetPosition(myGUISprites[ELevelSelectButtons_Back].GetComponent<CSpriteComponent>()->GetPosition());
	back->SetSize(190.f, 130.f);
	myGUI.AddGUIObject(back);

	CGButton* play = new CGButton();
	play->SetOnMouseReleased(std::bind(&CLevelSelectState::StartLevel, this));
	play->SetOnMouseOverStart(std::bind(&CLevelSelectState::OnHoverButton, this, ELevelSelectButtons_Play));
	play->SetOnMouseOverEnd(std::bind(&CLevelSelectState::EndHoverButton, this, ELevelSelectButtons_Play));
	play->SetPosition(myGUISprites[ELevelSelectButtons_Play].GetComponent<CSpriteComponent>()->GetPosition());
	play->SetSize(320.f, 140.f);
	myGUI.AddGUIObject(play);

	myGUI.SetIfMouseOnlyInput(true);

	CGButton* unlockMsgContinue = new CGButton();
	unlockMsgContinue->SetOnMouseReleased(std::bind(&CLevelSelectState::OnUnlockMessageClose, this));
	unlockMsgContinue->SetOnMouseOverStart(std::bind(&CLevelSelectState::OnHoverButton, this, ELevelSelectButtons_UnlockOK));
	unlockMsgContinue->SetOnMouseOverEnd(std::bind(&CLevelSelectState::EndHoverButton, this, ELevelSelectButtons_UnlockOK));
	unlockMsgContinue->SetPosition(myGUISprites[ELevelSelectButtons_UnlockOK].GetComponent<CSpriteComponent>()->GetPosition());
	unlockMsgContinue->SetSize(300.f, 300.f);
	myUnlockGUI.AddGUIObject(unlockMsgContinue);

	myUnlockGUI.SetDefaultFocusObject(unlockMsgContinue);
}

void CLevelSelectState::LoadUI()
{
	////////////// TimeToBeat (TEXT, ICON)
	myTimeToBeat.Init(mySceneID);
	CTextComponent* text = myTimeToBeat.AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");

	text->SetTint({ (250.f / 255.f), (192.f / 255.f), (63.f / 255.f), 1.f }); //GOLD COLOR
	text->SetText("00.00:00");
	text->SetScale({ 1.5f, 1.5f });

	CSpriteComponent* icon = myTimeToBeat.AddComponent<CSpriteComponent>("Assets/Sprites/uiTimerIconTimeAttack.dds");
	icon->SetPivot({ 0.5f,0.5f });
	icon->SetScale({ 0.5f, 0.5f });

	myTimeToBeat.SetActive(false);

	// Level Names
	myLevelNameDisplay[EGate_Tutorial].Init(mySceneID);
	CSpriteComponent* sprite = myLevelNameDisplay[EGate_Tutorial].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/level1Titel.dds");
	sprite->SetPosition(CommonUtilities::Vector2f(0.5f, 0.1f));
	sprite->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myLevelNameDisplay[EGate_Tutorial].SetActive(false);

	myLevelNameDisplay[EGate_Village].Init(mySceneID);
	sprite = myLevelNameDisplay[EGate_Village].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/level2Titel.dds");
	sprite->SetPosition(CommonUtilities::Vector2f(0.5f, 0.1f));
	sprite->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myLevelNameDisplay[EGate_Village].SetActive(false);

	myLevelNameDisplay[EGate_Pillars].Init(mySceneID);
	sprite = myLevelNameDisplay[EGate_Pillars].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/level3Titel.dds");
	sprite->SetPosition(CommonUtilities::Vector2f(0.5f, 0.1f));
	sprite->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myLevelNameDisplay[EGate_Pillars].SetActive(false);

	myLevelNameDisplay[EGate_Roots].Init(mySceneID);
	sprite = myLevelNameDisplay[EGate_Roots].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/level4Titel.dds");
	sprite->SetPosition(CommonUtilities::Vector2f(0.5f, 0.1f));
	sprite->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myLevelNameDisplay[EGate_Roots].SetActive(false);
	/////////// TIME (TEXT, ICON)
	myTime.Init(mySceneID);
	text = myTime.AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");
	text->SetText("00.00:00");
	text->SetTint({ (239.f / 255.f), (233.f / 255.f), (250.f / 255.f), 1.f });
	text->SetScale({ 1.5f, 1.5f });
	text->SetRotation(0);

	icon = myTime.AddComponent<CSpriteComponent>("Assets/Sprites/uiTimerIcon.dds");
	icon->SetScale({ 0.5f, 0.5f });
	icon->SetPivot({ 0.5f,0.5f });

	myTime.SetActive(false);

	////////////
	myShouldUpdateUI = true;
	///////////

	auto callback = [&](const std::wstring& aFilepath)
	{
		std::string fileName(aFilepath.begin(), aFilepath.end());
		myUIPositionsDoc.LoadFile(fileName.c_str(), false); //TODO: Set encode to true here and in SaveScoreToFile when we don't need to add anything more

		myShouldUpdateUI = true;
	};

	callback(L"levelSelectUIPositions.json");
	IWorld::GetFileWatcher().WatchFile(L"levelSelectUIPositions.json", callback);
	//IWorld::GetFileWatcher().StopWatchingFile(L"levelSelectUIPositions.json")
}

void CLevelSelectState::PlaceAllBabyBirds()
{
	//////////// BABY-BIRD ICONS
	for (unsigned short i = 0; i < 4; ++i)
	{
		char* lvlName = "";
		unsigned char lvl = 0;
		switch (i)
		{
		case 0: { lvl = 0; lvlName = "Tutorial"; break; }
		case 1: { lvl = 1; lvlName = "Village"; break; }
		case 2: { lvl = 2; lvlName = "Pillars"; break; }
		case 3: { lvl = 3; lvlName = "Roots"; break; }
		}

		if ((myScoreDoc.Find(lvlName) && myScoreDoc[lvlName].Find("played")) &&
			(myScoreDoc[lvlName]["played"].GetBool() && myScoreDoc[lvlName].Find("babyBirdCountHighscore")))
		{
			const unsigned short birdCount = static_cast<unsigned short>(myScoreDoc[lvlName]["babyBirdCountHighscore"].GetInt());

			SetBabyBirds(birdCount, lvl);
		}
		else
		{
			SetBabyBirds(0, lvl);
		}
	}
}

void CLevelSelectState::OnEnter()
{
	myHasStartedLevel = false;
	SetActiveScene();
	AM.Stop("Music/GameStateMusic", false, false);
	AM.Stop("Music/GameStateMusic2", false, false);
	AM.Play("Music/MenuMusic", false);
	//LOAD PLAYER SCORE
	myScoreDoc.LoadFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), true);

	if (myScoreDoc.Find("isFirstTimePlaying")) // DO THIS CHECK FOR BACKWARD-COMPATIBILITY
	{
		myFirstTimePlaying = myScoreDoc["isFirstTimePlaying"].GetBool();
	}
	else
	{
		myFirstTimePlaying = true;
		myScoreDoc.AddMember("isFirstTimePlaying", true);
	}
	if (myFirstTimePlaying)
	{
		myFirstTimePlayerPopUp.Play();
	}
	PlaceAllBabyBirds();

	char* lvlName;
	lvlName = "Tutorial";
	if (myScoreDoc.Find(lvlName))
	{
		if (myScoreDoc[lvlName].Find("locked"))
		{
			myLevelIsLocked[EGate_Tutorial] = myScoreDoc[lvlName]["locked"].GetBool();
		}
		if (myScoreDoc[lvlName].Find("played"))
		{
			myLevelIsPlayed[EGate_Tutorial] = myScoreDoc[lvlName]["played"].GetBool();
		}
		if (myScoreDoc[lvlName].Find("timeAttackMsgWasShown"))
		{
			myUnlockMsgWasShown[EGate_Tutorial] = myScoreDoc[lvlName]["timeAttackMsgWasShown"].GetBool();
		}
		else
		{
			myScoreDoc[lvlName].AddMember("timeAttackMsgWasShown", false);
			myUnlockMsgWasShown[EGate_Tutorial] = false;
		}
	}
	lvlName = "Village";
	if (myScoreDoc.Find(lvlName))
	{
		if (myScoreDoc[lvlName].Find("locked"))
		{
			myLevelIsLocked[EGate_Village] = myScoreDoc[lvlName]["locked"].GetBool();
		}
		if (myScoreDoc[lvlName].Find("played"))
		{
			myLevelIsPlayed[EGate_Village] = myScoreDoc[lvlName]["played"].GetBool();
		}
		if (myScoreDoc[lvlName].Find("timeAttackMsgWasShown"))
		{
			myUnlockMsgWasShown[EGate_Village] = myScoreDoc[lvlName]["timeAttackMsgWasShown"].GetBool();
		}
		else
		{
			myScoreDoc[lvlName].AddMember("timeAttackMsgWasShown", false);
			myUnlockMsgWasShown[EGate_Village] = false;
		}
	}
	lvlName = "Pillars";
	if (myScoreDoc.Find(lvlName))
	{
		if (myScoreDoc[lvlName].Find("locked"))
		{
			myLevelIsLocked[EGate_Pillars] = myScoreDoc[lvlName]["locked"].GetBool();
		}
		if (myScoreDoc[lvlName].Find("played"))
		{
			myLevelIsPlayed[EGate_Pillars] = myScoreDoc[lvlName]["played"].GetBool();
		}
		if (myScoreDoc[lvlName].Find("timeAttackMsgWasShown"))
		{
			myUnlockMsgWasShown[EGate_Pillars] = myScoreDoc[lvlName]["timeAttackMsgWasShown"].GetBool();
		}
		else
		{
			myScoreDoc[lvlName].AddMember("timeAttackMsgWasShown", false);
			myUnlockMsgWasShown[EGate_Pillars] = false;
		}
	}
	lvlName = "Roots";
	if (myScoreDoc.Find(lvlName))
	{
		if (myScoreDoc[lvlName].Find("locked"))
		{
			myLevelIsLocked[EGate_Roots] = myScoreDoc[lvlName]["locked"].GetBool();
		}
		if (myScoreDoc[lvlName].Find("played"))
		{
			myLevelIsPlayed[EGate_Roots] = myScoreDoc[lvlName]["played"].GetBool();
		}
		if (myScoreDoc[lvlName].Find("timeAttackMsgWasShown"))
		{
			myUnlockMsgWasShown[EGate_Roots] = myScoreDoc[lvlName]["timeAttackMsgWasShown"].GetBool();
		}
		else
		{
			myScoreDoc[lvlName].AddMember("timeAttackMsgWasShown", false);
			myUnlockMsgWasShown[EGate_Roots] = false;
		}
	}
	switch (myCurrentLevel)
	{
	case 0: { myGUISprites[EGate_Tutorial].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(0.1f, 0.1f, 0.1f, 0.1f)); myLevelNameDisplay[EGate_Tutorial].SetActive(true); break; }
	case 1: { myLevelNameDisplay[EGate_Village].SetActive(true); break; }
	case 2: { myLevelNameDisplay[EGate_Pillars].SetActive(true); break; }
	case 3: { myGUISprites[EGate_Roots].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(0.1f, 0.1f, 0.1f, 0.1f)); myLevelNameDisplay[EGate_Roots].SetActive(true);	break; }
	}

	if (myLevelIsPlayed[myCurrentLevel] && !myUnlockMsgWasShown[myCurrentLevel])
	{
		if (myLevelIsPlayed[EGate_Roots] && !myUnlockMsgWasShown[EGate_Roots] && !myPlayedEndStory)
		{
			myUnlockMsgWindow.GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(0.0f, 0.0f, 0.0f, 0.0f));
			myStoryEndPopUp.Play();
			myPlayedEndStory = true;
		}
		switch (myCurrentLevel)
		{
		case CLevelSelectState::EGate_Tutorial:
			AM.PlayNewInstance("SFX/SoundEffects/LevelUnlock", nullptr);
			OnUnlockMessageOpen("Tutorial");
			break;
		case CLevelSelectState::EGate_Village:
			AM.PlayNewInstance("SFX/SoundEffects/LevelUnlock", nullptr);
			OnUnlockMessageOpen("Village");
			break;
		case CLevelSelectState::EGate_Pillars:
			AM.PlayNewInstance("SFX/SoundEffects/LevelUnlock", nullptr);
			OnUnlockMessageOpen("Pillars");
			break;
		case CLevelSelectState::EGate_Roots:
			OnUnlockMessageOpen("Roots");
			break;
		default:
			break;
		}
	}

	myScoreDoc.SaveFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), false, true);
	myShouldUpdateUI = true;
}

void CLevelSelectState::OnLeave()
{
}

void CLevelSelectState::SetBabyBirds(const unsigned int& aBabyBirdCountHighscore, const unsigned short& aLevel)
{
	unsigned short birdStartIndex = 0;
	switch (aLevel)
	{
	case 0: birdStartIndex = 0; break;
	case 1: birdStartIndex = 4; break;
	case 2: birdStartIndex = 8; break;
	case 3: birdStartIndex = 12; break;
	}

	unsigned short currentBirdsSet = 0;

	unsigned short lastBird = birdStartIndex + 3;
	bool beatTimeAttack = false;

	char* lvlName = "";
	switch (aLevel)
	{
	case 0: { lvlName = "Tutorial"; break; }
	case 1: { lvlName = "Village"; break; }
	case 2: { lvlName = "Pillars"; break; }
	case 3: { lvlName = "Roots"; break; }
	}

	if (myScoreDoc.Find(lvlName) && myScoreDoc[lvlName].Find("timeAttackBeaten"))
	{
		if (myScoreDoc[lvlName]["timeAttackBeaten"].GetBool())
		{
			lastBird = birdStartIndex + 4;
			beatTimeAttack = true;
		}
	}

	for (unsigned short i = birdStartIndex; i < lastBird; ++i)
	{
		if (aBabyBirdCountHighscore > 0 || beatTimeAttack)
		{
			if (currentBirdsSet < aBabyBirdCountHighscore || beatTimeAttack)
			{
				if (beatTimeAttack && currentBirdsSet >= aBabyBirdCountHighscore && i < lastBird - 1)
				{
					i += (lastBird - 1) - i;
				}

				my3DBabyBirds[i].SetActive(true);
			}
		}
		else
		{
			my3DBabyBirds[i].SetActive(false);
		}

		++currentBirdsSet;
	}
}

void CLevelSelectState::UpdateUIPositions()
{
	////////// BUTTONS
	(myUIPositionsDoc.Find("Buttons") && myUIPositionsDoc["Buttons"].Find("leftArrow")) ?
		myGUISprites[ELevelSelectButtons_LeftArrow].GetComponent<CSpriteComponent>()->SetPosition({ myUIPositionsDoc["Buttons"]["leftArrow"]["x"].GetFloat(), myUIPositionsDoc["Buttons"]["leftArrow"]["y"].GetFloat() }) //JSON
		: myGUISprites[ELevelSelectButtons_LeftArrow].GetComponent<CSpriteComponent>()->SetPosition({ 0.06f, 0.95f }); //DEFAULT

	(myUIPositionsDoc.Find("Buttons") && myUIPositionsDoc["Buttons"].Find("startLevel")) ?
		myGUISprites[ELevelSelectButtons_Play].GetComponent<CSpriteComponent>()->SetPosition({ myUIPositionsDoc["Buttons"]["startLevel"]["x"].GetFloat(), myUIPositionsDoc["Buttons"]["startLevel"]["y"].GetFloat() }) //JSON
		: myGUISprites[ELevelSelectButtons_Play].GetComponent<CSpriteComponent>()->SetPosition({ 0.5f, 0.95f }); //DEFAULT

	(myUIPositionsDoc.Find("Buttons") && myUIPositionsDoc["Buttons"].Find("back")) ?
		myGUISprites[ELevelSelectButtons_Back].GetComponent<CSpriteComponent>()->SetPosition({ myUIPositionsDoc["Buttons"]["back"]["x"].GetFloat(), myUIPositionsDoc["Buttons"]["back"]["y"].GetFloat() }) //JSON
		: myGUISprites[ELevelSelectButtons_Back].GetComponent<CSpriteComponent>()->SetPosition({ 0.08f, 0.05f }); //DEFAULT

	(myUIPositionsDoc.Find("Buttons") && myUIPositionsDoc["Buttons"].Find("rightArrow")) ?
		myGUISprites[ELevelSelectButtons_RightArrow].GetComponent<CSpriteComponent>()->SetPosition({ myUIPositionsDoc["Buttons"]["rightArrow"]["x"].GetFloat(), myUIPositionsDoc["Buttons"]["rightArrow"]["y"].GetFloat() }) //JSON
		: myGUISprites[ELevelSelectButtons_RightArrow].GetComponent<CSpriteComponent>()->SetPosition({ 0.94f, 0.95f }); //DEFAULT

	(myUIPositionsDoc.Find("Buttons") && myUIPositionsDoc["Buttons"].Find("unlockOK")) ?
		myGUISprites[ELevelSelectButtons_UnlockOK].GetComponent<CSpriteComponent>()->SetPosition({ myUIPositionsDoc["Buttons"]["unlockOK"]["x"].GetFloat(), myUIPositionsDoc["Buttons"]["unlockOK"]["y"].GetFloat() }) //JSON
		: myGUISprites[ELevelSelectButtons_UnlockOK].GetComponent<CSpriteComponent>()->SetPosition({ 0.5f, 0.65f }); //DEFAULT


	///////////// UI
	(myUIPositionsDoc.Find(myLevelName.c_str()) && myUIPositionsDoc[myLevelName.c_str()].Find("timeToBeatText")) ?
		myTimeToBeat.GetComponent<CTextComponent>()->SetPosition({ myUIPositionsDoc[myLevelName.c_str()]["timeToBeatText"]["x"].GetFloat(), myUIPositionsDoc[myLevelName.c_str()]["timeToBeatText"]["y"].GetFloat() }) //JSON
		: myTimeToBeat.GetComponent<CTextComponent>()->SetPosition({ 0.5f, 0.4f }); //DEFAULT

	(myUIPositionsDoc.Find(myLevelName.c_str()) && myUIPositionsDoc[myLevelName.c_str()].Find("timeToBeatIcon")) ?
		myTimeToBeat.GetComponent<CSpriteComponent>()->SetPosition({ myUIPositionsDoc[myLevelName.c_str()]["timeToBeatIcon"]["x"].GetFloat(), myUIPositionsDoc[myLevelName.c_str()]["timeToBeatIcon"]["y"].GetFloat() }) //JSON
		: myTimeToBeat.GetComponent<CSpriteComponent>()->SetPosition({ 0.6f, 0.44f }); //DEFAULT

	(myUIPositionsDoc.Find(myLevelName.c_str()) && myUIPositionsDoc[myLevelName.c_str()].Find("timeIcon")) ?
		myTime.GetComponent<CSpriteComponent>()->SetPosition({ myUIPositionsDoc[myLevelName.c_str()]["timeIcon"]["x"].GetFloat(), myUIPositionsDoc[myLevelName.c_str()]["timeIcon"]["y"].GetFloat() }) //JSON
		: myTime.GetComponent<CSpriteComponent>()->SetPosition({ 0.3f, 0.45f }); //DEFAULT

	(myUIPositionsDoc.Find(myLevelName.c_str()) && myUIPositionsDoc[myLevelName.c_str()].Find("timeText")) ?
		myTime.GetComponent<CTextComponent>()->SetPosition({ myUIPositionsDoc[myLevelName.c_str()]["timeText"]["x"].GetFloat(), myUIPositionsDoc[myLevelName.c_str()]["timeText"]["y"].GetFloat() }) //JSON
		: myTime.GetComponent<CTextComponent>()->SetPosition({ 0.35f, 0.42f }); //DEFAULT

}

void CLevelSelectState::UpdateUI()
{
	if (myScoreDoc.Find(myLevelName.c_str()))
	{
		if (myScoreDoc[myLevelName.c_str()].Find("played"))
		{
			if (myScoreDoc[myLevelName.c_str()]["played"].GetBool())
			{
				short startIndex = 0;
				switch (myCurrentLevel)
				{
				case 0: { startIndex = 0; myLevelNameDisplay[EGate_Tutorial].SetActive(true); break; }
				case 1: { startIndex = 4; myLevelNameDisplay[EGate_Village].SetActive(true); break; }
				case 2: { startIndex = 8; myLevelNameDisplay[EGate_Pillars].SetActive(true); break; }
				case 3: { startIndex = 12; myLevelNameDisplay[EGate_Roots].SetActive(true);	break; }
				}

				for (int i = startIndex; i <= (startIndex + 3); ++i)
				{
					if (my3DBabyBirds[i].IsActive())
					{
						my3DBabyBirds[i].GetComponent<CParticleSystemComponent>()->PlayInstant();
					}
				}


				if (myScoreDoc[myLevelName.c_str()].Find("playedTimeAttack"))
				{
					myTime.SetActive(true);

					if (myScoreDoc[myLevelName.c_str()]["playedTimeAttack"].GetBool())
					{
						CTextComponent* text = myTime.GetComponent<CTextComponent>();

						float timeHighscore = 0.0f;
						if (myScoreDoc[myLevelName.c_str()].Find("bestTime"))
						{
							timeHighscore = (myScoreDoc[myLevelName.c_str()]["bestTime"].GetFloat());
						}
						std::ostringstream yourTimeSS;
						const unsigned short mins = static_cast<unsigned short>(timeHighscore) / 60;
						const unsigned short secs = static_cast<unsigned short>(timeHighscore) % 60;
						const unsigned short millisecs = static_cast<unsigned short>(timeHighscore * 100.f) % 100;
						yourTimeSS << std::setfill('0') << std::setw(2) << mins << ":" << std::setfill('0') << std::setw(2) << secs << "." << std::setfill('0') << std::setw(2) << millisecs;

						text->SetText(yourTimeSS.str());
						text->SetRotation(0);
					}
					else
					{
						myTime.GetComponent<CTextComponent>()->SetText("--:--.--");
					}

					////////////////////// TimeToBeat
					myTimeToBeat.SetActive(true);
					CTextComponent* text = myTimeToBeat.GetComponent<CTextComponent>();

					JsonDocument timeToBeatJSON;
					timeToBeatJSON.LoadFile("settings.json");

					float timeToBeat = 180.f;
					const std::string timeToBeatMember = "timeToBeat" + myLevelName;

					if (timeToBeatJSON.Find("misc") && timeToBeatJSON["misc"].Find(timeToBeatMember.c_str()))
					{
						timeToBeat = timeToBeatJSON["misc"][timeToBeatMember.c_str()].GetFloat();
					}
					else
					{
						// should never get here, but just in case
						GAMEPLAY_LOG(CONCOL_WARNING, "Settings-file missing 'misc' or 'timeToBeat' in 'misc'!!");
					}

					std::ostringstream timeToBeatSS;
					const unsigned short mins = static_cast<unsigned short>(timeToBeat) / 60;
					const unsigned short secs = static_cast<unsigned short>(timeToBeat) % 60;
					const unsigned short millisecs = static_cast<unsigned short>(timeToBeat * 100.f) % 100;
					timeToBeatSS << std::setfill('0') << std::setw(2) << mins << ":" << std::setfill('0') << std::setw(2) << secs << "." << std::setfill('0') << std::setw(2) << millisecs;

					text->SetText(timeToBeatSS.str());
					text->SetRotation(0);


					UpdateUIPositions();
				}
				else
				{
					myScoreDoc[myLevelName.c_str()].AddMember("playedTimeAttack", false);
					myScoreDoc.SaveFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), false, true);
				}
			}
		}
		else
		{
			myScoreDoc[myLevelName.c_str()].AddMember("played", false);
			myScoreDoc.SaveFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), false, true);
		}
	}
}

void CLevelSelectState::HandleInput()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::XBOXController* xbox = &IWorld::XBOX();
	const float xboxStickSensetivity = 0.7f;
	const bool xBoxStickMovedRight(xbox->GetStick(CommonUtilities::XStick_Left).x > xboxStickSensetivity);
	const bool xBoxStickMovedLeft(xbox->GetStick(CommonUtilities::XStick_Left).x < -xboxStickSensetivity);
	myXBoxInputTimer += IWorld::Time().GetDeltaTime();
	if (myXBoxInputTimer > 0.8f)
	{
		myAllowXBoxInput = true;
		myXBoxInputTimer = 0.0f;
	}
	if (myCurrentLevel == EGate_Tutorial)
	{
		myGUISprites[ELevelSelectButtons_LeftArrow].GetComponent<CSpriteComponent>()->SetTint({ 0.1f, 0.1f, 0.1f, 0.1f });
	}
	if (myCurrentLevel == EGate_Roots)
	{
		myGUISprites[ELevelSelectButtons_RightArrow].GetComponent<CSpriteComponent>()->SetTint({ 0.1f, 0.1f, 0.1f, 0.1f });
	}

	if ((input.IsKeyPressed(Input::Key_Right) || input.IsKeyPressed(Input::Key_D) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_Right) || xBoxStickMovedRight) && myAllowXBoxInput)
	{
		myAllowXBoxInput = false;
		GoRight();
	}
	else if ((input.IsKeyPressed(Input::Key_Left) || input.IsKeyPressed(Input::Key_A) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_Left) || xBoxStickMovedLeft) && myAllowXBoxInput)
	{
		myAllowXBoxInput = false;
		GoLeft();
	}

	if (input.IsKeyPressed(Input::Key_Space) || input.IsKeyPressed(Input::Key_Return) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_A))
	{
		StartLevel();
	}
	else if (IWorld::Input().IsKeyPressed(Input::Key_Escape) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_Back) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_B))
	{
		Back();
	}
}

EStateUpdate CLevelSelectState::Update()
{
	float dt = IWorld::Time().GetDeltaTime();
	myFirstTimePlayerPopUp.Update();
	myStoryEndPopUp.Update();
	if (myFirstTimePlaying)
	{
		myFirstTimePlaying = false;
		if (myScoreDoc.Find("isFirstTimePlaying"))
		{
			myScoreDoc["isFirstTimePlaying"].SetBool(myFirstTimePlaying);
		}
		//else
		//{
		//	myScoreDoc.AddMember("isFirstTimePlaying").SetBool(myFirstTimePlaying);
		//	myScoreDoc["isFirstTimePlaying"].SetBool(myFirstTimePlaying);
		//}
		myScoreDoc.SaveFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), false, true);
	}
	else if (myUnlockMsgWindow.IsActive() || myLastUnlockMsgWindow.IsActive())
	{
		if (myLevelIsPlayed[EGate_Roots] && !myUnlockMsgWasShown[EGate_Roots] && !myPlayedEndStory)
		{
			myPlayedEndStory = true;
		}
		else if (!myShownEndCredits && myPlayedEndStory)
		{
			if (!myStoryEndPopUp.GetIsPlaying())
			{
				CCreditsState* creditState = new CCreditsState();
				creditState->Init();
				ourStateStack->PushSubState(creditState);
				myShownEndCredits = true;
			}
		}
		else
		{
			myUnlockMsgWindow.GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
			myUnlockGUI.Update();
		}
	}
	else if (!myFirstTimePlayerPopUp.GetIsPlaying() && !myStoryEndPopUp.GetIsPlaying())
	{
		myGUI.Update();

		if (myCurrentLevel < 3)
		{
			myLevelIsLocked[myCurrentLevel + 1] = !myLevelIsPlayed[myCurrentLevel];
		}

		static float birdChipTimer = 0.f;
		birdChipTimer += IWorld::Time().GetDeltaTime();
		int birdToChip = std::rand() % 3 + 3 * myCurrentLevel;
		if (birdChipTimer > 1.f)
		{
			if (my3DBabyBirds[birdToChip].IsActive())
			{
				my3DBabyBirds[birdToChip].GetComponent<CAudioSourceComponent>()->PlayNewInstance("SFX/SoundEffects/BabyBirdChip", AudioChannel::SoundEffects);
			}
			birdChipTimer = 0.f;
		}

		myMainCamera.GetTransform().SetLookAtRotation({ 0.f, 0.f, 0.f }, CommonUtilities::Slerp(myMainCamera.GetTransform().GetForward(), myCameraTransforms[myCurrentLevel].GetForward(), dt*5.f));

		CommonUtilities::Vector3f posToLerpTo = CommonUtilities::Lerp(myMainCamera.GetTransform().GetPosition(), myCameraTransforms[myCurrentLevel].GetPosition(), dt*5.f);
		const CommonUtilities::Vector3f diff = myCameraTransforms[myCurrentLevel].GetPosition() - myMainCamera.GetTransform().GetPosition();

		myMainCamera.SetPosition(posToLerpTo);

		if (diff.Length2() > 0.1f)
		{
			myShouldUpdateUI = true;
		}

		if (myShouldUpdateUI && diff.Length2() <= 0.1f)
		{
			myShouldUpdateUI = false;

			if (myLevelIsLocked[myCurrentLevel])
			{
				myGUISprites[ELevelSelectButtons_Play].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(0.1f, 0.1f, 0.1f, 0.5f));
			}
			else
			{
				myGUISprites[ELevelSelectButtons_Play].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.f, 1.f, 1.f, 1.f));
			}

			switch (myCurrentLevel)
			{
			case 0: { myLevelNameDisplay[EGate_Tutorial].SetActive(true); break; }
			case 1: { myLevelNameDisplay[EGate_Village].SetActive(true); break; }
			case 2: { myLevelNameDisplay[EGate_Pillars].SetActive(true); break; }
			case 3: { myLevelNameDisplay[EGate_Roots].SetActive(true);	break; }
			}

			UpdateLevelName();
			UpdateUI();
		}

		if (myShouldPop)
		{
			return EPop_Main;
		}

		HandleInput();
	}

	return EDoNothing;
}

const int CLevelSelectState::GetCurrentLevel()
{
	return myCurrentLevel;
}

void CLevelSelectState::MakeIconsInvisible()
{
	myTimeToBeat.SetActive(false);
	myTime.SetActive(false);
	myLevelNameDisplay[EGate_Tutorial].SetActive(false);
	myLevelNameDisplay[EGate_Village].SetActive(false);
	myLevelNameDisplay[EGate_Pillars].SetActive(false);
	myLevelNameDisplay[EGate_Roots].SetActive(false);

	short startIndex = 0;
	switch (myCurrentLevel)
	{
	case 0: { startIndex = 0; break; }
	case 1: { startIndex = 4; break; }
	case 2: { startIndex = 8; break; }
	case 3: { startIndex = 12; break; }
	}

	for (int i = startIndex; i <= (startIndex + 3); ++i)
	{
		if (my3DBabyBirds[i].IsActive())
		{
			my3DBabyBirds[i].GetComponent<CParticleSystemComponent>()->Stop();
		}
	}
}

void CLevelSelectState::OnUnlockMessageOpen(std::string aLevelName)
{
	if (myScoreDoc.Find(aLevelName.c_str()))
	{
		myScoreDoc[aLevelName.c_str()]["timeAttackMsgWasShown"].SetBool(true);
	}
	if (myCurrentLevel == 3)
	{
		myLastUnlockMsgWindow.SetActive(true);
	}
	else
	{
		myUnlockMsgWindow.SetActive(true);
	}
	myGUISprites[ELevelSelectButtons_UnlockOK].SetActive(true);
	for (unsigned short i = 0; i < myGUISprites.size() - 1; ++i)
	{
		myGUISprites[i].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(0.1f, 0.1f, 0.1f, 0.5f));
	}
}

void CLevelSelectState::OnUnlockMessageClose()
{
	myUnlockMsgWasShown[myCurrentLevel] = true;
	if (myCurrentLevel == 3)
	{
		myLastUnlockMsgWindow.SetActive(false);
	}
	else
	{
		myUnlockMsgWindow.SetActive(false);
		myLevelIsLocked[myCurrentLevel + 1] = false;
	}
	myGUISprites[ELevelSelectButtons_UnlockOK].SetActive(false);
	for (unsigned short i = 0; i < myGUISprites.size() - 1; ++i)
	{
		myGUISprites[i].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.f, 1.f, 1.f, 1.f));
	}
	GoRight();
}

void CLevelSelectState::GoLeft()
{
	if (myCurrentLevel > 0)
	{
		myGUISprites[ELevelSelectButtons_RightArrow].GetComponent<CSpriteComponent>()->SetTint({ 1.0f, 1.0f, 1.0f, 1.0f });
		AM.PlayNewInstance("SFX/SoundEffects/CameraTurn");
		MakeIconsInvisible();
		--myCurrentLevel;
	}
}

void CLevelSelectState::GoRight()
{
	if (myCurrentLevel < 3)
	{
		myGUISprites[ELevelSelectButtons_LeftArrow].GetComponent<CSpriteComponent>()->SetTint({ 1.0f, 1.0f, 1.0f, 1.0f });
		AM.PlayNewInstance("SFX/SoundEffects/CameraTurn");
		MakeIconsInvisible();

		++myCurrentLevel;
	}
}

void CLevelSelectState::Back()
{
	AM.PlayNewInstance("SFX/Buttons/ButtonBack", nullptr, AudioChannel::SoundEffects);
	myShouldPop = true;
}

void CLevelSelectState::StartLevel()
{
	if (!myLevelIsLocked[myCurrentLevel] && !myHasStartedLevel)
	{
		AM.PlayNewInstance("SFX/Buttons/Startlevel", nullptr, AudioChannel::SoundEffects);

		CGameState* gameState = new CGameState();
		myHasStartedLevel = ourStateStack->LoadAsync([=]()
		{
			switch (myCurrentLevel)
			{
			case 0: {gameState->Init("Assets/Levels/Tutorial.json"); break; }
			case 1: {gameState->Init("Assets/Levels/Village.json"); break; }
			case 2: {gameState->Init("Assets/Levels/Pillars.json"); break; }
			case 3: {gameState->Init("Assets/Levels/Roots.json"); break; }
			}
			ourStateStack->StartFadeIn([=] {ourStateStack->PushMainState(gameState); });
		});
	}
}

void CLevelSelectState::OnHoverButton(ELevelSelectButtons aButton)
{
	if (aButton == ELevelSelectButtons_Play && myLevelIsLocked[myCurrentLevel])
	{
		myGUISprites[ELevelSelectButtons_Play].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(0.1f, 0.1f, 0.1f, 0.5f));
	}
	else
	{
		myGUISprites[aButton].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.06f, 1.06f));
		myGUISprites[aButton].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.1f, 1.1f, 1.1f, 1.0f));
	}
}

void CLevelSelectState::EndHoverButton(ELevelSelectButtons aButton)
{
	if (aButton == ELevelSelectButtons_Play && myLevelIsLocked[myCurrentLevel])
	{
		myGUISprites[ELevelSelectButtons_Play].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(0.1f, 0.1f, 0.1f, 0.5f));
	}
	else
	{
		myGUISprites[aButton].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.0f, 1.0f));
		myGUISprites[aButton].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

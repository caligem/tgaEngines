#pragma once
#include "State.h"

#include "GameObject.h"
#include "Transform.h"
#include "GUI.h"
#include <JsonDocument.h>

#include <array>
#include "PopUp.h"

class CLevelSelectState : public CState
{
public:
	enum EGate
	{
		EGate_Tutorial,
		EGate_Village,
		EGate_Pillars,
		EGate_Roots,
		EGate_Count
	};
	enum ELevelSelectButtons
	{
		ELevelSelectButtons_LeftArrow,
		ELevelSelectButtons_RightArrow,
		ELevelSelectButtons_Play,
		ELevelSelectButtons_Back,
		ELevelSelectButtons_UnlockOK,
		ELevelSelectButtons_Count
	};

	CLevelSelectState();
	~CLevelSelectState();

	void Init(const std::string& aFilepath);
	void OnEnter() override;
	void OnLeave() override;

	EStateUpdate Update() override;
	const int GetCurrentLevel();

private:
	//CallBack
	void GoLeft();
	void GoRight();
	void Back();
	void StartLevel();
	void OnHoverButton(ELevelSelectButtons aButton);
	void EndHoverButton(ELevelSelectButtons aButton);

	void PlaceAllBabyBirds();
	void SetBabyBirds(const unsigned int& aBabyBirdCountHighscore, const unsigned short& aLevel);
	void MakeIconsInvisible();
	void OnUnlockMessageOpen(std::string aLevelName);
	void OnUnlockMessageClose();

	void UpdateLevelName();
	void UpdateUI();
	void UpdateUIPositions();
	void HandleInput();

	/////LOAD
	void LoadGameObjects(JsonDocument& aDoc);
	void LoadCameras(JsonDocument& aDoc);
	void LoadPointLights(JsonDocument& aDoc);
	void LoadGUISprites();
	void LoadButtons();
	void LoadUI();

	CGUI myGUI;
	CGUI myUnlockGUI;
	CPopUpSprite myFirstTimePlayerPopUp;
	CPopUpSprite myStoryEndPopUp;
	std::array<CGameObject, ELevelSelectButtons_Count> myGUISprites;
	std::array<CTransform, EGate_Count> myCameraTransforms;
	std::array<bool, EGate_Count> myLevelIsLocked;
	std::array<bool, EGate_Count> myLevelIsPlayed;
	std::array<bool, EGate_Count> myUnlockMsgWasShown;
	std::array<CGameObject, EGate_Count> myLevelNameDisplay;
	char myCurrentLevel = 0;
	
	JsonDocument myScoreDoc;
	JsonDocument myUIPositionsDoc;

	int myTotalNrOfHoops;
	int myHoopHighScore;
	bool myShouldUpdateUI;
	bool myAllowXBoxInput;
	float myXBoxInputTimer;

	CGameObject myTime;
	CGameObject myTimeToBeat;
	CGameObject myUnlockMsgWindow;
	CGameObject myLastUnlockMsgWindow;
	std::array<CGameObject, 16> my3DBabyBirds;

	std::string myLevelName;
	
	bool myHasStartedLevel;
	bool myShouldPop;
	bool myFirstTimePlaying;
	bool myPlayedEndStory;
	bool myShownEndCredits;
};


#include "stdafx.h"
#include "LoadingScreenState.h"

#include <JsonDocument.h>
#include <Quaternion.h>

#include "TextComponent.h"

CLoadingScreenState::CLoadingScreenState()
{
}


CLoadingScreenState::~CLoadingScreenState()
{
}

bool CLoadingScreenState::Init()
{
	CState::Init();

	JsonDocument doc;
	doc.LoadFile("Assets/Levels/LoadingScreen.json");

	std::map<int, std::string> meshFilters;
	if (doc.Find("myMeshFilters"))
	{
		for (int i = 0; i < doc["myMeshFilters"].GetSize(); ++i)
		{
			meshFilters[doc["myMeshFilters"][i]["myParent"].GetInt()] = doc["myMeshFilters"][i]["myPath"].GetString();
		}
	}

	if (doc.Find("myGameObjects"))
	{
		for (int i = 0; i < doc["myGameObjects"].GetSize(); ++i)
		{
			auto obj = doc["myGameObjects"][i];
			if (meshFilters.find(obj["myID"].GetInt()) != meshFilters.end())
			{
				CGameObject tempModel;
				tempModel.Init(mySceneID);
				tempModel.AddComponent<CModelComponent>(meshFilters[obj["myID"].GetInt()].c_str());

				tempModel.Move({
					obj["myPosition"]["myX"].GetFloat(),
					obj["myPosition"]["myY"].GetFloat(),
					obj["myPosition"]["myZ"].GetFloat()
				});
				tempModel.GetTransform().Scale({
					obj["myScale"]["myX"].GetFloat(),
					obj["myScale"]["myY"].GetFloat(),
					obj["myScale"]["myZ"].GetFloat()
				});
				CommonUtilities::Quatf quat(
					obj["myRotation"]["myW"].GetFloat(),
					obj["myRotation"]["myX"].GetFloat(),
					obj["myRotation"]["myY"].GetFloat(),
					obj["myRotation"]["myZ"].GetFloat()
				);

				tempModel.GetTransform().Rotate(quat.GetEulerAngles());
			}
			
			if (!obj.Find("myTag"))
			{
				continue;
			}

			if (std::string(obj["myTag"].GetString()) == "MainCamera")
			{
				myMainCamera.SetPosition({
					obj["myPosition"]["myX"].GetFloat(),
					obj["myPosition"]["myY"].GetFloat(),
					obj["myPosition"]["myZ"].GetFloat()
				});
				CommonUtilities::Quatf quat(
					obj["myRotation"]["myW"].GetFloat(),
					obj["myRotation"]["myX"].GetFloat(),
					obj["myRotation"]["myY"].GetFloat(),
					obj["myRotation"]["myZ"].GetFloat()
				);
				myMainCamera.GetTransform().SetRotation({
					-quat.GetEulerAngles().x,
					quat.GetEulerAngles().y + CommonUtilities::Pif,
					quat.GetEulerAngles().z
				});
			}
			else if (std::string(obj["myTag"].GetString()) == "ParticleSystem")
			{
				CGameObject tempModel;
				tempModel.Init(mySceneID);

				std::string filePath = "Assets/Particles/";
				filePath += obj["myName"].GetString();
				filePath += ".json";

				tempModel.AddComponent<CParticleSystemComponent>(filePath.c_str());

				tempModel.Move({
					obj["myPosition"]["myX"].GetFloat(),
					obj["myPosition"]["myY"].GetFloat(),
					obj["myPosition"]["myZ"].GetFloat()
				});
				tempModel.GetTransform().Scale({
					obj["myScale"]["myX"].GetFloat(),
					obj["myScale"]["myY"].GetFloat(),
					obj["myScale"]["myZ"].GetFloat()
				});
				CommonUtilities::Quatf quat(
					obj["myRotation"]["myW"].GetFloat(),
					obj["myRotation"]["myX"].GetFloat(),
					obj["myRotation"]["myY"].GetFloat(),
					obj["myRotation"]["myZ"].GetFloat()
				);
				tempModel.GetTransform().Rotate(quat.GetEulerAngles());
				continue;
			}
		}
	}

	//PointLights
	for (int i = 0; i < doc["myPointLights"].GetSize(); ++i)
	{
		auto obj = doc["myPointLights"][i];
		CGameObject gameObject;
		gameObject.Init(mySceneID);
		gameObject.AddComponent<CPointLightComponent>(CPointLightComponent::SPointLightComponentData(
			obj["myRange"].GetFloat(),
			{
				obj["myColor"]["myX"].GetFloat(),
				obj["myColor"]["myY"].GetFloat(),
				obj["myColor"]["myZ"].GetFloat()
			}
		));
		gameObject.SetPosition({
			obj["myPosition"]["myX"].GetFloat(),
			obj["myPosition"]["myY"].GetFloat(),
			obj["myPosition"]["myZ"].GetFloat()
		});
	}

	CGameObject blackBackground;
	blackBackground.Init(mySceneID);
	CSpriteComponent* black = blackBackground.AddComponent<CSpriteComponent>("Assets/Sprites/SplashScreen/black.dds");
	black->SetPosition({ 0.5f, 0.5f });
	black->SetPivot({ 0.5f, 0.5f });
	black->SetScaleRelativeToScreen({ 1.0f, 1.0f });
	black->SetTint({ 0.f, 0.f, 0.f, 0.5f });

	CGameObject loadingText;
	loadingText.Init(mySceneID);
	CTextComponent* text = loadingText.AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");
	text->SetText("Loading...");
	text->SetPosition({ 0.5f, 0.5f });
	text->SetPivot({ 0.5f, 0.5f });
	
	return true;
}

EStateUpdate CLoadingScreenState::Update()
{
	return EDoNothing;
}

void CLoadingScreenState::OnEnter()
{
	SetActiveScene();
}

void CLoadingScreenState::OnLeave()
{
}


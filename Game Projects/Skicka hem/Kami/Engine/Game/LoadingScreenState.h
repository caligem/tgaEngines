#pragma once
#include "State.h"

class CLoadingScreenState : public CState
{
public:
	CLoadingScreenState();
	~CLoadingScreenState();

	virtual bool Init() override;

	virtual EStateUpdate Update() override;

	virtual void OnEnter() override;

	virtual void OnLeave() override;

private:


};


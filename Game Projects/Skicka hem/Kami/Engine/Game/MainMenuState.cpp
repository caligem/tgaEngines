#include "stdafx.h"
#include "MainMenuState.h"

#include "GUIBase.h"
#include "SceneManager.h"
#include "AudioManager.h"
#include "AudioChannels.h"
#include "IWorld.h"
#include "GButton.h"

#include "StateStack.h"
//States that can be started here
#include "OptionState.h"
#include "CreditState.h"
#include "HowToPlayState.h"
#include "LevelSelectState.h"
#include "ShowRoomState.h"


CMainMenuState::CMainMenuState()
{
	myShouldQuit = false;
}

CMainMenuState::~CMainMenuState()
{
}

bool CMainMenuState::Init()
{
	CState::Init();

	myGUI.Init();

	myMenuBGSprite.Init(mySceneID);
	myMenuBGSprite.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/mainMenyBack.dds");
	myMenuBGSprite.GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myMenuBGSprite.GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.5f, 0.5f));
	myMenuBGSprite.SetActive(true);
	for (int i = 0; i < EMainMenuButtons_Count; ++i)
	{
		myMenuButtons[i].Init(mySceneID);
		myMenuButtons[i].SetActive(true);
	}
	CSpriteComponent* sprite = myMenuButtons[EMainMenuButtons_StartGame].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/playButton.dds");
	myMenuButtons[EMainMenuButtons_StartGame].AddComponent<CAudioSourceComponent>(CAudioSourceComponent::SAudioComponentData(CommonUtilities::Vector3f(0.0f, 0.0f, 0.0f)));
	sprite->SetPosition(CommonUtilities::Vector2f(0.58f, 0.2f));
	sprite = myMenuButtons[EMainMenuButtons_HowToPlay].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/howToPlayButton.dds");
	myMenuButtons[EMainMenuButtons_HowToPlay].AddComponent<CAudioSourceComponent>(CAudioSourceComponent::SAudioComponentData(CommonUtilities::Vector3f(0.0f, 0.0f, 0.0f)));
	sprite->SetPosition(CommonUtilities::Vector2f(0.62f, 0.325f));
	sprite = myMenuButtons[EMainMenuButtons_ShowRoom].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/showroomButton.dds");
	myMenuButtons[EMainMenuButtons_ShowRoom].AddComponent<CAudioSourceComponent>(CAudioSourceComponent::SAudioComponentData(CommonUtilities::Vector3f(0.0f, 0.0f, 0.0f)));
	sprite->SetPosition(CommonUtilities::Vector2f(0.62f, 0.45f));
	sprite = myMenuButtons[EMainMenuButtons_Options].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/optionButton.dds");
	myMenuButtons[EMainMenuButtons_Options].AddComponent<CAudioSourceComponent>(CAudioSourceComponent::SAudioComponentData(CommonUtilities::Vector3f(0.0f, 0.0f, 0.0f)));
	sprite->SetPosition(CommonUtilities::Vector2f(0.61f, 0.575f));
	sprite = myMenuButtons[EMainMenuButtons_Credits].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/creditsButton.dds");
	myMenuButtons[EMainMenuButtons_Credits].AddComponent<CAudioSourceComponent>(CAudioSourceComponent::SAudioComponentData(CommonUtilities::Vector3f(0.0f, 0.0f, 0.0f)));
	sprite->SetPosition(CommonUtilities::Vector2f(0.61f, 0.7f));
	sprite = myMenuButtons[EMainMenuButtons_Quit].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/exitButton.dds");
	myMenuButtons[EMainMenuButtons_Quit].AddComponent<CAudioSourceComponent>(CAudioSourceComponent::SAudioComponentData(CommonUtilities::Vector3f(0.0f, 0.0f, 0.0f)));
	sprite->SetPosition(CommonUtilities::Vector2f(0.55f, 0.825f));

	for (int i = 0; i < EMainMenuButtons_Count; ++i)
	{
		myMenuButtons[i].GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	}

	AddButtons();
	return true;
}

EStateUpdate CMainMenuState::Update()
{
	myGUI.Update();
	if (myShouldQuit == true)
	{
		return EQuitGame;
	}
	return EDoNothing;
}

void CMainMenuState::OnEnter()
{
	AM.Stop("Music/GameStateMusic", false);
	AM.Stop("Music/GameStateMusic2", false);
	AM.Play("Music/MenuMusic");
	myGUI.SetFocusObject(myStartGame);
	SetActiveScene();
	myHasClicked = false;
}

void CMainMenuState::OnLeave()
{
}

void CMainMenuState::StartGame()
{
	if (myHasClicked) return;
	myHasClicked = true;
	CLevelSelectState* levelSelect = new CLevelSelectState();
	levelSelect->Init("Assets/Levels/LevelSelect.json");
	ourStateStack->LoadAsync([=] {ourStateStack->PushMainState(levelSelect); }, false);
}

void CMainMenuState::ClickSound()
{
	AM.PlayNewInstance("SFX/Buttons/ButtonClick");
}

void CMainMenuState::HowToPlay()
{
	if (myHasClicked) return;
	myHasClicked = true;
	CHowToPlayState* howToPlay = new CHowToPlayState();
	howToPlay->Init();
	ourStateStack->PushSubState(howToPlay);
}

void CMainMenuState::ShowRoom()
{
	if (myHasClicked) return;
	myHasClicked = true;
	CShowRoomState* showRoom = new CShowRoomState();
	showRoom->Init();
	ourStateStack->PushMainState(showRoom);
}

void CMainMenuState::Option()
{
	if (myHasClicked) return;
	myHasClicked = true;
	COptionState* optionState = new COptionState;
	optionState->Init(true);
	ourStateStack->PushSubState(optionState);
}

void CMainMenuState::Credits()
{
	if (myHasClicked) return;
	myHasClicked = true;
	CCreditsState* credits = new CCreditsState();
	credits->Init();
	ourStateStack->PushSubState(credits);
}

void CMainMenuState::QuitGame()
{
	myShouldQuit = true;
}

void CMainMenuState::OnHoverButton(EMainMenuButtons aButton)
{
	myMenuButtons[aButton].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.06f, 1.06f));
	myMenuButtons[aButton].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.1f, 1.1f, 1.1f, 1.0f));
}

void CMainMenuState::EndHoverButton(EMainMenuButtons aButton)
{
	myMenuButtons[aButton].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.f, 1.f));
	myMenuButtons[aButton].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
}

void CMainMenuState::AddButtons()
{
	myStartGame = new CGButton();
	myStartGame->SetOnMouseReleased(std::bind(&CMainMenuState::StartGame, this));
	myStartGame->SetOnMouseClicked(std::bind(&CMainMenuState::ClickSound, this));
	myStartGame->SetOnMouseOverStart(std::bind(&CMainMenuState::OnHoverButton, this, EMainMenuButtons_StartGame));
	myStartGame->SetOnMouseOverEnd(std::bind(&CMainMenuState::EndHoverButton, this, EMainMenuButtons_StartGame));
	myStartGame->SetPosition(myMenuButtons[EMainMenuButtons_StartGame].GetComponent<CSpriteComponent>()->GetPosition());
	myStartGame->SetSize(300.f, 120.f);
	myGUI.AddGUIObject(myStartGame);

	CGButton* howToPlay = new CGButton();
	howToPlay->SetOnMouseReleased(std::bind(&CMainMenuState::HowToPlay, this));
	howToPlay->SetOnMouseClicked(std::bind(&CMainMenuState::ClickSound, this));
	howToPlay->SetOnMouseOverStart(std::bind(&CMainMenuState::OnHoverButton, this, EMainMenuButtons_HowToPlay));
	howToPlay->SetOnMouseOverEnd(std::bind(&CMainMenuState::EndHoverButton, this, EMainMenuButtons_HowToPlay));
	howToPlay->SetPosition(myMenuButtons[EMainMenuButtons_HowToPlay].GetComponent<CSpriteComponent>()->GetPosition());
	howToPlay->SetSize(440.f, 120.f);
	myGUI.AddGUIObject(howToPlay);

	CGButton* showroom = new CGButton();
	showroom->SetOnMouseReleased(std::bind(&CMainMenuState::ShowRoom, this));
	showroom->SetOnMouseClicked(std::bind(&CMainMenuState::ClickSound, this));
	showroom->SetOnMouseOverStart(std::bind(&CMainMenuState::OnHoverButton, this, EMainMenuButtons_ShowRoom));
	showroom->SetOnMouseOverEnd(std::bind(&CMainMenuState::EndHoverButton, this, EMainMenuButtons_ShowRoom));
	showroom->SetPosition(myMenuButtons[EMainMenuButtons_ShowRoom].GetComponent<CSpriteComponent>()->GetPosition());
	showroom->SetSize(380.f, 120.f);
	myGUI.AddGUIObject(showroom);

	CGButton* option = new CGButton();
	option->SetOnMouseReleased(std::bind(&CMainMenuState::Option, this));
	option->SetOnMouseClicked(std::bind(&CMainMenuState::ClickSound, this));
	option->SetOnMouseOverStart(std::bind(&CMainMenuState::OnHoverButton, this, EMainMenuButtons_Options));
	option->SetOnMouseOverEnd(std::bind(&CMainMenuState::EndHoverButton, this, EMainMenuButtons_Options));
	option->SetPosition(myMenuButtons[EMainMenuButtons_Options].GetComponent<CSpriteComponent>()->GetPosition());
	option->SetSize(340.f, 120.f);
	myGUI.AddGUIObject(option);

	CGButton* credits = new CGButton();
	credits->SetOnMouseReleased(std::bind(&CMainMenuState::Credits, this));
	credits->SetOnMouseClicked(std::bind(&CMainMenuState::ClickSound, this));
	credits->SetOnMouseOverStart(std::bind(&CMainMenuState::OnHoverButton, this, EMainMenuButtons_Credits));
	credits->SetOnMouseOverEnd(std::bind(&CMainMenuState::EndHoverButton, this, EMainMenuButtons_Credits));
	credits->SetPosition(myMenuButtons[EMainMenuButtons_Credits].GetComponent<CSpriteComponent>()->GetPosition());
	credits->SetSize(340.f, 120.f);
	myGUI.AddGUIObject(credits);

	CGButton* quitGame = new CGButton();
	quitGame->SetOnMouseReleased(std::bind(&CMainMenuState::QuitGame, this));
	quitGame->SetOnMouseClicked(std::bind(&CMainMenuState::ClickSound, this));
	quitGame->SetOnMouseOverStart(std::bind(&CMainMenuState::OnHoverButton, this, EMainMenuButtons_Quit));
	quitGame->SetOnMouseOverEnd(std::bind(&CMainMenuState::EndHoverButton, this, EMainMenuButtons_Quit));
	quitGame->SetPosition(myMenuButtons[EMainMenuButtons_Quit].GetComponent<CSpriteComponent>()->GetPosition());
	quitGame->SetSize(200.f, 120.f);
	myGUI.AddGUIObject(quitGame);

	//Start Game
	myStartGame->SetConnectedObject(quitGame, TOP);
	myStartGame->SetConnectedObject(howToPlay, BOTTOM);
	// HowtoPlay
	howToPlay->SetConnectedObject(myStartGame, TOP);
	howToPlay->SetConnectedObject(showroom, BOTTOM);
	//Showroom
	showroom->SetConnectedObject(howToPlay, TOP);
	showroom->SetConnectedObject(option, BOTTOM);
	//Option
	option->SetConnectedObject(showroom, TOP);
	option->SetConnectedObject(credits, BOTTOM);
	//Credits
	credits->SetConnectedObject(option, TOP);
	credits->SetConnectedObject(quitGame, BOTTOM);
	// Quit Game
	quitGame->SetConnectedObject(credits, TOP);
	quitGame->SetConnectedObject(myStartGame, BOTTOM);

	myGUI.SetDefaultFocusObject(myStartGame);
}

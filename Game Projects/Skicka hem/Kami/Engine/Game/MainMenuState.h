#pragma once
#include "GameObject.h"
#include "GUI.h"

#include "State.h"
#include <array>

class CGButton;

class CMainMenuState : public CState
{
public:
	enum EMainMenuButtons
	{
		EMainMenuButtons_StartGame,
		EMainMenuButtons_HowToPlay,
		EMainMenuButtons_ShowRoom,
		EMainMenuButtons_Options,
		EMainMenuButtons_Credits,
		EMainMenuButtons_Quit,
		EMainMenuButtons_Count
	};
	CMainMenuState();
	~CMainMenuState();

	bool Init();
	EStateUpdate Update() override;
	void OnEnter() override;
	void OnLeave() override;

private:
	CGUI myGUI;
	CGameObject myMenuBGSprite;
	std::array<CGameObject, EMainMenuButtons_Count> myMenuButtons;
	bool myShouldQuit;
	CGButton* myStartGame;
	bool myHasClicked;
private:
	// Button Functionds
	void AddButtons();
	void StartGame();
	void ClickSound();
	void HowToPlay();
	void ShowRoom();
	void Option();
	void Credits();
	void QuitGame();
	void OnHoverButton(EMainMenuButtons aButton);
	void EndHoverButton(EMainMenuButtons aButton);
};


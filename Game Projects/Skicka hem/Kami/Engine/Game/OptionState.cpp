#include "stdafx.h"
#include "OptionState.h"
#include "GButton.h"
#include "JsonDocument.h"

#include "AudioManager.h"
#include "AudioChannels.h"

#include "IWorld.h"
#include "XBOXController.h"

#include "FileDialog.h"

COptionState::COptionState()
{
	myShouldPop = false;
	myMutedSound = false;
	myBackButton = nullptr;
}


COptionState::~COptionState()
{
}

bool COptionState::Init(bool aFromMenu)
{
	myFromMenu = aFromMenu;
	CState::Init();

	myGUI.Init();

	JsonDocument jsonDoc;
	jsonDoc.LoadFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), true);
	if (jsonDoc.Find("player"))
	{
		if (jsonDoc["player"].Find("controls"))
		{
			if (jsonDoc["player"]["controls"].Find("invertedYAxis"))
			{
				myInvertedYAxis = jsonDoc["player"]["controls"]["invertedYAxis"].GetBool();
			}
			else
			{
				GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->controls->InvertedYAxis! Default value applied.");
				myInvertedYAxis = true;
			}
			if (jsonDoc["player"]["controls"].Find("mutedSound"))
			{
				myMutedSound = jsonDoc["player"]["controls"]["mutedSound"].GetBool();
			}
			else
			{
				GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->controls->mutedSound! Default value applied.");
				myMutedSound = false;
			}
		}
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->controls! Default value applied.");
		myInvertedYAxis = true;
		myMutedSound = false;
	}
	AM.SetMute(myMutedSound);


	myBackground.Init(mySceneID);
	myBackground.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/optionBack.dds");
	myBackground.GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myBackground.GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.5f, 0.5f));
	myBackground.SetActive(true);
	
	for (int i = 0; i < EOptionButtons_Count; ++i)
	{
		myOptionMenuButtons[i].Init(mySceneID);
		myOptionMenuButtons[i].SetActive(true);
	}
	for (int i = 0; i < EOptionText_Count; ++i)
	{
		myTextObjects[i].Init(mySceneID);
		myTextObjects[i].SetActive(true);
	}
	CSpriteComponent* spriteComponent = myOptionMenuButtons[EOptionButtons_Back].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/backButton.dds");
	spriteComponent->SetPosition(CommonUtilities::Vector2f(0.92f, 0.92f));
	spriteComponent->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	spriteComponent = myOptionMenuButtons[EOptionButtons_InvertedYAxisNo].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/boxNo.dds");
	spriteComponent->SetPosition(CommonUtilities::Vector2f(0.72f, 0.22f));
	spriteComponent->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	spriteComponent = myOptionMenuButtons[EOptionButtons_InvertedYAxisYes].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/boxYes.dds");
	spriteComponent->SetPosition(CommonUtilities::Vector2f(0.72f, 0.22f));
	spriteComponent->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	spriteComponent = myOptionMenuButtons[EOptionButtons_ResetGameProgression].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/resetBotton.dds");
	if (!myFromMenu)
	{
		spriteComponent->SetTint(CommonUtilities::Vector4f(0.1f, 0.1f , 0.1f, 1.0f));
	}
	spriteComponent->SetPosition(CommonUtilities::Vector2f(0.9f, 0.11f));
	spriteComponent->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	spriteComponent = myOptionMenuButtons[EOptionButtons_MutedSoundNo].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/boxNo.dds");
	spriteComponent->SetPosition(CommonUtilities::Vector2f(0.72f, 0.3f));
	spriteComponent->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	spriteComponent = myOptionMenuButtons[EOptionButtons_MutedSoundYes].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/boxYes.dds");
	spriteComponent->SetPosition(CommonUtilities::Vector2f(0.72f, 0.3f));
	spriteComponent->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));

	CTextComponent* textComponent = myTextObjects[EOptionText_InvertedYAxisText].AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");
	textComponent->SetText("Inverted Y-Axis");
	textComponent->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
	textComponent->SetPosition(CommonUtilities::Vector2f(0.56f, 0.225f));
	textComponent->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	textComponent->SetScale(CommonUtilities::Vector2f(1.0f, 1.0f));

	textComponent = myTextObjects[EOptionText_ResetGameProgression].AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");
	textComponent->SetText("Reset Game Progression");
	textComponent->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
	textComponent->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	textComponent->SetPosition(CommonUtilities::Vector2f(0.9f, 0.05f));
	textComponent->SetScale(CommonUtilities::Vector2f(1.0f, 1.0f));

	textComponent = myTextObjects[EOptionText_MutedSound].AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");
	textComponent->SetText("Mute Sound");
	textComponent->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
	textComponent->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	textComponent->SetPosition(CommonUtilities::Vector2f(0.56f, 0.3f));
	textComponent->SetScale(CommonUtilities::Vector2f(1.0f, 1.0f));

	if (myInvertedYAxis)
	{
		myOptionMenuButtons[EOptionButtons_InvertedYAxisNo].SetActive(false);
	}
	else
	{
		myOptionMenuButtons[EOptionButtons_InvertedYAxisYes].SetActive(false);
	}
	if (myMutedSound)
	{
		myOptionMenuButtons[EOptionButtons_MutedSoundNo].SetActive(false);
	}
	else
	{
		myOptionMenuButtons[EOptionButtons_MutedSoundYes].SetActive(false);
	}
	AddButtons();

	myPopUpSprite.Init(mySceneID, "Assets/Sprites/Menu/resetPopUp.dds", 2.f, 0.5f, false);
	return true;
}

EStateUpdate COptionState::Update()
{
	CommonUtilities::XBOXController& xbox = IWorld::XBOX();
	myGUI.Update();
	myPopUpSprite.Update();
	if (myPopUpSprite.GetIsPlaying()){myTextObjects[EOptionText_MutedSound].SetActive(false);}
	else 
	{
		if (myShouldPop || xbox.IsButtonPressed(CommonUtilities::XButton_B) || xbox.IsButtonPressed(CommonUtilities::XButton_Back) || IWorld::Input().IsKeyReleased(Input::Key_Escape))
		{
			return EPop_Sub;
		}
		myTextObjects[EOptionText_MutedSound].SetActive(true);
	}


	
	return EDoNothing;
}

void COptionState::OnEnter()
{
	myGUI.SetFocusObject(myBackButton);
	SetActiveScene();
}

void COptionState::OnLeave()
{
	JsonDocument jsonDoc;
	jsonDoc.LoadFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), true);
	
	if (!jsonDoc.Find("player"))
	{
		JsonObject playerObj;
		jsonDoc.AddObject("player", playerObj);
	}
	if (!jsonDoc["player"].Find("controls"))
	{
		JsonObject controlsObj;
		controlsObj.AddMember("invertedYAxis", myInvertedYAxis);
		controlsObj.AddMember("mutedSound", myMutedSound);
		jsonDoc["player"].AddObject("controls", controlsObj);
	}
	else if (jsonDoc["player"]["controls"].Find("invertedYAxis"))
	{
		jsonDoc["player"]["controls"]["invertedYAxis"].SetBool(myInvertedYAxis);
	}
	else
	{
		jsonDoc["player"]["controls"].AddMember("invertedYAxis", myInvertedYAxis);
	}
	if (jsonDoc["player"]["controls"].Find("mutedSound"))
	{
		jsonDoc["player"]["controls"]["mutedSound"].SetBool(myMutedSound);
	}
	else
	{
		jsonDoc["player"]["controls"].AddMember("mutedSound", myMutedSound);
	}
	jsonDoc.SaveFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), false, true);
}

void COptionState::AddButtons()
{
	CGButton* invertedYAxis = new CGButton();
	invertedYAxis->SetOnMouseReleased(std::bind(&COptionState::InvertedYAxis, this));
	invertedYAxis->SetOnMouseOverStart(std::bind(&COptionState::OnHoverButton, this, EOptionButtons_InvertedYAxisNo));
	invertedYAxis->SetOnMouseOverEnd(std::bind(&COptionState::EndHoverButton, this, EOptionButtons_InvertedYAxisNo));
	invertedYAxis->SetPosition(myOptionMenuButtons[EOptionButtons_InvertedYAxisNo].GetComponent<CSpriteComponent>()->GetPosition());
	invertedYAxis->SetSize(100.f, 100.f);
	myGUI.AddGUIObject(invertedYAxis);

	CGButton* resetGameProgression = new CGButton();
	if (myFromMenu)
	{
		resetGameProgression->SetOnMouseReleased(std::bind(&COptionState::ResetGameProgression, this));
		resetGameProgression->SetOnMouseOverStart(std::bind(&COptionState::OnHoverButton, this, EOptionButtons_ResetGameProgression));
		resetGameProgression->SetOnMouseOverEnd(std::bind(&COptionState::EndHoverButton, this, EOptionButtons_ResetGameProgression));
		resetGameProgression->SetPosition(myOptionMenuButtons[EOptionButtons_ResetGameProgression].GetComponent<CSpriteComponent>()->GetPosition());
		resetGameProgression->SetSize(260.f, 120.f);
		myGUI.AddGUIObject(resetGameProgression);
	}

	CGButton* muteSound = new CGButton();
	muteSound->SetOnMouseReleased(std::bind(&COptionState::MutedSound, this));
	muteSound->SetOnMouseOverStart(std::bind(&COptionState::OnHoverButton, this, EOptionButtons_MutedSoundNo));
	muteSound->SetOnMouseOverEnd(std::bind(&COptionState::EndHoverButton, this, EOptionButtons_MutedSoundNo));
	muteSound->SetPosition(myOptionMenuButtons[EOptionButtons_MutedSoundNo].GetComponent<CSpriteComponent>()->GetPosition());
	muteSound->SetSize(260.f, 120.f);
	myGUI.AddGUIObject(muteSound);

	myBackButton = new CGButton();
	myBackButton->SetOnMouseReleased(std::bind(&COptionState::Back, this));
	myBackButton->SetOnMouseOverStart(std::bind(&COptionState::OnHoverButton, this, EOptionButtons_Back));
	myBackButton->SetOnMouseOverEnd(std::bind(&COptionState::EndHoverButton, this, EOptionButtons_Back));
	myBackButton->SetPosition(myOptionMenuButtons[EOptionButtons_Back].GetComponent<CSpriteComponent>()->GetPosition());
	myBackButton->SetSize(200.f, 140.f);
	myGUI.AddGUIObject(myBackButton);

	if (myFromMenu)
	{
		resetGameProgression->SetConnectedObject(myBackButton, TOP);
		resetGameProgression->SetConnectedObject(invertedYAxis, BOTTOM);
		invertedYAxis->SetConnectedObject(resetGameProgression, TOP);
		invertedYAxis->SetConnectedObject(muteSound, BOTTOM);
		muteSound->SetConnectedObject(invertedYAxis, TOP);
		muteSound->SetConnectedObject(myBackButton, BOTTOM);
		myBackButton->SetConnectedObject(muteSound, TOP);
		myBackButton->SetConnectedObject(resetGameProgression, BOTTOM);
	}
	else
	{
		invertedYAxis->SetConnectedObject(myBackButton, TOP);
		invertedYAxis->SetConnectedObject(muteSound, BOTTOM);
		muteSound->SetConnectedObject(invertedYAxis, TOP);
		muteSound->SetConnectedObject(myBackButton, BOTTOM);
		myBackButton->SetConnectedObject(muteSound, TOP);
		myBackButton->SetConnectedObject(invertedYAxis, BOTTOM);
	}
	//Start Game

	myGUI.SetDefaultFocusObject(myBackButton);
}

void COptionState::Back()
{
	AM.PlayNewInstance("SFX/Buttons/ButtonBack", nullptr, AudioChannel::SoundEffects);
	myShouldPop = true;
}

void COptionState::ResetGameProgression()
{
	if (myFromMenu)
	{
		myPopUpSprite.Play();
		AM.PlayNewInstance("SFX/Buttons/ResetGame");
		JsonDocument jsonDoc;
		jsonDoc.LoadFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), true);
		jsonDoc.ClearJsonDoc();
		jsonDoc.SaveFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), false, true);
	}
}

void COptionState::InvertedYAxis()
{
	AM.PlayNewInstance("SFX/Buttons/CheckBox", nullptr, AudioChannel::SoundEffects);
	myOptionMenuButtons[EOptionButtons_InvertedYAxisNo].SetActive(!myOptionMenuButtons[EOptionButtons_InvertedYAxisNo].IsActive());
	myOptionMenuButtons[EOptionButtons_InvertedYAxisYes].SetActive(!myOptionMenuButtons[EOptionButtons_InvertedYAxisYes].IsActive());
	myInvertedYAxis = !myInvertedYAxis;
}

void COptionState::MutedSound()
{
	AM.PlayNewInstance("SFX/Buttons/CheckBox", nullptr, AudioChannel::SoundEffects);
	myOptionMenuButtons[EOptionButtons_MutedSoundNo].SetActive(!myOptionMenuButtons[EOptionButtons_MutedSoundNo].IsActive());
	myOptionMenuButtons[EOptionButtons_MutedSoundYes].SetActive(!myOptionMenuButtons[EOptionButtons_MutedSoundYes].IsActive());
	myMutedSound = !myMutedSound;
	AM.SetMute(myMutedSound);
}

void COptionState::OnHoverButton(EOptionButtons aButton)
{
	if (!myFromMenu && aButton == EOptionButtons_ResetGameProgression)
	{

	}
	else if (aButton == EOptionButtons_InvertedYAxisNo || aButton == EOptionButtons_InvertedYAxisYes)
	{
		myOptionMenuButtons[EOptionButtons_InvertedYAxisNo].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.06f, 1.06f));
		myOptionMenuButtons[EOptionButtons_InvertedYAxisNo].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.1f, 1.1f, 1.1f, 1.0f));
		myOptionMenuButtons[EOptionButtons_InvertedYAxisYes].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.06f, 1.06f));
		myOptionMenuButtons[EOptionButtons_InvertedYAxisYes].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.1f, 1.1f, 1.1f, 1.0f));
	}
	else if (aButton == EOptionButtons_MutedSoundNo || aButton == EOptionButtons_MutedSoundYes)
	{
		myOptionMenuButtons[EOptionButtons_MutedSoundNo].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.06f, 1.06f));
		myOptionMenuButtons[EOptionButtons_MutedSoundNo].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.1f, 1.1f, 1.1f, 1.0f));
		myOptionMenuButtons[EOptionButtons_MutedSoundYes].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.06f, 1.06f));
		myOptionMenuButtons[EOptionButtons_MutedSoundYes].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.1f, 1.1f, 1.1f, 1.0f));
	}
	else
	{
		myOptionMenuButtons[aButton].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.06f, 1.06f));
		myOptionMenuButtons[aButton].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.1f, 1.1f, 1.1f, 1.0f));
	}
}

void COptionState::EndHoverButton(EOptionButtons aButton)
{
	if (aButton == EOptionButtons_InvertedYAxisNo || aButton == EOptionButtons_InvertedYAxisYes)
	{
		myOptionMenuButtons[EOptionButtons_InvertedYAxisNo].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.0f, 1.0f));
		myOptionMenuButtons[EOptionButtons_InvertedYAxisNo].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
		myOptionMenuButtons[EOptionButtons_InvertedYAxisYes].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.0f, 1.0f));
		myOptionMenuButtons[EOptionButtons_InvertedYAxisYes].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
	}
	else if (aButton == EOptionButtons_MutedSoundNo || aButton == EOptionButtons_MutedSoundYes)
	{
		myOptionMenuButtons[EOptionButtons_MutedSoundNo].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.0f, 1.0f));
		myOptionMenuButtons[EOptionButtons_MutedSoundNo].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
		myOptionMenuButtons[EOptionButtons_MutedSoundYes].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.0f, 1.0f));
		myOptionMenuButtons[EOptionButtons_MutedSoundYes].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
	}
	else if (aButton == EOptionButtons_ResetGameProgression && !myFromMenu)	{}
	else
	{
		myOptionMenuButtons[aButton].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.0f, 1.0f));
		myOptionMenuButtons[aButton].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
	}
}

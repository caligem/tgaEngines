#pragma once
#include "State.h"
#include "GUI.h"
#include "PopUp.h"

class CGButton;

class COptionState : public CState
{
public:
	enum EOptionButtons
	{
		EOptionButtons_Back,
		EOptionButtons_InvertedYAxisNo,
		EOptionButtons_InvertedYAxisYes,
		EOptionButtons_MutedSoundNo,
		EOptionButtons_MutedSoundYes,
		EOptionButtons_ResetGameProgression,
		EOptionButtons_Count
	};
	enum EOptionText
	{
		EOptionText_InvertedYAxisText,
		EOptionText_ResetGameProgression,
		EOptionText_MutedSound,
		EOptionText_Count
	};
	COptionState();
	~COptionState();

	bool Init(bool aFromMenu);
	EStateUpdate Update() override;
	void OnEnter() override;
	void OnLeave() override;
private:
	CGUI myGUI;
	std::array<CGameObject, EOptionButtons_Count> myOptionMenuButtons;
	std::array<CGameObject, EOptionText_Count> myTextObjects;
	CGameObject myBackground;
	CGButton* myBackButton;
	bool myInvertedYAxis;
	bool myMutedSound;
private: 
	// Button functions
	void AddButtons();
	void Back();
	void ResetGameProgression();
	void InvertedYAxis();
	void MutedSound();
	void OnHoverButton(EOptionButtons aButton);
	void EndHoverButton(EOptionButtons aButton);

	CPopUpSprite myPopUpSprite;
	bool myShouldPop;
	bool myFromMenu;
};


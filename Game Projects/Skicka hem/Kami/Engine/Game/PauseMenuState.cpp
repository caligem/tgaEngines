#include "stdafx.h"
#include "PauseMenuState.h"

#include "StateStack.h"
#include "IWorld.h"
#include "XBOXController.h"
#include "GButton.h"
//States that can be started here
#include "OptionState.h"
#include "HowToPlayState.h"

CPauseMenuState::CPauseMenuState()
{
	myShouldPopSub = false;
	myShouldPopMain = false;
	myShouldQuit = false;
}


CPauseMenuState::~CPauseMenuState()
{
}

bool CPauseMenuState::Init()
{
	CState::Init();

	myGUI.Init();

	//Background
	myPauseMenuBG.Init(mySceneID);
	myPauseMenuBG.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/pausBack.dds");
	myPauseMenuBG.GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myPauseMenuBG.GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.5f, 0.5f));
	myPauseMenuBG.SetActive(true);
	//Button Sprites
	for (int i = 0; i < EPauseMenuButtons_Count; ++i)
	{
		myPauseMenuButtons[i].Init(mySceneID);
		myPauseMenuButtons[i].SetActive(true);
	}
	myPauseMenuButtons[EPauseMenuButtons_Resume].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/continueButton.dds");
	myPauseMenuButtons[EPauseMenuButtons_Resume].GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.6f, 0.2f));
	myPauseMenuButtons[EPauseMenuButtons_LevelSelect].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/levelSelectButton.dds");
	myPauseMenuButtons[EPauseMenuButtons_LevelSelect].GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.6f, 0.35f));
	myPauseMenuButtons[EPauseMenuButtons_Options].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/optionButton.dds");
	myPauseMenuButtons[EPauseMenuButtons_Options].GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.6f, 0.5f));
	myPauseMenuButtons[EPauseMenuButtons_HowToPlay].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/howToPlayButton.dds");
	myPauseMenuButtons[EPauseMenuButtons_HowToPlay].GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.6f, 0.65f));
	myPauseMenuButtons[EPauseMenuButtons_QuitGame].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/exitToDesktopButton.dds");
	myPauseMenuButtons[EPauseMenuButtons_QuitGame].GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.6f, 0.80f));
	for (int i = 0; i < EPauseMenuButtons_Count; ++i)
	{
		myPauseMenuButtons[i].GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	}

	AddButtons();
	return true;
}

EStateUpdate CPauseMenuState::Update()
{
	myGUI.Update();

	if (IWorld::Input().IsKeyPressed(Input::Key_Escape) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_Start))
	{
		return EPop_Sub;
	}
	if (myShouldPopMain)
	{
		return EPop_Main;
	}
	else if (myShouldPopSub)
	{
		return EPop_Sub;
	}
	else if (myShouldQuit)
	{
		return EQuitGame;
	}
	return EDoNothing;
}

void CPauseMenuState::OnEnter()
{
	SetActiveScene();
}

void CPauseMenuState::OnLeave()
{

}

void CPauseMenuState::AddButtons()
{
	CGButton* resume = new CGButton();
	resume->SetOnMouseClicked(std::bind(&CPauseMenuState::Resume, this));
	resume->SetOnMouseOverStart(std::bind(&CPauseMenuState::OnHoverButton, this, EPauseMenuButtons_Resume));
	resume->SetOnMouseOverEnd(std::bind(&CPauseMenuState::EndHoverButton, this, EPauseMenuButtons_Resume));
	resume->SetPosition(myPauseMenuButtons[EPauseMenuButtons_Resume].GetComponent<CSpriteComponent>()->GetPosition());
	resume->SetSize(300.f, 120.f);
	myGUI.AddGUIObject(resume);

	CGButton* howToPlay = new CGButton();
	howToPlay->SetOnMouseClicked(std::bind(&CPauseMenuState::HowToPlay, this));
	howToPlay->SetOnMouseOverStart(std::bind(&CPauseMenuState::OnHoverButton, this, EPauseMenuButtons_HowToPlay));
	howToPlay->SetOnMouseOverEnd(std::bind(&CPauseMenuState::EndHoverButton, this, EPauseMenuButtons_HowToPlay));
	howToPlay->SetPosition(myPauseMenuButtons[EPauseMenuButtons_HowToPlay].GetComponent<CSpriteComponent>()->GetPosition());
	howToPlay->SetSize(440.f, 120.f);
	myGUI.AddGUIObject(howToPlay);

	CGButton* options = new CGButton();
	options->SetOnMouseClicked(std::bind(&CPauseMenuState::Options, this));
	options->SetOnMouseOverStart(std::bind(&CPauseMenuState::OnHoverButton, this, EPauseMenuButtons_Options));
	options->SetOnMouseOverEnd(std::bind(&CPauseMenuState::EndHoverButton, this, EPauseMenuButtons_Options));
	options->SetPosition(myPauseMenuButtons[EPauseMenuButtons_Options].GetComponent<CSpriteComponent>()->GetPosition());
	options->SetSize(340.f, 120.f);
	myGUI.AddGUIObject(options);

	CGButton* levelSelect = new CGButton();
	levelSelect->SetOnMouseClicked(std::bind(&CPauseMenuState::MainMenu, this));
	levelSelect->SetOnMouseOverStart(std::bind(&CPauseMenuState::OnHoverButton, this, EPauseMenuButtons_LevelSelect));
	levelSelect->SetOnMouseOverEnd(std::bind(&CPauseMenuState::EndHoverButton, this, EPauseMenuButtons_LevelSelect));
	levelSelect->SetPosition(myPauseMenuButtons[EPauseMenuButtons_LevelSelect].GetComponent<CSpriteComponent>()->GetPosition());
	levelSelect->SetSize(200.f, 120.f);
	myGUI.AddGUIObject(levelSelect);

	CGButton* quitGame = new CGButton();
	quitGame->SetOnMouseClicked(std::bind(&CPauseMenuState::QuitGame, this));
	quitGame->SetOnMouseOverStart(std::bind(&CPauseMenuState::OnHoverButton, this, EPauseMenuButtons_QuitGame));
	quitGame->SetOnMouseOverEnd(std::bind(&CPauseMenuState::EndHoverButton, this, EPauseMenuButtons_QuitGame));
	quitGame->SetPosition(myPauseMenuButtons[EPauseMenuButtons_QuitGame].GetComponent<CSpriteComponent>()->GetPosition());
	quitGame->SetSize(200.f, 120.f);
	myGUI.AddGUIObject(quitGame);

	// Resume
	resume->SetConnectedObject(quitGame, TOP);
	resume->SetConnectedObject(levelSelect, BOTTOM);
	// Level Select
	levelSelect->SetConnectedObject(resume, TOP);
	levelSelect->SetConnectedObject(options, BOTTOM);
	// Options
	options->SetConnectedObject(levelSelect, TOP);
	options->SetConnectedObject(howToPlay, BOTTOM);
	// HowtoPlay
	howToPlay->SetConnectedObject(options, TOP);
	howToPlay->SetConnectedObject(quitGame, BOTTOM);
	// Quit Game
	quitGame->SetConnectedObject(howToPlay, TOP);
	quitGame->SetConnectedObject(resume, BOTTOM);

	myGUI.SetDefaultFocusObject(resume);
}

void CPauseMenuState::Resume()
{
	myShouldPopSub = true;
}

void CPauseMenuState::HowToPlay()
{
	CHowToPlayState* howToPlay = new CHowToPlayState();
	howToPlay->Init();
	ourStateStack->PushSubState(howToPlay);
}

void CPauseMenuState::Options()
{
	COptionState* optionState = new COptionState();
	optionState->Init(false);
	ourStateStack->PushSubState(optionState);
}

void CPauseMenuState::MainMenu()
{
	myShouldPopMain = true;
}

void CPauseMenuState::QuitGame()
{
	myShouldQuit = true;
}

void CPauseMenuState::OnHoverButton(EPauseMenuButtons aButton)
{
	myPauseMenuButtons[aButton].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.1f, 1.1f));
	myPauseMenuButtons[aButton].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.1f, 1.1f, 1.1f, 1.0f));

}

void CPauseMenuState::EndHoverButton(EPauseMenuButtons aButton)
{
	myPauseMenuButtons[aButton].GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.f, 1.f));
	myPauseMenuButtons[aButton].GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
}

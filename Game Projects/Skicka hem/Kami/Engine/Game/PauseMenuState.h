#pragma once
#include "State.h"
#include "GUI.h"
class CPauseMenuState : public CState
{
public:
	enum EPauseMenuButtons
	{
		EPauseMenuButtons_Resume,
		EPauseMenuButtons_HowToPlay,
		EPauseMenuButtons_Options,
		EPauseMenuButtons_LevelSelect,
		EPauseMenuButtons_QuitGame,
		EPauseMenuButtons_Count
	};
	CPauseMenuState();
	~CPauseMenuState();

	bool Init() override;
	EStateUpdate Update() override;
	void OnEnter() override;
	void OnLeave() override;
private:
	void AddButtons();
	void Resume();
	void HowToPlay();
	void Options();
	void MainMenu();
	void QuitGame();
	void OnHoverButton(EPauseMenuButtons aButton);
	void EndHoverButton(EPauseMenuButtons aButton);

	CGUI myGUI;
	CGameObject myPauseMenuBG;
	std::array<CGameObject, EPauseMenuButtons_Count> myPauseMenuButtons;

	bool myShouldPopSub;
	bool myShouldPopMain;
	bool myShouldQuit;
};


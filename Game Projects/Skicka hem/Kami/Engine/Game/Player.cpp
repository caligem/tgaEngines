#include "stdafx.h"
#include "Player.h"

#include "IWorld.h"
#include "Matrix.h"
#include "Scene.h"

#include <InputManager.h>
#include "XBOXController.h"
#include "AudioManager.h"

#include "Tunnel.h"

#include "Mathf.h"
#include "DL_Debug.h"

#include "FileDialog.h"

CPlayer::CPlayer()
{
	myPoints = 0;
	mySpeedBoost = 0.f;
	mySlowDown = 1.f;
	myColliding = false;
	myCanBoost = false;
}

CPlayer::~CPlayer()
{
}

void CPlayer::Init(ID_T(CScene) aSceneID)
{
	CGameObject::Init(aSceneID);
	AddComponent<CModelComponent>("Assets/Models/player/player_body.fbx");

	AddComponent<CAudioListenerComponent>(0);

	myLeftWing.Init(aSceneID);
	myRightWing.Init(aSceneID);
	myLeftWingFX.Init(aSceneID);
	myRightWingFX.Init(aSceneID);
	InitWings();

	InitBoostWindEffect(aSceneID);
}

void CPlayer::LoadPlayerPropsFromJson(JsonValue aPlayerJson)
{
	//movement
	if (aPlayerJson["movement"].Find("acceleration"))
	{
		myProps.myAcceleration = aPlayerJson["movement"]["acceleration"].GetFloat();
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->movement->acceleration! Default value applied.");
		myProps.myAcceleration = 5.f;
	}
	if (aPlayerJson["movement"].Find("deceleration"))
	{
		myProps.myDeceleration = aPlayerJson["movement"]["deceleration"].GetFloat();
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->movement->deceleration! Default value applied.");
		myProps.myDeceleration = 2.5f;
	}
	if (aPlayerJson["movement"].Find("speed"))
	{
		myProps.mySpeed = aPlayerJson["movement"]["speed"].GetFloat();
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->movement->speed! Default value applied.");
		myProps.mySpeed = 5.0f;
	}
	if (aPlayerJson["movement"].Find("rotationLerpSpeed"))
	{
		myProps.myRotationLerpSpeed = aPlayerJson["movement"]["rotationLerpSpeed"].GetFloat();
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->movement->rotationLerpSpeed! Default value applied.");
		myProps.myRotationLerpSpeed = 2.0f;
	}
	if (aPlayerJson["movement"].Find("slowForwardSpeed"))
	{
		myProps.mySlowForwardSpeed = aPlayerJson["movement"]["slowForwardSpeed"].GetFloat();
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->movement->slowForwardSpeed! Default value applied.");
		myProps.mySlowForwardSpeed = 5.0f;
	}
	if (aPlayerJson["movement"].Find("fastForwardSpeed"))
	{
		myProps.myFastForwardSpeed = aPlayerJson["movement"]["fastForwardSpeed"].GetFloat();
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->movement->fastForwardSpeed! Default value applied.");
		myProps.myFastForwardSpeed = 10.0f;
	}

	//Boost
	if (aPlayerJson["boost"].Find("flapCount"))
	{
		myProps.myFlapCount = aPlayerJson["boost"]["flapCount"].GetFloat();
		myProps.myMaxFlapCount = myProps.myFlapCount;
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->boost->flapCount! Default value applied.");
		myProps.myFlapCount = 3.0f;
		myProps.myMaxFlapCount = myProps.myFlapCount;
	}
	if (aPlayerJson["boost"].Find("flapStrength"))
	{
		myProps.myFlapStrength = aPlayerJson["boost"]["flapStrength"].GetFloat();
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->boost->flapStrength! Default value applied.");
		myProps.myFlapStrength = 0.5f;
	}
	if (aPlayerJson["boost"].Find("flapIncrease"))
	{
		myProps.myFlapIncrease = aPlayerJson["boost"]["flapIncrease"].GetFloat();
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->boost->flapIncrease! Default value applied.");
		myProps.myFlapIncrease = 1.f / 3.f;
	}
	if (aPlayerJson["boost"].Find("flapDecay"))
	{
		myProps.myFlapDecay = aPlayerJson["boost"]["flapDecay"].GetFloat();
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->boost->flapDecay! Default value applied.");
		myProps.myFlapDecay = 1.f;
	}
}


void CPlayer::SetDefaultValues()
{
	myProps.myAcceleration = 5.0f;
	myProps.myDeceleration = 2.5f;
	myProps.mySpeed = 5.0f;
	myProps.myRotationLerpSpeed = 2.0f;
	myProps.mySlowForwardSpeed = 5.f;
	myProps.myFastForwardSpeed = 10.f;

	myProps.myFlapCount = 3.0f;
	myProps.myMaxFlapCount = myProps.myFlapCount;
	myProps.myFlapStrength = 0.5f;
	myProps.myFlapIncrease = 1.f / 3.f;
	myProps.myFlapDecay = 1.f;
	myProps.myInvertedYAxis = true;
}

void CPlayer::SetCanBoost(const bool& aCanBoost)
{
	myCanBoost = aCanBoost;
}

void CPlayer::Update(const bool& aIsLevelComplete)
{
	Input::CInputManager& input = IWorld::Input();
	CommonUtilities::XBOXController& xBox = IWorld::XBOX();
	float dt = IWorld::Time().GetDeltaTime();

	//Flap - Boost
	if (myCanBoost && (input.IsKeyPressed(Input::Key_Space) || xBox.IsButtonPressed(CommonUtilities::XButton_A)))
	{
		Flap();
	}

	//Todo: remove debug stuff
#ifndef _RETAIL
	if (input.IsKeyDown(Input::Key_F))
	{
		BoostHoop();
	}

	if (input.IsKeyDown(Input::Key_O))
	{
		mySpeedBoost = 7.0f;
	}
#endif

	float finalSpeed = GetSpeed();

	mySpeedBoost = CommonUtilities::Lerp(mySpeedBoost, 0.f, dt*myProps.myFlapDecay);

	if (aIsLevelComplete)
	{
		mySlowDown = CommonUtilities::Lerp(mySlowDown, 0.3f, dt*myProps.myFlapDecay * 1.2f);
	}
	else
	{
		mySlowDown = CommonUtilities::Lerp(mySlowDown, 1.f, dt*myProps.myFlapDecay);
	}


	if (mySlowDown < 0.7f && !aIsLevelComplete)
	{
		if (myColliding == false)
		{
			myColliding = true;
			AM.PlayNewInstance("SFX/SoundEffects/Collision", nullptr, AudioChannel::SoundEffects);
		}
	}
	else
	{
		myColliding = false;
		AM.Stop("SFX/SoundEffects/Collision", false);
	}
	Move(myCurrentTunnelDirection.GetForward() * dt * finalSpeed);

	CommonUtilities::Vector2f acceleration = { 0.f, 0.f };
	if (xBox.IsConnected())
	{
		acceleration = xBox.GetStick(CommonUtilities::XStick_Left);
	}
	if (input.IsKeyDown(Input::Key_Up) || input.IsKeyDown(Input::Key_W))
	{
		acceleration.y += 1.f;
	}
	if (input.IsKeyDown(Input::Key_Down) || input.IsKeyDown(Input::Key_S))
	{
		acceleration.y -= 1.f;
	}
	if (input.IsKeyDown(Input::Key_Right) || input.IsKeyDown(Input::Key_D))
	{
		acceleration.x += 1.f;
	}
	if (input.IsKeyDown(Input::Key_Left) || input.IsKeyDown(Input::Key_A))
	{
		acceleration.x -= 1.f;
	}
	if (myProps.myInvertedYAxis)
	{
		acceleration.y *= -1.f;
	}
	acceleration.Normalize();

	if (acceleration.Length2() > 0.f)
	{
		myVelocity = CommonUtilities::Lerp(myVelocity, acceleration, dt*myProps.myAcceleration);
	}
	else
	{
		myVelocity = CommonUtilities::Lerp(myVelocity, { 0.f, 0.f }, dt*myProps.myDeceleration);
	}

	Move(
		((myTunnelDirection.GetUp()*myVelocity.y) + (myTunnelDirection.GetRight()*myVelocity.x)) *
		dt * myProps.mySpeed
	);

	CommonUtilities::Vector3f playerPos = GetTransform().GetPosition();
	myCurrentTunnelDirection.SetLookAtRotation(
	{ 0.f, 0.f, 0.f },
		CommonUtilities::Slerp(myCurrentTunnelDirection.GetForward(), myTunnelDirection.GetForward(), dt * myProps.myRotationLerpSpeed)
	);
	GetTransform().SetLookDirection(myCurrentTunnelDirection.GetForward());
	Sway();

	SetPosition(playerPos);
}

void CPlayer::UpdateWings()
{
	SetWingPosition();

	if (mySpeedBoost >= 0.1f)
	{
		PlayBoostWindEffect();
	}
}

void CPlayer::AddPoints()
{
	++myPoints;
	myProps.myFlapCount = CommonUtilities::Clamp(myProps.myFlapCount + myProps.myFlapIncrease, 0.f, myProps.myMaxFlapCount);
}

int CPlayer::GetPoints() const
{
	return myPoints;
}

void CPlayer::Flap()
{
	if (myProps.myFlapCount < 1.f || mySlowDown < 0.9f)
	{
		return;
	}
	AM.PlayNewInstance("SFX/SoundEffects/BoostActivate");
	mySpeedBoost = CommonUtilities::Clamp01(mySpeedBoost + myProps.myFlapStrength);
	--myProps.myFlapCount;
}

void CPlayer::Sway()
{
	float dt = IWorld::Time().GetDeltaTime();

	//Sideways sway
	CommonUtilities::Vector3f dir1 = { myCurrentTunnelDirection.GetForward().x, 0.f, myCurrentTunnelDirection.GetForward().z };
	CommonUtilities::Vector3f dir2 = { myTunnelDirection.GetForward().x, 0.f, myTunnelDirection.GetForward().z };
	if (dir1.Cross(dir2).y > 0.f)
	{
		myLevelRotation.x = CommonUtilities::Lerp(myLevelRotation.x, -std::acosf(CommonUtilities::Clamp01(dir1.GetNormalized().Dot(dir2.GetNormalized()))), dt*3.f);
	}
	else
	{
		myLevelRotation.x = CommonUtilities::Lerp(myLevelRotation.x, std::acosf(CommonUtilities::Clamp01(dir1.GetNormalized().Dot(dir2.GetNormalized()))), dt*3.f);
	}

	GetTransform().RotateAround(GetTransform().GetForward(), -myVelocity.x + myLevelRotation.x);
	GetTransform().RotateAround(GetTransform().GetRight(), -myVelocity.y - 0.2f);
}

float CPlayer::GetSpeed() const
{
	return (CommonUtilities::Lerp(myProps.mySlowForwardSpeed, myProps.myFastForwardSpeed, mySpeedBoost) - CommonUtilities::Clamp(myCurrentTunnelDirection.GetForward().y, -1.f, 0.f) * myProps.mySlowForwardSpeed) * mySlowDown;
}

void CPlayer::BoostHoop()
{
	mySpeedBoost = 1.0f;
}

void CPlayer::Collide()
{
	mySlowDown = 0.35f;
}

float CPlayer::GetMaxFlapCount() const
{
	return myProps.myMaxFlapCount;
}

float CPlayer::GetCurrentFlapCount() const
{
	return myProps.myFlapCount;
}

void CPlayer::SetWingPosition()
{
	CTransform& transform = GetTransform();
	CommonUtilities::Vector3f playerPos = transform.GetPosition();

	float _x = CommonUtilities::Clamp01(GetTransform().GetForward().y + 0.1f + mySpeedBoost);
	_x *= _x;

	static float x = 0.f;
	x = CommonUtilities::Lerp(x, _x, IWorld::Time().GetDeltaTime()*10.f);

	static float time = 0.f;
	time += IWorld::Time().GetDeltaTime() * 5.f;

	myLeftWing.GetTransform().SetRotation({ 0.f, 0.f, std::sinf(time)*x });
	myRightWing.GetTransform().SetRotation({ 0.f, 0.f, -std::sinf(time)*x });
}

void CPlayer::InitWings()
{
	myWingFXOffset = { 0.7f, -0.18f, -0.2f };
	myWingOffset = { 0.08148f, 0.03249f, -0.05619f };

	myLeftWing.AddComponent<CModelComponent>("Assets/Models/player/player_l_wing.fbx");
	myRightWing.AddComponent<CModelComponent>("Assets/Models/player/player_r_wing.fbx");

	myLeftWingFX.AddComponent<CParticleSystemComponent>("Assets/Particles/playerWingTrail.json");
	myRightWingFX.AddComponent<CParticleSystemComponent>("Assets/Particles/playerWingTrail.json");


	myLeftWing.GetTransform().SetParent(&GetTransform());
	myRightWing.GetTransform().SetParent(&GetTransform());
	myLeftWing.SetPosition({ -myWingOffset.x, myWingOffset.y, myWingOffset.z });
	myRightWing.SetPosition({ myWingOffset.x, myWingOffset.y, myWingOffset.z });

	myLeftWingFX.GetTransform().SetParent(&myLeftWing.GetTransform());
	myRightWingFX.GetTransform().SetParent(&myRightWing.GetTransform());
	myLeftWingFX.SetPosition({ -myWingFXOffset.x, myWingFXOffset.y, myWingFXOffset.z });
	myRightWingFX.SetPosition({ myWingFXOffset.x, myWingFXOffset.y, myWingFXOffset.z });
}


void CPlayer::InitBoostWindEffect(ID_T(CScene) aSceneID)
{
	myBoostWindEffects.Init(myNumberOfBoostWindEffects);
		
	for (unsigned short i = 0; i < myNumberOfBoostWindEffects; ++i)
	{
		myBoostWindEffects.EmplaceBack();
		myBoostWindEffects[i].myGameObject.Init(aSceneID);

		if (i % 3 == 0)
		{
			myBoostWindEffects[i].myParticleComponent = myBoostWindEffects[i].myGameObject.AddComponent<CParticleSystemComponent>("Assets/Particles/boostWindEffect_Short.json");
		}
		else if (i % 3 == 1)
		{
			myBoostWindEffects[i].myParticleComponent = myBoostWindEffects[i].myGameObject.AddComponent<CParticleSystemComponent>("Assets/Particles/boostWindEffect_Medium.json");
		}
		else if (i % 3 == 2)
		{
			myBoostWindEffects[i].myParticleComponent = myBoostWindEffects[i].myGameObject.AddComponent<CParticleSystemComponent>("Assets/Particles/boostWindEffect_Long.json");
		}

		myBoostWindEffects[i].myParticleComponent->Stop();

		myBoostWindEffects[i].myAngle = (2.f * CommonUtilities::Pif) * (i / static_cast<float>(myNumberOfBoostWindEffects));
		myBoostWindEffects[i].myDistance = 15.f + 10.f * std::cosf(i * i * 5.375f);
	}
}

void CPlayer::PlayBoostWindEffect()
{
	static float spawnRestriction = 0.f;
	spawnRestriction += IWorld::Time().GetDeltaTime();
	if (spawnRestriction < (1.f / myNumberOfBoostWindEffects))
	{
		return;
	}
	for (unsigned short i = 0; i < myBoostWindEffects.Size(); ++i)
	{
		if (!myBoostWindEffects[i].myParticleComponent->IsPlaying())
		{
			SetBoostWindEffectPosition(i);
			myBoostWindEffects[i].myParticleComponent->Play();
			spawnRestriction = 0.f;
			return;
		}
	}
}

void CPlayer::StopBoostWindEffect()
{
	for (unsigned short i = 0; i < myBoostWindEffects.Size(); ++i)
	{
		myBoostWindEffects[i].myParticleComponent->Stop();
	}
}

void CPlayer::SetBoostWindEffectPosition(unsigned short aBoostWindEffectIndex)
{
	float time = IWorld::Time().GetTotalTime();
	SBoostWindEffect& windEffect = myBoostWindEffects[aBoostWindEffectIndex];

	CommonUtilities::Matrix44f orientation = CommonUtilities::LookAt({ 0.f, 0.f, 0.f }, GetTransform().GetForward());
	orientation *= CommonUtilities::Matrix44f::CreateRotateAround(orientation.myRightAxis, -myVelocity.y - 0.2f);
	const auto& pos = GetTransform().GetPosition();

	windEffect.myGameObject.SetPosition(pos + 
		orientation.myRightAxis * (std::cosf(windEffect.myAngle + time * 137.32f) * windEffect.myDistance) +
		orientation.myUpAxis * (std::sinf(windEffect.myAngle + time * 133.8f) * windEffect.myDistance) +
		orientation.myForwardAxis * (-14.f + std::cosf(windEffect.myDistance) * 6.f)
	);

	windEffect.myGameObject.GetTransform().SetLookDirection(orientation.myForwardAxis);
}

void CPlayer::ReadControlsFromJson()
{
	myProps.myInvertedYAxis = true;
	JsonDocument playerControlsDoc;
	playerControlsDoc.LoadFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), true);
	
	if (playerControlsDoc.Find("player"))
	{
		JsonValue playerControls = playerControlsDoc["player"];
		if (playerControls.Find("controls"))
		{
			if (playerControls["controls"].Find("invertedYAxis"))
			{
				myProps.myInvertedYAxis = playerControls["controls"]["invertedYAxis"].GetBool();
			}
			else
			{
				GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->controls->InvertedYAxis! Default value applied.");
				myProps.myInvertedYAxis = true;
			}
		}
		else
		{
			GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member player->controls! Default value applied.");
			myProps.myInvertedYAxis = true;
		}
	}
}

#pragma once

#include "GameObject.h"
#include "Vector.h"

#include "JsonDocument.h"

class CModelComponent;
class CCameraComponent;

class CPlayer : public CGameObject
{
public:
	CPlayer();
	~CPlayer();

	void Init(ID_T(CScene) aSceneID) override;
	void LoadPlayerPropsFromJson(JsonValue aPlayerJson);
	void SetDefaultValues();

	void SetCanBoost(const bool& aCanBoost);

	void Update(const bool& aIsLevelComplete);
	void UpdateWings();

	void AddPoints();
	int GetPoints() const;

	void Flap();

	void SetTunnelDirection(const CommonUtilities::Vector3f& aDirection) { myTunnelDirection.SetLookDirection(aDirection); }
	void SetCurrentTunnelDirection(const CommonUtilities::Vector3f& aDirection) { myCurrentTunnelDirection.SetLookDirection(aDirection); }
	void Sway();
	float GetSpeed() const;
	float GetSpeedBoost() const { return mySpeedBoost; }
	float GetSlowDown() const { return mySlowDown; }

	void BoostHoop();
	void Collide();

	float GetMaxFlapCount() const;
	float GetCurrentFlapCount() const;

	void SetWingPosition();
	void InitWings();
	void InitBoostWindEffect(ID_T(CScene) aSceneID);
	void PlayBoostWindEffect();
	void StopBoostWindEffect();
	void SetBoostWindEffectPosition(unsigned short aBoostWindEffectIndex);
	void ReadControlsFromJson();
	 

private:
	int myPoints;

	struct
	{
		float myAcceleration;
		float myDeceleration;
		float mySpeed;
		float myRotationLerpSpeed;
		float mySlowForwardSpeed;
		float myFastForwardSpeed;

		float myFlapCount;
		float myMaxFlapCount;
		float myFlapStrength;
		float myFlapIncrease;
		float myFlapDecay;

		bool myInvertedYAxis;
	} myProps;

	CommonUtilities::Vector2f myVelocity;
	CommonUtilities::Vector2f myLevelRotation;
	float mySpeedBoost;
	float mySlowDown;
	bool myColliding;

	CTransform myTunnelDirection;
	CTransform myCurrentTunnelDirection;
	CGameObject myLeftWingFX;
	CGameObject myRightWingFX;
	CommonUtilities::Vector3f myWingFXOffset;

	CGameObject myLeftWing;
	CommonUtilities::Vector3f myWingOffset;
	CGameObject myRightWing;

	static constexpr unsigned short myNumberOfBoostWindEffects = 70;

	struct SBoostWindEffect
	{
		CGameObject myGameObject;
		float myAngle;
		float myDistance;
		CParticleSystemComponent* myParticleComponent;
	};

	CommonUtilities::GrowingArray<SBoostWindEffect> myBoostWindEffects;

	bool myCanBoost;
};
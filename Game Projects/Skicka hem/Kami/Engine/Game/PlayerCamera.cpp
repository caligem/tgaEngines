#include "stdafx.h"
#include "PlayerCamera.h"

#include "IWorld.h"
#include <Random.h>

CPlayerCamera::CPlayerCamera()
{
	myCurrentFov = 0.f;
}

CPlayerCamera::~CPlayerCamera()
{
}

void CPlayerCamera::Update(const CommonUtilities::Vector3f& aPlayerPosition, const CommonUtilities::Vector3f& aClosestPoint, CTransform aTransform, float aSpeedBoost, float aSlowDown, const bool& aIsLevelFinished)
{
	float dt = IWorld::Time().GetDeltaTime();

	CommonUtilities::Vector3f target = CommonUtilities::Lerp(aPlayerPosition, aClosestPoint, myCenterDrag);
	target += aTransform.GetRight() * myOffset.x;
	target += aTransform.GetUp() * myOffset.y;
	target += aTransform.GetForward() * myOffset.z;

	const CommonUtilities::Vector3f cameraPosistionToMoveTo = CommonUtilities::Lerp(
		GetTransform().GetPosition(),
		target,
		dt * myLerpSpeed
	);

	CCameraComponent* camera = GetComponent<CCameraComponent>();
	if (camera == nullptr)return;

	myCurrentFov = CommonUtilities::Lerp(myCurrentFov, CommonUtilities::Clamp01(aSpeedBoost), dt);
	camera->SetFov(CommonUtilities::Lerp(mySlowFov, myFastFov, myCurrentFov));

	SetPosition(cameraPosistionToMoveTo);

	float s = (1.f - aSlowDown);
	s *= 0.5f;

	if (aIsLevelFinished)
	{
		s = 0;
	}

	GetTransform().SetLookAtRotation(
	{ 0.f, 0.f, 0.f },
		CommonUtilities::Slerp(GetTransform().GetForward(), aTransform.GetForward() + CommonUtilities::Vector3f(
				CommonUtilities::Random()-0.5f, CommonUtilities::Random()-0.5f, CommonUtilities::Random()-0.5f
			) *s, dt * myRotationLerpSpeed)
	);
}

void CPlayerCamera::SetDefaultValues()
{
	SetRotationLerpSpeed(2.0f);
	SetLerpSpeed(2.0f);
	myCenterDrag = 0.3f;
	SetSlowFov(60.f);
	SetFastFov(90.f);
	SetOffset({ 0.f, 1.f, -5.5f });
}

void CPlayerCamera::LoadFromJson(JsonValue aCameraJson)
{
	if (aCameraJson.Find("rotationLerpSpeed"))
	{
		SetRotationLerpSpeed(aCameraJson["rotationLerpSpeed"].GetFloat());
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member camera->rotationLerpSpeed! Default value applied.");
		SetRotationLerpSpeed(2.0f);
	}
	if (aCameraJson.Find("lerpSpeed"))
	{
		SetLerpSpeed(aCameraJson["lerpSpeed"].GetFloat());
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member camera->lerpSpeed! Default value applied.");
		SetLerpSpeed(2.0f);
	}
	if (aCameraJson.Find("centerDrag"))
	{
		myCenterDrag = aCameraJson["centerDrag"].GetFloat();
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member camera->offset! Default value applied.");
		myCenterDrag = 0.3f;
	}

	if (aCameraJson.Find("slowFov"))
	{
		SetSlowFov(aCameraJson["slowFov"].GetFloat());
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member camera->slowFov! Default value applied.");

		SetSlowFov(60.f);
	}

	if (aCameraJson.Find("fastFov"))
	{
		SetFastFov(aCameraJson["fastFov"].GetFloat());
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member camera->fastFov! Default value applied.");

		SetFastFov(90.f);
	}

	if (aCameraJson.Find("offset"))
	{
		SetOffset({
			aCameraJson["offset"]["x"].GetFloat(),
			aCameraJson["offset"]["y"].GetFloat(),
			aCameraJson["offset"]["z"].GetFloat()
		});
	}
	else
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Error: Settingsfile is missing member camera->offset! Default value applied.");
		SetOffset({ 0.f, 1.f, -5.5f });
	}
}
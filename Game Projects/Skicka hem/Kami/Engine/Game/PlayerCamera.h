#pragma once

#include "GameObject.h"
#include "Player.h"
#include "Vector.h"

#include "JsonDocument.h"

class CPlayerCamera : public CGameObject
{
public:
	CPlayerCamera();
	~CPlayerCamera();
	
	void Update(const CommonUtilities::Vector3f& aPlayerPosition, const CommonUtilities::Vector3f& aClosestPoint, CTransform aTransform, float aSpeedBoost, float aSlowDown, const bool& aIsLevelFinished);

	void SetRotationLerpSpeed(float aRotationLerpSpeed) { myRotationLerpSpeed = aRotationLerpSpeed; }
	void SetLerpSpeed(float aLerpSpeed) { myLerpSpeed = aLerpSpeed; }
	void SetSlowFov(float aSlowFov) { mySlowFov = aSlowFov; }
	void SetFastFov(float aFastFov) { myFastFov = aFastFov; }
	void SetOffset(const CommonUtilities::Vector3f& aOffset) { myOffset = aOffset; }

	float GetRotationLerpSpeed() const { return myRotationLerpSpeed; }
	float GetLerpSpeed() const { return myLerpSpeed; }
	float GetSlowFov() const { return mySlowFov; }
	float GetFastFov() const { return myFastFov; }
	const CommonUtilities::Vector3f& GetOffset() const { return myOffset; }
	void SetDefaultValues();
	void LoadFromJson(JsonValue aCameraJson);

private:
	float myRotationLerpSpeed;
	float myLerpSpeed;
	float myCenterDrag;
	float mySlowFov;
	float myFastFov;
	float myCurrentFov;
	CommonUtilities::Vector3f myOffset;
};


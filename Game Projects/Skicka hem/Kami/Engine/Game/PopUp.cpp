#include "stdafx.h"
#include "PopUp.h"

#include "IWorld.h"
#include "XBoxController.h"

CPopUpSprite::CPopUpSprite()
{
	myShouldBePlayed = false;
	myIsDone = false;
	myDurationToBeShown = 0.f;
	myFadeTime = 0.f;
	myFadeTimer = 0.f;
	myDurationTimer = 0.f;
}

CPopUpSprite::~CPopUpSprite()
{

}

void CPopUpSprite::Init(ID_T(CScene) aSceneID, char* aSpriteName, float aDurationToBeShown, float aFadeTime, bool aShouldFadeOut, CommonUtilities::Vector2f aPosition)
{
	myDurationToBeShown = aDurationToBeShown;
	myFadeTime = aFadeTime;
	myFadeTimer = myFadeTime;
	myShouldFadeOut = aShouldFadeOut;
	mySpriteObject.Init(aSceneID);
	mySprite = mySpriteObject.AddComponent<CSpriteComponent>(aSpriteName);
	mySprite->SetPosition(aPosition);
	mySprite->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	mySprite->SetPriority(2);
	mySprite->SetTint(CommonUtilities::Vector4f(1.0f ,1.0f, 1.0f, 1.0f));
	mySpriteObject.SetActive(false);
}

void CPopUpSprite::Update()
{
	float dt = IWorld::Time().GetRealDeltaTime();
	Input::CInputManager& input = IWorld::Input();

	if (myShouldBePlayed && !myIsDone)
	{
		if (myDurationTimer < myDurationToBeShown)
		{
			myDurationTimer += dt;
		}
		else if (myShouldFadeOut)
		{
			mySprite->SetTint({ 1.f, 1.f, 1.f, CommonUtilities::Lerp(myFadeTimer, 0.f, dt) });
			myFadeTimer -= dt;

			if (myFadeTimer < 0)
			{
				myShouldBePlayed = false;
				myIsDone = true;
				mySpriteObject.SetActive(false);
			}
		}
		else
		{
			myShouldBePlayed = false;
			myIsDone = true;
			mySpriteObject.SetActive(false);
		}
	}
	if (input.IsKeyPressed(Input::Key_Space) ||
		input.IsKeyPressed(Input::Key_Return) ||
		input.IsKeyPressed(Input::Key_Escape) ||
		input.IsButtonPressed(Input::Button_Left) ||
		input.IsButtonPressed(Input::Button_Right) ||
		IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton::XButton_A))
	{
		myDurationTimer = myDurationToBeShown - dt;
		myFadeTimer = dt;
		mySpriteObject.SetActive(false);
	}
}

void CPopUpSprite::Play()
{
	myShouldBePlayed = true;
	mySpriteObject.SetActive(true);
	myIsDone = false;
	myDurationTimer = 0.f;
	myFadeTimer = myFadeTime;
	mySpriteObject.GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
}

const bool CPopUpSprite::GetIsPlaying() const
{
	return myShouldBePlayed;
}

const bool CPopUpSprite::GetIsDone() const
{
	return myIsDone;
}

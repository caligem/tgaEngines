#pragma once

class CSpriteComponent;

class CPopUpSprite
{
public:
	CPopUpSprite();
	~CPopUpSprite();
	void Init(ID_T(CScene) aSceneID, char* aSpriteName, float aDuration = 1.f, float aFadeTime = 1.f, bool aShouldFadeOut = true, CommonUtilities::Vector2f aPosition = { 0.5f, 0.5f });
	void Update();
	void Play();
	const bool GetIsPlaying() const;
	const bool GetIsDone() const;
private:
	CGameObject mySpriteObject;
	CSpriteComponent* mySprite;
	float myDurationTimer;
	float myDurationToBeShown;
	float myFadeTimer;
	float myFadeTime;
	bool myShouldBePlayed;
	bool myShouldFadeOut;
	bool myIsDone;
};


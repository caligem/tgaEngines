#include "stdafx.h"
#include "ScorescreenState.h"
#include "StateStack.h"
#include "GameObject.h"
#include <JsonDocument.h>
#include "IWorld.h"
#include "GButton.h"
#include "FileWatcher.h"

#include "AudioManager.h"
#include <iomanip>
#include <sstream>

#include "FileDialog.h"

CScorescreenState::CScorescreenState()
{
	myCapturedHoopsLastRace = 0;
	myHighScore = 0;
	myTotalNrOfHoops = 0;
	myRaceTime = 0;
	myIsTimeAttackRound = false;
	myBeatTimeAttack = false;
}


CScorescreenState::~CScorescreenState()
{
	IWorld::GetFileWatcher().StopWatchingFile(L"scoreScreenUIPositions.json", myUIPosFilewatcherID);
}


void CScorescreenState::SetBirdIcons(const unsigned int& aBabyBirdCountHighscore)
{
	unsigned short currentBirdsSet = 0;

	if (myScoreDoc[myLevelName.c_str()].Find("timeAttackBeaten"))
	{
		myBeatTimeAttack = myScoreDoc[myLevelName.c_str()]["timeAttackBeaten"].GetBool();
	}

	int lastBird = EScoreScreenState_ThirdBird;
	if (myIsTimeAttackRound)
	{
		lastBird = EScoreScreenState_TimeAttackBird;
	}

	for (int index = EScoreScreenState_FirstBird; index <= lastBird; ++index)
	{
		myScoreGameObjects[index].SetActive(true);

		if ((aBabyBirdCountHighscore > 0 && currentBirdsSet < aBabyBirdCountHighscore)
			|| (myBeatTimeAttack && index == EScoreScreenState_TimeAttackBird))
		{
			if (index == EScoreScreenState_TimeAttackBird)
			{
				myScoreGameObjects[index].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/iconBirdGoldenLight.dds");
			}
			else
			{
				myScoreGameObjects[index].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/iconBirdLight.dds");
			}
		}
		else
		{
			myScoreGameObjects[index].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/iconBirdDark.dds");
		}

		myScoreGameObjects[index].GetComponent<CSpriteComponent>()->SetPivot({ 0.5f, 0.5f });
		myScoreGameObjects[index].GetComponent<CSpriteComponent>()->SetScale({ 0.8f, 0.8f });
		++currentBirdsSet;
	}
}

bool CScorescreenState::Init(const std::string& aLvlName)
{
	myLevelName = aLvlName;

	////CREATE SCENE
	CState::Init();

	myGUI.Init();

	/////LOAD SCORE
	myScoreDoc.LoadFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), true);
	if (!myScoreDoc.Find(myLevelName.c_str()))
	{
		GAMEPLAY_LOG(CONCOL_ERROR, "Failed CWinScreenState::Init(), lvlName not found!");
		return false;
	}

	/////////////SCORE VARIABLES
	myHighScore = static_cast<unsigned short>(myScoreDoc[myLevelName.c_str()]["hoopHighscore"].GetInt());
	myCapturedHoopsLastRace = static_cast<unsigned short>(myScoreDoc[myLevelName.c_str()]["hoopsCapturedLastRace"].GetInt());
	myTotalNrOfHoops = static_cast<unsigned short>(myScoreDoc[myLevelName.c_str()]["totalNrOfHoops"].GetInt());
	myRaceTime = (myScoreDoc[myLevelName.c_str()]["raceTime"].GetFloat());

	for (int i = 0; i < EScoreScreenState_Count; ++i)
	{
		myScoreGameObjects[i].Init(mySceneID);
		myScoreGameObjects[i].SetActive(true);
	}

	//////////// TIME SCORE
	if (myScoreDoc[myLevelName.c_str()].Find("timeAttackMsgWasShown"))
	{
		myIsTimeAttackRound = myScoreDoc[myLevelName.c_str()]["timeAttackMsgWasShown"].GetBool();
		myScoreGameObjects[EScoreScreenState_Time].SetActive(myIsTimeAttackRound);
	}

	if (myScoreDoc[myLevelName.c_str()].Find("timeAttackBeaten"))
	{
		if (myScoreDoc[myLevelName.c_str()]["timeAttackBeaten"].GetBool())
		{
			myBeatTimeAttack = true;
		}
	}

	if (myIsTimeAttackRound)
	{
		myCurrentMode = "TimeAttack";
	}
	else
	{
		myCurrentMode = "FirstRound";
	}

	if (myIsTimeAttackRound)
	{
		CSpriteComponent* spriteComponent = myScoreGameObjects[EScoreScreenState_Time].AddComponent<CSpriteComponent>("Assets/Sprites/uiTimerIcon.dds");
		spriteComponent->SetTint({ 1.0f, 1.0f, 1.0f, 0.5f });
		spriteComponent->SetPivot({ 0.5f,0.5f });
		spriteComponent->SetScale({ 0.9f, 0.9f });

		std::ostringstream yourTime;
		unsigned short mins = static_cast<unsigned short>(myRaceTime) / 60;
		unsigned short secs = static_cast<unsigned short>(myRaceTime) % 60;
		unsigned short millisecs = static_cast<unsigned short>(myRaceTime * 100.f) % 100;
		yourTime << std::setfill('0') << std::setw(2) << mins << ":" << std::setfill('0') << std::setw(2) << secs << "." << std::setfill('0') << std::setw(2) << millisecs;

		CTextComponent* textComponent = myScoreGameObjects[EScoreScreenState_Time].AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");
		textComponent->SetText("Your time: " + yourTime.str());
		textComponent->SetTint({ 0.4f, 0.4f, 0.4f, 1.4f });
		textComponent->SetScale({ 1.3f, 1.3f });
		textComponent->SetRotation(0);

		textComponent = myScoreGameObjects[EScoreScreenState_TimeToBeatText].AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");

		JsonDocument timeToBeatJSON;
		timeToBeatJSON.LoadFile("settings.json");

		float timeToBeat = 180;
		const std::string timeToBeatMember = "timeToBeat" + myLevelName;

		if (timeToBeatJSON.Find("misc") && timeToBeatJSON["misc"].Find(timeToBeatMember.c_str()))
		{
			timeToBeat = timeToBeatJSON["misc"][timeToBeatMember.c_str()].GetFloat();
		}
		else
		{
			// should never get here, but just in case
			GAMEPLAY_LOG(CONCOL_WARNING, "Settings-file missing 'misc' or 'timeToBeat' in 'misc'!!");
		}

		std::ostringstream timeToBeatSS;
		mins = static_cast<unsigned short>(timeToBeat) / 60;
		secs = static_cast<unsigned short>(timeToBeat) % 60;
		millisecs = static_cast<unsigned short>(timeToBeat * 100.f) % 100;
		timeToBeatSS << std::setfill('0') << std::setw(2) << mins << ":" << std::setfill('0') << std::setw(2) << secs << "." << std::setfill('0') << std::setw(2) << millisecs;


		textComponent->SetText("Time to beat: " + timeToBeatSS.str());
		textComponent->SetTint({ 0.4f, 0.4f, 0.4f, 1.4f });
		textComponent->SetScale({ 1.3f, 1.3f });
		textComponent->SetRotation(0);

		textComponent = myScoreGameObjects[EScoreScreenState_TimeResultText].AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");
		textComponent->SetTint({ 0.4f, 0.4f, 0.4f, 1.4f });
		textComponent->SetScale({ 1.5f, 1.5f });
		textComponent->SetRotation(0);
	}
	
	////////////SPRITES
	myScorescreenBG.Init(mySceneID);
	myScorescreenBG.SetActive(true);

	if (myIsTimeAttackRound)
	{
		myScorescreenBG.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/scoreTimeAtackBack.dds");
	}
	else
	{
		myScorescreenBG.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/scoreBack.dds");
	}

	myScorescreenBG.GetComponent<CSpriteComponent>()->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myScorescreenBG.GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(0.5f, 0.5f));

	//////////// HOOP SCORE
	CSpriteComponent* spriteComponent = myScoreGameObjects[EScoreScreenState_Hoop].AddComponent<CSpriteComponent>("Assets/Sprites/Menu/iconHoop.dds");
	spriteComponent->SetPivot({ 0.5f,0.5f });
	spriteComponent->SetScale({ 0.9f, 0.9f });
	CTextComponent* textComponent = myScoreGameObjects[EScoreScreenState_Hoop].AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");
	textComponent->SetTint({ 0.4f, 0.4f, 0.4f, 1.0f });
	textComponent->SetScale({ 1.9f, 1.9f });
	textComponent->SetRotation(0);
		
	float fraction = static_cast<float>(myCapturedHoopsLastRace) / static_cast<float>(myTotalNrOfHoops);
	textComponent->SetText(std::to_string(static_cast<int>(round(fraction * 100))) + "%");

	/////////// LEVEL NAME 
	std::string filePath = "";
	if (myLevelName == "Tutorial")
	{
		filePath = "level1Titel.dds";
	}
	else if (myLevelName == "Village")
	{
		filePath = "level2Titel.dds";
	}
	else if (myLevelName == "Pillars")
	{
		filePath = "level3Titel.dds";
	}
	else if (myLevelName == "Roots")
	{
		filePath = "level4Titel.dds";
	}

	CSpriteComponent* sprite = myScoreGameObjects[EScoreScreenState_LevelTitle].AddComponent<CSpriteComponent>(std::string("Assets/Sprites/Menu/" + filePath).c_str());
	sprite->SetPosition(CommonUtilities::Vector2f(0.5f, 0.1f));
	sprite->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));

	//////////// BABY-BIRD ICONS
	unsigned short babyBirdCount = 0;

	if (myScoreDoc[aLvlName.c_str()].Find("babyBirdCountLastRace"))
	{
		babyBirdCount = static_cast<unsigned short>(myScoreDoc[aLvlName.c_str()]["babyBirdCountLastRace"].GetInt());
	}
	
	SetBirdIcons(babyBirdCount);

	myContinueButton.Init(mySceneID);
	CSpriteComponent* continueBtn = myContinueButton.AddComponent<CSpriteComponent>("Assets/Sprites/Menu/continueButton.dds");
	continueBtn->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
	myContinueButton.SetActive(true);


	myShouldGoToLvlSelect = false;

	auto callback = [&](const std::wstring& aFilepath)
	{
		std::string fileName(aFilepath.begin(), aFilepath.end());
		myUIPositionsDoc.LoadFile(fileName.c_str(), false); //TODO: Set encode to true here and in SaveScoreToFile when we don't need to add anything more

		UpdateUIPositions();
	};

	callback(L"scoreScreenUIPositions.json");
	myUIPosFilewatcherID = IWorld::GetFileWatcher().WatchFile(L"scoreScreenUIPositions.json", callback);

	return true;
}

void CScorescreenState::UpdateUIPositions() // SETS ALL UI POSITIONS 
{
	if (myUIPositionsDoc.Find(myCurrentMode))
	{
		myUIPositionsDoc[myCurrentMode].Find("hoopIcon") ?
			myScoreGameObjects[EScoreScreenState_Hoop].GetComponent<CSpriteComponent>()->SetPosition({ myUIPositionsDoc[myCurrentMode]["hoopIcon"]["x"].GetFloat(), myUIPositionsDoc[myCurrentMode]["hoopIcon"]["y"].GetFloat() }) //JSON
			: myScoreGameObjects[EScoreScreenState_Hoop].GetComponent<CSpriteComponent>()->SetPosition({ 0.005f, 0.005f }); //DEFAULT

		myUIPositionsDoc[myCurrentMode].Find("hoopPercentageText") ?
			myScoreGameObjects[EScoreScreenState_Hoop].GetComponent<CTextComponent>()->SetPosition({ myUIPositionsDoc[myCurrentMode]["hoopPercentageText"]["x"].GetFloat(), myUIPositionsDoc[myCurrentMode]["hoopPercentageText"]["y"].GetFloat() }) //JSON
			: myScoreGameObjects[EScoreScreenState_Hoop].GetComponent<CTextComponent>()->SetPosition({ 0.03f, 0.15f }); //DEFAULT

		if (myIsTimeAttackRound)
		{
			myUIPositionsDoc[myCurrentMode].Find("timeIcon") ?
				myScoreGameObjects[EScoreScreenState_Time].GetComponent<CSpriteComponent>()->SetPosition({ myUIPositionsDoc[myCurrentMode]["timeIcon"]["x"].GetFloat(), myUIPositionsDoc[myCurrentMode]["timeIcon"]["y"].GetFloat() }) //JSON
				: myScoreGameObjects[EScoreScreenState_Time].GetComponent<CSpriteComponent>()->SetPosition(CommonUtilities::Vector2f(1.f, 0.0f)); //DEFAULT

			myUIPositionsDoc[myCurrentMode].Find("yourTimeText") ?
				myScoreGameObjects[EScoreScreenState_Time].GetComponent<CTextComponent>()->SetPosition({ myUIPositionsDoc[myCurrentMode]["yourTimeText"]["x"].GetFloat(), myUIPositionsDoc[myCurrentMode]["yourTimeText"]["y"].GetFloat() }) //JSON
				: myScoreGameObjects[EScoreScreenState_Time].GetComponent<CTextComponent>()->SetPosition({ 0.445f, 0.42f }); //DEFAULT

			myUIPositionsDoc[myCurrentMode].Find("timeToBeatText") ?
				myScoreGameObjects[EScoreScreenState_TimeToBeatText].GetComponent<CTextComponent>()->SetPosition({ myUIPositionsDoc[myCurrentMode]["timeToBeatText"]["x"].GetFloat(), myUIPositionsDoc[myCurrentMode]["timeToBeatText"]["y"].GetFloat() }) //JSON
				: myScoreGameObjects[EScoreScreenState_TimeToBeatText].GetComponent<CTextComponent>()->SetPosition({ 0.825f, 0.05f }); //DEFAULT


			myUIPositionsDoc[myCurrentMode].Find("timeResultText") ?
				myScoreGameObjects[EScoreScreenState_TimeResultText].GetComponent<CTextComponent>()->SetPosition({ myUIPositionsDoc[myCurrentMode]["timeResultText"]["x"].GetFloat(), myUIPositionsDoc[myCurrentMode]["timeResultText"]["y"].GetFloat() }) //JSON
				: myScoreGameObjects[EScoreScreenState_TimeResultText].GetComponent<CTextComponent>()->SetPosition({ 0.825f, 0.05f }); //DEFAULT

			if (myScoreDoc[myLevelName.c_str()].Find("timeAttackBeaten") && myScoreDoc[myLevelName.c_str()]["timeAttackBeaten"].GetBool())
			{
				if (myUIPositionsDoc[myCurrentMode].Find("timeResultText"))
				{
					if (myUIPositionsDoc[myCurrentMode]["timeResultText"].Find("winText"))
					{
						myScoreGameObjects[EScoreScreenState_TimeResultText].GetComponent<CTextComponent>()->SetText(myUIPositionsDoc[myCurrentMode]["timeResultText"]["winText"].GetString());
					}
				}
				else
				{
					myScoreGameObjects[EScoreScreenState_TimeResultText].GetComponent<CTextComponent>()->SetText("You beat the time!");
				}
			}
			else
			{
				if (myUIPositionsDoc[myCurrentMode].Find("timeResultText") && myUIPositionsDoc[myCurrentMode]["timeResultText"].Find("loseText"))
				{
					myScoreGameObjects[EScoreScreenState_TimeResultText].GetComponent<CTextComponent>()->SetText(myUIPositionsDoc[myCurrentMode]["timeResultText"]["loseText"].GetString());
				}
				else
				{
					myScoreGameObjects[EScoreScreenState_TimeResultText].GetComponent<CTextComponent>()->SetText("You didn't beat the time!");
				}
			}
		}
		
		myUIPositionsDoc[myCurrentMode].Find("continueButton") ?
			myContinueButton.GetComponent<CSpriteComponent>()->SetPosition({ myUIPositionsDoc[myCurrentMode]["continueButton"]["x"].GetFloat(), myUIPositionsDoc[myCurrentMode]["continueButton"]["y"].GetFloat() }) //JSON
			: myContinueButton.GetComponent<CSpriteComponent>()->SetPosition({ 0.75f, 0.75f }); //DEFAULT


		// needs to be here rather than Init() since its position is dependent on myContinueButton which is set above
		CGButton* toLevelSelect = new CGButton();
		toLevelSelect->SetOnMouseClicked(std::bind(&CScorescreenState::ToLevelSelect, this));
		toLevelSelect->SetOnMouseOverStart(std::bind(&CScorescreenState::OnHoverButton, this));
		toLevelSelect->SetOnMouseOverEnd(std::bind(&CScorescreenState::EndHoverButton, this));
		toLevelSelect->SetPosition(myContinueButton.GetComponent<CSpriteComponent>()->GetPosition());
		toLevelSelect->SetSize(320.f, 120.f);
		myGUI.AddGUIObject(toLevelSelect);

		toLevelSelect->SetConnectedObject(toLevelSelect, TOP);
		toLevelSelect->SetConnectedObject(toLevelSelect, BOTTOM);
		myGUI.SetDefaultFocusObject(toLevelSelect);

		/////////// BIRDS
		int birdNr = 0;
		int lastBird = EScoreScreenState_ThirdBird;

		if (myIsTimeAttackRound)
		{
			lastBird = EScoreScreenState_TimeAttackBird;
		}

		for (int index = EScoreScreenState_FirstBird; index <= lastBird; ++index)
		{
			CommonUtilities::Vector2f defaultPos = { 0.0f, 0.0f };
			char* birdName = "";
			switch (birdNr)
			{
			case 0: { defaultPos = { 0.1f, 0.2f }; birdName = "firstBird"; break; }
			case 1: { defaultPos = { 0.2f, 0.2f }; birdName = "secondBird"; break; }
			case 2: { defaultPos = { 0.3f, 0.2f }; birdName = "thirdBird"; break; }
			case 3: { defaultPos = { 0.4f, 0.2f }; birdName = "timeAttackBird"; break; }
			}

			if (myUIPositionsDoc[myCurrentMode].Find(birdName))
			{
				JsonValue currentJsonBird = myUIPositionsDoc[myCurrentMode][birdName];

				myScoreGameObjects[index].GetComponent<CSpriteComponent>()->SetPosition({ currentJsonBird["x"].GetFloat(), currentJsonBird["y"].GetFloat() });
			}
			else
			{
				myScoreGameObjects[index].GetComponent<CSpriteComponent>()->SetPosition(defaultPos); //DEFAULT
			}

			++birdNr;
		}
	}
}

EStateUpdate CScorescreenState::Update()
{
	myGUI.Update();
	if (myShouldGoToLvlSelect == true)
	{
		return EPop_Main;
	}
	return EDoNothing;
}

void CScorescreenState::OnEnter()
{
	AM.Stop("SFX/SoundEffects/Collision", true, nullptr);
	SetActiveScene();
}

void CScorescreenState::OnLeave()
{
}

void CScorescreenState::ToLevelSelect()
{
	myShouldGoToLvlSelect = true;
}

void CScorescreenState::OnHoverButton()
{
	myContinueButton.GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.06f, 1.06f));
	myContinueButton.GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.1f, 1.1f, 1.1f, 1.0f));
}

void CScorescreenState::EndHoverButton()
{
	myContinueButton.GetComponent<CSpriteComponent>()->SetScale(CommonUtilities::Vector2f(1.f, 1.f));
	myContinueButton.GetComponent<CSpriteComponent>()->SetTint(CommonUtilities::Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
}

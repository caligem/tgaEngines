#pragma once
#include "State.h"
#include "GUI.h"
#include <array>
#include <JsonDocument.h>

class CScorescreenState : public CState
{
public:
	enum EScoreScreenState
	{
		EScoreScreenState_Time,
		EScoreScreenState_TimeToBeatText,
		EScoreScreenState_TimeResultText,
		EScoreScreenState_FirstBird,
		EScoreScreenState_SecondBird,
		EScoreScreenState_ThirdBird,
		EScoreScreenState_TimeAttackBird,
		EScoreScreenState_Hoop,
		EScoreScreenState_LevelTitle,
		EScoreScreenState_Count
	};

	CScorescreenState();
	~CScorescreenState();

	bool Init(const std::string& aLvlName);
	EStateUpdate Update() override;
	void OnEnter() override;
	void OnLeave() override;

private:
	void ToLevelSelect();
	void OnHoverButton();
	void EndHoverButton();

	void SetBirdIcons(const unsigned int& aBabyBirdCountHighscore);
	void UpdateUIPositions();

	CGameObject myScorescreenBG;
	CGUI myGUI;
	std::array<CGameObject, EScoreScreenState_Count> myScoreGameObjects;

	CGameObject myContinueButton;
	bool myShouldGoToLvlSelect;
	unsigned short myTotalNrOfHoops;
	unsigned short myCapturedHoopsLastRace;
	unsigned short myHighScore;
	float myRaceTime;

	JsonDocument myUIPositionsDoc;
	JsonDocument myScoreDoc;
	std::string myLevelName;

	bool myIsTimeAttackRound;
	bool myBeatTimeAttack;
	char* myCurrentMode;

	int myUIPosFilewatcherID;
};


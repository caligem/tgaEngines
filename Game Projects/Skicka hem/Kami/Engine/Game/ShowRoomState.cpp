#include "stdafx.h"
#include "ShowRoomState.h"

#include <InputManager.h>
#include <JsonDocument.h>
#include <IWorld.h>

#include <Quaternion.h>
#include <XBOXController.h>

CShowRoomState::CShowRoomState()
{
}

CShowRoomState::~CShowRoomState()
{
}

bool CShowRoomState::Init()
{
	CState::Init();

	myControlsInfo.Init(mySceneID);
	CTextComponent *controlsInfoTxt = myControlsInfo.AddComponent<CTextComponent>("Assets/Fonts/Papercuts/Papercuts");
	controlsInfoTxt->SetText(" [ESC](or [BACK] on your controller) to quit \n [Alt] + Mouse for camera \n Arrow keys to jump between models");
	controlsInfoTxt->SetScale({ 1.1f, 1.1f });
	controlsInfoTxt->SetTint({ 1.0f, 1.0f, 1.0f, 1.f });
	controlsInfoTxt->SetPivot({0.0f, 1.0f});
	controlsInfoTxt->SetPosition({ 0.0f, 0.97f });
	myControlsInfo.SetActive(true);

	myCyclePositions.Init(8);
	myCycleIndex = 0;

	JsonDocument doc("Assets/Levels/showroom.json");
	std::map<int, std::string> meshFilters;
	if (doc.Find("myDirectionalLight"))
	{
		IWorld::GetSceneManager().GetActiveScene()->SetDirectionalLight({
			doc["myDirectionalLight"]["myX"].GetFloat(),
			doc["myDirectionalLight"]["myY"].GetFloat(),
			doc["myDirectionalLight"]["myZ"].GetFloat()
		});
	}
	for (int i = 0; i < doc["myMeshFilters"].GetSize(); ++i)
	{
		meshFilters[doc["myMeshFilters"][i]["myParent"].GetInt()] = doc["myMeshFilters"][i]["myPath"].GetString();
	}
	for (int i = 0; i < doc["myGameObjects"].GetSize(); ++i)
	{
		auto obj = doc["myGameObjects"][i];
		if (meshFilters.find(obj["myID"].GetInt()) != meshFilters.end())
		{
			CGameObject tempModel;
			tempModel.Init(mySceneID);
			tempModel.AddComponent<CModelComponent>(meshFilters[obj["myID"].GetInt()].c_str());
			tempModel.GetTransform().Move({
				obj["myPosition"]["myX"].GetFloat(),
				obj["myPosition"]["myY"].GetFloat(),
				obj["myPosition"]["myZ"].GetFloat()
			});
			myCyclePositions.Add({
				obj["myPosition"]["myX"].GetFloat(),
				obj["myPosition"]["myY"].GetFloat(),
				obj["myPosition"]["myZ"].GetFloat()
			});
			CommonUtilities::Quatf quat(
				obj["myRotation"]["myW"].GetFloat(),
				obj["myRotation"]["myX"].GetFloat(),
				obj["myRotation"]["myY"].GetFloat(),
				obj["myRotation"]["myZ"].GetFloat()
			);
			tempModel.GetTransform().Rotate(quat.GetEulerAngles());
			tempModel.GetTransform().Scale({
				obj["myScale"]["myX"].GetFloat(),
				obj["myScale"]["myY"].GetFloat(),
				obj["myScale"]["myZ"].GetFloat()
			});
		}
	}

	std::sort(myCyclePositions.begin(), myCyclePositions.end(), [](auto a, auto b)
	{
		if (std::fabsf(a.z - b.z) < 10.f)
		{
			return a.x > b.x;
		}
		return a.z > b.z;
	});

	myPivot = { 0.f, 0.f, 0.f };
	myPivot = myCyclePositions[myCycleIndex];
	myRotation = { 0.f, CommonUtilities::Pif };
	myZoom = 20.f;

	return true;
}

EStateUpdate CShowRoomState::Update()
{
	myShouldPop = false;
	const float movementSpeed = 25.0f;
	const float rotationSpeed = 3.0f;

	Input::CInputManager input = IWorld::Input();

	CommonUtilities::Vector2f movement = input.GetMouseMovement();
	movement.x *= (2.f*CommonUtilities::Pif)/1280.f;
	movement.y *= (CommonUtilities::Pif)/720.f;

	if (input.IsKeyDown(Input::Key_Alt))
	{
		if (input.IsButtonDown(Input::Button_Left))
		{
			myRotation.x += movement.y;
			myRotation.y += movement.x;

			if (myRotation.x >= CommonUtilities::Pif / 2.f)
			{
				myRotation.x = CommonUtilities::Pif / 2.f-0.001f;
			}
			if (myRotation.x <= -CommonUtilities::Pif / 2.f)
			{
				myRotation.x = -CommonUtilities::Pif / 2.f+0.001f;
			}
		}

		if (input.IsButtonDown(Input::Button_Middle))
		{
			myPivot -= myMainCamera.GetTransform().GetRight() * movement.x * myZoom;
			myPivot += myMainCamera.GetTransform().GetUp() * movement.y * myZoom;
		}


		if (input.IsButtonDown(Input::Button_Right))
		{
			myZoom -= (movement.x + movement.y) * myZoom;
			if (myZoom < 0.1f)
			{
				myZoom = 0.1f;
				myPivot += myMainCamera.GetTransform().GetForward() * (movement.x + movement.y);
			}
		}
	}

	if (input.IsKeyPressed(Input::Key_Left))
	{
		if (myCycleIndex == 0)
		{
			myCycleIndex = myCyclePositions.Size() - 1;
		}
		else
		{
			--myCycleIndex;
		}
		myPivot = myCyclePositions[myCycleIndex];
	}
	if (input.IsKeyPressed(Input::Key_Right))
	{
		++myCycleIndex;
		if (myCycleIndex == myCyclePositions.Size())
		{
			myCycleIndex = 0;
		}
		myPivot = myCyclePositions[myCycleIndex];
	}

	if (input.IsKeyPressed(Input::Key_Escape) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_Back))
	{
		myShouldPop = true;
	}

	CommonUtilities::Vector3f newPos(0.f, 0.f, -myZoom);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundX(myRotation.x);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundY(myRotation.y);
	newPos += myPivot;

	myMainCamera.GetTransform().SetPosition(newPos);
	myMainCamera.GetTransform().LookAt(myPivot);

	if (myShouldPop)
	{
		return EPop_Main;
	}
	return EDoNothing;
}

void CShowRoomState::OnEnter()
{
	SetActiveScene();
}

void CShowRoomState::OnLeave()
{
}

#pragma once
#include "State.h"
#include <GrowingArray.h>
#include <Vector.h>

class CShowRoomState : public CState
{
public:
	CShowRoomState();
	~CShowRoomState();

	bool Init();
	EStateUpdate Update() override;
	void OnEnter() override;
	void OnLeave() override;


private:
	CGameObject myControlsInfo;

	CommonUtilities::Vector3f myPivot;
	CommonUtilities::Vector2f myRotation;
	float myZoom;

	CommonUtilities::GrowingArray<CommonUtilities::Vector3f> myCyclePositions;
	unsigned short myCycleIndex;
	bool myShouldPop;
};


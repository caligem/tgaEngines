#include "stdafx.h"
#include "State.h"

#include "StateStack.h"
#include "IWorld.h"

CStateStack* CState::ourStateStack = nullptr;

bool CState::Init()
{
	mySceneID = IWorld::GetSceneManager().CreateScene();

	myMainCamera.Init(mySceneID);
	myMainCamera.AddComponent<CCameraComponent>(
		CCameraComponent::SCameraComponentData(
			60.f,
			static_cast<float>(IWorld::GetWindowSize().x / IWorld::GetWindowSize().y),
			0.1f, 1000.f
		)
	)->SetAsActiveCamera();

	return true;
}

void CState::SetActiveScene()
{
	IWorld::GetSceneManager().SetActiveScene(mySceneID);
}

void CState::DestoryScene()
{
	IWorld::GetSceneManager().DestroyScene(mySceneID);
}

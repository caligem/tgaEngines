#include "stdafx.h"
#include "Tunnel.h"

#include "Mathf.h"

CTunnel::CTunnel()
{
	myDirection = { 0.f, 0.f, 1.f };
	myTunnelRadius = 7.f;

	myAimAssist = 0.05f;
}

CTunnel::~CTunnel()
{
}

void CTunnel::SetAimAssist(float aPercentage)
{
	const float x = (aPercentage - 1.f);

	const float x3 = x*x*x;

	myAimAssist = (x3*x3) * 0.5f;
}

float CTunnel::CalculateInwardsForce(const CommonUtilities::Vector3f& aPosition) const
{
	const CommonUtilities::Vector3f closestPoint = GetClosestPoint(aPosition);

	float distance = (closestPoint - aPosition).Length();

	float x = distance / myTunnelRadius;

	// y = x^16
	const float y1 = x*x;
	const float y2 = y1*y1;
	const float y3 = y2*y2;
	const float y4 = y3*y3;

	return CommonUtilities::Clamp(y4, 0.f, std::fmaxf(2.f*x, 1.f));
}

float CTunnel::CalculateAimAssistForce(const CommonUtilities::Vector3f & aPosition) const
{
	const CommonUtilities::Vector3f closestPoint = GetHoopClosestPoint(aPosition);

	float distance = (closestPoint - aPosition).Length();

	float x = distance / myTunnelRadius;

	const float _y5 = (5.f*x - 1.f);
	const float y5 = (1.f - _y5*_y5);
	const float y6 = std::fmaxf(y5, 0.f);
	const float y62 = y6*y6;
	const float y64 = y62*y62;
	const float y7 = y64 * myAimAssist;

	return CommonUtilities::Clamp(y7, 0.f, std::fmaxf(2.f*x, 1.f));
}

const CommonUtilities::Vector3f CTunnel::GetClosestPoint(const CommonUtilities::Vector3f& aPosition) const
{
	const CommonUtilities::Vector3f tunnelToPlayer = aPosition - myPosition;
	float dot = myDirection.Dot(tunnelToPlayer);
	const CommonUtilities::Vector3f closestPoint = myPosition + myDirection*dot;

	return closestPoint;
}

const CommonUtilities::Vector3f CTunnel::GetHoopClosestPoint(const CommonUtilities::Vector3f & aPosition) const
{
	const CommonUtilities::Vector3f tunnelToPlayer = aPosition - myHoopPosition;
	float dot = myHoopDirection.Dot(tunnelToPlayer);
	const CommonUtilities::Vector3f closestPoint = myHoopPosition + myHoopDirection*dot;

	return closestPoint;
}

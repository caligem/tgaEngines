#pragma once

#include "Vector.h"

class CTunnel
{
public:
	CTunnel();
	~CTunnel();

	void SetPosition(const CommonUtilities::Vector3f& aPosition) { myPosition = aPosition; }
	void SetDirection(const CommonUtilities::Vector3f& aDirection) { myDirection = aDirection.GetNormalized(); }
	void SetHoopPosition(const CommonUtilities::Vector3f& aPosition) { myHoopPosition = aPosition; }
	void SetHoopDirection(const CommonUtilities::Vector3f& aDirection) { myHoopDirection = aDirection.GetNormalized(); }
	void SetRadius(float aRadius) { myTunnelRadius = aRadius; }
	void SetAimAssist(float aPercentage);

	const CommonUtilities::Vector3f& GetPosition() const { return myPosition; }
	const CommonUtilities::Vector3f& GetDirection() const { return myDirection; }
	const CommonUtilities::Vector3f& GetHoopPosition() const { return myHoopPosition; }
	const CommonUtilities::Vector3f& GetHoopDirection() const { return myHoopDirection; }
	const float& GetRadius() const { return myTunnelRadius; }
	float GetRadius2() const { return myTunnelRadius*myTunnelRadius; }
	float CalculateInwardsForce(const CommonUtilities::Vector3f& aPosition) const;
	float CalculateAimAssistForce(const CommonUtilities::Vector3f& aPosition) const;
	const CommonUtilities::Vector3f GetClosestPoint(const CommonUtilities::Vector3f& aPosition) const;
	const CommonUtilities::Vector3f GetHoopClosestPoint(const CommonUtilities::Vector3f& aPosition) const;


private:
	CommonUtilities::Vector3f myPosition;
	CommonUtilities::Vector3f myDirection;
	CommonUtilities::Vector3f myHoopPosition;
	CommonUtilities::Vector3f myHoopDirection;
	float myTunnelRadius;
	float myAimAssist;
};

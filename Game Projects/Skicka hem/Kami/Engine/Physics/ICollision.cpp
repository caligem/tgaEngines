#include "stdafx.h"
#include "ICollision.h"

#include "ColliderSphere.h"
#include "ColliderOBB.h"

bool ICollision::SphereVsSphere(const CColliderSphere & aSphere0, const CColliderSphere & aSphere1, CommonUtilities::Vector3f * aContactPoint)
{
	float d2 = (aSphere0.GetPosition() - aSphere1.GetPosition()).Length2();
	float r2 = aSphere0.GetRadius() + aSphere1.GetRadius();
	r2 *= r2;

	*aContactPoint = aSphere1.GetPosition() + (aSphere0.GetPosition() - aSphere1.GetPosition());

	if (d2 > aSphere1.GetRadius()*aSphere1.GetRadius())
	{
		*aContactPoint = aSphere1.GetPosition() + (aSphere0.GetPosition() - aSphere1.GetPosition()).GetNormalized()*aSphere1.GetRadius();
	}

	if( d2 <= r2 )
	{
		return true;
	}
	return false;
}

bool ICollision::SphereVsOBB(const CColliderSphere & aSphere, const CColliderOBB & aOBB, CommonUtilities::Vector3f * aContactPoint)
{
    CommonUtilities::Vector3f result = aOBB.GetPosition();
    CommonUtilities::Vector3f dir = aSphere.GetPosition() - aOBB.GetPosition();
    for (unsigned short i = 0; i < 3; ++i) 
    {
        const float* orientation = &aOBB.GetOrientation()[i * 3];
        CommonUtilities::Vector3f axis(orientation[0], orientation[1], orientation[2]);
        float distance = dir.Dot(axis);
        if (distance > (&aOBB.GetRadius().x)[i]) 
        {
            distance = (&aOBB.GetRadius().x)[i];
        }
        if (distance < -(&aOBB.GetRadius().x)[i]) 
        {
            distance = -(&aOBB.GetRadius().x)[i];
        }
        result = result + (axis * distance);
    }
	*aContactPoint = result;

	return ((result - aSphere.GetPosition()).Length2() <= aSphere.GetRadius()*aSphere.GetRadius());
}

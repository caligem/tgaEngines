#pragma once
#include <string>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>

#define DL_ASSERT(string) DL_Debug::Debug::GetInstance()->AssertMessage(__FILE__,__LINE__,__FUNCTION__, string);
#define DL_PRINT(string)  DL_Debug::Debug::GetInstance()->PrintMessage(string);
#define DL_PRINTSTACK(string)  DL_Debug::Debug::GetInstance()->PrintStack(string);
#define DL_DEBUG( ... )  DL_Debug::Debug::GetInstance()->DebugMessage(__LINE__,__FUNCTION__,__VA_ARGS__);

namespace DL_Debug
{
	class Debug
	{
	public:
		static void AssertMessage(const char *aFileName, int aLine, const char *aFunctionName, const char *aString);
		static void PrintMessage(const char *aString);
		static void PrintStack(const char *aString);
		static void DebugMessage(const int aLine, const char *aFunctionName, const char *aFormattedString, ...);
		static bool Create();
		static bool Destroy();
		static Debug* GetInstance();
	private:
		Debug();
		~Debug();
		static Debug* ourInstance;
		static std::ofstream ourDebugFile;
		static std::string ourDebugFileName;
	};

}
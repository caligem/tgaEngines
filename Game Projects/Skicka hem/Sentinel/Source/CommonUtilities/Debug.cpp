#include "DL_Debug.h"
#include "DL_StackWalker.h"

DL_Debug::Debug* DL_Debug::Debug::ourInstance;
std::ofstream DL_Debug::Debug::ourDebugFile;
std::string DL_Debug::Debug::ourDebugFileName;

bool DL_Debug::Debug::Create()
{
	assert(ourInstance == nullptr && "Debugobject already created");
	ourInstance = new Debug();
	if (ourInstance == nullptr)
	{
		return(false);
	}
	ourDebugFileName = "DebugLogger - " + (std::string)__DATE__ + ".txt";
	ourInstance->ourDebugFile.open(ourDebugFileName, std::ofstream::app);
	return(true);
}
bool DL_Debug::Debug::Destroy()
{
	ourInstance->ourDebugFile.close();
	delete ourInstance;
	ourInstance = nullptr;
	return(true);
}

DL_Debug::Debug* DL_Debug::Debug::GetInstance()
{
	return(ourInstance);
}

void DL_Debug::Debug::AssertMessage(const char * aFileName, int aLine, const char * aFunctionName, const char * aString)
{
	ourInstance->ourDebugFile << (std::string)__DATE__ + " - " + (std::string)__TIME__ << std::endl;
	ourInstance->ourDebugFile << "Assert in: \"" << aFileName << "\"\n> In function: '" << aFunctionName << "'\n> At line: " << aLine << "\n> Error message: \"" << aString << "\"" << std::endl;
	ourInstance->ourDebugFile << "---------------------------" << std::endl;
	std::wstringstream errorTextStream;

	errorTextStream << "Assert in: \"" << aFileName << "\"\n> In function: '" << aFunctionName << "'\n> At line: " << aLine << "\n> Error message: \"" << aString << "\"";
	std::wcerr << errorTextStream.str() << std::endl;

	ourInstance->ourDebugFile << "Stack: " << std::endl;
	StackWalker stackWalker;
	stackWalker.ShowCallstack();
	ourInstance->ourDebugFile << "---------------------------" << std::endl;

	std::abort();
}
void DL_Debug::Debug::PrintMessage(const char * aString)
{
	ourInstance->ourDebugFile << (std::string)__DATE__ + " - " + (std::string)__TIME__ << std::endl;
	ourInstance->ourDebugFile << "Print: " << aString << std::endl;
	ourInstance->ourDebugFile << "---------------------------" << std::endl;

	std::cout << "Print: " << aString << std::endl;
}
void DL_Debug::Debug::PrintStack(const char * aString)
{
	ourInstance->ourDebugFile << aString;
}
void DL_Debug::Debug::DebugMessage(const int aLine, const char * aFunctionName, const char * aFormattedString, ...)
{
	char buffer[256];
	va_list args;
	va_start(args, aFormattedString);
	vsprintf_s(buffer, aFormattedString, args);
	perror(buffer);
	va_end(args);

	ourInstance->ourDebugFile << (std::string)__DATE__ + " - " + (std::string)__TIME__ << std::endl;
	ourInstance->ourDebugFile << "Debug message in: \"" << aFunctionName << "\"\n> At line: " << aLine << "\n> Message: \"" << buffer << "\"" << std::endl;
	ourInstance->ourDebugFile << "---------------------------" << std::endl;
}
DL_Debug::Debug::Debug()
{
}
DL_Debug::Debug::~Debug()
{
}

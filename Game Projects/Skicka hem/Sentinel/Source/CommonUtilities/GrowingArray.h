#pragma once
#include <assert.h>
#include <string>
namespace CommonUtilities
{
	template<typename ObjectType, typename SizeType = unsigned short>
	class GrowingArray
	{
	public:
		GrowingArray();
		GrowingArray(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag = true);
		GrowingArray(const GrowingArray& aGrowingArray);
		GrowingArray(GrowingArray&& aGrowingArray);

		~GrowingArray();

		GrowingArray& operator=(const GrowingArray& aGrowingArray);
		GrowingArray& operator=(GrowingArray&& aGrowingArray);

		void Init(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag = true);
		void ReInit(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag = true);

		inline ObjectType& operator[](const SizeType& aIndex);
		inline const ObjectType& operator[](const SizeType& aIndex) const;

		inline void Add(const ObjectType& aObject);
		inline void Insert(SizeType aIndex, ObjectType& aObject);
		inline void DeleteCyclic(ObjectType& aObject);
		inline void DeleteCyclicAtIndex(SizeType aItemNumber);
		inline void RemoveCyclic(const ObjectType& aObject);
		inline void RemoveCyclicAtIndex(SizeType aItemNumber);
		inline SizeType Find(const ObjectType& aObject);


		inline ObjectType& GetLast();


		inline const ObjectType& GetLast() const;

		static const SizeType FoundNone = static_cast<SizeType>(-1);

		inline void RemoveAll();
		inline void DeleteAll();

		void Optimize();
		__forceinline SizeType Size() const;
		inline void Resize(SizeType aNewSize);


	private:
		SizeType myCurrentSize;
		ObjectType *myData;
		SizeType myAllocatedSize;
		bool mySafeModeFlag;
		bool myHasBeenInitialized;

	};

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<ObjectType, SizeType>::GrowingArray()
	{
		myData = nullptr;
		myCurrentSize = 0;
		myAllocatedSize = 0;
		mySafeModeFlag = true;
		myHasBeenInitialized = false;
	}

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<ObjectType, SizeType>::GrowingArray(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag)
	{
		myData = new ObjectType[aNrOfRecommendedItems];
		myCurrentSize = 0;
		myAllocatedSize = aNrOfRecommendedItems;
		mySafeModeFlag = aUseSafeModeFlag;
		myHasBeenInitialized = true;
	}

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<ObjectType, SizeType>::GrowingArray(const GrowingArray & aGrowingArray)
	{
		myData = aGrowingArray.myData;
		myHasBeenInitialized = true;
		myCurrentSize = aGrowingArray.myCurrentSize;
		myAllocatedSize = aGrowingArray.myAllocatedSize;
		mySafeModeFlag = aGrowingArray.mySafeModeFlag;


	}

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<ObjectType, SizeType>::GrowingArray(GrowingArray && aGrowingArray)
	{
		myData = aGrowingArray.myData;
		aGrowingArray.myData = nullptr;
		myHasBeenInitialized = true;
		myCurrentSize = aGrowingArray.myCurrentSize;
		myAllocatedSize = aGrowingArray.myAllocatedSize;
		mySafeModeFlag = aGrowingArray.mySafeModeFlag;
	}

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<ObjectType, SizeType>::~GrowingArray()
	{
		DeleteAll();
	}

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<typename ObjectType, typename SizeType> & GrowingArray<ObjectType, SizeType>::operator=(const GrowingArray & aGrowingArray)
	{
		/*if (aGrowingArray.Size() == 0)
		{
			assert((aGrowingArray.myHasBeenInitialized == true) && "Attempt to assign from uninitialized GrowingArray");
		}*/

		myCurrentSize = aGrowingArray.myCurrentSize;
		myAllocatedSize = aGrowingArray.myAllocatedSize;
		myData = nullptr;
		myData = new ObjectType[myAllocatedSize];
		mySafeModeFlag = aGrowingArray.mySafeModeFlag;
		myHasBeenInitialized = aGrowingArray.myHasBeenInitialized;

		if (mySafeModeFlag == true)
		{
			for (SizeType i = static_cast<SizeType>(0); i < myCurrentSize; i++)
			{
				myData[i] = aGrowingArray.myData[i];
			}
		}
		else
		{
			memcpy(myData, aGrowingArray.myData, (sizeof(ObjectType) * myCurrentSize));
		}
		return *this;
	}

	template<typename ObjectType, typename SizeType>
	inline GrowingArray<typename ObjectType, typename SizeType> & GrowingArray<ObjectType, SizeType>::operator=(GrowingArray && aGrowingArray)
	{
		assert((aGrowingArray.myHasBeenInitialized == true) && "Attempt to assign from uninitialized GrowingArray");

		myData = aGrowingArray.myData;
		aGrowingArray.myData = nullptr;
		myCurrentSize = aGrowingArray.myCurrentSize;
		myAllocatedSize = aGrowingArray.myAllocatedSize;
		myHasBeenInitialized = true;
		return(*this);
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::Init(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag)
	{

		assert((myHasBeenInitialized == false) && "Attempt to initialize an initialized GrowingArray");
		myAllocatedSize = aNrOfRecommendedItems;
		//delete myData;
		myData = nullptr;
		myData = new ObjectType[myAllocatedSize];
		myCurrentSize = 0;
		mySafeModeFlag = aUseSafeModeFlag;
		myHasBeenInitialized = true;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::ReInit(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag)
	{
		assert((myHasBeenInitialized == true) && "Attempt to reinitialize an uninitialized GrowingArray");
		myCurrentSize = 0;
		myAllocatedSize = aNrOfRecommendedItems;
		delete[] myData;
		myData = nullptr;
		myData = new ObjectType[myAllocatedSize];
		mySafeModeFlag = aUseSafeModeFlag;
	}

	template<typename ObjectType, typename SizeType>
	inline ObjectType & GrowingArray<ObjectType, SizeType>::operator[](const SizeType & aIndex)
	{
		assert((aIndex < myCurrentSize && aIndex >= 0) && "That index is not a valid size of the array!");
		return myData[aIndex];
	}

	template<typename ObjectType, typename SizeType>
	inline const ObjectType & GrowingArray<ObjectType, SizeType>::operator[](const SizeType & aIndex) const
	{
		assert((aIndex < myCurrentSize && aIndex >= 0) && "That index is not a valid size of the array!");
		return myData[aIndex];
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::Add(const ObjectType & aObject)
	{
		if (myCurrentSize >= myAllocatedSize)
		{
			Resize(myAllocatedSize * 2);
		}

		myData[myCurrentSize] = aObject;
		myCurrentSize++;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::Insert(SizeType aIndex, ObjectType & aObject)
	{
		assert((aIndex < myCurrentSize && aIndex >= 0) && "That index is not a valid size of the array!");
		if (myCurrentSize >= myAllocatedSize)
		{
			Resize(myAllocatedSize * 2);
		}
		for (SizeType i = myCurrentSize; i > aIndex; i--)
		{
			myData[i] = myData[i - 1];
		}
		myData[aIndex] = aObject;
		myCurrentSize++;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::DeleteCyclic(ObjectType & aObject)
	{
		for (SizeType i = 0; i < myCurrentSize; i++)
		{
			if (myData[i] == aObject)
			{
				myData[i] = nullptr;
				delete myData[i];
				myData[i] = myData[myCurrentSize - 1];
				myCurrentSize--;
				return;
			}
		}
		assert(true && "Object not found");
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::DeleteCyclicAtIndex(SizeType aItemNumber)
	{
		assert(aItemNumber < myCurrentSize && "Out of bounds");
		delete myData[aItemNumber];
		myData[aItemNumber] = nullptr;
		myData[aItemNumber] = myData[myCurrentSize - 1];


		myCurrentSize--;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::RemoveCyclic(const ObjectType & aObject)
	{
		for (SizeType i = 0; i < myCurrentSize; i++)
		{
			if (myData[i] == aObject)
			{
				myData[i] = myData[myCurrentSize - 1];
				myCurrentSize--;
				return;
			}
		}
		assert(true && "Object not found");
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::RemoveCyclicAtIndex(SizeType aItemNumber)
	{
		assert(aItemNumber < myCurrentSize && "Out of bounds");
		myData[aItemNumber] = myData[myCurrentSize - 1];
		myCurrentSize--;
	}

	template<typename ObjectType, typename SizeType>
	inline SizeType GrowingArray<ObjectType, SizeType>::Find(const ObjectType & aObject)
	{
		for (SizeType i = 0; i < myCurrentSize; i++)
		{
			if (myData[i] == aObject)
			{
				return i;
			}
		}
		return FoundNone;
	}

	template<typename ObjectType, typename SizeType>
	inline ObjectType & GrowingArray<ObjectType, SizeType>::GetLast()
	{
		return myData[myCurrentSize - 1];
	}

	template<typename ObjectType, typename SizeType>
	inline const ObjectType & GrowingArray<ObjectType, SizeType>::GetLast() const
	{
		return myData[myCurrentSize - 1];
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::RemoveAll()
	{
		myCurrentSize = 0;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::DeleteAll()
	{
		if (myAllocatedSize != 0)
		{
			delete[] myData;
			myData = nullptr;
			myCurrentSize = 0;
			myAllocatedSize = 0;
			mySafeModeFlag = false;
		}
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::Optimize()
	{
		Resize(myCurrentSize);
	}

	template<typename ObjectType, typename SizeType>
	__forceinline SizeType GrowingArray<ObjectType, SizeType>::Size() const
	{
		return myCurrentSize;
	}

	template<typename ObjectType, typename SizeType>
	inline void GrowingArray<ObjectType, SizeType>::Resize(SizeType aNewSize)
	{
		if (aNewSize > myAllocatedSize)
		{
			ObjectType *newArrayOfObjects = new ObjectType[aNewSize];

			for (SizeType i = static_cast<SizeType>(0); i < myCurrentSize; i++)
			{
				newArrayOfObjects[i] = myData[i];
			}
			myAllocatedSize = aNewSize;
			delete[] myData;
			myData = newArrayOfObjects;
		}
		else
		{
			myCurrentSize = aNewSize;
		}

	}
};

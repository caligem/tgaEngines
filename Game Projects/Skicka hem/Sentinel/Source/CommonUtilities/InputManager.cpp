#include "InputManager.h"
#include <windows.h>
#include <Xinput.h>
using namespace CommonUtilities;
InputManager::InputManager()
{

}
InputManager::~InputManager()
{
}

void InputManager::Update()
{
	myPreviousState = myCurrentState;
	UpdateCursorPosition();
	myXBOXControllerManager.Update();
}

void InputManager::UpdateCursorPosition()
{
	myCursorPositionOnScreenLastUpdate = myCursorPositionOnScreen;
	POINT winCursorPosition;
	GetCursorPos(&winCursorPosition);
	ScreenToClient(myApplicationWindow, &winCursorPosition);
	myCursorPositionOnScreen.x = static_cast<float>(winCursorPosition.x);
	myCursorPositionOnScreen.y = static_cast<float>(winCursorPosition.y);
}
void InputManager::SetCursorPositionOnScreen(int aX, int aY)
{
	SetCursorPos(aX, aY);
}

CommonUtilities::Vector2<float> InputManager::GetCursorPositionOnScreen() const
{
	return myCursorPositionOnScreen;
}
CommonUtilities::Vector2<float> InputManager::GetCursorPositionOnScreenLastUpdate() const
{
	return myCursorPositionOnScreenLastUpdate;
}

short InputManager::GetWheelDeltaSinceLastFrame() const
{
	return myWheelDeltaSinceLastFrame;
}

bool InputManager::IsMouseButtonClicked(MouseButton aButtonToCompareWith) const
{
	if (myPreviousState.myVirtualKeys[static_cast<int>(aButtonToCompareWith)] == false && myCurrentState.myVirtualKeys[static_cast<int>(aButtonToCompareWith)] == true)
	{
		return true;
	}
	return false;
}
bool InputManager::IsMouseButtonDown(MouseButton aButtonToCompareWith) const
{
	if (myCurrentState.myVirtualKeys[static_cast<int>(aButtonToCompareWith)] == true)
	{
		return true;
	}
	return false;
}
bool InputManager::IsMouseButtonReleased(MouseButton aButtonToCompareWith) const
{
	if (myPreviousState.myVirtualKeys[static_cast<int>(aButtonToCompareWith)] == true && myCurrentState.myVirtualKeys[static_cast<int>(aButtonToCompareWith)] == false)
	{
		return true;
	}
	return false;
}

bool CommonUtilities::InputManager::IsXBOXButtonDown(const unsigned int aPlayerNumber, GamePadButton aButton)
{
	return myXBOXControllerManager.myControllers[aPlayerNumber].myCurrentState.myButtons[static_cast<int>(aButton)];
}

bool CommonUtilities::InputManager::IsXBOXButtonPressed(const unsigned int aPlayerNumber, GamePadButton aButton)
{
	if (myXBOXControllerManager.myControllers[aPlayerNumber].myCurrentState.myButtons[static_cast<int>(aButton)] == true && myXBOXControllerManager.myControllers[aPlayerNumber].myPreviousState.myButtons[static_cast<int>(aButton)] == false)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool CommonUtilities::InputManager::IsXBOXButtonReleased(const unsigned int aPlayerNumber, GamePadButton aButton)
{
	if (myXBOXControllerManager.myControllers[aPlayerNumber].myCurrentState.myButtons[static_cast<int>(aButton)] == false && myXBOXControllerManager.myControllers[aPlayerNumber].myPreviousState.myButtons[static_cast<int>(aButton)] == true)
	{
		return true;
	}
	else
	{
		return false;
	}
}

CommonUtilities::Vector2<float> CommonUtilities::InputManager::GetXBOXLeftThumb(const unsigned int aPlayerNumber) const
{
	float oldXValue = myXBOXControllerManager.myControllers[aPlayerNumber].myCurrentState.myLeftThumbstick.x + 32768;
	 
	const float max = 65535.0f;
	oldXValue /= max;

	oldXValue *= 200.0f;
	oldXValue -= 100.0f;

	if (oldXValue > -0.5f && oldXValue < 0.5f)
	{
		oldXValue = 0.0f;
	}

	float oldYValue = myXBOXControllerManager.myControllers[aPlayerNumber].myCurrentState.myLeftThumbstick.y + 32768;

	oldYValue /= max;

	oldYValue *= 200.0f;
	oldYValue -= 100.0f;

	if (oldYValue > -0.5f && oldYValue < 0.5f)
	{
		oldYValue = 0.0f;
	}

	CommonUtilities::Vector2f valueToReturn(oldXValue, oldYValue);
	return valueToReturn;
}

CommonUtilities::Vector2<float> CommonUtilities::InputManager::GetXBOXRightThumb(const unsigned int aPlayerNumber) const
{
	float oldXValue = myXBOXControllerManager.myControllers[aPlayerNumber].myCurrentState.myRightThumbstick.x + 32768;

	const float max = 65535.0f;
	oldXValue /= max;

	oldXValue *= 200.0f;
	oldXValue -= 100.0f;

	if (oldXValue > -0.5f && oldXValue < 0.5f)
	{
		oldXValue = 0.0f;
	}

	float oldYValue = myXBOXControllerManager.myControllers[aPlayerNumber].myCurrentState.myRightThumbstick.y + 32768;

	oldYValue /= max;

	oldYValue *= 200.0f;
	oldYValue -= 100.0f;

	if (oldYValue > -0.5f && oldYValue < 0.5f)
	{
		oldYValue = 0.0f;
	}

	CommonUtilities::Vector2f valueToReturn(oldXValue, oldYValue);
	return valueToReturn;
}

float CommonUtilities::InputManager::GetXBOXLeftTrigger(const unsigned int aPlayerNumber) const
{
	return myXBOXControllerManager.myControllers[aPlayerNumber].myCurrentState.myLeftTrigger;
}

float CommonUtilities::InputManager::GetXBOXRightTrigger(const unsigned int aPlayerNumber) const
{
	return myXBOXControllerManager.myControllers[aPlayerNumber].myCurrentState.myRightTrigger;
}

void CommonUtilities::InputManager::VibrateXBOXController(const unsigned int aPlayerNumber, float aLeftmotor, float aRightmotor, float aDuration)
{
	myXBOXControllerManager.myControllers[aPlayerNumber].SetVibration(aDuration, aLeftmotor, aRightmotor);
}

void InputManager::HandleInput(unsigned int aMessage, unsigned int aVirtualKeyIndex)
{
	switch (aMessage)
	{
	case WM_KEYDOWN:
		myCurrentState.myVirtualKeys[aVirtualKeyIndex] = true;
		break;
	case WM_KEYUP:
		myCurrentState.myVirtualKeys[aVirtualKeyIndex] = false;
		break;
	case WM_LBUTTONDOWN:
		myCurrentState.myVirtualKeys[static_cast<int>(MouseButton::Left)] = true;
		break;
	case WM_LBUTTONUP:
		myCurrentState.myVirtualKeys[static_cast<int>(MouseButton::Left)] = false;
		break;
	case WM_RBUTTONDOWN:
		myCurrentState.myVirtualKeys[static_cast<int>(MouseButton::Right)] = true;
		break;
	case WM_RBUTTONUP:
		myCurrentState.myVirtualKeys[static_cast<int>(MouseButton::Right)] = false;
		break;
	case WM_MBUTTONDOWN:
		myCurrentState.myVirtualKeys[static_cast<int>(MouseButton::Middle)] = true;
		break;
	case WM_MBUTTONUP:
		myCurrentState.myVirtualKeys[static_cast<int>(MouseButton::Middle)] = false;
		break;
	case WM_MOUSEWHEEL:
		myWheelDeltaSinceLastFrame = GET_WHEEL_DELTA_WPARAM(aVirtualKeyIndex) / WHEEL_DELTA;
		break;
	default:
		break;
	}
}

void CommonUtilities::InputManager::SetApplicationWindow(HWND aHwnd)
{
	myApplicationWindow = aHwnd;
}

bool InputManager::IsKeyPressed(KeyCode aKeyToCompareWith) const
{
	if (myPreviousState.myVirtualKeys[static_cast<int>(aKeyToCompareWith)] == false && myCurrentState.myVirtualKeys[static_cast<int>(aKeyToCompareWith)] == true)
	{
		return true;
	}
	return false;
}
bool InputManager::IsKeyDown(KeyCode aKeyToCompareWith) const
{
	return myCurrentState.myVirtualKeys[static_cast<int>(aKeyToCompareWith)];
}
bool InputManager::IsKeyReleased(KeyCode aKeyToCompareWith) const
{
	if (myPreviousState.myVirtualKeys[static_cast<int>(aKeyToCompareWith)] == true && myCurrentState.myVirtualKeys[static_cast<int>(aKeyToCompareWith)] == false)
	{
		return true;
	}
	return false;
}
#include "JsonWrapper.h"
#include <string>
#include <iostream>
const json LoadData(const char* aFilePath)
{
	std::ifstream file(aFilePath);
	json data = json::parse(file);
	file.close();
	return data;
}

const json LoadLevel(const char* aLevelName)
{
	json rootData = LoadData("Data/root.json");
	std::string filePath = rootData["levels"][aLevelName];
	filePath = "Data/" + filePath;

	json levelData = LoadData(filePath.c_str());
	rootData.clear();
	return levelData;
}

const json LoadRoom(const json &aLevelData, const char* aLevelName)
{
	json rootData = LoadData("Data/root.json");
	std::string filePath = aLevelName;
	filePath = "Data/" + filePath;

	json levelData = LoadData(filePath.c_str());
	rootData.clear();
	filePath = filePath + "/" + levelData["folderSource"].get<std::string>();
	return levelData;
}



const json LoadLevelBackground(const char* aLevelName)
{
	json rootData = LoadData("Data/root.json");
	std::string filePath = rootData["levelsBackgrounds"][aLevelName];
	filePath = "Data/" + filePath;

	json levelData = LoadData(filePath.c_str());
	rootData.clear();
	return levelData;
}

const json LoadRoomFromTiled(const char * aRoomPath)
{
	std::string filePath = aRoomPath;
	filePath = "Data/" + filePath;
	json levelData = LoadData(filePath.c_str());
	return levelData;
}

const json  LoadPlayerData()
{
	json rootData = LoadData("Data/root.json");
	std::string filePath = rootData["player"];
	filePath = "Data/" + filePath;

	json playerData = LoadData(filePath.c_str());
	rootData.clear();
	return playerData;
}

const json LoadHookData()
{
	json rootData = LoadData("Data/root.json");
	std::string filePath = rootData["hook"];
	filePath = "Data/" + filePath;

	json hookData = LoadData(filePath.c_str());
	rootData.clear();
	return hookData;
}

const json LoadDialogueData()
{
	json rootData = LoadData("Data/root.json");
	std::string filePath = rootData["dialogueData"];
	filePath = "Data/" + filePath;

	json dialogueData = LoadData(filePath.c_str());
	rootData.clear();
	return dialogueData;
}

const json LoadDialogues()
{
	json rootData = LoadData("Data/root.json");
	std::string filePath = rootData["dialogues"];
	filePath = "Data/" + filePath;

	json dialogueData = LoadData(filePath.c_str());
	rootData.clear();
	return dialogueData;
}

const json LoadEnemyData(const char* aPath)
{
	json enemyData = LoadData(aPath);
	return enemyData;
}

const json LoadObjectType(const char * aTypePath)
{
	json rootData = LoadData(aTypePath);
	return rootData;
}

const json LoadAnimationData()
{
	return LoadData("Data/animationData.json");
}

std::string & CleanImagePathLoadedFromTiled(std::string & aPath)
{
	std::string search1 = "../";
	std::string search2 = "\\//";
	std::string search3 = "/S";
	std::string replace1 = "";
	std::string replace2 = "S";

	size_t curIndex = 0;
	while ((curIndex = aPath.find(search1, curIndex)) != std::string::npos) {
		aPath.replace(curIndex, search1.length(), replace1);
		curIndex += replace1.length();
	}

	curIndex = 0;
	while ((curIndex = aPath.find(search2, curIndex)) != std::string::npos) {
		aPath.replace(curIndex, search2.length(), replace1);
		curIndex += replace1.length();
	}
	curIndex = 0;
	while ((curIndex = aPath.find(search3, curIndex)) != std::string::npos) {
		aPath.replace(curIndex, search3.length(), replace2);
		curIndex += replace2.length();
	}
	return aPath;
}

std::string GetObjectTypePath(const std::string & aCategory, const std::string & aType)
{
	json rootData = LoadData("Data/objects/objectRoot.json");
	std::string filePath = rootData[aCategory][aType];
	filePath = "Data/objects/" + filePath;

	return filePath;
}
#pragma once
#include "Tile.h"
#include "json.hpp"
#include <fstream>

using json = nlohmann::json;

const json LoadData(const char* aFilePath);

const json LoadLevel(const char* aLevelName);

const json LoadRoomFromTiled(const char * aRoomPath);

const json LoadPlayerData();

const json LoadHookData();

const json LoadDialogueData();

const json LoadDialogues();

const json LoadEnemyData(const char* aPath);

const json LoadObjectType(const char * aTypePath);

const json LoadAnimationData();

std::string& CleanImagePathLoadedFromTiled(std::string& aPath);

std::string GetObjectTypePath(const std::string& aCategory, const std::string& aType);
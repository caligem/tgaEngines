#pragma once
#include "Vector.h"

namespace CommonUtilities
{
	template <typename T>
	class Line
	{
	public:
		Line();
		Line(const Vector2<T> &aPoint0, const Vector2<T> &aPoint1);

		void InitWith2Points(const Vector2<T> &aPoint0, const Vector2<T> &aPoint1);
		void InitWithPointAndDirection(const Vector2<T> &aPoint, const Vector2<T> &aDirection);
		bool Inside(const Vector2<T> &aPosition) const;

	private:
		Vector2<T> myPoint;
		Vector2<T> myNormal;
	};
	template<typename T>
	inline Line<T>::Line()
	{
	}
	template<typename T>
	inline Line<T>::Line(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1)
	{
		myPoint = aPoint0;
		Vector2<T> direction = aPoint1 - aPoint0;
		myNormal.x = -direction.y;
		myNormal.y = direction.x;
		myNormal.Normalize();
	}
	template<typename T>
	inline void Line<T>::InitWith2Points(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1)
	{
		myPoint = aPoint0;
		Vector2<T> direction = aPoint1 - aPoint0;
		myNormal.x = -direction.y;
		myNormal.y = direction.x;
		myNormal.Normalize();
	}
	template<typename T>
	inline void Line<T>::InitWithPointAndDirection(const Vector2<T>& aPoint, const Vector2<T>& aDirection)
	{
		myPoint = aPoint;
		myNormal.x = -aDirection.y;
		myNormal.y = aDirection.x;
		myNormal.Normalize();

	}
	template<typename T>
	inline bool Line<T>::Inside(const Vector2<T>& aPosition) const
	{
		if (myNormal.Dot((aPosition - myPoint)) < 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
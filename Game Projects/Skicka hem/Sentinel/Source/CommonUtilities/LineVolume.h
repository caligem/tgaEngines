#pragma once
#include "Vector.h"
#include <vector>
#include "Line.h"
namespace CommonUtilities
{
	template <typename T>
	class LineVolume
	{
	public:
		LineVolume(const std::vector<Line<T>> &aLineList);
		void AddLine(const Line<T> &aLine);
		bool Inside(const Vector2<T> &aPosition) const;

	private:
		std::vector<Line<T>> myLines;
	};
	template<typename T>
	inline LineVolume<T>::LineVolume(const std::vector<Line<T>>& aLineList)
	{
		myLines = aLineList;
	}
	template<typename T>
	inline void LineVolume<T>::AddLine(const Line<T>& aLine)
	{
		myLines.push_back(aLine);
	}
	template<typename T>
	inline bool LineVolume<T>::Inside(const Vector2<T>& aPosition) const
	{
		for (unsigned int i = 0; i < myLines.size(); i++)
		{
			if (myLines[i].Inside(aPosition) == false)
			{
				return false;
			}
		}
		return true;
	}
}
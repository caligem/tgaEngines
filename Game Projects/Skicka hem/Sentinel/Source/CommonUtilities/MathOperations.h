#pragma once

namespace CommonUtilities
{
	static float Lerp(float aStartValue, float aEndValue, float aInterpolation)
	{
		return (aStartValue * (1.0f - aInterpolation)) + (aEndValue * aInterpolation);
	}

	template<typename T>
	static T DegreesToRadian(T aDegree)
	{
		return aDegree * (3.14f / 180.f);
	}

	template<typename T>
	static T RadianToDegrees(T aRadian)
	{
		return aRadian * (180.f / 3.14f);
	}
}
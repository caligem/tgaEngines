#pragma once
#include "Vector.h"
#include <math.h>
#include "Matrix44.h"
namespace CommonUtilities
{
	template <class T>
	class Matrix33
	{
	public:
		Matrix33<T>();
		Matrix33<T>(const Matrix33<T>& aMatrix);
		Matrix33<T>(const Matrix44<T>& aMatrix44);
		Matrix33<T>(T aMatrixValues[9]);

		Matrix33<T>& operator=(const Matrix33<T>& aMatrix33);

		static Matrix33<T> CreateRotateAroundX(T aAngleInRadians);
		static Matrix33<T> CreateRotateAroundY(T aAngleInRadians);
		static Matrix33<T> CreateRotateAroundZ(T aAngleInRadians);

		static Matrix33<T> Transpose(const Matrix33<T>& aMatrixToTranspose);

		T myMatrix[9];

	private:
	};

	template<class T>
	inline Matrix33<T>::Matrix33()
	{
		myMatrix[0] = 1;
		myMatrix[1] = 0;
		myMatrix[2] = 0;
		myMatrix[3] = 0;
		myMatrix[4] = 1;
		myMatrix[5] = 0;
		myMatrix[6] = 0;
		myMatrix[7] = 0;
		myMatrix[8] = 1;
	}

	template<class T>
	inline Matrix33<T>::Matrix33(const Matrix33<T>& aMatrix)
	{
		*this = aMatrix;
	}

	template<class T>
	inline Matrix33<T>::Matrix33(const Matrix44<T>& aMatrix44)
	{
		myMatrix[0] = aMatrix44.myMatrix[0];
		myMatrix[1] = aMatrix44.myMatrix[1];
		myMatrix[2] = aMatrix44.myMatrix[2];
		myMatrix[3] = aMatrix44.myMatrix[4];
		myMatrix[4] = aMatrix44.myMatrix[5];
		myMatrix[5] = aMatrix44.myMatrix[6];
		myMatrix[6] = aMatrix44.myMatrix[8];
		myMatrix[7] = aMatrix44.myMatrix[9];
		myMatrix[8] = aMatrix44.myMatrix[10];
	}

	template<class T>
	inline Matrix33<T>::Matrix33(T aMatrixValues[9])
	{
		myMatrix = {aMatrixValues[0], aMatrixValues[1], aMatrixValues[2],
					aMatrixValues[3], aMatrixValues[4], aMatrixValues[5],
					aMatrixValues[6], aMatrixValues[7], aMatrixValues[8]};
	}

	template<class T>
	inline Matrix33<T> operator+(const Matrix33<T>& aMatrix330, const Matrix33<T>& aMatrix331)
	{
		Matrix33<T> countWith = aMatrix330;
		countWith += aMatrix331;
		return countWith;
	}

	template<class T>
	inline Matrix33<T> operator+=(Matrix33<T>& aMatrix330, const Matrix33<T>& aMatrix331)
	{
		for (int i = 0; i < 9; i++)
		{
			aMatrix330.myMatrix[i] = aMatrix330.myMatrix[i] + aMatrix331.myMatrix[i];
		}
		return aMatrix330;
	}
	template<class T>
	inline Matrix33<T> operator-(const Matrix33<T>& aMatrix330, const Matrix33<T>& aMatrix331)
	{
		Matrix33<T> countWith = aMatrix330;
		countWith -= aMatrix331;
		return countWith;
	}
	template<class T>
	inline Matrix33<T> operator-=(Matrix33<T>& aMatrix330, const Matrix33<T>& aMatrix331)
	{
		for (int i = 0; i < 9; i++)
		{
			aMatrix330.myMatrix[i] -= aMatrix331.myMatrix[i];
		}
		return aMatrix330;
	}
	template<class T>
	inline Matrix33<T> operator*(const Matrix33<T>& aMatrix330, const Matrix33<T>& aMatrix331)
	{
		Matrix33<T> matrixToReturn;
		matrixToReturn.myMatrix[0] = (aMatrix330.myMatrix[0] * aMatrix331.myMatrix[0]) + (aMatrix330.myMatrix[1] * aMatrix331.myMatrix[3]) + (aMatrix330.myMatrix[2] * aMatrix331.myMatrix[6]);
		matrixToReturn.myMatrix[1] = (aMatrix330.myMatrix[0] * aMatrix331.myMatrix[1]) + (aMatrix330.myMatrix[1] * aMatrix331.myMatrix[4]) + (aMatrix330.myMatrix[2] * aMatrix331.myMatrix[7]);
		matrixToReturn.myMatrix[2] = (aMatrix330.myMatrix[0] * aMatrix331.myMatrix[2]) + (aMatrix330.myMatrix[1] * aMatrix331.myMatrix[5]) + (aMatrix330.myMatrix[2] * aMatrix331.myMatrix[8]);

		matrixToReturn.myMatrix[3] = (aMatrix330.myMatrix[3] * aMatrix331.myMatrix[0]) + (aMatrix330.myMatrix[4] * aMatrix331.myMatrix[3]) + (aMatrix330.myMatrix[5] * aMatrix331.myMatrix[6]);
		matrixToReturn.myMatrix[4] = (aMatrix330.myMatrix[3] * aMatrix331.myMatrix[1]) + (aMatrix330.myMatrix[4] * aMatrix331.myMatrix[4]) + (aMatrix330.myMatrix[5] * aMatrix331.myMatrix[7]);
		matrixToReturn.myMatrix[5] = (aMatrix330.myMatrix[3] * aMatrix331.myMatrix[2]) + (aMatrix330.myMatrix[4] * aMatrix331.myMatrix[5]) + (aMatrix330.myMatrix[5] * aMatrix331.myMatrix[8]);
		
		matrixToReturn.myMatrix[6] = (aMatrix330.myMatrix[6] * aMatrix331.myMatrix[0]) + (aMatrix330.myMatrix[7] * aMatrix331.myMatrix[3]) + (aMatrix330.myMatrix[8] * aMatrix331.myMatrix[6]);
		matrixToReturn.myMatrix[7] = (aMatrix330.myMatrix[6] * aMatrix331.myMatrix[1]) + (aMatrix330.myMatrix[7] * aMatrix331.myMatrix[4]) + (aMatrix330.myMatrix[8] * aMatrix331.myMatrix[7]);
		matrixToReturn.myMatrix[8] = (aMatrix330.myMatrix[6] * aMatrix331.myMatrix[2]) + (aMatrix330.myMatrix[7] * aMatrix331.myMatrix[5]) + (aMatrix330.myMatrix[8] * aMatrix331.myMatrix[8]);

		return matrixToReturn;
	}
	template<class T>
	inline Matrix33<T> operator*=(Matrix33<T>& aMatrix330, const Matrix33<T>& aMatrix331)
	{
		aMatrix330 = operator*(aMatrix330, aMatrix331);
		return aMatrix330;
	}
	template<class T>
	inline Vector3<T> operator*(const Vector3<T>& aVector3, const Matrix33<T>& aMatrix33)
	{
		Vector3<T> vectorToReturn;

		vectorToReturn.x = (aVector3.x * aMatrix33.myMatrix[0]) + (aVector3.y * aMatrix33.myMatrix[3]) + (aVector3.z * aMatrix33.myMatrix[6]);
		vectorToReturn.y = (aVector3.x * aMatrix33.myMatrix[1]) + (aVector3.y * aMatrix33.myMatrix[4]) + (aVector3.z * aMatrix33.myMatrix[7]);
		vectorToReturn.z = (aVector3.x * aMatrix33.myMatrix[2]) + (aVector3.y * aMatrix33.myMatrix[5]) + (aVector3.z * aMatrix33.myMatrix[8]);

		return vectorToReturn;
	}
	template<class T>
	inline bool operator==(const Matrix33<T>& aMatrix330, const Matrix33<T>& aMatrix331)
	{
		for (int i = 0; i < 9; i++)
		{
			if (aMatrix330.myMatrix[i] != aMatrix331.myMatrix[i])
			{
				return false;
			}
		}
		return true;
	}
	template<class T>
	inline Matrix33<T>& Matrix33<T>::operator=(const Matrix33<T>& aMatrix33)
	{
		for (int i = 0; i < 9; i++)
		{
			this->myMatrix[i] = aMatrix33.myMatrix[i];
		}
		return *this;
	}
	template<class T>
	inline Matrix33<T> Matrix33<T>::CreateRotateAroundX(T aAngleInRadians)
	{
		Matrix33<T> matrixToReturn;
		matrixToReturn.myMatrix[0] = 1;
		matrixToReturn.myMatrix[1] = 0;
		matrixToReturn.myMatrix[2] = 0;
		matrixToReturn.myMatrix[3] = 0;
		matrixToReturn.myMatrix[4] = cos(aAngleInRadians);
		matrixToReturn.myMatrix[5] = sin(aAngleInRadians);
		matrixToReturn.myMatrix[6] = 0;
		matrixToReturn.myMatrix[7] = -sin(aAngleInRadians);
		matrixToReturn.myMatrix[8] = cos(aAngleInRadians);

		return matrixToReturn;
	}
	template<class T>
	inline Matrix33<T> Matrix33<T>::CreateRotateAroundY(T aAngleInRadians)
	{
		Matrix33<T> matrixToReturn;
		matrixToReturn.myMatrix[0] = cos(aAngleInRadians);
		matrixToReturn.myMatrix[1] = 0;
		matrixToReturn.myMatrix[2] = -sin(aAngleInRadians);
		matrixToReturn.myMatrix[3] = 0;
		matrixToReturn.myMatrix[4] = 1;
		matrixToReturn.myMatrix[5] = 0;
		matrixToReturn.myMatrix[6] = sin(aAngleInRadians);
		matrixToReturn.myMatrix[7] = 0;
		matrixToReturn.myMatrix[8] = cos(aAngleInRadians);
		return matrixToReturn;
	}
	template<class T>
	inline Matrix33<T> Matrix33<T>::CreateRotateAroundZ(T aAngleInRadians)
	{
		Matrix33<T> matrixToReturn;
		matrixToReturn.myMatrix[0] = cos(aAngleInRadians);
		matrixToReturn.myMatrix[1] = sin(aAngleInRadians);
		matrixToReturn.myMatrix[2] = 0;
		matrixToReturn.myMatrix[3] = -sin(aAngleInRadians);
		matrixToReturn.myMatrix[4] = cos(aAngleInRadians);
		matrixToReturn.myMatrix[5] = 0;
		matrixToReturn.myMatrix[6] = 0;
		matrixToReturn.myMatrix[7] = 0;
		matrixToReturn.myMatrix[8] = 1;
		return matrixToReturn;
	}
	template<class T>
	inline Matrix33<T> Matrix33<T>::Transpose(const Matrix33<T>& aMatrixToTranspose)
	{
		Matrix33<T> matrixToReturn;
		matrixToReturn.myMatrix[0] = aMatrixToTranspose[0];
		matrixToReturn.myMatrix[1] = aMatrixToTranspose[3];
		matrixToReturn.myMatrix[2] = aMatrixToTranspose[6];
		matrixToReturn.myMatrix[3] = aMatrixToTranspose[1];
		matrixToReturn.myMatrix[4] = aMatrixToTranspose[4];
		matrixToReturn.myMatrix[5] = aMatrixToTranspose[7];
		matrixToReturn.myMatrix[6] = aMatrixToTranspose[2];
		matrixToReturn.myMatrix[7] = aMatrixToTranspose[5];
		matrixToReturn.myMatrix[8] = aMatrixToTranspose[8];
		return matrixToReturn;
	}
}

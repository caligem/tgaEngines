#pragma once
#include "Vector.h"
#include <math.h>
#include <assert.h>
namespace CommonUtilities
{

	template <class T>
	class Matrix44
	{
	public:
		Matrix44<T>();
		Matrix44<T>(const Matrix44<T>& aMatrix);
		Matrix44<T>(T aMatrixValues[16]);
		Matrix44<T>(const T &i0, const T &i1, const T &i2, const T &i3, const T &i4, const T &i5, const T &i6, const T &i7, const T &i8, const T &i9, const T &i10, const T &i11, const T &i12, const T &i13, const T &i14, const T &i15);
		Matrix44<T>& operator=(const Matrix44<T>& aMatrix44);
		T& operator[](const unsigned int aIndex);
		const T& operator[](const unsigned int aIndex) const;
		static Matrix44<T> CreateRotateAroundX(T aAngleInRadians);
		static Matrix44<T> CreateRotateAroundY(T aAngleInRadians);
		static Matrix44<T> CreateRotateAroundZ(T aAngleInRadians);

		static Matrix44<T> Transpose(const Matrix44<T>& aMatrixToTranspose);
		
		static Matrix44<T> GetFastInverse(const Matrix44<T>& aTransform);

		T myMatrix[16];
		
	private:
	};

	template<class T>
	inline Matrix44<T>::Matrix44()
	{
		myMatrix[0] = 1;
		myMatrix[1] = 0;
		myMatrix[2] = 0;
		myMatrix[3] = 0;
		myMatrix[4] = 0;
		myMatrix[5] = 1;
		myMatrix[6] = 0;
		myMatrix[7] = 0;
		myMatrix[8] = 0;
		myMatrix[9] = 0;
		myMatrix[10] = 1;
		myMatrix[11] = 0;
		myMatrix[12] = 0;
		myMatrix[13] = 0;
		myMatrix[14] = 0;
		myMatrix[15] = 1;
	}

	template<class T>
	inline Matrix44<T>::Matrix44(const Matrix44<T>& aMatrix)
	{
		*this = aMatrix;
	}

	template<class T>
	inline Matrix44<T>::Matrix44(T aMatrixValues[16])
	{
		myMatrix = {aMatrixValues[0], aMatrixValues[1], aMatrixValues[2], aMatrixValues[3],
					aMatrixValues[4], aMatrixValues[5], aMatrixValues[6], aMatrixValues[7],
					aMatrixValues[8], aMatrixValues[9], aMatrixValues[10], aMatrixValues[11],
					aMatrixValues[12], aMatrixValues[13], aMatrixValues[14], aMatrixValues[15]};
	}

	template<class T>
	inline Matrix44<T>::Matrix44(const T & i0, const T & i1, const T & i2, const T & i3, const T & i4, const T & i5, const T & i6, const T & i7, const T & i8, const T & i9, const T & i10, const T & i11, const T & i12, const T & i13, const T & i14, const T & i15)
	{
		myMatrix[0] = i0;
		myMatrix[1] = i1;
		myMatrix[2] = i2;
		myMatrix[3] = i3;
		myMatrix[4] = i4;
		myMatrix[5] = i5;
		myMatrix[6] = i6;
		myMatrix[7] = i7;
		myMatrix[8] = i8;
		myMatrix[9] = i9;
		myMatrix[10] = i10;
		myMatrix[11] = i11;
		myMatrix[12] = i12;
		myMatrix[13] = i13;
		myMatrix[14] = i14;
		myMatrix[15] = i15;
	}

	template<class T>
	inline Matrix44<T> operator+(const Matrix44<T>& aMatrix440, const Matrix44<T>& aMatrix441)
	{
		Matrix44<T> countWith = aMatrix440;
		countWith += aMatrix441;
		return countWith;
	}

	template<class T>
	inline Matrix44<T> operator+=(Matrix44<T>& aMatrix440, const Matrix44<T>& aMatrix441)
	{
		for (int i = 0; i < 16; i++)
		{
			aMatrix440.myMatrix[i] += aMatrix441.myMatrix[i];
		}
		return aMatrix440;
	}
	template<class T>
	inline Matrix44<T> operator-(const Matrix44<T>& aMatrix440, const Matrix44<T>& aMatrix441)
	{
		Matrix44<T> countWith = aMatrix440;
		countWith -= aMatrix441;
		return countWith;
	}
	template<class T>
	inline Matrix44<T> operator-=(Matrix44<T>& aMatrix440, const Matrix44<T>& aMatrix441)
	{
		for (int i = 0; i < 16; i++)
		{
			aMatrix440.myMatrix[i] -= aMatrix441.myMatrix[i];
		}
		return aMatrix440;
	}
	template<class T>
	inline Matrix44<T> operator*(const Matrix44<T>& aMatrix440, const Matrix44<T>& aMatrix441)
	{
		Matrix44<T> matrixToReturn;
		matrixToReturn.myMatrix[0] = (aMatrix440.myMatrix[0] * aMatrix441.myMatrix[0]) + (aMatrix440.myMatrix[1] * aMatrix441.myMatrix[4]) + (aMatrix440.myMatrix[2] * aMatrix441.myMatrix[8]) + (aMatrix440.myMatrix[3] * aMatrix441.myMatrix[12]);
		matrixToReturn.myMatrix[1] = (aMatrix440.myMatrix[0] * aMatrix441.myMatrix[1]) + (aMatrix440.myMatrix[1] * aMatrix441.myMatrix[5]) + (aMatrix440.myMatrix[2] * aMatrix441.myMatrix[9]) + (aMatrix440.myMatrix[3] * aMatrix441.myMatrix[13]);
		matrixToReturn.myMatrix[2] = (aMatrix440.myMatrix[0] * aMatrix441.myMatrix[2]) + (aMatrix440.myMatrix[1] * aMatrix441.myMatrix[6]) + (aMatrix440.myMatrix[2] * aMatrix441.myMatrix[10]) + (aMatrix440.myMatrix[3] * aMatrix441.myMatrix[14]);
		matrixToReturn.myMatrix[3] = (aMatrix440.myMatrix[0] * aMatrix441.myMatrix[3]) + (aMatrix440.myMatrix[1] * aMatrix441.myMatrix[7]) + (aMatrix440.myMatrix[2] * aMatrix441.myMatrix[11]) + (aMatrix440.myMatrix[3] * aMatrix441.myMatrix[15]);
								    
		matrixToReturn.myMatrix[4] = (aMatrix440.myMatrix[4] * aMatrix441.myMatrix[0]) + (aMatrix440.myMatrix[5] * aMatrix441.myMatrix[4]) + (aMatrix440.myMatrix[6] * aMatrix441.myMatrix[8]) + (aMatrix440.myMatrix[7] * aMatrix441.myMatrix[12]);
		matrixToReturn.myMatrix[5] = (aMatrix440.myMatrix[4] * aMatrix441.myMatrix[1]) + (aMatrix440.myMatrix[5] * aMatrix441.myMatrix[5]) + (aMatrix440.myMatrix[6] * aMatrix441.myMatrix[9]) + (aMatrix440.myMatrix[7] * aMatrix441.myMatrix[13]);
		matrixToReturn.myMatrix[6] = (aMatrix440.myMatrix[4] * aMatrix441.myMatrix[2]) + (aMatrix440.myMatrix[5] * aMatrix441.myMatrix[6]) + (aMatrix440.myMatrix[6] * aMatrix441.myMatrix[10]) + (aMatrix440.myMatrix[7] * aMatrix441.myMatrix[14]);
		matrixToReturn.myMatrix[7] = (aMatrix440.myMatrix[4] * aMatrix441.myMatrix[3]) + (aMatrix440.myMatrix[5] * aMatrix441.myMatrix[7]) + (aMatrix440.myMatrix[6] * aMatrix441.myMatrix[11]) + (aMatrix440.myMatrix[7] * aMatrix441.myMatrix[15]);
								    
		matrixToReturn.myMatrix[8] = (aMatrix440.myMatrix[8] * aMatrix441.myMatrix[0]) + (aMatrix440.myMatrix[9] * aMatrix441.myMatrix[4]) + (aMatrix440.myMatrix[10] * aMatrix441.myMatrix[8]) + (aMatrix440.myMatrix[11] * aMatrix441.myMatrix[12]);
		matrixToReturn.myMatrix[9] = (aMatrix440.myMatrix[8] * aMatrix441.myMatrix[1]) + (aMatrix440.myMatrix[9] * aMatrix441.myMatrix[5]) + (aMatrix440.myMatrix[10] * aMatrix441.myMatrix[9]) + (aMatrix440.myMatrix[11] * aMatrix441.myMatrix[13]);
		matrixToReturn.myMatrix[10] = (aMatrix440.myMatrix[8] * aMatrix441.myMatrix[2]) + (aMatrix440.myMatrix[9] * aMatrix441.myMatrix[6]) + (aMatrix440.myMatrix[10] * aMatrix441.myMatrix[10]) + (aMatrix440.myMatrix[11] * aMatrix441.myMatrix[14]);
		matrixToReturn.myMatrix[11] = (aMatrix440.myMatrix[8] * aMatrix441.myMatrix[3]) + (aMatrix440.myMatrix[9] * aMatrix441.myMatrix[7]) + (aMatrix440.myMatrix[10] * aMatrix441.myMatrix[11]) + (aMatrix440.myMatrix[11] * aMatrix441.myMatrix[15]);
	
		matrixToReturn.myMatrix[12] = (aMatrix440.myMatrix[12] * aMatrix441.myMatrix[0]) + (aMatrix440.myMatrix[13] * aMatrix441.myMatrix[4]) + (aMatrix440.myMatrix[14] * aMatrix441.myMatrix[8]) + (aMatrix440.myMatrix[15] * aMatrix441.myMatrix[12]);
		matrixToReturn.myMatrix[13] = (aMatrix440.myMatrix[12] * aMatrix441.myMatrix[1]) + (aMatrix440.myMatrix[13] * aMatrix441.myMatrix[5]) + (aMatrix440.myMatrix[14] * aMatrix441.myMatrix[9]) + (aMatrix440.myMatrix[15] * aMatrix441.myMatrix[13]);
		matrixToReturn.myMatrix[14] = (aMatrix440.myMatrix[12] * aMatrix441.myMatrix[2]) + (aMatrix440.myMatrix[13] * aMatrix441.myMatrix[6]) + (aMatrix440.myMatrix[14] * aMatrix441.myMatrix[10]) + (aMatrix440.myMatrix[15] * aMatrix441.myMatrix[14]);
		matrixToReturn.myMatrix[15] = (aMatrix440.myMatrix[12] * aMatrix441.myMatrix[3]) + (aMatrix440.myMatrix[13] * aMatrix441.myMatrix[7]) + (aMatrix440.myMatrix[14] * aMatrix441.myMatrix[11]) + (aMatrix440.myMatrix[15] * aMatrix441.myMatrix[15]);
	
		return matrixToReturn;
	}
	template<class T>
	inline Matrix44<T> operator*=(Matrix44<T>& aMatrix440, const Matrix44<T>& aMatrix441)
	{
		aMatrix440 = operator*(aMatrix440, aMatrix441);
		return aMatrix440;
	}
	template<class T>
	inline Vector4<T> operator*(const Vector4<T>& aVector4, const Matrix44<T>& aMatrix44)
	{
		Vector4<T> vectorToReturn;

		vectorToReturn.x = (aVector4.x * aMatrix44.myMatrix[0]) + (aVector4.y * aMatrix44.myMatrix[4]) + (aVector4.z * aMatrix44.myMatrix[8]) + (aVector4.w * aMatrix44.myMatrix[12]);
		vectorToReturn.y = (aVector4.x * aMatrix44.myMatrix[1]) + (aVector4.y * aMatrix44.myMatrix[5]) + (aVector4.z * aMatrix44.myMatrix[9]) + (aVector4.w * aMatrix44.myMatrix[13]);
		vectorToReturn.z = (aVector4.x * aMatrix44.myMatrix[2]) + (aVector4.y * aMatrix44.myMatrix[6]) + (aVector4.z * aMatrix44.myMatrix[10]) + (aVector4.w * aMatrix44.myMatrix[14]);
		vectorToReturn.w = (aVector4.x * aMatrix44.myMatrix[3]) + (aVector4.y * aMatrix44.myMatrix[7]) + (aVector4.z * aMatrix44.myMatrix[11]) + (aVector4.w * aMatrix44.myMatrix[15]);

		return vectorToReturn;
	}
	template<class T>
	inline bool operator==(const Matrix44<T>& aMatrix440, const Matrix44<T>& aMatrix441)
	{
		for (int i = 0; i < 16; i++)
		{
			if (aMatrix440.myMatrix[i] != aMatrix441.myMatrix[i])
			{
				return false;
			}
		}
		return true;
	}
	template<class T>
	inline Matrix44<T> & Matrix44<T>::operator=(const Matrix44<T>& aMatrix44)
	{
		for (int i = 0; i < 16; i++)
		{
			myMatrix[i] = aMatrix44.myMatrix[i];
		}
		return *this;
	}
	template<class T>
	inline T & Matrix44<T>::operator[](const unsigned int aIndex)
	{
		assert(aIndex < 16 && aIndex >= 0 && "Index is out of range");
		return myMatrix[aIndex];
	}
	template<class T>
	inline const T & Matrix44<T>::operator[](const unsigned int aIndex) const
	{
		assert(aIndex < 16 && aIndex >= 0 && "Index is out of range");
		return myMatrix[aIndex];
	}
	template<class T>
	inline Matrix44<T> Matrix44<T>::CreateRotateAroundX(T aAngleInRadians)
	{
		T cosi = static_cast<T>(cos(aAngleInRadians));
		T sinu = static_cast<T>(sin(aAngleInRadians));
		Matrix44<T> matrixToReturn;
		matrixToReturn.myMatrix[0] = 1;
		matrixToReturn.myMatrix[1] = 0;
		matrixToReturn.myMatrix[2] = 0;
		matrixToReturn.myMatrix[3] = 0;
		matrixToReturn.myMatrix[4] = 0;
		matrixToReturn.myMatrix[5] = cosi;
		matrixToReturn.myMatrix[6] = sinu;
		matrixToReturn.myMatrix[7] = 0;
		matrixToReturn.myMatrix[8] = 0;
		matrixToReturn.myMatrix[9] = -sinu;
		matrixToReturn.myMatrix[10] = cosi;
		matrixToReturn.myMatrix[11] = 0;
		matrixToReturn.myMatrix[12] = 0;
		matrixToReturn.myMatrix[13] = 0;
		matrixToReturn.myMatrix[14] = 0;
		matrixToReturn.myMatrix[15] = 1;

		return matrixToReturn;
	}
	template<class T>
	inline Matrix44<T> Matrix44<T>::CreateRotateAroundY(T aAngleInRadians)
	{
		T cosi = static_cast<T>(cos(aAngleInRadians));
		T sinu = static_cast<T>(sin(aAngleInRadians));
		Matrix44<T> matrixToReturn;
		matrixToReturn.myMatrix[0] = cosi;
		matrixToReturn.myMatrix[1] = 0;
		matrixToReturn.myMatrix[2] = -sinu;
		matrixToReturn.myMatrix[3] = 0;
		matrixToReturn.myMatrix[4] = 0;
		matrixToReturn.myMatrix[5] = 1;
		matrixToReturn.myMatrix[6] = 0;
		matrixToReturn.myMatrix[7] = 0;
		matrixToReturn.myMatrix[8] = sinu;
		matrixToReturn.myMatrix[9] = 0;
		matrixToReturn.myMatrix[10] = cosi;
		matrixToReturn.myMatrix[11] = 0;
		matrixToReturn.myMatrix[12] = 0;
		matrixToReturn.myMatrix[13] = 0;
		matrixToReturn.myMatrix[14] = 0;
		matrixToReturn.myMatrix[15] = 1;

		return matrixToReturn;
	}
	template<class T>
	inline Matrix44<T> Matrix44<T>::CreateRotateAroundZ(T aAngleInRadians)
	{
		T cosi = static_cast<T>(cos(aAngleInRadians));
		T sinu = static_cast<T>(sin(aAngleInRadians));
		Matrix44<T> matrixToReturn;
		matrixToReturn.myMatrix[0] = cosi;
		matrixToReturn.myMatrix[1] = sinu;
		matrixToReturn.myMatrix[2] = 0;
		matrixToReturn.myMatrix[3] = 0;
		matrixToReturn.myMatrix[4] = -sinu;
		matrixToReturn.myMatrix[5] = cosi;
		matrixToReturn.myMatrix[6] = 0;
		matrixToReturn.myMatrix[7] = 0;
		matrixToReturn.myMatrix[8] = 0;
		matrixToReturn.myMatrix[9] = 0;
		matrixToReturn.myMatrix[10] = 1;
		matrixToReturn.myMatrix[11] = 0;
		matrixToReturn.myMatrix[12] = 0;
		matrixToReturn.myMatrix[13] = 0;
		matrixToReturn.myMatrix[14] = 0;
		matrixToReturn.myMatrix[15] = 1;
		return matrixToReturn;
	}
	template<class T>
	inline Matrix44<T> Matrix44<T>::Transpose(const Matrix44<T>& aMatrixToTranspose)
	{
		Matrix44<T> matrixToReturn;
		matrixToReturn.myMatrix[0] = aMatrixToTranspose[0];
		matrixToReturn.myMatrix[1] = aMatrixToTranspose[4];
		matrixToReturn.myMatrix[2] = aMatrixToTranspose[8];
		matrixToReturn.myMatrix[3] = aMatrixToTranspose[12];
		matrixToReturn.myMatrix[4] = aMatrixToTranspose[1];
		matrixToReturn.myMatrix[5] = aMatrixToTranspose[5];
		matrixToReturn.myMatrix[6] = aMatrixToTranspose[9];
		matrixToReturn.myMatrix[7] = aMatrixToTranspose[13];
		matrixToReturn.myMatrix[8] = aMatrixToTranspose[2];
		matrixToReturn.myMatrix[9] = aMatrixToTranspose[6];
		matrixToReturn.myMatrix[10] = aMatrixToTranspose[10];
		matrixToReturn.myMatrix[11] = aMatrixToTranspose[14];
		matrixToReturn.myMatrix[12] = aMatrixToTranspose[3];
		matrixToReturn.myMatrix[13] = aMatrixToTranspose[7];
		matrixToReturn.myMatrix[14] = aMatrixToTranspose[11];
		matrixToReturn.myMatrix[15] = aMatrixToTranspose[15];
		return matrixToReturn;
	}
	template<class T>
	inline Matrix44<T> Matrix44<T>::GetFastInverse(const Matrix44<T>& aTransform)
	{
		return Matrix44<T>({ aTransform[0], aTransform[4], aTransform[8], static_cast<T>(0),
			aTransform[1], aTransform[5], aTransform[9], static_cast<T>(0),
			aTransform[2], aTransform[6], aTransform[10], static_cast<T>(0),
			(-aTransform[12] * aTransform[0] - aTransform[13] * aTransform[1] - aTransform[14] * aTransform[2]), (-aTransform[12] * aTransform[4] - aTransform[13] * aTransform[5] - aTransform[14] * aTransform[6]), (-aTransform[12] * aTransform[8] - aTransform[13] * aTransform[9] - aTransform[14] * aTransform[10]), static_cast<T>(1) });

	}
}

#pragma once
#include "Vector.h"
namespace CommonUtilities
{
	template <typename T>
	class Plane
	{
	public:
		Plane();
		Plane(const Vector3<T> &aPoint0, const Vector3<T> &aPoint1, const Vector3<T> & aPoint2);
		Plane(const Vector3<T> &aPoint0, const Vector3<T> &aNormal);

		void InitWith3Points(const Vector3<T> &aPoint0, const Vector3<T> &aPoint1, const Vector3<T> & aPoint2);
		void InitWithPointAndNormal(const Vector3<T> &aPoint0, const Vector3<T> &aNormal);

		bool Inside(const Vector3<T> &aPosition) const;

	private:
		Vector3<T> myPoint;
		Vector3<T> myNormal;
	};
	template<typename T>
	inline Plane<T>::Plane()
	{
	}
	template<typename T>
	inline Plane<T>::Plane(const Vector3<T>& aPoint0, const Vector3<T>& aPoint1, const Vector3<T>& aPoint2)
	{
		myPoint = aPoint0;
		Vector3<T> dirFrom0To1 = aPoint1 - aPoint0;
		myNormal = dirFrom0To1.Cross((aPoint2 - aPoint0));
		myNormal.Normalize();
	}
	template<typename T>
	inline Plane<T>::Plane(const Vector3<T>& aPoint0, const Vector3<T>& aNormal)
	{
		myPoint = aPoint0;
		myNormal = aNormal;
		myNormal.Normalize();
	}
	template<typename T>
	inline void Plane<T>::InitWith3Points(const Vector3<T>& aPoint0, const Vector3<T>& aPoint1, const Vector3<T>& aPoint2)
	{
		myPoint = aPoint0;
		Vector3<T> dirFrom0To1 = aPoint1 - aPoint0;
		myNormal = dirFrom0To1.Cross((aPoint2 - aPoint0));
		myNormal.Normalize();
	}
	template<typename T>
	inline void Plane<T>::InitWithPointAndNormal(const Vector3<T>& aPoint0, const Vector3<T>& aNormal)
	{
		myPoint = aPoint0;
		myNormal = aNormal;
		myNormal.Normalize();
	}
	template<typename T>
	inline bool Plane<T>::Inside(const Vector3<T>& aPosition) const
	{
		if (myNormal.Dot((aPosition - myPoint)) < 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
}
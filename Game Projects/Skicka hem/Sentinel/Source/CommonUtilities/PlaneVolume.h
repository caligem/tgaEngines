#pragma once
#include "Vector.h"
#include <vector>
#include "Plane.h"
namespace CommonUtilities
{
	template <typename T>
	class PlaneVolume
	{
	public:
		PlaneVolume(const std::vector<Plane<T>> &aPlaneList);
		void AddPlane(const Plane<T> &aPlane);
		bool Inside(const Vector3<T> &aPosition) const;

	private:
		std::vector<Plane<T>> myPlanes;
	};
	template<typename T>
	inline PlaneVolume<T>::PlaneVolume(const std::vector<Plane<T>>& aPlaneList)
	{
		myPlanes = aPlaneList;
	}
	template<typename T>
	inline void PlaneVolume<T>::AddPlane(const Plane<T>& aPlane)
	{
		myPlanes.push_back(aPlane);
	}
	template<typename T>
	inline bool PlaneVolume<T>::Inside(const Vector3<T>& aPosition) const
	{
		for (unsigned int i = 0; i < myPlanes.size(); i++)
		{
			if (myPlanes[i].Inside(aPosition) == false)
			{
				return false;
			}
		}
		return true;
	}
}

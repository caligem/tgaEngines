#include "Timer.h"
#include <assert.h>

using namespace CommonUtilities;

Timer *Timer::ourInstance;
bool CommonUtilities::Timer::Create()
{
	assert(ourInstance == nullptr && "PatternMaster already created");
	ourInstance = new Timer();
	if (ourInstance == nullptr)
	{
		return(false);
	}

	ourInstance->myTimerMultiplier = 1.f;

	ourInstance->myTotalTime = 0;
	ourInstance->myPreviousFrameTime = std::chrono::high_resolution_clock::now();
	return(true);
}
bool CommonUtilities::Timer::Destroy()
{
	delete ourInstance;
	ourInstance = nullptr;
	return true;
}
void Timer::Update()
{
	ourInstance->myCurrentFrameTime = std::chrono::high_resolution_clock::now();

	auto floatDur = ourInstance->myCurrentFrameTime - ourInstance->myPreviousFrameTime;
	auto sec = std::chrono::duration_cast<std::chrono::duration<float>>(floatDur);

	ourInstance->myDeltaTime = sec.count();
	if (ourInstance->myDeltaTime > 0.1f)
	{
		ourInstance->myDeltaTime = 0.1f;
	}
	ourInstance->myTotalTime += ourInstance->myDeltaTime;

	ourInstance->myPreviousFrameTime = std::chrono::high_resolution_clock::now();
}
float Timer::GetDeltaTime()
{
	return ourInstance->myDeltaTime * ourInstance->myTimerMultiplier;
}
double Timer::GetTotalTime()
{
	return ourInstance->myTotalTime;
}
float CommonUtilities::Timer::GetRawDeltaTime()
{
	return ourInstance->myDeltaTime;
}
void CommonUtilities::Timer::SetDeltaTimeMultiplier(float aMultiplier)
{
	ourInstance->myTimerMultiplier = aMultiplier;
}
Timer::Timer()
{

}

CommonUtilities::Timer::~Timer()
{
}

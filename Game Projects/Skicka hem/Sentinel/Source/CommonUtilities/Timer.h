#pragma once
#include <chrono>

namespace CommonUtilities
{
	class Timer
	{
	public:
		static bool Create();
		static bool Destroy();
		static void Update();

		static float GetDeltaTime();
		static double GetTotalTime();
		static float GetRawDeltaTime();

		static void SetDeltaTimeMultiplier(float aMultiplier);
	private:
		Timer();
		~Timer();
		static Timer *ourInstance;

		float myDeltaTime;
		double myTotalTime;
		std::chrono::high_resolution_clock::time_point myCurrentFrameTime;
		std::chrono::high_resolution_clock::time_point myPreviousFrameTime;

		float myTimerMultiplier;
	};
}


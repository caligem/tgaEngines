#pragma once
#include <math.h>
#include "Vector3.h"
namespace CommonUtilities
{ 
	template <class T>
	class Vector2
	{
	public:
		T x;
		T y;

		Vector2<T>();

		Vector2<T>(const T& aX, const T& aY);
		Vector2<T>(const Vector3<T> &aVec);

		Vector2<T>(const Vector2<T>& aVector) = default;

		Vector2<T>& operator=(const Vector2<T>& aVector2) = default;

		~Vector2<T>() = default;

		T Length2() const;

		T Length() const;

		Vector2<T> GetNormalized() const;

		void Normalize();

		T Dot(const Vector2<T>& aVector) const;

		Vector2<T> Cross(const Vector2<T>& aVector) const;
	};
	typedef Vector2<float> Vector2f;

	template <class T> Vector2<T>::Vector2()
	{
		x = 0;
		y = 0;
	}

	template <class T> Vector2<T>::Vector2(const T& aX, const T& aY)
	{
		x = aX;
		y = aY;
	}

	template<class T>
	inline Vector2<T>::Vector2(const Vector3<T> &aVec )
	{
		x = aVec.x;
		y = aVec.y;
	}

	template <class T> T Vector2<T>::Length2() const
	{
		return (x*x) + (y*y);
	}

	template <class T> T Vector2<T>::Length() const
	{
		return sqrt((x*x) + (y*y));
	}

	template <class T> Vector2<T> Vector2<T>::GetNormalized() const
	{
		return (*this) * (1 / Length());
	}

	template <class T> void Vector2<T>::Normalize()
	{
		(*this) *= (1 / Length());
	}

	template <class T> T Vector2<T>::Dot(const Vector2<T>& aVector) const
	{
		return (x * aVector.x) + (y * aVector.y);
	}

	template <class T> Vector2<T> Vector2<T>::Cross(const Vector2<T>& aVector) const
	{
		Vector2<T> crossVector;
		crossVector.x = (y * aVector.z) - (z * aVector.y);
		crossVector.y = (z * aVector.x) - (x * aVector.z);
		return crossVector;
	}

	template <class T> Vector2<T> operator+(const Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		Vector2<T> countWith = aVector0;
		countWith.x += aVector1.x;
		countWith.y += aVector1.y;
		return countWith;
	}

	template <class T> Vector2<T> operator-(const Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		Vector2<T> countWith = aVector0;
		countWith.x -= aVector1.x;
		countWith.y -= aVector1.y;
		return countWith;
	}

	template <class T> Vector2<T> operator*(const Vector2<T>& aVector, const T& aScalar)
	{
		Vector2<T> countWith = aVector;
		countWith.x *= aScalar;
		countWith.y *= aScalar;
		return countWith;
	}

	template <class T> Vector2<T> operator*(const T& aScalar, const Vector2<T>& aVector)
	{
		Vector2<T> countWith = aVector;
		countWith.x *= aScalar;
		countWith.y *= aScalar;
		return countWith;
	}

	template <class T> Vector2<T> operator/(const Vector2<T>& aVector, const T& aScalar)
	{
		Vector2<T> countWith = aVector;
		countWith.x /= aScalar;
		countWith.y /= aScalar;
		return countWith;
	}

	template <class T> Vector2<T> operator+=(Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		aVector0.x += aVector1.x;
		aVector0.y += aVector1.y;
		return aVector0;
	}

	template <class T> Vector2<T> operator-=(Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		aVector0.x -= aVector1.x;
		aVector0.y -= aVector1.y;
		return aVector0;
	}

	template <class T> Vector2<T> operator*=(Vector2<T>& aVector, const T& aScalar)
	{
		aVector.x *= aScalar;
		aVector.y *= aScalar;
		return aVector;
	}

	template <class T> Vector2<T> operator/=(Vector2<T>& aVector, const T& aScalar)
	{
		aVector.x /= aScalar;
		aVector.y /= aScalar;
		return aVector;
	}

	template <class T> Vector2<T> operator/(const Vector2<T>& aVector, const Vector2<T>& aVector1)
	{
		Vector2<T> tempVector = aVector;
		tempVector.x /= aVector1.x;
		tempVector.y /= aVector1.y;
		return tempVector;
	}

	template <class T> bool operator==(Vector2<T>& aVector, Vector2<T>& aVector1)
	{
		return aVector.x == aVector1.x && aVector.y == aVector1.y;
	}

	template <class T> bool operator!=(Vector2<T>& aVector, Vector2<T>& aVector1)
	{
		return !(aVector == aVector1);
	}

}

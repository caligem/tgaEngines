#pragma once
#include <math.h>
#include "Vector4.h"
namespace CommonUtilities
{

	template <class T>
	class Vector3
	{
	public:
		T x;
		T y;
		T z;

		Vector3<T>();
		Vector3<T>(const Vector4<T> &aVec);
		Vector3<T>(const T& aX, const T& aY, const T& aZ);

		Vector3<T>(const Vector3<T>& aVector) = default;

		Vector3<T>& operator=(const Vector3<T>& aVector3) = default;

		~Vector3<T>() = default;

		T Length2() const;

		T Length() const;

		Vector3<T> GetNormalized() const;

		void Normalize();

		T Dot(const Vector3<T>& aVector) const;

		Vector3<T> Cross(const Vector3<T>& aVector) const;
	};
	typedef Vector3<float> Vector3f;

	template <class T> Vector3<T>::Vector3()
	{
		x = 0;
		y = 0;
		z = 0;
	}

	template<class T>
	inline Vector3<T>::Vector3(const Vector4<T>& aVec)
	{
		x = aVec.x;
		y = aVec.y;
		z = aVec.z;
	}

	template <class T> Vector3<T>::Vector3(const T& aX, const T& aY, const T& aZ)
	{
		x = aX;
		y = aY;
		z = aZ;
	}

	template <class T> T Vector3<T>::Length2() const
	{
		return (x*x) + (y*y) + (z*z);
	}

	template <class T> T Vector3<T>::Length() const
	{
		return sqrt((x*x) + (y*y) + (z*z));
	}

	template <class T> Vector3<T> Vector3<T>::GetNormalized() const
	{
		return (*this) * (1 / Length());
	}

	template <class T> void Vector3<T>::Normalize()
	{
		(*this) *= (1 / Length());
	}

	template <class T> T Vector3<T>::Dot(const Vector3<T>& aVector) const
	{
		return (x * aVector.x) + (y * aVector.y) + (z * aVector.z);
	}

	template <class T> Vector3<T> Vector3<T>::Cross(const Vector3<T>& aVector) const
	{
		Vector3<T> crossVector;
		crossVector.x = (y * aVector.z) - (z * aVector.y);
		crossVector.y = (z * aVector.x) - (x * aVector.z);
		crossVector.z = (x * aVector.y) - (y * aVector.x);
		return crossVector;
	}

	template <class T> Vector3<T> operator+(const Vector3<T>& aVector0, const Vector3<T>& aVector1)
	{
		Vector3<T> countWith = aVector0;
		countWith.x += aVector1.x;
		countWith.y += aVector1.y;
		countWith.z += aVector1.z;
		return countWith;
	}

	template <class T> Vector3<T> operator-(const Vector3<T>& aVector0, const Vector3<T>& aVector1)
	{
		Vector3<T> countWith = aVector0;
		countWith.x -= aVector1.x;
		countWith.y -= aVector1.y;
		countWith.z -= aVector1.z;
		return countWith;
	}

	template <class T> Vector3<T> operator*(const Vector3<T>& aVector, const T& aScalar)
	{
		Vector3<T> countWith = aVector;
		countWith.x *= aScalar;
		countWith.y *= aScalar;
		countWith.z *= aScalar;
		return countWith;
	}

	template <class T> Vector3<T> operator*(const T& aScalar, const Vector3<T>& aVector)
	{
		Vector3<T> countWith = aVector;
		countWith.x *= aScalar;
		countWith.y *= aScalar;
		countWith.z *= aScalar;
		return countWith;
	}

	template <class T> Vector3<T> operator/(const Vector3<T>& aVector, const T& aScalar)
	{
		Vector3<T> countWith = aVector;
		countWith.x /= aScalar;
		countWith.y /= aScalar;
		countWith.z /= aScalar;
		return countWith;
	}

	template <class T> Vector3<T> operator+=(Vector3<T>& aVector0, const Vector3<T>& aVector1)
	{
		aVector0.x += aVector1.x;
		aVector0.y += aVector1.y;
		aVector0.z += aVector1.z;
		return aVector0;
	}

	template <class T> Vector3<T> operator-=(Vector3<T>& aVector0, const Vector3<T>& aVector1)
	{
		aVector0.x -= aVector1.x;
		aVector0.y -= aVector1.y;
		aVector0.z -= aVector1.z;
		return aVector0;
	}

	template <class T> Vector3<T> operator*=(Vector3<T>& aVector, const T& aScalar)
	{
		aVector.x *= aScalar;
		aVector.y *= aScalar;
		aVector.z *= aScalar;
		return aVector;
	}

	template <class T> Vector3<T> operator/=(Vector3<T>& aVector, const T& aScalar)
	{
		aVector.x /= aScalar;
		aVector.y /= aScalar;
		aVector.z /= aScalar;
		return aVector;
	}

}

#include "XBOXController.h"
#include "Timer.h"
XBOXController::XBOXController(GamePadIndex player)
{
	myPlayerIndex = player;
	myCurrentState.Reset();
	myPreviousState.Reset();
	myIsVibrating = false;
	myVibrationDuration = -1.0f;
	myVibrationTimer = 0.0f;
	myLMotorVibration = 0.0f;
	myRMotorVibration = 0.0f;
}

XBOXController::~XBOXController(void)
{
	// dont vibrate if exiting
	if (Is_connected())
	{
		Vibrate(0.0f, 0.0f);
	}
}

bool XBOXController::Is_connected()
{
	memset(&myControllerState, 0, sizeof(XINPUT_STATE));
	DWORD result = XInputGetState((int)myPlayerIndex, &myControllerState);

	if (result == ERROR_SUCCESS)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void XBOXController::SetVibration(float aDuration, float aLeftmotor, float aRightmotor)
{
	myIsVibrating = true;
	myVibrationDuration = aDuration;
	myLMotorVibration = aLeftmotor;
	myRMotorVibration = aRightmotor;
}

void XBOXController::Vibrate(float aLeftmotor, float aRightmotor)
{
	XINPUT_VIBRATION Vibration;

	memset(&Vibration, 0, sizeof(XINPUT_VIBRATION));

	int leftVib = (int)(aLeftmotor*65535.0f);
	int rightVib = (int)(aRightmotor*65535.0f);

	// Set the Vibration Values
	Vibration.wLeftMotorSpeed = leftVib;
	Vibration.wRightMotorSpeed = rightVib;
	// Vibrate the controller
	if (Is_connected())
	{
		XInputSetState((int)myPlayerIndex, &Vibration);
	}
}

void XBOXController::Update()
{
	myPreviousState = myCurrentState;
	myCurrentState.Reset();
	// The values of the Left and Right Triggers go from 0 to 255. We just convert them to 0.0f=>1.0f
	if (myControllerState.Gamepad.bRightTrigger && myControllerState.Gamepad.bRightTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
	{
		myCurrentState.myRightTrigger = myControllerState.Gamepad.bRightTrigger / 255.0f;
	}

	if (myControllerState.Gamepad.bLeftTrigger && myControllerState.Gamepad.bLeftTrigger > XINPUT_GAMEPAD_TRIGGER_THRESHOLD)
	{
		myCurrentState.myLeftTrigger = myControllerState.Gamepad.bLeftTrigger / 255.0f;
	}

	// Get the Buttons
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_A) myCurrentState.myButtons[static_cast<int>(GamePadButton::A)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_B) myCurrentState.myButtons[static_cast<int>(GamePadButton::B)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_X) myCurrentState.myButtons[static_cast<int>(GamePadButton::X)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_Y) myCurrentState.myButtons[static_cast<int>(GamePadButton::Y)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN) myCurrentState.myButtons[static_cast<int>(GamePadButton::DPAD_DOWN)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP) myCurrentState.myButtons[static_cast<int>(GamePadButton::DPAD_UP)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT) myCurrentState.myButtons[static_cast<int>(GamePadButton::DPAD_LEFT)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT) myCurrentState.myButtons[static_cast<int>(GamePadButton::DPAD_RIGHT)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_START) myCurrentState.myButtons[static_cast<int>(GamePadButton::START)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_BACK) myCurrentState.myButtons[static_cast<int>(GamePadButton::BACK)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB) myCurrentState.myButtons[static_cast<int>(GamePadButton::LEFT_THUMB)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB) myCurrentState.myButtons[static_cast<int>(GamePadButton::RIGHT_THUMB)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER) myCurrentState.myButtons[static_cast<int>(GamePadButton::LEFT_SHOULDER)] = true;
	if (myControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER) myCurrentState.myButtons[static_cast<int>(GamePadButton::RIGHT_SHOULDER)] = true;

	if ((myControllerState.Gamepad.sThumbLX < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE &&
		myControllerState.Gamepad.sThumbLX > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE) &&
		(myControllerState.Gamepad.sThumbLY < XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE &&
			myControllerState.Gamepad.sThumbLY > -XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE))
	{
		myControllerState.Gamepad.sThumbLX = 0;
		myControllerState.Gamepad.sThumbLY = 0;
	}

	// Check left thumbStick
	float leftThumbY = myControllerState.Gamepad.sThumbLY;
	if (leftThumbY)
	{
		myCurrentState.myLeftThumbstick.y = leftThumbY;
	}
	float leftThumbX = myControllerState.Gamepad.sThumbLX;
	if (leftThumbX)
	{
		myCurrentState.myLeftThumbstick.x = leftThumbX;
	}
	// Check right thumbStick
	float rightThumbY = myControllerState.Gamepad.sThumbRY;
	if (rightThumbY)
	{
		myCurrentState.myRightThumbstick.y = rightThumbY;
	}
	float rightThumbX = myControllerState.Gamepad.sThumbRX;
	if (rightThumbX)
	{
		myCurrentState.myRightThumbstick.x = rightThumbX;
	}
	UpdateVibration();
}

void XBOXController::UpdateVibration()
{
	if (myIsVibrating == true)
	{
		myVibrationTimer += CommonUtilities::Timer::GetDeltaTime();

		Vibrate(1.f, 1.f);
	}
	if (myVibrationTimer >= myVibrationDuration)
	{
		Vibrate(.0f, .0f);
		myVibrationTimer = 0.0f;
		myIsVibrating = false;
	}
}

#pragma once
#define WIN32_LEAN_AND_MEAN // We don't want the extra stuff like MFC and such
#include <windows.h>
#include <XInput.h>
#pragma comment(lib, "XInput.lib")
#include "Vector.h"
enum class GamePadButton
{
	DPAD_UP = 0,
	DPAD_DOWN = 1,
	DPAD_LEFT = 2,
	DPAD_RIGHT = 3,
	START = 4,
	BACK = 5,
	LEFT_THUMB = 6,
	RIGHT_THUMB = 7,
	LEFT_SHOULDER = 8,
	RIGHT_SHOULDER = 9,
	A = 10,
	B = 11,
	X = 12,
	Y = 13,
	GamePadButton_Max = 14
};

enum class GamePadIndex
{
	GamePadIndex_One = 0,
	GamePadIndex_Two = 1,
	GamePadIndex_Three = 2,
	GamePadIndex_Four = 3
};
class XBOXController
{
private:
struct GamePadState
{
	bool myButtons[static_cast<int>(GamePadButton::GamePadButton_Max)];
	CommonUtilities::Vector2f myLeftThumbstick;               
	CommonUtilities::Vector2f myRightThumbstick;
	float myLeftTrigger;
	float myRightTrigger;

	void Reset()
	{
		for (int i = 0; i < static_cast<int>(GamePadButton::GamePadButton_Max); ++i)
		{
			myButtons[i] = false;
		}
		myLeftThumbstick.x = 0.0f;
		myLeftThumbstick.y = 0.0f;
		myRightThumbstick.x = 0.0f;
		myRightThumbstick.y = 0.0f;
		myLeftTrigger = 0.0f;
		myRightTrigger = 0.0f;
	}
};
public:
	XBOXController(GamePadIndex player);
	virtual ~XBOXController(void);

	bool Is_connected();
	void SetVibration(float aDuration, float aLeftmotor = 0.0f, float aRightmotor = 0.0f);
	void Update();

	GamePadState myCurrentState;
	GamePadState myPreviousState;
private:
	void Vibrate(float aLeftmotor = 0.0f, float aRightmotor = 0.0f);
	void UpdateVibration();
	bool myIsVibrating;
	float myVibrationDuration;
	float myVibrationTimer;
	float myLMotorVibration;
	float myRMotorVibration;
	XINPUT_STATE myControllerState;
	GamePadIndex myPlayerIndex;
};


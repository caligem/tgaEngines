#include "XBOXControllerManager.h"



XBOXControllerManager::XBOXControllerManager()
{
	myMaxAmountOfControllers = 4;
}


XBOXControllerManager::~XBOXControllerManager()
{

}

void XBOXControllerManager::Update()
{
	for (int i = 0; i < myMaxAmountOfControllers; i++)
	{
		if (myControllers[i].Is_connected())
		{
			myControllers[i].Update();
		}
	}
}

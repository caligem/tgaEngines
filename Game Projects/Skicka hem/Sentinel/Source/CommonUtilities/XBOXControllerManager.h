#pragma once
#include "XBOXController.h"
#include <vector>
class XBOXControllerManager
{
public:
	XBOXControllerManager();
	~XBOXControllerManager();

	void Update();
	XBOXController myControllers[4] = {
		XBOXController(GamePadIndex::GamePadIndex_One),
		XBOXController(GamePadIndex::GamePadIndex_Two),
		XBOXController(GamePadIndex::GamePadIndex_Three),
		XBOXController(GamePadIndex::GamePadIndex_Four)
	};

private:
	int myMaxAmountOfControllers;
};


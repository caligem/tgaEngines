#include "stdafx.h"
#include "Animation.h"
#include "tga2d\sprite\sprite.h"
#include "cit\utility.h"
#include "Timer.h"
#include "TileSize.h"
#include "JsonWrapper.h"

Animation::Animation()
{
	mySprite = nullptr;
	SetLayer(1);
}
Animation::~Animation()
{
	cit::SafeDelete(mySprite);
}
void Animation::Init(const char * aAnimationKey)
{
	json animationData = LoadAnimationData();
	mySprite = new Tga2D::CSprite(animationData[aAnimationKey]["spriteSource"].get<std::string>().c_str());
	mySprite->SetPivot({ 0.5f, 0.5f });
	myFrameSize = { animationData[aAnimationKey]["frameSizeInPixelsX"].get<float>() / mySprite->GetImageSize().x, animationData[aAnimationKey]["frameSizeInPixelsY"].get<float>() / mySprite->GetImageSize().y };
	mySprite->SetSizeRelativeToImage({ TILE_MULTIPLIER * (animationData[aAnimationKey]["frameSizeInPixelsX"].get<float>() / mySprite->GetImageSize().x), TILE_MULTIPLIER * (animationData[aAnimationKey]["frameSizeInPixelsY"].get<float>() / mySprite->GetImageSize().y) });
	myTimeBetweenFrameTimer.Set(animationData[aAnimationKey]["timeBetweenFrame"].get<float>(), cit::countdown::type::autoreset, [&] {SwapFrame(); });
	myTimeBetweenFrameTimer.Start();
	if (animationData[aAnimationKey]["type"].get<std::string>() == "oneshot")
	{
		myType = Type::Oneshot;
	}
	else if (animationData[aAnimationKey]["type"].get<std::string>() == "static")
	{
		myType = Type::Static;
	}
	else
	{
		myType = Type::Continuous;
	}
	myPadding = animationData[aAnimationKey]["padding"].get<float>() / mySprite->GetImageSize().x;
	myAmountOfFrames = animationData[aAnimationKey]["amountOfFrames"].get<int>();
	myCurrentFrame = 0;
	myFrames.Init(static_cast<unsigned short>(myAmountOfFrames));
	CalculateFrameOffset();
	myIsPaused = false;
	myShouldDrawThisFrame = true;
}

void Animation::Render()
{
	if ((myType == Type::Continuous || myType == Type::Oneshot) && myIsPaused == false)
	{
		myTimeBetweenFrameTimer.Update(CommonUtilities::Timer::GetDeltaTime());
	}
	mySprite->SetPosition({ myPosition.x, myPosition.y });
	mySprite->SetTextureRect(myFrames[myCurrentFrame].myXOffset, myFrames[myCurrentFrame].myYOffset, myFrames[myCurrentFrame].myXOffset + myFrameSize.x, myFrames[myCurrentFrame].myYOffset + myFrameSize.y);
	if (myShouldDrawThisFrame)
	{
		mySprite->Render();
	}
}

void Animation::RawRender()
{
	if (myType == Type::Continuous || myType == Type::Oneshot)
	{
		myTimeBetweenFrameTimer.Update(CommonUtilities::Timer::GetRawDeltaTime());
	}
	mySprite->SetPosition({ myPosition.x, myPosition.y });
	mySprite->SetTextureRect(myFrames[myCurrentFrame].myXOffset, myFrames[myCurrentFrame].myYOffset, myFrames[myCurrentFrame].myXOffset + myFrameSize.x, myFrames[myCurrentFrame].myYOffset + myFrameSize.y);
	if (myShouldDrawThisFrame)
	{
		mySprite->Render();
	}
}

const CommonUtilities::Vector2<unsigned int> Animation::GetImagesSizePixel() const
{
	return {static_cast<unsigned int>(mySprite->GetImageSize().x*myFrameSize.x), static_cast<unsigned int>(mySprite->GetImageSize().y * myFrameSize.y)};
}

void Animation::SetSizeRelativeToScreen(const CommonUtilities::Vector2f & aSize)
{
	if (mySprite->GetSize().x < 0.0f)
	{
		mySprite->SetSizeRelativeToScreen({ aSize.x * -1.0f , aSize.y });
	}
	else
	{
		mySprite->SetSizeRelativeToScreen({ aSize.x , aSize.y });
	}
}


void Animation::SetColor(const CommonUtilities::Vector4f & aColor)
{
	mySprite->SetColor({aColor.x, aColor.y, aColor.z, aColor.w});
}

void Animation::SetType(Type aValueToSet)
{
	myType = aValueToSet;
}

void Animation::SetCurrentFrame(int aFrameIndex)
{
	myCurrentFrame = static_cast<unsigned short>(aFrameIndex);
}

void Animation::SwapFrame()
{
	if (myCurrentFrame + 1 >= myAmountOfFrames)
	{
		if (myType == Type::Oneshot)
		{
			return;
		}
	}
	myCurrentFrame++;
	if (myCurrentFrame >= myAmountOfFrames)
	{
		myCurrentFrame = 0;
	}
}

void Animation::Rotate(float aRadius)
{
	mySprite->SetRotation(aRadius);
}

void Animation::SetInversedX(bool aValue)
{
	if ((aValue == true && mySprite->GetSize().x > 0.0f) || (aValue == false && mySprite->GetSize().x < 0.0f))
	{
		mySprite->SetSizeRelativeToScreen({ mySprite->GetSize().x * -1.0f, mySprite->GetSize().y });
	}
}

bool Animation::GetIsFlipped() const
{
	return mySprite->GetSize().x < 0.0f;
}

void Animation::SetRotation(const float aRotation)
{
	mySprite->SetRotation(aRotation);
}

float Animation::GetTotalTime() const
{
	return myFrames.Size() * myTimeBetweenFrameTimer.GetMaxTime();
}

void Animation::Pause()
{
	myIsPaused = true;
}

void Animation::Unpause()
{
	myIsPaused = false;
}

void Animation::SetTimerBetweenFrames(float aTimer)
{
	myTimeBetweenFrameTimer.Set(aTimer, cit::countdown::type::autoreset, [&] {SwapFrame(); });
	myTimeBetweenFrameTimer.Start();
}

int Animation::GetAmountOfFrames() const
{
	return myAmountOfFrames;
}

unsigned short Animation::GetCurrentFrame() const
{
	return myCurrentFrame;
}

void Animation::ReplaceSprite(std::string & aSource)
{
	Tga2D::CSprite *replaceSprite = new Tga2D::CSprite(aSource.c_str());
	replaceSprite->SetPivot(mySprite->GetPivot());
	replaceSprite->SetSizeRelativeToScreen(mySprite->GetSize());
	cit::SafeDelete(mySprite);
	mySprite = replaceSprite;
}

void Animation::CalculateFrameOffset()
{
	float myCurrOffsetX = 0.0f;
	float myCurrOffsetY = 0.0f;
	myFrames.Add(FrameData());
	myFrames[0].myXOffset = myCurrOffsetX;
	myFrames[0].myYOffset = myCurrOffsetY;
	for (unsigned short i = 1; i < myAmountOfFrames; i++)
	{
		myCurrOffsetX += myFrameSize.x;
		myCurrOffsetX += myPadding;
		if (myCurrOffsetX >= 1.0f)
		{
			myCurrOffsetX = 0.0f;
			myCurrOffsetY += myFrameSize.y;
			myCurrOffsetY += myPadding;
		}

		myFrames.Add(FrameData());
		myFrames[i].myXOffset = myCurrOffsetX;
		myFrames[i].myYOffset = myCurrOffsetY;
		
	}
}

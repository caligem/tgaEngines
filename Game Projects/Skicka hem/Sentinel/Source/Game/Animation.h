#pragma once
#include "Drawable.h"
#include <cit\countdown.h>
#include "GrowingArray.h"

class Animation : public Drawable
{
public:
	enum class Type
	{
		Oneshot,
		Continuous,
		Static
	};
	Animation();
	~Animation() override;
	void Init(const char* aAnimationKey) override;
	void Render() override;
	void RawRender();

	const CommonUtilities::Vector2<unsigned int> GetImagesSizePixel() const override;
	void SetSizeRelativeToScreen(const CommonUtilities::Vector2f & aSize) override;
	void SetColor(const CommonUtilities::Vector4f & aColor);
	void SetType(Type aValueToSet);
	void SetCurrentFrame(int aFrameIndex);
	void SwapFrame();
	void Rotate(float aRadius);
	void SetInversedX(bool aValue);
	bool GetIsFlipped() const override;
	void SetRotation(const float aRotation) override;
	float GetTotalTime() const;
	void Pause();
	void Unpause();
	void SetTimerBetweenFrames(float aTimer);
	int GetAmountOfFrames() const;
	unsigned short GetCurrentFrame() const;

	void ReplaceSprite(std::string &aSource);
private:
	struct FrameData
	{
		float myXOffset;
		float myYOffset;
	};
	Type myType;
	CommonUtilities::GrowingArray<FrameData> myFrames;
	void CalculateFrameOffset();
	Tga2D::CSprite *mySprite;

	CommonUtilities::Vector2f myFrameSize;
	CommonUtilities::Vector2f myCurrentTextureRectOffset;
	cit::countdown myTimeBetweenFrameTimer;

	float myPadding;
	int myAmountOfFrames;
	unsigned short myCurrentFrame;
	bool myIsPaused;
};
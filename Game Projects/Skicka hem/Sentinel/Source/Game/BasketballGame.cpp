#include "stdafx.h"
#include "BasketballGame.h"
#include "cit/utility.h"
#include "Timer.h"
#include "PostMaster.h"

BasketballGame::BasketballGame()
{
	myScoreboard = nullptr;
	myHoopLeftSideRing = nullptr;
	myHoopRightSideRing = nullptr;
	myScore = 0;
	myHasSentMessage = false;
}


BasketballGame::~BasketballGame()
{
	cit::SafeDelete(myScoreboard);
	cit::SafeDelete(myHoopLeftSideRing);
	cit::SafeDelete(myHoopRightSideRing);
}

void BasketballGame::Init()
{
	myMaxTime = 60.0f;
	myMaxScore = 8;
	myScoreboard = new GameObject();
	myScoreboard->InitGO(myPosition, "sprites/level2/basketBall_scoreBoard.dds", new Sprite());
	myScoreboard->AccessDrawable().SetLayer(1);
	myScoreboardScoreText.Init("text/Mecha.ttf");
	myScoreboardScoreText.SetScale(1.8f);
	myScoreboardScoreText.SetLayer(2);
	myScoreboardScoreText.SetColor(CommonUtilities::Vector4f{ 0.0f, 1.0f, 0.0f, 1.0f });
	myScoreboardTimeText.Init("text/Mecha.ttf");
	myScoreboardTimeText.SetScale(1.8f);
	myScoreboardTimeText.SetLayer(2);
	myScoreboardTimeText.SetColor(CommonUtilities::Vector4f{ 0.0f, 1.0f, 0.0f, 1.0f });

	myHoopLeftSideRing = new GameObject();
	myHoopLeftSideRing->InitGO(myPosition, "sprites/noObj.dds", new Sprite());
	BoxCollider leftSideCol;
	leftSideCol.Init(0.006f, 0.02f, CommonUtilities::Vector2f{});
	leftSideCol.SetIsSolid(true);
	leftSideCol.SetTag(ColliderTag::Tile);
	leftSideCol.SetOwner(myHoopLeftSideRing); //set to scoreboard to prevent oncollisionenter from happening
	myHoopLeftSideRing->AttachCollider(leftSideCol);

	myHoopRightSideRing = new GameObject();
	myHoopRightSideRing->InitGO(myPosition, "sprites/noObj.dds", new Sprite());
	BoxCollider rightSideCol;
	rightSideCol.Init(0.006f, 0.02f, CommonUtilities::Vector2f{});
	rightSideCol.SetIsSolid(true);
	rightSideCol.SetTag(ColliderTag::Tile);
	rightSideCol.SetOwner(myHoopRightSideRing); //set to scoreboard to prevent oncollisionenter from happening
	myHoopRightSideRing->AttachCollider(leftSideCol);

	BoxCollider myCol;
	myCol.Init(0.02f, 0.04f, CommonUtilities::Vector2f{ -0.02f, -0.08f });
	myCol.SetIsSolid(false);
	myCol.SetTag(ColliderTag::BasketballNet);
	AttachCollider(myCol);
	myDrawable->SetLayer(2);

	myScoreBoardOffset = { -0.32f, 0.12f };

	myTimeRunOut = false;
	myCountDown.Set(myMaxTime, cit::countdown::type::oneshot, [&] { myTimeRunOut = true; });
	myCountDown.Start();

	myHoop.Init("sprites/level2/basketBall_hoop.dds");
	myHoop.SetLayer(6);

	myNet.Init("hoopAnimated");
	myNet.SetLayer(5);
	myNet.SetCurrentFrame(0);
	myNet.Pause();
}

void BasketballGame::OnUpdate()
{
	myCountDown.Update(CommonUtilities::Timer::GetDeltaTime());
	if (myTimeRunOut == true && myScore < myMaxScore)
	{
		myScore = 0;
		myCountDown.Reset();
		myCountDown.Start();
		myTimeRunOut = false;
	}
	else if (myScore >= myMaxScore)
	{
		if (!myHasSentMessage)
		{
			Message msg;
			msg.myMessageType = MessageType::FinishedWarpZone;
			PostMaster::SendMessages(msg);
			myHasSentMessage = true;
		}
	}
	myHoopRightSideRing->SetPosition(myPosition + CommonUtilities::Vector2f{0.0069f, -0.108f});
	myHoopRightSideRing->OnUpdate();
	myHoopLeftSideRing->SetPosition(myPosition + CommonUtilities::Vector2f{ -0.051f, -0.108f });
	myHoopLeftSideRing->OnUpdate();
	myScoreboard->SetPosition(myPosition + myScoreBoardOffset);
	myScoreboard->OnUpdate();
	GameObject::OnUpdate();

	myScoreboardTimeText.SetText(std::to_string(static_cast<int>(myCountDown.GetMaxTime() - myCountDown.GetCurrentTime())));
	myScoreboardTimeText.SetPosition(myScoreboard->GetPosition() + CommonUtilities::Vector2f(0.0345f, -0.075f));
	myScoreboardScoreText.SetText(std::to_string(myScore) + "/" + std::to_string(myMaxScore));
	myScoreboardScoreText.SetPosition(myScoreboard->GetPosition() + CommonUtilities::Vector2f(0.0345f, -0.03f));
	myHoop.SetPosition(myPosition + CommonUtilities::Vector2f(0.0f, -0.17f));
	myNet.SetPosition(myHoop.GetPosition() +CommonUtilities::Vector2f(-0.018f, 0.11f));
}

void BasketballGame::OnCollisionEnter(Collider & other)
{
	if (other.GetTag() != ColliderTag::Grabbable)
	{
		return;
	}
	if (other.GetOwner()->GetPosition().y < myCollider->GetOffset().y + myPosition.y)
	{
		myNet.Unpause();
		myNet.SetCurrentFrame(0);
		myScore++;
	}
}

void BasketballGame::FillCollisionBuffer(CommonUtilities::GrowingArray<GameObject*>& aBufferToAddTo)
{
	aBufferToAddTo.Add(myHoopLeftSideRing);
	aBufferToAddTo.Add(myHoopRightSideRing);
	aBufferToAddTo.Add(this);
}

void BasketballGame::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(&myScoreboardTimeText);
	aRenderBuffer.Add(&myScoreboardScoreText);
	aRenderBuffer.Add(myDrawable);
	aRenderBuffer.Add(&myScoreboard->AccessDrawable());
	aRenderBuffer.Add(&myHoop);
	aRenderBuffer.Add(&myNet);
}
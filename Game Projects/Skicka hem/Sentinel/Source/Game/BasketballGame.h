#pragma once
#include "GameObject.h"
#include "Text.h"
#include "Animation.h"
#include "cit/countdown.h"
class BasketballGame : public GameObject
{
public:
	BasketballGame();
	~BasketballGame();

	void Init();
	void OnUpdate() override;
	void OnCollisionEnter(Collider &other) override;

	void FillCollisionBuffer(CommonUtilities::GrowingArray<GameObject *> &aBufferToAddTo) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable *> &aRenderBuffer) override;
private:
	bool myHasSentMessage;

	GameObject *myScoreboard;
	CommonUtilities::Vector2f myScoreBoardOffset;
	Text myScoreboardTimeText;
	Text myScoreboardScoreText;
	GameObject *myHoopLeftSideRing;
	GameObject *myHoopRightSideRing;
	//Animation myHoopAnimation;
	int myScore;
	cit::countdown myCountDown;
	Sprite myHoop;
	Animation myNet;

	int myMaxScore;
	float myMaxTime;
	bool myTimeRunOut;
};


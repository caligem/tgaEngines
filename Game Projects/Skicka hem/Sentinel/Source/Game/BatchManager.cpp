#include "stdafx.h"
#include "BatchManager.h"
#include "JsonWrapper.h"
#include "miscutils.h"
#include <Vector.h>
#include <assert.h>
#include "TileSize.h"
#include <tga2d\engine.h>
BatchManager* BatchManager::ourInstance = nullptr;

void BatchManager::Create()
{
	assert(ourInstance == nullptr && "Instance already created");
	ourInstance = new BatchManager();
	ourInstance->myBatches.Init(256);
}

void BatchManager::Destroy()
{
	assert(ourInstance != nullptr && "Instance not created");
	delete ourInstance;
	ourInstance = nullptr;
}

void BatchManager::InitBatches()
{
	json dataFile = LoadData("Data/batches.json");

	ourInstance->myBatches.ReInit(64);
	for (unsigned short i = 0; i < dataFile["batches"].size(); i++)
	{
		ourInstance->myBatches.Add(Tga2D::CSpriteBatch(true));
		ourInstance->myBatches.GetLast().Init(dataFile["batches"][i].get<std::string>().c_str());
	}
}

int BatchManager::AddToBatchAndGetIndex(int &aSpriteIndexToSet, std::string aPath)
{
	Tga2D::CSprite *add = new Tga2D::CSprite(nullptr);
	add->SetPivot({ 0.5f, 0.5f });
	add->SetSizeRelativeToScreen({ TILE_SIZE /  1920.f , TILE_SIZE / 1080.f });
	for (unsigned short i = 0; i < ourInstance->myBatches.Size(); i++)
	{
		if (ToLower(ourInstance->myBatches[i].GetImagePath()) == ToLower(aPath))
		{
			if (ourInstance->myBatches[i].myCurrentSpriteCount >= 999)
			{
				continue;
			}
			else
			{
				ourInstance->myBatches[i].AddObject(add);
				aSpriteIndexToSet = static_cast<int>(ourInstance->myBatches[i].mySprites[0].size() - 1);
				return i;
			}
		}
	}
	ourInstance->myBatches.Add(Tga2D::CSpriteBatch(true));
	ourInstance->myBatches.GetLast().Init(aPath.c_str());
	ourInstance->myBatches.GetLast().AddObject(add);
	aSpriteIndexToSet = static_cast<int>(ourInstance->myBatches.GetLast().mySprites[0].size() - 1);
	return ourInstance->myBatches.Size() -1;
}

void BatchManager::RemoveFromBatchAtIndex(Tga2D::CSprite * aSpriteToRemove, int aIndex)
{
	ourInstance->myBatches[static_cast<unsigned short>(aIndex)].RemoveObject(aSpriteToRemove);
}

void BatchManager::RenderBatch()
{
	for (size_t i = 0; i < ourInstance->myBatches.Size(); i++)
	{
		ourInstance->myBatches[static_cast<unsigned short>(i)].Render();
	}
}

void BatchManager::RenderBatch(Tga2D::CSprite * aSprite)
{
	ourInstance->myBatches[0].Render();
}

const Tga2D::CSprite * BatchManager::GetLastSpriteInBatch(int aIndex)
{
	std::vector<Tga2D::CSprite*> &batchSprites = ourInstance->myBatches[static_cast<unsigned short>(aIndex)].mySprites[0];
	return batchSprites[batchSprites.size() - 1];
}

Tga2D::CSprite * BatchManager::GetSpriteAtIndex(int aBatchIndex, int aSpriteIndex)
{
	std::vector<Tga2D::CSprite*> &batchSprites = ourInstance->myBatches[static_cast<unsigned short>(aBatchIndex)].mySprites[0];
	return batchSprites[static_cast<unsigned short>(aSpriteIndex)];
}

CommonUtilities::Vector2<unsigned int> BatchManager::GetImageSizeOfBatch(int aBatchIndex)
{
	return{ ourInstance->myBatches[static_cast<unsigned short>(aBatchIndex)].GetImageSize().x, ourInstance->myBatches[static_cast<unsigned short>(aBatchIndex)].GetImageSize().y };
}


bool BatchManager::IsLastInBatch(int aBatchIndex, int aSpriteIndex)
{
	return (ourInstance->myBatches[static_cast<unsigned short>(aBatchIndex)].mySprites[0].size() - 1 == static_cast<unsigned short>(aSpriteIndex));
}

void BatchManager::EmptyBatches()
{
	for (size_t batchIndex = 0; batchIndex < ourInstance->myBatches.Size(); batchIndex++)
	{
			ourInstance->myBatches[batchIndex].ClearAll();
	}
	ourInstance->myBatches.DeleteAll();
}


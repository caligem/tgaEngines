#pragma once
#include "GrowingArray.h"
#include <tga2d\sprite\sprite.h>
#include <tga2d\sprite\sprite_batch.h>
#include "Vector.h"

class BatchManager
{
public:

	static void Create();
	static void Destroy();
	static void InitBatches();

	static int AddToBatchAndGetIndex(int &aSpriteIndexToSet, std::string aPath);
	static void RemoveFromBatchAtIndex(Tga2D::CSprite *aSpriteToRemove, int aIndex);

	static void RenderBatch();
	static void RenderBatch(Tga2D::CSprite* aSprite);
	static const Tga2D::CSprite* GetLastSpriteInBatch(int aIndex);
	static Tga2D::CSprite* GetSpriteAtIndex(int aBatchIndex, int aSpriteIndex);
	static CommonUtilities::Vector2<unsigned int> GetImageSizeOfBatch(int aBatchIndex);
	static bool IsLastInBatch(int aBatchIndex, int aSpriteIndex);
	static void EmptyBatches();

private:
	static BatchManager *ourInstance;
	CommonUtilities::GrowingArray<Tga2D::CSpriteBatch> myBatches;
};
#include "stdafx.h"
#include "BatchedSprite.h"
#include <tga2d\sprite\sprite.h>
#include "BatchManager.h"
#include "TileSize.h"

BatchedSprite::BatchedSprite()
{
	myLayer = 1;
	myShouldDrawThisFrame = true;
	mySize = { static_cast<unsigned int>(TILE_SIZE), static_cast<unsigned int>(TILE_SIZE) };
}

BatchedSprite::~BatchedSprite()
{
	//BatchManager is deleting, when level is deleted.
}

void BatchedSprite::Init(const char * aPath)
{
	myBatchIndex = BatchManager::AddToBatchAndGetIndex(mySpriteIndex, aPath);
	
}

void BatchedSprite::Render()
{
	
		BatchManager::GetSpriteAtIndex(myBatchIndex, mySpriteIndex)->SetPosition({ myPosition.x, myPosition.y });
	
}

void BatchedSprite::SetTextureRectPosFromTileID(const int aID, const TileSet & aTileSet)
{
	float textureRectStartX = 0.0f;
	float textureRectStartY = 0.0f;
	float textureRectEndX = 0.0f;
	float textureRectEndY = 0.0f;
	if (aTileSet.tileObjects.Size() > 0)
	{
		textureRectEndX = 1.0f;
		textureRectEndY = 1.0f;
	}
	else
	{
		int tilesPerWidth = aTileSet.columns;
		float xSize = aTileSet.imageWidth / aTileSet.columns / TILE_SIZE;
		float ySize = aTileSet.imageHeight / (aTileSet.tileCount / aTileSet.columns) / TILE_SIZE;
		
		Tga2D::CSprite *spriteInBatch = BatchManager::GetSpriteAtIndex(myBatchIndex, mySpriteIndex);
		spriteInBatch->SetSizeRelativeToImage({ xSize , ySize });


		for (size_t tileIndex = aTileSet.startGridID; tileIndex < aTileSet.tileCount + aTileSet.startGridID; tileIndex++)
		{

			if (tileIndex == aID)
			{
				break;
			}
			else
			{ 
				if ((tileIndex + 1 - aTileSet.startGridID) % (tilesPerWidth) == 0 && (tileIndex - aTileSet.startGridID) != 0)
				{
					textureRectStartY += TILE_SIZE;
					textureRectStartX = 0.0f;
				}
				else
				{
					textureRectStartX += TILE_SIZE;
				}
			}
		}

		textureRectEndX = (textureRectStartX + TILE_SIZE + aTileSet.spacing) / BatchManager::GetImageSizeOfBatch(myBatchIndex).x;
		textureRectEndY = (textureRectStartY + TILE_SIZE + aTileSet.spacing) / BatchManager::GetImageSizeOfBatch(myBatchIndex).y;

		textureRectStartX = (textureRectStartX + aTileSet.spacing) / BatchManager::GetImageSizeOfBatch(myBatchIndex).x;
		textureRectStartY = (textureRectStartY + aTileSet.spacing) / BatchManager::GetImageSizeOfBatch(myBatchIndex).y;
	}

	BatchManager::GetSpriteAtIndex(myBatchIndex, mySpriteIndex)->SetTextureRect(textureRectStartX, textureRectStartY, textureRectEndX, textureRectEndY);
}
const CommonUtilities::Vector2<unsigned int> BatchedSprite::GetImagesSizePixel() const
{
	return mySize;
}

void BatchedSprite::SetSizeRelativeToScreen(const CommonUtilities::Vector2f & aSize)
{
	BatchManager::GetSpriteAtIndex(myBatchIndex, mySpriteIndex)->SetSizeRelativeToScreen({ aSize.x, aSize.y });
}

void BatchedSprite::SetShouldRender(const bool aShouldRender)
{
	myShouldDrawThisFrame = aShouldRender;
	BatchManager::GetSpriteAtIndex(myBatchIndex, mySpriteIndex)->SetShouldRender(aShouldRender);
}

void BatchedSprite::SetRotation(const float aRotation)
{
}

bool BatchedSprite::GetIsFlipped() const
{
	return BatchManager::GetSpriteAtIndex(myBatchIndex, mySpriteIndex)->GetSize().x < 0.0f;
}

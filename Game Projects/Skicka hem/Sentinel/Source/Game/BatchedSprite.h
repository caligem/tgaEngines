#pragma once
#include "Drawable.h"

class BatchedSprite : public Drawable
{
public:
	BatchedSprite();
	~BatchedSprite() override;
	void Init(const char* aPath) override;
	void Render() override;
	void SetTextureRectPosFromTileID(const int aID, const TileSet & aTileSet);

	const CommonUtilities::Vector2<unsigned int> BatchedSprite::GetImagesSizePixel() const override;
	void SetSizeRelativeToScreen(const CommonUtilities::Vector2f & aSize) override;
	bool GetIsFlipped() const override;
	void SetShouldRender(const bool aShouldRender) override;
	void SetRotation(const float aRotation) override;
private:
	int myBatchIndex;
	int mySpriteIndex;
	CommonUtilities::Vector2<unsigned int> mySize;
};
#include "stdafx.h"
#include "BlackBars.h"

BlackBars::BlackBars()
{
	
}

BlackBars::~BlackBars()
{
	/*delete myBlackBar;
	myBlackBar = nullptr;*/
}

void BlackBars::Init(float aSpeed, float aSize)
{
	
	myBlackBar = new Tga2D::CSprite("Sprites/white_pixel.dds");
	myBlackBar->SetColor(Tga2D::CColor(0.f, 0.f, 0.f, 1.0f));

	myBlackBar->SetSizeRelativeToScreen({ 2.0f, aSize });
	myBlackBar->SetPivot({ 0.5f, 0.5f });

	myBlackBarOnePos = Vector2<float>(0.5f, -0.5f);
	myBlackBarTwoPos = Vector2<float>(0.5f, 1.5f);

	myBlackBarOneMaxY = 0.08f;
	myBlackBarTwoMaxY = 0.92f;

	myTimer = 0;

	mySpeed = aSpeed;

	myBlackBarsHasEntered = false;
	myBlackBarsShouldLeave = false;
	myBlackBarsHasLeft = false;
	myShouldRemove = false;

	myShouldDecrease = false;
	myShouldEnter = true;
}

void BlackBars::Update(float aDeltaTime, bool aIsDialogueDone)
{
	if (myShouldEnter && !aIsDialogueDone) 
	{
		if (!myBlackBarsHasEntered)
		{
			if (myBlackBarOnePos.y < myBlackBarOneMaxY)
			{
				myBlackBarOnePos.y += aDeltaTime * mySpeed;
			}
			else 
			{
				myBlackBarsHasEntered = true;
			}

			if (myBlackBarTwoPos.y > myBlackBarTwoMaxY)
			{
				myBlackBarTwoPos.y -= aDeltaTime * mySpeed;
			}
		}
	}

	if (!myShouldRemove) 
	{
		if (!myBlackBarsHasEntered && myBlackBarOnePos.y >= myBlackBarOneMaxY)
		{
			myTimer += aDeltaTime;
		}
	}
	
	if(aIsDialogueDone && !myBlackBarsHasLeft)
	{
		myBlackBarOnePos.y -= aDeltaTime * mySpeed;
		myBlackBarTwoPos.y += aDeltaTime * mySpeed;

		if (myBlackBarOnePos.y < -0.3f)
		{
			myBlackBarsHasLeft = true;
		}
	}

}

void BlackBars::Render() 
{
	myBlackBar->SetPosition({ myBlackBarOnePos.x, myBlackBarOnePos.y });
	myBlackBar->Render();

	myBlackBar->SetPosition({ myBlackBarTwoPos.x, myBlackBarTwoPos.y });
	myBlackBar->Render();
}

void BlackBars::Remove()
{
	myShouldRemove = true;
}

bool BlackBars::HasLeft()
{
	return myBlackBarsHasLeft;
}

bool BlackBars::HasEntered()
{
	return myBlackBarsHasEntered;
}

float BlackBars::GetTimer()
{
	return myTimer;
}



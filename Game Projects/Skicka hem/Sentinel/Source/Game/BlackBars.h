#pragma once
#include "tga2d\sprite\sprite.h"
#include "Vector.h"

using namespace CommonUtilities;

class BlackBars
{
public:
	BlackBars();
	~BlackBars();

	void Init(float aSpeed, float aSize);
	void Update(float aDeltaTime, bool aIsDialogueDone);
	void Render();
	void Remove();

	bool HasLeft();
	bool HasEntered();

	float GetTimer();

	const float GetSpeed() const { return mySpeed; }
private:
	bool myBlackBarsHasEntered;
	bool myBlackBarsShouldLeave;
	bool myBlackBarsHasLeft;
	bool myShouldDecrease;

	Tga2D::CSprite* myBlackBar;

	Vector2<float> myBlackBarOnePos;
	Vector2<float> myBlackBarTwoPos;

	float myBlackBarOneMaxY;
	float myBlackBarTwoMaxY;

	bool myShouldRemove;
	bool myShouldEnter;

	float myTimer;
	float mySpeed;

	float myEnterDelay;

};


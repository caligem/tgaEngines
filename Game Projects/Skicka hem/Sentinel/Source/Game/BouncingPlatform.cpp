#include "stdafx.h"
#include "BouncingPlatform.h"
#include "Player.h"
#include "JsonWrapper.h"
#include "SoundEngine.h"

BouncingPlatform::BouncingPlatform()
{
}


BouncingPlatform::~BouncingPlatform()
{
	mySound->Annahilate();
}

void BouncingPlatform::Init(const char * aPath)
{
	json bouncingPlatformData = LoadObjectType(aPath);

	myBounceHeight = bouncingPlatformData["playerBounceHeight"];
	myDrawable->SetLayer(1);

	mySound = new Sound("Sounds/Sfx/jump_pad.wav");
}

void BouncingPlatform::OnUpdate()
{
	myDrawable->SetPosition({ myPosition.x, myPosition.y });
	GameObject::OnUpdate();
}

void BouncingPlatform::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(myDrawable);
}

void BouncingPlatform::OnCollisionEnter(Collider & other)
{
	if (other.GetTag() == ColliderTag::Player)
	{
		CommonUtilities::Vector2f collisionNormal = other.GetOwner()->GetPosition() - myPosition;
		collisionNormal.Normalize();
		float dot = CommonUtilities::Vector2f({ 0, -1 }).Dot(collisionNormal);
		if (dot > 0 && acosf(dot) < 0.5f)
		{
			dynamic_cast<Animation*>(myDrawable)->SetType(Animation::Type::Oneshot);
			dynamic_cast<Player*>(other.GetOwner())->Jump(myBounceHeight, true);
			SoundEngine::PlaySound(*mySound);
		}
	}
}

void BouncingPlatform::OnCollisionLeave(Collider & other)
{
	dynamic_cast<Animation*>(myDrawable)->SetCurrentFrame(0);
}

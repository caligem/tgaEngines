#pragma once
#include "GameObject.h"

class Sound;

class BouncingPlatform : public GameObject
{
public:
	BouncingPlatform();
	~BouncingPlatform();

	void Init(const char* aPath);
	void OnUpdate() override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*> &aRenderBuffer) override;

private:

	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionLeave(Collider &other) override;
	float myBounceHeight;
	Sound* mySound;

};


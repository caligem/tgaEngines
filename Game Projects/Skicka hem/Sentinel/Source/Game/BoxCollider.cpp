#include "stdafx.h"
#include "BoxCollider.h"
#include "GameObject.h"
#include "LineCollider.h"
#include "cit/utility.h"

BoxCollider::BoxCollider()
{
	
}

BoxCollider::BoxCollider(float aWidth, float aHeight, CommonUtilities::Vector2f & aOffset)
{
	myWidth = aWidth;
	myHeight = aHeight;
	myOffset = aOffset;
}


BoxCollider::~BoxCollider()
{
	myGameObject = nullptr;
}

void BoxCollider::Init(float aWidth, float aHeight, CommonUtilities::Vector2f  aOffset)
{
	myWidth = aWidth;
	myHeight = aHeight;
	myOffset = aOffset;
}

bool BoxCollider::IsCollidedWith(Collider * aCol)
{
	if (this != aCol  && aCol->GetTag() != ColliderTag::None)
	{
		CommonUtilities::Vector2f colPos = { myGameObject->GetPosition().x + myOffset.x, myGameObject->GetPosition().y + myOffset.y };
		return aCol->IsCollidedWith(this, colPos);
	}
	return false;
}

bool BoxCollider::IsCollidedWith(BoxCollider * aBoxCol, const CommonUtilities::Vector2f & aGOPos)
{
	return BoxWithBoxCollision(CommonUtilities::Vector2f{ myGameObject->GetPosition().x + myOffset.x, myGameObject->GetPosition().y + myOffset.y }, myWidth, myHeight, CommonUtilities::Vector2f{ aGOPos.x, aGOPos.y}, aBoxCol->GetWidth(), aBoxCol->GetHeight());
}

bool BoxCollider::IsCollidedWith(CircleCollider * aCircleCol, const CommonUtilities::Vector2f & aGOPos)
{
	return CircleWithBoxCollision(CommonUtilities::Vector2f{ aGOPos.x + aCircleCol->GetOffset().x, aGOPos.y + aCircleCol->GetOffset().y }, aCircleCol->GetRadius(), CommonUtilities::Vector2f{ myGameObject->GetPosition().x + myOffset.x, myGameObject->GetPosition().y + myOffset.y }, myWidth, myHeight);
}

bool BoxCollider::IsCollidedWith(LineCollider * aLineCol, const CommonUtilities::Vector2f & aGOPos)
{
	return BoxWithLineCollision(myGameObject->GetPosition() + myOffset, myWidth, myHeight, aGOPos, aGOPos + (aLineCol->GetDirection() *aLineCol->GetLength()));
}

Collider::Hit BoxCollider::CollisionDirectionWith(Collider * aCol)
{
	return aCol->CollisionDirectionWith(this);
}

Collider::Hit BoxCollider::CollisionDirectionWith(BoxCollider * aCol)
{
	CommonUtilities::Vector2f otherBoxPos = aCol->GetOwner()->GetPosition() + aCol->GetOffset();
	CommonUtilities::Vector2f myBoxPos = myOffset + myGameObject->GetPosition();
	float dx = otherBoxPos.x - myBoxPos.x;
	float px = (aCol->myWidth / 2.0f) + (myWidth / 2.0f) - fabsf(dx);
	
	Hit hit;

	if (px <= 0.0f)
	{
		return hit;
	}
	float dy = otherBoxPos.y - myBoxPos.y;
	float py = (aCol->myHeight / 2.0f) + (myHeight / 2.0f) - fabsf(dy);

	if (py <= 0.0f)
	{
		return hit;
	}

	if (px < py)
	{
		float sx = static_cast<float>(cit::sign(dx));
		hit.delta.x = px * sx;
		hit.normal.x = sx;
	}
	else
	{
		float sy = static_cast<float>(cit::sign(dy));
		hit.delta.y = px * sy;
		hit.normal.y = sy;
	}
	hit.normal *= -1.0f;
	return hit;
}

#include "Camera.h"
void BoxCollider::Draw()
{
	CommonUtilities::Vector2f topLeft = Camera::GetInstance().ConvertPositionToCameraSpace(myGameObject->GetPosition() + myOffset);
	topLeft.x -= myWidth / 2.0f;
	topLeft.y -= myHeight / 2.0f;

	CommonUtilities::Vector2f bottomRight = topLeft;
	bottomRight.x += myWidth;
	bottomRight.y += myHeight;

	Tga2D::CLinePrimitive AB;
	AB.SetFrom(topLeft.x, topLeft.y);
	AB.SetTo(bottomRight.x, topLeft.y);
	AB.myColor = { 0.0f, 1.0f, 0.0f, 1.0f };
	
	Tga2D::CLinePrimitive BC;
	BC.SetFrom(bottomRight.x, topLeft.y);
	BC.SetTo(bottomRight.x, bottomRight.y);
	BC.myColor = { 0.0f, 1.0f, 0.0f, 1.0f };

	Tga2D::CLinePrimitive CD;
	CD.SetFrom(bottomRight.x, bottomRight.y);
	CD.SetTo(topLeft.x, bottomRight.y);
	CD.myColor = { 0.0f, 1.0f, 0.0f, 1.0f };

	Tga2D::CLinePrimitive DA;
	DA.SetFrom(topLeft.x, bottomRight.y);
	DA.SetTo(topLeft.x, topLeft.y);
	DA.myColor = { 0.0f, 1.0f, 0.0f, 1.0f };

	Tga2D::CLinePrimitive DB;
	DB.SetFrom(topLeft.x, bottomRight.y);
	DB.SetTo(bottomRight.x, topLeft.y);
	DB.myColor = { 0.0f, 1.0f, 0.0f, 1.0f };

	AB.Render();
	BC.Render();
	CD.Render();
	DA.Render();
	DB.Render();
}

CommonUtilities::Vector2f BoxCollider::RelativePoint(const CommonUtilities::Vector2f & aPoint)
{
	CommonUtilities::Vector2f returnValue;
	float amountInsideFromTop = myGameObject->GetPosition().y + myOffset.y - (myHeight / 2.0f) - aPoint.y;
	float amountInsideFromBottom = myGameObject->GetPosition().y + myOffset.y + (myHeight / 2.0f) - aPoint.y;

	if (amountInsideFromBottom * amountInsideFromBottom < amountInsideFromTop * amountInsideFromTop)
	{
		returnValue.y = amountInsideFromBottom;
	}
	else
	{
		returnValue.y = amountInsideFromTop;
	}

	float amountInsideFromLeft = myGameObject->GetPosition().x + myOffset.x - (myWidth / 2.0f) - aPoint.x;
	float amountInsideFromRight = myGameObject->GetPosition().x + myOffset.x + (myWidth / 2.0f) - aPoint.x;

	if (amountInsideFromRight * amountInsideFromRight < amountInsideFromLeft * amountInsideFromLeft)
	{
		returnValue.x = amountInsideFromRight;
	}
	else
	{
		returnValue.x = amountInsideFromLeft;
	}
	return returnValue;
}

void BoxCollider::SetHeight(float aHeight)
{
	myHeight = aHeight;
}

void BoxCollider::SetWidth(float aWidth)
{
	myWidth = aWidth;
}

const float BoxCollider::GetWidth() const
{
	return myWidth;
}

const float BoxCollider::GetHeight() const
{
	return myHeight;
}

CommonUtilities::Vector2f & BoxCollider::GetOffset()
{
	return myOffset;
}
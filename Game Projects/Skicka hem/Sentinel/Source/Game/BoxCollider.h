#pragma once
#include "Collider.h"
#include "Vector.h"
class BoxCollider : public Collider
{
public:
	BoxCollider();
	BoxCollider(float aWidth, float aHeight, CommonUtilities::Vector2f & aOffset);
	~BoxCollider();
	void Init(float aWidth, float aHeight, CommonUtilities::Vector2f aOffset);
	bool IsCollidedWith(Collider *aCol) override;
	bool IsCollidedWith(BoxCollider *aBoxCol, const CommonUtilities::Vector2f &aGOPos) override;
	bool IsCollidedWith(CircleCollider *aCircleCol, const CommonUtilities::Vector2f &aGOPos) override;
	bool IsCollidedWith(LineCollider *aLineCol, const CommonUtilities::Vector2f &aGOPos) override;

	Hit CollisionDirectionWith(Collider *aCol) override;
	Hit CollisionDirectionWith(BoxCollider *aCol) override;

	void Draw() override;
	CommonUtilities::Vector2f RelativePoint(const CommonUtilities::Vector2f &aPoint) override;
	
	void SetHeight(float aHeight);
	void SetWidth(float aWidth);
	const float GetWidth() const;
	const float GetHeight() const;
	CommonUtilities::Vector2f &GetOffset() override;

private:
	float myWidth;
	float myHeight;
	CommonUtilities::Vector2f myOffset;
};


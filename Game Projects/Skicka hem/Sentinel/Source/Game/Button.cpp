#include "stdafx.h"
#include "Button.h"
#include "InputManager.h"
#include "Animation.h"
#include "tga2d\engine.h"
#include "Timer.h"
#include "tga2d\primitives\line_primitive.h"
#include "SoundEngine.h"

							  
Button::Button()
	: myAnimation(nullptr)
	, myInputManager(nullptr)
	, myHitboxOffsetX(0)
	, myHitboxOffsetY(0)
	, myDefaultAnimationFrameIndex(0)
{
}


Button::~Button()
{
	if (myAnimation != nullptr)
	{
		delete myAnimation;
		myAnimation = nullptr;
	}

}

void Button::Init(std::function<void()> aFunction, const char * aPath, CommonUtilities::Vector2f aPosition, CommonUtilities::InputManager* aInputManager, bool aIsClickable)
{
	myWasHovoredLastFrame = false;
	myFunction = aFunction;
	myInputManager = aInputManager;
	InitSprite(aPath, aPosition);
	myIsClickable = aIsClickable;
	InitHitbox();
	myClickSpriteCountdown.Set(0.2f, cit::countdown::type::oneshot, [&] {myAnimation->SetCurrentFrame(0); });
}

void Button::Update()
{
	myClickSpriteCountdown.Update(CommonUtilities::Timer::GetDeltaTime());
}

void Button::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(myAnimation);
}

void Button::RenderDebugLines()
{
	Tga2D::CLinePrimitive line;
	line.SetFrom(myMinPosition.x, myMinPosition.y);
	line.SetTo(myMinPosition.x, myMaxPosition.y);
	line.Render();
	line.SetFrom(myMinPosition.x, myMinPosition.y);
	line.SetTo(myMaxPosition.x, myMinPosition.y);
	line.Render();
	line.SetFrom(myMinPosition.x, myMaxPosition.y);
	line.SetTo(myMaxPosition.x, myMaxPosition.y);
	line.Render();
	line.SetFrom(myMaxPosition.x, myMinPosition.y);
	line.SetTo(myMaxPosition.x, myMaxPosition.y);
	line.Render();
}

void Button::SetHitboxOffsetX(float aOffset)
{
	myMinPosition.x += aOffset;
	myMaxPosition.x -= aOffset;
}

void Button::SetHitboxOffsetY(float aOffset)
{
	myMinPosition.y += aOffset;
	myMaxPosition.y -= aOffset;
}

void Button::Rotate(float aRadius)
{
	myAnimation->Rotate(aRadius);
}

void Button::SetDefaultFrame(int aFrameIndex)
{
	myDefaultAnimationFrameIndex = aFrameIndex;
}

void Button::SetCurrentFrame(int aFrameIndex)
{
	myAnimation->SetCurrentFrame(aFrameIndex);
}

void Button::FlipAnimation()
{
	myAnimation->SetInversedX(true);
}

const CommonUtilities::Vector2f & Button::GetPosition()
{
	return myAnimation->GetPosition();
}

void Button::ConnectButton(Button* aButton, eConnectSide aConnectSide)
{
	if (aConnectSide == eConnectSide::Left)
	{
		myConnectedButtons.myLeftButton = aButton;
		aButton->myConnectedButtons.myRightButton = this;
	}
	else if (aConnectSide == eConnectSide::Right)
	{
		myConnectedButtons.myRightButton = aButton;
		aButton->myConnectedButtons.myLeftButton = this;
	}
	else if (aConnectSide == eConnectSide::Down)
	{
		myConnectedButtons.myDownButton = aButton;
		aButton->myConnectedButtons.myUpButton = this;
	}
	else if (aConnectSide == eConnectSide::Up)
	{
		myConnectedButtons.myUpButton = aButton;
		aButton->myConnectedButtons.myDownButton = this;
	}
}

void Button::InitHitbox()
{
	float screenX = Tga2D::CEngine::GetInstance()->GetWindowSize().x;
	float screenY = Tga2D::CEngine::GetInstance()->GetWindowSize().y;
		
	float imageSizeX = (static_cast<float>(myAnimation->GetImagesSizePixel().x));
	float imageSizeY = (static_cast<float>(myAnimation->GetImagesSizePixel().y));

	myMinPosition.x = myAnimation->GetPosition().x - ((imageSizeX / screenX) * 2);
    myMaxPosition.x = myAnimation->GetPosition().x + ((imageSizeX / screenX) * 2);
	myMinPosition.y = myAnimation->GetPosition().y - ((imageSizeY / screenY) * 2);
	myMaxPosition.y = myAnimation->GetPosition().y + ((imageSizeY / screenY) * 2);
}

void Button::InitSprite(const char * aPath, CommonUtilities::Vector2f & aPosition)
{
	myAnimation = new Animation();
	myAnimation->Init(aPath);
	myAnimation->SetPosition(aPosition);
}

void Button::OnClick()
{
	SoundEngine::PlaySound(*SoundEngine::GetClickSound());
	myAnimation->SetCurrentFrame(myDefaultAnimationFrameIndex + 2);
	myClickSpriteCountdown.Start();
	myFunction();
}

bool Button::IsMouseInside()
{
	CommonUtilities::Vector2f myMousePos = myInputManager->GetCursorPositionOnScreen();

	float screenX = Tga2D::CEngine::GetInstance()->GetWindowSize().x;
	float screenY = Tga2D::CEngine::GetInstance()->GetWindowSize().y;

	myMousePos.x /= screenX;
	myMousePos.y /= screenY;

	if (myMousePos.x > myMinPosition.x && myMousePos.x < myMaxPosition.x
		&& myMousePos.y < myMaxPosition.y && myMousePos.y > myMinPosition.y)
	{
		if (myWasHovoredLastFrame == false)
		{
			SoundEngine::PlaySound(*SoundEngine::GetHoverSound());
			myWasHovoredLastFrame = true;
		}

		return true;
	}

	myWasHovoredLastFrame = false;
	return false;
}

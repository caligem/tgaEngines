#pragma once
#include <functional>
#include "cit\countdown.h"
#include "Vector2.h"
#include "GrowingArray.h"

class Sound;

namespace CommonUtilities
{
	class InputManager;
}

class Animation;
class Drawable;


class Button
{
public:
	Button();
	~Button();

	enum class eConnectSide
	{
		Left,
		Right,
		Down,
		Up
	};

	void Init(std::function<void()> aFunction, const char* aPath, CommonUtilities::Vector2f aPosition, CommonUtilities::InputManager* aInputManager, bool aIsClickable = true);
	void Update();
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer);

	void RenderDebugLines();
	void SetHitboxOffsetX(float aOffset);
	void SetHitboxOffsetY(float aOffset);
	void Rotate(float aRadius);
	void SetDefaultFrame(int aFrameIndex);
	inline const int GetDefaultFrame() const { return myDefaultAnimationFrameIndex; }
	void SetCurrentFrame(int aFrameIndex);
	void FlipAnimation();

	inline void SetClickable(bool aIsClickable) { myIsClickable = aIsClickable; }
	inline const bool GetIsClickable() const { return myIsClickable; }

	bool IsMouseInside();
	void OnClick();

	const CommonUtilities::Vector2f& GetPosition();
	
	void ConnectButton(Button* aButton, eConnectSide aConnectSide);

	struct ConnectedButtons
	{
		Button* myLeftButton = nullptr;
		Button* myRightButton = nullptr;
		Button* myDownButton = nullptr;
		Button* myUpButton = nullptr;
	} myConnectedButtons;


private:
	bool myIsClickable;

	Animation* myAnimation;
	CommonUtilities::Vector2f myMaxPosition;
	CommonUtilities::Vector2f myMinPosition;

	int myDefaultAnimationFrameIndex;

	float myHitboxOffsetX;
	float myHitboxOffsetY;

	CommonUtilities::InputManager* myInputManager;
	cit::countdown myClickSpriteCountdown;
	void InitHitbox();
	void InitSprite(const char* aPath, CommonUtilities::Vector2f& aPosition);
	std::function<void()> myFunction;

	bool myWasHovoredLastFrame;
};


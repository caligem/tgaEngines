#include "stdafx.h"
#include "Camera.h"
#include "InputManager.h"
#include "Timer.h"
#include "Drawable.h"
#include "GameObject.h"
#include "tga2d\math\common_math.h"
#include "tga2d\engine.h"
#include "DialogueManager.h"
#include "TileSize.h"
#include "cit\utility.h"
#include "Player.h"

using namespace CommonUtilities;

void Camera::Init(const Vector2f& aPosition)
{
	aPosition;
	myPosition = { 0.5f, 0.5f };//aPosition;
	myZoom = 1.f;
	myShouldTrigger = false;
	myOldTarget = nullptr;
	myLerpSpeed = 1.5f;
	myWaitTimer = 0;
	myIsWaiting = false;
}

void Camera::Update(InputManager * aInputManager)
{
	if (DialogueManager::IsPlaying())
	{
		float diffrence = myPosition.y - myTarget->GetPosition().y;
		
		if (diffrence > -0.005f && diffrence < 0.005f)
		{
			myPosition.y = myTarget->GetPosition().y;
		}
		else
		{
			if (myPosition.y < myTarget->GetPosition().y)
			{
				myPosition.y += CommonUtilities::Timer::GetDeltaTime() * DialogueManager::GetCurrentBlackbarSpeed() * 0.6f;
			}
			else if (myPosition.y > myTarget->GetPosition().y)
			{
				myPosition.y -= CommonUtilities::Timer::GetDeltaTime() * DialogueManager::GetCurrentBlackbarSpeed() * 0.6f;
			}
		}
	}
	else
	{
		Move(aInputManager);
	}

	if (myIsShaking)
	{
		float dt = Timer::GetDeltaTime();
		myEffectTimer += dt;
		if (myEffectTimer < myEffectDuration)
		{
			myPosition.y += sinf(50.f * static_cast<float>(Timer::GetTotalTime())) * dt;
			myPosition.x += sinf(32.f * static_cast<float>(Timer::GetTotalTime())) * dt;
		}
		else
		{
			myEffectTimer = 0;
			myIsShaking = false;
		}
	}
}
void Camera::ConvertRenderBufferToCameraSpace(CommonUtilities::GrowingArray<Drawable*>& aSpriteBuffer)
{
	float heightOfScreen = 576.0f;
	float WidthOfScreen = 1024.0f;
	for (int i = 0; i < aSpriteBuffer.Size(); ++i)
	{
		Vector2f newPos = aSpriteBuffer[static_cast<unsigned short>(i)]->GetPosition();
		newPos -= myPosition;
		newPos *= myZoom;
		newPos += Vector2f(0.5f, 0.5f);
		aSpriteBuffer[static_cast<unsigned short>(i)]->SetPosition({ newPos.x, newPos.y });
		float xSize = (aSpriteBuffer[static_cast<unsigned short>(i)]->GetImagesSizePixel().x * TILE_MULTIPLIER / WidthOfScreen);
		float ySize = (aSpriteBuffer[static_cast<unsigned short>(i)]->GetImagesSizePixel().y * TILE_MULTIPLIER / heightOfScreen) * Tga2D::CEngine::GetInstance()->GetWindowRatioInversed();
		
		aSpriteBuffer[static_cast<unsigned short>(i)]->SetSizeRelativeToScreen({ ( xSize * myZoom), (ySize * myZoom) });
	}

}


const CommonUtilities::Vector2f Camera::ConvertPositionToCameraSpace(const CommonUtilities::Vector2f & aPosToConvert)
{
	Vector2f newPos;
	newPos += aPosToConvert;
	newPos -= myPosition;
	newPos *= myZoom;
	newPos.x += .5f;
	newPos.y += .5f;
	return newPos;
}

void Camera::Shake(const float aDuration)
{
	myIsShaking = true;
	myEffectDuration = aDuration;
}

void Camera::SetPosition(const Vector2f & aPosition)
{
	myHardSet = true;
	myPosition = aPosition;
}

void Camera::SetTarget(GameObject * aGameObject)
{
	myTarget = aGameObject;
}

const Vector2f & Camera::GetPosition() const
{
	return myPosition;
}

void Camera::SetMinEdges(CommonUtilities::Vector2f aMinEdges)
{
	myMinEdges = { aMinEdges.x - ((TILE_SIZE / 2.0f * TILE_MULTIPLIER) /  1920.0f), aMinEdges.y - ((TILE_SIZE / 2.0f * TILE_MULTIPLIER) /  1080.0f) };;
}

void Camera::SetMaxEdges(CommonUtilities::Vector2f aMaxEdges)
{
	myMaxEdges = { aMaxEdges.x + ((TILE_SIZE / 2.0f * TILE_MULTIPLIER) /  1920.0f), aMaxEdges.y + ((TILE_SIZE / 2.0f * TILE_MULTIPLIER) /  1080.0f) };
}




void Camera::LerpAndTrigger(GameObject* aNewTarget, const float aSpeedModifier)
{
	dynamic_cast<Player*>(myTarget)->SetPreventInput(true);
	myOldTarget = myTarget;
	myShouldTrigger = true;
	SetTarget(aNewTarget);
	myHardSet = false;
	myLerpSpeed = aSpeedModifier;
}

Camera::Camera()
{
	myScreenRatio = 1920.f / 1080.f;
}

void Camera::Move(InputManager* aInputManager)
{

	
	if (aInputManager != nullptr)
	{
		bool changedPositionManually = false;
		Vector2f myInitialPosition = myPosition;


		if (myPosition != myInitialPosition)
		{
			changedPositionManually = true;
		}

		if (changedPositionManually == false && myTarget != nullptr)
		{
			Vector2f targetPos = myTarget->GetPosition();
			
			if (myTarget->GetPosition().x + 0.5f >= myMaxEdges.x)
			{
				targetPos.x = myMaxEdges.x - 0.5f;
			}
			else if (myTarget->GetPosition().x - 0.5f <= myMinEdges.x)
			{
				targetPos.x = myMinEdges.x + 0.5f;
			}
			else
			{
				targetPos.x = myTarget->GetPosition().x;
			}

			if (myTarget->GetPosition().y + 0.5f >= myMaxEdges.y)
			{
				targetPos.y = myMaxEdges.y - 0.5f;
			}
			else if (myTarget->GetPosition().y - 0.5f <= myMinEdges.y)
			{
				targetPos.y = myMinEdges.y + 0.5f;
			}
			else
			{
				targetPos.y = myTarget->GetPosition().y;
			}
			

			if (myHardSet == false)
			{
				if ((targetPos - myPosition).Length2() > 0.000001f)
				{
					myPosition.x = cit::Lerp(myPosition.x, targetPos.x, myLerpSpeed * Timer::GetDeltaTime());
					myPosition.y = cit::Lerp(myPosition.y, targetPos.y, myLerpSpeed * Timer::GetDeltaTime());
				}
				else if (myShouldTrigger)
				{
					myTarget->OnTriggerd();
					myShouldTrigger = false;
					myWaitTimer = 0.f;
					myIsWaiting = true;
				}

				if (myIsWaiting)
				{
					myWaitTimer += Timer::GetDeltaTime();
				}
				if (myWaitTimer > 1.f)
				{
					myWaitTimer = 0.f;
					myIsWaiting = false;
					myLerpSpeed = 1.f;
					SetTarget(myOldTarget);
					dynamic_cast<Player*>(myTarget)->SetPreventInput(false);
				}
			}
			else
			{
				myPosition = targetPos;
				myHardSet = false;
			}
				
					
		}
	}
}


Camera::~Camera()
{
}

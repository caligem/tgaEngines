#pragma once
#include "Vector.h"
#include "GrowingArray.h"
#include <functional>

class GameObject;
class Drawable;

namespace CommonUtilities
{
	class InputManager;
}

class Camera
{
public:
	~Camera();

	static Camera& GetInstance()
	{
		static Camera instance;
		return instance;
	}

	void Init(const CommonUtilities::Vector2f& aPosition = {0, 0});
	void Update(CommonUtilities::InputManager* aInputManager = nullptr);
	void ConvertRenderBufferToCameraSpace(CommonUtilities::GrowingArray<Drawable*>& aSpriteBuffer);
	void Shake(const float aDuration);
	const CommonUtilities::Vector2f ConvertPositionToCameraSpace(const CommonUtilities::Vector2f &aPosToConvert);
	void SetPosition(const CommonUtilities::Vector2f& aPosition);
	void SetTarget(GameObject* aGameObject);
	const CommonUtilities::Vector2f& GetPosition() const;
	void SetMinEdges(CommonUtilities::Vector2f aMinEdges);
	void SetMaxEdges(CommonUtilities::Vector2f aMaxEdges);
	void LerpAndTrigger(GameObject* aNewTarget, const float aSpeedModifier);
private:
	Camera();
	void Move(CommonUtilities::InputManager* aInputManager);

	GameObject* myTarget;
	CommonUtilities::Vector2f myPosition;
	float myScreenRatio;
	float myZoom;

	float myEffectDuration;
	float myEffectTimer;
	CommonUtilities::Vector2f myMinEdges;
	CommonUtilities::Vector2f myMaxEdges;
	bool myIsShaking;
	bool myHardSet;

	GameObject* myOldTarget;
	bool myShouldTrigger;
	float myLerpSpeed;
	float myWaitTimer;
	bool myIsWaiting;
};


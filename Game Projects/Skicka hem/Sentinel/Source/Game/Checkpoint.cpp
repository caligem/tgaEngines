#include "stdafx.h"
#include "Checkpoint.h"
#include "Player.h"
#include "TileSize.h"
#include "JsonWrapper.h"
#include "Timer.h"
#include "SoundEngine.h"
Checkpoint::Checkpoint()
{
}

Checkpoint::~Checkpoint()
{
	mySound->Destroy();
}


void Checkpoint::Init(const char* aTypePath)
{
	json checkPointData = LoadObjectType(aTypePath);

	myTriggerAnimation = new Animation();
	myTriggerAnimation->Init(checkPointData["triggerAnimation"].get<std::string>().c_str());
	myTriggerAnimation->SetPosition(myPosition);

	myLitAnimation = new Animation();
	myLitAnimation->Init(checkPointData["endAnimation"].get<std::string>().c_str());
	myLitAnimation->SetPosition(myPosition);

	myCollider->SetIsSolid(false);
	myState = CheckpointState::NotActive;

	mySound = new Sound("Sounds/Sfx/checkpoint.wav");
}

void Checkpoint::OnUpdate()
{
	myDrawable->SetPosition({ myPosition.x, myPosition.y });
	
	
	if (myTimeBeforeChangeToFinalAnimation > 0.0f)
	{
		if (myState != CheckpointState::Active)
		{
			myTimeBeforeChangeToFinalAnimation -= CommonUtilities::Timer::GetDeltaTime();
		}
	}
	else if (myState == CheckpointState::Activated)
	{
		myDrawable = myLitAnimation;
		myTimeBeforeChangeToFinalAnimation = 0.0f;
	}
}

void Checkpoint::OnCollisionEnter(Collider  &other)
{
	if (other.GetTag() == ColliderTag::Player)
	{
		Player* player = static_cast<Player*>(other.GetOwner());
		player->SetRespawnPosition({ myPosition.x, myPosition.y + (TILE_SIZE / 2.0f * TILE_MULTIPLIER / 1080.0f) });
		player->LockCollectibles();
		myState = CheckpointState::Activated;
		myDrawable = myTriggerAnimation;
		myTimeBeforeChangeToFinalAnimation = dynamic_cast<Animation*>(myDrawable)->GetTotalTime();
		SoundEngine::PlaySound(*mySound);
		if (player->HasHook())
		{
			player->SetShouldRespawnWithHook(true);
		}
	}
}

void Checkpoint::OnCollisionStay(Collider  &other)
{
	other;
}

void Checkpoint::OnCollisionLeave(Collider  &other)
{
	other;
}

#pragma once
#include "GameObject.h"

class Sound;


class Checkpoint : public GameObject
{
	enum class CheckpointState
	{
		NotActive,
		Activated,
		Active
	};

public:
	Checkpoint();

	~Checkpoint();
	void Init(const char* aTypePath);
	void OnUpdate() override;
	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionStay(Collider  &other) override;
	void OnCollisionLeave(Collider  &other) override;

private:
	CheckpointState myState;

	Drawable *myLitAnimation;
	Drawable *myTriggerAnimation;
	float myTimeBeforeChangeToFinalAnimation;
	Sound* mySound;
};


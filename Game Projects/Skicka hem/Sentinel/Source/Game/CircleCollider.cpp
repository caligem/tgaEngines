#include "stdafx.h"
#include "CircleCollider.h"
#include "LineCollider.h"
#include "GameObject.h"


CircleCollider::CircleCollider()
{
}

CircleCollider::CircleCollider(float aRadius, CommonUtilities::Vector2f & aOffset)
{
	myRadius = aRadius;
	myOffset = aOffset;
}


CircleCollider::~CircleCollider()
{
	myGameObject = nullptr;
}

void CircleCollider::Init(float aRadius, CommonUtilities::Vector2f  aOffset)
{
	myRadius = aRadius;
	myOffset = aOffset;
}

bool CircleCollider::IsCollidedWith(Collider *aCol)
{
	if (this != aCol  && aCol->GetTag() != ColliderTag::None)
	{
		CommonUtilities::Vector2f colPos = { myGameObject->GetPosition().x + myOffset.x, myGameObject->GetPosition().y + myOffset.y };
		return aCol->IsCollidedWith(this, colPos);
	}
	return false;
}
bool CircleCollider::IsCollidedWith(BoxCollider * aBoxCol, const CommonUtilities::Vector2f &aGOPos)
{
	CommonUtilities::Vector2f vector1 = { myGameObject->GetPosition().x + myOffset.x, myGameObject->GetPosition().y + myOffset.y };

	return CircleWithBoxCollision(vector1, myRadius, aGOPos, aBoxCol->GetWidth(), aBoxCol->GetHeight());
}

bool CircleCollider::IsCollidedWith(CircleCollider * aCircleCol, const CommonUtilities::Vector2f &aGOPos)
{
	CommonUtilities::Vector2f vector1 = { myGameObject->GetPosition().x + myOffset.x, myGameObject->GetPosition().y + myOffset.y };
	return CircleWithCircleCollision(vector1, myRadius, aGOPos, aCircleCol->myRadius);
}

bool CircleCollider::IsCollidedWith(LineCollider * aLineCol, const CommonUtilities::Vector2f & aGOPos)
{
	CommonUtilities::Vector2f vector1 = { myGameObject->GetPosition().x + myOffset.x, myGameObject->GetPosition().y + myOffset.y };
	return CircleWithLineCollision(vector1, myRadius, aGOPos, aGOPos + (aLineCol->GetDirection() * aLineCol->GetLength()));
}

void CircleCollider::Draw()
{
}

CommonUtilities::Vector2f CircleCollider::RelativePoint(const CommonUtilities::Vector2f & aPoint)
{
	aPoint;
	return CommonUtilities::Vector2f();
}

CommonUtilities::Vector2f & CircleCollider::GetOffset()
{
	return myOffset;
}

const float CircleCollider::GetRadius() const
{
	return myRadius;
}

#pragma once
#include "Collider.h"
#include "Vector.h"
class CircleCollider : public Collider
{
public:
	CircleCollider();
	CircleCollider(float aRadius, CommonUtilities::Vector2f & aOffset);
	~CircleCollider();

	void Init(float aRadius, CommonUtilities::Vector2f  aOffset);
	bool IsCollidedWith(Collider *aCol) override;
	bool IsCollidedWith(BoxCollider *aBoxCol, const CommonUtilities::Vector2f &aGOPos) override;
	bool IsCollidedWith(CircleCollider *aCircleCol, const CommonUtilities::Vector2f &aGOPos) override;
	bool IsCollidedWith(LineCollider *aLineCol, const CommonUtilities::Vector2f &aGOPos) override;

	void Draw() override;
	CommonUtilities::Vector2f RelativePoint(const CommonUtilities::Vector2f &aPoint) override;

	CommonUtilities::Vector2f & GetOffset() override;
	const float GetRadius() const;
private:
	float myRadius;
};


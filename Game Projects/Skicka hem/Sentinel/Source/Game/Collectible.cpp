#include "stdafx.h"
#include "Collectible.h"
#include "ParticleEmitterManager.h"
Collectible::Collectible()
{
	myCurrentState = ColState::Free;
	myPickupEmitter = ParticleEmitterManager::SpawnOwnedEmitterByIndex(0);
}


Collectible::~Collectible()
{
}

void Collectible::SetState(ColState aState) 
{
	myCurrentState = aState;
}

ColState Collectible::GetState()
{
	return myCurrentState;
}

void Collectible::OnUpdate()
{
	myCollider->SetIsSolid(false);
	myDrawable->SetPosition(myPosition);
	myPickupEmitter.SetPosition({myPosition.x, myPosition.y});
}

void Collectible::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*> &aRenderBuffer)
{
	if (myCurrentState == ColState::Free)
	{
		myDrawable->SetPosition(myPosition);
		aRenderBuffer.Add(myDrawable);
	}
	else if (myCurrentState == ColState::PickedUp)
	{
		aRenderBuffer.Add(&myPickupEmitter);
	}
}

void Collectible::ResetEmitter()
{
	myPickupEmitter = ParticleEmitterManager::SpawnOwnedEmitterByIndex(0);
}



#pragma once
#include "GameObject.h"
#include "ParticleEmitter.h"

enum class ColState
{
	Free,
	PickedUp,
	Locked
};

class Collectible :
	public GameObject
{
public:
	Collectible();
	~Collectible();

	void SetState(ColState aState);

	ColState GetState();

	void OnUpdate() override;

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;
	void ResetEmitter();
private:
	ParticleEmitter myPickupEmitter;
	ColState myCurrentState;
};


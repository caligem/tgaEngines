#include "stdafx.h"
#include "CollectibleCounter.h"
#include "tga2d\sprite\sprite.h"
#include "Text.h"
#include "Animation.h"
#include <sstream>
#include "InputWrapper.h"
#include "Drawable.h"
#include "Timer.h"
#include "tga2d\engine.h"


CollectibleCounter::CollectibleCounter()
{
}


CollectibleCounter::~CollectibleCounter()
{
}
 
void CollectibleCounter::Init(int aCurrentCollectibles, int aMaxCollectibles, bool aIsStatic)
{
	myRenderBuffer.Init(6);

	myShouldFadeIn = false;
	myShouldFadeOut = false;
	myWaitingToDissapear = false;
	myTimer = 0.f;
	myTimeToBeShown = 2.0f;

	if (aIsStatic) 
	{
		myAlpha = 1.0f;
	}
	else 
	{
		myAlpha = 0.0f;
	}

	myFadeSpeed = 1.0f;

	myPosition = { 0.15f, 0.1f };

	myAnimation = new Animation();
	myAnimation->Init("collectibleCounterIcon");
	myAnimation->SetPosition({ myPosition.x, myPosition.y - 0.015f });
	myAnimation->SetColor({ 1.0f, 1.0f, 1.0f, myAlpha });

	float scale = Tga2D::CEngine::GetInstance()->GetWindowSize().x / 1920.f;

	if (aIsStatic) 
	{
		//myAnimation->SetType(Animation::Type::Static);
	}

	myText = new Text();
	myText->Init("Text/Mecha.ttf", Text::EFontSize_36, 0);
	myTextColor = CommonUtilities::Vector4f(1.f, 1.f, 1.f, myAlpha); 
	myText->SetScale(scale);


	std::stringstream ss;
	
	ss << aCurrentCollectibles << "/" << aMaxCollectibles;

	myText->SetText(ss.str());
	myText->SetPosition(myPosition);
	myText->SetColor(Vector4f(1.0f, 1.0f, 1.0f, myAlpha));

}

void CollectibleCounter::Appear() 
{
	if (!myWaitingToDissapear && !myShouldFadeOut) 
	{
		myShouldFadeIn = true;
	}
}

void CollectibleCounter::SetPosition(const CommonUtilities::Vector2f& aPos)
{
	myPosition = aPos; 

	float scale = Tga2D::CEngine::GetInstance()->GetWindowSize().x / 1920.f;
	myText->SetScale(scale);
	myAnimation->SetPosition({ myPosition.x + 0.040f, myPosition.y - 0.015f });
	myText->SetPosition(myPosition);
}

void CollectibleCounter::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(myText);
	aRenderBuffer.Add(myAnimation);
}

void CollectibleCounter::UpdateCounter(int aCurrentCollectibles, int aMaxCollectibles)
{
	myRenderBuffer.RemoveAll();

	std::stringstream ss;
	ss << aCurrentCollectibles << "/" << aMaxCollectibles;
	myText->SetText(ss.str());

	if (myShouldFadeIn) 
	{
		if (FadeIn(Timer::GetDeltaTime()))
		{
			myShouldFadeIn = false;
			myWaitingToDissapear = true;
		}
	}
	else if (myWaitingToDissapear) 
	{
		myTimer += Timer::GetDeltaTime();

		if (myTimer > myTimeToBeShown) 
		{
			myTimer = 0.f;
			myWaitingToDissapear = false;
			myShouldFadeOut = true;
		}
	}
	else if (myShouldFadeOut) 
	{
		if (FadeOut(Timer::GetDeltaTime()))
		{
			myShouldFadeOut = false;
		}
	}
}

bool CollectibleCounter::FadeOut(float aDeltaTime)
{
	if (myAlpha > 0) 
	{
		myAlpha -= aDeltaTime * myFadeSpeed;

		if (myAlpha < 0) 
		{
			myAlpha = 0;
			myAnimation->SetColor({ 1.0f, 1.0f, 1.0f, myAlpha });
			myText->SetColor(Vector4f(myTextColor.x, myTextColor.y, myTextColor.z, myAlpha));
			return true;
		}
	}


	myAnimation->SetColor({ 1.0f, 1.0f, 1.0f, myAlpha });
	myText->SetColor(Vector4f(myTextColor.x, myTextColor.y, myTextColor.z, myAlpha));

	return false;
}

bool CollectibleCounter::FadeIn(float aDeltaTime)
{
	if (myAlpha < 1.0f)
	{
		myAlpha += aDeltaTime * myFadeSpeed;

		if (myAlpha > 1.0f)
		{
			myAlpha = 1.0f;
			myAnimation->SetColor({ 1.0f, 1.0f, 1.0f, myAlpha });
			myText->SetColor(Vector4f(myTextColor.x, myTextColor.y, myTextColor.z, myAlpha));
			return true;
		}
	}

	myAnimation->SetColor({ 1.0f, 1.0f, 1.0f, myAlpha });
	myText->SetColor(Vector4f(myTextColor.x, myTextColor.y, myTextColor.z, myAlpha));
	return false;
}

void CollectibleCounter::Render()
{
	if (myRenderBuffer.Size() < 2)
	{
		FillRenderBuffer(myRenderBuffer);
	}

	for (unsigned short i = 0; i < myRenderBuffer.Size(); i++)
	{
		myRenderBuffer[i]->Render();
	}
}

#pragma once
#include "Vector2.h"
#include "GrowingArray.h"

namespace Tga2D 
{
	class CSprite;
}

class Text;
class Animation;
class Drawable;

class CollectibleCounter
{
public:
	CollectibleCounter();
	~CollectibleCounter();

	void Init(int aCurrentCollectibles = 0, int aMaxCollectibles = 0, bool aIsStatic = true);
	void Appear();
	void SetPosition(const CommonUtilities::Vector2f & aPos);
	void UpdateCounter(int aCurrentCollectibles, int aMaxCollectibles);

	void Render();

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer);
private:

	bool FadeOut(float aDeltaTime);
	bool FadeIn(float aDeltaTime);


	CommonUtilities::Vector4f myTextColor;

	Text* myText;
	Animation* myAnimation;

	CommonUtilities::Vector2f myPosition;

	float myAlpha;
	float myFadeSpeed;
	float myTimer;
	float myTimeToBeShown;

	bool myShouldFadeIn;
	bool myShouldFadeOut;
	bool myWaitingToDissapear;


	CommonUtilities::GrowingArray<Drawable*> myRenderBuffer;



};

#include "stdafx.h"
#include "Collider.h"
#include "GameObject.h"
#include "Camera.h"

#define MAX_COLLISIONS 16

Collider::Collider()
{
	myTag = ColliderTag::None;
	myIsSolid = true;
	myCollisions.Init(MAX_COLLISIONS);
}


Collider::~Collider()
{
	myGameObject = nullptr;
}

void Collider::UpdateAllColliders(const CommonUtilities::GrowingArray<GameObject*> &aGrowingArrayOfGameObjects)
{
	const int AmountOfGOs = aGrowingArrayOfGameObjects.Size();
	for (unsigned short i = 0; i < AmountOfGOs; i++)
	{
		if (aGrowingArrayOfGameObjects[i]->GetCollider() != nullptr && aGrowingArrayOfGameObjects[i]->GetCollider()->GetTag() != ColliderTag::Tile)
		{
			if (aGrowingArrayOfGameObjects[i]->myShouldRemove == false && aGrowingArrayOfGameObjects[i]->GetCollider()->GetTag() != ColliderTag::None)
			{
				if (aGrowingArrayOfGameObjects[i]->GetCollider() != nullptr)
				{
					aGrowingArrayOfGameObjects[i]->GetCollider()->CheckIfCollided(aGrowingArrayOfGameObjects);
					aGrowingArrayOfGameObjects[i]->GetCollider()->Update();
				}
			}
		}
	}
}
void Collider::UpdateAgainstList(GameObject & aGO, CommonUtilities::GrowingArray<GameObject*>& aGrowingArrayOfGameObjects)
{
	if (aGO.myShouldRemove == false && aGO.GetCollider()->GetTag() != ColliderTag::None &&
		aGO.GetPosition().x < Camera::GetInstance().GetPosition().x + 0.6f &&
		aGO.GetPosition().x > Camera::GetInstance().GetPosition().x - 0.6f &&
		aGO.GetPosition().y < Camera::GetInstance().GetPosition().y + 0.6f &&
		aGO.GetPosition().y > Camera::GetInstance().GetPosition().y - 0.6f)
	{
		if (aGO.GetCollider() != nullptr)
		{
			aGO.GetCollider()->CheckIfCollided(aGrowingArrayOfGameObjects);
			aGO.GetCollider()->Update();
		}
	}
}

void Collider::DrawAllColliders(const CommonUtilities::GrowingArray<GameObject*>& aGrowingArrayOfGameObjects)
{
	const int AmountOfGOs = aGrowingArrayOfGameObjects.Size();
	for (unsigned short i = 0; i < AmountOfGOs; i++)
	{
		if (aGrowingArrayOfGameObjects[i]->GetCollider() != nullptr &&
			aGrowingArrayOfGameObjects[i]->GetPosition().x < Camera::GetInstance().GetPosition().x + 0.6f &&
			aGrowingArrayOfGameObjects[i]->GetPosition().x > Camera::GetInstance().GetPosition().x - 0.6f &&
			aGrowingArrayOfGameObjects[i]->GetPosition().y < Camera::GetInstance().GetPosition().y + 0.6f &&
			aGrowingArrayOfGameObjects[i]->GetPosition().y > Camera::GetInstance().GetPosition().y - 0.6f)
		{
			if (aGrowingArrayOfGameObjects[i]->myShouldRemove == false && aGrowingArrayOfGameObjects[i]->GetCollider()->GetTag() != ColliderTag::None)
			{
				if (aGrowingArrayOfGameObjects[i]->GetCollider() != nullptr)
				{
					aGrowingArrayOfGameObjects[i]->GetCollider()->Draw();
				}
			}
		}
	}
}

void Collider::Draw()
{
}

void Collider::SetOwner(GameObject * aOwner)
{
	myGameObject = aOwner;
}
GameObject* Collider::GetOwner()
{
	return myGameObject;
}

void Collider::SetTag(ColliderTag aTag)
{
	myTag = aTag;
}

ColliderTag Collider::GetTag() const
{
	return myTag;
}

void Collider::CheckIfCollided(const CommonUtilities::GrowingArray<GameObject*> &aGrowingArrayOfGameObjects)
{
	const int AmountOfGOs = aGrowingArrayOfGameObjects.Size();
	for (unsigned short i = 0; i < AmountOfGOs; i++)
	{
		if (aGrowingArrayOfGameObjects[i]->GetCollider() != nullptr)
		{
			if ((myTag == ColliderTag::Player && aGrowingArrayOfGameObjects[i]->GetCollider()->myTag == ColliderTag::Hook) || (myTag == ColliderTag::Hook && aGrowingArrayOfGameObjects[i]->GetCollider()->myTag == ColliderTag::Player))
				continue;
			if (myTag != ColliderTag::Grabbable && myTag == aGrowingArrayOfGameObjects[i]->GetCollider()->myTag)
				continue;
			if (aGrowingArrayOfGameObjects[i]->GetCollider()->myTag == ColliderTag::GroundChecker)
				continue;
			if (this != aGrowingArrayOfGameObjects[i]->GetCollider() && aGrowingArrayOfGameObjects[i]->myShouldRemove == false && aGrowingArrayOfGameObjects[i]->GetCollider() && aGrowingArrayOfGameObjects[i]->GetCollider()->GetTag() != ColliderTag::None)
			{
				if (aGrowingArrayOfGameObjects[i]->GetCollider()->IsCollidedWith(this))
				{
					AddToHit(aGrowingArrayOfGameObjects[i]->GetCollider());
				}
			}
		}
	}
}

bool Collider::IsColliding()
{
	return myCollisions.Size() > 0;
}

Collider::Hit Collider::CollisionDirectionWith(Collider * aCol)
{
	return Hit();
}

Collider::Hit Collider::CollisionDirectionWith(BoxCollider * aCol)
{
	return Hit();
}

bool Collider::PointInBox(CommonUtilities::Vector2f aBox1, float aBox1Width, float aBox1Height, CommonUtilities::Vector2f aPoint)
{
	float minX1 = aBox1.x - (aBox1Width / 2);

	float minY1 = aBox1.y - (aBox1Height / 2);

	if (minX1 < aPoint.x &&
		minX1 + aBox1Width > aPoint.x &&
		minY1 < aPoint.y &&
		aBox1Height + minY1 > aPoint.y)
	{
		return true;
	}
	return false;
}

void Collider::Update()
{
	for (unsigned short i = 0; i < myCollisions.Size(); i++)
	{
		if (myCollisions[i].myColStateWithOther == CollisionState::Enter || myCollisions[i].myColStateWithOther == CollisionState::Stay)
		{
			if (myCollisions[i].myHitThisFrame == true)
			{
				myCollisions[i].myColStateWithOther = CollisionState::Stay;
				myGameObject->OnCollisionStay(*myCollisions[i].myOtherCol);
			}
			else
			{
				myCollisions[i].myColStateWithOther = CollisionState::Leave;
				myGameObject->OnCollisionLeave(*myCollisions[i].myOtherCol);
			}
		}
		else if (myCollisions[i].myColStateWithOther == CollisionState::Leave)
		{
			if (myCollisions[i].myHitThisFrame == true)
			{
				myCollisions[i].myColStateWithOther = CollisionState::Enter;
				myGameObject->OnCollisionEnter(*myCollisions[i].myOtherCol);
			}
			else
			{
				myCollisions.RemoveCyclicAtIndex(i);
			}
		}
		else
		{
			myCollisions[i].myColStateWithOther = CollisionState::Enter;
			myGameObject->OnCollisionEnter(*myCollisions[i].myOtherCol);
		}
		if (i < myCollisions.Size())
		{
			myCollisions[i].myHitThisFrame = false;
		}
	}
}

void Collider::AddToHit(Collider * aOther)
{
	for (unsigned short i = 0; i < myCollisions.Size(); i++)
	{
		if (myCollisions[i].myOtherCol == aOther)
		{
			myCollisions[i].myHitThisFrame = true;
			return;
		}
	}
	if (myCollisions.Size() < MAX_COLLISIONS)
	{
		myCollisions.Add(Collision());
		myCollisions.GetLast().myColStateWithOther = CollisionState::Fresh;
		myCollisions.GetLast().myOtherCol = aOther;
	}
}

void Collider::SetOffset(const CommonUtilities::Vector2f & aOffset)
{
	myOffset = aOffset;
}

const bool Collider::GetIsSolid() const
{
	return myIsSolid;
}

void Collider::SetIsSolid(const bool aValueToSet)
{
	myIsSolid = aValueToSet;
}

bool Collider::CircleWithBoxCollision(CommonUtilities::Vector2f aCircleCenter, float aCircleRadius, CommonUtilities::Vector2f aBox2, float aBox2Width, float aBox2Height)
{
	CommonUtilities::Vector2f boxMin = { aBox2.x - (aBox2Width / 2), aBox2.y - (aBox2Height / 2) };
	CommonUtilities::Vector2f boxMax = { boxMin.x + aBox2Width, boxMin.y + aBox2Height };

	float radius2 = aCircleRadius*aCircleRadius;
	if ((aCircleCenter - boxMin).Length2() < radius2)
	{
		return true;
	}
	if ((aCircleCenter - boxMax).Length2() < radius2)
	{
		return true;
	}
	if ((aCircleCenter - CommonUtilities::Vector2f{ boxMin.x, boxMax.y }).Length2() < radius2)
	{
		return true;
	}
	if ((aCircleCenter - CommonUtilities::Vector2f{ boxMax.x, boxMin.y }).Length2() < radius2)
	{
		return true;
	}

	if (aCircleCenter.x < boxMax.x + aCircleRadius &&
		aCircleCenter.x > boxMin.x - aCircleRadius &&
		aCircleCenter.y < boxMax.y + aCircleRadius &&
		aCircleCenter.y > boxMin.y - aCircleRadius)
	{
		return true;
	}
	return false;
}

bool Collider::CircleWithCircleCollision(CommonUtilities::Vector2f aCircle1, float aCircleRadius1, CommonUtilities::Vector2f aCircle2, float aCircleRadius2)
{
	if ((aCircle1 - aCircle2).Length2() < (aCircleRadius1 * aCircleRadius1) + (aCircleRadius2 * aCircleRadius2))
	{
		return true;
	}
	return false;
}

bool Collider::CircleWithLineCollision(CommonUtilities::Vector2f aCircle, float aCircleRadius, CommonUtilities::Vector2f aLineStart, CommonUtilities::Vector2f aLineEnd)
{
	CommonUtilities::Vector2f aLineStartLoc = aLineStart - aCircle;
	CommonUtilities::Vector2f aLineEndLoc = aLineEnd - aCircle;
	
	CommonUtilities::Vector2f P2MinusP1 = aLineEndLoc - aLineStartLoc;

	float a = (P2MinusP1.x) * (P2MinusP1.x) + (P2MinusP1.y) * (P2MinusP1.y);
	float b = 2 * ((P2MinusP1.x * aLineStartLoc.x) + (P2MinusP1.y * aLineStartLoc.y));
	float c = (aLineStartLoc.x * aLineStartLoc.x) + (aLineStartLoc.y * aLineStartLoc.y) -(aCircleRadius * aCircleRadius);
	float delta = b * b -(4 * a * c);
	if (delta == 0) // 1
	{
		return true;
	}
	else if (delta > 0) // 2
	{
		return true;
	}
	return false;
}

bool Collider::BoxWithBoxCollision(CommonUtilities::Vector2f aBox1, float aBox1Width, float aBox1Height, CommonUtilities::Vector2f aBox2, float aBox2Width, float aBox2Height)
{
	float minX1 = aBox1.x - (aBox1Width / 2);
	float minX2 = aBox2.x - (aBox2Width / 2);

	float minY1 = aBox1.y - (aBox1Height / 2);
	float minY2 = aBox2.y - (aBox2Height / 2);

	CommonUtilities::Vector2f minPos1(minX1, minY1);
	CommonUtilities::Vector2f maxPos1(minX1 + aBox1Width, minY1 + aBox1Height);
	CommonUtilities::Vector2f minPos2(minX2, minY2);
	CommonUtilities::Vector2f maxPos2(minX2 + aBox2Width, minY2 + aBox2Height);

	if (minPos1.x < maxPos2.x &&
		maxPos1.x > minPos2.x &&
		minPos1.y < maxPos2.y &&
		maxPos1.y > minPos2.y)
	{
		return true;
	}
	return false;
}

bool Collider::LineWithLineCollision(CommonUtilities::Vector2f aLineStart1, CommonUtilities::Vector2f aLineEnd1, CommonUtilities::Vector2f aLineStart2, CommonUtilities::Vector2f aLineEnd2)
{
	CommonUtilities::Vector2f s1 = aLineEnd1 - aLineStart1;
	CommonUtilities::Vector2f s2 = aLineEnd2 - aLineStart2;

	float s, t;
	s = (-s1.y * (aLineStart1.x - aLineStart2.x) + s1.x * (aLineStart1.y - aLineStart2.y)) / (-s2.x * s1.y + s1.x * s2.y);
	t = (s2.x * (aLineStart1.y - aLineStart2.y) - s2.y * (aLineStart1.x - aLineStart2.x)) / (-s2.x * s1.y + s1.x * s2.y);

	if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
	{
		return true;
	}
	return false;
}

bool Collider::BoxWithLineCollision(CommonUtilities::Vector2f aBox, float aBoxWidth, float aBoxHeight, CommonUtilities::Vector2f aLineStart, CommonUtilities::Vector2f aLineEnd)
{
	CommonUtilities::Vector2f A = { aBox.x - (aBoxWidth / 2.0f),aBox.y - (aBoxHeight / 2.0f) };
	CommonUtilities::Vector2f B = { aBox.x + (aBoxWidth / 2.0f),aBox.y - (aBoxHeight / 2.0f) };
	CommonUtilities::Vector2f C = { aBox.x + (aBoxWidth / 2.0f),aBox.y + (aBoxHeight / 2.0f) };
	CommonUtilities::Vector2f D = { aBox.x - (aBoxWidth / 2.0f),aBox.y + (aBoxHeight / 2.0f) };

	if (LineWithLineCollision(A, B, aLineStart, aLineEnd))
	{
		return true;
	}
	if (LineWithLineCollision(B, C, aLineStart, aLineEnd))
	{
		return true;
	}
	if (LineWithLineCollision(C, D, aLineStart, aLineEnd))
	{
		return true;
	}
	if (LineWithLineCollision(D, A, aLineStart, aLineEnd))
	{
		return true;
	}
	if (PointInBox(aBox, aBoxWidth, aBoxHeight, aLineStart))
	{
		return true;
	}
	if (PointInBox(aBox, aBoxWidth, aBoxHeight, aLineEnd))
	{
		return true;
	}
	return false;
}

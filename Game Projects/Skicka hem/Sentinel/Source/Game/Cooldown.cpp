#include "stdafx.h"
#include "Cooldown.h"


Cooldown::Cooldown()
	: myFunction(NULL)
	, myRunCode(false)
{
}


Cooldown::~Cooldown()
{
}

void Cooldown::SetTimer(float aCooldownTimer, std::function<void()> aFunction)
{
	myCooldownTimer = aCooldownTimer;
	myFunction = aFunction;
	myRunCode = true;
}

void Cooldown::Update()
{
	myCooldownTimer -= CommonUtilities::Timer::GetDeltaTime();

	if (myCooldownTimer <= 0.f && myRunCode)
	{
		myFunction();
		myRunCode = false;
	}
} 

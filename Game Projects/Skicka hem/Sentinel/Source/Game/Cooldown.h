#pragma once
#include "Timer.h"
#include <functional>

class Cooldown
{
public:
	Cooldown();
	~Cooldown();
	
	void SetTimer(float aCooldownTimer, std::function<void()> aFunction);
	void Update();

private:
	std::function<void()> myFunction;

	bool myRunCode;
	float myCooldownTimer;
};
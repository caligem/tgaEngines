#include "stdafx.h"
#include "CreditsState.h"
#include "tga2d\sprite\sprite.h"
#include "InputWrapper.h"
#include "Timer.h"
#include "SoundEngine.h"
#include "StateStack.h"

CreditsState::CreditsState(CommonUtilities::InputManager* aInputManager, bool aShouldPushLevelCompleted)
	: myShouldPushLevelCompleted(aShouldPushLevelCompleted)
	, myInputManager(aInputManager)
{
}

CreditsState::~CreditsState()
{
	delete myTextSprite;
	myTextSprite = nullptr;

	delete myBackgroundSprite;
	myBackgroundSprite = nullptr;

	myMusic->Destroy();
}

void CreditsState::Init()
{
	myBackgroundSprite = new Tga2D::CSprite("Sprites/white_pixel.dds");
	myBackgroundSprite->SetSizeRelativeToScreen({ 2.0f, 1.0f });
	myBackgroundSprite->SetColor({0.0f, 0.0f, 0.0f, 1.0f});
	myBackgroundSprite->SetPivot({ 0.5f, 0.5f });
	myBackgroundSprite->SetPosition({0.5f, 0.5f});

	myTextSprite = new Tga2D::CSprite("Sprites/credits_text.dds");
	myTextSprite->SetSizeRelativeToImage({ 3.5f, 3.5f });
	myTextSprite->SetPivot({ 0.5f, 1.0f });

	myPosition = { 0.5f, 1.0f + (myTextSprite->GetSize().y) };

	myScrollSpeed = 0.15f;
	myIsDone = false;
	myMaxPosition = (0.0f);

	myMusic = new Sound("Sounds/Music/credits.ogg");
	myMusic->SetAsGenericMusic();
}

void CreditsState::Render() 
{
	myBackgroundSprite->Render();
	myTextSprite->Render();
}

eStateStackMessage CreditsState::Update()
{
	while (ShowCursor(false) >= 0);
	myPosition.y -= (Timer::GetDeltaTime() * myScrollSpeed);
	myTextSprite->SetPosition({ myPosition.x, myPosition.y });

	if (InputWrapper::IsButtonPressed(KeyButton::Back) || myPosition.y < myMaxPosition)
	{
		myIsDone = true;
	}

	if (myIsDone) 
	{
		if (myShouldPushLevelCompleted)
		{
			return eStateStackMessage::PopCreditsAndPushLevelCompleted;
		}
		else
		{
			return eStateStackMessage::PopSubState;
		}
	}

	return eStateStackMessage::KeepState;

}

void CreditsState::OnEnter()
{
	Init();
	SoundEngine::PlaySound(*myMusic);
}

void CreditsState::OnExit()
{
}


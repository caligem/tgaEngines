#pragma once
#include "Vector2.h"
#include "GameState.h"

class Sound;

namespace Tga2D 
{
	class CSprite;
}

class CreditsState : public GameState
{
public:
	CreditsState(CommonUtilities::InputManager* aInputManager, bool aShouldPushLevelCompleted = false);
	~CreditsState(); 

	void Init() override;
	void Render() override;
	eStateStackMessage Update() override;

	void OnEnter() override;
	void OnExit() override;

private:
	CommonUtilities::InputManager* myInputManager;

	Tga2D::CSprite* myTextSprite;
	Tga2D::CSprite* myBackgroundSprite;
	CommonUtilities::Vector2f myPosition;

	bool myShouldPushLevelCompleted;

	bool myIsDone;
	float myScrollSpeed;

	Sound* myMusic;

	float myMaxPosition;
};


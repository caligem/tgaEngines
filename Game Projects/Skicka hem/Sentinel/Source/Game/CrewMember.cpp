#include "stdafx.h"
#include "CrewMember.h"
#include "DialogueManager.h"
#include "Timer.h"
#include "PostMaster.h"
#include "Player.h"

CrewMember::CrewMember()
	: myDialogIndex(0)
	, myHasCollided(false)
	, myState(eState::Idle)
	, myMoveSpeed(0.f)
	, myPlayer (nullptr)
{
}


CrewMember::~CrewMember()
{
}

void CrewMember::Init(int aDialogIndex, float aDirection, bool aIsFlipped, uint16_t aLevelIndex)
{
	myLevelIndex = aLevelIndex;
	myDialogIndex = aDialogIndex;
	myDirection = aDirection;
	static_cast<Animation*>(myDrawable)->SetInversedX(aIsFlipped);
	
	if (myLevelIndex == 2)
	{
		myShouldWalk = false;
	}
	else
	{
		myShouldWalk = true;
		InitRunningAnimation(aIsFlipped);
	}

}

void CrewMember::OnUpdate()
{
	GameObject::OnUpdate();

	if (IsReady())
	{
		SendMessageToPostMaster();
	}

	if (myState == eState::Walking)
	{
		myPosition.x += myMoveSpeed * myDirection *CommonUtilities::Timer::GetDeltaTime();
	}
}

void CrewMember::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	if (myState == eState::Walking)
	{
		myRunningAnimation.SetPosition(myPosition);
		aRenderBuffer.Add(&myRunningAnimation);
	}
	else if (myState == eState::Idle)
	{
		myDrawable->SetPosition(myPosition);
		aRenderBuffer.Add(myDrawable);
	}
}

void CrewMember::OnCollisionEnter(Collider & other)
{

}

void CrewMember::OnCollisionStay(Collider & other)
{
	if (!myHasCollided && other.GetTag() == ColliderTag::Player)
	{
		myPlayer = static_cast<Player*>(other.GetOwner());
		myCollider->SetIsSolid(false);
		myPlayer->SetAnimationStateToIdle();
		myMoveSpeed = myPlayer->GetMoveSpeed();
		DialogueManager::PlayDialogue(myDialogIndex);
		myHasCollided = true;
	}
}

void CrewMember::OnCollisionLeave(Collider & other)
{
}

bool CrewMember::IsReady()
{
	if (myHasCollided && !DialogueManager::IsPlaying() && myState == eState::Idle)
	{
		return true;
	}

	return false;
}

void CrewMember::SendMessageToPostMaster()
{
	if (myShouldWalk)
	{
		Message newMessage;
		newMessage.myMessageType = MessageType::LevelCompleted;
		PostMaster::SendMessages(newMessage);
		myState = eState::Walking;
	}
	else
	{
		myPlayer->LockCollectibles();
		Message newMessage;
		newMessage.myMessageType = MessageType::ChangeLevel;
		PostMaster::SendMessages(newMessage);
	}


}

void CrewMember::InitRunningAnimation(bool aIsFlipped)
{
	std::string animationKey = "Sprites/level" + std::to_string(myLevelIndex) + "/crewmember_run.dds";
	myRunningAnimation.Init(animationKey.c_str());
	myRunningAnimation.SetPosition(myPosition);
	myRunningAnimation.SetInversedX(aIsFlipped);

}

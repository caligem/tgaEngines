#pragma once
#include "GameObject.h"
#include "Player.h"

class CrewMember : public GameObject
{
public:
	enum class eState
	{
		Idle,
		Walking
	};

	CrewMember();
	~CrewMember();

	void Init(int aDialogIndex, float aDirection, bool aIsFlipped, uint16_t aLevelIndex);
	void OnUpdate() override;

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*> &aRenderBuffer) override;
	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionStay(Collider  &other) override;
	void OnCollisionLeave(Collider  &other) override;

	const float GetDirection() { return myDirection; }

private:
	eState myState;
	float myDirection;
	int myDialogIndex;
	bool myHasCollided;
	float myMoveSpeed;

	bool IsReady();
	void SendMessageToPostMaster();
	bool myShouldWalk;

	void InitRunningAnimation(bool aIsFlipped);
	Animation myRunningAnimation;

	uint16_t myLevelIndex;

	Player *myPlayer;
};


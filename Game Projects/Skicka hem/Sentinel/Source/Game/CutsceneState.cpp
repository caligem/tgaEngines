#include "stdafx.h"
#include "CutsceneState.h"
#include "InputWrapper.h"
#include <tga2d\videoplayer\video.h>
#include "Timer.h"
#include "cit\utility.h"
#include "Fader.h"
#include <iostream>
#include "SoundEngine.h"

CutsceneState::CutsceneState(InGameState::eLoadingState &aLoadingState, int aCutsceneIndex, eCutsceneType aCutsceneType)
{
	myVid = new Tga2D::CVideo();
	myLoadingState = &aLoadingState;
	myCutsceneIndex = aCutsceneIndex;
	myCutsceneType = aCutsceneType;
	myVidSound = nullptr;
}


CutsceneState::~CutsceneState()
{
	cit::SafeDelete(myVid);

	if(myVidSound != nullptr)
	{
		myVidSound->Destroy();
	}

	myLoadingState = nullptr;
}

void CutsceneState::Init()
{
	myLoadingAnim.Init("loadingIcon");
	
	myLoadingAnim.SetPosition({ 0.96f, 0.95f });
	LoadCutsceneByIndex(myCutsceneIndex);

	if (myCutsceneIndex == 1) 
	{
		myVidSound = new Sound("Sounds/Music/cutscene1_music.ogg");
		myVidSound->SetAsGenericMusic();
		myVidSound->myFadeSpeed = 0.4f;
	}
	else 
	{
		myVidSound = nullptr;
	}


	myVid->GetSprite()->SetSizeRelativeToScreen({ 16.0f/9.0f, 1.0f });

	myVid->Play();
	if (myVidSound != nullptr) 
	{
		SoundEngine::PlaySound(*myVidSound);
	}

	Fader::FadeOut(FadeColor::Black);
}

void CutsceneState::OnEnter()
{
	Init();
}

void CutsceneState::OnExit()
{
}

eStateStackMessage CutsceneState::Update()
{
	while (ShowCursor(false) >= 0);
	myVid->Update(CommonUtilities::Timer::GetDeltaTime());
	if (InputWrapper::IsButtonPressed(KeyButton::Jump) || myVid->myStatus == Tga2D::VideoStatus_ReachedEnd)
	{
		if (Fader::GetState() == FadeState::Invisable)
		{
			myVid->Pause();
			Fader::FadeIn(FadeColor::Black);
		}
	}
	if (Fader::GetState() == FadeState::Visable && (*myLoadingState == InGameState::eLoadingState::FinishedLoading || *myLoadingState == InGameState::eLoadingState::Waiting))
	{
		Fader::FadeOut(FadeColor::Black);

		if (myCutsceneType == eCutsceneType::Intro)
		{
			return eStateStackMessage::PopSubState;
		}
		else
		{
			return eStateStackMessage::PopCutsceneAndPushCredits;
		}
	}
	return eStateStackMessage::KeepState;
}

void CutsceneState::Render()
{
	myVid->Render();
	Fader::Render();
	if (*myLoadingState == InGameState::eLoadingState::Loading)
	{
		myLoadingAnim.Render();
	}
}

void CutsceneState::LoadCutsceneByIndex(int aIndex)
{
	switch (aIndex)
	{
	case 1:
		myVid->Init("Cutscenes/chapterCutscene1.mp4");
		break;
	case 2:
		myVid->Init("Cutscenes/chapterCutscene2.mp4");
		break;
	case 3:
		myVid->Init("Cutscenes/chapterCutscene3.mp4");
		break;
	case 4:
		myVid->Init("Cutscenes/outroCutscene.mp4");
		break;
	default:
		assert(false && "hecking shit that cutscene index is NOT RIGHT");
		break;
	}
}

#pragma once
#include "GameState.h"
#include "InGameState.h"
#include "Animation.h"

class Sound;

namespace Tga2D
{
	class CVideo;
}
class CutsceneState : public GameState
{
public:
	enum class eCutsceneType
	{
		Intro,
		Outro
	};

	CutsceneState(InGameState::eLoadingState &aLoadingState, int aCutsceneIndex, eCutsceneType aCutsceneType);
	~CutsceneState();

	void Init() override;

	void OnEnter() override;
	void OnExit() override;

	eStateStackMessage Update() override;
	void Render() override;
private:
	void LoadCutsceneByIndex(int aIndex);
	Tga2D::CVideo *myVid;
	InGameState::eLoadingState *myLoadingState;
	Animation myLoadingAnim;
	int myCutsceneIndex;
	eCutsceneType myCutsceneType;
	Sound* myVidSound;
};


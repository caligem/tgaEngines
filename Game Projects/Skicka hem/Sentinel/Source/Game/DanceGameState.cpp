#include "stdafx.h"
#include "DanceGameState.h"
#include <iostream>
#include "Sprite.h"
#include "Timer.h"
#include "InputWrapper.h"
#include "SoundEngine.h"
#include "Text.h"
#include <sstream>
#include "Animation.h"
#include "PostMaster.h"

using namespace CommonUtilities;

DanceGameState::DanceGameState()
{
}


DanceGameState::~DanceGameState()
{
}

void DanceGameState::Init()
{
	myIsCompleted = false;
	myMaxGameTime = 108.f;

	myCompletedTimer = 0;
	myXPos = 0.425f;
	myScoreText = new Text();
	myScoreText->Init("Text/Mecha.ttf", Text::EFontSize_36);
	myScoreText->SetColor(Vector4f(1.0f, 1.0f, 1.0f, 1.0f));
	myScoreText->SetPosition({ 0.5f, 0.1f});
	myScore = 0;

	myTextMaxLifeTime = 0.5f;

	myScale = 4.5f;
	myDistance = 0.05f;
	myTime = 0;

	myMusic = new Sound("Sounds/Music/disco.mp3");
	myMusic->SetAsGenericMusic();
	myMusic->myShouldFadein = false;
	myMusic->myFadeSpeed = 0.1f;

	myShouldPop = false;
	myRenderBuffer.Init(64);

	InitButtons();

	myWinSprite = new Sprite();
	myWinSprite->Init("Sprites/DanceGame/youreWinner.dds");
	Vector2f pivot(0.5f, 0.5f);
	myWinSprite->SetPivot(pivot);
	myWinSprite->SetPosition({ 0.5f, 0.3f });

	myData.myDistance = myDistance;
	myData.myScale = myScale;
	myData.myXPos = myLeftBasePos.x;

	InitRanges();
	InitNoteSetBuffer();

	myScoreNeededToWin = 10000.f;

	myVictoryText = new Text();
	myVictoryText->Init("Text/Mecha.ttf", Text::EFontSize_36);
	myVictoryText->SetColor(Vector4f(1.0f, 1.0f, 1.0f, 1.0f));


}

void DanceGameState::DisplayTexts() 
{
	for (int i = 0; i < myTexts.size(); i++)
	{
		myRenderBuffer.Add(myTexts[i]);
		myTextLife[i] += Timer::GetDeltaTime();
		myTexts[i]->SetPosition({ myTexts[i]->GetPosition().x, myTexts[i]->GetPosition().y - 0.00001f });
		myTexts[i]->SetScale(myTexts[i]->GetScale() + 0.0001f);

		if (myTextLife[i] > myTextMaxLifeTime)
		{
			myTextLife.erase(myTextLife.begin() + i);
			myTexts.erase(myTexts.begin() + i);
		}
	}

	std::stringstream rip;
	rip << "Score: " << myScore;
	myScoreText->SetText(rip.str());
	myRenderBuffer.Add(myScoreText);
}

void DanceGameState::AddText(TextType aTextType)
{
	Text* text = new Text();
	text->Init("Text/Mecha.ttf", Text::EFontSize_30);
	text->SetPosition({ myUpBasePos.x + 0.02f, myUpBasePos.y - 0.075f });

	Vector4f perfectColor(255.f / 255.f, 236.0f / 255.f, 0.0f / 255.f, 1.0f);
	Vector4f missColor(255.f / 255.f, 60.0f / 255.f, 0.0f, 1.0f);
	Vector4f weakColor(239.f / 255.f, 129.0f / 255.f, 0.0f / 255.f, 1.0f);
	Vector4f greatColor(0.f / 255.f, 197.0f / 255.f, 239.0f / 255.f, 1.0f);

	switch (aTextType)
	{
	case TextType::Miss:
		text->SetColor(missColor);
		text->SetText("MISS!");
		break;
	case TextType::Perfect:
		text->SetColor(perfectColor);
		text->SetText("PERFECT! +100");
		myScore += 100;
		break;
	case TextType::Great:
		text->SetColor(greatColor);
		text->SetText("GREAT! +75");
		myScore += 75;
		break;
	case TextType::Weak:
		text->SetColor(weakColor);
		text->SetText("WEAK! + 50");
		myScore += 50;
		break;
	default:
		break;
	}


	myTexts.push_back(text); 
	myTextLife.push_back(0);
}

void DanceGameState::InitButtons() 
{
	Vector2f pivot(0.5f, 0.5f);

	myBackgroundSprite = new Sprite();
	myBackgroundSprite->Init("Sprites/white_pixel.dds");
	myBackgroundSprite->SetPivot(pivot);
	myBackgroundSprite->SetSizeRelativeToScreen({ 2.0f, 1.0f });
	myBackgroundSprite->SetPosition(pivot);
	myBackgroundSprite->SetColor({0.f, 0.f, 0.f, 0.75f});

	myLeftBasePos = { myXPos + (myDistance * 0), 0.25f };
	myUpBasePos = { myXPos + (myDistance * 1), 0.25f };
	myDownBasePos = { myXPos + (myDistance * 2), 0.25f };
	myRightBasePos = { myXPos + (myDistance * 3), 0.25f };

	myLeftButton.Init("Sprites/DanceGame/note_base_left.dds", myLeftBasePos, myScale, KeyButton::Left);
	myUpButton.Init("Sprites/DanceGame/note_base_up.dds", myUpBasePos, myScale, KeyButton::Up);
	myDownButton.Init("Sprites/DanceGame/note_base_down.dds", myDownBasePos, myScale, KeyButton::Down);
	myRightButton.Init("Sprites/DanceGame/note_base_right.dds", myRightBasePos, myScale, KeyButton::Right);

	myCRightButton.Init("Sprites/DanceGame/c_note_base_right.dds", myRightBasePos, myScale, KeyButton::Select);
	myCDownButton.Init("Sprites/DanceGame/c_note_base_down.dds", myDownBasePos, myScale, KeyButton::Hook);


}

void DanceGameState::OnEnter()
{
	Init();
	SoundEngine::PlaySound(*myMusic);
}

void DanceGameState::OnExit()
{
	CleanUp();

	for (int i = 0; i < mySpritesToDelete.size(); i++)
	{
		delete mySpritesToDelete[i];
	}

	delete myBackgroundSprite;
	myBackgroundSprite = nullptr;

	myMusic->Annahilate();

	if (myScore >= myScoreNeededToWin) 
	{
		Message message;
		message.myMessageType = MessageType::UnlockCollectible;
		PostMaster::SendMessages(message);
	}
}

//Renderbuffer
void DanceGameState::FillRenderBuffer()
{
	myRenderBuffer.Add(myBackgroundSprite);
	
	if (!myIsCompleted) 
	{
		myLeftButton.FillRenderBuffer(myRenderBuffer);
		myUpButton.FillRenderBuffer(myRenderBuffer);

		if (InputWrapper::GetInputType() == InputType::Keyboard)
		{
			myDownButton.FillRenderBuffer(myRenderBuffer);
			myRightButton.FillRenderBuffer(myRenderBuffer);
		}
		else
		{
			myCDownButton.FillRenderBuffer(myRenderBuffer);
			myCRightButton.FillRenderBuffer(myRenderBuffer);
		}

		for (int i = 0; i < myNoteSets.size(); i++)
		{
			myNoteSets[i]->FillRenderBuffer(myRenderBuffer);
		}

		DisplayTexts();
	}
	else 
	{

		myScoreText->SetPosition({ 0.485f, 0.4f });
		myRenderBuffer.Add(myScoreText);


		std::stringstream ss;

		if (myScore >= myScoreNeededToWin)
		{
			myRenderBuffer.Add(myWinSprite);
			ss << "You needed " << myScoreNeededToWin << " to win!";
			myVictoryText->SetText(ss.str());
			myVictoryText->SetPosition({ 0.48f, 0.5f });
		}
		else 
		{
			ss << "You need " << myScoreNeededToWin << " to win!";
			myVictoryText->SetText(ss.str());
			myVictoryText->SetPosition({ 0.48f, 0.5f });
		}

		myRenderBuffer.Add(myVictoryText);

	}


}

void DanceGameState::Render()
{
	for (unsigned short i = 0; i < myRenderBuffer.Size(); i++)
	{
		myRenderBuffer[i]->Render();
	}
}

bool IsInRange(ScoreRange aRange, float aPosY) 
{
	if (aPosY < aRange.myMaxY && aPosY > aRange.myMinY) 
	{
		return true;
	}

	return false;
}

PressState DanceGameState::PressedTheCorrectButtons(NoteSet& aNoteSet)
{
	int totalNumberOfButtons = aNoteSet.GetNumberOfNotes();
	int numberOfRightlyPressedButtons = 0;
	int timesPressed = 0;
	int wrongs = 0;

	NoteSetStruct noteSet = aNoteSet.GetNotes();

	if (myLeftButton.IsPressed())
	{
		++timesPressed;

		if (noteSet.myHasLeft) 
		{
			++numberOfRightlyPressedButtons;
		}
		else 
		{
			++wrongs;
		}
	}

	if (myUpButton.IsPressed())
	{
		++timesPressed;

		if (noteSet.myHasUp)
		{
			++numberOfRightlyPressedButtons;
		}
		else
		{
			++wrongs;
		}
	}

	if (InputWrapper::GetInputType() == InputType::Keyboard) 
	{
		if (myDownButton.IsPressed())
		{
			++timesPressed;

			if (noteSet.myHasDown)
			{
				++numberOfRightlyPressedButtons;
			}
			else
			{
				++wrongs;
			}
		}

		if (myRightButton.IsPressed())
		{
			++timesPressed;

			if (noteSet.myHasRight)
			{
				++numberOfRightlyPressedButtons;
			}
			else
			{
				++wrongs;
			}
		}
	}
	else 
	{

		if (myCDownButton.IsPressed())
		{
			++timesPressed;

			if (noteSet.myHasDown)
			{
				++numberOfRightlyPressedButtons;
			}
			else
			{
				++wrongs;
			}
		}

		if (myCRightButton.IsPressed())
		{
			++timesPressed;

			if (noteSet.myHasRight)
			{
				++numberOfRightlyPressedButtons;
			}
			else
			{
				++wrongs;
			}
		}
	}


	if (totalNumberOfButtons == numberOfRightlyPressedButtons && timesPressed > 0)
	{
		if (wrongs > 0) 
		{
			return PressState::PressedWrong;
		}

		if (noteSet.myHasDown) 
		{
			myDownButton.EmittParticles();
			myCDownButton.EmittParticles();
		}
		if (noteSet.myHasUp)
		{
			myUpButton.EmittParticles();
		}
		if (noteSet.myHasLeft)
		{
			myLeftButton.EmittParticles();
		}
		if (noteSet.myHasRight)
		{
			myRightButton.EmittParticles();
			myCRightButton.EmittParticles();
		}


		return PressState::PressedRight;
	}
	
	if (timesPressed == totalNumberOfButtons) 
	{
		if (wrongs > 0) 
		{
			return PressState::PressedWrong;
		}
	}


	return PressState::DidNotPress;

}

void DanceGameState::UpdateGameLogic()
{
	for (int i = 0; i < myNoteSets.size(); i++)
	{
		if (IsInRange(myViableRange, myNoteSets[i]->GetPosY()))
		{
			PressState pressed = PressedTheCorrectButtons(*myNoteSets[i]);

			if (pressed == PressState::PressedRight)
			{
				if (IsInRange(myPerfectRange, myNoteSets[i]->GetPosY()))
				{
					AddText(TextType::Perfect);
				}
				else if (IsInRange(myGoodRange, myNoteSets[i]->GetPosY()))
				{
					AddText(TextType::Great);
				}
				else if (IsInRange(myWeakRange, myNoteSets[i]->GetPosY()))
				{
					AddText(TextType::Weak);
				}
				else
				{
					AddText(TextType::Miss);
				}

				myNoteSets.erase(myNoteSets.begin() + i);
			}
			else if (pressed == PressState::PressedWrong) 
			{
				AddText(TextType::Miss);
				myNoteSets.erase(myNoteSets.begin() + i);
			}
		}
	}
}

void DanceGameState::InitRanges()
{
	float posY = myLeftBasePos.y;
	ScoreRange range;

	range.myMaxY = posY + 0.01f;
	range.myMinY = posY - 0.01f;
	myPerfectRange = range;

	range.myMaxY = posY + 0.025f;
	range.myMinY = posY - 0.025f;
	myGoodRange = range;

	range.myMaxY = posY + 0.03f;
	range.myMinY = posY - 0.03f;

	myWeakRange = range;

	range.myMaxY = posY + 0.04f;
	range.myMinY = posY - 0.04f;
	myViableRange = range;

}

void DanceGameState::UpdateVictoryScreen()
{

	myCompletedTimer += Timer::GetDeltaTime();

	if (myCompletedTimer > 7.5f) 
	{
		myShouldPop = true;
	}
}

eStateStackMessage DanceGameState::Update()
{
	CleanUp();

	if (!myIsCompleted) 
	{
		UpdateNoteSetBuffer();
		UpdateButtons();
		UpdateGameLogic();


		for (auto noteSet : myNoteSets)
		{
			noteSet->MoveUp();
		}
	}
	else 
	{
		UpdateVictoryScreen();
	}

	FillRenderBuffer();



	if (InputWrapper::IsButtonPressed(KeyButton::Pause)) 
	{
		myShouldPop = true;
	}

	if (myShouldPop)
	{
		return eStateStackMessage::PopSubState;
	}

	return eStateStackMessage::KeepState;
}

void DanceGameState::InitNoteSetBuffer()
{
	float loop = 0.f;
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 3.5f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.8f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 5.4f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.6f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.2f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.8f - 2.8f, new NoteSet(0, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.75f - 2.8f, new NoteSet(1, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.2f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.65f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.5f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.4f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.1f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.8f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));

	loop = 10.0f;

	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 2.3f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 3.5f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.8f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 5.4f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.6f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.2f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.8f - 2.8f, new NoteSet(0, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.75f - 2.8f, new NoteSet(1, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.2f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.65f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.5f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.4f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.1f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.8f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));

	loop = 10.0f * 2;

	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 2.3f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 3.5f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.8f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 5.4f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.6f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.2f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.8f - 2.8f, new NoteSet(0, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.75f - 2.8f, new NoteSet(1, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.2f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.65f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.5f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.4f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.1f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.8f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));


	loop = 10.0f * 3;

	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 2.3f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 3.5f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.8f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 5.4f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.6f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.2f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.8f - 2.8f, new NoteSet(0, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.75f - 2.8f, new NoteSet(1, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.2f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.65f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.5f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.4f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.1f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.8f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));

	loop = 10.0f * 4;

	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 2.3f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 3.5f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.8f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 5.4f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.6f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.2f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.8f - 2.8f, new NoteSet(0, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.75f - 2.8f, new NoteSet(1, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.2f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.65f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.5f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.4f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.1f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.8f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));


	loop = 10.0f * 5;

	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 2.3f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 3.5f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.8f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 5.4f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.6f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.2f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.8f - 2.8f, new NoteSet(0, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.75f - 2.8f, new NoteSet(1, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.2f - 2.8f, new NoteSet(0, 1, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.65f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.5f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.4f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.1f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.8f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));


	loop = 10.0f * 6;

	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 3.8f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.8f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 5.4f - 2.8f, new NoteSet(0, 1, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.6f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.2f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.8f - 2.8f, new NoteSet(0, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.75f - 2.8f, new NoteSet(1, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.2f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.65f - 2.8f, new NoteSet(0, 1, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.5f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.4f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.1f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.8f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));


	loop = 10.0f * 7;

	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 0.5f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 2.3f - 2.8f, new NoteSet(1, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 3.5f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.8f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 5.4f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.0f - 2.8f, new NoteSet(1, 0, 1, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.6f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.2f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.8f - 2.8f, new NoteSet(0, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.75f - 2.8f, new NoteSet(1, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.2f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.65f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.5f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.4f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.1f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.8f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));


	loop = 10.0f * 8;

	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 0.5f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 2.3f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 3.5f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.8f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 5.4f - 2.8f, new NoteSet(0, 0, 1, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.6f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.2f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.8f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.75f - 2.8f, new NoteSet(1, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.2f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.65f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.5f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.0f - 2.8f, new NoteSet(1, 1, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.4f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.1f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.8f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));


	loop = 10.0f * 9;

	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 0.5f - 2.8f, new NoteSet(1,0,0,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 2.3f - 2.8f, new NoteSet(0,1,0,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 3.5f - 2.8f, new NoteSet(0,0,0,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.2f - 2.8f, new NoteSet(1,0,0,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.8f - 2.8f, new NoteSet(1,0,0,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 5.4f - 2.8f, new NoteSet(0,1,0,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.0f - 2.8f, new NoteSet(0,0,1,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.6f - 2.8f, new NoteSet(0,0,0,1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.2f - 2.8f, new NoteSet(0,0,1,1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.8f - 2.8f, new NoteSet(0,1,0,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.2f - 2.8f, new NoteSet(0,1,1,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 8.75f - 2.8f, new NoteSet(1, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.2f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 9.65f - 2.8f, new NoteSet(0, 1, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.0f - 2.8f, new NoteSet(0,1,0,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 10.5f - 2.8f, new NoteSet(0,0,1,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.0f - 2.8f, new NoteSet(0,1,0,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 11.4f - 2.8f, new NoteSet(1,0,1,0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.1f - 2.8f, new NoteSet(1,0,0,1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 12.8f - 2.8f, new NoteSet(1,1,0,0, myData, mySpritesToDelete)));

	loop = 10.0f * 10;

	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 0.5f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 2.3f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 3.5f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.2f - 2.8f, new NoteSet(1, 0, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 4.8f - 2.8f, new NoteSet(1, 0, 1, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 5.4f - 2.8f, new NoteSet(0, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.0f - 2.8f, new NoteSet(1, 0, 0, 1, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 6.6f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));
	myNoteBuffer.push_back(std::pair<float, NoteSet*>(loop + 7.2f - 2.8f, new NoteSet(0, 1, 0, 0, myData, mySpritesToDelete)));


}

void DanceGameState::UpdateNoteSetBuffer()
{
	myTime += Timer::GetDeltaTime();
	
	if (myTime > myMaxGameTime) 
	{
		myIsCompleted = true;
		SoundEngine::StopPlayingSound(*myMusic);
	}

	for (int i = 0; i < myNoteBuffer.size(); i++)
	{
		if (myNoteBuffer[i].first < myTime)
		{
			myNoteSets.push_back(myNoteBuffer[i].second);
			myNoteBuffer.erase(myNoteBuffer.begin() + i);
		}
	}
}

void DanceGameState::UpdateButtons()
{
	myLeftButton.Update();
	myUpButton.Update();

	if (InputWrapper::GetInputType() == InputType::Keyboard) 
	{
		myDownButton.Update();
		myRightButton.Update();
	}
	else 
	{
		myCDownButton.Update();
		myCRightButton.Update();
	}
		
}

void DanceGameState::CleanUp()
{
	myRenderBuffer.RemoveAll();
}

NoteSet::NoteSet(bool aLeft, bool aUp, bool aDown, bool aRight, DanceData aData, std::vector<Sprite*>& aVector)
{
	myTimeActive = 0;

	myHasUp = aUp;
	myHasDown = aDown;
	myHasRight = aRight;
	myHasLeft = aLeft;

	myLeftNote = new Sprite();
	myLeftNote->Init("Sprites/DanceGame/note_left.dds");
	aVector.push_back(myLeftNote);

	myRightNote = new Sprite();
	myRightNote->Init("Sprites/DanceGame/note_right.dds");
	aVector.push_back(myRightNote);

	myCRightNote = new Sprite();
	myCRightNote->Init("Sprites/DanceGame/c_note_right.dds");
	aVector.push_back(myCRightNote);

	myUpNote = new Sprite();
	myUpNote->Init("Sprites/DanceGame/note_up.dds");
	aVector.push_back(myUpNote);

	myDownNote = new Sprite();
	myDownNote->Init("Sprites/DanceGame/note_down.dds");
	aVector.push_back(myDownNote);

	myCDownNote = new Sprite();
	myCDownNote->Init("Sprites/DanceGame/c_note_down.dds");
	aVector.push_back(myCDownNote);

	myLeftNote->SetSizeRelativeToImage({ aData.myScale, aData.myScale });
	myRightNote->SetSizeRelativeToImage({ aData.myScale, aData.myScale });
	myCRightNote->SetSizeRelativeToImage({ aData.myScale, aData.myScale });
	myDownNote->SetSizeRelativeToImage({ aData.myScale, aData.myScale });
	myCDownNote->SetSizeRelativeToImage({ aData.myScale, aData.myScale });
	myUpNote->SetSizeRelativeToImage({ aData.myScale, aData.myScale });

	myLeftNote->SetPivot(Vector2f(0.5f, 0.5f));
	myRightNote->SetPivot(Vector2f(0.5f, 0.5f));
	myCRightNote->SetPivot(Vector2f(0.5f, 0.5f));
	myUpNote->SetPivot(Vector2f(0.5f, 0.5f));
	myDownNote->SetPivot(Vector2f(0.5f, 0.5f));
	myCDownNote->SetPivot(Vector2f(0.5f, 0.5f));


	myPosition = { aData.myXPos, 1.25f };
	mySpeed = 0.35f;

	myDistance = aData.myDistance;
}

NoteSet::~NoteSet()
{
}

void NoteSet::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	if (myHasLeft) 
	{
		myLeftNote->SetPosition({ myPosition.x + (myDistance * 0), myPosition.y});
		aRenderBuffer.Add(myLeftNote);
	}

	if (myHasUp)
	{
		myUpNote->SetPosition({ myPosition.x + (myDistance * 1), myPosition.y });
		aRenderBuffer.Add(myUpNote);
	}

	if (myHasDown)
	{
		if (InputWrapper::GetInputType() == InputType::Keyboard) 
		{
			myDownNote->SetPosition({ myPosition.x + (myDistance * 2), myPosition.y });
			aRenderBuffer.Add(myDownNote);
		}
		else 
		{
			myCDownNote->SetPosition({ myPosition.x + (myDistance * 2), myPosition.y });
			aRenderBuffer.Add(myCDownNote);
		}
	}

	if (myHasRight)
	{
		if (InputWrapper::GetInputType() == InputType::Keyboard)
		{
			myRightNote->SetPosition({ myPosition.x + (myDistance * 3), myPosition.y });
			aRenderBuffer.Add(myRightNote);
		}
		else
		{
			myCRightNote->SetPosition({ myPosition.x + (myDistance * 3), myPosition.y });
			aRenderBuffer.Add(myCRightNote);
		}
	}
}

void NoteSet::MoveUp()
{
	myPosition.y -= mySpeed * Timer::GetDeltaTime();
	myTimeActive += Timer::GetDeltaTime();
}

float NoteSet::GetTimeActive() 
{
	return myTimeActive;
}

float NoteSet::GetPosY()
{
	return myPosition.y;
}

int NoteSet::GetNumberOfNotes()
{
	int number = 0;

	if (myHasDown) 
	{
		++number;
	}
	if (myHasUp) 
	{
		++number;
	}
	if (myHasLeft) 
	{
		++number;
	}
	if (myHasRight) 
	{
		++number;
	}

	return number;
}

NoteSetStruct NoteSet::GetNotes()
{
	NoteSetStruct set;

	set.myHasLeft = myHasLeft;
	set.myHasUp = myHasUp;
	set.myHasDown = myHasDown;
	set.myHasRight = myHasRight;

	return set;
}

Dancer::Dancer()
{
}

Dancer::~Dancer()
{
}

void Dancer::Init(const char * aPath, const Vector2f & aPos)
{
	myPosition = aPos;
	myAnimation = new Animation();
	myAnimation->Init(aPath);
	myAnimation->SetPivot({ 0.5f, 0.5f });
	myAnimation->SetPosition(myPosition);
}

void Dancer::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(myAnimation);
}

#pragma once
#include "GameState.h"
#include "Vector2.h"
#include <vector>
#include <map>
#include "NoteBaseButton.h"

using namespace CommonUtilities;

class Sprite;
class Sound;
class Text;
class Animation;

enum class PressState 
{
	PressedRight,
	PressedWrong,
	DidNotPress
};

enum class TextType
{
	Miss,
	Perfect,
	Great,
	Weak
};

struct DanceData
{
	float myScale;
	float myXPos;
	float myDistance;
};

struct NoteSetStruct 
{
	bool myHasLeft;
	bool myHasUp;
	bool myHasDown;
	bool myHasRight;
};

struct ScoreRange
{
	float myMinY;
	float myMaxY;
};

class NoteSet
{
public:
	NoteSet(bool aLeft, bool aUp, bool aDown, bool aRight, DanceData, std::vector<Sprite*>& aVector);
	~NoteSet();

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer);
	void MoveUp();
	float GetTimeActive();
	float GetPosY();

	int GetNumberOfNotes();

	NoteSetStruct GetNotes();

private:
	Sprite* myLeftNote;
	Sprite* myRightNote;
	Sprite* myCRightNote;
	Sprite* myDownNote;
	Sprite* myCDownNote;
	Sprite* myUpNote;

	float myDistance;
	float mySpeed;
	float myTimeActive;

	bool myHasLeft;
	bool myHasRight;
	bool myHasUp;
	bool myHasDown;

	CommonUtilities::Vector2f myPosition;

};

class Dancer 
{
public:
	Dancer();
	~Dancer();

	void Init(const char* aPath, const Vector2f& aPos);
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer);

private:
	Animation* myAnimation;
	Vector2f myPosition;

};

class DanceGameState :
	public GameState
{
public:
	DanceGameState();
	~DanceGameState();


	void Init() override;
	void OnEnter() override;
	void OnExit() override;
	void Render() override;
	eStateStackMessage Update() override;
	inline const bool LetThroughRender() const override { return true; }

private:

	void InitNoteSetBuffer();
	void UpdateNoteSetBuffer();
	void FillRenderBuffer();
	void UpdateButtons();
	void InitRanges();
	void UpdateVictoryScreen();
	void UpdateGameLogic();
	void DisplayTexts();
	void AddText(TextType aTextType);
	void InitButtons();
	void CleanUp();


	PressState PressedTheCorrectButtons(NoteSet & aNoteSet);

	Vector2f myLeftBasePos;
	Vector2f myRightBasePos;
	Vector2f myUpBasePos;
	Vector2f myDownBasePos;

	float myScale;
	float myDistance;
	float myTime;
	float myXPos;
	float myTextMaxLifeTime;
	float myMaxGameTime;


	int myScore;

	NoteBaseButton myLeftButton;
	NoteBaseButton myRightButton;
	NoteBaseButton myUpButton;
	NoteBaseButton myDownButton;

	NoteBaseButton myCDownButton;
	NoteBaseButton myCRightButton;

	Sprite* myBackgroundSprite;
	Sprite* myWinSprite;


	Text* myScoreText;
	Text* myVictoryText;

	float myScoreNeededToWin;

	ScoreRange myWeakRange;
	ScoreRange myGoodRange;
	ScoreRange myPerfectRange;
	ScoreRange myViableRange;

	CommonUtilities::GrowingArray<Drawable*> myRenderBuffer;

	std::vector<NoteSet*> myNoteSets;
	std::vector<std::pair<float, NoteSet*>> myNoteBuffer;
	std::vector<Sprite*> mySpritesToDelete;
	std::vector<Text*> myTexts;
	std::vector<float> myTextLife;


	std::map<KeyButton, bool> myWindowBuffer;

	Sound* myMusic;

	DanceData myData;

	bool myShouldPop;
	bool myIsCompleted;
	float myCompletedTimer;


};

bool IsInRange(ScoreRange aRange, float aPosY);

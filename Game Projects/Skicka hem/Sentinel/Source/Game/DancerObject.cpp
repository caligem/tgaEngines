#include "stdafx.h"
#include "DancerObject.h"
#include "InputWrapper.h"
#include "JsonWrapper.h"
#include "Animation.h"
#include "PostMaster.h"


DancerObject::DancerObject()
{
}


DancerObject::~DancerObject()
{
}

void DancerObject::OnUpdate()
{
	myDrawable->SetPosition({ myPosition.x, myPosition.y });
}

void DancerObject::InitDO(bool aIsBoomBox)
{
	myIsBoomBox = aIsBoomBox;

	myXButtonAnimation = new Animation();
	myXButtonAnimation->Init("press_button_x");

	myCButtonAnimation = new Animation();
	myCButtonAnimation->Init("press_button_c");


}

void DancerObject::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(myDrawable);

	if (myShouldRenderButton) 
	{
		if (InputWrapper::GetInputType() == InputType::Controller) 
		{
			myXButtonAnimation->SetPosition({ myPosition.x, myPosition.y - 0.05f });
			aRenderBuffer.Add(myXButtonAnimation);
		}
		else 
		{
			myCButtonAnimation->SetPosition({ myPosition.x, myPosition.y - 0.05f });
			aRenderBuffer.Add(myCButtonAnimation);
		}

		myShouldRenderButton = false;
	}
}

#include <iostream>

void DancerObject::OnCollisionStay(Collider & other)
{
	if (other.GetTag() == ColliderTag::Player) 
	{
		if (!myHaveBeenTriggered)
		{
			if (other.GetTag() == ColliderTag::Player)
			{
				myShouldRenderButton = true;

				if (InputWrapper::IsButtonPressed(KeyButton::Use))
				{
					Message newMessage;
					newMessage.myMessageType = MessageType::PushDanceGame;
					PostMaster::SendMessages(newMessage);
				}
			}
		}
	}	
}

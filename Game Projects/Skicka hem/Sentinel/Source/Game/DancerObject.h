#pragma once
#include "GameObject.h"

class DancerObject :
	public GameObject
{
public:
	DancerObject();
	~DancerObject();

	void OnUpdate() override;
	void InitDO(bool aIsBoomBox);

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;
	void OnCollisionStay(Collider  &other) override;

private:
	bool myIsBoomBox;

	bool myHaveBeenTriggered;
	bool myShouldRenderButton;

	Drawable* myXButtonAnimation;
	Drawable* myCButtonAnimation;

};


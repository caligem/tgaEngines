#include "stdafx.h"
#include "Dialogue.h"
#include "JsonWrapper.h"
#include "SoundEngine.h"

Dialogue::Dialogue()
{
	myHasLoadedSound = false;
}


Dialogue::~Dialogue()
{
	//mySound->Destroy();
}

void Dialogue::LoadSound() 
{
	if (!myHasLoadedSound) 
	{
		mySound = new Sound("Sounds/Sfx/talk.wav");
		myHasLoadedSound = true;
	}
}

void Dialogue::Init()
{
	json dialogueData = LoadDialogueData();

	dialogueData["maxTravelTime"];

	mySlideIndex = 0;
	myColor = Tga2D::CColor(1.0f, 1.0f, 1.0f, 1.0f);
	myIsDone = false;

	myPosition = { dialogueData["dialoguePosX"], dialogueData["dialoguePosY"] };
	myBlackBars.Init(dialogueData["blackbarSpeed"], dialogueData["blackbarSize"]);

	LoadSound();

	dialogueData.clear();
}

void Dialogue::ReInit() 
{
	Init();

	for (size_t i = 0; i < mySlides.size(); i++)
	{
		mySlides[i].ReInit();
	}
	
}

void Dialogue::Update(float aDeltaTime)
{
	myBlackBars.Update(aDeltaTime, myIsDone);

	if (myBlackBars.HasEntered()) 
	{
		mySlides[mySlideIndex].Update(aDeltaTime);
	}
}

void Dialogue::Render()
{
	myBlackBars.Render();

	if (mySlideIndex < mySlides.size() && myBlackBars.HasEntered())
	{
		if (!myIsDone && mySlideIndex < mySlides.size())
		{
			mySlides[mySlideIndex].Render();
		}
	}
}

void Dialogue::AddSlide(std::string aString, std::string aSpeakerName)
{
	TextSlide slide(mySound);

	slide.Init(myPosition, aString, aSpeakerName);

	mySlides.push_back(slide);
	
}

void Dialogue::SkipAllSlides()
{
	mySlideIndex = static_cast<int>(mySlides.size());
}

void Dialogue::PrintWholeSlide()
{
	mySlides[mySlideIndex].PrintAll();
}

bool Dialogue::IsEmpty()
{
	return mySlides.size() == 0;
}

void Dialogue::SetIsDone(bool aIsDone)
{
	myIsDone = aIsDone;
}

bool Dialogue::IsCurrentSlideDone() 
{
	return mySlides[mySlideIndex].IsSlideDone();
}

void Dialogue::NextSlide()
{
	//Checks if current slide has printed, if not it prints the whole slide
	
	if (mySlides[mySlideIndex].IsSlideDone()) 
	{
		if (mySlideIndex + 1 == mySlides.size()) 
		{
			myIsDone = true;
		}
		else 
		{
			mySlideIndex++;
		}
	}
	else if (!mySlides[mySlideIndex].IsSlideDone())
	{
		mySlides[mySlideIndex].PrintAll();
	}
}

bool Dialogue::IsDone()
{
	if (myBlackBars.HasLeft() && myIsDone)
	{
		return true;
	}

	return false;
}

void Dialogue::RemoveSlideAtIndex(int aIndex)
{
	if (aIndex < mySlides.size()) 
	{
		mySlides.erase(mySlides.begin() + aIndex);
	}
}

TextSlide Dialogue::GetCurrentSlide()
{
	return mySlides[mySlideIndex];
}

int Dialogue::GetCurrentSlideIndex()
{
	return mySlideIndex;
}

bool Dialogue::IsCurrentSlideSwitch() 
{
	return mySlides[mySlideIndex].IsSwitch();
}

void Dialogue::SetSwitchAtIndex(int aIndex, bool aBool)
{
	mySlides[aIndex].SetSwitch(aBool);
}

int Dialogue::GetAmmountOfSlides() 
{
	return static_cast<int>(mySlides.size());
}
#pragma once
#include "TextSlide.h"
#include <vector>
#include "BlackBars.h"

class Sound;

class Dialogue
{
public:
	Dialogue();
	~Dialogue();

	void LoadSound();

	void Init();
	void ReInit();
	void Update(float aTimeDelta);
	void Render();
	void AddSlide(std::string aString, std::string aSpeakerName);
	void SkipAllSlides();
	void PrintWholeSlide();
	bool IsEmpty();
	void SetIsDone(bool aIsDone);

	void NextSlide();

	void RemoveSlideAtIndex(int aIndex);
	void SetSwitchAtIndex(int aIndex, bool aBool);

	bool IsCurrentSlideSwitch();
	bool IsCurrentSlideDone();
	bool IsDone();
	
	int GetCurrentSlideIndex();
	int GetAmmountOfSlides();

	TextSlide GetCurrentSlide();
	const float GetBlackbarSpeed() const { return myBlackBars.GetSpeed(); }

private:

	float myTextWidth;
	bool myHasLoadedSound;

	Vector2<float> myPosition;

	std::vector<TextSlide> mySlides;
	Tga2D::CColor myColor;

	int mySlideIndex;
	bool myIsDone;

	BlackBars myBlackBars;
	Sound* mySound;

};


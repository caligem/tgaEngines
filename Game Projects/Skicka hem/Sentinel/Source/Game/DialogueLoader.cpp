#include "stdafx.h"
#include "DialogueLoader.h"
#include "JsonWrapper.h"
#include <sstream>


DialogueLoader::DialogueLoader()
{
}


DialogueLoader::~DialogueLoader()
{
}

std::map<std::string, Dialogue>& DialogueLoader::GetDialogues()
{
	return myLoadedDialogues;
}

void DialogueLoader::Load()
{
	json dialogueData = LoadDialogues();

	int diaSize = dialogueData["Dialogues"].size();

	std::stringstream index;

	for (int i = 1; i < diaSize + 1; ++i)
	{
		Dialogue dialogue;
		dialogue.Init();

		//stupid solution
		std::stringstream index;
		index << i;


		std::string id = dialogueData["Dialogues"][index.str()]["id"];
		std::string name = dialogueData["Dialogues"][index.str()]["name"];

		int numberOfLines = dialogueData["Dialogues"][index.str()]["lines"].size();
		for (int x = 1; x < numberOfLines + 1; ++x)
		{
			std::stringstream lineIndex;
			lineIndex << x;

			std::string line = dialogueData["Dialogues"][index.str()]["lines"][lineIndex.str()];

			for (int w = 0; w < line.size(); w++)
			{
				
			}

			dialogue.AddSlide(line, name);
		}


		myLoadedDialogues.insert(std::pair<std::string, Dialogue>(id, dialogue));

	}

	myLoadedDialogues["Number One Text"];
	myLoadedDialogues["Number Two Text"];

}

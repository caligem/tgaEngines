#pragma once
#include "Dialogue.h"
#include <map>
#include <string>

class DialogueLoader
{
public:
	DialogueLoader();
	~DialogueLoader();

	std::map<std::string, Dialogue>& GetDialogues();
	void Load();

private:

	std::map<std::string, Dialogue> myLoadedDialogues;
};


#include "stdafx.h"
#include "DialogueManager.h"
#include "Dialogue.h"
#include "JsonWrapper.h"


DialogueManager::DialogueManager()
{
}

DialogueManager::~DialogueManager()
{
}

DialogueManager * DialogueManager::GetInstance()
{
	static DialogueManager dialogueManager;

	return &dialogueManager;
}


void DialogueManager::Load()
{
	GetInstance()->myCurrentDialogue = nullptr;

	json dialogueData = LoadDialogues();

	int diaSize = dialogueData["Dialogues"].size();

	std::stringstream index;

	for (int i = 1; i < diaSize + 1; ++i)
	{
		Dialogue dialogue;
		dialogue.Init();

		//stupid solution
		std::stringstream index;
		index << i;


		int id = i;

		int numberOfLines = dialogueData["Dialogues"][index.str()]["lines"].size();
		for (int x = 1; x < numberOfLines + 1; ++x)
		{
			std::stringstream lineIndex;
			lineIndex << x;

			std::string line = dialogueData["Dialogues"][index.str()]["lines"][lineIndex.str()];

			std::stringstream nameStream;

			bool nameCopyIsActive = false;
			int removeFrIndex = -1;

			for (int w = 0; w < line.size(); w++)
			{
				if (line.at(w) == ']')
				{
					nameCopyIsActive = false;
					line.erase(line.begin() + removeFrIndex, line.begin() + w + 1);
				}
				else if (nameCopyIsActive)
				{
					nameStream << line.at(w);
				}
				else if (line.at(w) == '[')
				{
					nameCopyIsActive = true;
					removeFrIndex = w;
				}
			}
			

			dialogue.AddSlide(line, nameStream.str());
		}


		GetInstance()->myLoadedDialogues.insert(std::pair<int, Dialogue>(id, dialogue));

	}

	
}

void DialogueManager::Update(float aDeltaTime)
{
	if (GetInstance()->myCurrentDialogue != nullptr) 
	{
		if (GetInstance()->myIsPlaying)
		{
			GetInstance()->myCurrentDialogue->Update(aDeltaTime);
			if (GetInstance()->myCurrentDialogue->IsDone()) 
			{
				GetInstance()->myIsPlaying = false;
			}
		}
	}
}

void DialogueManager::Render() 
{
	if (GetInstance()->myCurrentDialogue != nullptr)
	{
		if (GetInstance()->myIsPlaying) 
		{
			GetInstance()->myCurrentDialogue->Render();
		}
	}
}

void DialogueManager::PlayDialogue(int aID) 
{
	GetInstance()->myIsPlaying = true;
	GetInstance()->myCurrentDialogue = &(GetInstance()->myLoadedDialogues.at(aID));
	GetInstance()->myCurrentDialogue->ReInit();
}

void DialogueManager::StopPlaying() 
{
	if (GetInstance()->myCurrentDialogue != nullptr) 
	{
		GetInstance()->myCurrentDialogue->SetIsDone(true);
		//GetInstance()->myIsPlaying = false;
		//GetInstance()->myCurrentDialogue->ReInit();
	}
	
}

void DialogueManager::Reset()
{
	GetInstance()->myIsPlaying = false;
	GetInstance()->myCurrentDialogue->ReInit();
}

void DialogueManager::NextSlide()
{
	if (GetInstance()->myCurrentDialogue != nullptr) 
	{
		if (GetInstance()->myIsPlaying)
		{
			GetInstance()->myCurrentDialogue->NextSlide();
		}
	}
}

bool DialogueManager::IsPlaying()
{
	return GetInstance()->myIsPlaying;
}

const float DialogueManager::GetCurrentBlackbarSpeed()
{
	if (GetInstance()->myCurrentDialogue == nullptr)
	{
		return 0.f;
	}

	return GetInstance()->myCurrentDialogue->GetBlackbarSpeed();
}

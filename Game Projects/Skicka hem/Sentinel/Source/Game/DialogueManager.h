#pragma once
#include <map>
#include <string>

class Dialogue;

class DialogueManager
{
public:
	~DialogueManager();

	static void Load();
	static void Update(float aDeltaTime);
	static void Render();
	static void PlayDialogue(int aID);
	static void StopPlaying();
	static void Reset();
	static void NextSlide();
	static bool IsPlaying();

	const static float GetCurrentBlackbarSpeed();
private:
	DialogueManager();

	static DialogueManager* GetInstance();

	Dialogue* myCurrentDialogue;

	std::map<int, Dialogue> myLoadedDialogues;

	bool myIsPlaying;

};


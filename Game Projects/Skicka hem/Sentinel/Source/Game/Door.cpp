#include "stdafx.h"
#include "Door.h"
#include "RoomChangeObserver.h"
#include "TriggerManager.h"

#include "JsonWrapper.h"
#include "TileSize.h"
#include "Player.h"
#include "Fader.h"
#include "SoundEngine.h"

Door::Door(RoomChangeObserver* aRoomChangeObserver)
	: myShouldRender(true)
{
	AttachObserver(aRoomChangeObserver);
	
}


Door::~Door()
{
	mySound->Annahilate();
	myUnlockSound->Annahilate();
}


void Door::Init(const char * aTypePath)
{
	json DoorData = LoadData(aTypePath);
	myNextRoomIndex = DoorData["nextRoom"];
	mySpawnPosInNextRoom.x = DoorData["spawnPosX"];
	mySpawnPosInNextRoom.x -= TILE_SIZE;
	mySpawnPosInNextRoom.x *= TILE_MULTIPLIER;
	mySpawnPosInNextRoom.x /= 1920.0f;

	mySpawnPosInNextRoom.y = DoorData["spawnPosY"];
	mySpawnPosInNextRoom.y -= TILE_SIZE;
	mySpawnPosInNextRoom.y *= TILE_MULTIPLIER;
	mySpawnPosInNextRoom.y /= 1080.0f;

	mySound = new Sound("Sounds/Sfx/door.wav");
	myUnlockSound = new Sound("Sounds/Sfx/door_unlock.wav");
}

void Door::OnUpdate()
{
	myDrawable->SetPosition({ myPosition.x, myPosition.y });
}

void Door::OnCollisionEnter(Collider & other)
{
	if (other.GetTag() == ColliderTag::Player)
	{
		if (!myIsLocked)
		{
		
			other.GetOwner()->SetPosition(mySpawnPosInNextRoom);
			Player* player = static_cast<Player*>(other.GetOwner());
			player->OnRoomEnter();
			player->SetState(PlayerStates::Idle);
			player->SetRespawnPosition(mySpawnPosInNextRoom);
			player->LockCollectibles();
			SendMessageToObserver(eMessageType::ReachedDoor, myNextRoomIndex);
			SetIsLocked(true);
			if (myShouldRender)
			{
				//SoundEngine::PlaySound(*mySound);
			}
		}
	} 
}

void Door::OnCollisionStay(Collider & other)
{

}

void Door::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	if (myShouldRender)
	{
		aRenderBuffer.Add(myDrawable);
	}
}

void Door::AddToTriggerManager(int aIndex)
{
	TriggerManager::GetInstance()->InsertDoorAtIndex(aIndex, this);
}

void Door::OnTriggerd()
{
	if (myIsLocked)
	{
		SetIsLocked(false);
		if (myShouldRender) 
		{
			SoundEngine::PlaySound(*myUnlockSound);
		}
	}
}

void Door::SetIsLocked(bool aIsLocked)
{
	myIsLocked = aIsLocked;
	if (myIsAnimated)
	{
		if (myIsLocked)
		{
			dynamic_cast<Animation*>(myDrawable)->SetCurrentFrame(1);
		}
		else
		{
			dynamic_cast<Animation*>(myDrawable)->SetCurrentFrame(0);
		}
	}

}

void Door::SetFlipped(bool aFlipped)
{
	if (myIsAnimated)
	{
		dynamic_cast<Animation*>(myDrawable)->SetInversedX(aFlipped);
	}
}

void Door::SetShouldRender(bool aShouldRender)
{
	myShouldRender = aShouldRender;
}

int Door::GetNextRoomIndex()
{
	return myNextRoomIndex;
}

const CommonUtilities::Vector2f & Door::GetDoorPosition()
{
	return myPosition;
}

const CommonUtilities::Vector2f& Door::GetNextRoomSpawnPoint()
{
	return mySpawnPosInNextRoom;
}

void Door::SetIsAnimated(const bool aIsAnimated)
{
	myIsAnimated = aIsAnimated;
}

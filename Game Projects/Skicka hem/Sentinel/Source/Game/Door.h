#pragma once
#include "GameObject.h"
#include "Subject.h"
class RoomChangeObserver;
class Sound;

class Door : public GameObject, public Subject
{
public:
	Door(RoomChangeObserver* aRoomChangeObserver);
	~Door() override;
	
	void Init(const char* aTypePath);
	void OnUpdate() override;
	void OnCollisionEnter(Collider &other) override;
	void OnCollisionStay(Collider &other) override;
	
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;

	void AddToTriggerManager(int aIndex);
	void OnTriggerd() override;

	void SetIsLocked(bool aIsLocked);
	void SetFlipped(bool aFlipped);
	void SetShouldRender(bool aShouldRender);
	void SetIsAnimated(const bool aIsAnimated);
	int GetNextRoomIndex();
	const CommonUtilities::Vector2f& GetDoorPosition();
	const CommonUtilities::Vector2f& GetNextRoomSpawnPoint();

private:
	bool myShouldRender;
	bool myIsLocked;
	bool myIsAnimated;
	RoomChangeObserver* myRoomChangeObserver;
	CommonUtilities::Vector2f mySpawnPosInNextRoom;
	int myNextRoomIndex;
	Sound* mySound;
	Sound *myUnlockSound;
};


#include "stdafx.h"
#include "Drawable.h"

Drawable::~Drawable()
{
}

void Drawable::Update()
{
}

void Drawable::SetPosition(const CommonUtilities::Vector2f & aPos)
{
	myPosition = aPos;
}

const CommonUtilities::Vector2f & Drawable::GetPosition() const
{
	return myPosition;
}

const CommonUtilities::Vector2<unsigned int> Drawable::GetImagesSizePixel() const
{
	return CommonUtilities::Vector2<unsigned int>();
}

void Drawable::SetSizeRelativeToScreen(const CommonUtilities::Vector2f & aSize)
{
	aSize;
}

bool Drawable::GetIsFlipped() const
{
	return false;
}

void Drawable::SetShouldRender(const bool aShouldRender)
{
	myShouldDrawThisFrame = aShouldRender;
}

void Drawable::SetPivot(const CommonUtilities::Vector2f & aPivot)
{
	
}

const bool Drawable::GetShouldRender() const
{
	return myShouldDrawThisFrame;
}

const int Drawable::GetLayer() const
{
	return myLayer;
}

void Drawable::SetLayer(const int aLayer)
{
	myLayer = aLayer;
}
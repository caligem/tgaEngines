#pragma once
#include "Vector.h"
#include "GrowingArray.h"
#include <string>
namespace Tga2D
{
	class CSprite;
}
class Drawable
{
public:
	virtual ~Drawable() = 0;
	virtual void Render() = 0;
	virtual void Update();
	virtual void Init(const char* aPath) = 0;
	void SetPosition(const CommonUtilities::Vector2f &aPos);
	const CommonUtilities::Vector2f & GetPosition() const;
	virtual const CommonUtilities::Vector2<unsigned int>  GetImagesSizePixel() const;
	virtual void SetSizeRelativeToScreen(const CommonUtilities::Vector2f & aSize);
	virtual bool GetIsFlipped() const;
	virtual void SetRotation(const float aRotation) = 0;
	const int GetLayer() const;
	void SetLayer(const int aLayer);
	virtual void SetShouldRender(const bool aShouldRender);
	void SetPivot(const CommonUtilities::Vector2f& aPivot);
	const bool GetShouldRender() const;
protected:
	CommonUtilities::Vector2f myPosition;
	bool myShouldDrawThisFrame;
	int myLayer;
};

struct TextureRect
{
	float startX;
	float startY;
	float endX;
	float endY;
};


struct TileObjectData
{
	std::string objectType;
	std::string tilePath;
};

struct AnimationData
{
	std::string objectType;
	int duration;
	int tileID;
};

struct TileSet
{
	int startGridID;
	int tileCount;
	int columns;
	int imageHeight;
	int imageWidth;
	int spacing;
	std::string tilesetPath;
	CommonUtilities::GrowingArray<TileObjectData> tileObjects;
	CommonUtilities::GrowingArray<AnimationData> animationDatas;
};
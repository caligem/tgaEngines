#include "stdafx.h"
#include "Enemy.h"


Enemy::Enemy()
{
}


Enemy::~Enemy()
{
}

void Enemy::TakeDamage(const int aDamageToTake)
{
	myHealth -= aDamageToTake;

	if (myHealth <= 0)
	{
		myIsDead = true;
	}
}

bool Enemy::IsDead()
{
	return myIsDead;
}

#pragma once
#include "GameObject.h"
#include "Vector.h"

class Enemy : public GameObject
{
public:
	Enemy();
	virtual ~Enemy();

	void TakeDamage(const int aDamageToTake);
	bool IsDead();

protected:
	int myHealth;
	bool myIsDead;
	CommonUtilities::Vector2f myDirection;
};


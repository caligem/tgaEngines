#include "stdafx.h"
#include "Fader.h"
#include "tga2d\sprite\sprite.h"


Fader::Fader()
{
}

Fader *Fader::GetInstance()
{
	static Fader fader;

	return &fader;
}



Fader::~Fader()
{
}

void Fader::Init()
{
	GetInstance()->mySprite = new Tga2D::CSprite("Sprites/white_pixel.dds");
	GetInstance()->mySprite->SetPivot({ 0.5f, 0.5f });
	GetInstance()->mySprite->SetPosition({0.5f, 0.5f});
	GetInstance()->mySprite->SetSizeRelativeToScreen({2.0f, 1.0f});

	GetInstance()->myState = FadeState::Invisable;

	//The different fade colors
	GetInstance()->myRedColor = new Tga2D::CColor(150.0f / 255.0f, 8.0f / 255.0f, 8.0f / 255.0f, 1.0f);
	GetInstance()->myBlackColor = new Tga2D::CColor(0.0f / 255.0f, 0.0f / 255.0f, 0.0f / 255.0f, 0.0f);
	GetInstance()->myWhiteColor = new Tga2D::CColor(200.0f / 255.0f, 200.0f / 255.0f, 200.0f / 255.0f, 1.0f);
	GetInstance()->myTransparentColor = new Tga2D::CColor(200.0f / 255.0f, 200.0f / 255.0f, 200.0f / 255.0f, 0.0f);

	GetInstance()->myCurrentColor = new Tga2D::CColor(255.0f / 255.0f, 255.0f / 255.0f, 255.0f / 255.0f, 1.0f);
	GetInstance()->myAlpha = 0.0f;

	GetInstance()->myCurrentColor->myA = GetInstance()->myAlpha;

	GetInstance()->myFadeSpeed = 1.0f;


	GetInstance()->myShouldFadeIn = false;
	GetInstance()->myShouldFadeOut = false;

	GetInstance()->mySprite->SetColor(*(GetInstance()->myCurrentColor));
	GetInstance()->myNextColor = nullptr;

}

FadeState Fader::GetState() 
{
	return GetInstance()->myState;
}

//Returns true when done
bool Fader::DecreaseAlpha(float aDeltaTime)
{
	if (GetInstance()->myAlpha > 0.f) 
	{
		GetInstance()->myState = FadeState::Fading;
		GetInstance()->myAlpha -= aDeltaTime * GetInstance()->myFadeSpeed;

		if(GetInstance()->myAlpha < 0.f)
		{
			GetInstance()->myAlpha = 0.0f;
		}

		GetInstance()->myCurrentColor->myA = GetInstance()->myAlpha;
		GetInstance()->mySprite->SetColor(*(GetInstance()->myCurrentColor));
	}

	if (GetInstance()->myAlpha <= 0.0f) 
	{
		GetInstance()->myState = FadeState::Invisable;
		return true;
	}

	return false;
}

bool Fader::IncreaseAlpha(float aDeltaTime)
{
	if (GetInstance()->myAlpha < 1.0f)
	{
		GetInstance()->myState = FadeState::Fading;
		GetInstance()->myAlpha += aDeltaTime * GetInstance()->myFadeSpeed;

		if (GetInstance()->myAlpha > 1.0f)
		{
			GetInstance()->myAlpha = 1.0f;
		}

		GetInstance()->myCurrentColor->myA = GetInstance()->myAlpha;
		GetInstance()->mySprite->SetColor(*(GetInstance()->myCurrentColor));
	}


	if (GetInstance()->myAlpha >= 1.0f)
	{
		GetInstance()->myState = FadeState::Visable;
		return true;
	}

	return false;
}

void Fader::Update(float aDeltaTime)
{
	if (GetInstance()->myShouldFadeIn)
	{
		if (IncreaseAlpha(aDeltaTime))
		{
			GetInstance()->myShouldFadeIn = false;
		}
	}
	else if (GetInstance()->myShouldFadeOut)
	{
		if (DecreaseAlpha(aDeltaTime))
		{
			GetInstance()->myShouldFadeOut = false;
		}
	}
}

void Fader::Render()
{
	GetInstance()->mySprite->Render();
}

void Fader::FadeIn(FadeColor aColor, float aSpeed)
{

	switch (aColor)
	{
	case FadeColor::Red:
		FadeIn(*(GetInstance()->myRedColor), aSpeed);
		break;
	case FadeColor::Black:
		FadeIn(*(GetInstance()->myBlackColor), aSpeed);
		break;
	case FadeColor::White:
		FadeIn(*(GetInstance()->myWhiteColor), aSpeed);
		break;
	case FadeColor::Transparent:
		FadeIn(*(GetInstance()->myTransparentColor), aSpeed);
	default:
		break;
	}
}

void Fader::FadeIn(Tga2D::CColor aColor, float aSpeed)
{
	GetInstance()->myFadeSpeed = aSpeed;

	if (GetInstance()->myShouldFadeOut == true) 
	{
		GetInstance()->myShouldFadeOut = false;
	}
	else
	{
		GetInstance()->myAlpha = 0.0f;
		GetInstance()->myCurrentColor->myA = GetInstance()->myAlpha;
	}
	ChangeColor(aColor);

	GetInstance()->myShouldFadeIn = true;
}

void Fader::FadeOut(FadeColor aColor, float aSpeed)
{
	switch (aColor)
	{
	case FadeColor::Red:
		FadeOut(*(GetInstance()->myRedColor), aSpeed);
		break;
	case FadeColor::Black:
		FadeOut(*(GetInstance()->myBlackColor), aSpeed);
		break;
	case FadeColor::White:
		FadeOut(*(GetInstance()->myWhiteColor), aSpeed);
		break;
	default:
		break;
	}
}

void Fader::FadeOut(Tga2D::CColor aColor, float aSpeed)
{
	GetInstance()->myFadeSpeed = aSpeed;

	if (GetInstance()->myShouldFadeIn == true)
	{
		GetInstance()->myShouldFadeIn = false;
	}
	else 
	{
		GetInstance()->myAlpha = 1.0f;
		GetInstance()->myCurrentColor->myA = GetInstance()->myAlpha;
	}



	ChangeColor(aColor);
	GetInstance()->myShouldFadeOut = true;
}

bool Fader::IsNotFading()
{
	if (GetInstance()->myShouldFadeIn == false && GetInstance()->myShouldFadeOut == false) 
	{
		return true;
	}

	return true;
}

bool Fader::IsRendering()
{
	if(GetInstance()->myCurrentColor->myA > 0)
	{
		return true;
	}

	return false;
}

void Fader::ChangeColor(Tga2D::CColor aColor)
{
	*(GetInstance()->myCurrentColor) = aColor;

	GetInstance()->myCurrentColor->myA = GetInstance()->myAlpha;
}
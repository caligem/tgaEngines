#pragma once

namespace Tga2D 
{
	class CSprite;
	class CColor;
}

enum class FadeColor
{
	Red,
	Black, 
	White,
	Transparent
};

enum class FadeState
{
	Fading,
	Visable,
	Invisable
};

class Fader
{
public:
	~Fader();

	static void Init();

	static void Update(float aDeltaTime);
	static void Render();

	static void FadeIn(FadeColor aColor, float aSpeed = 1.0f);
	static void FadeIn(Tga2D::CColor aColor, float aSpeed = 1.0f);

	static void FadeOut(FadeColor aColor, float aSpeed = 1.0f);
	static void FadeOut(Tga2D::CColor aColor, float aSpeed = 1.0f);


	static bool IsNotFading();

	static bool IsRendering();
	static FadeState GetState();

private: 

	Fader();

	static Fader* GetInstance(); 
	static void ChangeColor(Tga2D::CColor aColor);


	static bool DecreaseAlpha(float aDeltaTime);
	static bool IncreaseAlpha(float aDeltaTime);

	Tga2D::CSprite* mySprite;
	Tga2D::CColor* myCurrentColor;
	Tga2D::CColor* myNextColor;

	Tga2D::CColor* myRedColor;
	Tga2D::CColor* myBlackColor;
	Tga2D::CColor* myWhiteColor;
	Tga2D::CColor* myTransparentColor;

	bool myShouldFadeOut;
	bool myShouldFadeIn;

	float myAlpha;
	float myFadeSpeed;

	FadeState myState;
};


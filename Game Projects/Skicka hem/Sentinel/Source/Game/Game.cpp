#include "stdafx.h"
#include <tga2d/Engine.h>
#include "Game.h"
#include "Timer.h"
#include "SoundEngine.h"
#include <tga2d/error/error_manager.h>
#include "Fader.h"
#include "DialogueManager.h"
#include <exception>

#include "InputWrapper.h"
#include <string>
using namespace std::placeholders;

#ifdef _DEBUG
#pragma comment(lib,"DX2DEngine_Debug.lib")
#endif // DEBUG
#ifdef _RELEASE
#pragma comment(lib,"DX2DEngine_Release.lib")
#endif // DEBUG
#ifdef _RETAIL
#pragma comment(lib,"DX2DEngine_Retail.lib")
#endif // DEBUG

CGame::CGame()
{
}


CGame::~CGame()
{
	CommonUtilities::Timer::Destroy();
}


bool CGame::Init(const std::wstring& aVersion, HWND aHWND)
{
	unsigned short windowWidth = 1920;
	unsigned short windowHeight = 1080; 

	// This is used to get the size of the top border, and the to change the windows size width that only use it to change window size not resolution / render size!!!!
	int height = (GetSystemMetrics(SM_CYFRAME) + GetSystemMetrics(SM_CYCAPTION) +
		GetSystemMetrics(SM_CXPADDEDBORDER));

	//In Engine.cpp in function void Tga2D::CEngine::SetResolution(const Tga2D::Vector2<unsigned int> &aResolution, bool aAlsoSetWindowSize) height is set as well
	//the function is on line 306 in engine.cpp

    Tga2D::SEngineCreateParameters createParameters;
	createParameters.myActivateDebugSystems = Tga2D::eDebugFeature_Fps | Tga2D::eDebugFeature_Mem | Tga2D::eDebugFeature_Filewatcher | Tga2D::eDebugFeature_Cpu | Tga2D::eDebugFeature_Drawcalls | Tga2D::eDebugFeature_OptimiceWarnings;
    
    createParameters.myInitFunctionToCall = std::bind( &CGame::InitCallBack, this );
    createParameters.myUpdateFunctionToCall = std::bind( &CGame::UpdateCallBack, this );
	createParameters.myWinProcCallback = std::bind(&CGame::WinProc, this, _1, _2, _3, _4);
    createParameters.myLogFunction = std::bind( &CGame::LogCallback, this, _1 );
    createParameters.myWindowHeight = windowHeight + height;
    createParameters.myWindowWidth = windowWidth;
	createParameters.myRenderHeight = windowHeight;
	createParameters.myRenderWidth = windowWidth;
	createParameters.myTargetWidth = windowWidth;
	createParameters.myTargetHeight = windowHeight;
	createParameters.myWindowSetting = Tga2D::EWindowSetting_Overlapped;
	createParameters.myAutoUpdateViewportWithWindow = true;
    //createParameters.myClearColor.Set(0.0f / 255.0f, 143.0f / 255.0f, 212.0f / 255.0f, 1.0f );
	createParameters.myClearColor.Set(0.0f, 0.0f, 0.0f , 1.0f);
	createParameters.myStartInFullScreen = false;

	if (aHWND != nullptr)
	{
		createParameters.myHwnd = new HWND(aHWND);
	}
#ifdef _RELEASE
	std::wstring appname = L"TGA 2D RELEASE [" + aVersion + L"] ";
#endif
#ifdef _RETAIL
	std::wstring appname = L"TGA 2D RETAIL  [" + aVersion + L"] ";
#endif // _RETAIL
#ifdef _DEBUG
	std::wstring appname = L"TGA 2D DEBUG  [" + aVersion + L"] ";
#endif

	createParameters.myApplicationName = L"Sentinel";
    createParameters.myEnableVSync = false;

    Tga2D::CEngine::CreateInstance( createParameters );

    if( !Tga2D::CEngine::GetInstance()->Start() )
    {
        ERROR_PRINT( "Fatal error! Engine could not start!" );
		system("pause");
		return false;
    }

	return true;
}


LRESULT CGame::WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	lParam;
	myInputManager.SetApplicationWindow(hWnd);
	myInputManager.HandleInput(message, static_cast<unsigned int>(wParam));

	switch (message)
	{
		case WM_DESTROY:
		{
			// close the application entirely
			if (myGameWorld.GetInGameState() != nullptr)
			{
				myGameWorld.GetInGameState()->TerminateThread();
			}
			PostQuitMessage(0);
			return 0;
		}
	}

	return 0;
}

void CGame::InitCallBack()
{
	CommonUtilities::Timer::Create();

	SoundEngine::Init();
	InputWrapper::Init(myInputManager);
	Fader::Init();
	DialogueManager::Load();

	Fader::Init();
    myGameWorld.Init(myInputManager);

	DialogueManager::Load();
}

void CGame::UpdateCallBack()
{
	CommonUtilities::Timer::Update();
	myGameWorld.Update();
	myGameWorld.Render();

	DialogueManager::Update(Timer::GetDeltaTime());
	DialogueManager::Render();
	SoundEngine::Update(Timer::GetDeltaTime());

	Fader::Update(Timer::GetDeltaTime());



	if (InputWrapper::IsButtonPressed(KeyButton::Jump) || InputWrapper::IsButtonPressed(KeyButton::Use) 
		|| InputWrapper::IsButtonPressed(KeyButton::Hook) || InputWrapper::IsButtonPressed(KeyButton::Pickup))
	{
		if (DialogueManager::IsPlaying()) 
		{
			DialogueManager::NextSlide();
		}
	}

	if (InputWrapper::IsButtonPressed(KeyButton::Pause)) 
	{
		DialogueManager::StopPlaying();
	}



	myInputManager.Update();
}


void CGame::LogCallback( std::string aText )
{
}

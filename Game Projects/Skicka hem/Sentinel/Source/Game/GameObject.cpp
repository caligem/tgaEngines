#include "stdafx.h"
#include "GameObject.h"
#include "TileSize.h"
#include "Camera.h"
#include "LineCollider.h"
#include "cit/utility.h"
GameObject::GameObject()
{
	myCollider = nullptr;
	myShouldRemove = false;
	myCollider = nullptr;
	myZ = 0.0f;
}

void GameObject::InitGO(const CommonUtilities::Vector2f &aPosition, const char * aPath, Drawable * aDrawable)
{
	myPosition = aPosition * TILE_MULTIPLIER;
	myRotation = 0.0f;
	myDrawable = aDrawable;
	myDrawable->Init(aPath);
	float amountOfTilesX = myDrawable->GetImagesSizePixel().x / TILE_SIZE;
	float amountOfTilesY = myDrawable->GetImagesSizePixel().y / TILE_SIZE;
	myPosition.x += (static_cast<float>(myDrawable->GetImagesSizePixel().x) / amountOfTilesX) * TILE_MULTIPLIER / 1920.0f;
	myPosition.y -= (static_cast<float>(myDrawable->GetImagesSizePixel().y) / amountOfTilesY) * TILE_MULTIPLIER / 1080.0f;

	myDrawable->SetPosition(myPosition);
	myOrginalPosition = myPosition;
}

GameObject::~GameObject()
{

	cit::SafeDelete(myCollider);

	cit::SafeDelete(myDrawable);
}

void GameObject::UpdateAllGameObjects(CommonUtilities::GrowingArray<GameObject*>& aGrowingArrayOfGameObjects)
{
	const int allGOSize = aGrowingArrayOfGameObjects.Size();
	for (unsigned short i = 0; i < allGOSize; i++)
	{	
		if (aGrowingArrayOfGameObjects[i]->GetPosition().x < Camera::GetInstance().GetPosition().x + 0.6f &&
			aGrowingArrayOfGameObjects[i]->GetPosition().x > Camera::GetInstance().GetPosition().x - 0.6f &&
			aGrowingArrayOfGameObjects[i]->GetPosition().y < Camera::GetInstance().GetPosition().y + 0.6f &&
			aGrowingArrayOfGameObjects[i]->GetPosition().y > Camera::GetInstance().GetPosition().y - 0.6f)
		{
			aGrowingArrayOfGameObjects[i]->AccessDrawable().SetShouldRender(true);
		}
		else
		{
			aGrowingArrayOfGameObjects[i]->AccessDrawable().SetShouldRender(false);
		}
		aGrowingArrayOfGameObjects[i]->OnUpdate();
	}
}

void GameObject::Reset(Player* aPlayerPointer)
{
	myPosition = myOrginalPosition;
}

void GameObject::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*> &aRenderBuffer)
{
	myDrawable->SetPosition(myPosition);
	aRenderBuffer.Add(myDrawable);
}

const CommonUtilities::Vector2f & GameObject::GetPosition() const
{
	return myPosition;
}

Drawable& GameObject::AccessDrawable()
{
	return *myDrawable;
}

void GameObject::AttachCollider(BoxCollider &aBoxColToAttach)
{
	myCollider = new BoxCollider(aBoxColToAttach.GetWidth(), aBoxColToAttach.GetHeight(), aBoxColToAttach.GetOffset());
	myCollider->SetIsSolid(aBoxColToAttach.GetIsSolid());
	myCollider->SetTag(aBoxColToAttach.GetTag());
	myCollider->SetOwner(this);
	
}

void GameObject::AttachCollider(CircleCollider & aCircleColToAttach)
{
	myCollider = new CircleCollider(aCircleColToAttach.GetRadius(), aCircleColToAttach.GetOffset());
	myCollider->SetIsSolid(aCircleColToAttach.GetIsSolid());
	myCollider->SetTag(aCircleColToAttach.GetTag());
	myCollider->SetOwner(this);
}

void GameObject::AttachCollider(LineCollider & aLineColToAttach)
{
	myCollider = new LineCollider(aLineColToAttach.GetDirection(), aLineColToAttach.GetLength(), aLineColToAttach.GetOffset());
	myCollider->SetIsSolid(aLineColToAttach.GetIsSolid());
	myCollider->SetTag(aLineColToAttach.GetTag());
	myCollider->SetOwner(this);
}

void GameObject::FillCollisionBuffer(CommonUtilities::GrowingArray<GameObject*>& aBufferToAddTo)
{
	if (myCollider != nullptr)
	{
		aBufferToAddTo.Add(this);
	}
}

void GameObject::SetPosition(const CommonUtilities::Vector2f & aPosToSet)
{
	myPosition = aPosToSet;
	myDrawable->SetPosition(myPosition);
}

void GameObject::SetRotation(const float aRotation)
{
	myRotation = aRotation;
}

float GameObject::GetRotation()
{
	return myRotation;
}

Collider *GameObject::GetCollider() 
{
	return myCollider;
}

bool GameObject::GetShouldRemove()
{
	return myShouldRemove;
}

void GameObject::SetShouldRemove(const bool aShouldRemove)
{
	myShouldRemove = aShouldRemove;
}

void GameObject::OnCollisionEnter(Collider  &other)
{
	other;
}

void GameObject::OnCollisionStay(Collider  &other)
{
	other;
}

void GameObject::OnCollisionLeave(Collider  &other)
{
	other;
}

void GameObject::OnUpdate()
{
	myDrawable->SetPosition(myPosition);
}

void GameObject::SetOriginalPosition()
{
	myOrginalPosition = myPosition;
}


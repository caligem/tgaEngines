#pragma once
#include "Vector.h"
#include "BoxCollider.h"
#include "CircleCollider.h"
#include "GrowingArray.h"
#include "Drawable.h"
#include "Sprite.h"

class Player;

class GameObject
{
	friend Collider;
public:
	GameObject();
	virtual ~GameObject();

	void InitGO(const CommonUtilities::Vector2f &aPosition, const char* aPath, Drawable* aDrawable);
	static void UpdateAllGameObjects(CommonUtilities::GrowingArray<GameObject*> & aGrowingArrayOfGameObjects);
	virtual void Reset(Player* aPlayerPointer = nullptr);
	void AttachCollider(BoxCollider &aBoxColToAttach);
	void AttachCollider(CircleCollider &aCircleColToAttach);
	void AttachCollider(LineCollider &aCircleColToAttach);
	virtual void FillCollisionBuffer(CommonUtilities::GrowingArray<GameObject*> &aBufferToAddTo);
	void SetPosition(const CommonUtilities::Vector2f &aPosToSet);
	void SetRotation(const float aRotation);
	float GetRotation();

	virtual void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*> &aRenderBuffer);
	const CommonUtilities::Vector2f & GetPosition() const;

	Drawable& AccessDrawable();
	Collider *GetCollider();
	bool GetShouldRemove();
	void SetShouldRemove(const bool aShouldRemove);
	virtual void OnTriggerd() {};

	virtual void OnUpdate();

	virtual void SetOriginalPosition();

protected:
	virtual void OnCollisionEnter(Collider  &other);
	virtual void OnCollisionStay(Collider  &other);
	virtual void OnCollisionLeave(Collider  &other);
	Collider *myCollider;
	Drawable *myDrawable;
	CommonUtilities::Vector2f myPosition;
	CommonUtilities::Vector2f myOrginalPosition;
	float myZ;
	float myRotation;
	bool myShouldRemove;
private:
};


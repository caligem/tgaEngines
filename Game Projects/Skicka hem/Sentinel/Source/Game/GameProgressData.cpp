#include "stdafx.h"
#include "GameProgressData.h"

void SaveGameProgress(const GameProgressData aProgress)
{
	json progressFile = LoadData("Data/progress.json");

	progressFile["level1Completed"] = aProgress.myHasCompletedLevel1;
	progressFile["level2Completed"] = aProgress.myHasCompletedLevel2;
	progressFile["level3Completed"] = aProgress.myHasCompletedLevel3;
	progressFile["level1PickedUpCollectibles"] = aProgress.myLevel1Collectables;
	progressFile["level2PickedUpCollectibles"] = aProgress.myLevel2Collectables;
	progressFile["level3PickedUpCollectibles"] = aProgress.myLevel3Collectables;
	progressFile["level1MaxCollectibles"] = aProgress.myLevel1NumberOfCollectables;
	progressFile["level2MaxCollectibles"] = aProgress.myLevel2NumberOfCollectables;
	progressFile["level3MaxCollectibles"] = aProgress.myLevel3NumberOfCollectables;
	std::ofstream saveTo("Data/progress.json");
	saveTo << std::setw(4) << progressFile << std::endl;
	saveTo.close();

}

GameProgressData LoadGameProgress()
{
	json progressFile = LoadData("Data/progress.json");

	GameProgressData returnProgress;
	returnProgress.myCurrentLevelIndex = 0;
	returnProgress.myHasCompletedLevel1 = progressFile["level1Completed"];
	returnProgress.myHasCompletedLevel2 = progressFile["level2Completed"];
	returnProgress.myHasCompletedLevel3 = progressFile["level3Completed"];

	returnProgress.myLevel1Collectables = progressFile["level1PickedUpCollectibles"];
	returnProgress.myLevel2Collectables = progressFile["level2PickedUpCollectibles"];
	returnProgress.myLevel3Collectables = progressFile["level3PickedUpCollectibles"];

	returnProgress.myLevel1NumberOfCollectables = progressFile["level1MaxCollectibles"];
	returnProgress.myLevel2NumberOfCollectables = progressFile["level2MaxCollectibles"];
	returnProgress.myLevel3NumberOfCollectables = progressFile["level3MaxCollectibles"];
	returnProgress.myShouldResetRoom = false;

	return returnProgress;
}

GameProgressData CreateNewGame()
{
	json progressFile = LoadData("Data/progress.json");

	progressFile["level1Completed"] = false;
	progressFile["level2Completed"] = false;
	progressFile["level3Completed"] = false;

	progressFile["level1PickedUpCollectibles"] = 0;
	progressFile["level2PickedUpCollectibles"] = 0;
	progressFile["level3PickedUpCollectibles"] = 0;

	progressFile["level1MaxCollectibles"] = progressFile["level1MaxCollectibles"];
	progressFile["level2MaxCollectibles"] = progressFile["level2MaxCollectibles"];
	progressFile["level3MaxCollectibles"] = progressFile["level3MaxCollectibles"];

	std::ofstream saveTo("Data/progress.json");
	saveTo << std::setw(4) << progressFile << std::endl;
	saveTo.close();

	return LoadGameProgress();
}

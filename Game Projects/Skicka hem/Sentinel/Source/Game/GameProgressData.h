#pragma once
#include "JsonWrapper.h"

struct GameProgressData
{
	int myCurrentLevelIndex = 0;
	bool myHasCompletedLevel1 = false;
	bool myHasCompletedLevel2 = false;
	bool myHasCompletedLevel3 = false;
	int myLevel1Collectables = 0;
	int myLevel2Collectables = 0;
	int myLevel3Collectables = 0;
	int myLevel1NumberOfCollectables = 0;
	int myLevel2NumberOfCollectables = 0;
	int myLevel3NumberOfCollectables = 0;
	bool myShouldResetRoom = false;
	bool myHasCompletedWarpZone1 = false;
	bool myHasCompletedWarpZone2 = false;
	bool myHasCompletedWarpZone3 = false;
};

void SaveGameProgress(const GameProgressData aProgress);

GameProgressData LoadGameProgress();

GameProgressData CreateNewGame();
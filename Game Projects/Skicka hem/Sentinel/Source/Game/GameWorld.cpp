#include "stdafx.h"
#include "GameWorld.h"

#include <tga2d/Engine.h>
#include <stdlib.h>
#include <time.h>

#include "SoundEngine.h"

#include "Camera.h"
#include "BatchManager.h"
#include "TriggerManager.h"
#include "InputManager.h"
#include "PostMaster.h"
#include "ParticleEmitterManager.h"
#include "SplashScreenState.h"
CGameWorld::CGameWorld()
{
}


CGameWorld::~CGameWorld()
{
	BatchManager::Destroy();
	TriggerManager::Destroy();
	PostMaster::Destroy();
	ParticleEmitterManager::Destroy();
}

void CGameWorld::Init(CommonUtilities::InputManager &aInputManager)
{
	Tga2D::CEngine::GetInstance()->SetFullScreen(true);
	myInputManager = &aInputManager;
	srand(static_cast<unsigned int>(time(static_cast<unsigned int>(NULL))));

	ParticleEmitterManager::Create();
	BatchManager::Create();
	TriggerManager::Create();
	PostMaster::Create();

	myStateStack.PushMainState(new MainMenuState(myInputManager));
	myStateStack.PushSubState(new SplashScreenState());
	// REVERT WHEN SHIP

	Tga2D::CEngine::GetInstance()->SetSampler(ESamplerType_Point);

	Camera::GetInstance().Init();
}


void Start()
{

}

void Update()
{

}


void CGameWorld::Update()
{ 		
	if (myStateStack.GetSize() > 0)
	{
		eStateStackMessage returnState = myStateStack.GetCurrentState()->Update();
		if (returnState == eStateStackMessage::PopMainState)
		{
			myStateStack.PopMainState();
		}
		else if (returnState == eStateStackMessage::PopSubState)
		{
			myStateStack.PopSubState();
		}
		else if (returnState == eStateStackMessage::PopGameAndLevelSelect)
		{
			myStateStack.PopMainState();
			myStateStack.PopSubState();
		}
		else if (returnState == eStateStackMessage::PopMainStateAndPushInGameState)
		{
			myStateStack.PopMainState();
			myStateStack.PushMainState(new InGameState(myInputManager, myStateStack.myGameProgressData.myCurrentLevelIndex));
		}
		else if (returnState == eStateStackMessage::PopCutsceneAndPushCredits)
		{
			myStateStack.PopSubState();
			myStateStack.PushSubState(new CreditsState(myInputManager, true));
		}
		else if (returnState == eStateStackMessage::PopCreditsAndPushLevelCompleted)
		{
			myStateStack.PopSubState();
			CompletedLevelData levelCompletedData;
			levelCompletedData.myCollectibles = myStateStack.myGameProgressData.myLevel1Collectables;
			levelCompletedData.myTotalCollectibles = myStateStack.myGameProgressData.myLevel3NumberOfCollectables;
			levelCompletedData.myLevelindex = myStateStack.myGameProgressData.myCurrentLevelIndex;

			myStateStack.PushSubState(new LevelCompletedState(myInputManager, levelCompletedData));
		}
		else if (returnState == eStateStackMessage::PopPromptAndPushLevelSelect)
		{
			myStateStack.PopSubState();
			myStateStack.PushSubState(new LevelSelectState(myInputManager));
		}

	}
	else
	{
		Tga2D::CEngine::GetInstance()->Shutdown();
	}
}

void CGameWorld::Render()
{
	if (myStateStack.GetSize() != 0)
	{
		myStateStack.Render();
	}
}

GameState* CGameWorld::GetInGameState()
{
	return myStateStack.GetInGameState();
}

#pragma once
#include "StateStack.h"
class GameState;

namespace CommonUtilities
{
	class InputManager;
}
namespace Tga2D
{
	class CSprite;
}
class CGameWorld
{
public:
	CGameWorld(); 
	~CGameWorld();

	void Init(CommonUtilities::InputManager &aInputManager);
	void Update(); 
	void Render();

	GameState* GetInGameState();

private:
	CommonUtilities::InputManager *myInputManager;
	StateStack myStateStack;
};
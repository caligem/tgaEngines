#include "stdafx.h"
#include "Goal.h"
#include "RoomChangeObserver.h"
#include "TriggerManager.h"

#include "JsonWrapper.h"
#include "TileSize.h"
#include "Player.h"
Goal::Goal(RoomChangeObserver* aRoomChangeObserver)
{
	AttachObserver(aRoomChangeObserver);

}


Goal::~Goal()
{

}

#include <tga2d\engine.h>
void Goal::Init(const char * aTypePath)
{
	json goalData = LoadData(aTypePath);
	myNextRoomIndex = goalData["nextRoom"];
	mySpawnPosInNextRoom.x = goalData["spawnPosX"];
	mySpawnPosInNextRoom.x -= TILE_SIZE;
	mySpawnPosInNextRoom.x *= TILE_MULTIPLIER;
	mySpawnPosInNextRoom.x /= Tga2D::CEngine::GetInstance()->GetWindowSize().x;

	mySpawnPosInNextRoom.y = goalData["spawnPosY"];
	mySpawnPosInNextRoom.y -= TILE_SIZE;
	mySpawnPosInNextRoom.y *= TILE_MULTIPLIER;
	mySpawnPosInNextRoom.y /= Tga2D::CEngine::GetInstance()->GetWindowSize().y;
}

void Goal::OnUpdate()
{
	myDrawable->SetPosition({ myPosition.x, myPosition.y });
}

void Goal::OnCollisionEnter(Collider & other)
{
	if (other.GetTag() == ColliderTag::Player)
	{
		SendMessageToObserver(eMessageType::ReachedGoal, myNextRoomIndex);
		other.GetOwner()->SetPosition(mySpawnPosInNextRoom);
		static_cast<Player*>(other.GetOwner())->SetRespawnPosition(mySpawnPosInNextRoom);
	}
}

void Goal::OnCollisionStay(Collider & other)
{

}

void Goal::AddToTriggerManager(int aIndex)
{
	TriggerManager::GetInstance()->InsertGoalAtIndex(aIndex, this);
}

#include <iostream>
void Goal::OnTriggerd()
{
	std::cout << "Goal Triggerd" << std::endl;
}

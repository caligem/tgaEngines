#include "stdafx.h"
#include "Goomba.h"
#include "Timer.h"
#include "Player.h"
#include "JsonWrapper.h"
#include "LineCollider.h"
#include "SoundEngine.h"

using namespace CommonUtilities;

Goomba::Goomba()
{
	myGroundChecker.InitGO({ myPosition.x, myPosition.y }, "sprites/noObj.dds", new Sprite());
	LineCollider myGroundCheckCol;
	myGroundCheckCol.Init(CommonUtilities::Vector2f{ 1.0f, 0.0f }, 0.001f, CommonUtilities::Vector2f{ -0.018f, 0.05f });
	myGroundCheckCol.SetTag(ColliderTag::GroundChecker);
	myGroundChecker.AttachCollider(myGroundCheckCol);
}


Goomba::~Goomba()
{
	myDeathSound->Annahilate();
	myAttackSound->Annahilate();
}

void Goomba::Init(const char* aPath, const float aStaringDirectionX)
{
	json goombaData = LoadEnemyData(aPath);

	myHasPlayedDSound = false;
	myDeathSound = new Sound("Sounds/sfx/goomba_death.wav");
	myAttackSound = new Sound("Sounds/sfx/goomba_death.wav");

	mySpeed = goombaData["speed"];
	myPhysX.SetWeight(goombaData["weight"]);
	myTurnFrequency = goombaData["turnFrequency"];
	myHealth = goombaData["health"];
	myBounceHeight = goombaData["playerBounceHeight"];

	myAttackAnim.Init(goombaData["attackAnimationKey"].get<std::string>().c_str());
	myMoveAnim.Init(goombaData["moveAnimationKey"].get<std::string>().c_str());
	myDeathAnim.Init(goombaData["deathAnimationKey"].get<std::string>().c_str());
	myActiveAnim = &myMoveAnim;

	myDirection.x = aStaringDirectionX;
	myStartingDirection = aStaringDirectionX;

	myIsDead = false;
	myTurnTimer = 0.f;
	myTurnFrequency = 0.3f;
	myRemoveTimer.Set(myDeathAnim.GetTotalTime(), cit::countdown::type::oneshot, [&] {	myShouldRemove = true;});
}

void Goomba::OnUpdate()
{
	myRemoveTimer.Update(CommonUtilities::Timer::GetDeltaTime());
	if (myIsDead)
	{
		myPhysX.SetVelocity({ 0.0f,0.0f });
		myActiveAnim = &myDeathAnim;
		if (!myHasPlayedDSound) 
		{
			SoundEngine::PlaySound(*myDeathSound);
			myHasPlayedDSound = true;
		}
	}

	if (myGroundChecker.IsColliding() == false)
	{
		myDirection.x *= -1;
	}

	if (myDirection.x < 0)
	{
		myActiveAnim->SetInversedX(false);
		myGroundChecker.GetCollider()->SetOffset({ -0.035f, myGroundChecker.GetCollider()->GetOffset().y });
	}
	else if (myDirection.x > 0)
	{
		myActiveAnim->SetInversedX(true);
		myGroundChecker.GetCollider()->SetOffset({ 0.035f, myGroundChecker.GetCollider()->GetOffset().y });
	}

	if (myIsDead == false)
	{
		myPhysX.SetVelocity({ myDirection.x * mySpeed, myPhysX.GetVelocity().y });
	}

	myPhysX.Update(*myCollider, myPosition);
	myDrawable->SetPosition({ myPosition.x, myPosition.y });
	myActiveAnim->SetPosition(myPosition);
	GameObject::OnUpdate();

	myGroundChecker.GetCollider()->SetIsSolid(false);
	myGroundChecker.SetPosition(myPosition);
	myGroundChecker.OnUpdate();
}

void Goomba::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*> &aRenderBuffer)
{
	aRenderBuffer.Add(myActiveAnim);
}

void Goomba::FillCollisionBuffer(CommonUtilities::GrowingArray<GameObject*>& aBufferToAddTo)
{
	if (myIsDead == false)
	{
		GameObject::FillCollisionBuffer(aBufferToAddTo);
	}
	aBufferToAddTo.Add(&myGroundChecker);
}

void Goomba::Reset(Player* aPlayerPointer)
{
	GameObject::Reset();
	myShouldRemove = false;
	myAttackAnim.SetCurrentFrame(0);
	myMoveAnim.SetCurrentFrame(0);
	myDeathAnim.SetCurrentFrame(0);
	myIsDead = false;
	myActiveAnim = &myMoveAnim;
	myRemoveTimer.Reset();
	myDirection.x = myStartingDirection;
	myHasPlayedDSound = false;
}

void Goomba::OnCollisionEnter(Collider & other)
{
	if (other.GetTag() == ColliderTag::Player)
	{
		if (myIsDead == false)
		{
			myActiveAnim = &myAttackAnim;
			dynamic_cast<Player*>(other.GetOwner())->Die();
		}
	}
	else if (other.GetTag() == ColliderTag::Grabbable)
	{
		Vector2f collisionVelocity = dynamic_cast<GrabbableObject*>(other.GetOwner())->GetPhysx().GetVelocity();
		if (collisionVelocity.Length() > 0.1f)
		{
			TakeDamage(1);
			myActiveAnim = &myDeathAnim;
			myRemoveTimer.Start();
			other.GetOwner()->SetShouldRemove(true);
		}
		else
		{
			Vector2f collisionNormal = other.GetOwner()->GetPosition() - myPosition;
			collisionNormal.Normalize();
			float dot = Vector2f({ myDirection.x, 0 }).Dot(collisionNormal);
			if (acosf(dot) < 0.35f)
			{
				myDirection.x *= -1;
				myPosition.x += myDirection.x * mySpeed * Timer::GetDeltaTime();
			}
		}
	}
	else
	{
		Vector2f collisionNormal = other.GetOwner()->GetPosition() - myPosition;
		collisionNormal.Normalize();
		float dot = Vector2f({ myDirection.x, 0 }).Dot(collisionNormal);
		if (acosf(dot) < 0.2f)
		{
			myDirection.x *= -1;
			myPosition.x += myDirection.x * mySpeed * Timer::GetDeltaTime();
		}
	}
}

void Goomba::OnCollisionStay(Collider & other)
{
	if (other.GetTag() == ColliderTag::Tile)
	{
		myPhysX.SetVelocity({ myPhysX.GetVelocity().x, 0.f });
	}
}

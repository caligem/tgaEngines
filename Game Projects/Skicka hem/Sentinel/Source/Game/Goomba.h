#pragma once
#include "Enemy.h"
#include "PhysicsComponent.h"
#include "GroundChecker.h"
#include "Animation.h"

class Sound;

class Goomba : public Enemy
{
public:
	Goomba();
	~Goomba();

	void Init(const char* aPath, const float aStaringDirectionX);
	void OnUpdate() override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*> &aRenderBuffer) override;
	void FillCollisionBuffer(CommonUtilities::GrowingArray<GameObject*>& aBufferToAddTo) override;

	void Reset(Player* aPlayerPointer = nullptr) override;
protected:
	bool myHasPlayedDSound;
	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionStay(Collider &other) override;
	float myTurnTimer;
	float myTurnFrequency;
	float mySpeed;
	float myBounceHeight;
	float myStartingDirection;
	PhysicsComponent myPhysX;
	GroundChecker myGroundChecker;

	Animation myMoveAnim;
	Animation myAttackAnim;
	Animation myDeathAnim;

	Animation *myActiveAnim;

	cit::countdown myRemoveTimer;

	Sound* myDeathSound;
	Sound* myAttackSound;
};


#include "stdafx.h"
#include "GrabbableObject.h"
#include "Timer.h"
#include "JsonWrapper.h"
#include "MovingTile.h"
#include "Animation.h"
#include "SoundEngine.h"

GrabbableObject::GrabbableObject()
{
	myThrownDirection = { 1.0f, -1.0f };
	myThrownDirection.Normalize();
	myLastHitMovingTile = nullptr;
	myRespawnAnimation = nullptr;
	myDestroyAnimation = nullptr;
	myOriginalSprite = nullptr;
	myIsBeingDestroyed = false;
}


GrabbableObject::~GrabbableObject()
{
	myDestroyedSound->Annahilate();
}

void GrabbableObject::Init(const std::string& aPhysicsType)
{
	json data = LoadObjectType(aPhysicsType.c_str());
	myPhysX.Init(*myCollider);
	myPhysX.SetWeight(data["weight"]);
	myPhysX.SetFriction(data["friction"]);
	myPhysX.SetBouncyness(data["bouncyness"]);
	myPhysX.IgnoreTag(ColliderTag::Climbable);
	myPhysX.IgnoreTag(ColliderTag::Enemy);

	if (data["respawnAnimationKey"].is_string())
	{
		std::string &respawnKey = data["respawnAnimationKey"].get<std::string>();
		myRespawnAnimation = new Animation();
 		myRespawnAnimation->Init(respawnKey.c_str());
	}
	if (data["destroyAnimationKey"].is_string())
	{
		std::string &destroyKey = data["destroyAnimationKey"].get<std::string>();
		myDestroyAnimation = new Animation();
		myDestroyAnimation->Init(destroyKey.c_str());
	}
	myOriginalSprite = static_cast<Sprite*>(myDrawable);

	myDestroyedSound = new Sound("Sounds/Sfx/grabbable_destroyed.wav");
}

void GrabbableObject::OnUpdate()
{
	if (myIsBeingDestroyed == true)
	{
		if (myDestroyAnimation != nullptr)
		{
			myDrawable = myDestroyAnimation;
			if (myDestroyAnimation->GetCurrentFrame() == myDestroyAnimation->GetAmountOfFrames()-1)
			{
				if (myRespawnAnimation != nullptr)
				{
					myRespawnAnimation->SetCurrentFrame(0);
					myDrawable = myRespawnAnimation;
				}
				else
				{
					myDrawable = myOriginalSprite;
				}
				myPhysX.SetVelocity(CommonUtilities::Vector2f({ 0.0f,0.0f }));
				myPosition = myOrginalPosition;
				myIsBeingDestroyed = false;
			}
		}
		else
		{
			if (myRespawnAnimation != nullptr)
			{
				myRespawnAnimation->SetCurrentFrame(0);
				myDrawable = myRespawnAnimation;
			}
			else
			{
				myDrawable = myOriginalSprite;
			}
			myPhysX.SetVelocity(CommonUtilities::Vector2f({ 0.0f,0.0f }));
			myPosition = myOrginalPosition;
			myIsBeingDestroyed = false;
		}
	}
	if (myLastHitMovingTile != nullptr)
	{
		MovingTile *movingTileHit = static_cast<MovingTile*>(myLastHitMovingTile->GetOwner());
		myPosition.x += movingTileHit->GetMovementThisFrame().x;
		myPosition.y += movingTileHit->GetMovementThisFrame().y;
	}

	if (myIsBeingDestroyed == false)
	{
		myPhysX.Update(*myCollider, myPosition);
	}
	myDrawable->SetPosition({ myPosition.x, myPosition.y });
	GameObject::OnUpdate();
	myLastHitMovingTile = nullptr;

	if (myRespawnAnimation != nullptr && myDrawable == myRespawnAnimation)
	{
		if (myRespawnAnimation->GetCurrentFrame() == myRespawnAnimation->GetAmountOfFrames() - 1)
		{
			myDrawable = myOriginalSprite;
		}
	}
}

void GrabbableObject::ThrowRight(const float aForce) 
{
	myPhysX.SetVelocity(CommonUtilities::Vector2f{ myThrownDirection.x, myThrownDirection.y} * aForce);
	myPhysX.SetIsKinematic(false);
}

void GrabbableObject::ThrowLeft(const float aForce)
{
	myPhysX.SetVelocity(CommonUtilities::Vector2f{ -myThrownDirection.x, myThrownDirection.y }*aForce);
	myPhysX.SetIsKinematic(false);
}

void GrabbableObject::Release()
{
	myPhysX.SetVelocity(CommonUtilities::Vector2f{ 0.0f, 0.0f });
	myPhysX.SetIsKinematic(false);
}
#include "TileSize.h"
void GrabbableObject::OnCollisionEnter(Collider & other)
{	
	other;
	//ISSUE IS IF THERE IS ALREADY SOMETHING ELSE ON ITS ORIGINAL POSITION
	if (other.GetTag() == ColliderTag::Trap && myIsBeingDestroyed == false)
	{
		if (myDestroyAnimation != nullptr)
		{
			myDestroyAnimation->SetCurrentFrame(0);
			myDrawable = myDestroyAnimation;
		}
		else
		{
			myDrawable = myOriginalSprite;
		}
		myIsBeingDestroyed = true;

		SoundEngine::PlaySound(*myDestroyedSound);
	}
	else
	{
		myPhysX.OnIntersection(other);
	}
}

void GrabbableObject::OnCollisionStay(Collider & other)
{
	if (other.GetTag() == ColliderTag::MovingTile)
	{
		myLastHitMovingTile = &other;
	}
	if (myIsBeingDestroyed == false)
	{
		myPhysX.OnIntersection(other);
	}

}

void GrabbableObject::OnCollisionLeave(Collider & other)
{
	if (other.GetTag() == ColliderTag::Player)
	{
		RemoveIgnoreTag(ColliderTag::Player);
	}
	myPhysX.OnIntersection(other);
}

const PhysicsComponent & GrabbableObject::GetPhysx() const
{
	return myPhysX;
}

void GrabbableObject::SetPhysxKinematic(bool aValue)
{
	myPhysX.SetIsKinematic(aValue);
}

void GrabbableObject::SetVelocity(const CommonUtilities::Vector2f & aValue)
{
	myPhysX.SetVelocity(aValue);
}

void GrabbableObject::AddIgnoreTag(const ColliderTag & aTagToAdd)
{
	myPhysX.IgnoreTag(aTagToAdd);
}

void GrabbableObject::RemoveIgnoreTag(const ColliderTag & aTagToRemove)
{
	myPhysX.RemoveIgnoredTag(aTagToRemove);
}

void GrabbableObject::Reset(Player* aPlayerPointer)
{
	myPhysX.SetVelocity(CommonUtilities::Vector2f{ 0.0f, 0.0f });
	GameObject::Reset();
}

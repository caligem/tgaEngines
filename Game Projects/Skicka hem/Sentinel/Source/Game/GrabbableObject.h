#pragma once
#include "GameObject.h"
#include "PhysicsComponent.h"

class Animation;
class Sound;

class GrabbableObject : public GameObject
{
public:
	GrabbableObject();
	~GrabbableObject();

	void Init(const std::string& aPhysicsType);
	void OnUpdate() override;
	void ThrowRight(const float aForce);
	void ThrowLeft(const float aForce);
	void Release();
	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionStay(Collider  &other) override;
	void OnCollisionLeave(Collider  &other) override;

	const PhysicsComponent &GetPhysx() const;
	void SetPhysxKinematic(bool aValue);
	void SetVelocity(const CommonUtilities::Vector2f&aValue);
	void AddIgnoreTag(const ColliderTag &aTagToAdd);
	void RemoveIgnoreTag(const ColliderTag &aTagToRemove);

	void Reset(Player* aPlayerPointer = nullptr) override;
private:
	CommonUtilities::Vector2f myThrownDirection;
	float myThrownAmount;
	PhysicsComponent myPhysX;
	Collider *myLastHitMovingTile;

	bool myIsBeingDestroyed;
	Sprite *myOriginalSprite;
	Animation *myRespawnAnimation;
	Animation *myDestroyAnimation;

	Sound* myDestroyedSound;
};


#include "stdafx.h"
#include "GroundChecker.h"

GroundChecker::GroundChecker()
{
	myHit = nullptr;
}

GroundChecker::~GroundChecker()
{
}

void GroundChecker::OnUpdate()
{
	myDrawable->SetPosition({ myPosition.x, myPosition.y });
	myHit = nullptr;
	myColliding = false;
}
void GroundChecker::OnCollisionEnter(Collider  &other)
{
	if (other.GetTag() != ColliderTag::Player && other.GetIsSolid() == true)
	{
		myHit = &other;
		myColliding = true;
	}
}

void GroundChecker::OnCollisionStay(Collider  &other)
{
	if (other.GetTag() != ColliderTag::Player && other.GetTag() != ColliderTag::Climbable && other.GetIsSolid() == true)
	{
		myHit = &other;
		myColliding = true;
	}
}

void GroundChecker::OnCollisionLeave(Collider  &other)
{
	other;
}

bool GroundChecker::OnGround()
{
	return myColliding;
}

bool GroundChecker::IsColliding()
{
	return myCollider->IsColliding();;
}

Collider * GroundChecker::GetHit() const
{
	return myHit;
}


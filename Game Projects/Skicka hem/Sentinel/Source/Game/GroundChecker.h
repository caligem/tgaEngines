#pragma once
#include "GameObject.h"
class GroundChecker : public GameObject
{
public:
	GroundChecker();

	~GroundChecker();
	void OnUpdate() override;
	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionStay(Collider  &other) override;
	void OnCollisionLeave(Collider  &other) override;
	bool OnGround();
	bool IsColliding();
	Collider *GetHit() const;
private:
	bool myColliding;
	Collider *myHit;
};
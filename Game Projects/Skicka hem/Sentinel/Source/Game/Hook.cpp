#include "stdafx.h"
#include "Hook.h"
#include "Timer.h"
#include "tga2d\math\common_math.h"
#include "JsonWrapper.h"
#include "Camera.h"
#include "HookableTile.h"
#include "TileSize.h"
#include "Player.h"

#include "cit/utility.h"
#include "SoundEngine.h"

using namespace CommonUtilities;

Hook::Hook()
{
	CommonUtilities::Vector2f offset = { 0.0f, 0.0f };
	
	//CircleCollider actualCol;
	//actualCol.Init(0.01f, offset);
	//actualCol.SetTag(ColliderTag::Hook);
	//AttachCollider(actualCol);

	CircleCollider forgivingCol;
	forgivingCol.Init(0.01f, offset);
	forgivingCol.SetTag(ColliderTag::Hook);
	AttachCollider(forgivingCol);
}


Hook::~Hook()
{
	myHitSound->Annahilate();
	myShootSound->Annahilate();
}

void Hook::Init(GameObject * aPlayer, GameObject * aOwner)
{
	myPlayer = aPlayer;
	myOwner = aOwner;
	myState = HookState::AtPlayer;
	myTravelTime = 0.f;
	Sprite *hookSprite = new Sprite();
	GameObject::InitGO(myPlayer->GetPosition(), "sprites/noObj.dds", hookSprite);
	myRopeSprite.Init("sprites/hookRope.dds");
	myRopeSprite.SetLayer(1);
	myJustAnchored = true;


	json hookData = LoadHookData();
	myMaxTravelTime = hookData["maxTravelTime"];
	mySpeed = hookData["speed"];
	myPullSpeed = hookData["pullSpeed"];
	myDiagonalAngle = hookData["diagonalAngle"];
	myMaxLength = hookData["maxLength"];
	hookData.clear();
	int maxAmountOfRopes = 150;
	myRopeSprites.Init(maxAmountOfRopes);
	for (int i = 0; i < maxAmountOfRopes; ++i)
	{
		myRopeSprites.Add(new Sprite(myRopeSprite));
	}

	myHitSound = new Sound("Sounds/Sfx/hook_hit.wav");
	myShootSound = new Sound("Sounds/Sfx/hook_shoot.wav");
}

#include <tga2d\engine.h>
void Hook::Update()
{
	switch (myState)
	{
	case HookState::AtPlayer:
		myPosition = myPlayer->GetPosition();
		myTravelTime = 0;
		myAnchoredTime = 0;
		break;
	case HookState::Thrown:
		myDirection = myPosition;
		myPosition += mySpeed * Vector2f(cos(myRotation), sin(myRotation) * (Tga2D::CEngine::GetInstance()->GetWindowSize().x / Tga2D::CEngine::GetInstance()->GetWindowSize().y)) * Timer::GetDeltaTime();
		myDirection -= myPosition;
		myDirection *= -1.f;
		myDirection.Normalize();
		myTravelTime += Timer::GetDeltaTime();
		myJustAnchored = true;

		if (myTravelTime > myMaxTravelTime)
		{
			myState = HookState::ReelingIn;
			myTravelTime = 0;
		}
		break;
	case HookState::ReelingIn:
	{
		if ((myPosition - myPlayer->GetPosition()).Length() < 0.05f)
		{
			myState = HookState::AtPlayer;
		}
		Vector2f direction = (myPosition - myPlayer->GetPosition()).GetNormalized();
		myPosition -= 2.f * mySpeed * direction * Timer::GetDeltaTime();
		myRotation = atan2f(direction.y, direction.x);
	}
		break;
	case HookState::Anchored:
	{
		myAnchoredTime += Timer::GetDeltaTime();
		myJustAnchored = false;
	}
		break;
	case HookState::PullObject:
	{
		myDirection = (myPosition - myPlayer->GetPosition()).GetNormalized();
		myPosition -= myDirection * myPullSpeed * Timer::GetDeltaTime();
		myAnchor->SetPosition(myPosition);

		if ((myPosition - myPlayer->GetPosition()).Length() < 0.05f)
		{
			dynamic_cast<Player*>(myPlayer)->PickUp(dynamic_cast<GrabbableObject*>(myAnchor));
			myState = HookState::ReelingIn;
		}
		break;
	}
	case HookState::PullPlayer:
	{
		myDirection = (myPosition - myPlayer->GetPosition()).GetNormalized();

		CommonUtilities::Vector2f newPosition;
		if (myDirection.x > 0)
		{
			float test = (myPlayer->GetPosition().x + 0.015f) - (myAnchor->GetPosition().x - (((TILE_SIZE * TILE_MULTIPLIER) / 2.0f) / Tga2D::CEngine::GetInstance()->GetWindowSize().x));
			if (test >= -0.005f)
			{
				newPosition.x = myAnchor->GetPosition().x - (((TILE_SIZE * TILE_MULTIPLIER) / 2.0f) / Tga2D::CEngine::GetInstance()->GetWindowSize().x) - 0.015f;
				myState = HookState::HoldPlayerToWall;
			}
			else
			{
				newPosition.x = myPlayer->GetPosition().x + myPullSpeed * Timer::GetDeltaTime();
			}
			myPullDirection = PullPlayerDirection::Right;
		}
		else if (myDirection.x < 0)
		{
			float test = (myPlayer->GetPosition().x - 0.015f) - (myAnchor->GetPosition().x + (((TILE_SIZE * TILE_MULTIPLIER) / 2.0f) / Tga2D::CEngine::GetInstance()->GetWindowSize().x));
			if (test <= 0.005f)
			{
				newPosition.x = myAnchor->GetPosition().x + (((TILE_SIZE * TILE_MULTIPLIER) / 2.0f) / Tga2D::CEngine::GetInstance()->GetWindowSize().x) + 0.015f;
				myState = HookState::HoldPlayerToWall;
			}
			else
			{
				newPosition.x = myPlayer->GetPosition().x - myPullSpeed * Timer::GetDeltaTime();
			}
			myPullDirection = PullPlayerDirection::Left;
		}
		
		if (myPosition.y - 0.005f <= myPlayer->GetPosition().y)
		{
			newPosition.y = myPlayer->GetPosition().y - myPullSpeed * Timer::GetDeltaTime() * 1.78f;
		}
		else if(myPosition.y + 0.005f >= myPlayer->GetPosition().y)
		{
			newPosition.y = myPlayer->GetPosition().y + myPullSpeed * Timer::GetDeltaTime() * 1.78f;
		}
		else
		{
			newPosition.y = myPosition.y;
		}
		myPlayer->SetPosition(newPosition);
	}
		break;
	case HookState::HoldPlayerToWall:
	{
		CommonUtilities::Vector2f newPosition;
		if (myPullDirection == PullPlayerDirection::Right)
		{
			newPosition.x = myAnchor->GetPosition().x - (((TILE_SIZE * TILE_MULTIPLIER) / 2.0f) / Tga2D::CEngine::GetInstance()->GetWindowSize().x) - 0.03f;
		}
		else if (myPullDirection == PullPlayerDirection::Left)
		{
			newPosition.x = myAnchor->GetPosition().x + (((TILE_SIZE * TILE_MULTIPLIER) / 2.0f) / Tga2D::CEngine::GetInstance()->GetWindowSize().x) + 0.03f;
		}
		newPosition.y = myPosition.y;
		myPlayer->SetPosition(newPosition);
	}
	break;
	default:
		break;
	}
	GameObject::OnUpdate();
}

void Hook::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	if (myState != HookState::AtPlayer && myState != HookState::HoldPlayerToWall)
	{
		Vector2f offset = myPosition;
		if (myState == HookState::Anchored && myAnchor->GetCollider()->GetTag() == ColliderTag::HookableTile)
		{
			offset.y += (32.f) / 1080.f;
		}
		Vector2f ropeLength = myOwner->GetPosition() - offset;
		Vector2f ropeSpriteSize = Vector2f(static_cast<float>(myRopeSprite.GetImagesSizePixel().x), static_cast<float>(myRopeSprite.GetImagesSizePixel().y));
		int amountOfRopes = static_cast<int>(ropeLength.Length() / ((ropeSpriteSize / Vector2f{ static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().x), static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().y) }).Length() * 0.5f));
		amountOfRopes = Tga2D::Clamp(amountOfRopes, 0, 150);

		for (int i = 0; i < amountOfRopes; ++i)
		{
			Vector2f differenceVector = (static_cast<float>(i) * ropeLength) / static_cast<float>(amountOfRopes) + offset;
			differenceVector;

			myRopeSprites[i]->SetPosition(differenceVector);
			aRenderBuffer.Add(myRopeSprites[i]);
		}
	}
}

void Hook::Throw(const Vector2f & aDirection)
{
	if (myState == HookState::AtPlayer)
	{
		SoundEngine::PlaySound(*myShootSound);

		myRotation = atan2f(aDirection.y, aDirection.x);
		myState = HookState::Thrown;

		if (aDirection.x != 0 && aDirection.y != 0)
		{
			myRotation = atan2f(-1, 0) + aDirection.x * myDiagonalAngle;
		}
	}
}

void Hook::ReelIn()
{
	if (myState == HookState::Anchored)
	{
		myState = HookState::ReelingIn;
	}
}

const HookState & Hook::GetState() const
{
	return myState;
}

GameObject * Hook::GetAnchor()
{
	return myAnchor;
}

void Hook::Extend(const float aDifferenceInLength)
{
	myJustAnchored = true;
	myLength += aDifferenceInLength * Timer::GetDeltaTime();
	if (myLength < 0.1f)
		myLength = 0.1f;
	else if (myLength > myMaxLength)
	{
		myLength = myMaxLength;
	}
}

float Hook::GetLength()
{
	Vector2f direction = myPlayer->GetPosition() - myAnchor->GetPosition();
	direction.Normalize();
	direction.x *= 0.5625f * (cosf(atan2f(direction.y, direction.x)));
	direction *= myLength;
	return direction.Length();
}

float Hook::GetDiagonalAngle()
{
	return myDiagonalAngle;
}

float Hook::GetAnchoredTime()
{
	return myAnchoredTime;
}

void Hook::OnCollisionEnter(Collider & other)
{
	if (myState == HookState::Thrown)
	{

		SoundEngine::PlaySound(*myHitSound);

		if (other.GetTag() == ColliderTag::HookableTile)
		{
			HookableType type = static_cast<HookableTile*>(other.GetOwner())->GetHookableType();
			myAnchor = other.GetOwner();
			if (type == HookableType::Roof)
			{
 				myState = HookState::Anchored;
				myPosition = other.GetOwner()->GetPosition();
				myAnchor = other.GetOwner();
				Vector2f lengthVector = myPosition - myPlayer->GetPosition();
				myLength = lengthVector.Length();
			}
			else if (type == HookableType::LeftWall)
			{
				// REEL IN
				if (static_cast<Player*>(myPlayer)->GetLookDirection() == LookDirection::Left)
				{
					myState = HookState::PullPlayer;
					myPlayerStartPullPosition = myPlayer->GetPosition();
				}
				else
				{
					myState = HookState::ReelingIn;
				}
			}
			else if (type == HookableType::RightWall)
			{
				if (static_cast<Player*>(myPlayer)->GetLookDirection() == LookDirection::Right)
				{
					myState = HookState::PullPlayer;
					myPlayerStartPullPosition = myPlayer->GetPosition();
				}
				else
				{
					myState = HookState::ReelingIn;
				}
			}
		}
		else if (other.GetTag() == ColliderTag::Grabbable)
		{
			myState = HookState::PullObject;
			myPosition = other.GetOwner()->GetPosition();
			myAnchor = other.GetOwner();
		}
		else if (other.GetTag() == ColliderTag::Pullable)
		{
			myState = HookState::PullPlayer;
		}
		else if (other.GetTag() == ColliderTag::Tile)
		{
			myState = HookState::ReelingIn;
		}
	}
}

bool Hook::JustAnchored()
{
	return myJustAnchored;
}

void Hook::ReleaseHook()
{
	myState = HookState::AtPlayer;
}

const PullPlayerDirection Hook::GetPullDirection()
{
	return myPullDirection;
}

void Hook::SetDirection(const CommonUtilities::Vector2f & aDirection)
{
	if (myState == HookState::AtPlayer)
	{
		myRotation = atan2f(aDirection.y, aDirection.x);

		if (aDirection.x != 0 && aDirection.y != 0)
		{
			myRotation = atan2f(-1, 0) + aDirection.x * myDiagonalAngle * 0.7f;
		}
	}
}

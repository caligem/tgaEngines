#pragma once
#include "GameObject.h"
#include "BatchedSprite.h"
#include "GrowingArray.h"

class Sound;

enum class HookState
{
	AtPlayer,
	Thrown,
	Anchored,
	ReelingIn,
	PullObject,
	PullPlayer,
	HoldPlayerToWall
};

enum class PullPlayerDirection
{
	Right,
	Left
};

class Hook : public GameObject
{
public:
	Hook();
	~Hook();

	void Init(GameObject* aPlayer, GameObject* aOwner);
	void Update();
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;
	void Throw(const CommonUtilities::Vector2f& aDirection);
	void ReelIn();
	const HookState& GetState() const;
	GameObject* GetAnchor();
	void Extend(const float aDifferenceInLength);
	float GetLength();
	float GetDiagonalAngle();
	float GetAnchoredTime();
	void OnCollisionEnter(Collider  &other) override;
	bool JustAnchored();
	void ReleaseHook();
	const PullPlayerDirection GetPullDirection();
	void SetDirection(const CommonUtilities::Vector2f& aDirection);
private:
	HookState myState;
	PullPlayerDirection myPullDirection;
	GameObject* myOwner;
	GameObject* myAnchor;
	float mySpeed;
	float myTravelTime;
	float myMaxTravelTime;
	Sprite myRopeSprite;
	//BatchedSprite myRopeBatch;
	float myLength;
	float myAmplitude;
	float myAnchoredTime;
	float myPullSpeed;
	float myDiagonalAngle;
	float myMaxLength;
	CommonUtilities::Vector2f myDirection;
	CommonUtilities::Vector2f myPlayerStartPullPosition;
	bool myJustAnchored;
	CommonUtilities::GrowingArray<Sprite*> myRopeSprites;
	GameObject* myPlayer;

	Sound* myShootSound;
	Sound* myHitSound;
};

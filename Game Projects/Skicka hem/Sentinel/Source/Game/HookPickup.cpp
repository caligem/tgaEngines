#include "stdafx.h"
#include "HookPickup.h"
#include "Player.h"
#include "SoundEngine.h"

HookPickup::HookPickup()
{
	mySound = new Sound("Sounds/Sfx/hook_pickup.wav");
}


HookPickup::~HookPickup()
{
	mySound->Annahilate();
}

void HookPickup::OnCollisionEnter(Collider & other)
{
	if (other.GetTag() == ColliderTag::Player)
	{
		dynamic_cast<Player*>(other.GetOwner())->ActivateHook();
		myShouldRemove = true;
		SoundEngine::PlaySound(*mySound);
	}
}

void HookPickup::Reset(Player* aPlayerPointer)
{
	if (aPlayerPointer != nullptr && aPlayerPointer->HasHook())
	{
		return;
	}
	else
	{
		GameObject::Reset();
	}
}

#pragma once
#include "GameObject.h"
class Player;
class Sound;

class HookPickup :
	public GameObject
{
public:
	HookPickup();
	~HookPickup();

	void OnCollisionEnter(Collider  &other) override;
	void Reset(Player* aPlayerPointer) override;

private:
	Sound* mySound;
};


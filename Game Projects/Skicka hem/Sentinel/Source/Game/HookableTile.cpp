#include "stdafx.h"
#include "HookableTile.h"
#include "JsonWrapper.h"
#include <string>
HookableTile::HookableTile()
{
}

HookableTile::~HookableTile()
{
}

void HookableTile::Init(const char * aTypePath)
{
	json hookableTileData = LoadObjectType(aTypePath);
	std::string type = hookableTileData["type"].get<std::string>();

	if (type == "roof")
	{
		myType = HookableType::Roof;
	}
	else if (type == "leftWall")
	{
		myType = HookableType::LeftWall;
	}
	else if (type == "rightWall")
	{
		myType = HookableType::RightWall;
	}
}

void HookableTile::OnUpdate()
{
	myDrawable->SetPosition({ myPosition.x, myPosition.y });
}
void HookableTile::OnCollisionEnter(Collider  &other)
{
	other;
}

void HookableTile::OnCollisionStay(Collider  &other)
{
	other;
}

void HookableTile::OnCollisionLeave(Collider  &other)
{
	other;
}

const HookableType & HookableTile::GetHookableType() const
{
	return myType;
}



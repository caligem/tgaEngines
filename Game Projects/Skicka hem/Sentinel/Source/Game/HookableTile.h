#pragma once
#include "GameObject.h"
enum class HookableType
{
	Roof,
	LeftWall,
	RightWall
};

class HookableTile : public GameObject
{
public:
	HookableTile();

	~HookableTile();
	void Init(const char* aTypePath);
	void OnUpdate() override;
	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionStay(Collider  &other) override;
	void OnCollisionLeave(Collider  &other) override;
	const HookableType & GetHookableType() const;
private:
	HookableType myType;
};


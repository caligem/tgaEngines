#include "stdafx.h"
#include "InGameState.h"
#include "LevelScene.h"
#include "StateStack.h"
#include "InputWrapper.h"
#include "Fader.h"
#include "DialogueManager.h"
#include "PostMaster.h"

#include "TileSize.h"
#include "Collectible.h"
#include "MovingTile.h"

#define LastLevelIndex 3

InGameState::InGameState(CommonUtilities::InputManager* aInputManager, int aCurrentLevelIndex)
	: myCurrentLevelIndex(aCurrentLevelIndex)
	, myLoadingThread(&InGameState::LoadLevel, this)
	, myShouldPushWarpZone(false)
{
	myInputManager = aInputManager;
}


InGameState::~InGameState()
{
	PostMaster::UnSubscribe(MessageType::PushWarpZone, this);
	PostMaster::UnSubscribe(MessageType::PopWarpZone, this);
}

void InGameState::Init()
{
	PostMaster::Subscribe(MessageType::PushWarpZone, this);
	PostMaster::Subscribe(MessageType::PopWarpZone, this);
	myStateStack->PushSubState(new CutsceneState(myLoadingState, myCurrentLevelIndex, CutsceneState::eCutsceneType::Intro));
}

void InGameState::OnEnter()
{
	Init();
}

void InGameState::UpdateCollectibles(int aLevelIndex)
{
	switch (aLevelIndex)
	{
	case 1:
		if (myCurrentLevel->GetPlayerCollectibles() > myStateStack->myGameProgressData.myLevel1Collectables && myStateStack->myGameProgressData.myHasCompletedLevel1) 
		{
			myStateStack->myGameProgressData.myLevel1Collectables = myCurrentLevel->GetPlayerCollectibles();
		}
		break;
	case 2:
		if (myCurrentLevel->GetPlayerCollectibles() > myStateStack->myGameProgressData.myLevel2Collectables && myStateStack->myGameProgressData.myHasCompletedLevel2)
		{
			myStateStack->myGameProgressData.myLevel2Collectables = myCurrentLevel->GetPlayerCollectibles();
		}
		break;
	case 3:
		if (myCurrentLevel->GetPlayerCollectibles() > myStateStack->myGameProgressData.myLevel3Collectables && myStateStack->myGameProgressData.myHasCompletedLevel3)
		{
			myStateStack->myGameProgressData.myLevel3Collectables = myCurrentLevel->GetPlayerCollectibles();
		}
		break;
	default:
		break;
	}
}

void InGameState::OnExit()
{
	UpdateCollectibles(myCurrentLevelIndex);
	delete myCurrentLevel;
	myCurrentLevel = nullptr;
}

eStateStackMessage InGameState::Update()
{
	while (ShowCursor(false) >= 0);
	if (myLoadingState == eLoadingState::Waiting)
	{

		myCurrentLevel->Update();
		ShouldResetRoom();

		if (myCurrentLevel->ChangeLevel())
		{
			CompletedLevelData levelCompletedData;
			levelCompletedData.myCollectibles = myCurrentLevel->GetPlayerCollectibles();
			levelCompletedData.myLevelindex = myCurrentLevelIndex;

			if (myCurrentLevelIndex == 1)
			{
				myStateStack->myGameProgressData.myHasCompletedWarpZone1 = false;
				myStateStack->myGameProgressData.myLevel1Collectables = myCurrentLevel->GetPlayerCollectibles();
				levelCompletedData.myTotalCollectibles = myStateStack->myGameProgressData.myLevel1NumberOfCollectables;
				myStateStack->PushSubState(new LevelCompletedState(myInputManager, levelCompletedData));
				++myStateStack->myGameProgressData.myCurrentLevelIndex;
				myStateStack->myGameProgressData.myHasCompletedLevel1 = true;
				SaveGameProgress(myStateStack->myGameProgressData);

			}

			if (myCurrentLevelIndex == 2)
			{
				myStateStack->myGameProgressData.myHasCompletedWarpZone2 = false;
				myStateStack->myGameProgressData.myLevel2Collectables = myCurrentLevel->GetPlayerCollectibles();
				levelCompletedData.myTotalCollectibles = myStateStack->myGameProgressData.myLevel1NumberOfCollectables;
				myStateStack->PushSubState(new LevelCompletedState(myInputManager, levelCompletedData));
				++myStateStack->myGameProgressData.myCurrentLevelIndex;
				myStateStack->myGameProgressData.myHasCompletedLevel2 = true;
				SaveGameProgress(myStateStack->myGameProgressData);
			}
			else if (myCurrentLevelIndex == 3)
			{
				myStateStack->myGameProgressData.myHasCompletedWarpZone3 = false;
				myStateStack->myGameProgressData.myLevel3Collectables = myCurrentLevel->GetPlayerCollectibles();
				myStateStack->myGameProgressData.myHasCompletedLevel3 = true;

				SaveGameProgress(myStateStack->myGameProgressData);
				myStateStack->PushSubState(new CutsceneState(myLoadingState, 4, CutsceneState::eCutsceneType::Outro));
			}
		}

		if (InputWrapper::IsButtonPressed(KeyButton::Pause) && !DialogueManager::IsPlaying())
		{
			CurrentCollData data;
			data.myColl = myCurrentLevel->GetPlayerCollectibles();

			switch (myCurrentLevelIndex)
			{
			case 1:
				data.myTotalColl = myStateStack->myGameProgressData.myLevel1NumberOfCollectables;
				break;
			case 2:
				data.myTotalColl = myStateStack->myGameProgressData.myLevel2NumberOfCollectables;
				break;
			case 3:
				data.myTotalColl = myStateStack->myGameProgressData.myLevel3NumberOfCollectables;
				break;
			default:
				break;
			}

			myStateStack->PushSubState(new PauseMenuState(myInputManager, data));
		}

		if (myShouldPushWarpZone)
		{
			if (Fader::GetState() == FadeState::Visable)
			{
				myCurrentLevel->StopRendering();
				myStateStack->PushSubState(new WarpZoneState(myInputManager, myCurrentLevelIndex, myCurrentLevel->GetPlayer(), myCurrentLevel->GetCollectibleCounter()));
				myShouldPushWarpZone = false;
			}
		}
	}
	else if (myLoadingState == eLoadingState::FinishedLoading)
	{
		myLoadingThread.join();
		myLoadingState = eLoadingState::Waiting;
	}
	else if (myLoadingState == eLoadingState::Loading)
	{
		return eStateStackMessage::KeepState;
	}

	return eStateStackMessage::KeepState;
}

void InGameState::Render()
{
	if (myLoadingState == eLoadingState::Waiting)
	{
		myCurrentLevel->Render();
	}
	Fader::Render();
}

void InGameState::ReceiveMessage(const Message aMessage)
{
	if (aMessage.myMessageType == MessageType::PushWarpZone)
	{
		Fader::FadeIn(FadeColor::Black);
		myCurrentLevel->StopRendering();
		myStateStack->PushSubState(new WarpZoneState(myInputManager, myCurrentLevelIndex, myCurrentLevel->GetPlayer(), myCurrentLevel->GetCollectibleCounter()));
	}
	else if (aMessage.myMessageType == MessageType::PopWarpZone)
	{
		Fader::FadeOut(FadeColor::Black);
		myCurrentLevel->SetCameraTargetToPlayer();
	}
}

inline void InGameState::ShouldResetRoom()
{
	if (myStateStack->myGameProgressData.myShouldResetRoom)
	{
		myCurrentLevel->KillPlayerAndResetRoom();
		myStateStack->myGameProgressData.myShouldResetRoom = false;
	}
}

void InGameState::TerminateThread()
{
	if (myLoadingThread.joinable())
	{
		myLoadingThread.join();
	}
}

void InGameState::LoadLevel()
{
	myLoadingState = eLoadingState::Loading;

	std::string levelFileName = "level" + std::to_string(myCurrentLevelIndex);

	LevelScene* newScene = new LevelScene(myCurrentLevelIndex);
	newScene->Init(*myInputManager, levelFileName.c_str());
	myCurrentLevel = newScene;

	if (myCurrentLevelIndex == 1)
	{
		myCurrentLevel->SetNumberOfCollectibles(myStateStack->myGameProgressData.myLevel1NumberOfCollectables);
	}
	else if (myCurrentLevelIndex == 2)
	{
		myCurrentLevel->SetNumberOfCollectibles(myStateStack->myGameProgressData.myLevel2NumberOfCollectables);
	}
	else if (myCurrentLevelIndex == 3)
	{
		myCurrentLevel->SetNumberOfCollectibles(myStateStack->myGameProgressData.myLevel3NumberOfCollectables);
	}


	myLoadingState = eLoadingState::FinishedLoading;
}
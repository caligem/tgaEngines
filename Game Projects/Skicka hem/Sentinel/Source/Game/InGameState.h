#pragma once
#include "GameState.h"
#include "Subscriber.h"
#include "WarpZoneState.h"
#include <thread>

class LevelScene;

class InGameState :	public GameState, public Subscriber
{
public:
	InGameState(CommonUtilities::InputManager* aInputManager, int aCurrentLevelIndex);
	~InGameState();

	void Init() override;

	void OnEnter() override;
	void UpdateCollectibles(int aLevelIndex);
	void OnExit() override;

	eStateStackMessage Update() override;
	void Render() override;

	void ReceiveMessage(const Message aMessage) override;

	inline void ShouldResetRoom();
	
	void TerminateThread() override;

	enum class eLoadingState
	{
		Loading,
		Waiting,
		FinishedLoading
	};
private:
	bool myShouldPushWarpZone;

	int myCurrentLevelIndex;
	LevelScene* myCurrentLevel;
	std::thread myLoadingThread;

	eLoadingState myLoadingState;
	void LoadLevel();
};


#include "stdafx.h"
#include "InputWrapper.h"
#include <iostream>

//Change buttons here
#define USE_BUTTON KeyCode::C
#define C_USE_BUTTON GamePadButton::Y

#define BACK_BUTTON KeyCode::Escape
#define C_BACK_BUTTON GamePadButton::B

#define PAUSE_BUTTON KeyCode::Escape
#define C_PAUSE_BUTTON GamePadButton::START

#define LEFT_BUTTON KeyCode::Left 
#define C_LEFT_BUTTON GamePadButton::DPAD_LEFT

#define RIGHT_BUTTON KeyCode::Right
#define C_RIGHT_BUTTON GamePadButton::DPAD_RIGHT

#define UP_BUTTON KeyCode::Up
#define C_UP_BUTTON GamePadButton::DPAD_UP

#define DOWN_BUTTON KeyCode::Down
#define C_DOWN_BUTTON GamePadButton::DPAD_DOWN

#define HOOK_BUTTON KeyCode::X
#define C_HOOK_BUTTON GamePadButton::X
#define ALT_C_HOOK_BUTTON GamePadButton::RIGHT_SHOULDER

#define JUMP_BUTTON KeyCode::Spacebar
#define C_JUMP_BUTTON GamePadButton::A

#define JUMP_BUTTON KeyCode::Spacebar
#define C_JUMP_BUTTON GamePadButton::A

#define PICKUP_BUTTON KeyCode::Z
#define C_PICKUP_BUTTON GamePadButton::B

#define SELECT_BUTTON KeyCode::Spacebar
#define TEMP_ENTER KeyCode::Enter
#define C_SELECT_BUTTON GamePadButton::A


InputWrapper * InputWrapper::GetInstance()
{
	static InputWrapper inputWrapper;

	return &inputWrapper;
}

InputWrapper::InputWrapper()
{
}


InputWrapper::~InputWrapper()
{
}

void InputWrapper::Init(CommonUtilities::InputManager & aInputManager)
{
	GetInstance()->myInputManager = &aInputManager;
	GetInstance()->myInputType = InputType::Keyboard;
	GetInstance()->myDeadZone = 40.0f;
	GetInstance()->myResetZone = GetInstance()->myDeadZone / 2.0f;
}

bool InputWrapper::IsButtonPressed(KeyButton aButton)
{
	bool isClicked = false;
	GetInstance()->myLeftStick = GetInstance()->myInputManager->GetXBOXLeftThumb(0);

	//Firstly a check for controller sticks 
	switch (aButton)
	{
	case KeyButton::Left:
		if (GetInstance()->myLeftStick.x < -GetInstance()->myDeadZone && GetInstance()->myCanReturnStick)
		{
			isClicked = true;
			GetInstance()->myCanReturnStick = false;
		}
		break;
	case KeyButton::Right:
		if ((GetInstance()->myLeftStick.x > GetInstance()->myDeadZone) && GetInstance()->myCanReturnStick)
		{
			isClicked = true;
			GetInstance()->myCanReturnStick = false;
		}
		break;
	case KeyButton::Up:
		if ((GetInstance()->myLeftStick.y > GetInstance()->myDeadZone) && GetInstance()->myCanReturnStick)
		{
			isClicked = true;
			GetInstance()->myCanReturnStick = false;
		}

		break;
	case KeyButton::Down:
		if (GetInstance()->myLeftStick.y < -GetInstance()->myDeadZone && GetInstance()->myCanReturnStick)
		{
			isClicked = true;
			GetInstance()->myCanReturnStick = false;
		}
		break;
	default:
		break;
	}

	if (GetInstance()->myLeftStick.x > -GetInstance()->myResetZone && GetInstance()->myLeftStick.x < GetInstance()->myResetZone &&
		GetInstance()->myLeftStick.y > -GetInstance()->myResetZone && GetInstance()->myLeftStick.y < GetInstance()->myResetZone)
	{
		GetInstance()->myCanReturnStick = true;
	}

	switch (aButton)
	{
	case KeyButton::Left:
		if (GetInstance()->CheckKeyboardPressed(LEFT_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckControllerPressed(C_LEFT_BUTTON)) { isClicked = true; }
		break;
	case KeyButton::Right:
		if (GetInstance()->CheckKeyboardPressed(RIGHT_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckControllerPressed(C_RIGHT_BUTTON)) { isClicked = true; }
		break;
	case KeyButton::Up:
		if (GetInstance()->CheckKeyboardPressed(UP_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckControllerPressed(C_UP_BUTTON)) { isClicked = true; }
		break;
	case KeyButton::Down:
		if (GetInstance()->CheckKeyboardPressed(DOWN_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckControllerPressed(C_DOWN_BUTTON)) { isClicked = true; }
		break;
	case KeyButton::Select:
		if (GetInstance()->CheckKeyboardPressed(SELECT_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckControllerPressed(C_SELECT_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckKeyboardPressed(TEMP_ENTER)) { isClicked = true; }
		break;
	case KeyButton::Jump:
		if (GetInstance()->CheckKeyboardPressed(JUMP_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckControllerPressed(C_JUMP_BUTTON)) { isClicked = true; }
		break;
	case KeyButton::Pickup:
		if (GetInstance()->CheckKeyboardPressed(PICKUP_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckControllerPressed(C_PICKUP_BUTTON)) { isClicked = true; }
		break;
	case KeyButton::Hook:
		if (GetInstance()->CheckKeyboardPressed(HOOK_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckControllerPressed(C_HOOK_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckControllerPressed(ALT_C_HOOK_BUTTON)) { isClicked = true; }
		break;
	case KeyButton::Pause:
		if (GetInstance()->CheckKeyboardPressed(PAUSE_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckControllerPressed(C_PAUSE_BUTTON)) { isClicked = true; }
		break;
	case KeyButton::Back:
		if (GetInstance()->CheckKeyboardPressed(BACK_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckControllerPressed(C_BACK_BUTTON)) { isClicked = true; }
		break;
	case KeyButton::Use:
		if (GetInstance()->CheckKeyboardPressed(USE_BUTTON)) { isClicked = true; }
		else if (GetInstance()->CheckControllerPressed(C_USE_BUTTON)) { isClicked = true; }
		break;
	default:
		break;
	}

	return isClicked;

}

InputType InputWrapper::GetInputType()
{
	return GetInstance()->myInputType;
}

bool InputWrapper::IsButtonDown(KeyButton aButton)
{
	GetInstance()->myLeftStick = GetInstance()->myInputManager->GetXBOXLeftThumb(0);

	switch (aButton)
	{
	case KeyButton::Left:
		if (GetInstance()->CheckKeyboardDown(LEFT_BUTTON)) { return true; }
		else if (GetInstance()->myLeftStick.x < -(GetInstance()->myDeadZone)) { return true; }
		else if (GetInstance()->CheckControllerDown(C_LEFT_BUTTON)) { return true; }
		break;
	case KeyButton::Right:
		if (GetInstance()->CheckKeyboardDown(RIGHT_BUTTON)) { return true; }
		else if (GetInstance()->myLeftStick.x > (GetInstance()->myDeadZone)) { return true; }
		else if (GetInstance()->CheckControllerDown(C_RIGHT_BUTTON)) { return true; }
		break;
	case KeyButton::Up:
		if (GetInstance()->CheckKeyboardDown(UP_BUTTON)) { return true; }
		else if (GetInstance()->myLeftStick.y > GetInstance()->myDeadZone) { return true; }
		else if (GetInstance()->CheckControllerDown(C_UP_BUTTON)) { return true; }
		break;
	case KeyButton::Down:
		if (GetInstance()->CheckKeyboardDown(DOWN_BUTTON)) { return true; }
		else if (GetInstance()->myLeftStick.y < -GetInstance()->myDeadZone) { return true; }
		else if (GetInstance()->CheckControllerDown(C_DOWN_BUTTON)) { return true; }
		break;
	case KeyButton::Select:
		if (GetInstance()->CheckKeyboardDown(SELECT_BUTTON)) { return true; }
		else if (GetInstance()->CheckControllerDown(C_SELECT_BUTTON)) { return true; }
		break;
	case KeyButton::Jump:
		if (GetInstance()->CheckKeyboardDown(JUMP_BUTTON)) { return true; }
		else if (GetInstance()->CheckControllerDown(C_JUMP_BUTTON)) { return true; }
		break;
	case KeyButton::Pickup:
		if (GetInstance()->CheckKeyboardDown(PICKUP_BUTTON)) { return true; }
		else if (GetInstance()->CheckControllerDown(C_PICKUP_BUTTON)) { return true; }
		break;
	case KeyButton::Hook:
		if (GetInstance()->CheckKeyboardDown(HOOK_BUTTON)) { return true; }
		else if (GetInstance()->CheckControllerDown(C_HOOK_BUTTON)) { return true; }
		else if (GetInstance()->CheckControllerDown(ALT_C_HOOK_BUTTON)) { return true; }
		break;
	case KeyButton::Pause:
		if (GetInstance()->CheckKeyboardDown(PAUSE_BUTTON)) { return true; }
		else if (GetInstance()->CheckControllerDown(C_PAUSE_BUTTON)) { return true; }
		break;
	case KeyButton::Back:
		if (GetInstance()->CheckKeyboardDown(BACK_BUTTON)) { return true; }
		else if (GetInstance()->CheckControllerDown(C_BACK_BUTTON)) { return true; }
		break;
	case KeyButton::Use:
		if (GetInstance()->CheckKeyboardDown(USE_BUTTON)) { return true; }
		else if (GetInstance()->CheckControllerDown(C_USE_BUTTON)) { return true; }
		break;
	default:
		break;
	}

	return false;

}

bool InputWrapper::CheckKeyboardPressed(KeyCode aKey)
{
	if (GetInstance()->myInputManager->IsKeyPressed(aKey))
	{
		GetInstance()->myInputType = InputType::Keyboard;
		return true; 
	}

	return false;
}

bool InputWrapper::CheckKeyboardDown(KeyCode aKey)
{
	if (GetInstance()->myInputManager->IsKeyDown(aKey))
	{
		GetInstance()->myInputType = InputType::Keyboard;
		return true;
	}

	return false;
}

bool InputWrapper::CheckControllerPressed(GamePadButton aButton)
{
	if(GetInstance()->myInputManager->IsXBOXButtonPressed(0, aButton))
	{
		GetInstance()->myInputType = InputType::Controller;
		return true; 
	}

	return false;
}

bool InputWrapper::CheckControllerDown(GamePadButton aButton)
{
	if (GetInstance()->myInputManager->IsXBOXButtonDown(0, aButton))
	{
		GetInstance()->myInputType = InputType::Controller;
		return true;
	}

	return false;
}


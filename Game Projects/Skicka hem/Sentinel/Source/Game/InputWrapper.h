#pragma once
#include "InputManager.h"
#include "Vector2.h"



enum class InputType 
{
	Keyboard,
	Controller
};

enum class KeyButton
{
	Left,
	Right,
	Up,
	Down,
	Select,
	Jump,
	Pickup,
	Hook,
	Pause,
	Back,
	Use
};

using namespace CommonUtilities;

class InputWrapper
{
public:
	~InputWrapper();
	
	static void Init(CommonUtilities::InputManager& aInputManager);
	static bool IsButtonPressed(KeyButton aButton);

	static InputType GetInputType();
	static bool IsButtonDown(KeyButton aButton);

private:

	InputManager* myInputManager;
	static InputWrapper *GetInstance();

	InputWrapper();

	static bool CheckKeyboardPressed(KeyCode aKey);
	static bool CheckKeyboardDown(KeyCode aKey);

	static bool CheckControllerPressed(GamePadButton aButton);
	static bool CheckControllerDown(GamePadButton aButton);

	InputType myInputType;
	float myDeadZone;
	float myResetZone;
	bool myCanReturnStick;
	Vector2f myLeftStick;

};


#include "stdafx.h"
#include "Ladder.h"


Ladder::Ladder()
{
}


Ladder::~Ladder()
{
}

void Ladder::SetSegmentType(const LadderSegment & aSegmentType)
{
	myLadderSegment = aSegmentType;
}

const LadderSegment & Ladder::GetSegmentType() const
{
	return myLadderSegment;
}

#pragma once
#include "Tile.h"

enum class LadderSegment
{
	Top,
	Middle,
	Bottom
};

class Ladder :	public Tile
{
public:
	Ladder();
	~Ladder();

	void SetSegmentType(const LadderSegment& aSegmentType);
	const LadderSegment& GetSegmentType() const;

private:

	LadderSegment myLadderSegment;

};


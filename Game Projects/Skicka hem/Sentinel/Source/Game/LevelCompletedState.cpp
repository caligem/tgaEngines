#include "stdafx.h"
#include "LevelCompletedState.h"
#include "Sprite.h"
#include "InputWrapper.h"
#include "LevelScene.h"
#include "Fader.h"
#include "SoundEngine.h"

LevelCompletedState::LevelCompletedState(CommonUtilities::InputManager* aInputManager, CompletedLevelData aLevelCompletedData)
{
	myMusic = nullptr;
	myInputManager = aInputManager;
	myWhereToGo = eWhereToGo::Stay;
	myLevelCompletedData = aLevelCompletedData;
}


LevelCompletedState::~LevelCompletedState()
{
}

void LevelCompletedState::Init()
{
	myRenderBuffer.Init(10);
	InitButtons();
	ConnectButtons();
	InitDrawbables();

	
	myCollectibleCounter = new CollectibleCounter();
	myCollectibleCounter->Init();
	myCollectibleCounter->UpdateCounter(myLevelCompletedData.myCollectibles, myLevelCompletedData.myTotalCollectibles);
	myCollectibleCounter->SetPosition({ 0.5f, 0.3f });
	Fader::FadeOut(FadeColor::Black);

 	myMusic = new Sound("Sounds/Music/menu_music.mp3");
 	myMusic->SetAsGenericMusic();
	myMusic->myFadeSpeed = 0.55f;

}

void LevelCompletedState::OnEnter()
{
	Init();
	SoundEngine::PlaySound(*myMusic);
}

void LevelCompletedState::OnExit()
{
	myMusic->Annahilate();
}

eStateStackMessage LevelCompletedState::Update()
{
	while (ShowCursor(true) <= 0);
	CleanUp();
	UpdateButtons();
	FillRenderBuffer();
	if (myWhereToGo == eWhereToGo::Continue)
	{
		myLevelCompletedData.myLevelindex = myLevelCompletedData.myLevelindex + 1;
		return eStateStackMessage::PopMainStateAndPushInGameState;
	}
	if (myWhereToGo == eWhereToGo::LevelSelect)
	{
		return eStateStackMessage::PopMainState;
	}
	if (myWhereToGo == eWhereToGo::MainMenu)
	{
		return eStateStackMessage::PopGameAndLevelSelect;
	}

	return eStateStackMessage::KeepState;
}

void LevelCompletedState::Render()
{
	for (unsigned short i = 0; i < myRenderBuffer.Size(); i++)
	{
		myRenderBuffer[i]->Render();
	}
	Fader::Render();
}

void LevelCompletedState::InitDrawbables()
{
	myBackground = new Sprite();
	myBackground->Init("Sprites/menu/menuBackground.dds");
	myBackground->SetPosition({ 0.5f, 0.5f });

	myLogo = new Sprite();
	if (myLevelCompletedData.myLevelindex == 1)
	{
		myLogo->Init("Sprites/menu/logoLevel1Complete.dds");
	}
	else if (myLevelCompletedData.myLevelindex == 2)
	{
		myLogo->Init("Sprites/menu/logoLevel2Complete.dds");
	}
	else
	{
		myLogo->Init("Sprites/menu/logoLevel3Complete.dds");
	}
	myLogo->SetPosition({ 0.5f, 0.1f });

	myArrow = new Animation();
	myArrow->Init("menuArrow_animated");
	if (myLevelCompletedData.myLevelindex < 3)
	{
		myArrow->SetPosition(myContinueButton.GetPosition() - CommonUtilities::Vector2f(0.15f, 0.f));
	}
}

void LevelCompletedState::InitButtons()
{
	myButtons.Init(8);
	if (myLevelCompletedData.myLevelindex < 3)
	{
		myContinueButton.Init([&] {myWhereToGo = eWhereToGo::Continue; }, "buttonContinue", CommonUtilities::Vector2f(0.5f, 0.6f), myInputManager);
		myContinueButton.SetHitboxOffsetX(0.08f);
		myContinueButton.SetHitboxOffsetY(0.03f);
		myButtons.Add(&myContinueButton);
		mySelectedButton = &myContinueButton;
	}
	else
	{
		mySelectedButton = &myLevelSelectButton;
	}

	myLevelSelectButton.Init([&] {myWhereToGo = eWhereToGo::LevelSelect; }, "buttonLevelSelect", CommonUtilities::Vector2f(0.5f, 0.7f), myInputManager);
	myLevelSelectButton.SetHitboxOffsetX(0.08f);
	myLevelSelectButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myLevelSelectButton);

	myExitToMenuButton.Init([&] {myWhereToGo = eWhereToGo::MainMenu; }, "buttonExitmenu", CommonUtilities::Vector2f(0.5f, 0.8f), myInputManager);
	myExitToMenuButton.SetHitboxOffsetX(0.08f);
	myExitToMenuButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myExitToMenuButton);


	ConnectButtons();
}

void LevelCompletedState::UpdateButtons()
{
	CheckMouseInput();
	CheckControllerInput();

	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		myButtons[i]->Update();
		myButtons[i]->SetCurrentFrame(myButtons[i]->GetDefaultFrame());
	}

	mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame() + 1);
}

void LevelCompletedState::ConnectButtons()
{
	myContinueButton.myConnectedButtons.myUpButton = &myExitToMenuButton;
	myContinueButton.ConnectButton(&myLevelSelectButton, Button::eConnectSide::Down);
	myLevelSelectButton.ConnectButton(&myExitToMenuButton, Button::eConnectSide::Down);
	myExitToMenuButton.myConnectedButtons.myDownButton = &myContinueButton;
}

void LevelCompletedState::CleanUp()
{
	myRenderBuffer.RemoveAll();
}

void LevelCompletedState::FillRenderBuffer()
{
	myRenderBuffer.Add(myBackground);
	myRenderBuffer.Add(myLogo);
	myRenderBuffer.Add(myArrow);
	
	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		myButtons[i]->FillRenderBuffer(myRenderBuffer);
	}
	myCollectibleCounter->FillRenderBuffer(myRenderBuffer);
}

void LevelCompletedState::CheckMouseInput()
{
	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		Button* currentButton = myButtons[i];

		currentButton->Update();

		if (myInputManager->GetCursorPositionOnScreenLastUpdate() != myInputManager->GetCursorPositionOnScreen())
		{
			if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
			{
				mySelectedButton = currentButton;
				currentButton->SetCurrentFrame(currentButton->GetDefaultFrame() + 1);
			}
			else
			{
				currentButton->SetCurrentFrame(currentButton->GetDefaultFrame());
			}
		}
	}

	if (myInputManager->IsMouseButtonClicked(CommonUtilities::MouseButton::Left))
	{
		for (unsigned short i = 0; i < myButtons.Size(); i++)
		{
			Button* currentButton = myButtons[i];
			if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
			{
				currentButton->OnClick();
			}
		}
	}

	myArrow->SetPosition(mySelectedButton->GetPosition() - CommonUtilities::Vector2f(0.14f, 0.f));
}

void LevelCompletedState::CheckControllerInput()
{
	if (InputWrapper::IsButtonPressed(KeyButton::Down))
	{
		if (mySelectedButton->myConnectedButtons.myDownButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myDownButton;
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Up))
	{
		if (mySelectedButton->myConnectedButtons.myUpButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myUpButton;
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Left))
	{
		if (mySelectedButton->myConnectedButtons.myLeftButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myLeftButton;
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Right))
	{
		if (mySelectedButton->myConnectedButtons.myRightButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myRightButton;
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Select))
	{
		mySelectedButton->OnClick();
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Back))
	{
		//myExitToMenuButton.OnClick();
	}
}

#pragma once
#include "GameState.h"

#include "CollectibleCounter.h"
class LevelScene;
class Sound;
struct CompletedLevelData
{
	int myLevelindex = 0;
	int myCollectibles = 0;
	int myTotalCollectibles = 0;
};

class LevelCompletedState : public GameState
{
public:
	LevelCompletedState(CommonUtilities::InputManager* aInputManager, CompletedLevelData aLevelCompletedData);
	~LevelCompletedState();

	void Init() override;
	void OnEnter() override;
	void OnExit() override;

	eStateStackMessage Update() override;
	void Render() override;

	inline const bool LetThroughRender() const override { return false; }

private:
	enum class eWhereToGo
	{
		Continue,
		LevelSelect,
		MainMenu,
		Stay
	} myWhereToGo;
	CompletedLevelData myLevelCompletedData;
	void InitDrawbables();
	void InitButtons();
	void UpdateButtons();
	void ConnectButtons();

	void CheckMouseInput();
	void CheckControllerInput();

	void CleanUp();
	void FillRenderBuffer();

	CommonUtilities::GrowingArray<Drawable*> myRenderBuffer;

	Drawable* myBackground;
	Drawable* myLogo;
	Drawable* myArrow;

	Button* mySelectedButton;
	Button myContinueButton;
	Button myLevelSelectButton;
	Button myExitToMenuButton;

	Sound* myMusic;

	CommonUtilities::GrowingArray<Button*> myButtons;

	CollectibleCounter* myCollectibleCounter;
};


#include "stdafx.h"
#include "LevelScene.h"

#include "InputManager.h"
#include "JsonWrapper.h"
#include "Door.h"
#include "Camera.h"
#include "tga2d\engine.h"
#include "TileSize.h"
#include "BatchManager.h"
#include "PostMaster.h"
#include "Fader.h"
#include "SoundEngine.h"
#include "CrewMember.h"

LevelScene::LevelScene(int aCurrentLevelIndex)
	: myRoomChangeObserver(this)
	, myBackground(aCurrentLevelIndex)
	, myCurrentRoomIndex(0)
{
	myChangeLevel = false;
	myShouldStartFadingToChangeLevel = false;
	myChangingRoom = false;
	myLevelIndex = aCurrentLevelIndex;
	switch (aCurrentLevelIndex)
	{
	case 1:
		SetMusic("Sounds/Music/level1_music.ogg");
		SetAmbience("Sounds/Ambience/level1_ambience.ogg");
		break;
	case 2:
		SetMusic("Sounds/Music/level2_music.ogg");
		SetAmbience("Sounds/Ambience/level2_ambience.ogg");
		break;
	case 3:
		SetMusic("Sounds/Music/level3_music.ogg");
		SetAmbience("Sounds/Ambience/level3_ambience.ogg");
		break;
	default:
		break;
	}

}

LevelScene::~LevelScene()
{
	BatchManager::EmptyBatches();
	myRenderBuffer.DeleteAll();
	myRooms.DeleteAll();

 	myCurrentMusic->Destroy();
	myCurrentAmbience->Annahilate();

	PostMaster::UnSubscribe(MessageType::PlayerDeath, this);
	PostMaster::UnSubscribe(MessageType::LevelCompleted, this);
	PostMaster::UnSubscribe(MessageType::ChangeLevel, this);
}

#include <tga2d\text\text.h>
void LevelScene::Init(CommonUtilities::InputManager &aInputManager, const char* aRootName)
{
	myOldRoomGameObjects.Init(256);
	myShouldReset = false;
	PostMaster::Subscribe(MessageType::PlayerDeath, this);
	PostMaster::Subscribe(MessageType::LevelCompleted, this);
	PostMaster::Subscribe(MessageType::ChangeLevel, this);
	//For level 1, dunnoo
	myCurrentLevelCollectibles = 2;

	myInputManager = &aInputManager;
	InitPlayer();

	myCollCounter.Init(0, 7, false);
	myCollCounter.SetPosition({ 0.1f, 0.1f });

	myRenderBuffer.Init(512);
	BatchManager::InitBatches();

	LoadRooms(aRootName);

	myCurrentRoom = &myRooms[0];

	myCurrentRoom->EnterRoom();
	Camera::GetInstance().SetTarget(&myPlayer);
	
	myBackground.Init(myMinWorldCoordinates, myMaxWorldCoordinates);

}


void LevelScene::PlayMusic() 
{
	SoundEngine::PlaySound(*myCurrentMusic);
	SoundEngine::PlaySound(*myCurrentAmbience);
}


void LevelScene::SetMusic(const char * aPath)
{
	myCurrentMusic = new Sound(aPath);
	myCurrentMusic->SetAsGenericMusic();
}

void LevelScene::SetAmbience(const char * aPath)
{
	myCurrentAmbience = new Sound(aPath);
	myCurrentAmbience->SetAsGenericMusic();
	myCurrentAmbience->myIsMusic = false;
}


void LevelScene::Update()
{
	if (!SoundEngine::CheckIfSoundIsPlaying(*myCurrentMusic))
	{
 		PlayMusic();
 	}

	if (!SoundEngine::CheckIfSoundIsPlaying(*myCurrentAmbience))
	{
		SoundEngine::PlaySound(*myCurrentAmbience);
	}

	CleanUp();

	myCurrentRoom->Update();
	Camera::GetInstance().Update(myInputManager);
	FillBufferAndCalculatePoints();	

	if (myShouldReset == true) 
	{
		if (Fader::GetState() == FadeState::Visable) 
		{
			ResetRoom();
			Camera::GetInstance().SetPosition(myPlayer.GetPosition());
			Fader::FadeOut(FadeColor::White);

			myShouldReset = false;
		}
	}
	if (myChangingRoom == true)
	{
			if (myLevelIndex != 2)
			{
				if (Fader::GetState() == FadeState::Visable)
				{
					Fader::FadeOut(FadeColor::Black, 1.75f);
					Camera::GetInstance().SetPosition(myPlayer.GetPosition());
					Camera::GetInstance().SetTarget(&myPlayer);
					myChangingRoom = false;
					myCurrentRoom->SetChnageRoom(false);
					myPlayer.SetPreventInput(false);
				}
			}
			else
			{
				myChangingRoom = false;
				myCurrentRoom->SetChnageRoom(false);
				myPlayer.SetPreventInput(false);
			}
			
	}

	myCollCounter.UpdateCounter(myPlayer.GetCurrentNumberOfCollectibles(), myCurrentLevelCollectibles);

	if (myPlayer.HasPickedUpCollectible())
	{
		myCollCounter.Appear();
	}
	if (myShouldStartFadingToChangeLevel == true && Fader::GetState() == FadeState::Visable)
	{
		SetChangeLevel(true);
	}
}

void LevelScene::Render()
{
	myBackground.Render();

	if (myRenderBuffer.Size() > 0)
	{
		std::sort(&myRenderBuffer[0], &myRenderBuffer[0] + myRenderBuffer.Size(), [](Drawable* a, Drawable* b)
		{
			return a->GetLayer() < b->GetLayer();
		});
	}

	
	myCurrentRoom->RenderBatchedSprites();

	for (unsigned short i = 0; i < myRenderBuffer.Size(); i++)
	{
		{
			myRenderBuffer[i]->Render();
		}
	}


	myCollCounter.Render();
}

void LevelScene::CleanUp()
{
	myCurrentRoom->CleanUpGameObjects();
	myRenderBuffer.RemoveAll();
}

void LevelScene::ChangeRoomToNext(const int aIndex)
{
	
	if(myLevelIndex != 2)
	{
		Fader::FadeIn(FadeColor::Black, 1.75f);	
		Camera::GetInstance().SetTarget(nullptr);
	}
	else
	{
		Fader::FadeOut(FadeColor::Transparent, 1.75f);
	}
	
	myChangingRoom = true;
	myCurrentRoom->SetChnageRoom(true);
	myPlayer.SetPreventInput(true);

	myOldRoomGameObjects.RemoveAll();
	myCurrentRoom->LeaveRoom(myOldRoomGameObjects);
	myCurrentRoom = &myRooms[aIndex - 1];


	myCurrentRoom->EnterRoom();
	++myCurrentRoomIndex;
}

inline void LevelScene::ResetRoom()
{
	myCurrentRoom->ResetRoom();
}

void LevelScene::KillPlayerAndResetRoom()
{
	myPlayer.Die();
}

void LevelScene::FillBufferAndCalculatePoints()
{
	if (Camera::GetInstance().GetPosition().x  - 0.48f < myCurrentRoom->GetMinEdges().x || Camera::GetInstance().GetPosition().y - 0.48f < myCurrentRoom->GetMinEdges().y
		|| Camera::GetInstance().GetPosition().x + 0.48f > myCurrentRoom->GetMinEdges().x || Camera::GetInstance().GetPosition().y + 0.48f > myCurrentRoom->GetMinEdges().y)
	{
		for (size_t i = myOldRoomGameObjects.Size(); i > 0; --i)
		{
			if (myOldRoomGameObjects[i-1]->GetPosition().x < Camera::GetInstance().GetPosition().x + 0.6f &&
				myOldRoomGameObjects[i-1]->GetPosition().x > Camera::GetInstance().GetPosition().x - 0.6f &&
				myOldRoomGameObjects[i-1]->GetPosition().y < Camera::GetInstance().GetPosition().y + 0.6f &&
				myOldRoomGameObjects[i-1]->GetPosition().y > Camera::GetInstance().GetPosition().y - 0.6f)
			{
				myOldRoomGameObjects[i-1]->AccessDrawable().SetShouldRender(true);
				
				if (myOldRoomGameObjects[i - 1]->GetCollider() != nullptr)
				{
					if (myOldRoomGameObjects[i - 1]->GetCollider()->GetTag() == ColliderTag::Door)
					{
						myOldRoomGameObjects[i - 1]->OnUpdate();
					}
				}
				
				myOldRoomGameObjects[i - 1]->FillRenderBuffer(myRenderBuffer);
			}
			else
			{
				myOldRoomGameObjects[i-1]->AccessDrawable().SetShouldRender(false);
				myOldRoomGameObjects.RemoveCyclicAtIndex(i-1);
			}
			
		}
	}
	else
	{
		myOldRoomGameObjects.RemoveAll();
	}
	myCurrentRoom->FillRenderBuffer(myRenderBuffer);
	
	Camera::GetInstance().ConvertRenderBufferToCameraSpace(myRenderBuffer);
}

void LevelScene::InitPlayer()
{
	myPlayer.InitGO({ 0.5f, 0.0f }, "sprites/player.dds", new Sprite());
	BoxCollider playerCol;
	CommonUtilities::Vector2f offset = { 0.0f, 0.0f };
	playerCol.Init( 0.03f, 0.115f, offset);
	playerCol.SetTag(ColliderTag::Player);
	myPlayer.AttachCollider(playerCol);
	if (myLevelIndex != 1)
	{
		myPlayer.SetHasHook(true);
	}

	myPlayer.SetInputManager(*myInputManager);
	myPlayer.SetData();
	myPlayer.SetLevelIndex(myLevelIndex);
}

void LevelScene::AllignRoomsWithOffsets()
{
	myRooms[0].SetRoomOffset(CommonUtilities::Vector2f({ 0.0f,0.0f }));
	for (unsigned short roomIndex = 0; roomIndex < myRooms.Size(); roomIndex++)
	{
		for (unsigned short doorIndex = 0; doorIndex < myRooms[roomIndex].GetDoors().Size(); doorIndex++)
		{
			int nextRoomIndex = myRooms[roomIndex].GetDoors()[doorIndex]->GetNextRoomIndex();
			if (nextRoomIndex < myRooms.Size() + 1)
			{
				if (nextRoomIndex != 0)
				{
					if (myRooms[nextRoomIndex - 1].GetRoomOffset().x == 0.f && myRooms[nextRoomIndex - 1].GetRoomOffset().y == 0.f)
				{
					CommonUtilities::Vector2f nextSpawnPos = myRooms[roomIndex].GetRoomOffset() + myRooms[roomIndex].GetDoors()[doorIndex]->GetNextRoomSpawnPoint();
					CommonUtilities::Vector2f nextRoomOffset = myRooms[roomIndex].GetDoors()[doorIndex]->GetDoorPosition() - nextSpawnPos;
				
					CommonUtilities::Vector2f minMaxEdgesOffset;

					float roomMinEdgeX = myRooms[roomIndex].GetMinEdges().x;
					float nextRoomMinEdgeX = nextRoomOffset.x + myRooms[nextRoomIndex - 1].GetMinEdges().x;
					
					float testXOffset = nextRoomOffset.x;
					float testYOffset = nextRoomOffset.y;
					if (nextRoomOffset.x < 0)
					{
						testXOffset *= -1;
					}
					if(nextRoomOffset.y < 0)
					{
						testYOffset *= -1;
					}

					bool xHasGreaterOffsetThanY = testXOffset > testYOffset;

					if (xHasGreaterOffsetThanY)
					{
						if (nextRoomOffset.x > 0.0f)
						{
							minMaxEdgesOffset.x = myRooms[roomIndex].GetMaxEdges().x - myRooms[roomIndex].GetDoors()[doorIndex]->GetDoorPosition().x + (TILE_SIZE * TILE_MULTIPLIER / 1920.0f);
							minMaxEdgesOffset.x += myRooms[roomIndex].GetDoors()[doorIndex]->GetNextRoomSpawnPoint().x - myRooms[nextRoomIndex - 1].GetMinEdges().x;
						}
						else
						{
							minMaxEdgesOffset.x = myRooms[roomIndex].GetDoors()[doorIndex]->GetDoorPosition().x - myRooms[roomIndex].GetMinEdges().x - (TILE_SIZE * TILE_MULTIPLIER / 1920.0f) ;
							minMaxEdgesOffset.x -= myRooms[nextRoomIndex - 1].GetMaxEdges().x - myRooms[roomIndex].GetDoors()[doorIndex]->GetNextRoomSpawnPoint().x;
						}
						if (roomMinEdgeX < nextRoomMinEdgeX)
						{
							minMaxEdgesOffset.y = 0.0f;
						}
						else
						{
							minMaxEdgesOffset.y = 0.0f;
						}
					}
					else
					{
						float roomMinEdgeY = myRooms[roomIndex].GetMinEdges().y;
						float nextRoomMinEdgeY = nextRoomOffset.y + myRooms[nextRoomIndex - 1].GetMinEdges().y;
						if (nextRoomOffset.y > 0.0f)
						{
							minMaxEdgesOffset.y = myRooms[roomIndex].GetMaxEdges().y - myRooms[roomIndex].GetDoors()[doorIndex]->GetDoorPosition().y + ((TILE_SIZE * 1.5f) * TILE_MULTIPLIER / 1080.0f);
							minMaxEdgesOffset.y += myRooms[roomIndex].GetDoors()[doorIndex]->GetNextRoomSpawnPoint().y - myRooms[nextRoomIndex - 1].GetMinEdges().y;
						}
						else
						{
							minMaxEdgesOffset.y = myRooms[roomIndex].GetDoors()[doorIndex]->GetDoorPosition().y - myRooms[roomIndex].GetMinEdges().y - ((TILE_SIZE * 1.5f) * TILE_MULTIPLIER / 1080.0f);
							minMaxEdgesOffset.y -= myRooms[nextRoomIndex - 1].GetMaxEdges().y - myRooms[roomIndex].GetDoors()[doorIndex]->GetNextRoomSpawnPoint().y;
						}
						if (roomMinEdgeX < nextRoomMinEdgeX)
						{
							minMaxEdgesOffset.x = 0.0f;
						}
						else
						{
							minMaxEdgesOffset.x = 0.0f;
						}
					}

					nextRoomOffset += minMaxEdgesOffset;
					myRooms[nextRoomIndex - 1].SetRoomOffset(myRooms[roomIndex].GetRoomOffset() + nextRoomOffset);
				}
				}
			}
		}
	}
}

void LevelScene::CalculateWorldCoordinates()
{
	myMinWorldCoordinates = { 0.f, 0.f };
	myMaxWorldCoordinates = { 0.f, 0.f };

	for (unsigned short roomIndex = 0; roomIndex < myRooms.Size(); roomIndex++)
	{
		Room* currentRoom = &myRooms[roomIndex];

		if (currentRoom->GetMinEdges().x < myMinWorldCoordinates.x)
		{
			myMinWorldCoordinates.x = currentRoom->GetMinEdges().x;
		}
		if (currentRoom->GetMinEdges().y < myMinWorldCoordinates.y)
		{
			myMinWorldCoordinates.y = currentRoom->GetMinEdges().y;
		}
		if (currentRoom->GetMaxEdges().x > myMaxWorldCoordinates.x)
		{
			myMaxWorldCoordinates.x = currentRoom->GetMaxEdges().x;
		}
		if (currentRoom->GetMaxEdges().y > myMaxWorldCoordinates.y)
		{
			myMaxWorldCoordinates.y = currentRoom->GetMaxEdges().y;
		}
	}
}

void LevelScene::ReceiveMessage(const Message aMessage)
{
	if (aMessage.myMessageType == MessageType::PlayerDeath)
	{
		myShouldReset = true;
		myCollCounter.Appear();
	}
	else if (aMessage.myMessageType == MessageType::LevelCompleted)
	{
		Door* door;
		if (myLevelIndex == 3)
		{
			door = myCurrentRoom->GetDoors()[0];
		}
		else
		{
			door = myCurrentRoom->GetDoors().GetLast();
		}
		myPlayer.WalkOffLevelCompleted(myCurrentRoom->GetCrewMember()->GetDirection(), door->GetPosition());
		door->SetIsLocked(false);
	}
	else if (aMessage.myMessageType == MessageType::ChangeLevel && myShouldStartFadingToChangeLevel == false)
	{
		myShouldStartFadingToChangeLevel = true;
		Fader::FadeIn(FadeColor::Black);
	}
}

int LevelScene::GetPlayerCollectibles()
{
	return myPlayer.GetCurrentNumberOfCollectibles();
}

void LevelScene::StopRendering()
{
	myCurrentRoom->StopRendering();
	CleanUp();
}

void LevelScene::SetCameraTargetToPlayer()
{
	Camera::GetInstance().SetMaxEdges(myCurrentRoom->GetMaxEdges());
	Camera::GetInstance().SetMinEdges(myCurrentRoom->GetMinEdges());
	Camera::GetInstance().SetTarget(&myPlayer);
	Camera::GetInstance().SetPosition(myPlayer.GetPosition());
}

void LevelScene::SetNumberOfCollectibles(int aNumber)
{
	myCurrentLevelCollectibles = aNumber;
}

Player & LevelScene::GetPlayer()
{
	return myPlayer;
}

CollectibleCounter & LevelScene::GetCollectibleCounter()
{
	return myCollCounter;
}

void LevelScene::LoadRooms(const char * aRootName)
{
	json levelData = LoadLevel(aRootName);

	myRooms.Init(static_cast<unsigned short>(levelData["rooms"].size()));

	for (json::iterator roomIt = levelData["rooms"].begin(); roomIt != levelData["rooms"].end(); ++roomIt)
	{
		myRooms.Add(Room(&myRoomChangeObserver, myLevelIndex));
		myRooms.GetLast().Init(myPlayer, (levelData["folderSource"].get<std::string>() + "/" + roomIt.value().get<std::string>()).c_str());
	}

	AllignRoomsWithOffsets();

	CalculateWorldCoordinates();

	levelData.clear();
}

void LevelScene::CheckChangeRoomShortcut()
{
	//TODO: Remove Later when ship :PpPP

		if (GetCurrentRoom() != GetLastRoom())
		{
			//TODO: FIX SpawnPos;
			//CommonUtilities::Vector2f nextSpawnPos = myRooms[myCurrentRoomIndex].GetRoomOffset() + myRooms[myCurrentRoomIndex].GetDoors()[0]->GetNextRoomSpawnPoint();
			//myPlayer.SetRespawnPosition(nextSpawnPos);
			//myPlayer.SetPosition(nextSpawnPos);

			ChangeRoomToNext(myCurrentRoomIndex + 2);


		}
		else
		{
			SetChangeLevel(true);
		}
	
}

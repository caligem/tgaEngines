#pragma once
#include "GrowingArray.h"
#include "GrabbableObject.h"
#include "Player.h"
#include "Room.h"
#include "RoomChangeObserver.h"
#include "CollectibleCounter.h"
#include "Subscriber.h"
#include "ParallaxBackground.h"

class Sprite;

namespace Tga2D
{
	class CText;
}

namespace CommonUtilities
{
	class InputManager;
}

class Sound;

class LevelScene : public Subscriber
{
public:
	LevelScene(int aCurrentLevelIndex);
	~LevelScene();
	void Init(CommonUtilities::InputManager &aInputManager, const char* aRootName);
	void PlayMusic();
	void SetMusic(const char * aPath);
	void SetAmbience(const char * aPath);
	void Update();
	void Render();
	void CleanUp();

	inline const Room* GetCurrentRoom() const { return myCurrentRoom; }
	const Room* GetLastRoom() const { return &myRooms.GetLast(); }
	inline void ChangeRoomToNext(const int aIndex);
	inline void ResetRoom();
	void KillPlayerAndResetRoom();
	bool ChangeLevel() { return myChangeLevel; }
	void SetChangeRoom(bool aShouldChangeRoom) { myChangingRoom = aShouldChangeRoom; myCurrentRoom->SetChnageRoom(aShouldChangeRoom); }
	void SetChangeLevel(bool aShouldChangeScene) { myChangeLevel = aShouldChangeScene; myCurrentRoom->SetChangeLevel(aShouldChangeScene); }
	void ReceiveMessage(const Message aMessage) override;
	int GetPlayerCollectibles();

	void StopRendering();
	void SetCameraTargetToPlayer();

	void SetNumberOfCollectibles(int aNumber);

	Player &GetPlayer();
	CollectibleCounter &GetCollectibleCounter();
private:
	int myCurrentLevelCollectibles;

	void LoadRooms(const char* aPaths);

	void CheckChangeRoomShortcut();

	void FillBufferAndCalculatePoints();

	void InitPlayer();
	
	void AllignRoomsWithOffsets();

	void CalculateWorldCoordinates();
	CommonUtilities::Vector2f myMinWorldCoordinates;
	CommonUtilities::Vector2f myMaxWorldCoordinates;

	ParallaxBackground myBackground;

	Player myPlayer;
	CommonUtilities::InputManager *myInputManager;
	CommonUtilities::GrowingArray<Room> myRooms;
	Room *myCurrentRoom;
	int myCurrentRoomIndex;

	CommonUtilities::GrowingArray<GameObject*> myOldRoomGameObjects;

	CommonUtilities::GrowingArray<Drawable*> myRenderBuffer;
	bool myChangeLevel;
	bool myChangingRoom;
	bool myShouldReset;

	bool myShouldStartFadingToChangeLevel;

	RoomChangeObserver myRoomChangeObserver;
	CollectibleCounter myCollCounter;

	Tga2D::CText* myCurrentRoomText;

	Sound* myCurrentMusic;
	Sound* myCurrentAmbience;

	uint16_t myLevelIndex;
};


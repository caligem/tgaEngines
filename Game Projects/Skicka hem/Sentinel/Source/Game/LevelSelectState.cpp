#include "stdafx.h"
#include "LevelSelectState.h"
#include "InputManager.h"
#include "StateStack.h"
#include "Sprite.h"
#include "InputWrapper.h"
#include "SoundEngine.h"
#include <iostream>

LevelSelectState::LevelSelectState(CommonUtilities::InputManager* aInputManager)
{
	myInputManager = aInputManager;
	myShouldPop = false;
	myShowCollCount = false;
}


LevelSelectState::~LevelSelectState()
{
	mySelectedButton = nullptr;
	myHoveredSound->Destroy();
}

void LevelSelectState::Init()
{

	myRenderBuffer.Init(64);

	InitSprites();
	InitButtons();
	myHoveredSound = new Sound("Sounds/Sfx/button_hovored.wav");

}

void LevelSelectState::OnEnter()
{
	Init();
}

void LevelSelectState::OnExit()
{
	DeleteSprites();
}

eStateStackMessage LevelSelectState::Update()
{
	if (InputWrapper::IsButtonPressed(KeyButton::Left) || InputWrapper::IsButtonPressed(KeyButton::Up)
		|| InputWrapper::IsButtonPressed(KeyButton::Down) || InputWrapper::IsButtonPressed(KeyButton::Right)
		|| InputWrapper::IsButtonPressed(KeyButton::Back))
	{
		SoundEngine::PlaySound(*myHoveredSound);
	}

	while (ShowCursor(true) <= 0);
	CleanUp();
	UpdateButtons();
	FillRenderBuffer();
	CollectibleCounterUpdate();



	if (InputWrapper::IsButtonPressed(KeyButton::Pause))
	{
		return eStateStackMessage::PopSubState;
	}

	if (myShouldPop)
	{
		return eStateStackMessage::PopSubState;
	}

	return eStateStackMessage::KeepState;
}

void LevelSelectState::Render()
{
	for (unsigned short i = 0; i < myRenderBuffer.Size(); i++)
	{
		myRenderBuffer[i]->Render();
	}

	for (unsigned short i = 0; i < myCollCounters.Size(); i++)
	{
		myCollCounters[i]->Render();
	}
}

void LevelSelectState::CleanUp()
{
	myRenderBuffer.RemoveAll();
}

void LevelSelectState::FillRenderBuffer()
{
	myRenderBuffer.Add(myBackground);
	myRenderBuffer.Add(myLogo);

	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		myButtons[i]->FillRenderBuffer(myRenderBuffer);
	}
	
}

void LevelSelectState::InitSprites()
{
	myBackground = new Sprite();
	myBackground->Init("Sprites/menu/menuBackground.dds");
	myBackground->SetPosition({ 0.5f, 0.5f });

	myLogo = new Sprite();
	myLogo->Init("Sprites/menu/logoMain.dds");
	myLogo->SetPosition({ 0.5f, 0.2f });
}

void LevelSelectState::DeleteSprites()
{
	delete myBackground;
	myBackground = nullptr;

	delete myLogo;
	myLogo = nullptr;
}

void LevelSelectState::InitButtons()
{
	myCollCounters.Init(4);
	for (int i = 0; i < 3; i++)
	{
		CollectibleCounter* col;
		col = new CollectibleCounter();
		col->Init();
		myCollCounters.Add(col);
	}

	myButtons.Init(8);

	myBackButton.Init([&] {myShouldPop = true; }, "buttonBack", CommonUtilities::Vector2f(0.5f, 0.9f), myInputManager);
	myBackButton.SetHitboxOffsetX(0.08f);
	myBackButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myBackButton);


	mySelectLevel1Button.Init([&] {LoadLevel(1); }, "selectLevel1", CommonUtilities::Vector2f(0.19f, 0.55f), myInputManager);
	mySelectLevel1Button.SetHitboxOffsetX(0.12f);
	mySelectLevel1Button.SetHitboxOffsetY(0.05f);
	myButtons.Add(&mySelectLevel1Button);


	if (!myStateStack->myGameProgressData.myHasCompletedLevel1)
	{
		mySelectLevel2Button.Init([&] {LoadLevel(2); }, "selectLevel2", CommonUtilities::Vector2f(0.497f, 0.55f), myInputManager, false);
		mySelectLevel2Button.SetDefaultFrame(2);
	}
	else
	{
		mySelectLevel2Button.Init([&] {LoadLevel(2); }, "selectLevel2", CommonUtilities::Vector2f(0.497f, 0.55f), myInputManager);

	}
	mySelectLevel2Button.SetHitboxOffsetX(0.12f);
	mySelectLevel2Button.SetHitboxOffsetY(0.05f);
	myButtons.Add(&mySelectLevel2Button);



	if (!myStateStack->myGameProgressData.myHasCompletedLevel2)
	{
		mySelectLevel3Button.Init([&] {LoadLevel(3); }, "selectLevel3", CommonUtilities::Vector2f(0.8f, 0.55f), myInputManager, false);
		mySelectLevel3Button.SetDefaultFrame(2);
	}
	else
	{
		mySelectLevel3Button.Init([&] {LoadLevel(3); }, "selectLevel3", CommonUtilities::Vector2f(0.8f, 0.55f), myInputManager);
	}
	mySelectLevel3Button.SetHitboxOffsetX(0.12f);
	mySelectLevel3Button.SetHitboxOffsetY(0.05f);
	myButtons.Add(&mySelectLevel3Button);


	mySelectedButton = &mySelectLevel1Button;
	ConnectButtons();
}

void LevelSelectState::UpdateButtons()
{

	CheckMouseInput();
	CheckControllerInput();

	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		myButtons[i]->Update();
		myButtons[i]->SetCurrentFrame(myButtons[i]->GetDefaultFrame());
	}

	mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame() + 1);

	if (myStateStack->myGameProgressData.myHasCompletedLevel1)
	{
		mySelectLevel2Button.SetClickable(true);
		mySelectLevel2Button.SetDefaultFrame(0);
	}
	if (myStateStack->myGameProgressData.myHasCompletedLevel2)
	{
		mySelectLevel3Button.SetClickable(true);
		mySelectLevel3Button.SetDefaultFrame(0);
	}
}

void LevelSelectState::ConnectButtons()
{
	mySelectLevel1Button.ConnectButton(&mySelectLevel2Button, Button::eConnectSide::Right);
	mySelectLevel2Button.ConnectButton(&mySelectLevel3Button, Button::eConnectSide::Right);
	mySelectLevel3Button.ConnectButton(&mySelectLevel1Button, Button::eConnectSide::Right);
	mySelectLevel1Button.ConnectButton(&myBackButton, Button::eConnectSide::Down);
	mySelectLevel2Button.myConnectedButtons.myDownButton = &myBackButton;
	mySelectLevel3Button.myConnectedButtons.myDownButton = &myBackButton;
	mySelectLevel1Button.myConnectedButtons.myUpButton = &myBackButton;
	mySelectLevel2Button.myConnectedButtons.myUpButton = &myBackButton;
	mySelectLevel3Button.myConnectedButtons.myUpButton = &myBackButton;
}

void LevelSelectState::CheckMouseInput()
{
	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		Button* currentButton = myButtons[i];

		currentButton->Update();
		if (myInputManager->GetCursorPositionOnScreenLastUpdate() != myInputManager->GetCursorPositionOnScreen())
		{
			if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
			{
				mySelectedButton = currentButton;
				currentButton->SetCurrentFrame(currentButton->GetDefaultFrame() + 1);
			}
			else
			{
				currentButton->SetCurrentFrame(currentButton->GetDefaultFrame());
			}
		}
	}

	if (myInputManager->IsMouseButtonClicked(CommonUtilities::MouseButton::Left))
	{
		for (unsigned short i = 0; i < myButtons.Size(); i++)
		{
			Button* currentButton = myButtons[i];
			if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
			{
				currentButton->OnClick();
			}
		}
	}
}

void LevelSelectState::CheckControllerInput()
{
	if (InputWrapper::IsButtonPressed(KeyButton::Down))
	{
		if (mySelectedButton->myConnectedButtons.myDownButton != nullptr && mySelectedButton->myConnectedButtons.myDownButton->GetIsClickable())
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myDownButton;
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Up))
	{
		if (mySelectedButton->myConnectedButtons.myUpButton != nullptr && mySelectedButton->myConnectedButtons.myUpButton->GetIsClickable())
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myUpButton;
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Left))
	{
		if (mySelectedButton->myConnectedButtons.myLeftButton != nullptr && mySelectedButton->myConnectedButtons.myLeftButton->GetIsClickable())
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myLeftButton;
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Right))
	{
		if (mySelectedButton->myConnectedButtons.myRightButton != nullptr && mySelectedButton->myConnectedButtons.myRightButton->GetIsClickable())
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myRightButton;
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Select))
	{
		mySelectedButton->OnClick();
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Back))
	{
		myBackButton.OnClick();
	}
}

void LevelSelectState::CollectibleCounterUpdate()
{
	for (int i = 1; i < myCollCounters.Size() + 1; i++)
	{
		myCollCounters[i - 1]->SetPosition({ myButtons[i]->GetPosition().x - 0.01f, myButtons[i]->GetPosition().y + 0.25f });

		if (i == 1)
		{
			myCollCounters[i - 1]->UpdateCounter(myStateStack->myGameProgressData.myLevel1Collectables, myStateStack->myGameProgressData.myLevel1NumberOfCollectables);
		}
		else if (i == 2)
		{
			myCollCounters[i - 1]->UpdateCounter(myStateStack->myGameProgressData.myLevel2Collectables, myStateStack->myGameProgressData.myLevel2NumberOfCollectables);
		}
		else if (i == 3)
		{
			myCollCounters[i - 1]->UpdateCounter(myStateStack->myGameProgressData.myLevel3Collectables, myStateStack->myGameProgressData.myLevel3NumberOfCollectables);
		}
	}
}

void LevelSelectState::LoadLevel(int aLevelIndex)
{
	myStateStack->myGameProgressData.myCurrentLevelIndex = aLevelIndex;
	myStateStack->PushMainState(new InGameState(myInputManager, myStateStack->myGameProgressData.myCurrentLevelIndex));
}
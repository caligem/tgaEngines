#pragma once
#include "GameState.h"
#include "CollectibleCounter.h"

class Sound;

class LevelSelectState : public GameState
{
public:
	LevelSelectState(CommonUtilities::InputManager* aInputManager);
	~LevelSelectState();

	void Init() override;
	void OnEnter() override;
	void OnExit() override;

	eStateStackMessage Update() override;
	void Render() override;

	inline const bool LetThroughRender() const override { return false; }

private:
	bool myShouldPop;
	bool myShowCollCount;

	void CleanUp();
	void FillRenderBuffer();
	void InitSprites();

	void DeleteSprites();

	void InitButtons();
	void UpdateButtons();
	void ConnectButtons();

	void CheckMouseInput();
	void CheckControllerInput();

	void CollectibleCounterUpdate();

	void LoadLevel(int aLevelIndex);

	Drawable* myBackground;
	Drawable* myLogo;
	CommonUtilities::GrowingArray<Drawable*> myRenderBuffer;

	Button* mySelectedButton;
	Button mySelectLevel1Button;
	Button mySelectLevel2Button;
	Button mySelectLevel3Button;
	Button myBackButton;
	CommonUtilities::GrowingArray<Button*> myButtons;
	CommonUtilities::GrowingArray<CollectibleCounter*> myCollCounters;

	Sound* myHoveredSound;
};


#include "stdafx.h"
#include "LineCollider.h"
#include "GameObject.h"

LineCollider::LineCollider()
{

}

LineCollider::LineCollider(CommonUtilities::Vector2f &aDirection, float aLength, CommonUtilities::Vector2f & aOffset)
{
	myDirection = aDirection;
	myLength = aLength;
	myOffset = aOffset;
}


LineCollider::~LineCollider()
{
	myGameObject = nullptr;
}

void LineCollider::Init(CommonUtilities::Vector2f &aDirection, float aLength, CommonUtilities::Vector2f & aOffset)
{
	myDirection = aDirection;
	myLength = aLength;
	myOffset = aOffset;
}
bool LineCollider::IsCollidedWith(Collider * aCol)
{
	if (this != aCol  && aCol->GetTag() != ColliderTag::None)
	{
		CommonUtilities::Vector2f colPos = { myGameObject->GetPosition().x + myOffset.x, myGameObject->GetPosition().y + myOffset.y };
		return aCol->IsCollidedWith(this, colPos);
	}
	return false;
}

bool LineCollider::IsCollidedWith(BoxCollider * aBoxCol, const CommonUtilities::Vector2f & aGOPos)
{
	return BoxWithLineCollision(CommonUtilities::Vector2f{ aGOPos.x, aGOPos.y }, aBoxCol->GetWidth(), aBoxCol->GetHeight(), (myGameObject->GetPosition() + myOffset), (myGameObject->GetPosition() + myOffset) + (myDirection *myLength));
}

bool LineCollider::IsCollidedWith(CircleCollider * aCircleCol, const CommonUtilities::Vector2f & aGOPos)
{
	return CircleWithLineCollision(CommonUtilities::Vector2f{ aGOPos.x + aCircleCol->GetOffset().x, aGOPos.y + aCircleCol->GetOffset().y }, aCircleCol->GetRadius(), (myGameObject->GetPosition() + myOffset), (myGameObject->GetPosition() + myOffset) + (myDirection *myLength));
}

bool LineCollider::IsCollidedWith(LineCollider * aLineCol, const CommonUtilities::Vector2f & aGOPos)
{
	return LineWithLineCollision(myGameObject->GetPosition() + myOffset, myGameObject->GetPosition() + myOffset + (myDirection*myLength), aGOPos, aGOPos + (aLineCol->GetDirection() * aLineCol->GetLength()));
}

#include "Camera.h"
void LineCollider::Draw()
{
	CommonUtilities::Vector2f pointPos = Camera::GetInstance().ConvertPositionToCameraSpace(myGameObject->GetPosition() + myOffset);
	CommonUtilities::Vector2f lineEnd = (myDirection *myLength) + pointPos;

	Tga2D::CLinePrimitive AB;
	AB.SetFrom(pointPos.x, pointPos.y);
	AB.SetTo(lineEnd.x, lineEnd.y);
	AB.myColor = { 0.0f, 1.0f, 0.0f, 1.0f };
	AB.Render();
}

CommonUtilities::Vector2f LineCollider::RelativePoint(const CommonUtilities::Vector2f & aPoint)
{
	return aPoint;
}

CommonUtilities::Vector2f & LineCollider::GetDirection() 
{
	return myDirection;
}

const float LineCollider::GetLength() const
{
	return myLength;
}

CommonUtilities::Vector2f & LineCollider::GetOffset()
{
	return myOffset;
}

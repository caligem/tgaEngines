#pragma once
#include "Collider.h"
#include "Vector.h"
class LineCollider : public Collider
{
public:
	LineCollider();
	LineCollider(CommonUtilities::Vector2f &aDirection, float aLength, CommonUtilities::Vector2f & aOffset);
	~LineCollider();
	void Init(CommonUtilities::Vector2f &aDirection, float aLength, CommonUtilities::Vector2f & aOffset);
	bool IsCollidedWith(Collider *aCol) override;
	bool IsCollidedWith(BoxCollider *aBoxCol, const CommonUtilities::Vector2f &aGOPos) override;
	bool IsCollidedWith(CircleCollider *aCircleCol, const CommonUtilities::Vector2f &aGOPos) override;
	bool IsCollidedWith(LineCollider *aLineCol, const CommonUtilities::Vector2f &aGOPos) override;
	void Draw() override;
	CommonUtilities::Vector2f RelativePoint(const CommonUtilities::Vector2f &aPoint) override;

	CommonUtilities::Vector2f & GetDirection();
	const float GetLength() const;
	CommonUtilities::Vector2f &GetOffset() override;

private:
	float myLength;
	CommonUtilities::Vector2f myDirection;
};


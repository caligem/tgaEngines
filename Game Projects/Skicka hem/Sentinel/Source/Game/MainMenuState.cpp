#include "stdafx.h"
#include "MainMenuState.h"
#include "InputManager.h"
#include "InputWrapper.h"
#include "StateStack.h"
#include "Sprite.h"
#include "TileSize.h"
#include "GameProgressData.h"
#include "SoundEngine.h"
#include "ParticleEmitter.h"
#include "ParticleEmitterManager.h"
#include "DanceGameState.h"


MainMenuState::MainMenuState(CommonUtilities::InputManager* aInputManager)
{
	myInputManager = aInputManager;
	myShouldPop = false;
	mySelectedButton = nullptr;
}


MainMenuState::~MainMenuState()
{
	mySelectedButton = nullptr;
	myMusic->Destroy();
	myHoveredSound->Annahilate();
}

void MainMenuState::Init()
{
	myRenderBuffer.Init(64);
	InitButtons();
	InitSprites();

	myMusic = new Sound("Sounds/Music/menu_music.mp3");
	myMusic->SetAsGenericMusic();
	myMusic->myFadeSpeed = 0.2f;

	myHoveredSound = new Sound("Sounds/Sfx/button_hovored.wav");


	myEmitter = ParticleEmitterManager::SpawnOwnedEmitterByIndex(1);
	mySecondEmitter = ParticleEmitterManager::SpawnOwnedEmitterByIndex(2);
}

void MainMenuState::OnEnter()
{
	Init();
}

void MainMenuState::OnExit()
{
	DeleteSprites();
}
eStateStackMessage MainMenuState::Update()
{
	if (InputWrapper::IsButtonPressed(KeyButton::Up) || InputWrapper::IsButtonPressed(KeyButton::Down))
	{
		SoundEngine::PlaySound(*myHoveredSound);
	}


	//Plays the song if it's not being played! :)
	if (!SoundEngine::CheckIfSoundIsPlaying(*myMusic))
	{
		SoundEngine::PlaySound(*myMusic);
	}

	while (ShowCursor(true) <= 0);
	myEmitter.SetPosition({ -0.2f, 0.1f });
	mySecondEmitter.SetPosition({ 1.2f, 0.1f });

	

	CleanUp();
	UpdateButtons();
	FillRenderBuffer();


	if (myInputManager->IsKeyPressed(CommonUtilities::KeyCode::Escape))
	{
		return eStateStackMessage::PopMainState;
	}
	if (myShouldPop)
	{
		return eStateStackMessage::PopMainState;
	}

	return eStateStackMessage::KeepState;
}

void MainMenuState::Render()
{
	for (unsigned short i = 0; i < myRenderBuffer.Size(); i++)
	{

		myRenderBuffer[i]->Render();
	}
}

void MainMenuState::NewGame()
{
	if (myStateStack->myGameProgressData.myHasCompletedLevel1 == true)
	{
		myStateStack->PushSubState(new PromptState(myInputManager));
	}
	else
	{
		myStateStack->myGameProgressData = CreateNewGame();
		myStateStack->PushSubState(new LevelSelectState(myInputManager));
	}
}

void MainMenuState::CleanUp()
{
	myRenderBuffer.RemoveAll();
}

void MainMenuState::FillRenderBuffer()
{
	myRenderBuffer.Add(myBackground);
	myRenderBuffer.Add(myLogo);
	myRenderBuffer.Add(myArrow);

	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		if (myButtons[i] != &myContinueButton)
		{
			myButtons[i]->FillRenderBuffer(myRenderBuffer);
		}
		else
		{
			if (myStateStack->myGameProgressData.myHasCompletedLevel1 == true)
			{
				myButtons[i]->FillRenderBuffer(myRenderBuffer);
			}
		}
	}

	myRenderBuffer.Add(&myEmitter);
	myRenderBuffer.Add(&mySecondEmitter);
}

void MainMenuState::InitSprites()
{
	myBackground = new Sprite();
	myBackground->Init("Sprites/menu/menuBackground.dds");
	myBackground->SetPosition({ 0.5f, 0.5f });

	myLogo = new Sprite();
	myLogo->Init("Sprites/menu/logoMain.dds");
	myLogo->SetPosition({ 0.5f, 0.2f });

	myArrow = new Animation();
	myArrow->Init("menuArrow_animated");
	myArrow->SetPosition(myStartButton.GetPosition() - CommonUtilities::Vector2f(0.125f, 0.f));
}

void MainMenuState::DeleteSprites()
{
	delete myLogo;
	myLogo = nullptr;

	delete myBackground;
	myBackground = nullptr;

	delete myArrow;
	myArrow = nullptr;
}

void MainMenuState::InitButtons()
{
	myButtons.Init(8);
	//myStateStack->myGameProgressData = CreateNewGame(); WE WILL USE THIS WHEN IMPLEMENTING START NEW GAME
	//myStateStack->myGameProgressData = LoadGameProgress(); WE WILL USE THIS WHEN IMPLEMENTING CONTINUE GAME
	
	
	myContinueButton.Init([&] {myStateStack->PushSubState(new LevelSelectState(myInputManager)); }, "buttonContinue", CommonUtilities::Vector2f(0.5f, 0.475f), myInputManager);
	myContinueButton.SetHitboxOffsetX(0.08f);
	myContinueButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myContinueButton);

	myStartButton.Init([&] { NewGame(); }, "buttonStart", CommonUtilities::Vector2f(0.5f, 0.55f), myInputManager);
	myStartButton.SetHitboxOffsetX(0.08f);
	myStartButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myStartButton);	
	
	myOptionsButton.Init([&] {myStateStack->PushSubState(new OptionState(myInputManager)); }, "buttonOptions", CommonUtilities::Vector2f(0.5f, 0.625f), myInputManager);
	myOptionsButton.SetHitboxOffsetX(0.06f);
	myOptionsButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myOptionsButton);

	myCreditsButton.Init([&] {myStateStack->PushSubState(new CreditsState(myInputManager)); }, "buttonCredits", CommonUtilities::Vector2f(0.5f, 0.7f), myInputManager);
	myCreditsButton.SetHitboxOffsetX(0.06f);
	myCreditsButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myCreditsButton);

	myExitButton.Init([&] {myShouldPop = true; }, "buttonExit", CommonUtilities::Vector2f(0.5f, 0.775f), myInputManager);
	myExitButton.SetHitboxOffsetX(0.045f);
	myExitButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myExitButton);

	if (myStateStack->myGameProgressData.myHasCompletedLevel1 == true)
	{
		mySelectedButton = &myContinueButton;
	}
	else
	{
		mySelectedButton = &myStartButton;
	}

	ConnectButtons();
}

void MainMenuState::UpdateButtons()
{
	CheckMouseInput();
	CheckControllerInput();

	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		if (myButtons[i] != &myContinueButton)
		{
			myButtons[i]->Update();
			myButtons[i]->SetCurrentFrame(myButtons[i]->GetDefaultFrame());
		}
		else
		{
			if (myStateStack->myGameProgressData.myHasCompletedLevel1 == true)
			{
				myButtons[i]->Update();
				myButtons[i]->SetCurrentFrame(myButtons[i]->GetDefaultFrame());
			}
		}
		
	}

	mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame() + 1);


	myArrow->SetPosition(mySelectedButton->GetPosition() - CommonUtilities::Vector2f(0.125f, 0.f));
}

void MainMenuState::ConnectButtons()
{
	myContinueButton.ConnectButton(&myStartButton, Button::eConnectSide::Down);
	myStartButton.ConnectButton(&myOptionsButton, Button::eConnectSide::Down);
	myOptionsButton.ConnectButton(&myCreditsButton, Button::eConnectSide::Down);
	myCreditsButton.ConnectButton(&myExitButton, Button::eConnectSide::Down);
	myExitButton.ConnectButton(&myContinueButton, Button::eConnectSide::Down);
}

void MainMenuState::CheckMouseInput()
{
		for (unsigned short i = 0; i < myButtons.Size(); i++)
		{
			Button* currentButton;
			if (myButtons[i] != &myContinueButton)
			{
				currentButton = myButtons[i];
			}
			else
			{
				if (myStateStack->myGameProgressData.myHasCompletedLevel1 == true)
				{
					currentButton = myButtons[i];
				}
				else
				{
					continue;
				}
			}

		

			currentButton->Update();

			if (myInputManager->GetCursorPositionOnScreenLastUpdate() != myInputManager->GetCursorPositionOnScreen())
			{
				if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
				{
					mySelectedButton = currentButton;
					currentButton->SetCurrentFrame(currentButton->GetDefaultFrame() + 1);
				}
				else
				{
					currentButton->SetCurrentFrame(currentButton->GetDefaultFrame());
				}
			}
		}

	if (myInputManager->IsMouseButtonClicked(CommonUtilities::MouseButton::Left))
	{
		for (unsigned short i = 0; i < myButtons.Size(); i++)
		{
			Button* currentButton;
			if (myButtons[i] != &myContinueButton)
			{
				currentButton = myButtons[i];
			}
			else
			{
				if (myStateStack->myGameProgressData.myHasCompletedLevel1 == true)
				{
					currentButton = myButtons[i];
				}
				else
				{
					continue;
				}
			}
			if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
			{
				currentButton->OnClick();
			}
		}
	}
}

void MainMenuState::CheckControllerInput()
{

	if (InputWrapper::IsButtonPressed(KeyButton::Down))
	{
		if (mySelectedButton->myConnectedButtons.myDownButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			if (mySelectedButton->myConnectedButtons.myDownButton != &myContinueButton)
			{
				mySelectedButton = mySelectedButton->myConnectedButtons.myDownButton;
			}
			else
			{
				if (myStateStack->myGameProgressData.myHasCompletedLevel1 == true)
				{
					mySelectedButton = mySelectedButton->myConnectedButtons.myDownButton;
				}
				else
				{
					mySelectedButton = mySelectedButton->myConnectedButtons.myDownButton->myConnectedButtons.myDownButton;
				}
			}
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Up))
	{
		if (mySelectedButton->myConnectedButtons.myUpButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			if (mySelectedButton->myConnectedButtons.myUpButton != &myContinueButton)
			{
				mySelectedButton = mySelectedButton->myConnectedButtons.myUpButton;
			}
			else
			{
				if (myStateStack->myGameProgressData.myHasCompletedLevel1 == true)
				{
					mySelectedButton = mySelectedButton->myConnectedButtons.myUpButton;
				}
				else
				{
					mySelectedButton = mySelectedButton->myConnectedButtons.myUpButton->myConnectedButtons.myUpButton;
				}
			}
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Select))
	{
		mySelectedButton->OnClick();
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Hook))
	{
		myExitButton.OnClick();
	}
}

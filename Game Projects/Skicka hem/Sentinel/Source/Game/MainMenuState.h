#pragma once
#include "GameState.h"
#include "ParticleEmitter.h"

class Sound;

class MainMenuState : public GameState
{
public:
	MainMenuState(CommonUtilities::InputManager* aInputManager);
	~MainMenuState();

	void Init() override;

	void OnEnter() override;
	void OnExit() override;

	eStateStackMessage Update() override;
	void Render() override;

private:
	bool myShouldPop;

	void NewGame();

	void CleanUp();
	void FillRenderBuffer();
	void InitSprites();

	void DeleteSprites();

	void InitButtons();
	void UpdateButtons();
	void ConnectButtons();

	void CheckMouseInput();
	void CheckControllerInput();

	Drawable* myBackground;
	Drawable* myLogo;
	Drawable* myArrow;
	CommonUtilities::GrowingArray<Drawable*> myRenderBuffer;

	Button* mySelectedButton;
	Button myContinueButton;
	Button myStartButton;
	Button myOptionsButton;
	Button myCreditsButton;
	Button myExitButton;

	Sound* myMusic;
	Sound* myHoveredSound;
	ParticleEmitter myEmitter;
	ParticleEmitter mySecondEmitter;

	CommonUtilities::GrowingArray<Button*> myButtons;
};


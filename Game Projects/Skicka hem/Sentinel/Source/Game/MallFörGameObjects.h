#pragma once
#include "GameObject.h"
class MallFörGameObjects : public GameObject
{
public:
	MallFörGameObjects();

	~MallFörGameObjects();
	void OnUpdate() override;
	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionStay(Collider  &other) override;
	void OnCollisionLeave(Collider  &other) override;
};


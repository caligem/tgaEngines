#include "stdafx.h"
#include "MovingTile.h"
#include "cit\utility.h"
#include "Timer.h"
#include "TileSize.h"
#include "TriggerManager.h"

MovingTile::MovingTile()
{
	myState = State::Idle;
	myTotalDistanceX = 0.1f;
	myTotalDistanceY = 0.1f;
	myIsConstant = true;
	myMoveSpeed = 0.3f;

	myDistanceLerpValue = 0.0f;
	myStartPos = myPosition;
	myReachedEndX = false;
	myReachedEndY = false;

	myForcedMove = false;
}

MovingTile::~MovingTile()
{
}

void MovingTile::Init(json& aJson, unsigned short aLayerIndex, unsigned short aObjectIndex)
{
	std::string &moveStartType = aJson["layers"][aLayerIndex]["objects"][aObjectIndex]["properties"]["startingDirection"].get<std::string>();
	float tileXDist = aJson["layers"][aLayerIndex]["objects"][aObjectIndex]["properties"]["tileXDistance"].get<float>();
	float tileYDist = aJson["layers"][aLayerIndex]["objects"][aObjectIndex]["properties"]["tileYDistance"].get<float>();
	bool isConstant = aJson["layers"][aLayerIndex]["objects"][aObjectIndex]["properties"]["isConstant"].get<bool>();
	float moveSpeed = aJson["layers"][aLayerIndex]["objects"][aObjectIndex]["properties"]["moveSpeed"].get<float>();
	MovingTile::State stateToSet;
	if (moveStartType == "horizontal")
	{
		stateToSet = MovingTile::State::MovingHorizontal;
	}
	else if (moveStartType == "vertical")
	{
		stateToSet = MovingTile::State::MovingVertical;
	}
	else
	{
		stateToSet = MovingTile::State::Idle;
	}

	SetState(stateToSet);
	myTotalDistanceX = (tileXDist * TILE_SIZE * TILE_MULTIPLIER) /1920.f;
	myTotalDistanceY = (tileYDist * TILE_SIZE * TILE_MULTIPLIER) / 1080.f;
  	myIsConstant = isConstant;
	myMoveSpeed = moveSpeed;
}

void MovingTile::OnUpdate()
{
	if (myIsConstant == true || myForcedMove == true)
	{
		myOldPosition = myPosition;
		if (myState == State::MovingHorizontal)
		{
			if (myDistanceLerpValue >= 1.0f)
			{
				myStartPos = myPosition;
				if (myIsConstant == true)
				{
					myState = State::MovingHorizontal;
				}
				else
				{
					myState = State::Idle;
				}
				myDistanceLerpValue = 0.0f;
				myReachedEndX = !myReachedEndX;
			}
			else
			{
				myDistanceLerpValue += cit::Clamp(myMoveSpeed * CommonUtilities::Timer::GetDeltaTime(), 0.0f, 1.0f);
				if (myReachedEndX == false)
				{
					myPosition.x = cit::Lerp(myStartPos.x, myStartPos.x + myTotalDistanceX, myDistanceLerpValue);
				}
				else
				{
					myPosition.x = cit::Lerp(myStartPos.x, myStartPos.x - myTotalDistanceX, myDistanceLerpValue);
				}
			}
		}
		else if (myState == State::MovingVertical)
		{
			if (myDistanceLerpValue >= 1.0f)
			{
				myStartPos = myPosition;
				if (myIsConstant == true)
				{
					myState = State::MovingVertical;
				}
				else
				{
					myState = State::Idle;
				}
				myDistanceLerpValue = 0.0f;
				myReachedEndY = !myReachedEndY;
			}
			else
			{
				myDistanceLerpValue += cit::Clamp(myMoveSpeed * CommonUtilities::Timer::GetDeltaTime(), 0.0f, 1.0f);
				if (myReachedEndY == false)
				{
					myPosition.y = cit::Lerp(myStartPos.y, myStartPos.y + myTotalDistanceY, myDistanceLerpValue);
				}
				else
				{
					myPosition.y = cit::Lerp(myStartPos.y, myStartPos.y - myTotalDistanceY, myDistanceLerpValue);
				}
			}
		}

		GameObject::OnUpdate();
	}
}
void MovingTile::OnCollisionEnter(Collider  &other)
{
	other;
}
void MovingTile::OnCollisionStay(Collider  &other)
{
	other;
}
void MovingTile::OnCollisionLeave(Collider  &other)
{
	other;
}

void MovingTile::SetState(State aStateToSet)
{
	if (myState == State::Idle)
	{
		myStartPos = myPosition;
		myState = aStateToSet;
	}
}

CommonUtilities::Vector2f MovingTile::GetMovementThisFrame() const
{
	return { myPosition.x - myOldPosition.x,myPosition.y - myOldPosition.y };
}

void MovingTile::OnTriggerd()
{
	myForcedMove = true;
}

void MovingTile::Deactivate()
{
	myForcedMove = false;
}

void MovingTile::AddToTriggerManager(int aIndex)
{
	TriggerManager::GetInstance()->InsertDoorAtIndex(aIndex, this);
}

void MovingTile::SetOriginalPosition()
{
	myOrginalPosition = myPosition;
	myStartPos = myPosition;
}
void MovingTile::Reset(Player* aPlayerPointer)
{
	GameObject::Reset();
	myStartPos = myOrginalPosition;
	myReachedEndX = false;
	myReachedEndY = false;
	myDistanceLerpValue = 0.0f;
	myForcedMove = false;
}

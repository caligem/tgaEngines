#pragma once
#include "GameObject.h"
#include "JsonWrapper.h"

class MovingTile : public GameObject
{
public:
	enum class State
	{
		MovingHorizontal,
		MovingVertical,
		Idle
	};
	MovingTile();

	~MovingTile()override;
	void Init(json& aJson, unsigned short aLayerIndex, unsigned short aObjectIndex);
	void OnUpdate() override;
	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionStay(Collider  &other) override;
	void OnCollisionLeave(Collider  &other) override;
	void SetState(State aStateToSet);
	CommonUtilities::Vector2f GetMovementThisFrame() const;
	virtual void OnTriggerd() override;
	void Deactivate();

	void AddToTriggerManager(int aIndex);

	void SetOriginalPosition() override;
	void Reset(Player* aPlayerPointer = nullptr) override;
private:
	CommonUtilities::Vector2f myOldPosition;
	CommonUtilities::Vector2f myStartPos;
	State myState;
	bool myIsConstant;
	bool myReachedEndX;
	bool myReachedEndY;
	float myMoveSpeed;
	float myTotalDistanceX;
	float myTotalDistanceY;
	float myDistanceLerpValue;

	bool myForcedMove;
};
#include "stdafx.h"
#include "NoteBaseButton.h"
#include "Timer.h"
#include "Sprite.h"
#include "ParticleEmitterManager.h"
#include "SoundEngine.h"


NoteBaseButton::NoteBaseButton()
{
}


NoteBaseButton::~NoteBaseButton()
{
	delete mySprite;
	mySprite = nullptr;
}

void NoteBaseButton::Init(const char * aPath, Vector2f aPosition, float aScale, KeyButton aKeyButton)
{
	myTimer = 0;
	myMaxTimer = 0.05f;
	myScale = aScale;
	myIsPressed = false;

	myPosition = aPosition;
	myKeyButton = aKeyButton;

	myEmitter = ParticleEmitterManager::SpawnEmitterByIndex(3, {0.5f, 0.5f});
	myEmitter.SetPosition({ myPosition.x, myPosition.y });

	mySprite = new Sprite();
	mySprite->Init(aPath);

	Vector2f pivot(0.5f, 0.5f);
	mySprite->SetPivot(pivot);
	mySprite->SetSizeRelativeToImage({ myScale, myScale });
	mySprite->SetPosition(myPosition);

	mySound = new Sound("Sounds/Sfx/note_hit.wav");
}

void NoteBaseButton::FillRenderBuffer(GrowingArray<Drawable*>& aRenderBuffer)
{
	aRenderBuffer.Add(mySprite);
	aRenderBuffer.Add(&myEmitter);
}

void NoteBaseButton::Update()
{
	UpdatePressed();
	UpdateBaseScales();
}

bool NoteBaseButton::IsPressed()
{
	return myIsPressed;
}

void NoteBaseButton::EmittParticles()
{
	myEmitter.SetEmitDuration(0.05f);
}
#include <iostream>

void NoteBaseButton::UpdatePressed()
{
	if (InputWrapper::IsButtonDown(myKeyButton) && myIsPressed == false)
	{
		myIsPressed = true; 
	}

	if (InputWrapper::IsButtonPressed(myKeyButton)) 
	{
		SoundEngine::PlaySound(*mySound);
	}

	if (myIsPressed == true)
	{
		myTimer += Timer::GetDeltaTime();

		if (myTimer > myMaxTimer)
		{
			myTimer = 0;
			myIsPressed = false;
		}
	}
}


void NoteBaseButton::UpdateBaseScales()
{
	Vector2f newScale = Vector2f(myScale * 1.2f, myScale * 1.2f);
	Vector2f originalScale = Vector2f(myScale, myScale);

	if (InputWrapper::IsButtonDown(myKeyButton))
	{
		mySprite->SetSizeRelativeToImage(newScale);
	}
	else
	{
		mySprite->SetSizeRelativeToImage(originalScale);
	}


}
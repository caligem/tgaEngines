#pragma once
#include "Vector.h"
#include "GrowingArray.h"
#include "InputWrapper.h"
#include "ParticleEmitter.h"

using namespace CommonUtilities;

class Sprite;
class Drawable;
class Sound;


class NoteBaseButton
{
public:
	NoteBaseButton();
	~NoteBaseButton();

	void Init(const char * aPath, Vector2f aPosition, float aScale, KeyButton aKeyButton);
	void FillRenderBuffer(GrowingArray<Drawable*>& aRenderBuffer);
	void Update();
	bool IsPressed();
	void EmittParticles();

private:
	void UpdatePressed();
	void UpdateBaseScales();

	ParticleEmitter myEmitter;

	Sprite* mySprite;
	Sound* mySound;

	Vector2f myPosition;
	KeyButton myKeyButton;

	float myTimer;
	float myMaxTimer;
	float myScale;

	
	bool myIsPressed;

};


#pragma once

enum class eMessageType
{
	ReachedDoor,
	ResetRoom
};

class Observer
{
public:
	Observer();
	virtual ~Observer();

	virtual void RecieveMessage(const eMessageType& aMessage, const int aIndex) = 0;
};


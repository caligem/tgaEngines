#include "stdafx.h"
#include "OptionState.h"
#include "InputManager.h"
#include "StateStack.h"
#include "Sprite.h"
#include "Text.h"
#include "cit/utility.h"
#include "tga2d\engine.h"
#include "SoundEngine.h"
#include "tga2d\math\common_math.h"
#include "InputWrapper.h"
#include "SoundEngine.h"

OptionState::OptionState(CommonUtilities::InputManager* aInputManager)
{
	myInputManager = aInputManager;
	myShouldPop = false;
}


OptionState::~OptionState()
{
	myHoveredSound->Destroy();
}

void OptionState::Init()
{
	LoadData();

	InitResolutionsArray();

	myRenderBuffer.Init(64);

	InitSprites();
	InitButtons();
	InitText();

	myHoveredSound = new Sound("Sounds/Sfx/button_hovored.wav");
}

void OptionState::OnEnter()
{
	Init();
}

void OptionState::OnExit()
{
	DeleteSprites();
	DeleteText();
}

eStateStackMessage OptionState::Update()
{
	if (InputWrapper::IsButtonPressed(KeyButton::Left) || InputWrapper::IsButtonPressed(KeyButton::Up)
		|| InputWrapper::IsButtonPressed(KeyButton::Down) || InputWrapper::IsButtonPressed(KeyButton::Right)
		|| InputWrapper::IsButtonPressed(KeyButton::Back))
	{
		SoundEngine::PlaySound(*myHoveredSound);
	}

	while (ShowCursor(true) <= 0);
	CleanUp();
	UpdateButtons();
	UpdateText();
	FillRenderBuffer();

	if (myInputManager->IsKeyPressed(CommonUtilities::KeyCode::Escape))
	{
		return eStateStackMessage::PopSubState;
	}
	if (myShouldPop)
	{
		return eStateStackMessage::PopSubState;
	}
	
	return eStateStackMessage::KeepState;
}

void OptionState::Render()
{
	for (unsigned short i = 0; i < myRenderBuffer.Size(); i++)
	{
		myRenderBuffer[i]->Render();
	}
}

void OptionState::LoadData()
{
	myVolume = myStateStack->myOptionsData.myVolume;
	myResolutionIterator = myStateStack->myOptionsData.myResolutionIterator;
	myIsFullscreen = myStateStack->myOptionsData.myIsFullscreen;
}

void OptionState::SaveData()
{
	myStateStack->myOptionsData.myVolume = myVolume;
	myStateStack->myOptionsData.myResolutionIterator = myResolutionIterator;
	myStateStack->myOptionsData.myIsFullscreen = myIsFullscreen;
}

void OptionState::ApplyChanges()
{
	SaveData();

	Tga2D::Vector2ui myResolution = { myResolutions[myResolutionIterator].x, myResolutions[myResolutionIterator].y };
	Tga2D::CEngine::GetInstance()->SetResolution(myResolution);
	float volume = static_cast<float>(myVolume / 100.f);
	SoundEngine::SetMasterVolume(volume);
	Tga2D::CEngine::GetInstance()->SetFullScreen(myIsFullscreen);
}

void OptionState::CleanUp()
{
	myRenderBuffer.RemoveAll();
}

#include "iostream"
void OptionState::FillRenderBuffer()
{
	myRenderBuffer.Add(myBackground);
	myRenderBuffer.Add(myLogo);
	myRenderBuffer.Add(myMasterVolumeSprite);
	myRenderBuffer.Add(myResolutionSprite);
	myRenderBuffer.Add(myFullscreenSprite);

	myRenderBuffer.Add(myMasterVolumeText);
	myRenderBuffer.Add(myResolutionText);

	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		myButtons[i]->FillRenderBuffer(myRenderBuffer);
	}
}

void OptionState::InitSprites()
{
	myBackground = new Sprite();

	myBackground->Init("Sprites/menu/menuBackground.dds");
	myBackground->SetPosition({ 0.5f, 0.5f });

	myLogo = new Sprite();
	myLogo->Init("Sprites/menu/logoOptions.dds");
	myLogo->SetPosition({ 0.5f, 0.1f });

	myMasterVolumeSprite = new Sprite();
	myMasterVolumeSprite->Init("Sprites/menu/optionsVolume.dds");
	myMasterVolumeSprite->SetPosition({ 0.3f, 0.3f });

	myResolutionSprite = new Sprite();
	myResolutionSprite->Init("Sprites/menu/optionsResolution.dds");
	myResolutionSprite->SetPosition({ 0.26f, 0.45f });

	myFullscreenSprite = new Sprite();
	myFullscreenSprite->Init("Sprites/menu/optionsFullscreen.dds");
	myFullscreenSprite->SetPosition({ 0.264f, 0.6f });
}

void OptionState::DeleteSprites()
{
	delete myLogo;
	myLogo = nullptr;

	delete myBackground;
	myBackground = nullptr;

	delete myMasterVolumeSprite;
	myMasterVolumeSprite = nullptr;

	delete myResolutionSprite;
	myResolutionSprite = nullptr;

	delete myFullscreenSprite;
	myFullscreenSprite = nullptr;
}

void OptionState::InitText()
{
	myMasterVolumeText = new Text();
	myMasterVolumeText->Init("Text/Mecha.ttf", Text::EFontSize_36);
	myMasterVolumeText->SetText(std::to_string(static_cast<int>(myVolume * 100.f)) + "%");
	myMasterVolumeText->SetPosition({ 0.7f, 0.31f });
	myMasterVolumeText->SetColor(CommonUtilities::Vector4f(1.f, 1.f, 1.f, 1.f));

	myResolutionText = new Text();
	myResolutionText->Init("Text/Mecha.ttf", Text::EFontSize_36);
	myResolutionText->SetText(std::to_string(myResolutions[myResolutionIterator].x) + " x " + std::to_string(myResolutions[myResolutionIterator].y));
	myResolutionText->SetPosition({ 0.695f, 0.46f });
	myResolutionText->SetColor(CommonUtilities::Vector4f(1.f, 1.f, 1.f, 1.f));
}

void OptionState::UpdateText()
{
	myMasterVolumeText->SetText(std::to_string(myVolume) + "%");
	myResolutionText->SetText(std::to_string(myResolutions[myResolutionIterator].x) + " x " + std::to_string(myResolutions[myResolutionIterator].y));
}

void OptionState::DeleteText()
{
	delete myMasterVolumeText;
	myMasterVolumeText = nullptr;

	delete myResolutionText;
	myResolutionText = nullptr;
}

void OptionState::InitButtons()
{
	myButtons.Init(8);

	myBackButton.Init([&] {myShouldPop = true; }, "buttonBack", CommonUtilities::Vector2f(0.4f, 0.9f), myInputManager);
	myBackButton.SetHitboxOffsetX(0.08f);
	myBackButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myBackButton);

	myApplyButton.Init([&] {ApplyChanges(); }, "buttonApply", CommonUtilities::Vector2f(0.6f, 0.9f), myInputManager);
	myApplyButton.SetHitboxOffsetX(0.08f);
	myApplyButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myApplyButton);

	myLeftMasterVolumeArrow.Init([&] {AdjustVolume(-10); }, "optionsArrows", CommonUtilities::Vector2f(0.6f, 0.3f), myInputManager);
	myButtons.Add(&myLeftMasterVolumeArrow);

	myRightMasterVolumeArrow.Init([&] {AdjustVolume(10); }, "optionsArrows", CommonUtilities::Vector2f(0.8f, 0.3f), myInputManager);
	myRightMasterVolumeArrow.FlipAnimation();
	myButtons.Add(&myRightMasterVolumeArrow);

	myLeftResolutionArrow.Init([&] {AdjustResolution(1); }, "optionsArrows", CommonUtilities::Vector2f(0.6f, 0.45f), myInputManager);
	myButtons.Add(&myLeftResolutionArrow);

	myRightResolutionArrow.Init([&] {AdjustResolution(-1); }, "optionsArrows", CommonUtilities::Vector2f(0.8f, 0.45f), myInputManager);
	myRightResolutionArrow.FlipAnimation();
	myButtons.Add(&myRightResolutionArrow);

	myFullscreenCheckbox.Init([&] {ToggleFullscreen(); }, "optionsCheckbox", CommonUtilities::Vector2f(0.7f, 0.6f), myInputManager);
	if (myIsFullscreen)
	{
		myFullscreenCheckbox.SetDefaultFrame(0);
	}
	else
	{
		myFullscreenCheckbox.SetDefaultFrame(3);
	}
	myButtons.Add(&myFullscreenCheckbox);

	mySelectedButton = &myLeftMasterVolumeArrow;

	ConnectButtons();
}

void OptionState::UpdateButtons()
{

	CheckMouseInput();
	CheckControllerInput();

	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		myButtons[i]->Update();
		myButtons[i]->SetCurrentFrame(myButtons[i]->GetDefaultFrame());
	}

	mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame() + 1);
}

void OptionState::ConnectButtons()
{
	myLeftMasterVolumeArrow.ConnectButton(&myRightMasterVolumeArrow, Button::eConnectSide::Right);
	myLeftMasterVolumeArrow.ConnectButton(&myLeftResolutionArrow, Button::eConnectSide::Down);
	myRightMasterVolumeArrow.ConnectButton(&myLeftMasterVolumeArrow, Button::eConnectSide::Right);
	myRightMasterVolumeArrow.myConnectedButtons.myUpButton = &myApplyButton;
	myRightMasterVolumeArrow.ConnectButton(&myRightResolutionArrow, Button::eConnectSide::Down);
	myLeftResolutionArrow.ConnectButton(&myRightResolutionArrow, Button::eConnectSide::Right);
	myRightResolutionArrow.ConnectButton(&myLeftResolutionArrow, Button::eConnectSide::Right);
	myLeftResolutionArrow.ConnectButton(&myFullscreenCheckbox, Button::eConnectSide::Down);
	myRightResolutionArrow.myConnectedButtons.myDownButton = &myFullscreenCheckbox;
	myFullscreenCheckbox.ConnectButton(&myApplyButton, Button::eConnectSide::Down);
	myBackButton.myConnectedButtons.myUpButton = &myFullscreenCheckbox;
	myBackButton.ConnectButton(&myLeftMasterVolumeArrow, Button::eConnectSide::Down);
	myBackButton.ConnectButton(&myApplyButton, Button::eConnectSide::Right);
	myApplyButton.ConnectButton(&myBackButton, Button::eConnectSide::Right);
	myApplyButton.myConnectedButtons.myDownButton = &myRightMasterVolumeArrow;
}

void OptionState::CheckMouseInput()
{
	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		Button* currentButton = myButtons[i];

		currentButton->Update();

		if (myInputManager->GetCursorPositionOnScreenLastUpdate() != myInputManager->GetCursorPositionOnScreen())
		{
			if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
			{
				mySelectedButton = currentButton;
				currentButton->SetCurrentFrame(currentButton->GetDefaultFrame() + 1);
			}
			else
			{
				currentButton->SetCurrentFrame(currentButton->GetDefaultFrame());
			}
		}
	}

	if (myInputManager->IsMouseButtonClicked(CommonUtilities::MouseButton::Left))
	{
		for (unsigned short i = 0; i < myButtons.Size(); i++)
		{
			Button* currentButton = myButtons[i];
			if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
			{
				currentButton->OnClick();
			}
		}
	}
}

void OptionState::CheckControllerInput()
{
	if (InputWrapper::IsButtonPressed(KeyButton::Down))
	{
		if (mySelectedButton->myConnectedButtons.myDownButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myDownButton;
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Up))
	{
		if (mySelectedButton->myConnectedButtons.myUpButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myUpButton;
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Left))
	{
		if (mySelectedButton->myConnectedButtons.myLeftButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myLeftButton;
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Right))
	{
		if (mySelectedButton->myConnectedButtons.myRightButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myRightButton;
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Select))
	{
		mySelectedButton->OnClick();
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Back))
	{
		myBackButton.OnClick();
	}
}

void OptionState::AdjustVolume(int aAdjustment)
{
	myVolume += aAdjustment;
	myVolume = cit::Clamp(myVolume, 0, 100);
}

void OptionState::ToggleFullscreen()
{
	if (myIsFullscreen)
	{
		myIsFullscreen = false;
		myFullscreenCheckbox.SetDefaultFrame(3);
	}
	else
	{
		myIsFullscreen = true;
		myFullscreenCheckbox.SetDefaultFrame(0);
	}
}

void OptionState::AdjustResolution(int aAdjustment)
{
	myResolutionIterator += aAdjustment;

	myResolutionIterator = cit::Clamp(myResolutionIterator, 0, 3);
}

void OptionState::InitResolutionsArray()
{
	myResolutions.Init(4);
	myResolutions.Add(CommonUtilities::Vector2<unsigned int>(1920, 1080));
	myResolutions.Add(CommonUtilities::Vector2<unsigned int>(1600, 900));
	myResolutions.Add(CommonUtilities::Vector2<unsigned int>(1280, 720));
	myResolutions.Add(CommonUtilities::Vector2<unsigned int>(1024, 576));
}
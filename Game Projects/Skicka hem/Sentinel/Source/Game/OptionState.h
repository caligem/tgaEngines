#pragma once
#include "GameState.h"
class Text;
class Sound;

class OptionState : public GameState
{
public:
	OptionState(CommonUtilities::InputManager* aInputManager);
	~OptionState();

	void Init() override;
	void OnEnter() override;
	void OnExit() override;

	eStateStackMessage Update() override;
	void Render() override;

	inline const bool LetThroughRender() const override { return false; }

private:
	bool myShouldPop;

	int myVolume;
	unsigned int myResolutionIterator;
	bool myIsFullscreen;
	
	void LoadData();
	void SaveData();
	void ApplyChanges();

	void CleanUp();
	void FillRenderBuffer();

	void InitSprites();
	void DeleteSprites();

	void InitText();
	void UpdateText();
	void DeleteText();

	void InitButtons();
	void UpdateButtons();
	void ConnectButtons();

	void CheckMouseInput();
	void CheckControllerInput();

	void AdjustVolume(int aAdjustment);
	void ToggleFullscreen();
	void AdjustResolution(int aAdjustment);

	void InitResolutionsArray();
	CommonUtilities::GrowingArray<CommonUtilities::Vector2<unsigned int>> myResolutions;

	Drawable* myBackground;
	Drawable* myLogo;
	Drawable* myMasterVolumeSprite;
	Drawable* myResolutionSprite;
	Drawable* myFullscreenSprite;

	Text* myMasterVolumeText;
	Text* myResolutionText;

	CommonUtilities::GrowingArray<Drawable*> myRenderBuffer;

	Button* mySelectedButton;
	Button myBackButton;
	Button myApplyButton;
	Button myLeftMasterVolumeArrow;
	Button myRightMasterVolumeArrow;
	Button myLeftResolutionArrow;
	Button myRightResolutionArrow;
	Button myFullscreenCheckbox;
	CommonUtilities::GrowingArray<Button*> myButtons;

	Sound* myHoveredSound;
};


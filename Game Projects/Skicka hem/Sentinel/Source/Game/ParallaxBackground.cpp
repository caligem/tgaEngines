#include "stdafx.h"
#include "ParallaxBackground.h"
#include "Sprite.h"
#include "Camera.h"
#include <algorithm>
#include <string>

ParallaxBackground::ParallaxBackground(int aCurrentLevelIndex)
	: myCurrentLevelIndex(aCurrentLevelIndex)
	, myIsWarpZone(false)
{
}


ParallaxBackground::~ParallaxBackground()
{
}

void ParallaxBackground::Init(CommonUtilities::Vector2f & aMinCoordinate, CommonUtilities::Vector2f & aMaxCoordinate, bool aIsWarpzone)
{
	myIsWarpZone = aIsWarpzone;
	myCenterPos.x = (aMinCoordinate.x + aMaxCoordinate.x) / 2;
	myCenterPos.y = (aMinCoordinate.y + aMaxCoordinate.y) / 2;

	InitSprites();
}

void ParallaxBackground::Render()
{
	for (unsigned short i = 0; i < myBackgrounds.size(); i++)
	{
		if (myIsWarpZone)
		{
			myBackgrounds[i]->SetPosition(CommonUtilities::Vector2f(0.5f, 1.f) - Camera::GetInstance().GetPosition() / myBackgrounds[i]->GetZ());
		}
		else
		{
			myBackgrounds[i]->SetPosition((CommonUtilities::Vector2f(0.5f, 0.5f) - Camera::GetInstance().GetPosition()) / myBackgrounds[i]->GetZ());
		}
		myBackgrounds[i]->Render();
	}
}

void ParallaxBackground::InitSprites()
{
	myBackgroundLayer1 = new Sprite();
	myBackgroundLayer2 = new Sprite();
	myBackgroundLayer3 = new Sprite();
	myBackgroundLayer4 = new Sprite();
	myBackgroundLayer5 = new Sprite();

	const char* spritePath = ("Sprites/level" + std::to_string(myCurrentLevelIndex) + "/backgroundLayer1.dds").c_str();

	myBackgroundLayer1->Init(("Sprites/level" + std::to_string(myCurrentLevelIndex) + "/backgroundLayer1.dds").c_str());
	myBackgroundLayer2->Init(("Sprites/level" + std::to_string(myCurrentLevelIndex) + "/backgroundLayer2.dds").c_str());
	myBackgroundLayer3->Init(("Sprites/level" + std::to_string(myCurrentLevelIndex) + "/backgroundLayer3.dds").c_str());
	myBackgroundLayer4->Init(("Sprites/level" + std::to_string(myCurrentLevelIndex) + "/backgroundLayer4.dds").c_str());
	myBackgroundLayer5->Init(("Sprites/level" + std::to_string(myCurrentLevelIndex) + "/backgroundLayer5.dds").c_str());

	myBackgrounds.push_back(myBackgroundLayer1);
	myBackgrounds.push_back(myBackgroundLayer2);
	myBackgrounds.push_back(myBackgroundLayer3);
	myBackgrounds.push_back(myBackgroundLayer4);
	myBackgrounds.push_back(myBackgroundLayer5);

	for (unsigned short i = 0; i < myBackgrounds.size(); i++)
	{
		if (myIsWarpZone)
		{
			myBackgrounds[i]->SetPivot(CommonUtilities::Vector2f(0.5f, 0.5f));
			myBackgrounds[i]->SetPosition(CommonUtilities::Vector2f(0.5f, 1.f));
		}
		else
		{
			myBackgrounds[i]->SetPivot(CommonUtilities::Vector2f(0.f, 0.f));
			myBackgrounds[i]->SetPosition(CommonUtilities::Vector2f(0.5f, 0.5f));
		}

	}

	myBackgroundLayer1->SetZ(8.f);
	myBackgroundLayer2->SetZ(7.f);
	myBackgroundLayer3->SetZ(6.f);
	myBackgroundLayer4->SetZ(5.f);
	myBackgroundLayer5->SetZ(4.f);
}

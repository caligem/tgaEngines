#pragma once

#include <Vector2.h>
#include <vector>
#include "GrowingArray.h"

class Sprite;
class Drawable;

class ParallaxBackground
{
public:
	ParallaxBackground(int aCurrentLevelIndex);
	~ParallaxBackground();

	void Init(CommonUtilities::Vector2f& aMinCoordinate, CommonUtilities::Vector2f& aMaxCoordinate, bool aIsWarpZone = false);
	void Render();
private:
	void InitSprites();
	int myCurrentLevelIndex;

	Sprite* myBackgroundLayer1;
	Sprite* myBackgroundLayer2;
	Sprite* myBackgroundLayer3;
	Sprite* myBackgroundLayer4;
	Sprite* myBackgroundLayer5;

	CommonUtilities::Vector2f myCenterPos;
	std::vector<Sprite*> myBackgrounds;

	bool myIsWarpZone;
};


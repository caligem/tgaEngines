#include "stdafx.h"
#include "ParticleEmitter.h"
#include "Timer.h"
#include <tga2d\engine.h>
#include "TileSize.h"
#include "ParticleEmitterManager.h"
ParticleEmitter::ParticleEmitter() : mySpriteBatch(true)
{
	myTimer = 0.0f;
	myEmitTime = 0.0f;
	myParticles.Init(1);
	myStartScale = 0.0f;
	myEndScale = 0.0f;
	myMinTimeBetweenParticleSpawns = 0.0f;
	myMaxTimeBetweenParticleSpawns = 0.0f;
	myMinStartSpeed = 0.0f;
	myMaxStartSpeed = 0.0f;
	myMinAngle = 0.0f;
	myMaxAngle = 0.0f;
	myMinLifeTime = 0.0f;
	myMaxLifeTime = 0.0f;
	EBlendState myBlendState = EBlendState_Alphablend;
	myEmitTime = 0.0f;
	myHasDuration = 0.0f;
	myMinRotation = 0.0f;
	myMaxRotation = 0.0f;
	myCurrParticleSpawnTime = 0.0f;
	myFinishedEmitting = false;
	mySource = "sprites/particle_pixel.dds";
}


ParticleEmitter::~ParticleEmitter()
{
	ClearEmitter();
}
void ParticleEmitter::Init(const char * aEmitterIndexToSpawn)
{
	*this = ParticleEmitterManager::SpawnOwnedEmitterByIndex(aEmitterIndexToSpawn[0] - '0');
}
ParticleEmitter::ParticleEmitter(const ParticleEmitter & aParticleEmitter) : mySpriteBatch(true)
{
	*this = aParticleEmitter;
}

ParticleEmitter &ParticleEmitter::operator=(const ParticleEmitter & aParticleEmitter)
{
	myStartScale = aParticleEmitter.myStartScale;
	myEndScale = aParticleEmitter.myEndScale;
	myMinTimeBetweenParticleSpawns = aParticleEmitter.myMinTimeBetweenParticleSpawns;
	myMaxTimeBetweenParticleSpawns = aParticleEmitter.myMaxTimeBetweenParticleSpawns;
	myCurrParticleSpawnTime = aParticleEmitter.myCurrParticleSpawnTime;
	myMinStartSpeed = aParticleEmitter.myMinStartSpeed;
	myMaxStartSpeed = aParticleEmitter.myMaxStartSpeed;
	myMinAngle = aParticleEmitter.myMinAngle;
	myMaxAngle = aParticleEmitter.myMaxAngle;
	myMinLifeTime = aParticleEmitter.myMinLifeTime;
	myMaxLifeTime = aParticleEmitter.myMaxLifeTime;
	myMinRotation = aParticleEmitter.myMinRotation;
	myMaxRotation = aParticleEmitter.myMaxRotation;
	myPosition = aParticleEmitter.myPosition;
	myAcceleration = aParticleEmitter.myAcceleration;
	myStartColor = aParticleEmitter.myStartColor;
	myMid1Color = aParticleEmitter.myMid1Color;
	myMid2Color = aParticleEmitter.myMid2Color;
	myEndColor = aParticleEmitter.myEndColor;
	myBlendState = aParticleEmitter.myBlendState;
	myEmitTime = aParticleEmitter.myEmitTime;
	myHasDuration = aParticleEmitter.myHasDuration;
	ClearEmitter();
	mySpriteBatch = Tga2D::CSpriteBatch(true);
	mySpriteBatch.Init(aParticleEmitter.mySource.c_str(), myBlendState);
	myParticles = aParticleEmitter.myParticles;
	for (unsigned int i = 0; i < myParticles.Size(); i++)
	{
		Tga2D::CSprite* sprite = new Tga2D::CSprite(nullptr);
		mySpriteBatch.AddObject(sprite);
	}
	myTimer = aParticleEmitter.myTimer;
	mySource = aParticleEmitter.mySource;
	myOriginalScreenSize = aParticleEmitter.myOriginalScreenSize;
	myFinishedEmitting = aParticleEmitter.myFinishedEmitting;
	return *this;
}
void ParticleEmitter::Update()
{
	myEmitTime -= CommonUtilities::Timer::GetDeltaTime();
	if (myTimer >= myCurrParticleSpawnTime)
	{
		if (myEmitTime > 0.0f || myHasDuration == false)
		{
			int particlesToSpawn = 1;
			float spawnTimePerIteration = myCurrParticleSpawnTime;
			while (spawnTimePerIteration < CommonUtilities::Timer::GetDeltaTime())
			{
				spawnTimePerIteration += myCurrParticleSpawnTime;
				particlesToSpawn++;
			}
			for (int i = 0; i < particlesToSpawn; i++)
			{
				if (mySpriteBatch.mySprites[0].size() >= 1000)
				{
					mySpriteBatch.RemoveObject(mySpriteBatch.mySprites[0][0], true);
				}
				Tga2D::Vector2f dir;
				float randAngle = RandomRange(myMinAngle, myMaxAngle);
				dir.x = cos(randAngle);
				dir.y = sin(randAngle) * Tga2D::CEngine::GetInstance()->GetWindowRatio();
				float randSpeed = RandomRange(myMinStartSpeed, myMaxStartSpeed);
				float randLife = RandomRange(myMinLifeTime, myMaxLifeTime);
				float randRotation = RandomRange(myMinRotation, myMaxRotation);
				myParticles.Add(Particle(randLife, Tga2D::Vector2f(myPosition.x, myPosition.y), dir*randSpeed, randRotation));
				Tga2D::CSprite* sprite = new Tga2D::CSprite(nullptr);
				mySpriteBatch.AddObject(sprite);
			}
			myTimer = 0.0f;
			myCurrParticleSpawnTime = RandomRange(myMinTimeBetweenParticleSpawns, myMaxTimeBetweenParticleSpawns);
		}
	}
	else
	{
		myTimer += CommonUtilities::Timer::GetDeltaTime();
	}

	int diff = 0;
	int sizeOfBatch = mySpriteBatch.mySprites[0].size();
	if (sizeOfBatch > 0)
	{
		for (short i = sizeOfBatch - 1; i >= 0; i--)
		{
			myParticles[diff + i].myVelocity += myAcceleration * CommonUtilities::Timer::GetDeltaTime();
			myParticles[diff + i].myPosition += myParticles[diff + i].myVelocity* CommonUtilities::Timer::GetDeltaTime();
			myParticles[diff + i].myTime += CommonUtilities::Timer::GetDeltaTime();
			float t = myParticles[diff + i].myTime / myParticles[diff + i].myLifeTime;
			if (myParticles[diff + i].myTime >= myParticles[diff + i].myLifeTime)
			{
				myParticles.RemoveCyclicAtIndex(diff + i);
				Tga2D::CSprite *last = mySpriteBatch.mySprites[0][mySpriteBatch.mySprites[0].size() - 1];
				mySpriteBatch.mySprites[0][diff + i]->SetPosition(last->GetPosition());
				mySpriteBatch.mySprites[0][diff + i]->SetColor(last->GetColor());
				mySpriteBatch.mySprites[0][diff + i]->SetSizeRelativeToScreen(last->GetSize());
				mySpriteBatch.mySprites[0][diff + i]->SetPivot(last->GetPivot());
				mySpriteBatch.mySprites[0][diff + i]->SetRotation(last->GetRotation());
				mySpriteBatch.RemoveObject(last, true);
				continue;
			}
			{
				mySpriteBatch.mySprites[0][i]->SetPosition(myParticles[diff + i].myPosition + Tga2D::Vector2f{ myPosition.x, myPosition.y });
				float size = Lerp(myStartScale, myEndScale, t);
				float rotation = Lerp(0.0f, myParticles[diff + i].myRotation, t);
				if (t <= 0.33f)
				{
					mySpriteBatch.mySprites[0][i]->SetColor(Lerp(myStartColor, myMid1Color, t*3.0f));
				}
				else if (t <= 0.66f)
				{
					mySpriteBatch.mySprites[0][i]->SetColor(Lerp(myMid1Color, myMid2Color, (t - 0.33f)*3.0f));
				}
				else
				{
					mySpriteBatch.mySprites[0][i]->SetColor(Lerp(myMid2Color, myEndColor, (t - 0.66f)*3.0f));
				}
				mySpriteBatch.mySprites[0][i]->SetSizeRelativeToScreen({ myOriginalScreenSize.x * size,myOriginalScreenSize.y * size });
				mySpriteBatch.mySprites[0][i]->SetPivot({ 0.5f, 0.5f });
				mySpriteBatch.mySprites[0][i]->SetRotation(rotation);
			}
		}
	}
	if (myHasDuration == true && myEmitTime <= 0.0f && myParticles.Size() <= 0)
	{
		myFinishedEmitting = true;
	}
}

void ParticleEmitter::Render()
{
	Update();
	mySpriteBatch.Render();
}

void ParticleEmitter::SetSpriteSource(const std::string & aSpriteSource, int aBlendMode)
{
	myParticles.ReInit(1024);
	mySpriteBatch.DeleteAll();
	myBlendState = static_cast<EBlendState>(aBlendMode);
	mySpriteBatch = Tga2D::CSpriteBatch(true);
	mySpriteBatch.Init(aSpriteSource.c_str(), myBlendState);
	mySource = aSpriteSource;
	myOriginalScreenSize.x = (mySpriteBatch.GetImageSize().x * TILE_MULTIPLIER) / 1920.f;
	myOriginalScreenSize.y = (mySpriteBatch.GetImageSize().y * TILE_MULTIPLIER) / 1920.f;
}

void ParticleEmitter::SetScale(float aStartScale, float aEndScale)
{
	myStartScale = aStartScale;
	myEndScale = aEndScale;
}

void ParticleEmitter::SetTimeBetweenSpawn(float aMinTime, float aMaxTime) ///milliseconds
{
	myMinTimeBetweenParticleSpawns = aMinTime;
	myMaxTimeBetweenParticleSpawns = aMaxTime;
	if (myMinTimeBetweenParticleSpawns < 0.0001f)
	{
		myMinTimeBetweenParticleSpawns = 0.0001f;
	}
	if (myMaxTimeBetweenParticleSpawns < 0.0001f)
	{
		myMaxTimeBetweenParticleSpawns = 0.0001f;
	}
	myCurrParticleSpawnTime = myMinTimeBetweenParticleSpawns;
}

void ParticleEmitter::SetStartSpeed(float aMinSpeed, float aMaxSpeed)
{
	//previewer uses screenspace, reverse
	myMinStartSpeed = aMinSpeed / 30.0f;
	myMaxStartSpeed = aMaxSpeed / 30.0f;
}

void ParticleEmitter::SetAngle(float aMinAngle, float aMaxAngle) ///degrees
{
	myMinAngle = aMinAngle* (3.1415f / 180.0f);
	myMaxAngle = aMaxAngle* (3.1415f / 180.0f);
}

void ParticleEmitter::SetAngleRadians(float aMinAngle, float aMaxAngle)
{
	myMinAngle = aMinAngle;
	myMaxAngle = aMaxAngle;
}

void ParticleEmitter::SetParticleLifeTime(float aMinLifeTime, float aMaxLifeTime)
{
	myMinLifeTime = aMinLifeTime;
	myMaxLifeTime = aMaxLifeTime;
}

void ParticleEmitter::SetAcceleration(const Tga2D::Vector2f & aAcceleration)
{
	myAcceleration.x = aAcceleration.x / 30.0f;
	myAcceleration.y = aAcceleration.y / 16.875f;
}

void ParticleEmitter::SetColor(const Tga2D::CColor & aStartColor, const Tga2D::CColor &aMid1Color, const Tga2D::CColor &aMid2Color, const Tga2D::CColor & aEndColor)
{
	myStartColor = aStartColor;
	myMid1Color = aMid1Color;
	myMid1Color.myA = Lerp(aStartColor.myA, aEndColor.myA, 0.33f);
	myMid2Color = aMid2Color;
	myMid2Color.myA = Lerp(aStartColor.myA, aEndColor.myA, 0.66f);
	myEndColor = aEndColor;
}

void ParticleEmitter::SetEmitDuration(float aDuration)
{
	myTimer = 0.0f;
	myFinishedEmitting = false;
	if (aDuration <= 0.0f)
	{
		myHasDuration = false;
	}
	else
	{
		myEmitTime = aDuration;
		myHasDuration = true;
	}
}

void ParticleEmitter::SetRotation(float aMinRotation, float aMaxRotation)
{
	myMinRotation = aMinRotation* (3.1415f / 180.0f);
	myMaxRotation = aMaxRotation* (3.1415f / 180.0f);
}

void ParticleEmitter::SetPosition(const Tga2D::Vector2f & aPosition)
{
	myPosition.x = aPosition.x;
	myPosition.y = aPosition.y;
}

void ParticleEmitter::SetRotation(const float aRotation)
{
	SetRotation(aRotation, aRotation);
}

int ParticleEmitter::GetSize()
{
	return myParticles.Size();
}

float ParticleEmitter::GetMinAngle()
{
	return myMinAngle;
}

float ParticleEmitter::GetMaxAngle()
{
	return myMaxAngle;
}

const bool ParticleEmitter::GetHasDuration()
{
	return myHasDuration;
}

const bool ParticleEmitter::GetFinishedEmitting()
{
	return myFinishedEmitting;
}

void ParticleEmitter::ClearEmitter()
{
	myParticles.~GrowingArray();
	mySpriteBatch.DeleteAll();
}

float ParticleEmitter::RandomRange(float aMin, float aMax)
{
	return ((static_cast<float>(rand()) / static_cast<float>(RAND_MAX)) * (aMax - aMin)) + aMin;
}

float ParticleEmitter::Lerp(float aStart, float aEnd, float aT)
{
	return aStart + aT* (aEnd - aStart);
}

const Tga2D::CColor ParticleEmitter::Lerp(const Tga2D::CColor & aStart, const Tga2D::CColor & aEnd, float aT)
{
	return{ Lerp(aStart.myR, aEnd.myR, aT),Lerp(aStart.myG, aEnd.myG, aT),Lerp(aStart.myB, aEnd.myB, aT),Lerp(aStart.myA, aEnd.myA, aT) };
}

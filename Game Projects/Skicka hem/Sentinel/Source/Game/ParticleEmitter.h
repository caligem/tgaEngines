#pragma once
#include "Drawable.h"
#include "tga2d\sprite\sprite.h"
#include "tga2d\sprite\sprite_batch.h"
#include <GrowingArray.h>

class ParticleEmitter : public Drawable
{
public:
	ParticleEmitter();
	ParticleEmitter(const ParticleEmitter& aParticleEmitter);
	ParticleEmitter &operator=(const ParticleEmitter& aParticleEmitter);
	~ParticleEmitter() override;
	void Init(const char* aEmitterIndexToSpawn) override;
	void Render() override;
	void Update();
	void SetSpriteSource(const std::string &aSpriteSource, int aBlendMode);
	void SetScale(float aStartScale, float aEndScale);
	void SetTimeBetweenSpawn(float aMinTime, float aMaxTime);
	void SetStartSpeed(float aMinSpeed, float aMaxSpeed);
	void SetAngle(float aMinAngle, float aMaxAngle);
	void SetAngleRadians(float aMinAngle, float aMaxAngle);
	void SetParticleLifeTime(float aMinLifeTime, float aMaxLifeTime);
	void SetAcceleration(const Tga2D::Vector2f &aAcceleration);
	void SetColor(const Tga2D::CColor &aStartColor, const Tga2D::CColor &aMid1Color, const Tga2D::CColor &aMid2Color, const Tga2D::CColor &aEndColor);
	void SetEmitDuration(float aDuration = -1.0f);
	void SetRotation(float aMinRotation, float aMaxRotation);
	void SetPosition(const Tga2D::Vector2f &aPosition);
	void SetRotation(const float aRotation) override;

	int GetSize();
	float GetMinAngle();
	float GetMaxAngle();
	const bool GetHasDuration();
	const bool GetFinishedEmitting();
	void ClearEmitter();
private:
	struct Particle {
		Particle() {}
		Particle(float aLifeTime, Tga2D::Vector2f aPosition, Tga2D::Vector2f aVelocity, float aRotation)
		{
			myLifeTime = aLifeTime;
			myTime = 0.0f;
			mySpawnPosition = aPosition;
			myVelocity = aVelocity;
			myRotation = aRotation;
		}
		Particle &operator=(const Particle &aParticle)
		{
			myLifeTime = aParticle.myLifeTime;
			myTime = aParticle.myTime;
			myPosition = aParticle.myPosition;
			mySpawnPosition = aParticle.mySpawnPosition;
			myVelocity = aParticle.myVelocity;
			myRotation = aParticle.myRotation;
			return *this;
		}
		float myLifeTime;
		float myTime;
		Tga2D::Vector2f myPosition;
		Tga2D::Vector2f mySpawnPosition;
		Tga2D::Vector2f myVelocity;
		float myRotation;
	};
	float myStartScale;
	float myEndScale;
	float myMinTimeBetweenParticleSpawns;
	float myMaxTimeBetweenParticleSpawns;
	float myCurrParticleSpawnTime;
	float myMinStartSpeed;
	float myMaxStartSpeed;
	float myMinAngle;
	float myMaxAngle;
	float myMinLifeTime;
	float myMaxLifeTime;
	float myMinRotation;
	float myMaxRotation;
	Tga2D::Vector2f myAcceleration;
	Tga2D::CColor myStartColor;
	Tga2D::CColor myMid1Color;
	Tga2D::CColor myMid2Color;
	Tga2D::CColor myEndColor;
	EBlendState myBlendState;
	float myEmitTime;
	bool myHasDuration;
	Tga2D::CSpriteBatch mySpriteBatch;
	CommonUtilities::GrowingArray<Particle> myParticles;
	float myTimer;
	std::string mySource;
	float RandomRange(float aMin, float aMax);
	float Lerp(float aStart, float aEnd, float aT);
	const Tga2D::CColor Lerp(const Tga2D::CColor &aStart, const Tga2D::CColor &aEnd, float aT);
	bool myFinishedEmitting;

	CommonUtilities::Vector2f myOriginalScreenSize;
};
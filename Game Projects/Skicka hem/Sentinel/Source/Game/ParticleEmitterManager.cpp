#include "stdafx.h"
#include "ParticleEmitterManager.h"
#include <assert.h>
#include "JsonWrapper.h"
#include <tga2d/engine.h>

ParticleEmitterManager *ParticleEmitterManager::ourInstance;
bool ParticleEmitterManager::Create()
{
	assert(ourInstance == nullptr && "ParticleManager already created");
	ourInstance = new ParticleEmitterManager();
	if (ourInstance == nullptr)
	{
		return(false);
	}
	json emitterDataFile = LoadData("data/particleEmitters.json");
	ourInstance->myParticleEmitterTemplates.Init(emitterDataFile["particleEmitters"].size());
	for (size_t i = 0; i < emitterDataFile["particleEmitters"].size(); i++)
	{
		ourInstance->myParticleEmitterTemplates.Add(ParticleEmitter());
		ParticleEmitter &lastElement = ourInstance->myParticleEmitterTemplates[ourInstance->myParticleEmitterTemplates.Size() - 1];
		lastElement.SetSpriteSource(emitterDataFile["particleEmitters"][i]["sprite"].get<std::string>().c_str(), emitterDataFile["particleEmitters"][i]["blendMode"].get<int>());
		lastElement.SetRotation(emitterDataFile["particleEmitters"][i]["minRotation"].get<float>(), emitterDataFile["particleEmitters"][i]["maxRotation"].get<float>());
		lastElement.SetAcceleration({ emitterDataFile["particleEmitters"][i]["accelerationX"].get<float>(), emitterDataFile["particleEmitters"][i]["accelerationY"].get<float>() });
		lastElement.SetColor(
			{ emitterDataFile["particleEmitters"][i]["startColor"][0].get<float>(), emitterDataFile["particleEmitters"][i]["startColor"][1].get<float>(), emitterDataFile["particleEmitters"][i]["startColor"][2].get<float>(), emitterDataFile["particleEmitters"][i]["startColor"][3].get<float>() },
			{ emitterDataFile["particleEmitters"][i]["mid1Color"][0].get<float>(), emitterDataFile["particleEmitters"][i]["mid1Color"][1].get<float>(), emitterDataFile["particleEmitters"][i]["mid1Color"][2].get<float>(), emitterDataFile["particleEmitters"][i]["mid1Color"][3].get<float>() },
			{ emitterDataFile["particleEmitters"][i]["mid2Color"][0].get<float>(), emitterDataFile["particleEmitters"][i]["mid2Color"][1].get<float>(), emitterDataFile["particleEmitters"][i]["mid2Color"][2].get<float>(), emitterDataFile["particleEmitters"][i]["mid2Color"][3].get<float>() },
			{ emitterDataFile["particleEmitters"][i]["endColor"][0].get<float>(), emitterDataFile["particleEmitters"][i]["endColor"][1].get<float>(), emitterDataFile["particleEmitters"][i]["endColor"][2].get<float>(), emitterDataFile["particleEmitters"][i]["endColor"][3].get<float>() }
		);
		lastElement.SetAngle(emitterDataFile["particleEmitters"][i]["minAngle"].get<float>(), emitterDataFile["particleEmitters"][i]["maxAngle"].get<float>());
		lastElement.SetStartSpeed(emitterDataFile["particleEmitters"][i]["minStartSpeed"].get<float>(), emitterDataFile["particleEmitters"][i]["maxStartSpeed"].get<float>());
		lastElement.SetScale(emitterDataFile["particleEmitters"][i]["startScale"].get<float>(), emitterDataFile["particleEmitters"][i]["endScale"].get<float>());
		lastElement.SetParticleLifeTime(emitterDataFile["particleEmitters"][i]["minParticleLifeTime"].get<float>(), emitterDataFile["particleEmitters"][i]["maxParticleLifeTime"].get<float>());
		lastElement.SetEmitDuration(emitterDataFile["particleEmitters"][i]["emitDuration"].get<float>());
		lastElement.SetTimeBetweenSpawn(emitterDataFile["particleEmitters"][i]["minTimeBetweenSpawn"].get<float>(), emitterDataFile["particleEmitters"][i]["maxTimeBetweenSpawn"].get<float>());
	}

	ourInstance->myActiveParticleEmitters.Init(256);
	return(true);
}

bool ParticleEmitterManager::Destroy()
{
	delete ourInstance;
	ourInstance = nullptr;
	return true;
}

void ParticleEmitterManager::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*> &aRenderBuffer)
{
	unsigned short size = ourInstance->myActiveParticleEmitters.Size();
	for (unsigned short particleEmitterIndex = size; particleEmitterIndex > 0; --particleEmitterIndex)
	{
		if ((ourInstance->myActiveParticleEmitters[particleEmitterIndex - 1].GetHasDuration() == true && ourInstance->myActiveParticleEmitters[particleEmitterIndex - 1].GetFinishedEmitting() == true))
		{
			ourInstance->myActiveParticleEmitters[particleEmitterIndex - 1].ClearEmitter();
  			ourInstance->myActiveParticleEmitters.RemoveCyclicAtIndex(particleEmitterIndex - 1);
		}
		else
		{
			aRenderBuffer.Add(&ourInstance->myActiveParticleEmitters[particleEmitterIndex - 1]);
		}
	}
}
ParticleEmitter& ParticleEmitterManager::SpawnEmitterByIndex(int aEmitterIndex, const Tga2D::Vector2f &aPosToSpawnOn)
{
	ourInstance->myActiveParticleEmitters.Add(ourInstance->myParticleEmitterTemplates[aEmitterIndex]);
	ourInstance->myActiveParticleEmitters[ourInstance->myActiveParticleEmitters.Size() - 1].SetPosition(aPosToSpawnOn);
	return ourInstance->myActiveParticleEmitters[ourInstance->myActiveParticleEmitters.Size() - 1];
}

const ParticleEmitter & ParticleEmitterManager::SpawnOwnedEmitterByIndex(int aEmitterIndex)
{
	return ourInstance->myParticleEmitterTemplates[aEmitterIndex];
}



ParticleEmitterManager::ParticleEmitterManager()
{
}


ParticleEmitterManager::~ParticleEmitterManager()
{
}

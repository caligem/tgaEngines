#pragma once
#include "GrowingArray.h"
#include "tga2d\sprite\sprite.h"
#include "ParticleEmitter.h"
class ParticleEmitterManager
{
public:
	ParticleEmitterManager(const ParticleEmitterManager& aParticleEmitterManager) = delete;
	static bool Create();
	static bool Destroy();
	static void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*> &aRenderBuffer);
	static ParticleEmitter& SpawnEmitterByIndex(int aEmitterIndex, const Tga2D::Vector2f &aPosToSpawnOn);
	static const ParticleEmitter& SpawnOwnedEmitterByIndex(int aEmitterIndex);
private:
	ParticleEmitterManager();
	~ParticleEmitterManager();
	CommonUtilities::GrowingArray<ParticleEmitter> myParticleEmitterTemplates;
	CommonUtilities::GrowingArray<ParticleEmitter> myActiveParticleEmitters;
	static ParticleEmitterManager *ourInstance;
};


#include "stdafx.h"
#include "PauseMenuState.h"
#include "InputManager.h"
#include "StateStack.h"
#include "Sprite.h"
#include "InputWrapper.h"
#include "Timer.h"
#include "SoundEngine.h"

PauseMenuState::PauseMenuState(CommonUtilities::InputManager* aInputManager, CurrentCollData aData)
{
	myInputManager = aInputManager;
	myShouldPop = false;
	myShouldPopToMenu = false;
	myCurrColData = aData;
}


PauseMenuState::~PauseMenuState()
{
	mySound->Destroy();
	mySelectedButton = nullptr;
}

void PauseMenuState::Init()
{
	myRenderBuffer.Init(64);
	InitButtons();
	InitSprites();
	myColCounter.Init(0, 0, true);
	myColCounter.SetPosition({ 0.1f, 0.1f });
	Timer::SetDeltaTimeMultiplier(0.f);

	mySound = new Sound("Sounds/Sfx/button_hovored.wav");
}

void PauseMenuState::OnEnter()
{
	Init();
}

void PauseMenuState::OnExit()
{
	Timer::SetDeltaTimeMultiplier(1.f);
	DeleteSprites();
}

void PauseMenuState::UpdateCollectibleCounter() 
{
	myColCounter.UpdateCounter(myCurrColData.myColl, myCurrColData.myTotalColl);
}

eStateStackMessage PauseMenuState::Update()
{
	if (InputWrapper::IsButtonPressed(KeyButton::Up) || InputWrapper::IsButtonPressed(KeyButton::Down)
		|| InputWrapper::IsButtonPressed(KeyButton::Back))
	{
		SoundEngine::PlaySound(*mySound);
	}

	while (ShowCursor(true) <= 0);
	CleanUp();
	UpdateButtons();
	FillRenderBuffer();
	UpdateCollectibleCounter();

	if (InputWrapper::IsButtonPressed(KeyButton::Pause))
	{
		return eStateStackMessage::PopSubState;
	}
	if (myShouldPopToMenu)
	{
		return eStateStackMessage::PopGameAndLevelSelect;
	}
	if (myShouldPop)
	{
		return eStateStackMessage::PopSubState;
	}

	return eStateStackMessage::KeepState;
}

void PauseMenuState::Render()
{
	for (unsigned short i = 0; i < myRenderBuffer.Size(); i++)
	{
		myRenderBuffer[i]->Render();
	}
	static_cast<Animation*>(myArrow)->RawRender();
}

void PauseMenuState::ResetRoom()
{
	myStateStack->myGameProgressData.myShouldResetRoom = true;
	myShouldPop = true;
}

void PauseMenuState::CleanUp()
{
	myRenderBuffer.RemoveAll();
}

void PauseMenuState::FillRenderBuffer()
{
	myRenderBuffer.Add(myBackground);
	myRenderBuffer.Add(myLogo);
	myColCounter.FillRenderBuffer(myRenderBuffer);

	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		myButtons[i]->FillRenderBuffer(myRenderBuffer);
	}
}

void PauseMenuState::InitSprites()
{
	myBackground = new Sprite();
	myBackground->Init("Sprites/menu/pauseBackground.dds");
	myBackground->SetPosition({ 0.5f, 0.5f });
	myBackground->SetSizeRelativeToScreen({ 2.f, 1.f });

	myLogo = new Sprite();
	myLogo->Init("Sprites/menu/logoPaused.dds");
	myLogo->SetPosition({ 0.5f, 0.2f });

	myArrow = new Animation();
	myArrow->Init("menuArrow_animated");
	myArrow->SetPosition(myContinueButton.GetPosition() - CommonUtilities::Vector2f(0.15f, 0.f));
}

void PauseMenuState::DeleteSprites()
{
	delete myBackground;
	myBackground = nullptr;

	delete myLogo;
	myLogo = nullptr;
}

void PauseMenuState::InitButtons()
{
	myButtons.Init(16);

	myContinueButton.Init([&] {myShouldPop = true; }, "buttonContinue", CommonUtilities::Vector2f(0.5f, 0.5f), myInputManager);
	myContinueButton.SetHitboxOffsetX(0.05f);
	myContinueButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myContinueButton);

	myResetButton.Init([&] {ResetRoom(); }, "buttonRestart", CommonUtilities::Vector2f(0.5f, 0.6f), myInputManager);
	myResetButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myResetButton);

	myOptions.Init([&] {myStateStack->PushSubState(new OptionState(myInputManager)); }, "buttonOptions", CommonUtilities::Vector2f(0.5f, 0.7f), myInputManager);
	myOptions.SetHitboxOffsetX(0.055f);
	myOptions.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myOptions);

	myExitToMenuButton.Init([&] {myShouldPopToMenu = true; }, "buttonExitmenu", CommonUtilities::Vector2f(0.5f, 0.8f), myInputManager);
	myExitToMenuButton.SetHitboxOffsetX(0.01f);
	myExitToMenuButton.SetHitboxOffsetY(0.03f);
	myButtons.Add(&myExitToMenuButton);

	mySelectedButton = &myContinueButton;
	ConnectButtons();
}

void PauseMenuState::UpdateButtons()
{
	CheckMouseInput();
	CheckControllerInput();

	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		myButtons[i]->Update();
		myButtons[i]->SetCurrentFrame(myButtons[i]->GetDefaultFrame());
	}

	mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame() + 1);

	myArrow->SetPosition(mySelectedButton->GetPosition() - CommonUtilities::Vector2f(0.15f, 0.f));

}
void PauseMenuState::ConnectButtons()
{
	myContinueButton.ConnectButton(&myResetButton, Button::eConnectSide::Down);
	myResetButton.ConnectButton(&myOptions, Button::eConnectSide::Down);
	myOptions.ConnectButton(&myExitToMenuButton, Button::eConnectSide::Down);
	myExitToMenuButton.ConnectButton(&myContinueButton, Button::eConnectSide::Down);
}

void PauseMenuState::CheckMouseInput()
{
	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		Button* currentButton = myButtons[i];

		currentButton->Update();
		if (myInputManager->GetCursorPositionOnScreenLastUpdate() != myInputManager->GetCursorPositionOnScreen())
		{
			if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
			{
				mySelectedButton = currentButton;
				currentButton->SetCurrentFrame(currentButton->GetDefaultFrame() + 1);
			}
			else
			{
				currentButton->SetCurrentFrame(currentButton->GetDefaultFrame());
			}
		}
	}

	if (myInputManager->IsMouseButtonClicked(CommonUtilities::MouseButton::Left))
	{
		for (unsigned short i = 0; i < myButtons.Size(); i++)
		{
			Button* currentButton = myButtons[i];

			if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
			{
				currentButton->OnClick();
			}
		}
	}
}

void PauseMenuState::CheckControllerInput()
{
	if (InputWrapper::IsButtonPressed(KeyButton::Down))
	{
		if (mySelectedButton->myConnectedButtons.myDownButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myDownButton;
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame() + 1);
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Up))
	{
		if (mySelectedButton->myConnectedButtons.myUpButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myUpButton;
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame() + 1);
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Select))
	{
		mySelectedButton->OnClick();
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Back))
	{
		myContinueButton.OnClick();
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Pause))
	{
		myContinueButton.OnClick();
	}
}

#pragma once
#include "GameState.h"
#include "CollectibleCounter.h"

class Sound;

struct CurrentCollData 
{
	int myColl = 0;
	int myTotalColl = 0;
};

class PauseMenuState : public GameState
{
public:
	PauseMenuState(CommonUtilities::InputManager* aInputManager, CurrentCollData aData);
	~PauseMenuState();

	void Init() override;
	void OnEnter() override;
	void OnExit() override;

	void UpdateCollectibleCounter();

	eStateStackMessage Update() override;
	void Render() override;

	inline const bool LetThroughRender() const override { return true; }

private:
	bool myShouldPop;
	bool myShouldPopToMenu;
	
	void ResetRoom();

	void CleanUp();
	void FillRenderBuffer();
	void InitSprites();

	void DeleteSprites();

	void InitButtons();
	void UpdateButtons();
	void ConnectButtons();

	void CheckMouseInput();
	void CheckControllerInput();

	Drawable* myBackground;
	Drawable* myLogo;
	Drawable* myArrow;
	CommonUtilities::GrowingArray<Drawable*> myRenderBuffer;

	Button* mySelectedButton;
	Button myContinueButton;
	Button myOptions;
	Button myResetButton;
	Button myExitToMenuButton;
	CommonUtilities::GrowingArray<Button*> myButtons;

	CollectibleCounter myColCounter;

	Sound* mySound;

	CurrentCollData myCurrColData;
};


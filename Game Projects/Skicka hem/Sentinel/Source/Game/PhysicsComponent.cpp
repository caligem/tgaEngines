#include "stdafx.h"
#include "PhysicsComponent.h"

#include "Vector.h"
#include "Collider.h"
#include "GameObject.h"
#include "Timer.h"
#include "cit\utility.h"

#define MAX_VELOCITY 1.f
float PhysicsComponent::ourGravity = 0.35f;
PhysicsComponent::PhysicsComponent()
{
	myWeight = 1.0f;
	myFriction = 0.0f;
	myBouncyness = 0.0f;
	myIsKinematic = false;
	myCollidingLeft = false;
	myCollidingRight = false;
	myCollidingTop = false;
	myCollidingBottom = false;
}


PhysicsComponent::~PhysicsComponent()
{
}

void PhysicsComponent::Init(Collider & aOwnerCol)
{
	BoxCollider *ownerBox = dynamic_cast<BoxCollider*>(&aOwnerCol);
	myOwnerCol = &aOwnerCol;
	myLeftCheck.Init(ownerBox->GetWidth() / 8.0f, ownerBox->GetHeight() - ownerBox->GetHeight()/8.0f, CommonUtilities::Vector2f{ -ownerBox->GetWidth() / 2.0f, 0.0f });
	myRightCheck.Init(ownerBox->GetWidth() / 8.0f, ownerBox->GetHeight() - ownerBox->GetHeight() / 8.0f, CommonUtilities::Vector2f{ ownerBox->GetWidth() / 2.0f, 0.0f });
	myBottomCheck.Init(ownerBox->GetWidth() - ownerBox->GetWidth() / 6.0f, ownerBox->GetHeight() / 12.0f, CommonUtilities::Vector2f{ 0.0f, ownerBox->GetHeight() / 2.0f });
	myTopCheck.Init(ownerBox->GetWidth() - ownerBox->GetWidth() / 6.0f, ownerBox->GetHeight() / 12.0f, CommonUtilities::Vector2f{ 0.0f, -ownerBox->GetHeight() / 2.0f });

	
	myLeftCheck.SetOwner(aOwnerCol.GetOwner());
	myRightCheck.SetOwner(aOwnerCol.GetOwner());
	myBottomCheck.SetOwner(aOwnerCol.GetOwner());
	myTopCheck.SetOwner(aOwnerCol.GetOwner());

	myIgnoredTags.Init(8);
}

void PhysicsComponent::Update(Collider&aOwnerCol, CommonUtilities::Vector2f & aPos)
{
	myOwnerCol = &aOwnerCol;
	
	/*myLeftCheck.SetOwner(aOwnerCol.GetOwner());
	myRightCheck.SetOwner(aOwnerCol.GetOwner());
	myBottomCheck.SetOwner(aOwnerCol.GetOwner());
	myTopCheck.SetOwner(aOwnerCol.GetOwner());*/

	myVelocity.x = cit::Clamp(myVelocity.x, -MAX_VELOCITY, MAX_VELOCITY);
	myVelocity.y = cit::Clamp(myVelocity.y, -MAX_VELOCITY, MAX_VELOCITY);
	aPos += myVelocity * CommonUtilities::Timer::GetDeltaTime();
	if (myIsKinematic == false)
	{
		myVelocity.y += ourGravity * myWeight * CommonUtilities::Timer::GetDeltaTime();
	}
	else
	{
		myVelocity.x = 0.0f;
		myVelocity.y = 0.0f;
	}
	myCollidingBottomOldFrame = myCollidingBottom;
	myCollidingLeftOldFrame = myCollidingLeft;
	myCollidingRightOldFrame = myCollidingRight;
	myCollidingTopOldFrame = myCollidingTop;
	myCollidingBottom = false;
	myCollidingLeft = false;
	myCollidingTop = false;
	myCollidingRight = false;
	//myBottomCheck.Draw();
	//myRightCheck.Draw();
	//myLeftCheck.Draw();
	//myTopCheck.Draw();
}

void PhysicsComponent::OnIntersection(Collider & aOtherCol)
{
	for (unsigned short i = 0; i < myIgnoredTags.Size(); i++)
	{
		if (myIgnoredTags[i] == aOtherCol.GetTag())
		{
			return;
		}
	}
	if (aOtherCol.GetIsSolid() && myIsKinematic == false)
	{
		if (myBottomCheck.IsCollidedWith(&aOtherCol))
		{
			if (myCollidingTop == true)
			{
				myVelocity.y = 0.0f;
			}
			if (myCollidingBottom == true || myCollidingTop == true || myTopCheck.IsCollidedWith(&aOtherCol))
			{
				return;
			}
			float velocityToMove = HandleBottomGhosting(aOtherCol);
			myCollidingBottom = true;
			if (myVelocity.y > 0.0f)
			{
				Bounce(CommonUtilities::Vector2f(0.0f, 1.0f));
				myVelocity.x *= (1.0f - (myFriction));
			}
			if (velocityToMove < -0.001f)
			{
				myVelocity.y += velocityToMove;
			}
		}
		else if (myRightCheck.IsCollidedWith(&aOtherCol))
		{
			if (myCollidingLeft == true)
			{
				myVelocity.x = 0.0f;
			}
			if (myCollidingRight == true || myCollidingLeft == true || myLeftCheck.IsCollidedWith(&aOtherCol))
			{
				return;
			}
			float velocityToMove = HandleRightGhosting(aOtherCol);
			myCollidingRight = true;
			if (myVelocity.x > 0.0f)
			{
				Bounce(CommonUtilities::Vector2f(1.0f, 0.0f));
				myVelocity.y *= (1.0f - (myFriction));
			}
			if (velocityToMove < -0.001f)
			{
				myVelocity.x += velocityToMove;
			}
		}
		else if (myLeftCheck.IsCollidedWith(&aOtherCol))
		{
			if (myCollidingRight == true)
			{
				myVelocity.x = 0.0f;
			}
			if (myCollidingRight == true || myCollidingLeft == true || myRightCheck.IsCollidedWith(&aOtherCol))
			{
				return;
			}
			float velocityToMove = HandleLeftGhosting(aOtherCol);
			myCollidingLeft = true;
			if (myVelocity.x < 0.0f)
			{
				Bounce(CommonUtilities::Vector2f(-1.0f, 0.0f));
				myVelocity.y *= (1.0f - (myFriction));
			}
			if (velocityToMove > 0.001f)
			{
				myVelocity.x += velocityToMove;
			}
		}
		else if (myTopCheck.IsCollidedWith(&aOtherCol))
		{
			if (myCollidingBottom == true)
			{
				myVelocity.y = 0.0f;
			}
			if (myCollidingBottom == true || myCollidingTop == true || myBottomCheck.IsCollidedWith(&aOtherCol))
			{
				return;
			}
			float velocityToMove = HandleTopGhosting(aOtherCol);
			myCollidingTop = true;
			if (myVelocity.y < 0.0f)
			{
				Bounce(CommonUtilities::Vector2f(0.0f, -1.0f));
				myVelocity.x *= (1.0f - (myFriction));
			}
			if (velocityToMove > 0.001f)
			{
				myVelocity.y += velocityToMove;
			}
		}
	}
}
void PhysicsComponent::IgnoreTag(ColliderTag aColTagToIgnore)
{
	for (unsigned short i = 0; i < myIgnoredTags.Size(); i++)
	{
		if (myIgnoredTags[i] == aColTagToIgnore)
		{
			return;
		}
	}
	myIgnoredTags.Add(aColTagToIgnore);
}
void PhysicsComponent::RemoveIgnoredTag(ColliderTag aColTagToRemoveFromIgnored)
{
	for (unsigned short i = 0; i < myIgnoredTags.Size(); i++)
	{
		if (myIgnoredTags[i] == aColTagToRemoveFromIgnored)
		{
			myIgnoredTags.RemoveCyclicAtIndex(i);
			break;
		}
	}
}
void PhysicsComponent::SetWeight(float aWeightToSet)
{
	myWeight = aWeightToSet;
}

const float PhysicsComponent::GetWeight() const
{
	return myWeight;
}

void PhysicsComponent::SetFriction(float aFrictionToSet)
{
	if (aFrictionToSet < 0.0f)
	{
		myFriction = 0.0f;
	}
	else
	{
		if (aFrictionToSet > 1.0f)
		{
			aFrictionToSet = 1.0f;
		}
		myFriction = aFrictionToSet;
	}
}

const float PhysicsComponent::GetFriction() const
{
	return myFriction;
}

void PhysicsComponent::SetBouncyness(float aBouncynessToSet)
{
	if (aBouncynessToSet < 0.0f)
	{
		myBouncyness = 0.0f;
	}
	else
	{
		myBouncyness = aBouncynessToSet;
	}
}

const float PhysicsComponent::GetBouncyness() const
{
	return myBouncyness;
}

void PhysicsComponent::SetGravity(float aGravToSet)
{
	ourGravity = aGravToSet;
}

const float PhysicsComponent::GetGravity()
{
	return ourGravity;
}

void PhysicsComponent::SetVelocity(const CommonUtilities::Vector2f & aVelocityToSet)
{
	myVelocity = aVelocityToSet;
}

const CommonUtilities::Vector2f & PhysicsComponent::GetVelocity() const
{
	return myVelocity;
}

const bool PhysicsComponent::GetCollidingLeft() const
{
	return myCollidingLeftOldFrame;
}

const bool PhysicsComponent::GetCollidingRight() const
{
	return myCollidingRightOldFrame;
}

const bool PhysicsComponent::GetCollidingTop() const
{
	return myCollidingTopOldFrame;
}

const bool PhysicsComponent::GetCollidingBottom() const
{
	return myCollidingBottomOldFrame;
}

bool PhysicsComponent::LeftIsCollidingWith(Collider & aOther)
{
	return myLeftCheck.IsCollidedWith(&aOther);
}

bool PhysicsComponent::RightIsCollidingWith(Collider & aOther)
{
	return myRightCheck.IsCollidedWith(&aOther);
}

bool PhysicsComponent::TopIsCollidingWith(Collider & aOther)
{
	return myTopCheck.IsCollidedWith(&aOther);
}

bool PhysicsComponent::BottomIsCollidingWith(Collider & aOther)
{
	return myBottomCheck.IsCollidedWith(&aOther);
}

void PhysicsComponent::SetIsKinematic(bool aValue)
{
	myIsKinematic = aValue;
}

bool PhysicsComponent::GetIsKinematic() const
{
	return myIsKinematic;
}

float PhysicsComponent::HandleBottomGhosting(Collider &aOther)
{
	CommonUtilities::Vector2f comparisionPoint;
	comparisionPoint = myBottomCheck.GetOwner()->GetPosition() + myBottomCheck.GetOffset();
	comparisionPoint.y += myBottomCheck.GetHeight() / 2.0f;
	return aOther.RelativePoint(comparisionPoint).y;
 }

float PhysicsComponent::HandleTopGhosting(Collider & aOther)
{
	CommonUtilities::Vector2f comparisionPoint;
	comparisionPoint = myTopCheck.GetOwner()->GetPosition() + myTopCheck.GetOffset();
	comparisionPoint.y -= myTopCheck.GetHeight() / 2.0f;
	return aOther.RelativePoint(comparisionPoint).y;
}

float PhysicsComponent::HandleLeftGhosting(Collider & aOther)
{
	CommonUtilities::Vector2f comparisionPoint;
	comparisionPoint = myLeftCheck.GetOwner()->GetPosition() + myLeftCheck.GetOffset();
	comparisionPoint.x -= myLeftCheck.GetWidth() / 2.0f;
	return aOther.RelativePoint(comparisionPoint).x;
}

float PhysicsComponent::HandleRightGhosting(Collider & aOther)
{
	CommonUtilities::Vector2f comparisionPoint;
	comparisionPoint = myRightCheck.GetOwner()->GetPosition() + myRightCheck.GetOffset();
	comparisionPoint.x += myRightCheck.GetWidth() / 2.0f;
	return aOther.RelativePoint(comparisionPoint).x;
}

void PhysicsComponent::Bounce(const CommonUtilities::Vector2f & aReversedNormal)
{
	float bounceLength = ((myVelocity * myBouncyness )/ myWeight).Length2();
	if (aReversedNormal.y != 0.0f)
	{
		if (bounceLength > 0.1f)
		{
			float angle = aReversedNormal.Dot(myVelocity.GetNormalized());
			if (aReversedNormal.x == 1.0f)
			{
				myVelocity = CommonUtilities::Vector2f(myVelocity.x, -aReversedNormal.y - sinf(angle)) * sqrtf(bounceLength);
			}
			else
			{
				myVelocity = CommonUtilities::Vector2f(myVelocity.x, -aReversedNormal.y + sinf(angle)) * sqrtf(bounceLength);
			}
			myVelocity.x *= 9.0f / 16.0f;
		}
		else
		{
			myVelocity.y = 0.0f;
		}
	}
	else
	{
		if (bounceLength > 0.1f)
		{
			float angle = aReversedNormal.Dot(myVelocity.GetNormalized());
			if (aReversedNormal.x == -1.0f)
			{
				myVelocity = CommonUtilities::Vector2f(-aReversedNormal.x - cosf(angle), myVelocity.y) * sqrtf(bounceLength);
			}
			else
			{
				myVelocity = CommonUtilities::Vector2f(-aReversedNormal.x + cosf(angle), myVelocity.y) * sqrtf(bounceLength);
			}
			myVelocity.x *= 9.0f / 16.0f;
		}
		else
		{
			myVelocity.x = 0.0f;
		}
	}
	
}

#pragma once
#include "Vector.h"
#include "BoxCollider.h"
#include "GrowingArray.h"
class Collider;

class PhysicsComponent
{
public:
	PhysicsComponent();
	~PhysicsComponent();
	void Init(Collider &aOwnerCol);
	void Update(Collider&aOwnerCol, CommonUtilities::Vector2f &aPos);
	void OnIntersection(Collider& aOtherCol);

	void IgnoreTag(ColliderTag aColTagToIgnore);
	void RemoveIgnoredTag(ColliderTag aColTagToRemoveFromIgnored);

	void SetWeight(float aWeightToSet);
	const float GetWeight() const;

	void SetFriction(float aFrictionToSet);
	const float GetFriction() const;

	void SetBouncyness(float aBouncynessToSet);
	const float GetBouncyness() const;

	static void SetGravity(float aGravToSet);
	static const float GetGravity();

	void SetVelocity(const CommonUtilities::Vector2f &aVelocityToSet);
	const CommonUtilities::Vector2f &GetVelocity() const;

	const bool GetCollidingLeft() const;
	const bool GetCollidingRight() const;
	const bool GetCollidingTop() const;
	const bool GetCollidingBottom() const;

	bool LeftIsCollidingWith(Collider &aOther);
	bool RightIsCollidingWith(Collider &aOther);
	bool TopIsCollidingWith(Collider &aOther);
	bool BottomIsCollidingWith(Collider &aOther);

	void SetIsKinematic(bool aValue);
	bool GetIsKinematic() const;
private:	
	float HandleBottomGhosting(Collider &aOther);
	float HandleTopGhosting(Collider &aOther);
	float HandleLeftGhosting(Collider &aOther);
	float HandleRightGhosting(Collider &aOther);
	void Bounce(const CommonUtilities::Vector2f &aReversedNormal);
	bool myIsKinematic;
	static float ourGravity;
	float myWeight;
	float myFriction;
	float myBouncyness;
	CommonUtilities::Vector2f myVelocity;
	CommonUtilities::GrowingArray<ColliderTag> myIgnoredTags;
	Collider * myOwnerCol;
	BoxCollider myLeftCheck;
	BoxCollider myRightCheck;
	BoxCollider myTopCheck;
	BoxCollider myBottomCheck;

	bool myCollidingLeft;
	bool myCollidingRight;
	bool myCollidingTop;
	bool myCollidingBottom;

	bool myCollidingLeftOldFrame; 
	bool myCollidingRightOldFrame;
	bool myCollidingTopOldFrame;
	bool myCollidingBottomOldFrame;
};


#include "stdafx.h"
#include "Player.h"
#include "Timer.h"
#include "JsonWrapper.h"
#include "InputWrapper.h"
#include "tga2d\math\common_math.h"
#include "LineCollider.h"
#include "PostMaster.h"
#include "Collectible.h"
#include "MovingTile.h"
#include "Camera.h"
#include "Fader.h"
#include "DialogueManager.h"

#include "SoundEngine.h"
#include "Ladder.h"
#include "TileSize.h"

#include "ParticleEmitterManager.h"

using namespace CommonUtilities;

Player::Player()
{
	myCurrentLevelIndex = 0;
	mySwappedSprites = false;
	myHasHook = false;
	myPickedUpHookInThisRoom = false;
	myPreventInput = false;
	myHasPickedUpCollectible = false;
	myHeldObject = nullptr;
	myCanPickUp = true;
	myCanSideJumpLeftSide = false;
	myLookDir = LookDirection::Right;
	myPrevLookDir = myLookDir;
	myPlayerState = PlayerStates::Idle;
	myCanSecondJump = true;

	myHand.InitGO({ 0.02f, 0.02f }, "Sprites/hook.dds", new Sprite());
	myHook.Init(this, &myHand);
	myCurrentAnimationState = AnimationState::Idle;
	InitAnimations();
	myTimerBetweenPickup.Set(0.5f, cit::countdown::type::oneshot, [&] {myCanPickUp = true; });
	myAimSprite.Init("Sprites/hookAim.dds");
	myAimSprite.SetPivot(Vector2f{ 0.5f, 0.5f });

	Sprite *nullSprite = new Sprite();
	nullSprite->Init("sprites/noObj.dds");
	myGroundChecker.InitGO({ myPosition.x, myPosition.y }, "sprites/noObj.dds", nullSprite);
	LineCollider myGroundCheckCol;
	myGroundCheckCol.Init(CommonUtilities::Vector2f{ 1.0f, 0.0f }, 0.028f, CommonUtilities::Vector2f{ -0.014f, 0.09f });
	myGroundCheckCol.SetTag(ColliderTag::GroundChecker);
	myGroundChecker.AttachCollider(myGroundCheckCol);
	myIsGrounded = false;

	InitGrabChecker();
	//Very specific load order for the sounds, if changed, change the PlayerSounds-enum as well to get the appropriate index when calling a sound!
	Sound* soundP = new Sound("Sounds/Sfx/jump.wav");
	mySounds.push_back(soundP);
	soundP = new Sound("Sounds/Sfx/flip.wav");
	mySounds.push_back(soundP);
	soundP = new Sound("Sounds/Sfx/collectible_pickup.wav");
	mySounds.push_back(soundP);
	soundP = new Sound("Sounds/Sfx/walk.wav");
	mySounds.push_back(soundP);
	soundP = new Sound("Sounds/Sfx/throw.wav");
	mySounds.push_back(soundP);
	soundP = new Sound("Sounds/Sfx/pickup.wav");
	mySounds.push_back(soundP);
	soundP = new Sound("Sounds/Sfx/respawn.wav");
	mySounds.push_back(soundP);
	soundP = new Sound("Sounds/Sfx/death.wav");
	mySounds.push_back(soundP);
	soundP = new Sound("Sounds/Sfx/wall_jump.wav");
	mySounds.push_back(soundP);
	soundP = new Sound("Sounds/Sfx/hook_jump.wav");
	mySounds.push_back(soundP);
	soundP = new Sound("Sounds/Sfx/climbing_ladder.wav");
	mySounds.push_back(soundP);
	soundP = new Sound("Sounds/Sfx/climbing_vine.wav");
	mySounds.push_back(soundP);

	myClimbingTimer = 0.0f;

	myIsWalking = false;
	mySoundTimer = 0.0f;
	//Change this to speed up/Slow down the interval of the walking sound 
	myMaxSoundTimer = 0.50f;

	myThrowIndicator.Init("boxScope");
	myDustPMLeft = ParticleEmitterManager::SpawnOwnedEmitterByIndex(3);
	myDustPMRight = ParticleEmitterManager::SpawnOwnedEmitterByIndex(3);
}


Player::~Player()
{
	for (auto sound : mySounds) 
	{
		sound->Annahilate();
		sound = nullptr;
	}
}

void Player::OnUpdate()
{
	if (mySwappedSprites == false)
	{
		if (myHasHook)
		{
			ReplaceAnimationsToHook();
		}
		else
		{
			ReplaceAnimationsToNoHook();
		}
		mySwappedSprites = true;
	}
	mySwappedAnimThisFrame = false;
	OnMovingTileChecker();
	if (myPlayerState != PlayerStates::Death)
	{
		UpdateHandPosition();

		
			myPhysX.SetIsKinematic(false);
			if (myHook.GetState() != HookState::HoldPlayerToWall && myHook.GetState() != HookState::PullPlayer && myPlayerState != PlayerStates::ClimbingMetal && myHook.GetState() != HookState::Anchored)
			{
				myRotation += (-myRotation) * 10.f * Timer::GetDeltaTime();
			}
			if (myHook.GetState() == HookState::Anchored)
			{
				myCanSecondJump = true;
				HandleSwinging();
			}
			else
			{
				if (myHook.GetState() != HookState::HoldPlayerToWall && myHook.GetState() != HookState::PullPlayer)
				{
					myPhysX.Update(*myCollider, myPosition);
				}
				
				if(!DialogueManager::IsPlaying())
					UpdateMovement();
			}
	


		Vector2f hookThrowDirection = {0, 0};
		if (InputWrapper::IsButtonDown(KeyButton::Up) == false)
			{
				if (myLookDir == LookDirection::Left)
				{
					hookThrowDirection.x = -1;
				}
				else if (myLookDir == LookDirection::Right)
				{
					hookThrowDirection.x = 1;
				}
			}

		if (InputWrapper::IsButtonDown(KeyButton::Left))
			{
				hookThrowDirection.x = -1;
			}
		if (InputWrapper::IsButtonDown(KeyButton::Up))
			{
				hookThrowDirection.y = -1.f;
			}
		if (InputWrapper::IsButtonDown(KeyButton::Right))
			{
				hookThrowDirection.x = 1;
			}
		myHook.SetDirection(hookThrowDirection);
		myAimSprite.SetRotation(myHook.GetRotation());

		if (!DialogueManager::IsPlaying())
		{
			if (myHasHook && InputWrapper::IsButtonPressed(KeyButton::Hook) && myPlayerState != PlayerStates::ClimbingMetal)
			{
				if (myHook.GetState() == HookState::Anchored)
				{
					myHook.ReelIn();
				}
				else if (hookThrowDirection.Length2() != 0 && myHeldObject == nullptr) // Until player has a direction
				{
					hookThrowDirection.Normalize();
					if (hookThrowDirection.x != 0.0f && hookThrowDirection.y != 0.0f)
					{
						SwapActiveAnimation(AnimationState::HookDiagonal);
					}
					else if (hookThrowDirection.x != 0.0f && hookThrowDirection.y == 0.0f)
					{
						SwapActiveAnimation(AnimationState::HookHorizontal);
					}
					else if (hookThrowDirection.x == 0.0f && hookThrowDirection.y != 0.0f)
					{
						SwapActiveAnimation(AnimationState::HookVertical);
					}
					myHook.Throw(hookThrowDirection);

					ThrowHeldObject();
				}
			}
		}

		myDoubleJumpCooldown.Update();
		myHook.Update();
		GameObject::OnUpdate();
		UpdateHeldObject();
	}
	else
	{
		if (myCurrentAnimationState == AnimationState::Respawn)
		{
			if (myAnimations[static_cast<unsigned int>(myCurrentAnimationState)].GetCurrentFrame() == myAnimations[static_cast<unsigned int>(myCurrentAnimationState)].GetAmountOfFrames() - 1)
			{
				SwapActiveAnimation(AnimationState::Idle);
				SetState(PlayerStates::Idle);
			}
		}
		
	}
	
	myAnimationBlender.CheckForStateChange(myCurrentAnimationState, CommonUtilities::Timer::GetDeltaTime());
	if (myHook.GetState() == HookState::Thrown)
	{
		myAnimationBlender.Pause();
	}
	else
	{
		myAnimationBlender.Unpause();
	}

	myAimSprite.SetPosition(myPosition);

	if (myHook.GetState() == HookState::HoldPlayerToWall)
	{
		myAnimations[static_cast<unsigned short>(AnimationState::WallJump)].Pause();
		if (myHook.GetPullDirection() == PullPlayerDirection::Left)
		{
			SetLookDirection(LookDirection::Right);
			SwapActiveAnimation(AnimationState::WallJump);
		}
		else
		{
			SetLookDirection(LookDirection::Left);
			SwapActiveAnimation(AnimationState::WallJump);
		}
	}
	else
	{
		myAnimations[static_cast<unsigned short>(AnimationState::WallJump)].Unpause();
	}
	CheckForDirectionChange();
	//NEEDS TO BE LAST
	myGroundChecker.GetCollider()->SetIsSolid(false);
	myGroundChecker.SetPosition(myPosition);
	myGroundChecker.OnUpdate();
	if (myGrabbableChecker.GetHit() != nullptr && myGrabbableChecker.GetHit()->GetTag() == ColliderTag::Grabbable && myHeldObject == nullptr)
	{
		if (InputWrapper::IsButtonDown(KeyButton::Pickup) && myCanPickUp == true)
		{
			PickUp(dynamic_cast<GrabbableObject*>(myGrabbableChecker.GetHit()->GetOwner()));
		}
	}
	myGrabbableChecker.GetCollider()->SetIsSolid(false);
	myGrabbableChecker.SetPosition(myPosition);
	myGrabbableChecker.OnUpdate();
}

void Player::UpdateHeldObject() 
{
	if (myHeldObject == nullptr) 
	{
		myTimerBetweenPickup.Update(CommonUtilities::Timer::GetDeltaTime());
	}
	if(myHeldObject != nullptr)
	{
		myHeldObject->SetVelocity(CommonUtilities::Vector2f(0.0f, 0.0f));
		
		myHeldObject->SetPosition({ myPosition.x, myPosition.y - 0.09f });

		if (InputWrapper::IsButtonPressed(KeyButton::Pickup) || InputWrapper::IsButtonPressed(KeyButton::Hook))
		{
			ThrowHeldObject();
		}
		myThrowIndicator.SetLayer(myDrawable->GetLayer());
		if (myLookDir == LookDirection::Left)
		{
			myThrowIndicator.SetPosition({ myPosition.x - ((myThrowIndicator.GetImagesSizePixel().x * TILE_MULTIPLIER / 1024.f) / 4.0f), myPosition.y - 0.09f });
			myThrowIndicator.SetInversedX(true);
		}
		else
		{
			myThrowIndicator.SetPosition({ myPosition.x + ((myThrowIndicator.GetImagesSizePixel().x * TILE_MULTIPLIER / 1024.f) / 4.0f), myPosition.y - 0.09f });
			myThrowIndicator.SetInversedX(false);
		}
	}
}

void Player::SetLevelIndex(int aIndex)
{
	myCurrentLevelIndex = aIndex;
}

void Player::FillCollisionBuffer(CommonUtilities::GrowingArray<GameObject*>& aBufferToAddTo)
{
	GameObject::FillCollisionBuffer(aBufferToAddTo);
	aBufferToAddTo.Add(&myGroundChecker);
	aBufferToAddTo.Add(&myGrabbableChecker);
}

void Player::OnCollisionEnter(Collider & other)
{
	if (myPlayerState != PlayerStates::Death) 
	{
		if (other.GetTag() == ColliderTag::Trap)
		{
			Camera::GetInstance().Shake(0.2f);
			Die();
		}
		else if (other.GetTag() == ColliderTag::Door)
		{
			myHook.ReleaseHook();
		}
		else if (myHook.GetState() == HookState::Anchored && other.GetIsSolid() && other.GetTag() == ColliderTag::Tile)
		{
			Vector2f collisionNormal = other.GetOwner()->GetPosition() - myPosition;
			collisionNormal.Normalize();
			float dot = Vector2f({ 0, 1 }).Dot(collisionNormal);
			if (acosf(dot) < 0.35f)
			{
				myHook.ReleaseHook();
				Jump(.25f, true);
				SwapActiveAnimation(AnimationState::bsFallRun);
			}
		}
		myPhysX.OnIntersection(other);
	}
}

void Player::PlayClimbingSound() 
{
	float myClimbingMaxTimer = 0.65f;

	myClimbingTimer += Timer::GetDeltaTime();

	if (myClimbingTimer > myClimbingMaxTimer) 
	{
		if (myCurrentLevelIndex == 2) 
		{
			SoundEngine::PlaySound(*mySounds[PlayerSounds::ClimbingVine]);
		}
		else 
		{
			SoundEngine::PlaySound(*mySounds[PlayerSounds::ClimbingMetal]);
		}
		myClimbingTimer = 0.0f;
	}
}

void Player::OnCollisionStay(Collider & other)
{
	if (myPlayerState != PlayerStates::Death)
	{
		if (other.GetTag() == ColliderTag::Collectible)
		{
			Collectible* collectible = dynamic_cast<Collectible*>(other.GetOwner());
			if (collectible->GetState() == ColState::Free)
			{
				PickUpCollectible(collectible);
			}
			else
			{
				myHasPickedUpCollectible = false;
			}
		}

		if (other.GetTag() == ColliderTag::Climbable && myHeldObject == nullptr)
		{
			Vector2f climbingVelocity = { 0.f, 0.f };
			if (InputWrapper::IsButtonDown(KeyButton::Up))
			{
				myPlayerState = PlayerStates::ClimbingMetal;
				climbingVelocity.y -= myClimbingSpeed;
				climbingVelocity.x = 0;

				Ladder* tempLadder = dynamic_cast<Ladder*>(other.GetOwner());
				if (tempLadder->GetSegmentType() == LadderSegment::Top && tempLadder->GetPosition().y - myPosition.y > 0.11f)
				{
					myPlayerState = PlayerStates::Idle;
					Jump(0.1f, false);
				}
				else 
				{
					PlayClimbingSound();
				}
			}
			if (InputWrapper::IsButtonDown(KeyButton::Down))
			{
				myPlayerState = PlayerStates::ClimbingMetal;
				climbingVelocity.y += myClimbingSpeed;
				climbingVelocity.x = 0;

				Ladder* tempLadder = dynamic_cast<Ladder*>(other.GetOwner());
				if (tempLadder->GetSegmentType() == LadderSegment::Bottom && tempLadder->GetPosition().y - myPosition.y < 0.01f)
				{
					myPlayerState = PlayerStates::Idle;
				}
				else
				{
					PlayClimbingSound();
				}
			}
			if (!InputWrapper::IsButtonDown(KeyButton::Down) && !InputWrapper::IsButtonDown(KeyButton::Up) && myPlayerState == PlayerStates::ClimbingMetal)
			{
				myPhysX.SetVelocity({ 0.f, 0.f });
				myAnimations[static_cast<int>(myCurrentAnimationState)].Pause();
			}
			else
			{
				myAnimations[static_cast<int>(myCurrentAnimationState)].Unpause();
			}

			if (myPlayerState == PlayerStates::ClimbingMetal)
			{
				myPhysX.SetVelocity(climbingVelocity);
				myPosition.x = other.GetOwner()->GetPosition().x;

				Ladder* tempLadder = dynamic_cast<Ladder*>(other.GetOwner());
				if (tempLadder->GetSegmentType() == LadderSegment::Bottom)
				{
					myPosition.y = Tga2D::Clamp(myPosition.y, myPosition.y, tempLadder->GetPosition().y);
				}
			}
		}

		if (myPhysX.GetCollidingLeft() && !myCanSideJumpLeftSide)
		{
			myCanSideJumpLeftSide = true;
			myDoubleJumpCooldown.SetTimer(0.2f, [&] { myCanSideJumpLeftSide = false; });
		}

		if (myPlayerState != PlayerStates::ClimbingMetal)
		{
			myPhysX.OnIntersection(other);
		}
	}
}

void Player::OnCollisionLeave(Collider & other)
{
}

void Player::SetRespawnPosition(const CommonUtilities::Vector2f & aRespawnPosition)
{
	myRespawnPosition = aRespawnPosition;
}

void Player::LockCollectibles()
{
	for (int i = 0; i < myPickedUpCollectibles.size(); i++)
	{
		myPickedUpCollectibles[i]->SetState(ColState::Locked);
	}
}

void Player::LoseCollectibles()
{
	for (int i = myPickedUpCollectibles.size() - 1; i >= 0; --i)
	{
		if (myPickedUpCollectibles.size() > 0)
		{
			if (myPickedUpCollectibles[i]->GetState() == ColState::PickedUp)
			{
				myPickedUpCollectibles[i]->SetState(ColState::Free);
				myPickedUpCollectibles.erase(myPickedUpCollectibles.begin() + i);
			}
		}
		else 
		{
			break;
		}
	}

}

void Player::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*> &aRenderBuffer)
{
	myAnimations[static_cast<unsigned short>(myAnimationBlender.GetActiveState())].SetPosition(myPosition);
	aRenderBuffer.Add(&myAnimations[static_cast<unsigned short>(myAnimationBlender.GetActiveState())]);
	if (myHasHook && myPlayerState != PlayerStates::ClimbingMetal && !DialogueManager::IsPlaying())
	{
		aRenderBuffer.Add(&myAimSprite);
	}
	if (myHeldObject != nullptr)
	{
		aRenderBuffer.Add(&myThrowIndicator);
	}
	aRenderBuffer.Add(&myDustPMLeft);
	aRenderBuffer.Add(&myDustPMRight);
}

void Player::SetInputManager(CommonUtilities::InputManager & aInputManager)
{
	myInputManager = &aInputManager;
}

void Player::SetData()
{
	myPhysX.Init(*myCollider);

	json playerData = LoadPlayerData();
	myMoveSpeed = playerData["maxSpeed"];;
	myJumpHeight = playerData["jumpHeight"];
	mySecondJumpHeight = playerData["secondJumpHeight"];
	myJumpAcceleration = playerData["moveAccelerationInAir"];
	myWallJumpHeight = playerData["wallJumpHeight"];
	myWallJumpLength = playerData["wallJumpLength"];
	myClimbingSpeed = playerData["climbingSpeed"];

	myPhysX.SetWeight(playerData["weight"]);
	myPhysX.SetBouncyness(playerData["bouncyness"]);
	myPhysX.SetFriction(playerData["friction"]);
	playerData.clear();

	myPhysX.IgnoreTag(ColliderTag::Climbable);
}

void Player::Die()
{
	if (myPlayerState == PlayerStates::Death)
	{
		return; 
	}
	if (myHeldObject != nullptr)
	{
		if (myLookDir == LookDirection::Right)
		{
			myHeldObject->ThrowRight(0.3f);
		}
		else
		{
			myHeldObject->ThrowLeft(0.3f);
		}
		myHeldObject = nullptr;
		myCanPickUp = true;
	}
	myPhysX.SetVelocity({ 0.0f, 0.0f });
	myPreventInput = true;

	if (myPickedUpHookInThisRoom)
	{
		myHasHook = false;
	}

	SoundEngine::PlaySound(*mySounds[PlayerSounds::Death]);
	SetState(PlayerStates::Death);
	// PLAY DEATH ANIMATION
	Fader::FadeIn(FadeColor::Red);

	Message newMessage;
	newMessage.myMessageType = MessageType::PlayerDeath;
	PostMaster::SendMessages(newMessage);
}

void Player::RespawnAtLatestCheckPoint()
{
	if (myHeldObject != nullptr)
	{
		if (myLookDir == LookDirection::Right)
		{
			myHeldObject->ThrowRight(0.3f);
		}
		else
		{
			myHeldObject->ThrowLeft(0.3f);
		}
		myHeldObject = nullptr;
		myCanPickUp = true;
	}
	myPhysX.SetVelocity({ 0.0f, 0.0f });
	myPreventInput = true;
	myPosition = { myRespawnPosition.x, myRespawnPosition.y + (TILE_SIZE / 2.0f * TILE_MULTIPLIER) / 1080.0f };
	SwapActiveAnimation(AnimationState::Respawn);
	LoseCollectibles();
	myHook.ReleaseHook();
}

Hook & Player::GetHook()
{
	return myHook;
}

void Player::SwapActiveAnimation(AnimationState aNewStateToSet)
{
	if (mySwappedAnimThisFrame)
	{
		return;
	}
	if (myCurrentAnimationState != aNewStateToSet)
	{
		myAnimations[static_cast<unsigned short>(myCurrentAnimationState)].SetCurrentFrame(0);
		myAnimations[static_cast<unsigned short>(myAnimationBlender.GetActiveState())].SetCurrentFrame(0);
		myCurrentAnimationState = aNewStateToSet;
		mySwappedAnimThisFrame = true;
	}
}

void Player::SetLookDirection(LookDirection aDirToSet)
{
	if (myLookDir != aDirToSet)
	{
		if (aDirToSet == LookDirection::Left)
		{
			myGrabbableChecker.GetCollider()->SetOffset(CommonUtilities::Vector2f{ -myGrabDistance, -myGrabHeight / 2.0f });
			for (unsigned short i = 0; i < myAnimations.Size(); i++)
			{
				myAnimations[i].SetInversedX(true);
			}
		}
		else
		{
			myGrabbableChecker.GetCollider()->SetOffset(CommonUtilities::Vector2f{ myGrabDistance, -myGrabHeight / 2.0f });
			for (unsigned short i = 0; i < myAnimations.Size(); i++)
			{
				myAnimations[i].SetInversedX(false);
			}
		}
		myLookDir = aDirToSet;
	}
}

const LookDirection Player::GetLookDirection()
{
	return myLookDir;
}

void Player::PlayWalkingSound() 
{
	if (mySoundTimer > myMaxSoundTimer) 
	{
		//SoundEngine::PlaySound(*mySounds[PlayerSounds::Walk]);
		mySoundTimer = 0.f;
	}
}

void Player::WalkOffLevelCompleted(float aDirection, const CommonUtilities::Vector2f& aDoorPosition)
{
	myPreventInput = true;
	myPhysX.SetVelocity({ myMoveSpeed * aDirection, 0.f });
	SwapActiveAnimation(AnimationState::Run);

	float distanceToDoor;

	float playerPos;
	float doorPos;

	if (aDirection > 0.f)
	{
		SetLookDirection(LookDirection::Right);
		playerPos = myPosition.x + myAnimations[static_cast<unsigned short>(myCurrentAnimationState)].GetImagesSizePixel().x / 1920.f * 3.f;
		distanceToDoor = aDoorPosition.x - playerPos;
	}
	else
	{
		SetLookDirection(LookDirection::Left);
		playerPos = myPosition.x - myAnimations[static_cast<unsigned short>(myCurrentAnimationState)].GetImagesSizePixel().x / 1920.f * 3.f;
		distanceToDoor = playerPos - aDoorPosition.x;
	}

	float movePercentage = 1.0f / distanceToDoor;

	Fader::FadeIn(FadeColor::Black, movePercentage * myMoveSpeed);

}

void Player::EnterNewRoom(const CommonUtilities::Vector2f& aDirection, const CommonUtilities::Vector2f& aSpawnPos)
{

	myPreventInput = true;
	myPhysX.SetVelocity({ myMoveSpeed * aDirection.x, myMoveSpeed * aDirection.y });

	float playerPos;
	float spawnPos;

	float distanceToSpawn;
	if (aDirection.x != 0.0f)
	{
		SwapActiveAnimation(AnimationState::Run);
		if (aDirection.x > 0.0f)
		{
			SetLookDirection(LookDirection::Right);
			playerPos = myPosition.x + myAnimations[static_cast<unsigned short>(myCurrentAnimationState)].GetImagesSizePixel().x / 1920.f * 3.f;
			distanceToSpawn = aSpawnPos.x - playerPos;
		}
		else
		{
			SetLookDirection(LookDirection::Left);
			playerPos = myPosition.x - myAnimations[static_cast<unsigned short>(myCurrentAnimationState)].GetImagesSizePixel().x / 1920.f * 3.f;
			distanceToSpawn = playerPos - aSpawnPos.x;
		}
	}
	else if (aDirection.y != 0.0f)
	{
		//STUFF FOR STAIRS
	}


	myPreventInput = false;
}

void Player::SetAnimationStateToIdle()
{
	myCurrentAnimationState = AnimationState::Idle;
	myPhysX.SetVelocity({ 0.0f, 0.0f });
}

GroundChecker & Player::GetGroundChecker()
{
	return myGroundChecker;
}

void Player::OnRoomEnter()
{
	ThrowHeldObject();
	myPickedUpHookInThisRoom = false;
}

void Player::Reset(Player* aPlayerPointer)
{
	RespawnAtLatestCheckPoint();
	SoundEngine::PlaySound(*mySounds[PlayerSounds::Respawn]);
	SetPreventInput(false);
	if (myPickedUpHookInThisRoom)
	{
		mySwappedSprites = false;
		myHasHook = false;
	}
}

void Player::ReplaceAnimationsToNoHook()
{
	for (int i = 0; i < static_cast<int>(AnimationState::SizeOfEnum); i++)
	{
		auto it = myNHSprites.find(static_cast<AnimationState>(i));
		if (it != myNHSprites.end())
		{
			myAnimations[static_cast<unsigned short>(it->first)].ReplaceSprite(it->second);
		}
	}
}

void Player::ReplaceAnimationsToHook()
{
	for (int i = 0; i < static_cast<int>(AnimationState::SizeOfEnum); i++)
	{
		auto it = myHSprites.find(static_cast<AnimationState>(i));
		if (it != myHSprites.end())
		{
			myAnimations[static_cast<unsigned short>(it->first)].ReplaceSprite(it->second);
		}
	}
}

void Player::CheckForDirectionChange()
{
	if (myGroundChecker.OnGround())
	{
		myDustPMLeft.SetLayer(2);
		myDustPMRight.SetLayer(2);
		if (myLookDir == LookDirection::Left && myPrevLookDir == LookDirection::Right)
		{
			myDustPMRight.SetAngle(0.0f, -90.0f);
			myDustPMRight.SetEmitDuration(0.1f);
			myCurrentDustPosition = { myPosition.x + 0.01f, myPosition.y + 0.05f };
		}
		else if (myLookDir == LookDirection::Right && myPrevLookDir == LookDirection::Left)
		{
			myDustPMLeft.SetAngle(180.0f, 270.0f);
			myDustPMLeft.SetEmitDuration(0.1f);
			myCurrentDustPosition = { myPosition.x - 0.01f, myPosition.y + 0.05f };
		}
	}
	myDustPMLeft.SetPosition({ myCurrentDustPosition.x, myCurrentDustPosition.y });
	myDustPMRight.SetPosition({ myCurrentDustPosition.x, myCurrentDustPosition.y });
	myPrevLookDir = myLookDir;
}

void Player::UpdateMovement()
{
	if (myPreventInput)
	{
		//myPhysX.SetVelocity({ 0.0f, 0.0f });
		return;
	}

	if (myGroundChecker.OnGround())
	{
		mySoundTimer += Timer::GetDeltaTime();

		if (InputWrapper::IsButtonDown(KeyButton::Left) == false && InputWrapper::IsButtonDown(KeyButton::Right) == false)
		{
			if (myPhysX.GetVelocity().y >= 0.0f && myPlayerState != PlayerStates::ClimbingMetal)//prevents multiple idle sets
			{
				if(myHeldObject == nullptr)
				{
					SwapActiveAnimation(AnimationState::Idle);
					mySoundTimer = 0.f;
				}
				else
				{
					SwapActiveAnimation(AnimationState::PickupIdle);
				}
			}
		}
		myCanSecondJump = true;
		myPhysX.SetVelocity(CommonUtilities::Vector2f{ 0.0f, myPhysX.GetVelocity().y });

		//Reaaaally???
		//if (InputWrapper::IsButtonDown(KeyButton::Down))
		//{
		//	SwapActiveAnimation(AnimationState::Headbang);
		//}
	}
	if (InputWrapper::IsButtonDown(KeyButton::Left) && myPlayerState != PlayerStates::ClimbingMetal)
	{
		if (myHeldObject != nullptr && myHeldObject->GetPhysx().GetCollidingLeft() == true )
		{
			myPhysX.SetVelocity(CommonUtilities::Vector2f{ 0.0f, myPhysX.GetVelocity().y });
			return;
		}
		if (myGroundChecker.OnGround())
		{
			PlayWalkingSound();

			if (myPhysX.GetVelocity().y >= 0.0f && myPlayerState != PlayerStates::ClimbingMetal)//prevents multiple idle sets
			{
				if (myHeldObject == nullptr)
				{
					SwapActiveAnimation(AnimationState::Run);
				}
				else
				{
					SwapActiveAnimation(AnimationState::PickupRun);
				}
			}
			myPhysX.SetVelocity( CommonUtilities::Vector2f{-myMoveSpeed, myPhysX.GetVelocity().y });
		}
		else
		{
			if (myPhysX.GetVelocity().x - myJumpAcceleration < -myMoveSpeed)
			{
				myPhysX.SetVelocity(CommonUtilities::Vector2f{ -myMoveSpeed, myPhysX.GetVelocity().y });
			}
			else
			{
				if (myPhysX.GetVelocity().x > 0.0f)
				{
					myPhysX.SetVelocity(CommonUtilities::Vector2f{ 0.0f, myPhysX.GetVelocity().y });
				}
				myPhysX.SetVelocity(CommonUtilities::Vector2f{ myPhysX.GetVelocity().x - myJumpAcceleration * Timer::GetDeltaTime(), myPhysX.GetVelocity().y });
			}
		}
		SetLookDirection(LookDirection::Left);
	}
	else
	{
		if (!myGroundChecker.OnGround() && myPhysX.GetVelocity().x < 0.0f)
		{
			myPhysX.SetVelocity(CommonUtilities::Vector2f{ myPhysX.GetVelocity().x + myJumpAcceleration * Timer::GetDeltaTime(), myPhysX.GetVelocity().y });
		}
	}
	if (InputWrapper::IsButtonDown(KeyButton::Right) && myPlayerState != PlayerStates::ClimbingMetal)
	{
		if (myHeldObject != nullptr && myHeldObject->GetPhysx().GetCollidingRight() == true)
		{
			myPhysX.SetVelocity(CommonUtilities::Vector2f{ 0.0f, myPhysX.GetVelocity().y });
			return;
		}
		if (myGroundChecker.OnGround())
		{
			PlayWalkingSound();
			if (myPhysX.GetVelocity().y >= 0.0f && myPlayerState != PlayerStates::ClimbingMetal)//prevents multiple idle sets
			{
				if (myHeldObject == nullptr)
				{
					SwapActiveAnimation(AnimationState::Run);
				}
				else
				{
					SwapActiveAnimation(AnimationState::PickupRun);
				}
			}
			myPhysX.SetVelocity(CommonUtilities::Vector2f{ myMoveSpeed, myPhysX.GetVelocity().y });
		}
		else
		{
			if (myPhysX.GetVelocity().x + myJumpAcceleration > myMoveSpeed)
			{
				myPhysX.SetVelocity(CommonUtilities::Vector2f{ myMoveSpeed, myPhysX.GetVelocity().y });
			}
			else
			{
				if (myPhysX.GetVelocity().x < 0.0f)
				{
					myPhysX.SetVelocity(CommonUtilities::Vector2f{ 0.0f, myPhysX.GetVelocity().y });
				}
				myPhysX.SetVelocity(CommonUtilities::Vector2f{ myPhysX.GetVelocity().x + myJumpAcceleration * Timer::GetDeltaTime(), myPhysX.GetVelocity().y });
			}
		}
		SetLookDirection(LookDirection::Right);
	}
	else
	{
		if (!myGroundChecker.OnGround() && myPhysX.GetVelocity().x > 0.0f)
		{
			myPhysX.SetVelocity(CommonUtilities::Vector2f{ myPhysX.GetVelocity().x - myJumpAcceleration* Timer::GetDeltaTime(), myPhysX.GetVelocity().y });
		}
	}
	if (InputWrapper::IsButtonPressed(KeyButton::Jump) && myPlayerState != PlayerStates::ClimbingMetal)
	{
		if (myHeldObject == nullptr)
		{
			if (myHook.GetState() != HookState::HoldPlayerToWall)
			{
				if (myGroundChecker.OnGround())
				{
					Jump(myJumpHeight, true);
					if (myPhysX.GetVelocity().x == 0.0f)
					{
						SwapActiveAnimation(AnimationState::IdleJump);
					}
					else
					{
						SwapActiveAnimation(AnimationState::RunJump);
					}
					myCanSecondJump = true;
				}
				else if (!myGroundChecker.OnGround() && myCanSecondJump)
				{
					Jump(mySecondJumpHeight, false);

					SwapActiveAnimation(AnimationState::DoubleJump);
					SoundEngine::PlaySound(*mySounds[PlayerSounds::Flip]);

					myCanSecondJump = false;
				}
			}
			else
			{
				if (myHook.GetPullDirection() == PullPlayerDirection::Left)
				{
					CommonUtilities::Vector2f jumpDir = CommonUtilities::Vector2f({ 1.0f * myWallJumpLength, -1.0f * myWallJumpHeight });
					myPhysX.SetVelocity(jumpDir);
					SetLookDirection(LookDirection::Right);
					myCanSecondJump = true;
				}
				else if (myHook.GetPullDirection() == PullPlayerDirection::Right)
				{
					CommonUtilities::Vector2f jumpDir = CommonUtilities::Vector2f({ -1.0f * myWallJumpLength , -1.0f * myWallJumpHeight });
					myPhysX.SetVelocity(jumpDir);
					SetLookDirection(LookDirection::Left);
					myCanSecondJump = true;
				}
				myHook.ReleaseHook();
				SwapActiveAnimation(AnimationState::WallJump);
				SoundEngine::PlaySound(*mySounds[PlayerSounds::Walljump]);
			}
		}
	}
	if (myPlayerState == PlayerStates::ClimbingMetal)
	{
		SwapActiveAnimation(AnimationState::ClimbingMetal);
		if (InputWrapper::IsButtonDown(KeyButton::Up) || InputWrapper::IsButtonDown(KeyButton::Down))
		{
			myAnimationBlender.Unpause();
		}
		else
		{
			myPhysX.SetVelocity({ 0.f, 0.f });
			myAnimationBlender.Pause();
		}
		myPosition += myPhysX.GetVelocity() * Timer::GetDeltaTime();
	}
	if (myPhysX.GetVelocity().y > 0.36f && myGroundChecker.OnGround() == false && myPlayerState != PlayerStates::ClimbingMetal && myHook.GetState() != HookState::HoldPlayerToWall)
	{
		SwapActiveAnimation(AnimationState::Fall);
	}
}

void Player::ThrowHeldObject()
{
	if (myHeldObject != nullptr)
	{
		float force = 0.3f;
		if (InputWrapper::IsButtonDown(KeyButton::Down))
		{
			force = 0.15f;
		}
		SoundEngine::PlaySound(*mySounds[PlayerSounds::Throw]);
		if (myLookDir == LookDirection::Left)
		{
			myHeldObject->ThrowLeft(force);
		}
		else
		{
			myHeldObject->ThrowRight(force);
		}
		myHeldObject->RemoveIgnoreTag(ColliderTag::Player);
		myHeldObject = nullptr;
		myTimerBetweenPickup.Reset();
		myTimerBetweenPickup.Start();
	}
}

void Player::OnMovingTileChecker()
{
	if (myPlayerState != PlayerStates::Death)
	{
		if (myGroundChecker.GetHit() != nullptr && myGroundChecker.GetHit()->GetTag() == ColliderTag::MovingTile)
		{
			MovingTile *movingTileHit = static_cast<MovingTile*>(myGroundChecker.GetHit()->GetOwner());
			myPosition.x += movingTileHit->GetMovementThisFrame().x;
			myPosition.y += movingTileHit->GetMovementThisFrame().y;
		}
	}
}

void Player::PickUpCollectible(Collectible* aCollectible)
{
	myHasPickedUpCollectible = true;
	aCollectible->SetState(ColState::PickedUp);
	aCollectible->ResetEmitter();
	myPickedUpCollectibles.push_back(aCollectible);
	SoundEngine::PlaySound(*mySounds[PlayerSounds::CollectiblePickup]);
}



void Player::InitGrabChecker()
{
	myGrabDistance = 0.03f;
	myGrabHeight = 0.08f;
	Sprite *nullSprite = new Sprite();
	nullSprite->Init("sprites/noObj.dds");
	myGrabbableChecker.InitGO({ myPosition.x, myPosition.y }, "sprites/noObj.dds", nullSprite);
	LineCollider myGrabCheckCol;
	myGrabCheckCol.Init(CommonUtilities::Vector2f{ 0.0f, 1.0f }, myGrabHeight, CommonUtilities::Vector2f{ myGrabDistance, -myGrabHeight / 2.0f });
	myGrabCheckCol.SetTag(ColliderTag::GroundChecker);
	myGrabbableChecker.AttachCollider(myGrabCheckCol);
}

void Player::HandleSwinging()
{
	if (InputWrapper::IsButtonPressed(KeyButton::Jump))
	{
		myHook.ReleaseHook();
		Jump(mySecondJumpHeight, false);
		SoundEngine::PlaySound(*mySounds[PlayerSounds::Jump]);
		SwapActiveAnimation(AnimationState::DoubleJump);
	}
	float dt = Timer::GetDeltaTime();
	Vector2f nextFramePosition = myPosition;
	Vector2f startPosition = myPosition;

	float velYDelta = myPhysX.GetGravity() * myPhysX.GetWeight() * dt;
	myPhysX.SetVelocity({ myPhysX.GetVelocity().x, myPhysX.GetVelocity().y + velYDelta });
	nextFramePosition += myPhysX.GetVelocity() * dt;

	Vector2f distanceToAnchor = nextFramePosition - myHook.GetAnchor()->GetPosition();

	myRotation += (-atan2f(distanceToAnchor.x, distanceToAnchor.y) - myRotation) * 10.f * Timer::GetDeltaTime();

	float distanceDifference = distanceToAnchor.Length() - myHook.GetLength();
	if (distanceDifference > 0)
	{
		if (InputWrapper::IsButtonDown(KeyButton::Up) && !(InputWrapper::IsButtonDown(KeyButton::Right) || InputWrapper::IsButtonDown(KeyButton::Left)))
		{
			myHook.Extend(-.2f);
		}
		if (InputWrapper::IsButtonDown(KeyButton::Down) && !(InputWrapper::IsButtonDown(KeyButton::Right) || InputWrapper::IsButtonDown(KeyButton::Left)))
		{
			myHook.Extend(.2f);
		}

		myPosition = myHook.GetAnchor()->GetPosition() + distanceToAnchor.GetNormalized() * myHook.GetLength();
		Vector2f newVelocity = (myPosition - startPosition) / dt;
		if (InputWrapper::IsButtonDown(KeyButton::Left))
		{
			newVelocity.x -= 0.2f * dt;
			
		}
		if (InputWrapper::IsButtonDown(KeyButton::Right))
		{
			newVelocity.x += 0.2f * dt;
		}

		if (!myHook.JustAnchored())
			myPhysX.SetVelocity(newVelocity);
		else
			SwapActiveAnimation(AnimationState::Swinging);

		float angle = atan2f(distanceToAnchor.x, distanceToAnchor.y);
		angle = 4 + 1.57f + 10.f * angle;
		angle = Tga2D::Clamp(angle, 0.f, 11.f);
		if (myLookDir == LookDirection::Left)
		{
			angle = 11.f - angle;
		}
		myAnimations[static_cast<unsigned short>(myCurrentAnimationState)].SetCurrentFrame(static_cast<int>(angle));
	}
	else
	{
		myPhysX.SetVelocity({ myPhysX.GetVelocity().x, myPhysX.GetVelocity().y + velYDelta });
		myPosition += myPhysX.GetVelocity() * dt;
	}
	myPhysX.Update(*myCollider, Vector2f{ 0,0 });

	if ((myHand.GetPosition().y - myHook.GetAnchor()->GetPosition().y) > 0.f)
	{
		if (myPhysX.GetVelocity().x < 0)
		{
			SetLookDirection(LookDirection::Left);
		}
		else
		{
			SetLookDirection(LookDirection::Right);
		}
	}
}

void Player::Jump(const float aHeight, const bool aShouldPlaySound, const bool aAllowDoubleJump)
{
	if (!aAllowDoubleJump)
	{
		myPhysX.SetVelocity(CommonUtilities::Vector2f{ myPhysX.GetVelocity().x , myPhysX.GetVelocity().y - aHeight });
	}
	else
	{
		myPhysX.SetVelocity(CommonUtilities::Vector2f{ myPhysX.GetVelocity().x , -aHeight });
	}
	
	if (aShouldPlaySound)
	{
		SoundEngine::PlaySound(*mySounds[PlayerSounds::Jump]);
	}
	
	if (!aAllowDoubleJump)
	{
		myCanSecondJump = false;
	}
}

void Player::PickUp(GrabbableObject* aObject)
{
	SoundEngine::PlaySound(*mySounds[PlayerSounds::PickUp]);
	myHeldObject = aObject;
	myHeldObject->SetPhysxKinematic(true);
	myHeldObject->AddIgnoreTag(ColliderTag::Player);
	myCanPickUp = false;
}

void Player::ActivateHook()
{
	mySwappedSprites = false;
	myHasHook = true;
	myPickedUpHookInThisRoom = true;
}

void Player::SetHasHook(bool aHasHook)
{
	myHasHook = aHasHook;
}

bool Player::HasHook()
{
	return myHasHook;
}

void Player::SetShouldRespawnWithHook(const bool aBool)
{
	myPickedUpHookInThisRoom = false;
}

void Player::SetState(PlayerStates aPlayerState)
{
	myPlayerState = aPlayerState;
}

int Player::GetCurrentNumberOfCollectibles()
{
	return static_cast<int>(myPickedUpCollectibles.size());
}

bool Player::HasPickedUpCollectible()
{
	if (myHasPickedUpCollectible) 
	{
		myHasPickedUpCollectible = false;
		return true;
	}

	return false;
}

void Player::InitAnimations()
{
	myAnimations.Init(static_cast<unsigned short>(AnimationState::SizeOfEnum));
	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerIdle");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::Idle);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerRun");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::Run);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerLadderClimb");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::ClimbingMetal);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerIdleJump");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::IdleJump);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerRunJump");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::RunJump);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerFall");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::Fall);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerDoubleJump");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::DoubleJump);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerHookDiagonal");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::HookDiagonal);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerHookHorizontal");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::HookHorizontal);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerHookVertical");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::HookVertical);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerPickupIdle");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::PickupIdle);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerPickupWalk");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::PickupRun);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerWallJump");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::WallJump);

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerSwing");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::Swinging);
	
	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerBSIdleRun");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddBlendState(AnimationState::Idle, AnimationState::Run, AnimationState::bsIdleRun, myAnimations.GetLast().GetTotalTime());

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerBSIdlePickupIdle");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddBlendState(AnimationState::Idle, AnimationState::PickupIdle, AnimationState::bsIdlePickupIdle, myAnimations.GetLast().GetTotalTime());
	myAnimationBlender.AddBlendState(AnimationState::Idle, AnimationState::PickupRun, AnimationState::bsIdlePickupIdle, myAnimations.GetLast().GetTotalTime());
	myAnimationBlender.AddBlendState(AnimationState::Run, AnimationState::PickupRun, AnimationState::bsIdlePickupIdle, myAnimations.GetLast().GetTotalTime());
	myAnimationBlender.AddBlendState(AnimationState::Run, AnimationState::PickupIdle, AnimationState::bsIdlePickupIdle, myAnimations.GetLast().GetTotalTime());

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerBSFallIdle");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddBlendState(AnimationState::Fall, AnimationState::Idle, AnimationState::bsFallIdle, myAnimations.GetLast().GetTotalTime());

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerBSFallRun");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddBlendState(AnimationState::Fall, AnimationState::Run, AnimationState::bsFallRun, myAnimations.GetLast().GetTotalTime());

	myAnimations.Add(Animation());
	myAnimations.GetLast().Init("playerRespawn01");
	myAnimations.GetLast().SetLayer(10);
	myAnimationBlender.AddState(AnimationState::Respawn);

	myNHSprites.insert(std::make_pair(AnimationState::bsFallIdle, "sprites/player/bsFallIdleNH.dds"));
	myNHSprites.insert(std::make_pair(AnimationState::bsFallRun, "sprites/player/bsFallRunNH.dds"));
	myNHSprites.insert(std::make_pair(AnimationState::bsIdlePickupIdle, "sprites/player/bsIdlePickupIdleNH.dds"));
	myNHSprites.insert(std::make_pair(AnimationState::bsIdleRun, "sprites/player/bsIdleRunNH.dds"));
	myNHSprites.insert(std::make_pair(AnimationState::DoubleJump, "sprites/player/doubleJumpNH.dds"));
	myNHSprites.insert(std::make_pair(AnimationState::Fall, "sprites/player/fallNH.dds"));
	myNHSprites.insert(std::make_pair(AnimationState::IdleJump, "sprites/player/idleJumpNH.dds"));
	myNHSprites.insert(std::make_pair(AnimationState::Idle, "sprites/player/idleNH.dds"));
	myNHSprites.insert(std::make_pair(AnimationState::PickupIdle, "sprites/player/pickup_idleNH.dds"));
	myNHSprites.insert(std::make_pair(AnimationState::PickupRun, "sprites/player/pickup_walkNH.dds"));
	myNHSprites.insert(std::make_pair(AnimationState::PickupRun, "sprites/player/pickup_walkNH.dds"));
	myNHSprites.insert(std::make_pair(AnimationState::RunJump, "sprites/player/runJumpNH.dds"));
	myNHSprites.insert(std::make_pair(AnimationState::Run, "sprites/player/runNH.dds"));

	myHSprites.insert(std::make_pair(AnimationState::bsFallIdle, "sprites/player/bsFallIdle.dds"));
	myHSprites.insert(std::make_pair(AnimationState::bsFallRun, "sprites/player/bsFallRun.dds"));
	myHSprites.insert(std::make_pair(AnimationState::bsIdlePickupIdle, "sprites/player/bsIdlePickupIdle.dds"));
	myHSprites.insert(std::make_pair(AnimationState::bsIdleRun, "sprites/player/bsIdleRun.dds"));
	myHSprites.insert(std::make_pair(AnimationState::DoubleJump, "sprites/player/doubleJump.dds"));
	myHSprites.insert(std::make_pair(AnimationState::Fall, "sprites/player/fall.dds"));
	myHSprites.insert(std::make_pair(AnimationState::IdleJump, "sprites/player/idleJump.dds"));
	myHSprites.insert(std::make_pair(AnimationState::Idle, "sprites/player/idle.dds"));
	myHSprites.insert(std::make_pair(AnimationState::PickupIdle, "sprites/player/pickup_idle.dds"));
	myHSprites.insert(std::make_pair(AnimationState::PickupRun, "sprites/player/pickup_walk.dds"));
	myHSprites.insert(std::make_pair(AnimationState::PickupRun, "sprites/player/pickup_walk.dds"));
	myHSprites.insert(std::make_pair(AnimationState::RunJump, "sprites/player/runJump.dds"));
	myHSprites.insert(std::make_pair(AnimationState::Run, "sprites/player/run.dds"));

	SwapActiveAnimation(AnimationState::Idle);
}

void Player::UpdateHandPosition()
{
	if (myHook.GetState() == HookState::Anchored)
	{
		myHandOffset = { 0.f, -0.07f };
	}
	else if (myLookDir == LookDirection::Left)
	{
		switch (myAnimationBlender.GetActiveState())
		{
		case AnimationState::HookDiagonal:
			myHandOffset = { -0.031f, -0.05f };
			break;
		case AnimationState::HookHorizontal:
			myHandOffset = { -0.031f, -0.012f };
			break;
		case AnimationState::HookVertical:
			myHandOffset = { -0.007f, -0.062f };
			break;
		case AnimationState::Fall:
			myHandOffset = { 0.011f, -0.062f };
			break;
		}
	}
	else if (myLookDir == LookDirection::Right)
	{
		switch (myAnimationBlender.GetActiveState())
		{
		case AnimationState::HookDiagonal:
			myHandOffset = { 0.031f, -0.05f };
			break;
		case AnimationState::HookHorizontal:
			myHandOffset = { 0.031f, -0.012f };
			break;
		case AnimationState::HookVertical:
			myHandOffset = { 0.007f, -0.062f };
			break;
		case AnimationState::Fall:
			myHandOffset = { -0.011f, -0.062f };
			break;
		}
	}
		myHand.SetPosition(myPosition + myHandOffset);

}

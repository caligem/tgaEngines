#pragma once
#include "GameObject.h"
#include "InputManager.h"
#include "GrabbableObject.h"
#include "GroundChecker.h"
#include "PhysicsComponent.h"
#include "Hook.h"
#include "Cooldown.h"
#include "Animation.h"
#include "ParticleEmitter.h"
#include <map>
#include <cit\stateblender.h>

class Collectible;
class Sound;

enum class PlayerStates
{
	Death,
	Idle,
	ClimbingMetal
};

enum class LookDirection
{
	Left,
	Right
};

enum PlayerSounds
{
	Jump = 0,
	Flip = 1,
	CollectiblePickup = 2,
	Walk = 3,
	Throw = 4,
	PickUp = 5,
	Respawn = 6,
	Death = 7,
	Walljump = 8,
	HookJump = 9,
	ClimbingMetal = 10,
	ClimbingVine = 11
};

class Player : public GameObject
{
public:
	Player();
	~Player() override;

	void OnUpdate() override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer) override;
	void SetInputManager(CommonUtilities::InputManager &aInputManager);

	void SetData();

	void SetLevelIndex(int aIndex);
	void FillCollisionBuffer(CommonUtilities::GrowingArray<GameObject*> &aBufferToAddTo) override;
	void OnCollisionEnter(Collider  &other) override;
	void PlayClimbingSound();
	void OnCollisionStay(Collider  &other) override;
	void OnCollisionLeave(Collider  &other) override;
	void SetRespawnPosition(const CommonUtilities::Vector2f& aRespawnPosition);
	void LockCollectibles();
	void LoseCollectibles();
	void UpdateHeldObject();
	void Die();
	void RespawnAtLatestCheckPoint();
	void Jump(const float aHeight, const bool aShouldPlaySound, const bool aAllowDoubleJump = true);
	void PickUp(GrabbableObject* aObject);
	void ActivateHook();
	void SetHasHook(bool aHasHook);
	bool HasHook();
	void SetShouldRespawnWithHook(const bool aBool);

	void SetState(PlayerStates aPlayerState);

	int GetCurrentNumberOfCollectibles();

	bool HasPickedUpCollectible();

	Hook& GetHook();
	const LookDirection GetLookDirection();
	void PlayWalkingSound();

	void WalkOffLevelCompleted(float aDirection, const CommonUtilities::Vector2f& aDoorPosition);
	void EnterNewRoom(const CommonUtilities::Vector2f& aDirection, const CommonUtilities::Vector2f& aSpawnPos);

	void SetPreventInput(bool aPreventInput) { myPreventInput = aPreventInput; }
	float GetMoveSpeed() { return myMoveSpeed; }
	void SetAnimationStateToIdle();

	GroundChecker &GetGroundChecker();

	void OnRoomEnter();
	void Reset(Player* aPlayerPointer = nullptr) override;
private:
	void ReplaceAnimationsToNoHook();
	void ReplaceAnimationsToHook();
	enum class AnimationState
	{
		Idle,
		Run,
		ClimbingMetal,
		IdleJump,
		RunJump,
		Fall,
		DoubleJump,
		HookDiagonal,
		HookHorizontal,
		HookVertical,
		PickupIdle,
		PickupRun,
		WallJump,
		Swinging,
		bsIdleRun,
		bsIdlePickupIdle,
		bsFallIdle,
		bsFallRun,
		Respawn,
		SizeOfEnum
	};
	void SwapActiveAnimation(AnimationState aNewStateToSet);
	bool mySwappedAnimThisFrame;
	void SetLookDirection(LookDirection aDirToSet);
	LookDirection myLookDir;
	LookDirection myPrevLookDir;
	void CheckForDirectionChange();
	void UpdateMovement();
	void ThrowHeldObject();
	void OnMovingTileChecker();
	void PickUpCollectible(Collectible* aCollectible);


	void InitGrabChecker();

	float myMoveSpeed;
	float myJumpHeight;
	float mySecondJumpHeight;
	bool myCanSecondJump;
	float myJumpAcceleration;
	float myWallJumpHeight;
	float myWallJumpLength;
	float myClimbingSpeed;
	bool myHasHook;
	bool myPickedUpHookInThisRoom;
	CommonUtilities::InputManager *myInputManager;
	PhysicsComponent myPhysX;
	GrabbableObject* myHeldObject;
	cit::countdown myTimerBetweenPickup;
	bool myCanPickUp;
	Hook myHook;
	GameObject myHand;
	CommonUtilities::Vector2f myHandOffset;
	Sprite myAimSprite;

	bool myHasPickedUpCollectible;

	Cooldown myDoubleJumpCooldown;
	PlayerStates myPlayerState;
	bool myCanSideJumpLeftSide;

	CommonUtilities::Vector2f myRespawnPosition;
	void HandleSwinging();

	AnimationState myCurrentAnimationState;
	cit::stateblender<AnimationState> myAnimationBlender;
	CommonUtilities::GrowingArray<Animation> myAnimations;

	std::map<AnimationState, std::string> myNHSprites;
	std::map<AnimationState, std::string> myHSprites;
	bool mySwappedSprites;

	void InitAnimations();
	void UpdateHandPosition();
	GroundChecker myGroundChecker;
	GroundChecker myGrabbableChecker;
	float myGrabDistance;
	float myGrabHeight;
	bool myIsGrounded;

	std::vector<Collectible*> myPickedUpCollectibles;

	std::vector<Sound*> mySounds;

	float mySoundTimer;
	float myMaxSoundTimer;
	float myClimbingTimer;

	bool myIsWalking;

	bool myPreventInput;

	int myCurrentLevelIndex;

	Animation myThrowIndicator;
	ParticleEmitter myDustPMLeft;
	ParticleEmitter myDustPMRight;
	CommonUtilities::Vector2f myCurrentDustPosition;
};


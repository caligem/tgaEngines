#pragma once
#include <vector>
#include "Subscriber.h"


class PostMaster
{
public:
	static void Create();
	static void Destroy();
	static void Subscribe(const MessageType aMessageType, Subscriber* aSubScriber);
	static void SendMessages(const Message aMessage);
	static void UnSubscribe(const MessageType aMessageType, Subscriber* aSubScriber);
private:
	PostMaster();
	~PostMaster();
	static PostMaster *ourInstance;
	std::vector<std::vector<Subscriber*>> mySubscribers;
};


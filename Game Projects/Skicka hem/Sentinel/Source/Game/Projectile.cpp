#include "stdafx.h"
#include "Projectile.h"
#include "Player.h"
#include "Timer.h"
Projectile::Projectile()
{
}


Projectile::~Projectile()
{
}

void Projectile::Init(const CommonUtilities::Vector2f& aDirection, float aSpeed)
{
	myDirection = aDirection;
	mySpeed = aSpeed;
}

void Projectile::SetEndPoints(const CommonUtilities::Vector2f & aMinPoint, const CommonUtilities::Vector2f & aMaxPoint)
{
	myMinEndPoint = aMinPoint;
	myMaxEndPoint = aMaxPoint;
}

void Projectile::OnUpdate()
{
	myPosition += myDirection * mySpeed * CommonUtilities::Timer::GetDeltaTime();
	myDrawable->SetPosition({ myPosition.x, myPosition.y });

	if (IsOutOfScreen())
	{
		myShouldRemove = true;
	}
}

void Projectile::OnCollisionEnter(Collider  &other)
{
	if (other.GetTag() == ColliderTag::Tile || other.GetTag() == ColliderTag::Grabbable)
	{
		myShouldRemove = true;
		//TODO: Particle??
	}
	else if (other.GetTag() == ColliderTag::Player)
	{
		myShouldRemove = true;
		static_cast<Player*>(other.GetOwner())->Die();
	}
}

void Projectile::OnCollisionStay(Collider  &other)
{
	other;
}

void Projectile::OnCollisionLeave(Collider  &other)
{
	other;
}

bool Projectile::IsOutOfScreen()
{
	if (myPosition.x > myMaxEndPoint.x || myPosition.y > myMaxEndPoint.y || myPosition.x < myMinEndPoint.x || myPosition.y < myMinEndPoint.y)
	{
		return true;
	}

	return false;
}

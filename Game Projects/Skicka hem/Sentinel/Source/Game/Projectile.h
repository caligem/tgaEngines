#pragma once
#include "GameObject.h"
class Projectile :
	public GameObject
{
public:
	Projectile();
	~Projectile();

	void Init(const CommonUtilities::Vector2f& aDirection, float aSpeed);
	void SetEndPoints(const CommonUtilities::Vector2f& aMinPoint, const CommonUtilities::Vector2f& aMaxPoint);
	void OnUpdate() override;
	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionStay(Collider  &other) override;
	void OnCollisionLeave(Collider  &other) override;

private:
	CommonUtilities::Vector2f myDirection;
	CommonUtilities::Vector2f myMinEndPoint;
	CommonUtilities::Vector2f myMaxEndPoint;
	float mySpeed;

	bool IsOutOfScreen();
};


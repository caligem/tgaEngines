#include "stdafx.h"
#include "ProjectileManager.h"
#include "Projectile.h"
#include "Room.h"
#include "Animation.h"

ProjectileManager::~ProjectileManager()
{
}

void ProjectileManager::Init(Room * aRoom)
{
	myRoom = aRoom;
}

#include <iostream>
void ProjectileManager::CreateProjectile(const CommonUtilities::Vector2f& aPosition, const CommonUtilities::Vector2f& aDirection, float aSpeed)
{
	Projectile* projectile = new Projectile();
	Drawable* animation = new Animation();
	projectile->InitGO(aPosition/4.f, "Sprites/enemies/turret_bullet.dds", animation);
	projectile->Init(aDirection, aSpeed);
	projectile->SetEndPoints(myRoom->GetMinEdges(), myRoom->GetMaxEdges());
	if (aDirection.x < 0.f)
	{
		static_cast<Animation*>(&projectile->AccessDrawable())->SetInversedX(true);
	}

	BoxCollider projectileCollider;
	projectileCollider.Init(0.015f, 0.015f, { 0.f, 0.f });
	projectileCollider.SetTag(ColliderTag::Projectile);

	projectile->AttachCollider(projectileCollider);

	myRoom->AddGameObjectToBuffer(projectile);
}

#pragma once
#include "Vector2.h"
class Room;
class ProjectileManager
{
public:
	ProjectileManager() = default;
	~ProjectileManager();

	void Init(Room* aRoom);
	void CreateProjectile(const CommonUtilities::Vector2f& aPosition, const CommonUtilities::Vector2f& aDirection, float aSpeed);

private:
	Room* myRoom;
};


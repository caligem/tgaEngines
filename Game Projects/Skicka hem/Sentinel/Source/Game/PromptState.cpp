#include "stdafx.h"
#include "PromptState.h"
#include "InputManager.h"
#include "InputWrapper.h"
#include "Sprite.h"
#include "StateStack.h"
#include "SoundEngine.h"

PromptState::PromptState(CommonUtilities::InputManager* aInputManager)
{
	myInputManager = aInputManager;
	myState = eState::UnClicked;
	mySelectedButton = nullptr;
}

PromptState::~PromptState()
{
	mySound->Annahilate();
	mySelectedButton = nullptr;
}

void PromptState::Init()
{
	myRenderBuffer.Init(64);
	InitButtons();
	InitSprites();

	mySound = new Sound("Sounds/Sfx/button_hovored.wav");
}

void PromptState::OnEnter()
{
	Init();
}

void PromptState::OnExit()
{
	DeleteSprites();
}

eStateStackMessage PromptState::Update()
{
	if (InputWrapper::IsButtonPressed(KeyButton::Left) || InputWrapper::IsButtonPressed(KeyButton::Right))
	{
		SoundEngine::PlaySound(*mySound);
	}

	CleanUp();
	UpdateButtons();
	FillRenderBuffer();

	if (myInputManager->IsKeyPressed(CommonUtilities::KeyCode::Escape))
	{
		return eStateStackMessage::PopSubState;
	}
	if (myState == eState::ClickedNo)
	{
		return eStateStackMessage::PopSubState;
	}
	if (myState == eState::ClickedYes)
	{
		return eStateStackMessage::PopPromptAndPushLevelSelect;
	}
	return eStateStackMessage::KeepState;
}

void PromptState::Render()
{
	for (unsigned short i = 0; i < myRenderBuffer.Size(); i++)
	{
		myRenderBuffer[i]->Render();
	}
}

void PromptState::CleanUp()
{
	myRenderBuffer.RemoveAll();
}

void PromptState::FillRenderBuffer()
{
	myRenderBuffer.Add(myBackground);
	myRenderBuffer.Add(myPromptDrawable);

	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		myButtons[i]->FillRenderBuffer(myRenderBuffer);
	}
}

void PromptState::InitSprites()
{
	myBackground = new Sprite();
	myBackground->Init("Sprites/menu/pauseBackground.dds");
	myBackground->SetPosition({ 0.5f, 0.5f });
	myBackground->SetSizeRelativeToScreen({ 2.f, 1.f });
	// Fix

	myPromptDrawable = new Sprite();
	myPromptDrawable->Init("Sprites/menu/overrideGame_text.dds");
	myPromptDrawable->SetPosition({ 0.5f, 0.5f });
}

void PromptState::DeleteSprites()
{
	if (myPromptDrawable != nullptr)
	{
		delete myPromptDrawable;
		myPromptDrawable = nullptr;
	}

	if (myBackground != nullptr)
	{
		delete myBackground;
		myBackground = nullptr;
	}
}

void PromptState::InitButtons()
{
	myButtons.Init(8);

	myYesButton.Init([&] { NewGame(); }, "buttonYes", CommonUtilities::Vector2f(0.55f, 0.6f), myInputManager);
	myYesButton.SetHitboxOffsetX(0.01f);
	myYesButton.SetHitboxOffsetY(0.02f);
	myButtons.Add(&myYesButton);

	myNoButton.Init([&] { myState = eState::ClickedNo; }, "buttonNo", CommonUtilities::Vector2f(0.45f, 0.6f), myInputManager);
	myNoButton.SetHitboxOffsetX(0.01f);
	myNoButton.SetHitboxOffsetY(0.02f);
	myButtons.Add(&myNoButton);

	mySelectedButton = &myNoButton;

	ConnectButtons();
}

void PromptState::UpdateButtons()
{
	CheckMouseInput();
	CheckControllerInput();

	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		myButtons[i]->Update();
		myButtons[i]->SetCurrentFrame(myButtons[i]->GetDefaultFrame());
	}

	mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame() + 1);
}

void PromptState::ConnectButtons()
{
	myYesButton.ConnectButton(&myNoButton, Button::eConnectSide::Left);
	myNoButton.ConnectButton(&myYesButton, Button::eConnectSide::Right);
}

void PromptState::CheckMouseInput()
{
	for (unsigned short i = 0; i < myButtons.Size(); i++)
	{
		Button* currentButton = myButtons[i];

		currentButton->Update();
		if (myInputManager->GetCursorPositionOnScreenLastUpdate() != myInputManager->GetCursorPositionOnScreen())
		{
			if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
			{
				mySelectedButton = currentButton;
				currentButton->SetCurrentFrame(currentButton->GetDefaultFrame() + 1);
			}
			else
			{
				currentButton->SetCurrentFrame(currentButton->GetDefaultFrame());
			}
		}
	}

	if (myInputManager->IsMouseButtonClicked(CommonUtilities::MouseButton::Left))
	{
		for (unsigned short i = 0; i < myButtons.Size(); i++)
		{
			Button* currentButton = myButtons[i];

			if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
			{
				currentButton->OnClick();
			}
		}
	}
}

void PromptState::CheckControllerInput()
{
	if (InputWrapper::IsButtonPressed(KeyButton::Left))
	{
		if (mySelectedButton->myConnectedButtons.myLeftButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myLeftButton;
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame() + 1);
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Right))
	{
		if (mySelectedButton->myConnectedButtons.myRightButton != nullptr)
		{
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame());
			mySelectedButton = mySelectedButton->myConnectedButtons.myRightButton;
			mySelectedButton->SetCurrentFrame(mySelectedButton->GetDefaultFrame() + 1);
		}
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Select))
	{
		mySelectedButton->OnClick();
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Back))
	{
		myState = eState::ClickedNo;
	}
	else if (InputWrapper::IsButtonPressed(KeyButton::Pause))
	{
		myState = eState::ClickedNo;
	}
}

void PromptState::NewGame()
{
	myStateStack->myGameProgressData = CreateNewGame();
	myState = eState::ClickedYes;
}

#pragma once
#include "GameState.h"
#include <functional>

class Sound;

class PromptState : public GameState
{
public:
	enum class eState
	{
		ClickedNo,
		ClickedYes,
		UnClicked
	};

	PromptState(CommonUtilities::InputManager* aInputManager);
	~PromptState();

	void Init() override;

	void OnEnter() override;
	void OnExit() override;

	eStateStackMessage Update() override;
	void Render() override;

	inline const bool LetThroughRender() const { return true; }

private:

	eState myState;

	void CleanUp();
	void FillRenderBuffer();
	void InitSprites();
	void DeleteSprites();

	Drawable* myBackground;
	Drawable* myPromptDrawable;
	CommonUtilities::GrowingArray<Drawable*> myRenderBuffer;

	void InitButtons();
	void UpdateButtons();
	void ConnectButtons();

	Button* mySelectedButton;
	Button myYesButton;
	Button myNoButton;

	Sound* mySound;

	CommonUtilities::GrowingArray<Button*> myButtons;

	void CheckMouseInput();
	void CheckControllerInput();

	std::function<void()>* myFunction;
	void NewGame();
};


#include "stdafx.h"
#include "Room.h"
#include "BatchManager.h"
#include "BatchedSprite.h"

#include "GrabbableObject.h"
#include "Player.h"
#include "Door.h"
#include "Checkpoint.h"
#include "Tile.h"
#include "MovingTile.h"
#include "HookableTile.h"
#include "Trap.h"
#include "Trigger.h"
#include "DancerObject.h"

#include "Timer.h"
#include "RoomChangeObserver.h"
#include "Camera.h"
#include "Collectible.h"

#include "Goomba.h"
#include "Enemy.h"
#include "BouncingPlatform.h"
#include "TileSize.h"
#include "Turret.h"
#include "CrewMember.h"
#include "Ladder.h"
#include "HookPickup.h"
#include "Warp.h"
#include "BasketballGame.h"

Room::Room()
{
}

Room::Room(RoomChangeObserver* aRoomChangeObserver, uint16_t aLevelIndex)
	: myRoomChangeObserver(aRoomChangeObserver)
	, myLevelIndex(aLevelIndex)
{
}


Room::~Room()
{
	for (unsigned short i = 0; i < myGameObjects.Size(); i++)
	{
		if (myGameObjects[i] != myPlayer && myGameObjects[i] != &myPlayer->GetHook())
		{
			delete myGameObjects[i];
			myGameObjects[i] = nullptr;
		}
	}
	for (unsigned short i = 0; i < myRemovedGameObjects.Size(); i++)
	{
		if (myRemovedGameObjects[i] != myPlayer && myRemovedGameObjects[i] != &myPlayer->GetHook())
		{
			delete myRemovedGameObjects[i];
			myRemovedGameObjects[i] = nullptr;
		}
	}
}

void Room::Init(Player & aPlayer, const char* aRootName)
{
	myCrewMember = nullptr;
	myChangeLevel = false;
	myChangeRoom = false;
	myDoors.Init(8);
	myGameObjects.Init(512);
	myRemovedGameObjects.Init(8);
	myPlayer = &aPlayer;
	myProjectileManager.Init(this);

	myMaxEdges = { 0.0f, 0.0f };
	myMinEdges = { 0.0f, 0.0f };

	LoadDataAndFillGameObjectBuffer(aRootName);

	myGameObjects.Add(myPlayer);
	myGameObjects.Add(&myPlayer->GetHook());
	Camera::GetInstance().SetMinEdges(myMinEdges);
	Camera::GetInstance().SetMaxEdges(myMaxEdges);
	myCollisionBuffer.Init(myGameObjects.Size());
}

void Room::Update()
{
	GameObject::UpdateAllGameObjects(myGameObjects);
	myCollisionBuffer.RemoveAll();
	for (unsigned short i = 0; i < myGameObjects.Size(); i++)
	{
		myGameObjects[i]->FillCollisionBuffer(myCollisionBuffer);
	}
	Collider::UpdateAllColliders(myCollisionBuffer);

	if (myPlayer->GetPosition().y - 0.3f > myMaxEdges.y && myChangeLevel == false && myChangeRoom == false)
	{
		myPlayer->Die();
	}
}

void Room::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	for (unsigned short i = 0; i < myGameObjects.Size(); i++)
	{
		myGameObjects[i]->FillRenderBuffer(aRenderBuffer);
	}
}

void Room::LoadDataAndFillGameObjectBuffer(const char* aRootName)
{
	float windowWidth = 1920.0f;
	float windowHeight = 1080.0f;
	myTileSets.Init(5);
	CommonUtilities::Vector2f tilePosition = { 0.0f,0.0f };
	float tilePositoinX;
	float tilePositoinY;
	json roomData = LoadRoomFromTiled(aRootName);

	for (unsigned short tilesetsIndex = 0; tilesetsIndex < roomData["tilesets"].size(); tilesetsIndex++)
	{
		myTileSets.Add(TileSet());
		myTileSets.GetLast().tileObjects.Init(5);
		myTileSets.GetLast().animationDatas.Init(5);
		myTileSets.GetLast().columns = roomData["tilesets"][tilesetsIndex]["columns"];
		myTileSets.GetLast().startGridID = roomData["tilesets"][tilesetsIndex]["firstgid"];
		myTileSets.GetLast().spacing = roomData["tilesets"][tilesetsIndex]["spacing"];
		myTileSets.GetLast().tileCount = roomData["tilesets"][tilesetsIndex]["tilecount"];

		if (!roomData["tilesets"][tilesetsIndex]["tiles"].is_object())
		{
			myTileSets.GetLast().imageHeight = roomData["tilesets"][tilesetsIndex]["imageheight"];
			myTileSets.GetLast().imageWidth = roomData["tilesets"][tilesetsIndex]["imagewidth"];
			myTileSets.GetLast().tilesetPath = CleanImagePathLoadedFromTiled(roomData["tilesets"][tilesetsIndex]["image"].get<std::string>());
		}
		else
		{
			if (roomData["tilesets"][tilesetsIndex]["tiles"]["0"]["animation"].is_array())
			{
				myTileSets.GetLast().tilesetPath = CleanImagePathLoadedFromTiled(roomData["tilesets"][tilesetsIndex]["image"].get<std::string>());
				myTileSets.GetLast().imageHeight = roomData["tilesets"][tilesetsIndex]["imageheight"];
				myTileSets.GetLast().imageWidth = roomData["tilesets"][tilesetsIndex]["imagewidth"];
				
				for (size_t i = 0; i < roomData["tilesets"][tilesetsIndex]["tiles"]["0"]["animation"].size(); i++)
				{
					myTileSets[tilesetsIndex].animationDatas.Add(AnimationData());
					myTileSets[tilesetsIndex].animationDatas.GetLast().duration = roomData["tilesets"][tilesetsIndex]["tiles"]["0"]["animation"][i]["duration"];
					myTileSets[tilesetsIndex].animationDatas.GetLast().tileID = roomData["tilesets"][tilesetsIndex]["tiles"]["0"]["animation"][i]["tileid"];
				}
				int gaIndex = 0;
				for (json::iterator it = roomData["tilesets"][tilesetsIndex]["tileproperties"].begin(); it != roomData["tilesets"][tilesetsIndex]["tileproperties"].end(); ++it)
				{
					myTileSets[tilesetsIndex].animationDatas[gaIndex].objectType = GetObjectTypePath((*it)["category"].get<std::string>(), (*it)["type"].get<std::string>());
					++gaIndex;
				}
			}
			else
			{
				myTileSets.GetLast().imageHeight = roomData["tilesets"][tilesetsIndex]["tileheight"];
				myTileSets.GetLast().imageWidth = roomData["tilesets"][tilesetsIndex]["tilewidth"];


				for (json::iterator it = roomData["tilesets"][tilesetsIndex]["tiles"].begin(); it != roomData["tilesets"][tilesetsIndex]["tiles"].end(); ++it)
				{
					myTileSets.GetLast().tileObjects.Add(TileObjectData());
					myTileSets.GetLast().tileObjects.GetLast().tilePath = CleanImagePathLoadedFromTiled((*it)["image"].get<std::string>());
				}

				int gaIndex = 0;
				for (json::iterator it = roomData["tilesets"][tilesetsIndex]["tileproperties"].begin(); it != roomData["tilesets"][tilesetsIndex]["tileproperties"].end(); ++it)
				{
					myTileSets[tilesetsIndex].tileObjects[gaIndex].objectType = GetObjectTypePath((*it)["category"].get<std::string>(), (*it)["type"].get<std::string>());
					++gaIndex;
				}
			}
		}

	}

	for (unsigned short layerIndex = 0; layerIndex < roomData["layers"].size(); ++layerIndex)
	{

		tilePosition.y = 0.0f;
		tilePosition.x = 0.0f;
		tilePositoinX = 0.0f;
		tilePositoinY = 0.0f;
		if (roomData["layers"][layerIndex]["type"] == "tilelayer" && roomData["layers"][layerIndex]["visible"].get<bool>() == true)
		{
			for (unsigned short tileIndex = 0; tileIndex < roomData["layers"][layerIndex]["data"].size(); tileIndex++)
			{
					if (roomData["layers"][layerIndex]["data"][tileIndex].get<int>() != 0)
					{
						Tile *newTile = new Tile();

						for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
						{

							if (roomData["layers"][layerIndex]["data"][tileIndex].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["data"][tileIndex].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
							{
								BatchedSprite *batchedSprite = new BatchedSprite();
								newTile->InitGO({ tilePositoinX / windowWidth, tilePositoinY / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), batchedSprite);
								batchedSprite->SetTextureRectPosFromTileID(roomData["layers"][layerIndex]["data"][tileIndex], myTileSets[tileSetIndex]);
								batchedSprite->SetShouldRender(false);
							}
						}

						if (roomData["layers"][layerIndex]["properties"]["solid"].get<bool>() == true)
						{
							BoxCollider tileCol;
							tileCol.Init((TILE_SIZE / windowWidth) * TILE_MULTIPLIER, (TILE_SIZE / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.0f });
							tileCol.SetTag(ColliderTag::Tile);
							newTile->AttachCollider(tileCol);
						}
						myGameObjects.Add(newTile);
					}
					tilePositoinX += TILE_SIZE;
					if ((tileIndex + 1) % roomData["layers"][layerIndex]["width"].get<int>() == 0 && tileIndex != 0)
					{
						tilePositoinY += TILE_SIZE;
						tilePositoinX = 0.0f;
					}
				
				
			}


			myMinEdges = myGameObjects[0]->GetPosition();
			myMaxEdges = myGameObjects.GetLast()->GetPosition();

			for (size_t GAIndex = 0; GAIndex < myGameObjects.Size(); GAIndex++)
			{
				if (myMinEdges.x > myGameObjects[GAIndex]->GetPosition().x)
				{
					myMinEdges.x = myGameObjects[GAIndex]->GetPosition().x;
				}
				if (myMinEdges.y > myGameObjects[GAIndex]->GetPosition().y)
				{
					myMinEdges.y = myGameObjects[GAIndex]->GetPosition().y;
				}


				if (myMaxEdges.x < myGameObjects[GAIndex]->GetPosition().x)
				{
					myMaxEdges.x = myGameObjects[GAIndex]->GetPosition().x;
				}
				if (myMaxEdges.y < myGameObjects[GAIndex]->GetPosition().y)
				{
					myMaxEdges.y = myGameObjects[GAIndex]->GetPosition().y;
				}
			}

		}
		else if (roomData["layers"][layerIndex]["type"] == "objectgroup" && roomData["layers"][layerIndex]["visible"].get<bool>() == true)
		{
			for (unsigned short ObjectIndex = 0; ObjectIndex < roomData["layers"][layerIndex]["objects"].size(); ObjectIndex++)
			{
				if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "spawn")
				{
					myPlayer->SetPosition({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<float>()) * TILE_MULTIPLIER / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<float>()) * TILE_MULTIPLIER / windowHeight });
					myPlayerSpawnPos = { (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<float>()) * TILE_MULTIPLIER/ windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<float>()) * TILE_MULTIPLIER / windowHeight };
					myPlayer->SetRespawnPosition(myPlayerSpawnPos);
					Camera::GetInstance().SetPosition(myPlayerSpawnPos);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "door" && roomData["layers"][layerIndex]["objects"][ObjectIndex]["visible"].get<bool>() == true)
				{
					Door *newDoor = new Door(myRoomChangeObserver);

					std::string physicsType = "null";
					for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].animationDatas.Size() > 0)
							{
								Animation *doorAnimation = new Animation();
								doorAnimation->SetLayer(LAYER_DOORS);
								newDoor->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE * 2.0f) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), doorAnimation);
								physicsType = myTileSets[tileSetIndex].animationDatas[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
								physicsType += "/" + roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["doorData"].get<std::string>() + ".json";
								newDoor->SetIsAnimated(true);
							
								newDoor->SetIsLocked(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["isLocked"].get<bool>());
								newDoor->SetFlipped(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["flipped"].get<bool>());
								newDoor->SetShouldRender(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["render"].get<bool>());
								int doorIndex = roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["triggerIndex"].get<int>();

								if (doorIndex != 0)
								{
									newDoor->AddToTriggerManager(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["triggerIndex"].get<int>());
								}
							}

							else if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								Sprite *doorSprite = new Sprite();
								newDoor->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>() ) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), doorSprite);
								physicsType = myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
								physicsType += "/" + roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["doorData"].get<std::string>() + ".json";
								newDoor->SetIsAnimated(false);
							
								newDoor->SetIsLocked(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["isLocked"].get<bool>());
								newDoor->SetFlipped(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["flipped"].get<bool>());
								newDoor->SetShouldRender(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["render"].get<bool>());
								int doorIndex = roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["triggerIndex"].get<int>();

								if (doorIndex != 0)
								{
									newDoor->AddToTriggerManager(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["triggerIndex"].get<int>());
								}

							}
						}
					}
					BoxCollider DoorCol;
					DoorCol.Init(newDoor->AccessDrawable().GetImagesSizePixel().x / windowWidth * TILE_MULTIPLIER , newDoor->AccessDrawable().GetImagesSizePixel().y / windowHeight * TILE_MULTIPLIER , { 0.0f,0.0f });
					newDoor->Init(physicsType.c_str());
					DoorCol.SetTag(ColliderTag::Door);
					newDoor->AttachCollider(DoorCol);
					myDoors.Add(newDoor);
					myGameObjects.Add(newDoor);

				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "doorTrigger")
				{
					Trigger *newTrigger = new Trigger(Trigger::eTriggerType::DoorTrigger);
					std::string triggerType = "null";
					for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						Animation *triggerAnimation = new Animation();
						triggerAnimation->SetLayer(LAYER_TRIGGERS);
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].animationDatas.Size() > 0)
							{
								newTrigger->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / 1920.0f, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / 1080.0f }, myTileSets[tileSetIndex].tilesetPath.c_str(), triggerAnimation);
								triggerType = myTileSets[tileSetIndex].animationDatas[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
								newTrigger->SetTriggerIndex(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["triggerIndex"].get<int>());
							}
							else if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								newTrigger->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / 1920.0f, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / 1080.0f }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), triggerAnimation);
								newTrigger->SetTriggerIndex(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["triggerIndex"].get<int>());
							}
						}
					}
					BoxCollider triggerCollider;
					triggerCollider.Init((TILE_SIZE /windowWidth) * TILE_MULTIPLIER, (TILE_SIZE / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.0f });

					triggerCollider.SetTag(ColliderTag::Trigger);
					newTrigger->AttachCollider(triggerCollider);
					newTrigger->Init(triggerType.c_str());
				
					myGameObjects.Add(newTrigger);

				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "platformTrigger")
				{
					Trigger *newTrigger = new Trigger(Trigger::eTriggerType::PlatformTrigger);

					for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						Animation *triggerAnimation = new Animation();
						triggerAnimation->SetLayer(LAYER_TRIGGERS);
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								newTrigger->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / 1920.0f, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / 1080.0f }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), triggerAnimation);
								newTrigger->SetTriggerIndex(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["triggerIndex"].get<int>());

								if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["direction"].get<std::string>() == "horizontal")
								{
									newTrigger->SetDirection(TriggerManager::eDirection::Horizontal);
								}
								else
								{ 
									newTrigger->SetDirection(TriggerManager::eDirection::Vertical);
								}
							}
						}
					}
					BoxCollider triggerCollider;
					triggerCollider.Init((TILE_SIZE / windowWidth) * TILE_MULTIPLIER, (TILE_SIZE / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.0f });

					triggerCollider.SetTag(ColliderTag::Trigger);
					newTrigger->AttachCollider(triggerCollider);
					myGameObjects.Add(newTrigger);

				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "dialogTrigger")
				{
					Trigger *newTrigger = new Trigger(Trigger::eTriggerType::DialogTrigger);

					for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						Sprite *triggerSprite = new Sprite();
						triggerSprite->SetLayer(LAYER_TRIGGERS);
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								newTrigger->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / 1920.0f, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / 1080.0f }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), triggerSprite);
								newTrigger->SetTriggerIndex(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["triggerIndex"].get<int>());
							}
						}
					}
					BoxCollider triggerCollider;
					triggerCollider.Init((TILE_SIZE / windowWidth) * TILE_MULTIPLIER, (TILE_SIZE / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.0f });

					triggerCollider.SetTag(ColliderTag::Trigger);
					newTrigger->AttachCollider(triggerCollider);
					newTrigger->Init(nullptr);
					myGameObjects.Add(newTrigger);

				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "checkpoint")
				{
					Checkpoint* newCheckpoint = new Checkpoint();
					std::string physicsType = "null";
					for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						Sprite *checkpointSprite = new Sprite();
						checkpointSprite->SetLayer(LAYER_TRIGGERS);
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								newCheckpoint->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE * 2.5f) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), checkpointSprite);
								physicsType = myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
						}
					}
					BoxCollider checkpointCol;
					checkpointCol.Init((newCheckpoint->AccessDrawable().GetImagesSizePixel().x / windowWidth) * TILE_MULTIPLIER, (newCheckpoint->AccessDrawable().GetImagesSizePixel().y / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.0f });

					checkpointCol.SetTag(ColliderTag::Checkpoint);
					newCheckpoint->AttachCollider(checkpointCol);
					newCheckpoint->Init(physicsType.c_str());
					myGameObjects.Add(newCheckpoint);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "collectible")
				{
					Collectible* newCollectible = new Collectible();
					std::string physicsType = "null";
					for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						Animation *collectibleAnimation = new Animation();
						collectibleAnimation->SetLayer(LAYER_COLLECTIBLES);
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								newCollectible->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), collectibleAnimation);
								physicsType = myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
							else if (myTileSets[tileSetIndex].animationDatas.Size() > 0)
							{
								newCollectible->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), collectibleAnimation);
								physicsType = myTileSets[tileSetIndex].animationDatas[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
							else
							{
								newCollectible->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), collectibleAnimation);
							}
						}
					}

					BoxCollider collectibleCol;
					collectibleCol.Init((TILE_SIZE / windowWidth) * TILE_MULTIPLIER, (TILE_SIZE / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.0f });

					collectibleCol.SetTag(ColliderTag::Collectible);
					newCollectible->AttachCollider(collectibleCol);
					myCollectibleInWarpZone = newCollectible;
					myGameObjects.Add(newCollectible);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "ladder")
				{
					Ladder* newLadder = new Ladder();
					std::string physicsType = "null";
					for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						Sprite *ladderSprite = new Sprite();
						ladderSprite->SetLayer(LAYER_LADDERS);
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								newLadder->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - 24.f) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), ladderSprite);
								physicsType = myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
								std::string segmentTypeAsString = roomData["layers"][layerIndex]["objects"][ObjectIndex]["name"].get<std::string>();
								LadderSegment segmentType = LadderSegment::Middle;
								if (segmentTypeAsString == "top")
								{
									segmentType = LadderSegment::Top;
								}
								else if (segmentTypeAsString == "bottom")
								{
									segmentType = LadderSegment::Bottom;
								}
								newLadder->SetSegmentType(segmentType);
							}
						}
					}
					BoxCollider ladderCol;
					ladderCol.Init((newLadder->AccessDrawable().GetImagesSizePixel().x / windowWidth) * TILE_MULTIPLIER, (newLadder->AccessDrawable().GetImagesSizePixel().y / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.0f });

					ladderCol.SetTag(ColliderTag::Climbable);
					newLadder->AttachCollider(ladderCol);
					ladderCol.SetIsSolid(false);
					myGameObjects.Add(newLadder);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "grabbable" && roomData["layers"][layerIndex]["objects"][ObjectIndex]["visible"].get<bool>() == true)
				{
					GrabbableObject *newGrabbable = new GrabbableObject();
					std::string physicsType = "null";
					for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								Sprite *grabbableSprite = new Sprite();
								grabbableSprite->SetLayer(LAYER_ITEMS);
								newGrabbable->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), grabbableSprite);
								physicsType = myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
						}
					}
					BoxCollider grabbableCol;
					grabbableCol.Init((TILE_SIZE / windowWidth) * TILE_MULTIPLIER, (TILE_SIZE / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.0f });

					grabbableCol.SetTag(ColliderTag::Grabbable);
					newGrabbable->AttachCollider(grabbableCol);
					newGrabbable->Init(physicsType);
					myGameObjects.Add(newGrabbable);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "goomba")
				{
					Goomba* newEnemy = new Goomba();
					std::string physicsType = "null";
					for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								Sprite *grabbableSprite = new Sprite();
								grabbableSprite->SetLayer(LAYER_ENEMIES);
								newEnemy->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), grabbableSprite);
								physicsType = myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
								newEnemy->Init(physicsType.c_str(), roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["startingDirection"].get<float>());
							}
						}
					}
					BoxCollider enemyCol;
					enemyCol.Init((2.f * TILE_SIZE / windowWidth) * TILE_MULTIPLIER, (1.5f * TILE_SIZE / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.f });

					enemyCol.SetTag(ColliderTag::Enemy);
					newEnemy->AttachCollider(enemyCol);
					myGameObjects.Add(newEnemy);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "bouncingPlatform")
				{
					BouncingPlatform* newBoungcingPlatform = new BouncingPlatform();
					std::string physicsType = "null";
					for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						Animation *bouncingPlatformAnimation = new Animation();
						bouncingPlatformAnimation->SetLayer(LAYER_TILES);
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								newBoungcingPlatform->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), bouncingPlatformAnimation);
								physicsType = myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
							else if (myTileSets[tileSetIndex].animationDatas.Size() > 0)
							{
								newBoungcingPlatform->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), bouncingPlatformAnimation);
								physicsType = myTileSets[tileSetIndex].animationDatas[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
							else
							{
								newBoungcingPlatform->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), bouncingPlatformAnimation);
							}
						}
					}

					BoxCollider bouncingPlatformCol;
					bouncingPlatformCol.Init((newBoungcingPlatform->AccessDrawable().GetImagesSizePixel().x / windowWidth) * TILE_MULTIPLIER, (newBoungcingPlatform->AccessDrawable().GetImagesSizePixel().y / windowHeight) * TILE_MULTIPLIER / 2.0f, CommonUtilities::Vector2f{ 0.0f, 0.0f });

					bouncingPlatformCol.SetTag(ColliderTag::BouncingPlatform);
					newBoungcingPlatform->AttachCollider(bouncingPlatformCol);
					newBoungcingPlatform->Init(physicsType.c_str());
					myGameObjects.Add(newBoungcingPlatform);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "trap")
				{
					Trap* newTrap = new Trap();
					std::string objectTypePath = "null";
					for (size_t tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								Sprite *trapSprite = new Sprite();
								trapSprite->SetLayer(LAYER_TRAPS);
								newTrap->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), trapSprite);
								objectTypePath = myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
							else if (myTileSets[tileSetIndex].animationDatas.Size() > 0)
							{
								Animation *trapAnimation = new Animation();
								trapAnimation->SetLayer(LAYER_TRAPS);
								newTrap->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), trapAnimation);
								objectTypePath = myTileSets[tileSetIndex].animationDatas[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
							else
							{
								Sprite *trapSprite = new Sprite();
								trapSprite->SetLayer(LAYER_TRAPS);
								newTrap->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), trapSprite);
								objectTypePath = myTileSets[tileSetIndex].animationDatas[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
						}
					}
					BoxCollider trapCol;
					trapCol.Init((TILE_SIZE / windowWidth) * TILE_MULTIPLIER / 1.5f, (TILE_SIZE / windowHeight) * TILE_MULTIPLIER / 2.0f, CommonUtilities::Vector2f{ 0.0f, 0.015f });
					trapCol.SetTag(ColliderTag::Trap);
					newTrap->AttachCollider(trapCol);
					newTrap->Init();
					if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["name"].get<std::string>() == "toBeActivated")
					{
						newTrap->DeActivate();
					}
					myGameObjects.Add(newTrap);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "danceObject")
				{
					DancerObject* newDancerObject = new DancerObject();
					std::string objectTypePath = "null";

					bool isBoomBox = roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["isBoomBox"].get<bool>();

					for (size_t tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{					
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								Sprite *trapSprite = new Sprite();
								trapSprite->SetLayer(LAYER_TRAPS);
								newDancerObject->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), trapSprite);
								newDancerObject->InitDO(isBoomBox);
								objectTypePath = myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
							else if (myTileSets[tileSetIndex].animationDatas.Size() > 0)
							{
								Animation *DOAnimation = new Animation();
								DOAnimation->SetLayer(LAYER_TRAPS);
								newDancerObject->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), DOAnimation);
								newDancerObject->InitDO(isBoomBox);
								objectTypePath = myTileSets[tileSetIndex].animationDatas[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
							else
							{
								Sprite *trapSprite = new Sprite();
								trapSprite->SetLayer(LAYER_TRAPS);
								newDancerObject->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), trapSprite);
								newDancerObject->InitDO(isBoomBox);
								objectTypePath = myTileSets[tileSetIndex].animationDatas[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
						}
					}
					if (isBoomBox)
					{
						BoxCollider boomboxCol;
						boomboxCol.Init((TILE_SIZE / windowWidth) * TILE_MULTIPLIER, (TILE_SIZE / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.0f });
						boomboxCol.SetTag(ColliderTag::BoomBox);
						boomboxCol.SetIsSolid(false);
						newDancerObject->AttachCollider(boomboxCol);
					}

					myGameObjects.Add(newDancerObject);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "hookable")
				{
					HookableTile* newHookableTile = new HookableTile();
					std::string objectTypePath = "null";
					for (size_t tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							Sprite *hookableTileSprite = new Sprite();
							hookableTileSprite->SetLayer(LAYER_LADDERS);
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								newHookableTile->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), hookableTileSprite);
								objectTypePath = myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
						}
					}
					BoxCollider hookableTileCol;
					hookableTileCol.Init((TILE_SIZE / windowWidth) * TILE_MULTIPLIER, (TILE_SIZE / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.0f });
					hookableTileCol.SetTag(ColliderTag::HookableTile);
					newHookableTile->AttachCollider(hookableTileCol);
					newHookableTile->Init(objectTypePath.c_str());
					myGameObjects.Add(newHookableTile);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "turret")
				{
					Turret *newTurret = new Turret(myProjectileManager, myPlayer);

					for (size_t tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							Sprite *turretSprite = new Sprite();
							turretSprite->SetLayer(LAYER_ENEMIES);
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								newTurret->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), turretSprite);
							}
						}
					}
					BoxCollider turretCol;
					turretCol.Init(0.06f, 0.05f, { 0.0f, 0.03f });
					turretCol.SetTag(ColliderTag::Enemy);
					newTurret->AttachCollider(turretCol);
					newTurret->Init(roomData, layerIndex, ObjectIndex);
					myGameObjects.Add(newTurret);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "movingTile")
				{
					
					MovingTile* newMovingTile = new MovingTile();
					std::string objectTypePath = "null";
					for (size_t tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							Animation *movingTileAnimation = new Animation();
							movingTileAnimation->SetLayer(LAYER_TILES);
							if (myTileSets[tileSetIndex].animationDatas.Size() > 0)
							{
								newMovingTile->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), movingTileAnimation);
							}
						}
					}

					int movingTileTriggerIndex = roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["triggerIndex"].get<int>();

					if (movingTileTriggerIndex != 0)
					{
						newMovingTile->AddToTriggerManager(roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["triggerIndex"].get<int>());
					}

					BoxCollider movingTileCol;
					movingTileCol.Init((newMovingTile->AccessDrawable().GetImagesSizePixel().x / windowWidth) * TILE_MULTIPLIER, (newMovingTile->AccessDrawable().GetImagesSizePixel().y / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.0f });
					movingTileCol.SetTag(ColliderTag::MovingTile);
					newMovingTile->AttachCollider(movingTileCol);
		
					newMovingTile->Init(roomData, layerIndex, ObjectIndex);
					myGameObjects.Add(newMovingTile);				
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "crewmember")
				{

					CrewMember* newCrewMember = new CrewMember();
					for (size_t tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							Animation *newCrewMemberAnimation = new Animation();
							newCrewMemberAnimation->SetLayer(LAYER_CREWMEMBERS);
							if (myTileSets[tileSetIndex].animationDatas.Size() > 0)
							{
								newCrewMember->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), newCrewMemberAnimation);
							}
						}
					}

					bool isFlipped = roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["isFlipped"].get<bool>();
					float walkOffDirection = roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["direction"].get<float>();
					int dialogIndex = roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["dialogIndex"].get<int>();

					BoxCollider crewMemberCol;
					crewMemberCol.Init(0.1f, 0.12f, CommonUtilities::Vector2f{ 0.0f, 0.0f });
					crewMemberCol.SetTag(ColliderTag::CrewMember);
					newCrewMember->AttachCollider(crewMemberCol);
					myCrewMember = newCrewMember;

					newCrewMember->Init(dialogIndex, walkOffDirection, isFlipped, myLevelIndex);

					myGameObjects.Add(newCrewMember);

				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "warp")
				{
					Warp* newWarp = new Warp();
					for (size_t tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							Animation *newWarpSprite = new Animation();
							newWarpSprite->SetLayer(LAYER_CREWMEMBERS);
							newWarp->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, "sprites/objects/warp.dds", newWarpSprite);
						}
					}
					BoxCollider newWarpCollider;
					newWarpCollider.Init((TILE_SIZE / windowWidth) * TILE_MULTIPLIER, (TILE_SIZE / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.0f });
					newWarpCollider.SetTag(ColliderTag::Warp);
					newWarp->AttachCollider(newWarpCollider);
					if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["properties"]["isEntrance"].get<bool>())
					{
						newWarp->SetWarpType(eWarpType::In);
					}
					else
					{
						newWarp->SetWarpType(eWarpType::Out);
					}
					myGameObjects.Add(newWarp);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "hookPickup")
				{
					HookPickup* newEnemy = new HookPickup();
					std::string physicsType = "null";
					for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].animationDatas.Size() > 0)
							{
								Animation *hookAnimation = new Animation();
								hookAnimation->SetLayer(LAYER_COLLECTIBLES);
								newEnemy->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), hookAnimation);
								physicsType = myTileSets[tileSetIndex].animationDatas[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
							else if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								Sprite *grabbableSprite = new Sprite();
								grabbableSprite->SetLayer(LAYER_COLLECTIBLES);
								newEnemy->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), grabbableSprite);
								physicsType = myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
						}
					}
					BoxCollider enemyCol;
					enemyCol.Init((TILE_SIZE / windowWidth) * TILE_MULTIPLIER, (TILE_SIZE / windowHeight) * TILE_MULTIPLIER, CommonUtilities::Vector2f{ 0.0f, 0.f });

					enemyCol.SetTag(ColliderTag::HookPickup);
					newEnemy->AttachCollider(enemyCol);
					myGameObjects.Add(newEnemy);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "scenery")
				{
					GameObject* newCollectible = new GameObject();
					std::string physicsType = "null";
					for (unsigned short tileSetIndex = 0; tileSetIndex < myTileSets.Size(); tileSetIndex++)
					{
						Animation *collectibleAnimation = new Animation();
						collectibleAnimation->SetLayer(LAYER_COLLECTIBLES);
						if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() >= myTileSets[tileSetIndex].startGridID && roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() < myTileSets[tileSetIndex].startGridID + myTileSets[tileSetIndex].tileCount)
						{
							if (myTileSets[tileSetIndex].tileObjects.Size() > 0)
							{
								newCollectible->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].tilePath.c_str(), collectibleAnimation);
								physicsType = myTileSets[tileSetIndex].tileObjects[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
							else if (myTileSets[tileSetIndex].animationDatas.Size() > 0)
							{
								newCollectible->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), collectibleAnimation);
								physicsType = myTileSets[tileSetIndex].animationDatas[roomData["layers"][layerIndex]["objects"][ObjectIndex]["gid"].get<int>() - (myTileSets[tileSetIndex].startGridID)].objectType;
							}
							else
							{
								newCollectible->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, myTileSets[tileSetIndex].tilesetPath.c_str(), collectibleAnimation);
							}
						}
					}
					myGameObjects.Add(newCollectible);
				}
				else if (roomData["layers"][layerIndex]["objects"][ObjectIndex]["type"].get<std::string>() == "basketballGame")
				{
					BasketballGame* newBasketballGame = new BasketballGame();
					newBasketballGame->InitGO({ (roomData["layers"][layerIndex]["objects"][ObjectIndex]["x"].get<int>()) / windowWidth, (roomData["layers"][layerIndex]["objects"][ObjectIndex]["y"].get<int>() - TILE_SIZE) / windowHeight }, "sprites/level2/basketballStand.dds", new Sprite());
					newBasketballGame->Init();
					myGameObjects.Add(newBasketballGame);
				}
			}
		}
	}
	roomData.clear();
}

void Room::ResetRoom()
{
	for (unsigned short i = myRemovedGameObjects.Size(); i > 0; --i)
	{
		myRemovedGameObjects[i-1]->Reset(myPlayer);
		myRemovedGameObjects[i-1]->SetShouldRemove(false);
		if (myRemovedGameObjects[i-1]->GetCollider()->GetTag() != ColliderTag::HookPickup || myPlayer->HasHook() == false)
			myGameObjects.Add(myRemovedGameObjects[i-1]);
		myRemovedGameObjects.RemoveCyclicAtIndex(i-1);
	}

	for (unsigned short i = 0; i < myGameObjects.Size(); i++)
	{
		if (myGameObjects[i] != &myPlayer->GetHook())
		{
			myGameObjects[i]->Reset(myPlayer);
		}
	}

}

void Room::LeaveRoom(CommonUtilities::GrowingArray<GameObject*>& myOldRoomGameObjects)
{
	for (size_t i = 0; i < myGameObjects.Size(); i++)
	{
		if (myGameObjects[i] != myPlayer && myGameObjects[i] != &myPlayer->GetHook())
		{
			if (myGameObjects[i]->GetPosition().x < Camera::GetInstance().GetPosition().x + 0.6f &&
				myGameObjects[i]->GetPosition().x > Camera::GetInstance().GetPosition().x - 0.6f &&
				myGameObjects[i]->GetPosition().y < Camera::GetInstance().GetPosition().y + 0.6f &&
				myGameObjects[i]->GetPosition().y > Camera::GetInstance().GetPosition().y - 0.6f)
			{
				myOldRoomGameObjects.Add(myGameObjects[i]);
			}
		}
	}	
}

void Room::EnterRoom()
{
	for (unsigned short i = 0; i < myGameObjects.Size(); i++)
	{
		if (myGameObjects[i] != myPlayer && myGameObjects[i] != &myPlayer->GetHook())
		{
			myGameObjects[i]->AccessDrawable().SetShouldRender(true);
		}
	}
	

	myPlayer->SetPosition(myPlayer->GetPosition() + myRoomOffset);

	myPlayer->SetRespawnPosition(myPlayer->GetPosition());
	Camera::GetInstance().SetMaxEdges(myMaxEdges);
	Camera::GetInstance().SetMinEdges(myMinEdges);
}

void Room::RenderColliders()
{
	Collider::DrawAllColliders(myCollisionBuffer);
}

void Room::RenderBatchedSprites()
{
	BatchManager::RenderBatch();
}

void Room::UpdateNewOffsetPositions()
{
	myMinEdges = CommonUtilities::Vector2f(1.0f, 1.0f) + myRoomOffset;
	myMaxEdges = CommonUtilities::Vector2f(0.0f, 0.0f);
	for (unsigned short i = 0; i < myGameObjects.Size(); i++)
	{
		if (myGameObjects[i] != myPlayer && myGameObjects[i] != &myPlayer->GetHook())
		{
			myGameObjects[i]->SetPosition(myGameObjects[i]->GetPosition() + myRoomOffset);
			myGameObjects[i]->SetOriginalPosition();

			if (myMinEdges.x > myGameObjects[i]->GetPosition().x)
			{
				myMinEdges.x = myGameObjects[i]->GetPosition().x;
			}
			if (myMinEdges.y > myGameObjects[i]->GetPosition().y)
			{
				myMinEdges.y = myGameObjects[i]->GetPosition().y;
			}
			if (myMaxEdges.x < myGameObjects[i]->GetPosition().x)
			{
				myMaxEdges.x = myGameObjects[i]->GetPosition().x;
			}
			if (myMaxEdges.y < myGameObjects[i]->GetPosition().y)
			{
				myMaxEdges.y = myGameObjects[i]->GetPosition().y;
			}

		}
	}
}

void Room::SetRoomOffset(CommonUtilities::Vector2f& aRoomOffset)
{
	myRoomOffset = aRoomOffset;
	UpdateNewOffsetPositions();
}

void Room::AddGameObjectToBuffer(GameObject * aGameObject)
{
	myGameObjects.Add(aGameObject);
}

void Room::CleanUpGameObjects()
{
	for (unsigned short i = myGameObjects.Size(); i > 0; --i)
	{
		if (myGameObjects[i-1]->GetShouldRemove() == true)
		{
			myRemovedGameObjects.Add(myGameObjects[i-1]);
			myGameObjects.RemoveCyclicAtIndex(i-1);
		}
	}


}

void Room::SetChnageRoom(const bool aChangeRoom)
{
	myChangeRoom = aChangeRoom;
}

void Room::SetChangeLevel(const bool aChangeLevel)
{
	myChangeLevel = aChangeLevel;
}

void Room::StopRendering()
{
	for (size_t i = 0; i < myGameObjects.Size(); i++)
	{
		myGameObjects[i]->AccessDrawable().SetShouldRender(false);
	}
}

void Room::TriggerPlatforms()
{
	for (int i = 0; i < myGameObjects.Size(); ++i)
	{
		if (dynamic_cast<MovingTile*>(myGameObjects[i]) != nullptr)
		{
			myGameObjects[i]->OnTriggerd();
		}
	}
}

void Room::TriggerTraps()
{
	for (int i = 0; i < myGameObjects.Size(); ++i)
	{
		if (dynamic_cast<Trap*>(myGameObjects[i]) != nullptr)
		{
			myGameObjects[i]->OnTriggerd();
		}
	}
}

void Room::DeactivatePlatforms()
{
	for (int i = 0; i < myGameObjects.Size(); ++i)
	{
		if (dynamic_cast<MovingTile*>(myGameObjects[i]) != nullptr)
		{
			dynamic_cast<MovingTile*>(myGameObjects[i])->Deactivate();
		}
	}
}

void Room::DeactivateTraps()
{
	for (int i = 0; i < myGameObjects.Size(); ++i)
	{
		if (dynamic_cast<Trap*>(myGameObjects[i]) != nullptr)
		{
			dynamic_cast<Trap*>(myGameObjects[i])->DeActivate();
		}
	}
}

#pragma once
#include "GrowingArray.h"
#include "ProjectileManager.h"

class CrewMember;

class GameObject;
class Player;
class Drawable;
class RoomChangeObserver;
class Door;
class Collectible;
#include "JsonWrapper.h"

#define LAYER_COLLECTIBLES 8
#define LAYER_CREWMEMBERS 7
#define LAYER_DOORS 6
#define LAYER_TRIGGERS 5
#define LAYER_ITEMS 4
#define LAYER_ENEMIES 3
#define LAYER_TRAPS 2
#define LAYER_LADDERS 1
#define LAYER_TILES 0

class Room
{
public:
	Room();
	Room(RoomChangeObserver* aRoomChangeObserver, uint16_t aLevelIndex);
	~Room();

	void Init(Player &aPlayer, const char* aRootName);
	void Update();

	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*> &aRenderBuffer);

	void LoadDataAndFillGameObjectBuffer(const char* aRootName);
	void ResetRoom();
	void LeaveRoom(CommonUtilities::GrowingArray<GameObject*>& myOldRoomGameObjects);
	void EnterRoom();
	void RenderColliders();
	void RenderBatchedSprites();

	void UpdateNewOffsetPositions();

	const CommonUtilities::GrowingArray<Door*>& GetDoors() { return myDoors; }

	void SetRoomOffset(CommonUtilities::Vector2f& aRoomOffset);
	const CommonUtilities::Vector2f& GetRoomOffset() { return myRoomOffset; }

	const CommonUtilities::Vector2f& GetMinEdges() { return myMinEdges; }
	const CommonUtilities::Vector2f& GetMaxEdges() { return myMaxEdges; }

	void AddGameObjectToBuffer(GameObject* aGameObject);
	void CleanUpGameObjects();
	void SetChnageRoom(const bool aChangeRoom);
	void SetChangeLevel(const bool aChangeLevel);
	CrewMember* GetCrewMember() { return myCrewMember; }
	Collectible* GetCollectibleInWarpZone() { return myCollectibleInWarpZone; };
	void StopRendering();
	void TriggerPlatforms();
	void TriggerTraps();
	void DeactivatePlatforms();
	void DeactivateTraps();

private:
	bool myChangeRoom;
	bool myChangeLevel;
	CommonUtilities::Vector2f myRoomOffset;

	CommonUtilities::Vector2f myPlayerSpawnPos;
	CommonUtilities::Vector2f myMinEdges;
	CommonUtilities::Vector2f myMaxEdges;

	CommonUtilities::GrowingArray<Door*> myDoors;

	CommonUtilities::GrowingArray<GameObject*> myGameObjects;
	CommonUtilities::GrowingArray<GameObject*> myCollisionBuffer;
	
	CommonUtilities::GrowingArray<GameObject*> myRemovedGameObjects;

	CommonUtilities::GrowingArray<TileSet> myTileSets;

	Player *myPlayer;
	CrewMember* myCrewMember;
	Collectible* myCollectibleInWarpZone;
	RoomChangeObserver* myRoomChangeObserver;
	ProjectileManager myProjectileManager;
	uint16_t myLevelIndex;
};


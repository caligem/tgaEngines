#include "stdafx.h"
#include "RoomChangeObserver.h"
#include "Room.h"
#include "LevelScene.h"

RoomChangeObserver::RoomChangeObserver(LevelScene* aCurrentLevel)
	: myCurrentLevel(aCurrentLevel)
{
}

RoomChangeObserver::~RoomChangeObserver()
{
}

void RoomChangeObserver::RecieveMessage(const eMessageType & aMessage, const int aIndex)
{
	if (aMessage == eMessageType::ReachedDoor)
	{
		if (myCurrentLevel->GetCurrentRoom() != myCurrentLevel->GetLastRoom())
		{
			myCurrentLevel->ChangeRoomToNext(aIndex);
		}
		else
		{
			myCurrentLevel->SetChangeLevel(true);		
		}
	}
	else if (aMessage == eMessageType::ResetRoom)
	{
		myCurrentLevel->ResetRoom();
	}
}
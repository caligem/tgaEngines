#pragma once
#include "Observer.h"
#include <GrowingArray.h>

class LevelScene;

class RoomChangeObserver : public Observer
{
public:
	RoomChangeObserver(LevelScene* myCurrentLevel);
	~RoomChangeObserver();

	void RecieveMessage(const eMessageType& aMessage, const int aIndex) override;

private:
	LevelScene* myCurrentLevel;
};


#include "stdafx.h"
#include "SoundEngine.h"
#include <assert.h>


SoundEngine::SoundEngine()
{
	myChannel = 0;
	myExtraDriverData = 0; 
	myNumberOfChannels = 32;
}

SoundEngine * SoundEngine::GetInstance()
{
	static SoundEngine soundEngine;
	return &soundEngine;
}

SoundEngine::~SoundEngine()
{
	delete myText;
	myText = nullptr;
}

void SoundEngine::Init()
{
	GetInstance()->myText = new Tga2D::CText("Text/arial.ttf", Tga2D::EFontSize_11, 1);

	//Create a mySystem object and initialize
	GetInstance()->myResult = FMOD::System_Create(&GetInstance()->mySystem);
	GetInstance()->myResult = GetInstance()->mySystem->getVersion(&GetInstance()->myVersion);
	GetInstance()->myResult = GetInstance()->mySystem->init(GetInstance()->myNumberOfChannels, FMOD_INIT_NORMAL, GetInstance()->myExtraDriverData);

	GetInstance()->myMusicChannel->setPriority(0);

	GetInstance()->myShowDebug = false;
	GetInstance()->myVolume = 1.0f;

	GetInstance()->myClickSound = new Sound("Sounds/Sfx/button_clicked.wav");
	GetInstance()->myHoveredSound = new Sound("Sounds/Sfx/button_hovored.wav");
}


Sound* SoundEngine::GetClickSound() 
{
	return GetInstance()->myClickSound;
}

Sound* SoundEngine::GetHoverSound()
{
	return GetInstance()->myHoveredSound;
}

FMOD::Sound * SoundEngine::AddSoundFile(const char* aPath)
{
	FMOD::Sound *pSound = nullptr;
	GetInstance()->myResult = GetInstance()->mySystem->createSound(aPath, FMOD_DEFAULT, 0, &pSound);

	assert(pSound != nullptr && "Sound was not loaded!");

	GetInstance()->myLoadedSounds.push_back(pSound);

	const int length = 32;
	char name[length];

	pSound->getName(name, length);

	GetInstance()->mySoundNames.push_back(name);

	return pSound;
}

void SoundEngine::PlaySound(Sound & aSound)
{
	FMOD::Channel* pChannel; 


	int channelIndex = -1;

	if (aSound.myIsMusic == true && GetInstance()->myPlayingSongs.size() > 0)
	{
		StopPlayingSound(*GetInstance()->myPlayingSongs[0]);
	}

	if (aSound.myIsMusic == true) 
	{
		GetInstance()->myPlayingSongs.push_back(&aSound);
	}

	GetInstance()->myPlayingSounds.push_back(&aSound);
	

	if (GetInstance()->myVolume > 0 || aSound.myIsMusic == true) 
	{
		//Sets repeat on or off
		if (aSound.myShouldRepeat)
		{
			aSound.mySound->setMode(FMOD_LOOP_NORMAL);
			aSound.mySound->setLoopCount(-1);
		}
		else
		{
			aSound.mySound->setMode(FMOD_LOOP_OFF);
		}

		//Checks which playing method to use
		if (!aSound.myShouldFadein)
		{
			if (aSound.myIsMusic)
			{
				GetInstance()->myMusicChannel->setPriority(0);
				GetInstance()->mySystem->playSound(aSound.mySound, 0, false, &GetInstance()->myMusicChannel);
				GetInstance()->myMusicChannel->setVolume(aSound.myVolume * GetInstance()->myVolume);
				GetInstance()->myMusicChannel->getIndex(&channelIndex);
				aSound.channelIndexes.push_back(channelIndex);
			}
			else
			{
				GetInstance()->myChannel->setPriority(256);
				GetInstance()->mySystem->playSound(aSound.mySound, 0, false, &GetInstance()->myChannel);
				GetInstance()->myChannel->setVolume(1.0 * (GetInstance()->myVolume * aSound.myVolume));
				GetInstance()->myChannel->getIndex(&channelIndex);
				aSound.channelIndexes.push_back(channelIndex);
			}
		}
		else
		{
			GetInstance()->PlaySoundWithFade(aSound);
		}
	}


}

void SoundEngine::AddToFadeInBuffer(Sound* aSound) 
{
	//Checks if the sound is being faded out and removes it 
	if (GetInstance()->mySoundsToFadeOut.size() > 0) 
	{
		for (int i = 0; i < GetInstance()->mySoundsToFadeOut.size(); i++)
		{
			if (aSound == GetInstance()->mySoundsToFadeOut[i]) 
			{
				GetInstance()->mySoundsToFadeOut.erase(GetInstance()->mySoundsToFadeOut.begin() + i);
			}
		}
	}

	GetInstance()->mySoundsToFadeIn.push_back(aSound);
}

void SoundEngine::PlaySoundWithFade(Sound &aSound)
{
	int channelIndex = -1;

	if (aSound.myIsMusic)
	{
		GetInstance()->myMusicChannel->setPriority(0);
		GetInstance()->mySystem->playSound(aSound.mySound, 0, false, &GetInstance()->myMusicChannel);
		GetInstance()->myMusicChannel->setVolume(0.0f);

		AddToFadeInBuffer(&aSound);

		GetInstance()->myMusicChannel->getIndex(&channelIndex);
		aSound.channelIndexes.push_back(channelIndex);
	}
	else
	{
		GetInstance()->myChannel->setPriority(256);
		GetInstance()->mySystem->playSound(aSound.mySound, 0, false, &GetInstance()->myChannel);
		GetInstance()->myChannel->setVolume(0.0f);

		AddToFadeInBuffer(&aSound);

		GetInstance()->myChannel->getIndex(&channelIndex);
		aSound.channelIndexes.push_back(channelIndex);
	}

}

void SoundEngine::SetMasterVolume(float aVolume)
{
	GetInstance()->myVolume = aVolume;
	UpdateVolume();
}

void SoundEngine::SetSoundVolume(Sound& aSound, const float& aVolume)
{
	aSound.myVolume = aVolume;
	UpdateVolume();
}

void SoundEngine::UpdateVolume()
{
	GetInstance()->CheckPlayingChannels();
	FMOD::Channel* pChannel;

	for (size_t i = 0; i < GetInstance()->myPlayingSounds.size(); i++)
	{
		for (size_t x = 0; x < GetInstance()->myPlayingSounds[i]->channelIndexes.size(); ++x)
		{
			GetInstance()->mySystem->getChannel(GetInstance()->myPlayingSounds[i]->channelIndexes[x], &pChannel);
			pChannel->setVolume(1.0f * (GetInstance()->myVolume * GetInstance()->myPlayingSounds[i]->myVolume));
		}
	}
}

const float SoundEngine::GetVolume()
{
	return GetInstance()->myVolume;
}

void SoundEngine::StopChannel(int aChannelIndex)
{
	bool isPlaying = false;
	FMOD::Channel *pChannel;

	GetInstance()->mySystem->getChannel(aChannelIndex, &pChannel);
	pChannel->isPlaying(&isPlaying);

	if (isPlaying) 
	{
		pChannel->stop();
	}
}

void SoundEngine::StopPlayingSound(Sound &aSound)	
{
	if (!aSound.myShouldFadeOut) 
	{
		for (int i = 0; i < aSound.channelIndexes.size(); i++)
		{
			bool isPlaying = false;
			FMOD::Channel *pChannel;

			GetInstance()->mySystem->getChannel(aSound.channelIndexes[i], &pChannel);
			pChannel->isPlaying(&isPlaying);

			if (aSound.myIsMusic) 
			{
				pChannel->setPriority(128);
			}

			pChannel->stop();
		}

		aSound.channelIndexes.clear();
	}
	else 
	{
		for (int i = 0; i < aSound.channelIndexes.size(); i++)
		{
			GetInstance()->AddToFadeOutBuffer(&aSound);
		}
	}

	if (aSound.myIsMusic)
	{
		for (size_t i = 0; i < GetInstance()->myPlayingSongs.size(); i++)
		{
			if (GetInstance()->myPlayingSongs[i] == &aSound) 
			{
				GetInstance()->myPlayingSongs.erase(GetInstance()->myPlayingSongs.begin() + i);
			}
		}
	}
	
}
#include <iostream>
void SoundEngine::AddToDeleteBuffer(Sound* aSound)
{
	for (int i = 0; i < GetInstance()->mySoundsToDelete.size(); ++i)
	{
		if (aSound == GetInstance()->mySoundsToDelete[i]) 
		{
			return;
		}
	}
	
	GetInstance()->mySoundsToDelete.push_back(aSound);
}

void SoundEngine::StopPlayingSoundInstantly(Sound &aSound)
{
	for (int i = 0; i < aSound.channelIndexes.size(); i++)
	{
		bool isPlaying = false;
		FMOD::Channel *pChannel;

		GetInstance()->mySystem->getChannel(aSound.channelIndexes[i], &pChannel);
		pChannel->isPlaying(&isPlaying);

		pChannel->stop();
		aSound.channelIndexes.erase(aSound.channelIndexes.begin() + i);
	}

	if (aSound.myIsMusic)
	{
		for (size_t i = 0; i < GetInstance()->myPlayingSongs.size(); i++)
		{
			if (GetInstance()->myPlayingSongs[i] == &aSound)
			{
				GetInstance()->myPlayingSongs.erase(GetInstance()->myPlayingSongs.begin() + i);
			}
		}
	}

	for (size_t i = 0; i < GetInstance()->myPlayingSounds.size(); i++)
	{
		if (GetInstance()->myPlayingSounds[i] == &aSound)
		{
			GetInstance()->myPlayingSounds.erase(GetInstance()->myPlayingSounds.begin() + i);
		}
	}

	aSound.channelIndexes.clear();

}

void SoundEngine::StopChannel(FMOD::Channel * aChannel)
{
	bool isPlaying = false;
	aChannel->isPlaying(&isPlaying);

	if (isPlaying)
	{
		aChannel->stop();
	}

}

void SoundEngine::StopAllChannels()
{
	GetInstance()->CheckPlayingChannels();

	//stop playing sound through ALL channels
	for (int i = 0; i < GetInstance()->myPlayingChannelIDs.size(); i++)
	{
		FMOD::Channel *pChannel;
		GetInstance()->myResult = GetInstance()->mySystem->getChannel(GetInstance()->myPlayingChannelIDs[i], &pChannel);
		
		pChannel->stop();
	}

}

void SoundEngine::ToggleDebugRender()
{
	GetInstance()->myShowDebug = !GetInstance()->myShowDebug;
}

void SoundEngine::DeleteSongsFromBuffer() 
{
	for (int i = 0; i < GetInstance()->mySoundsToDelete.size(); i++)
	{
		if(GetInstance()->mySoundsToDelete[i]->channelIndexes.size() == 0)
		{
			GetInstance()->ReleaseSound((GetInstance()->mySoundsToDelete[i])->mySound);
			delete GetInstance()->mySoundsToDelete[i];
			GetInstance()->mySoundsToDelete[i] = nullptr;
			GetInstance()->mySoundsToDelete.erase(GetInstance()->mySoundsToDelete.begin() + i);
		}
	}
}

void SoundEngine::Update(float aDeltaTime)
{
	GetInstance()->mySystem->update();

	if (GetInstance()->myVolume < 0.01 && GetInstance()->myVolume != 0)
	{
		GetInstance()->SetMasterVolume(0.0f);
	}

	for (int i = 0; i < GetInstance()->myPlayingSounds.size(); i++)
	{
		if (GetInstance()->myPlayingSounds[i]->channelIndexes.size() == 0) 
		{
			GetInstance()->myPlayingSounds.erase(GetInstance()->myPlayingSounds.begin() + i);
		}
	}

	//Checks if there is any channels to fade out this frame
	if (GetInstance()->mySoundsToFadeOut.size() > 0)
	{
		for (int i = 0; i < GetInstance()->mySoundsToFadeOut.size(); i++)
		{
			if (GetInstance()->FadeOutAndStopSound(GetInstance()->mySoundsToFadeOut[i], aDeltaTime)) 
			{
				GetInstance()->mySoundsToFadeOut[i]->channelIndexes.clear();
				GetInstance()->mySoundsToFadeOut.erase(GetInstance()->mySoundsToFadeOut.begin() + i);
			}
		}
	}

	//Checks if there is any channels to fade in this frame
	if (GetInstance()->mySoundsToFadeIn.size() > 0)
	{
		for (int i = 0; i < GetInstance()->mySoundsToFadeIn.size(); i++)
		{
			if (GetInstance()->FadeIn(GetInstance()->mySoundsToFadeIn[i], aDeltaTime)) 
			{
				GetInstance()->mySoundsToFadeIn.erase(GetInstance()->mySoundsToFadeIn.begin() + i);
			}
		}
	}

	if (GetInstance()->myShowDebug) 
	{
		GetInstance()->RenderDebugText();
	}

	GetInstance()->DeleteSongsFromBuffer();
}

bool SoundEngine::FadeOutAndStopSound(Sound * aSound, float aDeltaTime)
{
	FMOD::Channel* pChannel;

	for (size_t i = 0; i < aSound->channelIndexes.size(); i++)
	{
		int channelID = aSound->channelIndexes[i];
		mySystem->getChannel(channelID, &pChannel);

		float volume;
		pChannel->getVolume(&volume);

		volume -= aSound->myFadeSpeed * aDeltaTime;
		pChannel->setVolume(volume);

		if (volume <= 0)
		{
			volume = 0;
			pChannel->setVolume(volume);

			if (aSound->myIsMusic) 
			{
				pChannel->setPriority(128);
			}

			StopChannel(pChannel);

			aSound->channelIndexes.erase(aSound->channelIndexes.begin() + i);
		}

	}

	if (aSound->channelIndexes.size() == 0)
	{
		return true;
	}

	return false;
	
	
}

bool SoundEngine::FadeIn(Sound* aSound, float aDeltaTime)
{
	FMOD::Channel* pChannel;
	
	float volume;
	float maxVolume = 1.0f * (GetInstance()->myVolume * aSound->myVolume);

	mySystem->getChannel(aSound->channelIndexes[0], &pChannel);

	pChannel->getVolume(&volume);

	volume += aSound->myFadeSpeed * aDeltaTime;

	if (volume >= maxVolume)
	{
		volume = maxVolume;
		return true;
	}

	pChannel->setVolume(volume);

	return false;


}

void SoundEngine::PrintAllLoadedSounds() 
{
	std::cout << "Sounds loaded: " << std::endl;

	for (int i = 0; i < GetInstance()->mySoundNames.size(); i++)
	{
		std::cout << GetInstance()->mySoundNames[i] << std::endl;
	}
}

bool SoundEngine::CheckIfSoundIsPlaying(Sound & aSound)
{
	GetInstance()->CheckPlayingChannels();

	if (GetInstance()->myPlayingChannelIDs.size() > 0) 
	{
		for (int i = 0; i < GetInstance()->myPlayingChannelIDs.size(); i++)
		{
			for (int x = 0; x < aSound.channelIndexes.size(); x++)
			{
				if (aSound.channelIndexes[x] == GetInstance()->myPlayingChannelIDs[i]) 
				{
					return true;
				}
			}
		}
	}

	return false;
}

std::string SoundEngine::GetSoundPositionString(int aChannelIndex)
{
	unsigned int ms = 0;
	unsigned int lenms = 0;


	FMOD::Channel *pChannel;
	mySystem->getChannel(aChannelIndex, &pChannel);
	pChannel->getPosition(&ms, FMOD_TIMEUNIT_MS);

	FMOD::Sound *pSound;
	pChannel->getCurrentSound(&pSound);
	pSound->getLength(&lenms, FMOD_TIMEUNIT_MS);

	//Add which mode channel is in later perhaps?
	std::stringstream positionStream;
	positionStream << "[" << (ms / 1000 / 60) << ":" << (ms / 1000 % 60) << ":" << (ms / 10 % 100) << " / " << (lenms / 1000 / 60) << ":" << (lenms / 1000 % 60) << ":" << (lenms / 10 % 100) << "]";

	return positionStream.str();
}

int SoundEngine::GetSecondsPlayed(int aChannelIndex)
{
	unsigned int ms = 0;
	FMOD::Channel *pChannel;
	GetInstance()->mySystem->getChannel(aChannelIndex, &pChannel);
	pChannel->getPosition(&ms, FMOD_TIMEUNIT_MS);
	ms /= 1000;
	return ms;
}

//Updates the containers with playing sounds and active channels
void SoundEngine::CheckPlayingChannels()
{
	myPlayingChannelIDs.clear();
	myPlayingFileNames.clear();

	FMOD::Channel *pChannel;

	for (int i = 0; i < myNumberOfChannels; i++)
	{
		bool isPlaying = false;
		mySystem->getChannel(i, &pChannel); 
		pChannel->isPlaying(&isPlaying);

		if (isPlaying) 
		{
			FMOD::Sound *sound;
			const int length = 32;
			char name[length];

			int channelID = 0;
			pChannel->getIndex(&channelID);
			pChannel->getCurrentSound(&sound);
			sound->getName(name, length);

			myPlayingChannelIDs.push_back(channelID);
			myPlayingFileNames.push_back(name);
		}
	}
}

void SoundEngine::RenderDebugText()
{
	GetInstance()->CheckPlayingChannels();

	FMOD::Channel* pChannel; 
	GetInstance()->myTexts.clear();

	std::stringstream instanceStringStream;
	instanceStringStream << "There are (" << GetInstance()->myPlayingChannelIDs.size() << ") channel(s) playing " << "and (" << GetInstance()->myLoadedSounds.size() << ") sounds loaded.";
	GetInstance()->myText->myText = instanceStringStream.str();
	GetInstance()->myText->myPosition.Set(0.02f, 0.15f);
	GetInstance()->myText->Render();

	for (int i = 0; i < GetInstance()->myPlayingChannelIDs.size(); i++)
	{
		GetInstance()->mySystem->getChannel(GetInstance()->myPlayingChannelIDs[i], &pChannel);

		std::stringstream channelStringStream;
		float volume = 0; 
		pChannel->getVolume(&volume);
		channelStringStream << "Ch.ID: " << GetInstance()->myPlayingChannelIDs[i] << " is playing '" << GetInstance()->myPlayingFileNames[i] << "'" << " at " << (volume * 100) << "% volume" << "\n" << GetInstance()->GetSoundPositionString(GetInstance()->myPlayingChannelIDs[i]);

		GetInstance()->myText->myText = channelStringStream.str();
		GetInstance()->myText->myPosition.Set(0.02f, 0.20f + (0.075f * i));

		GetInstance()->myTexts.push_back(GetInstance()->myText);

		GetInstance()->myTexts[i]->Render();
	}
}

void SoundEngine::ReleaseSound(FMOD::Sound *aSound)
{

	const int length = 32;
	char name[length];

	aSound->getName(name, length);

	for (int i = 0; i < GetInstance()->mySoundNames.size(); i++)
	{
		if (GetInstance()->mySoundNames[i] == name) 
		{
			GetInstance()->mySoundNames.erase(GetInstance()->mySoundNames.begin() + i);
		}
	}

	for (int i = 0; i < GetInstance()->myLoadedSounds.size(); i++)
	{
		if (aSound == GetInstance()->myLoadedSounds[i])
		{
			GetInstance()->myLoadedSounds.erase(GetInstance()->myLoadedSounds.begin() + i);
		}
	}

	aSound->release();

}

void SoundEngine::AddToFadeOutBuffer(Sound* aSound)
{
	if (GetInstance()->mySoundsToFadeIn.size() > 0)
	{
		for (int i = 0; i < GetInstance()->mySoundsToFadeIn.size(); i++)
		{
			if (aSound == GetInstance()->mySoundsToFadeIn[i])
			{
				GetInstance()->mySoundsToFadeIn.erase(GetInstance()->mySoundsToFadeIn.begin() + i);
			}
		}
	}

	mySoundsToFadeOut.push_back(aSound);
}

Sound::Sound(FMOD::Sound * aSound)
{
	mySound = aSound;
}

Sound::Sound(const char * aPath)
{
	mySound = SoundEngine::AddSoundFile(aPath);
}

void Sound::Destroy()
{
	SoundEngine::AddToDeleteBuffer(this);
}

void Sound::Annahilate()
{
	SoundEngine::ReleaseSound(this->mySound);
	SoundEngine::StopPlayingSoundInstantly(*this);

	for (int i = 0; i < SoundEngine::GetInstance()->mySoundsToFadeIn.size(); i++)
	{
		if (this == SoundEngine::GetInstance()->mySoundsToFadeIn[i]) 
		{
			SoundEngine::GetInstance()->mySoundsToFadeIn.erase(SoundEngine::GetInstance()->mySoundsToFadeIn.begin() + i);
		}
	}

	for (int i = SoundEngine::GetInstance()->mySoundsToFadeOut.size(); i < 0; --i)
	{
		if (this == SoundEngine::GetInstance()->mySoundsToFadeIn[i-1])
		{
			SoundEngine::GetInstance()->mySoundsToFadeIn.erase(SoundEngine::GetInstance()->mySoundsToFadeIn.begin() + i - 1);
		}
	}

	delete this;
}

void Sound::SetAsGenericMusic()
{
	myShouldFadeOut = true;
	myShouldFadein = true;
	myShouldRepeat = true;
	myIsMusic = true;

	myFadeSpeed = 0.25f;
}

bool Sound::IsBeingPlayed()
{
	if (channelIndexes.size() > 0) 
	{
		return true;
	}

	return false;
}

Sound::~Sound()
{
}

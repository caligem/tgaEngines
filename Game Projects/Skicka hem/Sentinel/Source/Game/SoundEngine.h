#pragma once
#include "fmod.hpp"
#include <vector>
#include <string>
#include <sstream>

#include "tga2d\sprite\sprite.h"
#include "tga2d\text\text.h"

//FMOD had already defined PlaySound for some reason
#undef PlaySound



class Sound 
{
public:
	Sound() = default;
	Sound(FMOD::Sound * aSound);
	Sound(const char* aPath);

	void Destroy();
	void Annahilate();
	void SetAsGenericMusic();
	bool IsBeingPlayed();

    ~Sound();
	FMOD::Sound *mySound;

	float myFadeSpeed = 0.5f;
	float myVolume = 1.0;

	bool myShouldFadeOut = false;
	bool myShouldFadein = false;
	bool myShouldRepeat = false;
	bool myIsMusic = false;

	std::vector<int> channelIndexes;

};

class SoundEngine
{
public:
	~SoundEngine();

	static void Init();

	static Sound * GetClickSound();

	static Sound * GetHoverSound();

	//returns a FMOD-sound pointer
	static FMOD::Sound *AddSoundFile(const char* aPath);
	static void PlaySound(Sound & aSound);

	static void AddToFadeInBuffer(Sound * aSound);

	static void SetMasterVolume(float aVolume);
	static void SetSoundVolume(Sound & aSound, const float & aVolume);
	static const float GetVolume();

	static void StopChannel(int aChannelIndex);
	static void StopChannel(FMOD::Channel *aChannel);
	
	static void StopPlayingSound(Sound &aSound);

	static void AddToDeleteBuffer(Sound* aSound);

	static void StopPlayingSoundInstantly(Sound & aSound);
	
	static void ToggleDebugRender();
	static void Update(float aDeltaTime);
	
	static int GetSecondsPlayed(int aChannelIndex);
	static void PrintAllLoadedSounds();

	static bool CheckIfSoundIsPlaying(Sound& aSound);

	Sound* myClickSound;
	Sound* myHoveredSound;


private:
	friend class Sound;
	SoundEngine();

	static void DeleteSongsFromBuffer();
	static void ReleaseSound(FMOD::Sound *aSound);
	static void UpdateVolume();
	static void StopAllChannels();

	static SoundEngine *GetInstance();

	//Private methods
	std::string GetSoundPositionString(int aChannelIndex);
	void CheckPlayingChannels();

	bool FadeIn(Sound * aSound, float aDeltaTime);


	void AddToFadeOutBuffer(Sound * aSound);
	bool FadeOutAndStopSound(Sound * aSound, float aDeltaTime);

	void RenderDebugText();

	void PlaySoundWithFade(Sound &aSound);

	FMOD::System     *mySystem;
	FMOD::Channel    *myChannel;

	FMOD::Channel *myMusicChannel;

	FMOD_RESULT       myResult;

	std::vector<int>			myPlayingChannelIDs;
	std::vector<std::string>	myPlayingFileNames;

	std::vector<Sound*> mySoundsToFadeOut;
	std::vector<Sound*>	mySoundsToFadeIn;

	std::vector<Sound*>			myPlayingSongs;
	std::vector<Sound*>			myPlayingSounds;
	std::vector<FMOD::Sound*>			myLoadedSounds;

	std::vector<Tga2D::CText*>	myTexts;

	std::vector<std::string>	mySoundNames;

	Tga2D::CText*		myText;

	std::vector<Sound*> mySoundsToDelete;

	void*				 myExtraDriverData;

	unsigned int		 myVersion;
	int					 myNumberOfChannels;

	bool				 myShowDebug;
	float				 myVolume;


};


#include "stdafx.h"
#include "SplashScreenState.h"
#include "InputWrapper.h"
#include "Timer.h"
#include <tga2d\sprite\sprite.h>
#include "cit\utility.h"
#include "Fader.h"
#include <tga2d\engine.h>
SplashScreenState::SplashScreenState()
{
	myFadeSkipTime = 1.3f;
	myFadeSpeed = 0.7f;
	myTransitionToMenuFadeSpeed = 0.25f;
	myOriginalWatchTime = 4.0f;
}

SplashScreenState::~SplashScreenState()
{
}

void SplashScreenState::Init()
{
}

void SplashScreenState::Render()
{
	myBG->Render();
	myActiveLogo->Render();
	Fader::Render();
}
#undef GetCurrentTime
eStateStackMessage SplashScreenState::Update()
{
	while (ShowCursor(false) >= 0);
	mySwapTimer.Update(CommonUtilities::Timer::GetDeltaTime());
	if (Fader::GetState() == FadeState::Invisable)
	{
		if (InputWrapper::IsButtonPressed(KeyButton::Jump))
		{
			mySwapTimer.Set(myFadeSkipTime, cit::countdown::type::autoreset, [&] {Swap(); });
			mySwapTimer.Start();
		}
		if (mySwapTimer.GetCurrentTime() <= myFadeSkipTime)
		{
			Fader::FadeIn(FadeColor::Black, myFadeSpeed);
		}
	}
	if (myFinished == false)
	{
		return eStateStackMessage::KeepState;
	}
	else
	{
		return eStateStackMessage::PopSubState;
	}
}

void SplashScreenState::OnEnter()
{
	myFinished = false;
	myBG = new Tga2D::CSprite("sprites/white_pixel.dds");
	myBG->SetPivot({ 0.5f, 0.5f });
	myBG->SetPosition({ 0.5f, 0.5f });
	myBG->SetColor({ 0.0f, 0.0f, 0.0f, 1.0f });
	myBG->SetSizeRelativeToScreen({ 4.0f, 4.0f });
	myTgaLogo = new Tga2D::CSprite("sprites/menu/logoTGA.dds");
	myTgaLogo->SetPivot({ 0.5f, 0.5f });
	myTgaLogo->SetPosition({ 0.5f, 0.5f });
	myStudioLogo = new Tga2D::CSprite("sprites/menu/logoHookshot.dds");
	myStudioLogo->SetPivot({ 0.5f, 0.5f });
	myStudioLogo->SetPosition({ 0.5f, 0.5f });
	myActiveLogo = myTgaLogo;
	mySwapTimer.Set(myOriginalWatchTime, cit::countdown::type::autoreset, [&] {Swap(); });
	mySwapTimer.Start();
	Fader::FadeOut(FadeColor::Black, myFadeSpeed);
}

void SplashScreenState::OnExit()
{
	cit::SafeDelete(myTgaLogo);
	cit::SafeDelete(myStudioLogo);
	cit::SafeDelete(myBG);
	Fader::FadeOut(FadeColor::Black, myTransitionToMenuFadeSpeed);
}

void SplashScreenState::Swap()
{
	mySwapTimer.Set(myOriginalWatchTime, cit::countdown::type::autoreset, [&] {Swap(); });
	mySwapTimer.Start();
	if (myActiveLogo == myTgaLogo)
	{
		myActiveLogo = myStudioLogo;
		Fader::FadeOut(FadeColor::Black, myFadeSpeed);
	}
	else if (myActiveLogo == myStudioLogo)
	{
		myFinished = true;
	}
}

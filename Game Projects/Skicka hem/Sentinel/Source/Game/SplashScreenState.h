#pragma once
#include "Vector2.h"
#include "GameState.h"
#include "cit\countdown.h"

namespace Tga2D
{
	class CSprite;
}

class SplashScreenState : public GameState
{
public:
	SplashScreenState();
	~SplashScreenState();

	void Init() override;
	void Render() override;
	eStateStackMessage Update() override;

	void OnEnter() override;
	void OnExit() override;

private:
	bool myFinished;
	void Swap();
	Tga2D::CSprite* myActiveLogo;
	Tga2D::CSprite* myTgaLogo;
	Tga2D::CSprite* myStudioLogo;
	Tga2D::CSprite* myBG;
	cit::countdown mySwapTimer;
	float myTransitionToMenuFadeSpeed;
	float myFadeSkipTime;
	float myFadeSpeed;
	float myOriginalWatchTime;
};


#include "stdafx.h"
#include "Sprite.h"
#include <tga2d\sprite\sprite.h>
#include "TileSize.h"
#include "cit\utility.h"
Sprite::Sprite()
{
	myLayer = 1;
	myShouldDrawThisFrame = true;
	mySprite = nullptr;
}

Sprite::~Sprite()
{
	cit::SafeDelete(mySprite);
}

void Sprite::Init(const char * aPath)
{
	mySprite = new Tga2D::CSprite(aPath);
	mySprite->SetPivot({ 0.5f, 0.5f });
	mySprite->SetSizeRelativeToImage({ TILE_MULTIPLIER, TILE_MULTIPLIER });
}

void Sprite::Render()
{
	if (myShouldDrawThisFrame == true)
	{
		mySprite->SetPosition({ myPosition.x, myPosition.y });
		mySprite->Render();
	}
}

void Sprite::SetColor(const CommonUtilities::Vector4f& aColor) 
{
	mySprite->SetColor({aColor.x, aColor.y, aColor.z, aColor.w});
}

const CommonUtilities::Vector2<unsigned int> Sprite::GetImagesSizePixel() const
{
	return{ mySprite->GetImageSize().x, mySprite->GetImageSize().y };
}

void Sprite::SetSizeRelativeToScreen(const CommonUtilities::Vector2f & aSize)
{
	mySprite->SetSizeRelativeToScreen({aSize.x, aSize.y });
}

void Sprite::SetSizeRelativeToImage(const CommonUtilities::Vector2f & aSize)
{
	mySprite->SetSizeRelativeToImage({ aSize.x, aSize.y });
}

bool Sprite::GetIsFlipped() const
{
	return mySprite->GetSize().x < 0.0f;
}

void Sprite::SetRotation(const float aRotation)
{
	mySprite->SetRotation(aRotation);
}

void Sprite::SetPivot(CommonUtilities::Vector2f & aPivotPosition)
{
	mySprite->SetPivot({ aPivotPosition.x, aPivotPosition.y });
}

void Sprite::SetInversedX(bool aValue)
{
	if ((aValue == true && mySprite->GetSize().x > 0.0f) || (aValue == false && mySprite->GetSize().x < 0.0f))
	{
		mySprite->SetSizeRelativeToScreen({ mySprite->GetSize().x * -1.0f, mySprite->GetSize().y });
	}
}


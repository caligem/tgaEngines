#pragma once
#include "Drawable.h"

class Sprite : public Drawable
{
public:
	Sprite();
	~Sprite() override;
	void Init(const char* aPath) override;
	void Render() override;

	void SetColor(const CommonUtilities::Vector4f & aColor);

	const CommonUtilities::Vector2<unsigned int> GetImagesSizePixel() const override;
	void SetSizeRelativeToScreen(const CommonUtilities::Vector2f & aSize) override;
	void SetSizeRelativeToImage(const CommonUtilities::Vector2f & aSize);
	bool GetIsFlipped() const override;
	void SetRotation(const float aRotation) override;
	void SetZ(float aZValue) { myZ = aZValue; }
	const float GetZ() { return myZ; }
	void SetPivot(CommonUtilities::Vector2f& aPivotPosition);
	void SetInversedX(bool aValue);

private:
	float myZ;
	Tga2D::CSprite *mySprite;
};
#include "stdafx.h"
#include "StateStack.h"
#include "GameState.h"

StateStack::StateStack()
{
	myGameStates.Init(8);

	myGameProgressData = LoadGameProgress();
}


StateStack::~StateStack()
{
}

void StateStack::PushSubState(GameState* aState)
{
	myGameStates.GetLast().Add(aState);
	aState->AttachStateStack(this);
	aState->OnEnter();
}

void StateStack::PushMainState(GameState * aState)
{
	myGameStates.Add(CommonUtilities::GrowingArray<GameState*>());
	myGameStates.GetLast().Init(8);
	myGameStates.GetLast().Add(aState);
	aState->AttachStateStack(this);
	aState->OnEnter();

}

void StateStack::PopSubState()
{
	myGameStates.GetLast().GetLast()->OnExit();
	myGameStates.GetLast().DeleteCyclicAtIndex(myGameStates.GetLast().Size()-1);
}

void StateStack::PopMainState()
{
	unsigned short quantityOfSubStates = myGameStates.GetLast().Size() - 1;
	
	for (short index = quantityOfSubStates; index >= 0; --index)
	{
		myGameStates.GetLast()[index]->OnExit();
		myGameStates.GetLast().DeleteCyclicAtIndex(index);
	}
	myGameStates.RemoveCyclicAtIndex(static_cast<unsigned short>(GetLastIndex()));
}

void StateStack::Render()
{
	RenderGameStateAtIndex(myGameStates.GetLast().Size() - 1);
}

GameState * StateStack::GetInGameState()
{
	unsigned short quantityOfSubStates = myGameStates.GetLast().Size() - 1;
	for (short index = quantityOfSubStates; index >= 0; --index)
	{
		GameState* currentState = dynamic_cast<InGameState*>(myGameStates.GetLast()[index]);

		if (currentState != nullptr)
		{
			return currentState;
		}
		else
		{
			continue;
		}
	}

	return nullptr;
}

void StateStack::RenderGameStateAtIndex(int aIndex)
{
	if (aIndex < 0)
	{
		return;
	}

	if (myGameStates.GetLast()[static_cast<unsigned short>(aIndex)]->LetThroughRender())
	{
		RenderGameStateAtIndex(aIndex - 1);
	}

	myGameStates.GetLast()[static_cast<unsigned short>(aIndex)]->Render();
}

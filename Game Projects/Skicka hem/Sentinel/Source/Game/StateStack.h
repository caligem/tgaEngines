#pragma once
#include "GameState.h"
#include "InGameState.h"
#include "MainMenuState.h"
#include "OptionState.h"
#include "PauseMenuState.h"
#include "LevelSelectState.h"
#include "CreditsState.h"
#include "CutsceneState.h"
#include "LevelCompletedState.h"
#include "GameProgressData.h"
#include "DanceGameState.h"
#include "WarpZoneState.h"
#include "PromptState.h"
#include <GrowingArray.h>
#include <map>

class GameState;

class StateStack
{
public:
	StateStack();
	~StateStack();

	struct OptionsData
	{
		int myVolume = 50;
		int myResolutionIterator = 0;
		bool myIsFullscreen = false;
	} myOptionsData;

	GameProgressData myGameProgressData;

	void PushSubState(GameState* aState);
	void PushMainState(GameState* aState);

	void PopSubState();
	void PopMainState();
	
	void Render();

	inline const int GetLastIndex() const { return myGameStates.Size()-1; }
	inline GameState* GetCurrentState() { return myGameStates.GetLast().GetLast(); };
	inline const int GetSize() const { return myGameStates.Size(); }

	GameState* GetInGameState();

private:

	void RenderGameStateAtIndex(int aIndex);
	GameState* myCurrentState;
	CommonUtilities::GrowingArray<CommonUtilities::GrowingArray<GameState*>> myGameStates;
};


#include "stdafx.h"
#include "Subject.h"

Subject::Subject()
{
	myObservers.Init(16);
}


Subject::~Subject()
{
}

void Subject::AttachObserver(Observer* aObserver)
{
	myObservers.Add(aObserver);
}

void Subject::SendMessageToObserver(const eMessageType& aMessage, const int aIndex)
{
	for (unsigned short i = 0; i < myObservers.Size(); i++)
	{
		myObservers[i]->RecieveMessage(aMessage, aIndex);
	}
}

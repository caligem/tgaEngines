#pragma once

#include <GrowingArray.h>

#include "Observer.h"

class Subject
{
public:
	Subject();
	virtual ~Subject();

	void AttachObserver(Observer* aObserver);
	void SendMessageToObserver(const eMessageType& aMessage, const int aIndex);

private:
	CommonUtilities::GrowingArray<Observer*> myObservers;
};


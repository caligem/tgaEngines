#pragma once

enum class MessageType
{
	PlayerDeath,
	LevelCompleted,
	ChangeLevel,
	PushWarpZone,
	PushDanceGame,
	PopWarpZone,
	FinishedWarpZone,
	ExitedWarpZone,
	UnlockCollectible,
	COUNT
};

struct Message
{
	MessageType myMessageType;
};



class Subscriber
{
public:
	Subscriber();
	~Subscriber();

	virtual void ReceiveMessage(const Message aMessage);
};


#include "stdafx.h"
#include "Text.h"
#include <tga2d\text\text.h>
#include "cit/utility.h"

Text::Text()
{
	SetLayer(1);
	myShouldDrawThisFrame = true;
	myText = nullptr;
}

Text::~Text()
{
	cit::SafeDelete(myText);
}

void Text::Init(const char * aPath)
{
	myText = new Tga2D::CText(aPath);
}

void Text::Init(const char * aPath, EFontSize aFontSize, const int aBorderSize)
{
	myText = new Tga2D::CText(aPath, static_cast<Tga2D::EFontSize>(aFontSize), aBorderSize);
}

void Text::Render()
{
	myText->myPosition = { myPosition.x - myText->GetWidth() / 2.f, myPosition.y };

	if (myShouldDrawThisFrame)
	{
		myText->Render();
	}
}

void Text::SetScale(float aScale)
{
	myText->myScale = aScale;
}

const float Text::GetScale()
{
	return myText->myScale;
}

void Text::SetText(std::string aText)
{
	myText->myText = aText;
}

void Text::SetColor(CommonUtilities::Vector4f& aColor)
{
	myText->myColor.Set(aColor.x, aColor.y, aColor.z, aColor.w);
}

void Text::SetRotation(const float aRotation)
{
	assert(false && "Attempted to rotate text...");
}

#pragma once
#include "Drawable.h"
namespace Tga2D
{
	class CText;
}
class Text : public Drawable
{
public:
	enum EFontSize
	{
		EFontSize_6 = 6,
		EFontSize_8 = 8,
		EFontSize_9 = 9,
		EFontSize_10 = 10,
		EFontSize_11 = 11,
		EFontSize_12 = 12,
		EFontSize_14 = 14,
		EFontSize_18 = 18,
		EFontSize_24 = 24,
		EFontSize_30 = 30,
		EFontSize_36 = 36,
		EFontSize_48 = 48,
		EFontSize_60 = 60,
		EFontSize_72 = 72,
		EFontSize_Count
	};

	Text();
	~Text() override;
	void Init(const char* aPath) override;
	void Init(const char* aPath, EFontSize aFontSize, const int aBorderSize = 1);
	void Render() override;
	
	void SetScale(float aScale);
	const float GetScale();
	void SetText(std::string aText);
	void SetColor(CommonUtilities::Vector4f& aColor);

	//not implemented but is pure virtual in Base Class
	const CommonUtilities::Vector2<unsigned int> GetImagesSizePixel() const override { return { 0, 0 }; }
	void SetSizeRelativeToScreen(const CommonUtilities::Vector2f & aSize) override {}
	bool GetIsFlipped() const override { return false; }
	void SetRotation(const float aRotation) override;

private:
	Tga2D::CText *myText;
};
#include "stdafx.h"
#include "TextSlide.h"
#include "JsonWrapper.h"
#include "tga2d\engine.h"
#include "SoundEngine.h"

#define FONT_SIZE Tga2D::EFontSize_30
#define LARGER_FONT_SIZE Tga2D::EFontSize_36
#define FONT_PATH "Text/Mecha.ttf"


TextSlide::TextSlide(Sound * aPrintSound)
{
	mySound = aPrintSound;
}

TextSlide::~TextSlide()
{
	//for (size_t i = 0; i < myPrintingTexts.size(); ++i)
	//{
	//	delete myPrintingTexts[i];
	//	myPrintingTexts[i] = nullptr;
	//}

	//delete myNameText;
	//myNameText = nullptr;
}

void TextSlide::Init(Vector2<float> aPosition, std::string& aString, std::string aName)
{
	//Very specific and wierd value, change with absolute caution please
	myMaxWidth = 0.52f;
	myTextWidthMultiplier = 1.35f;
	myIsASwitch = false;

	json dialogueData = LoadDialogueData();
	json colorData = dialogueData["textColor"];
	
	myColor = Tga2D::CColor(colorData["R"], colorData["G"], colorData["B"], 1.0f);

	myPrintSpeed = dialogueData["printSpeed"];
	myMaxPrintTimer = 0.05f / myPrintSpeed;
	myOriginalMaxPrintTimer = myMaxPrintTimer;

	

	Tga2D::CText* tempText = new Tga2D::CText(FONT_PATH, FONT_SIZE, 0);
	myStringsToPrint = BreakString(aString, tempText);

	float scale = Tga2D::CEngine::GetInstance()->GetWindowSize().x / 1920.f;

	for (size_t i = 0; i < myStringsToPrint.size(); i++)
	{
		Tga2D::CText* printingText = new Tga2D::CText(FONT_PATH, FONT_SIZE, 0);
		printingText->myText.clear();
		printingText->myPosition.Set(myOriginalPosition.x, myOriginalPosition.y);
		printingText->myColor = myColor;
		printingText->myScale = scale;

		myPrintingTexts.push_back(printingText);
	}

	myPrintTimer = 0;
	myPrintPosition = 0;
	myCurrentTextIndex = 0;

	myPosition = aPosition;
	myOriginalPosition = myPosition;


	myNameText = new Tga2D::CText(FONT_PATH, FONT_SIZE, 0);
	myNameText->myText = aName + ":";
	myNameText->myPosition.Set(myOriginalPosition.x - ((myNameText->GetWidth() * myTextWidthMultiplier) / 2), myOriginalPosition.y - 0.05f);
	myNameText->myScale = scale;
	myNameText->myColor = Tga2D::CColor(myColor.myR * 0.75f, myColor.myG * 0.75f, myColor.myB * 0.75f, myColor.myA);
	

	dialogueData.clear();
}

void TextSlide::ReInit()
{
	float scale = Tga2D::CEngine::GetInstance()->GetWindowSize().x / 1920.f;

	for (int i = 0; i < myPrintingTexts.size(); i++)
	{
		myPrintingTexts[i]->myText.clear();
		myPrintingTexts[i]->myScale = scale;
		myPrintingTexts[i]->myPosition.Set(myOriginalPosition.x, myOriginalPosition.y);
		myPrintingTexts[i]->myColor = myColor;
	}

	myPosition = myOriginalPosition;

	myPrintTimer = 0;
	myPrintPosition = 0;
	myCurrentTextIndex = 0;

	myNameText->myPosition.Set((myOriginalPosition.x - (((myNameText->GetWidth() * scale) * myTextWidthMultiplier) / 2) - 0.005f), myOriginalPosition.y - 0.05f);
	myNameText->myScale = scale;
	

}

void TextSlide::SetSwitch(bool aBool)
{
	myIsASwitch = aBool;
}

bool TextSlide::IsSwitch()
{
	return myIsASwitch;
}


bool TextSlide::IsSlideDone()
{
	if (myPrintingTexts[myPrintingTexts.size() - 1]->myText == myStringsToPrint[myStringsToPrint.size() - 1])
	{
		return true;
	}

	return false;
}

void TextSlide::Render()
{
	myNameText->Render();

	for (int i = 0; i < myPrintingTexts.size(); i++)
	{
		myPrintingTexts[i]->Render();
	}
}

void TextSlide::Update(float aDeltaTime) 
{
	myPrintTimer += aDeltaTime;

	if (myPrintingTexts[myCurrentTextIndex]->myText.size() != myStringsToPrint[myCurrentTextIndex].size())
	{
		Print(myStringsToPrint[myCurrentTextIndex], myPrintingTexts[myCurrentTextIndex]);
		UpdateTextPosition(myCurrentTextIndex);
	}
	else 
	{
		if (myCurrentTextIndex < myPrintingTexts.size() - 1)
		{
			++myCurrentTextIndex;
			myPrintPosition = 0;
		}
	}
}

void TextSlide::UpdateTextPosition(int aIndex) 
{
	myPosition.x = myOriginalPosition.x - ((myPrintingTexts[aIndex]->GetWidth() * myTextWidthMultiplier) / 2);
	myPosition.y = myOriginalPosition.y + (0.05f) * aIndex;
	myPrintingTexts[aIndex]->myPosition.Set(myPosition.x, myPosition.y);
}

void TextSlide::Print(std::string aTextToPrint, Tga2D::CText* aTextToPrintTo) 
{
	if (myPrintTimer >= myMaxPrintTimer) 
	{
		if (aTextToPrint.at(myPrintPosition) == ',' || aTextToPrint.at(myPrintPosition) == '.' || aTextToPrint.at(myPrintPosition) == '?' || aTextToPrint.at(myPrintPosition) == '!')
		{
			myMaxPrintTimer = myOriginalMaxPrintTimer * 10.0f;
		}
		else
		{
			myMaxPrintTimer = myOriginalMaxPrintTimer;
		}

		aTextToPrintTo->myText += aTextToPrint.at(myPrintPosition);
		myPrintTimer = 0;

		SoundEngine::PlaySound(*mySound);
		myPrintPosition++;
	}

}

void TextSlide::PrintAll()
{
	for (int i = 0; i < myStringsToPrint.size(); i++)
	{
		myPrintingTexts[i]->myText = myStringsToPrint[i];
		UpdateTextPosition(i);
	}
}

int TextSlide::GetTextposition()
{
	return myPrintPosition;
}

//Breaks down the text into substrings
std::vector<std::string> TextSlide::BreakString(std::string aString, Tga2D::CText* aText)
{
	std::vector<std::string> vectorToReturn;

	std::string string = aString;

	aText->myText = string;
	float charWidth = (aText->GetWidth() * myTextWidthMultiplier) / aText->myText.size();

	int timesToRun = static_cast<int>(((charWidth * string.size()) / myMaxWidth));
	int widthCharIndex = static_cast<int>(myMaxWidth / charWidth);
	int breakPoint = 0;

	int pastBreakPoint = 0;

	for (int i = 0; i < timesToRun; i++)
	{
		breakPoint += widthCharIndex;

		while (string.at(breakPoint) != ' ' && breakPoint > 0)
		{
			breakPoint--;
		}

		//string.insert(breakPoint + 1, "\n");

		std::string stringToInsert = string.substr(0, breakPoint);
		vectorToReturn.push_back(stringToInsert);

		pastBreakPoint = breakPoint;

		string = string.substr(breakPoint, string.size());
		breakPoint = 0;

	}


	if (timesToRun == 0) 
	{
		vectorToReturn.push_back(string);
	}
	else 
	{
		//insert the rest of the string
		std::string stringToInsert = string.substr(breakPoint, string.size());
		vectorToReturn.push_back(stringToInsert);
	}

	myTextWidth = aText->GetWidth() * myTextWidthMultiplier;

	return vectorToReturn;

}

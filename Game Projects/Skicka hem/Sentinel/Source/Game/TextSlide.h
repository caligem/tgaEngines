#pragma once
#include "tga2d\sprite\sprite.h"
#include "tga2d\text\text.h"
#include <string>
#include <sstream>
#include "Vector2.h"
#include <vector>

class Sound;

using namespace CommonUtilities;


class TextSlide
{
public:
	TextSlide(Sound* aPrintSound);
	~TextSlide();

	void SetSwitch(bool aBool);

	bool IsSwitch();
	bool IsSlideDone();
	void Init(Vector2<float> aPosition, std::string& aString, std::string aName);

	void ReInit();

	void Render();
	void Update(float aDeltaTime);

	void Print(std::string aTextToPrint, Tga2D::CText * aTextToPrintTo);
	void PrintAll();

private:

	void UpdateTextPosition(int aIndex);
	int GetTextposition();
	std::vector<std::string> BreakString(std::string aString, Tga2D::CText* aText);

	std::vector<std::string> myStringsToPrint;
	std::vector<Tga2D::CText*> myPrintingTexts;

	std::vector<Vector2<float>> myTextPositions;

	Vector2<float> myPosition;
	Vector2<float> myOriginalPosition;

	Tga2D::CText* myNameText;
	Tga2D::CColor myColor;


	float myMaxWidth;
	float myTextWidth;
	float myPrintTimer;
	float myMaxPrintTimer;
	float myOriginalMaxPrintTimer;
	float myTextWidthMultiplier;

	float myPrintSpeed;

	int myPrintPosition;
	int myCurrentTextIndex;

	bool myIsASwitch;

	Sound* mySound;
};


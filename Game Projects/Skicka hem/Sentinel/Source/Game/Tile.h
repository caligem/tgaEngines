#pragma once
#include "GameObject.h"

class Tile : public GameObject
{
public:
	Tile();

	~Tile()override;
	void OnUpdate() override;
	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionStay(Collider  &other) override;
	void OnCollisionLeave(Collider  &other) override;

};


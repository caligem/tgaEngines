#include "stdafx.h"
#include "Trap.h"
#include "Player.h"
#include "Camera.h"
Trap::Trap()
{
}


Trap::~Trap()
{
}

void Trap::Init()
{
	myCollider->SetIsSolid(false);
	myIsActivated = true;
}


void Trap::OnUpdate()
{
	myDrawable->SetShouldRender(myIsActivated);
	myDrawable->SetPosition({ myPosition.x, myPosition.y });
}
void Trap::OnCollisionEnter(Collider & other)
{
}

void Trap::OnCollisionStay(Collider & other)
{
}

void Trap::OnCollisionLeave(Collider & other)
{
}

void Trap::OnTriggerd()
{
	myIsActivated = true;
}

void Trap::DeActivate()
{
	myIsActivated = false;
}

bool Trap::IsActivated()
{
	return myIsActivated;
}

void Trap::FillCollisionBuffer(CommonUtilities::GrowingArray<GameObject*>& aBufferToAddTo)
{
	if (myIsActivated)
	{
		aBufferToAddTo.Add(this);
	}
}

#pragma once
#include "GameObject.h"

class Trap :
	public GameObject
{
public:
	Trap();
	~Trap();
	void Init();
	void OnUpdate() override;
	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionStay(Collider  &other) override;
	void OnCollisionLeave(Collider  &other) override;
	void OnTriggerd() override;
	void DeActivate();
	bool IsActivated();

	void FillCollisionBuffer(CommonUtilities::GrowingArray<GameObject *> &aBufferToAddTo) override;
private:
	bool myIsActivated;
};


#include "stdafx.h"
#include "Trigger.h"
#include "DialogueManager.h"
#include "JsonWrapper.h"
#include "Animation.h"
#include "Player.h"
#include <string>

#include "SoundEngine.h"
#include "InputWrapper.h"


Trigger::Trigger(eTriggerType aTriggerType)
	: myHaveBeenTriggerd(false)
	, myShouldRenderButton(false)
{
	myTriggerType = aTriggerType;
}


Trigger::~Trigger()
{
 	myDoorSound->Destroy();
 	myPlatformSound->Destroy();
 	myDialogueSound->Destroy();
}

void Trigger::Init(const char * aTriggerTypePath)
{
	if (aTriggerTypePath != nullptr)
	{
		json checkPointData = LoadObjectType(aTriggerTypePath);
		
		myTriggeredAnimation = new Animation();
		myTriggeredAnimation->Init(checkPointData["activatedAnimation"].get<std::string>().c_str());
		myTriggeredAnimation->SetLayer(myDrawable->GetLayer());

		myXButtonAnimation = new Animation();
		myXButtonAnimation->Init(checkPointData["button_animation_x"].get<std::string>().c_str());
		myXButtonAnimation->SetLayer(myDrawable->GetLayer() + 1);

		myCButtonAnimation = new Animation();
		myCButtonAnimation->Init(checkPointData["button_animation_c"].get<std::string>().c_str());
		myCButtonAnimation->SetLayer(myDrawable->GetLayer() + 1);

		myTriggeredAnimation->SetPosition(myPosition);
		myTriggeredAnimation->SetShouldRender(true);
	}


	myDoorSound = new Sound("Sounds/Sfx/door_trigger.wav");
	myPlatformSound = new Sound("Sounds/Sfx/platform_trigger.wav");
	myDialogueSound = new Sound("Sounds/Sfx/dialogue_trigger.wav");

	myCollider->SetIsSolid(false);
}

void Trigger::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	if (myHaveBeenTriggerd && myTriggerType == eTriggerType::DoorTrigger)
	{
		myTriggeredAnimation->SetPosition(myPosition);
		aRenderBuffer.Add(myTriggeredAnimation);
	}
	else
	{
		aRenderBuffer.Add(myDrawable);
	}

	if (myShouldRenderButton)
	{
		if (InputWrapper::GetInputType() == InputType::Controller)
		{
			myXButtonAnimation->SetPosition({ myPosition.x, myPosition.y - 0.05f });
			aRenderBuffer.Add(myXButtonAnimation);
		}
		else
		{
			myCButtonAnimation->SetPosition({ myPosition.x, myPosition.y - 0.05f });
			aRenderBuffer.Add(myCButtonAnimation);
		}

		myShouldRenderButton = false;
	}
}

void Trigger::OnCollisionEnter(Collider & other)
{
 	
}

void Trigger::OnCollisionStay(Collider & other)
{
	if (!myHaveBeenTriggerd)
	{
		if (other.GetTag() == ColliderTag::Player)
		{
			if (myTriggerType == eTriggerType::DoorTrigger)
			{
				myShouldRenderButton = true;

				if (InputWrapper::IsButtonPressed(KeyButton::Use))
				{
					SendAMessage();
					static_cast<Player*>(other.GetOwner())->SetAnimationStateToIdle();
				}
			}
			else if (myTriggerType == eTriggerType::DialogTrigger)
			{
				SendAMessage();
				static_cast<Player*>(other.GetOwner())->SetAnimationStateToIdle();
			}
		}
	}

}

void Trigger::SendAMessage()
{
	myHaveBeenTriggerd = true;

	if (myTriggerType == eTriggerType::DoorTrigger)
	{
		TriggerManager::GetInstance()->TriggerAtIndex(myTriggerIndex);
		SoundEngine::PlaySound(*myDoorSound);
	}
	if (myTriggerType == eTriggerType::PlatformTrigger)
	{
		TriggerManager::GetInstance()->TriggerAtIndex(myTriggerIndex, myDirection);
		SoundEngine::PlaySound(*myPlatformSound);
	}
	else if (myTriggerType == eTriggerType::DialogTrigger)
	{
		DialogueManager::PlayDialogue(myTriggerIndex);
		//SoundEngine::PlaySound(*myDialogueSound);
	}
}

void Trigger::SetTriggerIndex(int aIndex)
{
	myTriggerIndex = aIndex;
}

void Trigger::SetDirection(TriggerManager::eDirection aDirection)
{
	myDirection = aDirection;
}

void Trigger::Reset(Player* aPlayerPointer)
{
	if (myTriggerType == eTriggerType::DoorTrigger)
	{
		TriggerManager::GetInstance()->ResetAtIndex(myTriggerIndex);
	}

	myPosition = myOrginalPosition;
	myHaveBeenTriggerd = false;
}


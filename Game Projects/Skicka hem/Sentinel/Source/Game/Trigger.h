#pragma once
#include "GameObject.h"
#include <functional>
#include "TriggerManager.h"
#include "PhysicsComponent.h"

class Sound;
class Animation;

class Trigger : public GameObject
{
public:

	enum class eTriggerType
	{
		DoorTrigger,
		PlatformTrigger,
		DialogTrigger
	};
	
	Trigger(eTriggerType aTriggerType);
	~Trigger();

	void Init(const char* aTriggerTypePath);
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*> &aRenderBuffer);

	void OnCollisionEnter(Collider &other) override;

	void OnCollisionStay(Collider &other) override;

	void SendAMessage();

	void SetTriggerIndex(int aIndex);
	void SetDirection(TriggerManager::eDirection aDirection);

	void Reset(Player* aPlayerPointer = nullptr) override;

protected:
	bool myHaveBeenTriggerd;
private:

	eTriggerType myTriggerType;
	TriggerManager::eDirection myDirection;

	Drawable* myTriggeredAnimation;
	Drawable* myXButtonAnimation;
	Drawable* myCButtonAnimation;

	int myTriggerIndex;

	Sound* myDoorSound;
	Sound* myDialogueSound;
	Sound* myPlatformSound;

	bool myShouldRenderButton;
};


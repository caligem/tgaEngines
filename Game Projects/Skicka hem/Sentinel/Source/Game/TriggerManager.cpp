#include "stdafx.h"
#include "TriggerManager.h"
#include "Trigger.h"
#include "GameObject.h"
#include "MovingTile.h"
#include "Door.h"
#include "Camera.h"

TriggerManager* TriggerManager::ourInstance = nullptr;

void TriggerManager::Create()
{
	if (ourInstance == nullptr)
	{
		ourInstance = new TriggerManager();
	}
}

void TriggerManager::Destroy()
{
	if (ourInstance != nullptr)
	{
		delete ourInstance;
		ourInstance = nullptr;
	}
}

TriggerManager* TriggerManager::GetInstance()
{
	return ourInstance;
}

void TriggerManager::InsertDoorAtIndex(int aIndex, GameObject * aDoor)
{
	myGameObjects[aIndex] = aDoor;
}

void TriggerManager::TriggerAtIndex(int aIndex)
{
	Camera::GetInstance().LerpAndTrigger(myGameObjects[aIndex], 2.f);
}

void TriggerManager::TriggerAtIndex(int aIndex, TriggerManager::eDirection aDirection)
{
	if (aDirection == eDirection::Horizontal)
	{
		static_cast<MovingTile*>(myGameObjects[aIndex])->SetState(MovingTile::State::MovingHorizontal);
	}
	else if (aDirection == eDirection::Vertical)
	{
		static_cast<MovingTile*>(myGameObjects[aIndex])->SetState(MovingTile::State::MovingVertical);
	}
}

void TriggerManager::ResetAtIndex(int aIndex)
{
	static_cast<Door*>(myGameObjects[aIndex])->SetIsLocked(true);
}

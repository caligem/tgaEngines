#pragma once
#include <GrowingArray.h>
#include <array>

class Trigger;
class GameObject;

class TriggerManager
{
public:
	enum class eDirection
	{
		Vertical,
		Horizontal
	};

	static void Create();
	static void Destroy();
	static TriggerManager* GetInstance();

	void InsertDoorAtIndex(int aIndex, GameObject* aDoor);
	void TriggerAtIndex(int aIndex);
	void TriggerAtIndex(int aIndex, TriggerManager::eDirection aDirection);
	void ResetAtIndex(int aIndex);
private:
	TriggerManager() = default;
	~TriggerManager() = default;

	static TriggerManager* ourInstance;

	std::array<GameObject*, 64> myGameObjects;
};


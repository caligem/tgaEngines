#include "stdafx.h"
#include "Turret.h"
#include "Timer.h"
#include "ProjectileManager.h"
#include "SoundEngine.h"
#include <string>
#include "Player.h"

#undef GetCurrentTime

typedef CommonUtilities::Vector2f vector2f;

Turret::Turret(ProjectileManager& aProjectileManager, Player* aPlayer)
	: myProjectileManager(aProjectileManager)
	, myBoxCol(nullptr)
	, myPlayer(aPlayer)
{
}


Turret::~Turret()
{
	myShootSound->Destroy();
	myShootSound = nullptr;
	myBoxCol = nullptr;
	myDrawable = myOriginalSprite;
}

void Turret::Init(json& aJson, unsigned short aLayerIndex, unsigned short aObjectIndex)
{
	myOriginalSprite = static_cast<Sprite*>(myDrawable);
	myShootSound = new Sound("Sounds/Sfx/turret_shoot.wav");

	myCurrentShots = 0;
	myShootCooldown = aJson["layers"][aLayerIndex]["objects"][aObjectIndex]["properties"]["shootCooldown"].get<float>();
	myFireRate = aJson["layers"][aLayerIndex]["objects"][aObjectIndex]["properties"]["fireRate"].get<float>();
	myMaxShots = aJson["layers"][aLayerIndex]["objects"][aObjectIndex]["properties"]["maxShots"].get<int>();
	myBulletSpeed = aJson["layers"][aLayerIndex]["objects"][aObjectIndex]["properties"]["bulletSpeed"].get<float>();
	myIsFlipped = aJson["layers"][aLayerIndex]["objects"][aObjectIndex]["properties"]["isFlipped"].get<bool>();

	myDirection.x = aJson["layers"][aLayerIndex]["objects"][aObjectIndex]["properties"]["horizontal"].get<float>();
	myDirection.y = aJson["layers"][aLayerIndex]["objects"][aObjectIndex]["properties"]["vertical"].get<float>();

	myShootingTimer.Set(myFireRate, cit::countdown::type::autoreset, [&] {Shoot(); });
	myReloadingTimer.Set(myShootCooldown, cit::countdown::type::oneshot, [&] {Reload(); });
	

	myShootingAnimationTimer.Set(myFireRate / 4.f, cit::countdown::type::oneshot, [&]	{	myShootingAnimation.SetCurrentFrame(1);
																							if (myCurrentShots <= 0)
																							{

																								myReloadingAnimation.SetCurrentFrame(0);
																								myReloadingAnimation.Pause();
																								myDrawable = &myReloadingAnimation;
																							}
																						});


	myShootingAnimation.Init("Sprites/enemies/turret_shoot.dds");
	myShootingAnimation.SetPosition(myPosition);
						
	myReloadingAnimation.Init("Sprites/enemies/turret_charge.dds");
	myReloadingAnimation.SetPosition(myPosition);
	myBoxCol = static_cast<BoxCollider*>(myCollider);
	myOriginalBoxHeight = myBoxCol->GetHeight();

	if (myIsFlipped)
	{
		myShootingAnimation.SetInversedX(true);
		myReloadingAnimation.SetInversedX(true);
		myDirection.x *= -1.f;
	}

	Reload();
}

void Turret::OnUpdate()
{
	myShootingTimer.Update(CommonUtilities::Timer::GetDeltaTime());
	myReloadingTimer.Update(CommonUtilities::Timer::GetDeltaTime());
	myShootingAnimationTimer.Update(CommonUtilities::Timer::GetDeltaTime());


	if (myReloadingTimer.GetCurrentTime() + myReloadingAnimation.GetTotalTime() >= myReloadingTimer.GetMaxTime())
	{
		myReloadingAnimation.Unpause();
	}

	if (myShootingAnimation.GetCurrentFrame() == 1)
	{
		myBoxCol->SetHeight(myOriginalBoxHeight);
	}
	else
	{
		myBoxCol->SetHeight(myOriginalBoxHeight + 0.02f);
	}

	GameObject::OnUpdate();
	myDrawable->SetPosition({ myPosition.x, myPosition.y });

}

void Turret::Shoot()
{
	float xDistance = abs(myPlayer->GetPosition().x - myPosition.x);
	float yDistance = abs(myPlayer->GetPosition().y - myPosition.y);

	if (xDistance > 0.6f || yDistance > 0.6f)
	{
		return;
	}

	myDrawable = &myShootingAnimation;
	myShootingAnimation.SetCurrentFrame(0);
	myShootingAnimationTimer.Reset();
	myShootingAnimationTimer.Start();

	--myCurrentShots;

	float xStartPos = 0.033f;
	float xOffset = 0.037f;
	float yOffset = 0.09f;

	vector2f myBulletSpawnPoint = myPosition - vector2f(xStartPos, 0.f) + vector2f(xOffset * myDirection.x, yOffset);
	myProjectileManager.CreateProjectile(myBulletSpawnPoint, myDirection, myBulletSpeed);

	SoundEngine::PlaySound(*myShootSound);

	if (myCurrentShots <= 0)
	{
		myShootingTimer.Stop();
		myReloadingTimer.Reset();
		myReloadingTimer.Start();
	}
}

void Turret::Reload()
{
	myCurrentShots = myMaxShots;
	myShootingTimer.Reset();
	myShootingTimer.Start();
}

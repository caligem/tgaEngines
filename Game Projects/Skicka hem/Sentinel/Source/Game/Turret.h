#pragma once
#include "GameObject.h"
#include "cit\countdown.h"
#include "JsonWrapper.h"
#include "Animation.h"

class Player;
class ProjectileManager;
class Sound;

class Turret : public GameObject
{
public:

	Turret(ProjectileManager& aProjectileManager, Player* aPlayer);
	~Turret();
	void Init(json& aJson, unsigned short aLayerIndex, unsigned short aObjectIndex);
	void OnUpdate() override;

private:
	CommonUtilities::Vector2f myDirection;
	float myShootCooldown;
	float myFireRate;
	float myBulletSpeed;
	int myCurrentShots;
	int myMaxShots;
	bool myIsFlipped;

	Player* myPlayer;

	cit::countdown myShootingTimer;
	cit::countdown myShootingAnimationTimer;
	cit::countdown myReloadingTimer;
	void Shoot();
	void Reload();

	Sprite *myOriginalSprite;
	Animation myShootingAnimation;
	Animation myReloadingAnimation;

	ProjectileManager& myProjectileManager;
	Sound* myShootSound;

	float myOriginalBoxHeight;

	BoxCollider* myBoxCol;
};


#include "stdafx.h"
#include "Warp.h"
#include "PostMaster.h"
#include "Player.h"
#include "ParticleEmitterManager.h"

Warp::Warp()
{
	myWarpType = eWarpType::In;
	myIsOpen = true;
	myClueEmitter = ParticleEmitterManager::SpawnOwnedEmitterByIndex(1);
	myClosedAnimation.Init("sprites/objects/warp_closed.dds");
}


Warp::~Warp()
{
}

void Warp::OnUpdate()
{
	GameObject::OnUpdate();
	myClosedAnimation.SetPosition(myPosition);
	myCollider->SetIsSolid(false);
}

void Warp::OnCollisionEnter(Collider & other)
{
	if (myIsOpen)
	{
		if (other.GetTag() == ColliderTag::Player)
		{
			static_cast<Player*>(other.GetOwner())->SetAnimationStateToIdle();
			myIsOpen = false;
			Message newMessage;
			if (myWarpType == eWarpType::In)
			{
				newMessage.myMessageType = MessageType::PushWarpZone;
			}
			else if (myWarpType == eWarpType::Out)
			{
				newMessage.myMessageType = MessageType::ExitedWarpZone;
			}
			PostMaster::SendMessages(newMessage);
		}
	}
}

void Warp::OnCollisionStay(Collider & other)
{
	other;
}

void Warp::OnCollisionLeave(Collider & other)
{
	other;
}

void Warp::SetWarpType(eWarpType aWarpType)
{
	myWarpType = aWarpType;
}

void Warp::FillCollisionBuffer(CommonUtilities::GrowingArray<GameObject*>& aBufferToAddTo)
{
	if (myIsOpen)
	{
		aBufferToAddTo.Add(this);
	}
}

void Warp::FillRenderBuffer(CommonUtilities::GrowingArray<Drawable*>& aRenderBuffer)
{
	if (myIsOpen)
	{
		aRenderBuffer.Add(myDrawable);
	}
	else
	{
		aRenderBuffer.Add(&myClosedAnimation);
	}
	myClueEmitter.SetLayer(myDrawable->GetLayer() - 1);
	myClueEmitter.SetPosition(Tga2D::Vector2f(myPosition.x, myPosition.y));
	aRenderBuffer.Add(&myClueEmitter);
}

void Warp::Reset(Player* aPlayerPointer)
{
	GameObject::Reset();
	myIsOpen = true;
}

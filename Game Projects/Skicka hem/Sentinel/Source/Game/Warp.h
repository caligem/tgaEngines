#pragma once
#include "GameObject.h"
#include "JsonWrapper.h"
#include "ParticleEmitter.h"
#include "Animation.h"

enum class eWarpType
{
	In,
	Out
};

class Warp :
	public GameObject
{
public:
	Warp();
	~Warp();

	void OnUpdate() override;
	void OnCollisionEnter(Collider  &other) override;
	void OnCollisionStay(Collider  &other) override;
	void OnCollisionLeave(Collider  &other) override;

	void SetWarpType(eWarpType aWarpType);

	void FillCollisionBuffer(CommonUtilities::GrowingArray<GameObject *> &aBufferToAddTo) override;
	void FillRenderBuffer(CommonUtilities::GrowingArray<Drawable *> &aRenderBuffer) override;
	void Reset(Player* aPlayerPointer = nullptr) override;
private:
	eWarpType myWarpType;
	bool myIsOpen;
	ParticleEmitter myClueEmitter;
	Animation myClosedAnimation;
};


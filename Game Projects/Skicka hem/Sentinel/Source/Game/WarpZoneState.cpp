#include "stdafx.h"
#include "WarpZoneState.h"
#include "BatchManager.h"
#include "InputWrapper.h"
#include "DialogueManager.h"
#include "StateStack.h"
#include "Camera.h"
#include "PostMaster.h"
#include "Fader.h"
#include "DanceGameState.h"
#include "SoundEngine.h"
#include "CollectibleCounter.h"
#include "Collectible.h"
#include "TileSize.h"
#include "Text.h"

typedef CommonUtilities::Vector2f vector2f;
WarpZoneState::WarpZoneState(CommonUtilities::InputManager* aInputManager, int aCurrentLevelIndex, Player &aPlayer, CollectibleCounter &aCollCounter)
	: myShouldExit(false)
	, myCurrentLevelIndex(aCurrentLevelIndex)
	, myInputManager(aInputManager)
	, myBackground(aCurrentLevelIndex)
{
	myWarpZonePlayer = &aPlayer;
	myCollCounter = &aCollCounter;
	myPlayerPosBeforeWarpzone = aPlayer.GetPosition();
}


WarpZoneState::~WarpZoneState()
{
	//BatchManager::EmptyBatches();
	myRenderBuffer.DeleteAll();
	PostMaster::UnSubscribe(MessageType::PushDanceGame, this);
	PostMaster::UnSubscribe(MessageType::FinishedWarpZone, this);
	PostMaster::UnSubscribe(MessageType::ExitedWarpZone, this);
	PostMaster::UnSubscribe(MessageType::UnlockCollectible, this);
}

void WarpZoneState::Init()
{
	myRenderBuffer.Init(256);

	InitRoom();

	Camera::GetInstance().SetTarget(myWarpZonePlayer);
	Camera::GetInstance().SetPosition(myWarpZonePlayer->GetPosition());

	PostMaster::Subscribe(MessageType::PushDanceGame, this);
	PostMaster::Subscribe(MessageType::UnlockCollectible, this);
	PostMaster::Subscribe(MessageType::ExitedWarpZone, this);
	PostMaster::Subscribe(MessageType::FinishedWarpZone, this);

	Fader::FadeOut(FadeColor::Black);
	myBackground.Init(vector2f(0.f, 0.f), vector2f(0.f, 0.f), true);

	if (myCurrentLevelIndex == 1) 
	{
		myMusic = new Sound("Sounds/Music/disco_room.ogg");
	}
	else if(myCurrentLevelIndex == 2)
	{
		myMusic = new Sound("Sounds/Music/basket.ogg");
	}
	if (myCurrentLevelIndex == 3)
	{
		myMusic = new Sound("Sounds/Music/jump_game.ogg");
	}

	myMusic->SetAsGenericMusic();
	myGameHasBegun = false;
	myTimer = 0.f;
	myWarpZone.DeactivatePlatforms();
}

void WarpZoneState::OnEnter()
{
	Init();

	
	SoundEngine::PlaySound(*myMusic);
	
}

void WarpZoneState::OnExit()
{
	myWarpZonePlayer->SetPosition(myPlayerPosBeforeWarpzone);
	myWarpZonePlayer->SetRespawnPosition(myPlayerPosBeforeWarpzone);
	myWarpZonePlayer->SetPreventInput(false);
	myWarpZonePlayer->LockCollectibles();
	Message newMessage;
	newMessage.myMessageType = MessageType::PopWarpZone;
	PostMaster::SendMessages(newMessage);
	StopRendering();
	myMusic->Destroy();
}

void WarpZoneState::ReceiveMessage(const Message aMessage)
{
	if (aMessage.myMessageType == MessageType::PushDanceGame) 
	{
		myStateStack->PushSubState(new DanceGameState());
	}
	else if (aMessage.myMessageType == MessageType::FinishedWarpZone)
	{
		myCollectibleBlocker->SetShouldRemove(true);
		SaveProgress();
	}
	else if (aMessage.myMessageType == MessageType::ExitedWarpZone)
	{
		Fader::FadeIn(FadeColor::Black);
		myShouldExit = true;
	}
	else if (aMessage.myMessageType == MessageType::UnlockCollectible)
	{
		myCollectibleBlocker->SetShouldRemove(true);
		SaveProgress();
	}
}

eStateStackMessage WarpZoneState::Update()
{
	if (myCurrentLevelIndex == 1)
	{
		if (myMusic->channelIndexes.size() == 0)
		{
			SoundEngine::PlaySound(*myMusic);
		}
	}
	if (Fader::GetState() == FadeState::Invisable)
	{
		myWarpZonePlayer->SetPreventInput(false);
	}
	else
	{
		myWarpZonePlayer->SetPreventInput(true);
	}

	CleanUp();

	myWarpZone.Update();

	if (myCurrentLevelIndex == 3)
	{
		myTimerText.SetPosition(myTimerBoard->GetPosition() + CommonUtilities::Vector2f(0.0345f, -0.075f));
		myWarningText.SetPosition(myTimerBoard->GetPosition() + CommonUtilities::Vector2f(0.004f, -0.027f));
		myRenderBuffer.Add(&myTimerText);
		myRenderBuffer.Add(&myWarningText);
	}
	myWarpZone.FillRenderBuffer(myRenderBuffer);
	Camera::GetInstance().ConvertRenderBufferToCameraSpace(myRenderBuffer);

	Camera::GetInstance().Update(myInputManager);

	if (InputWrapper::IsButtonPressed(KeyButton::Pause) && !DialogueManager::IsPlaying())
	{
		CurrentCollData data;
		data.myColl =  myWarpZonePlayer->GetCurrentNumberOfCollectibles();

		switch (myCurrentLevelIndex)
		{
		case 1:
			data.myTotalColl = myStateStack->myGameProgressData.myLevel1NumberOfCollectables;
			break;
		case 2:
			data.myTotalColl = myStateStack->myGameProgressData.myLevel2NumberOfCollectables;
			break;
		case 3:
			data.myTotalColl = myStateStack->myGameProgressData.myLevel3NumberOfCollectables;
			break;
		default:
			break;
		}

		myStateStack->PushSubState(new PauseMenuState(myInputManager, data));
	}

	if (myCurrentLevelIndex == 3)
	{
		if (myGameHasBegun == false && myWarpZonePlayer->GetGroundChecker().OnGround() && myWarpZonePlayer->GetGroundChecker().GetHit()->GetTag() == ColliderTag::MovingTile)
		{
			myWarpZone.TriggerPlatforms();
			myGameHasBegun = true;
		}
		if (myGameHasBegun)
		{
			myTimer += Timer::GetDeltaTime();
			myTimerText.SetText(std::to_string(static_cast<int>(21.0f - myTimer)));
			myWarningText.SetText("JUMP!");
			myWarningText.SetColor(CommonUtilities::Vector4f{ 1.0f, 0.0f, 0.0f, 1.0f });
			if (myTimer > 21.0f)
			{
				myWarpZone.DeactivatePlatforms();
				myCollectibleBlocker->SetShouldRemove(true);
				myTimerText.SetText(std::to_string(static_cast<int>(0.0f)));
			}
			else if (myWarpZonePlayer->GetGroundChecker().OnGround() && myWarpZonePlayer->GetGroundChecker().GetHit()->GetTag() == ColliderTag::Tile)
			{
				float windowWidth = 1920.0f;
				float windowHeight = 1080.0f;
				myWarpZone.ResetRoom();
				myCollectibleBlocker->SetPosition(myWarpZone.GetCollectibleInWarpZone()->GetPosition());
				myTimer = 0.0f;
				myGameHasBegun = false;
			}
		}
		else
		{
			myTimerText.SetText(std::to_string(static_cast<int>(21.0f)));
			myWarningText.SetText("FLOOR = LAVA");
			myWarningText.SetColor(CommonUtilities::Vector4f{ 0.0f, 1.0f, 0.0f, 1.0f });
		}

	}

	if (myShouldExit)
	{
		if (Fader::GetState() == FadeState::Visable)
		{
			return eStateStackMessage::PopSubState;
		}
	}
	int nrOfCols = 0;
	switch (myCurrentLevelIndex)
	{
	case 1:
		nrOfCols = myStateStack->myGameProgressData.myLevel1NumberOfCollectables;
		break;
	case 2:
		nrOfCols = myStateStack->myGameProgressData.myLevel2NumberOfCollectables;
		break;
	case 3:
		nrOfCols = myStateStack->myGameProgressData.myLevel3NumberOfCollectables;
		break;
	default:
		break;
	}
	myCollCounter->UpdateCounter(myWarpZonePlayer->GetCurrentNumberOfCollectibles(), nrOfCols);

	if (myWarpZonePlayer->HasPickedUpCollectible())
	{
		myCollCounter->Appear();
	}
	
	return eStateStackMessage::KeepState;
}

void WarpZoneState::Render()
{
	myBackground.Render();

	if (myRenderBuffer.Size() > 0)
	{
		std::sort(&myRenderBuffer[0], &myRenderBuffer[0] + myRenderBuffer.Size(), [](Drawable* a, Drawable* b)
		{
			return a->GetLayer() < b->GetLayer();
		});
	}


	//render batched sprites from other states
	myWarpZone.RenderBatchedSprites();

	for (unsigned short i = 0; i < myRenderBuffer.Size(); i++)
	{
		
		{
			myRenderBuffer[i]->Render();
		}
	}
	
	myCollCounter->Render();
	Fader::Render();
}

void WarpZoneState::InitRoom()
{
	std::string warpZoneFilePath = "level" + std::to_string(myCurrentLevelIndex) + "/warpzone.json";

	myWarpZone.Init(*myWarpZonePlayer, warpZoneFilePath.c_str());
	myWarpZone.EnterRoom();

	InitCollectibleBlocker();

	HandleCompletedState();
}

void WarpZoneState::InitCollectibleBlocker()
{
	myCollectibleBlocker = new GameObject();
	myCollectibleBlocker->InitGO(myWarpZone.GetCollectibleInWarpZone()->GetPosition() / TILE_MULTIPLIER, "sprites/objects/collectible_blocker.dds", new Sprite());
	myCollectibleBlocker->AccessDrawable().SetLayer(myWarpZone.GetCollectibleInWarpZone()->AccessDrawable().GetLayer() + 1);

	float windowWidth = 1920.0f;
	float windowHeight = 1080.0f;
	myCollectibleBlocker->SetPosition(CommonUtilities::Vector2f(myCollectibleBlocker->GetPosition().x - ((TILE_SIZE / windowWidth) * TILE_MULTIPLIER), myCollectibleBlocker->GetPosition().y + ((TILE_SIZE / windowHeight) * TILE_MULTIPLIER)));
	BoxCollider grabbableCol;
	grabbableCol.Init((TILE_SIZE / windowWidth) * TILE_MULTIPLIER *2.0f, (TILE_SIZE / windowHeight) * TILE_MULTIPLIER*2.0f, CommonUtilities::Vector2f{ 0.0f, 0.0f });

	grabbableCol.SetTag(ColliderTag::Tile);
	myCollectibleBlocker->AttachCollider(grabbableCol);

	myWarpZone.AddGameObjectToBuffer(myCollectibleBlocker);

	if (myStateStack->myGameProgressData.myCurrentLevelIndex == 3)
	{
		myTimerBoard = new GameObject();
		myTimerBoard->InitGO(CommonUtilities::Vector2f{ 0.05f, 0.2f }, "sprites/level3/secretRoom_timeboard.dds", new Sprite());
		myTimerText.Init("text/mecha.ttf");
		myTimerBoard->AccessDrawable().SetLayer(1);
		myWarpZone.AddGameObjectToBuffer(myTimerBoard);

		myTimerText.Init("text/Mecha.ttf");
		myTimerText.SetScale(1.8f);
		myTimerText.SetLayer(2);
		myTimerText.SetColor(CommonUtilities::Vector4f{ 0.0f, 1.0f, 0.0f, 1.0f });

		myWarningText.Init("text/Mecha.ttf");
		myWarningText.SetScale(1.8f);
		myWarningText.SetLayer(2);
		myWarningText.SetColor(CommonUtilities::Vector4f{ 0.0f, 1.0f, 0.0f, 1.0f });
	}
	else
	{
		myTimerBoard = nullptr;
	}
}

void WarpZoneState::HandleCompletedState()
{
	if (myCurrentLevelIndex == 1)
	{
		if (myStateStack->myGameProgressData.myHasCompletedWarpZone1)
		{
			myWarpZone.GetCollectibleInWarpZone()->SetShouldRemove(true);
			myCollectibleBlocker->SetShouldRemove(true);
		}
	}
	else if (myCurrentLevelIndex == 2)
	{
		if (myStateStack->myGameProgressData.myHasCompletedWarpZone2)
		{
			myWarpZone.GetCollectibleInWarpZone()->SetShouldRemove(true);
			myCollectibleBlocker->SetShouldRemove(true);
		}
	}
	else if (myCurrentLevelIndex == 3)
	{
		if (myStateStack->myGameProgressData.myHasCompletedWarpZone3)
		{
			myWarpZone.GetCollectibleInWarpZone()->SetShouldRemove(true);
			myCollectibleBlocker->SetShouldRemove(true);
		}
	}
}

void WarpZoneState::SaveProgress()
{
	if (myCurrentLevelIndex == 1)
	{
		myStateStack->myGameProgressData.myHasCompletedWarpZone1 = true;	
	}
	else if (myCurrentLevelIndex == 2)
	{
		myStateStack->myGameProgressData.myHasCompletedWarpZone2 = true;
	}
	else if (myCurrentLevelIndex == 3)
	{
		myStateStack->myGameProgressData.myHasCompletedWarpZone3 = true;
	}
	myWarpZonePlayer->LockCollectibles();
}

void WarpZoneState::StopRendering()
{
	myWarpZone.StopRendering();
	CleanUp();
}

void WarpZoneState::CleanUp()
{
	myRenderBuffer.RemoveAll();
	myWarpZone.CleanUpGameObjects();
}

#pragma once
#include "ParallaxBackground.h"
#include "GameState.h"
#include "GameObject.h"
#include "Player.h"
#include "Room.h"
#include <vector>
#include "Subscriber.h"
#include "Text.h"
class Sound;
class CollectibleCounter;

class WarpZoneState : public GameState, public Subscriber
{
public:
	WarpZoneState(CommonUtilities::InputManager* aInputManager, int aCurrentLevelIndex, Player &aPlayer, CollectibleCounter &aCollCounter);
	~WarpZoneState();

	void Init();

	void OnEnter();
	void OnExit();

	void ReceiveMessage(const Message aMessage) override;

	eStateStackMessage Update();
	void Render();

private:
	bool myShouldExit;
	int myCurrentLevelIndex;

	float myTimer;
	bool myGameHasBegun;

	Player *myWarpZonePlayer;
	CommonUtilities::Vector2f myPlayerPosBeforeWarpzone;

	CollectibleCounter *myCollCounter;


	Room myWarpZone;
	void InitRoom();

	void InitCollectibleBlocker();
	void HandleCompletedState();
	void SaveProgress();

	CommonUtilities::InputManager* myInputManager;
	CommonUtilities::GrowingArray<Drawable*> myRenderBuffer;

	void StopRendering();
	void CleanUp();
	ParallaxBackground myBackground;
 	Sound* myMusic;

	GameObject *myCollectibleBlocker;

	GameObject *myTimerBoard;
	Text myTimerText;
	Text myWarningText;
};


#include "stdafx.h"
#include "miscutils.h"
#include <locale>


const std::string ToLower(const std::string& aString)
{
	std::string arg = "";

	std::locale loc;

	for (auto elem : aString)
	{
		arg += std::tolower(elem, loc);
	}

	return arg;
}

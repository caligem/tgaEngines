#pragma once
#include<chrono>
namespace carlivan_tools
{
	typedef std::chrono::seconds seconds;
	typedef std::chrono::milliseconds milliseconds;
	typedef std::chrono::microseconds microseconds;
	typedef std::chrono::nanoseconds nanoseconds;

	template <typename TimeUnit>
	class benchmark
	{
	public:
		void Start();
		void Stop();
		long GetTime() const;

	private:
		std::chrono::steady_clock::time_point myStart;
		long myTime;
	};

	template<typename TimeUnit>
	inline void benchmark<TimeUnit>::Start()
	{
		myStart = std::chrono::high_resolution_clock::now();
	}

	template<typename TimeUnit>
	inline void benchmark<TimeUnit>::Stop()
	{
		myTime = std::chrono::duration_cast<TimeUnit>(std::chrono::high_resolution_clock::now() - myStart).count();
	}

	template<typename TimeUnit>
	inline long benchmark<TimeUnit>::GetTime() const
	{
		return myTime;
	}
};
namespace cit = carlivan_tools;
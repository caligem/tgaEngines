#include "countdown.h"
using namespace cit;
countdown::countdown()
{
	myMaxTime = -1.0f;
	myCurrTime = 0.0f;
	myTrigger = nullptr;
	myType = type::oneshot;
	myFinished = false;
	myIsStarted = false;
}

countdown::~countdown()
{

}

void carlivan_tools::countdown::Start()
{
	myIsStarted = true;
}

void carlivan_tools::countdown::Stop()
{
	myIsStarted = false;
}

void countdown::Set(const float aMaxTime, const type aCountdownType, const std::function<void()>& aTrigger)
{
	myMaxTime = aMaxTime;
	myCurrTime = 0.0f;
	myTrigger = aTrigger;
	myType = aCountdownType;
	myFinished = false;
	myIsStarted = false;
}

void countdown::Update(const float aDeltaTime)
{
	if (myFinished == false && myIsStarted == true)
	{
		if (myCurrTime >= myMaxTime)
		{
			myTrigger();
			if (myType == type::autoreset)
			{
				myCurrTime = 0.0f;
			}
			else if (myType == type::oneshot)
			{
				myFinished = true;
			}
		}
		else
		{
			myCurrTime += aDeltaTime;
		}
	}
}

void countdown::Reset()
{
	myCurrTime = 0.0f;
	myFinished = false;
	myIsStarted = false;
}

float carlivan_tools::countdown::GetCurrentTime() const
{
	return myCurrTime;
}

float carlivan_tools::countdown::GetMaxTime() const
{
	return myMaxTime;
}


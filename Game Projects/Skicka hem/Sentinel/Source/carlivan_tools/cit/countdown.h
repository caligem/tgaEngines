#pragma once
#include <functional>

namespace carlivan_tools
{
	class countdown
	{
	public:
		enum class type
		{
			//triggers once
			oneshot,
			//calls reset after triggered
			autoreset,
			//keeps calling trigger when finished
			continuous
		};
		countdown();
		~countdown();
		void Start();
		void Stop();
		void Set(const float aMaxTime, const type aCountdownType, const std::function<void()> &aTrigger);
		void Update(const float aDeltaTime);
		void Reset();

		float GetCurrentTime() const;
		float GetMaxTime() const;
	private:
		bool myIsStarted;
		bool myFinished;
		float myMaxTime;
		float myCurrTime;
		std::function<void()> myTrigger;
		type myType;
	};
}
namespace cit = carlivan_tools;


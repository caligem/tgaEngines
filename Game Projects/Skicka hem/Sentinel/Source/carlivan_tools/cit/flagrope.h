#pragma once
#include <initializer_list>
#include <assert.h>
#include <algorithm>
namespace carlivan_tools
{
	template<typename EnumType, unsigned int ContainerSize>
	class flagrope
	{
	public:
		flagrope();
		flagrope(const std::initializer_list<EnumType> &aFlagropeToSet);
		~flagrope();
		flagrope &operator=(const flagrope &aFlagropeToAssignFrom);
		bool operator==(const flagrope &aFlagropeToCompareWith) const;
		bool operator!=(const flagrope &aFlagropeToCompareWith) const;
		void Enable(const EnumType &aFlagToAdd);
		void Enable(const flagrope &aFlagropeToAdd);
		void Disable(const EnumType &aFlagToRemove);
		void Disable(const flagrope &aFlagropeToRemove);
		void Flip(const EnumType &aFlagToFlipWith);
		void Flip(const flagrope &aFlagropeToFlipWith);
		bool Has(const EnumType &aFlagToCheck);
		bool Has(const flagrope &aFlagropeToCompare);

		bool HasAny();

		int Size() const;
	private:
		unsigned char ConvertToPowerOfTwo(const unsigned int &aValueToConvert);
		bool IsPowerOfTwo(const unsigned char &aValueToCheck) const;

		unsigned char myContainer[((ContainerSize-1)/8) + 1];
		int myAmountOfElements;
	};

	template<typename EnumType, unsigned int ContainerSize>
	inline flagrope<EnumType, ContainerSize>::flagrope()
	{
		myAmountOfElements = ((ContainerSize - 1) / 8) + 1;
		std::fill_n(myContainer, myAmountOfElements, 0);
	}

	template<typename EnumType, unsigned int ContainerSize>
	inline flagrope<EnumType, ContainerSize>::flagrope(const std::initializer_list<EnumType>& aFlagropeToSet)
	{
		myAmountOfElements = ((ContainerSize - 1) / 8) + 1;
		std::fill_n(myContainer, myAmountOfElements, 0);
		for (unsigned char i = 0; i < aFlagropeToSet.size(); i++)
		{
			Enable(*(aFlagropeToSet.begin() + i));
		}
	}

	template<typename EnumType, unsigned int ContainerSize>
	inline flagrope<EnumType, ContainerSize>::~flagrope()
	{
	}

	template<typename EnumType, unsigned int ContainerSize>
	inline flagrope<EnumType, ContainerSize> &flagrope<EnumType, ContainerSize>::operator=(const flagrope & aFlagropeToAssignFrom)
	{
		for (unsigned char i = 0; i < myAmountOfElements; i++)
		{
			myContainer[i] = aFlagropeToAssignFrom.myContainer[i];
		}
		return *this;
	}

	template<typename EnumType, unsigned int ContainerSize = unsigned char >
	inline bool flagrope<EnumType, ContainerSize>::operator==(const flagrope & aFlagropeToCompareWith) const
	{
		for (unsigned char i = 0; i < myAmountOfElements; i++)
		{
			if (myContainer[i] != aFlagropeToCompareWith.myContainer[i])
			{
				return false;
			}
		}
		return true;
	}

	template<typename EnumType, unsigned int ContainerSize = unsigned char >
	inline bool flagrope<EnumType, ContainerSize>::operator!=(const flagrope & aFlagropeToCompareWith) const
	{
		for (unsigned char i = 0; i < myAmountOfElements; i++)
		{
			if (myContainer[i] != aFlagropeToCompareWith.myContainer[i])
			{
				return true;
			}
		}
		return false;
	}

	template<typename EnumType, unsigned int ContainerSize = unsigned char >
	inline void flagrope<EnumType, ContainerSize>::Enable(const EnumType &aFlagToAdd)
	{
		assert(static_cast<float>(aFlagToAdd) >= 0 && "Lowest value should be 0!");
		int indexInContainer = static_cast<unsigned char>(aFlagToAdd) / (sizeof(unsigned char) * CHAR_BIT);
		int indexInSegment = static_cast<unsigned char>(aFlagToAdd) - (indexInContainer *(sizeof(unsigned char) * CHAR_BIT));
		myContainer[indexInContainer] |= ConvertToPowerOfTwo(indexInSegment);
	}

	template<typename EnumType, unsigned int ContainerSize>
	inline void flagrope<EnumType, ContainerSize>::Enable(const flagrope & aFlagropeToAdd)
	{
		for (unsigned char i = 0; i < myAmountOfElements; i++)
		{
			myContainer[i] |= aFlagropeToAdd.myContainer[i];
		}
	}

	template<typename EnumType, unsigned int ContainerSize = unsigned char >
	inline void flagrope<EnumType, ContainerSize>::Disable(const EnumType &aFlagToRemove)
	{
		int indexInContainer = static_cast<unsigned char>(aFlagToRemove) / (sizeof(unsigned char) * CHAR_BIT);
		int indexInSegment = static_cast<unsigned char>(aFlagToRemove) - (indexInContainer *(sizeof(unsigned char) * CHAR_BIT));
		myContainer[indexInContainer] &= ~ConvertToPowerOfTwo(indexInSegment);
	}

	template<typename EnumType, unsigned int ContainerSize>
	inline void flagrope<EnumType, ContainerSize>::Disable(const flagrope & aFlagropeToRemove)
	{
		for (unsigned char i = 0; i < myAmountOfElements; i++)
		{
			myContainer[i] &= ~aFlagropeToRemove.myContainer[i];
		}
	}

	template<typename EnumType, unsigned int ContainerSize>
	inline void flagrope<EnumType, ContainerSize>::Flip(const EnumType & aFlagToFlipWith)
	{
		int indexInContainer = static_cast<unsigned char>(aFlagToFlipWith) / (sizeof(unsigned char) * CHAR_BIT);
		int indexInSegment = static_cast<unsigned char>(aFlagToFlipWith) - (indexInContainer *(sizeof(unsigned char) * CHAR_BIT));
		myContainer[indexInContainer] ^= ConvertToPowerOfTwo(indexInSegment);
	}

	template<typename EnumType, unsigned int ContainerSize>
	inline void flagrope<EnumType, ContainerSize>::Flip(const flagrope & aFlagropeToFlipWith)
	{
		for (unsigned char i = 0; i < myAmountOfElements; i++)
		{
			myContainer[i] ^= aFlagropeToFlipWith.myContainer[i];
		}
	}

	template<typename EnumType, unsigned int ContainerSize = unsigned char >
	inline bool flagrope<EnumType, ContainerSize>::Has(const EnumType &aFlagToCheck)
	{
		int indexInContainer = static_cast<unsigned char>(aFlagToCheck) / (sizeof(unsigned char) * CHAR_BIT);
		int indexInSegment = static_cast<unsigned char>(aFlagToCheck) - (indexInContainer *(sizeof(unsigned char) * CHAR_BIT));
		return(myContainer[indexInContainer] & ConvertToPowerOfTwo(indexInSegment) != 0);
	}

	template<typename EnumType, unsigned int ContainerSize>
	inline bool flagrope<EnumType, ContainerSize>::Has(const flagrope & aFlagropeToCompare)
	{
		for (unsigned char i = 0; i < myAmountOfElements; i++)
		{
			if (myContainer[i] < aFlagropeToCompare.myContainer[i])
			{
				return false;
			}
		}
		return true;
	}

	template<typename EnumType, unsigned int ContainerSize>
	inline bool flagrope<EnumType, ContainerSize>::HasAny()
	{
		for (unsigned char i = 0; i < myAmountOfElements; i++)
		{
			if (myContainer[i] != 0)
			{
				return true;
			}
		}
		return false;
	}

	template<typename EnumType, unsigned int ContainerSize>
	inline int flagrope<EnumType, ContainerSize>::Size() const
	{
		return ContainerSize;
	}

	template<typename EnumType, unsigned int ContainerSize>
	inline unsigned char flagrope<EnumType, ContainerSize>::ConvertToPowerOfTwo(const unsigned int &aValueToConvert)
	{
		unsigned char result = 1;
		assert(ContainerSize > aValueToConvert && "EnumType index too high for unsigned char!");
		result = result << (aValueToConvert);

		return result;
	}

	template<typename EnumType, unsigned int ContainerSize>
	inline bool flagrope<EnumType, ContainerSize>::IsPowerOfTwo(const unsigned char & aValueToCheck) const
	{
		return ((aValueToCheck != 0) && (aValueToCheck & (aValueToCheck - 1)) == 0);
	}
}
namespace cit = carlivan_tools;
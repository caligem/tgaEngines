#pragma once
#include <initializer_list>
#include <assert.h>

namespace carlivan_tools
{
	//ContainerType decides max amount of elements(1 bit per element) 
	template<typename EnumType, typename ContainerType = unsigned char >
	class flagropeprimitive
	{
	public:
		flagropeprimitive();
		flagropeprimitive(const std::initializer_list<EnumType> &aFlagropeToSet);
		~flagropeprimitive();
		flagropeprimitive &operator=(const flagropeprimitive &aFlagropeToAssignFrom);
		bool operator==(const flagropeprimitive &aFlagropeToCompareWith) const;
		bool operator!=(const flagropeprimitive &aFlagropeToCompareWith) const;
		void Enable(const EnumType &aFlagToAdd);
		void Enable(const flagropeprimitive &aFlagropeToAdd);
		void Disable(const EnumType &aFlagToRemove);
		void Disable(const flagropeprimitive &aFlagropeToRemove);
		void Flip(const EnumType &aFlagToFlipWith);
		void Flip(const flagropeprimitive &aFlagropeToFlipWith);
		bool Has(const EnumType &aFlagToCheck);
		bool Has(const flagropeprimitive &aFlagropeToCompare) const;

		bool HasAny();

		int Size() const;
	private:
		ContainerType ConvertToPowerOfTwo(const ContainerType &aValueToConvert);
		bool IsPowerOfTwo(const ContainerType &aValueToCheck) const;

		ContainerType myContainer;
	};

	template<typename EnumType, typename ContainerType = unsigned char >
	inline flagropeprimitive<EnumType, ContainerType>::flagropeprimitive()
	{
		myContainer = 0;
	}

	template<typename EnumType, typename ContainerType>
	inline flagropeprimitive<EnumType, ContainerType>::flagropeprimitive(const std::initializer_list<EnumType>& aFlagropeToSet)
	{
		myContainer = 0;
		for (ContainerType i = 0; i < aFlagropeToSet.size(); i++)
		{
			Enable(*(aFlagropeToSet.begin() + i));
		}
	}

	template<typename EnumType, typename ContainerType>
	inline flagropeprimitive<EnumType, ContainerType>::~flagropeprimitive()
	{
	}

	template<typename EnumType, typename ContainerType>
	inline flagropeprimitive<EnumType, ContainerType> &flagropeprimitive<EnumType, ContainerType>::operator=(const flagropeprimitive & aFlagropeToAssignFrom)
	{
		myContainer = aFlagropeToAssignFrom.myContainer;
		return *this;
	}

	template<typename EnumType, typename ContainerType = unsigned char >
	inline bool flagropeprimitive<EnumType, ContainerType>::operator==(const flagropeprimitive & aFlagropeToCompareWith) const
	{
		return (myContainer == aFlagropeToCompareWith.myContainer);
	}

	template<typename EnumType, typename ContainerType = unsigned char >
	inline bool flagropeprimitive<EnumType, ContainerType>::operator!=(const flagropeprimitive & aFlagropeToCompareWith) const
	{
		return (myContainer != aFlagropeToCompareWith.myContainer);
	}

	template<typename EnumType, typename ContainerType = unsigned char >
	inline void flagropeprimitive<EnumType, ContainerType>::Enable(const EnumType &aFlagToAdd)
	{
		assert(static_cast<float>(aFlagToAdd) >= 0 && "Lowest value should be 0!");
		myContainer |= ConvertToPowerOfTwo(static_cast<ContainerType>(aFlagToAdd));
	}

	template<typename EnumType, typename ContainerType>
	inline void flagropeprimitive<EnumType, ContainerType>::Enable(const flagropeprimitive & aFlagropeToAdd)
	{
		assert(static_cast<float>(aFlagropeToAdd.myContainer) >= 0 && "Lowest value should be 0!");
		myContainer |= ConvertToPowerOfTwo(aFlagropeToAdd.myContainer);
	}

	template<typename EnumType, typename ContainerType = unsigned char >
	inline void flagropeprimitive<EnumType, ContainerType>::Disable(const EnumType &aFlagToRemove)
	{
		myContainer &= ~ConvertToPowerOfTwo(static_cast<ContainerType>(aFlagToRemove));
	}

	template<typename EnumType, typename ContainerType>
	inline void flagropeprimitive<EnumType, ContainerType>::Disable(const flagropeprimitive & aFlagropeToRemove)
	{
		myContainer &= ~aFlagropeToRemove.myContainer;
	}

	template<typename EnumType, typename ContainerType>
	inline void flagropeprimitive<EnumType, ContainerType>::Flip(const EnumType & aFlagToFlipWith)
	{
		myContainer ^= ConvertToPowerOfTwo(static_cast<ContainerType>(aFlagToFlipWith));
	}

	template<typename EnumType, typename ContainerType>
	inline void flagropeprimitive<EnumType, ContainerType>::Flip(const flagropeprimitive & aFlagropeToFlipWith)
	{
		myContainer ^= aFlagropeToFlipWith.myContainer;
	}

	template<typename EnumType, typename ContainerType = unsigned char >
	inline bool flagropeprimitive<EnumType, ContainerType>::Has(const EnumType &aFlagToCheck)
	{
		return ((myContainer & ConvertToPowerOfTwo(static_cast<ContainerType>(aFlagToCheck))) != 0);
	}

	template<typename EnumType, typename ContainerType>
	inline bool flagropeprimitive<EnumType, ContainerType>::Has(const flagropeprimitive & aFlagropeToCompare) const
	{
		return myContainer >= aFlagropeToCompare.myContainer;
	}

	template<typename EnumType, typename ContainerType>
	inline bool flagropeprimitive<EnumType, ContainerType>::HasAny()
	{
		return (myContainer > 0);
	}

	template<typename EnumType, typename ContainerType>
	inline int flagropeprimitive<EnumType, ContainerType>::Size() const
	{
		return std::numeric_limits<ContainerType>::digits;
	}

	template<typename EnumType, typename ContainerType>
	inline ContainerType flagropeprimitive<EnumType, ContainerType>::ConvertToPowerOfTwo(const ContainerType &aValueToConvert)
	{
		ContainerType result = 1;
		assert(std::numeric_limits<ContainerType>::digits > aValueToConvert && "EnumType index too high for ContainerType!");
		result = result << (aValueToConvert);

		return result;
	}

	template<typename EnumType, typename ContainerType>
	inline bool flagropeprimitive<EnumType, ContainerType>::IsPowerOfTwo(const ContainerType & aValueToCheck) const
	{
		return ((aValueToCheck != 0) && (aValueToCheck & (aValueToCheck - 1)) == 0);
	}
}
namespace cit = carlivan_tools;
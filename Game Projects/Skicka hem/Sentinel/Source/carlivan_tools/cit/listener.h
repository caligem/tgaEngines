#pragma once
namespace carlivan_tools
{
	template<typename NotificationType>
	class listener
	{
	public:
		virtual void RecieveNotification(const NotificationType aNotificationToRecieve) = 0;
	};
}
namespace cit = carlivan_tools;
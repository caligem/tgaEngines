#pragma once
#include "listener.h"
#include <vector>

namespace carlivan_tools
{
	template<typename MessageType>
	class notifier
	{
	public:
		void NotifyListeners(const MessageType aMessageToSend);
		void AddListener(listener<MessageType> &aListenerToAdd);
		void RemoveListener(listener<MessageType> &aListenerToAdd);

	private:
		std::vector<listener<MessageType>*> myListeners;

	};

	template<typename MessageType>
	void notifier<MessageType>::NotifyListeners(const MessageType aMessageToSend)
	{
		for (unsigned int i = 0; i < myListeners.size(); i++)
		{
			myListeners[i]->RecieveNotification(aMessageToSend);
		}
	}

	template<typename MessageType>
	void notifier<MessageType>::AddListener(listener<MessageType>& aListenerToAdd)
	{
		myListeners.push_back(&aListenerToAdd);
	}

	template<typename MessageType>
	void notifier<MessageType>::RemoveListener(listener<MessageType>& aListenerToAdd)
	{
		for (unsigned int i = 0; i < myListeners.size(); i++)
		{
			if (myListeners[i] == &aListenerToAdd)
			{
				myListeners.erase(myListeners.begin() + i);
				return;
			}
		}
	}
}
namespace cit = carlivan_tools;

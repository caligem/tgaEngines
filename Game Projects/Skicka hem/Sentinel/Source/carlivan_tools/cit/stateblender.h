#pragma once
#include <vector>
#include <assert.h>
#include "countdown.h"
#include <iostream>

namespace carlivan_tools
{
	template <typename StateType>
	class stateblender
	{
	public:
		stateblender();
		void CheckForStateChange(StateType aCurrentState, float aDeltaTime);

		void AddState(StateType aStateToSet);
		void AddBlendState(StateType aFrom, StateType aTo, StateType aStateToSet, float aBlendTime);
		void Pause();
		void Unpause();
		StateType GetActiveState() const;
	private:
		void SetActiveState(StateType aStateToSet);
		void SetBlendState(StateType aPreviousState, StateType aCurrentState);
		bool CurrentStateIsBlendState() const;
		bool EdgeExistsBetween(StateType aFromState, StateType aToState) const;
		bool StateAlreadyAdded(StateType aStateToCheck);
		struct StateNode
		{
			StateType myState;
		};
		struct BlendEdge
		{
			StateNode *myFromNode = nullptr;
			StateNode *myToNode = nullptr;
			StateType myBlendState;
			float myBlendTime;
		};
		std::vector<StateNode> myNodes;
		std::vector<BlendEdge> myEdges;
		cit::countdown myCurrentBlendTimer;
		StateType myActiveState;
		StateType myPreviousState;
		BlendEdge *myEdgeToSwapFrom;

		bool myIsSwapping;
		bool myIsPaused;
	};

	template<typename StateType>
	inline stateblender<StateType>::stateblender()
	{
		myEdgeToSwapFrom = nullptr;
		myIsSwapping = false;
		myActiveState = static_cast<StateType>(0);
		myPreviousState = static_cast<StateType>(0);
		myIsPaused = false;
	}

	template<typename StateType>
	inline void stateblender<StateType>::CheckForStateChange(StateType aCurrentState, float aDeltaTime)
	{
		if (myIsPaused == true)
		{
			return;
		}
		if (myIsSwapping == true)
		{
			SetActiveState(myEdgeToSwapFrom->myToNode->myState);
			myEdgeToSwapFrom = nullptr;
			myIsSwapping = false;
		}
		else if (myPreviousState != aCurrentState && CurrentStateIsBlendState() == false && myActiveState != aCurrentState)
		{
			SetBlendState(myPreviousState, aCurrentState);
			myPreviousState = myActiveState;
		}
		else
		{
			myPreviousState = aCurrentState;
		}
		if (myEdgeToSwapFrom == nullptr)
		{
			myActiveState = aCurrentState;
		}
		else
		{
			if (EdgeExistsBetween(myEdgeToSwapFrom->myFromNode->myState, aCurrentState) == false)
			{
				myActiveState = aCurrentState;
			}
		}
		myCurrentBlendTimer.Update(aDeltaTime);
	}

	template<typename StateType>
	inline void stateblender<StateType>::AddState(StateType aStateToSet)
	{
		assert(StateAlreadyAdded(aStateToSet) == false && "That state already exists in stateblender");
		myNodes.push_back(StateNode());
		myNodes[myNodes.size() - 1].myState = aStateToSet;
	}

	template<typename StateType>
	inline void stateblender<StateType>::AddBlendState(StateType aFrom, StateType aTo, StateType aStateToSet, float aBlendTime)
	{
		//assert(StateAlreadyAdded(aStateToSet) == false && "That state already exists in stateblender!");
		BlendEdge toAdd;
		for (size_t i = 0; i < myNodes.size(); i++)
		{
			if (myNodes[i].myState == aFrom)
			{
				toAdd.myFromNode = &myNodes[i];
			}
			if (myNodes[i].myState == aTo)
			{
				toAdd.myToNode = &myNodes[i];
			}
		}
		assert((toAdd.myFromNode != nullptr && toAdd.myToNode != nullptr) && "Can't find connecting nodes!");
		toAdd.myBlendState = aStateToSet;
		toAdd.myBlendTime = aBlendTime;
		myEdges.push_back(toAdd);
	}

	template<typename StateType>
	inline void stateblender<StateType>::Pause()
	{
		myIsPaused = true;
	}

	template<typename StateType>
	inline void stateblender<StateType>::Unpause()
	{
		myIsPaused = false;
	}

	template<typename StateType>
	inline StateType stateblender<StateType>::GetActiveState() const
	{
		return myActiveState;
	}

	template<typename StateType>
	inline void stateblender<StateType>::SetActiveState(StateType aStateToSet)
	{
		myActiveState = aStateToSet;
	}

	template<typename StateType>
	inline void stateblender<StateType>::SetBlendState(StateType aPreviousState, StateType aCurrentState)
	{
		for (size_t i = 0; i < myEdges.size(); i++)
		{
			if (myEdges[i].myFromNode->myState == aPreviousState && myEdges[i].myToNode->myState == aCurrentState)
			{
				myActiveState = myEdges[i].myBlendState;
				myEdgeToSwapFrom = &myEdges[i];
				myCurrentBlendTimer.Set(myEdgeToSwapFrom->myBlendTime, cit::countdown::type::oneshot, [&] {myIsSwapping = true; });
				myCurrentBlendTimer.Start();
				return;
			}
		}
		myActiveState = aCurrentState;
	}

	template<typename StateType>
	inline bool stateblender<StateType>::CurrentStateIsBlendState() const
	{
		for (size_t i = 0; i < myEdges.size(); i++)
		{
			if (myEdges[i].myBlendState == myActiveState)
			{
				return true;
			}
		}
		return false;
	}

	template<typename StateType>
	inline bool stateblender<StateType>::EdgeExistsBetween(StateType aFromState, StateType aToState) const
	{
		for (size_t i = 0; i < myEdges.size(); i++)
		{
			if (myEdges[i].myFromNode->myState == aFromState && myEdges[i].myToNode->myState == aToState)
			{
				return true;
			}
		}
		return false;
	}

	template<typename StateType>
	inline bool stateblender<StateType>::StateAlreadyAdded(StateType aStateToCheck)
	{
		for (size_t i = 0; i < myNodes.size(); i++)
		{
			if (myNodes[i].myState == aStateToCheck)
			{
				return true;
			}
		}
		for (size_t i = 0; i < myEdges.size(); i++)
		{
			if (myEdges[i].myBlendState == aStateToCheck)
			{
				return true;
			}
		}
		return false;
	}
}
namespace cit = carlivan_tools;
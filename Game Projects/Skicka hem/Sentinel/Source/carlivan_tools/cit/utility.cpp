#include "utility.h"
#include <algorithm>

float carlivan_tools::Lerp(const float aFrom, const float aTo, const float aPercentage)
{
	return (1 - aPercentage)*aFrom + aPercentage*aTo;
}

float carlivan_tools::Clamp(const float aValueToClamp, const float aMin, const float aMax)
{
	return std::max(aMin, std::min(aValueToClamp, aMax));
}

int carlivan_tools::Clamp(const int aValueToClamp, const int aMin, const int aMax)
{
	return std::max(aMin, std::min(aValueToClamp, aMax));
}

#pragma once
#include "Vector.h"
struct AABB
{
	AABB(CommonUtilities::Vector2f aMin, CommonUtilities::Vector2f aMax)
	{
		myMin = aMin;
		myMax = aMax;
	}
	CommonUtilities::Vector2f myMin;
	CommonUtilities::Vector2f myMax;
};

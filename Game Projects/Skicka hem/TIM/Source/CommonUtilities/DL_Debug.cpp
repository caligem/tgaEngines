#include "DL_Debug.h"
#include <ctime>
#include <iomanip>
#include <sstream>

DL_Debug::Debug::Debug(std::string aFile)
{
	myDebugFile.open(aFile, std::ofstream::app);
}

DL_Debug::Debug::~Debug()
{
	if (ourInstance != nullptr)
	{
		ourInstance->myDebugFile.close();
		SAFE_DELETE(ourInstance);
	}
}

std::string DL_Debug::Debug::Filename()
{
	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);

	std::ostringstream oss;
	oss << std::put_time(&tm, "debugLogger_%Y-%m-%d-%H.txt");
	auto str = oss.str();
	return str;
}

DL_Debug::Debug* DL_Debug::Debug::ourInstance;

bool DL_Debug::Debug::Create(std::string aFile)
{
	ourInstance = new Debug(aFile);
	DL_ASSERT(ourInstance != nullptr && "Debugobject already created");
	if (ourInstance == nullptr)
	{
		return(false);
	}
	return(true);
}

bool DL_Debug::Debug::Destroy()
{
	ourInstance->myDebugFile.close();
	SAFE_DELETE(ourInstance);
	return(true);
}

DL_Debug::Debug* DL_Debug::Debug::GetInstance()
{
	return(ourInstance);
}

bool DL_Debug::Debug::AssertMessage(const char* aFileName, int aLine, const char* aFunctionName, const char* aString)
{
	std::ostringstream oss;
	oss << "File: " << aFileName << ", Line: " << aLine << ", Function: " << aFunctionName << ", Message: " << aString << "\n";
	oss << "\n======= CALLSTACK BELOW =======\n";
	const std::string& str = oss.str();
	const char* message = str.c_str();
	DL_PRINT(message);
	DL_Debug::StackWalker stackWalker;
	stackWalker.ShowCallstack();
	return false;
}

void DL_Debug::Debug::PrintMessage(const char* aString)
{
	myDebugFile << aString << "\n";
	myDebugFile.flush();
}

void DL_Debug::Debug::DebugMessage(const int aLine, const char* aFunctionName, const char* aFormattedString, ...)
{
	va_list args;
	va_start(args, aFormattedString);
	
	std::ostringstream oss;
	oss << "File: " << aFunctionName << ", Line: " << aLine << ", Message: ";

	std::string tempstr = "";

	while (*aFormattedString != '\0') {
		if (*aFormattedString == '%' && (*(aFormattedString + 1)) == 'd') {
			int i = va_arg(args, int);
			oss << i;
			++aFormattedString;
		}
		else if (*aFormattedString == '%' && (*(aFormattedString + 1)) == 'c') {
			int c = va_arg(args, int);
			oss << static_cast<char>(c);
			++aFormattedString;
		}
		else if (*aFormattedString == '%' && (*(aFormattedString + 1)) == 'f') {
			double d = va_arg(args, double);
			oss << d;
			++aFormattedString;
		}
		else
		{
			oss << *aFormattedString;
		}

		++aFormattedString;
	}
		
	va_end(args);

	const std::string& str = oss.str();
	const char* message = str.c_str();
	DL_PRINT(message);
}

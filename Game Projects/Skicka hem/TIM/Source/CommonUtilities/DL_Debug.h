#pragma once

#include <string>
#include <fstream>

#include "Macros.h"
#include "DL_Assert.h"
#include "DL_StackWalker.h"

//#define LOG_ASSERT(string) ;
#define DL_PRINT(string) DL_Debug::Debug::GetInstance()->PrintMessage(string);
#define DL_DEBUG(...) DL_Debug::Debug::GetInstance()->DebugMessage(__LINE__, __FUNCTION__, __VA_ARGS__);

namespace DL_Debug
{
	class Debug
	{
	public:
		static bool Create(std::string aFile = Filename());
		static bool Destroy();
		static Debug* GetInstance();

		bool AssertMessage(const char *aFileName, int aLine, const char *aFunctionName, const char *aString);
		void PrintMessage(const char *aString);
		void DebugMessage(const int aLine, const char *aFunctionName, const char *aFormattedString, ...);
	private:
		Debug(std::string aFile);
		~Debug();
		static Debug* ourInstance;
		std::ofstream myDebugFile;
		static std::string Filename();
	};
}

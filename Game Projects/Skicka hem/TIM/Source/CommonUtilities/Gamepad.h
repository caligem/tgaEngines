#pragma once

#include <Xinput.h>

// XInput Button values
static const WORD XINPUT_Buttons[] = {
	XINPUT_GAMEPAD_A,
	XINPUT_GAMEPAD_B,
	XINPUT_GAMEPAD_X,
	XINPUT_GAMEPAD_Y,
	XINPUT_GAMEPAD_DPAD_UP,
	XINPUT_GAMEPAD_DPAD_DOWN,
	XINPUT_GAMEPAD_DPAD_LEFT,
	XINPUT_GAMEPAD_DPAD_RIGHT,
	XINPUT_GAMEPAD_LEFT_SHOULDER,
	XINPUT_GAMEPAD_RIGHT_SHOULDER,
	XINPUT_GAMEPAD_LEFT_THUMB,
	XINPUT_GAMEPAD_RIGHT_THUMB,
	XINPUT_GAMEPAD_START,
	XINPUT_GAMEPAD_BACK
};

// XInput Button IDs
struct XButtonIDs
{
	XButtonIDs();

	// Member variables
	//---------------------//

	// 'Action' buttons
	int A;
	int B;
	int X;
	int Y;

	// Directional Pad (D-Pad)
	int DPad_Up;
	int DPad_Down;
	int DPad_Left;
	int DPad_Right;

	// Shoulder ('Bumper') buttons
	int L_Shoulder;
	int R_Shoulder;

	// Thumbstick buttons
	int L_Thumbstick;
	int R_Thumbstick;

	int Start;
	int Back;
};

class Gamepad
{
public:
	Gamepad();
	Gamepad(int aIndex);
	~Gamepad();

	// Update gamepad state
	void Update();
	void RefreshState();

	// Thumbstick functions
	// - Return true if stick is inside deadzone, false if outside
	bool LStick_InDeadzone();
	bool RStick_InDeadzone();

	float LeftStick_X();  // Return X axis of left stick
	float LeftStick_Y();  // Return Y axis of left stick
	float RightStick_X(); // Return X axis of right stick
	float RightStick_Y(); // Return Y axis of right stick

	// Trigger functions
	float LeftTrigger();  // Return value of left trigger
	float RightTrigger(); // Return value of right trigger

	// Button functions
	// - 'Pressed' - Return true if pressed, false if not
	// - 'Down'    - Same as 'Pressed', but ONLY on current frame
	bool GetButtonDown(int aButton);
	bool GetButtonPressed(int aButton);

	// Utility functions
	XINPUT_STATE GetState(); // Return gamepad state
	int GetIndex();          // Return gamepad index
	bool Connected();        // Return true if gamepad is connected

	// Vibrate the gamepad (0.0f cancel, 1.0f max speed)
	void Rumble(float aLeftMotor = 0.0f, float aRightMotor = 0.0f);
	bool GetAnyButtonPressed();
private:
	// Member variables
	//---------------------//

	XINPUT_STATE myState; // Current gamepad state
	int myGamepadIndex;  // Gamepad index (eg. 1,2,3,4)

	static const int myButtonCount = 14;    // Total gamepad buttons
	bool myPrevButtonStates[myButtonCount]; // Previous frame button states
	bool myButtonStates[myButtonCount];     // Current frame button states

	// Buttons pressed on current frame
	bool myGamepadButtonsDown[myButtonCount];
};

// Externally define the 'XButtonIDs' struct as 'XButtons'
extern XButtonIDs XButtons;

/*

// The left stick is outside of its deadzone
if (!LStick_InDeadzone())
{
// Use left stick input here...
}

if (LeftTrigger() > 0.0f)
{
// The trigger has been pressed (even slightest tap)...
}

// Only rumble left
Rumble(1.0f, 0.0f);

// Only rumble right
Rumble(0.0f, 0.6f);

// Rumble both
Rumble(0.3f, 0.3f);

// Cancel rumble
Rumble();

*/
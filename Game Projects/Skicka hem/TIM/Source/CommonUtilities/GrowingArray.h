#pragma once
#include <memory>
#include <assert.h>

namespace CommonUtilities
{
	template<typename T, typename SizeType = unsigned short>
	class GrowingArray
	{
	public:
		GrowingArray();
		GrowingArray(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag = true);
		GrowingArray(const GrowingArray& aGrowingArray);
		GrowingArray(GrowingArray&& aGrowingArray);

		~GrowingArray();

		GrowingArray& operator=(const GrowingArray& aGrowingArray);
		GrowingArray& operator=(GrowingArray&& aGrowingArray);

		void Init(SizeType aNrOfRecomendedItems, bool aUseSafeModeFlag = true);
		void ReInit(SizeType aNrOfRecomendedItems, bool aUseSafeModeFlag = true);

		inline T& operator[](const SizeType& aIndex);
		inline const T& operator[](const SizeType& aIndex) const;

		inline void Add(const T& aObject);
		inline void Insert(SizeType aIndex, T& aObject);
		inline void DeleteCyclic(T& aObject);
		inline void DeleteCyclicAtIndex(SizeType aIndex);
		inline void RemoveCyclic(const T& aObject);
		inline void RemoveCyclicAtIndex(SizeType aIndex);
		inline SizeType Find(const T& aObject);

		inline T& GetLast();
		inline const T& GetLast() const;

		static const SizeType FoundNone = static_cast<SizeType>(-1);

		inline void RemoveAll();
		inline void DeleteAll();

		inline T* begin();
		inline const T* begin() const;
		inline T* end();
		inline const T* end() const;

		void Optimize();
		__forceinline SizeType Size() const;
		__forceinline SizeType Count() const;
		inline void Resize(SizeType aNewSize);

	private:
		SizeType myCount;
		SizeType myCapacity;
		T* myElements;
		bool myInitiated;
		bool mySafeFlag;
	};

	template<typename T, typename SizeType>
	inline GrowingArray<T, SizeType>::GrowingArray()
	{
		myCount = static_cast<SizeType>(0);
		myCapacity = static_cast<SizeType>(0);
		myElements = nullptr;
		myInitiated = false;
		mySafeFlag = true;
	}

	template<typename T, typename SizeType>
	inline GrowingArray<T, SizeType>::GrowingArray(SizeType aNrOfRecommendedItems, bool aUseSafeModeFlag)
	{
		Init(aNrOfRecommendedItems, aUseSafeModeFlag);
	}

	template<typename T, typename SizeType>
	inline GrowingArray<T, SizeType>::GrowingArray(const GrowingArray& aGrowingArray)
	{
		*this = aGrowingArray;
	}

	template<typename T, typename SizeType>
	inline GrowingArray<T, SizeType>::GrowingArray(GrowingArray&& aGrowingArray)
	{
		myCount = aGrowingArray.myCount;
		myCapacity = aGrowingArray.myCapacity;
		myElements = aGrowingArray.myElements;
		mySafeFlag = aGrowingArray.mySafeFlag;
		aGrowingArray.myElements = nullptr;
	}

	template<typename T, typename SizeType>
	inline GrowingArray<T, SizeType>::~GrowingArray()
	{
		myCount = static_cast<SizeType>(0);
		myCapacity = static_cast<SizeType>(0);
		myInitiated = false;
		mySafeFlag = true;
		if (myElements != nullptr)
		{
			delete[] myElements;
			myElements = nullptr;
		}
	}

	template<typename T, typename SizeType>
	inline GrowingArray<T, SizeType>& GrowingArray<T, SizeType>::operator=(const GrowingArray& aGrowingArray)
	{
		myCount = aGrowingArray.myCount;
		myCapacity = aGrowingArray.myCapacity;
		myElements = new T[aGrowingArray.myCapacity];
		myInitiated = aGrowingArray.myInitiated;
		mySafeFlag = aGrowingArray.mySafeFlag;
		if (mySafeFlag)
		{
			for (SizeType i = static_cast<SizeType>(0); i < myCount; ++i)
			{
				myElements[i] = aGrowingArray.myElements[i];
			}
		}
		else
		{
			memcpy(myElements, aGrowingArray.myElements, sizeof(T)*myCount);
		}
		return *this;
	}

	template<typename T, typename SizeType>
	inline GrowingArray<T, SizeType>& GrowingArray<T, SizeType>::operator=(GrowingArray&& aGrowingArray)
	{
		myCount = aGrowingArray.myCount;
		myCapacity = aGrowingArray.myCapacity;
		myElements = aGrowingArray.myElements;
		mySafeFlag = aGrowingArray.mySafeFlag;
		aGrowingArray.myElements = nullptr;
		return *this;
	}

	template<typename T, typename SizeType>
	inline void GrowingArray<T, SizeType>::Init(SizeType aNrOfRecomendedItems, bool aUseSafeModeFlag)
	{
		myCount = static_cast<SizeType>(0);
		myCapacity = aNrOfRecomendedItems;
		myElements = new T[myCapacity];
		myInitiated = true;
		mySafeFlag = aUseSafeModeFlag;
	}

	template<typename T, typename SizeType>
	inline void GrowingArray<T, SizeType>::ReInit(SizeType aNrOfRecomendedItems, bool aUseSafeModeFlag)
	{
		if (myInitiated)
		{
			Init(aNrOfRecomendedItems, aUseSafeModeFlag);
		}
	}

	template<typename T, typename SizeType>
	inline T& GrowingArray<T, SizeType>::operator[](const SizeType& aIndex)
	{
		assert(myInitiated && "Array not initiated.");
		assert((aIndex >= static_cast<SizeType>(0) && aIndex < myCount) && "Index outside array");
		return myElements[aIndex];
	}

	template<typename T, typename SizeType>
	inline const T& GrowingArray<T, SizeType>::operator[](const SizeType& aIndex) const
	{
		assert(myInitiated && "Array not initiated.");
		assert((aIndex >= static_cast<SizeType>(0) && aIndex < myCount) && "Index outside array");
		return myElements[aIndex];
	}

	template<typename T, typename SizeType>
	inline void GrowingArray<T, SizeType>::Add(const T& aObject)
	{
		assert(myInitiated && "Array not initiated.");
		if (myCount == myCapacity)
		{
			Resize(myCapacity * 2);
		}
		myElements[myCount] = aObject;
		++myCount;
	}

	template<typename T, typename SizeType>
	inline void GrowingArray<T, SizeType>::Insert(SizeType aIndex, T& aObject)
	{
		assert(myInitiated && "Array not initiated.");
		assert(myCount > static_cast<SizeType>(0) && "Empty array");
		if (myCount == myCapacity)
		{
			Resize(myCapacity * 2);
		}
		for (SizeType i = myCount; i > aIndex; --i)
		{
			myElements[i] = myElements[i - 1];
		}
		myElements[aIndex] = aObject;
		++myCount;
	}

	template<typename T, typename SizeType>
	inline void GrowingArray<T, SizeType>::DeleteCyclic(T& aObject)
	{
		assert(myInitiated && "Array not initiated.");
		SizeType index = Find(aObject);
		if (index != FoundNone)
		{
			DeleteCyclicAtIndex(index);
		}
	}

	template<typename T, typename SizeType>
	inline void GrowingArray<T, SizeType>::DeleteCyclicAtIndex(SizeType aIndex)
	{
		assert(myInitiated && "Array not initiated.");
		assert((aIndex >= static_cast<SizeType>(0) && aIndex < myCount) && "Index outside array");
		delete myElements[aIndex];
		RemoveCyclicAtIndex(aIndex);
	}

	template<typename T, typename SizeType>
	inline void GrowingArray<T, SizeType>::RemoveCyclic(const T& aObject)
	{
		assert(myInitiated && "Array not initiated.");
		SizeType index = Find(aObject);
		if (index != FoundNone)
		{
			RemoveCyclicAtIndex(index);
		}
	}

	template<typename T, typename SizeType>
	inline void GrowingArray<T, SizeType>::RemoveCyclicAtIndex(SizeType aIndex)
	{
		assert(myInitiated && "Array not initiated.");
		assert((aIndex >= static_cast<SizeType>(0) && aIndex < myCount) && "Index outside array");
		myElements[aIndex] = myElements[myCount - 1];
		--myCount;
	}

	template<typename T, typename SizeType>
	inline SizeType GrowingArray<T, SizeType>::Find(const T & aObject)
	{
		assert(myInitiated && "Array not initiated.");
		for (SizeType i = 0; i < myCount; ++i)
		{
			if (myElements[i] == aObject)
			{
				return i;
			}
		}
		return FoundNone;
	}

	template<typename T, typename SizeType>
	inline T& GrowingArray<T, SizeType>::GetLast()
	{
		assert(myInitiated && "Array not initiated.");
		assert((myCount > 0) && "Empty array");
		return myElements[myCount - 1];
	}

	template<typename T, typename SizeType>
	inline const T& GrowingArray<T, SizeType>::GetLast() const
	{
		assert(myInitiated && "Array not initiated.");
		assert((myCount > 0) && "Empty array");
		return myElements[myCount - 1];
	}

	template<typename T, typename SizeType>
	inline void GrowingArray<T, SizeType>::RemoveAll()
	{
		assert(myInitiated && "Array not initiated.");
		myCount = static_cast<SizeType>(0);
	}

	template<typename T, typename SizeType>
	inline void GrowingArray<T, SizeType>::DeleteAll()
	{
		assert(myInitiated && "Array not initiated.");
		for (int i = myCount - 1; i > 0; --i)
		{
			delete myElements[i];
			myElements[i] = nullptr;
		}
		RemoveAll();
	}

	template<typename T, typename SizeType>
	inline T * GrowingArray<T, SizeType>::begin()
	{
		return &myElements[0];
	}

	template<typename T, typename SizeType>
	inline const T * GrowingArray<T, SizeType>::begin() const
	{
		return &myElements[0];
	}

	template<typename T, typename SizeType>
	inline T * GrowingArray<T, SizeType>::end()
	{
		return &myElements[myCount];
	}

	template<typename T, typename SizeType>
	inline const T * GrowingArray<T, SizeType>::end() const
	{
		return &myElements[myCount];
	}

	template<typename T, typename SizeType>
	inline void GrowingArray<T, SizeType>::Optimize()
	{
		assert(myInitiated && "Array not initiated.");
		if (myCount != myCapacity)
		{
			T* newArray = new T[myCount];
			newArray = myElements;
			delete myElements;
			myElements = newArray;
		}
	}

	template<typename T, typename SizeType>
	inline SizeType GrowingArray<T, SizeType>::Size() const
	{
		return myCount;
	}

	template<typename T, typename SizeType>
	inline SizeType GrowingArray<T, SizeType>::Count() const
	{
		return myCount;
	}

	template<typename T, typename SizeType>
	inline void GrowingArray<T, SizeType>::Resize(SizeType aNewSize)
	{
		T* newArray = new T[aNewSize];
		if (mySafeFlag)
		{
			for (SizeType i = static_cast<SizeType>(0); i < myCount; ++i)
			{
				newArray[i] = myElements[i];
			}
		}
		else
		{
			memcpy(newArray, myElements, sizeof(T)*myCount);
		}
		delete[] myElements;
		myElements = newArray;
		myCapacity = aNewSize;
	}
};

namespace CU = CommonUtilities;
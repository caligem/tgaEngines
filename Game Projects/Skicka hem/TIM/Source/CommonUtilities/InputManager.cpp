#include "InputManager.h"
#include <string>
#include <windowsx.h>
#include "EnumKeys.h"

namespace CommonUtilities
{
	InputManager::InputManager()
	{
		myCurrentFrameKeyStates.reset();
		myPreviousFrameKeyStates.reset();
		myCurrentMousePosX = 0;
		myPreviousMousePosX = 0;
		myCurrentMousePosY = 0;
		myPreviousMousePosY = 0;
		myCurrentMouseWheelState = 0;
		myPreviousMouseWheelState = 0;
	}

	InputManager::~InputManager()
	{
	}

	void InputManager::Update()
	{
		myPreviousFrameKeyStates = myCurrentFrameKeyStates;

		myPreviousMousePosX = myCurrentMousePosX;
		myPreviousMousePosY = myCurrentMousePosY;

		myCurrentMouseWheelState = 0;
		//myPreviousMouseWheelState = myCurrentMouseWheelState;
	}

	void InputManager::HandleMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
	{
		switch (message)
		{
		case WM_MOUSEMOVE:
			HandleMouseMovement(lParam);
			break;
		case WM_MOUSEWHEEL:
			HandleMousewheelMovement(wParam);
			break;
		case WM_KEYDOWN:
			if (wParam < 256)
			{
				HandleInput(wParam, true);
			}
			break;
		case WM_LBUTTONDOWN:
			HandleMousebuttonInput(CU::Keys::LeftMouseButton, true);
			break;
		case WM_MBUTTONDOWN:
			HandleMousebuttonInput(CU::Keys::MiddleMouseButton, true);
			break;
		case WM_RBUTTONDOWN:
			HandleMousebuttonInput(CU::Keys::RightMouseButton, true);
			break;
		case WM_LBUTTONUP:
			HandleMousebuttonInput(CU::Keys::LeftMouseButton, false);
			break;
		case WM_MBUTTONUP:
			HandleMousebuttonInput(CU::Keys::MiddleMouseButton, false);
			break;
		case WM_RBUTTONUP:
			HandleMousebuttonInput(CU::Keys::RightMouseButton, false);
			break;
		case WM_KEYUP:
			if (wParam < 256)
			{
				HandleInput(wParam, false);
			}
			break;
		case WM_XBUTTONDOWN: //XBUTTON1 or XBUTTON2 down
			break;
		case WM_XBUTTONUP: // XBUTTON1 or XBUTTON2 up
			break;
		}
	}

	void InputManager::HandleInput(WPARAM wParam, bool aState)
	{
		myCurrentFrameKeyStates.set(wParam, aState);
	}

	void InputManager::HandleMousebuttonInput(const uint8_t aButton, bool aState)
	{
		myCurrentFrameKeyStates.set(aButton, aState);
	}

	void InputManager::HandleMouseMovement(LPARAM lParam)
	{
		myCurrentMousePosX = GET_X_LPARAM(lParam);
		myCurrentMousePosY = GET_Y_LPARAM(lParam);
	}

	void InputManager::HandleMousewheelMovement(WPARAM wParam)
	{
		myCurrentMouseWheelState = GET_WHEEL_DELTA_WPARAM(wParam);
	}

	bool InputManager::KeyDown(const uint8_t aKey)
	{
		return myCurrentFrameKeyStates[aKey];
	}

	bool InputManager::KeyUp(const uint8_t aKey)
	{
		return !myCurrentFrameKeyStates[aKey];
	}

	bool InputManager::KeyPressed(const uint8_t aKey)
	{
		return (!myPreviousFrameKeyStates[aKey] && myCurrentFrameKeyStates[aKey]);
	}

	bool InputManager::KeyReleased(const uint8_t aKey)
	{
		return (myPreviousFrameKeyStates[aKey] && !myCurrentFrameKeyStates[aKey]);
	}

	int InputManager::DeltaScrollwheelMovement() const
	{
		return myCurrentMouseWheelState - myPreviousMouseWheelState;
	}

	float InputManager::GetScroll()
	{
		return static_cast<float>(myCurrentMouseWheelState) / WHEEL_DELTA;
	}

	void InputManager::GetMousePosition(int & aX, int & aY) const
	{
		aX = myCurrentMousePosX;
		aY = myCurrentMousePosY;
	}

	void InputManager::SetMousePosition(int aX, int aY)
	{
		myCurrentMousePosX = aX;
		myCurrentMousePosY = aY;
	}

	void InputManager::DeltaMouseMovement(int & aX, int & aY) const
	{
		aX = myCurrentMousePosX - myPreviousMousePosX;
		aY = myCurrentMousePosY - myPreviousMousePosY;
	}
	void InputManager::ResetInput()
	{
		myCurrentFrameKeyStates.reset();
		myPreviousFrameKeyStates.reset();
	}
}

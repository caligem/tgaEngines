#pragma once
#include <windows.h>
#include <bitset>
#include "EnumKeys.h"

namespace CommonUtilities
{
	class InputManager
	{
	public:
		InputManager();
		~InputManager();

		void Update();

		void HandleMessage(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);

		bool KeyDown(const uint8_t aKey);
		bool KeyUp(const uint8_t aKey);
		bool KeyPressed(const uint8_t aKey);
		bool KeyReleased(const uint8_t aKey);

		// Kolla scrollhjulets f�rflyttning sedan f�reg�ende frame
		int DeltaScrollwheelMovement() const;
		float GetScroll();
		// Kolla musens absoluta position(anv�nd Windows inbyggda funktioner)
		void GetMousePosition(int &aX, int &aY) const;

		// S�tta musens position(anv�nds Windows inbyggda funktioner)
		void SetMousePosition(const int aX, const int aY);

		// H�mta musens f�rflyttning sedan f�reg�ende update
		void DeltaMouseMovement(int &aX, int &aY) const;

		void ResetInput();
	private:
		void HandleInput(WPARAM aWParam, bool aState);
		void HandleMousebuttonInput(const uint8_t aButton, bool aState);
		void HandleMouseMovement(LPARAM lParam);
		void HandleMousewheelMovement(WPARAM wParam);

		std::bitset<256> myCurrentFrameKeyStates;
		std::bitset<256> myPreviousFrameKeyStates;
		int myCurrentMousePosX;
		int myPreviousMousePosX;
		int myCurrentMousePosY;
		int myPreviousMousePosY;
		int myCurrentMouseWheelState;
		int myPreviousMouseWheelState;
	};
}

namespace CU = CommonUtilities;

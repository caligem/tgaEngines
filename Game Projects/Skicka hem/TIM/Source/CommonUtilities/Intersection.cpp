#include "Intersection.h"
#include "Macros.h"

namespace CommonUtilities
{
	namespace Intersection
	{
		bool PointInsideSphere(const Sphere& aSphere, const Vector3f aPoint)
		{
			return aSphere.radius * aSphere.radius > (aPoint - aSphere.center).Length2();
		}

		bool PointInsideAABB(const AABB3D& aAABB, const Vector3f& aPoint)
		{
			if (
				!(aPoint.x > aAABB.min.x) ||
				!(aPoint.x < aAABB.max.x) ||
				!(aPoint.y > aAABB.min.y) ||
				!(aPoint.y < aAABB.max.y) ||
				!(aPoint.z > aAABB.min.z) ||
				!(aPoint.z < aAABB.max.z))
				return false;
			return true;
		}

		bool IntersectionPlaneLine(const Plane& aPlane, const Line3D& aLine, Vector3f& aOutIntersectionPoint)
		{
			//Om linjen �r parallell med planet, returneras false och aOutIntersectionPoint �ndras inte
			// Line if parallel with plane
			float linePlaneAngleDifference = aPlane.normal.Dot(aLine.direction);
			if (linePlaneAngleDifference == 0.0f)
			{
				return false;
			}

			// Line is inside plane
			if ((aPlane.point - aLine.point).Dot(aPlane.normal) == 0.0f)
			{
				return true;
			}

			//Om linjen inte �r parallell med planet skrivs sk�rningspunkten till aOutIntersectionPoint och true returneras
			aOutIntersectionPoint = ((aPlane.point - aLine.point).Dot(aPlane.normal) / linePlaneAngleDifference) * aLine.direction + aLine.point;
			return true;
		}

		bool IntersectionAABBLine(const AABB3D& aAABB, const Line3D& aLine)
		{
			// Using "slab-method", https://tavianator.com/fast-branchless-raybounding-box-intersections/
			// Also explained here, https://www.scratchapixel.com/lessons/3d-basic-rendering/minimal-ray-tracer-rendering-simple-shapes/ray-box-intersection
			/* Very effective method but copied from the internet, below is my own code.
			CU::Vector3f reverseNormal = 1.0f / aLine.direction;
			float tx1 = (aAABB.min.x - aLine.point.x) * reverseNormal.x;
			float tx2 = (aAABB.max.x - aLine.point.x) * reverseNormal.x;

			float tmin MIN(tx1, tx2);
			float tmax MAX(tx1, tx2);

			float ty1 = (aAABB.min.y - aLine.point.y) * reverseNormal.y;
			float ty2 = (aAABB.max.y - aLine.point.y) * reverseNormal.y;

			tmin = MAX(tmin, MIN(ty1, ty2));
			tmax = MIN(tmax, MAX(ty1, ty2));

			float tz1 = (aAABB.min.z - aLine.point.z) * reverseNormal.z;
			float tz2 = (aAABB.max.z - aLine.point.z) * reverseNormal.z;

			tmin = MAX(tmin, MIN(tz1, tz2));
			tmax = MIN(tmax, MAX(tz1, tz2));

			return tmax >= tmin;
			*/

			// --Fallback method-- Make 6 planes out of the cube, use IntersectionPlaneLine for all 6 planes.
			Plane p1(aAABB.min, Vector3f(-1.0f, 0.0f, 0.0f)); // Left side
			Plane p2(aAABB.min, Vector3f(0.0f, -1.0f, 0.0f)); // Bottom
			Plane p3(aAABB.min, Vector3f(0.0f, 0.0f, -1.0f)); // Front
			Plane p4(aAABB.max, Vector3f(1.0f, 0.0f, 0.0f)); // Right
			Plane p5(aAABB.max, Vector3f(0.0f, 1.0f, 0.0f)); // Top
			Plane p6(aAABB.max, Vector3f(0.0f, 0.0f, 1.0f)); // Back
			Vector3f intersectionPoint;

			if (!IntersectionPlaneLine(p1, aLine, intersectionPoint))
			{
				if (aLine.point.x < p1.point.x)
				{
					return false;
				}
			}
			if (!IntersectionPlaneLine(p2, aLine, intersectionPoint))
			{
				if (aLine.point.y < p2.point.y)
				{
					return false;
				}
			}
			if (!IntersectionPlaneLine(p3, aLine, intersectionPoint))
			{
				if (aLine.point.z < p3.point.z)
				{
					return false;
				}
			}
			if (!IntersectionPlaneLine(p4, aLine, intersectionPoint))
			{
				if (aLine.point.x > p4.point.x)
				{
					return false;
				}
			}
			if (!IntersectionPlaneLine(p5, aLine, intersectionPoint))
			{
				if (aLine.point.y > p5.point.y)
				{
					return false;
				}
			}
			if (!IntersectionPlaneLine(p6, aLine, intersectionPoint))
			{
				if (aLine.point.z > p6.point.z)
				{
					return false;
				}
			}

			return true;
		}

		int inline GetIntersection(float fDst1, float fDst2, CU::Vector3f P1, CU::Vector3f P2, CU::Vector3f &Hit) {
			if ((fDst1 * fDst2) >= 0.0f) return 0;
			if (fDst1 == fDst2) return 0;
			Hit = P1 + (P2 - P1) * (-fDst1 / (fDst2 - fDst1));
			return 1;
		}

		int inline InBox(CU::Vector3f Hit, CU::Vector3f B1, CU::Vector3f B2, const int Axis) {
			if (Axis == 1 && Hit.z > B1.z && Hit.z < B2.z && Hit.y > B1.y && Hit.y < B2.y) return 1;
			if (Axis == 2 && Hit.z > B1.z && Hit.z < B2.z && Hit.x > B1.x && Hit.x < B2.x) return 1;
			if (Axis == 3 && Hit.x > B1.x && Hit.x < B2.x && Hit.y > B1.y && Hit.y < B2.y) return 1;
			return 0;
		}

		bool IntersectionSphereLine(const Sphere& aSphere, const Line3D& aLine)
		{
			float pointOnLineClostestToSphereCenter = aLine.direction.Dot(aLine.point - aSphere.center);
			if ((pointOnLineClostestToSphereCenter * pointOnLineClostestToSphereCenter) -
				((aLine.point - aSphere.center).Length2()) +
				(aSphere.radius * aSphere.radius)
				< 0.0f)
			{
				return false;
			}
			return true;
		}
	}
}

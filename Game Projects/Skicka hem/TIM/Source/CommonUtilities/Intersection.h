#pragma once

#include "Vector.h"

namespace CommonUtilities
{
	typedef Vector3<float> Vector3f;

	namespace Intersection
	{
		struct Sphere
		{
			Sphere(const Vector3f& aCenter, float aRadius)
				: center(aCenter)
				, radius(aRadius)
			{}

			Vector3f center;
			float radius;
		};

		struct AABB3D
		{
			AABB3D(const Vector3f& aMin, const Vector3f& aMax)
				: min(aMin)
				, max(aMax)
			{}

			Vector3f min;
			Vector3f max;
		};

		struct Line3D
		{
			Line3D(const Vector3f& aPoint, const Vector3f& aDir)
				: point(aPoint)
				, direction(aDir)
			{
			}

			Vector3f point;
			Vector3f direction;
		};

		struct Plane
		{
			Plane(const Vector3f& aPoint, const Vector3f& aNormal)
				: point(aPoint)
				, normal(aNormal)
			{
			}

			Vector3f point;
			Vector3f normal;
		};

		bool PointInsideSphere(const Sphere& aSphere, const Vector3f aPoint);
		bool PointInsideAABB(const AABB3D& aAABB, const Vector3f& aPoint);
		bool IntersectionPlaneLine(const Plane& aPlane, const Line3D& aLine, Vector3f& aOutIntersectionPoint);
		bool IntersectionAABBLine(const AABB3D& aAABB, const Line3D& aLine);
		bool IntersectionSphereLine(const Sphere& aSphere, const Line3D& aLine);
	};
}


#pragma once
#include "Vector2.h"

namespace CommonUtilities
{
	template <class T>
	class Line
	{
	public:
		Line();
		Line(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1);

		void InitWith2Points(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1);
		void InitWithPointAndDirection(const Vector2<T>& aPoint, const Vector2<T>& aDirection);
		bool Inside(const Vector2<T>& aPosition) const;
		Vector2<T>& GetPoint() const;
		Vector2<T>& GetNormal() const;
	private:
		Vector2<T> myPoint;
		Vector2<T> myNormal; // Normalized
	};

	template<class T>
	inline Line<T>::Line()
	{
		myPoint = { static_cast<T>(0), static_cast<T>(0) };
		myNormal = { static_cast<T>(0), static_cast<T>(0) };
	}

	template<class T>
	inline Line<T>::Line(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1)
	{
		InitWith2Points(aPoint0, aPoint1);
	}

	template<class T>
	inline void Line<T>::InitWith2Points(const Vector2<T>& aPoint0, const Vector2<T>& aPoint1)
	{
		InitWithPointAndDirection(aPoint0, aPoint1 - aPoint0);
	}

	template<class T>
	inline void Line<T>::InitWithPointAndDirection(const Vector2<T>& aPoint, const Vector2<T>& aDirection)
	{
		if (aDirection.x != 0 ||aDirection.y != 0)
		{
			myPoint = aPoint;
			float length = aDirection.Length();
			myNormal = Vector2<T>({ -aDirection.y / length, aDirection.x / length });
		}
	}

	template<class T>
	inline bool Line<T>::Inside(const Vector2<T>& aPosition) const
	{
		return (aPosition - myPoint).Dot(myNormal) <= 0 ? true : false;
	}

	template<class T>
	inline Vector2<T>& Line<T>::GetPoint() const
	{
		return myPoint;
	}

	template<class T>
	inline Vector2<T>& Line<T>::GetNormal() const
	{
		return myNormal;
	}
}

#pragma once
#include "Vector2.h"
#include "Line.h"
#include <vector>

namespace CommonUtilities
{
	template <class T>
	class LineVolume
	{
	public:
		LineVolume(const std::vector<Line<T>>& aLineList);
		void AddLine(const Line<T>& aLine);
		bool Inside(const Vector2<T>& aPosition);
	private:
		std::vector<Line<T>> myLineList;
	};

	template<class T>
	inline LineVolume<T>::LineVolume(const std::vector<Line<T>>& aLineList)
	{
		myLineList = aLineList;
	}
	
	template<class T>
	inline void LineVolume<T>::AddLine(const Line<T>& aLine)
	{
		myLineList.push_back(aLine);
	}
	
	template<class T>
	inline bool LineVolume<T>::Inside(const Vector2<T>& aPosition)
	{
		// Only reliable for convex plygons
		for (auto line : myLineList)
		{
			if (!line.Inside(aPosition))
			{
				return false;
			}
		}
		return true;
	}
}
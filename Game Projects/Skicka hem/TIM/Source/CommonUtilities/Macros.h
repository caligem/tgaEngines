#pragma once

#define MIN(aNumber1, aNumber2) (((aNumber1) < (aNumber2)) ? (aNumber1) : (aNumber2))
#define MAX(aNumber1, aNumber2) (((aNumber1) > (aNumber2)) ? (aNumber1) : (aNumber2))

#define SAFE_DELETE(aPointer) delete aPointer; aPointer = nullptr;

#define CYCLIC_ERASE(aVector, aIndex) aVector[aIndex] = aVector.back(); aVector.pop_back();

#define SC_I(aVar) static_cast<int>(aVar)
#define SC_UI(aVar) static_cast<unsigned int>(aVar)
#define SC_F(aVar) static_cast<float>(aVar)
#define SC_L(aVar) static_cast<long>(aVar)
#define SC_D(aVar) static_cast<double>(aVar)
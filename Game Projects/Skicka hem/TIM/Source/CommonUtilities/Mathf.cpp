#include "Mathf.h"

namespace CommonUtilities
{
	int Clamp(int aNumber, int aMin, int aMax)
	{
		if( aNumber < aMin )
			return aMin;
		if( aNumber > aMax )
			return aMax;
		return aNumber;
	}
	float Clamp(float aNumber, float aMin, float aMax)
	{
		if( aNumber < aMin )
			return aMin;
		if( aNumber > aMax )
			return aMax;
		return aNumber;
	}
	float Lerp(float aStart, float aEnd, float aTime)
	{
		return aStart + (aEnd-aStart)*aTime;
	}
	CommonUtilities::Vector2f Lerp(CommonUtilities::Vector2f aStart, CommonUtilities::Vector2f aEnd, float aT)
	{
		return aStart*(1.0f - aT) + aEnd*aT;
	}
	CommonUtilities::Vector3f Lerp(CommonUtilities::Vector3f aStart, CommonUtilities::Vector3f aEnd, float aT)
	{
		return aStart*(1.0f - aT) + aEnd*aT;
	}
	CommonUtilities::Vector4f Lerp(CommonUtilities::Vector4f aStart, CommonUtilities::Vector4f aEnd, float aT)
	{
		return aStart*(1.0f - aT) + aEnd*aT;
	}
}
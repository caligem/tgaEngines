#pragma once

#include "Vector.h"

namespace CommonUtilities
{
	int Clamp(int aNumber, int aMin, int aMax);
	float Clamp(float aNumber, float aMin, float aMax);

	float Lerp(float aStart, float aEnd, float aTime);
	CommonUtilities::Vector2f Lerp(CommonUtilities::Vector2f aStart, CommonUtilities::Vector2f aEnd, float aT);
	CommonUtilities::Vector3f Lerp(CommonUtilities::Vector3f aStart, CommonUtilities::Vector3f aEnd, float aT);
	CommonUtilities::Vector4f Lerp(CommonUtilities::Vector4f aStart, CommonUtilities::Vector4f aEnd, float aT);
}

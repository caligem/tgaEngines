#pragma once
#include "Matrix33.h"
#include "Matrix44.h"

namespace CU = CommonUtilities;

namespace CommonUtilities
{
	typedef CU::Matrix33<float> Matrix33f;
	typedef CU::Matrix44<float> Matrix44f;
}

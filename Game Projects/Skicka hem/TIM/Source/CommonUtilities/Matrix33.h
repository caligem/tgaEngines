#pragma once
#include "Matrix44.h"
#include "Vector.h"
#include <cmath>

namespace CommonUtilities
{
	static const short M3_SIZE = 9;
	template <typename T>
	class Matrix33
	{
	public:
		Matrix33<T>();
		Matrix33<T>(int aValue);
		Matrix33<T>(T a0, T a1, T a2, T a3, T a4, T a5, T a6, T a7, T a8);
		Matrix33<T>(const Matrix33<T>& aMatrix);
		Matrix33<T>(const Matrix44<T>& aMatrix);

		Matrix33& operator=(const Matrix33<T>& aMatrix);
		inline const T& operator[](const int& aIndex) const;
		inline T& operator[](const int& aIndex);

		static Matrix33<T> CreateRotateAroundX(T aAngleInRadians);
		static Matrix33<T> CreateRotateAroundY(T aAngleInRadians);
		static Matrix33<T> CreateRotateAroundZ(T aAngleInRadians);
		static Matrix33<T> Transpose(const Matrix33<T>& aMatrixToTranspose);

		T myMatrix[M3_SIZE] = {
			static_cast<T>(0),static_cast<T>(0),static_cast<T>(0),
			static_cast<T>(0),static_cast<T>(0),static_cast<T>(0),
			static_cast<T>(0),static_cast<T>(0),static_cast<T>(0) };
	};

	template<typename T>
	inline Matrix33<T>::Matrix33()
	{
		myMatrix[0] = static_cast<T>(1);
		myMatrix[4] = static_cast<T>(1);
		myMatrix[8] = static_cast<T>(1);
	}

	template<typename T>
	inline Matrix33<T>::Matrix33(int aValue)
	{
		if (aValue == 0)
		{
			return;
		}
	}

	template<typename T>
	inline Matrix33<T>::Matrix33(T a0, T a1, T a2, T a3, T a4, T a5, T a6, T a7, T a8)
	{
		myMatrix[0] = a0;
		myMatrix[1] = a1;
		myMatrix[2] = a2;
		myMatrix[3] = a3;
		myMatrix[4] = a4;
		myMatrix[5] = a5;
		myMatrix[6] = a6;
		myMatrix[7] = a7;
		myMatrix[8] = a8;
	}
	
	template<typename T>
	inline Matrix33<T>::Matrix33(const Matrix33<T>& aMatrix)
	{
		for (int i = 0; i < M3_SIZE; ++i)
		{
			myMatrix[i] = aMatrix.myMatrix[i];
		}
	}

	template<typename T>
	inline Matrix33<T>::Matrix33(const Matrix44<T>& aMatrix)
	{
		myMatrix[0] = aMatrix.myMatrix[0];
		myMatrix[1] = aMatrix.myMatrix[1];
		myMatrix[2] = aMatrix.myMatrix[2];

		myMatrix[3] = aMatrix.myMatrix[4];
		myMatrix[4] = aMatrix.myMatrix[5];
		myMatrix[5] = aMatrix.myMatrix[6];

		myMatrix[6] = aMatrix.myMatrix[8];
		myMatrix[7] = aMatrix.myMatrix[9];
		myMatrix[8] = aMatrix.myMatrix[10];
	}

	template<typename T>
	inline Matrix33<T>& Matrix33<T>::operator=(const Matrix33<T>& aMatrix)
	{
		for (int i = 0; i < M3_SIZE; ++i)
		{
			myMatrix[i] = aMatrix.myMatrix[i];
		}
		return *this;
	}

	template<typename T>
	inline const T& Matrix33<T>::operator[](const int& aIndex) const
	{
		return myMatrix[aIndex];
	}

	template<typename T>
	inline T& Matrix33<T>::operator[](const int& aIndex)
	{
		return myMatrix[aIndex];
	}

	template<typename T>
	inline Matrix33<T> Matrix33<T>::CreateRotateAroundX(T aAngleInRadians)
	{
		Matrix33<T> m = Matrix33<T>();
		m.myMatrix[4] = static_cast<T>(cos(aAngleInRadians));
		m.myMatrix[5] = static_cast<T>(sin(aAngleInRadians));
		m.myMatrix[7] = static_cast<T>(-sin(aAngleInRadians));
		m.myMatrix[8] = static_cast<T>(cos(aAngleInRadians));
		return m;
	}

	template<typename T>
	inline Matrix33<T> Matrix33<T>::CreateRotateAroundY(T aAngleInRadians)
	{
		Matrix33<T> m = Matrix33();
		m.myMatrix[0] = static_cast<T>(cos(aAngleInRadians));
		m.myMatrix[2] = static_cast<T>(-sin(aAngleInRadians));
		m.myMatrix[6] = static_cast<T>(sin(aAngleInRadians));
		m.myMatrix[8] = static_cast<T>(cos(aAngleInRadians));
		return m;
	}

	template<typename T>
	inline Matrix33<T> Matrix33<T>::CreateRotateAroundZ(T aAngleInRadians)
	{
		Matrix33<T> m = Matrix33();
		m.myMatrix[0] = static_cast<T>(cos(aAngleInRadians));
		m.myMatrix[1] = static_cast<T>(sin(aAngleInRadians));
		m.myMatrix[3] = static_cast<T>(-sin(aAngleInRadians));
		m.myMatrix[4] = static_cast<T>(cos(aAngleInRadians));
		return m;
	}

	template<typename T>
	inline Matrix33<T> Matrix33<T>::Transpose(const Matrix33<T>& aMatrixToTranspose)
	{
		Matrix33<T> m = aMatrixToTranspose;
		m.myMatrix[1] = aMatrixToTranspose.myMatrix[3];
		m.myMatrix[2] = aMatrixToTranspose.myMatrix[6];
		m.myMatrix[3] = aMatrixToTranspose.myMatrix[1];
		m.myMatrix[5] = aMatrixToTranspose.myMatrix[7];
		m.myMatrix[6] = aMatrixToTranspose.myMatrix[2];
		m.myMatrix[7] = aMatrixToTranspose.myMatrix[5];
		return m;
	}

	template<typename T>
	inline Matrix33<T> Hadamard(const Matrix33<T>& aMatrix1, const Matrix33<T>& aMatrix2)
	{
		Matrix33<T> m;
		for (unsigned short i = 0; i < M3_SIZE; ++i)
		{
			m.myMatrix[i] = aMatrix1.myMatrix[i] * aMatrix2.myMatrix[i];
		}
		return m;
	}

	template<typename T>
	inline Matrix33<T> operator+(const Matrix33<T>& aMatrix0, const Matrix33<T>& aMatrix1)
	{
		Matrix33<T> m = Matrix33<T>();
		for (int i = 0; i < M3_SIZE; ++i)
		{
			m.myMatrix[i] = aMatrix0.myMatrix[i] + aMatrix1.myMatrix[i];
		}
		return m;
	}

	template<typename T>
	void operator+=(Matrix33<T>& aMatrix0, const Matrix33<T>& aMatrix1)
	{
		for (int i = 0; i < M3_SIZE; ++i)
		{
			aMatrix0.myMatrix[i] += aMatrix1.myMatrix[i];
		}
	}

	template<typename T>
	inline Matrix33<T> operator-(const Matrix33<T>& aMatrix0, const Matrix33<T>& aMatrix1)
	{
		Matrix33<T> m = Matrix33<T>();
		for (int i = 0; i < M3_SIZE; ++i)
		{
			m.myMatrix[i] = aMatrix0.myMatrix[i] - aMatrix1.myMatrix[i];
		}
		return m;
	}

	template<typename T>
	void operator-=(Matrix33<T>& aMatrix0, const Matrix33<T>& aMatrix1)
	{
		for (int i = 0; i < M3_SIZE; ++i)
		{
			aMatrix0.myMatrix[i] -= aMatrix1.myMatrix[i];
		}
	}

	template<typename T>
	inline Matrix33<T> operator*(const Matrix33<T>& aMatrix0, const Matrix33<T>& aMatrix1)
	{
		int dimension = 3;
		Matrix33<T> m(0);
		for (int row = 0; row < dimension; ++row)
		{
			for (int col = 0; col < dimension; ++col)
			{
				for (int inner = 0; inner < dimension; ++inner)
				{
					m.myMatrix[dimension*row + col] += aMatrix0.myMatrix[dimension*row+inner] * aMatrix1.myMatrix[dimension*inner + col];
				}
			}
		}
		return m;
	}

	template<typename T>
	void operator*=(Matrix33<T>& aMatrix0, const Matrix33<T>& aMatrix1)
	{
		aMatrix0 = aMatrix0 * aMatrix1;
	}

	template<typename T>
	inline Vector3<T> operator*(const Vector3<T>& aVector, const Matrix33<T>& aMatrix)
	{
		Vector3<T> v;
		v.x = aVector.x * aMatrix[0] + aVector.y * aMatrix[3] + aVector.z * aMatrix[6];
		v.y = aVector.x * aMatrix[1] + aVector.y * aMatrix[4] + aVector.z * aMatrix[7];
		v.z = aVector.x * aMatrix[2] + aVector.y * aMatrix[5] + aVector.z * aMatrix[8];
		return v;
	}

	template<typename T>
	inline Vector3<T> operator*=(Vector3<T>& aVector, const Matrix33<T>& aMatrix)
	{
		aVector = aVector * aMatrix;
		return aVector;
	}

	template<typename T>
	inline bool operator==(const Matrix33<T>& aMatrix0, const Matrix33<T>& aMatrix1)
	{
		for (int i = 0; i < M3_SIZE; ++i)
		{
			if (aMatrix0.myMatrix[i] != aMatrix1.myMatrix[i])
			{
				return false;
			}
		}
		return true;
	}
}
#pragma once
#include "Vector.h"
#include <cmath>

namespace CommonUtilities
{
	static const int M4_SIZE = 16;
	template <typename T>
	class Matrix44
	{
	public:
		Matrix44<T>();
		Matrix44(int aValue);
		Matrix44<T>(T a0, T a1, T a2, T a3, T a4, T a5, T a6, T a7, T a8, T a9, T a10, T a11, T a12, T a13, T a14, T a15);
		Matrix44<T>(const Matrix44<T>& aMatrix);

		Matrix44& operator=(const Matrix44<T>& aMatrix);
		inline const T& operator[](const int& aIndex) const;
		inline T& operator[](const int& aIndex);

		static Matrix44<T> CreateRotateAroundX(T aAngleInRadians);
		static Matrix44<T> CreateRotateAroundY(T aAngleInRadians);
		static Matrix44<T> CreateRotateAroundZ(T aAngleInRadians);
		static Matrix44<T> Transpose(const Matrix44<T>& aMatrixToTranspose);

		static Matrix44<T> GetFastInverse(const Matrix44<T>& aMatrix);
		static Matrix44<T> GetInverseFast(const Matrix44<T>& aMatrix);


		T myMatrix[M4_SIZE] = {
			static_cast<T>(0),static_cast<T>(0),static_cast<T>(0),static_cast<T>(0),
			static_cast<T>(0),static_cast<T>(0),static_cast<T>(0),static_cast<T>(0),
			static_cast<T>(0),static_cast<T>(0),static_cast<T>(0),static_cast<T>(0),
			static_cast<T>(0),static_cast<T>(0),static_cast<T>(0),static_cast<T>(0) };
	};

	template<typename T>
	inline Matrix44<T>::Matrix44()
	{
		myMatrix[0] = static_cast<T>(1);
		myMatrix[5] = static_cast<T>(1);
		myMatrix[10] = static_cast<T>(1);
		myMatrix[15] = static_cast<T>(1);
	}

	template<typename T>
	inline Matrix44<T>::Matrix44(int aValue)
	{
		if (aValue == 0)
		{
			return;
		}
	}

	template<typename T>
	inline Matrix44<T>::Matrix44(T a0, T a1, T a2, T a3, T a4, T a5, T a6, T a7, T a8, T a9, T a10, T a11, T a12, T a13, T a14, T a15)
	{
		myMatrix[0] = a0;
		myMatrix[1] = a1;
		myMatrix[2] = a2;
		myMatrix[3] = a3;
		myMatrix[4] = a4;
		myMatrix[5] = a5;
		myMatrix[6] = a6;
		myMatrix[7] = a7;
		myMatrix[8] = a8;
		myMatrix[9] = a9;
		myMatrix[10] = a10;
		myMatrix[11] = a11;
		myMatrix[12] = a12;
		myMatrix[13] = a13;
		myMatrix[14] = a14;
		myMatrix[15] = a15;
	}

	template<typename T>
	inline Matrix44<T>::Matrix44(const Matrix44<T>& aMatrix)
	{
		for (int i = 0; i < M4_SIZE; ++i)
		{
			myMatrix[i] = aMatrix.myMatrix[i];
		}
	}

	template<typename T>
	inline Matrix44<T>& Matrix44<T>::operator=(const Matrix44<T>& aMatrix)
	{
		for (int i = 0; i < M4_SIZE; ++i)
		{
			myMatrix[i] = aMatrix.myMatrix[i];
		}
		return *this;
	}

	template<typename T>
	inline const T& Matrix44<T>::operator[](const int& aIndex) const
	{
		return myMatrix[aIndex];
	}

	template<typename T>
	inline T& Matrix44<T>::operator[](const int& aIndex)
	{
		return myMatrix[aIndex];
	}

	template<typename T>
	inline Matrix44<T> Matrix44<T>::CreateRotateAroundX(T aAngleInRadians)
	{
		Matrix44<T> m = Matrix44();
		m.myMatrix[5] = static_cast<T>(cos(aAngleInRadians));
		m.myMatrix[6] = static_cast<T>(sin(aAngleInRadians));
		m.myMatrix[9] = static_cast<T>(-sin(aAngleInRadians));
		m.myMatrix[10] = static_cast<T>(cos(aAngleInRadians));
		return m;
	}

	template<typename T>
	inline Matrix44<T> Matrix44<T>::CreateRotateAroundY(T aAngleInRadians)
	{
		Matrix44<T> m = Matrix44();
		m.myMatrix[0] = static_cast<T>(cos(aAngleInRadians));
		m.myMatrix[2] = static_cast<T>(-sin(aAngleInRadians));
		m.myMatrix[8] = static_cast<T>(sin(aAngleInRadians));
		m.myMatrix[10] = static_cast<T>(cos(aAngleInRadians));
		return m;
	}

	template<typename T>
	inline Matrix44<T> Matrix44<T>::CreateRotateAroundZ(T aAngleInRadians)
	{
		Matrix44<T> m = Matrix44();
		m.myMatrix[0] = static_cast<T>(cos(aAngleInRadians));
		m.myMatrix[1] = static_cast<T>(sin(aAngleInRadians));
		m.myMatrix[4] = static_cast<T>(-sin(aAngleInRadians));
		m.myMatrix[5] = static_cast<T>(cos(aAngleInRadians));
		return m;
	}

	template<typename T>
	inline Matrix44<T> Matrix44<T>::Transpose(const Matrix44<T>& aMatrixToTranspose)
	{
		Matrix44<T> m = aMatrixToTranspose;
		m.myMatrix[1] = aMatrixToTranspose.myMatrix[4];
		m.myMatrix[2] = aMatrixToTranspose.myMatrix[8];
		m.myMatrix[3] = aMatrixToTranspose.myMatrix[12];
		m.myMatrix[4] = aMatrixToTranspose.myMatrix[1];
		m.myMatrix[6] = aMatrixToTranspose.myMatrix[9];
		m.myMatrix[7] = aMatrixToTranspose.myMatrix[13];
		m.myMatrix[8] = aMatrixToTranspose.myMatrix[2];
		m.myMatrix[9] = aMatrixToTranspose.myMatrix[6];
		m.myMatrix[11] = aMatrixToTranspose.myMatrix[14];
		m.myMatrix[12] = aMatrixToTranspose.myMatrix[3];
		m.myMatrix[13] = aMatrixToTranspose.myMatrix[7];
		m.myMatrix[14] = aMatrixToTranspose.myMatrix[11];

		return m;
	}

	template<typename T>
	inline Matrix44<T> Matrix44<T>::GetFastInverse(const Matrix44<T>& aMatrix)
	{
		return GetInverseFast(aMatrix);
	}

	template<typename T>
	inline Matrix44<T> Matrix44<T>::GetInverseFast(const Matrix44<T>& aMatrix)
	{
		Matrix44<T> m = Matrix44::Transpose(aMatrix);
		m[3] = 0;
		m[7] = 0;
		m[11] = 0;
		m[15] = 1;

		m[12] = -aMatrix[12] * m[0] + -aMatrix[13] * m[4] + -aMatrix[14] * m[8];
		m[13] = -aMatrix[12] * m[1] + -aMatrix[13] * m[5] + -aMatrix[14] * m[9];
		m[14] = -aMatrix[12] * m[2] + -aMatrix[13] * m[6] + -aMatrix[14] * m[10];
		return m;
	}

	template<typename T>
	inline Matrix44<T> Hadamard(const Matrix44<T>& aMatrix1, const Matrix44<T>& aMatrix2)
	{
		Matrix44<T> m;
		for (unsigned short i = 0; i < M4_SIZE; ++i)
		{
			m.myMatrix[i] = aMatrix1.myMatrix[i] * aMatrix2.myMatrix[i];
		}
		return m;
	}

	template<typename T>
	inline Matrix44<T> operator+(const Matrix44<T>& aMatrix0, const Matrix44<T>& aMatrix1)
	{
		Matrix44<T> m = Matrix44<T>();
		for (int i = 0; i < M4_SIZE; ++i)
		{
			m.myMatrix[i] = aMatrix0.myMatrix[i] + aMatrix1.myMatrix[i];
		}
		return m;
	}

	template<typename T>
	inline Matrix44<T>& operator+=(Matrix44<T>& aMatrix0, const Matrix44<T>& aMatrix1)
	{
		for (int i = 0; i < M4_SIZE; ++i)
		{
			aMatrix0.myMatrix[i] += aMatrix1.myMatrix[i];
		}
		return aMatrix0;
	}

	template<typename T>
	inline Matrix44<T> operator-(const Matrix44<T>& aMatrix0, const Matrix44<T>& aMatrix1)
	{
		Matrix44<T> m = Matrix44<T>();
		for (int i = 0; i < M4_SIZE; ++i)
		{
			m.myMatrix[i] = aMatrix0.myMatrix[i] - aMatrix1.myMatrix[i];
		}
		return m;
	}

	template<typename T>
	inline Matrix44<T>& operator-=(Matrix44<T>& aMatrix0, const Matrix44<T>& aMatrix1)
	{
		for (int i = 0; i < M4_SIZE; ++i)
		{
			aMatrix0.myMatrix[i] -= aMatrix1.myMatrix[i];
		}
		return aMatrix0;
	}

	template<typename T>
	inline Matrix44<T> operator*(const Matrix44<T>& aMatrix0, const Matrix44<T>& aMatrix1)
	{
		int dimension = 4;
		Matrix44<T> m(0);
		for (int row = 0; row < dimension; ++row)
		{
			for (int col = 0; col < dimension; ++col)
			{
				for (int inner = 0; inner < dimension; ++inner)
				{
					m.myMatrix[dimension*row + col] += aMatrix0.myMatrix[dimension*row + inner] * aMatrix1.myMatrix[dimension*inner + col];
				}
			}
		}
		return m;
	}

	template<typename T>
	void operator*=(Matrix44<T>& aMatrix0, const Matrix44<T>& aMatrix1)
	{
		aMatrix0 = aMatrix0 * aMatrix1;
	}

	template<typename T>
	inline Vector4<T> operator*(const Vector4<T>& aVector, const Matrix44<T>& aMatrix)
	{
		Vector4<T> v;
		v.x = aVector.x * aMatrix[0] + aVector.y * aMatrix[4] + aVector.z * aMatrix[8] + aVector.w * aMatrix[12];
		v.y = aVector.x * aMatrix[1] + aVector.y * aMatrix[5] + aVector.z * aMatrix[9] + aVector.w * aMatrix[13];
		v.z = aVector.x * aMatrix[2] + aVector.y * aMatrix[6] + aVector.z * aMatrix[10] + aVector.w * aMatrix[14];
		v.w = aVector.x * aMatrix[3] + aVector.y * aMatrix[7] + aVector.z * aMatrix[11] + aVector.w * aMatrix[15];
		return v;
	}

	template<typename T>
	inline Vector4<T> operator*=(Vector4<T>& aVector, const Matrix44<T>& aMatrix)
	{
		aVector = aVector * aMatrix;
		return aVector;
	}

	template<typename T>
	inline bool operator==(const Matrix44<T>& aMatrix0, const Matrix44<T>& aMatrix1)
	{
		for (int i = 0; i < M4_SIZE; ++i)
		{
			if (aMatrix0.myMatrix[i] != aMatrix1.myMatrix[i])
			{
				return false;
			}
		}
		return true;
	}
}
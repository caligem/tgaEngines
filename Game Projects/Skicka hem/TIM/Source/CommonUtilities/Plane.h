#pragma once
#include "Vector3.h"

namespace CommonUtilities
{
	template <class T>
	class Plane
	{
	public:
		Plane();
		Plane(const Vector3<T>& aPoint0, const Vector3<T>& aPoint1, const Vector3<T>& aPoint2);
		Plane(const Vector3<T>& aPoint0, const Vector3<T>& aNormal);

		void InitWith3Points(const Vector3<T>& aPoint0, const Vector3<T>& aPoint1, const Vector3<T>& aPoint2);
		void InitWithPointAndNormal(const Vector3<T>& aPoint, const Vector3<T>& aNormal);
		bool Inside(const Vector3<T>& aPosition) const;
	private:
		Vector3<T> myPoint;
		Vector3<T> myNormal;
	};
	
	template<class T>
	inline Plane<T>::Plane()
	{
		myPoint = { static_cast<T>(0), static_cast<T>(0) };
		myNormal = { static_cast<T>(0), static_cast<T>(0) };
	}
	
	template<class T>
	inline Plane<T>::Plane(const Vector3<T>& aPoint0, const Vector3<T>& aPoint1, const Vector3<T>& aPoint2)
	{
		InitWith3Points(aPoint0, aPoint1, aPoint2);
	}
	
	template<class T>
	inline Plane<T>::Plane(const Vector3<T>& aPoint0, const Vector3<T>& aNormal)
	{
		InitWithPointAndNormal(aPoint0, aNormal);
	}
	
	template<class T>
	inline void Plane<T>::InitWith3Points(const Vector3<T>& aPoint0, const Vector3<T>& aPoint1, const Vector3<T>& aPoint2)
	{
		Vector3<T> normal = (aPoint1 - aPoint0).Cross(aPoint2 - aPoint0);
		InitWithPointAndNormal(aPoint0, normal);
	}
	
	template<class T>
	inline void Plane<T>::InitWithPointAndNormal(const Vector3<T>& aPoint, const Vector3<T>& aNormal)
	{
		myPoint = aPoint;
		myNormal = aNormal;
	}
	
	template<class T>
	inline bool Plane<T>::Inside(const Vector3<T>& aPosition) const
	{
		return (aPosition - myPoint).Dot(myNormal) <= 0 ? true : false;
	}
}

namespace CU = CommonUtilities;

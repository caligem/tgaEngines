#pragma once
#include <iomanip>
#include <random>

namespace CommonUtilities
{
	template <typename T>
	class Random
	{
	public:
		Random<T>();
		void SeedRNG(std::random_device& aRD);
		T GetNumber(T aMin, T aMax, T aScale = static_cast<T>(1));
	private:
		std::random_device myRD;
		std::mt19937 myRNG;
	};

	template<typename T>
	inline Random<T>::Random()
	{
		SeedRNG(myRD);
	}

	template<typename T>
	inline void Random<T>::SeedRNG(std::random_device& aRD)
	{
		myRNG.seed(aRD);
	}

	template<typename T>
	inline T Random<T>::GetNumber(T aMin, T aMax, T aScale)
	{
		std::uniform_real_distribution<T> dist(aMin, aMax + aScale);

		T value = static_cast<int>(dist(myRNG) / aScale) * aScale;

		return value;
	}
}

#pragma once
#include <assert.h>

namespace CommonUtilities
{
	template <typename T, int size>
	class StaticArray
	{
	public:
		StaticArray() = default;
		StaticArray(const StaticArray& aStaticArray);

		~StaticArray() = default;

		StaticArray& operator=(const StaticArray& aStaticArray);

		inline const T& operator[](const int& aIndex) const;
		inline T& operator[](const int& aIndex);

		// Utility functions
		inline void Insert(int aIndex, T& aObject);
		inline void DeleteAll();
	private:
		T myElements[size];
	};

	template<typename T, int size>
	inline StaticArray<T, size>::StaticArray(const StaticArray& aStaticArray)
	{
		*this = aStaticArray;
	}

	template<typename T, int size>
	inline StaticArray<T, size>& StaticArray<T, size>::operator=(const StaticArray& aStaticArray)
	{
		for (int i = 0; i < size; ++i)
		{
			myElements[i] = aStaticArray.myElements[i];
		}
		return *this;
	}

	template<typename T, int size>
	inline const T& StaticArray<T, size>::operator[](const int& aIndex) const
	{
		assert((aIndex >= 0 && aIndex < size) && "Index outside array");
		return myElements[aIndex];
	}

	template<typename T, int size>
	inline T& StaticArray<T, size>::operator[](const int& aIndex)
	{
		assert((aIndex >= 0 && aIndex < size) && "Index outside array");
		return myElements[aIndex];
	}
	
	template<typename T, int size>
	inline void StaticArray<T, size>::Insert(int aIndex, T& aObject)
	{
		assert((aIndex >= 0 && aIndex < size) && "Unable to insert element outside array");
		for (int i = size - 2; i >= aIndex; --i)
		{
			myElements[i + 1] = myElements[i];
		}
		myElements[aIndex] = aObject;
	}
	
	template<typename T, int size>
	inline void StaticArray<T, size>::DeleteAll()
	{
		for (int i = 0; i < size; ++i)
		{
			delete myElements[i];
			myElements[i] = nullptr;
		}
	}
}

namespace CU = CommonUtilities;

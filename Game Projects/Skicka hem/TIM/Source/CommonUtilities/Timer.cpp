#include "Timer.h"

namespace CommonUtilities
{
	Timer::Timer() :
		myClockStartTimePoint(std::chrono::high_resolution_clock::now()),
		myLastFrameTimePoint(myClockStartTimePoint),
		myCurrentFrameTimePoint(myClockStartTimePoint),
		myTotalTime(0.0),
		myDeltaTime(0.0f)
	{

	}

	Timer::~Timer()
	{
	}

	void Timer::Update()
	{
		myLastFrameTimePoint = myCurrentFrameTimePoint;
		myCurrentFrameTimePoint = std::chrono::high_resolution_clock::now();

		myDeltaTime = std::chrono::duration_cast<std::chrono::duration<float>>(myCurrentFrameTimePoint - myLastFrameTimePoint).count();
		myTotalTime = std::chrono::duration_cast<std::chrono::duration<double>>(myClockStartTimePoint - std::chrono::high_resolution_clock::now()).count();
	}

	float Timer::GetDeltaTime() const
	{
		return myDeltaTime;
	}

	double Timer::GetTotalTime() const
	{
		return myTotalTime;
	}
}

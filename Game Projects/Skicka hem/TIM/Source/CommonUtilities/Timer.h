#pragma once
#include <chrono>

namespace CommonUtilities
{
	class Timer
	{
	public:
		Timer();
		~Timer();
		Timer(const Timer& aTimer) = delete;
		Timer& operator=(const Timer& aTimer) = delete;

		void Update();

		double GetTotalTime() const;
		float GetDeltaTime() const;
	private:
		const std::chrono::high_resolution_clock::time_point myClockStartTimePoint;
		std::chrono::high_resolution_clock::time_point myLastFrameTimePoint;
		std::chrono::high_resolution_clock::time_point myCurrentFrameTimePoint;
		double myTotalTime;
		float myDeltaTime;
	};
}

namespace CU = CommonUtilities;

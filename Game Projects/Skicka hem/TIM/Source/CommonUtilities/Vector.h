#pragma once
#include "Vector2.h"
#include "Vector3.h"
#include "Vector4.h"

namespace CU = CommonUtilities;

namespace CommonUtilities
{
	typedef CU::Vector2<float> Vector2f;
	typedef CU::Vector3<float> Vector3f;
	typedef CU::Vector4<float> Vector4f;
}

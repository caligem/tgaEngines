#pragma once
#include <math.h>
#include <assert.h>

namespace CommonUtilities
{
	template <class T>
	class Vector2
	{
	public:
		T x;
		T y;

		//Creates a null-vector
		Vector2<T>();

		//Creates a vector (aX, aY, aZ)
		Vector2<T>(const T& aX, const T& aY);

		//Copy constructor (compiler generated)
		Vector2<T>(const Vector2<T>& aVector) = default;

		//Assignment operator (compiler generated)
		Vector2<T>& operator=(const Vector2<T>& aVector2) = default;

		//Destructor (compiler generated)
		~Vector2<T>() = default;

		//Returns the squared length of the vector
		T Length2() const;

		//Returns the length of the vector
		T Length() const;

		//Returns a normalized copy of this
		Vector2<T> GetNormalized() const;

		//Normalizes the vector
		void Normalize();

		//Returns the dot product of this and aVector
		T Dot(const Vector2<T>& aVector) const;
	};

	//Returns the vector sum of aVector0 and aVector1
	template <class T> Vector2<T> operator+(const Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		return Vector2<T>{ aVector0.x + aVector1.x, aVector0.y + aVector1.y };
	}

	//Returns the vector difference of aVector0 and aVector1
	template <class T> Vector2<T> operator-(const Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		return Vector2<T>{ aVector0.x - aVector1.x, aVector0.y - aVector1.y };
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector2<T> operator*(const Vector2<T>& aVector, const T& aScalar)
	{
		return Vector2<T>{ aVector.x * aScalar, aVector.y * aScalar };
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector2<T> operator*(const T& aScalar, const Vector2<T>& aVector)
	{
		return Vector2<T>{ aVector.x * aScalar, aVector.y * aScalar };
	}

	//Returns the vector aVector divided by the scalar aScalar (equivalent to aVector multiplied by 1/aScalar)
	template <class T> Vector2<T> operator/(const Vector2<T>& aVector, const T& aScalar)
	{
		return Vector2<T>{ aVector.x / aScalar, aVector.y / aScalar };
	}

	//Returns the scalar aScalar divided by the vector aVector
	template <class T> Vector2<T> operator/(const T& aScalar, const Vector2<T>& aVector)
	{
		return Vector2<T>{ aScalar / aVector.x, aScalar / aVector.y };
	}

	//Equivalent to setting aVector0 to (aVector0 + aVector1)
	template <class T> void operator+=(Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		aVector0 = Vector2<T>( aVector0.x + aVector1.x, aVector0.y + aVector1.y );
	}

	//Equivalent to setting aVector0 to (aVector0 - aVector1)
	template <class T> void operator-=(Vector2<T>& aVector0, const Vector2<T>& aVector1)
	{
		aVector0 = Vector2<T>( aVector0.x - aVector1.x, aVector0.y - aVector1.y );
	}

	//Equivalent to setting aVector to (aVector * aScalar)
	template <class T> void operator*=(Vector2<T>& aVector, const T& aScalar)
	{
		aVector = Vector2<T>( aVector.x * aScalar, aVector.y * aScalar );
	}

	//Equivalent to setting aVector to (aVector / aScalar)
	template <class T> void operator/=(Vector2<T>& aVector, const T& aScalar)
	{
		aVector = Vector2<T>( aVector.x / aScalar, aVector.y / aScalar );
	}

	template<class T>
	inline Vector2<T>::Vector2()
	{
		x = static_cast<T>(0);
		y = static_cast<T>(0);
	}

	template<class T>
	inline Vector2<T>::Vector2(const T & aX, const T & aY)
	{
		x = aX;
		y = aY;
	}

	template<class T>
	inline T Vector2<T>::Length2() const
	{
		return (x*x + y*y);
	}

	template<class T>
	inline T Vector2<T>::Length() const
	{
		return static_cast<T>(sqrt(x*x + y*y));
	}

	template<class T>
	inline Vector2<T> Vector2<T>::GetNormalized() const
	{
		if (x == 0 && y == 0)
		{
			return Vector2(x, y);
		}
		T length = Length();
		return Vector2(x / length, y / length);
	}

	template<class T>
	inline void Vector2<T>::Normalize()
	{
		if (x == 0 && y == 0)
		{
			return;
		}
		T length = Length();
		x = x / length;
		y = y / length;
	}

	template<class T>
	inline T Vector2<T>::Dot(const Vector2<T>& aVector) const
	{
		return x * aVector.x + y * aVector.y;
	}

	template<class T>
	inline static Vector2<T> Hadamard(const Vector2<T>& aVector1, const Vector2<T>& aVector2)
	{
		return Vector2<T>(aVector1.x * aVector2.x, aVector1.y * aVector2.y);
	}
}

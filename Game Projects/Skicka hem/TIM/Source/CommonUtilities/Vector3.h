#pragma once
#include <math.h>

namespace CommonUtilities
{
	template <class T>
	class Vector3
	{
	public:
		T x;
		T y;
		T z;

		//Creates a null-vector
		Vector3<T>();

		//Creates a vector (aX, aY, aZ)
		Vector3<T>(const T& aX, const T& aY, const T& aZ);

		//Copy constructor (compiler generated)
		Vector3<T>(const Vector3<T>& aVector) = default;

		//Assignment operator (compiler generated)
		Vector3<T>& operator=(const Vector3<T>& aVector3) = default;

		//Destructor (compiler generated)
		~Vector3<T>() = default;

		//Returns the squared length of the vector
		T Length2() const;

		//Returns the length of the vector
		T Length() const;

		//Returns a normalized copy of this
		Vector3<T> GetNormalized() const;

		//Normalizes the vector
		void Normalize();

		//Returns the dot product of this and aVector
		T Dot(const Vector3<T>& aVector) const;

		//Returns the cross product of this and aVector
		Vector3<T> Cross(const Vector3<T>& aVector) const;
	};

	//Returns the vector sum of aVector0 and aVector1
	template <class T> Vector3<T> operator+(const Vector3<T>& aVector0, const Vector3<T>& aVector1)
	{
		return Vector3<T>{ aVector0.x + aVector1.x, aVector0.y + aVector1.y, aVector0.z + aVector1.z };
	}

	//Returns the vector difference of aVector0 and aVector1
	template <class T> Vector3<T> operator-(const Vector3<T>& aVector0, const Vector3<T>& aVector1)
	{
		return Vector3<T>{ aVector0.x - aVector1.x, aVector0.y - aVector1.y, aVector0.z - aVector1.z };
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector3<T> operator*(const Vector3<T>& aVector, const T& aScalar)
	{
		return Vector3<T>{ aVector.x * aScalar, aVector.y * aScalar, aVector.z * aScalar };
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector3<T> operator*(const T& aScalar, const Vector3<T>& aVector)
	{
		return Vector3<T>{ aVector.x * aScalar, aVector.y * aScalar, aVector.z * aScalar };
	}

	//Returns the vector aVector divided by the scalar aScalar (equivalent to aVector multiplied by 1/aScalar)
	template <class T> Vector3<T> operator/(const Vector3<T>& aVector, const T& aScalar)
	{
		return Vector3<T>{ aVector.x / aScalar, aVector.y / aScalar, aVector.z / aScalar };
	}

	//Returns the scalar aScalar divided by the vector aVector
	template <class T> Vector3<T> operator/(const T& aScalar, const Vector3<T>& aVector)
	{
		return Vector3<T>{ aScalar / aVector.x, aScalar / aVector.y, aScalar / aVector.z };
	}

	//Equivalent to setting aVector0 to (aVector0 + aVector1)
	template <class T> void operator+=(Vector3<T>& aVector0, const Vector3<T>& aVector1)
	{
		aVector0 = Vector3<T>( aVector0.x + aVector1.x, aVector0.y + aVector1.y, aVector0.z + aVector1.z );
	}

	//Equivalent to setting aVector0 to (aVector0 - aVector1)
	template <class T> void operator-=(Vector3<T>& aVector0, const Vector3<T>& aVector1)
	{
		aVector0 = Vector3<T>( aVector0.x - aVector1.x, aVector0.y - aVector1.y, aVector0.z - aVector1.z );
	}

	//Equivalent to setting aVector to (aVector * aScalar)
	template <class T> void operator*=(Vector3<T>& aVector, const T& aScalar)
	{
		aVector = Vector3<T>( aVector.x * aScalar, aVector.y * aScalar, aVector.z * aScalar );
	}

	//Equivalent to setting aVector to (aVector / aScalar)
	template <class T> void operator/=(Vector3<T>& aVector, const T& aScalar)
	{
		aVector = Vector3<T>( aVector.x / aScalar, aVector.y / aScalar, aVector.z / aScalar );
	}

	template<class T>
	inline Vector3<T>::Vector3()
	{
		x = static_cast<T>(0);
		y = static_cast<T>(0);
		z = static_cast<T>(0);
	}

	template<class T>
	inline Vector3<T>::Vector3(const T & aX, const T & aY, const T & aZ)
	{
		x = aX;
		y = aY;
		z = aZ;
	}

	template<class T>
	inline T Vector3<T>::Length2() const
	{
		return (x*x + y*y + z*z);
	}

	template<class T>
	inline T Vector3<T>::Length() const
	{
		return sqrt(x*x + y*y + z*z);
	}

	template<class T>
	inline Vector3<T> Vector3<T>::GetNormalized() const
	{
		if (x == 0 && y == 0 && z == 0)
		{
			return Vector3(x, y, z);
		}
		T length = Length();
		return Vector3(x / length, y / length, z / length);
	}

	template<class T>
	inline void Vector3<T>::Normalize()
	{
		if (x == 0 && y == 0 && z == 0)
		{
			return;
		}
		T length = Length();
		x = x / length;
		y = y / length;
		z = z / length;
	}

	template<class T>
	inline T Vector3<T>::Dot(const Vector3<T>& aVector) const
	{
		return x * aVector.x + y * aVector.y + z * aVector.z;
	}

	template<class T>
	inline Vector3<T> Vector3<T>::Cross(const Vector3<T>& aVector) const
	{
		return { y * aVector.z - z * aVector.y, z * aVector.x - x * aVector.z, x * aVector.y - y * aVector.x };
	}

	template<class T>
	inline Vector3<T> Hadamard(const Vector3<T>& aVector1, const Vector3<T>& aVector2)
	{
		return Vector3f{ aVector1.x * aVector2.x, aVector1.y * aVector2.y, aVector1.z * aVector2.z };
	}
}

#pragma once
#include <math.h>

namespace CommonUtilities
{
	template <class T>
	class Vector4
	{
	public:
		T x;
		T y;
		T z;
		T w;

		//Creates a null-vector
		Vector4<T>();

		//Creates a vector (aX, aY, aZ)
		Vector4<T>(const T& aX, const T& aY, const T& aZ, const T& aW);

		//Copy constructor (compiler generated)
		Vector4<T>(const Vector4<T>& aVector) = default;

		//Assignment operator (compiler generated)
		Vector4<T>& operator=(const Vector4<T>& aVector4) = default;

		//Destructor (compiler generated)
		~Vector4<T>() = default;

		//Returns the squared length of the vector
		T Length2() const;

		//Returns the length of the vector
		T Length() const;

		//Returns a normalized copy of this
		Vector4<T> GetNormalized() const;

		//Normalizes the vector
		void Normalize();

		//Returns the dot product of this and aVector
		T Dot(const Vector4<T>& aVector) const;

		//Returns the cross product of this and aVector
		Vector4<T> Cross(const Vector4<T>& aVector) const;
	};
	
	//Returns the vector sum of aVector0 and aVector1
	template <class T> Vector4<T> operator+(const Vector4<T>& aVector0, const Vector4<T>& aVector1)
	{
		return Vector4<T>{ aVector0.x + aVector1.x, aVector0.y + aVector1.y, aVector0.z + aVector1.z, aVector0.w + aVector1.w };
	}

	//Returns the vector difference of aVector0 and aVector1
	template <class T> Vector4<T> operator-(const Vector4<T>& aVector0, const Vector4<T>& aVector1)
	{
		return Vector4<T>{ aVector0.x - aVector1.x, aVector0.y - aVector1.y, aVector0.z - aVector1.z, aVector0.w - aVector1.w };
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector4<T> operator*(const Vector4<T>& aVector, const T& aScalar)
	{
		return Vector4<T>{ aVector.x * aScalar, aVector.y * aScalar, aVector.z * aScalar, aVector.w * aScalar };
	}

	//Returns the vector aVector multiplied by the scalar aScalar
	template <class T> Vector4<T> operator*(const T& aScalar, const Vector4<T>& aVector)
	{
		return Vector4<T>{ aVector.x * aScalar, aVector.y * aScalar, aVector.z * aScalar, aVector.w * aScalar };
	}

	//Returns the vector aVector divided by the scalar aScalar (equivalent to aVector multiplied by 1/aScalar)
	template <class T> Vector4<T> operator/(const Vector4<T>& aVector, const T& aScalar)
	{
		return Vector4<T>{ aVector.x / aScalar, aVector.y / aScalar, aVector.z / aScalar, aVector.w / aScalar };
	}
	
	//Returns the scalar aScalar divided by the vector aVector
	template <class T> Vector4<T> operator/(const T& aScalar, const Vector4<T>& aVector)
	{
		return Vector4<T>{ aScalar / aVector.x, aScalar / aVector.y, aScalar / aVector.z, aScalar / aVector.w };
	}

	//Equivalent to setting aVector0 to (aVector0 + aVector1)
	template <class T> void operator+=(Vector4<T>& aVector0, const Vector4<T>& aVector1)
	{
		aVector0 = Vector4<T>( aVector0.x + aVector1.x, aVector0.y + aVector1.y, aVector0.z + aVector1.z, aVector0.w + aVector1.w );
	}

	//Equivalent to setting aVector0 to (aVector0 - aVector1)
	template <class T> void operator-=(Vector4<T>& aVector0, const Vector4<T>& aVector1)
	{
		aVector0 = Vector4<T>( aVector0.x - aVector1.x, aVector0.y - aVector1.y, aVector0.z - aVector1.z, aVector0.w - aVector1.w );
	}

	//Equivalent to setting aVector to (aVector * aScalar)
	template <class T> void operator*=(Vector4<T>& aVector, const T& aScalar)
	{
		aVector = Vector4<T>( aVector.x * aScalar, aVector.y * aScalar, aVector.z * aScalar, aVector.w * aScalar );
	}

	//Equivalent to setting aVector to (aVector / aScalar)
	template <class T> void operator/=(Vector4<T>& aVector, const T& aScalar)
	{
		aVector = Vector4<T>( aVector.x / aScalar, aVector.y / aScalar, aVector.z / aScalar, aVector.w / aScalar );
	}

	template<class T>
	inline Vector4<T>::Vector4()
	{
		x = static_cast<T>(0);
		y = static_cast<T>(0);
		z = static_cast<T>(0);
		w = static_cast<T>(0);
	}

	template<class T>
	inline Vector4<T>::Vector4(const T & aX, const T & aY, const T & aZ, const T & aW)
	{
		x = aX;
		y = aY;
		z = aZ;
		w = aW;
	}

	template<class T>
	inline T Vector4<T>::Length2() const
	{
		return (x*x + y*y + z*z + w*w);
	}

	template<class T>
	inline T Vector4<T>::Length() const
	{
		return sqrt(x*x + y*y + z*z + w*w);
	}

	template<class T>
	inline Vector4<T> Vector4<T>::GetNormalized() const
	{
		if (x == 0 && y == 0 && z == 0 && w == 0)
		{
			return Vector4(x, y, z, w);
		}
		T length = Length();
		return Vector4(x / length, y / length, z / length, w / length);
	}

	template<class T>
	inline void Vector4<T>::Normalize()
	{
		if (x == 0 && y == 0 && z == 0 && w == 0)
		{
			return;
		}
		T length = Length();
		x = x / length;
		y = y / length;
		z = z / length;
		w = w / length;
	}

	template<class T>
	inline T Vector4<T>::Dot(const Vector4<T>& aVector) const
	{
		return x * aVector.x + y * aVector.y + z * aVector.z + w * aVector.w;
	}

	template<class T>
	inline Vector4<T> Vector4<T>::Cross(const Vector4<T>& aVector) const
	{
		return{ y * aVector.z - z * aVector.y, z * aVector.x - x * aVector.z, x * aVector.y - y * aVector.x, 1 };
	}

	template<class T>
	inline Vector4<T> Hadamard(const Vector4<T>& aVector1, const Vector4<T>& aVector2)
	{
		return Vector4f{ aVector1.x * aVector2.x, aVector1.y * aVector2.y, aVector1.z * aVector2.z, aVector1.w * aVector2.w };
	}
}

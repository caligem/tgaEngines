#pragma once
#include <assert.h>
#include "StaticArray.h"

namespace CommonUtilities
{
	template <typename T, int S, typename CountType = unsigned short, bool UseSafeModeFlag = true>
	class VectorOnStack
	{
	public:
		VectorOnStack();
		VectorOnStack(const VectorOnStack& aVectorOnStack);

		~VectorOnStack();

		VectorOnStack& operator=(const VectorOnStack& aVectorOnStack);

		inline const T& operator[](const CountType& aIndex) const;
		inline T& operator[](const CountType& aIndex);

		inline void Add(const T& aObject);
		inline void Insert(CountType aIndex, T& aObject);
		inline void DeleteCyclic(T& aObject);
		inline void DeleteCyclicAtIndex(CountType aIndex);
		inline void RemoveCyclic(const T& aObject);
		inline void RemoveCyclicAtIndex(CountType aIndex);
		inline T GetLast() const;

		inline void Clear();
		inline void DeleteAll();

		inline CountType Size() const;
	private:
		CountType myCount;
		StaticArray<T, S> myElements;
	};

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline VectorOnStack<T, S, CountType, UseSafeModeFlag>::VectorOnStack()
	{
		myCount = static_cast<CountType>(0);
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline VectorOnStack<T, S, CountType, UseSafeModeFlag>::VectorOnStack(const VectorOnStack& aVectorOnStack)
	{
		*this = aVectorOnStack;
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline VectorOnStack<T, S, CountType, UseSafeModeFlag>::~VectorOnStack()
	{
		myCount = static_cast<CountType>(0);
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline VectorOnStack<T, S, CountType, UseSafeModeFlag> & VectorOnStack<T, S, CountType, UseSafeModeFlag>::operator=(const VectorOnStack& aVectorOnStack)
	{
		bool safeFlag = UseSafeModeFlag;
		if (safeFlag)
		{
			myElements = aVectorOnStack.myElements;
		}
		else
		{
			std::memcpy(&this->myElements, &aVectorOnStack.myElements, sizeof(T)*S);
		}
		myCount = aVectorOnStack.Size();
		return *this;
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline const T& VectorOnStack<T, S, CountType, UseSafeModeFlag>::operator[](const CountType& aIndex) const
	{
		assert((aIndex >= 0 && aIndex < myCount) && "Index outside vector");
		return myElements[aIndex];
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline T& VectorOnStack<T, S, CountType, UseSafeModeFlag>::operator[](const CountType& aIndex)
	{
		assert((aIndex >= 0 && aIndex < myCount) && "Index outside vector");
		return myElements[aIndex];
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, S, CountType, UseSafeModeFlag>::Add(const T& aObject)
	{
		assert(myCount < S && "Vector is full");
		myElements[myCount] = aObject;
		++myCount;
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, S, CountType, UseSafeModeFlag>::Insert(CountType aIndex, T& aObject)
	{
		assert((aIndex >= 0 && aIndex < S) && "Index outside vector");
		for (CountType i = myCount; i > aIndex; --i)
		{
			myElements[i] = myElements[i - 1];
		}
		myElements[aIndex] = aObject;
		++myCount;
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, S, CountType, UseSafeModeFlag>::DeleteCyclic(T& aObject)
	{
		for (int i = 0; i < myCount; ++i)
		{
			if (myElements[i] == aObject)
			{
				DeleteCyclicAtIndex(i);
				break;
			}
		}
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, S, CountType, UseSafeModeFlag>::DeleteCyclicAtIndex(CountType aIndex)
	{
		assert((aIndex >= 0 && aIndex < S) && "Index outside vector");
		delete myElements[aIndex];
		RemoveCyclicAtIndex(aIndex);
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, S, CountType, UseSafeModeFlag>::RemoveCyclic(const T& aObject)
	{
		for (int i = 0; i < myCount; ++i)
		{
			if (myElements[i] == aObject)
			{
				RemoveCyclicAtIndex(i);
				break;
			}
		}
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, S, CountType, UseSafeModeFlag>::RemoveCyclicAtIndex(CountType aIndex)
	{
		assert((aIndex >= 0 && aIndex < S) && "Index outside vector");
		myElements[aIndex] = myElements[myCount - 1];
		--myCount;
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline T VectorOnStack<T, S, CountType, UseSafeModeFlag>::GetLast() const
	{
		assert((myCount > 0) && "Empty array");
		return myElements[myCount - 1];
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, S, CountType, UseSafeModeFlag>::Clear()
	{
		myCount = static_cast<CountType>(0);
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline void VectorOnStack<T, S, CountType, UseSafeModeFlag>::DeleteAll()
	{
		for (int i = myCount - 1; i > 0; --i)
		{
			delete myElements[i];
			myElements[i] = nullptr;
		}
		Clear();
	}

	template<typename T, int S, typename CountType, bool UseSafeModeFlag>
	inline CountType VectorOnStack<T, S, CountType, UseSafeModeFlag>::Size() const
	{
		return myCount;
	}
}

namespace CU = CommonUtilities;

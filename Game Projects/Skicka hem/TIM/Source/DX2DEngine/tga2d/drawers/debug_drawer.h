#pragma once
#include <vector>
#include <tga2d/math/vector2.h>
#include <tga2d/math/vector4.h>
#include <tga2d/math/color.h>

#ifndef _RETAIL
#define MAXSAMPLES 100
namespace Tga2D
{
	class CText;
	class CLinePrimitive;
	class CSprite;
	class CPerformanceGraph;
	class CLineMultiPrimitive;
	class CDebugDrawer
	{
	public:
		CDebugDrawer(int aFlagset);
		~CDebugDrawer(void);
		void Init();
		void Update(float aTimeDelta);
		void Render();
		void DrawLine(Vector2f aFrom, Vector2f aTo, Vector4f aColor = Vector4f(1, 1, 1, 1));
		void DrawLines(Vector2f* aFrom, Vector2f* aTo, CColor* aColor, unsigned int aCount);

		void DrawArrow(Vector2f aFrom, Vector2f aTo, Vector4f aColor = Vector4f(1, 1, 1, 1));
		void ShowErrorImage();
	private:
		double CalcAverageTick(int newtick);
		std::vector<CLinePrimitive*> myLineBuffer;
		CLineMultiPrimitive* myLineMultiBuffer;
		int myNumberOfRenderedLines;
		int myMaxLines;
		CText* myFPS; 
		CText* myMemUsage;
		CText* myDrawCallText;
		CText* myCPUText;
		CText* myErrorsText;

		CSprite* myErrorSprite;
		
		
		int tickindex;
		int ticksum;
		int ticklist[MAXSAMPLES];
		float myShowErrorTimer;

		int myDebugFeatureSet;
		CPerformanceGraph *myPerformanceGraph;
		unsigned short myRealFPS;
	};
}
#else
namespace Tga2D
{
	class CDebugDrawer
	{
	public:
		CDebugDrawer(int /*aFlagset*/){}
		~CDebugDrawer(){}
		void Init(){}
		void Update(float /*aTimeDelta*/){}
		void Render(){}
		void DrawLine(Vector2f /*aFrom*/, Vector2f /*aTo*/, Vector4f /*aColor*/){}
		void DrawLines(Vector2f* /*aFrom*/, Vector2f* /*aTo*/, CColor* /*aColor*/, unsigned int /*aCount*/){}

		void DrawArrow(Vector2f /*aFrom*/, Vector2f /*aTo*/, Vector4f /*aColor*/){}
		void ShowErrorImage(){}
	private:

	};
}

#endif
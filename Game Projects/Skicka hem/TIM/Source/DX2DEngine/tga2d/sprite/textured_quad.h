#pragma once
#include <tga2d/render/render_object.h>
#include <tga2d/render/render_common.h>

namespace Tga2D
{
	struct STextureRext
	{
		float myStartX;
		float myStartY;
		float myEndX;
		float myEndY;
	};
	struct ID3D11Resource;
	class CEngine;
	class CTextureManager;
	class CTexture;
	class CTexturedQuad : public CRenderObjectSprite
	{
	public:
		CTexturedQuad(void);
		~CTexturedQuad(void);
		void Init(const char* aTexturePath);
		void Render();
		void SetTextureRect(float aStartX, float aStartY, float aEndX, float aEndY);
		CEngine* myEngine;	
		CTexture* myTexture;

		STextureRext& GetTextureRect(){return myTextureRect;}
		const Vector4f& GetTextureRectAsVector() const { return myTextureRectAsVector; }
		void SetColor(const CColor aColor);
		const CColor& GetColor() const;
		const CColor& GetColor();
		void SetSizeRelativeToScreen(const Vector2f& aSize);
		void SetSizeRelativeToImage(const Vector2f& aSize);
		virtual void SetUV(const Vector2f& aUV);
		void SetMap(EShaderMap aMapType, const char* aPath);


		void SetShader(EShader aShaderType) { myShaderType = aShaderType; }
		EShader GetShaderType() const { return myShaderType; }
		
		CTexture* myMaps[MAP_MAX];
		class CCustomShader* myCustomShader;
	private:
		Vector4f myTextureRectAsVector;
		STextureRext myTextureRect;
		EShader myShaderType;
	};
}

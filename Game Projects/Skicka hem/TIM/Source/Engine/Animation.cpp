#include "stdafx.h"
#include "Animation.h"

#include <cmath>
#include <iostream>

#include <tga2d\engine.h>
#include <tga2d\sprite\sprite.h>
#include <Mathf.h>



Animation::Animation()
{
	mySprite = nullptr;
}

Animation::Animation(const char * aPath, const CommonUtilities::Vector2f & aPosition, const CommonUtilities::Vector2f & aScale, float aRotation, eLayerType aLayerType)
{
	Init(aPath, aPosition, aLayerType, aScale, aRotation);
}

Animation::~Animation()
{}

void Animation::Init(const char * aPath, const CommonUtilities::Vector2f & aPosition, eLayerType aLayerType, const CommonUtilities::Vector2f & aScale, float aRotation)
{
	Sprite::Init(aPath, aPosition, aLayerType, aScale, aRotation);
	myDuration = 1.f;
	myTimer = 0.f;
	myLoop = true;
	myIsReversed = false;
	myAnimationSpeed = 1.0f;
	myCurrentFrame = 0;
	myPreviousFrame = 0;
	Setup();
}

void Animation::Update(float aDeltaTime)
{
	if (!myLoop && IsFinished())
	{
		return;
	}

	myTimer += aDeltaTime * myAnimationSpeed;
}

void Animation::Render()
{
	myPreviousFrame = myCurrentFrame;
	myCurrentFrame = (static_cast<int>((myTimer / myDuration) * (myFrames)) % myFrames);
	int index = myCurrentFrame + myOffset;
	if (myIsReversed)
	{
		index = (myFrames - (static_cast<int>((myTimer / myDuration) * (myFrames + 1)) % myFrames) - 1) + myOffset;
	}
	int x = index;
	int y = 0;
	while (x - myCols >= 0)
	{
		x -= myCols;
		++y;
	}

	mySprite->SetUVScale({ myInvCols, myInvRows });
	mySprite->SetUVOffset({
		myInvCols * x,
		myInvRows * y,
	});

	myScaledSize.x *= myInvCols;
	myScaledSize.y *= myInvRows;
	mySprite->SetSizeRelativeToScreen({
		myInvCols * myScale.x* (myOriginalSize.x / RenderObject::OurScreenSizeX) ,
		myInvRows * myScale.y* (myOriginalSize.y / RenderObject::OurScreenSizeY)
	});

	if (myFrameActions[myCurrentFrame] && (myPreviousFrame != myCurrentFrame))
	{
		myFrameActions[myCurrentFrame]();
	}

	Sprite::Render();
}

void Animation::RenderAt(const CU::Vector2f & aPosInCameraSpace)
{
	myPreviousFrame = myCurrentFrame;
	myCurrentFrame = (static_cast<int>((myTimer / myDuration) * (myFrames)) % myFrames);
	int index = myCurrentFrame + myOffset;
	if (myIsReversed)
	{
		index = (myFrames - (static_cast<int>((myTimer / myDuration) * (myFrames + 1)) % myFrames) - 1) + myOffset;
	}
	int x = index;
	int y = 0;
	while (x - myCols >= 0)
	{
		x -= myCols;
		++y;
	}

	mySprite->SetUVScale({ myInvCols, myInvRows });
	mySprite->SetUVOffset({
		myInvCols * x,
		myInvRows * y,
	});

	myScaledSize.x *= myInvCols;
	myScaledSize.y *= myInvRows;
	mySprite->SetSizeRelativeToScreen({
		myInvCols * myScale.x* (myOriginalSize.x / RenderObject::OurScreenSizeY) ,
		myInvRows * myScale.y* (myOriginalSize.y / RenderObject::OurScreenSizeY)
	});

	if (myFrameActions[myCurrentFrame] && (myPreviousFrame != myCurrentFrame))
	{
		myFrameActions[myCurrentFrame]();
	}

	Sprite::RenderAt(aPosInCameraSpace);
}

const Tga2D::Vector2f Animation::GetSeenOnScreenPixelSize() const
{
	Tga2D::Vector2f quadSize;
	quadSize.x = GetSizeInPixels().x / myCols;
	quadSize.y = GetSizeInPixels().y / myRows;
	return quadSize;
}

void Animation::Setup(int aRows, int aCols, int aFrames, int aOffset)
{
	myTimer = 0.f;
	SetRows(aRows);
	SetCols(aCols);
	SetFrames(aFrames);
	SetOffset(aOffset);
}

bool Animation::AddFrameAction(int aFrame, const std::function<void()>& aAction)
{
	if (aFrame > myFrames)
	{
		return false;
	}
	myFrameActions[aFrame] = aAction;
	return true;
}

void Animation::SetRows(int aRows)
{
	myRows = CommonUtilities::Clamp(aRows, 1, 64);
	myInvRows = 1.f / static_cast<float>(myRows);
	mySprite->SetUVScale({ myInvCols, myInvRows });
}
void Animation::SetCols(int aCols)
{
	myCols = CommonUtilities::Clamp(aCols, 1, 64);
	myInvCols = 1.f / static_cast<float>(myCols);
	mySprite->SetUVScale({ myInvCols, myInvRows });
}
void Animation::SetFrames(int aFrames)
{
	myFrames = CommonUtilities::Clamp(aFrames, 1, myRows*myCols);
}
void Animation::SetOffset(int aOffset)
{
	myOffset = CommonUtilities::Clamp(aOffset, 0, myRows*myCols - myFrames);
}
void Animation::SetDuration(float aDuration)
{
	myDuration = std::fabsf(aDuration);
}

bool Animation::IsOnLastFrame()
{
	int currentFrame = (static_cast<int>((myTimer / myDuration) * (myFrames + 1)) % myFrames) + myOffset;
	int lastFrame = (static_cast<int>((myDuration / myDuration) * (myFrames + 1)) % myFrames) + myOffset;
	return currentFrame == lastFrame;
}

void Animation::SetRowToAnimate(int aRow)
{
	if (myRows < aRow)
	{
		aRow = myRows - 1;
	}
	myOffset = myCols * aRow;
}

void Animation::Reset()
{
	myTimer = 0.0f;
}

#include "stdafx.h"
#include "Emitter.h"

#include "tga2d/sprite/sprite_batch.h"
#include "tga2d/sprite/sprite.h"
#include "tga2d/math/common_math.h"

#include "../Game/SpaceConverter.h"
#include <algorithm>

#define PARTICLESINEMITTER myEmittorData->totalParticles
#define RadToDeg(x) (x / (2.f * 3.1415927f)) * 360.f
Emitter::Emitter()
{
}

Emitter::~Emitter()
{
}

void Emitter::Init(const EmitterData & aEmitterType, const CU::Vector2f& aParentPos)
{
	myEmittorData = &aEmitterType;
	myParentPosition = &aParentPos;

	myObjectPool.reserve(PARTICLESINEMITTER);

	mySpriteBatch = new Tga2D::CSpriteBatch(false);
	mySpriteBatch->Init(myEmittorData->spritePath.c_str(), EBlendState_Additativeblend);

	myFirstFreeParticle = PARTICLESINEMITTER - 1;
	for (int particleNbr = 0; particleNbr < PARTICLESINEMITTER; ++particleNbr)
	{
		Particle particle;
		myObjectPool.push_back(particle);
		myObjectPool.back().sprite = new Tga2D::CSprite(nullptr);
		myObjectPool.back().sprite->SetPivot({ 0.5f, 0.5f });
		myObjectPool.back().sprite->SetShouldRender(true);
		ResetParticle(&myObjectPool.back());
	}

	myScreenSize.x = myEmittorData->border.y + myEmittorData->border.w;
	myScreenSize.y = myEmittorData->border.x + myEmittorData->border.z;
	myPivot.x = myEmittorData->border.y / (myScreenSize.x);
	myPivot.y = myEmittorData->border.x / (myScreenSize.y);

	myEmissionTimer = 0.f;
	mySpawnParticleTimer = 0.f;

	myLayerType = eLayerType::EGameObjectLayer;
}

void Emitter::Update(float aDeltaTime)
{
	assert(myParentPosition != nullptr && "A particle emitter haven't gotten a parent pos.");

	myEmissionTimer -= aDeltaTime;
	mySpawnParticleTimer -= aDeltaTime;

	if (mySpawnParticleTimer < 0.f && (myEmissionTimer > 0.f || myEmittorData->emissionDuration == -1))
	{
		ResetParticle(&myObjectPool[myFirstFreeParticle]);
		mySpriteBatch->AddObject(myObjectPool[myFirstFreeParticle].sprite);

		mySpawnParticleTimer = 1.f / myEmittorData->emissionRate;
		myFirstFreeParticle = myFirstFreeParticle - 1 > 0 ? myFirstFreeParticle - 1 : 0; // Takes whichever is bigger.
	}
		
	for (size_t activeParticle = myFirstFreeParticle + 1; activeParticle < myObjectPool.size(); ++activeParticle)
	{
		myObjectPool[activeParticle].life -= aDeltaTime;

		if (myObjectPool[activeParticle].life > 0.f)
		{
			//Position
			myObjectPool[activeParticle].x += cos(myObjectPool[activeParticle].angle) * aDeltaTime * myObjectPool[activeParticle].speed.x;
			myObjectPool[activeParticle].y -= sin(myObjectPool[activeParticle].angle) * aDeltaTime * myObjectPool[activeParticle].speed.y;
			//

			//Scale
			float currentScale = (myObjectPool[activeParticle].life / myObjectPool[activeParticle].lifeTime) * (myObjectPool[activeParticle].startScale - myObjectPool[activeParticle].endScale) + myObjectPool[activeParticle].endScale;
			myObjectPool[activeParticle].sprite->SetSizeRelativeToScreen({ currentScale * SpaceConverter::PixelXToScreen(myObjectPool[activeParticle].radius * 2.f), currentScale * SpaceConverter::PixelYToScreen(myObjectPool[activeParticle].radius * 2.f) });
			//

			//Color
			CU::Vector4f currentColor = (myObjectPool[activeParticle].life / myObjectPool[activeParticle].lifeTime) * (myObjectPool[activeParticle].startColor - myObjectPool[activeParticle].endColor) + myObjectPool[activeParticle].endColor;
			myObjectPool[activeParticle].sprite->SetColor(Tga2D::CColor{ currentColor.x, currentColor.y, currentColor.z, currentColor.w });
			//

			//Acceleration
			myObjectPool[activeParticle].speed.x += myEmittorData->gravity.x * aDeltaTime;
			myObjectPool[activeParticle].speed.y += myEmittorData->gravity.y * aDeltaTime;
			//
		}
		else
		{
			Tga2D::CSprite* tempSprite = myObjectPool[activeParticle].sprite;
			myObjectPool[activeParticle] = myObjectPool[myFirstFreeParticle + 1];
			myObjectPool[activeParticle].sprite = tempSprite;

			mySpriteBatch->RemoveObject(myObjectPool[myFirstFreeParticle + 1].sprite);
			myFirstFreeParticle = myFirstFreeParticle + 1 < myObjectPool.size() - 1 ? myFirstFreeParticle + 1 : myObjectPool.size() - 1; // Takes whichever is smaller.
		}
	}
}

void Emitter::RenderAt(const CU::Vector2f & aPosInCameraSpace)
{
	for (size_t activeParticle = myFirstFreeParticle + 1; activeParticle < myObjectPool.size(); ++activeParticle)
	{
		CU::Vector2f pixelDistParentToParticle(myObjectPool[activeParticle].x - SpaceConverter::TileXToPixel(myParentPosition->x), myObjectPool[activeParticle].y - SpaceConverter::TileXToPixel(myParentPosition->y));
		Tga2D::Vector2f particlePos(aPosInCameraSpace.x + SpaceConverter::PixelXToScreen(pixelDistParentToParticle.x), aPosInCameraSpace.y + SpaceConverter::PixelYToScreen(pixelDistParentToParticle.y));

		myObjectPool[activeParticle].sprite->SetPosition(particlePos);
	}
	mySpriteBatch->Render();
}


const Tga2D::Vector2f Emitter::GetSeenOnScreenPixelSize() const
{
	return myScreenSize;
}

void Emitter::StartEmission()
{
	myEmissionTimer = myEmittorData->emissionDuration;
	myFirstFreeParticle = myObjectPool.size() - 1;
	mySpawnParticleTimer = 0.f;
}

void Emitter::Render()
{
	mySpriteBatch->Render();
}

const Tga2D::Vector2f & Emitter::GetPivot() const
{
	return myPivot;
}

inline const CommonUtilities::Vector2f& Emitter::GetPosition() const
{
	float reallySmallNumber = 1.0001f;
	return *myParentPosition * reallySmallNumber; // This means it will always be slightly below the parent and therefore rendered after.
}

void Emitter::ResetParticle(Particle* aParticle)
{
	// Life
	aParticle->life = myEmittorData->life;
	aParticle->lifeTime = myEmittorData->life;
	//

	// Start position
	float xVar = 0.f;
	if (myEmittorData->posVar.x != 0.f)
	{
		xVar = cast_f(rand() % cast_i(myEmittorData->posVar.x * 2) - myEmittorData->posVar.x);
	}
	aParticle->x = SpaceConverter::TileXToPixel(myParentPosition->x) + myEmittorData->pos.x + xVar;

	float yVar = 0.f;
	if (myEmittorData->posVar.y != 0.f)
	{
		yVar = cast_f(rand() % cast_i(myEmittorData->posVar.y * 2) - myEmittorData->posVar.y);
	}
	aParticle->y = SpaceConverter::TileYToPixel(myParentPosition->y) + myEmittorData->pos.y + yVar;
	//

	// Angle
	float angleVar = 0.f;
	if (myEmittorData->angleVar != 0.f)
	{
		angleVar = cast_f(rand() % cast_i(myEmittorData->angleVar * 2) - myEmittorData->angleVar);
	}
	aParticle->angle = Tga2D::DegToRad(myEmittorData->angle + angleVar);
	//

	// Scale
	float radiusVar = 0.f;
	if (myEmittorData->radiusVar != 0.f)
	{
		radiusVar = cast_f(rand() % cast_i(myEmittorData->radiusVar * 2) - myEmittorData->radiusVar);
	}
	aParticle->radius = myEmittorData->radius + radiusVar;

	float startScale = myEmittorData->startScale;
	if (myEmittorData->startScaleVar != 0.f)
	{
		startScale += cast_f(rand() % cast_i(myEmittorData->startScaleVar * 2) - myEmittorData->startScaleVar);
	}
	aParticle->startScale = startScale;

	float endScale = myEmittorData->endScale;
	if (myEmittorData->endScaleVar != 0.f)
	{
		endScale += cast_f(rand() % cast_i(myEmittorData->endScaleVar * 2) - myEmittorData->endScaleVar);
	}
	aParticle->endScale = endScale;

	aParticle->sprite->SetSizeRelativeToScreen({ aParticle->startScale * SpaceConverter::PixelXToScreen(aParticle->radius * 2.f), aParticle->startScale * SpaceConverter::PixelYToScreen(aParticle->radius * 2.f) });
	//

	//Color
	aParticle->startColor = myEmittorData->startColor + myEmittorData->startColorVar;
	aParticle->endColor = myEmittorData->endColor + myEmittorData->endColor;
	//

	//Speed
	aParticle->speed.x = myEmittorData->speed.x;
	aParticle->speed.y = myEmittorData->speed.y;
	//
}
#pragma once
#include <vector>
#include "tga2d/math/color.h"
#include "Vector.h"
#include "tga2d/sprite/sprite.h"
#include "RenderObject.h"

namespace Tga2D
{
	class CSpriteBatch;
	class CSprite;
}

struct EmitterData
{
	std::string spritePath;
	CU::Vector4f startColor;
	CU::Vector4f startColorVar;
	CU::Vector4f endColor;
	CU::Vector4f endColorVar; 
	CU::Vector4f border;
	CU::Vector2f gravity;
	CU::Vector2f posVar;
	CU::Vector2f pos;
	CU::Vector2f speed;
	float speedVar;
	float angle;
	float angleVar;
	float life;
	float lifeVar;
	float radius;
	float radiusVar;
	float startScale;
	float startScaleVar;
	float endScale;
	float endScaleVar;
	float emissionRate;
	float emissionDuration;
	float totalParticles;
	bool textureAdditive;
};

struct Particle
{
	~Particle()
	{
		if (sprite != nullptr)
		{
			delete sprite;
			sprite = nullptr;
		}
	}
	Tga2D::CSprite* sprite = nullptr;
	CU::Vector4f startColor;
	CU::Vector4f endColor;
	CU::Vector2f speed;
	float life;
	float lifeTime;
	float x;
	float y;
	float radius;
	float angle;
	float startScale; 
	float endScale;
};

class Emitter : public RenderObject
{
public:
	Emitter();
	~Emitter();
	
	void Update(float aDeltaTime) override;
	void Init(const EmitterData & aEmitterType, const CU::Vector2f& aParentPos);
	void StartEmission();

	// Stuff for RenderObject in camera
	void Render() override;
	void RenderAt(const CU::Vector2f& aPosInCameraSpace) override;
	const Tga2D::Vector2f& GetPivot() const;
	const Tga2D::Vector2f GetSeenOnScreenPixelSize() const override;
	inline const CommonUtilities::Vector2f& GetPosition() const override;
	//

private:
	void ResetParticle(Particle* aParticle);

	float myEmissionTimer;
	float mySpawnParticleTimer;

	int myFirstFreeParticle;
	std::vector<Particle> myObjectPool;
	EmitterData const * myEmittorData;
	Tga2D::CSpriteBatch* mySpriteBatch;
	Tga2D::CSprite* mySprite;
	CU::Vector2f const * myParentPosition;

	Tga2D::Vector2f myPivot;
	Tga2D::Vector2f myScreenSize;
};


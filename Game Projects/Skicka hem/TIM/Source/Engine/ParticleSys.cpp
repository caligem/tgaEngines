#include "stdafx.h"
#include "ParticleSys.h"
#include <fstream>

ParticleSys::ParticleSys()
{
}


ParticleSys::~ParticleSys()
{
}

void ParticleSys::Init(const nlohmann::json & aEmitterFiles)
{
	std::ifstream fileStream;
	nlohmann::json emitterType;

	for (const auto& emittorFile : aEmitterFiles)
	{

		fileStream = std::ifstream(emittorFile.get<std::string>());
		fileStream >> emitterType;
		emitterType = emitterType[0];
		EmitterData emitter;

		emitter.pos.x = emitterType["pos"]["x"].get<float>();
		emitter.pos.y = emitterType["pos"]["y"].get<float>();
		emitter.posVar.x = emitterType["posVar"]["x"].get<float>();
		emitter.posVar.y = emitterType["posVar"]["y"].get<float>();
		emitter.gravity.x = emitterType["gravity"]["x"].get<float>();
		emitter.gravity.y = emitterType["gravity"]["y"].get<float>();
		emitter.startColor = {
			emitterType["startColor"][0].get<float>() / 255.f,
			emitterType["startColor"][1].get<float>() / 255.f,
			emitterType["startColor"][2].get<float>() / 255.f,
			emitterType["startColor"][3].get<float>()
		};
		emitter.startColorVar = {
			emitterType["startColorVar"][0].get<float>() / 255.f,
			emitterType["startColorVar"][1].get<float>() / 255.f,
			emitterType["startColorVar"][2].get<float>() / 255.f,
			emitterType["startColorVar"][3].get<float>()
		};
		emitter.endColor = {
			emitterType["endColor"][0].get<float>() / 255.f,
			emitterType["endColor"][1].get<float>() / 255.f,
			emitterType["endColor"][2].get<float>() / 255.f,
			emitterType["endColor"][3].get<float>()
		};
		emitter.endColorVar = {
			emitterType["endColorVar"][0].get<float>() / 255.f,
			emitterType["endColorVar"][1].get<float>() / 255.f,
			emitterType["endColorVar"][2].get<float>() / 255.f,
			emitterType["endColorVar"][3].get<float>()
		};
		emitter.border.x = emitterType["border"]["top"].get<float>();
		emitter.border.y = emitterType["border"]["left"].get<float>();
		emitter.border.z = emitterType["border"]["bottom"].get<float>();
		emitter.border.w = emitterType["border"]["right"].get<float>();
		emitter.speed.x = emitterType["speed"].get<float>();
		emitter.speed.y = emitterType["speed"].get<float>();
		emitter.speedVar = emitterType["speedVar"].get<float>();
		emitter.angle = emitterType["angle"].get<float>();
		emitter.angleVar = emitterType["angleVar"].get<float>();
		emitter.life = emitterType["life"].get<float>();
		emitter.lifeVar = emitterType["lifeVar"].get<float>();
		emitter.radius = emitterType["radius"].get<float>();
		emitter.radiusVar = emitterType["radiusVar"].get<float>();
		emitter.startScale = emitterType["startScale"].get<float>();
		emitter.startScaleVar = emitterType["startScaleVar"].get<float>();
		emitter.endScale = emitterType["endScale"].get<float>();
		emitter.endScaleVar = emitterType["endScaleVar"].get<float>();
		emitter.emissionRate = emitterType["emissionRate"].get<float>();
		emitter.totalParticles = emitterType["totalParticles"].get<float>();
		if (emitterType["duration"] != nullptr)
		{
			emitter.emissionDuration = emitterType["duration"].get<float>();
		}
		else
		{
			emitter.emissionDuration = -1.f;
		}
		emitter.endScaleVar = emitterType["endScaleVar"].get<float>();
		emitter.textureAdditive = emitterType["textureAdditive"].get<bool>();
		emitter.spritePath = emitterType["texture"].get<std::string>();
		myEmitterTypes.push_back(emitter);
	}
	fileStream.close();
}

EmitterData const * ParticleSys::GetEmitter(const int aIndex) const
{
	return &myEmitterTypes[aIndex];
}
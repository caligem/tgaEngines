#pragma once
#include <vector>
#include "../Game/json.h"
#include "Emitter.h"

class ParticleSys
{
public:
	ParticleSys();
	~ParticleSys();
	void Init(const  nlohmann::json& aJson);
	EmitterData const* GetEmitter(const int aIndex) const; // maybe should be a enum instead if it gets confusing, but then someone has to manually update the enum with the json file.
private:
	std::vector<EmitterData> myEmitterTypes;
};


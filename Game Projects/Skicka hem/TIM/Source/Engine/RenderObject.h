#pragma once
#include <Vector.h>
#include <tga2d\math\vector2.h>
#include <tga2d\render\render_common.h>
#include <tga2d\error\error_manager.h>

enum class eLayerType
{
	EBackGround,
	EGameObjectLayer,
	EForeground,
	EUserInterface
};


class RenderObject
{
public:
	RenderObject() { myLayerType = eLayerType::EBackGround; }
	virtual ~RenderObject() {}
	virtual void Render() = 0;
	virtual void RenderAt(const CU::Vector2f& aPosInCameraSpace) = 0;
	virtual void Update(float) {}
	virtual const Tga2D::Vector2f& GetPivot() const = 0;
	virtual void SetPivot(const CommonUtilities::Vector2f& aPivotPosition) { aPivotPosition; };
	virtual const Tga2D::Vector2f GetSeenOnScreenPixelSize() const = 0;
	
	__declspec(deprecated("this function only returns false because the implementation is shit, use the colliders GetinsideScreen instead")) bool IsOutsideScreen() { return false; };

	virtual void SetRotation(float aRotation) { myRotation = aRotation; }
	virtual void SetPosition(const CommonUtilities::Vector2f& aPos) { myPosition = aPos; }
	inline void SetLayerType(eLayerType aType) { myLayerType = aType; }
	inline eLayerType GetType() { return myLayerType; }
	inline const Tga2D::Vector2f& GetImageSize() const { return myPixelSize; }
	virtual inline const CommonUtilities::Vector2f& GetPosition() const { return myPosition; }


	static float OurScreenSizeX;
	static float OurScreenSizeY;

	protected:
	CommonUtilities::Vector4f myTint;
	CommonUtilities::Vector2f myPosition;
	CommonUtilities::Vector2f myScale;
	
	Tga2D::Vector2f myScaledSize;
	Tga2D::Vector2f myPixelSize;

	eLayerType myLayerType;

	float myRotation;
};


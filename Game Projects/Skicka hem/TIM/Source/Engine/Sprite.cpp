#include "stdafx.h"
#include "Sprite.h"
#include <tga2d/sprite/sprite.h>
#include <tga2d/math/color.h>
#include <tga2d/sprite/textured_quad.h>

Sprite::Sprite()
{
	mySprite = std::make_shared<Tga2D::CSprite>();
}

Sprite::Sprite(const char * aPath, const CommonUtilities::Vector2f & aPosition, eLayerType aLayer, const CommonUtilities::Vector2f & aScale, float aRotation, const CommonUtilities::Vector4f & aTint)
{
	Init(aPath, aPosition, aLayer, aScale, aRotation, aTint);
}

Sprite::~Sprite()
{
}

void Sprite::Init(const char * aPath, const CommonUtilities::Vector2f & aPosition, eLayerType aLayer, const CommonUtilities::Vector2f & aScale, float aRotation, const CommonUtilities::Vector4f & aTint)
{
	mySprite = std::make_shared<Tga2D::CSprite>(aPath);
	mySprite->SetPivot({ 0.5f, 0.8f });
	myLayerType = aLayer;
	myPosition = aPosition;
	myScale = aScale;
	myRotation = aRotation;
	
	myOriginalSize = { static_cast<float>(mySprite->GetImageSize().x), static_cast<float>(mySprite->GetImageSize().y) };
	mySprite->SetRotation(aRotation);
	mySprite->SetPosition({aPosition.x, aPosition.y});
	myPixelSize = {
		static_cast<float>(mySprite->GetImageSize().x),
		static_cast<float>(mySprite->GetImageSize().y)
	};
	myTint = aTint;

	SetBlendState(EBlendState::EBlendState_Alphablend);
}

void Sprite::Render()
{
	if (mySprite == nullptr)
	{
		std::string msg = "ERROR: Sprite was nullptr, try Initiating all sprites before rendering them";
		ERROR_PRINT(msg.c_str());
		mySprite = std::make_shared<Tga2D::CSprite>("Sprites/ErrorSprite.dds");
	}


	mySprite->Render();
}

void Sprite::RenderAt(const CU::Vector2f & aPosInCameraSpace)
{
	if (mySprite == nullptr)
	{
		std::string msg = "ERROR: Sprite was nullptr, try Initiating all sprites before rendering them";
		ERROR_PRINT(msg.c_str());
		mySprite = std::make_shared<Tga2D::CSprite>("Sprites/ErrorSprite.dds");
	}

	mySprite->SetPosition({ aPosInCameraSpace.x, aPosInCameraSpace.y });
	mySprite->Render();
}

const Tga2D::Vector2f Sprite::GetSeenOnScreenPixelSize() const
{
	Tga2D::Vector4f textRect = GetTexturedQuad()->GetTextureRectAsVector();
	if (textRect.x == 0.f && textRect.y == 0.f && textRect.z == 0.f && textRect.w == 0.f)
	{
		return Tga2D::Vector2f(GetSizeInPixels().x, GetSizeInPixels().y);
	}

	Tga2D::Vector2f quadSize;
	quadSize.x = abs(textRect.x - textRect.z) * GetSizeInPixels().x;
	quadSize.y = abs(textRect.y - textRect.w) * GetSizeInPixels().y;
	return quadSize;
}

void Sprite::SetUVScale(const CommonUtilities::Vector2f & aUv)
{
	mySprite->SetUVScale({ aUv.x, aUv.y });
}

void Sprite::SetBlendState(EBlendState aBlendState)
{
	myBlendState = aBlendState;
	mySprite->SetBlendState(myBlendState);
}

void Sprite::SetPosition(CommonUtilities::Vector2f aPos)
{
	 myPosition = aPos;
	 mySprite->SetPosition({ aPos.x, aPos.y }); 
}

void Sprite::SetColor(CommonUtilities::Vector4f aColor)
{
	mySprite->SetColor({ aColor.x, aColor.y, aColor.z, aColor.w });
}

void Sprite::SetRotation(float aRotation) 
{ 
	mySprite->SetRotation(aRotation);
}

void Sprite::SetSizeRelativeToImage(const CommonUtilities::Vector2f& aSize) 
{
	mySprite->SetSizeRelativeToImage({ aSize.x, aSize.y });
}

void Sprite::SetSizeRelativeToScreen(const CommonUtilities::Vector2f& aSize) 
{
	mySprite->SetSizeRelativeToScreen({ aSize.x, aSize.y });
}

void Sprite::SetTextureRect(float aStartX, float aStartY, float aEndX, float aEndY) 
{
	mySprite->SetTextureRect(aStartX, aStartY, aEndX, aEndY);
}

void Sprite::SetShouldRender(bool aShouldRender) 
{
	mySprite->SetShouldRender(aShouldRender);
}

bool Sprite::GetShouldRender() const
{
	return mySprite->GetShouldRender();
}

float Sprite::GetRotation() const
{
	return mySprite->GetRotation();
}

const Tga2D::Vector2f& Sprite::GetPivot() const
{
	return mySprite->GetPivot();
}

void Sprite::SetPivot(const CommonUtilities::Vector2f & aPivotPosition)
{
	mySprite->SetPivot({ aPivotPosition.x, aPivotPosition.y });
}

const CommonUtilities::Vector2f Sprite::GetSizeInPixels() const
{
	return CommonUtilities::Vector2f(mySprite->GetImageSize().x, mySprite->GetImageSize().y);
}

const CommonUtilities::Vector4f Sprite::GetColor() const
{
	Tga2D::CColor tempColor = mySprite->GetColor();
	return{ tempColor.myR, tempColor.myG,tempColor.myB,tempColor.myA };
}

CommonUtilities::Vector4f Sprite::GetPixelColor(unsigned short aX, unsigned short aY) const
{
	Tga2D::CColor& tempColor = mySprite->GetPixelColor(aX, aY);
	return {tempColor.myR, tempColor.myG, tempColor.myB, tempColor.myA};
}
Tga2D::CTexturedQuad * Sprite::GetTexturedQuad() const
{
	return mySprite->GetTexturedQuad();
}
std::shared_ptr<Tga2D::CSprite> Sprite::GetSprite() const
{
	return mySprite;
}
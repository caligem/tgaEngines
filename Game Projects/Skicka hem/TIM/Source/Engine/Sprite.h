#pragma once
#include "RenderObject.h"
#include "tga2d/render/render_common.h"
#include <memory>

namespace Tga2D
{
	class CSprite;
	class CTexturedQuad;
}


class Sprite : public RenderObject
{
public:
	Sprite();
	Sprite(
		const char* aPath,
		const CommonUtilities::Vector2f& aPosition = { 0.f, 0.f},
		const eLayerType aLayer = eLayerType::EBackGround,
		const CommonUtilities::Vector2f& aScale = { 1.f, 1.f },
		float aRotation = 0.f,
		const CommonUtilities::Vector4f& aTint = { 1.f, 1.f, 1.f, 1.f }
	);
	virtual ~Sprite();

	virtual void Init(
		const char* aPath,
		const CommonUtilities::Vector2f& aPosition = { 0.f, 0.f},
		const eLayerType aLayer = eLayerType::EBackGround,
		const CommonUtilities::Vector2f& aScale = { 1.f, 1.f },
		float aRotation = 0.f,
		const CommonUtilities::Vector4f& aTint = { 1.f, 1.f, 1.f, 1.f }
	);
	virtual void Update(float) override {};
	virtual void Render() override;
	virtual void RenderAt(const CU::Vector2f& aPosInCameraSpace) override;
	const Tga2D::Vector2f& GetPivot() const override;
	void SetPivot(const CommonUtilities::Vector2f& aPivotPosition) override;
	const Tga2D::Vector2f GetSeenOnScreenPixelSize() const override;
	void SetUVScale(const CommonUtilities::Vector2f& aUv);
	void SetBlendState(EBlendState aBlendState);
	void SetPosition(CommonUtilities::Vector2f aPos);
	void SetColor(CommonUtilities::Vector4f aColor);
	void SetRotation(float aRotation);
	void SetSizeRelativeToImage(const CommonUtilities::Vector2f& aSize);
	void SetSizeRelativeToScreen(const CommonUtilities::Vector2f& aSize);
	void SetTextureRect(float aStartX, float aStartY, float aEndX, float aEndY);
	void SetShouldRender(bool aShouldRender);
	

	bool GetShouldRender() const;
	float GetRotation() const;
	const CommonUtilities::Vector2f GetSizeInPixels() const;
	const CommonUtilities::Vector4f GetColor() const;
	CommonUtilities::Vector4f GetPixelColor(unsigned short aX, unsigned short aY) const;

	Tga2D::CTexturedQuad* GetTexturedQuad() const;
	std::shared_ptr<Tga2D::CSprite> GetSprite() const;
protected:
	std::shared_ptr<Tga2D::CSprite> mySprite;
	EBlendState myBlendState;
	CommonUtilities::Vector2f myOriginalSize;
};


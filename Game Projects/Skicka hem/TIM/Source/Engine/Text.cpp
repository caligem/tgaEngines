#include "stdafx.h"
#include "Text.h"


Text::Text()
	: myBoundingBox({0.f, 0.f}, {1.f, 1.f})
{
	myText = nullptr;
}

Text::Text(const char* aPathAndName, bool aShouldScaleInsteadOfWrap, Tga2D::EFontSize aFontSize, unsigned char aBorderSize)
	: myBoundingBox({ 0.f, 0.f }, { 1.f, 1.f })
{
	Init(aPathAndName, aShouldScaleInsteadOfWrap, aFontSize, aBorderSize);
}

Text::Text(const char * aPathAndName, AABB aBoundingBox, bool aShouldScaleInsteadOfWrap, Tga2D::EFontSize aFontSize, unsigned char aBorderSize)
	: myBoundingBox(aBoundingBox)
{
	Init(aPathAndName, aShouldScaleInsteadOfWrap, aFontSize, aBorderSize);
}


Text::~Text()
{
	//if (myText != nullptr)
	//{
	//	delete myText;
	//	myText = nullptr;
	//}
}

void Text::Init(const char * aPathAndName, bool aShouldScaleInsteadOfWrap, Tga2D::EFontSize aFontSize, unsigned char aBorderSize)
{
	myShouldScaleInsteadOfWrap = aShouldScaleInsteadOfWrap;
	myText = new Tga2D::CText(aPathAndName, aFontSize, aBorderSize);

}

void Text::Render()
{
	myText->Render();
}

void Text::RenderAt(const CU::Vector2f & aPosInCameraSpace)
{
	myText->myPosition = { aPosInCameraSpace.x, aPosInCameraSpace.y };
	myText->Render();
	myText->myPosition = { myPosition.x, myPosition.y };
}


void Text::SetPosition(const CommonUtilities::Vector2f & aPosition)
{
	myPosition = aPosition;
	myText->myPosition = { myPosition.x, myPosition.y };
}

void Text::SetScale(const float aScale)
{
	myText->myScale = aScale;
}

void Text::CheckForSplit()
{
	if (myText->GetWidth() > myBoundingBox.myMax.x - myBoundingBox.myMin.x)
	{
		std::string aTextToSplit = myText->myText;
		myText->myText = "";
		int amountOfLines = 1;
		for (int i = 0; i < aTextToSplit.size(); ++i)
		{
			if (myText->GetWidth() + (10 / 1920.f) > (myBoundingBox.myMax.x - myBoundingBox.myMin.x) * amountOfLines)
			{
				if (aTextToSplit[i] == ' ')
				{
					++amountOfLines;
					myText->myText += "\n";
				}
				else
				{
					myText->myText += aTextToSplit[i];
				}
			}
			else
			{
				myText->myText += aTextToSplit[i];
			}
		}

	}
}

void Text::SetColor(const CommonUtilities::Vector4f & aColor)
{
	myTint = aColor;
	myText->myColor = { myTint.x, myTint.y, myTint.z, myTint.w };
}

void Text::SetText(const char * aText)
{
	myText->myText = aText;
	SetScale(1.f);
	if (myShouldScaleInsteadOfWrap)
	{
		ChangeScaleToFit();
	}
	else
	{
		CheckForSplit();
	}
}

const char * Text::GetText() const
{
	return myText->myText.c_str();
}

void Text::ChangeScaleToFit()
{
	float newScale = myText->myScale;
	while (myText->GetWidth() > myBoundingBox.myMax.x - myBoundingBox.myMin.x)
	{
		newScale = newScale * 0.95f;
		SetScale(newScale);
	}
}

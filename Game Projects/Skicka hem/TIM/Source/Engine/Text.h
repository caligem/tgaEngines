#pragma once
#include "RenderObject.h"
#include "tga2d\text\text.h"
#include <memory>
#include "AABB.h"

class Text : public RenderObject
{
public:
	Text();
	Text(const char* aPathAndName, bool aShouldScaleInsteadOfWrap = false, Tga2D::EFontSize aFontSize = Tga2D::EFontSize_14, unsigned char aBorderSize = 1);
	Text(const char* aPathAndName, AABB aBoundingBox, bool aShouldScaleInsteadOfWrap = false, Tga2D::EFontSize aFontSize = Tga2D::EFontSize_14, unsigned char aBorderSize = 1);
	~Text();

	void Init(const char* aPathAndName, bool aShouldScaleInsteadOfWrap = false, Tga2D::EFontSize aFontSize = Tga2D::EFontSize_14, unsigned char aBorderSize = 1);
	virtual void Render() override;
	virtual void RenderAt(const CU::Vector2f& aPosInCameraSpace) override;

	inline void SetAABB(AABB aBoxToSet) { myBoundingBox = aBoxToSet; }
	inline const AABB& GetAABB() { return myBoundingBox; }
	void ChangeScaleToFit();
	void CheckForSplit();

	void SetPosition(const CommonUtilities::Vector2f& aPosition) override;

	void SetScale(const float aScale);
	inline const float GetScale() const { return myText->myScale; }


	void SetColor(const CommonUtilities::Vector4f& aColor);
	inline const CommonUtilities::Vector4f& GetColor() const { return myTint; }

	void SetText(const char* aText);
	const char* GetText() const;

	inline const float GetWidth() const { return myText->GetWidth(); }

	__declspec(deprecated("not useful")) virtual void Update(float) override {};
	__declspec(deprecated("not useful")) virtual const Tga2D::Vector2f& GetPivot() const override { return myPivot; }
	__declspec(deprecated("not useful")) virtual const Tga2D::Vector2f GetSeenOnScreenPixelSize() const override { return{ 0.f, 0.f }; }

private:
	Tga2D::CText* myText;
	Tga2D::Vector2f myPivot;
	AABB myBoundingBox;
	bool myShouldScaleInsteadOfWrap;
};


#include "stdafx.h"
#include "Activator.h"
#include "CollisionObject.h"
#include "PostMaster.h"
#include "Sprite.h"


Activator::Activator()
{
	myIsActivated = false;
}


Activator::~Activator()
{
}

void Activator::Init(CollisionManager * aCollisionManager, const CommonUtilities::Vector2f & aPosition, const int aConnectID)
{
	myTag = eGameObjectTag::Activator;
	InitGO(aPosition, aCollisionManager, eCollisionLayer::Terrain);

	myConnectID = aConnectID;
}

void Activator::OnCollisionEnter(CollisionObject & aOther)
{
	if (aOther.GetOwner() != nullptr)
	{
		if (aOther.GetOwner()->GetTag() == eGameObjectTag::Wand && !myIsActivated)
		{
			myIsActivated = true;
			LockUnlockDoor();
			myDrawable = myActivatedSprite;
		}
	}
}

void Activator::SetActivatedSprite(const char * aPath)
{
	Sprite* activatedSprite = new Sprite(aPath, myPosition, eLayerType::EGameObjectLayer);
	assert(activatedSprite != nullptr && "Cannot find activated activator image");
	activatedSprite->SetPosition({ myPosition.x, myPosition.y });
	activatedSprite->SetPivot({ 0.5f, 0.5f });
	myActivatedSprite = activatedSprite;
}

void Activator::LockUnlockDoor()
{
	Message Message(eMessageType::DoorLockUnlock);
	Message.data.myInt = myConnectID;
	PostMaster::SendMessages(Message);
}
#pragma once
#include "GameObject.h"
#include "Subscriber.h"

class Activator : public GameObject, public Subscriber
{
public:
	Activator();
	~Activator();

	void Init(CollisionManager* aCollisionManager, const CommonUtilities::Vector2f& aPosition, const int aConnectID);

	inline int const GetConnectID() const { return myConnectID; }

	void OnCollisionEnter(CollisionObject &aOther) override;
	void SetActivatedSprite(const char * aPath);
	void LockUnlockDoor();

protected:
	RenderObject* myActivatedSprite;
	bool myIsActivated;

	int myConnectID;
};


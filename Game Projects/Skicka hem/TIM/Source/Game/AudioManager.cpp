#include "stdafx.h"
#include "AudioManager.h"
#include "fmod_common.h"
#include "fmod_errors.h"

AudioManager* AudioManager::ourAudioManager = nullptr;

bool AudioManager::Create()
{
	ourAudioManager = new AudioManager;
	ourAudioManager->FMODErrorCheck(FMOD::System_Create(&ourAudioManager->mySystem));
	ourAudioManager->mySystem->init(100, FMOD_INIT_NORMAL, ourAudioManager->myExtradriverdata);
	ourAudioManager->mySystem->createChannelGroup(NULL, &ourAudioManager->myMusicGroup);
	ourAudioManager->mySystem->createChannelGroup(NULL, &ourAudioManager->myEffectGroup);
	ourAudioManager->myEffectGroup->setVolume(0.5f);
	ourAudioManager->myMusicGroup->setVolume(0.3f);
	ourAudioManager->mySoundPath = "Audio/Sound/";
	ourAudioManager->myMusicPath = "Audio/Music/";
	ourAudioManager->myIsMuted = false;
	ourAudioManager->myCurrentSong = "";
	return true;
}

void AudioManager::Destroy()
{
	delete ourAudioManager;
	ourAudioManager = nullptr;
}

bool AudioManager::LoadMusic(nlohmann::json aMusicObject)
{
	FMOD::Sound* sound;

	for (auto it = aMusicObject.begin(); it != aMusicObject.end(); ++it)
	{
		std::string musicName = it.key();
		std::string fileName = it.value();
		ourAudioManager->myResult = ourAudioManager->mySystem->createStream((ourAudioManager->myMusicPath + fileName).c_str(), FMOD_DEFAULT, 0, &sound);
		ourAudioManager->FMODErrorCheck(ourAudioManager->myResult);
		ourAudioManager->FMODErrorCheck(sound->setMode(FMOD_LOOP_NORMAL));
		ourAudioManager->myMusic.insert(std::make_pair(musicName, sound));
	}
	return true;
}

bool AudioManager::LoadSound(nlohmann::json aSoundObjects)
{
	FMOD::Sound* sound;

	for (auto it = aSoundObjects.begin(); it != aSoundObjects.end(); ++it)
	{
		std::string soundName = it.key();
		std::string fileName = it.value();
		ourAudioManager->myResult = ourAudioManager->mySystem->createSound((ourAudioManager->mySoundPath + fileName).c_str(), FMOD_DEFAULT, 0, &sound);
		ourAudioManager->FMODErrorCheck(ourAudioManager->myResult);
		ourAudioManager->FMODErrorCheck(sound->setMode(FMOD_LOOP_OFF));
		ourAudioManager->mySounds.insert(std::make_pair(soundName, sound));
	}
	return true;
}

bool AudioManager::PlayRandomSound(std::string aSoundName, int aRandomNumber)
{
	int hitSound = rand() % aRandomNumber + 1;
	return ourAudioManager->PlaySound((aSoundName + std::to_string(hitSound)).c_str());
}

bool AudioManager::PlaySound(const std::string& aSoundName)
{
	//static int count = 0;
	//std::cout << ++count << "\t" << aSoundName << std::endl;
	ourAudioManager->myResult = ourAudioManager->mySystem->playSound(
		ourAudioManager->mySounds[aSoundName],
		ourAudioManager->myEffectGroup,
		false,
		&ourAudioManager->myChannel[static_cast<int>(Audiochannel::Effects)]);
	ourAudioManager->FMODErrorCheck(ourAudioManager->myResult);
	return true;
}

bool AudioManager::PlayMusic(const std::string& aMusicName)
{
	if (ourAudioManager->myCurrentSong != aMusicName)
	{
		SetCurrentSong(aMusicName);
		PlayMusic();
	}
	return true;
}

bool AudioManager::PlayMusic()
{
	StopMusic();
	ourAudioManager->myResult = ourAudioManager->mySystem->playSound(
		ourAudioManager->myMusic[ourAudioManager->myCurrentSong],
		ourAudioManager->myMusicGroup,
		false,
		&ourAudioManager->myChannel[static_cast<int>(Audiochannel::Music)]);
	ourAudioManager->FMODErrorCheck(ourAudioManager->myResult);
	return true;
}

bool AudioManager::SetCurrentSong(const std::string& aMusicName)
{
	ourAudioManager->myCurrentSong = aMusicName;
	return false;
}

bool AudioManager::StopSound(const std::string& /*aSoundName*/)
{
	return false;
}

bool AudioManager::StopMusic()
{
	ourAudioManager->myMusicGroup->stop();
	return true;
}

bool AudioManager::SetMusicVolume(float aValue)
{
	ourAudioManager->myMusicGroup->setVolume(aValue);
	return false;
}

bool AudioManager::SetEffectsVolume(float aValue)
{
	ourAudioManager->myEffectGroup->setVolume(aValue);
	return false;
}

bool AudioManager::ToggleMute()
{
	if (ourAudioManager->myIsMuted)
	{
		ourAudioManager->myEffectGroup->setMute(false);
		ourAudioManager->myMusicGroup->setMute(false);
		ourAudioManager->myIsMuted = false;
	}
	else
	{
		ourAudioManager->myEffectGroup->setMute(true);
		ourAudioManager->myMusicGroup->setMute(true);
		ourAudioManager->myIsMuted = true;
	}
	return true;
}

void AudioManager::Update()
{
	ourAudioManager->mySystem->update();
}

bool AudioManager::IsMuted()
{
	return ourAudioManager->myIsMuted;
}

AudioManager* AudioManager::GetInstance()
{
	return ourAudioManager;
}

AudioManager::AudioManager()
{
}

AudioManager::~AudioManager()
{
	for (auto& sound : ourAudioManager->mySounds)
	{
		ourAudioManager->FMODErrorCheck(sound.second->release());
	}
	for (auto& music : ourAudioManager->myMusic)
	{
		ourAudioManager->FMODErrorCheck(music.second->release());
	}
	ourAudioManager->FMODErrorCheck(mySystem->close());
	ourAudioManager->FMODErrorCheck(mySystem->release());
}

void AudioManager::FMODErrorCheck(FMOD_RESULT aResult)
{
	if (aResult != FMOD_OK)
	{
		std::cout << "FMOD error! (" << aResult << ") " << FMOD_ErrorString(aResult) << std::endl;
		assert(false && "Soundfile missing or incorrectly named");
	}
}

#pragma once
#include <string>
#include <map>
#include <math.h>
#include <iostream>
#include "Vector.h"
#include <fmod.hpp>
#include "json.h"

/*	To play a sound	 */
/*
	Check soundData.json for what the sound is called.
	Include this file where you wanna play it.
	Use this line to play sound.
	AudioManager::GetInstance()->PlaySound("nameOfSound");
	Rejoice.
*/

class AudioManager
{
public:
	static AudioManager* GetInstance();
	static bool Create();
	static void Destroy();
	static bool LoadMusic(nlohmann::json aMusicObject);
	static bool LoadSound(nlohmann::json aSoundObject);
	static bool PlayRandomSound(std::string aSoundName, int aRandomNumber);
	static bool PlaySound(const std::string& aSoundName);
	static bool PlayMusic(const std::string& aMusicName);
	static bool PlayMusic();
	static bool SetCurrentSong(const std::string& aMusicName);
	static bool StopSound(const std::string& aSoundName);
	static bool StopMusic();
	static bool SetMusicVolume(float aValue);
	static bool SetEffectsVolume(float aValue);
	static bool ToggleMute();
	static void Update();
	static bool IsMuted();

private:
	enum class Audiochannel
	{
		Effects = 0,
		Music = 1
	};
	AudioManager();
	~AudioManager();
	static AudioManager*					ourAudioManager;
	void FMODErrorCheck(FMOD_RESULT result);

	std::string								mySoundPath;
	std::string								myMusicPath;

	FMOD::System*							mySystem;
	std::map<std::string, FMOD::Sound*>		mySounds;
	std::map<std::string, FMOD::Sound*>		myMusic;
	FMOD::Channel*							myChannel[2];
	FMOD::ChannelGroup*						myMusicGroup;
	FMOD::ChannelGroup*						myEffectGroup;
	bool									myIsMuted;
	std::string								myCurrentSong;
	FMOD_RESULT								myResult;
	unsigned int							myVersion;
	void*									myExtradriverdata = 0;
};

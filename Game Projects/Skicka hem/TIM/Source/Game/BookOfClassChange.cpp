#include "stdafx.h"
#include "BookOfClassChange.h"
#include "CollisionObject.h"
#include "Wand.h"
#include "Player.h"
#include "PostMaster.h"
#include "AudioManager.h"
#include "Boss.h"


BookOfClassChange::BookOfClassChange()
{


}


BookOfClassChange::~BookOfClassChange()
{
}

void BookOfClassChange::Init(CollisionManager * aCollisionManager, const CommonUtilities::Vector2f & aPosition, const int aConnectID1, const int aConnectID2, const int aConnectID3)
{
	myTag = eGameObjectTag::BookOfClassChange;
	myBookState = eBookState::ThreeLocks;

	myFirstConnectID = aConnectID1;
	mySecondConnectID = aConnectID2;
	myThirdConnectID = aConnectID3;

	myHoldedWandConnectID = -1;
	myHoldedWandIndex = 0;

	myPlayerIsInReach = false;

	InitGO(aPosition, aCollisionManager, eCollisionLayer::Interactable);
}

void BookOfClassChange::OnCollisionEnter(CollisionObject & aOther)
{
	if (aOther.GetOwner()->GetTag() == eGameObjectTag::Player)
	{
		Player* player = dynamic_cast<Player*>(aOther.GetOwner());
		myHoldedWandConnectID = player->GetInventory().GetActiveItem()->myConnectID;
		myHoldedWandIndex = cast_i(player->GetInventory().GetActiveItemIndex());

		myPlayerIsInReach = true;

		if (MatchingConnectID(myHoldedWandConnectID) || myBookState == eBookState::Unlocked)
		{
			Message message(eMessageType::ShowInteractButton);
			message.data.myVec2f = myPosition;
			PostMaster::SendMessages(message);
		}
	}
}

void BookOfClassChange::OnCollisionStay(CollisionObject & aOther)
{
	if (aOther.GetOwner()->GetTag() == eGameObjectTag::Player)
	{
		Player* player = dynamic_cast<Player*>(aOther.GetOwner());
		int currentWandIndex = cast_i(player->GetInventory().GetActiveItemIndex());

		if (myHoldedWandIndex != currentWandIndex)
		{
			myHoldedWandIndex = currentWandIndex;
			myHoldedWandConnectID = player->GetInventory().GetActiveItem()->myConnectID;

			if (MatchingConnectID(myHoldedWandConnectID))
			{
				Message message(eMessageType::ShowInteractButton);
				message.data.myVec2f = myPosition;
				PostMaster::SendMessages(message);
			}
			else
			{
				PostMaster::SendMessages(eMessageType::HideInteractButton);
			}
		}
	}
}

void BookOfClassChange::OnCollisionLeave(CollisionObject & aOther)
{
	if (aOther.GetOwner()->GetTag() == eGameObjectTag::Player)
	{
		PostMaster::SendMessages(eMessageType::HideInteractButton);

		myPlayerIsInReach = false;
	}
}

void BookOfClassChange::SetSprites(const char * aPath)
{
	std::string path = aPath;
	std::string fileType = ".dds";
	size_t letterBeforeFiletype = path.rfind(fileType);
	path = std::string(path.begin(), path.begin() + letterBeforeFiletype);

	{
		std::string newPath = path;
		newPath += "Locked.dds";
		myThreeLocksSprite = new Sprite(newPath.c_str(), myPosition, eLayerType::EBackGround);
		myThreeLocksSprite->SetPivot({ 0.5f,0.5f });
	}
	{
		std::string newPath = path;
		newPath += "Unlock1.dds";
		myTwoLocksSprite = new Sprite(newPath.c_str(), myPosition, eLayerType::EBackGround);
		myTwoLocksSprite->SetPivot({ 0.5f,0.5f });
	}
	{
		std::string newPath = path;
		newPath += "Unlock2.dds";
		myOneLockSprite = new Sprite(newPath.c_str(), myPosition, eLayerType::EBackGround);
		myOneLockSprite->SetPivot({ 0.5f,0.5f });
	}
	{
		std::string newPath = path;
		newPath += "Unlock3.dds";
		myUnlockedSprite = new Sprite(newPath.c_str(), myPosition, eLayerType::EBackGround);
		myUnlockedSprite->SetPivot({ 0.5f,0.5f });
	}
	{
		std::string newPath = path;
		newPath += ".dds";
		myNoLockSprite = new Sprite(newPath.c_str(), myPosition, eLayerType::EBackGround);
		myNoLockSprite->SetPivot({ 0.5f,0.5f });
	}
	myDrawable = myThreeLocksSprite;
}

void BookOfClassChange::ReceiveMessage(const Message aMessage)
{
	if (aMessage.myMessageType == eMessageType::PlayerInteract)
	{
		if (myPlayerIsInReach && MatchingConnectID(myHoldedWandConnectID))
		{
			Unlock();

			Message message(eMessageType::KeyUsed);
			message.data.myInt = myHoldedWandIndex;
			PostMaster::SendMessages(message);
		}

		if (myBookState == eBookState::Unlocked)
		{
			myDrawable = myNoLockSprite;
			AudioManager::GetInstance()->PlaySound("openChest");
		}
	}
}

void BookOfClassChange::AttachBoss(Boss * aBoss)
{
	myBoss = aBoss;
}

bool BookOfClassChange::MatchingConnectID(const int aKey) const
{
	return aKey == myFirstConnectID || aKey == mySecondConnectID || aKey == myThirdConnectID;
}

void BookOfClassChange::Unlock()
{
	switch (myBookState)
	{
	case eBookState::ThreeLocks:
		myBookState = eBookState::TwoLocks;
		myDrawable = myTwoLocksSprite;
		break;
	case eBookState::TwoLocks:
		myBookState = eBookState::OneLock;
		myDrawable = myOneLockSprite;
		break;
	case eBookState::OneLock:
		myBookState = eBookState::Unlocked;
		myDrawable = myUnlockedSprite;
		myBoss->Reactivate();
		break;
	}
	AudioManager::GetInstance()->PlaySound("menuButtonClick");
}

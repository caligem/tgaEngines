#pragma once
#include "Activator.h"
#include "GameObject.h"
#include "Subscriber.h"

class Boss;

enum class eBookState
{
	ThreeLocks,
	TwoLocks,
	OneLock,
	Unlocked,
	NoLock
};

class BookOfClassChange : public GameObject, public Subscriber
{
public:
	BookOfClassChange();
	~BookOfClassChange();

	void Init(CollisionManager* aCollisionManager, const CommonUtilities::Vector2f& aPosition, const int aConnectID1, const int aConnectID2, const int aConnectID3);
	void OnCollisionEnter(CollisionObject &aOther) override;
	void OnCollisionStay(CollisionObject &aOther) override;
	void OnCollisionLeave(CollisionObject &aOther) override;
	void SetSprites(const char* aPath);
	void ReceiveMessage(const Message aMessage);
	const eBookState GetBookState() { return myBookState; }
	void AttachBoss(Boss* aBoss);

private:
	bool MatchingConnectID(const int aKey) const;
	void Unlock();
	RenderObject* myThreeLocksSprite;
	RenderObject* myTwoLocksSprite;
	RenderObject* myOneLockSprite;
	RenderObject* myUnlockedSprite;
	RenderObject* myNoLockSprite;

	eBookState myBookState;
	int myFirstConnectID;
	int mySecondConnectID;
	int myThirdConnectID;

	bool myPlayerIsInReach;
	int myHoldedWandConnectID;
	int myHoldedWandIndex;

	Boss* myBoss;
};
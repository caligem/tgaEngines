#include "stdafx.h"
#include "Boss.h"
#include "CollisionObject.h"
#include "Player.h"
#include "ProjectileManager.h"
#include "tga2d/math/common_math.h"
#include "BookOfClassChange.h"
#include "PostMaster.h"

#define MAX_WAIT_TIMER 2.f

Boss::Boss()
{
}


Boss::~Boss()
{
	if (myColliderIndex != -1)
	{
		myCollisionManager->RemoveCollider(myCollisionLayer, myColliderIndex);
		myColliderIndex = -1;
	}
	if (myVisionColliderIndex != -1)
	{
		myCollisionManager->RemoveCollider(eCollisionLayer::Vision, myVisionColliderIndex);
		myVisionColliderIndex = -1;
	}
}

void Boss::Init(const nlohmann::json & aData)
{
	aData;
	myWait = false;
	myWaitTimer = 0.f;
	myHasSentMessage = false;
	myAnimations[static_cast<int>(eState::eIdle)] = Animation("Sprites/enemies/bossBeard_idle.dds");
	myAnimations[static_cast<int>(eState::eAttacking)] = Animation("Sprites/enemies/bossBeard_fire.dds");
	myAnimations[static_cast<int>(eState::eDying)] = Animation("Sprites/enemies/bossBeard_death.dds");

	myAnimations[static_cast<int>(eState::eIdle)].Setup(4, 16, 10);
	myAnimations[static_cast<int>(eState::eAttacking)].Setup(4, 16, 7);
	myAnimations[static_cast<int>(eState::eDying)].Setup(4, 16, 10);
	myAnimations[static_cast<int>(eState::eIdle)].SetLoop(true);
	myAnimations[static_cast<int>(eState::eAttacking)].SetLoop(true);
	myAnimations[static_cast<int>(eState::eDying)].SetLoop(false);

	myCurrentAnimation = &myAnimations[static_cast<int>(eState::eIdle)];
	myAnimations[static_cast<int>(eState::eAttacking)].AddFrameAction(5, [&] { Attack(); });

	myHitFlashTimer.Set(0.25f, Countdown::type::oneshot, [&] {EndHitFlash(); });

	myUnitID = 5;

	myDamage = 2;
	myMaxHealth = 20;
	myCurrentHealth = myMaxHealth;
	mySightRange = 25.f;
	myIsDead = false;

	myAimingDirection = { 0.f, 0.f };

	myColliderRadius = 1.5f;
	AddCircleCollider(myColliderRadius, { 0.f, -1.f });
	myVisionColliderIndex = myCollisionManager->AddCircleCollider(eCollisionLayer::Vision, mySightRange, { 0.f, 0.f });
	myCollisionManager->AttachGameObject(eCollisionLayer::Vision, myVisionColliderIndex, this);

	myAttackSpeed = 3.f;
	myProjectileSpeed = 6.f;

	myTag = eGameObjectTag::CannonBeard;
	myState = eState::eIdle;

	myAttackCounter = 0;

	Inactivate();
}

void Boss::OnUpdate(float aDeltatime)
{
	if (!myIsActivated)
	{
		return;
	}

	SetCurrentAnimation();
	myHitFlashTimer.Update(aDeltatime);

	if (myIsDead)
	{
		if (!myCurrentAnimation->IsFinished())
		{
			myCurrentAnimation->Update(aDeltatime);
		}
		return;
	}

	if (myState == eState::eAttacking)
	{
		if (myCurrentAnimation != &myAnimations[static_cast<int>(eState::eAttacking)])
		{
			myCurrentAnimation = &myAnimations[static_cast<int>(eState::eAttacking)];
		}
	}

	if (myWait)
	{
		if (myWaitTimer > MAX_WAIT_TIMER)
		{
			myWaitTimer = 0.f;
			myWait = false;
		}
		else
		{
			myWaitTimer += aDeltatime;
		}
	}

	eOrientation orientation = GetOrientation();

	if (!myIsDead)
	{
		myCurrentAnimation->SetRowToAnimate(static_cast<int>(orientation));
	}

	myCurrentAnimation->Update(aDeltatime);

	for (int i = static_cast<int>(myMinions.size()) - 1; i >= 0; i--)
	{
		if (myMinions[i]->GetIsDead())
		{
			myMinions.erase(myMinions.begin() + i);
		}
	}

	if (myAttackCounter >= 3)
	{
		float offset = 4.f;
		myAttackCounter = 0;

		myMinions.push_back(myEnemyFactory->AddEnemyBossCall(0, { myPosition.x + offset, myPosition.y }));
		myMinions.push_back(myEnemyFactory->AddEnemyBossCall(0, { myPosition.x - offset, myPosition.y }));
		myMinions.push_back(myEnemyFactory->AddEnemyBossCall(0, { myPosition.x, myPosition.y + offset }));
		myMinions.push_back(myEnemyFactory->AddEnemyBossCall(2, { myPosition.x, myPosition.y - offset }));
	}
}

void Boss::FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer)
{
	if (myIsActivated)
	{
		myCurrentAnimation->SetPosition({ myPosition.x, myPosition.y });
		aRenderBuffer.push_back(myCurrentAnimation);
	}
}

void Boss::AttachManagers(EnemyFactory * aEnemyFactory, ProjectileManager * aProjectileManager)
{
	myEnemyFactory = aEnemyFactory;
	myProjectileManager = aProjectileManager;
}

void Boss::TakeDamage(int aDamage)
{
	if (!myIsDead)
	{
		StartHitFlash();
	}
	AudioManager::GetInstance()->PlayRandomSound("enemyHurt", 4);
	myCurrentHealth -= aDamage;
	if (myCurrentHealth <= 0)
	{
		if (!myHasSentMessage)
		{
			PostMaster::SendMessages(eMessageType::Outro);
			myHasSentMessage = true;
		}
		myIsDead = true;
		myProjectileManager->Reset();
		Inactivate();
		AudioManager::GetInstance()->PlaySound("enemyDeath");
	}
}

void Boss::OnCollisionEnter(CollisionObject & aOther)
{
	if (myIsDead) return;

	if (aOther.GetLayer() == eCollisionLayer::Player)
	{
		if (myWait)
		{
			return;
		}
		Player* player = static_cast<Player*>(aOther.GetOwner());
		if (player->GetPlayerState() == Player::ePlayerState::eDying || player->GetPlayerState() == Player::ePlayerState::eDead)
		{
			myState = eState::eIdle;
			return;
		}
		if (RayTracePlayer(aOther, eRayTraceType::CannonBeard))
		{
			const CommonUtilities::Vector2f playerPosition = aOther.GetPosition() + aOther.GetOffset();
			const CommonUtilities::Vector2f enemyPosition = GetCollider()->GetPosition() + GetCollider()->GetOffset();
			myAimingDirection = playerPosition - enemyPosition;
			myState = eState::eAttacking;
		}
	}
}

void Boss::OnCollisionStay(CollisionObject & aOther)
{
	if (myIsDead) return;

	if (aOther.GetLayer() == eCollisionLayer::Player)
	{
		if (myWait)
		{
			return;
		}
		Player* player = static_cast<Player*>(aOther.GetOwner());
		if (player->GetPlayerState() == Player::ePlayerState::eDying || player->GetPlayerState() == Player::ePlayerState::eDead)
		{
			myState = eState::eIdle;
			return;
		}
		if (RayTracePlayer(aOther, eRayTraceType::CannonBeard))
		{
			const CommonUtilities::Vector2f playerPosition = aOther.GetPosition() + aOther.GetOffset();
			const CommonUtilities::Vector2f enemyPosition = GetCollider()->GetPosition() + GetCollider()->GetOffset();
			myAimingDirection = playerPosition - enemyPosition;
			myState = eState::eAttacking;
		}
		else
		{
			myState = eState::eIdle;
		}
	}
}

void Boss::OnCollisionLeave(CollisionObject & aOther)
{
	if (myIsDead) return;

	if (aOther.GetLayer() == eCollisionLayer::Player)
	{
		myState = eState::eIdle;
	}
}

void Boss::Inactivate()
{
	if (myColliderIndex != -1)
	{
		GetCollider()->SetIsSolid(false);

	}
	if (myVisionColliderIndex != -1)
	{
		myCollisionManager->GetCollisionObject(eCollisionLayer::Vision, myVisionColliderIndex)->SetIsSolid(false);
	}

	myIsActivated = false;
}

void Boss::Reactivate()
{
	if (myColliderIndex != -1)
	{
		GetCollider()->SetIsSolid(true);
	}
	if (myVisionColliderIndex != -1)
	{
		myCollisionManager->GetCollisionObject(eCollisionLayer::Vision, myVisionColliderIndex)->SetIsSolid(true);
	}

	myIsActivated = true;
}


Boss::eOrientation Boss::GetOrientation()
{
	const float rotation = atan2(myAimingDirection.y, myAimingDirection.x);
	eOrientation orientation;

	if (rotation > 0.75f && rotation < 2.5f)
	{
		orientation = eOrientation::eDown;
	}
	else if (rotation > -2.5f && rotation < -0.75f)
	{
		orientation = eOrientation::eUp;
	}
	else if (rotation < -2.5f || rotation > 2.5f) // Yes, the || is correct.
	{
		orientation = eOrientation::eLeft;
	}
	else //if (rotation > -0.75f && rotation < 0.75f)
	{
		orientation = eOrientation::eRight;
	}

	return orientation;
}

void Boss::SetCurrentAnimation()
{
	if (myIsDead || myState == eState::eDying)
	{
		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eDying)];
		myCurrentAnimation->SetRowToAnimate(static_cast<int>(GetOrientation()));
	}
	else if (myState == eState::eAttacking)
	{
		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eAttacking)];
	}
	else
	{
		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eIdle)];
	}
}

void Boss::Attack()
{
	if (myMinions.size() <= 0)
	{
		myAttackCounter++;
	}

	if (myAttackCounter < 3)
	{
		CommonUtilities::Vector2f position = GetCollider()->GetPosition() + GetCollider()->GetOffset();
		position.y -= 1.2f;

		float degIncrease = 5.f;
		for (float bulletIndex = -60.f; bulletIndex < 60.f; bulletIndex += degIncrease)
		{
			float rad = Tga2D::DegToRad(bulletIndex);
			const CommonUtilities::Vector2f myLeftVector = { 1.f, 0.f };
			const CommonUtilities::Vector2f normalizedAimingDirection = myAimingDirection.GetNormalized();
			float cosAngle = myLeftVector.Dot(normalizedAimingDirection);
			const float angle = acosf((cosAngle));
			CommonUtilities::Vector2f newDirection;
			newDirection.x = cosf(rad + angle);
		
			if (normalizedAimingDirection.y < 0)
			{
				newDirection.y = -sinf(rad + angle);
			}
			else
			{
				newDirection.y = sinf(rad + angle);
			}

			myProjectileManager->CreateBullet(myProjectileSpeed, newDirection, position, myDamage, "Sprites/enemies/bossBullet.dds");
		}
		AudioManager::GetInstance()->PlayRandomSound("enemyShoot", 2);
	}

	myWait = true;
	myState = eState::eIdle;
}

void Boss::StartHitFlash()
{
	myHitFlashTimer.Reset();
	myHitFlashTimer.Start();
	for (int i = 0; i < myAnimations.size(); ++i)
	{
		myAnimations[i].SetColor({ 1.0f, 0.3f, 0.3f, 0.9f });
	}
}

void Boss::EndHitFlash()
{
	for (int i = 0; i < myAnimations.size(); ++i)
	{
		myAnimations[i].SetColor({ 1.0f, 1.0f, 1.0f, 1.0f });
	}
}

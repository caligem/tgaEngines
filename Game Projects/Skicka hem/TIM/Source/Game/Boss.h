#pragma once
#include "Enemy.h"
#include "Animation.h"
#include "EnemyFactory.h"
#include "Countdown.h"

class BookOfClassChange;
class ProjectileManager;

class Boss : public Enemy
{
public:
	Boss();
	~Boss();
	
	void Init(const nlohmann::json& aData) override;
	void OnUpdate(float aDeltatime) override;
	void FillRenderBuffer(std::vector<RenderObject*> &aRenderBuffer);
	void AttachManagers(EnemyFactory* aEnemyFactory, ProjectileManager* aProjectileManager);

	void TakeDamage(int aDamage) override;

	enum class eState
	{
		eIdle,
		eAttacking,
		eDying,
		count
	};

	enum class eOrientation
	{
		eDown,
		eLeft,
		eUp,
		eRight,
		COUNT
	};

	virtual void OnCollisionEnter(CollisionObject &aOther) override;
	virtual void OnCollisionStay(CollisionObject &aOther) override;
	virtual void OnCollisionLeave(CollisionObject &aOther) override;

	void Inactivate() override;
	void Reactivate() override;

private:
	bool myHasSentMessage;
	bool myWait;
	float myAttackSpeed;
	float myProjectileSpeed;

	bool myIsActivated;

	float myWaitTimer;
	int myAttackCounter;

	eState myState;

	CommonUtilities::Vector2f myAimingDirection;

	eOrientation GetOrientation();
	void SetCurrentAnimation();
	void Attack();
	void StartHitFlash() override;
	void EndHitFlash();
	std::array<Animation, static_cast<int>(eState::count)> myAnimations;
	Animation* myCurrentAnimation;

	EnemyFactory* myEnemyFactory;
	ProjectileManager* myProjectileManager;

	Countdown myHitFlashTimer;
	std::vector<Enemy*> myMinions;
	BookOfClassChange* myBook;
};


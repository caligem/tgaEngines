#include "stdafx.h"
#include "BoxCollider.h"
#include "CircleCollider.h"
#include "LineCollider.h"
#include "tga2d\primitives\line_primitive.h"

BoxCollider::BoxCollider(float aHeight, float aWidth, const CommonUtilities::Vector2f & aOffset)
{
	myHeight = aHeight;
	myWidth = aWidth;
	myOffset = aOffset;
}

BoxCollider::~BoxCollider()
{
}

bool BoxCollider::TestCollision(CollisionObject * aCollisionObject)
{
	return aCollisionObject->TestCollision(this);
}

bool BoxCollider::TestCollision(BoxCollider * aBoxCollider)
{
	return aBoxCollider->BoxVSBoxCollision(myPosition + myOffset, myWidth, myHeight, aBoxCollider->GetPosition() + aBoxCollider->GetOffset(), aBoxCollider->GetWidth(), aBoxCollider->GetHeight());
}

bool BoxCollider::TestCollision(CircleCollider * aCircleCollider)
{
	return aCircleCollider->CircleVSBoxCollision(aCircleCollider->GetPosition() + aCircleCollider->GetOffset(), aCircleCollider->GetRadius(), myPosition + myOffset, myWidth, myHeight);
}

bool BoxCollider::TestCollision(LineCollider * aLineCollider)
{
	return aLineCollider->BoxVSLineCollision(myPosition + myOffset, myWidth, myHeight, aLineCollider->GetPosition() + aLineCollider->GetOffset(), aLineCollider->GetEndPosition());
}

void BoxCollider::DebugDraw(const CommonUtilities::Vector2f& aCameraPosition)
{
	if (!myIsInsideBoundaries)
	{
		return;
	}

	const CommonUtilities::Vector2f origin = myPosition + myOffset - aCameraPosition;
	const CommonUtilities::Vector2f originInScreenSpace = SpaceConverter::TileSpaceToScreenSpace(origin) + CommonUtilities::Vector2f(0.5f, 0.5f);
	const float widthInScreenSpace = SpaceConverter::TileXToScreen(myWidth);
	const float heightInScreenSpace = SpaceConverter::TileYToScreen(myHeight);

	CommonUtilities::Vector2f topLeft = originInScreenSpace;
	topLeft.x -= widthInScreenSpace / 2.0f;
	topLeft.y -= heightInScreenSpace / 2.0f;

	CommonUtilities::Vector2f bottomRight = topLeft;
	bottomRight.x += widthInScreenSpace;
	bottomRight.y += heightInScreenSpace;

	Tga2D::CLinePrimitive AB;
	AB.SetFrom(topLeft.x, topLeft.y);
	AB.SetTo(bottomRight.x, topLeft.y);
	AB.myColor = { 1.0f, 0.0f, 0.0f, 1.0f };

	Tga2D::CLinePrimitive BC;
	BC.SetFrom(bottomRight.x, topLeft.y);
	BC.SetTo(bottomRight.x, bottomRight.y);
	BC.myColor = { 1.0f, 0.0f, 0.0f, 1.0f };

	Tga2D::CLinePrimitive CD;
	CD.SetFrom(bottomRight.x, bottomRight.y);
	CD.SetTo(topLeft.x, bottomRight.y);
	CD.myColor = { 1.0f, 0.0f, 0.0f, 1.0f };

	Tga2D::CLinePrimitive DA;
	DA.SetFrom(topLeft.x, bottomRight.y);
	DA.SetTo(topLeft.x, topLeft.y);
	DA.myColor = { 1.0f, 0.0f, 0.0f, 1.0f };

	Tga2D::CLinePrimitive DB;
	DB.SetFrom(topLeft.x, bottomRight.y);
	DB.SetTo(bottomRight.x, topLeft.y);
	DB.myColor = { 1.0f, 0.0f, 0.0f, 1.0f };

	AB.Render();
	BC.Render();
	CD.Render();
	DA.Render();
	DB.Render();
}

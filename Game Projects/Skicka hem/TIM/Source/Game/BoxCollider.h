#pragma once
#include "CollisionObject.h"
#include "Vector.h"

class BoxCollider :	public CollisionObject
{
public:
	BoxCollider(float aHeight, float aWidth, const CommonUtilities::Vector2f & aOffset);
	~BoxCollider();

	bool TestCollision(CollisionObject* aCollisionObject) override;
	bool TestCollision(BoxCollider* aBoxCollider) override;
	bool TestCollision(CircleCollider* aCircleCollider) override;
	bool TestCollision(LineCollider* aLineCollider) override;

	void DebugDraw(const CommonUtilities::Vector2f& aCameraPosition) override;

	inline const float GetHeight() const { return myHeight; }
	inline const float GetWidth() const { return myWidth; }
private:
	float myHeight;
	float myWidth;
};


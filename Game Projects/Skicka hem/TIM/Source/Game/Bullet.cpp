#include "stdafx.h"
#include "Bullet.h"
#include "CollisionObject.h"
#include "AudioManager.h"

Bullet::Bullet()
{
}


Bullet::~Bullet()
{
}

void Bullet::Init()
{
	myTag = eGameObjectTag::Projectile;
	myDamage = 0;
	myColliderRadius = 0.3f;
	myProjectileSpeed = 0.f;
	myDirection = { 0.f, 0.f };

	AddCircleCollider(myColliderRadius, { 0.f, 0.f });
}

void Bullet::OnUpdate(float aDeltaTime)
{
	if (!myShouldRemove)
	{
		const CommonUtilities::Vector2f distance = myDirection * myProjectileSpeed * aDeltaTime;
		myDistanceTraveled += distance.Length();
		myPosition += distance;

		myBulletAnimation.SetPosition(myPosition);
		myBulletAnimation.Update(aDeltaTime);

		if (myDistanceTraveled >= 120.f)
		{
			Inactivate();
		}
	}
}

void Bullet::FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer)
{
	if (!myShouldRemove)
	{
		aRenderBuffer.push_back(&myBulletAnimation);
	}
}

void Bullet::OnCollisionEnter(CollisionObject & aOther)
{
	if (aOther.GetLayer() == eCollisionLayer::Terrain || aOther.GetLayer() == eCollisionLayer::Player)
	{
		Inactivate();
		AudioManager::GetInstance()->PlayRandomSound("cannonShotImpact", 2);
	}
}

void Bullet::OnCollisionStay(CollisionObject & aOther)
{
	aOther;
}

void Bullet::OnCollisionLeave(CollisionObject & aOther)
{
	aOther;
}

void Bullet::Inactivate()
{
	if (myColliderIndex != -1)
	{
		GetCollider()->SetIsSolid(false);
	}

	myShouldRemove = true;
	myDistanceTraveled = 0.f;
}

void Bullet::Reactivate()
{
	if (myColliderIndex != -1)
	{
		GetCollider()->SetIsSolid(true);
	}

	myBulletAnimation.Reset();
	
	myShouldRemove = false;

	myDistanceTraveled = 0.f;
}

void Bullet::Reset(const float aProjectileSpeed, const CommonUtilities::Vector2f & aDirection, const char * aPath, const int aDamage)
{
	myDamage = aDamage;
	myProjectileSpeed = aProjectileSpeed;
	myDirection = aDirection.GetNormalized();
	myBulletAnimation.Init(aPath);
	myBulletAnimation.Setup(1, 4, 4);
	myBulletAnimation.SetAnimationSpeed(2.5f);
	const float rotation = atan2f(myDirection.y, myDirection.x);
	myBulletAnimation.SetRotation(rotation);
	myBulletAnimation.SetPivot({ 0.5f, 0.5f });
	myBulletAnimation.SetLayerType(eLayerType::EForeground);

	Reactivate();
}

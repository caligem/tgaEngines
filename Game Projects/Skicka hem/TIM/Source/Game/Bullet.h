#pragma once
#include "GameObject.h"
#include "Animation.h"

class ExplosionManager;

class Bullet : public GameObject
{
public:
	Bullet();
	~Bullet();
	void Init();
	void OnUpdate(float aDeltaTime) override;

	void FillRenderBuffer(std::vector<RenderObject*> &aRenderBuffer);

	virtual void OnCollisionEnter(CollisionObject &aOther) override;
	virtual void OnCollisionStay(CollisionObject &aOther) override;
	virtual void OnCollisionLeave(CollisionObject &aOther) override;

	int GetDamage() { return myDamage; }

	void Inactivate() override;
	void Reactivate() override;
	void Reset(const float aProjectileSpeed, const CommonUtilities::Vector2f & aDirection, const char* aPath, const int aDamage);

private:
	Animation myBulletAnimation;

	int myDamage;
	float myDistanceTraveled;
	float myColliderRadius;
	float myProjectileSpeed;
	CommonUtilities::Vector2f myDirection;
};


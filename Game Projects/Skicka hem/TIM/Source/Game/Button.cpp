#include "stdafx.h"
#include "Button.h"
#include "tga2d\engine.h"
#include "tga2d\primitives\line_primitive.h"
#include "InputManager.h"
#include "Timer.h"
#include "AudioManager.h"
							  
Button::Button()
	: mySprite(nullptr)
	, myHoveredSprite(nullptr)
	, myCurrentSprite(nullptr)
	, myInputManager(nullptr)
	, myHitboxOffsetX(0)
	, myHitboxOffsetY(0)
	, myPosition({0.f, 0.f})
{
}


Button::~Button()
{
	myCurrentSprite = nullptr;
}

void Button::Init(std::function<void()> aFunction, const char * aPath, CommonUtilities::Vector2f aPosition, CommonUtilities::InputManager* aInputManager, bool aIsClickable, bool aIsDraggable)
{
	myFunction = aFunction;
	myInputManager = aInputManager;
	myIsClickable = aIsClickable;
	myIsDraggable = aIsDraggable;

	myPosition = aPosition;
	InitSprite(aPath);
	InitHitbox();

	SetSprite(eSpriteType::Default);
}

void Button::FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer)
{
	if (myCurrentSprite != nullptr)
	{
		aRenderBuffer.push_back(myCurrentSprite);
	}
}

void Button::AttachHoveredSprite(const char* aPath)
{
	myHoveredSprite.Init(aPath, myPosition);
	myHoveredSprite.SetPivot({ 0.5f, 0.5f });
}

void Button::RenderDebugLines()
{
	Tga2D::CLinePrimitive line;
	line.SetFrom(myMinPosition.x, myMinPosition.y);
	line.SetTo(myMinPosition.x, myMaxPosition.y);
	line.Render();
	line.SetFrom(myMinPosition.x, myMinPosition.y);
	line.SetTo(myMaxPosition.x, myMinPosition.y);
	line.Render();
	line.SetFrom(myMinPosition.x, myMaxPosition.y);
	line.SetTo(myMaxPosition.x, myMaxPosition.y);
	line.Render();
	line.SetFrom(myMaxPosition.x, myMinPosition.y);
	line.SetTo(myMaxPosition.x, myMaxPosition.y);
	line.Render();
}

void Button::SetHitboxOffsetX(float aOffset)
{
	myMinPosition.x += aOffset;
	myMaxPosition.x -= aOffset;
}

void Button::SetHitboxOffsetY(float aOffset)
{
	myMinPosition.y += aOffset;
	myMaxPosition.y -= aOffset;
}

void Button::FlipAnimation()
{
	//myAnimation->SetInversedX(true);
	//if hoverd sprite != nullptr, rotate aswell
}

void Button::SetSprite(const eSpriteType aSpriteType)
{
	if (aSpriteType == eSpriteType::Default)
	{
		myCurrentSprite = &mySprite;
	}
	else if (aSpriteType == eSpriteType::Hovered)
	{
		myCurrentSprite = &myHoveredSprite;
	}
}

void Button::FadeButton()
{
	mySprite.SetColor({0.4f, 0.4f, 0.4f, 1.0f});
}

void Button::UnfadeButton()
{
	mySprite.SetColor({ 1.0f, 1.0f, 1.0f, 1.0f });
}

void Button::SetPosition(const CommonUtilities::Vector2f & aPosition)
{
	myPosition = aPosition;
	mySprite.SetPosition(myPosition);
	myHoveredSprite.SetPosition(myPosition);
	InitHitbox();
}

const CommonUtilities::Vector2f Button::GetPosition()
{
	return { mySprite.GetPosition().x, mySprite.GetPosition().y };
}

void Button::ConnectButton(Button* aButton, eConnectSide aConnectSide)
{
	if (aConnectSide == eConnectSide::Left)
	{
		myConnectedButtons.myLeftButton = aButton;
		aButton->myConnectedButtons.myRightButton = this;
	}
	else if (aConnectSide == eConnectSide::Right)
	{
		myConnectedButtons.myRightButton = aButton;
		aButton->myConnectedButtons.myLeftButton = this;
	}
	else if (aConnectSide == eConnectSide::Down)
	{
		myConnectedButtons.myDownButton = aButton;
		aButton->myConnectedButtons.myUpButton = this;
	}
	else if (aConnectSide == eConnectSide::Up)
	{
		myConnectedButtons.myUpButton = aButton;
		aButton->myConnectedButtons.myDownButton = this;
	}
}

void Button::InitHitbox()
{
	float screenX = static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().x);
	float screenY = static_cast<float>(Tga2D::CEngine::GetInstance()->GetWindowSize().y);
		
	float imageSizeX = static_cast<float>(mySprite.GetImageSize().x);
	float imageSizeY = static_cast<float>(mySprite.GetImageSize().y);

	myMinPosition.x = mySprite.GetPosition().x - ((imageSizeX / screenX) * 2);
	myMaxPosition.x = mySprite.GetPosition().x + ((imageSizeX / screenX) * 2);
	myMinPosition.y = mySprite.GetPosition().y - ((imageSizeY / screenY) * 2);
	myMaxPosition.y = mySprite.GetPosition().y + ((imageSizeY / screenY) * 2);
}

void Button::InitSprite(const char * aPath)
{
	mySprite.Init(aPath, { myPosition.x, myPosition.y});
	mySprite.SetPivot({ 0.5f, 0.5f });
}

void Button::OnClick()
{
	AudioManager::GetInstance()->PlaySound("menuButtonClick");
	myFunction();
}

bool Button::IsMouseInside()
{
	CommonUtilities::Vector2<int> myMousePosInt;
	myInputManager->GetMousePosition(myMousePosInt.x, myMousePosInt.y);

	CommonUtilities::Vector2f myMousePos;
	myMousePos.x = static_cast<float>(myMousePosInt.x);
	myMousePos.y = static_cast<float>(myMousePosInt.y);
	int screenX = Tga2D::CEngine::GetInstance()->GetWindowSize().x;
	int screenY = Tga2D::CEngine::GetInstance()->GetWindowSize().y;

	myMousePos.x /= screenX;
	myMousePos.y /= screenY;

	if (myMousePos.x > myMinPosition.x && myMousePos.x < myMaxPosition.x
		&& myMousePos.y < myMaxPosition.y && myMousePos.y > myMinPosition.y)
	{
		return true;
	}
	return false;
}

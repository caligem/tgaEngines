#pragma once
#include <functional>
#include "Countdown.h"
#include "Vector.h"
#include <vector>
//#include "tga2d\sprite\sprite.h"
#include "Sprite.h"

namespace CommonUtilities
{
	class InputManager;
}

class Animation;
class Drawable;


class Button
{
public:
	Button();
	~Button();

	enum class eSpriteType
	{
		Default,
		Hovered
	};

	enum class eConnectSide
	{
		Left,
		Right,
		Down,
		Up
	};

	virtual void Init(std::function<void()> aFunction, const char* aPath, CommonUtilities::Vector2f aPosition, CommonUtilities::InputManager* aInputManager, bool aIsClickable = true, bool aIsDraggable = false);
	virtual void FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer);

	void AttachHoveredSprite(const char* aPath);

	void RenderDebugLines();
	void SetHitboxOffsetX(float aOffset);
	void SetHitboxOffsetY(float aOffset);

	void FlipAnimation();
	void SetSprite(const eSpriteType aSpriteType);
	void FadeButton();
	void UnfadeButton();

	inline void SetClickable(bool aIsClickable) { myIsClickable = aIsClickable; }
	inline const bool GetIsClickable() const { return myIsClickable; }

	inline void SetDraggable(bool aIsDraggable) { myIsDraggable = aIsDraggable; }
	inline const bool GetIsDraggable() const { return myIsDraggable; }

	bool IsMouseInside();
	virtual void OnClick();

	void SetPosition(const CommonUtilities::Vector2f &aPosition);
	const CommonUtilities::Vector2f GetPosition();
	
	void ConnectButton(Button* aButton, eConnectSide aConnectSide);

	struct ConnectedButtons
	{
		Button* myLeftButton = nullptr;
		Button* myRightButton = nullptr;
		Button* myDownButton = nullptr;
		Button* myUpButton = nullptr;
	} myConnectedButtons;


protected:
	bool myIsClickable;
	bool myIsDraggable;

	Sprite* myCurrentSprite;

	Sprite mySprite;
	Sprite myHoveredSprite;

	CommonUtilities::Vector2f myPosition;
	CommonUtilities::Vector2f myMaxPosition;
	CommonUtilities::Vector2f myMinPosition;

	float myHitboxOffsetX;
	float myHitboxOffsetY;

	CommonUtilities::InputManager* myInputManager;

	std::function<void()> myFunction;
	void InitHitbox();
	void InitSprite(const char* aPath);
};


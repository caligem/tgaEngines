#include "stdafx.h"
#include "Camera.h"
#include "RenderObject.h"
#include "Tile.h"
#include "TiledMap.h"
#include "Sprite.h"
#include "GameObject.h"
#include "tga2d/sprite/sprite_batch.h"
#include <algorithm>

#define CameraXSize 60.f
#define CameraYSize 34.f

Camera::Camera()
{
	myPosition = { 30.f, 17.f };
	myParentPos = nullptr;
	mySnapToParent = true;
}


Camera::~Camera()
{
	myThisFramesMap = nullptr;
}

void Camera::Update(float aDeltaTime)
{
	if (mySnapToParent)
	{
		return;
	}
	else
	{
		float speed = 4.f;
		CU::Vector2f distToParent(*myParentPos - myPosition);
		float verySmallDiff = 0.05f;
		if (distToParent.Length() < verySmallDiff)
		{
			SnapToParent();
		}
		else
		{
			myPosition += distToParent * aDeltaTime * speed;
		}
	}
}

void Camera::RenderMapUnderPlayer(TiledMap& aMap)
{
	myThisFramesMap = &aMap;

	std::vector<Tga2D::CSpriteBatch*>& TileMapSpriteBatches = aMap.GetTileMapBatches();
	CU::Vector2f firstCorner;
	CU::Vector2f secondCorner;
	CU::Vector2f objectPosInCameraSpace;
	for (const auto& tileLayer : aMap.GetTiles())
	{
		if (tileLayer.size() > 0)
		{
			for (auto& tile : tileLayer)
			{
				objectPosInCameraSpace = tile.GetPosition() - GetPos();
				if (objectPosInCameraSpace.x >= (CameraXSize + 2.f) / -2.f && // + 2 so that the camera renders tiles outside of the screen.
					objectPosInCameraSpace.x < (CameraXSize + 2.f) / 2.f &&
					objectPosInCameraSpace.y >= (CameraYSize + 2.f) / -2.f &&
					objectPosInCameraSpace.y < (CameraYSize + 2.f) / 2.f 
					)
				{
					for (int i = 0; i < TileMapSpriteBatches.size(); ++i)
					{
						if (TileMapSpriteBatches[i]->GetImagePath() == aMap.GetSpriteSheet(tile.GetTilesetIndex()).GetSprite()->GetImagePath())
						{
							Tga2D::CSprite* SpriteToModify = TileMapSpriteBatches[i]->GetNext();
							

							firstCorner = { tile.GetTextureRect().x,  tile.GetTextureRect().y };
							secondCorner = { tile.GetTextureRect().z, tile.GetTextureRect().w };

							float dx = 0.03f * (secondCorner.x - firstCorner.x);  // These are to fix line tearing between tiles. The multiplier should be as little as possible 
							float dy = 0.03f * (secondCorner.y - firstCorner.y);  // but still fix the tearing. Should be updated when we get graphics.

							SpriteToModify->SetTextureRect(firstCorner.x + dx / 2.f, firstCorner.y + dy / 2.f, secondCorner.x - dx / 2.f, secondCorner.y - dy / 2.f);
							SpriteToModify->SetSizeRelativeToScreen({ 32 / 1080.f, 32 / 1080.f });
							CommonUtilities::Vector2f aCamPos = SpaceConverter::TileSpaceToScreenSpace(objectPosInCameraSpace) + CU::Vector2f(0.5f, 0.5f);
							SpriteToModify->RenderAt({ aCamPos.x, aCamPos.y });
							SpriteToModify->SetShouldRender(true);

							
							i = INT_MAX;
						}
					}
					//spriteSheet = aMap.GetSpriteSheet(tile.GetTilesetIndex());
					
				}
			}					
		}
	}
	for (int i = 0; i < TileMapSpriteBatches.size(); ++i)
	{
		Tga2D::CSprite* temp = TileMapSpriteBatches[i]->GetNext();
		while ( temp != nullptr)
		{
			temp->SetShouldRender(false);
			temp = TileMapSpriteBatches[i]->GetNext();
		}
	}

	for (int i = 0; i < TileMapSpriteBatches.size(); ++i)
	{
		TileMapSpriteBatches[i]->Render();
	}
}

void Camera::RenderMapOverPlayer()
{
	std::vector<Tga2D::CSpriteBatch*>& TileMapSpriteBatches = myThisFramesMap->GetTileMapBatches();
	CU::Vector2f firstCorner;
	CU::Vector2f secondCorner;
	CU::Vector2f objectPosInCameraSpace;

	for (const auto& tileLayer : myThisFramesMap->GetTilesOverPlayer())
	{
		if (tileLayer.size() > 0 )
		{
			for (auto& tile : tileLayer)
			{
				objectPosInCameraSpace = tile.GetPosition() - GetPos();
				if (objectPosInCameraSpace.x >= (CameraXSize + 2.f) / -2.f && // + 2 so that the camera renders tiles outside of the screen.
					objectPosInCameraSpace.x < (CameraXSize + 2.f) / 2.f &&
					objectPosInCameraSpace.y >= (CameraYSize + 2.f) / -2.f &&
					objectPosInCameraSpace.y < (CameraYSize + 2.f) / 2.f
					)
				{
					for (int i = 0; i < TileMapSpriteBatches.size(); ++i)
					{
						if (TileMapSpriteBatches[i]->GetImagePath() == myThisFramesMap->GetSpriteSheet(tile.GetTilesetIndex()).GetSprite()->GetImagePath())
						{
							Tga2D::CSprite* SpriteToModify = TileMapSpriteBatches[i]->GetNext();


							firstCorner = { tile.GetTextureRect().x,  tile.GetTextureRect().y };
							secondCorner = { tile.GetTextureRect().z, tile.GetTextureRect().w };

							float dx = 0.03f * (secondCorner.x - firstCorner.x);  // These are to fix line tearing between tiles. The multiplier should be as little as possible 
							float dy = 0.03f * (secondCorner.y - firstCorner.y);  // but still fix the tearing. Should be updated when we get graphics.

							SpriteToModify->SetTextureRect(firstCorner.x + dx / 2.f, firstCorner.y + dy / 2.f, secondCorner.x - dx / 2.f, secondCorner.y - dy / 2.f);
							SpriteToModify->SetSizeRelativeToScreen({ 32 / 1080.f, 32 / 1080.f });
							CommonUtilities::Vector2f aCamPos = SpaceConverter::TileSpaceToScreenSpace(objectPosInCameraSpace) + CU::Vector2f(0.5f, 0.5f);
							SpriteToModify->RenderAt({ aCamPos.x, aCamPos.y });
							SpriteToModify->SetShouldRender(true);


							i = INT_MAX;
						}
					}
				}
			}
		}
	}
	for (int i = 0; i < TileMapSpriteBatches.size(); ++i)
	{
		Tga2D::CSprite* temp = TileMapSpriteBatches[i]->GetNext();
		while (temp != nullptr)
		{
			temp->SetShouldRender(false);
			temp = TileMapSpriteBatches[i]->GetNext();
		}
	}

	for (int i = 0; i < TileMapSpriteBatches.size(); ++i)
	{
		TileMapSpriteBatches[i]->Render();
	}
}

bool RenderOrder(RenderObject* lhs, RenderObject* rhs)
{
	float lhsY = lhs->GetPosition().y + (1.f - lhs->GetPivot().y) * SpaceConverter::PixelYToTile(lhs->GetSeenOnScreenPixelSize().y);
	float rhsY = rhs->GetPosition().y + (1.f - rhs->GetPivot().y) * SpaceConverter::PixelYToTile(rhs->GetSeenOnScreenPixelSize().y);

	int lhsZ = static_cast<int>(lhs->GetType());
	int rhsZ = static_cast<int>(rhs->GetType());

	return (lhsZ < rhsZ) || (!(rhsZ < lhsZ) && (lhsY < rhsY));
}

void Camera::RenderBuffer(const std::vector<RenderObject*>& aRenderBuffer)
{
	std::vector<RenderObject*> listOfStuffToRender = aRenderBuffer;
	std::sort(listOfStuffToRender.begin(), listOfStuffToRender.end(), RenderOrder);

	CU::Vector2f objectPosInCameraSpace;

	for (auto renderObject = listOfStuffToRender.begin(); renderObject != listOfStuffToRender.end(); ++renderObject)
	{
		if ((*renderObject)->GetType() == eLayerType::EUserInterface)
		{
			(*renderObject)->Render();
			continue;
		}

		objectPosInCameraSpace = (*renderObject)->GetPosition() - GetPos();
		if (objectPosInCameraSpace.x + (1.f - (*renderObject)->GetPivot().x) * SpaceConverter::PixelXToTile((*renderObject)->GetSeenOnScreenPixelSize().x) >= CameraXSize / -2.f &&
			objectPosInCameraSpace.x - (1.f - (*renderObject)->GetPivot().x) * SpaceConverter::PixelXToTile((*renderObject)->GetSeenOnScreenPixelSize().x) < CameraXSize / 2.f &&
			objectPosInCameraSpace.y + (1.f - (*renderObject)->GetPivot().y) * SpaceConverter::PixelYToTile((*renderObject)->GetSeenOnScreenPixelSize().y) >= CameraYSize / -2.f &&
			objectPosInCameraSpace.y - (1.f - (*renderObject)->GetPivot().y) * SpaceConverter::PixelYToTile((*renderObject)->GetSeenOnScreenPixelSize().y) < CameraYSize / 2.f
			)
		{
			(*renderObject)->RenderAt(SpaceConverter::TileSpaceToScreenSpace(objectPosInCameraSpace) + CU::Vector2f(0.5f, 0.5f));
		}
	}
}

void Camera::SetParentPos(const GameObject* aGameObject)
{
	assert(aGameObject != nullptr && "The camera got a nullptr as parent");
	myParentPos = &aGameObject->GetPosition();
}

const CU::Vector2f& Camera::GetPos() const
{
	if (mySnapToParent && myParentPos != nullptr)
	{
		return *myParentPos;
	}
	else
	{
		return myPosition;
	}
}

void Camera::SnapToParent()
{
	mySnapToParent = true;
}

void Camera::ReleaseFromParent()
{
	mySnapToParent = false;
	myPosition = *myParentPos;
}

void Camera::ReceiveMessage(const Message aMessage)
{
	if (aMessage.myMessageType == eMessageType::PlayerTeleport)
	{
		ReleaseFromParent();
	}
}
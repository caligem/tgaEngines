#pragma once
#include <Vector.h>
#include <vector>
#include "Subscriber.h"

class GameObject;
class TiledMap;
class Tga2D::CSpriteBatch;

class RenderObject;
class Camera : public Subscriber
{
public:
	Camera();
	~Camera();
	void Update(float aDeltaTime);

	void RenderMapUnderPlayer(TiledMap& aMap);
	void RenderMapOverPlayer();
	void RenderBuffer(const std::vector<RenderObject*>& aRenderBuffer);
	void SetParentPos(const GameObject* aGameObject);
	const CU::Vector2f& GetPos() const;
	void SnapToParent();
	void ReleaseFromParent();
	void ReceiveMessage(const Message aMessage) override;
private:

	CU::Vector2f myPosition;
	bool mySnapToParent;
	const CU::Vector2f* myParentPos;
	TiledMap* myThisFramesMap;
};


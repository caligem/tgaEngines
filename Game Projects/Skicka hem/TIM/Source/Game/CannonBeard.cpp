#include "stdafx.h"
#include "CannonBeard.h"
#include "CollisionObject.h"
#include "ProjectileManager.h"
#include "Player.h"

CannonBeard::CannonBeard()
{
	myVisionColliderIndex = -1;
}

CannonBeard::~CannonBeard()
{
	if (myColliderIndex != -1)
	{
		myCollisionManager->RemoveCollider(myCollisionLayer, myColliderIndex);
		myColliderIndex = -1;
	}
	if (myVisionColliderIndex != -1)
	{
		myCollisionManager->RemoveCollider(eCollisionLayer::Vision, myVisionColliderIndex);
		myVisionColliderIndex = -1;
	}
}

void CannonBeard::Init(const nlohmann::json & aData, ProjectileManager& aProjectileManager)
{
	myProjectileManager = &aProjectileManager;
	// Save variables from json
	ParseJSONFile(aData);
	myAnimations[static_cast<int>(eState::eIdle)].Setup(4, 16, 10);
	myAnimations[static_cast<int>(eState::eAttacking)].Setup(4, 16, 7);
	myAnimations[static_cast<int>(eState::eDying)].Setup(4, 16, 10);
	myAnimations[static_cast<int>(eState::eIdle)].SetLoop(true);
	myAnimations[static_cast<int>(eState::eAttacking)].SetLoop(true);
	myAnimations[static_cast<int>(eState::eDying)].SetLoop(false);

	myCurrentAnimation = &myAnimations[static_cast<int>(eState::eIdle)];
	myHitFlashTimer.Set(0.25f, Countdown::type::oneshot, [&] {EndHitFlash(); });

	myAnimations[static_cast<int>(eState::eAttacking)].AddFrameAction(5, [&] { Attack(); });
}

void CannonBeard::OnUpdate(float aDeltatime)
{
	SetCurrentAnimation();
	myHitFlashTimer.Update(aDeltatime);

	if (myIsDead)
	{
		if (!myCurrentAnimation->IsFinished())
		{
			myCurrentAnimation->Update(aDeltatime);
		}
		return;
	}

	if (myState == eState::eAttacking)
	{
		if (myCurrentAnimation != &myAnimations[static_cast<int>(eState::eAttacking)])
		{
			myCurrentAnimation = &myAnimations[static_cast<int>(eState::eAttacking)];
		}
	}
	eOrientation orientation = GetOrientation();

	if (!myIsDead)
	{
		myCurrentAnimation->SetRowToAnimate(static_cast<int>(orientation));
	}
	myCurrentAnimation->Update(aDeltatime);
}

void CannonBeard::ParseJSONFile(const nlohmann::json& aJSONData)
{
	// If / when we want debugging info, add else on the following if-statements

	if (aJSONData.find("spriteIdle") != aJSONData.end())
		myAnimations[static_cast<int>(eState::eIdle)] = Animation(aJSONData["spriteIdle"].get<std::string>().c_str());
	if (aJSONData.find("spriteFire") != aJSONData.end())
		myAnimations[static_cast<int>(eState::eAttacking)] = Animation(aJSONData["spriteFire"].get<std::string>().c_str());
	if (aJSONData.find("spriteDeath") != aJSONData.end())
		myAnimations[static_cast<int>(eState::eDying)] = Animation(aJSONData["spriteDeath"].get<std::string>().c_str());

	myCurrentAnimation = &myAnimations[static_cast<int>(eState::eIdle)];


	if (aJSONData.find("attackSpeed") != aJSONData.end())
	{
		myAttackSpeed = aJSONData["attackSpeed"].get<float>();
	}
	if (aJSONData.find("projectileSpeed") != aJSONData.end())
	{
		myProjectileSpeed = aJSONData["projectileSpeed"].get<float>();
	}
	if (aJSONData.find("damage") != aJSONData.end())
	{
		myDamage = aJSONData["damage"].get<int>();
	}
	if (aJSONData.find("maxHealth") != aJSONData.end())
	{
		myMaxHealth = aJSONData["maxHealth"].get<int>();
		myCurrentHealth = myMaxHealth;
	}
	if (aJSONData.find("sightRange") != aJSONData.end())
	{
		mySightRange = aJSONData["sightRange"].get<float>();
	}
	if (aJSONData.find("colliderRadius") != aJSONData.end())
	{
		myColliderRadius = aJSONData["colliderRadius"].get<float>();
	}

	myAimingDirection = { 0.f, 0.f };

	AddCircleCollider(myColliderRadius, { 0.f, -1.f });

	if (myCollisionManager == nullptr)
	{
		std::cout << "you forgot to call InitGO with CollisionManager" << std::endl;
	}

	myVisionColliderIndex = myCollisionManager->AddCircleCollider(eCollisionLayer::Vision, mySightRange, { 0.f, 0.f });
	myCollisionManager->AttachGameObject(eCollisionLayer::Vision, myVisionColliderIndex, this);

	myTag = eGameObjectTag::CannonBeard;
	myState = eState::eIdle;
}

void CannonBeard::StartHitFlash()
{
	myHitFlashTimer.Reset();
	myHitFlashTimer.Start();
	for (int i = 0; i < myAnimations.size(); ++i)
	{
		myAnimations[i].SetColor({ 1.0f, 0.3f, 0.3f, 0.9f });
	}
}

void CannonBeard::EndHitFlash()
{
	for (int i = 0; i < myAnimations.size(); ++i)
	{
		myAnimations[i].SetColor({ 1.0f, 1.0f, 1.0f, 1.0f });
	}
}

CannonBeard::eOrientation CannonBeard::GetOrientation()
{
	const float rotation = atan2(myAimingDirection.y, myAimingDirection.x);
	eOrientation orientation;

	if (rotation > 0.75f && rotation < 2.5f)
	{
		orientation = eOrientation::eDown;
	}
	else if (rotation > -2.5f && rotation < -0.75f)
	{
		orientation = eOrientation::eUp;
	}
	else if (rotation < -2.5f || rotation > 2.5f) // Yes, the || is correct.
	{
		orientation = eOrientation::eLeft;
	}
	else //if (rotation > -0.75f && rotation < 0.75f)
	{
		orientation = eOrientation::eRight;
	}

	return orientation;
}

void CannonBeard::SetCurrentAnimation()
{
	if (myIsDead || myState == eState::eDying)
	{
		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eDying)];
		myCurrentAnimation->SetRowToAnimate(static_cast<int>(GetOrientation()));
	}
	else if (myState == eState::eAttacking)
	{
		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eAttacking)];
	}
	else
	{
		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eIdle)];
	}
}

void CannonBeard::FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer)
{
	myCurrentAnimation->SetPosition({ myPosition.x, myPosition.y });
	aRenderBuffer.push_back(myCurrentAnimation);
}

void CannonBeard::OnCollisionEnter(CollisionObject & aOther)
{
	if (myIsDead) return;

	if (aOther.GetLayer() == eCollisionLayer::Player)
	{
		Player* player = static_cast<Player*>(aOther.GetOwner());
		if (player->GetPlayerState() == Player::ePlayerState::eDying || player->GetPlayerState() == Player::ePlayerState::eDead)
		{
			myState = eState::eIdle;
			return;
		}
		if (RayTracePlayer(aOther, eRayTraceType::CannonBeard))
		{
			const CommonUtilities::Vector2f playerPosition = aOther.GetPosition() + aOther.GetOffset();
			const CommonUtilities::Vector2f enemyPosition = GetCollider()->GetPosition() + GetCollider()->GetOffset();
			myAimingDirection = playerPosition - enemyPosition;
			myState = eState::eAttacking;
		}
	}
}

void CannonBeard::OnCollisionStay(CollisionObject & aOther)
{
	if (myIsDead) return;

	if (aOther.GetLayer() == eCollisionLayer::Player)
	{
		Player* player = static_cast<Player*>(aOther.GetOwner());
		if (player->GetPlayerState() == Player::ePlayerState::eDying || player->GetPlayerState() == Player::ePlayerState::eDead)
		{
			myState = eState::eIdle;
			return;
		}
		if (RayTracePlayer(aOther, eRayTraceType::CannonBeard))
		{
			const CommonUtilities::Vector2f playerPosition = aOther.GetPosition() + aOther.GetOffset();
			const CommonUtilities::Vector2f enemyPosition = GetCollider()->GetPosition() + GetCollider()->GetOffset();
			myAimingDirection = playerPosition - enemyPosition;
			myState = eState::eAttacking;
		}
		else
		{
			myState = eState::eIdle;
		}
	}
}

void CannonBeard::OnCollisionLeave(CollisionObject & aOther)
{
	if (myIsDead) return;

	if (aOther.GetLayer() == eCollisionLayer::Player)
	{
		myState = eState::eIdle;
	}
}

void CannonBeard::Inactivate()
{
	if (myColliderIndex != -1)
	{
		GetCollider()->SetIsSolid(false);

	}
	if (myVisionColliderIndex != -1)
	{
		myCollisionManager->GetCollisionObject(eCollisionLayer::Vision, myVisionColliderIndex)->SetIsSolid(false);
	}

	//myShouldRemove = true;
}

void CannonBeard::Reactivate()
{
	if (myColliderIndex != -1)
	{
		GetCollider()->SetIsSolid(true);
	}
	if (myVisionColliderIndex != -1)
	{
		myCollisionManager->GetCollisionObject(eCollisionLayer::Vision, myVisionColliderIndex)->SetIsSolid(false);
	}

	//myShouldRemove = false;
}

void CannonBeard::Attack()
{
	CommonUtilities::Vector2f position = GetCollider()->GetPosition() + GetCollider()->GetOffset();
	position.y -= 1.2f;
	myProjectileManager->CreateBullet(myProjectileSpeed, myAimingDirection, position, myDamage);
	AudioManager::GetInstance()->PlayRandomSound("enemyShoot", 2);
}

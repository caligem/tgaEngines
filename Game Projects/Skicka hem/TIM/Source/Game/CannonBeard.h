#pragma once
#include "Enemy.h"
#include "json.h"
#include "Animation.h"

class ProjectileManager;

class CannonBeard : public Enemy
{
public:
	CannonBeard();
	~CannonBeard();

	void Init(const nlohmann::json& aData, ProjectileManager& aProjectileManager);
	void OnUpdate(float aDeltatime);
	void FillRenderBuffer(std::vector<RenderObject*> &aRenderBuffer);

	enum class eState
	{
		eIdle,
		eAttacking,
		eDying,
		count
	};

	enum class eOrientation
	{
		eDown,
		eLeft,
		eUp,
		eRight,
		COUNT
	};

	virtual void OnCollisionEnter(CollisionObject &aOther) override;
	virtual void OnCollisionStay(CollisionObject &aOther) override;
	virtual void OnCollisionLeave(CollisionObject &aOther) override;

	void Inactivate() override;
	void Reactivate() override;

private:
	void Attack();

	eState myState;

	std::array<Animation, static_cast<int>(eState::count)> myAnimations;
	Animation* myCurrentAnimation;

	CommonUtilities::Vector2f myAimingDirection;

	eOrientation GetOrientation();
	void SetCurrentAnimation();
	void ParseJSONFile(const nlohmann::json& aJSONData);
	void StartHitFlash() override;
	void EndHitFlash();

	float myAttackSpeed;
	float myProjectileSpeed;

	ProjectileManager* myProjectileManager;

	Countdown myHitFlashTimer;
};

#include "stdafx.h"
#include "Chest.h"
#include "CollisionObject.h"
#include "PostMaster.h"
#include "Sprite.h"
#include "AudioManager.h"

Chest::Chest()
{
	myOpened = false;
	myPlayerInReach = false;
}

Chest::~Chest()
{
}

void Chest::Init(CollisionManager* aCollisionManager, const CommonUtilities::Vector2f& aPosition, const int aLootID)
{
	myTag = eGameObjectTag::Chest;
	InitGO(aPosition, aCollisionManager, eCollisionLayer::Interactable);

	myLootID = aLootID;

	myLootedItemAlpha = 1.0f;
	myFadeTimer = 1.0f;
	myLootedItemPos = myPosition;

	myTwinkleAnimation.Init("Sprites/fx/sparkles_spritesheet.dds");
	myTwinkleAnimation.Setup(2, 4, 4);
	myTwinkleAnimation.SetPivot({ 0.5f, 0.5f });
	myTwinkleAnimation.SetLayerType(eLayerType::EForeground);
	myTwinkleAnimation.SetPosition(myPosition);

	myLootItemBackground.Init("Sprites/Wands/itemBackGlow.dds");
	myLootItemBackground.SetPivot({ 0.5f, 0.5f });
	myLootItemBackground.SetLayerType(eLayerType::EForeground);
}

void Chest::OnCollisionEnter(CollisionObject& aOther)
{
	if (aOther.GetOwner() != nullptr)
	{
		if (aOther.GetOwner()->GetTag() == eGameObjectTag::Player)
		{
			myPlayerInReach = true;
			if (!myOpened)
			{
				Message message(eMessageType::ShowInteractButton);
				message.data.myVec2f = myPosition;
				PostMaster::SendMessages(message);
			}
		}
	}
}

void Chest::OnCollisionLeave(CollisionObject & aOther)
{
	if (aOther.GetOwner() != nullptr)
	{
		if (aOther.GetOwner()->GetTag() == eGameObjectTag::Player)
		{
			myPlayerInReach = false;
			if (!myOpened)
			{
				PostMaster::SendMessages(eMessageType::HideInteractButton);
			}
		}
	}
}

void Chest::ReceiveMessage(Message aMessage)
{
	if (aMessage.myMessageType == eMessageType::PlayerInteract && myPlayerInReach && !myOpened)
	{
		Message message(eMessageType::FoundWand);
		message.data.myInt = myLootID;
		PostMaster::SendMessages(message);
		AudioManager::GetInstance()->PlaySound("openChest");
		myOpened = true;

		RenderObject* swapSprite = myDrawable;
		myDrawable = myOpenedSprite;
		myOpenedSprite = swapSprite;

		PostMaster::SendMessages(eMessageType::HideInteractButton);
	}
}

void Chest::SetOpenedSprite(const char * aPath)
{
	Sprite* openedSprite = new Sprite(aPath, myPosition, eLayerType::EGameObjectLayer);
	assert(openedSprite != nullptr && "Cannot find opened chest image - is it called \"something\"_Open?");
	openedSprite->SetPosition({ myPosition.x, myPosition.y });
	openedSprite->SetPivot({ 0.5f, 0.5f });
	myOpenedSprite = openedSprite;
}

void Chest::OnUpdate(float aDeltaTime)
{
	GameObject::OnUpdate(aDeltaTime);
	if (myOpened)
	{
		// If still visible
		if (myLootedItemAlpha >= 0.0f)
		{
			myLootedItemPos.y -= aDeltaTime;

			// A little while before fading starts
			if (myFadeTimer >= 0.0f)
			{
				myFadeTimer -= aDeltaTime;
			}
			else
			{
				// Fade
				myLootedItemAlpha -= 0.5f * aDeltaTime;
			}
		}
	}
	else
	{
		myTwinkleAnimation.Update(aDeltaTime);
	}
}

void Chest::FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer)
{
	GameObject::FillRenderBuffer(aRenderBuffer);
	if (myOpened)
	{
		if (myLootedItemAlpha >= 0.0f)
		{
			myLootItemBackground.SetPosition(myLootedItemPos);
			myLootItemBackground.SetColor({ 1.0f, 1.0f, 1.0f, myLootedItemAlpha });
			aRenderBuffer.push_back(&myLootItemBackground);

			//const CU::Vector2f oldPos = myLootedItemSprite->GetPosition(); // this sprite is used on several spots. We do not want to interfere with those places.
			myLootedItemSprite->SetPosition(myLootedItemPos);

			//const CU::Vector4f oldColor = myLootedItemSprite->GetColor(); // this sprite is used on several spots. We do not want to interfere with those places.
			myLootedItemSprite->SetColor({ 1.0f, 1.0f, 1.0f, myLootedItemAlpha });
			
			aRenderBuffer.push_back(myLootedItemSprite);

			// Make other places happy by resetting
			//myLootedItemSprite->SetColor(oldColor);
			//myLootedItemSprite->SetPosition(oldPos);
		}
	}
	else
	{
		aRenderBuffer.push_back(&myTwinkleAnimation);
	}
}

void Chest::BindSprite(Sprite * aSprite)
{
	myLootedItemSprite = aSprite;
	myLootedItemSprite->SetLayerType(eLayerType::EForeground);
}

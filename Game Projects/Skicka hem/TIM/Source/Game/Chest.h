#pragma once
#include "Subscriber.h"
#include "GameObject.h"
#include "Animation.h"

class Chest :
	public GameObject, public Subscriber
{
public:
	Chest();
	~Chest();

	void Init(CollisionManager* aCollisionManager, const CommonUtilities::Vector2f& aPosition, const int aLootID);

	inline int const GetLootID() const { return myLootID; }

	void OnCollisionEnter(CollisionObject &aOther) override;
	void OnCollisionLeave(CollisionObject &aOther) override;
	void ReceiveMessage(const Message aMessage);
	void SetOpenedSprite(const char * aPath);

	void OnUpdate(float aDeltaTime);
	void FillRenderBuffer(std::vector<RenderObject*> &aRenderBuffer);

	void BindSprite(Sprite* aSprite);

private:
	RenderObject* myOpenedSprite;
	bool myPlayerInReach;
	bool myOpened;


	int myLootID;

	Animation myTwinkleAnimation;

	float myLootedItemAlpha;
	float myFadeTimer;
	CU::Vector2f myLootedItemPos;
	Sprite* myLootedItemSprite;
	Sprite myLootItemBackground;
};

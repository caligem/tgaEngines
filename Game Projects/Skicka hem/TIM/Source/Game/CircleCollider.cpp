#include "stdafx.h"
#include "BoxCollider.h"
#include "CircleCollider.h"
#include "LineCollider.h"
#include "tga2d\primitives\line_primitive.h"
#include "tga2d\math\common_math.h"

CircleCollider::CircleCollider(float aRadius, const CommonUtilities::Vector2f & aOffset)
{
	myRadius = aRadius;
	myOffset = aOffset;
}

CircleCollider::~CircleCollider()
{
}

bool CircleCollider::TestCollision(CollisionObject * aCollisionObject)
{
	return aCollisionObject->TestCollision(this);
}

bool CircleCollider::TestCollision(BoxCollider * aBoxCollider)
{
	return aBoxCollider->CircleVSBoxCollision(myPosition + myOffset, myRadius, aBoxCollider->GetPosition() + aBoxCollider->GetOffset(), aBoxCollider->GetWidth(), aBoxCollider->GetHeight());
}

bool CircleCollider::TestCollision(CircleCollider * aCircleCollider)
{
	return aCircleCollider->CircleVSCircleCollision(myPosition + myOffset, myRadius, aCircleCollider->GetPosition() + aCircleCollider->GetOffset(), aCircleCollider->GetRadius());
}

bool CircleCollider::TestCollision(LineCollider * aLineCollider)
{
	return aLineCollider->CircleVSLineCollision(myPosition + myOffset, myRadius, aLineCollider->GetPosition() + aLineCollider->GetOffset(), aLineCollider->GetEndPosition());
}

void CircleCollider::DebugDraw(const CommonUtilities::Vector2f& aCameraPosition)
{
	if (!myIsInsideBoundaries)
	{
		return;
	}

	const CommonUtilities::Vector2f origin = (myPosition + myOffset) - aCameraPosition;
	Tga2D::CLinePrimitive line;

	const float segmentValue = 18.f;
	for (float i = 0; i < 360; i+= segmentValue)
	{
		const float rad = Tga2D::DegToRad(i);
		const CommonUtilities::Vector2f firstPos = origin + (CommonUtilities::Vector2f(cos(rad), sin(rad)) * myRadius);
		const CommonUtilities::Vector2f firstPosInScreenSpace = SpaceConverter::TileSpaceToScreenSpace(firstPos) + CommonUtilities::Vector2f(0.5f, 0.5f);
		line.SetFrom(firstPosInScreenSpace.x, firstPosInScreenSpace.y);

		const float radIncrease = Tga2D::DegToRad(segmentValue);
		const CommonUtilities::Vector2f secondPos = origin + (CommonUtilities::Vector2f(cos(rad + radIncrease), sin(rad + radIncrease)) * myRadius);
		const CommonUtilities::Vector2f secondPosInScreenSpace = SpaceConverter::TileSpaceToScreenSpace(secondPos) + CommonUtilities::Vector2f(0.5f, 0.5f);
		line.SetTo(secondPosInScreenSpace.x, secondPosInScreenSpace.y);

		line.myColor = { 1.f, 0.f, 0.f, 1.f };
		line.Render();
	}
}

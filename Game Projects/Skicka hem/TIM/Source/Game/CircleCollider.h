#pragma once
#include "CollisionObject.h"
#include "Vector.h"

class CircleCollider : public CollisionObject
{
public:
	CircleCollider(float aRadius, const CommonUtilities::Vector2f & aOffset);
	~CircleCollider();

	bool TestCollision(CollisionObject* aCollisionObject) override;
	bool TestCollision(BoxCollider* aBoxCollider) override;
	bool TestCollision(CircleCollider* aCircleCollider) override;
	bool TestCollision(LineCollider* aLineCollider) override;

	void DebugDraw(const CommonUtilities::Vector2f& aCameraPosition) override;

	inline const float GetRadius() const { return myRadius; }
private:
	float myRadius;
};


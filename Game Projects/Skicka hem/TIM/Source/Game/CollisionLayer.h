#pragma once
#include <stack>
#include <array>
#include <vector>
#include "Vector.h"

class CollisionManager;
class CollisionObject;

enum class eCollisionLayer
{
	None = -1,
	Player,
	Wand,
	Enemies,
	FallPit,
	Terrain,
	Vision,
	Interactable,
	Projectile,
	Checkpoint
};

template <int size>
struct CollisionLayer
{
	friend class CollisionManager;
private:
	CollisionLayer() = default;
	CollisionLayer(int aCollisionLayer, int aCollisionLayersToCollideWith)
	{
		myLayersToCollideWith = aCollisionLayersToCollideWith;
		myLayer = aCollisionLayer;
	}

	std::array<CollisionObject*, size> myObjects;

	std::stack<int> myFreeIndexes;
	std::vector<int> myOccupiedIndexes;

	int myLayersToCollideWith;
	int myLayer;
};
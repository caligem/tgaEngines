#include "stdafx.h"
#include "CollisionManager.h"

#include "BoxCollider.h"
#include "CircleCollider.h"
#include "LineCollider.h"
#include "GameObject.h"

#include <assert.h>

CollisionManager::CollisionManager()
{
}

CollisionManager::~CollisionManager()
{
	DeleteAllCollisionObjects();
}

void CollisionManager::Init()
{
	myLayers.reserve(16);

	int playerLayerToCollideWith = EnumToIndex(eCollisionLayer::Wand) | EnumToIndex(eCollisionLayer::Enemies) | EnumToIndex(eCollisionLayer::FallPit) | EnumToIndex(eCollisionLayer::Terrain) | EnumToIndex(eCollisionLayer::Interactable) | EnumToIndex(eCollisionLayer::Vision) | EnumToIndex(eCollisionLayer::Checkpoint);
	int playerLayer = EnumToIndex(eCollisionLayer::Player);
	myLayers.push_back(CollisionLayer<1024>(playerLayer, playerLayerToCollideWith));

	int wandLayerToCollideWith = EnumToIndex(eCollisionLayer::Terrain) | EnumToIndex(eCollisionLayer::FallPit) | EnumToIndex(eCollisionLayer::Interactable);
	int wandLayer = EnumToIndex(eCollisionLayer::Wand);
	myLayers.push_back(CollisionLayer<1024>(wandLayer, wandLayerToCollideWith));

	int enemyLayerToCollideWith = EnumToIndex(eCollisionLayer::Wand) | EnumToIndex(eCollisionLayer::Enemies) | EnumToIndex(eCollisionLayer::FallPit) | EnumToIndex(eCollisionLayer::Terrain);
	int enemyLayer = EnumToIndex(eCollisionLayer::Enemies);
	myLayers.push_back(CollisionLayer<1024>(enemyLayer, enemyLayerToCollideWith));

	int fallPitLayer = EnumToIndex(eCollisionLayer::FallPit);
	myLayers.push_back(CollisionLayer<1024>(fallPitLayer, 0));

	int terrainLayer = EnumToIndex(eCollisionLayer::Terrain);
	myLayers.push_back(CollisionLayer<1024>(terrainLayer, 0));

	int visionLayer = EnumToIndex(eCollisionLayer::Vision);
	myLayers.push_back(CollisionLayer<1024>(visionLayer, 0));

	int interactableLayerToCollideWith = EnumToIndex(eCollisionLayer::Wand);
	int interactableLayer = EnumToIndex(eCollisionLayer::Interactable);
	myLayers.push_back(CollisionLayer<1024>(interactableLayer, interactableLayerToCollideWith));

	int projectileLayerToCollideWith = EnumToIndex(eCollisionLayer::Player) | EnumToIndex(eCollisionLayer::Terrain);
	int projectileLayer = EnumToIndex(eCollisionLayer::Projectile);
	myLayers.push_back(CollisionLayer<1024>(projectileLayer, projectileLayerToCollideWith));

	int checkpointLayer = EnumToIndex(eCollisionLayer::Checkpoint);
	myLayers.push_back(CollisionLayer<1024>(checkpointLayer, 0));
	
	FillFreeIndexes();
}

void CollisionManager::Update(const CommonUtilities::Vector2f& aCameraPosition)
{
	UpdateColliders(aCameraPosition);
	TestCollision();
	UpdateCollisions();
}

void CollisionManager::UpdateCollisions()
{
	for (int i = 0; i < myLayers.size(); i++)
	{
		for (auto it = myLayers[i].myOccupiedIndexes.begin(); it != myLayers[i].myOccupiedIndexes.end(); it++)
		{
			int occupiedIndex = *it;
			myLayers[i].myObjects[occupiedIndex]->UpdateCollisions();
		}
	}
}

int CollisionManager::AddBoxCollider(eCollisionLayer aLayer, float aHeight, float aWidth, const CommonUtilities::Vector2f & aOffset)
{
	assert(aLayer != eCollisionLayer::None && "Trying to add a box collider with no layer");

	BoxCollider* newBoxCollider = new BoxCollider(aHeight, aWidth, aOffset);
	newBoxCollider->SetLayer(aLayer);
	return AddColliderToRightList(aLayer, newBoxCollider);
}

int CollisionManager::AddCircleCollider(eCollisionLayer aLayer, float aRadius, const CommonUtilities::Vector2f & aOffset)
{
	assert(aLayer != eCollisionLayer::None && "Trying to add a circle collider with no layer");

	CircleCollider* newCircleCollider = new CircleCollider(aRadius, aOffset);
	newCircleCollider->SetLayer(aLayer);
	return AddColliderToRightList(aLayer, newCircleCollider);
}

int CollisionManager::AddBoxCollider(eCollisionLayer aLayer, float aHeight, float aWidth, const CommonUtilities::Vector2f & aOffset, const CommonUtilities::Vector2f & aPosition)
{
	assert(aLayer != eCollisionLayer::None && "Trying to add a box collider with no layer");

	BoxCollider* newBoxCollider = new BoxCollider(aHeight, aWidth, aOffset);
	newBoxCollider->SetPosition(aPosition);
	newBoxCollider->SetLayer(aLayer);
	return AddColliderToRightList(aLayer, newBoxCollider);
}

int CollisionManager::AddCircleCollider(eCollisionLayer aLayer, float aRadius, const CommonUtilities::Vector2f & aOffset, const CommonUtilities::Vector2f & aPosition)
{
	assert(aLayer != eCollisionLayer::None && "Trying to add a circle collider with no layer");

	CircleCollider* newCircleCollider = new CircleCollider(aRadius, aOffset);
	newCircleCollider->SetPosition(aPosition);
	newCircleCollider->SetLayer(aLayer);
	return AddColliderToRightList(aLayer, newCircleCollider);
}

int CollisionManager::AddLineCollider(eCollisionLayer aLayer, const CommonUtilities::Vector2f & aDirection, float aLength, const CommonUtilities::Vector2f & aOffset, const CommonUtilities::Vector2f & aPosition)
{
	assert(aLayer != eCollisionLayer::None && "Trying to add a line collider with no layer");

	LineCollider* newLineCollider = new LineCollider(aPosition, aDirection, aLength, aOffset);
	newLineCollider->SetLayer(aLayer);
	return AddColliderToRightList(aLayer, newLineCollider);
}

void CollisionManager::DebugDrawCollisionObjects(const CommonUtilities::Vector2f& aCameraPosition)
{
	for (int i = 0; i < myLayers.size(); i++)
	{
		for (auto it = myLayers[i].myOccupiedIndexes.begin(); it != myLayers[i].myOccupiedIndexes.end(); it++)
		{
			int occupiedIndex = *it;
			if (!myLayers[i].myObjects[occupiedIndex]->GetIsSolid())
			{
				continue;
			}
			myLayers[i].myObjects[occupiedIndex]->DebugDraw(aCameraPosition);
		}
	}
}

void CollisionManager::AttachGameObject(eCollisionLayer aLayer, int aColliderIndex, GameObject* aGameObject)
{
	assert(aLayer != eCollisionLayer::None && "Trying to Attach a GameObject with no collision layer");

	assert(myLayers[static_cast<int>(aLayer)].myObjects[aColliderIndex] != nullptr && "trying to attach gameObject with wrong collisionindex");
	assert(aGameObject != nullptr && "trying to attach gameobject but is nullptr");
	myLayers[static_cast<int>(aLayer)].myObjects[aColliderIndex]->SetOwner(aGameObject);
}

CollisionObject * CollisionManager::GetCollisionObject(eCollisionLayer aLayer, int aColliderIndex)
{
	assert(myLayers[static_cast<int>(aLayer)].myObjects[aColliderIndex] != nullptr && "trying to get collisionObject but is nullptr");
	return myLayers[static_cast<int>(aLayer)].myObjects[aColliderIndex];
}

void CollisionManager::RemoveCollider(eCollisionLayer aLayer, int aColliderIndex)
{
	assert(myLayers[static_cast<int>(aLayer)].myObjects[aColliderIndex] != nullptr && "trying to remove collider but is nullptr");
	delete myLayers[static_cast<int>(aLayer)].myObjects[aColliderIndex];
	myLayers[static_cast<int>(aLayer)].myObjects[aColliderIndex] = nullptr;
	myLayers[static_cast<int>(aLayer)].myFreeIndexes.push(aColliderIndex);

	for (size_t i = 0; i < myLayers[static_cast<int>(aLayer)].myOccupiedIndexes.size(); i++)
	{
		int index = myLayers[static_cast<int>(aLayer)].myOccupiedIndexes[i];

		if (index == aColliderIndex)
		{
			myLayers[static_cast<int>(aLayer)].myOccupiedIndexes.erase(myLayers[static_cast<int>(aLayer)].myOccupiedIndexes.begin() + i);
			return;
		}
	}
	assert(false && ("didnt find occupied index when trying to remove collider in layer: " + static_cast<int>(aLayer)));
}

bool CollisionManager::DidColliderCollideWithAnythingInLayer(CollisionObject * aCollisionObject, eCollisionLayer aLayerToCollideWith)
{
	int layerIndex = static_cast<int>(aLayerToCollideWith);
	for (auto iteratorLayer = myLayers[layerIndex].myOccupiedIndexes.begin(); iteratorLayer != myLayers[layerIndex].myOccupiedIndexes.end(); iteratorLayer++)
	{
		int occupiedIndex = *iteratorLayer;

		if (!myLayers[layerIndex].myObjects[occupiedIndex]->GetIsInsideScreen() || !myLayers[layerIndex].myObjects[occupiedIndex]->GetIsSolid())
		{
			continue;
		}

		if (aCollisionObject->TestCollision(myLayers[layerIndex].myObjects[occupiedIndex]))
		{
			return true;
		}
	}

	return false;
}

int CollisionManager::EnumToIndex(const eCollisionLayer& aCollisionLayer)
{
	int index = 1 << static_cast<int>(aCollisionLayer);
	return index;
}
#include <iostream>
void CollisionManager::TestCollision()
{
	for (size_t indexLayer1 = 0; indexLayer1 < myLayers.size(); indexLayer1++)
	{
		for (size_t indexLayer2 = 0; indexLayer2 < myLayers.size(); indexLayer2++)
		{
			if ((myLayers[indexLayer1].myLayer & myLayers[indexLayer2].myLayersToCollideWith) > 0)
			{
				for (auto iteratorLayer1 = myLayers[indexLayer1].myOccupiedIndexes.begin(); iteratorLayer1 != myLayers[indexLayer1].myOccupiedIndexes.end(); iteratorLayer1++)
				{
					int occupiedIndexLayer1 = *iteratorLayer1;

					if (!myLayers[indexLayer1].myObjects[occupiedIndexLayer1]->GetIsInsideScreen() || !myLayers[indexLayer1].myObjects[occupiedIndexLayer1]->GetIsSolid())
					{
						continue;
					}

					for (auto iteratorLayer2 = myLayers[indexLayer2].myOccupiedIndexes.begin(); iteratorLayer2 != myLayers[indexLayer2].myOccupiedIndexes.end(); iteratorLayer2++)
					{
						int occupiedIndexLayer2 = *iteratorLayer2;

						if (!myLayers[indexLayer2].myObjects[occupiedIndexLayer2]->GetIsInsideScreen() || !myLayers[indexLayer2].myObjects[occupiedIndexLayer2]->GetIsSolid())
						{
							continue;
						}

						if (&myLayers[indexLayer1].myObjects[occupiedIndexLayer1] != &myLayers[indexLayer2].myObjects[occupiedIndexLayer2])
						{
							if (myLayers[indexLayer1].myObjects[occupiedIndexLayer1]->TestCollision(myLayers[indexLayer2].myObjects[occupiedIndexLayer2]))
							{
								myLayers[indexLayer1].myObjects[occupiedIndexLayer1]->AddCollision(myLayers[indexLayer2].myObjects[occupiedIndexLayer2]);
								myLayers[indexLayer2].myObjects[occupiedIndexLayer2]->AddCollision(myLayers[indexLayer1].myObjects[occupiedIndexLayer1]);
							}
						}
						else
						{
							continue;
						}
					}
				}
			}
		}
	}
}

void CollisionManager::UpdateColliders(const CommonUtilities::Vector2f aCameraPosition)
{
	for (int i = 0; i < myLayers.size(); i++)
	{
		for (auto it = myLayers[i].myOccupiedIndexes.begin(); it != myLayers[i].myOccupiedIndexes.end(); it++)
		{
			int occupiedIndex = *it;
			myLayers[i].myObjects[occupiedIndex]->Update(aCameraPosition);
		}
	}
}

int CollisionManager::AddColliderToRightList(eCollisionLayer aLayer, CollisionObject* aObject)
{
	if (aObject == nullptr)
	{
		assert(false && "trying to add nullptr object in collision manager");
	}

	assert(aLayer != eCollisionLayer::None && "Trying to add collider to no layer");

	int freeIndex = myLayers[static_cast<int>(aLayer)].myFreeIndexes.top();
	myLayers[static_cast<int>(aLayer)].myFreeIndexes.pop();
	myLayers[static_cast<int>(aLayer)].myOccupiedIndexes.push_back(freeIndex);
	assert(freeIndex != myLayers[static_cast<int>(aLayer)].myFreeIndexes.top() && "index still in freePlayerIndex stack");

	myLayers[static_cast<int>(aLayer)].myObjects[freeIndex] = aObject;
	return freeIndex;
}

void CollisionManager::FillFreeIndexes()
{
	for (size_t layerIndex = 0; layerIndex < myLayers.size(); layerIndex++)
	{
		for (int objectIndex = 0; objectIndex < myLayers[layerIndex].myObjects.size(); objectIndex++)
		{
			myLayers[layerIndex].myFreeIndexes.push(objectIndex);
		}
	}
}

void CollisionManager::DeleteAllCollisionObjects()
{
	for (int layerIndex = 0; layerIndex < myLayers.size(); layerIndex++)
	{
		for (auto it = myLayers[layerIndex].myOccupiedIndexes.begin(); it != myLayers[layerIndex].myOccupiedIndexes.end(); it++)
		{
			int occupiedIndex = *it;
			if (myLayers[layerIndex].myObjects[occupiedIndex] != nullptr)
			{
				delete myLayers[layerIndex].myObjects[occupiedIndex];
				myLayers[layerIndex].myObjects[occupiedIndex] = nullptr;
			}
		}
	}
}

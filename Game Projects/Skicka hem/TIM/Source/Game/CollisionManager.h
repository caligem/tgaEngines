#pragma once
#include <vector>
#include <Vector.h>
#include "CollisionLayer.h"

class GameObject;

class CollisionManager
{
public:
	CollisionManager();
	~CollisionManager();

	void Init();
	void Update(const CommonUtilities::Vector2f& aCameraPosition);
	void UpdateCollisions();

	int AddBoxCollider(eCollisionLayer aLayer, float aHeight, float aWidth, const CommonUtilities::Vector2f & aOffset);
	int AddCircleCollider(eCollisionLayer aLayer, float aRadius, const CommonUtilities::Vector2f & aOffset);

	int AddBoxCollider(eCollisionLayer aLayer, float aHeight, float aWidth, const CommonUtilities::Vector2f & aOffset, const CommonUtilities::Vector2f& aPosition);
	int AddCircleCollider(eCollisionLayer aLayer, float aRadius, const CommonUtilities::Vector2f & aOffset, const CommonUtilities::Vector2f& aPosition);
	int AddLineCollider(eCollisionLayer aLayer, const CommonUtilities::Vector2f & aDirection, float aLength, const CommonUtilities::Vector2f & aOffset, const CommonUtilities::Vector2f& aPosition);

	void DebugDrawCollisionObjects(const CommonUtilities::Vector2f& aCameraPosition);

	CollisionObject* GetCollisionObject(eCollisionLayer aLayer, int aColliderIndex);
	void AttachGameObject(eCollisionLayer aLayer, int aColliderIndex, GameObject* aGameObject);
	void RemoveCollider(eCollisionLayer aLayer, int aColliderIndex);

	bool DidColliderCollideWithAnythingInLayer(CollisionObject* aCollisionObject, eCollisionLayer aLayerToCollideWith);

private:
	int EnumToIndex(const eCollisionLayer& aCollisionLayer);

	void TestCollision();
	void UpdateColliders(const CommonUtilities::Vector2f aCameraPosition);

	int AddColliderToRightList(eCollisionLayer aLayer, CollisionObject* aObject);

	void FillFreeIndexes();
	void DeleteAllCollisionObjects();

	std::vector<CollisionLayer<1024>> myLayers;
};




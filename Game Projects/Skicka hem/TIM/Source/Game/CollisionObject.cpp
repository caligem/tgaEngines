#include "stdafx.h"
#include "CollisionObject.h"
#include "GameObject.h"
#include <cmath>
#include "utility.h"

#define CameraXSize 60.f
#define CameraYSize 34.f
#define FORGIVENESS 100.f

CollisionObject::CollisionObject()
	: myIsInsideBoundaries(false)
	, myIsSolid(true)
{
	myOwner = nullptr;
}


CollisionObject::~CollisionObject()
{
}

void CollisionObject::Update(const CommonUtilities::Vector2f& aCameraPosition)
{
	if (myOwner != nullptr)
	{
		myPosition = myOwner->GetPosition();
	}

	const CommonUtilities::Vector2f distance = myPosition + myOffset - aCameraPosition;
	if (distance.x > (CameraXSize + FORGIVENESS) / -2.f &&
		distance.x < (CameraXSize + FORGIVENESS) / 2.f &&
		distance.y > (CameraYSize + FORGIVENESS) / -2.f &&
		distance.y < (CameraYSize + FORGIVENESS) / 2.f)
	{
		myIsInsideBoundaries = true;
	}
	else
	{
		myIsInsideBoundaries = false;
	}
}

void CollisionObject::UpdateCollisions()
{
	if (myOwner != nullptr)
	{
		HandleCollision();
	}
}

void CollisionObject::SetOwner(GameObject * aGameObject)
{
	myOwner = aGameObject;
}

bool CollisionObject::CircleVSCircleCollision(CommonUtilities::Vector2f aCircle1, float aCircleRadius1, CommonUtilities::Vector2f aCircle2, float aCircleRadius2)
{
	if ((aCircle1 - aCircle2).Length2() < (aCircleRadius1 + aCircleRadius2) * (aCircleRadius1 + aCircleRadius2))
	{
		return true;
	}
	return false;
}

bool CollisionObject::LineVSLineCollision(CommonUtilities::Vector2f aLineStart1, CommonUtilities::Vector2f aLineEnd1, CommonUtilities::Vector2f aLineStart2, CommonUtilities::Vector2f aLineEnd2)
{
	CommonUtilities::Vector2f s1 = aLineEnd1 - aLineStart1;
	CommonUtilities::Vector2f s2 = aLineEnd2 - aLineStart2;

	float s, t;
	s = (-s1.y * (aLineStart1.x - aLineStart2.x) + s1.x * (aLineStart1.y - aLineStart2.y)) / (-s2.x * s1.y + s1.x * s2.y);
	t = (s2.x * (aLineStart1.y - aLineStart2.y) - s2.y * (aLineStart1.x - aLineStart2.x)) / (-s2.x * s1.y + s1.x * s2.y);

	if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
	{
		return true;
	}
	return false;
}

bool CollisionObject::BoxVSBoxCollision(CommonUtilities::Vector2f aBox1, float aBox1Width, float aBox1Height, CommonUtilities::Vector2f aBox2, float aBox2Width, float aBox2Height)
{
	const float minX1 = aBox1.x - (aBox1Width / 2);
	const float minX2 = aBox2.x - (aBox2Width / 2);

	const float minY1 = aBox1.y - (aBox1Height / 2);
	const float minY2 = aBox2.y - (aBox2Height / 2);

	const CommonUtilities::Vector2f minPos1(minX1, minY1);
	const CommonUtilities::Vector2f maxPos1(minX1 + aBox1Width, minY1 + aBox1Height);
	const CommonUtilities::Vector2f minPos2(minX2, minY2);
	const CommonUtilities::Vector2f maxPos2(minX2 + aBox2Width, minY2 + aBox2Height);

	if (minPos1.x < maxPos2.x &&
		maxPos1.x > minPos2.x &&
		minPos1.y < maxPos2.y &&
		maxPos1.y > minPos2.y)
	{
		return true;
	}
	return false;
}

bool CollisionObject::CircleVSBoxCollision(CommonUtilities::Vector2f aCircleCenter, float aCircleRadius, CommonUtilities::Vector2f aBox, float aBoxWidth, float aBoxHeight)
{
	const float halfBoxHeight = aBoxHeight / 2.f;
	const float halfBoxWidth = aBoxWidth / 2.f;
	CommonUtilities::Vector2f closestBoxPoint = aCircleCenter;

	closestBoxPoint.x = cit::Clamp(closestBoxPoint.x, aBox.x - halfBoxWidth, aBox.x + halfBoxWidth);
	closestBoxPoint.y = cit::Clamp(closestBoxPoint.y, aBox.y - halfBoxHeight, aBox.y + halfBoxHeight);

	return PointInCircle(closestBoxPoint, aCircleCenter, aCircleRadius);
}

bool CollisionObject::BoxVSLineCollision(CommonUtilities::Vector2f aBox, float aBoxWidth, float aBoxHeight, CommonUtilities::Vector2f aLineStart, CommonUtilities::Vector2f aLineEnd)
{
	const CommonUtilities::Vector2f A = { aBox.x - (aBoxWidth / 2.0f),aBox.y - (aBoxHeight / 2.0f) };
	const CommonUtilities::Vector2f B = { aBox.x + (aBoxWidth / 2.0f),aBox.y - (aBoxHeight / 2.0f) };
	const CommonUtilities::Vector2f C = { aBox.x + (aBoxWidth / 2.0f),aBox.y + (aBoxHeight / 2.0f) };
	const CommonUtilities::Vector2f D = { aBox.x - (aBoxWidth / 2.0f),aBox.y + (aBoxHeight / 2.0f) };

	if (LineVSLineCollision(A, B, aLineStart, aLineEnd))
	{
		return true;
	}
	if (LineVSLineCollision(B, C, aLineStart, aLineEnd))
	{
		return true;
	}
	if (LineVSLineCollision(C, D, aLineStart, aLineEnd))
	{
		return true;
	}
	if (LineVSLineCollision(D, A, aLineStart, aLineEnd))
	{
		return true;
	}
	if (PointInBox(aBox, aBoxWidth, aBoxHeight, aLineStart))
	{
		return true;
	}
	if (PointInBox(aBox, aBoxWidth, aBoxHeight, aLineEnd))
	{
		return true;
	}
	return false;
}

bool CollisionObject::CircleVSLineCollision(CommonUtilities::Vector2f aCircle, float aCircleRadius, CommonUtilities::Vector2f aLineStart, CommonUtilities::Vector2f aLineEnd)
{
	CommonUtilities::Vector2f distanceBetweenSphereAndLine = aCircle - aLineStart;
	CommonUtilities::Vector2f direction = (aLineEnd - aLineStart).GetNormalized();
	float length = (aLineEnd - aLineStart).Length();

	float proj = direction.Dot(distanceBetweenSphereAndLine);
	if (proj < 0)
	{
		proj = 0;
	}
	if (proj > length)
	{
		proj = length;
	}
	CommonUtilities::Vector2f pointNearestSphere = aLineStart + proj * direction;
	return ((pointNearestSphere - aCircle).Length2() < aCircleRadius*aCircleRadius);
}

bool CollisionObject::PointInBox(CommonUtilities::Vector2f aBox1, float aBox1Width, float aBox1Height, CommonUtilities::Vector2f aPoint)
{
	float minX1 = aBox1.x - (aBox1Width / 2);

	float minY1 = aBox1.y - (aBox1Height / 2);

	if (minX1 < aPoint.x &&
		minX1 + aBox1Width > aPoint.x &&
		minY1 < aPoint.y &&
		aBox1Height + minY1 > aPoint.y)
	{
		return true;
	}
	return false;
}

bool CollisionObject::PointInCircle(CommonUtilities::Vector2f aPoint, CommonUtilities::Vector2f aCirclePosition, float aCircleRadius)
{
	CommonUtilities::Vector2f vectorBetweenPointAndCircle = aPoint - aCirclePosition;
	return (vectorBetweenPointAndCircle.Length2() < aCircleRadius * aCircleRadius);
}

void CollisionObject::AddCollision(CollisionObject * aOther)
{
	for (size_t i = 0; i < myCollisions.size(); i++)
	{
		if (aOther == myCollisions[i].pOtherCollisionObject)
		{
			myCollisions[i].bHitThisFrame = true;
			return;
		}
	}

	myCollisions.push_back(Collision());
	myCollisions.back().bHitThisFrame = true;
	myCollisions.back().eState = Collision::eCollisionState::eDefault;
	myCollisions.back().pOtherCollisionObject = aOther;
}

void CollisionObject::HandleCollision()
{
	for (int i = static_cast<int>(myCollisions.size()) - 1; i >= 0; --i)
	{
		if (myCollisions[i].bHitThisFrame)
		{
			if (myCollisions[i].eState == Collision::eCollisionState::eDefault || myCollisions[i].eState == Collision::eCollisionState::eLeave)
			{
				myCollisions[i].eState = Collision::eCollisionState::eEnter;
				myOwner->OnCollisionEnter(*myCollisions[i].pOtherCollisionObject);
			}
			else if (myCollisions[i].eState == Collision::eCollisionState::eEnter)
			{
				myCollisions[i].eState = Collision::eCollisionState::eStay;
				myOwner->OnCollisionStay(*myCollisions[i].pOtherCollisionObject);
			}
			else if (myCollisions[i].eState == Collision::eCollisionState::eStay)
			{
				myOwner->OnCollisionStay(*myCollisions[i].pOtherCollisionObject);
			}
		}
		else
		{
			if (myCollisions[i].eState == Collision::eCollisionState::eStay || myCollisions[i].eState == Collision::eCollisionState::eEnter)
			{
				myCollisions[i].eState = Collision::eCollisionState::eLeave;
				myOwner->OnCollisionLeave(*myCollisions[i].pOtherCollisionObject);
			}
			else if (myCollisions[i].eState == Collision::eCollisionState::eLeave)
			{
				myCollisions.erase(myCollisions.begin() + i);
				continue;
			}
		}

		myCollisions[i].bHitThisFrame = false;
	}
}

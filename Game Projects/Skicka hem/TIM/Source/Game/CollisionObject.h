#pragma once
#include <Vector.h>
#include <vector>
#include "CollisionLayer.h"

class GameObject;
class BoxCollider;
class CircleCollider;
class LineCollider;

class CollisionObject
{
public:
	CollisionObject();
	virtual ~CollisionObject();

	void Update(const CommonUtilities::Vector2f& aCameraPosition);
	void UpdateCollisions();

	virtual bool TestCollision(CollisionObject* aCollisionObject) = 0;
	virtual bool TestCollision(BoxCollider* aBoxCollider) = 0;
	virtual bool TestCollision(CircleCollider* aCircleCollider) = 0;
	virtual bool TestCollision(LineCollider* aLineCollider) = 0;

	inline void SetPosition(const CommonUtilities::Vector2f& aPosition) { myPosition = aPosition; }
	inline const CommonUtilities::Vector2f GetPosition() const { return myPosition; }

	inline void SetOffset(const CommonUtilities::Vector2f& aOffset) { myOffset = aOffset; }
	inline const CommonUtilities::Vector2f GetOffset() const { return myOffset; }

	void SetOwner(GameObject* aGameObject);
	const GameObject* GetOwner() const { return myOwner; }
	GameObject* GetOwner() { return myOwner; }

	inline void SetLayer(eCollisionLayer aCollisionLayer) { myCollisionLayer = aCollisionLayer; }
	inline const eCollisionLayer GetLayer() const { return myCollisionLayer; }

	inline void SetIsSolid(bool aIsSolid) { myIsSolid = aIsSolid; }
	inline const bool GetIsSolid() const { return myIsSolid; }

	void AddCollision(CollisionObject* aOther);
	virtual void DebugDraw(const CommonUtilities::Vector2f& aCameraPosition) = 0;
	inline const bool GetIsInsideScreen() const { return myIsInsideBoundaries; }
	
	bool CircleVSCircleCollision(CommonUtilities::Vector2f aCircle1, float aCircleRadius1, CommonUtilities::Vector2f aCircle2, float aCircleRadius2);
	bool LineVSLineCollision(CommonUtilities::Vector2f aLineStart1, CommonUtilities::Vector2f aLineEnd1, CommonUtilities::Vector2f aLineStart2, CommonUtilities::Vector2f aLineEnd2);
	bool BoxVSBoxCollision(CommonUtilities::Vector2f aBox1, float aBox1Width, float aBox1Height, CommonUtilities::Vector2f aBox2, float aBox2Width, float aBox2Height);
	bool CircleVSBoxCollision(CommonUtilities::Vector2f aCircleCenter, float aCircleRadius, CommonUtilities::Vector2f aBox, float aBoxWidth, float aBoxHeight);
	bool BoxVSLineCollision(CommonUtilities::Vector2f aBox, float aBoxWidth, float aBoxHeight, CommonUtilities::Vector2f aLineStart, CommonUtilities::Vector2f aLineEnd);
	bool CircleVSLineCollision(CommonUtilities::Vector2f aCircle, float aCircleRadius, CommonUtilities::Vector2f aLineStart, CommonUtilities::Vector2f aLineEnd);
	bool PointInBox(CommonUtilities::Vector2f aBox1, float aBox1Width, float aBox1Height, CommonUtilities::Vector2f aPoint);
	bool PointInCircle(CommonUtilities::Vector2f aPoint, CommonUtilities::Vector2f aCirclePosition, float aCircleRadius);

protected:
	bool myIsSolid;
	CommonUtilities::Vector2f myPosition;
	CommonUtilities::Vector2f myOffset;
	GameObject* myOwner;
	eCollisionLayer myCollisionLayer;
	bool myIsInsideBoundaries;

	void HandleCollision();

	struct Collision
	{
		enum class eCollisionState
		{
			eEnter,
			eStay,
			eLeave,
			eDefault
		};

		CollisionObject* pOtherCollisionObject = nullptr;
		eCollisionState eState = eCollisionState::eDefault;
		bool bHitThisFrame = false;
	};

	std::vector<Collision> myCollisions;
};


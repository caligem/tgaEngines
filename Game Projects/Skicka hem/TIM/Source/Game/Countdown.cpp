#include "stdafx.h"
#include "Countdown.h"

Countdown::Countdown()
{
	myMaxTime = -1.0f;
	myCurrTime = 0.0f;
	myTrigger = nullptr;
	myType = type::oneshot;
	myFinished = false;
	myIsStarted = false;
}

Countdown::~Countdown()
{
}

void Countdown::Start()
{
	myIsStarted = true;
}

void Countdown::Stop()
{
	myIsStarted = false;
}

void Countdown::Set(const float aMaxTime, const type aCountdownType, const std::function<void()>& aTrigger, bool aTriggersOnZero)
{
	myMaxTime = aMaxTime;
	myCurrTime = 0.0f;
	myTrigger = aTrigger;
	myType = aCountdownType;
	myFinished = false;
	myIsStarted = false;
	myTriggersOnZero = aTriggersOnZero;
	myMultiplier = 1.0f;
}

void Countdown::Update(const float aDeltaTime)
{
	if (myFinished == false && myIsStarted == true)
	{

		if (myCurrTime >= myMaxTime)
		{
			if (!myTriggersOnZero)
			{
				myTrigger();
			}
			if (myType == type::autoreset)
			{
				myCurrTime = 0.0f;
			}
			else if (myType == type::oneshot)
			{
				myFinished = true;
			}
		}
		else
		{
			if (myTriggersOnZero && myCurrTime == 0.0f)
			{
				myTrigger();
			}
			myCurrTime += aDeltaTime * myMultiplier;
		}
	}
}

void Countdown::Reset()
{
	myCurrTime = 0.0f;
	myFinished = false;
	myIsStarted = false;
}

void Countdown::SetMultiplier(const float aMultiplier)
{
	myMultiplier = aMultiplier;
}

float Countdown::GetCurrentTime() const
{
	return myCurrTime;
}

float Countdown::GetMaxTime() const
{
	return myMaxTime;
}


#pragma once
#include <functional>
#undef GetCurrentTime
class Countdown
{
public:
		enum class type
		{
			//triggers once
			oneshot,
			//calls reset after triggered
			autoreset,
			//keeps calling trigger when finished
			continuous
		};
		Countdown();
		~Countdown();
		void Start();
		void Stop();
		void Set(const float aMaxTime, const type aCountdownType, const std::function<void()> &aTrigger, bool aTriggersOnZero = false);
		void Update(const float aDeltaTime);
		void Reset();
		void SetMultiplier(const float aMultiplier);

		float GetCurrentTime() const;
		float GetMaxTime() const;
	private:
		bool myIsStarted;
		bool myFinished;
		bool myTriggersOnZero;
		float myMaxTime;
		float myCurrTime;
		float myMultiplier;
		std::function<void()> myTrigger;
		type myType;
};


#include "stdafx.h"
#include "CreditsState.h"
#include "InputManager.h"
#include "StateStack.h"
#include "Fader.h"


CreditsState::CreditsState(CommonUtilities::InputManager* aInputManager, Fader* aFader)
{
	myFader = aFader;
	myInputManager = aInputManager;
	myShouldPop = false;
}

CreditsState::~CreditsState()
{
}

void CreditsState::Init()
{
	myRenderBuffer.reserve(8);

	InitButtons();

	myBackground.Init("Sprites/menu/credits.dds");
	myBackground.SetPivot({ 0.5f, 0.5f });
	myBackground.SetPosition({ 0.5f, 0.5f });
}

void CreditsState::OnEnter()
{
	myFader->SendReadyMessage();
}

void CreditsState::OnExit()
{
}

eStateStackMessage CreditsState::Update(float aDeltaTime)
{
	aDeltaTime;
	while (ShowCursor(true) <= 0);
	CleanUp();
	UpdateButtons();
	FillRenderBuffer();

	if (myInputManager->KeyPressed(CommonUtilities::Keys::Escape))
	{
		return eStateStackMessage::PopSubState;
	}
	if (myShouldPop)
	{
		return eStateStackMessage::PopSubState;
	}

	return eStateStackMessage::KeepState;
}

void CreditsState::Render()
{
	for (unsigned short i = 0; i < myRenderBuffer.size(); ++i)
	{
		myRenderBuffer[i]->Render();
	}
}

void CreditsState::CleanUp()
{
	myRenderBuffer.clear();
}

void CreditsState::FillRenderBuffer()
{
	myRenderBuffer.push_back(&myBackground);
	myBackButton.FillRenderBuffer(myRenderBuffer);
}

void CreditsState::InitSprites()
{
}

void CreditsState::InitButtons()
{
	myBackButton.Init([&] {myShouldPop = true; }, "Sprites/menu/backButton.dds", CommonUtilities::Vector2f(0.9f, 0.85f), myInputManager);
	myBackButton.AttachHoveredSprite("Sprites/menu/backButtonHover.dds");
	myBackButton.SetHitboxOffsetX(96 / 1920.f);
	myBackButton.SetHitboxOffsetY(97 / 1080.f);
}

void CreditsState::UpdateButtons()
{
	myBackButton.SetSprite(Button::eSpriteType::Default);

	if (myBackButton.IsMouseInside())
	{
		mySelectedButton = &myBackButton;
		if (myInputManager->KeyPressed(CommonUtilities::Keys::LeftMouseButton))
		{
			myBackButton.OnClick();
		}
	}
	else
	{
		mySelectedButton = nullptr;
	}

	if (myInputManager->KeyPressed(CommonUtilities::Keys::Enter))
	{
		myBackButton.OnClick();
	}

	if (mySelectedButton != nullptr)
	{
	mySelectedButton->SetSprite(Button::eSpriteType::Hovered);
	}
}

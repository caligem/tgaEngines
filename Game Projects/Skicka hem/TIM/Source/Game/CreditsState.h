#pragma once
#include "GameState.h"
#include "Button.h"
#include "Text.h"

class RenderObject;
class Fader;
class Text;

class CreditsState : public GameState
{
public:
	CreditsState(CommonUtilities::InputManager* aInputManager, Fader* aFader);
	~CreditsState();

	void Init() override;
	void OnEnter() override;
	void OnExit() override;

	eStateStackMessage Update(float aDeltaTime) override;
	void Render() override;

	inline const bool LetThroughRender() const override { return false; }

private:
	bool myShouldPop;

	void CleanUp();
	void FillRenderBuffer();
	void InitSprites();

	void InitButtons();
	void UpdateButtons();

	Sprite myBackground;

	std::vector<RenderObject*> myRenderBuffer;

	Button myBackButton;
	Button *mySelectedButton;

	Fader *myFader;
};


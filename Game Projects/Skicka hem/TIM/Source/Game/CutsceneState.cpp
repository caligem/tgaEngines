#include "stdafx.h"
#include "CutsceneState.h"
#include "Timer.h"
#include "Fader.h"
#include "InputManager.h"
#include <iostream>
#include "DataBase.h"
#include "AudioManager.h"

#define MAX_TIME_BETWEEN_FRAME 2.5f

CutsceneState::CutsceneState(eCutsceneType aCutsceneType, CommonUtilities::InputManager* aInputManager, Fader* aFader, DataBase* aDataBase)
{
	myDataBase = aDataBase;
	myInputManager = aInputManager;
	myFader = aFader;
	myCutsceneType = aCutsceneType;
}


CutsceneState::~CutsceneState()
{
	myLoadingState = nullptr;
}

void CutsceneState::Init()
{
	myStartFadeInSprite = Sprite("Sprites/faderBackground.dds");
	myStartFadeInSprite.SetColor({ 1.f, 1.f, 1.f, 1.f });
	myStartFadeInSprite.SetPivot({ 0.5f, 0.5f });
	myStartFadeInSprite.SetSizeRelativeToScreen({ 2.f, 2.f });
	myStartFadeInSprite.SetPosition({ 0.5f, 0.5f });
	myFadingSpeed = 2.f;
	myFadingTime = 0.5f;
	myState = eCutsceneState::StartFadeIn;
	mySwapingTimer = 0.f;
	LoadCutsceneByType();
}

void CutsceneState::OnEnter()
{
	AudioManager::GetInstance()->PlayMusic(music);
}

void CutsceneState::OnExit()
{
	ResetCutScene();
}

eStateStackMessage CutsceneState::Update(float aDeltaTime)
{
	while (ShowCursor(true) <= 0);

	mySwapingTimer += aDeltaTime;

	if (myState == eCutsceneState::StartFadeIn)
	{
		float startFadeValue = myStartFadeInSprite.GetColor().w;
		startFadeValue -= aDeltaTime * myFadingSpeed;
		myStartFadeInSprite.SetColor({ 1.f, 1.f, 1.f, startFadeValue });

		if (startFadeValue <= 0.f)
		{
			myState = eCutsceneState::Displaying;
		}
	}

	if (myState == eCutsceneState::Fading)
	{
		SwapSprite(aDeltaTime);
	}

	if (myState == eCutsceneState::Displaying)
	{
		if (myInputManager->KeyPressed(CommonUtilities::Keys::LeftMouseButton) || myInputManager->KeyPressed(CommonUtilities::Keys::Space) || mySwapingTimer >= MAX_TIME_BETWEEN_FRAME)
		{
			ActivateSpriteChange();
		}
	}

	if (myState == eCutsceneState::FadeOut)
	{
		if (myFader->GetFadingState() == Fader::eState::FadeIn)
		{
			myState = eCutsceneState::Finished;
		}
	}

	if (myState == eCutsceneState::Finished)
	{
		if (myCutsceneType == eCutsceneType::Intro)
		{
			if (*myLoadingState == InGameState::eLoadingState::Finished || *myLoadingState == InGameState::eLoadingState::Waiting)
			{
				return eStateStackMessage::PopSubState;
			}
		}
		else
		{
			return eStateStackMessage::PopCutsceneAndPushCredits;
		}
	}
	return eStateStackMessage::KeepState;
}

void CutsceneState::Render()
{
	mySprites.back().Render();

	myStartFadeInSprite.Render();
}



void CutsceneState::ResetCutScene()
{
	myStartFadeInSprite = Sprite("Sprites/faderBackground.dds");
	myStartFadeInSprite.SetColor({ 1.f, 1.f, 1.f, 1.f });
	myStartFadeInSprite.SetPivot({ 0.5f, 0.5f });
	myStartFadeInSprite.SetSizeRelativeToScreen({ 2.f, 2.f });
	myStartFadeInSprite.SetPosition({ 0.5f, 0.5f });
	myFadingSpeed = 2.f;
	myFadingTime = 0.5f;
	myState = eCutsceneState::StartFadeIn;

	mySprites = myCopySprites;
	mySwapingTimer = 0.f;
}

void CutsceneState::LoadCutsceneByType()
{
	if (myCutsceneType == eCutsceneType::Intro)
	{
		for (int i = static_cast<int>(myDataBase->GetData("cutscene")["Intro"].size() - 1); i >= 0; i--)
		{
			std::string spritePath = myDataBase->GetData("cutscene")["Intro"][i].get<std::string>();
			myCopySprites.push_back(Sprite(spritePath.c_str()));
			myCopySprites.back().SetPivot({ 0.5f, 0.5f });
			myCopySprites.back().SetPosition({ 0.5f, 0.5f });
			mySprites.push_back(Sprite(spritePath.c_str()));
			mySprites.back().SetPivot({ 0.5f, 0.5f });
			mySprites.back().SetPosition({ 0.5f, 0.5f });
			music = "menu";
		}
	}
	else if (myCutsceneType == eCutsceneType::Outro)
	{
		for (int i = static_cast<int>(myDataBase->GetData("cutscene")["Outro"].size() - 1); i >= 0; i--)
		{
			std::string spritePath = myDataBase->GetData("cutscene")["Outro"][i].get<std::string>();
			myCopySprites.push_back(Sprite(spritePath.c_str()));
			myCopySprites.back().SetPivot({ 0.5f, 0.5f });
			myCopySprites.back().SetPosition({ 0.5f, 0.5f });
			mySprites.push_back(Sprite(spritePath.c_str()));
			mySprites.back().SetPivot({ 0.5f, 0.5f });
			mySprites.back().SetPosition({ 0.5f, 0.5f });
			music = "menu";
		}
	}
}

void CutsceneState::SwapSprite(float aDeltaTime)
{
	if (myStartFadeInSprite.GetColor().w < 1)
	{
		float startFadeValue = myStartFadeInSprite.GetColor().w;
		startFadeValue += aDeltaTime * myFadingSpeed;
		myStartFadeInSprite.SetColor({ 1.f, 1.f, 1.f, startFadeValue });
	}
	else
	{
		myState = eCutsceneState::Displaying;
		mySprites.pop_back();
	}
}

void CutsceneState::ActivateSpriteChange()
{
	mySwapingTimer = 0.f;

	if (mySprites.size() > 1)
	{
		size_t nextLastIndex = mySprites.size() - 2;
		myStartFadeInSprite = mySprites[nextLastIndex];
		myStartFadeInSprite.SetColor({ 1.f, 1.f, 1.f, 0.f });
		myState = eCutsceneState::Fading;
	}
	else
	{
		myFader->Activate(myFadingTime, myFadingSpeed);
		myState = eCutsceneState::FadeOut;
	}
}

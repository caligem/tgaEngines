#pragma once
#include "GameState.h"
#include "InGameState.h"
#include <vector>

class Fader;
class DataBase;

class CutsceneState : public GameState
{
public:
	enum class eCutsceneType
	{
		Intro,
		Outro
	};

	enum class eCutsceneState
	{
		StartFadeIn,
		Displaying,
		Fading,
		Finished,
		FadeOut
	};

	CutsceneState(eCutsceneType aCutsceneType, CommonUtilities::InputManager* aInputManager, Fader* aFader, DataBase* aDataBase);
	~CutsceneState();

	void Init() override;

	void OnEnter() override;
	void OnExit() override;

	eStateStackMessage Update(float aDeltaTime) override;
	void Render() override;

	void AttachLoadingState(InGameState::eLoadingState &aLoadingState) { myLoadingState = &aLoadingState; }
	void ResetCutScene();
private:
	float myFadingSpeed;
	float myFadingTime;
	float mySwapingTimer;

	std::vector<Sprite> mySprites;
	std::vector<Sprite> myCopySprites;
	Sprite myStartFadeInSprite;
	eCutsceneState myState;
	eCutsceneType myCutsceneType;
	Fader* myFader;
	DataBase* myDataBase;
	InGameState::eLoadingState *myLoadingState;
	CommonUtilities::InputManager* myInputManager;

	std::string music = "";

	void LoadCutsceneByType();
	void SwapSprite(float aDeltaTime);
	void ActivateSpriteChange();
};


#include "stdafx.h"
#include "DataBase.h"
#include <fstream>

DataBase::DataBase() { }
DataBase::~DataBase() { }

void DataBase::Init(json& aJSONData)
{
	myData["json"] = aJSONData;
	ParseJSONFiles();
}

const json& DataBase::GetData(const std::string& aDataName)
{
	return myData[aDataName];
}

/* PRIVATE FUNCTIONS */

void DataBase::ParseJSONFiles()
{
	ParseDataFromJsonFile("playerData", myData["player"]);
	ParseDataFromJsonFile("enemyData", myData["enemy"]);
	ParseDataFromJsonFile("soundData", myData["sound"]);
	ParseDataFromJsonFile("itemData", myData["items"]);
	ParseDataFromJsonFile("particleData", myData["particles"]);
	ParseDataFromJsonFile("cutSceneData", myData["cutscene"]);
	// Level l�ses in separat f�r att data.json enbart pekar p� de andra level json-filerna vilket ger lite duplicerad kod.
	// Finns alternativ, ett �r att returnera json-objektet och stoppa det i <name>Data filen efter�t.
	// Det k�nns dock inte lika rent. F�r vi flera loopar kan omskrivningen vara v�rd.

	/*nlohmann::json data;
	unsigned short levelNumber = 0;

	for (auto& levelFileName : myData["json"]["levels"])
	{
		++levelNumber;
		std::string levelName = "level" + std::to_string(levelNumber);

		std::string level = levelFileName;
		std::ifstream levelsFilestream(level.c_str());
		levelsFilestream >> data;
		levelsFilestream.close();

		myData[levelName] = data;
	}*/
}

void DataBase::ParseDataFromJsonFile(const std::string& aJsonDataName, json& aDataObject)
{
	// aJsonDataName = name of the data file
	// myJSONData = jsonfile pointing to other json-files "data.json" -> "playerData.json" + "enemyData.json" + ...
	// aDataObject = where the data should end up, each run of this function puts data in 1 object, ex data from "playerData.json" to "playerData" object
	std::string file = myData["json"][aJsonDataName];
	std::ifstream filestream(file.c_str());
	filestream >> aDataObject;
	filestream.close();
}

#pragma once
#include "GrowingArray.h"
#include "json.h"
// STD
#include <map>

using namespace nlohmann;

class DataBase
{
public:
	DataBase();
	~DataBase();
	DataBase(DataBase const&) = delete;
	void operator=(DataBase const&) = delete;

	void Init(json& aJSONData);
	const json& GetData(const std::string& aDataName);
private:
	std::map<std::string, json> myData;
	void ParseJSONFiles();
	void ParseDataFromJsonFile(const std::string& aJsonDataName, json& aDataObject);
};

#include "stdafx.h"
#include "Destructible.h"
#include "CollisionManager.h"
#include "CollisionObject.h"
#include "RenderObject.h"
#include "Sprite.h"
#include "AudioManager.h"
#include "Animation.h"

Destructible::Destructible()
{
}

Destructible::Destructible(const CU::Vector2f& aPosition, CollisionManager* aCollisionManager, eLocation aLocation, eType aType)
{
	InitGO(aPosition, aCollisionManager, eCollisionLayer::Interactable);

	myAnimation = new Animation("Sprites/fx/cloud_spritesheet.dds");
	myAnimation->Setup(2, 4, 8);
	myAnimation->SetLoop(false);
	myAnimation->SetPivot({ 0.5f, 0.5f });

	std::string path = "Sprites/levels/";

	switch (aLocation)
	{
		case Destructible::eLocation::eHub:
			path += "hub_Tutorial/";
			break;
		case Destructible::eLocation::eLibrary:
			path += "library/";
			break;
		case Destructible::eLocation::eAttic:
			path += "attic/";
			break;
		case Destructible::eLocation::eMansion:
			path += "mansion/";
			break;
		case Destructible::eLocation::count:
		default:
			break;
	}

	myType = aType;
	if (aType == eType::eCrate)
	{
		path += "SmallDestructibleCrate_spritesheet.dds";
	}
	else //if (aType == eType::eVase)
	{
		path += "destructibleVase_spritesheet.dds";
	}
	mySprite = new Sprite();
	mySprite->Init(path.c_str());
	mySprite->SetPosition(aPosition);
	mySprite->SetPivot({0.5f, 0.5f});
	mySprite->SetTextureRect(0.f, 0.f, 0.5f, 1.0f);
	mySprite->SetLayerType(eLayerType::EGameObjectLayer);
	if (myType == eType::eVase)
		mySprite->SetSizeRelativeToImage({ 0.5f, 1.0f });

	CU::Vector2f posOffset = { static_cast<float>((rand() % 30 + (-15)) / 20.f) , static_cast<float>((rand() % 20 + (-10)) / 20.f) };
	myAnimation->SetPosition(aPosition + posOffset);
	myIsDead = false;
}

Destructible::~Destructible()
{
	delete mySprite;
	mySprite = nullptr;
	delete myAnimation;
	myAnimation = nullptr;
}

void Destructible::OnCollisionEnter(CollisionObject & aOther)
{
	if (aOther.GetLayer() == eCollisionLayer::Wand)
	{
		if (!myIsDead)
		{
			Destroy();
		}
	}
}

void Destructible::OnCollisionStay(CollisionObject & /*aOther*/) {}
void Destructible::OnCollisionLeave(CollisionObject & /*aOther*/) {}

void Destructible::FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer)
{
	aRenderBuffer.push_back(mySprite);
	if (myIsDead && !myAnimation->IsFinished())
	{
		aRenderBuffer.push_back(myAnimation);
	}
}

void Destructible::OnUpdate(float aDeltaTime)
{
	if (myIsDead && !myAnimation->IsFinished())
	{
		myAnimation->Update(aDeltaTime);
	}
}

void Destructible::Reset()
{
	mySprite->SetTextureRect(0.f, 0.f, 0.5f, 1.0f);
}

void Destructible::Destroy()
{
	mySprite->SetTextureRect(0.5f, 0.f, 1.0f, 1.0f);
	myIsDead = true;
	GetCollider()->SetIsSolid(false);	
	mySprite->SetLayerType(eLayerType::EBackGround);

	myAnimation->Reset();

	if (myType == eType::eCrate)
		AudioManager::GetInstance()->PlayRandomSound("crateBreak", 4);
	else //if (myType == eType::eVase)
		AudioManager::GetInstance()->PlayRandomSound("vaseBreak", 3);
}

#pragma once
#include "GameObject.h"

class CollisionManager;
class Sprite;
class Animation;

class Destructible : public GameObject
{
public:
	enum class eType 
	{
		eCrate,
		eVase,
		count
	};

	enum class eLocation
	{
		eHub,
		eLibrary,
		eAttic,
		eMansion,
		count
	};

	Destructible();
	Destructible(const CU::Vector2f &aPosition, CollisionManager* aCollisionManager, eLocation aLocation, eType aType);
	~Destructible();

	void OnCollisionEnter(CollisionObject &aOther) override;
	void OnCollisionStay(CollisionObject &aOther) override;
	void OnCollisionLeave(CollisionObject &aOther) override;

	void FillRenderBuffer(std::vector<RenderObject*> &aRenderBuffer);
	void OnUpdate(float aDeltaTime);
	void Reset();
	void Destroy();
private:
	CollisionManager* myCollisionManager;
	eType myType;
	Sprite* mySprite;
	Animation* myAnimation;
	bool myIsDead;
};

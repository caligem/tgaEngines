#include "stdafx.h"
#include "Door.h"
#include "CollisionObject.h"
#include "Player.h"
#include "AudioManager.h"
#include "PostMaster.h"

Door::Door()
{
}


Door::~Door()
{
}

void Door::Init(CollisionManager* aCollisionManager, const CommonUtilities::Vector2f& aPosition, const int aDoorID, int aGoToZoneIndex)
{
	myTag = eGameObjectTag::Door;
	InitGO(aPosition, aCollisionManager, eCollisionLayer::Terrain);

	myGoToZoneIndex = aGoToZoneIndex;
	myDoorID = aDoorID;
	myShouldChangeZone = false;
	myPlayerIsInReach = false;
	myHoldedWandConnectID = 0;
	myHoldedWandIndex = 0;
	myIsLocked = false;
}

void Door::ReceiveMessage(Message aMessage)
{
	if (
		aMessage.myMessageType == eMessageType::DoorLockUnlock && 
		aMessage.data.myInt == myDoorID)
	{
		myIsLocked = !myIsLocked;
		RenderObject* swapSprite = myDrawable;
		myDrawable = myLockedSprite;
		myLockedSprite = swapSprite;

		if (!myIsLocked)
		{
			AudioManager::GetInstance()->PlaySound("openDoor");
		}
		if (myGoToZoneIndex == -1)
		{
			GetCollider()->SetIsSolid(myIsLocked);
		}
	}

	else if (aMessage.myMessageType == eMessageType::PlayerInteract)
	{
		if (myPlayerIsInReach && MatchingConnectID(myHoldedWandConnectID) && myIsLocked)
		{
			Message message(eMessageType::KeyUsed);
			message.data.myInt = myHoldedWandIndex;
			PostMaster::SendMessages(message);

			Unlock();
		}
	}
}

void Door::OnCollisionEnter(CollisionObject& aOther)
{
	if (aOther.GetOwner()->GetTag() == eGameObjectTag::Player)
	{
		if (myIsLocked)
		{
			Player* player = dynamic_cast<Player*>(aOther.GetOwner());
			myHoldedWandConnectID = player->GetInventory().GetActiveItem()->myConnectID;
			myHoldedWandIndex = cast_i(player->GetInventory().GetActiveItemIndex());

			myPlayerIsInReach = true;

			if (MatchingConnectID(myHoldedWandConnectID))
			{
				Message message(eMessageType::ShowInteractButton);
				message.data.myVec2f = myPosition;
				PostMaster::SendMessages(message);
			}
		}
		else
		{
			if (myGoToZoneIndex != -1)
			{
				myShouldChangeZone = true;
			}
		}
	}
}

void Door::OnCollisionStay(CollisionObject & aOther)
{
	if (aOther.GetOwner()->GetTag() == eGameObjectTag::Player)
	{
		Player* player = dynamic_cast<Player*>(aOther.GetOwner());
		int currentWandIndex = cast_i(player->GetInventory().GetActiveItemIndex());

		if (myHoldedWandIndex != currentWandIndex)
		{
			myHoldedWandIndex = currentWandIndex;
			myHoldedWandConnectID = player->GetInventory().GetActiveItem()->myConnectID;

			if (MatchingConnectID(myHoldedWandConnectID))
			{
				Message message(eMessageType::ShowInteractButton);
				message.data.myVec2f = myPosition;
				PostMaster::SendMessages(message);
			}
			else
			{
				PostMaster::SendMessages(eMessageType::HideInteractButton);
			}
		}
	}
}

void Door::OnCollisionLeave(CollisionObject & aOther)
{
	if (aOther.GetOwner()->GetTag() == eGameObjectTag::Player)
	{
		PostMaster::SendMessages(eMessageType::HideInteractButton);

		myPlayerIsInReach = false;
	}
}

bool Door::ShouldChangeZone()
{
	if (myShouldChangeZone)
	{
		myShouldChangeZone = false;
		return true;
	}
	return false;
}

void Door::SetLockedSprite(const char * aPath)
{
	Sprite* lockedSprite = new Sprite(aPath, myPosition, eLayerType::EGameObjectLayer);
	assert(lockedSprite != nullptr && "Cannot find unlocked door image - is it called \"something\"_locked?");
	lockedSprite->SetPosition({ myPosition.x, myPosition.y });
	lockedSprite->SetPivot({ 0.5f, 0.5f });
	myLockedSprite = lockedSprite;
}

bool Door::MatchingConnectID(const int aKey) const
{
	return aKey == myDoorID;
}

void Door::Unlock()
{
	myIsLocked = false;
	RenderObject* swapSprite = myDrawable;
	myDrawable = myLockedSprite;
	myLockedSprite = swapSprite;

	if (myGoToZoneIndex == -1)
	{
		GetCollider()->SetIsSolid(false);
		AudioManager::GetInstance()->PlaySound("openDoor");
	}
	else
	{
		AudioManager::GetInstance()->PlaySound("menuButtonClick");
	}
}

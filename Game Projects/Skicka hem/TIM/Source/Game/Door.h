#pragma once
#include "GameObject.h"
#include "Subscriber.h"

enum class eDoorDirection
{
	Vertical,
	Horizontal
};

class Door : public GameObject, public Subscriber
{
public:
	Door();
	~Door();
	void Init(CollisionManager* aCollisionManager, const CommonUtilities::Vector2f& aPosition, const int aDoorID, int aGoToZoneIndex);

	inline int const GetDoorID() const { return myDoorID; }
	inline int const GetDoorGoToZoneIndex() const { return myGoToZoneIndex; }
	void ReceiveMessage(const Message aMessage) override;
	void OnCollisionEnter(CollisionObject &aOther) override;
	void OnCollisionStay(CollisionObject &aOther) override;
	void OnCollisionLeave(CollisionObject &aOther) override;
	bool ShouldChangeZone();
	void SetLockedSprite(const char * aPath);

private:

	bool MatchingConnectID(const int aKey) const;
	void Unlock();

	RenderObject* myLockedSprite;
	bool myShouldChangeZone;
	int myDoorID;
	int myGoToZoneIndex;
	bool myIsLocked;

	bool myPlayerIsInReach;
	int myHoldedWandConnectID;
	int myHoldedWandIndex;
};


#include "stdafx.h"
#include "Enemy.h"
#include "LineCollider.h"
#include "CircleCollider.h"
#include "AudioManager.h"

//debug
#include "Camera.h"
//

Enemy::Enemy()
{
	myCamera = nullptr;
	myIsDead = false;
}


Enemy::~Enemy()
{
}

void Enemy::AttachCamera(Camera * aCamera)
{
	myCamera = aCamera;
}

void Enemy::TakeDamage(int aDamage)
{
	if (!myIsDead)
	{
		StartHitFlash();
	}
	AudioManager::GetInstance()->PlayRandomSound("enemyHurt", 4);
	myCurrentHealth -= aDamage;
	if (myCurrentHealth <= 0)
	{
		myIsDead = true;
		Inactivate();
		AudioManager::GetInstance()->PlaySound("enemyDeath");
	}
}

bool Enemy::DidWandCollideFromBehind(const CommonUtilities::Vector2f& aWandDirection)
{
	const float angle = aWandDirection.Dot(myDirection);

	if (angle >= 0)
	{
		return true;
	}

	return false;
}

bool Enemy::RayTracePlayer(CollisionObject & aOther, const eRayTraceType aRayTraceType)
{
	if (myIsDead) return false;

	const CommonUtilities::Vector2f playerColliderPosition = aOther.GetPosition() + aOther.GetOffset();
	const CommonUtilities::Vector2f enemyPosition = GetCollider()->GetPosition() + GetCollider()->GetOffset();

	const float lengthBetweenEnemyAndPlayer = (playerColliderPosition - enemyPosition).Length();
	const CommonUtilities::Vector2f directionBetweenEnemyAndPlayer = (playerColliderPosition - enemyPosition).GetNormalized();


	// ---- 2 LINE RAY CHECK ---- //
	// ---- CHECK THIS WHEN GAME IS DONE WHICH LOOKS BETTER ----//
	/*CommonUtilities::Vector2f sideDirection = { -directionBetweenEnemyAndPlayer.y, directionBetweenEnemyAndPlayer.x };
	sideDirection.Normalize();

	const CommonUtilities::Vector2f firstPosition = enemyPosition + sideDirection * myColliderRadius * 0.70f;
	const CommonUtilities::Vector2f secondPosition = enemyPosition - sideDirection * myColliderRadius * 0.70f;

	const CommonUtilities::Vector2f playerFirstPosition = playerColliderPosition + sideDirection * static_cast<CircleCollider*>(&aOther)->GetRadius() * 0.70f;
	const CommonUtilities::Vector2f playerSecondPosition = playerColliderPosition - sideDirection * static_cast<CircleCollider*>(&aOther)->GetRadius() * 0.70f;

	const CommonUtilities::Vector2f firstDirection = (playerFirstPosition - firstPosition).GetNormalized();
	const CommonUtilities::Vector2f secondDirection = (playerSecondPosition - secondPosition).GetNormalized();
*/
	LineCollider rayTracer(enemyPosition, directionBetweenEnemyAndPlayer, lengthBetweenEnemyAndPlayer, { 0.f, 0.f });

	if (aRayTraceType == eRayTraceType::CannonBeard)
	{
		if (!myCollisionManager->DidColliderCollideWithAnythingInLayer(&rayTracer, eCollisionLayer::Terrain))
		{
			return true;
		}
	}
	else if (aRayTraceType == eRayTraceType::Stubbler)
	{
		if(!myCollisionManager->DidColliderCollideWithAnythingInLayer(&rayTracer, eCollisionLayer::Terrain) &&
			!myCollisionManager->DidColliderCollideWithAnythingInLayer(&rayTracer, eCollisionLayer::FallPit))
		{
			return true;
		}
	}
	
	return false;
}
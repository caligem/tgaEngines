#pragma once
#include "GameObject.h"
#include "json.h"
#include "AudioManager.h"
#include "Countdown.h"

//debug
class Camera;
//

class Enemy : public GameObject
{
public:
	Enemy();
	virtual ~Enemy();

	//debug
	void AttachCamera(Camera* aCamera);
	//
	virtual void Init(const nlohmann::json& aData) { aData; };
	virtual void OnUpdate(float aDeltatime) { aDeltatime; };

	int GetDamage() { return myIsDead ? 0 : myDamage; }
	virtual void TakeDamage(int aDamage);

	const int GetUnitID() { return myUnitID; }

	const bool GetIsDead() { return myIsDead; } 

	bool DidWandCollideFromBehind(const CommonUtilities::Vector2f& aWandDirection);

	enum class eOrientation
	{
		eDown,
		eLeft,
		eUp,
		eRight,
		COUNT
	};

	enum class eRayTraceType
	{
		Stubbler,
		CannonBeard
	};

protected:
	int myUnitID;

	int myDamage;
	int myMaxHealth;
	int myCurrentHealth;
	float mySightRange;
	bool myIsDead;

	float myColliderRadius;
	int myVisionColliderIndex;

	bool RayTracePlayer(CollisionObject& aOther, const eRayTraceType aRayTraceType);
	virtual void ParseJSONFile(const nlohmann::json& aJSONData) { aJSONData; };
	virtual void StartHitFlash() = 0;

	CommonUtilities::Vector2f myDirection;

	//debug
	Camera* myCamera;
	//
};
#include "stdafx.h"
#include "EnemyFactory.h"
#include "CollisionManager.h"
#include "Database.h"
#include "ProjectileManager.h"

EnemyFactory::EnemyFactory()
{
	myStubblerIndex = 0;
	myCannonBeardIndex = 0;
}


EnemyFactory::~EnemyFactory()
{
}

void EnemyFactory::Init(std::vector<GameObject*>& aGameObjectList, CollisionManager& aCollisionManager, DataBase& aDataBase, ProjectileManager& aProjectileManager)
{
	myProjectileManager = &aProjectileManager;
	myGameObjects = &aGameObjectList;
	myDataBase = &aDataBase;
	myCollisionManager = &aCollisionManager;
}

void EnemyFactory::AddEnemy(const unsigned int aUnitID, const CommonUtilities::Vector2f & aPosition)
{
	if(aUnitID == 0)
	{
		Stubbler* newStubbler = &myStubblers[myStubblerIndex];
		myStubblerIndex++;
		newStubbler->InitGO(aPosition, myCollisionManager, eCollisionLayer::Enemies);

		auto enemies = myDataBase->GetData("enemy");
		newStubbler->Init(enemies["stubbler"]);

		myGameObjects->push_back(newStubbler);
	}
	else if (aUnitID == 1)
	{
		CannonBeard* newCannonBeard = &myCannonBeards[myCannonBeardIndex];
		myCannonBeardIndex++;
		newCannonBeard->InitGO(aPosition, myCollisionManager, eCollisionLayer::Enemies);

		auto enemies = myDataBase->GetData("enemy");
		newCannonBeard->Init(enemies["cannonBeard"], *myProjectileManager);

		myGameObjects->push_back(newCannonBeard);
	}
	else if (aUnitID == 2)
	{
		Stubbler* newStubbler = &myStubblers[myStubblerIndex];
		myStubblerIndex++;
		newStubbler->InitGO(aPosition, myCollisionManager, eCollisionLayer::Enemies);

		auto enemies = myDataBase->GetData("enemy");
		newStubbler->Init(enemies["stubblerElite"]);

		myGameObjects->push_back(newStubbler);
	}
	else if (aUnitID == 3)
	{
		CannonBeard* newCannonBeard = &myCannonBeards[myCannonBeardIndex];
		myCannonBeardIndex++;
		newCannonBeard->InitGO(aPosition, myCollisionManager, eCollisionLayer::Enemies);

		auto enemies = myDataBase->GetData("enemy");
		newCannonBeard->Init(enemies["cannonBeardElite"], *myProjectileManager);

		myGameObjects->push_back(newCannonBeard);
	}
}

Enemy* EnemyFactory::AddEnemyBossCall(const unsigned int aUnitID, const CommonUtilities::Vector2f & aPosition)
{
	if (aUnitID == 0)
	{
		Stubbler* newStubbler = &myStubblers[myStubblerIndex];
		myStubblerIndex++;
		newStubbler->InitGO(aPosition, myCollisionManager, eCollisionLayer::Enemies);

		auto enemies = myDataBase->GetData("enemy");
		newStubbler->Init(enemies["stubbler"]);

		myGameObjects->push_back(newStubbler);
	}
	else if (aUnitID == 1)
	{
		CannonBeard* newCannonBeard = &myCannonBeards[myCannonBeardIndex];
		myCannonBeardIndex++;
		newCannonBeard->InitGO(aPosition, myCollisionManager, eCollisionLayer::Enemies);

		auto enemies = myDataBase->GetData("enemy");
		newCannonBeard->Init(enemies["cannonBeard"], *myProjectileManager);

		myGameObjects->push_back(newCannonBeard);
	}
	else if (aUnitID == 2)
	{
		Stubbler* newStubbler = &myStubblers[myStubblerIndex];
		myStubblerIndex++;
		newStubbler->InitGO(aPosition, myCollisionManager, eCollisionLayer::Enemies);

		auto enemies = myDataBase->GetData("enemy");
		newStubbler->Init(enemies["stubblerElite"]);

		myGameObjects->push_back(newStubbler);
	}
	else if (aUnitID == 3)
	{
		CannonBeard* newCannonBeard = &myCannonBeards[myCannonBeardIndex];
		myCannonBeardIndex++;
		newCannonBeard->InitGO(aPosition, myCollisionManager, eCollisionLayer::Enemies);

		auto enemies = myDataBase->GetData("enemy");
		newCannonBeard->Init(enemies["cannonBeardElite"], *myProjectileManager);

		myGameObjects->push_back(newCannonBeard);
	}
	return static_cast<Enemy*>(myGameObjects->back());
}

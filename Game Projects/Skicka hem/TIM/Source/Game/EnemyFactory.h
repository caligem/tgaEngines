#pragma once
#include <vector>
#include <array>
#include "Stubbler.h"
#include "CannonBeard.h"

class Enemy;
class GameObject;
class CollisionManager;
class DataBase;
class ProjectileManager;

class EnemyFactory
{
public:
	EnemyFactory();
	~EnemyFactory();

	void Init(std::vector<GameObject*>& aGameObjectList, CollisionManager& aCollisionManager, DataBase& aDataBase, ProjectileManager& aProjectileManager);
	void AddEnemy(const unsigned int aUnitID, const CommonUtilities::Vector2f& aPosition);
	Enemy* AddEnemyBossCall(const unsigned int aUnitID, const CommonUtilities::Vector2f& aPosition);

private:
	CollisionManager* myCollisionManager;
	DataBase* myDataBase;
	ProjectileManager* myProjectileManager;

	std::vector<GameObject*>* myGameObjects;
	std::array<Stubbler, 256> myStubblers;
	std::array<CannonBeard, 256> myCannonBeards;
	int myStubblerIndex;
	int myCannonBeardIndex;
};


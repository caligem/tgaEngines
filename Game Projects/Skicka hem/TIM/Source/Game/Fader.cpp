#include "stdafx.h"
#include "Fader.h"
#include "Text.h"

Fader::Fader()
{
}

Fader::~Fader()
{
	if (myText != nullptr)
	{
		delete myText;
		myText = nullptr;
	}
}

void Fader::Init()
{
	myShouldRender = false;
	myState = eState::Inactive;
	myGameIsReady = false;
	myFadeSpeed = 0.f;


	myFadeBackground.Init("Sprites/faderBackground.dds");
	myFadeBackground.SetPivot({ 0.5f, 0.5f });
	myFadeBackground.SetPosition({ 0.5f, 0.5f });
	myFadeBackground.SetSizeRelativeToScreen({ 2.f, 2.f });
	myFadeBackground.SetColor({ 1.f, 1.f, 1.f, 0.f });

	myText = new Text("Text/Bradley Gratis.ttf", false, Tga2D::EFontSize_24);
}

void Fader::Activate(const float aTime, const float aSpeed, const char* aText, const CommonUtilities::Vector2f& aTextPosition, const CommonUtilities::Vector4f& aTextColor)
{
	if (aText != nullptr)
	{
		myText->SetText(aText);
		myText->SetPosition({ aTextPosition.x - (myText->GetWidth() / 2.f), aTextPosition.y });
		myText->SetColor(aTextColor);
	}

	myDeactivateTimer.Set(aTime, Countdown::type::oneshot, [&] { Deactivate(); });
	myDeactivateTimer.Start();
	myFadeSpeed = aSpeed;
	myGameIsReady = false;
	myShouldRender = true;
	myState = eState::FadeOut;
}

void Fader::Deactivate()
{
	if (myState != eState::FadeOut)
	{
		return;
	}

	myState = eState::FadeIn;
}

void Fader::SendReadyMessage()
{
	myGameIsReady = true;
}

void Fader::Update(float aDeltaTime)
{
	myDeactivateTimer.Update(aDeltaTime);

	if (myState == eState::FadeOut)
	{
		const float backgroundAlpha = myFadeBackground.GetColor().w;

		if (backgroundAlpha < 1.f)
		{
			myFadeBackground.SetColor({ 1.f, 1.f, 1.f, backgroundAlpha + (aDeltaTime * myFadeSpeed) });
		}
		else if (backgroundAlpha > 0.5f)
		{
			const float textAlpha = myText->GetColor().w;

			if (textAlpha < 1.f)
			{
				myText->SetColor({ 1.f, 1.f, 1.f, textAlpha + (aDeltaTime * myFadeSpeed) });
			}
		}
	}
	else if (myState == eState::FadeIn)
	{
		if (myGameIsReady)
		{
			const float alpha = myFadeBackground.GetColor().w;

			if (alpha > 0.f)
			{
				myFadeBackground.SetColor({ 1.f, 1.f, 1.f, alpha - (aDeltaTime * myFadeSpeed) });
				myText->SetColor({ 1.f, 1.f, 1.f, alpha - (aDeltaTime * myFadeSpeed * 5.f) });
			}
			else
			{
				Done();
			}
		}
	}
}

void Fader::Render()
{
	if (myShouldRender)
	{
		myFadeBackground.Render();

		if (strlen(myText->GetText()) != 0)
		{
			myText->Render();
		}
	}
}

void Fader::Done()
{
	myDeactivateTimer.Reset();
	myShouldRender = false;
	myState = eState::Inactive;
	myGameIsReady = false;
	myText->SetText("");
	myFadeSpeed = 0.0f;
}

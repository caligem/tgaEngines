#pragma once
#include "Sprite.h"
#include "Countdown.h"

class Text;

class Fader
{
public:
	enum class eState
	{
		Inactive,
		FadeIn,
		FadeOut

	};

	Fader();
	~Fader();

	void Init();

	void Activate(const float aTime, const float aSpeed, const char* aText = nullptr, const CommonUtilities::Vector2f& aTextPosition = { 0.5f, 0.5f }, const CommonUtilities::Vector4f& aTextColor = {1.f, 1.f, 1.f, 0.f});
	void Deactivate();
	void SendReadyMessage();

	void Update(float aDeltaTime);
	void Render();

	inline const eState GetFadingState() const { return myState; }

private:
	void Done();

	eState myState;
	
	bool myGameIsReady;
	bool myShouldRender;
	float myFadeSpeed;
	Countdown myDeactivateTimer;
	Sprite myFadeBackground;
	Text* myText;
};


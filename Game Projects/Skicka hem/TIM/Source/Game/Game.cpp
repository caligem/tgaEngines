#include "stdafx.h"
#include <tga2d/Engine.h>
#include "Game.h"
#include <tga2d/error/error_manager.h>

using namespace std::placeholders;

#ifdef _DEBUG
#pragma comment(lib,"DX2DEngine_Debug.lib")
#endif // DEBUG
#ifdef _RELEASE
#pragma comment(lib,"DX2DEngine_Release.lib")
#endif // DEBUG
#ifdef _RETAIL
#pragma comment(lib,"DX2DEngine_Retail.lib")
#endif // DEBUG

CGame::CGame()
	: myGameWorld(&myInputManager)
{
}


CGame::~CGame()
{
}


bool CGame::Init(const std::wstring& aVersion, HWND aHWND)
{
	unsigned short windowWidth = 1920;
	unsigned short windowHeight = 1080;

	Tga2D::SEngineCreateParameters createParameters;
	createParameters.myActivateDebugSystems = Tga2D::eDebugFeature_Fps | Tga2D::eDebugFeature_Mem | Tga2D::eDebugFeature_Filewatcher | Tga2D::eDebugFeature_Cpu | Tga2D::eDebugFeature_Drawcalls | Tga2D::eDebugFeature_OptimiceWarnings;

	createParameters.myInitFunctionToCall = std::bind(&CGame::InitCallBack, this);
	createParameters.myUpdateFunctionToCall = std::bind(&CGame::UpdateCallBack, this);
	createParameters.myWinProcCallback = std::bind(&CGame::WinProc, this, _1, _2, _3, _4);
	createParameters.myLogFunction = std::bind(&CGame::LogCallback, this, _1);
	createParameters.myWindowHeight = windowHeight;
	createParameters.myWindowWidth = windowWidth;
	createParameters.myRenderHeight = windowHeight;
	createParameters.myRenderWidth = windowWidth;
	createParameters.myTargetWidth = windowWidth;
	createParameters.myTargetHeight = windowHeight;
	createParameters.myWindowSetting = Tga2D::EWindowSetting_Overlapped;
	//createParameters.myAutoUpdateViewportWithWindow = true;
	createParameters.myClearColor.Set(0.0f / 255.0f, 0.f / 255.0f, 0.f / 255.0f, 1.0f);
	//createParameters.myClearColor.Set(0.0f, 0, 0 , 1.0f);
#ifndef _RETAIL
	createParameters.myStartInFullScreen = false;
#else
	createParameters.myStartInFullScreen = true;
#endif

	if (aHWND != nullptr)
	{
		createParameters.myHwnd = new HWND(aHWND);
	}

#ifdef _RELEASE
	std::wstring appname = L" - RELEASE v[" + aVersion + L"] ";
#endif
#ifdef _RETAIL
	std::wstring appname = L" v[" + aVersion + L"] ";
#endif // _RETAIL
#ifdef _DEBUG
	std::wstring appname = L" - DEBUG v[" + aVersion + L"] ";
#endif

	createParameters.myApplicationName = L"Wisest Wizard" + appname;
	createParameters.myEnableVSync = false;
	
	Tga2D::CEngine::CreateInstance(createParameters);
	if (!Tga2D::CEngine::GetInstance()->Start())
	{
		ERROR_PRINT("Fatal error! Engine could not start!");
		system("pause");
		return false;
	}

	return true;
}


LRESULT CGame::WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	lParam;
	wParam;
	hWnd;
	
	myInputManager.HandleMessage(hWnd, message, wParam, lParam);

	//myWindowHandle = Tga2D::CEngine::GetInstance()->GetHWND();
	//if (myWindowHandle != nullptr)
	//	GetClientRect(*myWindowHandle, &myWindowRect);

	switch (message)
	{
		// this message is read when the window is closed
		//case WM_MOUSEMOVE:
		//{
		//	if (GetActiveWindow() == hWnd)
		//	{
		//		ClipCursor(&myWindowRect);
		//	}
		//	break;
		//}
		case WM_DESTROY:
		{
			// close the application entirely
			myGameWorld.TerminateThreads();
			PostQuitMessage(0);
			return 0;
		}
	}

	return 0;
}

void CGame::InitCallBack()
{
	myCursor = LoadCursorFromFile(L"Sprites/UI/cursor.cur");
	myGameWorld.Init();
}

void CGame::UpdateCallBack()
{
	SetCursor(myCursor);
	myTimer.Update();
	float deltaTime = myTimer.GetDeltaTime();
#ifdef _DEBUG
	const float maxDeltaTime = 1.f / 30.f;
#else
	const float maxDeltaTime = 1.f / 60.f;
#endif
	deltaTime = (deltaTime > maxDeltaTime) ? maxDeltaTime : deltaTime;
	myGameWorld.Update(deltaTime);
	myGameWorld.Render();
	myInputManager.Update();
}

void CGame::LogCallback(std::string aText)
{
}

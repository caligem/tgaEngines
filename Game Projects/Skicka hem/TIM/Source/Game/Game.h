#pragma once
#include <tga2d/Engine.h>
#include <fstream>
#include "GameWorld.h"

#include <Timer.h>
#include <InputManager.h>

class CGame
{
public:
	CGame();
	~CGame();
	bool Init(const std::wstring& aVersion = L"", HWND aHWND = nullptr);
private:
	LRESULT WinProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
	void InitCallBack();
	void UpdateCallBack();
	void LogCallback(std::string aText);

	CGameWorld myGameWorld;
	CommonUtilities::InputManager myInputManager;
	CommonUtilities::Timer myTimer;
	HCURSOR myCursor;
	//HWND* myWindowHandle = nullptr;
	//RECT myWindowRect;
};

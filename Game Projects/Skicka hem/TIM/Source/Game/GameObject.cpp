#include "stdafx.h"
#include "GameObject.h"
#include "Sprite.h"
#include "CollisionObject.h"
#include "Emitter.h"

GameObject::GameObject()
{
	myTag = eGameObjectTag::None;
	myShouldRemove = false;
	myZ = 0.0f;
	myCollisionManager = nullptr;
	myCollisionLayer = eCollisionLayer::None;
	myDrawable = nullptr;
	myColliderIndex = -1;
}

void GameObject::InitGO(const CommonUtilities::Vector2f &aPosition, CollisionManager * aCollisionManager, const eCollisionLayer& aCollisionLayer)
{
	myCollisionLayer = aCollisionLayer;
	myCollisionManager = aCollisionManager;
	myPosition = aPosition;
	myRotation = 0.0f;
	myOrginalPosition = myPosition;
}

void GameObject::InitGO(const CommonUtilities::Vector2f & aPosition)
{
	myPosition = aPosition;
	myRotation = 0.0f;
	myOrginalPosition = myPosition;
}

GameObject::~GameObject()
{
	if (myDrawable != nullptr)
	{
		delete myDrawable;
		myDrawable = nullptr;
	}

	if (myColliderIndex != -1)
	{
		myCollisionManager->RemoveCollider(myCollisionLayer, myColliderIndex);
		myColliderIndex = -1;
	}
}

void GameObject::AddSprite(const char * aPath)
{
	myDrawable = new Sprite(aPath, myPosition, eLayerType::EGameObjectLayer);
	myDrawable->SetPosition({ myPosition.x, myPosition.y });
}

void GameObject::SetTag(const eGameObjectTag aGameObjectTag)
{
	myTag = aGameObjectTag;
}

void GameObject::UpdateAllGameObjects(std::vector<GameObject*>& aGameObjects, float aDeltaTime)
{
	for (size_t i = 0; i < aGameObjects.size(); i++)
	{
		if (aGameObjects[i]->GetShouldRemove())
		{
			continue;
		}
		
		aGameObjects[i]->OnUpdate(aDeltaTime);
	}
}

void GameObject::Reset()
{
	myPosition = myOrginalPosition;
}

void GameObject::AddBoxCollider(float aHeight, float aWidth, const CommonUtilities::Vector2f & aOffset)
{
	assert(myCollisionManager != nullptr && "forgot to send in collision manager to gameobject");
	myColliderIndex = myCollisionManager->AddBoxCollider(myCollisionLayer, aHeight, aWidth, aOffset);
	myCollisionManager->AttachGameObject(myCollisionLayer, myColliderIndex, this);
}

void GameObject::AddCircleCollider(float aRadius, const CommonUtilities::Vector2f & aOffset)
{
	assert(myCollisionManager != nullptr && "forgot to send in collision manager to gameobject");
	myColliderIndex = myCollisionManager->AddCircleCollider(myCollisionLayer, aRadius, aOffset);
	myCollisionManager->AttachGameObject(myCollisionLayer, myColliderIndex, this);
}

void GameObject::AddLineCollider(const CommonUtilities::Vector2f & aDirection, float aLength, const CommonUtilities::Vector2f & aOffset, const CommonUtilities::Vector2f& aPosition)
{
	assert(myCollisionManager != nullptr && "forgot to send in collision manager to gameobject");
	myColliderIndex = myCollisionManager->AddLineCollider(myCollisionLayer, aDirection, aLength, aOffset, aPosition);
	myCollisionManager->AttachGameObject(myCollisionLayer, myColliderIndex, this);
}

void GameObject::FillRenderBuffer(std::vector<RenderObject*> &aRenderBuffer)
{
	if (myDrawable != nullptr)
	{
		myDrawable->SetPosition({ myPosition.x, myPosition.y });
		aRenderBuffer.push_back(myDrawable);
		for (auto& emitter : myEmitters)
		{
			aRenderBuffer.push_back(emitter);
		}
	}
}

const CommonUtilities::Vector2f & GameObject::GetPosition() const
{
	return myPosition;
}

RenderObject& GameObject::AccessDrawable()
{
	return *myDrawable;
}

void GameObject::SetPosition(const CommonUtilities::Vector2f & aPosToSet)
{
	myPosition = aPosToSet;
	if (myDrawable != nullptr)
	{
		myDrawable->SetPosition({ myPosition.x, myPosition.y });
	}
}

void GameObject::SetRotation(const float aRotation)
{
	myRotation = aRotation;
	myDrawable->SetRotation(myRotation);
}

float GameObject::GetRotation()
{
	return myRotation;
}

CollisionObject* GameObject::GetCollider()
{
	return myCollisionManager->GetCollisionObject(myCollisionLayer, myColliderIndex);
}

bool GameObject::GetShouldRemove()
{
	return myShouldRemove;
}

void GameObject::SetShouldRemove(const bool aShouldRemove)
{
	myShouldRemove = aShouldRemove;
}

void GameObject::Inactivate()
{
	if (myColliderIndex != -1)
	{
		GetCollider()->SetIsSolid(false);
	}
	
	myShouldRemove = true;
}

void GameObject::Reactivate()
{
	if (myColliderIndex != -1)
	{
		GetCollider()->SetIsSolid(true);
	}

	myShouldRemove = false;
}

void GameObject::OnCollisionEnter(CollisionObject  &aOther)
{
	aOther;
}

void GameObject::OnCollisionStay(CollisionObject  &aOther)
{
	aOther;
}

void GameObject::OnCollisionLeave(CollisionObject  &aOther)
{
	aOther;
}

void GameObject::OnUpdate(float aDeltaTime)
{
	aDeltaTime;
	if (myDrawable != nullptr)
	{
		myDrawable->SetPosition({ myPosition.x, myPosition.y });
	}
	for (auto& emitter : myEmitters)
	{
		emitter->Update(aDeltaTime);
	}
}

void GameObject::SetOriginalPosition()
{
	myOrginalPosition = myPosition;
}

GameObject* GameObject::Clone()
{
	GameObject* newClone = new GameObject(*this);
	return newClone;
}

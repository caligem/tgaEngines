#pragma once
#include <Vector.h>
#include <vector>
#include "CollisionManager.h"
#include "RenderObject.h"

class CollisionObject;
class Emitter;

enum class eGameObjectTag
{
	None,
	Player,
	Stubbler,
	CannonBeard,
	Projectile,
	Door,
	Chest,
	Lever,
	Item,
	Wand,
	Activator,
	BookOfClassChange,
	Trigger
};


class GameObject
{
public:
	GameObject();
	virtual ~GameObject();

	void InitGO(const CommonUtilities::Vector2f &aPosition, CollisionManager * aCollisionManager, const eCollisionLayer& aCollisionLayer);
	void InitGO(const CommonUtilities::Vector2f &aPosition);

	void AddSprite(const char* aPath);
	RenderObject& AccessDrawable();

	void SetTag(const eGameObjectTag aGameObjectTag);
	const eGameObjectTag GetTag() const { return myTag; }

	static void UpdateAllGameObjects(std::vector<GameObject*>& aGameObjects, float aDeltaTime);
	virtual void FillRenderBuffer(std::vector<RenderObject*> &aRenderBuffer);
	virtual void OnTriggerd() {};
	virtual void OnUpdate(float aDeltaTime);

	virtual void Reset();

	void AddBoxCollider(float aHeight, float aWidth, const CommonUtilities::Vector2f& aOffset);
	void AddCircleCollider(float aRadius, const CommonUtilities::Vector2f& aOffset);
	void AddLineCollider(const CommonUtilities::Vector2f& aDirection, float aLength, const CommonUtilities::Vector2f& aOffset, const CommonUtilities::Vector2f& aPosition);
	CollisionObject* GetCollider();
	inline const eCollisionLayer GetCollisionLayer() const { return myCollisionLayer; }

	virtual void SetOriginalPosition();
	void SetPosition(const CommonUtilities::Vector2f &aPosToSet);
	const CommonUtilities::Vector2f & GetPosition() const;

	void SetRotation(const float aRotation);
	float GetRotation();

	bool GetShouldRemove();
	void SetShouldRemove(const bool aShouldRemove);

	virtual void Inactivate();
	virtual void Reactivate();

	virtual GameObject* Clone();

protected:
	eGameObjectTag myTag;
	int myColliderIndex;
	eCollisionLayer myCollisionLayer;
	CollisionManager* myCollisionManager;

	RenderObject* myDrawable;
	float myZ;
	float myRotation;
	CommonUtilities::Vector2f myPosition;
	CommonUtilities::Vector2f myOrginalPosition;

	bool myShouldRemove;

	friend class CollisionObject;
	virtual void OnCollisionEnter(CollisionObject &aOther);
	virtual void OnCollisionStay(CollisionObject &aOther);
	virtual void OnCollisionLeave(CollisionObject &aOther);

	std::vector<Emitter*> myEmitters;
};


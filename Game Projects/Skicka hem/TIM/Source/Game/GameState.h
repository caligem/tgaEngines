#pragma once
#include "Sprite.h"

class StateStack;

namespace CommonUtilities
{
	class InputManager;
}

enum class eStateStackMessage
{
	KeepState,
	PopMainState,
	PopSubState,
	PopCutsceneAndPushCredits
};


class GameState
{
public:
	GameState();
	virtual ~GameState();

	virtual void Init() = 0;

	virtual void OnEnter() = 0;
	virtual void OnExit() = 0;

	virtual eStateStackMessage Update(float aDeltaTime) = 0;
	virtual void Render() = 0;

	inline void AttachStateStack(StateStack* aStateStack) { myStateStack = aStateStack; }
	inline virtual const bool LetThroughRender() const { return false; }

	virtual void TerminateThread() {};
	inline bool ShouldDelete() { return myShouldDelete; }
protected:
	CommonUtilities::InputManager* myInputManager;
	StateStack* myStateStack;
	bool myShouldDelete = true;
};


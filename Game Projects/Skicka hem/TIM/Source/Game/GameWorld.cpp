#include "stdafx.h"
#include "GameWorld.h"
#include <tga2d/sprite/sprite.h>
#include <tga2d/Engine.h>
#include "PostMaster.h"
#include "InputManager.h"
#include "AudioManager.h"
#include "Timer.h"

CGameWorld::CGameWorld(CommonUtilities::InputManager* aInputManager)
	: myInputManager(aInputManager)
{
	myFader = nullptr;
	AudioManager::Create();
}

CGameWorld::~CGameWorld()
{
	if (myFader != nullptr)
	{
		delete myFader;
		myFader = nullptr;
	}

	PostMaster::Destroy();
	AudioManager::Destroy();
}

void CGameWorld::Init()
{
	srand(cast_ui(time(0)));
	myFader = new Fader();
	myFader->Init();

	std::ifstream fs("Data/data.json");
	fs >> myJSONData;
	fs.close();
	myDatabase.Init(myJSONData["root"]["files"]);

	myStateStack.Init(myInputManager, myFader, &myDatabase);

	PostMaster::Create();
	myStateStack.PushMainState(new MainMenuState(myInputManager, myFader, &myDatabase));

	//TODO: INL�MNING
#ifdef _RETAIL
	myStateStack.PushSubState(new SplashScreenState(myInputManager, myFader));
#endif
}

void CGameWorld::Update(float aDeltaTime)
{
	myFader->Update(aDeltaTime);

	if (myStateStack.GetSize() > 0)
	{
		eStateStackMessage returnState = myStateStack.GetCurrentState()->Update(aDeltaTime);
		if (returnState == eStateStackMessage::PopMainState)
		{
			myStateStack.PopMainState();
		}
		else if (returnState == eStateStackMessage::PopSubState)
		{
			myStateStack.PopSubState();
		}
		else if (returnState == eStateStackMessage::PopCutsceneAndPushCredits)
		{
			myStateStack.PopMainState();
			myStateStack.PushSubState(new CreditsState(myInputManager, myFader));
		}
	}
	else
	{
		Tga2D::CEngine::GetInstance()->Shutdown();
	}
}

void CGameWorld::Render()
{
	if (myStateStack.GetSize() != 0)
	{
		myStateStack.Render();
	}

	myFader->Render();
}

void CGameWorld::TerminateThreads()
{
	myStateStack.TerminateThreads();
}

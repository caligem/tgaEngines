#pragma once
#include "StateStack.h"
#include "json.h"
#include "Fader.h"
#include "DataBase.h"

namespace CommonUtilities
{
	class InputManager;
}

namespace Tga2D
{
	class CSprite;
}

class CGameWorld
{
public:
	CGameWorld(CommonUtilities::InputManager* aInputManager);
	~CGameWorld();

	void Init();
	void Update(float aTimeDelta);
	void Render();
	void TerminateThreads();
private:
	DataBase myDatabase;
	StateStack myStateStack;
	nlohmann::json myJSONData;
	CommonUtilities::InputManager* myInputManager;
	Fader* myFader;
};
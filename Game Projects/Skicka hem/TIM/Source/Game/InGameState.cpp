#include "stdafx.h"
#include "InGameState.h"
#include "StateStack.h"
#include "InputManager.h"
#include "Fader.h"
#include "PostMaster.h"
#include "AudioManager.h"
#include "StateStack.h"

InGameState::InGameState(CommonUtilities::InputManager* aInputManager, Fader* aFader, DataBase *aDatabase)
	: myLoadingThread(&InGameState::InitZones, this)
	, myInventoryState(aInputManager, myInventory)
	, myNextDoorIndex(-1)
	, myUI(myInventory, aInputManager)
{
	myFader = aFader;
	myInputManager = aInputManager;
	myDatabase = aDatabase;

	myShouldPop = false;
	myShowDebugLines = false;

	myCurrentZoneIndex = eZoneType::None;

	myShouldFinishTransition = false;
	myShouldPlayOutro = false;
	myNextZoneIndex = eZoneType::None;
}

InGameState::~InGameState()
{
#ifndef _RETAIL
	if (zoneIndex != nullptr)
	{
		delete zoneIndex;
		zoneIndex = nullptr;
	}
#endif
}

void InGameState::Init()
{
	myZoneNames.insert(std::pair<int, const char*>(1, "Entrance"));
	myZoneNames.insert(std::pair<int, const char*>(2, "Library"));
	myZoneNames.insert(std::pair<int, const char*>(3, "Attic"));
	myZoneNames.insert(std::pair<int, const char*>(4, "Mansion"));
	myStateStack->PushIntroCutscene(myLoadingState);
}

void InGameState::OnEnter()
{
	myFader->SendReadyMessage();
}

void InGameState::OnExit()
{
	PostMaster::UnSubscribe(eMessageType::PopUpTextBox);
	PostMaster::UnSubscribe(eMessageType::PlayerTeleport);
	PostMaster::UnSubscribe(eMessageType::PlayerInteract);
	PostMaster::UnSubscribe(eMessageType::DoorLockUnlock);
	PostMaster::UnSubscribe(eMessageType::ResetEnemies);
	PostMaster::UnSubscribe(eMessageType::FoundWand);
	PostMaster::UnSubscribe(eMessageType::ShowInteractButton);
	PostMaster::UnSubscribe(eMessageType::HideInteractButton);
	PostMaster::UnSubscribe(eMessageType::KeyUsed);
	PostMaster::UnSubscribe(eMessageType::Outro);
}

eStateStackMessage InGameState::Update(float aDeltaTime)
{
	while (ShowCursor(true) <= 0);
	if (myLoadingState == eLoadingState::Waiting)
	{
		myCurrentZone->Update(aDeltaTime);
		AudioManager::GetInstance()->Update();

		const eZoneType changeZoneType = myCurrentZone->ChangeZoneType();

		if (changeZoneType != eZoneType::None)
		{
			BeginTransition(changeZoneType);
		}
		if (myInputManager->KeyPressed(CU::Keys::I))
		{
			myStateStack->PushSubState(&myInventoryState);
		}

		if (myShouldFinishTransition)
		{
			FinishTransition();
		}

		if (myShouldPlayOutro)
		{
			FinishOutroTransition();
		}


		if (myInputManager->KeyPressed(CommonUtilities::Keys::Escape))
		{
			myStateStack->PushSubState(new PauseMenuState(myInputManager, myFader));
		}

#ifndef _RETAIL
		if (myInputManager->KeyPressed(CommonUtilities::Keys::F5))
		{	
			myStateStack->PushOutroCutscene();
		}
#endif

#ifndef _RETAIL
		if (myInputManager->KeyPressed(CommonUtilities::Keys::F4))
		{
			if (myCurrentZone == &myZones.back())
			{
				BeginTransition(static_cast<eZoneType>(0));
			}
			else
			{
				const int nextZoneIndex = static_cast<int>(myCurrentZoneIndex) + 1;
				BeginTransition(static_cast<eZoneType>(nextZoneIndex));
			}
		}
#endif

#ifndef _RETAIL
		if (myInputManager->KeyPressed(CommonUtilities::Keys::F3))
		{
			if (myShowDebugLines)
			{
				myShowDebugLines = false;
			}
			else
			{
				myShowDebugLines = true;
			}
		}
#endif
		if (myShouldPop)
		{
			return eStateStackMessage::PopMainState;
		}
	}
	else if (myLoadingState == eLoadingState::Finished)
	{
		myLoadingThread.join();
		myLoadingState = eLoadingState::Waiting;
	}
	else if (myLoadingState == eLoadingState::Loading)
	{
		return eStateStackMessage::KeepState;
	}


	return eStateStackMessage::KeepState;
}

void InGameState::Render()
{
	myRenderBuffer.clear();

	myCamera.RenderMapUnderPlayer(myCurrentZone->GetMap());
	myCurrentZone->FillRenderBuffer(myRenderBuffer);
	myUI.FillRenderBuffer(myRenderBuffer);
	myCamera.RenderBuffer(myRenderBuffer);
	myCamera.RenderMapOverPlayer();
	
	myUI.Render();

#ifndef _RETAIL
	if (myShowDebugLines)
	{
		myCurrentZone->DrawColliderDebugLines(myCamera.GetPos());
	}

	zoneIndex->Render();
#endif
}

void InGameState::TerminateThread()
{
	if (myLoadingThread.joinable())
	{
		myLoadingThread.join();
	}
}

void InGameState::ReceiveMessage(const Message aMessage)
{
	if (aMessage.myMessageType == eMessageType::PopUpTextBox)
	{
		myStateStack->PushSubState(new PopUpTextBoxState(myInputManager, aMessage));
	}
	else if (aMessage.myMessageType == eMessageType::Outro)
	{
		myFader->Activate(2.f, 1.f);
		myShouldPlayOutro = true;
	}
	else if (aMessage.myMessageType == eMessageType::ResetEnemies)
	{
		myZones[static_cast<int>(myCurrentZoneIndex)].ResetEnemies();
	}
}

void InGameState::InitZones()
{
#ifndef _RETAIL
	zoneIndex = new Tga2D::CText("Text/arial.ttf");
	zoneIndex->myText = "CurrentZoneIndex: " + std::to_string(static_cast<int>(myCurrentZoneIndex));
	zoneIndex->myPosition = { 0.45f, 0.1f };
	zoneIndex->myColor = { 1.f, 0.f, 0.f, 1.f };
#endif

	myInventory.Init(myDatabase->GetData("items"), myInputManager);

	myUI.Init();
	myRenderBuffer.reserve(512);
	PostMaster::Subscribe(eMessageType::PopUpTextBox, this);
	PostMaster::Subscribe(eMessageType::Outro, this);
	PostMaster::Subscribe(eMessageType::HideInteractButton, &myUI);
	PostMaster::Subscribe(eMessageType::ShowInteractButton, &myUI);
	PostMaster::Subscribe(eMessageType::ResetEnemies, this);
	PostMaster::Subscribe(eMessageType::KeyUsed, &myInventory);
	PostMaster::Subscribe(eMessageType::FoundWand, &myInventory);
	myZones.reserve(8);

	myLoadingState = eLoadingState::Loading;
	std::fstream pathsFile("Data/Zones/zonePaths.json");
	json paths;
	pathsFile >> paths;
	pathsFile.close();

	for (size_t zone = 0; zone < paths["ZonePaths"].size(); zone++)
	{
		std::fstream mapTiled(paths["ZonePaths"][zone].get<std::string>());
		json mapJson;
		mapTiled >> mapJson;
		mapTiled.close();

		myZones.push_back(Zone(myInventory, myUI));
		myZones.back().Init(static_cast<eZoneType>(zone), mapJson, myInputManager, myFader, &myCamera, myDatabase, myConnections, &myParticleSys);
	}

	myCurrentZone = &myZones[0];

	auto search = myConnections.find(0);
	if (search != myConnections.end())
	{
		myCurrentZone->EnterZone(search->second);
	}
	else
	{
		myCurrentZone->EnterZone({10.f, 10.f});
	}
	myCurrentZoneIndex = static_cast<eZoneType>(0);

#ifndef _RETAIL
	zoneIndex->myText = "CurrentZoneIndex: " + std::to_string(static_cast<int>(myCurrentZoneIndex));
#endif

	myLoadingState = eLoadingState::Finished;
}

void InGameState::BeginTransition(const eZoneType aIndex)
{
#ifndef _RETAIL
	if (static_cast<int>(aIndex) >= myZones.size())
	{
		zoneIndex->myText = "There's no next level";
		return;
	}
#endif
	myNextDoorIndex = myCurrentZone->GetTriggerdDoorID();
	myCurrentZone->LeaveZone();
	myNextZoneIndex = aIndex;
	myShouldFinishTransition = true;
	myCurrentZone->FreezePlayer(true);
	myFader->Activate(3.f, 2.f, (myZoneNames.find(static_cast<int>(myNextZoneIndex)+1)->second));
}

void InGameState::FinishTransition()
{
	if (myFader->GetFadingState() == Fader::eState::FadeIn)
	{
		myCurrentZone->TeleportWandToPlayer();
		myCurrentZone = &myZones[static_cast<int>(myNextZoneIndex)];
		myCurrentZone->EnterZone(GetStartPosition(myNextDoorIndex));
		myCurrentZoneIndex = myNextZoneIndex;
		myCurrentZone->FreezePlayer(false);

		myFader->SendReadyMessage();

		myNextZoneIndex = eZoneType::None;
		myShouldFinishTransition = false;
		myNextDoorIndex = -1;

#ifndef _RETAIL
		zoneIndex->myText = "CurrentZoneIndex: " + std::to_string(static_cast<int>(myCurrentZoneIndex));
#endif
	}
}

void InGameState::FinishOutroTransition()
{
	myCurrentZone->FreezePlayer(true);
	if (myFader->GetFadingState() == Fader::eState::FadeIn)
	{
		myFader->SendReadyMessage();
		myStateStack->PushOutroCutscene();
	}
}

CommonUtilities::Vector2f InGameState::GetStartPosition(const int aID)
{
	auto search = myConnections.find(aID);
	if (search != myConnections.end())
	{
		return search->second;
	}
	else
	{
		std::cout << "Didnt find startPosition at this Index" << std::endl;
	}
	return CommonUtilities::Vector2f(10.f, 10.f);
}
#pragma once
#include "GameState.h"
#include <vector>
#include "Zone.h"
#include "Camera.h"
#include "DataBase.h"
#include "InventoryState.h"
#include "ParticleSys.h"
#include <thread>
/*#include "Subscriber.h"*/

#ifndef _RETAIL
	#include "tga2d\text\text.h"
#endif

class Fader;

namespace Tga2D
{
	class CSprite;
}

class InGameState :	public GameState, public Subscriber
{
public:

	enum class eLoadingState
	{
		Loading,
		Waiting,
		Finished
	};


	InGameState(CommonUtilities::InputManager* aInputManager, Fader* aFader, DataBase *aDatabase);
	~InGameState();

	void Init();

	void OnEnter();
	void OnExit();

	eStateStackMessage Update(float aDeltaTime);
	void Render();
	void ReceiveMessage(const Message aMessage) override;
	void TerminateThread() override;

private:
	std::thread myLoadingThread;
	eLoadingState myLoadingState;
	eZoneType myNextZoneIndex;
	int myNextDoorIndex;

	bool myShouldFinishTransition;
	bool myShouldPlayOutro;
	bool myShouldPop;
	bool myShowDebugLines;
	std::vector<RenderObject*> myRenderBuffer;

	Zone* myCurrentZone;
	std::vector<Zone> myZones;
	Camera myCamera;

	DataBase *myDatabase;
	Inventory myInventory;
	InGameUI myUI;

	void BeginTransition(const eZoneType aIndex);
	void FinishTransition();
	void FinishOutroTransition();

	eZoneType myCurrentZoneIndex;

	std::map<int, const char*> myZoneNames;
	std::map<int, CommonUtilities::Vector2f> myConnections;
	CommonUtilities::Vector2f GetStartPosition(const int aID);
	InventoryState myInventoryState;

	Fader* myFader;

	ParticleSys myParticleSys;
#ifndef _RETAIL
	Tga2D::CText* zoneIndex;
#endif

	void InitZones();
};


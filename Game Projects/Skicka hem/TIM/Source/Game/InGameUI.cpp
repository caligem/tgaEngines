#include "stdafx.h"
#include "InGameUI.h"
#include "InputManager.h"
#include "SpaceConverter.h"

InGameUI::InGameUI(Inventory& aInventory, CommonUtilities::InputManager* aInputManager)
	: myInventory(aInventory),
	myInputmanager(aInputManager)
{
}


InGameUI::~InGameUI()
{
}

void InGameUI::Init()
{

	myHeartStartingPosition = { 30.f / 1920.f, 20.f / 1080.f };
	myInventorySpritePosition = { 1860.f / 1920.f, 960.f / 1080.f };

	myInventoryBg.Init("Sprites/Inventory/wandIconHover.dds");
	myInventoryBg.SetPivot({ 0.5f, 0.5f });
	myInventoryBg.SetPosition(myInventorySpritePosition);
	myInventoryBg.SetLayerType(eLayerType::EUserInterface);

	myHeart.Init("Sprites/UI/Heart.dds");
	myHeart.SetPivot({ 0.f, 0.f });
	myHeart.SetPosition(myHeartStartingPosition);
	myHeart.SetLayerType(eLayerType::EUserInterface);
	mySpaceBetweenHearts = (myHeart.GetImageSize().x + 20.f)/ 1920.f;

	myInteractableIndicator.Init("Sprites/UI/interactKey.dds");
	myInteractableIndicator.SetPivot({ 0.5f, 0.5f });
	myInteractableIndicator.SetLayerType(eLayerType::EForeground);
	myInteractableIndicator.SetShouldRender(false);
}

void InGameUI::OnInventoryChange()
{
}

void InGameUI::Render()
{
	const float deltaTimeForLight = 1.f / 60.f;
	if (myInputmanager->GetScroll() < 0 || myInputmanager->GetScroll() > 0)
	{
		myLightTimer = 7.f;
	}
	if (myLightTimer > 0.f)
	{
		myLightTimer -= deltaTimeForLight;
		RenderPreviousItem(0.4f);
		RenderCurrentItem(1.f);
		RenderNextItem(0.4f);
	}
	else
	{
		RenderPreviousItem(0.2f);
		RenderCurrentItem(0.5f);
		RenderNextItem(0.2f);
	}
	for (int i = 0; i < myHeartCount; ++i)
	{
		myHeart.SetPosition({ myHeartStartingPosition.x + (mySpaceBetweenHearts * i), myHeartStartingPosition.y });
		myHeart.Render();
	}
}

void InGameUI::SetNumberOfHearts(int aLife)
{
	myHeartCount = aLife;
}

void InGameUI::RenderPreviousItem(float aOpacity)
{
	InventoryItem* prevItem = myInventory.GetNextItem();
	if (prevItem != nullptr)
	{
		CommonUtilities::Vector2f myPreviousPosition = prevItem->myThumbnailImage.GetPosition();
		myInventoryBg.SetPosition({myInventorySpritePosition.x, myInventorySpritePosition.y + (myInventoryBg.GetImageSize().y) / 1080.f });
		myInventoryBg.SetColor({ 1.f, 1.f, 1.f, aOpacity });
		myInventoryBg.Render();
		prevItem->myThumbnailImage.SetPosition({ myInventoryBg.GetPosition().x, myInventoryBg.GetPosition().y });
		prevItem->myThumbnailImage.SetColor({ 1.f, 1.f, 1.f, aOpacity });
		prevItem->myThumbnailImage.Render();
		prevItem->myThumbnailImage.SetColor({ 1.f, 1.f, 1.f, 1.f });
		prevItem->myThumbnailImage.SetPosition(myPreviousPosition);
		
	}
}

void InGameUI::RenderCurrentItem(float aOpacity)
{
	myInventoryBg.SetPosition(myInventorySpritePosition);
	myInventoryBg.SetColor({ 1.f, 1.f, 1.f, aOpacity });
	myInventoryBg.Render();
	Sprite* currSprite = &myInventory.GetActiveItem()->myThumbnailImage;
	CommonUtilities::Vector2f myPreviousPosition = currSprite->GetPosition();
	currSprite->SetPosition({ myInventoryBg.GetPosition().x, myInventoryBg.GetPosition().y });
	currSprite->Render();
	currSprite->SetPosition(myPreviousPosition);
}

void InGameUI::RenderNextItem(float aOpacity)
{
	InventoryItem* nextItem = myInventory.GetPreviousItem();
	if (nextItem != nullptr)
	{
		CommonUtilities::Vector2f myPreviousPosition = nextItem->myThumbnailImage.GetPosition();
		myInventoryBg.SetPosition({ myInventorySpritePosition.x, myInventorySpritePosition.y - (myInventoryBg.GetImageSize().y) / 1080.f });
		myInventoryBg.SetColor({ 1.f, 1.f, 1.f, aOpacity });
		myInventoryBg.Render();
		nextItem->myThumbnailImage.SetPosition({ myInventoryBg.GetPosition().x, myInventoryBg.GetPosition().y });
		nextItem->myThumbnailImage.SetColor({ 1.f, 1.f, 1.f, aOpacity });
		nextItem->myThumbnailImage.Render();
		nextItem->myThumbnailImage.SetColor({ 1.f, 1.f, 1.f, 1.f });
		nextItem->myThumbnailImage.SetPosition(myPreviousPosition);
	}
}

void InGameUI::ReceiveMessage(const Message aMessage)
{
	if (aMessage.myMessageType == eMessageType::ShowInteractButton)
	{
		myInteractableinidicatorSpritePosition = aMessage.data.myVec2f;
		myInteractableinidicatorSpritePosition += {0.f, -2.f};
		myInteractableIndicator.SetPosition(myInteractableinidicatorSpritePosition);
		myInteractableIndicator.SetShouldRender(true);
	}
	else if (aMessage.myMessageType == eMessageType::HideInteractButton)
	{
		myInteractableIndicator.SetShouldRender(false);
	}
}

void InGameUI::FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer)
{
	aRenderBuffer.push_back(&myInteractableIndicator);
}

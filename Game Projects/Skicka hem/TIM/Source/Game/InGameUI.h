#pragma once
#include "Sprite.h"
#include "Inventory.h"
#include "Text.h"
#include "Subscriber.h"

namespace CommonUtilities
{
	class InputManager;
}
class InGameUI : public Subscriber
{
public:
	InGameUI(Inventory& aInventory, CommonUtilities::InputManager* aInputmanager);
	~InGameUI();

	void Init();
	void OnInventoryChange();
	void Render();
	
	void SetNumberOfHearts(int aLife);
	void RenderPreviousItem(float aOpacity);
	void RenderCurrentItem(float aOpacity);
	void RenderNextItem(float aOpacity);
	void ReceiveMessage(const Message aMessage) override;
	void FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer);

private:
	float mySpaceBetweenHearts;
	float myLightTimer;
	int myHeartCount;
	CommonUtilities::Vector2f myHeartStartingPosition;
	CommonUtilities::Vector2f myInventorySpritePosition;
	CommonUtilities::Vector2f myInteractableinidicatorSpritePosition;
	Sprite myInteractableIndicator;
	Sprite myInventoryBg;
	Inventory& myInventory;
	CommonUtilities::InputManager* myInputmanager;
	Sprite myHeart;
};


#include "stdafx.h"
#include "InteractableObject.h"


InteractableObject::InteractableObject()
{
}


InteractableObject::~InteractableObject()
{
}

void InteractableObject::Init(eObjectType aType, CollisionManager* aCollisionManager)
{
	myShouldRemove = false;
	myObjectType = aType;
	myCollisionManager = aCollisionManager;
	myTag = eGameObjectTag::Item;
	InitGO(myPosition, myCollisionManager, eCollisionLayer::Interactable);
}

void InteractableObject::Update()
{
}


#pragma once
#include "GameObject.h"

enum class eObjectType
{
	Wand,
	Lever,
	count
};


class InteractableObject :
	public GameObject
{
public:
	InteractableObject();
	~InteractableObject();

	void Init(eObjectType aType, CollisionManager* aCollisionManager);

	//FOr VFX
	void Update();
	//Stahp
	inline void SetID(int aId) { myID = aId; }
	const int GetID() const { return myID; }


protected:
	int myID;

	eObjectType myObjectType;
	CollisionManager* myCollisionManager;
};


#include "stdafx.h"
#include "Inventory.h"
#include "InputManager.h"
#include "RenderObject.h"
#include "PostMaster.h"


#ifndef _RETAIL
void Inventory::ReinitWands()
{
	nlohmann::json newData;
	std::string file = "Data/Items.json";
	std::ifstream filestream(file.c_str());
	filestream >> newData;
	filestream.close();

	myWorldItems.clear();
	myItemsInInventory.clear();
	myActiveItem = nullptr;
	Init(newData, myInputManager);

}
#endif


Inventory::Inventory()
{
}


Inventory::~Inventory()
{
}


void Inventory::Init(const nlohmann::json & aInventoryData, CU::InputManager* aInputManager)
{
	myInputManager = aInputManager;
	myHavePickedUpTeleportActivationItem = false;
	myActiveIndex = 0;
	myIndexOfTeleportUpgradeWand = aInventoryData["TeleportWandID"].get<int>();
	for (int i = 0; i < aInventoryData["Wands"].size(); ++i)
	{
		InventoryItem aTempItem;
		aTempItem.myID = i;
		aTempItem.myName = aInventoryData["Wands"][i]["Name"].get<std::string>();
		aTempItem.myDescription = aInventoryData["Wands"][i]["Description"].get<std::string>();
		aTempItem.myPlayerHealthMod = aInventoryData["Wands"][i]["Properties"]["Player"]["MaxHealthAmount"].get<int>();
		aTempItem.myPlayerMovementSpeedMod = aInventoryData["Wands"][i]["Properties"]["Player"]["MovementSpeed"].get<float>();
		aTempItem.myPlayerMovementSpeedOnThrowMod = aInventoryData["Wands"][i]["Properties"]["Player"]["ReducedMovementSpeed"].get<float>();
		aTempItem.myPlayerMovementSpeedWithoutWandMod = aInventoryData["Wands"][i]["Properties"]["Player"]["MovementSpeedWithoutWand"].get<float>();
		aTempItem.myPlayerInvulnerabilityTimeMod = aInventoryData["Wands"][i]["Properties"]["Player"]["InvulnerabilityTime"].get<float>();

		aTempItem.myWandDamage = aInventoryData["Wands"][i]["Properties"]["Wand"]["Damage"].get<float>();
		aTempItem.myWandRecallSpeed = aInventoryData["Wands"][i]["Properties"]["Wand"]["RecallSpeed"].get<float>();
		aTempItem.myWandThrownSpeed = aInventoryData["Wands"][i]["Properties"]["Wand"]["ThrownSpeed"].get<float>();
		aTempItem.myWandReturnRange = aInventoryData["Wands"][i]["Properties"]["Wand"]["ReturnRange"].get<float>();
		aTempItem.myConnectID = static_cast<int>(aInventoryData["Wands"][i]["Properties"]["Wand"]["ConnectID"].get<float>());

		aTempItem.myInventoryImage.Init(aInventoryData["Wands"][i]["Visuals"]["ImageLocation"].get<std::string>().c_str(), { 0.1f, 0.1f }, eLayerType::EUserInterface);
		aTempItem.myThumbnailImage.Init(aInventoryData["Wands"][i]["Visuals"]["Icon"].get<std::string>().c_str(), { 0.1f, 0.1f }, eLayerType::EUserInterface);
		aTempItem.myThumbnailImage.SetPivot({ 0.5f, 0.5f });
		myWorldItems.push_back(aTempItem);
	}
	myItemsInInventory.push_back(&myWorldItems[0]);
	myActiveItem = myItemsInInventory[0];

	myHaventSentKeyMessage = true;
	myHaventSentWandMessage = true;
}

void Inventory::Update(float aDeltaTime, bool aCanChangeInventory)
{
	myActiveItemIsChanged = false;
	aDeltaTime;
	if (aCanChangeInventory)
	{
		if (myInputManager->GetScroll() < 0)
		{
			SwapToNextInventoryItem();
		}
		if (myInputManager->GetScroll() > 0)
		{
			SwapToPreviousInventoryItem();
		}
	}
#ifndef _RETAIL
	if (myInputManager->KeyPressed(CU::Keys::F5))
	{
		ReinitWands();
	}
#endif
}

void Inventory::FillRenderBuffer(std::vector<RenderObject*>& aBuffer)
{
	aBuffer.push_back(&myActiveItem->myThumbnailImage);
}

void Inventory::ReceiveMessage(const Message aMessage)
{
	if (aMessage.myMessageType == eMessageType::FoundWand)
	{
		AddItemToInventory(aMessage.data.myInt);
	}
	if (aMessage.myMessageType == eMessageType::KeyUsed)
	{
		size_t inventoryIndex = GetActiveItemIndex() - 1;
		if (inventoryIndex < 0)
		{
			inventoryIndex = 0;
		}
		SetActiveInventoryItem(static_cast<size_t>(inventoryIndex));
		RemoveItemFromInventory(aMessage.data.myInt);
	}
}

void Inventory::AddItemToInventory(int aID)
{
	for (int i = 0; i < myItemsInInventory.size(); ++i)
	{
		if (myItemsInInventory[i]->myID == aID)
		{
			return;
		}
	}
	if (aID == myIndexOfTeleportUpgradeWand)
	{
		myHavePickedUpTeleportActivationItem = true;
		if (myHaventSentWandMessage)
		{
			Message message(eMessageType::PopUpTextBox);
			message.data.myPopUpTextBox = ePopUpTextBox::FirstWand;
			PostMaster::SendMessages(message);
			myHaventSentWandMessage = false;
		}

	}
	myItemsInInventory.push_back(&myWorldItems[aID]);

	if (myHaventSentKeyMessage && myWorldItems[aID].myConnectID != -1)
	{
		Message message(eMessageType::PopUpTextBox);
		message.data.myPopUpTextBox = ePopUpTextBox::FirstKey;
		PostMaster::SendMessages(message);
		myHaventSentKeyMessage = false;
	}
}

void Inventory::RemoveItemFromInventory(int aIndex)
{
	myItemsInInventory.erase(myItemsInInventory.begin() + aIndex);
}

void Inventory::SwapToNextInventoryItem()
{
	myActiveIndex++;
	if (myActiveIndex > myItemsInInventory.size() -1)
	{
		myActiveIndex = 0;
	}
	myActiveItem = myItemsInInventory[myActiveIndex];
	myActiveItemIsChanged = true;
}

void Inventory::SwapToPreviousInventoryItem()
{
	if (myActiveIndex > 0)
	{
		myActiveIndex--;
	}
	else
	{
		myActiveIndex = myItemsInInventory.size() - 1;
	}

	myActiveItem = myItemsInInventory[myActiveIndex];
	myActiveItemIsChanged = true;
}

InventoryItem * Inventory::GetPreviousItem()
{
	if (myActiveIndex == 0 && myItemsInInventory.size() > 1)
	{
		return myItemsInInventory[myItemsInInventory.size() - 1];
	}
	else if (myItemsInInventory.size() > 1)
	{
		return myItemsInInventory[myActiveIndex - 1];
	}
	return nullptr;
}

InventoryItem * Inventory::GetNextItem()
{
	if (myActiveIndex + 1 > myItemsInInventory.size() - 1 && myItemsInInventory.size() > 1)
	{
		return myItemsInInventory[0];
	}
	else if (myItemsInInventory.size() > 1)
	{
		return myItemsInInventory[myActiveIndex + 1];
	}
	return nullptr;
}

#pragma once
#include "json.h"
#include <vector>
#include "Sprite.h"
#include "Subscriber.h"

namespace CommonUtilities 
{
	class InputManager;
}
class RenderObject;

struct InventoryItem
{
	int myID;
	int myPlayerHealthMod;
	float myPlayerMovementSpeedMod;
	float myPlayerMovementSpeedOnThrowMod;
	float myPlayerMovementSpeedWithoutWandMod;;
	float myPlayerInvulnerabilityTimeMod;

	float myWandThrownSpeed;
	float myWandRecallSpeed;
	float myWandDamage;
	float myWandReturnRange;
	int myConnectID;

	std::string myName;
	std::string myDescription;

	Sprite myInventoryImage;
	Sprite myThumbnailImage;
};


class Inventory : public Subscriber
{
public:
	Inventory();
	~Inventory();

	void Init(const nlohmann::json& aInventoryData, CU::InputManager* aInputManager);
	void Update(float aDeltaTime, bool aCanChangeInventory);
	void FillRenderBuffer(std::vector<RenderObject*>& aBuffer);
	bool IsInventoryChanged() { return myActiveItemIsChanged; }
	inline InventoryItem* GetActiveItem() const { return myActiveItem; }
	void ReceiveMessage(const Message aMessage) override;
	
	inline size_t GetActiveItemIndex() const { return myActiveIndex; }
	inline std::vector<InventoryItem*>& GetConstInventoryList() { return myItemsInInventory; }
	inline const std::vector<InventoryItem>& GetWorldItems() const {return myWorldItems; }
	inline void SetActiveInventoryItem(size_t aInventoryIndex) { myActiveIndex = aInventoryIndex; myActiveItem = myItemsInInventory[aInventoryIndex]; };
	void AddItemToInventory(int aID);
	void RemoveItemFromInventory(int aIndex); // Not ID as above
	void SwapToNextInventoryItem();
	void SwapToPreviousInventoryItem();

	bool GetShouldGiveTeleport() { return myHavePickedUpTeleportActivationItem; }

	InventoryItem* GetPreviousItem();
	InventoryItem* GetNextItem();

private:
	bool myActiveItemIsChanged;
	bool myHaventSentWandMessage;
	bool myHaventSentKeyMessage;
	bool myHavePickedUpTeleportActivationItem;

	size_t myActiveIndex;
	int myIndexOfTeleportUpgradeWand;
	std::vector<InventoryItem> myWorldItems;
	std::vector<InventoryItem*> myItemsInInventory;
	InventoryItem* myActiveItem;
	CU::InputManager* myInputManager;


#ifndef _RETAIL
	void ReinitWands();
#endif
};


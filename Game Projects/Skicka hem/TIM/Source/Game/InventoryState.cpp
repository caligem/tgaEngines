 #include "stdafx.h"
#include "InventoryState.h"
#include "InputManager.h"
#include "StateStack.h"
#include "Sprite.h"
#include "tga2d\engine.h"
#include "tga2d\math\common_math.h"

InventoryState::InventoryState(CommonUtilities::InputManager* aInputManager, Inventory& aInventory)
: myInventory(aInventory),
	myListViewAABB({0.f, 0.f}, {0.f, 0.f})
{
	myInputManager = aInputManager;
	myShouldPop = false;
	myIsInitiated = false;
}


InventoryState::~InventoryState()
{
	myActiveInventoryItemLargeIcon = nullptr;
}

void InventoryState::Init()
{
	myShouldDelete = false;
	if (myIsInitiated == false)
	{
		myIsInitiated = true;
		myRenderBuffer.reserve(16);


		myAmountOfItemsShownInList = 7;
		myListViewAABB.myMin = { 0.55f, 0.257f };
		InitSprites();
		myItemSpritePosition = { myBackground.GetPosition().x + 15/1920.f, myBackground.GetPosition().y - 87/1080.f };
		myListViewAABB.myMax = { myListViewAABB.myMin.x + myInventoryListBackground.GetSizeInPixels().x / 1920.f, myListViewAABB.myMin.y + myInventoryListBackground.GetSizeInPixels().y / 1080.f };
		float myListViewStartY = 32.f / 1080.f;
		float myListViewStartX = 8.f / 1920.f;
#ifndef _RETAIL
		if (myInputManager->KeyDown(CU::Keys::L))
		{
			myInventory.AddItemToInventory(1);
			myInventory.AddItemToInventory(2);
			myInventory.AddItemToInventory(3);
			myInventory.AddItemToInventory(4);
			myInventory.AddItemToInventory(5);
			myInventory.AddItemToInventory(6);
			myInventory.AddItemToInventory(7);
			myInventory.AddItemToInventory(8);
			myInventory.AddItemToInventory(9);
			myInventory.AddItemToInventory(10);
			myInventory.AddItemToInventory(11); 
			myInventory.AddItemToInventory(12);
		}
#endif
		myCycleUpButton.Init([&] {if (myTopItemsIndexInInventoryVector != 0 && myInventory.GetConstInventoryList().size() > myAmountOfItemsShownInList) myTopItemsIndexInInventoryVector--;  }, "Sprites/Inventory/ScrollBox_upButton.dds", { myListViewAABB.myMin.x + (myListViewAABB.myMax.x - myListViewAABB.myMin.x)/2 , myListViewAABB.myMin.y - 15.f/1080.f }, myInputManager);
		myCycleUpButton.AttachHoveredSprite("Sprites/Inventory/ScrollBox_upButtonHover.dds");
		myCycleUpButton.SetHitboxOffsetX(385/1920.f);
		myCycleUpButton.SetHitboxOffsetY(98/1080.f);
		myButtons.push_back(&myCycleUpButton);

		myCycleDownButton.Init([&] {if (myTopItemsIndexInInventoryVector < myInventory.GetConstInventoryList().size() - myAmountOfItemsShownInList && myInventory.GetConstInventoryList().size() > myAmountOfItemsShownInList) myTopItemsIndexInInventoryVector++;  }, "Sprites/Inventory/ScrollBox_downButton.dds", { myListViewAABB.myMin.x + (myListViewAABB.myMax.x - myListViewAABB.myMin.x) / 2 , myListViewAABB.myMax.y + 16.f/1080.f }, myInputManager);
		myCycleDownButton.AttachHoveredSprite("Sprites/Inventory/ScrollBox_downButtonHover.dds");
		myCycleDownButton.SetHitboxOffsetX(385 / 1920.f);
		myCycleDownButton.SetHitboxOffsetY(98 / 1080.f);
		myButtons.push_back(&myCycleDownButton);

		myExitButton.Init([&] { myShouldPop = true; }, "Sprites/menu/xButton.dds", { myListViewAABB.myMin.x + 32 / 1920.f, myListViewAABB.myMin.y - 78 / 1080.f}, myInputManager);
		myExitButton.AttachHoveredSprite("Sprites/menu/xButtonHover.dds");
		myExitButton.SetHitboxOffsetX(96 / 1920.f);
		myExitButton.SetHitboxOffsetY(97 / 1080.f);
		myButtons.push_back(&myExitButton);


		for (size_t i = 0; i < myAmountOfItemsShownInList; ++i)
		{
			float YDistBetweenSelButts = (myListViewAABB.myMax.y - myListViewAABB.myMin.y - (myListViewStartY * 2))/myAmountOfItemsShownInList;
			CommonUtilities::Vector2f maxPos = { myListViewAABB.myMax.x, myListViewAABB.myMin.y + ((static_cast<float>(i) + 1.f)*YDistBetweenSelButts) + myListViewStartY };
			CommonUtilities::Vector2f minPos = { myListViewAABB.myMin.x + myListViewStartX, myListViewAABB.myMin.y + (static_cast<float>(i) * YDistBetweenSelButts) + myListViewStartY };
			SelectionButton tempButt(maxPos, minPos, static_cast<size_t>(-1));
			tempButt.mySprite.Init("Sprites/Inventory/scrollSlot.dds");
			tempButt.mySprite.SetPivot({ 0.f,0.f });
			tempButt.mySprite.SetLayerType(eLayerType::EUserInterface);
			tempButt.mySprite.SetColor({ 1.f, 1.f, 1.f, 1.f });
			tempButt.mySprite.SetPosition(minPos);

			tempButt.mySelectedSprite.Init("Sprites/Inventory/scrollSlotHover.dds");			
			tempButt.mySelectedSprite.SetPivot({ 0.f,0.f });
			tempButt.mySelectedSprite.SetLayerType(eLayerType::EUserInterface);
			tempButt.mySelectedSprite.SetColor({ 1.f, 1.f, 1.f, 1.f });
			tempButt.mySelectedSprite.SetPosition(minPos);
			tempButt.mySpriteToRender = &tempButt.mySprite;
			tempButt.myName.Init("Text/Bradley Gratis.ttf", false);
			tempButt.myName.SetColor({ 1.f, 1.f, 1.f, 1.f });

			maxPos = { tempButt.myAABB.myMax.x - 60.f / 1920.f , tempButt.myAABB.myMax.y - 23 / 1080.f };
			minPos = { tempButt.myAABB.myMin.x + 77.f / 1920.f , tempButt.myAABB.myMin.y + 23 / 1080.f };
			AABB tempButtAABB(minPos, maxPos);
			tempButt.myName.SetAABB(tempButtAABB);
			tempButt.myName.SetPosition(tempButtAABB.myMin);
			myInventoryItemButtons.push_back(tempButt);
		}
		myActiveName.Init("Text/Bradley Gratis.ttf", true, Tga2D::EFontSize::EFontSize_30);
		myActiveName.SetAABB(AABB({(myBackground.GetPosition().x - 512.f/1920.f) + 326 / 1920.f, (myBackground.GetPosition().y - 512.f / 1080.f) + 785.f / 1080.f }, { (myBackground.GetPosition().x - 512.f / 1920.f) + 727 / 1920.f, (myBackground.GetPosition().y - 512.f / 1080.f) + 795.f / 1080.f })); //min 326, 755 max 727, 795  (-512)
		myActiveName.SetPosition(myActiveName.GetAABB().myMin);
		myActiveName.SetColor({ 1.f, 1.f, 1.f, 1.f });
		myDescription.Init("Text/Bradley Gratis.ttf", false, Tga2D::EFontSize::EFontSize_18);
		myDescription.SetAABB(AABB({ (myBackground.GetPosition().x - 512.f / 1920.f) + 326 / 1920.f, (myBackground.GetPosition().y - 512.f / 1080.f) + 825.f / 1080.f }, { (myBackground.GetPosition().x - 512.f / 1920.f) + 727 / 1920.f, (myBackground.GetPosition().y - 512.f / 1080.f) + 910.f / 1080.f })); //min 326, 799 max 727, 910  (-512)
		myDescription.SetPosition({ myDescription.GetAABB().myMin });
		myDescription.SetColor({ 1.f, 1.f, 1.f, 1.f });
		myDescription.SetText(myInventory.GetActiveItem()->myDescription.c_str());
		myActiveName.SetText(myInventory.GetActiveItem()->myName.c_str());

	}
	
}

void InventoryState::OnEnter()
{
	myEquippedItemIndexInInventoryVector = myInventory.GetActiveItemIndex();
	myTopItemsIndexInInventoryVector = 0;
	myActiveInventoryItemLargeIcon = &myInventory.GetActiveItem()->myInventoryImage;
	myActiveInventoryItemLargeIcon->SetPivot({ 0.5f, 0.5f });
	myActiveInventoryItemLargeIcon->SetPosition(myItemSpritePosition);
 	for (size_t i = 0; i < myInventory.GetConstInventoryList().size() && i < myAmountOfItemsShownInList; ++i)
 	{
 		myInventoryItemButtons[i].myIndex = i;	
 	}
	myListChangedThisFrame = true;
	myShouldPop = false;
	UpdateInventorySelect();
}

void InventoryState::OnExit()
{
	CleanUp();
	myInventory.GetActiveItem()->myThumbnailImage.SetColor({ 1.f, 1.f, 1.f, 1.f });
}

eStateStackMessage InventoryState::Update(float aDeltaTime)
{
	myListChangedThisFrame = false;
	CleanUp();
	aDeltaTime;
	UpdateButtons();
	UpdateInventorySelect();
	while (ShowCursor(true) <= 0);
	FillRenderBuffer();

	if (myInputManager->KeyPressed(CommonUtilities::Keys::Escape) || myInputManager->KeyPressed(CU::Keys::I))
	{
		return eStateStackMessage::PopSubState;
	}
	if (myShouldPop)
	{
		return eStateStackMessage::PopSubState;
	}

	return eStateStackMessage::KeepState;
}

void InventoryState::UpdateButtons()
{
	for (unsigned short i = 0; i < myButtons.size(); i++)
	{
		Button* currentButton;

		currentButton = myButtons[i];


		if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
		{
			currentButton->SetSprite(Button::eSpriteType::Hovered);

			if (myInputManager->KeyPressed(CommonUtilities::Keys::LeftMouseButton))
			{
				myListChangedThisFrame = true;
				currentButton->OnClick();
			}
		}
		else
		{
			currentButton->SetSprite(Button::eSpriteType::Default);
		}
	}
}

void InventoryState::UpdateInventorySelect()
{
	ScrollChangeList();
	if (myListChangedThisFrame)
	{
		for (size_t i = 0; i < myInventoryItemButtons.size(); i++)
		{
			if (myTopItemsIndexInInventoryVector + i < myInventory.GetConstInventoryList().size())
			{
				myInventoryItemButtons[i].myIndex = myTopItemsIndexInInventoryVector + i;
				myInventoryItemButtons[i].myName.SetText(myInventory.GetConstInventoryList()[myTopItemsIndexInInventoryVector + i]->myName.c_str());
				myInventoryItemButtons[i].mySpriteToRender = &myInventoryItemButtons[i].mySprite;
			}
		}
	}
	CheckForSelectedInventoryItem();
}

void InventoryState::Render()
{

	for (unsigned short i = 0; i < myRenderBuffer.size(); i++)
	{
		if (static_cast<int>(myRenderBuffer[i]->GetType()) > -1 )
		{
			myRenderBuffer[i]->Render();
		}
	}
}

void InventoryState::CleanUp()
{
	myRenderBuffer.clear();
}

void InventoryState::ScrollChangeList()
{
	int mouseX;
	int mouseY;
	myInputManager->GetMousePosition(mouseX, mouseY);
	if (mouseX / 1920.f > myListViewAABB.myMin.x && mouseX / 1920.f < myListViewAABB.myMax.x && mouseY / 1080.f > myListViewAABB.myMin.y && mouseY / 1080.f < myListViewAABB.myMax.y)
	{
		if (myInputManager->GetScroll() < 0)
		{
			if (myTopItemsIndexInInventoryVector < myInventory.GetConstInventoryList().size() - myAmountOfItemsShownInList && myInventory.GetConstInventoryList().size() > myAmountOfItemsShownInList)
			{
				myTopItemsIndexInInventoryVector++;
				myListChangedThisFrame = true;
			}
		}
		else if (myInputManager->GetScroll() > 0)
		{
			if (myTopItemsIndexInInventoryVector != 0 && myInventory.GetConstInventoryList().size() > myAmountOfItemsShownInList)
			{
				myListChangedThisFrame = true;
				myTopItemsIndexInInventoryVector--;
			}
		}
	}
}

void InventoryState::FillRenderBuffer()
{
	myRenderBuffer.push_back(&myFadeSprite);
	myRenderBuffer.push_back(&myInventoryListBackground);
	myRenderBuffer.push_back(myActiveInventoryItemLargeIcon);
	for (unsigned short i = 0; i < myButtons.size(); i++)
	{
		myButtons[i]->FillRenderBuffer(myRenderBuffer);
	}
	myRenderBuffer.push_back(&myBackground);
	myRenderBuffer.push_back(&myActiveName);
	myRenderBuffer.push_back(&myDescription);
	std::vector<InventoryItem*>& currentinventory = myInventory.GetConstInventoryList();
	for (size_t i = 0; i < myInventoryItemButtons.size(); i++)
	{
		if (myTopItemsIndexInInventoryVector + i < currentinventory.size())
		{			
			myRenderBuffer.push_back(myInventoryItemButtons[i].mySpriteToRender);
			currentinventory[myInventoryItemButtons[i].myIndex]->myThumbnailImage.SetPosition(myInventoryItemButtons[i].myIconPosition);
			myRenderBuffer.push_back(&currentinventory[myInventoryItemButtons[i].myIndex]->myThumbnailImage);
			myRenderBuffer.push_back(&myInventoryItemButtons[i].myName);
		}
	}
}

void InventoryState::CheckForSelectedInventoryItem()
{
	int mouseX;
	int mouseY;
	myInputManager->GetMousePosition(mouseX, mouseY);
	if (mouseX / 1920.f > myListViewAABB.myMin.x && mouseX / 1920.f < myListViewAABB.myMax.x && mouseY / 1080.f > myListViewAABB.myMin.y && mouseY / 1080.f < myListViewAABB.myMax.y)
	{
		for (size_t i = 0; i < myInventoryItemButtons.size(); ++i)
		{
			if (myInventoryItemButtons[i].myIndex != -1)
			{
				if (mouseY / 1080.f > myInventoryItemButtons[i].myAABB.myMin.y && mouseY / 1080.f < myInventoryItemButtons[i].myAABB.myMax.y)
				{
					myInventoryItemButtons[i].mySprite.SetColor({ 1.f, 1.f, 1.f, 1.f });
					myInventory.GetConstInventoryList()[myInventoryItemButtons[i].myIndex]->myThumbnailImage.SetColor({ 1.f, 1.f, 1.f, 1.f });
					if (myInputManager->KeyPressed(CU::Keys::LeftMouseButton))
					{
						myInventory.SetActiveInventoryItem(myInventoryItemButtons[i].myIndex);
						myActiveInventoryItemLargeIcon = &myInventory.GetActiveItem()->myInventoryImage;
						myEquippedItemIndexInInventoryVector = myInventoryItemButtons[i].myIndex;
						myInventoryItemButtons[i].mySpriteToRender = &myInventoryItemButtons[i].mySelectedSprite;
						myActiveInventoryItemLargeIcon->SetPivot({ 0.5f, 0.5f });
						myActiveInventoryItemLargeIcon->SetPosition(myItemSpritePosition);
						myActiveName.SetText(myInventory.GetActiveItem()->myName.c_str());
						myDescription.SetText(myInventory.GetActiveItem()->myDescription.c_str());
					}
				}
				else
				{
					if (myInventory.GetConstInventoryList()[myInventoryItemButtons[i].myIndex]->myID == myInventory.GetActiveItem()->myID)
					{
						myInventoryItemButtons[i].mySprite.SetColor({ 1.f, 1.f, 1.f, 1.f });
						myInventory.GetConstInventoryList()[myInventoryItemButtons[i].myIndex]->myThumbnailImage.SetColor({ 1.f, 1.f, 1.f, 1.f });
						myInventoryItemButtons[i].mySpriteToRender = &myInventoryItemButtons[i].mySelectedSprite;
					}
					else
					{
						myInventoryItemButtons[i].mySprite.SetColor({ 1.f, 1.f, 1.f, 0.8f });
						myInventory.GetConstInventoryList()[myInventoryItemButtons[i].myIndex]->myThumbnailImage.SetColor({ 1.f, 1.f, 1.f, 0.8f });
						myInventoryItemButtons[i].mySpriteToRender = &myInventoryItemButtons[i].mySprite;

					}
				}
			}
		}
	}
	else
	{
		for (size_t i = 0; i < myInventoryItemButtons.size(); ++i)
		{

			if (myInventoryItemButtons[i].myIndex != -1)
			{
				if (myInventory.GetConstInventoryList()[myInventoryItemButtons[i].myIndex]->myID == myInventory.GetActiveItem()->myID)
				{
					myInventoryItemButtons[i].mySprite.SetColor({ 1.f, 1.f, 1.f, 1.f });
					myInventory.GetConstInventoryList()[myInventoryItemButtons[i].myIndex]->myThumbnailImage.SetColor({ 1.f, 1.f, 1.f, 1.f });
					myInventoryItemButtons[i].mySpriteToRender = &myInventoryItemButtons[i].mySelectedSprite;
				}
				else
				{
					myInventoryItemButtons[i].mySprite.SetColor({ 1.f, 1.f, 1.f, 0.8f });
					myInventory.GetConstInventoryList()[myInventoryItemButtons[i].myIndex]->myThumbnailImage.SetColor({ 1.f, 1.f, 1.f, 0.8f });
					myInventoryItemButtons[i].mySpriteToRender = &myInventoryItemButtons[i].mySprite;
				}
			}
		}
	}

}

void InventoryState::InitSprites()
{
	myFadeSprite.SetSizeRelativeToScreen({ 2.f, 2.f });
	myFadeSprite.SetLayerType(eLayerType::EUserInterface);
	myFadeSprite.SetPosition({ 0.5f, 0.5f });
	myFadeSprite.SetPivot({ 0.5f, 0.5f });
	myFadeSprite.SetColor({ 0.f,0.f,0.f,0.4f });

	myBackground.Init("Sprites/Inventory/bigWandBox.dds");
	myBackground.SetLayerType(eLayerType::EUserInterface);
	myBackground.SetPosition({ myListViewAABB.myMin.x - (247.f/1920.f), 0.5f });
	myBackground.SetPivot({ 0.5f, 0.5f });
	myBackground.SetSizeRelativeToImage({ 1.f, 1.f });

	myInventoryListBackground.Init("Sprites/Inventory/scrollBox.dds");
	myInventoryListBackground.SetPivot({ 0.f, 0.f });
	myInventoryListBackground.SetPosition(myListViewAABB.myMin);
	myInventoryListBackground.SetLayerType(eLayerType::EUserInterface);
	myInventoryListBackground.SetColor({ 1.f, 1.f, 1.f, 1.f });

}

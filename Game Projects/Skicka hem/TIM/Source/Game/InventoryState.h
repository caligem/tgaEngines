#pragma once
#include "GameState.h"
#include "Inventory.h"
#include "Text.h"
#include "vector"
#include "Button.h"

class RenderObject;
class Sprite;


struct SelectionButton
{
	SelectionButton(CommonUtilities::Vector2f aMaxPosition, CommonUtilities::Vector2f aMinPosition, size_t aIndex)
	:	myAABB(aMinPosition, aMaxPosition)
	{
		float buttHeight = aMaxPosition.y - aMinPosition.y;
		float buttWidth = aMaxPosition.x - aMinPosition.x;
		myIconPosition = { myAABB.myMin.x + (buttWidth / 6), myAABB.myMin.y + (buttHeight / 2) };
		myTextPosition = { myAABB.myMin.x + (buttWidth / 4), myAABB.myMin.y + (buttHeight / 3) };
		myIndex = aIndex;
	}
	AABB myAABB;
	CommonUtilities::Vector2f myIconPosition;
	CommonUtilities::Vector2f myTextPosition;
	Text myName;
	Sprite mySprite;
	Sprite mySelectedSprite;
	Sprite* mySpriteToRender;
	size_t myIndex;
};

class InventoryState : public GameState
{
public:
	InventoryState(CommonUtilities::InputManager* aInputManager, Inventory& aInventory);
	~InventoryState();

	void Init() override;
	void OnEnter() override;
	void OnExit() override;

	eStateStackMessage Update(float aDeltaTime) override;
	void UpdateButtons();
	void UpdateInventorySelect();
	void Render() override;

	inline const bool LetThroughRender() const override { return true; }

private:
	bool myShouldPop;
	bool myIsInitiated;
	bool myListChangedThisFrame;

	void CleanUp();
	void ScrollChangeList();
	void FillRenderBuffer();
	void CheckForSelectedInventoryItem();
	void InitSprites();

	//Inventory
	Inventory& myInventory;

	//ListViewStuff

	AABB myListViewAABB;

	size_t myAmountOfItemsShownInList;

	std::vector<SelectionButton> myInventoryItemButtons; //these guys will be the selectiondudes with std::functions that changes the specific index of the active inventory item when clicked
	size_t myTopItemsIndexInInventoryVector; //note: It would be cool to always have the equipped item on the top button when starting the inventory!
	size_t myEquippedItemIndexInInventoryVector;


	//Visuals
	Sprite* myActiveInventoryItemLargeIcon;
	Sprite myInventoryListBackground;
	Sprite myBackground;
	Sprite myFadeSprite;
	CommonUtilities::Vector2f myItemSpritePosition;

	//Text
	Text myDescription;
	Text myActiveName;
	//std::string myActiveDescription;
	//std::string myActiveName;

	//Render
	std::vector<RenderObject*> myRenderBuffer;


	//Buttons
	Button myCycleUpButton;
	Button myCycleDownButton;
	Button myExitButton;
	//Button myCloseInventoryButton;

	std::vector<Button*> myButtons;
};


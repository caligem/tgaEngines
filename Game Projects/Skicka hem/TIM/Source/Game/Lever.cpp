#include "stdafx.h"
#include "Lever.h"
#include "CollisionObject.h"
#include "PostMaster.h"
#include "Sprite.h"
#include "AudioManager.h"

Lever::Lever()
{
	myPulled = false;
	myPlayerInReach = false;
}


Lever::~Lever()
{
}

void Lever::Init(CollisionManager* aCollisionManager, const CommonUtilities::Vector2f& aPosition, const int aConnectID)
{
	myTag = eGameObjectTag::Lever;
	InitGO(aPosition, aCollisionManager, eCollisionLayer::Interactable);

	myConnectID = aConnectID;
}

void Lever::OnCollisionEnter(CollisionObject& aOther)
{
	if (aOther.GetOwner() != nullptr)
	{
		if (aOther.GetOwner()->GetTag() == eGameObjectTag::Player)
		{
			myPlayerInReach = true;
			if (!myPulled)
			{
				Message message(eMessageType::ShowInteractButton);
				message.data.myVec2f = myPosition;
				PostMaster::SendMessages(message);
			}
		}
	}
}

void Lever::OnCollisionLeave(CollisionObject & aOther)
{
	if (aOther.GetOwner() != nullptr)
	{
		if (aOther.GetOwner()->GetTag() == eGameObjectTag::Player)
		{
			myPlayerInReach = false;
			if (!myPulled)
			{
				PostMaster::SendMessages(eMessageType::HideInteractButton);
			}
		}
	}
}

void Lever::ReceiveMessage(Message aMessage)
{
	if (aMessage.myMessageType == eMessageType::PlayerInteract && myPlayerInReach && !myPulled)
	{
		myPulled = true;
		LockUnLockDoor();

		RenderObject* swapSprite = myDrawable;
		myDrawable = myPulledSprite;
		myPulledSprite = swapSprite;
		AudioManager::GetInstance()->PlaySound("pullLever");
		PostMaster::SendMessages(eMessageType::HideInteractButton);
	}
}

void Lever::LockUnLockDoor()
{
	Message message(eMessageType::DoorLockUnlock);
	message.data.myInt = myConnectID;
	PostMaster::SendMessages(message);
}

void Lever::SetUnlockedSprite(const char * aPath)
{
	Sprite* pulledSprite = new Sprite(aPath, myPosition, eLayerType::EGameObjectLayer);
	assert(pulledSprite != nullptr && "Cannot find pulled lever image - is it called \"something\"_Pulled?");
	pulledSprite->SetPosition({ myPosition.x, myPosition.y });
	pulledSprite->SetPivot({ 0.5f, 0.5f });
	myPulledSprite = pulledSprite;
}

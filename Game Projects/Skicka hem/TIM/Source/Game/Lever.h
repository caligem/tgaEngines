#pragma once
#include "Subscriber.h"
#include "GameObject.h"

class Lever :
	public GameObject, public Subscriber
{
public:
	Lever();
	~Lever();

	void Init(CollisionManager* aCollisionManager, const CommonUtilities::Vector2f& aPosition, const int aConnectID);

	inline int const GetConnectID() const { return myConnectID; }

	void OnCollisionEnter(CollisionObject &aOther) override;
	void OnCollisionLeave(CollisionObject &aOther) override;
	void ReceiveMessage(const Message aMessage);
	void LockUnLockDoor();
	void SetUnlockedSprite(const char * aPath);

private:
	RenderObject* myPulledSprite;
	bool myPlayerInReach;
	bool myPulled;

	int myConnectID;
};

#include "stdafx.h"
#include "BoxCollider.h"
#include "CircleCollider.h"
#include "LineCollider.h"

LineCollider::LineCollider(const CommonUtilities::Vector2f & aPosition, const CommonUtilities::Vector2f & aDirection, float aLength, const CommonUtilities::Vector2f & aOffset)
{
	myPosition = aPosition;
	myDirection = aDirection.GetNormalized();
	myLength = aLength;
	myOffset = aOffset;
}

LineCollider::~LineCollider()
{
}

bool LineCollider::TestCollision(CollisionObject * aCollisionObject)
{
	return aCollisionObject->TestCollision(this);
}

bool LineCollider::TestCollision(BoxCollider * aBoxCollider)
{
	return aBoxCollider->BoxVSLineCollision(aBoxCollider->GetPosition() + aBoxCollider->GetOffset(), aBoxCollider->GetWidth(), aBoxCollider->GetHeight(), myPosition + myOffset, GetEndPosition());
}

bool LineCollider::TestCollision(CircleCollider * aCircleCollider)
{
	return aCircleCollider->CircleVSLineCollision(aCircleCollider->GetPosition() + aCircleCollider->GetOffset(), aCircleCollider->GetRadius(), myPosition + myOffset, GetEndPosition());
}

bool LineCollider::TestCollision(LineCollider * aLineCollider)
{
	return aLineCollider->LineVSLineCollision(aLineCollider->GetPosition() + aLineCollider->GetOffset(), aLineCollider->GetEndPosition(), myPosition + myOffset, GetEndPosition());
}

void LineCollider::DebugDraw(const CommonUtilities::Vector2f& aCameraPosition)
{


	const CommonUtilities::Vector2f pointPos = (myPosition + myOffset) - aCameraPosition;
	const CommonUtilities::Vector2f pointPosInScreenSpace = SpaceConverter::TileSpaceToScreenSpace(pointPos) + CommonUtilities::Vector2f(0.5f, 0.5f);
	const CommonUtilities::Vector2f lineEndInScreenSpace = SpaceConverter::TileSpaceToScreenSpace(GetEndPosition() - aCameraPosition) + CommonUtilities::Vector2f(0.5f, 0.5f);

	Tga2D::CLinePrimitive AB;
	AB.SetFrom(pointPosInScreenSpace.x, pointPosInScreenSpace.y);
	AB.SetTo(lineEndInScreenSpace.x, lineEndInScreenSpace.y);
	AB.myColor = { 1.0f, 0.0f, 0.0f, 1.0f };
	AB.Render();
}

const CommonUtilities::Vector2f LineCollider::GetEndPosition() const
{
	CommonUtilities::Vector2f endPos = myPosition + myOffset + myDirection * myLength;
	return endPos;
}

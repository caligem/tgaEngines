#pragma once
#include "CollisionObject.h"
#include "Vector.h"
#include "tga2d\primitives\line_primitive.h"

class LineCollider : public CollisionObject
{
public:
	LineCollider(const CommonUtilities::Vector2f& aPosition, const CommonUtilities::Vector2f& aDirection, float aLength, const CommonUtilities::Vector2f& aOffset);
	~LineCollider();

	bool TestCollision(CollisionObject* aCollisionObject) override;
	bool TestCollision(BoxCollider* aBoxCollider) override;
	bool TestCollision(CircleCollider* aCircleCollider) override;
	bool TestCollision(LineCollider* aLineCollider) override;

	void DebugDraw(const CommonUtilities::Vector2f& aCameraPosition) override;

	inline const float GetLength() const { return myLength; }
	inline const CommonUtilities::Vector2f GetDirection() const { return myDirection; }
	const CommonUtilities::Vector2f GetEndPosition() const;
private:
	CommonUtilities::Vector2f myDirection;
	float myLength;
};


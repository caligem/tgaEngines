#include "stdafx.h"
#include "MainMenuState.h"
#include "InputManager.h"
#include "StateStack.h"
#include "Vector.h"
#include "Fader.h"
#include "DataBase.h"
#include "AudioManager.h"
//#include "DataBase.h"

MainMenuState::MainMenuState(CommonUtilities::InputManager* aInputManager, Fader* aFader, DataBase* aDataBase)
{
	myDatabase = aDataBase;
	myFader = aFader;
	myInputManager = aInputManager;
	myShouldPop = false;
	mySelectedButton = nullptr;
	myShowDebugLines = false;
}


MainMenuState::~MainMenuState()
{
	mySelectedButton = nullptr;
}

void MainMenuState::Init()
{
	myRenderBuffer.reserve(64);
	InitButtons();
	InitSprites();

	auto audiodata = myDatabase->GetData("sound");

	AudioManager::GetInstance()->LoadMusic(audiodata["music"]);
	AudioManager::GetInstance()->LoadSound(audiodata["sound"]);

	AudioManager::GetInstance()->PlayMusic("menu");
}

void MainMenuState::OnEnter()
{
	myFader->SendReadyMessage();
}

void MainMenuState::OnExit()
{
}

eStateStackMessage MainMenuState::Update(float aDeltaTime)
{
	aDeltaTime;
	while (ShowCursor(true) <= 0);
	
	CleanUp();
	FillRenderBuffer();

	UpdateButtons();
	if (myInputManager->KeyPressed(CommonUtilities::Keys::Escape))
	{
		Tga2D::CEngine::GetInstance()->SetFullScreen(false); //There is a crash if you exit the game in fullscreen. This is the fix
		return eStateStackMessage::PopMainState;
	}
	if (myShouldPop)
	{
		Tga2D::CEngine::GetInstance()->SetFullScreen(false); //There is a crash if you exit the game in fullscreen. This is the fix
		return eStateStackMessage::PopMainState;
	}


#ifndef _RETAIL
	if (myInputManager->KeyPressed(CommonUtilities::Keys::F3))
	{
		if (myShowDebugLines)
		{
			myShowDebugLines = false;
		}
		else
		{
			myShowDebugLines = true;
		}
	}
#endif

	return eStateStackMessage::KeepState;
}

void MainMenuState::Render()
{
	for (unsigned short i = 0; i < myRenderBuffer.size(); i++)
	{
		myRenderBuffer[i]->Render();
	}

	if (myShowDebugLines)
	{
		for (size_t index = 0; index < myButtons.size(); index++)
		{
			myButtons[index]->RenderDebugLines();
		}
	}
}

void MainMenuState::NewGame()
{
	myStateStack->PushMainState(new InGameState(myInputManager, myFader, myDatabase));
}

void MainMenuState::CleanUp()
{
	myRenderBuffer.clear();
}

void MainMenuState::FillRenderBuffer()
{
	myRenderBuffer.push_back(&myBackground);

	for (unsigned short i = 0; i < myButtons.size(); i++)
	{
		myButtons[i]->FillRenderBuffer(myRenderBuffer);
	}
}

void MainMenuState::InitSprites()
{
	myBackground.Init("Sprites/menu/menu_backShade.dds");
	myBackground.SetPivot({ 0.5f, 0.5f });
	myBackground.SetPosition({ 0.5f, 0.5f });
}

void MainMenuState::InitButtons()
{
	myButtons.reserve(8);

	myStartButton.Init([&] { NewGame(); }, "Sprites/menu/menu_play.dds", CommonUtilities::Vector2f(0.5f, 0.5f), myInputManager);
	myStartButton.SetHitboxOffsetX(0.45f);
	myStartButton.SetHitboxOffsetY(0.42f);
	myStartButton.AttachHoveredSprite("Sprites/menu/menu_play_hover.dds");
	myButtons.push_back(&myStartButton);

	myOptionsButton.Init([&] { myStateStack->PushSubState(new OptionState(myInputManager, myFader)); }, "Sprites/menu/menu_settings.dds", CommonUtilities::Vector2f(0.5f, 0.65f), myInputManager);
	myOptionsButton.SetHitboxOffsetX(0.45f);
	myOptionsButton.SetHitboxOffsetY(0.42f);
	myOptionsButton.AttachHoveredSprite("Sprites/menu/menu_settings_hover.dds");
	myButtons.push_back(&myOptionsButton);

	myCreditsButton.Init([&] {myStateStack->PushSubState(new CreditsState(myInputManager, myFader)); }, "Sprites/menu/creditsButton.dds", CommonUtilities::Vector2f(0.7f, 0.8f), myInputManager);
	myCreditsButton.SetHitboxOffsetX(96 / 1920.f);
	myCreditsButton.SetHitboxOffsetY(97 / 1080.f);
	myCreditsButton.AttachHoveredSprite("Sprites/menu/creditsButtonHover.dds");
	myButtons.push_back(&myCreditsButton);

	myExitButton.Init([&] {myShouldPop = true; }, "Sprites/menu/menu_exit.dds", CommonUtilities::Vector2f(0.5f, 0.8f), myInputManager);
	myExitButton.SetHitboxOffsetX(0.45f);
	myExitButton.SetHitboxOffsetY(0.42f);
	myExitButton.AttachHoveredSprite("Sprites/menu/menu_exit_hover.dds");
	myButtons.push_back(&myExitButton);

	mySelectedButton = &myStartButton;

	ConnectButtons();
}

void MainMenuState::UpdateButtons()
{
	for (unsigned short i = 0; i < myButtons.size(); i++)
	{
		myButtons[i]->SetSprite(Button::eSpriteType::Default);
	}

	CheckMouseInput();
	CheckKeyBoardInput();

	mySelectedButton->SetSprite(Button::eSpriteType::Hovered);
}

void MainMenuState::ConnectButtons()
{
	myStartButton.ConnectButton(&myOptionsButton, Button::eConnectSide::Down);
	myOptionsButton.ConnectButton(&myExitButton, Button::eConnectSide::Down);
	myExitButton.ConnectButton(&myCreditsButton, Button::eConnectSide::Down);
	myCreditsButton.ConnectButton(&myStartButton, Button::eConnectSide::Down);
}

void MainMenuState::CheckMouseInput()
{
	for (unsigned short i = 0; i < myButtons.size(); i++)
	{
		Button* currentButton;

		currentButton = myButtons[i];


		if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
		{
			mySelectedButton = currentButton;
			if (myInputManager->KeyPressed(CommonUtilities::Keys::LeftMouseButton))
			{
				currentButton->OnClick();
				break;
			}
		}
	}
}

void MainMenuState::CheckKeyBoardInput()
{
	if (myInputManager->KeyPressed(CommonUtilities::Keys::Down))
	{
		mySelectedButton = mySelectedButton->myConnectedButtons.myDownButton;
	}
	else if (myInputManager->KeyPressed(CommonUtilities::Keys::Up))
	{
		mySelectedButton = mySelectedButton->myConnectedButtons.myUpButton;
	}
	else if (myInputManager->KeyPressed(CommonUtilities::Keys::Enter))
	{
		mySelectedButton->OnClick();
	}
}
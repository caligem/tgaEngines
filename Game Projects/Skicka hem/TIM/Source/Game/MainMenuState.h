#pragma once
#include "GameState.h"
#include <vector>
#include "Button.h"
#include "json.h"

class DataBase;
class Fader;
class InGameState;
//class DataBase;

class MainMenuState : public GameState
{
public:
	MainMenuState(CommonUtilities::InputManager* aInputManager, Fader* aFader, DataBase* aDataBase);
	~MainMenuState();

	void Init() override;

	void OnEnter() override;
	void OnExit() override;

	eStateStackMessage Update(float aDeltaTime) override;
	void Render() override;

private:
	bool myShouldPop;
	bool myShowDebugLines;

	void NewGame();

	void CleanUp();
	void FillRenderBuffer();
	void InitSprites();

	void InitButtons();
	void UpdateButtons();
	void ConnectButtons();

	void CheckMouseInput();
	void CheckKeyBoardInput();
	
	Sprite myBackground;
	std::vector<RenderObject*> myRenderBuffer;
	DataBase* myDatabase;

	Button* mySelectedButton;

	Button myStartButton;
	Button myExitButton;
	Button myOptionsButton;
	Button myCreditsButton;

	std::vector<Button*> myButtons;

	Fader* myFader;
};


#include "stdafx.h"
#include "OptionState.h"
#include "InputManager.h"
#include "StateStack.h"
#include "Sprite.h"
#include "tga2d\engine.h"
#include "tga2d\math\common_math.h"
#include "Fader.h"
#include "AudioManager.h"

OptionState::OptionState(CommonUtilities::InputManager* aInputManager, Fader* aFader)
{
	myFader = aFader;
	myInputManager = aInputManager;
	myShouldPop = false;
}


OptionState::~OptionState()
{
}

void OptionState::Init()
{
	LoadData();

	myRenderBuffer.reserve(16);
	myIsDragging = false;

	InitSprites();
	InitButtons();
	InitText();
}

void OptionState::OnEnter()
{
	
}

void OptionState::OnExit()
{
	DeleteText();
}

eStateStackMessage OptionState::Update(float aDeltaTime)
{
	aDeltaTime;
	
	while (ShowCursor(true) <= 0);
	CleanUp();
	UpdateButtons(aDeltaTime);
	UpdateText();
	FillRenderBuffer();

	if (myInputManager->KeyPressed(CommonUtilities::Keys::Escape))
	{
		return eStateStackMessage::PopSubState;
	}
	if (myShouldPop)
	{
		return eStateStackMessage::PopSubState;
	}
	
	return eStateStackMessage::KeepState;
}

void OptionState::Render()
{
	for (unsigned short i = 0; i < myRenderBuffer.size(); i++)
	{
		myRenderBuffer[i]->Render();
	}
}

void OptionState::LoadData()
{
	myVolume = myStateStack->myOptionsData.myVolume;
	myIsMuted = myStateStack->myOptionsData.myIsMuted;
	myIsFullscreen = myStateStack->myOptionsData.myIsFullscreen;

}

void OptionState::SaveData()
{
	myStateStack->myOptionsData.myVolume = myVolume;
	myStateStack->myOptionsData.myIsMuted = myIsMuted;
	myStateStack->myOptionsData.myIsFullscreen = myIsFullscreen;
}

void OptionState::CleanUp()
{
	myRenderBuffer.clear();
}

void OptionState::FillRenderBuffer()
{
	myRenderBuffer.push_back(&myBackground);
	myRenderBuffer.push_back(&mySettingsBox);
	myRenderBuffer.push_back(&mySlider);

	for (unsigned short i = 0; i < myButtons.size(); i++)
	{
		myButtons[i]->FillRenderBuffer(myRenderBuffer);
	}
}

void OptionState::InitSprites()
{
	myBackground.Init("Sprites/menu/menu_backShade.dds");
	myBackground.SetPivot({ 0.5f, 0.5f });
	myBackground.SetPosition({ 0.5f, 0.5f });

	mySettingsBox.Init("Sprites/menu/settings_box.dds");
	mySettingsBox.SetPivot({ 0.5f, 0.5f });
	mySettingsBox.SetPosition({ 0.5f, 0.5f });

	mySlider.Init("Sprites/menu/settings_volumeSlider.dds");
	mySlider.SetPivot({ 0.5f, 0.5f });
	mySlider.SetPosition(CommonUtilities::Vector2f(0.375f, 0.42f));
}

void OptionState::InitText()
{
	
}

void OptionState::UpdateText()
{
	
}

void OptionState::DeleteText()
{

}

void OptionState::InitButtons()
{
	myButtons.reserve(8);

	myBackButton.Init([&] {Back(); }, "Sprites/menu/backButton.dds", CommonUtilities::Vector2f(0.2665f, 0.765f), myInputManager);
	myBackButton.AttachHoveredSprite("Sprites/menu/backButtonHover.dds");
	myBackButton.SetHitboxOffsetX(96 / 1920.f);
	myBackButton.SetHitboxOffsetY(97 / 1080.f);
	myButtons.push_back(&myBackButton);

	myFullscreenButton.Init([&] {ToggleFullscreen(); }, "Sprites/menu/settings_fullscreen.dds", CommonUtilities::Vector2f(0.375f, 0.545f ), myInputManager);
	myFullscreenButton.AttachHoveredSprite("Sprites/menu/settings_fullscreenHover.dds");
	myFullscreenButton.SetHitboxOffsetX(860 / 1920.f);
	myFullscreenButton.SetHitboxOffsetY(215/1080.f);
	myButtons.push_back(&myFullscreenButton);

	myWindowedButton.Init([&] {ToggleFullscreen(); }, "Sprites/menu/settings_windowed.dds", CommonUtilities::Vector2f(0.375f, 0.645f), myInputManager);
	myWindowedButton.AttachHoveredSprite("Sprites/menu/settings_windowedHover.dds");
	myWindowedButton.SetHitboxOffsetX(860 / 1920.f);
	myWindowedButton.SetHitboxOffsetY(215 / 1080.f);
	myButtons.push_back(&myWindowedButton);
	
	if (myIsFullscreen)
	{
		myFullscreenButton.SetClickable(false);
		myFullscreenButton.FadeButton();
		myWindowedButton.SetClickable(true);
	}
	else
	{
		myFullscreenButton.SetClickable(true);
		myWindowedButton.SetClickable(false);
		myWindowedButton.FadeButton();
	}

	if (myIsMuted == false)
	{
		myMuteButton.Init([&] {ToggleMute(); }, "Sprites/menu/settings_volumeOn.dds", CommonUtilities::Vector2f(0.375, 0.35f), myInputManager);
		myMuteButton.AttachHoveredSprite("Sprites/menu/settings_volumeOnHover.dds");
	}
	else
	{
		myMuteButton.Init([&] {ToggleMute(); }, "Sprites/menu/settings_volumeoff.dds", CommonUtilities::Vector2f(0.375, 0.35f), myInputManager);
		myMuteButton.AttachHoveredSprite("Sprites/menu/settings_volumeOffHover.dds");
	}
	myMuteButton.SetHitboxOffsetX(96 / 1920.f);
	myMuteButton.SetHitboxOffsetY(97 / 1080.f);
	myButtons.push_back(&myMuteButton);

	mySliderMinX = 0.306f;
	mySliderMaxX = 0.446f;
	mySliderXDistance = mySliderMaxX - mySliderMinX;

	float sliderHandleXPos = mySliderMinX + ((static_cast<float>(myVolume) / 100) * mySliderXDistance);
	mySliderHandle.Init([&] {AdjustVolume(); }, "Sprites/menu/settings_volumeSliderHandle.dds", CommonUtilities::Vector2f(sliderHandleXPos, 0.42f), myInputManager, true, true);
	mySliderHandle.AttachHoveredSprite("Sprites/menu/settings_volumeSliderHandleHover.dds");
	mySliderHandle.SetHitboxOffsetX(22 / 1920.f);
	mySliderHandle.SetHitboxOffsetY(102 / 1080.f);
	myButtons.push_back(&mySliderHandle);

	ConnectButtons();
	
	mySelectedButton = &myBackButton;
}

void OptionState::UpdateButtons(const float aDeltaTime)
{
	for (unsigned short i = 0; i < myButtons.size(); i++)
	{
		myButtons[i]->SetSprite(Button::eSpriteType::Default);
	}

	CheckMouseInput(aDeltaTime);
	CheckKeyBoardInput(aDeltaTime);

	mySelectedButton->SetSprite(Button::eSpriteType::Hovered);
}

void OptionState::ConnectButtons()
{
	myMuteButton.ConnectButton(&myBackButton, Button::eConnectSide::Up);
	mySliderHandle.ConnectButton(&myMuteButton, Button::eConnectSide::Up);
	if (myIsFullscreen)
	{
		if (myIsMuted)
		{
			myWindowedButton.ConnectButton(&myMuteButton, Button::eConnectSide::Up);
		}
		else
		{
			myWindowedButton.ConnectButton(&mySliderHandle, Button::eConnectSide::Up);
		}
		myWindowedButton.ConnectButton(&myBackButton, Button::eConnectSide::Down);
	}
	else
	{	
		if (myIsMuted)
		{
			myFullscreenButton.ConnectButton(&myMuteButton, Button::eConnectSide::Up);
		}
		else
		{
			myFullscreenButton.ConnectButton(&mySliderHandle, Button::eConnectSide::Up);
		}
		myFullscreenButton.ConnectButton(&myBackButton, Button::eConnectSide::Down);
	}
}

void OptionState::CheckMouseInput(const float aDeltaTime)
{
	for (unsigned short i = 0; i < myButtons.size(); i++)
	{
		Button* currentButton;

		currentButton = myButtons[i];

		if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
		{
			mySelectedButton = currentButton;

			if (currentButton->GetIsDraggable() && myInputManager->KeyDown(CommonUtilities::Keys::LeftMouseButton))
			{
				myIsDragging = true;
				break;
			}
			else if (myInputManager->KeyPressed(CommonUtilities::Keys::LeftMouseButton))
			{
				currentButton->OnClick();
				break;
			}
		}
		else if (myIsDragging)
		{
			if (myInputManager->KeyReleased(CommonUtilities::Keys::LeftMouseButton))
			{
				myIsDragging = false;
				mySliderHandle.OnClick();
				break;
			}
			SliderDragUpdate(&mySliderHandle, aDeltaTime);
			break;
		}
	}
}

void OptionState::CheckKeyBoardInput(const float aDeltaTime)
{
	if (myInputManager->KeyPressed(CommonUtilities::Keys::Down))
	{
		mySelectedButton = mySelectedButton->myConnectedButtons.myDownButton;
	}
	else if (myInputManager->KeyPressed(CommonUtilities::Keys::Up))
	{
		mySelectedButton = mySelectedButton->myConnectedButtons.myUpButton;
	}
	else if (myInputManager->KeyPressed(CommonUtilities::Keys::Enter))
	{
		mySelectedButton->OnClick();
	}
	else if (mySelectedButton == &mySliderHandle)
	{
		if (myInputManager->KeyDown(CommonUtilities::Keys::Left))
		{ 
			SliderKeyboardMove(aDeltaTime, -1);
		}
		else if (myInputManager->KeyDown(CommonUtilities::Keys::Right))
		{
			SliderKeyboardMove(aDeltaTime, 1);
		}
		else if (myInputManager->KeyReleased(CommonUtilities::Keys::Left) || myInputManager->KeyReleased(CommonUtilities::Keys::Right))
		{
			AdjustVolume();
		}
	}
}

void OptionState::SliderDragUpdate(Button *aCurrentButton, const float aDeltaTime)
{
	int mouseX;
	int mouseY;
	myInputManager->GetMousePosition(mouseX, mouseY);
	const CommonUtilities::Vector2f mousePos = { static_cast<float>(mouseX / 1920.f), static_cast<float>(mouseY / 1080.f) };
	const CommonUtilities::Vector2f normalizedDirection = (mousePos - aCurrentButton->GetPosition()).GetNormalized();

	float sliderXPos = aCurrentButton->GetPosition().x;
	sliderXPos += normalizedDirection.x * 0.5f * aDeltaTime;

	if (sliderXPos < mySliderMinX)
	{
		sliderXPos = mySliderMinX;
	}
	else if (sliderXPos > mySliderMaxX)
	{
		sliderXPos = mySliderMaxX;
	}

	aCurrentButton->SetPosition({ sliderXPos, aCurrentButton->GetPosition().y });
	mySliderHandle.SetHitboxOffsetX(22 / 1920.f);
	mySliderHandle.SetHitboxOffsetY(102 / 1080.f);
}

void OptionState::SliderKeyboardMove(const float aDeltaTime, const short aDirectionModifier)
{
	float sliderXPos = mySelectedButton->GetPosition().x + (0.5f * aDirectionModifier) * aDeltaTime;

	if (sliderXPos < mySliderMinX)
	{
		sliderXPos = mySliderMinX;
	}
	else if (sliderXPos > mySliderMaxX)
	{
		sliderXPos = mySliderMaxX;
	}

	mySelectedButton->SetPosition({ sliderXPos, mySelectedButton->GetPosition().y });
	mySliderHandle.SetHitboxOffsetX(22 / 1920.f);
	mySliderHandle.SetHitboxOffsetY(102 / 1080.f);
}

void OptionState::AdjustVolume()
{
	float volumePercent = (mySliderHandle.GetPosition().x - mySliderMinX) / mySliderXDistance;
	myVolume = static_cast<int>(volumePercent * 100);

	if (myVolume < 0)
	{
		myVolume = 0;
	}
	else if (myVolume > 100)
	{
		myVolume = 100;
	}
	
	AudioManager::GetInstance()->SetMusicVolume((static_cast<float>(myVolume) / 100) * 0.6f);
	AudioManager::GetInstance()->SetEffectsVolume(static_cast<float>(myVolume) / 100);
}

void OptionState::ToggleMute()
{
	if (myIsMuted == true)
	{
		myMuteButton.Init([&] {ToggleMute(); }, "Sprites/menu/settings_volumeOn.dds", CommonUtilities::Vector2f(0.375, 0.35f), myInputManager);
		myMuteButton.AttachHoveredSprite("Sprites/menu/settings_volumeOnHover.dds");
		myIsMuted = false;

		mySliderHandle.SetClickable(true);
		mySliderHandle.UnfadeButton();
		myMuteButton.ConnectButton(&mySliderHandle, Button::eConnectSide::Down);
		if (myIsFullscreen)
		{
			mySliderHandle.ConnectButton(&myWindowedButton, Button::eConnectSide::Down);
		}
		else
		{
			mySliderHandle.ConnectButton(&myFullscreenButton, Button::eConnectSide::Down);
		}
		AudioManager::GetInstance()->ToggleMute();
	}
	else
	{
		myMuteButton.Init([&] {ToggleMute(); }, "Sprites/menu/settings_volumeoff.dds", CommonUtilities::Vector2f(0.375, 0.35f), myInputManager);
		myMuteButton.AttachHoveredSprite("Sprites/menu/settings_volumeOffHover.dds");
		myIsMuted = true;

		mySliderHandle.SetClickable(false);
		mySliderHandle.FadeButton();

		if (myIsFullscreen)
		{
			myMuteButton.ConnectButton(&myWindowedButton, Button::eConnectSide::Down);
		}
		else
		{
			myMuteButton.ConnectButton(&myFullscreenButton, Button::eConnectSide::Down);
		}
		AudioManager::GetInstance()->ToggleMute();
	}
	myMuteButton.SetHitboxOffsetX(96 / 1920.f);
	myMuteButton.SetHitboxOffsetY(97 / 1080.f);
}

void OptionState::ToggleFullscreen()
{
	if (myIsFullscreen)
	{
		myIsFullscreen = false;
		Tga2D::CEngine::GetInstance()->SetFullScreen(myIsFullscreen);

		myFullscreenButton.SetClickable(true);
		myFullscreenButton.UnfadeButton();
		
		myWindowedButton.SetClickable(false);
		myWindowedButton.FadeButton();

		mySelectedButton = &myFullscreenButton;
	}
	else
	{
		myIsFullscreen = true;
		Tga2D::CEngine::GetInstance()->SetFullScreen(myIsFullscreen);

		myFullscreenButton.SetClickable(false);
		myFullscreenButton.FadeButton();

		myWindowedButton.SetClickable(true);
		myWindowedButton.UnfadeButton();
		
		mySelectedButton = &myWindowedButton;
	}
	ConnectButtons();
}

void OptionState::Back()
{
	SaveData();
	myShouldPop = true;
}
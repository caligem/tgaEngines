#pragma once
#include "GameState.h"
#include "vector"
#include "Button.h"

class RenderObject;
class Fader;

class OptionState : public GameState
{
public:
	OptionState(CommonUtilities::InputManager* aInputManager, Fader* aFader);
	~OptionState();

	void Init() override;
	void OnEnter() override;
	void OnExit() override;

	eStateStackMessage Update(float aDeltaTime) override;
	void Render() override;

	inline const bool LetThroughRender() const override { return true; }

private:
	bool myShouldPop;
	bool myIsFullscreen;
	bool myIsDragging;
	bool myIsMuted;

	int myVolume;
	
	void LoadData();
	void SaveData();

	void CleanUp();
	void FillRenderBuffer();

	void InitSprites();

	void InitText();
	void UpdateText();
	void DeleteText();

	void InitButtons();
	void UpdateButtons(const float aDeltaTime);
	void ConnectButtons();

	void CheckMouseInput(const float aDeltaTime);
	void CheckKeyBoardInput(const float aDeltaTime);

	void SliderDragUpdate(Button *aCurrentButton, const float aDeltaTime);
	void SliderKeyboardMove(const float aDeltaTime, const short aDirectionModifier);
	void AdjustVolume();
	void ToggleMute();
	void ToggleFullscreen();

	void Back();

	Sprite myBackground;
	Sprite mySettingsBox;
	Sprite mySlider;

	std::vector<RenderObject*> myRenderBuffer;

	Button* mySelectedButton;

	Button myBackButton;
	Button myFullscreenButton;
	Button myWindowedButton;
	Button myMuteButton;

	Button mySliderHandle;
	float mySliderMinX;
	float mySliderMaxX;
	float mySliderXDistance;

	std::vector<Button*> myButtons;

	Fader* myFader;
};


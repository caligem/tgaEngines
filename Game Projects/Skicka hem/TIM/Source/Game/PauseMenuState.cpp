#include "stdafx.h"
#include "PauseMenuState.h"
#include "InputManager.h"
#include "StateStack.h"
#include "Vector.h"
#include "Fader.h"

PauseMenuState::PauseMenuState(CommonUtilities::InputManager* aInputManager, Fader* aFader)
{
	myInputManager = aInputManager;
	myFader = aFader;
	myShouldPop = false;
	myShouldPopToMenu = false;
	mySelectedButton = nullptr;
	myShowDebugLines = false;
}


PauseMenuState::~PauseMenuState()
{
	mySelectedButton = nullptr;
}


void PauseMenuState::Init()
{
	myRenderBuffer.reserve(64);
	InitButtons();
	InitSprites();
}

void PauseMenuState::OnEnter()
{

}

void PauseMenuState::OnExit()
{
}

eStateStackMessage PauseMenuState::Update(float aDeltaTime)
{
	aDeltaTime;
	while (ShowCursor(true) <= 0);
	CleanUp();
	UpdateButtons();
	FillRenderBuffer();


	if (myInputManager->KeyPressed(CommonUtilities::Keys::Escape))
	{
		myShouldPop = true;
	}

	if (myShouldPop)
	{
		return eStateStackMessage::PopSubState;
	}
	if (myShouldPopToMenu)
	{
		return eStateStackMessage::PopMainState;
	}

#ifndef _RETAIL
	if (myInputManager->KeyPressed(CommonUtilities::Keys::F3))
	{
		if (myShowDebugLines)
		{
			myShowDebugLines = false;
		}
		else
		{
			myShowDebugLines = true;
		}
	}
#endif

	return eStateStackMessage::KeepState;
}

void PauseMenuState::Render()
{
	for (unsigned short i = 0; i < myRenderBuffer.size(); i++)
	{
		myRenderBuffer[i]->Render();
	}

	if (myShowDebugLines)
	{
		for (size_t index = 0; index < myButtons.size(); index++)
		{
			myButtons[index]->RenderDebugLines();
		}
	}
}

void PauseMenuState::CleanUp()
{
	myRenderBuffer.clear();
}

void PauseMenuState::FillRenderBuffer()
{
	myRenderBuffer.push_back(&myBackground);
	myRenderBuffer.push_back(&myMenuBox);

	for (unsigned short i = 0; i < myButtons.size(); i++)
	{
		myButtons[i]->FillRenderBuffer(myRenderBuffer);
	}
}

void PauseMenuState::InitSprites()
{
	myBackground.Init("Sprites/menu/menu_backShadeInGame.dds");
	myBackground.SetPivot({ 0.5f, 0.5f });
	myBackground.SetPosition({ 0.5f, 0.5f });

	myMenuBox.Init("Sprites/menu/menuBox.dds");
	myMenuBox.SetPivot({ 0.5f, 0.5f });
	myMenuBox.SetPosition({ 0.5f, 0.5f });
}

void PauseMenuState::InitButtons()
{
	myButtons.reserve(8);

	myResumeButton.Init([&] { myShouldPop = true; }, "Sprites/menu/menu_resume.dds", CommonUtilities::Vector2f(0.5f, 0.35f), myInputManager);
	myResumeButton.SetHitboxOffsetX(0.45f);
	myResumeButton.SetHitboxOffsetY(0.42f);
	myResumeButton.AttachHoveredSprite("Sprites/menu/menu_resume_hover.dds");
	myButtons.push_back(&myResumeButton);

	myOptionsButton.Init([&] { myStateStack->PushSubState(new OptionState(myInputManager, myFader)); }, "Sprites/menu/menu_settings.dds", CommonUtilities::Vector2f(0.5f, 0.50f), myInputManager);
	myOptionsButton.SetHitboxOffsetX(0.45f);
	myOptionsButton.SetHitboxOffsetY(0.42f);
	myOptionsButton.AttachHoveredSprite("Sprites/menu/menu_settings_hover.dds");
	myButtons.push_back(&myOptionsButton);

	myExitToMenuButton.Init([&] { myShouldPopToMenu = true; }, "Sprites/menu/menu_mainMenu.dds", CommonUtilities::Vector2f(0.5f, 0.65f), myInputManager);
	myExitToMenuButton.SetHitboxOffsetX(0.45f);
	myExitToMenuButton.SetHitboxOffsetY(0.42f);
	myExitToMenuButton.AttachHoveredSprite("Sprites/menu/menu_mainMenu_hover.dds");
	myButtons.push_back(&myExitToMenuButton);

	mySelectedButton = &myResumeButton;

	ConnectButtons();
}

void PauseMenuState::UpdateButtons()
{
	for (unsigned short i = 0; i < myButtons.size(); i++)
	{
		myButtons[i]->SetSprite(Button::eSpriteType::Default);
	}

	CheckMouseInput();
	CheckKeyBoardInput();

	mySelectedButton->SetSprite(Button::eSpriteType::Hovered);
}

void PauseMenuState::ConnectButtons()
{
	myResumeButton.ConnectButton(&myOptionsButton, Button::eConnectSide::Down);
	myOptionsButton.ConnectButton(&myExitToMenuButton, Button::eConnectSide::Down);
	myExitToMenuButton.ConnectButton(&myResumeButton, Button::eConnectSide::Down);
}

void PauseMenuState::CheckMouseInput()
{
	for (unsigned short i = 0; i < myButtons.size(); i++)
	{
		Button* currentButton;

		currentButton = myButtons[i];


		if (currentButton->IsMouseInside() && currentButton->GetIsClickable())
		{
			mySelectedButton = currentButton;
			if (myInputManager->KeyPressed(CommonUtilities::Keys::LeftMouseButton))
			{
				currentButton->OnClick();
			}
		}
	}
}

void PauseMenuState::CheckKeyBoardInput()
{
	if (myInputManager->KeyPressed(CommonUtilities::Keys::Down))
	{
		mySelectedButton = mySelectedButton->myConnectedButtons.myDownButton;
	}
	else if (myInputManager->KeyPressed(CommonUtilities::Keys::Up))
	{
		mySelectedButton = mySelectedButton->myConnectedButtons.myUpButton;
	}
	else if (myInputManager->KeyPressed(CommonUtilities::Keys::Enter))
	{
		mySelectedButton->OnClick();
	}
}
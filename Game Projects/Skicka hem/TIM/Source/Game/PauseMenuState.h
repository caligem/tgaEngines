#pragma once
#include "GameState.h"
#include <vector>

#include "Button.h"

class RenderObject;
class Fader;

class PauseMenuState : public GameState
{
public:
	PauseMenuState(CommonUtilities::InputManager* aInputManager, Fader* aFader);
	~PauseMenuState();

	void Init() override;

	void OnEnter() override;
	void OnExit() override;

	eStateStackMessage Update(float aDeltaTime) override;
	void Render() override;
	inline virtual const bool LetThroughRender() const override { return true; }

private:
	bool myShouldPop;
	bool myShouldPopToMenu;
	bool myShowDebugLines;

	void CleanUp();
	void FillRenderBuffer();
	void InitSprites();

	void InitButtons();
	void UpdateButtons();
	void ConnectButtons();

	void CheckMouseInput();
	void CheckKeyBoardInput();

	Sprite myBackground;
	Sprite myMenuBox;
	std::vector<RenderObject*> myRenderBuffer;

	Button* mySelectedButton;

	Button myResumeButton;
	Button myExitToMenuButton;
	Button myOptionsButton;

	std::vector<Button*> myButtons;

	Fader* myFader;
};
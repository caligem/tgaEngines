#include "stdafx.h"
#include "Player.h"
#include "CollisionObject.h"
#include "BoxCollider.h"
#include "CircleCollider.h"
#include "utility.h"
#include "Door.h"
#include "InteractableObject.h"
#include "Sprite.h"
#include "PostMaster.h"
#include "Enemy.h"
#include "ParticleSys.h"
#include "Fader.h"
#include "Text.h"
#include "Bullet.h"
#include "AudioManager.h"
#include "Text.h"

Player::Player(Inventory& aInventory, InGameUI& aUI)
	:myInventory(aInventory),
	myUI(aUI)
{
	myNoClip = false;
	myFreezePlayer = false;
	myIsInvincible = false;
	myPointerIsVisible = true;
	myIsPushBack = false;
	myTeleportUpgradeEnabled = false;
	myCanRecall = false;
}

Player::~Player()
{
}

void Player::Init(CU::InputManager* aInputManager, Fader* aFader, const nlohmann::json& aPlayerData, const ParticleSys& aParticleSys)
{
	ParseJSONFile(aPlayerData);
	myPushBackRange = 30.f;

	myTag = eGameObjectTag::Player;
	myRespawnPosition = myPosition;
	myInputManager = aInputManager;
	myDeathFader = aFader;

	myInvincibilityTimer.Set(myInvulnerableTime, Countdown::type::oneshot, [&] { myIsInvincible = false; });
	myPushBackTimer.Set(0.15f, Countdown::type::oneshot, [&] { myIsPushBack = false; FreezePlayer(false); });
	myFadeTimer.Set(0.2f, Countdown::type::oneshot, [&] {  Respawn(); myDeathFader->SendReadyMessage(); });
	//myWalkSoundTimer.Set(0.5f, Countdown::type::autoreset, [&] {AudioManager::GetInstance()->PlayRandomSound("playerWalk", 4); }, true);
	myHitFlashTimer.Set(0.2f, Countdown::type::oneshot, [&] {EndHitFlash(); });
	myRecallTimer.Set(0.4f, Countdown::type::oneshot, [&] { myCanRecall = true; });

	myGainedHpAnimation.Init("Sprites/fx/greenSparkles_spritesheet.dds");
	myGainedHpAnimation.Setup(2, 4, 8);
	myGainedHpAnimation.SetPivot({ 0.5f, 0.5f });
	myGainedHpAnimation.SetLayerType(eLayerType::EForeground);
	myGainedHpAnimation.SetAnimationSpeed(1.0f);
	myGainedHpAnimation.SetShouldRender(false);

	AddCircleCollider(1.f, { 0.f, -0.5f });
	myWand.Init(myPosition, myCollisionManager, myInventory);
	myWand.AttachHealFunction([&] { Heal(1); });
	myWand.AddCircleCollider(0.3f, { 0.0f, 0.0f });
	std::string path = "Sprites/Wands/inGameWand.dds";
	myWand.AddSprite(path.c_str());

	myMaxHealth = myWand.GetHealth();
	myCurrentHealth = myMaxHealth;
	myUI.SetNumberOfHearts(myCurrentHealth);

	ResetDancing();

	myAnimations[static_cast<int>(ePlayerAnimations::eIdleHoldingWand)].Setup(4, 16, 10);
	myAnimations[static_cast<int>(ePlayerAnimations::eIdleCharging)].Setup(4, 16, 10);
	myAnimations[static_cast<int>(ePlayerAnimations::eIdleNotHoldingWand)].Setup(4, 16, 10);
	myAnimations[static_cast<int>(ePlayerAnimations::eIdleRecalling)].Setup(4, 16, 10);
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingHoldingWand)].Setup(4, 16, 10);
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingCharging)].Setup(4, 16, 10);
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingNotHoldingWand)].Setup(4, 16, 10);
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingRecalling)].Setup(4, 16, 10);
	myAnimations[static_cast<int>(ePlayerAnimations::eThrowing)].Setup(4, 4, 3);
	myAnimations[static_cast<int>(ePlayerAnimations::eThrowing)].SetAnimationSpeed(6.0f);
	myAnimations[static_cast<int>(ePlayerAnimations::eDancing)].Setup(1, 16, 14);
	myAnimations[static_cast<int>(ePlayerAnimations::eDying)].Setup(1, 16, 12);

	myAnimations[static_cast<int>(ePlayerAnimations::eIdleHoldingWand)].SetLoop(true);
	myAnimations[static_cast<int>(ePlayerAnimations::eIdleCharging)].SetLoop(true);
	myAnimations[static_cast<int>(ePlayerAnimations::eIdleNotHoldingWand)].SetLoop(true);
	myAnimations[static_cast<int>(ePlayerAnimations::eIdleRecalling)].SetLoop(true);
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingHoldingWand)].SetLoop(true);
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingCharging)].SetLoop(true);
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingNotHoldingWand)].SetLoop(true);
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingRecalling)].SetLoop(true);
	myAnimations[static_cast<int>(ePlayerAnimations::eThrowing)].SetLoop(false);
	myAnimations[static_cast<int>(ePlayerAnimations::eDancing)].SetLoop(true);
	myAnimations[static_cast<int>(ePlayerAnimations::eDying)].SetLoop(false);

	myAnimations[static_cast<int>(ePlayerAnimations::eMovingHoldingWand)].AddFrameAction(2, [&] {AudioManager::GetInstance()->PlayRandomSound("playerWalk", 4); });
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingHoldingWand)].AddFrameAction(7, [&] {AudioManager::GetInstance()->PlayRandomSound("playerWalk", 4); });
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingCharging)].AddFrameAction(2, [&] {AudioManager::GetInstance()->PlayRandomSound("playerWalk", 4); });
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingCharging)].AddFrameAction(7, [&] {AudioManager::GetInstance()->PlayRandomSound("playerWalk", 4); });
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingNotHoldingWand)].AddFrameAction(2, [&] {AudioManager::GetInstance()->PlayRandomSound("playerWalk", 4); });
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingNotHoldingWand)].AddFrameAction(7, [&] {AudioManager::GetInstance()->PlayRandomSound("playerWalk", 4); });
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingRecalling)].AddFrameAction(2, [&] {AudioManager::GetInstance()->PlayRandomSound("playerWalk", 4); });
	myAnimations[static_cast<int>(ePlayerAnimations::eMovingRecalling)].AddFrameAction(7, [&] {AudioManager::GetInstance()->PlayRandomSound("playerWalk", 4); });

	myTeleportFXAnimation1.Setup(2, 4, 8);
	myTeleportFXAnimation1.SetLoop(false);
	myTeleportFXAnimation1.SetShouldRender(false);
	myTeleportFXAnimation1.SetAnimationSpeed(1.5f);

	myTeleportFXAnimation2.Setup(2, 4, 8);
	myTeleportFXAnimation2.SetLoop(false);
	myTeleportFXAnimation2.SetShouldRender(false);
	myTeleportFXAnimation2.SetAnimationSpeed(1.5f);

	myDirectionPointer.Init("Sprites/Player/pointer.dds");
	//myDirectionPointer.SetPivot({ 0.22f, 0.5f });
	myDirectionPointer.SetPivot({ -0.5f, 0.5f });
	myDirectionPointer.SetLayerType(eLayerType::EForeground);

	myCurrentOrientation = eOrientation::eDown;
	myCurrentPlayerState = ePlayerState::eIdle;
	myCurrentThrowState = eThrowingState::eHolding;
	SetCurrentAnimation();

	myInputManager->ResetInput();

	*aParticleSys.GetEmitter(0);

	myRotation = 1.0f;
};

void Player::OnUpdate(float aDeltatime)
{
	myMaxHealth = myWand.GetHealth();
#ifndef _RETAIL
	// Debug - Kill the player
	if (myInputManager->KeyDown(CU::Keys::K))
	{
		TakeDamage(3, this->GetCollider());
	}
#endif
	myDanceTimer += aDeltatime;

	myInvincibilityTimer.Update(aDeltatime);
	myPushBackTimer.Update(aDeltatime);
	myFadeTimer.Update(aDeltatime);
	myHitFlashTimer.Update(aDeltatime);
	myRecallTimer.Update(aDeltatime);

	if (myCurrentPlayerState == ePlayerState::eDead && myDeathFader->GetFadingState() == Fader::eState::Inactive)
	{
		if (myInputManager->KeyPressed(CU::Keys::Space) || myInputManager->KeyPressed(CU::Keys::Enter) || myInputManager->KeyPressed(CU::Keys::LeftMouseButton))
		{
			myDeathFader->Activate(.2f, 5.f, "");
			myFadeTimer.Reset();
			myFadeTimer.Start();
		}
	}

	if (myCurrentPlayerState != ePlayerState::eDead &&
		myCurrentPlayerState != ePlayerState::eDying)
	{
		myCurrentPlayerState = ePlayerState::eIdle;

		myInventory.Update(aDeltatime, myWand.IsInHands());
		myPosition += myVelocity;
		myVelocity = CU::Vector2f();
		//CU::Vector2f wandPosition = { myPosition.x, myPosition.y - 1.1f };
		//myWand.UpdatePosition(wandPosition);

		if (myIsPushBack)
		{
			myVelocity = pushBackDirection * myPushBackRange * aDeltatime;
		}
	}

	if (!myFreezePlayer)
	{
		if (myInputManager->KeyDown(CU::Keys::Right) || myInputManager->KeyDown(CU::Keys::D))
		{
			Move(eOrientation::eRight);
			myVelocity.x += 1.0f;
			//myWalkSoundTimer.Start();
		}
		if (myInputManager->KeyDown(CU::Keys::Left) || myInputManager->KeyDown(CU::Keys::A))
		{
			Move(eOrientation::eLeft);
			myVelocity.x += -1.0f;
			//myWalkSoundTimer.Start();
		}
		if (myInputManager->KeyDown(CU::Keys::Up) || myInputManager->KeyDown(CU::Keys::W))
		{
			Move(eOrientation::eUp);
			myVelocity.y += -1.0f;
			//myWalkSoundTimer.Start();
		}
		if (myInputManager->KeyDown(CU::Keys::Down) || myInputManager->KeyDown(CU::Keys::S))
		{
			Move(eOrientation::eDown);
			myVelocity.y += 1.0f;
			//myWalkSoundTimer.Start();
		}
		if (myInputManager->KeyPressed(CU::Keys::E))
		{
			PostMaster::SendMessages(eMessageType::PlayerInteract);
		}

		myVelocity.Normalize();

		if (myInputManager->KeyPressed(CU::Keys::LeftMouseButton))
		{
			if (myWand.IsInHands())
			{
				AudioManager::GetInstance()->PlaySound("wandThrow");

				myCurrentThrowState = eThrowingState::eNotHolding;
				myCurrentPlayerState = ePlayerState::eThrowing;

				myAimDirection = GetAimDirection();
				myWand.Throw(myAimDirection);
				myThrowRotation = atan2(myAimDirection.y, myAimDirection.x);

				//myCurrentAnimation->SetRowToAnimate(static_cast<int>(myCurrentOrientation));

				myCanRecall = false;
				myRecallTimer.Reset();
				myRecallTimer.Start();
			}
		}
		else if (myInputManager->KeyDown(CU::Keys::LeftMouseButton))
		{
			if (myCanRecall)
			{
				if (!myWand.IsInHands())
				{
					myCurrentThrowState = eThrowingState::eRecalling;
					myWand.Recall();
				}
			}
		}

		if (myWand.GetWandState() == eWandState::eInHands)
		{
			myCurrentThrowState = eThrowingState::eHolding;

			if (myNoClip)
			{
				myVelocity *= (myUnitData.myMovementSpeed * myInventory.GetActiveItem()->myPlayerMovementSpeedMod) * 5.0f * 32.0f / 800.0f * aDeltatime;
			}
			else
			{
				myVelocity *= (myUnitData.myMovementSpeed * myInventory.GetActiveItem()->myPlayerMovementSpeedMod) * 32.0f / 800.0f * aDeltatime;
			}
		}
		else
		{
			if (myNoClip)
			{
				myVelocity *= (myUnitData.myMovementSpeed * myInventory.GetActiveItem()->myPlayerMovementSpeedWithoutWandMod) * 5.0f * 32.0f / 800.0f * aDeltatime;
			}
			else
			{
				myVelocity *= (myUnitData.myMovementSpeed * myInventory.GetActiveItem()->myPlayerMovementSpeedWithoutWandMod) * 32.0f / 800.0f * aDeltatime;
			}
		}
	}

	if (myInputManager->KeyPressed(CU::Keys::Space) ||
		myInputManager->KeyPressed(CU::Keys::T) ||
		myInputManager->KeyPressed(CU::Keys::RightMouseButton))
	{
		ResetDancing();
		Teleport();
	}

	// Wand throw pointer
	myDirectionPointer.SetPosition({ myPosition.x, myPosition.y - 1.f });
	CU::Vector2f directionTowardsPointer = GetAimDirection().GetNormalized();
	const float rotationTowardsMouse = atan2(directionTowardsPointer.y, directionTowardsPointer.x);
	myDirectionPointer.SetRotation(rotationTowardsMouse);
	CU::Vector2f wandPosition = { myPosition.x, myPosition.y - 1.f };
	myWand.UpdatePosition(wandPosition);
	myWand.OnUpdate(aDeltatime);

	if (myGainedHpAnimation.IsFinished())
	{
		myGainedHpAnimation.SetShouldRender(false);
	}
	if (myGainedHpAnimation.GetShouldRender())
	{
		myGainedHpAnimation.SetPosition({ myPosition.x, myPosition.y - 1.f });
		myGainedHpAnimation.Update(aDeltatime);
	}

	if (myDanceTimer >= 11.43f &&
		myWand.IsInHands() &&
		myCurrentPlayerState != ePlayerState::eDying &&
		myCurrentPlayerState != ePlayerState::eDead)
	{
		myCurrentPlayerState = ePlayerState::eDancing;
	}

	ChangeOrientationDuringSpecialAction();

	SetCurrentAnimation();

	myCurrentAnimation->SetRowToAnimate(static_cast<int>(myCurrentOrientation));

	if (myCurrentPlayerState == ePlayerState::eDying || myCurrentPlayerState == ePlayerState::eDead)
	{
		myCurrentAnimation->Update(aDeltatime * 0.5f);
	}
	else
	{
		myCurrentAnimation->Update(aDeltatime);
	}

	if (myTeleportFXAnimation1.IsFinished())
	{
		myTeleportFXAnimation1.SetShouldRender(false);
		myTeleportFXAnimation2.SetShouldRender(false);
	}
	if (myTeleportFXAnimation1.GetShouldRender())
	{
		myTeleportFXAnimation1.Update(aDeltatime);
		myTeleportFXAnimation2.Update(aDeltatime);
	}

	/*	myInteractableIndicator.SetPosition({ myPosition.x - 3.0f, myPosition.y - 2.5f });*/

	for (auto& emitter : myEmitters)
	{
		emitter->Update(aDeltatime);
	}
}

void Player::FillRenderBuffer(std::vector<RenderObject*>& aBuffer)
{
	myCurrentAnimation->SetPosition({ myPosition.x, myPosition.y });
	aBuffer.push_back(myCurrentAnimation);
	/*	aBuffer.push_back(&myInteractableIndicator);*/
	if (myTeleportFXAnimation1.GetShouldRender())
	{
		aBuffer.push_back(&myTeleportFXAnimation1);
		aBuffer.push_back(&myTeleportFXAnimation2);
	}

	aBuffer.push_back(&myDirectionPointer);


	myWand.FillRenderBuffer(aBuffer);

	for (auto& emitter : myEmitters)
	{
		aBuffer.push_back(emitter);
	}

	if (myGainedHpAnimation.GetShouldRender())
	{
		aBuffer.push_back(&myGainedHpAnimation);
	}
}

void Player::Teleport()
{
	if (myTeleportUpgradeEnabled == false && myInventory.GetShouldGiveTeleport())
	{
		myTeleportUpgradeEnabled = true;
	}
	if (myTeleportUpgradeEnabled && !myWand.IsInHands() && myWand.CanTeleport())
	{
		PostMaster::SendMessages(eMessageType::PlayerTeleport);
		//TODO: Short teleport-out animation on position
		myTeleportFXAnimation2.Reset();
		myTeleportFXAnimation2.SetPosition({ myPosition.x, myPosition.y });
		myTeleportFXAnimation2.SetShouldRender(true);

		myPosition = (myWand.GetPosition() + (myWand.GetCollidedDirection() * static_cast<CircleCollider*>(GetCollider())->GetRadius())) - GetCollider()->GetOffset();
		myWand.SetStateToInHands();
		//TODO: Short teleport-in animation on position
		myTeleportFXAnimation1.Reset();
		myTeleportFXAnimation1.SetPosition({ myPosition.x, myPosition.y });
		myTeleportFXAnimation1.SetShouldRender(true);

		AudioManager::GetInstance()->PlaySound("playerTeleport");
	}

}

void Player::OnCollisionEnter(CollisionObject & aOther)
{
	if (aOther.GetLayer() == eCollisionLayer::Terrain)
	{
		PushBackPlayer(&aOther);
	}
	else if (aOther.GetLayer() == eCollisionLayer::Enemies)
	{
		if (aOther.GetOwner()->GetTag() == eGameObjectTag::Stubbler)
		{
			StubblerCollisionBehaviour(&aOther);
		}
		else if (aOther.GetOwner()->GetTag() == eGameObjectTag::CannonBeard)
		{
			CannonBeardCollisionBehaviour(&aOther);
		}

	}
	else if (aOther.GetLayer() == eCollisionLayer::FallPit)
	{
		if (aOther.GetOwner() != nullptr)
		{
			if (aOther.GetOwner()->GetTag() == eGameObjectTag::BookOfClassChange)
			{
				PushBackPlayer(&aOther);
			}
		}

		if (aOther.GetOwner() == nullptr)
		{
			PushBackPlayer(&aOther);
		}
	}
	else if (aOther.GetLayer() == eCollisionLayer::Interactable)
	{
		PushBackPlayer(&aOther);
		/*		myInteractableIndicator.SetShouldRender(true);*/
	}
	else if (aOther.GetLayer() == eCollisionLayer::Projectile)
	{
		Bullet* bullet = static_cast<Bullet*>(aOther.GetOwner());
		TakeDamage(bullet->GetDamage(), &aOther);
	}
	else if (aOther.GetLayer() == eCollisionLayer::Checkpoint)
	{
		SetRespawnPosition(aOther.GetPosition());
	}
}

void Player::OnCollisionStay(CollisionObject & aOther)
{
	if (aOther.GetLayer() == eCollisionLayer::Terrain)
	{
		PushBackPlayer(&aOther);
	}
	else if (aOther.GetLayer() == eCollisionLayer::FallPit)
	{
		if (aOther.GetOwner() != nullptr)
		{
			if (aOther.GetOwner()->GetTag() == eGameObjectTag::BookOfClassChange)
			{
				PushBackPlayer(&aOther);
			}
		}

		if (aOther.GetOwner() == nullptr)
		{
			PushBackPlayer(&aOther);
		}
	}
	else if (aOther.GetLayer() == eCollisionLayer::Enemies)
	{
		if (aOther.GetOwner()->GetTag() == eGameObjectTag::Stubbler)
		{
			StubblerCollisionBehaviour(&aOther);
		}
		else if (aOther.GetOwner()->GetTag() == eGameObjectTag::CannonBeard)
		{
			CannonBeardCollisionBehaviour(&aOther);
		}
	}
	else if (aOther.GetLayer() == eCollisionLayer::Interactable)
	{
		PushBackPlayer(&aOther);
	}
}

void Player::OnCollisionLeave(CollisionObject & aOther)
{
	// 	if (aOther.GetLayer() == eCollisionLayer::Interactable)
	// 	{
	// 		myInteractableIndicator.SetShouldRender(false);
	// 	}
	aOther;
}

void Player::ToggleNoClip()
{
	if (!myNoClip)
	{
		GetCollider()->SetIsSolid(false);
		myNoClip = true;
	}
	else
	{
		myNoClip = false;
		GetCollider()->SetIsSolid(true);
	}
}

void Player::StopVelocity()
{
	myVelocity = { 0.f, 0.f };
}

void Player::FreezePlayer(bool aShouldFreeze)
{
	myFreezePlayer = aShouldFreeze;
}

void Player::RecievedWand()
{
	myCurrentThrowState = eThrowingState::eHolding;
}

void Player::TeleportWandToPlayer()
{
	myWand.Reset();
}

void Player::PushBackPlayer(CollisionObject* aCollisionObject)
{
	CommonUtilities::Vector2f closestBoxPoint = GetCollider()->GetPosition() + GetCollider()->GetOffset();
	BoxCollider* terrainCollider = dynamic_cast<BoxCollider*>(aCollisionObject);
	if (terrainCollider != nullptr)
	{
		closestBoxPoint.x = cit::Clamp(closestBoxPoint.x, terrainCollider->GetPosition().x + terrainCollider->GetOffset().x - terrainCollider->GetWidth() / 2.f, terrainCollider->GetPosition().x + terrainCollider->GetOffset().x + terrainCollider->GetWidth() / 2.f);
		closestBoxPoint.y = cit::Clamp(closestBoxPoint.y, terrainCollider->GetPosition().y + terrainCollider->GetOffset().y - terrainCollider->GetHeight() / 2.f, terrainCollider->GetPosition().y + terrainCollider->GetOffset().y + terrainCollider->GetHeight() / 2.f);
		const CommonUtilities::Vector2f rangeToCollision = closestBoxPoint - (GetCollider()->GetPosition() + GetCollider()->GetOffset());
		//might add velocity to rangeToCollision 
		const CommonUtilities::Vector2f pushRange = { rangeToCollision.GetNormalized() * (static_cast<CircleCollider*>(myCollisionManager->GetCollisionObject(myCollisionLayer, myColliderIndex))->GetRadius()) };
		const CommonUtilities::Vector2f delta = { pushRange - rangeToCollision };

		const float antijitter = 0.04f;

		if (rangeToCollision.x * rangeToCollision.x > rangeToCollision.y * rangeToCollision.y)
		{
			if (delta.x * delta.x > antijitter)
			{
				myVelocity.x -= delta.x;
			}
			else
			{
				if ((myVelocity.x < 0.f && rangeToCollision.x < 0.f) || (myVelocity.x > 0.f && rangeToCollision.x > 0.f))
				{
					myVelocity.x = 0.0f;
				}
			}
		}
		else
		{
			if (delta.y * delta.y > antijitter)
			{
				myVelocity.y -= delta.y;
			}
			else
			{
				if ((myVelocity.y < 0.f && rangeToCollision.y < 0.f) || (myVelocity.y > 0.f && rangeToCollision.y > 0.f))
				{
					myVelocity.y = 0.0f;
				}
			}
		}
	}
}

void Player::Respawn()
{
	StopVelocity();
	myPosition = myRespawnPosition;
	myCurrentHealth = myWand.GetHealth();
	myCurrentPlayerState = ePlayerState::eIdle;
	myCurrentThrowState = eThrowingState::eHolding;
	myWand.Reset();
	FreezePlayer(false);
	myUI.SetNumberOfHearts(myCurrentHealth);
	myPointerIsVisible = true;
	AudioManager::GetInstance()->PlaySound("playerRespawn");
	PostMaster::SendMessages(eMessageType::ResetEnemies);
}

CU::Vector2f Player::GetAimDirection()
{
	int mouseX;
	int mouseY;
	myInputManager->GetMousePosition(mouseX, mouseY);

	const CommonUtilities::Vector2f mousePos = { static_cast<float>(mouseX), static_cast<float>(mouseY) };
	const CommonUtilities::Vector2f origin = { 1920.f / 2.f, 1080.f / 2.f };
	const CommonUtilities::Vector2f direction = mousePos - origin;

	return direction.GetNormalized();
}

void Player::SetCurrentAnimation()
{
	if (myCurrentPlayerState == ePlayerState::eDead)
	{
		//myCurrentAnimation = &myAnimations[static_cast<int>(ePlayerAnimations::eDying)];
	}
	else if (myCurrentPlayerState == ePlayerState::eDying)
	{
		myCurrentAnimation = &myAnimations[static_cast<int>(ePlayerAnimations::eDying)];
		if (myCurrentAnimation->IsFinished())
		{
			myCurrentPlayerState = ePlayerState::eDead;
		}
	}
	else if (myCurrentPlayerState == ePlayerState::eThrowing)
	{
		myCurrentAnimation = &myAnimations[static_cast<int>(ePlayerAnimations::eThrowing)];
		if (myCurrentAnimation->IsFinished())
		{
			myCurrentAnimation->Reset();
			myCurrentAnimation = &myAnimations[static_cast<int>(ePlayerAnimations::eIdleNotHoldingWand)];
		}
	}
	else if (myCurrentPlayerState == ePlayerState::eMoving)
	{
		switch (myCurrentThrowState)
		{
			case eThrowingState::eHolding:
			{
				myCurrentAnimation = &myAnimations[static_cast<int>(ePlayerAnimations::eMovingHoldingWand)];
				myCurrentAnimation->SetAnimationSpeed((myUnitData.myMovementSpeed / 300.0f) * myInventory.GetActiveItem()->myPlayerMovementSpeedMod);
				break;
			}
			case eThrowingState::eNotHolding:
			{
				myCurrentAnimation = &myAnimations[static_cast<int>(ePlayerAnimations::eMovingNotHoldingWand)];
				myCurrentAnimation->SetAnimationSpeed((myUnitData.myMovementSpeed / 300.0f) * myInventory.GetActiveItem()->myPlayerMovementSpeedWithoutWandMod);
				break;
			}
			case eThrowingState::eRecalling:
			{
				myCurrentAnimation = &myAnimations[static_cast<int>(ePlayerAnimations::eMovingRecalling)];
				myCurrentAnimation->SetAnimationSpeed((myUnitData.myMovementSpeed / 300.0f) * myInventory.GetActiveItem()->myPlayerMovementSpeedOnThrowMod);
				break;
			}
			default:
				assert(false && "Unknown wandstate found when trying to set player-animation.");
				break;
		}
	}
	else if (myCurrentPlayerState == ePlayerState::eIdle)
	{
		switch (myCurrentThrowState)
		{
			case eThrowingState::eHolding:
			{
				myCurrentAnimation = &myAnimations[static_cast<int>(ePlayerAnimations::eIdleHoldingWand)];
				break;
			}
			case eThrowingState::eNotHolding:
			{
				myCurrentAnimation = &myAnimations[static_cast<int>(ePlayerAnimations::eIdleNotHoldingWand)];
				break;
			}
			case eThrowingState::eRecalling:
			{
				myCurrentAnimation = &myAnimations[static_cast<int>(ePlayerAnimations::eIdleRecalling)];
				break;
			}
			default:
				assert(false && "Unknown wandstate found when trying to set player-animation.");
				break;
		}
	}
	else if (myCurrentPlayerState == ePlayerState::eDancing)
	{
		if (!myIsCurrentlyDancing)
		{
			// Start dancin'
			myIsCurrentlyDancing = true;
			myCurrentAnimation = &myAnimations[static_cast<int>(ePlayerAnimations::eDancing)];
			myCurrentAnimation->Reset();
			myCurrentOrientation = eOrientation::eDown;
		}
	}
	else
	{
		assert(false && "Unknown playerstate found when trying to set player-animation.");
	}
}

void Player::ActivateInvincibility()
{
	myIsInvincible = true;
	myInvincibilityTimer.Reset();
	myInvincibilityTimer.Start();
}

void Player::ChangeOrientationDuringSpecialAction()
{
	if (myInputManager->KeyDown(CU::Keys::LeftMouseButton))
	{
		ResetDancing();
		// Recalling wand -- orient towards the wand
		if (myCurrentThrowState == eThrowingState::eRecalling)
		{
			if ((myPosition - myWand.GetPosition()).Length2() > 100.f)
			{
				const CU::Vector2f directionTowardsWand = myPosition - myWand.GetPosition();
				myRotation = atan2(-directionTowardsWand.y, -directionTowardsWand.x);
			}
		}
		else
		{
			if (myCurrentPlayerState == ePlayerState::eThrowing)
			{
				const CU::Vector2f directionTowardsMouse = GetAimDirection();
				myRotation = atan2(directionTowardsMouse.y, directionTowardsMouse.x);
			}
			else
			{
				return;
			}
		}

		if (myRotation > 0.75f && myRotation < 2.5f)
		{
			myCurrentOrientation = eOrientation::eDown;
		}
		else if (myRotation > -2.5f && myRotation < -0.75f)
		{
			myCurrentOrientation = eOrientation::eUp;
		}
		else if (myRotation < -2.5f || myRotation > 2.5f) // Yes, the || is correct.
		{
			myCurrentOrientation = eOrientation::eLeft;
		}
		else if (myRotation > -0.75f && myRotation < 0.75f)
		{
			myCurrentOrientation = eOrientation::eRight;
		}
	}
	else if (myCurrentPlayerState == ePlayerState::eDying)
	{
		myCurrentOrientation = eOrientation::eDown;
	}
}

void Player::Move(eOrientation aOrientation)
{
	ResetDancing();

	myCurrentPlayerState = ePlayerState::eMoving;
	myCurrentOrientation = aOrientation;
}
void Player::ParseJSONFile(const nlohmann::json& aPlayerData)
{
	// If / when we want debugging info, add else on the following if-statements
	if (aPlayerData.find("spriteIdleWand") != aPlayerData.end())
		myAnimations[static_cast<int>(ePlayerAnimations::eIdleHoldingWand)] = Animation(aPlayerData["spriteIdleWand"].get<std::string>().c_str());
	if (aPlayerData.find("spriteIdleCharge") != aPlayerData.end())
		myAnimations[static_cast<int>(ePlayerAnimations::eIdleCharging)] = Animation(aPlayerData["spriteIdleCharge"].get<std::string>().c_str());
	if (aPlayerData.find("spriteIdleNoWand") != aPlayerData.end())
		myAnimations[static_cast<int>(ePlayerAnimations::eIdleNotHoldingWand)] = Animation(aPlayerData["spriteIdleNoWand"].get<std::string>().c_str());
	if (aPlayerData.find("spriteIdleRecall") != aPlayerData.end())
		myAnimations[static_cast<int>(ePlayerAnimations::eIdleRecalling)] = Animation(aPlayerData["spriteIdleRecall"].get<std::string>().c_str());

	if (aPlayerData.find("spriteMovingWand") != aPlayerData.end())
		myAnimations[static_cast<int>(ePlayerAnimations::eMovingHoldingWand)] = Animation(aPlayerData["spriteMovingWand"].get<std::string>().c_str());
	if (aPlayerData.find("spriteMovingCharge") != aPlayerData.end())
		myAnimations[static_cast<int>(ePlayerAnimations::eMovingCharging)] = Animation(aPlayerData["spriteMovingCharge"].get<std::string>().c_str());
	if (aPlayerData.find("spriteMovingNoWand") != aPlayerData.end())
		myAnimations[static_cast<int>(ePlayerAnimations::eMovingNotHoldingWand)] = Animation(aPlayerData["spriteMovingNoWand"].get<std::string>().c_str());
	if (aPlayerData.find("spriteMovingRecall") != aPlayerData.end())
		myAnimations[static_cast<int>(ePlayerAnimations::eMovingRecalling)] = Animation(aPlayerData["spriteMovingRecall"].get<std::string>().c_str());

	if (aPlayerData.find("spriteThrowing") != aPlayerData.end())
		myAnimations[static_cast<int>(ePlayerAnimations::eThrowing)] = Animation(aPlayerData["spriteThrowing"].get<std::string>().c_str());
	if (aPlayerData.find("spriteDancing") != aPlayerData.end())
		myAnimations[static_cast<int>(ePlayerAnimations::eDancing)] = Animation(aPlayerData["spriteDancing"].get<std::string>().c_str());
	if (aPlayerData.find("spriteDeath") != aPlayerData.end())
		myAnimations[static_cast<int>(ePlayerAnimations::eDying)] = Animation(aPlayerData["spriteDeath"].get<std::string>().c_str());

	if (aPlayerData.find("spriteTeleportFX1") != aPlayerData.end())
		myTeleportFXAnimation1 = Animation(aPlayerData["spriteTeleportFX1"].get<std::string>().c_str());
	if (aPlayerData.find("spriteTeleportFX2") != aPlayerData.end())
		myTeleportFXAnimation2 = Animation(aPlayerData["spriteTeleportFX2"].get<std::string>().c_str());

	auto xyz = myAnimations[static_cast<int>(ePlayerAnimations::eDancing)].GetColor();
	//if (aPlayerData.find("scale") != aPlayerData.end())
	//{
	//	float scale = aPlayerData["scale"];
	//	myCurrentAnimation->SetUVScale({ scale, scale });
	//}

	if (aPlayerData.find("movementSpeed") != aPlayerData.end())
	{
		myUnitData.myMovementSpeed = aPlayerData["movementSpeed"];
	}

	if (aPlayerData.find("reducedMovementSpeed") != aPlayerData.end())
	{
		myUnitData.myReducedMovementSpeed = aPlayerData["reducedMovementSpeed"];
	}

	if (aPlayerData.find("speedIncreasePerMilliSec") != aPlayerData.end())
	{
		myUnitData.mySpeedIncreasePerMilliSec = aPlayerData["speedIncreasePerMilliSec"];
	}

	if (aPlayerData.find("invulnerableTime") != aPlayerData.end())
	{
		myInvulnerableTime = aPlayerData["invulnerableTime"];
	}
	if (aPlayerData.find("chargeUpTime") != aPlayerData.end())
	{
		myChargeUpTime = aPlayerData["chargeUpTime"];
	}
	else
	{
		myChargeUpTime = 0.5f;
	}
}

inline void Player::TakeDamage(int aDamage, CollisionObject* aCollisionObject)
{
	if (!myIsInvincible && myCurrentPlayerState != ePlayerState::eDying && myCurrentPlayerState != ePlayerState::eDead)
	{
		myCurrentHealth -= aDamage;
		StartHitFlash();
		if (myCurrentHealth <= 0)
		{
			if (myCurrentPlayerState != ePlayerState::eDying && myCurrentPlayerState != ePlayerState::eDead)
			{
				FreezePlayer(true);
				// TODO: Freeze enemies?
				myCurrentPlayerState = ePlayerState::eDying;
				myAnimations[static_cast<int>(ePlayerAnimations::eDying)].Reset();
				AudioManager::GetInstance()->PlayRandomSound("playerDeath", 2);
				myPointerIsVisible = false;
			}
		}
		else
		{
			DamagePushBack(aCollisionObject);
			ActivateInvincibility();
			AudioManager::GetInstance()->PlayRandomSound("playerHurt", 3);
		}
		myUI.SetNumberOfHearts(myCurrentHealth);
	}
}

void Player::Heal(int aHealAmount)
{
	if (myCurrentHealth < myMaxHealth)
	{
		AudioManager::GetInstance()->PlaySound("playerHeal");

		myGainedHpAnimation.Reset();
		myGainedHpAnimation.SetPosition({ myPosition.x, myPosition.y - 1.f });
		myGainedHpAnimation.SetShouldRender(true);
	}

	myCurrentHealth += aHealAmount;
	if (myCurrentHealth > myMaxHealth)
	{
		myCurrentHealth -= aHealAmount;
	}

	myUI.SetNumberOfHearts(myCurrentHealth);
}

void Player::DamagePushBack(CollisionObject * aCollisionObject)
{
	const CommonUtilities::Vector2f playerPosition = GetCollider()->GetPosition() + GetCollider()->GetOffset();
	const CommonUtilities::Vector2f enemyPosition = aCollisionObject->GetPosition() + aCollisionObject->GetOffset();

	pushBackDirection = (playerPosition - enemyPosition).GetNormalized();
	myIsPushBack = true;
	FreezePlayer(true);
	myPushBackTimer.Reset();
	myPushBackTimer.Start();
}
void Player::ResetDancing()
{
	myDanceTimer = 0.0f;
	myIsCurrentlyDancing = false;
}

void Player::StubblerCollisionBehaviour(CollisionObject * aCollisionObject)
{
	Enemy* enemy = static_cast<Enemy*>(aCollisionObject->GetOwner());
	TakeDamage(enemy->GetDamage(), aCollisionObject);

	const CommonUtilities::Vector2f position = GetCollider()->GetPosition() + GetCollider()->GetOffset();
	const CommonUtilities::Vector2f collisionVector = (aCollisionObject->GetPosition() + aCollisionObject->GetOffset()) - position;
	const float colliderRadius = static_cast<CircleCollider*>(GetCollider())->GetRadius();
	const float enemyColliderRadius = static_cast<CircleCollider*>(aCollisionObject)->GetRadius();
	const float colliderDistance = (colliderRadius + enemyColliderRadius);
	const float collisionDistance = collisionVector.Length();

	if (colliderDistance < collisionDistance)
	{
		return;
	}

	const float collisionRange = colliderDistance - collisionDistance;
	const CommonUtilities::Vector2f collisionDirection = collisionVector.GetNormalized();

	aCollisionObject->GetOwner()->SetPosition(aCollisionObject->GetOwner()->GetPosition() + (collisionDirection * (collisionRange)));
}

void Player::CannonBeardCollisionBehaviour(CollisionObject * aCollisionObject)
{
	Enemy* enemy = static_cast<Enemy*>(aCollisionObject->GetOwner());
	TakeDamage(enemy->GetDamage(), aCollisionObject);

	const CommonUtilities::Vector2f position = GetCollider()->GetPosition() + GetCollider()->GetOffset();
	const CommonUtilities::Vector2f collisionVector = position - (aCollisionObject->GetPosition() + aCollisionObject->GetOffset());
	const float colliderRadius = static_cast<CircleCollider*>(GetCollider())->GetRadius();
	const float enemyColliderRadius = static_cast<CircleCollider*>(aCollisionObject)->GetRadius();
	const float collisionRange = (colliderRadius + enemyColliderRadius) - collisionVector.Length();
	const CommonUtilities::Vector2f collisionDirection = collisionVector.GetNormalized();

	myVelocity += (collisionDirection * (collisionRange));
}

void Player::StartHitFlash()
{
	myHitFlashTimer.Reset();
	myHitFlashTimer.Start();
	for (int i = 0; i < myAnimations.size(); ++i)
	{
		myAnimations[i].SetColor({ 4.0f, 4.0f, 4.0f, 0.8f });
	}
}

void Player::EndHitFlash()
{
	for (int i = 0; i < myAnimations.size(); ++i)
	{
		myAnimations[i].SetColor({ 1.0f, 1.0f, 1.0f, 1.0f });
	}
}

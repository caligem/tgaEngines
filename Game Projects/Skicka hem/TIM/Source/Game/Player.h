#pragma once
#include "GameObject.h"
#include "json.h"
#include "UnitData.h"
#include "Wand.h"
#include <array>
#include "InputManager.h"
#include "Animation.h"
#include "Inventory.h"
#include "Countdown.h"
#include "InGameUI.h"
#include "Vector.h"

class ParticleSys;
class Fader;

class Player : public GameObject
{
public:
	Player(Inventory& aInventory, InGameUI& aUI);
	~Player();
	void Init(CU::InputManager* aInputManager, Fader* aFader, const nlohmann::json& aPlayerData, const ParticleSys& aParticleSys);
	void OnUpdate(float aDeltatime) override;
	void FillRenderBuffer(std::vector<RenderObject*>& aBuffer);
	void Teleport();
	inline void SetRespawnPosition(const CommonUtilities::Vector2f& aRespawnPosition) { myRespawnPosition = aRespawnPosition; }
	inline Inventory& GetInventory() { return myInventory; }
	void OnCollisionEnter(CollisionObject &aOther) override;
	void OnCollisionStay(CollisionObject &aOther) override;
	void OnCollisionLeave(CollisionObject &aOther) override;

	enum class ePlayerState
	{
		eIdle,
		eMoving,
		eThrowing,
		eDancing,
		eDying,
		eDead,
		COUNT
	};

	enum class eThrowingState
	{
		eHolding,
		eNotHolding,
		eRecalling,
		COUNT
	};

	enum class ePlayerAnimations
	{
		eIdleHoldingWand,
		eIdleCharging,
		eIdleNotHoldingWand,
		eIdleRecalling,
		eMovingHoldingWand,
		eMovingCharging,
		eMovingNotHoldingWand,
		eMovingRecalling,
		eThrowing,
		eDancing,
		eDying,
		COUNT
	};

	enum class eOrientation
	{
		eDown,
		eLeft,
		eUp,
		eRight,
		COUNT
	};

	void ToggleNoClip();
	void StopVelocity();
	void FreezePlayer(bool aShouldFreeze);
	void RecievedWand();
	void TeleportWandToPlayer();

	const ePlayerState GetPlayerState() { return myCurrentPlayerState; }

private:
	bool myFreezePlayer;
	bool myNoClip;
	bool myTeleportUpgradeEnabled;
	bool myPointerIsVisible;
	bool myIsInvincible;
	bool myCanRecall;

	int myMaxHealth;
	int myCurrentHealth;
	float myInvulnerableTime;
	float myChargeUpTime;

	float myRotation;

	float myDanceTimer;
	bool myIsCurrentlyDancing;

	bool myIsPushBack;
	float myPushBackRange;
	CommonUtilities::Vector2f pushBackDirection;

	std::array<Animation, static_cast<int>(ePlayerAnimations::COUNT)> myAnimations;

	Animation* myCurrentAnimation;
	Animation myTeleportFXAnimation1;
	Animation myTeleportFXAnimation2;
	Animation myGainedHpAnimation;

	Sprite myDirectionPointer;

	Fader* myDeathFader;

	UnitData myUnitData;
	Wand myWand;

	CU::InputManager* myInputManager;
	Inventory& myInventory;
	InGameUI& myUI;

	ePlayerState myCurrentPlayerState;
	eThrowingState myCurrentThrowState;
	eOrientation myCurrentOrientation;

	CU::Vector2f GetAimDirection();
	CU::Vector2f myAimDirection;
	CU::Vector2f myVelocity;
	CU::Vector2f myRespawnPosition;

	Countdown myInvincibilityTimer;
	Countdown myPushBackTimer;
	Countdown myFadeTimer;
	Countdown myWalkSoundTimer;
	Countdown myHitFlashTimer;
	Countdown myRecallTimer;

	void Respawn();
	void PushBackPlayer(CollisionObject* aCollisionObject);
	void ActivateInvincibility();
	void ParseJSONFile(const nlohmann::json& aPlayerData);

	float myThrowRotation;
	void SetCurrentAnimation();
	void ChangeOrientationDuringSpecialAction();
	void Move(eOrientation aOrientation);
	
	void TakeDamage(int aDamage, CollisionObject* aCollisionObject);
	void Heal(int aHealAmount);
	void DamagePushBack(CollisionObject* aCollisionObject);
	void ResetDancing();
	void StubblerCollisionBehaviour(CollisionObject* aCollisionObject);
	void CannonBeardCollisionBehaviour(CollisionObject* aCollisionObject);
	void StartHitFlash();
	void EndHitFlash();
};

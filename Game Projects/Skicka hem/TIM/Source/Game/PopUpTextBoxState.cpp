#include "stdafx.h"
#include "PopUpTextBoxState.h"
#include "InputManager.h"
#include "Fader.h"
#include "Subscriber.h"

PopUpTextBoxState::PopUpTextBoxState()
{
	delete myTextBox;
	myTextBox = nullptr;
}


PopUpTextBoxState::PopUpTextBoxState(CU::InputManager* aInputManager, const Message& aMessage)
{
	Message cool = aMessage;
	myInputManager = aInputManager;
	myShouldPop = false;
	switch (aMessage.data.myPopUpTextBox)
	{
		case ePopUpTextBox::FirstWand:
		{
			myTextBox = new Tga2D::CSprite("Sprites/PopUpTextBoxes/FirstWand.dds");
			break;
		}
		case ePopUpTextBox::FirstKey:
		{
			myTextBox = new Tga2D::CSprite("Sprites/PopUpTextBoxes/FirstKey.dds");
			break;
		}
		case ePopUpTextBox::FirstEnemy:
		{
			myTextBox = new Tga2D::CSprite("Sprites/PopUpTextBoxes/FirstEnemy.dds");
			break;
		}
		case ePopUpTextBox::SeesBook:
		{
			myTextBox = new Tga2D::CSprite("Sprites/PopUpTextBoxes/BOCC.dds");
			break;
		}
	}
	myTextBox->SetPivot({ 0.5f,0.5f });
	myTextBox->SetPosition({ 0.5f,0.5f });
	
}

PopUpTextBoxState::~PopUpTextBoxState()
{
}

void PopUpTextBoxState::Init()
{
}

void PopUpTextBoxState::OnEnter()
{
}

void PopUpTextBoxState::OnExit()
{
}

eStateStackMessage PopUpTextBoxState::Update(float aDeltaTime)
{
	float killingErrors = aDeltaTime;	//Killing errors.
	killingErrors++;					//Killing errors.
	if (
		myInputManager->KeyPressed(CU::Keys::Escape) ||
		myInputManager->KeyPressed(CU::Keys::LeftMouseButton) ||
		myInputManager->KeyPressed(CU::Keys::Space) ||
		myInputManager->KeyPressed(CU::Keys::Enter) ||
		myInputManager->KeyPressed(CU::Keys::E)
		)
	{
		return eStateStackMessage::PopSubState;
	}
	else
	{
		return eStateStackMessage::KeepState;
	}
}

void PopUpTextBoxState::Render()
{
	myTextBox->Render();
}

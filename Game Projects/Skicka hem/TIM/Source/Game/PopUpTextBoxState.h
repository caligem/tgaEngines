#pragma once
#include "GameState.h"

struct Message;
namespace Tga2D
{
	class CSprite;
}

class InputManager;
class PopUpTextBoxState : public GameState
{
public:
	PopUpTextBoxState();
	PopUpTextBoxState(CU::InputManager* aInputManager, const Message& aMessage);
	~PopUpTextBoxState();

	virtual void Init();

	virtual void OnEnter();
	virtual void OnExit();

	virtual eStateStackMessage Update(float aDeltaTime);
	virtual void Render();
	inline const bool LetThroughRender() const override { return true; }
private:
	bool myShouldPop;
	Tga2D::CSprite* myTextBox;
};


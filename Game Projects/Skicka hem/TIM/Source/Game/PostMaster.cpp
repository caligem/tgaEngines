#include "stdafx.h"
#include "PostMaster.h"
#include <assert.h>

PostMaster* PostMaster::ourInstance = nullptr;

void PostMaster::Create()
{
	assert(ourInstance == nullptr && "Instance already created");
	ourInstance = new PostMaster();
	for (size_t messageTypeIndex = 0; messageTypeIndex < static_cast<int>(eMessageType::COUNT); ++messageTypeIndex)
	{
		std::vector<Subscriber*> newSubscriberList;
		ourInstance->mySubscribers.push_back(newSubscriberList);
	}
}

void PostMaster::Destroy()
{
	assert(ourInstance != nullptr && "Instance not created");

	for (size_t index = ourInstance->mySubscribers.size(); index > 0; --index)
	{
		for (size_t subscriberIndex = ourInstance->mySubscribers[index - 1].size(); subscriberIndex > 0; --subscriberIndex)
		{
			ourInstance->mySubscribers[index - 1].pop_back();
			continue;
		}

		ourInstance->mySubscribers.pop_back();
	}

	delete ourInstance;
	ourInstance = nullptr;
}

void PostMaster::Subscribe(const eMessageType aMessageType, Subscriber * aSubScriber)
{
	ourInstance->mySubscribers[static_cast<int>(aMessageType)].push_back(aSubScriber);
}

void PostMaster::SendMessages(const Message aMessage)
{
	int index = static_cast<int>(aMessage.myMessageType);

	for (size_t subscriberIndex = 0; subscriberIndex < ourInstance->mySubscribers[index].size(); subscriberIndex++)
	{
		ourInstance->mySubscribers[index][subscriberIndex]->ReceiveMessage(aMessage);
	}
}

void PostMaster::UnSubscribe(const eMessageType aMessageType, Subscriber * aSubScriber)
{
	int index = static_cast<int>(aMessageType);

	for (size_t subscriberIndex = ourInstance->mySubscribers[index].size(); subscriberIndex > 0; --subscriberIndex)
	{
		if (ourInstance->mySubscribers[index][subscriberIndex - 1] == aSubScriber)
		{
			ourInstance->mySubscribers[index].erase(ourInstance->mySubscribers[index].begin() + subscriberIndex - 1);
		}
	}
}

void PostMaster::UnSubscribe(const eMessageType aMessageType)
{
	int index = static_cast<int>(aMessageType);

	ourInstance->mySubscribers[index].clear();
}

PostMaster::PostMaster()
{
}


PostMaster::~PostMaster()
{
}

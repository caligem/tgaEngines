#include "stdafx.h"
#include "ProjectileManager.h"
#include "GameObject.h"
#include "CollisionManager.h"
#include "CollisionObject.h"
#include <iostream>

ProjectileManager::ProjectileManager()
{
}


ProjectileManager::~ProjectileManager()
{
}

void ProjectileManager::Init(CollisionManager& aCollisionManager)
{
	myCollisionManager = &aCollisionManager;

	FillFreeIndexes();
	NewAll();
}

void ProjectileManager::Update(float aDeltaTime)
{
	CleanUp();

	for (size_t i = 0; i < myOccupiedIndexes.size(); i++)
	{
		int index = myOccupiedIndexes[i];

		myBullets[index].OnUpdate(aDeltaTime);
	}

	for (size_t i = 0; i < myOccupiedExplosionIndexes.size(); i++)
	{
		int index = myOccupiedExplosionIndexes[i];

		myExplosionAnimations[index].Update(aDeltaTime);
	}
}

void ProjectileManager::FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer)
{
	for (size_t i = 0; i < myOccupiedIndexes.size(); i++)
	{
		int index = myOccupiedIndexes[i];

		myBullets[index].FillRenderBuffer(aRenderBuffer);
	}

	for (size_t i = 0; i < myOccupiedExplosionIndexes.size(); i++)
	{
		int index = myOccupiedExplosionIndexes[i];

		aRenderBuffer.push_back(&myExplosionAnimations[index]);
	}
}

void ProjectileManager::CreateBullet(const float aProjectileSpeed, const CommonUtilities::Vector2f & aDirection, const CommonUtilities::Vector2f& aPosition, const int aDamage, const char* aPath)
{
	int index = GetFreeIndex(eIndexType::eBullet);
	Bullet* newBullet = &myBullets[index];
	newBullet->Reset(aProjectileSpeed, aDirection, aPath, aDamage);
	newBullet->SetPosition(aPosition);
	myOccupiedIndexes.push_back(index);
}

void ProjectileManager::Reset()
{
	for (int i = static_cast<int>(myOccupiedIndexes.size()) - 1; i >= 0; i--)
	{
		int index = myOccupiedIndexes[i];
		myBullets[index].Inactivate();
	}

	CleanUp();
}

void ProjectileManager::FillFreeIndexes()
{
	for (int objectIndex = 0; objectIndex < myBullets.size(); objectIndex++)
	{
		myFreeIndexes.push(objectIndex);
	}
	for (int animationIndex = 0; animationIndex < myExplosionAnimations.size(); animationIndex++)
	{
		myFreeExplosionIndexes.push(animationIndex);
	}
}

int ProjectileManager::GetFreeIndex(const eIndexType aIndex)
{
	int freeIndex = -1;
	if (aIndex == eIndexType::eBullet)
	{
		freeIndex = myFreeIndexes.top();
		myFreeIndexes.pop();
	}
	else if (aIndex == eIndexType::eExplosion)
	{
		freeIndex = myFreeExplosionIndexes.top();
		myFreeExplosionIndexes.pop();
	}
	if (freeIndex == -1)
	{
		std::cout << "something went wrong when trying to get top Index in ProjectileManager" << std::endl;
	}
	return freeIndex;
}

void ProjectileManager::CleanUp()
{
	for (int i = static_cast<int>(myOccupiedIndexes.size()) - 1; i >= 0; i--)
	{
		int index = myOccupiedIndexes[i];
		
		if(myBullets[index].GetShouldRemove())
		{
			ShowExplosionAt(myBullets[index].GetCollider()->GetPosition() + myBullets[index].GetCollider()->GetOffset());
			myFreeIndexes.push(index);
			myOccupiedIndexes.erase(myOccupiedIndexes.begin() + i);
		}
	}

	for (int i = static_cast<int>(myOccupiedExplosionIndexes.size()) - 1; i >= 0; i--)
	{
		int index = myOccupiedExplosionIndexes[i];

		if (myExplosionAnimations[index].IsFinished())
		{
			myFreeExplosionIndexes.push(index);
			myOccupiedExplosionIndexes.erase(myOccupiedExplosionIndexes.begin() + i);
		}
	}
}

void ProjectileManager::NewAll()
{
	for (size_t i = 0; i < myBullets.size(); i++)
	{
		myBullets[i].InitGO({0.f, 0.f}, myCollisionManager, eCollisionLayer::Projectile);
		myBullets[i].Init();
		myBullets[i].Inactivate();
	}

	for (size_t i = 0; i < myExplosionAnimations.size(); i++)
	{
		myExplosionAnimations[i].Init("Sprites/enemies/cannonballExplosion.dds");
		myExplosionAnimations[i].Setup(1, 8, 7);
		myExplosionAnimations[i].SetAnimationSpeed(1.3f);
		myExplosionAnimations[i].SetPivot({ 0.5f, 0.5f });
		myExplosionAnimations[i].SetLayerType(eLayerType::EForeground);
	}
}

void ProjectileManager::ShowExplosionAt(const CommonUtilities::Vector2f & aBulletPosition)
{
	int index = GetFreeIndex(eIndexType::eExplosion);
	myOccupiedExplosionIndexes.push_back(index);

	myExplosionAnimations[index].Reset();
	myExplosionAnimations[index].SetPosition(aBulletPosition);
}

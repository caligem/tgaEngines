#pragma once
#include "Bullet.h"
#include <vector>
#include <array>

class CollisionManager;

class ProjectileManager
{
public:
	ProjectileManager();
	~ProjectileManager();

	void Init(CollisionManager& aCollisionManager);
	void Update(float aDeltaTime);
	void FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer);
	void CreateBullet(const float aProjectileSpeed, const CommonUtilities::Vector2f& aDirection, const CommonUtilities::Vector2f& aPosition, const int aDamage, const char* aPath = "Sprites/enemies/cannonball.dds");
	void Reset();
private:
	enum class eIndexType
	{
		eBullet,
		eExplosion
	};

	std::array<Bullet, 256> myBullets;
	std::array<Animation, 256> myExplosionAnimations;

	std::stack<int> myFreeExplosionIndexes;
	std::vector<int> myOccupiedExplosionIndexes;

	std::stack<int> myFreeIndexes;
	std::vector<int> myOccupiedIndexes;

	void FillFreeIndexes();
	int GetFreeIndex(const eIndexType aIndex);
	void CleanUp();
	void NewAll();
	void ShowExplosionAt(const CommonUtilities::Vector2f& aBulletPosition);

	CollisionManager* myCollisionManager;
};


#include "stdafx.h"
#include "Respawner.h"
#include "Player.h"
#include <iostream>
#define AUTOSAVEINTERVAL 3.f

Respawner::Respawner()
{
}


Respawner::~Respawner()
{
}

void Respawner::Init(Player * aPlayer)
{
	myPlayer = aPlayer;
	mySpawnPoints.reserve(8);
	myNextSpawnPointIndex = 0;
	myTimesSinceAutoSave = 0.f;
}

void Respawner::Update(float aDeltaTime)
{
	myTimesSinceAutoSave += aDeltaTime;

	if (myTimesSinceAutoSave > AUTOSAVEINTERVAL)
	{
		myTimesSinceAutoSave = 0.f;
		if (mySpawnPoints.size() == 8)
		{
			mySpawnPoints[myNextSpawnPointIndex] = myPlayer->GetPosition();
			++myNextSpawnPointIndex;
			if (myNextSpawnPointIndex == 8)
			{
				myNextSpawnPointIndex = 0;
			}
		}
		else
		{
			mySpawnPoints.push_back(myPlayer->GetPosition());
		}
	}
}

void Respawner::RespawnPlayer()
{
	CU::Vector2f spawnPlace;
	spawnPlace = mySpawnPoints[myNextSpawnPointIndex];
	mySpawnPoints.clear();
	myPlayer->SetPosition(spawnPlace);
	mySpawnPoints.push_back(myPlayer->GetPosition());
	myNextSpawnPointIndex = 0;
}

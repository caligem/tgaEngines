#pragma once
#include <vector>
#include "Vector.h"

class Player;
class Respawner
{
public:
	Respawner();
	~Respawner();
	void Init(Player* aPlayer);
	void Update(float aDeltaTime);
	void RespawnPlayer();
private:
	std::vector<CU::Vector2f> mySpawnPoints;
	Player* myPlayer;
	int myNextSpawnPointIndex;
	float myTimesSinceAutoSave;
};


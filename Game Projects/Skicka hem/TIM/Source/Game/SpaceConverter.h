#pragma once
#include "Vector.h"
#define TILE_SIZE 32.f
#define SCREENX 1920.f
#define SCREENY 1080.f
//	PIXEL CONVERTER
namespace SpaceConverter
{
	//	---- PIXEL TO TILE ----
	static CommonUtilities::Vector2f PixelSpaceToTileSpace(const CommonUtilities::Vector2f& aPixelSpace)
	{
		CommonUtilities::Vector2f newTilePos;
		newTilePos.x = aPixelSpace.x / TILE_SIZE;
		newTilePos.y = aPixelSpace.y / TILE_SIZE;
		return newTilePos;
	}

	static float PixelYToTile(float aY)
	{
		float newY = aY / TILE_SIZE;
		return newY;
	}
	static float PixelXToTile(float aX)
	{
		float newX = aX / TILE_SIZE;
		return newX;
	}
	//	---- TILE TO PIXEL ----
	static CommonUtilities::Vector2f TileSpaceToPixelSpace(const CommonUtilities::Vector2f& aPixelSpace)
	{
		CommonUtilities::Vector2f newTilePos;
		newTilePos.x = aPixelSpace.x * TILE_SIZE;
		newTilePos.y = aPixelSpace.y * TILE_SIZE;
		return newTilePos;
	}
	static float TileYToPixel(float aY)
	{
		float newY = aY * TILE_SIZE;
		return newY;
	}
	static float TileXToPixel(float aX)
	{
		float newX = aX * TILE_SIZE;
		return newX;
	}


	//	SCREEN CONVERTER

	//	----  SCREEN TO TILE ----
	static CommonUtilities::Vector2f ScreenSpaceToTileSpace(const CommonUtilities::Vector2f& aScreenSpace)
	{
		CommonUtilities::Vector2f newTilePos;
		newTilePos.x = aScreenSpace.x * (SCREENX / TILE_SIZE);
		newTilePos.y = aScreenSpace.y * (SCREENY / TILE_SIZE);
		return newTilePos;
	}
	static float ScreenYToTile(const float aY)
	{
		float newY = aY * (SCREENY / TILE_SIZE);
		return newY;
	}
	static float ScreenXToTile(const float aX)
	{
		float newX = aX * (SCREENX / TILE_SIZE);
		return newX;
	}

	//	---- TILE TO SCREEN ----
	static CommonUtilities::Vector2f TileSpaceToScreenSpace(const CommonUtilities::Vector2f& aTileSpace)
	{
		CommonUtilities::Vector2f newTilePos;
		newTilePos.x = aTileSpace.x / (SCREENX / TILE_SIZE);
		newTilePos.y = aTileSpace.y / (SCREENY / TILE_SIZE);
		return newTilePos;
	}
	static float TileYToScreen(float aY)
	{
		float newY = aY / (1080.f / TILE_SIZE);
		return newY;
	}
	static float TileXToScreen(float aX)
	{
		float newX = aX / (1920.f / TILE_SIZE);
		return newX;
	}

	static float PixelYToScreen(const float aY)
	{
		float newY = aY / SCREENY;
		return newY;
	}
	static float PixelXToScreen(const float aX)
	{
		float newX = aX / SCREENX;
		return newX;
	}

};
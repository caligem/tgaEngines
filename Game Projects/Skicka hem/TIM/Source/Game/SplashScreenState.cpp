#include "stdafx.h"
#include "SplashScreenState.h"
#include <tga2d\sprite\sprite.h>
#include "Fader.h"
#include <tga2d\engine.h>
#include "InputManager.h"

SplashScreenState::SplashScreenState(CommonUtilities::InputManager* aInputManager, Fader* aFader)
{
	myInputManager = aInputManager;
	myFader = aFader;
	myFadeSkipTime = 1.3f;
	myFadeSpeed = 1.f;
	myTransitionToMenuFadeSpeed = 0.25f;
	myOriginalWatchTime = 4.0f;
	myIsStartFading = false;
}

SplashScreenState::~SplashScreenState()
{
}

void SplashScreenState::Init()
{
	myFinished = false;
	myBG = new Tga2D::CSprite();
	myBG->SetPivot({ 0.5f, 0.5f });
	myBG->SetPosition({ 0.5f, 0.5f });
	myBG->SetColor({ 0.0f, 0.0f, 0.0f, 1.0f });
	myBG->SetSizeRelativeToScreen({ 4.0f, 4.0f });
	myTgaLogo = new Tga2D::CSprite("sprites/SplashScreen/logoTGA.dds");
	myTgaLogo->SetPivot({ 0.5f, 0.5f });
	myTgaLogo->SetPosition({ 0.5f, 0.5f });
	myStudioLogo = new Tga2D::CSprite("sprites/SplashScreen/logoTeam.dds");
	myStudioLogo->SetPivot({ 0.5f, 0.5f });
	myStudioLogo->SetPosition({ 0.5f, 0.5f });
	
	myGameLogo = new Tga2D::CSprite("sprites/SplashScreen/gameName.dds");
	myGameLogo->SetPivot({ 0.5f, 0.5f });
	myGameLogo->SetPosition({ 0.5f, 0.5f });
}

void SplashScreenState::Render()
{
	myBG->Render();
	myActiveLogo->Render();
}
#undef GetCurrentTime
eStateStackMessage SplashScreenState::Update(float aDeltaTime)
{
	while (ShowCursor(false) >= 0);
	mySwapTimer.Update(aDeltaTime);

	if (myIsStartFading)
	{
		float startFadeValue = myActiveLogo->GetColor().myA;
		startFadeValue += aDeltaTime * myFadeSpeed;
		myActiveLogo->SetColor({ 1.f, 1.f, 1.f, startFadeValue});

		if (startFadeValue >= 1.f)
		{
			myIsStartFading = false;
		}
	}
	else
	{
		if (myFader->GetFadingState() == Fader::eState::Inactive)
		{
			if (mySwapTimer.GetCurrentTime() >= myOriginalWatchTime - myFadeSkipTime)
			{
				myFader->Activate(myFadeSkipTime, myFadeSpeed);
			}
		}
	}

	if (myFinished == false)
	{
		return eStateStackMessage::KeepState;
	}
	else
	{
		return eStateStackMessage::PopSubState;
	}
}

void SplashScreenState::OnEnter()
{
	myActiveLogo = myTgaLogo;

	mySwapTimer.Set(myOriginalWatchTime, Countdown::type::autoreset, [&] {Swap(); });
	mySwapTimer.Start();
	
	myIsStartFading = true;
	myActiveLogo->SetColor({ 1.f, 1.f, 1.f, 0.0f});
}

void SplashScreenState::OnExit()
{
	delete myTgaLogo;
	myTgaLogo = nullptr;
	
	delete myStudioLogo;
	myStudioLogo = nullptr;

	delete myGameLogo;
	myGameLogo = nullptr;
	
	delete myBG;
	myBG = nullptr;
}

void SplashScreenState::Swap()
{
	if (myActiveLogo == myTgaLogo)
	{
		mySwapTimer.Set(myOriginalWatchTime, Countdown::type::autoreset, [&] {Swap(); });
		mySwapTimer.Start();
		myActiveLogo = myStudioLogo;
		myFader->SendReadyMessage();
	}
	else if (myActiveLogo == myStudioLogo)
	{
		mySwapTimer.Set(myOriginalWatchTime, Countdown::type::autoreset, [&] {Swap(); });
		mySwapTimer.Start();
		myActiveLogo = myGameLogo;
		myFader->SendReadyMessage();
	}
	else if (myActiveLogo == myGameLogo)
	{
		myFinished = true;
	}
}

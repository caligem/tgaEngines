#pragma once
#include "Vector.h"
#include "GameState.h"
#include "Countdown.h"

namespace Tga2D
{
	class CSprite;
}

class Fader;

class SplashScreenState : public GameState
{
public:
	SplashScreenState(CommonUtilities::InputManager* aInputManager, Fader* aFader);
	~SplashScreenState();

	void Init() override;
	void Render() override;
	eStateStackMessage Update(float aDeltaTime) override;

	void OnEnter() override;
	void OnExit() override;

private:
	bool myFinished;
	void Swap();
	Tga2D::CSprite* myActiveLogo;
	Tga2D::CSprite* myTgaLogo;
	Tga2D::CSprite* myStudioLogo;
	Tga2D::CSprite* myGameLogo;
	Tga2D::CSprite* myBG;
	Countdown mySwapTimer;
	float myTransitionToMenuFadeSpeed;
	float myFadeSkipTime;
	float myFadeSpeed;
	float myOriginalWatchTime;

	CommonUtilities::InputManager* myInputManager;
	Fader* myFader;

	bool myIsStartFading;
};


#include "stdafx.h"
#include "StateStack.h"
#include "GameState.h"

StateStack::StateStack()
{
	myGameStates.reserve(16);
	myIntro = nullptr;
	myOutro = nullptr;
}


StateStack::~StateStack()
{
	if (myOutro != nullptr)
	{
		delete myOutro;
		myOutro = nullptr;
	}
	if (myIntro != nullptr)
	{
		delete myIntro;
		myIntro = nullptr;
	}
}

void StateStack::PushSubState(GameState* aState)
{
	myGameStates.back().push_back(aState);
	aState->AttachStateStack(this);
	aState->Init();
	aState->OnEnter();
}

void StateStack::PushMainState(GameState * aState)
{
	myGameStates.push_back(std::vector<GameState*>());
	myGameStates.back().reserve(8);
	myGameStates.back().push_back(aState);
	aState->AttachStateStack(this);
	aState->Init();
	aState->OnEnter();

}

void StateStack::PopSubState()
{
	myGameStates.back().back()->OnExit();
	GameState* stateToBeDeleted = myGameStates.back().back();
	myGameStates.back().pop_back();

	if (stateToBeDeleted != myIntro && stateToBeDeleted != myOutro && stateToBeDeleted->ShouldDelete())
	{
		delete stateToBeDeleted;
		stateToBeDeleted = nullptr;
	}

	myGameStates.back().back()->OnEnter();
}

void StateStack::PopMainState()
{
	size_t lastStateSize = myGameStates.back().size();

	for (size_t index = 0; index < lastStateSize; ++index)
	{
		myGameStates.back().back()->OnExit();
		GameState* stateToBeDeleted = myGameStates.back().back();
		myGameStates.back().pop_back();

		if (stateToBeDeleted != myIntro && stateToBeDeleted != myOutro)
		{
			delete stateToBeDeleted;
			stateToBeDeleted = nullptr;
		}
	}
	myGameStates.pop_back();
	if (myGameStates.size() > 0)
	{
		myGameStates.back().back()->OnEnter();
	}
}

void StateStack::Render()
{
	RenderGameStateAtIndex(static_cast<int>(myGameStates.back().size() - 1));
}

void StateStack::Init(CommonUtilities::InputManager* aInputManager, Fader* aFader, DataBase* aDatabase)
{
	myIntro = new CutsceneState(CutsceneState::eCutsceneType::Intro, aInputManager, aFader, aDatabase);
	myIntro->Init();
	myOutro = new CutsceneState(CutsceneState::eCutsceneType::Outro, aInputManager, aFader, aDatabase);
	myOutro->Init();
}

void StateStack::PushIntroCutscene(InGameState::eLoadingState &aLoadingState)
{
	myIntro->AttachLoadingState(aLoadingState);
	myGameStates.back().push_back(myIntro);
	myIntro->AttachStateStack(this);
	myIntro->OnEnter();
}

void StateStack::PushOutroCutscene()
{
	myGameStates.back().push_back(myOutro);
	myOutro->AttachStateStack(this);
	myOutro->OnEnter();
}

void StateStack::TerminateThreads()
{
	size_t stateSize = myGameStates.size();

	for (size_t index = 0; index < stateSize; ++index)
	{
		for (size_t secondIndex = 0; secondIndex < myGameStates[index].size(); secondIndex++)
		{
			myGameStates[index][secondIndex]->TerminateThread();
		}
	}
}

void StateStack::RenderGameStateAtIndex(int aIndex)
{
	if (aIndex < 0)
	{
		return;
	}

	if (myGameStates.back()[static_cast<unsigned short>(aIndex)]->LetThroughRender())
	{
		RenderGameStateAtIndex(aIndex - 1);
	}

	myGameStates.back()[static_cast<unsigned short>(aIndex)]->Render();
}

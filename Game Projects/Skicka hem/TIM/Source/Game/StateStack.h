#pragma once
#include "GameState.h"
#include <vector>

//ADD INCLUDES FOR NEW STATES ONLY HERE
#include "MainMenuState.h"
#include "InGameState.h"
#include "OptionState.h"
#include "PauseMenuState.h"
#include "CreditsState.h"
#include "SplashScreenState.h"
#include "CutsceneState.h"
#include "PopUpTextBoxState.h"

class GameState;

class StateStack
{
public:
	StateStack();
	~StateStack();

	struct OptionsData
	{
		int myVolume = 50;
		bool myIsMuted = false;
		bool myIsFullscreen = false;

	}myOptionsData;

	void PushSubState(GameState* aState);
	void PushMainState(GameState* aState);

	void PopSubState();
	void PopMainState();
	
	void Render();

	inline const int GetLastIndex() const { return static_cast<int>(myGameStates.size() - 1); }
	inline GameState* GetCurrentState() { return myGameStates.back().back(); };
	inline const int GetSize() const { return static_cast<int>(myGameStates.size()); }

	void Init(CommonUtilities::InputManager* aInputManager, Fader* aFader, DataBase* aDatabase);

	void PushIntroCutscene(InGameState::eLoadingState &aLoadingState);
	void PushOutroCutscene();

	void TerminateThreads();
private:

	void RenderGameStateAtIndex(int aIndex);
	GameState* myCurrentState;
	std::vector<std::vector<GameState*>> myGameStates;
	CutsceneState* myIntro;
	CutsceneState* myOutro;
};


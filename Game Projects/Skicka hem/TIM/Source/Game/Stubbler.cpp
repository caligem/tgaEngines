#include "stdafx.h"
#include "Stubbler.h"
#include "CollisionObject.h"
#include "BoxCollider.h"
#include "CircleCollider.h"
#include "utility.h"
#include "Player.h"

//temp
#include "Camera.h"

#define MAXTILEWALKRANGE 3

Stubbler::Stubbler()
	: myLastKnownPlayerLocation({ 0.f, 0.f })
	, myKnowLocation(false)
{
	myDirection = { 0.f, 0.f };
	myIsDead = false;
}

Stubbler::~Stubbler()
{
}

void Stubbler::Reset()
{
	GameObject::Reset();
	myKnowLocation = false;
	myOriginPosition = myPosition;
}

void Stubbler::Init(const nlohmann::json& aData)
{
	myIsDead = false;
	// Save variables from json
	if (aData != nullptr)
	{
		ParseJSONFile(aData);
		myAnimations[static_cast<int>(eState::eIdle)].Setup(2, 16, 10);
		myAnimations[static_cast<int>(eState::eMoving)].Setup(2, 16, 9);
		myAnimations[static_cast<int>(eState::eDying)].Setup(2, 16, 10);

		myAnimations[static_cast<int>(eState::eIdle)].SetLoop(true);
		myAnimations[static_cast<int>(eState::eMoving)].SetLoop(true);
		myAnimations[static_cast<int>(eState::eDying)].SetLoop(false);

		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eIdle)];
	}
	else
	{
		std::cout << "Didnt find path to parse Stubbler" << std::endl;
	}
	myWalkSoundTimer.Set(0.5f, Countdown::type::autoreset, [&] {AudioManager::GetInstance()->PlayRandomSound("beardWalk", 4); });
	myHitFlashTimer.Set(0.2f, Countdown::type::oneshot, [&] {EndHitFlash(); });
	float randomTimer = (static_cast<float>(rand() % RAND_MAX) / RAND_MAX) + 1.f;
	myRandomDirectionTimer.Set(randomTimer, Countdown::type::autoreset, [&] { RandomDirection(); });
	myRandomDirectionTimer.Start();
	myOriginPosition = myPosition;
}

void Stubbler::OnUpdate(float aDeltatime)
{
	SetCurrentAnimation();
	myHitFlashTimer.Update(aDeltatime);
	myRandomDirectionTimer.Update(aDeltatime);

	if (myIsDead)
	{
		if (!myCurrentAnimation->IsFinished())
		{
			myCurrentAnimation->Update(aDeltatime);
		}
		return;
	}

	if (DidIArrivaAtPlayerLocation())
	{
		myDirection = { 0.f, 0.f };
	}
	else
	{
		Move(aDeltatime);

		int orientation = GetOrientation();
		
		myCurrentAnimation->SetRowToAnimate(orientation);
		myCurrentAnimation->Update(aDeltatime);
	}
}

void Stubbler::ParseJSONFile(const nlohmann::json& aJSONData)
{
	// If / when we want debugging info, add else on the following if-statements

	if (aJSONData.find("spriteIdle") != aJSONData.end())
	{
		myAnimations[static_cast<int>(eState::eIdle)] = Animation(aJSONData["spriteIdle"].get<std::string>().c_str());
		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eIdle)];
	}
	if (aJSONData.find("spriteMoving") != aJSONData.end())
	{
		myAnimations[static_cast<int>(eState::eMoving)] = Animation(aJSONData["spriteMoving"].get<std::string>().c_str());
		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eMoving)];
	}
	if (aJSONData.find("spriteDying") != aJSONData.end())
	{
		myAnimations[static_cast<int>(eState::eDying)] = Animation(aJSONData["spriteDying"].get<std::string>().c_str());
		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eDying)];
	}
	if (aJSONData.find("unitID") != aJSONData.end())
	{
		myUnitID = aJSONData["unitID"].get<int>();
	}
	if (aJSONData.find("maxHealth") != aJSONData.end())
	{
		myMaxHealth = aJSONData["maxHealth"].get<int>();
		myCurrentHealth = myMaxHealth;
	}
	if (aJSONData.find("movementSpeed") != aJSONData.end())
	{
		myMovementSpeed = aJSONData["movementSpeed"].get<float>();
	}
	if (aJSONData.find("sightRange") != aJSONData.end())
	{
		mySightRange = aJSONData["sightRange"].get<float>();
	}
	if (aJSONData.find("damage") != aJSONData.end())
	{
		myDamage = aJSONData["damage"].get<int>();
	}
	if (aJSONData.find("colliderRadius") != aJSONData.end())
	{
		myColliderRadius = aJSONData["colliderRadius"].get<float>();
	}

	AddCircleCollider(myColliderRadius, { 0.f, -0.5f });

	if (myCollisionManager == nullptr)
	{
		std::cout << "you forgot to call InitGO with CollisionManager" << std::endl;
	}

	myVisionColliderIndex = myCollisionManager->AddCircleCollider(eCollisionLayer::Vision, mySightRange, { 0.f, 0.f });
	myCollisionManager->AttachGameObject(eCollisionLayer::Vision, myVisionColliderIndex, this);

	myTag = eGameObjectTag::Stubbler;
}

int Stubbler::GetOrientation()
{
	int orientation = LEFT;
	if (myDirection.x < 0)
	{
		orientation = LEFT;
	}
	else
	{
		orientation = RIGHT;
	}
	return orientation;
}

void Stubbler::SetCurrentAnimation()
{
	if (myIsDead)
	{
		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eDying)];
		myCurrentAnimation->SetRowToAnimate(GetOrientation());
	}
	else if (myKnowLocation)
	{
		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eMoving)];
	}
	else
	{
		myCurrentAnimation = &myAnimations[static_cast<int>(eState::eIdle)];
	}
}

void Stubbler::StartHitFlash()
{
	myHitFlashTimer.Reset();
	myHitFlashTimer.Start();
	for (int i = 0; i < myAnimations.size(); ++i)
	{
		myAnimations[i].SetColor({ 1.0f, 0.3f, 0.3f, 0.9f });
	}
}

void Stubbler::EndHitFlash()
{
	for (int i = 0; i < myAnimations.size(); ++i)
	{
		myAnimations[i].SetColor({ 1.0f, 1.0f, 1.0f, 1.0f });
	}
}

void Stubbler::RandomDirection()
{
	const float xValue = static_cast<float>((rand() % (MAXTILEWALKRANGE*2 + 1)) - MAXTILEWALKRANGE);
	const float yValue = static_cast<float>((rand() % (MAXTILEWALKRANGE*2 + 1)) - MAXTILEWALKRANGE);
	myIdleWalkTargetLocation.x = myOriginPosition.x + xValue;
	myIdleWalkTargetLocation.y = myOriginPosition.y + yValue;
}

void Stubbler::FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer)
{
	myCurrentAnimation->SetPosition({ myPosition.x, myPosition.y });
	aRenderBuffer.push_back(myCurrentAnimation);
}

void Stubbler::OnCollisionEnter(CollisionObject & aOther)
{
	if (myIsDead) return;

	if (aOther.GetLayer() == eCollisionLayer::Player)
	{
		Player* player = static_cast<Player*>(aOther.GetOwner());
		if (player->GetPlayerState() == Player::ePlayerState::eDying || player->GetPlayerState() == Player::ePlayerState::eDead)
		{
			myKnowLocation = false;
			return;
		}
		if (RayTracePlayer(aOther, eRayTraceType::Stubbler))
		{
			const CommonUtilities::Vector2f playerColliderPosition = aOther.GetPosition() + aOther.GetOffset();
			myKnowLocation = true;
			myLastKnownPlayerLocation = playerColliderPosition;
			myOriginPosition = myPosition;
		}
	}
	else if (aOther.GetLayer() == eCollisionLayer::Enemies)
	{
		const CommonUtilities::Vector2f position = GetCollider()->GetPosition() + GetCollider()->GetOffset();
		const CommonUtilities::Vector2f collisionVector = position - (aOther.GetPosition() + aOther.GetOffset());
		const float colliderRadius = static_cast<CircleCollider*>(GetCollider())->GetRadius();
		const float enemyColliderRadius = static_cast<CircleCollider*>(&aOther)->GetRadius();
		const float collisionRange = (colliderRadius + enemyColliderRadius) - collisionVector.Length();
		const CommonUtilities::Vector2f collisionDirection = collisionVector.GetNormalized();

		if (aOther.GetOwner()->GetTag() == eGameObjectTag::CannonBeard)
		{
			myPosition += (collisionDirection * collisionRange);
		}
		else
		{
			myPosition += (collisionDirection * (collisionRange / 2.f));
		}
	}
	else if (aOther.GetLayer() == eCollisionLayer::Terrain || aOther.GetLayer() == eCollisionLayer::FallPit)
	{
		PushBackEnemy(aOther);
	}
}

void Stubbler::OnCollisionStay(CollisionObject & aOther)
{
	if (myIsDead) return;

	if (aOther.GetLayer() == eCollisionLayer::Player)
	{
		Player* player = static_cast<Player*>(aOther.GetOwner());
		if (player->GetPlayerState() == Player::ePlayerState::eDying || player->GetPlayerState() == Player::ePlayerState::eDead)
		{
			myKnowLocation = false;
			return;
		}
		if (RayTracePlayer(aOther, eRayTraceType::Stubbler))
		{
			const CommonUtilities::Vector2f playerColliderPosition = aOther.GetPosition() + aOther.GetOffset();
			myKnowLocation = true;
			myLastKnownPlayerLocation = playerColliderPosition;
			myOriginPosition = myPosition;
		}
	}
	else if (aOther.GetLayer() == eCollisionLayer::Enemies)
	{
		const CommonUtilities::Vector2f position = GetCollider()->GetPosition() + GetCollider()->GetOffset();
		const CommonUtilities::Vector2f collisionVector = position - (aOther.GetPosition() + aOther.GetOffset());
		const float colliderRadius = static_cast<CircleCollider*>(GetCollider())->GetRadius();
		const float enemyColliderRadius = static_cast<CircleCollider*>(&aOther)->GetRadius();
		const float collisionRange = (colliderRadius + enemyColliderRadius) - collisionVector.Length();
		const CommonUtilities::Vector2f collisionDirection = collisionVector.GetNormalized();

		if (aOther.GetOwner()->GetTag() == eGameObjectTag::CannonBeard)
		{
			myPosition += (collisionDirection * collisionRange);
		}
		else
		{
			myPosition += (collisionDirection * (collisionRange / 2.f));
		}
	}
	else if (aOther.GetLayer() == eCollisionLayer::Terrain || aOther.GetLayer() == eCollisionLayer::FallPit)
	{
		PushBackEnemy(aOther);
	}
}

void Stubbler::OnCollisionLeave(CollisionObject & aOther)
{
	aOther;
}

void Stubbler::Inactivate()
{
	if (myColliderIndex != -1)
	{
		GetCollider()->SetIsSolid(false);

	}

	if (myVisionColliderIndex != -1)
	{
		myCollisionManager->GetCollisionObject(eCollisionLayer::Vision, myVisionColliderIndex)->SetIsSolid(false);
	}

	//myShouldRemove = true;
}

void Stubbler::Reactivate()
{
	if (myColliderIndex != -1)
	{
		GetCollider()->SetIsSolid(true);
	}
	if (myVisionColliderIndex != -1)
	{
		myCollisionManager->GetCollisionObject(eCollisionLayer::Vision, myVisionColliderIndex)->SetIsSolid(false);
	}

	//myShouldRemove = false;
}

bool Stubbler::DidIArrivaAtPlayerLocation()
{
	//if (myIsDead) return false;

	if (myKnowLocation)
	{
		const float range = ((GetCollider()->GetPosition() + GetCollider()->GetOffset()) - myLastKnownPlayerLocation).Length2();

		if (range <= 0.0001f)
		{
			myOriginPosition = myPosition;
			myKnowLocation = false;
			return true;
		}
	}

	return false;
}

void Stubbler::Move(float aDeltaTime)
{
	if (myIsDead) return;

	if (myKnowLocation)
	{
		myWalkSoundTimer.Start();
		myWalkSoundTimer.Update(aDeltaTime);
		myDirection = (myLastKnownPlayerLocation - (GetCollider()->GetPosition() + GetCollider()->GetOffset())).GetNormalized();
		myPosition = myPosition + (myMovementSpeed * myDirection * aDeltaTime);
	}
	else
	{
		const CommonUtilities::Vector2f direction = (myIdleWalkTargetLocation - myPosition).GetNormalized();
		myPosition += direction * aDeltaTime;
	}
}

void Stubbler::PushBackEnemy(CollisionObject & aOther)
{
	if (myIsDead) return;

	CommonUtilities::Vector2f closestBoxPoint = GetCollider()->GetPosition() + GetCollider()->GetOffset();
	BoxCollider* terrainCollider = dynamic_cast<BoxCollider*>(&aOther);
	if (terrainCollider != nullptr)
	{
		closestBoxPoint.x = cit::Clamp(closestBoxPoint.x, terrainCollider->GetPosition().x + terrainCollider->GetOffset().x - terrainCollider->GetWidth() / 2.f, terrainCollider->GetPosition().x + terrainCollider->GetOffset().x + terrainCollider->GetWidth() / 2.f);
		closestBoxPoint.y = cit::Clamp(closestBoxPoint.y, terrainCollider->GetPosition().y + terrainCollider->GetOffset().y - terrainCollider->GetHeight() / 2.f, terrainCollider->GetPosition().y + terrainCollider->GetOffset().y + terrainCollider->GetHeight() / 2.f);
		const CommonUtilities::Vector2f collisionDirection = (GetCollider()->GetPosition() + GetCollider()->GetOffset()) - closestBoxPoint;
	
		const float maxPushRange = (collisionDirection.GetNormalized() * (static_cast<CircleCollider*>(myCollisionManager->GetCollisionObject(myCollisionLayer, myColliderIndex))->GetRadius())).Length();
		const float pushRange = maxPushRange - collisionDirection.Length();

		myPosition += collisionDirection * pushRange;

	}
}
#pragma once
#include "GameObject.h"
#include "json.h"
#include "Animation.h"
#include "Enemy.h"
//temp
class Camera;

class Stubbler : public Enemy
{
public:
	Stubbler();
	~Stubbler();

	void Reset() override;

	void Init(const nlohmann::json& aData) override;
	void OnUpdate(float aDeltatime) override;
	void FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer) override;

	enum class eState
	{
		eIdle,
		eMoving,
		eDying,
		count
	};

	virtual void OnCollisionEnter(CollisionObject &aOther) override;
	virtual void OnCollisionStay(CollisionObject &aOther) override;
	virtual void OnCollisionLeave(CollisionObject &aOther) override;

	void Inactivate() override;
	void Reactivate() override;

private:
	eState myState;

	std::array<Animation, static_cast<int>(eState::count)> myAnimations;
	Animation* myCurrentAnimation;

	bool myKnowLocation;
	float myMovementSpeed;

	CommonUtilities::Vector2f myLastKnownPlayerLocation;

	bool DidIArrivaAtPlayerLocation();
	void Move(float aDeltaTime);
	void PushBackEnemy(CollisionObject& aOther);
	void ParseJSONFile(const nlohmann::json& aJSONData) override;

	int GetOrientation();

	void SetCurrentAnimation();
	void StartHitFlash() override;

	void EndHitFlash();

	const int LEFT = 0;
	const int UP = LEFT;
	const int RIGHT = 1;
	const int DOWN = RIGHT;

	Countdown myWalkSoundTimer;
	Countdown myHitFlashTimer;
	Countdown myRandomDirectionTimer;

	void RandomDirection();
	CommonUtilities::Vector2f myOriginPosition;
	CommonUtilities::Vector2f myIdleWalkTargetLocation;
};

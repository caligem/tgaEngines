#pragma once
#include "Vector.h"

enum class eMessageType
{
	PopUpTextBox,
	PlayerTeleport,
	PlayerInteract,
	DoorLockUnlock,
	FoundWand,
	ShowInteractButton,
	HideInteractButton,
	KeyUsed,
	Outro,
	ResetEnemies,
	COUNT
};

enum class ePopUpTextBox
{
	FirstWand,
	FirstKey,
	FirstEnemy,
	SeesBook,
};

struct Message
{
	Message(eMessageType aMessageType) : myMessageType(aMessageType) {}
	eMessageType myMessageType;
	union messageData
	{
		messageData() { memset(this, 0, sizeof(messageData)); } // Needed a default constructor because of CU::Vector2f - internet told me to do this. 

		size_t mySize_t;
		CU::Vector2f myVec2f;
		int myInt;
		ePopUpTextBox myPopUpTextBox;
	} data;
};

class Subscriber
{
public:
	Subscriber();
	~Subscriber();

	virtual void ReceiveMessage(const Message aMessage);
};


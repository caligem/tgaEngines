#include "stdafx.h"
#include "Tile.h"


Tile::Tile()
	:myPosition(CU::Vector2<float>()), myTextureRect(CU::Vector4<float>()), myTilesetIndex(0)
{
}

Tile::Tile(const CU::Vector2f aPos, const CU::Vector4f aTextRect, const unsigned short aTilesetIndex)
	:myPosition(aPos), myTextureRect(aTextRect), myTilesetIndex(aTilesetIndex)
{
}

Tile::~Tile()
{
}
const CU::Vector2f Tile::GetPosition() const
{
	return myPosition;
}

const CU::Vector4f Tile::GetTextureRect() const
{
	return myTextureRect;
}

const unsigned short Tile::GetTilesetIndex() const
{
	return myTilesetIndex;
}

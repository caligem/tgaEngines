#pragma once
#include "Vector.h"

class Sprite;

class Tile
{
public:
	Tile();
	Tile(const CU::Vector2f aWorldPos, const CU::Vector4f aTextRect, const unsigned short aTilesetIndex);
	~Tile();
 	const CU::Vector2f GetPosition() const;
	const CU::Vector4f GetTextureRect() const;
	const unsigned short GetTilesetIndex() const;

private:
	CU::Vector2f myPosition;
	CU::Vector4f myTextureRect;
	const unsigned short myTilesetIndex;
};

#include "stdafx.h"
#include "TiledMap.h"
#include <string>
#include "Sprite.h"
#include "CollisionManager.h"
#include "tga2d/sprite/sprite_batch.h"
#include "GameObject.h"
#include "Door.h"
#include "Lever.h"
#include "Chest.h"
#include "Activator.h"
#include "BookOfClassChange.h"
#include "EnemyFactory.h"
#include "ParticleSys.h"
#include "Trigger.h"

TiledMap::TiledMap()
{

}

TiledMap::~TiledMap()
{
	for (size_t i = myTileMapBatches.size() - 1; i < 0; --i)
	{
		if (myTileMapBatches[i] != nullptr)
		{
			delete myTileMapBatches[i];
			myTileMapBatches[i] = nullptr;
		}
		if (i == 0)
		{
			break;
		}
	}
}

void TiledMap::Init(const json& aTilesets, EnemyFactory* aEnemyFactory)
{
	myEnemyFactory = aEnemyFactory;

	myTilesets.reserve(32);
	myTileLayers.reserve(8);
	for (auto& tileset : aTilesets)
	{
		if (tileset.find("image") != tileset.end())
		{
			Tileset tilesetData; 

			std::string spritePath("");
			spritePath = tileset["image"].get<std::string>();
			std::string dubbleDot = "..";
			size_t pathAboveBin = spritePath.rfind(dubbleDot);

			if (pathAboveBin != std::string::npos)
			{
				spritePath = std::string(spritePath.begin() + pathAboveBin + dubbleDot.length() + 1, spritePath.end());
			}

			tilesetData.mySpriteSheetPath = spritePath;
			tilesetData.mySpriteSheet = Sprite(spritePath.c_str());

			tilesetData.myTileGidStartIndex = tileset["firstgid"];
			tilesetData.myTileGidCount = tileset["tilecount"];
			tilesetData.myTileSize.x = tileset["tilewidth"];
			tilesetData.myTileSize.y = tileset["tileheight"];

			if (tileset.find("imagewidth") == tileset.end() && tileset.find("imageheight") == tileset.end())
			{
				tilesetData.myImageSize.x = tileset["tilewidth"];
				tilesetData.myImageSize.x = tileset["tileheight"];
			}
			else
			{
				tilesetData.myImageSize.x = tileset["imagewidth"];
				tilesetData.myImageSize.y = tileset["imageheight"];
			}

			tilesetData.myTileColumns = tileset["columns"];

			if (tilesetData.myTileColumns == 0) // If a tileset only has one column its number of rows equals its number of tiles
			{
				tilesetData.myTileColumns = 1;
			}

			tilesetData.myTileRows = tilesetData.myTileGidCount / tilesetData.myTileColumns;
			myTilesets.push_back(std::move(tilesetData));
		}
		else
		{
			unsigned int gidStartIndex = tileset["firstgid"];

			for (auto image : tileset["tiles"])
			{
				Tileset tilesetData;

				tilesetData.myTileGidStartIndex = gidStartIndex;
				++gidStartIndex;
				tilesetData.myTileGidCount = 1;
				tilesetData.myTileSize = CU::Vector2f(-1.f, -1.f);
				tilesetData.myImageSize = CU::Vector2f(-1.f, -1.f);
				tilesetData.myTileRows = 1;
				tilesetData.myTileColumns = 1;

				std::string spritePath("");
				spritePath = image["image"].get<std::string>();
				std::string dubbleDot = "..";
				size_t pathAboveBin = spritePath.rfind(dubbleDot);

				if (pathAboveBin != std::string::npos)
				{
					spritePath = std::string(spritePath.begin() + pathAboveBin + dubbleDot.length() + 1, spritePath.end());
				}

				tilesetData.mySpriteSheetPath = spritePath;
				tilesetData.mySpriteSheet = Sprite(spritePath.c_str());

				if (image.find("objectgroup") != image.end())
				{
					tilesetData.myBoxCollider.myPosX = image["objectgroup"]["objects"][0]["x"].get<float>();
					tilesetData.myBoxCollider.myPosY = image["objectgroup"]["objects"][0]["y"].get<float>();
					tilesetData.myBoxCollider.myHeight = image["objectgroup"]["objects"][0]["height"].get<float>();
					tilesetData.myBoxCollider.myWidth = image["objectgroup"]["objects"][0]["width"].get<float>();
				}

				myTilesets.push_back(std::move(tilesetData));

			}
		}
	}

}

void TiledMap::LoadTilesAndObjects(const json& aLayers, CollisionManager* aCollisionManager, std::vector<Door*>& aDoors, std::map<int, CommonUtilities::Vector2f>& aConnections, std::vector<GameObject*>& aGameObjects, std::vector<InteractableObject>& aZoneObjects, const ParticleSys& aParticleSys)
{
	aParticleSys.GetEmitter(0);
	assert(aCollisionManager != nullptr && "collisionManager is nullptr when trying to load tiles and object.");
	
	unsigned int zValue = 0;
	for (auto& layer : aLayers)
	{
		if (layer["type"].get<std::string>() == "tilelayer")
		{

			unsigned int layerWidth = layer["width"].get<int>();
			unsigned int layerHeight = layer["height"].get<int>();
			unsigned int length = layerWidth * layerHeight;

			std::vector<Tile> layerUnderConstruction;
			layerUnderConstruction.reserve(length);

			CU::Vector3f layerScale(1.0f, 1.0f, 1.0f);

			for (unsigned int tileIndex = 0; tileIndex < length; ++tileIndex)
			{
				unsigned int tileIndexData = layer["data"][tileIndex];

				unsigned int maxTileIndex = (myTilesets[myTilesets.size() - 1].myTileGidStartIndex + myTilesets[myTilesets.size() - 1].myTileGidCount);
				if (tileIndexData == 0 || tileIndexData > maxTileIndex) continue;

				unsigned short tilesetIndex = 0;
				int tilesetIndexAsInt = GetTileSetIndex(tileIndexData);
				if (tilesetIndexAsInt != -1)
				{
					tilesetIndex = static_cast<unsigned short>(tilesetIndexAsInt);
				}

				unsigned int x = (tileIndex % layerWidth);
				unsigned int y = (tileIndex / layerWidth);

				unsigned int adjustedIndex = tileIndexData - myTilesets[tilesetIndex].myTileGidStartIndex;
				unsigned int tilesetcolumn = myTilesets[tilesetIndex].myTileColumns;
				unsigned int tilesetrows = myTilesets[tilesetIndex].myTileRows;
				unsigned int uvIndexX = adjustedIndex % tilesetcolumn;
				unsigned int uvIndexY = adjustedIndex / tilesetcolumn;
				float uvY = (static_cast<float>(uvIndexY) / static_cast<float>(tilesetrows));
				float uvX = (static_cast<float>(uvIndexX) / static_cast<float>(tilesetcolumn));

				CU::Vector2f size(myTilesets[tilesetIndex].myTileSize.x, myTilesets[tilesetIndex].myTileSize.y);
				CU::Vector2f uvSize(
					size.x / myTilesets[tilesetIndex].myImageSize.x,
					size.y / myTilesets[tilesetIndex].myImageSize.y);
				CU::Vector2f worldPos(static_cast<float>(x), static_cast<float>(y));
				CU::Vector4f rect(uvX, uvY, uvX + uvSize.x, uvY + uvSize.y);

				Tile tile(worldPos, rect, tilesetIndex);

				layerUnderConstruction.push_back(std::move(tile));
			}
			++zValue; // This isn't used anymore, but could maybe be used in the future for getting layer depth.

			if (layer.find("properties") != layer.end() && layer["properties"].find("overPlayer") != layer["properties"].end())
			{
				myTileLayersOverPlayer.push_back(std::move(layerUnderConstruction));
			}
			else
			{
				myTileLayers.push_back(std::move(layerUnderConstruction));
			}
		}
		else if (layer["type"].get<std::string>() == "objectgroup")
		{
			std::string layerName = layer["name"].get<std::string>();

			std::string layerNamePrefix = std::string(layerName.begin(), layerName.begin() + 3);
			if (std::string(layerName.begin(), layerName.begin() + 3) == "GFX")
			{
				for (auto gfx : layer["objects"])
				{
					if (gfx.find("gid") == gfx.end())
					{
						std::cout << "Something that's not an image is on tiled layer " + layerName << std::endl;
						continue;
					}

					unsigned int tileIndexData = gfx["gid"];
					unsigned short tilesetIndex = 0;
					int tilesetIndexAsInt = GetTileSetIndex(tileIndexData);
					if (tilesetIndexAsInt != -1)
					{
						tilesetIndex = static_cast<unsigned short>(tilesetIndexAsInt);
					}

					Sprite gfxObject;
					CU::Vector2f position(SpaceConverter::PixelSpaceToTileSpace(CU::Vector2f(gfx["x"].get<float>(), gfx["y"].get<float>())));

					if (std::string(layerName.begin() + 4, layerName.begin() + 9) == "below")
					{
						gfxObject.Init(myTilesets[tilesetIndex].mySpriteSheetPath.c_str(), position, eLayerType::EBackGround);
					}
					else if (std::string(layerName.begin() + 4, layerName.begin() + 9) == "above")
					{
						gfxObject.Init(myTilesets[tilesetIndex].mySpriteSheetPath.c_str(), position, eLayerType::EForeground);
					}
					else
					{
						gfxObject.Init(myTilesets[tilesetIndex].mySpriteSheetPath.c_str(), position, eLayerType::EGameObjectLayer);
					}

					gfxObject.SetPivot({0.f, 1.f});
					myGFXObjects.push_back(std::move(gfxObject));
				}
			}
			else if (layerName == "Walls" || layerName == "Pits" || layerName == "Checkpoints")
			{
				for (auto box : layer["objects"])
				{
					float width = static_cast<float>(box["width"]);
					float height = static_cast<float>(box["height"]);
					CU::Vector2f worldPosition({ static_cast<float>(box["x"]), static_cast<float>(box["y"]) });

					if (layerName == "Walls")
					{
						aCollisionManager->AddBoxCollider(eCollisionLayer::Terrain, SpaceConverter::PixelYToTile(height), SpaceConverter::PixelXToTile(width), { 0.f, 0.f }, SpaceConverter::PixelSpaceToTileSpace(worldPosition + CU::Vector2f(width, height) / 2.f));
					}
					else if (layerName == "Pits")
					{
						aCollisionManager->AddBoxCollider(eCollisionLayer::FallPit, SpaceConverter::PixelYToTile(height), SpaceConverter::PixelXToTile(width), { 0.f, 0.f }, SpaceConverter::PixelSpaceToTileSpace(worldPosition + CU::Vector2f(width, height) / 2.f));
					}
					else
					{
						aCollisionManager->AddBoxCollider(eCollisionLayer::Checkpoint, SpaceConverter::PixelYToTile(height), SpaceConverter::PixelXToTile(width), { 0.f, 0.f }, SpaceConverter::PixelSpaceToTileSpace(worldPosition + CU::Vector2f(width, height) / 2.f));
					}
				}
			}
			else if (layerName == "StaticObjects")
			{
				for (auto object : layer["objects"])
				{
					if (object["name"] == "startPosition")
					{
						const float posX = object["x"].get<float>() + 16.f;
						const float posY = object["y"].get<float>() - 16.f;
						const CommonUtilities::Vector2f mySpawnPosition = { SpaceConverter::PixelXToTile(posX), SpaceConverter::PixelYToTile(posY) };
						const int connectID = object["properties"]["connectID"].get<int>();
						aConnections.insert(std::make_pair(connectID, mySpawnPosition));
					}
					else if (object["name"] == "door")
					{
						const float objectHeight = object["height"].get<float>();
						const float objectWidth = object["width"].get<float>();
						const float objectPosX = object["x"].get<float>() + objectWidth / 2.f;
						const float objectPosY = object["y"].get<float>() - objectHeight / 2.f;
						const CommonUtilities::Vector2f objectPosition = { SpaceConverter::PixelXToTile(objectPosX), SpaceConverter::PixelYToTile(objectPosY) };

						const int connectID = object["properties"]["connectID"].get<int>();
						const int zoneToGoIndex = object["properties"]["zoneToGoIndex"].get<int>();
						Door* newDoor = new Door();
						newDoor->Init(aCollisionManager, objectPosition, connectID, zoneToGoIndex);

						unsigned int tileIndexData = object["gid"];
						unsigned short tilesetIndex = 0;
						int tilesetIndexAsInt = GetTileSetIndex(tileIndexData);
						if (tilesetIndexAsInt != -1)
						{
							tilesetIndex = static_cast<unsigned short>(tilesetIndexAsInt);
						}

						newDoor->AddSprite(myTilesets[tilesetIndex].mySpriteSheetPath.c_str());
						Sprite* doorSprite = dynamic_cast<Sprite*>(&newDoor->AccessDrawable());
						doorSprite->SetPivot({ 0.5f, 0.5f });

						std::string spritePath = myTilesets[tilesetIndex].mySpriteSheetPath;
						std::string fileType = ".dds";
						size_t letterBeforeFiletype = spritePath.rfind(fileType);

						if (zoneToGoIndex == -1)
						{
							std::string open = "_Open";
							spritePath = std::string(spritePath.begin(), spritePath.begin() + letterBeforeFiletype - open.length()) + "_locked.dds";
						}
						else
						{
							spritePath = std::string(spritePath.begin(), spritePath.begin() + letterBeforeFiletype) + "_locked.dds";
						}
						newDoor->SetLockedSprite(spritePath.c_str());

						const float height = myTilesets[tilesetIndex].myBoxCollider.myHeight;
						const float width = myTilesets[tilesetIndex].myBoxCollider.myWidth;
						const float posX = myTilesets[tilesetIndex].myBoxCollider.myPosX;
						const float posY = myTilesets[tilesetIndex].myBoxCollider.myPosY;
						const CommonUtilities::Vector2f topLeft = { objectPosX - objectWidth / 2.f, objectPosY - objectHeight / 2.f };
						CommonUtilities::Vector2f boxPos = { topLeft.x + posX, topLeft.y + posY };
						boxPos.x = boxPos.x + width / 2.f;
						boxPos.y = boxPos.y + height / 2.f;
						newDoor->AddBoxCollider(SpaceConverter::PixelYToTile(height), SpaceConverter::PixelXToTile(width), SpaceConverter::PixelSpaceToTileSpace(boxPos) - objectPosition);
						aDoors.push_back(newDoor);
						aGameObjects.push_back(newDoor);
					}
					else if (object["name"] == "lever")
					{
						MakeLever(object, aCollisionManager, aGameObjects);
					}
					else if (object["name"] == "chest")
					{
						MakeChest(object, aCollisionManager, aGameObjects);
					}
					else if (object["name"] == "activator")
					{
						MakeActivator(object, aCollisionManager, aGameObjects);
					}
					else if (object["name"] == "book")
					{
						MakeBook(object, aCollisionManager, aGameObjects);
					}
					else if (object["name"] == "trigger")
					{
						MakeTrigger(object, aCollisionManager, aGameObjects);
					}
					else if (object["name"] == "crate")
					{
						MakeDestructible(object, aCollisionManager, aGameObjects, Destructible::eType::eCrate);
					}
					else if (object["name"] == "vase")
					{
						MakeDestructible(object, aCollisionManager, aGameObjects, Destructible::eType::eVase);
					}
				}
			}
			else if (layerName == "Items")
			{
				for (auto item : layer["objects"])
				{
					InteractableObject tempObject;
					tempObject.SetID(item["properties"]["ItemID"].get<int>());
					tempObject.SetPosition({ SpaceConverter::PixelXToTile(item["x"]), SpaceConverter::PixelYToTile(item["y"]) });
					aZoneObjects.push_back(tempObject);
				}
			}
			else if (layerName == "Enemies")
			{
				for (auto object : layer["objects"])
				{
					const unsigned int unitID = object["properties"]["unitID"].get<int>();
					const float unitHeight = object["height"].get<float>();
					const float unitWidth = object["width"].get<float>();
					const float unitPosX = object["x"].get<float>() + unitWidth / 2.f;
					const float unitPosY = object["y"].get<float>() - unitHeight / 2.f;
					const CommonUtilities::Vector2f unitPosition = { SpaceConverter::PixelXToTile(unitPosX), SpaceConverter::PixelYToTile(unitPosY) };
					myEnemyFactory->AddEnemy(unitID, unitPosition);
				}
			}
		}
	}
	InitiateTilingSpriteBatches();
}

const std::vector<std::vector<Tile>>& TiledMap::GetTiles() const
{
	return myTileLayers;
}

const std::vector<std::vector<Tile>>& TiledMap::GetTilesOverPlayer() const
{
	return myTileLayersOverPlayer;
}

const std::vector<Sprite>& TiledMap::GetGfx()
{
	return myGFXObjects;
}

std::vector<Tga2D::CSpriteBatch*>& TiledMap::GetTileMapBatches()
{
	for (Tga2D::CSpriteBatch* aBat: myTileMapBatches)
	{
		aBat->ResetNext();
	}
	return myTileMapBatches;
}

const Sprite & TiledMap::GetSpriteSheet(const unsigned short aIndex)
{
	return myTilesets[aIndex].mySpriteSheet;
}

void TiledMap::ClearAllSpriteBatches()
{
	for (int i = 0; i < myTileMapBatches.size(); ++i)
	{
		delete myTileMapBatches[i];
		myTileMapBatches[i] = nullptr;
	}
	myTileMapBatches.clear();

}

void TiledMap::FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer)
{
	for (auto& gfx : myGFXObjects)
	{
		if (gfx.GetSprite() != nullptr)
		{
			aRenderBuffer.push_back(&gfx);
		}
	}
}

void TiledMap::InitiateTilingSpriteBatches()
{
	for (int i = 0; i < myTilesets.size(); ++i)
	{
		myTileMapBatches.push_back(new Tga2D::CSpriteBatch(true));
		myTileMapBatches[myTileMapBatches.size() - 1]->Init(myTilesets[i].mySpriteSheetPath.c_str());
	}
	for (int i = 0; i < myTileMapBatches.size(); ++i)
	{
		for (int o = 0; o < 8000; ++o)
		{
			Tga2D::CSprite* tempSprite = new Tga2D::CSprite(nullptr);
			myTileMapBatches[i]->AddObject(tempSprite);
		}
	}
}

void TiledMap::MakeLever(const nlohmann::json & aObject, CollisionManager* aCollisionManager, std::vector<GameObject*>& aGameObjects)
{
	const float objectHeight = aObject["height"].get<float>();
	const float objectWidth = aObject["width"].get<float>();
	const float objectPosX = aObject["x"].get<float>() + objectWidth / 2.f;
	const float objectPosY = aObject["y"].get<float>() - objectHeight / 2.f;
	const CommonUtilities::Vector2f objectPosition = { SpaceConverter::PixelXToTile(objectPosX), SpaceConverter::PixelYToTile(objectPosY) };

	const int connectID = aObject["properties"]["connectID"].get<int>();
	Lever* lever = new Lever();
	lever->Init(aCollisionManager, objectPosition, connectID);

	unsigned int tileIndexData = aObject["gid"];
	unsigned short tilesetIndex = 0;
	int tilesetIndexAsInt = GetTileSetIndex(tileIndexData);
	if (tilesetIndexAsInt != -1)
	{
		tilesetIndex = static_cast<unsigned short>(tilesetIndexAsInt);
	}

	lever->AddSprite(myTilesets[tilesetIndex].mySpriteSheetPath.c_str());
	Sprite* leverSprite = dynamic_cast<Sprite*>(&lever->AccessDrawable());
	leverSprite->SetPivot({ 0.5f, 0.5f });

	std::string spritePath = myTilesets[tilesetIndex].mySpriteSheetPath;
	std::string fileType = ".dds";
	size_t letterBeforeFiletype = spritePath.rfind(fileType);

	spritePath = std::string(spritePath.begin(), spritePath.begin() + letterBeforeFiletype) + "_Pulled.dds";
	lever->SetUnlockedSprite(spritePath.c_str());

	const float height = myTilesets[tilesetIndex].myBoxCollider.myHeight;
	const float width = myTilesets[tilesetIndex].myBoxCollider.myWidth;
	const float posX = myTilesets[tilesetIndex].myBoxCollider.myPosX;
	const float posY = myTilesets[tilesetIndex].myBoxCollider.myPosY;
	const CommonUtilities::Vector2f topLeft = { objectPosX - objectWidth / 2.f, objectPosY - objectHeight / 2.f };
	CommonUtilities::Vector2f boxPos = { topLeft.x + posX, topLeft.y + posY };
	boxPos.x = boxPos.x + width / 2.f;
	boxPos.y = boxPos.y + height / 2.f;
	lever->AddBoxCollider(SpaceConverter::PixelYToTile(height), SpaceConverter::PixelXToTile(width), SpaceConverter::PixelSpaceToTileSpace(boxPos) - objectPosition);
	aGameObjects.push_back(lever);
}

void TiledMap::MakeChest(const nlohmann::json & aObject, CollisionManager * aCollisionManager, std::vector<GameObject*>& aGameObjects)
{
	const float objectHeight = aObject["height"].get<float>();
	const float objectWidth = aObject["width"].get<float>();
	const float objectPosX = aObject["x"].get<float>() + objectWidth / 2.f;
	const float objectPosY = aObject["y"].get<float>() - objectHeight / 2.f;
	const CommonUtilities::Vector2f objectPosition = { SpaceConverter::PixelXToTile(objectPosX), SpaceConverter::PixelYToTile(objectPosY) };

	const int lootID = aObject["properties"]["lootID"].get<int>();
	Chest* chest = new Chest();
	chest->Init(aCollisionManager, objectPosition, lootID);

	unsigned int tileIndexData = aObject["gid"];
	unsigned short tilesetIndex = 0;
	int tilesetIndexAsInt = GetTileSetIndex(tileIndexData);
	if (tilesetIndexAsInt != -1)
	{
		tilesetIndex = static_cast<unsigned short>(tilesetIndexAsInt);
	}

	chest->AddSprite(myTilesets[tilesetIndex].mySpriteSheetPath.c_str());
	Sprite* chestSprite = dynamic_cast<Sprite*>(&chest->AccessDrawable());
	chestSprite->SetPivot({ 0.5f, 0.5f });

	std::string spritePath = myTilesets[tilesetIndex].mySpriteSheetPath;
	std::string fileType = ".dds";
	size_t letterBeforeFiletype = spritePath.rfind(fileType);

	spritePath = std::string(spritePath.begin(), spritePath.begin() + letterBeforeFiletype) + "_Open.dds";
	chest->SetOpenedSprite(spritePath.c_str());

	const float height = myTilesets[tilesetIndex].myBoxCollider.myHeight;
	const float width = myTilesets[tilesetIndex].myBoxCollider.myWidth;
	const float posX = myTilesets[tilesetIndex].myBoxCollider.myPosX;
	const float posY = myTilesets[tilesetIndex].myBoxCollider.myPosY;
	const CommonUtilities::Vector2f topLeft = { objectPosX - objectWidth / 2.f, objectPosY - objectHeight / 2.f };
	CommonUtilities::Vector2f boxPos = { topLeft.x + posX, topLeft.y + posY };
	boxPos.x = boxPos.x + width / 2.f;
	boxPos.y = boxPos.y + height / 2.f;
	chest->AddBoxCollider(SpaceConverter::PixelYToTile(height), SpaceConverter::PixelXToTile(width), SpaceConverter::PixelSpaceToTileSpace(boxPos) - objectPosition);
	aGameObjects.push_back(chest);
}

void TiledMap::MakeActivator(const nlohmann::json & aObject, CollisionManager * aCollisionManager, std::vector<GameObject*>& aGameObjects)
{
	const float objectHeight = aObject["height"].get<float>();
	const float objectWidth = aObject["width"].get<float>();
	const float objectPosX = aObject["x"].get<float>() + objectWidth / 2.f;
	const float objectPosY = aObject["y"].get<float>() - objectHeight / 2.f;
	const CommonUtilities::Vector2f objectPosition = { SpaceConverter::PixelXToTile(objectPosX), SpaceConverter::PixelYToTile(objectPosY) };

	const int connectID = aObject["properties"]["connectID"].get<int>();
	Activator* activator = new Activator();
	activator->Init(aCollisionManager, objectPosition, connectID);

	unsigned int tileIndexData = aObject["gid"];
	unsigned short tilesetIndex = 0;
	int tilesetIndexAsInt = GetTileSetIndex(tileIndexData);
	if (tilesetIndexAsInt != -1)
	{
		tilesetIndex = static_cast<unsigned short>(tilesetIndexAsInt);
	}

	activator->AddSprite(myTilesets[tilesetIndex].mySpriteSheetPath.c_str());
	Sprite* activatorSprite = dynamic_cast<Sprite*>(&activator->AccessDrawable());
	activatorSprite->SetPivot({ 0.5f, 0.5f });

	std::string spritePath = myTilesets[tilesetIndex].mySpriteSheetPath;
	std::string fileType = ".dds";
	size_t letterBeforeFiletype = spritePath.rfind(fileType);

	spritePath = std::string(spritePath.begin(), spritePath.begin() + letterBeforeFiletype) + "_activated.dds";
	activator->SetActivatedSprite(spritePath.c_str());

	const float height = myTilesets[tilesetIndex].myBoxCollider.myHeight;
	const float width = myTilesets[tilesetIndex].myBoxCollider.myWidth;
	const float posX = myTilesets[tilesetIndex].myBoxCollider.myPosX;
	const float posY = myTilesets[tilesetIndex].myBoxCollider.myPosY;
	const CommonUtilities::Vector2f topLeft = { objectPosX - objectWidth / 2.f, objectPosY - objectHeight / 2.f };
	CommonUtilities::Vector2f boxPos = { topLeft.x + posX, topLeft.y + posY };
	boxPos.x = boxPos.x + width / 2.f;
	boxPos.y = boxPos.y + height / 2.f;
	activator->AddBoxCollider(SpaceConverter::PixelYToTile(height), SpaceConverter::PixelXToTile(width), SpaceConverter::PixelSpaceToTileSpace(boxPos) - objectPosition);
	aGameObjects.push_back(activator);
}

void TiledMap::MakeBook(const nlohmann::json & aObject, CollisionManager * aCollisionManager, std::vector<GameObject*>& aGameObjects)
{
	const float objectHeight = aObject["height"].get<float>();
	const float objectWidth = aObject["width"].get<float>();
	const float objectPosX = aObject["x"].get<float>() + objectWidth / 2.f;
	const float objectPosY = aObject["y"].get<float>() - objectHeight / 2.f;
	const CommonUtilities::Vector2f objectPosition = { SpaceConverter::PixelXToTile(objectPosX), SpaceConverter::PixelYToTile(objectPosY) };

	const int connectID1 = aObject["properties"]["connectID1"].get<int>();
	const int connectID2 = aObject["properties"]["connectID2"].get<int>();
	const int connectID3 = aObject["properties"]["connectID3"].get<int>();

	BookOfClassChange* book = new BookOfClassChange();
	book->Init(aCollisionManager, objectPosition, connectID1, connectID2, connectID3);

	unsigned int tileIndexData = aObject["gid"];
	unsigned short tilesetIndex = 0;
	int tilesetIndexAsInt = GetTileSetIndex(tileIndexData);
	if (tilesetIndexAsInt != -1)
	{
		tilesetIndex = static_cast<unsigned short>(tilesetIndexAsInt);
	}

	book->SetSprites(myTilesets[tilesetIndex].mySpriteSheetPath.c_str());

	const float height = myTilesets[tilesetIndex].myBoxCollider.myHeight;
	const float width = myTilesets[tilesetIndex].myBoxCollider.myWidth;
	const float posX = myTilesets[tilesetIndex].myBoxCollider.myPosX;
	const float posY = myTilesets[tilesetIndex].myBoxCollider.myPosY;
	const CommonUtilities::Vector2f topLeft = { objectPosX - objectWidth / 2.f, objectPosY - objectHeight / 2.f };
	CommonUtilities::Vector2f boxPos = { topLeft.x + posX, topLeft.y + posY };
	boxPos.x = boxPos.x + width / 2.f;
	boxPos.y = boxPos.y + height / 2.f;
	book->AddBoxCollider(SpaceConverter::PixelYToTile(height), SpaceConverter::PixelXToTile(width), SpaceConverter::PixelSpaceToTileSpace(boxPos) - objectPosition);

	aGameObjects.push_back(book);
}

void TiledMap::MakeTrigger(const nlohmann::json & aObject, CollisionManager * aCollisionManager, std::vector<GameObject*>& aGameObjects)
{
	const float objectHeight = aObject["height"].get<float>();
	const float objectWidth = aObject["width"].get<float>();
	const float objectPosX = aObject["x"].get<float>() + objectWidth / 2.f;
	const float objectPosY = aObject["y"].get<float>() + objectHeight / 2.f;
	const CommonUtilities::Vector2f objectPosition = { SpaceConverter::PixelXToTile(objectPosX), SpaceConverter::PixelYToTile(objectPosY) };

	Trigger* trigger = new Trigger();
	if (aObject["properties"]["event"].get<std::string>() == "firstEnemy")
	{
		trigger->Init(aCollisionManager, objectPosition, eTriggerType::FirstEnemy);
	}
	else if (aObject["properties"]["event"].get<std::string>() == "seesBook")
	{
		trigger->Init(aCollisionManager, objectPosition, eTriggerType::SeesBook);
	}

	trigger->AddBoxCollider(
		SpaceConverter::PixelYToTile(objectHeight),
		SpaceConverter::PixelXToTile(objectWidth),
		{0.f, 0.f}
	);

	aGameObjects.push_back(trigger);
}

void TiledMap::MakeDestructible(const nlohmann::json & aObject, CollisionManager * aCollisionManager, std::vector<GameObject*>& aGameObjects, Destructible::eType aType)
{
	const float objectHeight = aObject["height"].get<float>();
	const float objectWidth = aObject["width"].get<float>();
	const float objectPosX = aObject["x"].get<float>() + objectWidth * 0.5f;
	const float objectPosY = aObject["y"].get<float>() + objectHeight * 0.5f;
	const CU::Vector2f objectPosition = { SpaceConverter::PixelXToTile(objectPosX), SpaceConverter::PixelYToTile(objectPosY) };

	Destructible* d;

	Destructible::eLocation location;

	if (aObject["properties"]["path"].get<std::string>() == "hub_Tutorial")
	{
		location = Destructible::eLocation::eHub;
	}
	else if (aObject["properties"]["path"].get<std::string>() == "library")
	{
		location = Destructible::eLocation::eLibrary;
	}
	else if (aObject["properties"]["path"].get<std::string>() == "attic")
	{
		location = Destructible::eLocation::eAttic;
	}
	else //if (aObject["properties"]["path"].get<std::string>() == "mansion")
	{
		location = Destructible::eLocation::eMansion;
	}

	if (aType == Destructible::eType::eCrate)
	{
		d = new Destructible(objectPosition, aCollisionManager, location, Destructible::eType::eCrate);
		d->AddBoxCollider(
			SpaceConverter::PixelYToTile(objectHeight),
			SpaceConverter::PixelXToTile(objectWidth),
			{ 0.f, 0.f }
		);
	}
	else // Vase
	{
		d = new Destructible(objectPosition, aCollisionManager, location, Destructible::eType::eVase);
		d->AddBoxCollider(
			SpaceConverter::PixelYToTile(objectHeight * 0.5f),
			SpaceConverter::PixelXToTile(objectWidth * 0.5f),
			{ 0.f, 0.8f }
		);
	}

	aGameObjects.push_back(d);
}

int TiledMap::GetTileSetIndex(unsigned int aTileIndex)
{
	int index = 0;
	for (const auto& tileset : myTilesets)
	{
		if (aTileIndex >= tileset.myTileGidStartIndex && aTileIndex <= (tileset.myTileGidStartIndex + tileset.myTileGidCount - 1)) // If a set has nine tiles and start at 77 that will give us [77,  77 + 9 - 1] = [77, 85] witch is nine tiles.
		{
			return index;
		}
		++index;
	}
	assert(false && "Tile doesnt exist in tileset");
	return -1;
}


#pragma once
#include "Tile.h"
#include <vector>
#include "json.h"
#include "Sprite.h"
#include "InteractableObject.h"
#include "Destructible.h"

using namespace nlohmann;
namespace Tga2D
{
	class CSprite;
	class CSpriteBatch;
}
class EnemyFactory;
class CollisionManager;
class CollisionObject;
class RenderObject;
class Door;
class GameObject;
class ParticleSys;

class TiledMap
{
public:
	TiledMap();
	~TiledMap();
	void Init(const json& aTilesets, EnemyFactory* aEnemyFactory);
	void LoadTilesAndObjects
	(
		const json& aLayers,
		CollisionManager* aCollisionManager,
		std::vector<Door*>& aDoors,
		std::map<int, 
		CommonUtilities::Vector2f>& aConnections,
		std::vector<GameObject*>& aGameObjects,
		std::vector<InteractableObject>& aZoneObjects,
		const ParticleSys& aParticleSys
	);
	const std::vector<std::vector<Tile>>& GetTiles() const;
	const std::vector<std::vector<Tile>>& GetTilesOverPlayer() const;
	const std::vector<Sprite>& GetGfx();
	std::vector<Tga2D::CSpriteBatch*>& GetTileMapBatches();
	const Sprite& GetSpriteSheet(const unsigned short aIndex);
	void ClearAllSpriteBatches();
	void FillRenderBuffer(std::vector<RenderObject*> &aRenderBuffer);
	void InitiateTilingSpriteBatches();

	void MakeLever(const nlohmann::json& aObject, CollisionManager* aCollisionManager, std::vector<GameObject*>& aGameObjects);
	void MakeChest(const nlohmann::json& aObject, CollisionManager* aCollisionManager, std::vector<GameObject*>& aGameObjects);
	void MakeActivator(const nlohmann::json& aObject, CollisionManager* aCollisionManager, std::vector<GameObject*>& aGameObjects);
	void MakeBook(const nlohmann::json& aObject, CollisionManager* aCollisionManager, std::vector<GameObject*>& aGameObjects);
	void MakeTrigger(const nlohmann::json& aObject, CollisionManager* aCollisionManager, std::vector<GameObject*>& aGameObjects);
	void MakeDestructible(const nlohmann::json& aObject, CollisionManager* aCollisionManager, std::vector<GameObject*>& aGameObjects, Destructible::eType aType);
private:
	struct Tileset
	{
		Sprite mySpriteSheet;
		std::string mySpriteSheetPath;
		unsigned int myTileGidStartIndex;
		unsigned int myTileGidCount;
		unsigned int myTileColumns;
		unsigned int myTileRows;
		CU::Vector2f myImageSize;
		CU::Vector2f myTileSize;
		unsigned int myDepthLvl;

		struct BoxCollider
		{
			float myPosX;
			float myPosY;
			float myHeight;
			float myWidth;
		} myBoxCollider;
	};

	int GetTileSetIndex(unsigned int aTileIndex);
	std::vector<Tileset> myTilesets;
	std::vector<std::vector<Tile>> myTileLayers;
	std::vector<std::vector<Tile>> myTileLayersOverPlayer;
	std::vector<Sprite> myGFXObjects;
	std::vector<Tga2D::CSpriteBatch*> myTileMapBatches;
	EnemyFactory* myEnemyFactory;
};

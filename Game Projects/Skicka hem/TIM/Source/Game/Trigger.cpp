#include "stdafx.h"
#include "Trigger.h"
#include "PostMaster.h"
#include "CollisionObject.h"

Trigger::Trigger()
{
}

Trigger::~Trigger()
{
}

void Trigger::Init(CollisionManager * aCollisionManager, const CommonUtilities::Vector2f & aPosition, const eTriggerType aTriggerType)
{
	InitGO(aPosition, aCollisionManager, eCollisionLayer::Interactable);
	myTriggerType = aTriggerType;
	myHasTriggered = false;
	SetTag(eGameObjectTag::Trigger);
}

void Trigger::TriggerEvent()
{
	switch (myTriggerType)
	{
	case eTriggerType::FirstEnemy:
		if (myHasTriggered == false)
		{
			Message message(eMessageType::PopUpTextBox);
			message.data.myPopUpTextBox = ePopUpTextBox::FirstEnemy;
			PostMaster::SendMessages(message);

			myHasTriggered = true;
		}
		break;
	case eTriggerType::SeesBook:
		if (myHasTriggered == false)
		{
			Message message(eMessageType::PopUpTextBox);
			message.data.myPopUpTextBox = ePopUpTextBox::SeesBook;
			PostMaster::SendMessages(message);

			myHasTriggered = true;
		}
		break;
	default:
		break;
	}
}

void Trigger::OnCollisionEnter(CollisionObject & aOther)
{
	if (aOther.GetOwner()->GetTag() == eGameObjectTag::Player)
	{
		TriggerEvent();
		myHasTriggered = true;
		GetCollider()->SetIsSolid(false);
	}
}

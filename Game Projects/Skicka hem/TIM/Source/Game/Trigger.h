#pragma once
#include "GameObject.h"
enum class eTriggerType
{
	FirstEnemy,
	SeesBook,
};

class Trigger :
	public GameObject
{
public:
	Trigger();
	~Trigger();

	void Init(CollisionManager* aCollisionManager, const CommonUtilities::Vector2f& aPosition, const eTriggerType aTriggerType);
	void TriggerEvent();
	void OnCollisionEnter(CollisionObject &aOther);

private:
	eTriggerType myTriggerType;
	bool myHasTriggered;
};


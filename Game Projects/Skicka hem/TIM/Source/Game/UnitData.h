#pragma once

struct UnitData
{
	UnitData() {}
	~UnitData() {}

	float myMovementSpeed;
	float myReducedMovementSpeed;
	float myMaxHealth;
	float mySpeedIncreasePerMilliSec;
};

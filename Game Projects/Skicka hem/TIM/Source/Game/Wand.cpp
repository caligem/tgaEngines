#include "stdafx.h"
#include "Wand.h"
#include <iostream>
#include "CollisionLayer.h"
#include "BoxCollider.h"
#include "CircleCollider.h"
#include <Mathf.h>
#include "Player.h"

#include "tga2d/text/text.h"
#include <string>
#include <iomanip>
#include <sstream>

#include "utility.h"
#include "Enemy.h"

#define WANDMAXRANGE 60.f

Wand::~Wand()
{
	if (myDrawable != nullptr)
	{
		delete myDrawable;
		myDrawable = nullptr;
	}

	if (myColliderIndex != -1)
	{
		myCollisionManager->RemoveCollider(myCollisionLayer, myColliderIndex);
		myColliderIndex = -1;
	}
}

void Wand::Init(const CommonUtilities::Vector2f &aPosition, CollisionManager * aCollisionManager, Inventory& aInventory)
{
	myPlayerInventory = &aInventory;
	InitGO(aPosition, aCollisionManager, eCollisionLayer::Wand);

	myWandAnimation.Init("Sprites/Wands/inGameWandAnimation.dds");
	myWandAnimation.Setup(4, 8, 4);
	myWandAnimation.SetPivot({ 0.5f, 0.65f });
	myWandAnimation.SetLayerType(eLayerType::EGameObjectLayer);

	myTeleportUpgradeEnabled = false;
	myCurrentState = eWandState::eInHands;
}

void Wand::SetStateToInHands()
{
	myCurrentState = eWandState::eInHands;

	myCollidedWithWall = false;
	myCollidedDirection = { 0.f, 0.f };
}

void Wand::OnCollisionEnter(CollisionObject & aOther)
{
	if (aOther.GetLayer() == eCollisionLayer::Player)
	{
		SetStateToInHands();
		static_cast<Player*>(aOther.GetOwner())->RecievedWand();
	}

	if (aOther.GetLayer() == eCollisionLayer::Terrain)
	{
		myCanPlayerTeleportToMe = false;
		if (myCurrentState == eWandState::eThrown)
		{
			myCollidedWithWall = true;
			myCurrentState = eWandState::eStopped;
			myCollidedDirection = CalcCollidedDirection(aOther);
			AudioManager::GetInstance()->PlaySound("wandHittingWall");
		}
	}

	if (aOther.GetLayer() == eCollisionLayer::FallPit)
	{
		myCanPlayerTeleportToMe = false;
	}


	if (aOther.GetLayer() == eCollisionLayer::Enemies)
	{
		if (myCurrentState == eWandState::eRecalling || myCurrentState == eWandState::eStopping || myCurrentState == eWandState::eThrown)
		{
			Enemy* enemy = static_cast<Enemy*>(aOther.GetOwner());
			if (myCurrentState == eWandState::eRecalling || myCurrentState == eWandState::eStopping)
			{
				myHealFunction();
				enemy->TakeDamage(static_cast<int>(GetDamage() * 2.f));
			}
			else
			{
				myCurrentState = eWandState::eStopped;
				enemy->TakeDamage(static_cast<int>(GetDamage()));
			}
		}
	}
}

void Wand::OnCollisionStay(CollisionObject & aOther)
{
	if (aOther.GetLayer() == eCollisionLayer::Player)
	{
		if (myCurrentState != eWandState::eThrown)
		{
			SetStateToInHands();
			static_cast<Player*>(aOther.GetOwner())->RecievedWand();
		}
	}

	if (aOther.GetLayer() == eCollisionLayer::Terrain)
	{
		if (myCurrentState == eWandState::eStopped)
		{
			myCanPlayerTeleportToMe = true;
		}
		else
		{
			myCanPlayerTeleportToMe = false;
		}
	}

	if (aOther.GetLayer() == eCollisionLayer::FallPit)
	{
		myCanPlayerTeleportToMe = false;
	}

}

void Wand::OnCollisionLeave(CollisionObject & aOther)
{
	if (aOther.GetLayer() == eCollisionLayer::Terrain)
	{
		myCanPlayerTeleportToMe = true;
		myCurrentState = eWandState::eRecalling;
	}

	if (aOther.GetLayer() == eCollisionLayer::Player)
	{
		if (myCurrentState != eWandState::eThrown)
		{
			SetStateToInHands();
			static_cast<Player*>(aOther.GetOwner())->RecievedWand();
		}
	}

	if (aOther.GetLayer() == eCollisionLayer::FallPit)
	{
		myCanPlayerTeleportToMe = true;
	}
}

bool Wand::IsInHands()
{
	if (myCurrentState == eWandState::eInHands)
	{
		return true;
	}
	return false;
}

void Wand::UpdatePosition(CU::Vector2f &aPosition)
{
	myPlayerPosition = &aPosition;
}

void Wand::OnUpdate(float aDeltaTime)
{
	myWandAnimation.Update(aDeltaTime);

	switch (myCurrentState)
	{
		case eWandState::eInHands:
			myPosition = *myPlayerPosition;
			break;
		case eWandState::eThrown:
			UpdateThrow(aDeltaTime);
			break;
		case eWandState::eRecalling:
			UpdateRecall(aDeltaTime);
			break;
		case eWandState::eStopping:
			UpdateSlowDown(aDeltaTime);
			break;
		default:
			break;
	}
}

void Wand::FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer)
{
	if (myDrawable != nullptr && myCurrentState != eWandState::eInHands)
	{
		myDrawable->SetPosition({ myPosition.x, myPosition.y });
		aRenderBuffer.push_back(myDrawable);
	}

	if (!IsInHands() && CanTeleport() && myTeleportUpgradeEnabled)
	{
		myWandAnimation.SetPosition(myPosition);
		const float rotation = atan2(myDirection.x, -myDirection.y);
		myWandAnimation.SetRotation(rotation);
		aRenderBuffer.push_back(&myWandAnimation);
	}
}

void Wand::Throw(const CU::Vector2f &aDirection)
{
	myDirection = aDirection.GetNormalized();
	myDrawable->SetRotation(atan2(aDirection.x, -aDirection.y));
	myCurrentState = eWandState::eThrown;
}

void Wand::Recall()
{
	myCurrentState = eWandState::eRecalling;
}

void Wand::CancelRecall()
{
	myCurrentState = eWandState::eStopping;
}

void Wand::Reset()
{
	myPosition = *myPlayerPosition;
	SetStateToInHands();
}

const CommonUtilities::Vector2f Wand::CalcCollidedDirection(CollisionObject& aOther)
{
	CommonUtilities::Vector2f closestBoxPoint = myPosition;
	CommonUtilities::Vector2f rangeToCollision = { 0.f, 0.f };
	BoxCollider* terrainCollider = dynamic_cast<BoxCollider*>(&aOther);

	if (terrainCollider != nullptr)
	{
		closestBoxPoint.x = cit::Clamp(closestBoxPoint.x, terrainCollider->GetPosition().x + terrainCollider->GetOffset().x - terrainCollider->GetWidth() / 2.f, terrainCollider->GetPosition().x + terrainCollider->GetOffset().x + terrainCollider->GetWidth() / 2.f);
		closestBoxPoint.y = cit::Clamp(closestBoxPoint.y, terrainCollider->GetPosition().y + terrainCollider->GetOffset().y - terrainCollider->GetHeight() / 2.f, terrainCollider->GetPosition().y + terrainCollider->GetOffset().y + terrainCollider->GetHeight() / 2.f);
		const CommonUtilities::Vector2f newPosition = myPosition - (myDirection.GetNormalized() * static_cast<CircleCollider*>(GetCollider())->GetRadius());
		rangeToCollision = (newPosition - closestBoxPoint).GetNormalized();
	}

	return rangeToCollision;
}

void Wand::UpdateThrow(const float aDeltaTime)
{
	//if ((myCollisionManager->GetCollisionObject(eCollisionLayer::Wand, myColliderIndex))->GetIsInsideScreen())
	if ((myPosition - *myPlayerPosition).Length2() < WANDMAXRANGE * WANDMAXRANGE)
	{
		myPosition += myDirection * GetThrowSpeed() * aDeltaTime;
	}
	else
	{
		myCurrentState = eWandState::eRecalling;
	}
}

void Wand::UpdateRecall(const float aDeltaTime)
{
	myDirection = ((*myPlayerPosition) - myPosition).GetNormalized();
	myDrawable->SetRotation(atan2(myDirection.x, -myDirection.y));
	myPosition += myDirection * GetReturnSpeed() * aDeltaTime;
}

void Wand::UpdateSlowDown(const float aDeltaTime)
{
	mySlowDownTimer += aDeltaTime;
	float lerpedReturnSpeed = CommonUtilities::Lerp(GetReturnSpeed(), 0.0f, mySlowDownTimer);

	if (lerpedReturnSpeed < 0.f)
	{
		myCurrentState = eWandState::eStopped;
		mySlowDownTimer = 0.0f;
		return;
	}

	myDirection = ((*myPlayerPosition) - myPosition).GetNormalized();
	myDrawable->SetRotation(atan2(myDirection.x, -myDirection.y));
	myPosition += myDirection * lerpedReturnSpeed *  aDeltaTime;
}

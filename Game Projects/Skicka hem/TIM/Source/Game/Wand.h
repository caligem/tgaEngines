#pragma once
#include "GameObject.h"
#include "Inventory.h"
#include <functional>
#include "Text.h"
#include "Animation.h"

enum class eWandState
{
	eInHands,
	eThrown,
	eRecalling,
	eStopped,
	eStopping, //For slowdown when recall is cancelled
	count
};

class Wand : public GameObject
{
public:
	Wand()
	:
		myCanPlayerTeleportToMe(true),
		myCurrentState(eWandState::eInHands),
		mySlowDownTimer(0.0f),
		myCollidedWithWall(false),
		myCollidedDirection({0.f, 0.f}),
		myHealFunction(nullptr)
	{
		SetTag(eGameObjectTag::Wand);

	}

	~Wand();

	void Init(const CommonUtilities::Vector2f &aPosition, CollisionManager * aCollisionManager, Inventory& aInventory);
	float GetDamage()		const		{ return myPlayerInventory->GetActiveItem()->myWandDamage; }
	float GetThrowSpeed()	const		{ return myPlayerInventory->GetActiveItem()->myWandThrownSpeed; }
	float GetReturnSpeed()	const		{ return myPlayerInventory->GetActiveItem()->myWandRecallSpeed;; }
	float GetReturnRange()	const		{ return myPlayerInventory->GetActiveItem()->myWandReturnRange; }
	int   GetConnectID()	const		{ return myPlayerInventory->GetActiveItem()->myConnectID; }
	int&  GetHealth()		const		{ return myPlayerInventory->GetActiveItem()->myPlayerHealthMod; }
	eWandState GetWandState() const		{ return myCurrentState; }

	void SetStateToInHands();
	void OnCollisionEnter(CollisionObject &aOther) override;
	void OnCollisionStay(CollisionObject &aOther) override;
	void OnCollisionLeave(CollisionObject &aOther) override;

	void Throw(const CU::Vector2f &aDirection);
	void Recall();
	void CancelRecall();
	void Reset() override;

	bool IsInHands();
	void UpdatePosition(CU::Vector2f &aPosition);

	void OnUpdate(float aDeltaTime) override;
	void FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer) override;
	inline bool CanTeleport() { return myCanPlayerTeleportToMe; }

	const CommonUtilities::Vector2f& GetCollidedDirection() const { return myCollidedDirection; }

	inline void AttachHealFunction(std::function<void()> aHealFunction) { myHealFunction = aHealFunction; }

	void SetUpgradeStatus(bool aStatus) { myTeleportUpgradeEnabled = aStatus; }

private:
	const CommonUtilities::Vector2f CalcCollidedDirection(CollisionObject& aOther);
	void UpdateThrow(const float aDeltaTime);
	void UpdateRecall(const float aDeltaTime);
	void UpdateSlowDown(const float aDeltaTime);

	CommonUtilities::Vector2f myCollidedDirection;
	bool myCollidedWithWall;
	bool myCanPlayerTeleportToMe;
	bool myTeleportUpgradeEnabled;
 	float myOriginalThrowSpeed;
	float mySlowDownTimer;
	eWandState myCurrentState;
	CU::Vector2f myDirection;
	CU::Vector2f *myPlayerPosition;
	Inventory* myPlayerInventory;

	std::function<void()> myHealFunction;

	Animation myWandAnimation;
};


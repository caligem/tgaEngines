#include "stdafx.h"
#include "Zone.h"

#include "InputManager.h"
#include "Camera.h"
#include "Stubbler.h"
#include "CollisionManager.h"
#include "CollisionObject.h"
#include "DataBase.h"
#include "Door.h"
#include "Lever.h"
#include "Chest.h"
#include "Activator.h"
#include "CollisionObject.h"
#include "PostMaster.h"
#include "ParticleSys.h"
#include "AudioManager.h"
#include "BookOfClassChange.h"
#include "Boss.h"

Zone::Zone(Inventory & aInventory, InGameUI& aUI)
: myPlayer(aInventory, aUI)
{
}

Zone::~Zone()
{
	int size = static_cast<int>(myGameObjects.size() - 1);
	for (int i = size; i >= 0; --i)
	{
		if (myGameObjects[i] != nullptr)
		{
			if (myGameObjects[i]->GetTag() == eGameObjectTag::Player || myGameObjects[i]->GetTag() == eGameObjectTag::CannonBeard || myGameObjects[i]->GetTag() == eGameObjectTag::Stubbler)
			{
				continue;
			}
			delete myGameObjects[i];
			myGameObjects[i] = nullptr;
		}
	}
}

void Zone::Init(const eZoneType aZoneType, const json& aMap, CommonUtilities::InputManager* aInputManager, Fader* aFader, Camera* aCamera, DataBase* aDataBase, std::map<int, CommonUtilities::Vector2f>& aConnections, ParticleSys* aParticleSys)
{
	myZoneType = aZoneType;
	myChangeZoneType = eZoneType::None;
	myTriggerdDoorID = -1;
	myParticleSys = aParticleSys;
	myGameObjects.reserve(2048);
	myDoors.reserve(4);

	myCollisionManager.Init();

	myCamera = aCamera;

	myProjectileManager.Init(myCollisionManager);
	myEnemyFactory.Init(myGameObjects, myCollisionManager, *aDataBase, myProjectileManager);

	myInputManager = aInputManager;
	myTilemap.Init(aMap["tilesets"], &myEnemyFactory);

	if (myZoneType == eZoneType::Mansion)
	{
		myBoss = new Boss();
		myBoss->InitGO(myBossCoordinates, &myCollisionManager, eCollisionLayer::Enemies);
		myBoss->Init(nullptr);
		myBoss->AttachManagers(&myEnemyFactory, &myProjectileManager);
		myGameObjects.push_back(myBoss);
	}
	else
	{
		myBossCoordinates = { 0.f, 0.f };
		myBoss = nullptr;
	}

	myTilemap.LoadTilesAndObjects(aMap["layers"], &myCollisionManager, myDoors, aConnections, myGameObjects, myZoneItems, *myParticleSys);

	myPlayer.InitGO({ 0.f, 0.f }, &myCollisionManager, eCollisionLayer::Player);
	myPlayer.Init(myInputManager, aFader, aDataBase->GetData("player"), *myParticleSys);
	InitZoneItems(aDataBase->GetData("items"));
	myGameObjects.push_back(&myPlayer);

	myRespawner.Init(&myPlayer);

	PostMaster::Subscribe(eMessageType::PlayerTeleport, aCamera);
	for (auto& door : myDoors)
	{
		PostMaster::Subscribe(eMessageType::DoorLockUnlock, door);
		PostMaster::Subscribe(eMessageType::PlayerInteract, door);
	}
	for (auto& GO : myGameObjects)
	{
		if (GO->GetTag() == eGameObjectTag::Lever)
		{
			Lever* lever = dynamic_cast<Lever*>(GO);
			PostMaster::Subscribe(eMessageType::PlayerInteract, lever);
			lever->LockUnLockDoor();
		}
		else if (GO->GetTag() == eGameObjectTag::Activator)
		{
			Activator* activator = dynamic_cast<Activator*>(GO);
			activator->LockUnlockDoor();
		}
		else if (GO->GetTag() == eGameObjectTag::Chest)
		{
			Chest* chest = dynamic_cast<Chest*>(GO);
			PostMaster::Subscribe(eMessageType::PlayerInteract, chest);

			auto& inventoryItem = myPlayer.GetInventory().GetWorldItems()[chest->GetLootID()];
			int lootKeyID = inventoryItem.myConnectID;
			chest->BindSprite(const_cast<Sprite*>(&inventoryItem.myThumbnailImage));
			
			if (lootKeyID != -1)
			{
				Message message(eMessageType::DoorLockUnlock);
				message.data.myInt = lootKeyID;
				PostMaster::SendMessages(message);
			}
		}
		else if (GO->GetTag() == eGameObjectTag::BookOfClassChange)
		{
			BookOfClassChange* book = dynamic_cast<BookOfClassChange*>(GO);
			PostMaster::Subscribe(eMessageType::PlayerInteract, book);
			const CommonUtilities::Vector2f offset = { 0.f, 1.0f };
			book->AttachBoss(myBoss);
			myBossCoordinates = book->GetPosition();
			myBoss->SetPosition(myBossCoordinates + offset);
		}
	}
}

void Zone::InitZoneItems(const nlohmann::json& aInventoryData)
{

	for (int i = 0; i < myZoneItems.size(); ++i)
	{
		myZoneItems[i].Init(eObjectType::Wand, &myCollisionManager);
		myZoneItems[i].AddSprite(aInventoryData["Wands"][myZoneItems[i].GetID()]["Visuals"]["Icon"].get<std::string>().c_str());
		myZoneItems[i].AddCircleCollider(0.5f, { 0.f, 0.f });
	}
}

void Zone::EnterZone(const CommonUtilities::Vector2f aSpawnPosition)
{
	PlayCurrentLvlMusic();
	myPlayer.StopVelocity();
	myPlayer.SetPosition(aSpawnPosition);
	myPlayer.SetRespawnPosition(aSpawnPosition);
	myCamera->SetParentPos(&myPlayer);
}

void Zone::LeaveZone()
{
	myPlayer.StopVelocity();
	myChangeZoneType = eZoneType::None;
	myTriggerdDoorID = -1;
}

void Zone::Update(float aDeltaTime)
{
	UpdateInteractables();
#ifndef _RETAIL
	if (myInputManager->KeyPressed(CommonUtilities::Keys::F2))
	{
		myPlayer.ToggleNoClip();
	}
	if (myInputManager->KeyPressed(CommonUtilities::Keys::F8))
	{
		myRespawner.RespawnPlayer();
	}
#endif

	myProjectileManager.Update(aDeltaTime);
	GameObject::UpdateAllGameObjects(myGameObjects, aDeltaTime);
	myCamera->Update(aDeltaTime);
	myCollisionManager.Update(myCamera->GetPos());

	CheckDoorsForZoneChange();
	myRespawner.Update(aDeltaTime);
}

void Zone::UpdateInteractables()
{
	for (int i = 0; i < myZoneItems.size(); ++i)
	{
		if (myZoneItems[i].GetShouldRemove())
		{
			myZoneItems[i].GetCollider()->SetLayer(eCollisionLayer::None);
			break;
		}
	}

}

void Zone::FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer)
{
	for (int i = 0; i < myZoneItems.size(); ++i)
	{
		if (myZoneItems[i].GetShouldRemove() == false)
		{
			myZoneItems[i].FillRenderBuffer(aRenderBuffer);
		}
	}
	for (int i = 0; i < myGameObjects.size(); ++i)
	{
		if (!myGameObjects[i]->GetShouldRemove())
		{
			myGameObjects[i]->FillRenderBuffer(aRenderBuffer);
		}	
	}

	myProjectileManager.FillRenderBuffer(aRenderBuffer);

	myTilemap.FillRenderBuffer(aRenderBuffer);
}

TiledMap & Zone::GetMap()
{
	return myTilemap;
}

void Zone::DrawColliderDebugLines(const CommonUtilities::Vector2f& aCameraPosition)
{
	myCollisionManager.DebugDrawCollisionObjects(aCameraPosition);
}

const eZoneType Zone::ChangeZoneType()
{
	return myChangeZoneType;
}

void Zone::FreezePlayer(bool aShouldFreeze)
{
	myPlayer.FreezePlayer(aShouldFreeze);
}

void Zone::ResetEnemies()
{
	for (auto& obj : myGameObjects)
	{
		if (obj->GetTag() == eGameObjectTag::Stubbler && !static_cast<Stubbler*>(obj)->GetIsDead())
		{
			obj->Reset();
		}
	}

	myProjectileManager.Reset();
}

void Zone::TeleportWandToPlayer()
{
	myPlayer.TeleportWandToPlayer();
}

void Zone::CheckDoorsForZoneChange()
{
	for (Door* door : myDoors)
	{
		if (door->ShouldChangeZone())
		{
			myTriggerdDoorID = door->GetDoorID();
			myChangeZoneType = static_cast<eZoneType>(door->GetDoorGoToZoneIndex());
			return;
		}
	}
}

void Zone::PlayCurrentLvlMusic()
{
	switch (myZoneType)
	{
	case eZoneType::Entrance:
		AudioManager::GetInstance()->PlayMusic("Entrance");
		break;
	case eZoneType::Library:
		AudioManager::GetInstance()->PlayMusic("Library");
		break;
	case eZoneType::Attic:
		AudioManager::GetInstance()->PlayMusic("Attic");
		break;
	case eZoneType::Mansion:
		AudioManager::GetInstance()->PlayMusic("Mansion");
		break;
	default:
		break;
	}
}

#pragma once
#include <vector>
#include "GameObject.h"
#include "CollisionManager.h"
#include "TiledMap.h"
#include "Player.h"
#include "Respawner.h"
#include "EnemyFactory.h"
#include "ProjectileManager.h"

namespace CommonUtilities
{
	class InputManager;
}
class Camera;
class RenderObject;
class DataBase;
class Door;
class ParticleSys;
class Fader;
class Boss;

enum class eZoneType
{
	None = -1,
	Entrance,
	Library,
	Attic,
	Mansion,
};

class Zone
{
public:
	Zone(Inventory& aInventory, InGameUI& aUI);
	~Zone();

	void Init(const eZoneType aZoneType, const json& aMap, CommonUtilities::InputManager* aInputManager, Fader* aFader, Camera* aCamera, DataBase* aDataBase, std::map<int, CommonUtilities::Vector2f>& aConnections, ParticleSys* aParticleSys);
	void InitZoneItems(const nlohmann::json& aInventoryData);
	void EnterZone(const CommonUtilities::Vector2f aSpawnPosition);
	void LeaveZone();
	void Update(float aDeltaTime);
	void UpdateInteractables();
	void FillRenderBuffer(std::vector<RenderObject*>& aRenderBuffer);
	TiledMap& GetMap();

	void DrawColliderDebugLines(const CommonUtilities::Vector2f& aCameraPosition);
	const eZoneType ChangeZoneType();
	inline const int GetTriggerdDoorID() const { return myTriggerdDoorID; }

	void FreezePlayer(bool aShouldFreeze);

	void ResetEnemies();
	void TeleportWandToPlayer();
private:
	eZoneType myZoneType;
	eZoneType myChangeZoneType;
	int myTriggerdDoorID;

	std::vector<GameObject*> myGameObjects;
	ParticleSys* myParticleSys;
	TiledMap myTilemap;
	CollisionManager myCollisionManager;
	EnemyFactory myEnemyFactory;
	ProjectileManager myProjectileManager;

	CommonUtilities::InputManager* myInputManager;

	Camera* myCamera;
	Player myPlayer;
	Respawner myRespawner;
	std::vector<InteractableObject> myZoneItems;
	
	void CheckDoorsForZoneChange();
	void PlayCurrentLvlMusic();
	std::vector<Door*> myDoors;
	Boss* myBoss;
	CommonUtilities::Vector2f myBossCoordinates;
};


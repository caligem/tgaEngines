#pragma once
#pragma once
#include <vector>
#include <new>
#include <assert.h>

namespace carlivan_tools
{
	template<typename object_type>
	class dynamic_pool
	{
	public:
		class iterator
		{
			friend dynamic_pool<object_type>;
		public:
			iterator()
			{
				myPool = nullptr;
				myIndex = -1;
			}
			iterator(const iterator& aRhs)
			{
				myIndex = aRhs.myIndex;
				myPool = aRhs.myPool;
			}
			iterator &operator=(const iterator& aRhs)
			{
				myIndex = aRhs.myIndex;
				myPool = aRhs.myPool;
				return *this;
			}
			object_type* operator->()
			{
				return &myPool->get_object(*this);
			}
			object_type& operator*()
			{
				return myPool->get_object(*this);
			}
		private:
			iterator(const int& aIndex, dynamic_pool<object_type>& aPool)
			{
				myIndex = aIndex;
				myPool = &aPool;
			}
			int myIndex;
			dynamic_pool<object_type> *myPool;
		};
		dynamic_pool();
		~dynamic_pool();

		void allocate(int aByteAmount);
		void deallocate();
		iterator add_object();
		void remove_object(iterator &aIndexToRemoveAt);

		object_type &get_object(iterator aIndex);

	private:
		enum class index_state
		{
			free,
			used
		};
		char *myBuffer;
		int myMaxByteSize;
		std::vector<int> myFreeIndexes;
		std::vector<index_state> myIndexStates;
	};

	template<typename object_type>
	inline dynamic_pool<object_type>::dynamic_pool()
	{
		myBuffer = nullptr;
		myMaxByteSize = 0;
	}
	template<typename object_type>
	inline dynamic_pool<object_type>::~dynamic_pool()
	{
		assert(myBuffer == nullptr && "memory leak found, call deallocate.");
	}

	template<typename object_type>
	inline void dynamic_pool<object_type>::allocate(int aByteAmount)
	{
		assert(myBuffer == nullptr && "buffer already allocated.");
		myBuffer = new char[aByteAmount];
		myMaxByteSize = aByteAmount;
		myFreeIndexes.resize(static_cast<size_t>(aByteAmount / sizeof(object_type)));
		myIndexStates.resize(static_cast<size_t>(aByteAmount / sizeof(object_type)));
		for (size_t i = 0; i < myFreeIndexes.size(); i++)
		{
			myFreeIndexes[i] = i;
		}
	}

	template<typename object_type>
	inline void dynamic_pool<object_type>::deallocate()
	{
		assert(myFreeIndexes.size() * sizeof(object_type) == myMaxByteSize && "remove all values before deallocating.");
		if (myBuffer != nullptr)
		{
			delete[] myBuffer;
			myBuffer = nullptr;
			myMaxByteSize = 0;
		}
	}

	template<typename object_type>
	inline typename dynamic_pool<object_type>::iterator dynamic_pool<object_type>::add_object()
	{
		assert(myBuffer != nullptr && "buffer not allocated.");
		assert(myFreeIndexes.size() > 0 && "pool filled, consider allocating more memory.");
		int freeIndex = myFreeIndexes.back();
		myIndexStates[freeIndex] = dynamic_pool::index_state::used;
		myFreeIndexes.pop_back();
		object_type *newObj = new (&myBuffer[freeIndex * sizeof(object_type)]) object_type();
		return dynamic_pool<object_type>::iterator(freeIndex, *this);
	}

	template<typename object_type>
	inline void dynamic_pool<object_type>::remove_object(iterator &aIndexToRemoveAt)
	{
		assert(myIndexStates[aIndexToRemoveAt.myIndex] == dynamic_pool::index_state::used && "That index is not used.");
		myIndexStates[aIndexToRemoveAt.myIndex] = dynamic_pool::index_state::free;
		object_type *objToRemove = reinterpret_cast<object_type*>(&myBuffer[aIndexToRemoveAt.myIndex * sizeof(object_type)]);
		objToRemove->~object_type();
		aIndexToRemoveAt.myPool = nullptr;
		aIndexToRemoveAt.myIndex = -1;
		myFreeIndexes.push_back(aIndexToRemoveAt.myIndex);
	}

	template<typename object_type>
	inline object_type & dynamic_pool<object_type>::get_object(iterator aIndex)
	{
		assert(aIndex.myIndex != -1 && "iterator is not bound to object.");
		assert(myIndexStates[aIndex.myIndex] == dynamic_pool::index_state::used && "That index is not used.");
		return *reinterpret_cast<object_type*>(&myBuffer[aIndex.myIndex * sizeof(object_type)]);
	}
}
namespace cit = carlivan_tools;
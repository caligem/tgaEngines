// stdafx.h : include file for standard system include files,
// or project specific include files that are used frequently, but
// are changed infrequently
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // Exclude rarely-used stuff from Windows headers

#define cast_f(x) static_cast<float>(x)
#define cast_i(x) static_cast<int>(x)
#define cast_ui(x) static_cast<unsigned int>(x)

// TODO: reference additional headers your program requires here

#include "tga2d/sprite/sprite.h"
#include "tga2d/engine.h"
#include <fstream>
#include "SpaceConverter.h"
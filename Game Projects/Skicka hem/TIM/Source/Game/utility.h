#pragma once

namespace carlivan_tools
{
	float Lerp(const float aFrom, const float aTo, const float aPercentage);

	float Clamp(const float aValueToClamp, const float aMin, const float aMax);
	int Clamp(const int aValueToClamp, const int aMin, const int aMax);

	template<typename Pointer>
	void SafeDelete(Pointer *&aPointerToDelete)
	{
		if (aPointerToDelete != nullptr)
		{
			delete aPointerToDelete;
			aPointerToDelete = nullptr;
		}
	}

	template <typename T> int sign(T val) {
		return (T(0) < val) - (val < T(0));
	}
}
namespace cit = carlivan_tools;

// nevermind all this.
//
//struct Sound;
//struct AudioManager
//{
//	void PlaySound(Sound &);
//};
//
//
//struct Sound
//{
//	void Play()
//	{
//		ourManager->PlaySound(*this);
//	}
//	static void SetManager(AudioManager *aAudioManager)
//	{
//		ourManager = aAudioManager;
//	}
//private:
//	static AudioManager *ourManager;
//};
//
//Sound::SetManager(&AudioManager());
//
//Sound myHitSound;
//myHitSound.Play();
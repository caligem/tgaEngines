
----------------------------
--- Don't Edit this file ---
----------------------------

function protect(table)
    return setmetatable({}, {
        __index = table,
        __newindex = function(table, key, value)
            error("Attempting to change constant " ..
                   tostring(key) .. " to " .. tostring(value), 2)
        end
    })
end

Event = {
	OnEnter = 0,
	OnLeave = 1,
	LastElement = 2,
}
Event = protect(Event)
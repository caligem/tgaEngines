ExitGame(): 
		Exit the game
		No arguments

ExitToDesktop(): 
		Exit To Desktop
		No arguments

HideCanvas(): 
		Hide a canvas
		Takes name of canvas as argument

Print(): 
		Print Text
		A String to print to consol

SetMasterVolume(): 
		Sets the Master Volume
		A value between 0 - 100

SetMusicVolume(): 
		Sets the Music Volume
		A value between 0 - 100

SetSFXVolume(): 
		Sets the SFX Volume
		A value between 0 - 100

ShowCanvas(): 
		Show a canvas
		Takes name of canvas as argument

StartScene(): 
		Start Scene
		Takes levelName of scene/level as argument

StartShowroom(): 
		Start Showroom
		Takes no argument

debug_colliders_dynamic(): 
		Debug PlayerColliders

debug_colliders_static(): 
		Debug WorldColliders

debug_player(): 
		Debug Player

debug_serverpos(): 
		Debug ServerPos


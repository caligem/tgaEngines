#include "DataPass.si"
#include "../CameraBuffer.si"

cbuffer InstanceData : register(b1)
{
	float4x4 toWorld;
}

cbuffer BoneData : register(b2)
{
	float4x4 bones[64];
}

static const float4x4 rot180 = float4x4(
	-1.f, 0.f, 0.f, 0.f,
	0.f, 1.f, 0.f, 0.f,
	0.f, 0.f, -1.f, 0.f,
	0.f, 0.f, 0.f, 1.f
);

PixelInput main(VertexInput input)
{
	PixelInput output;

	input.myPosition.w = 1.f;

	
	if(bones[0]._41 != -1)
	{
		float4 vWeights = input.myWeights;
		uint4 vBones = uint4(
			(uint)input.myBones.x,
			(uint)input.myBones.y,
			(uint)input.myBones.z,
			(uint)input.myBones.w
		);
		
		float4x4 finalMatrix;
		finalMatrix = vWeights.x * bones[vBones.x];
		finalMatrix += vWeights.y * bones[vBones.y];
		finalMatrix += vWeights.z * bones[vBones.z];
		finalMatrix += vWeights.w * bones[vBones.w];
		float4 animatedPos = mul(finalMatrix, input.myPosition);
		
		output.myPosition = mul(toWorld, mul(rot180, animatedPos));	
	}
	else
	{
		output.myPosition = mul(toWorld, mul(rot180, input.myPosition));
	}
	output.myWorldPosition = output.myPosition;
	output.myPosition = mul(viewProjection, output.myPosition);

	output.myUV = input.myUV;
	input.myNormal.w = 0.f;
	output.myNormal.xyz = normalize(mul((float3x3)toWorld, mul((float3x3)rot180, input.myNormal.xyz)));
	output.myTangent.xyz = normalize(mul((float3x3)toWorld, mul((float3x3)rot180, input.myTangent.xyz)));
	output.myBinormal.xyz = normalize(mul((float3x3)toWorld, mul((float3x3)rot180, input.myBinormal.xyz)));

	output.myViewPosition = float4(cameraOrientation._14, cameraOrientation._24, cameraOrientation._34, 1.f);

	return output;
}

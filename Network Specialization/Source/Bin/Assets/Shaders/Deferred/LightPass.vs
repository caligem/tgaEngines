#include "LightPass.si"
#include "../CameraBuffer.si"

cbuffer InstanceData : register(b1)
{
	float4x4 toWorld;
}

PixelInput main(VertexInput input)
{
	PixelInput output;

	input.myPosition.w = 1.f;
	output.myPosition = mul(toWorld, input.myPosition);
	output.myWorldPosition = output.myPosition;

	output.myUV = input.myUV;

	return output;
}

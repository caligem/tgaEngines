#include "Sampling.si"

//Ambient Diffuse
float3 ReflectionFresnel(
	in float3 aCameraPosition,
	in float3 aWorldPosition,
	in float2 aUV
){
	float roughness = Roughness(aUV);
	float3 substance = Substance(aUV);
	float3 normal = ObjectNormal(aUV);
	float3 toEye = normalize(aCameraPosition - aWorldPosition);
	float VdotN = pow(1.f - saturate(dot(toEye, normal)), 5.f);

	float3 fresnel = VdotN * (1.f - substance);
	fresnel = fresnel / (9.f - 5.f * roughness) + substance;

	return fresnel;
}

float3 Fresnel(
	in float3 aToLight,
	in float3 aCameraPosition,
	in float3 aWorldPosition,
	in float2 aUV
){
	float3 toEye = normalize(aCameraPosition - aWorldPosition);
	float3 substance = Substance(aUV);
	float3 halfvec = normalize(aToLight + toEye);

	float LdotH = pow(1.f - saturate(dot(aToLight, halfvec)), 5.f);
	return LdotH * (1.f - substance) + substance;
}

float Distribution(
	in float3 aToLight,
	in float3 aCameraPosition,
	in float3 aWorldPosition,
	in float2 aUV
){
	float roughness = Roughness(aUV);
	float3 normal = ObjectNormal(aUV);
	float3 toEye = normalize(aCameraPosition - aWorldPosition);

	float3 halfvec = normalize(aToLight + toEye);
	float HdotN = saturate(dot(halfvec, normal));

	float m = roughness * roughness;
	float m2 = m*m;
	float denominator = HdotN*HdotN * (m2 - 1.f) + 1.f;

	return m2 / (3.1415926535898 * denominator * denominator);
}

float Visibility(
	in float3 aToLight,
	in float3 aCameraPosition,
	in float3 aWorldPosition,
	in float2 aUV
){
	float roughness = Roughness(aUV);
	float roughnessRemapped = (roughness + 1.f) / 2.f;
	float3 normal = ObjectNormal(aUV);
	float3 toEye = normalize(aCameraPosition - aWorldPosition);

	float NdotL = saturate(dot(normal, aToLight));
	float NdotV = saturate(dot(normal, toEye));

	float k = roughnessRemapped * roughnessRemapped * 1.772454f;
	float G1V = NdotV * (1.f - k) + k;
	float G1L = NdotL * (1.f - k) + k;

	return 0.5f * rcp(G1V * G1L);
	//return (NdotV * NdotL) / (G1V * G1L);
}

//Attenuation functions
float LambertAttenuation(in float3 aToLight, in float3 aNormal)
{
	return saturate(dot(aToLight, aNormal));
}
float DistanceAttenuation(in float aDist2, in float aRange2)
{
	float att = saturate(1.f - aDist2/aRange2);
	att *= att;
	return att;
}
float ConeAttenuation(in float3 aToLight, in float3 aLightDirection, in float aAngle)
{
	float rho = dot(aToLight, -aLightDirection);
	float phi = aAngle;
	float theta = aAngle*0.75f;
	float falloff = 1.0f;

	return pow(
		((rho-cos(phi/2.f)) / (cos(theta/2.f)-cos(phi/2.f))),
		falloff
	);
}

//Diffuse / Specularity
float3 Diffuse(
	in float3 aToLight,
	in float3 aCameraPosition,
	in float3 aWorldPosition,
	in float2 aUV
){
	float3 metalnessalbedo = MetalnessAlbedo(aUV);
	float lambert = LambertAttenuation(aToLight, ObjectNormal(aUV));
	float3 fresnel = Fresnel(aToLight, aCameraPosition, aWorldPosition, aUV);

	return metalnessalbedo * lambert * (1.f - fresnel);
}

float3 Specularity(
	in float3 aToLight,
	in float3 aCameraPosition,
	in float3 aWorldPosition,
	in float2 aUV
){
	float lambert = LambertAttenuation(aToLight, ObjectNormal(aUV));
	float3 fresnel = Fresnel(aToLight, aCameraPosition, aWorldPosition, aUV);

	float distribution = Distribution(aToLight, aCameraPosition, aWorldPosition, aUV);
	float visibility = Visibility(aToLight, aCameraPosition, aWorldPosition, aUV);

	return lambert * fresnel * distribution * visibility;
}

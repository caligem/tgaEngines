#include "Particle.si"

GeometryInput main(VertexInput input)
{
	GeometryInput output;

	input.myPosition.w = 1.f;
	output.myPosition = mul(toCamera, input.myPosition);
		
	output.myColor = input.myColor;
	output.mySize = input.mySize;
	output.myRotation = input.myRotation;
	
	return output;
}
#include "../Deferred/DataPass.si"
#include "../CameraBuffer.si"

#include "../VFXData.si"

cbuffer InstanceData : register(b1)
{
	float4x4 toWorld;
}

cbuffer BoneData : register(b2)
{
	float4x4 bones[64];
}

PixelInput main(VertexInput input)
{
	PixelInput output;

	float t = customData.x;
	
	if(input.myUV.x > 0.7f)
	{
		float x = 1.f - t;
		input.myUV.y += saturate(1.f - x * x * x);
	}
	else
	{
		input.myUV.y += saturate(t*t + 0.15f);
	}
	
	float4x4 rot180 = float4x4(
		-1.f, 0.f, 0.f, 0.f,
		0.f, 1.f, 0.f, 0.f,
		0.f, 0.f, -1.f, 0.f,
		0.f, 0.f, 0.f, 1.f
	);

	output.myPosition = mul(toWorld, mul(rot180, input.myPosition));
	
	output.myWorldPosition = output.myPosition;
	output.myPosition = mul(viewProjection, output.myPosition);

	output.myUV = input.myUV;
	input.myNormal.w = 0.f;
	output.myNormal.xyz = normalize(mul((float3x3)toWorld, mul((float3x3)rot180, input.myNormal.xyz)));
	output.myTangent.xyz = normalize(mul((float3x3)toWorld, mul((float3x3)rot180, input.myTangent.xyz)));
	output.myBinormal.xyz = normalize(mul((float3x3)toWorld, mul((float3x3)rot180, input.myBinormal.xyz)));

	output.myViewPosition = float4(cameraOrientation._14, cameraOrientation._24, cameraOrientation._34, 1.f);

	return output;
}

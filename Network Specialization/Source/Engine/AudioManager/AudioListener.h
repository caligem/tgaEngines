#pragma once
#include "../CommonUtilities/Vector.h"
#include "FMOD/fmod_studio.hpp"
class AudioListener
{
public:
	AudioListener();
	~AudioListener();
	CommonUtilities::Vector3f GetPosition() { return myPosition; }
	void SetPosition(CommonUtilities::Vector3f aPosition);
	FMOD_3D_ATTRIBUTES* Get3DAttributes() { return &my3DAttributes; }
private:
	CommonUtilities::Vector3f myPosition;
	FMOD_3D_ATTRIBUTES my3DAttributes;
};


#include "Bounds.h"

namespace CommonUtilities
{
	CBounds::CBounds()
	{
	}


	CBounds::~CBounds()
	{
	}

	void CommonUtilities::CBounds::SetMinMax(const Vector3f & aMin, const Vector3f & aMax)
	{
		myMin = aMin;
		myMax = aMax;
		RecalculateCenterAndExtents();
	}

	void CommonUtilities::CBounds::Encapsulate(const Vector3f & aPosition)
	{
		EncapsulateWithoutRecalculating(aPosition);

		RecalculateCenterAndExtents();
	}

	bool CommonUtilities::CBounds::Contains(const Vector3f & aPosition) const
	{

		if (aPosition.x < myMin.x)
		{
			return false;
		}
		else if (aPosition.x > myMax.x)
		{
			return false;
		}
		else if (aPosition.y < myMin.y)
		{
			return false;
		}
		else if (aPosition.y > myMax.y)
		{
			return false;
		}
		else if (aPosition.z < myMin.z)
		{
			return false;
		}
		else if (aPosition.z > myMax.z)
		{
			return false;
		}

		return true;
	}

	Vector3f CommonUtilities::CBounds::GetClosestPoint(const Vector3f & aPoint) const
	{
		CommonUtilities::Vector3f closestPoint = aPoint;

		if (aPoint.x < myMin.x)
		{
			closestPoint.x = myMin.x;
		}
		else if (aPoint.x > myMax.x)
		{
			closestPoint.x = myMax.x;
		}

		if (aPoint.y < myMin.y)
		{
			closestPoint.y = myMin.y;
		}
		else if (aPoint.y > myMax.y)
		{
			closestPoint.y = myMax.y;
		}

		if (aPoint.z < myMin.z)
		{
			closestPoint.z = myMin.z;
		}
		else if (aPoint.z > myMax.x)
		{
			closestPoint.z = myMax.z;
		}

		return closestPoint;
	}

	void CBounds::RecalculateCenterAndExtents()
	{
		myCenter = (myMin + myMax) * 0.5f;
		myExtents = (myMax - myMin) * 0.5f;
	}

	void CBounds::Inflate(float aInflation)
	{
		myMin -= CommonUtilities::Vector3f(aInflation, aInflation, aInflation);
		myMax += CommonUtilities::Vector3f(aInflation, aInflation, aInflation);
		RecalculateCenterAndExtents();
	}

	void CBounds::EncapsulateWithoutRecalculating(const Vector3f & aPosition)
	{
		if (aPosition.x < myMin.x)
		{
			myMin.x = aPosition.x;
		}
		else if (aPosition.x > myMax.x)
		{
			myMax.x = aPosition.x;
		}
		
		if (aPosition.y < myMin.y)
		{
			myMin.y = aPosition.y;
		}
		else if (aPosition.y > myMax.y)
		{
			myMax.y = aPosition.y;
		}

		if (aPosition.z < myMin.z)
		{
			myMin.z = aPosition.z;
		}
		else if (aPosition.z > myMax.z)
		{
			myMax.z = aPosition.z;
		}
	}

}
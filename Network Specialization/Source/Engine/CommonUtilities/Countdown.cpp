#include "Countdown.h"
#include "..\Engine\IWorld.h"
#include "Timer.h"

#include "Mathf.h"

Countdown::Countdown()
{
	myMaxTime = -1.0f;
	myCurrTime = 0.0f;
	myTrigger = nullptr;
	myType = type::oneshot;
	myFinished = false;
	myIsStarted = false;
}

Countdown::~Countdown()
{

}

void Countdown::Start()
{
	myIsStarted = true;
}

void Countdown::Stop()
{
	myIsStarted = false;
}

void Countdown::Set(const float aMaxTime, const type aCountdownType, const std::function<void()>& aTrigger)
{
	myMaxTime = aMaxTime;
	myCurrTime = 0.0f;
	myTrigger = aTrigger;
	myType = aCountdownType;
	myFinished = false;
	myIsStarted = false;
}

void Countdown::Update()
{
	if (myFinished == false && myIsStarted == true)
	{
		if (myCurrTime >= myMaxTime)
		{
			myTrigger();
			if (myType == type::autoreset)
			{
				myCurrTime = 0.0f;
			}
			else if (myType == type::oneshot)
			{
				myFinished = true;
			}
		}
		else
		{
			myCurrTime += IWorld::Time().GetDeltaTime();
		}
	}
}

void Countdown::Reset()
{
	myCurrTime = 0.0f;
	myFinished = false;
	myIsStarted = false;
}

void Countdown::SetIsFinished()
{
	myFinished = true;
	myCurrTime = myMaxTime;
}

void Countdown::SetMaxTime(const float aMaxTime)
{
	myMaxTime = aMaxTime;
}

float Countdown::GetCurrentTime() const
{
	return myCurrTime;
}

float Countdown::GetMaxTime() const
{
	return myMaxTime;
}

float Countdown::GetPercent() const
{
	return CommonUtilities::Clamp01(myCurrTime / myMaxTime);
}

bool Countdown::GetIsFinished() const
{
	return myFinished;
}

bool Countdown::GetIsStarted() const
{
	return myIsStarted;
}

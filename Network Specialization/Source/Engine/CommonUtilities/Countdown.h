#pragma once
#include <functional>

class Countdown
{
public:
	enum class type
	{
		//triggers once
		oneshot,
		//calls reset after triggered
		autoreset,
		//keeps calling trigger when finished
		continuous
	};

	Countdown();
	~Countdown();
	void Start();
	void Stop();
	void Set(const float aMaxTime, const type aCountdownType, const std::function<void()> &aTrigger);
	void Update();
	void Reset();
	void SetIsFinished();
	void SetMaxTime(const float aMaxTime);

	float GetCurrentTime() const;
	float GetMaxTime() const;
	float GetPercent() const;
	bool GetIsFinished() const;
	bool GetIsStarted() const;

private:
	std::function<void()> myTrigger;
	type myType;
	float myMaxTime;
	float myCurrTime;
	bool myFinished;
	bool myIsStarted;
};
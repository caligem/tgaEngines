#include "Intersection.h"

#include <limits>
#include "Mathf.h"

namespace CommonUtilities
{
	bool CircleVsCircle(const Vector2f & aPoint0, float aRadius0, const Vector2f & aPoint1, float aRadius1, CommonUtilities::Vector2f & aContactPoint)
	{
		float d2 = (aPoint0 - aPoint1).Length2();
		float r2 = aRadius0 + aRadius1;
		r2 *= r2;

		if( d2 <= r2 )
		{
			aContactPoint = (aPoint0 + aPoint1) / 2.f;
			return true;
		}
		return false;
	}

	bool CircleVsLine(const Vector2f & aPointCircle, float aRadiusCircle, const Vector2f & aStartPointLine, const Vector2f & aEndPointLine, CommonUtilities::Vector2f & aContactPoint)
	{
		CommonUtilities::Vector2f dir = aEndPointLine - aStartPointLine;
		CommonUtilities::Vector2f normal = dir.GetNormal().GetNormalized();

		CommonUtilities::Vector2f diff = aPointCircle - aStartPointLine;

		float t = diff.Dot(dir) / dir.Length2();
		CommonUtilities::Vector2f closestPoint = aStartPointLine + dir * t;
		if( t <= 0.f )
		{
			closestPoint = aStartPointLine;
		}
		else if( t >= 1.f )
		{
			closestPoint = aEndPointLine;
		}

		float d2 = (aPointCircle - closestPoint).Length2();

		float r2 = aRadiusCircle*aRadiusCircle;

		if( d2 <= r2 )
		{
			aContactPoint = closestPoint;
			return true;
		}
		return false;
	}

	float RayVsTriangle(
		const Vector3f & aRayOrigin,
		const Vector3f & aRayDirection,
		const Vector3f & aPoint0,
		const Vector3f & aPoint1,
		const Vector3f & aPoint2,
		Vector3f & aContactPoint,
		Vector3f & aClosestPoint,
		float aMinT
	) {
		const float kNoIntersection = FLT_MAX;
		bool testFailed = false;

		Vector3f e1 = aPoint1 - aPoint0;
		Vector3f e2 = aPoint2 - aPoint1;

		Vector3f n = e1.Cross(e2);

		float dot = n.Dot(aRayDirection);

		//if (!(dot < 0.f)) return false;

		float d = n.Dot(aPoint0);
		float t = d - n.Dot(aRayOrigin);

		//if (!(t <= 0.f)) return false;

		if (!(t < dot*aMinT)) testFailed = true;

		t /= dot;

		if (t < 0.f) testFailed = true;

		Vector3f p = aRayOrigin + aRayDirection * t;

		float u0, u1, u2;
		float v0, v1, v2;
		if (std::fabsf(n.x) > std::fabsf(n.y))
		{
			if (std::fabsf(n.x) > std::fabsf(n.z))
			{
				u0 = p.y - aPoint0.y;
				u1 = aPoint1.y - aPoint0.y;
				u2 = aPoint2.y - aPoint0.y;

				v0 = p.z - aPoint0.z;
				v1 = aPoint1.z - aPoint0.z;
				v2 = aPoint2.z - aPoint0.z;
			}
			else
			{
				u0 = p.x - aPoint0.x;
				u1 = aPoint1.x - aPoint0.x;
				u2 = aPoint2.x - aPoint0.x;

				v0 = p.y - aPoint0.y;
				v1 = aPoint1.y - aPoint0.y;
				v2 = aPoint2.y - aPoint0.y;
			}
		}
		else
		{
			if (std::fabsf(n.y) > std::fabsf(n.z))
			{
				u0 = p.x - aPoint0.x;
				u1 = aPoint1.x - aPoint0.x;
				u2 = aPoint2.x - aPoint0.x;

				v0 = p.z - aPoint0.z;
				v1 = aPoint1.z - aPoint0.z;
				v2 = aPoint2.z - aPoint0.z;
			}
			else
			{
				u0 = p.x - aPoint0.x;
				u1 = aPoint1.x - aPoint0.x;
				u2 = aPoint2.x - aPoint0.x;

				v0 = p.y - aPoint0.y;
				v1 = aPoint1.y - aPoint0.y;
				v2 = aPoint2.y - aPoint0.y;
			}
		}

		float temp = u1 * v2 - v1 * u2;
		if (!(temp != 0.f)) return kNoIntersection;

		temp = 1.f / temp;

		float alpha = (u0 * v2 - v0 * u2) * temp;
		if (!(alpha >= 0.f)) testFailed = true;

		float beta = (u1 * v0 - v1 * u0) * temp;
		if (!(beta >= 0.f)) testFailed = true;

		float gamma = 1.f - alpha - beta;
		if (!(gamma >= 0.f)) testFailed = true;

		aContactPoint = p;

		aClosestPoint = FindClosestPointOnTriangleFromPoint(aContactPoint, aPoint0, aPoint1, aPoint2);

		if (testFailed)
		{
			return kNoIntersection;
		}

		return t;
	}
	Vector3f FindClosestPointOnTriangleFromPoint(const Vector3f & aPoint, const Vector3f & aVertex0, const Vector3f & aVertex1, const Vector3f & aVertex2)
	{
		Vector3f points[3] = {
			FindClosestPointOnLineSegmentFromPoint(aPoint, aVertex0, aVertex1),
			FindClosestPointOnLineSegmentFromPoint(aPoint, aVertex1, aVertex2),
			FindClosestPointOnLineSegmentFromPoint(aPoint, aVertex2, aVertex0)
		};

		int closest = 0;
		float minDist = (aPoint - points[0]).Length2();
		for (int i = 1; i < 3; ++i)
		{
			float dist = (aPoint - points[i]).Length2();
			if(dist < minDist)
			{
				closest = i;
				minDist = dist;
			}
		}

		return points[closest];
	}
	Vector3f FindClosestPointOnLineSegmentFromPoint(const Vector3f & aPoint, const Vector3f & aStart, const Vector3f & aEnd)
	{
		Vector3f dir = aEnd - aStart;

		Vector3f diff = aPoint - aStart;

		float t = diff.Dot(dir) / dir.Length2();
		if (t < 0.f)
		{
			t = 0.f;
		}
		else if( t > 1.f )
		{
			t = 1.f;
		}
		return aStart + dir * t;
	}
}
#pragma once

#include "Vector.h"

namespace CommonUtilities
{

	bool CircleVsCircle(const Vector2f& aPoint0, float aRadius0, const Vector2f& aPoint1, float aRadius1, CommonUtilities::Vector2f& aContactPoint);

	bool CircleVsLine(const Vector2f& aPointCircle, float aRadiusCircle, const Vector2f& aStartPointLine, const Vector2f& aEndPointLine, CommonUtilities::Vector2f& aContactPoint);

	float RayVsTriangle(
		const Vector3f& aRayOrigin,
		const Vector3f& aRayDirection,
		const Vector3f& aPoint0,
		const Vector3f& aPoint1,
		const Vector3f& aPoint2,
		Vector3f& aContactPoint,
		Vector3f& aClosestPoint,
		float aMinT
	);

	Vector3f FindClosestPointOnTriangleFromPoint(
		const Vector3f& aPoint,
		const Vector3f& aVertex0,
		const Vector3f& aVertex1,
		const Vector3f& aVertex2
	);

	Vector3f FindClosestPointOnLineSegmentFromPoint(
		const Vector3f& aPoint,
		const Vector3f& aStart,
		const Vector3f& aEnd
	);

}

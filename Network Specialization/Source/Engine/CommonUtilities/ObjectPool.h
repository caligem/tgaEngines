#pragma once

#include <deque>
#include <DL_Debug.h>
#include <Macros.h>

#define ID_T(ObjectType) idptr__<ObjectType>
#define ID_T_INVALID(ObjectType) Iterator<ObjectType>::idptr_invalid

#define TYPENAME_STRING(T) #T

template <typename ObjectType>
struct idptr__ {
	idptr__()
	{
		val = -1;
	}
	idptr__(const int& aVal) : val(aVal) {}
	int val;

	template<typename ObjectType>
	inline bool operator< (const idptr__<ObjectType>& rhs) { return this->val < rhs.val; }

	template<typename ObjectType>
	inline bool operator< (const idptr__<ObjectType>& rhs) const { return this->val < rhs.val; }

	inline friend std::ostream& operator<<(std::ostream& os, const idptr__<ObjectType>& aIDPtr)
	{
		os << "idptr(" << typeid(ObjectType).name() << "): " << aIDPtr.val;
		return os;
	}

	inline bool operator==(const idptr__<ObjectType>& aRhs)
	{
		return val == aRhs.val;
	}
	inline const bool operator==(const idptr__<ObjectType>& aRhs) const
	{
		return val == aRhs.val;
	}
	inline const bool operator!=(const idptr__<ObjectType>& aRhs) const
	{
		return !(val == aRhs.val);
	}
};

template<typename ObjectType>
struct SDataWrapper : public ObjectType
{
	SDataWrapper()
	{
		id = Iterator<ObjectType>::idptr_invalid;
	}
	idptr__<ObjectType> id;
};

template<typename ObjectType>
class Iterator
{

	template<typename T>
	class ObjectPool;
public:
	static idptr__<ObjectType> idptr_invalid;
	Iterator() { myDataPtr = nullptr; }
	Iterator(SDataWrapper<ObjectType>** aBeginPtr, SDataWrapper<ObjectType>** aEndPtr)
	{
		myBeginPtr = aBeginPtr;
		myEndPtr = aEndPtr;
		myDataPtr = *aBeginPtr;
	};

	Iterator operator++();
	Iterator operator++(int junk);
	SDataWrapper<ObjectType>& operator*() { return *myDataPtr; }
	SDataWrapper<ObjectType>* operator->() { return myDataPtr; }

	Iterator operator=(const Iterator& other) { myDataPtr = other.myDataPtr; return *this; }
	bool operator==(const Iterator& rhs) { return myDataPtr == rhs.myDataPtr; }
	bool operator!=(const Iterator& rhs) { return myDataPtr != rhs.myDataPtr; }

	SDataWrapper<ObjectType>* GetPtr() { return myDataPtr; }

private:
	friend ObjectPool<ObjectType>;

	SDataWrapper<ObjectType>** myBeginPtr;
	SDataWrapper<ObjectType>** myEndPtr;
	SDataWrapper<ObjectType>* myDataPtr;
};

template<typename ObjectType>
class ObjectPool
{
public:
	ObjectPool(const unsigned short aChunkSize = 1024);
	~ObjectPool();

	idptr__<ObjectType> Acquire();
	bool Release(const idptr__<ObjectType>& aIDPtr);
	void ReleaseAll();
	ObjectType* GetObj(const idptr__<ObjectType>& aIDPtr);

	unsigned short GetSize();
	__forceinline const bool IsValid(const int& aIndex);

	bool IsEmpty();
	void PrintInfo() { ENGINE_LOG(CONCOL_DEFAULT, "ObjectPool<%s> contains %d items.", typeid(ObjectType).name(), counter); }

	Iterator<ObjectType> begin() { return Iterator<ObjectType>(&myBeginIterator, &myEndIterator); }
	Iterator<ObjectType> end() { return Iterator<ObjectType>(&myEndIterator, &myEndIterator); }
private:
	friend Iterator<ObjectType>;

	size_t counter = 0;
	unsigned short myChunkSize;
	SDataWrapper<ObjectType>* myData;

	SDataWrapper<ObjectType>* myBeginIterator;
	SDataWrapper<ObjectType>* myEndIterator;

	std::deque<idptr__<ObjectType>> myDeque;

	__forceinline int V(const idptr__<ObjectType>& aIDptr) { return aIDptr.val; }

	void AssignIteratorsAtAcquire(const idptr__<ObjectType>& aIDptr);
	void AssignIteratorsAtRelease(const idptr__<ObjectType>& aIDptr);
};


template<typename ObjectType>
typename idptr__<ObjectType> Iterator<ObjectType>::idptr_invalid = -1;

template<typename ObjectType>
inline ObjectPool<ObjectType>::ObjectPool(const unsigned short aChunkSize)
{
	myChunkSize = aChunkSize;
	myData = new SDataWrapper<ObjectType>[myChunkSize];

	for (int i = 0; i < myChunkSize; ++i)
	{
		myDeque.push_back(i);
	}
	
	myBeginIterator = &myData[0];
	myEndIterator = &myData[0];
}

template<typename ObjectType>
inline ObjectPool<ObjectType>::~ObjectPool()
{
	delete[] myData;
	myData = nullptr;
}

template<typename ObjectType>
inline typename idptr__<ObjectType> ObjectPool<ObjectType>::Acquire()
{
	if (myDeque.empty())
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to Acquire an ID from a full ObjectPool of type %s, size is %d!!!", typeid(ObjectType).name(), myChunkSize);
		return Iterator<ObjectType>::idptr_invalid;
	}
	idptr__<ObjectType> id = myDeque.front();
	myDeque.pop_front();
	AssignIteratorsAtAcquire(id);
	myData[V(id)].id = id;

	if (myDeque.empty())
	{
		ENGINE_LOG(CONCOL_WARNING, "Used up the last ID from an ObjectPool of type %s, size is %d!!!", typeid(ObjectType).name(), myChunkSize);
	}
	counter++;

	return std::move(id);
}

template<typename ObjectType>
inline bool ObjectPool<ObjectType>::Release(const idptr__<ObjectType> & aIDPtr)
{
	int index = V(aIDPtr);
	if (index < 0 || index >= myChunkSize)
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Trying to release an Object from ObjectPool<%s> but ID is outside ChunkSize(%d).", typeid(ObjectType).name(), myChunkSize);
		return false;
	}
	if (myData[index].id == Iterator<ObjectType>::idptr_invalid)
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Trying to get an Object(ID: %d) from ObjectPool<%s> but ID is invalid.", aIDPtr.val, typeid(ObjectType).name());
		return false;
	}
	counter--;
	myDeque.push_front(aIDPtr);
	AssignIteratorsAtRelease(aIDPtr);
	myData[index].id = Iterator<ObjectType>::idptr_invalid;

	return true;
}

template<typename ObjectType>
inline void ObjectPool<ObjectType>::ReleaseAll()
{
	myDeque.clear();
	counter = 0;
	for (int i = 0; i < myChunkSize; ++i)
	{
		myDeque.push_back(i);
	}
}

template<typename ObjectType>
inline ObjectType * ObjectPool<ObjectType>::GetObj(const idptr__<ObjectType>& aIDPtr)
{
	int index = V(aIDPtr);
	if (index < 0 || index >= myChunkSize)
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Trying to get an Object from ObjectPool<%s> but ID is outside ChunkSize(%d).", typeid(ObjectType).name(), myChunkSize);
		return nullptr;
	}

	if (myData[index].id == Iterator<ObjectType>::idptr_invalid)
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Trying to get an Object(ID: %d) from ObjectPool<%s> but ID is invalid.", aIDPtr.val, typeid(ObjectType).name());
		return nullptr;
	}

	return &myData[index];
}

template<typename ObjectType>
inline unsigned short ObjectPool<ObjectType>::GetSize()
{
	return myChunkSize;
}

template<typename ObjectType>
inline const bool ObjectPool<ObjectType>::IsValid(const int & aIndex)
{
	if (aIndex < 0 || aIndex > myChunkSize)
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Trying to run IsValid on Index outside ChunkSize in ObjectPool<%s>.", typeid(ObjectType).name());
		return false;
	}

	if (myData[V(aIndex)].id == Iterator<ObjectType>::idptr_invalid)
	{
		return false;
	}
	else
	{
		return true;
	}
}

template<typename ObjectType>
inline bool ObjectPool<ObjectType>::IsEmpty()
{
	return (&myBeginIterator == &myEndIterator);
}

template<typename ObjectType>
inline void ObjectPool<ObjectType>::AssignIteratorsAtAcquire(const idptr__<ObjectType> & aIDptr)
{
	int index = V(aIDptr);

	if (myBeginIterator->id == Iterator<ObjectType>::idptr_invalid)
	{
		myBeginIterator = &myData[index];
		myEndIterator = myBeginIterator + 1;
		return;
	}

	if (index < myBeginIterator->id.val)
	{
		myBeginIterator = &myData[index];
		return;
	}

	if (index > (myEndIterator-1)->id.val)
	{
		myEndIterator = &myData[index+1];
	}
}

template<typename ObjectType>
inline void ObjectPool<ObjectType>::AssignIteratorsAtRelease(const idptr__<ObjectType>& aIDptr)
{
	const int index = V(aIDptr);
	int newBeginIndex = index;
	int newEndIndex = index;

	if (newBeginIndex == myBeginIterator->id.val)
	{
		newBeginIndex++;
		while (myData[newBeginIndex].id == Iterator<ObjectType>::idptr_invalid && newBeginIndex <= myChunkSize)
		{
			if (&myData[newBeginIndex] == myEndIterator)
			{
				myBeginIterator = myEndIterator;
				return;
			}

			newBeginIndex++;
		}
		myBeginIterator = &myData[newBeginIndex];
	}

	if (newEndIndex == (myEndIterator-1)->id.val)
	{
		while (myData[newEndIndex].id == Iterator<ObjectType>::idptr_invalid && newEndIndex > 0)
		{
			newEndIndex--;
			if (&myData[newEndIndex] == myBeginIterator)
			{
				myBeginIterator = myEndIterator;
				return;
			}
		}
		myEndIterator = &myData[newEndIndex];
	}
}

template <typename ObjectType>
inline typename Iterator<ObjectType> Iterator<ObjectType>::operator++()
{
	while (myDataPtr != *myEndPtr)
	{
		myDataPtr++;
		if (!((myDataPtr)->id == Iterator<ObjectType>::idptr_invalid))
		{		
			return *this;
		}

	}
	return *this;
}

template <typename ObjectType>
inline typename Iterator<ObjectType> Iterator<ObjectType>::operator++(int junk)
{
	junk;
	Iterator i = *this;

	while (myDataPtr != *myEndPtr)
	{
		myDataPtr++;
		if (!((myDataPtr+1)->id == Iterator<ObjectType>::idptr_invalid))
		{
			return i;
		}
	}
	return i;
}
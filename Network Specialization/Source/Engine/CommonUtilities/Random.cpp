#include "Random.h"
#include <ctime>

namespace CommonUtilities
{
	int64_t _Random::mySeed = 0;	
	
	void _Random::Init(int64_t aSeed)
	{
		mySeed = (aSeed ^ int64_t(0x5DEECE66D)) & ((int64_t(1) << 48) - 1);
	}

	int32_t _Random::Next(int32_t aBits)
	{
		mySeed = (mySeed * int64_t(0x5DEECE66D) + int64_t(0xB)) & ((int64_t(1) << 48) - 1);
		return (int32_t)(uint64_t(mySeed) >> (48 - aBits));
	}

	int32_t _Random::NextInt(int32_t aRange)
	{
		if ((aRange & -aRange) == aRange)
			return (int32_t)((aRange * (int64_t)Next(31)) >> 31);

		int32_t bits, val;
		do
		{
			bits = Next(31);
			val = bits % aRange;
		} while (bits - val + (aRange - 1) < 0);
		return val;
	}

	void InitRand()
	{
		_Random::Init(static_cast<unsigned int>(time(0)));
	}
	void InitRand(int64_t seed)
	{
		_Random::Init(seed);
	}
	float Random()
	{
		return static_cast<float>(_Random::NextInt(1<<30)) / static_cast<float>(1<<30);
	}
	float RandomRange(float aMin, float aMax)
	{
		return Random() * (aMax - aMin) + aMin;
	}
}
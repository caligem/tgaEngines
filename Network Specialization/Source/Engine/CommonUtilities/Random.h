#pragma once

#include <inttypes.h>

namespace CommonUtilities
{
	void InitRand();
	void InitRand(int64_t seed);
	float Random();
	float RandomRange(float aMin, float aMax);

	class _Random
	{
	public:
		static void Init(int64_t aSeed);
		static int32_t NextInt(int32_t aRange);

	private:
		static int32_t Next(int32_t aBits);

		static int64_t mySeed;
	};
}


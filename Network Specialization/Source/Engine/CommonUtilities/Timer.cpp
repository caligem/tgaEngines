#include "Timer.h"

#include "Mathf.h"

namespace CommonUtilities
{
	Timer::Timer()
	{
		myStartTime = GetCurrentTime();
		myRealCurrentTime = myStartTime;
		myRealPreviousTime = myStartTime;
		myRealTotalTime = myRealCurrentTime - myRealPreviousTime;
		myCurrentTime = myStartTime;
		myPreviousTime = myStartTime;
		myTotalTime = myCurrentTime - myPreviousTime;
		mySpeed = 1.0;
		myInterpolateOffset = 0.04f;
		myPingOffset = 0.0;
		myServerTimer = 0.f;
	}

	Timer::~Timer() {}

	void Timer::Update()
	{
		myRealPreviousTime = myRealCurrentTime;
		myRealCurrentTime += GetCurrentTime() - myRealPreviousTime;

		myRealTotalTime = myRealCurrentTime - myStartTime + myPingOffset;

		myPreviousTime = myCurrentTime;
		myCurrentTime += (GetCurrentTime() - myRealPreviousTime) * mySpeed;

		myTotalTime = myCurrentTime - myStartTime + myPingOffset;
		myServerTimer += GetRealDeltaTime();
		printf("Get Server Timer : %f \n", myServerTimer);
	}

	float Timer::GetRealDeltaTime() const
	{
		return std::move(static_cast<float>(myRealCurrentTime - myRealPreviousTime));
	}
	float Timer::GetRealDeltaTimeInMS() const
	{
		return Clamp(static_cast<float>(myRealCurrentTime - myRealPreviousTime), 0.f, 1.f) * 1000.f;
	}
	float Timer::GetRealTotalTime() const
{
		return std::move(static_cast<float>(myRealTotalTime));
	}
	float Timer::GetRealTotalTimeInMS() const
	{
		return static_cast<float>(myRealTotalTime) * 1000.f;
	}
	float Timer::GetDeltaTime() const
	{
		return std::move(Clamp(static_cast<float>(myCurrentTime - myPreviousTime), 0.f, static_cast<float>(mySpeed)));
	}
	float Timer::GetDeltaTimeInMS() const
	{
		return Clamp(static_cast<float>(myCurrentTime - myPreviousTime), 0.f, static_cast<float>(mySpeed)) * 1000.f;
	}
	float Timer::GetTotalTime() const
	{
		return std::move(static_cast<float>(myTotalTime));
	}
	float Timer::GetTotalTimeInMS() const
	{
		return static_cast<float>(myTotalTime) * 1000.f;
	}
	float Timer::GetServerTimer() const
	{
		return  static_cast<float>(myServerTimer);
	}
	void Timer::SetServerTimer(double aStartTime)
	{
		myServerTimer = aStartTime;
	}
	float Timer::GetInterpolateTime()
	{
		return static_cast<float>(myServerTimer) - myInterpolateOffset;
	}

	float Timer::GetServerTimerInMS()
	{
		return static_cast<float>(myServerTimer * 1000.f);
	}

	double Timer::GetCurrentTime()
	{
		return static_cast<double>(
			std::chrono::high_resolution_clock::now().time_since_epoch().count()
		) / std::chrono::high_resolution_clock::period::den;
	}
}

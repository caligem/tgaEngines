#pragma once

#include <chrono>

namespace CommonUtilities
{
	class Timer
	{
	public:
		Timer();
		~Timer();

		void Update();

		float GetRealDeltaTime() const;
		float GetRealDeltaTimeInMS() const;
		float GetRealTotalTime() const;
		float GetRealTotalTimeInMS() const;
		float GetDeltaTime() const;
		float GetDeltaTimeInMS() const;
		float GetTotalTime() const;
		float GetTotalTimeInMS() const;
		float GetServerTimer() const;
		void SetServerTimer(double aStartTime);

		float GetInterpolateTime();

		void SetSpeed(float aSpeed) { mySpeed = static_cast<double>(aSpeed); }
		const float GetSpeed() const { return static_cast<float>(mySpeed); }

		void SetInterpolate(float aInterpolateVal) { myInterpolateOffset = aInterpolateVal; }
		float GetInterpolateOffset() const { return myInterpolateOffset; }
		double GetCurrentTime();
		void SetPingOffset(double aPingOffset) { myPingOffset = aPingOffset; }
		float GetServerTimerInMS();
	private:

		double myStartTime;
		double myServerTimer;

		double myRealCurrentTime;
		double myRealPreviousTime;
		double myRealTotalTime;

		double myCurrentTime;
		double myPreviousTime;
		double myTotalTime;

		double mySpeed;
		float myInterpolateOffset;
		double myPingOffset;
	};
}

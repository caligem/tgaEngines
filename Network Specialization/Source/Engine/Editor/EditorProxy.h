#pragma once

#include "PostMasterMessageType.cs"

class CEditor;

class CEditorProxy
{
public:
	CEditorProxy();
	~CEditorProxy();

	void Init(EEditorType aEditorType, void * aHandle);
	void Shutdown();
	void StartEngine();

	bool IsRunning();

	void SendWindowMessage(unsigned int Msg, void* wParam, void* lParam);

	void SendMessageToEngine(unsigned char* pData, EMessageType aMessageType, EDataType aDataType);
	void SetEditorMessageCallback(void(* aCallback)(const unsigned char*, EMessageType, EDataType));

	void GotFocus();
	void LostFocus();

private:
	CEditor* myEditor;

};


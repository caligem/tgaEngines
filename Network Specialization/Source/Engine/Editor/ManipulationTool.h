#pragma once

#include "GameObject.h"

class CManipulationTool
{
public:
	CManipulationTool();
	~CManipulationTool();

	enum EMode
	{
		EMode_None,
		EMode_Translate,
		EMode_Rotate,
		EMode_Scale
	};
	enum ESpace
	{
		ESpace_Local,
		ESpace_World
	};

	void UpdateGameObject(CGameObject aGameObject);

	void SetMode(EMode aMode);
	void SetSpace(ESpace aSpace);

private:
	EMode myMode;
	ESpace mySpace;

};


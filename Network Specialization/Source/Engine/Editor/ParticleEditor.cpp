#include "stdafx.h"
#include "ParticleEditor.h"
#include "IEngine.h"
#include "IWorld.h"

#include "JsonDocument.h"

#include <Mathf.h>

CParticleEditor::CParticleEditor()
{
	myApplicationName = L"ParticleEditor";

	myPivot = { 0.0f, 0.0f, 0.0f };
	myRotation = { 0.f, 0.f };
	myZoom = 20.f;
	myShowDebugLines = true;
}


CParticleEditor::~CParticleEditor()
{
}

void CParticleEditor::InitCallback()
{
	myHasFinishedLoading = true;
	CSceneManager& sceneManager = IWorld::GetSceneManager();
	ID_T(CScene) sceneID = sceneManager.CreateScene();
	sceneManager.SetActiveScene(sceneID);

	myCamera.Init(sceneID);
	myCamera.GetTransform().SetPosition({ 0.0f, 0.0f, -10.f });
	myCamera.AddComponent<CCameraComponent>({ 60.f, (IWorld::GetWindowSize().x / IWorld::GetWindowSize().y), 0.1f, 1000.f });
	myCamera.GetComponent<CCameraComponent>()->SetAsActiveCamera();

	myParticle.Init(sceneID);
	myParticle.AddComponent<CParticleSystemComponent>();
	myParticle.GetComponent<CParticleSystemComponent>()->Stop();
	myParticleEmitter = myEngine.myParticleManager.myParticleEmitters.GetObj(myParticle.GetComponent<CParticleSystemComponent>()->myParticleEmitterID);

	myEngine.myGraphicsPipeline.myActiveRenderers[CGraphicsPipeline::ERenderer_Fullscreen] = false;
}

void CParticleEditor::UpdateCallback()
{
	CEditor::UpdateCallback();

	UpdateCameraControls();
	RenderDebugLines();
	if (!myHasFinishedLoading)
	{
		static int frameCount = 0;
		if (frameCount >= 1)
		{
			myHasFinishedLoading = true;
			frameCount = 0;
		}
		frameCount++;
	}
}

void CParticleEditor::SaveFile(const char * aFilePath)
{
	JsonDocument doc;
	doc.AddMember("myDuration", myParticleEmitter->GetDuration());
	doc.AddMember("myIsLoopable", myParticleEmitter->GetIsLoopable());
	doc.AddMember("mySpawnRate", myParticleEmitter->GetSpawnRate());
	doc.AddMember("myLifetime", myParticleEmitter->GetLifetime());

	JsonObject accelerationObject;
	CommonUtilities::Vector3f acceleration = myParticleEmitter->GetAcceleration();
	accelerationObject.AddMember("myX", acceleration.x);
	accelerationObject.AddMember("myY", acceleration.y);
	accelerationObject.AddMember("myZ", acceleration.z);
	doc.AddObject("myAcceleration", accelerationObject);

	JsonObject startVelocityObject;
	CommonUtilities::Vector3f startVelocity = myParticleEmitter->GetStartVelocity();
	startVelocityObject.AddMember("myX", startVelocity.x);
	startVelocityObject.AddMember("myY", startVelocity.y);
	startVelocityObject.AddMember("myZ", startVelocity.z);
	doc.AddObject("myStartVelocity", startVelocityObject);

	JsonObject startRotationObject;
	CommonUtilities::Vector2f startRotation = myParticleEmitter->GetStartRotation();
	startRotationObject.AddMember("myX", startRotation.x);
	startRotationObject.AddMember("myY", startRotation.y);
	doc.AddObject("myStartRotation", startRotationObject);

	JsonObject rotationVelocityObject;
	CommonUtilities::Vector2f rotationVelocity= myParticleEmitter->GetRotationVelocity();
	rotationVelocityObject.AddMember("myX", rotationVelocity.x);
	rotationVelocityObject.AddMember("myY", rotationVelocity.y);
	doc.AddObject("myRotationVelocity", rotationVelocityObject);

	doc.AddMember("myGravityModifier", myParticleEmitter->GetGravityModifier());

	JsonObject startColorObject;
	CommonUtilities::Vector4f startColor = myParticleEmitter->GetStartColor();
	startColorObject.AddMember("myR", startColor.x);
	startColorObject.AddMember("myG", startColor.y);
	startColorObject.AddMember("myB", startColor.z);
	startColorObject.AddMember("myA", startColor.w);
	doc.AddObject("myStartColor", startColorObject);

	JsonObject endColorObject;
	CommonUtilities::Vector4f endColor = myParticleEmitter->GetEndColor();
	endColorObject.AddMember("myR", endColor.x);
	endColorObject.AddMember("myG", endColor.y);
	endColorObject.AddMember("myB", endColor.z);
	endColorObject.AddMember("myA", endColor.w);
	doc.AddObject("myEndColor", endColorObject);

	JsonObject startSizeObject;
	CommonUtilities::Vector2f startSize = myParticleEmitter->GetStartSize();
	startSizeObject.AddMember("myX", startSize.x);
	startSizeObject.AddMember("myY", startSize.y);
	doc.AddObject("myStartSize", startSizeObject);

	JsonObject endSizeObject;
	CommonUtilities::Vector2f endSize = myParticleEmitter->GetEndSize();
	endSizeObject.AddMember("myX", endSize.x);
	endSizeObject.AddMember("myY", endSize.y);
	doc.AddObject("myEndSize", endSizeObject);

	doc.AddMember("myBlendState", static_cast<int>(myParticleEmitter->GetBlendState()));

	doc.AddString("myTexture", myParticleEmitter->GetTexturePath());

	JsonObject shapeData;
	shapeData.AddMember("myShapeType", static_cast<int>(myParticleEmitter->GetShapeData().myShapeType));

	if (myParticleEmitter->GetShapeData().myShapeType == CParticleEmitter::ShapeType::ESphere)
	{
		shapeData.AddMember("myRadius", myParticleEmitter->GetShapeData().myRadius);
		shapeData.AddMember("myRadiusThickness", myParticleEmitter->GetShapeData().myRadiusThickness);
	}
	else if (myParticleEmitter->GetShapeData().myShapeType == CParticleEmitter::ShapeType::EBox)
	{
		JsonObject boxSizeObject;
		CommonUtilities::Vector3f boxSize = myParticleEmitter->GetShapeData().myBoxSize;
		boxSizeObject.AddMember("myX", boxSize.x);
		boxSizeObject.AddMember("myY", boxSize.y);
		boxSizeObject.AddMember("myZ", boxSize.z);
		
		shapeData.AddObject("myBoxSize", boxSizeObject);
		shapeData.AddMember("myBoxThickness", myParticleEmitter->GetShapeData().myBoxThickness);
	}

	doc.AddObject("myShapeData", shapeData);

	doc.SaveFile(aFilePath);
}

void CParticleEditor::LoadFile(const char * aFilePath)
{
	myHasFinishedLoading = false;
	GENERAL_LOG(CONCOL_DEFAULT, "\n Loading File: %s \n", aFilePath);
	JsonDocument doc;
	doc.LoadFile(aFilePath);

	if (doc.Find("myDuration"))
	{
		float duration = doc["myDuration"].GetFloat();
		myParticleEmitter->SetDuration(duration);
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading Duration: %f \n", duration);

		bool isLoopable = doc["myIsLoopable"].GetBool();
		myParticleEmitter->SetIsLoopable(isLoopable);
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading Duration: %s \n", isLoopable ? "true" : "false");

		float spawnRate = doc["mySpawnRate"].GetFloat();
		myEngine.mySyncFunctionBuffer.Add([=] {
			myParticleEmitter->SetSpawnRate(spawnRate);
		});
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading SpawnRate: %f \n", spawnRate);

		float lifetime = doc["myLifetime"].GetFloat();
		myEngine.mySyncFunctionBuffer.Add([=] {
			myParticleEmitter->SetLifetime(lifetime);
		});
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading Lifetime: %f \n", lifetime);

		CommonUtilities::Vector3f acceleration;
		acceleration.x = doc["myAcceleration"]["myX"].GetFloat();
		acceleration.y = doc["myAcceleration"]["myY"].GetFloat();
		acceleration.z = doc["myAcceleration"]["myZ"].GetFloat();
		myParticleEmitter->SetAcceleration(acceleration);
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading Acceleration: x: %f, y: %f, z: %f \n", acceleration.x, acceleration.y, acceleration.z);

		CommonUtilities::Vector3f startVelocity;
		startVelocity.x = doc["myStartVelocity"]["myX"].GetFloat();
		startVelocity.y = doc["myStartVelocity"]["myY"].GetFloat();
		startVelocity.z = doc["myStartVelocity"]["myZ"].GetFloat();
		myParticleEmitter->SetStartVelocity(startVelocity);
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading StartVelocity: x: %f, y: %f, z: %f \n", startVelocity.x, startVelocity.y, startVelocity.z);

		CommonUtilities::Vector2f startRotation;
		startRotation.x = doc["myStartRotation"]["myX"].GetFloat();
		startRotation.y = doc["myStartRotation"]["myY"].GetFloat();
		myParticleEmitter->SetStartRotation(startRotation);
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading StartRotation: x: %f, y: %f, \n", startRotation.x, startRotation.y);

		CommonUtilities::Vector2f rotationVelocity;
		rotationVelocity.x = doc["myRotationVelocity"]["myX"].GetFloat();
		rotationVelocity.y = doc["myRotationVelocity"]["myY"].GetFloat();
		myParticleEmitter->SetRotationVelocity(rotationVelocity);
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading RotationVelocity: x: %f, y: %f, \n", rotationVelocity.x, rotationVelocity.y);

		float gravityModifier = doc["myGravityModifier"].GetFloat();
		myParticleEmitter->SetGravityModifier(gravityModifier);
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading GravityModifier %d \n", gravityModifier);

		CommonUtilities::Vector4f startColor;
		startColor.x = doc["myStartColor"]["myR"].GetFloat();
		startColor.y = doc["myStartColor"]["myG"].GetFloat();
		startColor.z = doc["myStartColor"]["myB"].GetFloat();
		startColor.w = doc["myStartColor"]["myA"].GetFloat();
		myParticleEmitter->SetStartColor(startColor);
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading StartColor: x: %f, y: %f, z: %f w: %f \n", startColor.x, startColor.y, startColor.z, startColor.w);

		CommonUtilities::Vector4f endColor;
		endColor.x = doc["myEndColor"]["myR"].GetFloat();
		endColor.y = doc["myEndColor"]["myG"].GetFloat();
		endColor.z = doc["myEndColor"]["myB"].GetFloat();
		endColor.w = doc["myEndColor"]["myA"].GetFloat();
		myParticleEmitter->SetEndColor(endColor);
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading EndColor: x: %f, y: %f, z: %f w: %f \n", endColor.x, endColor.y, endColor.z, endColor.w);

		CommonUtilities::Vector2f startSize;
		startSize.x = doc["myStartSize"]["myX"].GetFloat();
		startSize.y = doc["myStartSize"]["myY"].GetFloat();
		myParticleEmitter->SetStartSize(startSize);
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading StartSize: x: %f, y: %f \n", startSize.x, startSize.y);

		CommonUtilities::Vector2f endSize;
		endSize.x = doc["myEndSize"]["myX"].GetFloat();
		endSize.y = doc["myEndSize"]["myY"].GetFloat();
		myParticleEmitter->SetEndSize(endSize);
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading EndSize: x: %f, y: %f \n", endSize.x, endSize.y);

		std::string texture = doc["myTexture"].GetString();
		myEngine.mySyncFunctionBuffer.Add([=]() {
			myParticleEmitter->SetTexture(texture.c_str());
		});
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading Texture: %s \n", texture.c_str());

		int blendState = doc["myBlendState"].GetInt();
		myEngine.mySyncFunctionBuffer.Add([=]() {
			myParticleEmitter->SetBlendState(static_cast<CGraphicsStateManager::EBlendState>(blendState));
		});
		GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading BlendState: %d \n", blendState);

		if (doc.Find("myShapeData"))
		{
			CParticleEmitter::SParticleData::SShapeData shapeData;
			shapeData.myShapeType = static_cast<CParticleEmitter::ShapeType>(doc["myShapeData"]["myShapeType"].GetInt());

			if (shapeData.myShapeType == CParticleEmitter::ShapeType::ESphere)
			{
				shapeData.myRadius = doc["myShapeData"]["myRadius"].GetFloat();
				shapeData.myRadiusThickness = doc["myShapeData"]["myRadiusThickness"].GetFloat();
			}
			else if (shapeData.myShapeType == CParticleEmitter::ShapeType::EBox)
			{
				CommonUtilities::Vector3f boxSize;
				boxSize.x = doc["myShapeData"]["myBoxSize"]["myX"].GetFloat();
				boxSize.y = doc["myShapeData"]["myBoxSize"]["myY"].GetFloat();
				boxSize.z = doc["myShapeData"]["myBoxSize"]["myZ"].GetFloat();
				shapeData.myBoxSize = boxSize;
				shapeData.myBoxThickness = doc["myShapeData"]["myBoxThickness"].GetFloat();
			}
			GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading ShapeData: \n ShapeType: %d \n", shapeData.myShapeType);
			myParticleEmitter->SetShapeData(shapeData);
		}
		else
		{
			myParticleEmitter->GetShapeData().myShapeType = CParticleEmitter::ShapeType::EPoint;
		}
	}
	GENERAL_LOG(CONCOL_DEFAULT, "Finished Loading File: %s \n", aFilePath);
}

void CParticleEditor::UpdateCameraControls()
{
	Input::CInputManager input = IWorld::Input();

	CommonUtilities::Vector2f movement = input.GetMouseMovement();
	movement.x *= (2.f*CommonUtilities::Pif) / IWorld::GetWindowSize().x;
	movement.y *= (CommonUtilities::Pif) / IWorld::GetWindowSize().y;

	//if (input.IsKeyDown(Input::Key_Alt))
	{
		if (input.IsButtonDown(Input::Button_Left))
		{
			myRotation.x += movement.y;
			myRotation.y += movement.x;

			if (myRotation.x >= CommonUtilities::Pif / 2.f)
			{
				myRotation.x = CommonUtilities::Pif / 2.f - 0.001f;
			}
			if (myRotation.x <= -CommonUtilities::Pif / 2.f)
			{
				myRotation.x = -CommonUtilities::Pif / 2.f + 0.001f;
			}
		}

		if (input.IsButtonDown(Input::Button_Middle))
		{
			myPivot -= myCamera.GetTransform().GetRight() * movement.x * myZoom;
			myPivot += myCamera.GetTransform().GetUp() * movement.y * myZoom;
		}

		if (input.IsButtonDown(Input::Button_Right))
		{
			myZoom -= (movement.x + movement.y) * myZoom;
			if (myZoom < 0.1f)
			{
				myZoom = 0.1f;
				myPivot += myCamera.GetTransform().GetForward() * (movement.x + movement.y);
			}
		}
	}

	CommonUtilities::Vector3f newPos(0.f, 0.f, -myZoom);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundX(myRotation.x);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundY(myRotation.y);
	newPos += myPivot;

	myCamera.GetTransform().SetPosition(newPos);
	myCamera.GetTransform().LookAt(myPivot);
}

void CParticleEditor::RenderDebugLines()
{
	if (myShowDebugLines)
	{
		IWorld::SetDebugColor({ 1.f, 0.f, 0.f, 1.f });
		IWorld::DrawDebugLine({ 0.f, 0.f, 0.f }, { 1.f, 0.f, 0.f });
		IWorld::SetDebugColor({ 0.f, 1.f, 0.f, 1.f });
		IWorld::DrawDebugLine({ 0.f, 0.f, 0.f }, { 0.f, 1.f, 0.f });
		IWorld::SetDebugColor({ 0.f, 0.f, 1.f, 1.f });
		IWorld::DrawDebugLine({ 0.f, 0.f, 0.f }, { 0.f, 0.f, 1.f });

		if (myParticleEmitter->GetShapeData().myShapeType == CParticleEmitter::ShapeType::ESphere)
		{
			float scale = 2.f * myParticleEmitter->GetShapeData().myRadius;
			IWorld::SetDebugColor({ 1.f, 1.f, 0.f, 1.f });
			IWorld::DrawDebugWireSphere({ 0.f, 0.f, 0.f }, { scale, scale, scale });
		}
		else if (myParticleEmitter->GetShapeData().myShapeType == CParticleEmitter::ShapeType::EBox)
		{
			IWorld::SetDebugColor({ 1.f, 1.f, 0.f, 1.f });
			IWorld::DrawDebugWireCube({ 0.f, 0.f, 0.f }, 2.f* myParticleEmitter->GetShapeData().myBoxSize);
		}
	}
}

void CParticleEditor::SetStartRotation(CommonUtilities::Vector2f aStartRotation)
{
	myParticleEmitter->SetStartRotation(aStartRotation);
}

void CParticleEditor::SetRotationVelocity(CommonUtilities::Vector2f aRotationVelocity)
{
	myParticleEmitter->SetRotationVelocity(aRotationVelocity);
}

void CParticleEditor::SetGravityModifier(float aGravityModifier)
{
	myParticleEmitter->SetGravityModifier(aGravityModifier);
}

const float* CParticleEditor::GetStartRotation()
{
	return myParticleEmitter->GetStartRotation().pData;
}

const float* CParticleEditor::GetRotationVelocity()
{
	return myParticleEmitter->GetRotationVelocity().pData;
}

const float CParticleEditor::GetGravityModifier()
{
	return myParticleEmitter->GetGravityModifier();
}

void CParticleEditor::SetCubemapTexture(const char * aCubemapTexturePath)
{
	myEngine.mySyncFunctionBuffer.Add([=] {
		IEngine::GetSceneManager().GetActiveScene()->SetCubemap(aCubemapTexturePath);
		IEngine::GetSceneManager().GetActiveScene()->SetSkybox(aCubemapTexturePath);
	});
}

void CParticleEditor::ToggleDebugLines()
{
	myShowDebugLines = !myShowDebugLines;
}

bool CParticleEditor::IsPlaying()
{
	return myParticle.GetComponent<CParticleSystemComponent>()->IsPlaying();
}

void CParticleEditor::ResetCameraPivot()
{
	myPivot = { 0.0f, 0.0f, 0.0f };
}

void CParticleEditor::ResetCameraScale()
{
	myZoom = 20.f;
}

void CParticleEditor::ResetCameraRotation()
{
	myRotation = { 0.f, 0.f };
}

void CParticleEditor::ResetParticleToDefault()
{
	myEngine.mySyncFunctionBuffer.Add([=] {
		myParticleEmitter->ResetToDefault();
	});
}

void CParticleEditor::SetTexture(const char * aTexturePath)
{
	myEngine.mySyncFunctionBuffer.Add([=] {
		myParticleEmitter->SetTexture(aTexturePath);
	});
}

void CParticleEditor::SetSpawnRate(float aSpawnRate)
{
	myEngine.mySyncFunctionBuffer.Add([=] {
		myParticleEmitter->SetSpawnRate(aSpawnRate);
	});
}

void CParticleEditor::SetLifetime(float aLifetime)
{
	myEngine.mySyncFunctionBuffer.Add([=] {
		myParticleEmitter->SetLifetime(aLifetime);
	});
}

void CParticleEditor::SetDuration(float aDuration)
{
	myParticle.GetComponent<CParticleSystemComponent>()->Reset();
	myParticleEmitter->SetDuration(aDuration);
}

void CParticleEditor::SetIsLoopable(bool aIsLoopable)
{
	myParticleEmitter->SetIsLoopable(aIsLoopable);
	myParticle.GetComponent<CParticleSystemComponent>()->Reset();
}

void CParticleEditor::SetAcceleration(CommonUtilities::Vector3f aAcceleration)
{
	myParticleEmitter->SetAcceleration(aAcceleration);
}

void CParticleEditor::SetStartVelocity(CommonUtilities::Vector3f aStartVelocity)
{
	myParticleEmitter->SetStartVelocity(aStartVelocity);
}

void CParticleEditor::SetStartColor(CommonUtilities::Vector4f aStartColor)
{
	myParticleEmitter->SetStartColor(aStartColor);
}

void CParticleEditor::SetEndColor(CommonUtilities::Vector4f aEndColor)
{
	myParticleEmitter->SetEndColor(aEndColor);
}

void CParticleEditor::SetStartSize(CommonUtilities::Vector2f aStartSize)
{
	myParticleEmitter->SetStartSize(aStartSize);
}

void CParticleEditor::SetEndSize(CommonUtilities::Vector2f aEndSize)
{
	myParticleEmitter->SetEndSize(aEndSize);
}

const float CParticleEditor::GetDuration()
{
	return myParticleEmitter->GetDuration();
}

const char* CParticleEditor::GetTexturePath()
{
	return myParticleEmitter->GetTexturePath();
}

const float CParticleEditor::GetSpawnRate()
{
	return myParticleEmitter->GetSpawnRate();
}

const float CParticleEditor::GetLifeTime()
{
	return myParticleEmitter->GetLifetime();
}

const bool CParticleEditor::GetIsLoopable()
{
	return myParticleEmitter->GetIsLoopable();
}

const float* CParticleEditor::GetAcceleration()
{
	return myParticleEmitter->GetAcceleration().pData;
}

const float * CParticleEditor::GetStartVelocity()
{
	return myParticleEmitter->GetStartVelocity().pData;
}

const float* CParticleEditor::GetStartColor()
{
	return myParticleEmitter->GetStartColor().pData;
}

const float* CParticleEditor::GetEndColor()
{
	return myParticleEmitter->GetEndColor().pData;
}

const float* CParticleEditor::GetStartSize()
{
	return myParticleEmitter->GetStartSize().pData;
}

const float* CParticleEditor::GetEndSize()
{
	return myParticleEmitter->GetEndSize().pData;
}

void CParticleEditor::SetBlendState(int aBlendState)
{
	myEngine.mySyncFunctionBuffer.Add([=] {
		myParticleEmitter->SetBlendState(static_cast<CGraphicsStateManager::EBlendState>(aBlendState));
	});
}

int CParticleEditor::GetBlendState()
{
	return static_cast<int>(myParticleEmitter->GetBlendState());
}

void CParticleEditor::SetShapeType(int aIndex)
{
	myParticleEmitter->GetShapeData().myShapeType = static_cast<CParticleEmitter::ShapeType>(aIndex);
}

int CParticleEditor::GetShapeType()
{
	return static_cast<int>(myParticleEmitter->GetShapeData().myShapeType);
}

void CParticleEditor::SetSphereRadius(float aRadius)
{
	myParticleEmitter->GetShapeData().myRadius = aRadius;
}

void CParticleEditor::SetSphereRadiusThickness(float aRadiusThickness)
{
	myParticleEmitter->GetShapeData().myRadiusThickness = aRadiusThickness;
}

float CParticleEditor::GetSphereRadius()
{
	return myParticleEmitter->GetShapeData().myRadius;
}

float CParticleEditor::GetSphereRadiusThickness()
{
	return myParticleEmitter->GetShapeData().myRadiusThickness;
}

void CParticleEditor::SetBoxSize(CommonUtilities::Vector3f aBoxSize)
{
	myParticleEmitter->GetShapeData().myBoxSize = aBoxSize;
}

void CParticleEditor::SetBoxThickness(float aBoxThickness)
{
	myParticleEmitter->GetShapeData().myBoxThickness = aBoxThickness;
}

const float * CParticleEditor::GetBoxSize()
{
	return myParticleEmitter->GetShapeData().myBoxSize.pData;
}

const float CParticleEditor::GetBoxThickness()
{
	return myParticleEmitter->GetShapeData().myBoxThickness;
}

void CParticleEditor::ParticleSystemPlay()
{
	return myParticle.GetComponent<CParticleSystemComponent>()->Play();
}

void CParticleEditor::ParticleSystemStop()
{
	return myParticle.GetComponent<CParticleSystemComponent>()->Stop();
}

void CParticleEditor::ParticleSystemPause()
{
	return myParticle.GetComponent<CParticleSystemComponent>()->Pause();
}

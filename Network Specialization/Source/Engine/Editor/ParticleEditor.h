#pragma once

#include "Editor.h"
#include "State.h"
#include "GameObject.h"
#include "Macros.h"
#include <atomic>

class CParticleEditor : public CEditor
{
public:
	CParticleEditor();
	~CParticleEditor();

	virtual void InitCallback() override;
	virtual void UpdateCallback() override;

	bool GetHasFinishedLoading() { return myHasFinishedLoading; }

	void SaveFile(const char* aFilePath);
	void LoadFile(const char* aFilePath);

	void SetCubemapTexture(const char* aCubemapTexturePath);

	void ToggleDebugLines();
	bool IsPlaying();

	void ResetCameraPivot();
	void ResetCameraScale();
	void ResetCameraRotation();

	void ResetParticleToDefault();

	void ParticleSystemPlay();
	void ParticleSystemStop();
	void ParticleSystemPause();

	// -- GeneralData -- 

	//SYNCJOB
	void SetTexture(const char* aTexturePath);
	void SetSpawnRate(float aSpawnRate);
	void SetLifetime(float aLifetime);

	//SET
	void SetDuration(float aDuration);
	void SetIsLoopable(bool aIsLoopable);
	void SetAcceleration(CommonUtilities::Vector3f aAcceleration);
	void SetStartVelocity(CommonUtilities::Vector3f aStartVelocity);
	void SetStartRotation(CommonUtilities::Vector2f aStartRotation);
	void SetRotationVelocity(CommonUtilities::Vector2f aRotationVelocity);
	void SetGravityModifier(float aGravityModifier);
	void SetStartColor(CommonUtilities::Vector4f aStartColor);
	void SetEndColor(CommonUtilities::Vector4f aEndColor);
	void SetStartSize(CommonUtilities::Vector2f aStartSize);
	void SetEndSize(CommonUtilities::Vector2f aEndSize);

	//GET
	const float GetDuration();
	const char* GetTexturePath();
	const float GetSpawnRate();
	const float GetLifeTime();
	const bool GetIsLoopable();
	const float* GetAcceleration();
	const float* GetStartVelocity();
	const float* GetStartRotation();
	const float* GetRotationVelocity();
	const float GetGravityModifier();
	const float* GetStartColor();
	const float* GetEndColor();
	const float* GetStartSize();
	const float* GetEndSize();

	// -- BlendState --
	void SetBlendState(int aBlendState);
	int GetBlendState();


	// -- ShapeData --
	void SetShapeType(int aIndex);
	int GetShapeType();

	// -- SphereData --
	void SetSphereRadius(float aRadius);
	void SetSphereRadiusThickness(float aRadiusThickness);
	float GetSphereRadius();
	float GetSphereRadiusThickness();

	// -- BoxData --
	void SetBoxSize(CommonUtilities::Vector3f aBoxSize);
	void SetBoxThickness(float aBoxThickness);
	const float* GetBoxSize();
	const float GetBoxThickness();

private:
	void UpdateCameraControls();
	void RenderDebugLines();
	CGameObject myCamera;
	CGameObject myParticle;

	CParticleEmitter* myParticleEmitter;

	CommonUtilities::Vector3f myPivot;
	CommonUtilities::Vector2f myRotation;
	float myZoom;
	bool myShowDebugLines;
	bool myHasFinishedLoading;
};


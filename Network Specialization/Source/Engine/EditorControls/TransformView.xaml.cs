﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EditorControls
{
	/// <summary>
	/// Interaction logic for TransformControl.xaml
	/// </summary>
	public partial class TransformView : UserControl
	{
		public delegate void OnValueChanged(object sender, EditorClasses.Vector3 aVector);
		public event OnValueChanged OnPositionChanged;
		public event OnValueChanged OnRotationChanged;
		public event OnValueChanged OnScaleChanged;

		public TransformView()
		{
			InitializeComponent();

			myVector3Position.OnValueChange += MyPosition_OnValueChange;
			myVector3Rotation.OnValueChange += MyRotation_OnValueChange;
			myVector3Scale.OnValueChange += MyScale_OnValueChange;
		}

		public void SetPosition(EditorClasses.Vector3 aVector)
		{
			myVector3Position.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
			{
				myVector3Position.SetVector3(aVector);
			}));
		}
		public void SetRotation(EditorClasses.Vector3 aVector)
		{
			myVector3Position.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
			{
				myVector3Rotation.SetVector3(aVector);
			}));
		}
		public void SetScale(EditorClasses.Vector3 aVector)
		{
			myVector3Position.Dispatcher.Invoke(System.Windows.Threading.DispatcherPriority.Normal, new Action(() =>
			{
				myVector3Scale.SetVector3(aVector);
			}));
		}

		private void MyPosition_OnValueChange(object sender, EditorClasses.Vector3 aVector)
		{
			OnPositionChanged?.Invoke(this, aVector);
		}
		private void MyRotation_OnValueChange(object sender, EditorClasses.Vector3 aVector)
		{
			OnRotationChanged?.Invoke(this, aVector);
		}
		private void MyScale_OnValueChange(object sender, EditorClasses.Vector3 aVector)
		{
			OnScaleChanged?.Invoke(this, aVector);
		}
	}
}

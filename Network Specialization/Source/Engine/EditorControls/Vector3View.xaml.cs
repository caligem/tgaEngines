﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EditorControls
{
	/// <summary>
	/// Interaction logic for Vector3Control.xaml
	/// </summary>
	public partial class Vector3View : UserControl
	{
		public delegate void OnValueChangeEventHandler(object sender, EditorClasses.Vector3 aVector);
		public event OnValueChangeEventHandler OnValueChange;

		public Vector3View()
		{
			InitializeComponent();

            AddEventHandlers();
		}

        private void AddEventHandlers()
        {
			myX.ValueChanged += NumericUpDown_ValueChanged;
			myY.ValueChanged += NumericUpDown_ValueChanged;
			myZ.ValueChanged += NumericUpDown_ValueChanged;
        }
        private void RemoveEventHandlers()
        {
			myX.ValueChanged -= NumericUpDown_ValueChanged;
			myY.ValueChanged -= NumericUpDown_ValueChanged;
			myZ.ValueChanged -= NumericUpDown_ValueChanged;
        }

		public void SetVector3(float aX, float aY, float aZ)
		{
            RemoveEventHandlers();
			myX.Value = (double)(Math.Round(aX, 2));
			myY.Value = (double)(Math.Round(aY, 2));
			myZ.Value = (double)(Math.Round(aZ, 2));
            AddEventHandlers();
		}
		public void SetVector3(EditorClasses.Vector3 aVector)
		{
            SetVector3(aVector.x, aVector.y, aVector.z);
		}
		private void NumericUpDown_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double?> e)
		{
			EditorClasses.Vector3 retval = new EditorClasses.Vector3();

			if(myX.Value.HasValue)
				retval.x = (float)myX.Value.Value;
			if(myY.Value.HasValue)
				retval.y = (float)myY.Value.Value;
			if(myZ.Value.HasValue)
				retval.z = (float)myZ.Value.Value;

            OnValueChange?.Invoke(this, retval);
		}
	}
}

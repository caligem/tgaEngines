#pragma once

class CEditor;

#include "PostMasterMessageType.cs"

class CEditorProxyParticleEditor
{
public:
	CEditorProxyParticleEditor();
	~CEditorProxyParticleEditor();

	void Init(EEditorType aEditorType, void * aHandle);
	void Shutdown();
	void StartEngine();

	bool IsRunning();
	bool GetHasFinishedLoading();

	void SaveFile(const char* aFilePath);
	void LoadFile(const char* aFilePath);

	void GotFocus();
	void LostFocus();

	void SendWindowMessage(unsigned int Msg, void* wParam, void* lParam);

	void ToggleDebugLines();
	bool IsPlaying();

	void ResetCameraPivot();
	void ResetCameraScale();
	void ResetCameraRotation();

	void ResetParticleToDefault();

	void SetCubemapTexture(const char* aCubemapTexturePath);
	void ParticleSystemPlay();
	void ParticleSystemStop();
	void ParticleSystemPause();

	void SetTexture(const char* aTexturePath);
	void SetDuration(float aDuration);
	void SetSpawnRate(float aSpawnRate);
	void SetLifetime(float aLifetime);
	void SetIsLoopable(bool aIsLoopable);
	void SetAcceleration(float aAcceleration[3]);
	void SetStartVelocity(float aAcceleration[3]);
	void SetStartRotation(float aStartRotation[2]);
	void SetRotationVelocity(float aRotationVelocity[2]);
	void SetGravityModifier(float aGravityModifier);
	void SetStartColor(float aStartColor[4]);
	void SetEndColor(float aEndColor[4]);
	void SetStartSize(float aStartSize[2]);
	void SetEndSize(float aEndSize[2]);

	const float GetDuration();
	const char* GetTexturePath();
	const float GetSpawnRate();
	const float GetLifeTime();
	const bool GetIsLoopable();
	const float* GetAcceleration();
	const float* GetStartVelocity();
	const float* GetStartRotation();
	const float* GetRotationVelocity();
	const float GetGravityModifier();
	const float* GetStartColor();
	const float* GetEndColor();
	const float* GetStartSize();
	const float* GetEndSize();
	
	// -- BlendState --
	void SetBlendState(int aBlendState);
	int GetBlendState();

	// -- ShapeData --
	void SetShapeType(int aIndex);
	int GetShapeType();

	// -- SphereData --
	void SetSphereRadius(float aRadius);
	void SetSphereRadiusThickness(float aRadiusThickness);
	float GetSphereRadius();
	float GetSphereRadiusThickness();
	// -- BoxData --
	void SetBoxSize(float aBoxSize[3]);
	void SetBoxThickness(float aBoxThickness);
	const float* GetBoxSize();
	const float GetBoxThickness();

	void SendMessageToEngine(unsigned char* pData, EMessageType aMessageType, EDataType aDataType);
	void SetEditorMessageCallback(void(* aCallback)(const unsigned char*, EMessageType, EDataType));

private:
	CEditor* myEditor;

};


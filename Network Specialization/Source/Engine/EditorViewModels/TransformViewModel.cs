﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditorViewModels
{
	public class TransformViewModel
	{
		public Vector3Model myPosition { get; set; }
		public Vector3Model myRotation { get; set; }
		public Vector3Model myScale { get; set; }
	}
}

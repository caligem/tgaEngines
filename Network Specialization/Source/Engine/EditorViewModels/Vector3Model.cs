﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditorViewModels
{
	public class Vector3Model : Vector3ViewModel
	{
		public static Vector3Model Instance => new Vector3Model("Hello");

		public Vector3Model(string aLabel, float aX = 0f, float aY = 0f, float aZ = 0f)
		{
			myLabel = aLabel;
			myX = aX;
			myY = aY;
			myZ = aZ;
		}
	}
}

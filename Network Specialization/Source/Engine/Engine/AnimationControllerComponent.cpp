#include "stdafx.h"
#include "AnimationControllerComponent.h"
#include "ComponentSystem.h"
#include "IWorld.h"

CAnimationControllerComponent::CAnimationControllerComponent()
{
	myTimer = 0.f;
	myModelID = ID_T_INVALID(CModel);
	myAnimationIndex = 0;
	myDuration = 0.f;
	myTicksPerSeconds = 0.f;
	myIsLooping = true;
}

CAnimationControllerComponent::~CAnimationControllerComponent()
{
	
}

void CAnimationControllerComponent::SetAnimation(const std::string & aAnimation)
{
	if (aAnimation == myAnimation)
	{
		return;
	}

	myAnimation = aAnimation;
	myTimer = 0.f;
	myAnimationIndex = IEngine::GetModelManager().GetModel(myModelID)->myModelData.mySceneAnimator->GetAnimationIndexByName(aAnimation);
	myDuration = IEngine::GetModelManager().GetModel(myModelID)->myModelData.mySceneAnimator->GetAnimationDurationByIndex(myAnimationIndex);
	myTicksPerSeconds = IEngine::GetModelManager().GetModel(myModelID)->myModelData.mySceneAnimator->GetAnimationSpeedByIndex(myAnimationIndex);
}

void CAnimationControllerComponent::SetAnimationForced(const std::string & aAnimation)
{
	myAnimation = aAnimation;
	myTimer = 0.f;
	myAnimationIndex = IEngine::GetModelManager().GetModel(myModelID)->myModelData.mySceneAnimator->GetAnimationIndexByName(aAnimation);
	myDuration = IEngine::GetModelManager().GetModel(myModelID)->myModelData.mySceneAnimator->GetAnimationDurationByIndex(myAnimationIndex);
	myTicksPerSeconds = IEngine::GetModelManager().GetModel(myModelID)->myModelData.mySceneAnimator->GetAnimationSpeedByIndex(myAnimationIndex);
}

void CAnimationControllerComponent::Init(CModelComponent* aComponent)
{
	myTimer = 0.f;
	myDuration = 0.f;
	myTicksPerSeconds = 0.f;
	myIsLooping = true;
	myAnimationIndex = 0;
	myModelID = aComponent->myModelID;
	myAnimation = "idle";
	myDuration = IEngine::GetModelManager().GetModel(myModelID)->myModelData.mySceneAnimator->GetAnimationDurationByIndex(myAnimationIndex);
	myTicksPerSeconds = IEngine::GetModelManager().GetModel(myModelID)->myModelData.mySceneAnimator->GetAnimationSpeedByIndex(myAnimationIndex);
}

void CAnimationControllerComponent::Update()
{
	float dt = IWorld::Time().GetDeltaTime();
	
	if (!myIsLooping)
	{
		float duration = GetCurrentAnimationDuration();
		float currDuration = (myTimer + dt) * GetCurrentAnimationTicksPerSecond();
		if (currDuration >= duration)
		{
			return;
		}
	}
	myTimer += dt;
}

void CAnimationControllerComponent::Release()
{
	myModelID = ID_T_INVALID(CModel);
	myTimer = 0.f;
	myAnimation = "";
}
#pragma once
#include "Component.h"
#include "..\AudioManager\AudioChannels.h"

class AudioSource;

class CAudioSourceComponent : public CComponent
{
public:
	CAudioSourceComponent();
	~CAudioSourceComponent();

	struct SComponentData
	{
		CommonUtilities::Vector3f myPosition;
	};
	void Init(SComponentData aSAudioComponentData);
	void Update();
	void SetPosition(const CommonUtilities::Vector3f& aPosition);

	void LoadAudioEvent(const char* aAudioName, bool aRemoveAfterPlayback = false, AudioChannel aAudioChannel = AudioChannel::SoundEffects);

	void Stop(const char* aAudioName, bool aShouldBeImmediate = true);
	void Play(const char* aAudioName);
	void PlayNewInstance(const char* aAudioName, AudioChannel aAudioChannel = AudioChannel::SoundEffects);
	void UnloadAudioEvent();

private:
		AudioSource* myAudioSource;
		float myRange;
};


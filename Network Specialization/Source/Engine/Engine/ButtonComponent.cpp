#include "stdafx.h"
#include "ButtonComponent.h"
#include "GameObject.h"
#include "TextComponent.h"
#include "IEngine.h"
#include "ScriptManager.h"
#include "AudioManager.h"

CButtonComponent::CButtonComponent()
{
}


CButtonComponent::~CButtonComponent()
{
}

void CButtonComponent::Init(SComponentData aData)
{
	mySoundName = "";
	myHasClickSound = false;
	myIsDisabled = false;
	myIsPressed = false;
	myShouldBeRendered = true;

	CGameObject tempGameObject;
	tempGameObject.PointToObject(myGameObjectDataID, mySceneID);
	myTextComponent = tempGameObject.AddComponent<CTextComponent>({ aData.fontPath });
	mySpriteComponent = tempGameObject.AddComponent<CSpriteComponent>({ aData.spritePath });
	myTextComponent->SetPivot({ 0.5f, 0.5f });
	myTextComponent->SetTint({ 0.f, 0.f, 0.f, 1.f });
	mySpriteComponent->SetPivot({ 0.5f, 0.5f });
	SetBounds();
}

void CButtonComponent::SetIsDisabled(bool aDisable)
{
	myIsDisabled = aDisable;
	if (myIsDisabled)
	{
		mySpriteComponent->SetTint(myTints.myDisabledTint);
	}
	else
	{
		mySpriteComponent->SetTint(myTints.myNormalTint);
	}
}

void CButtonComponent::SetPosition(const CommonUtilities::Vector2f & aPosition)
{
	myPosition = aPosition;
	mySpriteComponent->SetPosition(myPosition);
	SetBounds();
}

void CButtonComponent::SetButtonTints(SButtonTints & aButtonTints)
{
	myTints = aButtonTints;
	mySpriteComponent->SetTint(myTints.myNormalTint);
}

void CButtonComponent::SetScaleRelativeToScreen(const CommonUtilities::Vector2f & aSize)
{
	mySizeRelativeToScreen = aSize;
	mySpriteComponent->SetScaleRelativeToScreen(mySizeRelativeToScreen);
	SetBounds();
}

void CButtonComponent::SetText(const char * aText)
{
	myTextComponent->SetText(aText);
}

void CButtonComponent::SetTextPosition(const CommonUtilities::Vector2f & aTextPosition)
{
	myTextComponent->SetPosition(myPosition + aTextPosition);
}

void CButtonComponent::SetTextSize(const int aSize)
{
	float size = static_cast<float>(aSize) / myEngineFontSize;
	myTextComponent->SetScale({size, size});
}


void CButtonComponent::SetTextTint(const CommonUtilities::Vector4f & aTextTint)
{
	myTextComponent->SetTint(aTextTint);
}

void CButtonComponent::SetSound(const std::string & aName)
{
	mySoundName = aName;
	myHasClickSound = true;
}

void CButtonComponent::FillRenderCommands(CDoubleBuffer<SSpriteRenderCommand>& aSpriteRenderCommandsBuffer, CDoubleBuffer<STextRenderCommand>& aTextRenderCommandsBuffer)
{
	if (myShouldBeRendered)
	{
		mySpriteComponent->FillRenderCommands(aSpriteRenderCommandsBuffer, aTextRenderCommandsBuffer);
		myTextComponent->FillRenderCommands(aSpriteRenderCommandsBuffer, aTextRenderCommandsBuffer);
	}
}

bool CButtonComponent::OnMouseUp(const CommonUtilities::Vector2f & aMousePosition)
{
	if (myIsDisabled) return false;

	if (myBounds.Contains(aMousePosition) && myIsPressed)
	{
		mySpriteComponent->SetTint(myTints.myHighlightedTint);
		if (myCanvasLuaScript == nullptr)
		{
			ENGINE_LOG(CONCOL_ERROR, "Lua Script not Set")
		}
		else
		{
			IEngine::GetScriptManager().CallFunction(*myCanvasLuaScript, "OnButtonClick", GetName().c_str());
		}
		myIsPressed = false;
		return true;
	}
	else
	{
		mySpriteComponent->SetTint(myTints.myNormalTint);
		myIsPressed = false;
		return false;
	}
}

void CButtonComponent::OnEnter()
{
	if (myIsDisabled) return;

	if (!myIsPressed)
	{
		mySpriteComponent->SetTint(myTints.myHighlightedTint);
	}
	else
	{
		mySpriteComponent->SetTint(myTints.myPressedTint);
	}
}

void CButtonComponent::OnLeave()
{
	if (myIsDisabled) return;

	if (!myIsPressed)
	{
		mySpriteComponent->SetTint(myTints.myNormalTint);
	}
	else
	{
		mySpriteComponent->SetTint(myTints.myHighlightedTint);
	}
}

void CButtonComponent::OnMouseDown(const CommonUtilities::Vector2f &)
{
	if (myIsDisabled) return;

	mySpriteComponent->SetTint(myTints.myPressedTint);
	myIsPressed = true;
	if (myHasClickSound)
	{
		AM.PlayNewInstance(mySoundName.c_str());
	}
}

void CButtonComponent::SetBounds()
{
	myBounds.SetMinMax(myPosition - mySizeRelativeToScreen / 2.f, myPosition + mySizeRelativeToScreen / 2.f);
}

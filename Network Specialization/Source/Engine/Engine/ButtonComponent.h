#pragma once
#include "Component.h"
#include "UIElement.h"

class CButtonComponent : public CComponent, public CUIElement
{
public:
	CButtonComponent();
	~CButtonComponent();

	struct SComponentData
	{
		const char* fontPath;
		const char* spritePath;
	};

	void Init(SComponentData aData);

	void SetIsDisabled(bool aDisable);
	const bool GetIsDisabled() const { return myIsDisabled; }

	void SetPosition(const CommonUtilities::Vector2f& aPosition);
	inline const CommonUtilities::Vector2f& GetPosition() const { return myPosition; }

	void SetButtonTints(SButtonTints& aButtonTints);

	void SetScaleRelativeToScreen(const CommonUtilities::Vector2f& aSize);

	void SetText(const char* aText);
	void SetTextPosition(const CommonUtilities::Vector2f& aTextPosition);
	void SetTextSize(const int aSize);
	void SetTextTint(const CommonUtilities::Vector4f& aTextTint);

	void SetSound(const std::string& aName);

	void FillRenderCommands(CDoubleBuffer<SSpriteRenderCommand>& aSpriteRenderCommandsBuffer, CDoubleBuffer<STextRenderCommand>& aTextRenderCommandsBuffer) override;

	bool OnMouseUp(const CommonUtilities::Vector2f& aMousePosition) override;
	void OnLeave() override;
	void OnEnter() override;
	void OnMouseDown(const CommonUtilities::Vector2f& /*aMousePosition*/) override;

private:
	void SetBounds();

	SButtonTints myTints;

	CommonUtilities::Vector2f mySizeRelativeToScreen;
	CommonUtilities::Vector2f myPosition;

	static constexpr int myEngineFontSize = 32;

	CSpriteComponent* mySpriteComponent;
	CTextComponent* myTextComponent;

	std::string mySoundName;

	bool myHasClickSound;
	bool myIsDisabled;
	bool myIsPressed;
};


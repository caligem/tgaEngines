#pragma once

#include "Matrix.h"
#include "Vector.h"
#include "ObjectPool.h"
#include "Component.h"

class CCamera;
class CDirectXFramework;
class CCameraManager;
class CGraphicsPipline;
class CComponentSystem;

class CCameraComponent : public CComponent
{
public:
	CCameraComponent();
	~CCameraComponent();

	struct SComponentData
	{
		float myCameraFovInDegrees;
		float myAspectRatio;
		float myNearPlane;
		float myFarPlane;
	};

	void Init(SComponentData aComponentData);

	void SetFov(float aFov);
	void SetAsActiveCamera();

private:
	friend CCamera;
	friend CEngine;
	friend CGraphicsPipeline;
	friend CComponentSystem;

	void Release() override;
	static CCameraManager* ourCameraManager;

	ID_T(CCamera) myCameraID;
};


#pragma once
class CComponentSystem;

#include "ObjectPool.h"
#include "GameObjectData.h"
#include "ModelComponent.h"
#include "AnimationControllerComponent.h"
#include "CameraComponent.h"
#include "AudioSourceComponent.h"
#include "AudioListenerComponent.h"
#include "LightComponent.h"
#include "ParticleSystemComponent.h"
#include "VolumetricFogComponent.h"
#include "StreakComponent.h"
#include "SpriteComponent.h"
#include "TextComponent.h"
#include "ButtonComponent.h"
#include "CanvasComponent.h"
#include "ScriptComponent.h"
#include "SliderComponent.h"

class CComponentStorage
{
public:
	CComponentStorage();
	~CComponentStorage();

	void PrintStorageInfo();

private:
	friend CComponentSystem;

	template<typename T>
	void* GetObjectPool();

	ObjectPool<CGameObjectData> myGameObjectData;
	
#include "RedefineComponentRegistrer.h"
#define ComponentRegister(Type, Container, Size) ObjectPool<##Type> ##Container;
#include "RegisteredComponents.h"
};

template<typename T>
inline void* CComponentStorage::GetObjectPool()
{
#include "RedefineComponentRegistrer.h"
#define ComponentRegister(Type, Container, Size) \
else if (_UUID(T) == _UUID(##Type)) \
{ \
	return reinterpret_cast<void*>(&(##Container)); \
} \

	if (false) {} // beautiful hack
#include "RegisteredComponents.h"
	else
	{
		ENGINE_LOG(CONCOL_WARNING, "type T does not match any Component // CComponentStorage::GetObjectPool()");
		return nullptr;
	}
}

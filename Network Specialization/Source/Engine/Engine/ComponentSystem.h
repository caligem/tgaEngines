#pragma once

class CDirectXFramework;
class CModel;
class CModelManager;

#include "ObjectPool.h"
#include "ComponentStorage.h"
#include "IEngine.h"
#include "Scene.h"
#include <array>
#include <map>
#include <vector>
#include "SceneManager.h"

class CComponentSystem
{
public:
	CComponentSystem();
	~CComponentSystem();

	bool Init();
	void UpdateComponents();
	void RunDestroys();
	void PrintStorageInfos();

private:
	friend class CGameObjectData;
	friend class CGameObject;
	friend class CTransform;
	friend class CSceneManager;
	friend class CScene;
	friend class CGraphicsPipeline;
	friend C2DRenderer;
	friend class CCanvasComponent;

	struct SDestroyGameObjectData
	{
		SDestroyGameObjectData() {};

		SDestroyGameObjectData(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID)
		{
			gameObjectDataID = aGameObjectDataID;
			sceneID = aSceneID;
		}

		inline bool const operator==(const SDestroyGameObjectData& aRhs)
		{
			return gameObjectDataID == aRhs.gameObjectDataID && sceneID == aRhs.sceneID;
		}

		ID_T(CGameObjectData) gameObjectDataID;
		ID_T(CScene) sceneID;
	};

	struct SDestroyComponent
	{
		_UUID_T componentType;
		int componentID;
		int sceneID;
	};

	void UpdateAudioComponents(ID_T(CScene) aSceneID);
	void UpdateParticleComponents(ID_T(CScene) aSceneID);
	void UpdateStreaksComponents(ID_T(CScene) aSceneID);
	void UpdateCanvasComponents(ID_T(CScene) aSceneID);
	void UpdateScriptComponents(ID_T(CScene) aSceneID);
	void UpdateAnimationComponents(ID_T(CScene) aSceneID);

	void AcquireComponentStorage(ID_T(CScene) aSceneID);
	void ReleaseSceneObjectsAndComponentStorage(ID_T(CScene) aSceneID);


	ID_T(CComponentStorage) GetComponentStorageIDBasedOnSceneID(ID_T(CScene) aSceneID);
	CComponentStorage* GetComponentStorageBasedOnSceneID(ID_T(CScene) aSceneID);

	template<typename T, typename ...Args>
	T* AddComponent(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID, Args ...aArgs);
	template<typename T>
	T* AddComponent(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID, const typename T::SComponentData& aComponentData);
	template<typename T>
	T* AddComponent(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID);

	template<typename T>
	T* GetComponent(ID_T(CGameObjectData) aGameObjectData, ID_T(CScene) aSceneID);
	template<typename T>
	T* GetComponent(int aComponentID, ID_T(CScene) aSceneID);

	template<typename T>
	void FillComponentPtrBuffer(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID);
	template<typename T>
	CommonUtilities::GrowingArray<T*> GetComponents(ID_T(CGameObjectData) aGameObjectData, ID_T(CScene) aSceneID);

	template<typename T>
	bool RemoveComponent(T* aComponent);

	ID_T(CGameObjectData) GetGameObjectIDByAccessID(int aAccessID, ID_T(CScene) aSceneID);

	CGameObjectData* GetGameObjectData(ID_T(CGameObjectData) aGameObjectDataIDptr, ID_T(CScene) aSceneID);
	ID_T(CGameObjectData) CreateGameObjectData(ID_T(CScene) aSceneID, int aAccessID);

	void DestroyGameObject(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID);
	
	void DestroyGameObjectsInQueue();
	void DestroyComponentsInQueue();
	ObjectPool<CComponentStorage> myComponentStorages;
	std::array<ID_T(CComponentStorage), 8> myComponentStorageConnections;

	CommonUtilities::GrowingArray<SDestroyGameObjectData> myGameObjectsToBeDestroyed;
	CommonUtilities::GrowingArray<SDestroyComponent> myComponentsToBeDestroyed;

	//Growing Arrays for GetComponents()
	CommonUtilities::GrowingArray<CComponent*> myComponentPtrBuffer;
	CommonUtilities::GrowingArray<int> myComponentIDBuffer;
};

template<typename T, typename ...Args>
inline T * CComponentSystem::AddComponent(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID, Args ...aArgs)
{
	CGameObjectData* gameObjectData = GetGameObjectData(aGameObjectDataID, aSceneID);
	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aSceneID);
	CScene* scene = IEngine::GetSceneManager().GetSceneAt(gameObjectData->mySceneID);

	CComponent* component = nullptr;

	ObjectPool<T>* componentPool = reinterpret_cast<ObjectPool<T>*>(componentStorage->GetObjectPool<T>());

	if(componentPool == nullptr)
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Trying to add an unregistered component type (%s)!", typeid(T).name());
		return nullptr;
	}

	ID_T(T) componentID = componentPool->Acquire();
	component = componentPool->GetObj(componentID);
	scene->AddComponentID(_UUID(T), componentID.val);

	T* tPtr = static_cast<T*>(component);
	tPtr->SetParent(aGameObjectDataID, aSceneID, componentID.val, gameObjectData);
	tPtr->Init(aArgs...);
	GetGameObjectData(aGameObjectDataID, aSceneID)->AddSComponent(componentID.val, _UUID(T));

	return tPtr;
}

template<typename T>
inline T* CComponentSystem::AddComponent(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID, const typename T::SComponentData& aComponentData)
{
	CGameObjectData* gameObjectData = GetGameObjectData(aGameObjectDataID, aSceneID);
	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aSceneID);
	CScene* scene = IEngine::GetSceneManager().GetSceneAt(gameObjectData->mySceneID);

	CComponent* component = nullptr;

	ObjectPool<T>* componentPool = reinterpret_cast<ObjectPool<T>*>(componentStorage->GetObjectPool<T>());

	if(componentPool == nullptr)
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Trying to add an unregistered component type (%s)!", typeid(T).name());
		return nullptr;
	}

	ID_T(T) componentID = componentPool->Acquire();
	component = componentPool->GetObj(componentID);
	scene->AddComponentID(_UUID(T), componentID.val);

	GetGameObjectData(aGameObjectDataID, aSceneID)->AddSComponent(componentID.val, _UUID(T));

	T* tPtr = static_cast<T*>(component);
	tPtr->SetParent(aGameObjectDataID, aSceneID, componentID.val, gameObjectData);
	tPtr->Init(aComponentData);

	return tPtr;
}

template<typename T>
inline T* CComponentSystem::AddComponent(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID)
{
	CGameObjectData* gameObjectData = GetGameObjectData(aGameObjectDataID, aSceneID);
	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aSceneID);
	if (componentStorage == nullptr) return nullptr;

	CScene* scene = IEngine::GetSceneManager().GetSceneAt(gameObjectData->mySceneID);

	CComponent* component = nullptr;

	ObjectPool<T>* componentPool = reinterpret_cast<ObjectPool<T>*>(componentStorage->GetObjectPool<T>());

	if (componentPool == nullptr)
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Trying to add an unregistered component type (%s)!", typeid(T).name());
		return nullptr;
	}

	ID_T(T) componentID = componentPool->Acquire();
	component = componentPool->GetObj(componentID);
	scene->AddComponentID(_UUID(T), componentID.val);

	GetGameObjectData(aGameObjectDataID, aSceneID)->AddSComponent(componentID.val, _UUID(T));

	T* tPtr = static_cast<T*>(component);
	tPtr->SetParent(aGameObjectDataID, aSceneID, componentID.val, gameObjectData);
	tPtr->Init();

	return tPtr;
}

template<typename T>
inline T* CComponentSystem::GetComponent(ID_T(CGameObjectData) aGameObjectData, ID_T(CScene) aSceneID)
{
	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aSceneID);
	if (componentStorage == nullptr) return nullptr;

	ObjectPool<T>* componentPool = reinterpret_cast<ObjectPool<T>*>(componentStorage->GetObjectPool<T>());

	if (componentPool == nullptr)
	{
		ENGINE_LOG(CONCOL_WARNING, "type T does not match any Component // CComponentSystem::GetComponent()");
		return nullptr;
	}

	int id = componentStorage->myGameObjectData.GetObj(aGameObjectData)->GetComponentID<T>();
	ID_T(T) componentID = ID_T(T)(id);

	if (componentID == ID_T_INVALID(T))
	{
		return nullptr;
	}

	return static_cast<T*>(componentPool->GetObj(componentID));
}

template<typename T>
inline T * CComponentSystem::GetComponent(int aComponentID, ID_T(CScene) aSceneID)
{
	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aSceneID);
	if (componentStorage == nullptr) return nullptr;

	ObjectPool<T>* componentPool = reinterpret_cast<ObjectPool<T>*>(componentStorage->GetObjectPool<T>());

	if (componentPool == nullptr)
	{
		ENGINE_LOG(CONCOL_WARNING, "type T does not match any Component // CComponentSystem::GetComponent<%s>()", typeid(T).name());
		return nullptr;
	}

	return static_cast<T*>(componentPool->GetObj(aComponentID));
}



template<typename T>
inline void CComponentSystem::FillComponentPtrBuffer(ID_T(CGameObjectData) aGameObjectDataID, ID_T(CScene) aSceneID)
{
	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aSceneID);
	
	myComponentIDBuffer.RemoveAll();
	componentStorage->myGameObjectData.GetObj(aGameObjectDataID)->FillComponentIDBuffer<T>(myComponentIDBuffer);

	myComponentPtrBuffer.RemoveAll();

	ObjectPool<T>* componentPool = reinterpret_cast<ObjectPool<T>*>(componentStorage->GetObjectPool<T>());
	if (componentPool == nullptr)
	{
		return;
	}

	for (int id : myComponentIDBuffer)
	{
		myComponentPtrBuffer.Add(componentPool->GetObj(ID_T(T)(id)));
	}
}

template<typename T>
inline CommonUtilities::GrowingArray<T*> CComponentSystem::GetComponents(ID_T(CGameObjectData) aGameObjectData, ID_T(CScene) aSceneID)
{
	static CommonUtilities::GrowingArray<T*> specificTypeBuffer(2);

	specificTypeBuffer.RemoveAll();

	FillComponentPtrBuffer<T>(aGameObjectData, aSceneID);

	for (CComponent* component : myComponentPtrBuffer)
	{
		specificTypeBuffer.Add(static_cast<T*>(component));
	}

	return specificTypeBuffer;
}

template<typename T>
inline bool CComponentSystem::RemoveComponent(T* aComponent)
{
	SDestroyComponent data;
	data.componentType = _UUID(T);
	data.componentID = aComponent->myID;
	data.sceneID = aComponent->mySceneID.val;

	CComponentStorage* componentStorage = GetComponentStorageBasedOnSceneID(aComponent->mySceneID);

	if (!componentStorage->myGameObjectData.GetObj(aComponent->myGameObjectDataID)->RemoveSComponent(data.componentType, data.componentID))
	{
		return false;
	};

	IEngine::GetSceneManager().GetSceneAt(aComponent->mySceneID)->RemoveComponentID(data.componentType, data.componentID);

	myComponentsToBeDestroyed.Add(data);
	return true;
}

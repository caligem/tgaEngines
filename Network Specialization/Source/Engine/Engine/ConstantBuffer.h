#pragma once

struct ID3D11Buffer;

enum EShaderType
{
	EShaderType_Pixel,
	EShaderType_Vertex,
	EShaderType_Geometry
};

class CConstantBuffer
{
public:
	CConstantBuffer();
	~CConstantBuffer();

	bool Init(int aBufferSize);
	void SetData(const void* aDataPtr, int aSize = -1);
	void SetBuffer(int aSlot, EShaderType aShaderType);

private:
	ID3D11Buffer* myBuffer;

	int myBufferSize;
};


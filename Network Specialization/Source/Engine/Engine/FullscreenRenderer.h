#pragma once

#include <array>

#include "VertexDataWrapper.h"
#include "ConstantBuffer.h"

class CFullscreenTexture;
class CVertexShader;
class CPixelShader;

class CFullscreenRenderer
{
public:
	CFullscreenRenderer();
	~CFullscreenRenderer();

	enum EEffect
	{
		EEffect_Copy,
		EEffect_BloomLuminance,
		EEffect_GaussianBlurHorizontal,
		EEffect_GaussianBlurVertical,
		EEffect_BloomAdd,
		EEffect_FXAA,
		EEffect_ColorGrading,
		EEffect_SSAO,
		EEffect_Fog,
		EEffect_Fade,
		EEffect_Count
	};

	bool Init();
	void Render(EEffect aEffect, std::array<CFullscreenTexture*, 2> aFullscreenTextures);
	void Render(EEffect aEffect);
	void SetResources(ID3D11ShaderResourceView** aShaderResourceViews, unsigned int aCount, unsigned int aSlot);

private:
	struct SFullscreenTextureBufferData
	{
		CommonUtilities::Vector2f myTexelSize;
		CommonUtilities::Vector2f myPadding;
	};

	SVertexDataWrapper myVertexData;
	CVertexShader* myVertexShader;
	std::array<CPixelShader*, EEffect_Count> myPixelShaders;

	CConstantBuffer myFullscreenTextureBuffer;

};


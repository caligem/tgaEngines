#include "stdafx.h"
#include "FullscreenTexture.h"

#include <DL_Debug.h>
#include <d3d11.h>

#include "IEngine.h"
#include "DXMacros.h"

CFullscreenTexture::CFullscreenTexture()
{
	myDepthStencil = nullptr;
	myViewport = nullptr;
	myNumTargets = 0;
}

CFullscreenTexture::~CFullscreenTexture()
{
	if (!myIsInitFromTexture)
	{
		for (auto* texture : myTextures)
		{
			SafeRelease(texture);
		}
	}
	for (auto* srv : myShaderResources)
	{
		SafeRelease(srv);
	}

	for (auto* rtv : myRenderTargets)
	{
		SafeRelease(rtv);
	}
	
	SafeRelease(myDepthStencil);

	delete myViewport;
	myViewport = nullptr;
}

bool CFullscreenTexture::Init(const CommonUtilities::Vector2f & aSize, int aNumberOfTextures, DXGI_FORMAT aFormats[MaxSize], bool aCreateDepth)
{
	myNumTargets = aNumberOfTextures;

	HRESULT result;

	D3D11_TEXTURE2D_DESC textureDesc = {};
	textureDesc.Width = static_cast<unsigned int>(aSize.x);
	textureDesc.Height = static_cast<unsigned int>(aSize.y);
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 1;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = 0;

	for (int i = 0; i < aNumberOfTextures; ++i)
	{
		//textureDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		textureDesc.Format = aFormats[i];

		result = IEngine::GetDevice()->CreateTexture2D(&textureDesc, nullptr, &myTextures[i]);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to create Texture2D in CFullScreenTexture::Init!");
			return false;
		}

		result = IEngine::GetDevice()->CreateRenderTargetView(myTextures[i], nullptr, &myRenderTargets[i]);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to create RenderTargetView in CFullScreenTexture::Init!");
			return false;
		}

		result = IEngine::GetDevice()->CreateShaderResourceView(myTextures[i], NULL, &myShaderResources[i]);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to create shaderResourceView in CFullScreenTexture::Init!");
			return false;
		}

	}

	if (aCreateDepth)
	{
		ID3D11Texture2D* backDepthTexture;
		D3D11_TEXTURE2D_DESC backDepthTextureDesc = {};
		backDepthTextureDesc.Width = static_cast<unsigned int>(aSize.x);
		backDepthTextureDesc.Height = static_cast<unsigned int>(aSize.y);
		backDepthTextureDesc.MipLevels = 1;
		backDepthTextureDesc.ArraySize = 1;
		backDepthTextureDesc.Format = DXGI_FORMAT_D32_FLOAT;
		backDepthTextureDesc.SampleDesc.Count = 1;
		backDepthTextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		
		result = IEngine::GetDevice()->CreateTexture2D(&backDepthTextureDesc, nullptr, &backDepthTexture);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create Texture2D for DepthStencilView in CFullscreenTexture!");
			return false;
		}

		result = IEngine::GetDevice()->CreateDepthStencilView(backDepthTexture, nullptr, &myDepthStencil);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create DepthStencilView in CFullscreenTexture!");
			return false;
		}
	}
	
	myViewport = new D3D11_VIEWPORT();
	myViewport->TopLeftX = 0.f;
	myViewport->TopLeftY = 0.f;
	myViewport->Width = static_cast<float>(aSize.x);
	myViewport->Height = static_cast<float>(aSize.y);
	myViewport->MinDepth = 0.f;
	myViewport->MaxDepth = 1.0f;

	mySize = aSize;

	GENERAL_LOG(CONCOL_VALID, "Created viewport at: %d x %d", static_cast<int>(myViewport->Width), static_cast<int>(myViewport->Height));

	return true;
}

bool CFullscreenTexture::Init(const CommonUtilities::Vector2f & aSize, DXGI_FORMAT aFormat, bool aCreateDepth)
{
	DXGI_FORMAT formats[MaxSize] = { aFormat };
	return Init(aSize, 1, formats, aCreateDepth);
}

bool CFullscreenTexture::InitCube(const CommonUtilities::Vector2f & aSize, DXGI_FORMAT aFormat, bool aCreateDepth)
{
	myIsCubemap = true;

	HRESULT result;

	D3D11_TEXTURE2D_DESC textureDesc = {};
	textureDesc.Width = static_cast<unsigned int>(aSize.x);
	textureDesc.Height = static_cast<unsigned int>(aSize.y);
	textureDesc.MipLevels = 1;
	textureDesc.ArraySize = 6;
	textureDesc.SampleDesc.Count = 1;
	textureDesc.SampleDesc.Quality = 0;
	textureDesc.Usage = D3D11_USAGE_DEFAULT;
	textureDesc.BindFlags = D3D11_BIND_RENDER_TARGET | D3D11_BIND_SHADER_RESOURCE;
	textureDesc.CPUAccessFlags = 0;
	textureDesc.MiscFlags = D3D11_RESOURCE_MISC_TEXTURECUBE;

	textureDesc.Format = aFormat;

	result = IEngine::GetDevice()->CreateTexture2D(&textureDesc, nullptr, &myTextures[0]);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to create Texture2D in CFullScreenTexture::InitCube!");
		return false;
	}

	D3D11_RENDER_TARGET_VIEW_DESC rtvDesc = {};
	rtvDesc.Format = textureDesc.Format;
	rtvDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DARRAY;
	rtvDesc.Texture2DArray.ArraySize = 1;
	rtvDesc.Texture2DArray.MipSlice = 0;
	myNumTargets = 6;
	for (int i = 0; i < 6; ++i)
	{
		rtvDesc.Texture2DArray.FirstArraySlice = i;
		result = IEngine::GetDevice()->CreateRenderTargetView(myTextures[0], &rtvDesc, &myRenderTargets[i]);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to create RenderTargetView in CFullScreenTexture::InitCube!");
			return false;
		}
	}

	D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Format = textureDesc.Format;
	srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
	srvDesc.TextureCube.MipLevels = textureDesc.MipLevels;
	srvDesc.TextureCube.MostDetailedMip = 0;
	result = IEngine::GetDevice()->CreateShaderResourceView(myTextures[0], &srvDesc, &myShaderResources[0]);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to create shaderResourceView in CFullScreenTexture::InitCube!");
		return false;
	}

	if (aCreateDepth)
	{
		ID3D11Texture2D* backDepthTexture;
		D3D11_TEXTURE2D_DESC backDepthTextureDesc = {};
		backDepthTextureDesc.Width = static_cast<unsigned int>(aSize.x);
		backDepthTextureDesc.Height = static_cast<unsigned int>(aSize.y);
		backDepthTextureDesc.MipLevels = 1;
		backDepthTextureDesc.ArraySize = 1;
		backDepthTextureDesc.Format = DXGI_FORMAT_D32_FLOAT;
		backDepthTextureDesc.SampleDesc.Count = 1;
		backDepthTextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		
		result = IEngine::GetDevice()->CreateTexture2D(&backDepthTextureDesc, nullptr, &backDepthTexture);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create Texture2D for DepthStencilView in CFullscreenTexture!");
			return false;
		}

		result = IEngine::GetDevice()->CreateDepthStencilView(backDepthTexture, nullptr, &myDepthStencil);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create DepthStencilView in CFullscreenTexture!");
			return false;
		}
	}
	
	myViewport = new D3D11_VIEWPORT();
	myViewport->TopLeftX = 0.f;
	myViewport->TopLeftY = 0.f;
	myViewport->Width = static_cast<float>(aSize.x);
	myViewport->Height = static_cast<float>(aSize.y);
	myViewport->MinDepth = 0.f;
	myViewport->MaxDepth = 1.0f;

	mySize = aSize;

	GENERAL_LOG(CONCOL_VALID, "Created viewport at: %d x %d", static_cast<int>(myViewport->Width), static_cast<int>(myViewport->Height));

	return true;
}

bool CFullscreenTexture::Init(ID3D11Texture2D * aTexture, bool aCreateDepth)
{
	myIsInitFromTexture = true;
	myNumTargets = 1;

	HRESULT result;

	myTextures[0] = aTexture;

	result = IEngine::GetDevice()->CreateRenderTargetView(myTextures[0], nullptr, &myRenderTargets[0]);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to create RenderTargetView in CFullScreenTexture::Init!");
		return false;
	}

	D3D11_TEXTURE2D_DESC textureDesc;
	myTextures[0]->GetDesc(&textureDesc);

	if (aCreateDepth)
	{
		ID3D11Texture2D* backDepthTexture;
		D3D11_TEXTURE2D_DESC backDepthTextureDesc = {};
		backDepthTextureDesc.Width = textureDesc.Width;
		backDepthTextureDesc.Height = textureDesc.Height;
		backDepthTextureDesc.MipLevels = 1;
		backDepthTextureDesc.ArraySize = 1;
		backDepthTextureDesc.Format = DXGI_FORMAT_D32_FLOAT;
		backDepthTextureDesc.SampleDesc.Count = 1;
		backDepthTextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		
		result = IEngine::GetDevice()->CreateTexture2D(&backDepthTextureDesc, nullptr, &backDepthTexture);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create Texture2D for DepthStencilView in CFullscreenTexture!");
			return false;
		}

		result = IEngine::GetDevice()->CreateDepthStencilView(backDepthTexture, nullptr, &myDepthStencil);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create DepthStencilView in CFullscreenTexture!");
			return false;
		}
	}

	myViewport = new D3D11_VIEWPORT();
	myViewport->TopLeftX = 0.f;
	myViewport->TopLeftY = 0.f;
	myViewport->Width = static_cast<float>(textureDesc.Width);
	myViewport->Height = static_cast<float>(textureDesc.Height);
	myViewport->MinDepth = 0.f;
	myViewport->MaxDepth = 1.0f;

	mySize = { myViewport->Width, myViewport->Height };

	ENGINE_LOG(CONCOL_VALID, "Created viewport at: %d x %d", static_cast<int>(myViewport->Width), static_cast<int>(myViewport->Height));

	return true;
}

void CFullscreenTexture::ClearDepth()
{
	if(myDepthStencil)IEngine::GetContext()->ClearDepthStencilView(myDepthStencil, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.f, 0);
}

void CFullscreenTexture::ClearTexture(const CommonUtilities::Vector4f & aClearColor)
{
	for (int i = 0; i < myNumTargets; ++i)
	{
		IEngine::GetContext()->ClearRenderTargetView(myRenderTargets[i], aClearColor.pData);
	}
	if(myDepthStencil)IEngine::GetContext()->ClearDepthStencilView(myDepthStencil, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.f, 0);
}

void CFullscreenTexture::ClearTextureAt(int aIndex, const CommonUtilities::Vector4f & aClearColor)
{
	IEngine::GetContext()->ClearRenderTargetView(myRenderTargets[aIndex], aClearColor.pData);
}

void CFullscreenTexture::SetAsActiveTarget(ID3D11DepthStencilView * aDepthStencil)
{
	static ID3D11ShaderResourceView* nullResources[8] = { NULL };
	IEngine::GetContext()->PSSetShaderResources(0, 8, nullResources);

	IEngine::GetContext()->RSSetViewports(1, myViewport);
	if (aDepthStencil)
	{
		IEngine::GetContext()->OMSetRenderTargets(myNumTargets, myRenderTargets, aDepthStencil);
	}
	else
	{
		IEngine::GetContext()->OMSetRenderTargets(myNumTargets, myRenderTargets, myDepthStencil);
	}
}

void CFullscreenTexture::SetTextureAsActiveTarget(unsigned int aIndex, ID3D11DepthStencilView * aDepthStencil)
{
	IEngine::GetContext()->RSSetViewports(1, myViewport);
	if (aDepthStencil)
	{
		IEngine::GetContext()->OMSetRenderTargets(1, &myRenderTargets[aIndex], aDepthStencil);
	}
	else
	{
		IEngine::GetContext()->OMSetRenderTargets(1, &myRenderTargets[aIndex], myDepthStencil);
	}
}

void CFullscreenTexture::SetTextureAsResourceOnSlot(unsigned int aSlot, unsigned int aIndex)
{
	IEngine::GetContext()->PSSetShaderResources(aSlot, 1, &myShaderResources[aIndex]);
}

void CFullscreenTexture::SetAsResourceOnSlot(unsigned int aSlot)
{
	if (myIsCubemap)
	{
		IEngine::GetContext()->PSSetShaderResources(aSlot, 1, myShaderResources);
	}
	else
	{
		IEngine::GetContext()->PSSetShaderResources(aSlot, myNumTargets, myShaderResources);
	}
}

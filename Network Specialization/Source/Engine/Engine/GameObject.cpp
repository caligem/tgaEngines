#include "stdafx.h"
#include "GameObject.h"

#include "SceneManager.h"

CComponentSystem* CGameObject::ourComponentSystem = nullptr;

CGameObject::CGameObject()
{
}

CGameObject::~CGameObject()
{
}

void CGameObject::Init(ID_T(CScene) aSceneID, int aAccessID, ID_T(CGameObjectData) aParentID)
{
	mySceneID = aSceneID;
	myGameObjectDataID = ourComponentSystem->CreateGameObjectData(aSceneID, aAccessID);

	if (aParentID != ID_T_INVALID(CGameObjectData))
	{
		GetTransform().SetParent(&ourComponentSystem->GetGameObjectData(aParentID, mySceneID)->GetTransform());
	}

	AssignDebugPtr(CGameObjectData, ourComponentSystem->GetGameObjectData(myGameObjectDataID, aSceneID));
	AssignDebugPtr(CScene, IEngine::GetSceneManager().GetSceneAt(aSceneID));
}

void CGameObject::PointToObject(const CGameObject & aGameObject)
{
	myGameObjectDataID = aGameObject.myGameObjectDataID;
	mySceneID = aGameObject.mySceneID;
}

int CGameObject::GetAccesID()
{
	return ourComponentSystem->GetGameObjectData(myGameObjectDataID, mySceneID)->GetAccessID();
}

void CGameObject::PointToObject(const ID_T(CGameObjectData) aGameObjectDataID, const ID_T(CScene) aSceneID)
{
	myGameObjectDataID = aGameObjectDataID;
	mySceneID = aSceneID;
}

void CGameObject::SetActive(bool aIsActive)
{
	if (!IsValidCheck())
	{
		return;
	}
	ourComponentSystem->GetGameObjectData(myGameObjectDataID, mySceneID)->SetActive(aIsActive);
}

const bool CGameObject::IsActive()
{
	if (!IsValidCheck())
	{
		return false;
	}

	if (GetTransform().myParent != nullptr)
	{
		return ourComponentSystem->GetGameObjectData(myGameObjectDataID, mySceneID)->IsActive() && 
			GetTransform().myParent->myGameObjectData->IsActive();
	}

	return ourComponentSystem->GetGameObjectData(myGameObjectDataID, mySceneID)->IsActive();
}


void CGameObject::Destroy()
{
	if (!IsValidCheck())
	{
		return;
	}
	GetTransform().Destroy();
	myGameObjectDataID = ID_T_INVALID(CGameObjectData);
}

bool CGameObject::IsValidCheck() const
{
	if (!IsValid())
	{
		ENGINE_LOG(CONCOL_WARNING, "Trying to do Operations on a UnInitialized GameObject");
		return false;
	}

	return true;
}

void CGameObject::InitFromAccessID(int aAccessID, ID_T(CScene) aSceneID)
{
	myGameObjectDataID = ourComponentSystem->GetGameObjectIDByAccessID(aAccessID, aSceneID);
	mySceneID = aSceneID;
}

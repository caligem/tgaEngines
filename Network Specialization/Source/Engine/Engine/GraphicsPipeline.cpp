#include "stdafx.h"
#include "GraphicsPipeline.h"

#include "IEngine.h"
#include "IWorld.h"
#include "DirectXFramework.h"

#include "ComponentSystem.h"
#include "SceneManager.h"
#include "CameraManager.h"
#include "ModelManager.h"

#include "WorkerPool.h"
#include "Mathf.h"

#include <DL_Debug.h>

#include <InputManager.h>

CGraphicsPipeline::CGraphicsPipeline()
{
	myDrawWireframe = false;
	myUseOrbitCamera = false;
}

CGraphicsPipeline::~CGraphicsPipeline()
{
}

bool CGraphicsPipeline::Init()
{
	if (!myStateManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init StateManager!");
		return false;
	}

	if (!myCameraBuffer.Init(sizeof(SCameraBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create ForwardRenderer::myCameraBuffer");
	}

	if (!myFadeBuffer.Init(sizeof(CommonUtilities::Vector4f)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create ForwardRenderer::FadeBuffer");
	}

	if (!myForwardRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init ForwardRenderer!");
		return false;
	}
	if (!myDeferredRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init DeferredRenderer!");
		return false;
	}
	if (!myFullscreenRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init FullscreenRenderer!");
		return false;
	}
	if (!myDebugRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init DebugRenderer!");
		return false;
	}
	if (!my2DRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init 2DRenderer!");
		return false;
	}
	if (!myParticleRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init ParticleRenderer!");
		return false;
	}
	if (!mySkyboxRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init SkyboxRenderer!");
		return false;
	}
	if (!myVolumetricRenderer.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init VolumetricRenderer!");
		return false;
	}

	if (!myFullscreenTexture.Init(IEngine::GetCanvasSize(), DXGI_FORMAT_R8G8B8A8_UNORM, true))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myFullscreenTexture!");
		return false;
	}
	if (!myIntermediateTexture1.Init(IEngine::GetCanvasSize()))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myIntermediateTexture!");
		return false;
	}
	myReadPingpong = &myIntermediateTexture1;
	if (!myIntermediateTexture2.Init(IEngine::GetCanvasSize()))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myIntermediateTexture!");
		return false;
	}
	myWritePingpong = &myIntermediateTexture2;
	if (!myHalfTexture.Init(IEngine::GetCanvasSize() / 2.f))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myHalfTexture!");
		return false;
	}
	if (!myQuarterTexture1.Init(IEngine::GetCanvasSize() / 4.f))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myQuarterTexture1!");
		return false;
	}
	if (!myQuarterTexture2.Init(IEngine::GetCanvasSize() / 4.f))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myQuarterTexture2!");
		return false;
	}
	if (!mySSAODepthTexture.Init(IEngine::GetCanvasSize() / 2.f, DXGI_FORMAT_R32G32B32A32_FLOAT))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myHalfTexture!");
		return false;
	}

	DXGI_FORMAT formats[CFullscreenTexture::MaxSize] = {
		DXGI_FORMAT_R32G32B32A32_FLOAT,	// 0: Normal {n.x, n.y, n.z, depth}
		DXGI_FORMAT_R8G8B8A8_UNORM,		// 1: Albedo {a.r, a.g, a.b, a.a}
		DXGI_FORMAT_R8G8B8A8_UNORM,		// 2: RMAO   {roughness, metalness, AO, SSAO}
		DXGI_FORMAT_R8G8B8A8_UNORM		// 3: Emissive {e.r, e.g, e.b, e.a}
	};
	if (!myGBuffer.Init(IEngine::GetCanvasSize(), 4, formats, true))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myGBuffer!");
		return false;
	}
	if (!myLightTexture.Init(IEngine::GetCanvasSize()))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init CGraphicsPipeline::myLightTexture!");
		return false;
	}

	myColorGradingLUT = IEngine::GetTextureManager().CreateTextureFromFile("Assets/LUTS/RGBLUT_NoChange.dds");
	myNoiseTexture = IEngine::GetTextureManager().CreateTextureFromFile("Assets/Shaders/Noise.dds");
	myNoiseNormalTexture = IEngine::GetTextureManager().CreateTextureFromFile("Assets/Shaders/NoiseNormal.dds");
	myNoiseVolumeTexture = IEngine::GetTextureManager().CreateTextureFromFile("Assets/Shaders/NoiseVolume.dds");

	myModelsToRender.Init(16);
	myParticleSystemsToRender.Init(16);
	myStreaksToRender.Init(16);
	myLightsToRender.Init(16);
	myCanvasToRender.Init(16);
	myVolumetricFogToRender.Init(4);

	myModelRenderCommands.Init();
	myParticleSystemRenderCommands.Init();
	myStreakRenderCommands.Init();
	myVolumetricFogRenderCommands.Init();
	myDirectionalLightRenderCommands.Init();
	myPointLightRenderCommands.Init();
	mySpotLightRenderCommands.Init();
	myCanvasRenderCommands.Init();
	
	myActiveRenderers[ERenderer_Forward] = false;
	myActiveRenderers[ERenderer_Fullscreen] = true;
	myActiveRenderers[ERenderer_Debug] = true;
	myActiveRenderers[ERenderer_2D] = true;
	myActiveRenderers[ERenderer_Particle] = true;
	myActiveRenderers[ERenderer_FXAA] = true;
	myActiveRenderers[ERenderer_ColorGrading] = true;
	myActiveRenderers[ERenderer_SSAO] = true;

	myGBufferState = EGBuffer_None;

	myOrbitCamera.SetFov(60.f);

	return true;
}

void CGraphicsPipeline::Render()
{
#ifndef _RETAIL
	UpdateDebugState();
#endif

	ID3D11DeviceContext* context = IEngine::GetContext();
	ID3D11ShaderResourceView* noiseNormalTexture = IEngine::GetTextureManager().GetTexture(myNoiseNormalTexture);
	ID3D11ShaderResourceView* noiseVolumeTexture = IEngine::GetTextureManager().GetTexture(myNoiseVolumeTexture);
	ID3D11ShaderResourceView* colorGradingLUT = IEngine::GetTextureManager().GetTexture(myColorGradingLUT);
	ID3D11ShaderResourceView* skybox = IEngine::GetTextureManager().GetTexture(mySkybox);

	//Render objects in world
	myStateManager.SetSamplerState(CGraphicsStateManager::ESamplerState_Linear_Wrap);
	if (IsActive(ERenderer_Forward))
	{
		//Render skybox
		myFullscreenTexture.SetAsActiveTarget();
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
		mySkyboxRenderer.Render(myCameraBuffer, myCameraPosition);

		myFullscreenTexture.SetAsActiveTarget();
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_Depth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
		myForwardRenderer.Render(myModelRenderCommands.GetReadBuffer(), myCameraBuffer, myPointLightRenderCommands.GetReadBuffer());
	}
	else
	{
		//Render skybox
		myFullscreenTexture.SetAsActiveTarget();
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
		mySkyboxRenderer.Render(myCameraBuffer, myCameraPosition);

		if (myDrawWireframe)
		{
			myStateManager.SetRasterizerState(CGraphicsStateManager::ERasterizerState_Wireframe);
		}
		//Data Pass
		myGBuffer.SetAsActiveTarget();
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_Depth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
		myDeferredRenderer.RenderDataPass(myModelRenderCommands.GetReadBuffer(), myCameraBuffer, myCameraPosition, myFrustum);
		if (IsActive(ERenderer_Debug))
		{
			myDebugRenderer.RenderDebugMeshes(myCameraBuffer);
		}
		if (myDrawWireframe)
		{
			myStateManager.SetRasterizerState(CGraphicsStateManager::ERasterizerState_Solid);
		}

		//SSAO Pass
		if (IsActive(ERenderer_SSAO))
		{
			mySSAODepthTexture.SetAsActiveTarget();
			myGBuffer.SetTextureAsResourceOnSlot(0, 0);
			myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Copy);

			ClearResourceSlots();
			myWritePingpong->SetAsActiveTarget();
			myGBuffer.SetTextureAsResourceOnSlot(0, 2);
			myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Copy);
			SwapPingPong();

			ClearResourceSlots();
			myGBuffer.SetTextureAsActiveTarget(2);
			mySSAODepthTexture.SetAsResourceOnSlot(0);
			myReadPingpong->SetTextureAsResourceOnSlot(1);
			context->PSSetShaderResources(2, 1, &noiseNormalTexture);
			myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
			myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
			myStateManager.SetSamplerState(CGraphicsStateManager::ESamplerState_Linear_Wrap);
			myCameraBuffer.SetBuffer(0, EShaderType_Pixel);
			myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_SSAO);
		}

		//Light Passes
		myLightTexture.SetAsActiveTarget();
		myDeferredRenderer.RenderLightPass(
			myCameraBuffer,
			myModelRenderCommands.GetReadBuffer(),
			myDirectionalLightRenderCommands.GetReadBuffer(),
			myPointLightRenderCommands.GetReadBuffer(),
			mySpotLightRenderCommands.GetReadBuffer(),
			myGBuffer,
			myFrustum,
			myLightTexture,
			myStateManager,
			myNoiseVolumeTexture
		);

		myFullscreenTexture.SetAsActiveTarget();
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_AlphaBlend);
		myGBuffer.SetTextureAsResourceOnSlot(0, 0);
		myLightTexture.SetAsResourceOnSlot(1);
		context->PSSetShaderResources(2, 1, &skybox);
		myCameraBuffer.SetBuffer(0, EShaderType_Pixel);
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Fog);
	}

	//Render Particle emitters
	myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_Depth);
	myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);


	myFullscreenTexture.SetAsActiveTarget();

	//=== Volumetric Fog Start ===
	myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
	myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_AlphaBlend);
	myGBuffer.SetTextureAsResourceOnSlot(0, 0);
	context->PSSetShaderResources(1, 1, &noiseVolumeTexture);
	myVolumetricRenderer.RenderVolumetricFog(myVolumetricFogRenderCommands.GetReadBuffer(), myCameraBuffer, myFrustum);
	//=== Volumetric Fog End ===

	if (IsActive(ERenderer_Forward))
	{
		myFullscreenTexture.SetAsActiveTarget();
	}
	else
	{
		myFullscreenTexture.SetAsActiveTarget(myGBuffer.GetDepthStencilView());
	}
	if (IsActive(ERenderer_Particle))
	{
		if (myDrawWireframe)
		{
			myStateManager.SetRasterizerState(CGraphicsStateManager::ERasterizerState_Wireframe);
		}
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_AlphaBlend);
		myParticleRenderer.RenderParticles(myStateManager, myParticleSystemRenderCommands.GetReadBuffer(), myCameraBuffer, myFrustum);
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_Depth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);

		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Additive);
		myParticleRenderer.RenderStreaks(myStateManager, myStreakRenderCommands.GetReadBuffer(), myCameraBuffer, myFrustum);
		myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_Depth);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
		if (myDrawWireframe)
		{
			myStateManager.SetRasterizerState(CGraphicsStateManager::ERasterizerState_Solid);
		}
	}

	//Render debug lines
	if (IsActive(ERenderer_Debug))
	{
		myDebugRenderer.RenderDebugLines(myCameraBuffer);
	}
	// Fullscreen effects come here :) start ping-pong-ing
	myWritePingpong->SetAsActiveTarget();
	myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Copy, { &myFullscreenTexture, nullptr });
	SwapPingPong();

	myStateManager.SetDepthStencilState(CGraphicsStateManager::EDepthStencilState_NoDepth);
	myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
	myStateManager.SetSamplerState(CGraphicsStateManager::ESamplerState_Linear_Clamp);
	if (IsActive(ERenderer_Fullscreen))
	{
		//Down sample scene
		myHalfTexture.SetAsActiveTarget();
		myGBuffer.SetTextureAsResourceOnSlot(0, 3);
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_BloomLuminance);

		//Down sample some more :D
		myQuarterTexture1.SetAsActiveTarget();
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Copy, { &myHalfTexture, nullptr });

		//BLUUUUUUUUUUUUR
		for (int i = 0; i < 4; ++i)
		{
			myQuarterTexture2.SetAsActiveTarget();
			myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_GaussianBlurHorizontal, { &myQuarterTexture1, nullptr });
			myQuarterTexture1.SetAsActiveTarget();
			myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_GaussianBlurVertical, { &myQuarterTexture2, nullptr });
		}

		//Scale up to fullscreen and present
		myWritePingpong->SetAsActiveTarget();
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_BloomAdd, { myReadPingpong, &myQuarterTexture1 });
		SwapPingPong();
	}

	if (IsActive(ERenderer_ColorGrading))
	{
		myWritePingpong->SetAsActiveTarget();
		myReadPingpong->SetAsResourceOnSlot(0);
		context->PSSetShaderResources(1, 1, &colorGradingLUT);
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_ColorGrading);
		SwapPingPong();
	}

	myWritePingpong->SetAsActiveTarget();
	myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Copy, { myReadPingpong, nullptr });
	SwapPingPong();

	if (IsActive(ERenderer_FXAA))
	{
		myWritePingpong->SetAsActiveTarget();
		context->PSSetShaderResources(2, 1, &noiseNormalTexture);
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_FXAA, { myReadPingpong, nullptr });
		SwapPingPong();
	}

	myWritePingpong->SetAsActiveTarget();
	myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Copy, { myReadPingpong, nullptr });

	//Render 2D stuffs
	if (myDrawWireframe)
	{
		myStateManager.SetRasterizerState(CGraphicsStateManager::ERasterizerState_Wireframe);
	}
	if (IsActive(ERenderer_2D))
	{
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_AlphaBlend);
		myStateManager.SetSamplerState(CGraphicsStateManager::ESamplerState_Linear_Wrap);
		my2DRenderer.RenderUIOnCanvas(myCanvasRenderCommands.GetReadBuffer(), *myWritePingpong);
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_AlphaBlend);

		myWritePingpong->SetAsActiveTarget(myGBuffer.GetDepthStencilView());
		myStateManager.SetSamplerState(CGraphicsStateManager::ESamplerState_Linear_Clamp);
		myStateManager.SetRasterizerState(CGraphicsStateManager::ERasterizerState_Solid_NoCulling);
		my2DRenderer.RenderWorldSpaceCanvases(myCameraBuffer, myCanvasRenderCommands.GetReadBuffer());
		myStateManager.SetSamplerState(CGraphicsStateManager::ESamplerState_Linear_Wrap);
		myStateManager.SetRasterizerState(CGraphicsStateManager::ERasterizerState_Solid);

		/*
		myWritePingpong->SetAsActiveTarget();
		my2DRenderer.RenderScreenSpaceCanvases(myCanvasRenderCommands.GetReadBuffer());
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
		*/
	}
	if (myDrawWireframe)
	{
		myStateManager.SetRasterizerState(CGraphicsStateManager::ERasterizerState_Solid);
	}
	if (IsActive(ERenderer_Debug))
	{
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_AlphaBlend);
		myStateManager.SetSamplerState(CGraphicsStateManager::ESamplerState_Linear_Wrap);
		myDebugRenderer.RenderText();
		myStateManager.SetBlendState(CGraphicsStateManager::EBlendState_Disabled);
	}

	//FadeFX
	SwapPingPong();
	IEngine::GetDXFramework().GetBackBuffer()->SetAsActiveTarget();
	myFadeBuffer.SetData(&myFadeColor);
	myFadeBuffer.SetBuffer(2, EShaderType_Pixel);
	if(myGBufferState == EGBuffer_None)
	{
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Fade, { myReadPingpong, nullptr });
	}
	else
	{
		auto* ptr = myGBuffer.GetShaderResourceView(static_cast<int>(myGBufferState));
		context->PSSetShaderResources(0, 1, &ptr);
		myFullscreenRenderer.Render(CFullscreenRenderer::EEffect_Copy);
	}
}

void CGraphicsPipeline::SwapBuffers()
{
	myModelRenderCommands.SwapBuffers();
	myParticleSystemRenderCommands.SwapBuffers();
	myStreakRenderCommands.SwapBuffers();
	myVolumetricFogRenderCommands.SwapBuffers();
	myCanvasRenderCommands.SwapBuffers();
	myDirectionalLightRenderCommands.SwapBuffers();
	myPointLightRenderCommands.SwapBuffers();
	mySpotLightRenderCommands.SwapBuffers();

	//Temp swap canvas buffers
	CSceneManager& sceneManager = IEngine::GetSceneManager();
	CComponentSystem& componentSystem = IEngine::GetComponentSystem();

	CScene* activeScene = sceneManager.GetActiveScene();
	if (activeScene == nullptr) return;
	ID_T(CScene) activeSceneID = activeScene->mySceneID;

	for (auto canvas : myCanvasToRender)
	{
		CCanvasComponent* canvasComponent = componentSystem.GetComponent<CCanvasComponent>(canvas, activeSceneID);

		if (canvasComponent)
		{
			canvasComponent->SwapBuffers();
		}
	}

	myDebugRenderer.SwapBuffers();

	mySavedFadeColor = myFadeColor;
	mySavedPlayerPosition = myPlayerPosition;
}

void CGraphicsPipeline::SetRenderBuffers()
{
	CScene* activeScene = IEngine::GetSceneManager().GetActiveScene();
	if (activeScene == nullptr)return;

	mySkybox = activeScene->mySkybox;
	myCubemap = activeScene->myCubemap;

	FillModelBuffer();
	FillParticleSystemBuffer();
	FillStreakBuffer();
	FillStreakBuffer();
	FillVolumetricFogBuffer();
	FillLightBuffers();
	FillCanvasBuffer();
}

 void CGraphicsPipeline::SwapPingPong()
 {
	if (myWritePingpong == &myIntermediateTexture1)
	{
		myWritePingpong = &myIntermediateTexture2;
		myReadPingpong = &myIntermediateTexture1;
	}
	else
	{
		myWritePingpong = &myIntermediateTexture1;
		myReadPingpong = &myIntermediateTexture2;
	}
}

void CGraphicsPipeline::FillModelBuffer()
{
	CSceneManager& sceneManager = IEngine::GetSceneManager();
	CComponentSystem& componentSystem = IEngine::GetComponentSystem();

	CScene* activeScene = sceneManager.GetActiveScene();
	if (activeScene == nullptr) return;
	ID_T(CScene) activeSceneID = activeScene->mySceneID;

	myModelsToRender.RemoveAll();
	activeScene->FillRenderBuffer(_UUID(CModelComponent), myModelsToRender);
	for (auto modelComponentID : myModelsToRender)
	{
		CModelComponent* modelComponent = componentSystem.GetComponent<CModelComponent>(modelComponentID, activeSceneID);
		CGameObjectData* gameObjectData = componentSystem.GetGameObjectData(modelComponent->myGameObjectDataID, activeSceneID);
		CModel* model = IEngine::GetModelManager().GetModel(modelComponent->myModelID);
		int animationID = 0;
		float animationTime = 0.f;
		if (model->myModelData.mySceneAnimator && model->myModelData.mySceneAnimator->HasSkeleton())
		{
			CAnimationControllerComponent* animationControllerComponent = componentSystem.GetComponent<CAnimationControllerComponent>(modelComponent->myGameObjectDataID, activeSceneID);
			if (animationControllerComponent)
			{
				model->myModelData.mySceneAnimator->SetAnimIndex(animationControllerComponent->GetAnimationIndex());
				animationTime = animationControllerComponent->GetTime();
				animationID = model->myModelData.mySceneAnimator->GetAnimationIndex();
			}
		}

		if (gameObjectData->IsActive())
		{
			myModelRenderCommands.Write({
				gameObjectData->GetTransform().GetMatrix(),
				gameObjectData->GetTransform().GetScale(),
				modelComponent->myModelID,
				modelComponent->myMaterial,
				animationID,
				animationTime
			});
		}
	}
}

void CGraphicsPipeline::FillParticleSystemBuffer()
{
	CSceneManager& sceneManager = IEngine::GetSceneManager();
	CComponentSystem& componentSystem = IEngine::GetComponentSystem();

	CScene* activeScene = sceneManager.GetActiveScene();
	if (activeScene == nullptr) return;
	ID_T(CScene) activeSceneID = activeScene->mySceneID;

	myParticleSystemsToRender.RemoveAll();
	activeScene->FillRenderBuffer(_UUID(CParticleSystemComponent), myParticleSystemsToRender);
	for (auto particleSystemComponentID : myParticleSystemsToRender)
	{
		CParticleSystemComponent* particleSystemComponent = componentSystem.GetComponent<CParticleSystemComponent>(particleSystemComponentID, activeSceneID);
		CGameObjectData* gameObjectData = componentSystem.GetGameObjectData(particleSystemComponent->myGameObjectDataID, activeSceneID);

		if (gameObjectData->IsActive())
		{
			myParticleSystemRenderCommands.Write({
				particleSystemComponent->myParticles,
				particleSystemComponent->myBounds.GetMin(),
				particleSystemComponent->myBounds.GetMax(),
				particleSystemComponent->myParticleEmitterID
			});
		}
	}
}

void CGraphicsPipeline::FillStreakBuffer()
{
	CSceneManager& sceneManager = IEngine::GetSceneManager();
	CComponentSystem& componentSystem = IEngine::GetComponentSystem();

	CScene* activeScene = sceneManager.GetActiveScene();
	if (activeScene == nullptr) return;
	ID_T(CScene) activeSceneID = activeScene->mySceneID;

	myStreaksToRender.RemoveAll();
	activeScene->FillRenderBuffer(_UUID(CStreakComponent), myStreaksToRender);
	for (auto streakComponentID : myStreaksToRender)
	{
		CStreakComponent* streakComponent = componentSystem.GetComponent<CStreakComponent>(streakComponentID, activeSceneID);
		CGameObjectData* gameObjectData = componentSystem.GetGameObjectData(streakComponent->myGameObjectDataID, activeSceneID);

		if (gameObjectData->IsActive())
		{
			myStreakRenderCommands.Write({
				streakComponent->myLeadingPoint,
				streakComponent->myPoints,
				streakComponent->myBounds.GetMin(),
				streakComponent->myBounds.GetMax(),
				streakComponent->myStreakID
			});
		}
	}
}

void CGraphicsPipeline::FillVolumetricFogBuffer()
{
	CSceneManager& sceneManager = IEngine::GetSceneManager();
	CComponentSystem& componentSystem = IEngine::GetComponentSystem();

	CScene* activeScene = sceneManager.GetActiveScene();
	if (activeScene == nullptr) return;
	ID_T(CScene) activeSceneID = activeScene->mySceneID;

	myVolumetricFogToRender.RemoveAll();
	activeScene->FillRenderBuffer(_UUID(CVolumetricFogComponent), myVolumetricFogToRender);
	for (auto volumeComponentID : myVolumetricFogToRender)
	{
		CVolumetricFogComponent* volumeComponent = componentSystem.GetComponent<CVolumetricFogComponent>(volumeComponentID, activeSceneID);
		CGameObjectData* gameObjectData = componentSystem.GetGameObjectData(volumeComponent->myGameObjectDataID, activeSceneID);

		if (gameObjectData->IsActive())
		{
			auto pos = gameObjectData->GetTransform().GetPosition();
			myVolumetricFogRenderCommands.Write({
				gameObjectData->GetTransform().GetPosition() - gameObjectData->GetTransform().GetLocalScale(),
				gameObjectData->GetTransform().GetPosition() + gameObjectData->GetTransform().GetLocalScale()
			});
		}
	}
}

void CGraphicsPipeline::FillLightBuffers()
{
	CSceneManager& sceneManager = IEngine::GetSceneManager();
	CComponentSystem& componentSystem = IEngine::GetComponentSystem();

	CScene* activeScene = sceneManager.GetActiveScene();
	if (activeScene == nullptr) return;
	ID_T(CScene) activeSceneID = activeScene->mySceneID;

	myLightsToRender.RemoveAll();
	activeScene->FillRenderBuffer(_UUID(CLightComponent), myLightsToRender);
	for (auto lightComponentID : myLightsToRender)
	{
		CLightComponent* lightComponent = componentSystem.GetComponent<CLightComponent>(lightComponentID, activeSceneID);

		if (lightComponent->GetIntensity() <= 1e-2)
		{
			continue;
		}

		CGameObjectData* gameObjectData = componentSystem.GetGameObjectData(lightComponent->myGameObjectDataID, activeSceneID);

		if (gameObjectData->IsActive())
		{
			switch (lightComponent->GetType())
			{
			case CLightComponent::ELightType_Directional:
				myDirectionalLightRenderCommands.Write({
					gameObjectData->GetTransform().GetForward(),
					lightComponent->GetColor(),
					lightComponent->GetIntensity(),
					lightComponent->HasShadows()
				});
				break;
			case CLightComponent::ELightType_Point:
				myPointLightRenderCommands.Write({
					gameObjectData->GetTransform().GetPosition(),
					lightComponent->GetColor(),
					lightComponent->GetRange(),
					lightComponent->GetIntensity(),
					lightComponent->HasShadows()
				});
				break;
			case CLightComponent::ELightType_Spot:
				mySpotLightRenderCommands.Write({
					gameObjectData->GetTransform().GetPosition(),
					gameObjectData->GetTransform().GetForward(),
					lightComponent->GetColor(),
					lightComponent->GetRange(),
					lightComponent->GetSpotAngle() * CommonUtilities::Deg2Rad,
					lightComponent->GetIntensity(),
					lightComponent->HasShadows()
				});
				break;
			}
		}
	}
}

void CGraphicsPipeline::FillCanvasBuffer()
{
	CSceneManager& sceneManager = IEngine::GetSceneManager();
	CComponentSystem& componentSystem = IEngine::GetComponentSystem();

	CScene* activeScene = sceneManager.GetActiveScene();
	if (activeScene == nullptr) return;
	ID_T(CScene) activeSceneID = activeScene->mySceneID;

	myCanvasToRender.RemoveAll();
	activeScene->FillRenderBuffer(_UUID(CCanvasComponent), myCanvasToRender);

	for (auto canvas : myCanvasToRender)
	{
		CCanvasComponent* canvasComponent = componentSystem.GetComponent<CCanvasComponent>(canvas, activeSceneID);
		CGameObjectData* gameObject = componentSystem.GetGameObjectData(canvasComponent->myGameObjectDataID, activeSceneID);
		
		if (gameObject->IsActive())
		{
			myCanvasRenderCommands.Write({ canvas, gameObject->GetTransform().GetLocalScale(), canvasComponent->GetAlpha() });
			canvasComponent->FillRenderCommands();
		}
	}
}

void CGraphicsPipeline::ClearResourceSlots()
{
	ID3D11ShaderResourceView* res[CFullscreenTexture::MaxSize] = { NULL };
	IEngine::GetContext()->PSSetShaderResources(0, CFullscreenTexture::MaxSize, res);
}

void CGraphicsPipeline::SetCameraBufferData(const SCameraBufferData& data)
{
	myCameraBuffer.SetData(&data);
	myCameraPosition = {
		data.myCameraOrientation[12],
		data.myCameraOrientation[13],
		data.myCameraOrientation[14],
		0.f
	};

	myFrustum.Init(data.myViewProjection);
}

void CGraphicsPipeline::DrawDebugTexts()
{
	if (myDrawWireframe)
	{
		IWorld::DrawDebugText("Wireframe: On");
	}
	if (myUseOrbitCamera)
	{
		IWorld::DrawDebugText("Debug Camera: On");
	}
}

void CGraphicsPipeline::UpdateDebugState()
{
	Input::CInputManager& input = IEngine::GetInputManager();
	if (input.IsKeyDown(Input::Key_Ctrl, 99))
	{
		for (int i = 0; i < EGBuffer_None; ++i)
		{
			if (input.IsKeyPressed(static_cast<Input::Key>(Input::Key_F1 + i), 99))
			{
				if (myGBufferState == static_cast<EGBufferState>(i))
				{
					myGBufferState = EGBuffer_None;
				}
				else
				{
					myGBufferState = static_cast<EGBufferState>(i);
				}
				break;
			}
		}
	}
	else
	{
		for (int i = 0; i < ERenderer_Count; ++i)
		{
			if (input.IsKeyPressed(static_cast<Input::Key>(Input::Key_F1 + i), 99))
			{
				myActiveRenderers[i] = !myActiveRenderers[i];
			}
		}
	}
	if (input.IsKeyPressed(Input::Key_Num0, 99))myDrawWireframe = !myDrawWireframe;
	if (input.IsKeyPressed(Input::Key_Num1, 99))
	{
		myUseOrbitCamera = !myUseOrbitCamera;
		input.SetInputLevel(myUseOrbitCamera ? 1 : 0);
	}

	if (myUseOrbitCamera)
	{
		myOrbitCamera.Update();
		SCameraBufferData data;
		data.myCameraOrientation = myOrbitCamera.GetTransform().GetMatrix();
		data.myToCamera = data.myCameraOrientation.GetFastInverse();
		data.myProjection = myOrbitCamera.GetProjection();
		data.myInvertedProjection = CommonUtilities::Matrix44f::Inverse(myOrbitCamera.GetProjection());
		data.myViewProjection = data.myToCamera * data.myProjection;
		data.myInvertedViewProjection = CommonUtilities::Matrix44f::Inverse(data.myViewProjection);
		SetCameraBufferData(data);
	}
	else
	{
		myOrbitCamera.SetPosDir(mySavedCameraBuffer.myCameraOrientation.myPosition, mySavedCameraBuffer.myCameraOrientation.myForwardAxis);
	}
}

void CGraphicsPipeline::SetCameraBuffer()
{
	if (!myUseOrbitCamera)
	{
		CSceneManager& sceneManager = IEngine::GetSceneManager();
		CScene* activeScene = sceneManager.GetActiveScene();
		if (activeScene == nullptr)
		{
			ENGINE_LOG(CONCOL_ERROR, "Scene was nullptr in SetCameraBuffer.");
			return;
		}
		CCameraComponent* camera = activeScene->GetActiveCamera();
		if (camera == nullptr)
		{
			ENGINE_LOG(CONCOL_ERROR, "Failed to get active camera from scene!");
			return;
		}
		ID_T(CCamera) cameraID = camera->myCameraID;

		CCamera* cam = IEngine::GetCameraManager().GetCamera(cameraID);
		if (cam == nullptr)
		{
			return;
		}

		SCameraBufferData data;
		data.myCameraOrientation = IEngine::GetComponentSystem().GetGameObjectData(camera->myGameObjectDataID, sceneManager.GetActiveScene()->mySceneID)->GetTransform().GetOrientation();
		data.myToCamera = data.myCameraOrientation.GetFastInverse();
		data.myProjection = cam->GetProjection();
		data.myInvertedProjection = CommonUtilities::Matrix44f::Inverse(cam->GetProjection());
		data.myViewProjection = data.myToCamera * data.myProjection;
		data.myInvertedViewProjection = CommonUtilities::Matrix44f::Inverse(data.myViewProjection);

		mySavedCameraBuffer = data;

		SetCameraBufferData(data);
	}
	else
	{
		SCameraBufferData data;
		data.myCameraOrientation = myOrbitCamera.GetTransform().GetMatrix();
		data.myToCamera = data.myCameraOrientation.GetFastInverse();
		data.myProjection = myOrbitCamera.GetProjection();
		data.myInvertedProjection = CommonUtilities::Matrix44f::Inverse(myOrbitCamera.GetProjection());
		data.myViewProjection = data.myToCamera * data.myProjection;
		data.myInvertedViewProjection = CommonUtilities::Matrix44f::Inverse(data.myViewProjection);

		mySavedCameraBuffer = data;

		SetCameraBufferData(data);
	}
}

void CGraphicsPipeline::BeginFrame()
{
	float clearColor[] = { 1.0f, 0.7f, 0.3f, 1.f };
	myFullscreenTexture.ClearTexture(clearColor);
	myLightTexture.ClearTexture({ 0.f, 0.f, 0.f, 0.f });
	myGBuffer.ClearTexture({ 0.f, 0.f, 0.f, 0.f });
	myGBuffer.ClearTextureAt(0, { 0.f, 0.f, 0.f, FLT_MAX });
}
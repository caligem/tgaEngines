#include "stdafx.h"
#include "GraphicsStateManager.h"

#include <d3d11.h>

#include "DL_Debug.h"
#include "IEngine.h"

CGraphicsStateManager::CGraphicsStateManager()
{
	for (int i = 0; i < EDepthStencilState_Count; ++i)myDepthStencilStates[i] = NULL;
	for (int i = 0; i < EBlendState_Count; ++i)myBlendStates[i] = NULL;
	for (int i = 0; i < ESamplerState_Count; ++i)mySamplerStates[i] = NULL;
	for (int i = 0; i < ERasterizerState_Count; ++i)myRasterizerStates[i] = NULL;
}

CGraphicsStateManager::~CGraphicsStateManager()
{
}

bool CGraphicsStateManager::Init()
{
	if (!CreateDepthStencilStates())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create depth stencil states!");
		return false;
	}
	if (!CreateBlendStates())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create blend states!");
		return false;
	}
	if (!CreateSamplerStates())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create sampler states!");
		return false;
	}
	if (!CreateRasterizerStates())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create rasterizer states!");
		return false;
	}

	SetDepthStencilState(EDepthStencilState::EDepthStencilState_Depth);
	SetBlendState(EBlendState::EBlendState_Disabled);
	SetSamplerState(ESamplerState::ESamplerState_Linear_Wrap);
	SetRasterizerState(ERasterizerState::ERasterizerState_Solid);

	return true;
}

void CGraphicsStateManager::SetDepthStencilState(EDepthStencilState aDepthStencilState)
{
	IEngine::GetContext()->OMSetDepthStencilState(myDepthStencilStates[aDepthStencilState], 1);
}

void CGraphicsStateManager::SetBlendState(EBlendState aBlendState)
{
	float blendFactor[4];
	blendFactor[0] = 0.0f;
	blendFactor[1] = 0.0f;
	blendFactor[2] = 0.0f;
	blendFactor[3] = 0.0f;
	IEngine::GetContext()->OMSetBlendState(myBlendStates[aBlendState], blendFactor, 0xffffffff);
}

void CGraphicsStateManager::SetSamplerState(ESamplerState aSamplerState)
{
	IEngine::GetContext()->PSSetSamplers(0, 1, &mySamplerStates[aSamplerState]);
}

void CGraphicsStateManager::SetRasterizerState(ERasterizerState aRasterizerState)
{
	IEngine::GetContext()->RSSetState(myRasterizerStates[aRasterizerState]);
}

bool CGraphicsStateManager::CreateDepthStencilStates()
{
	HRESULT result;
	D3D11_DEPTH_STENCIL_DESC depthDesc;

	const D3D11_DEPTH_STENCILOP_DESC defaultStencilOp = {
		D3D11_STENCIL_OP_KEEP,
		D3D11_STENCIL_OP_KEEP,
		D3D11_STENCIL_OP_KEEP,
		D3D11_COMPARISON_ALWAYS
	};

	ZeroMemory(&depthDesc, sizeof(depthDesc));
	depthDesc.DepthEnable = TRUE;
	depthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthDesc.StencilEnable = FALSE;
	depthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
	depthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
	depthDesc.FrontFace = defaultStencilOp;
	depthDesc.BackFace = defaultStencilOp;
	result = IEngine::GetDevice()->CreateDepthStencilState(&depthDesc, &myDepthStencilStates[EDepthStencilState_Depth]);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create EDepthStencilState_Depth!");
		return false;
	}


	ZeroMemory(&depthDesc, sizeof(depthDesc));
	depthDesc.DepthEnable = TRUE;
	depthDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
	depthDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthDesc.StencilEnable = FALSE;
	depthDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
	depthDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
	depthDesc.FrontFace = defaultStencilOp;
	depthDesc.BackFace = defaultStencilOp;
	result = IEngine::GetDevice()->CreateDepthStencilState(&depthDesc, &myDepthStencilStates[EDepthStencilState_NoDepth]);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create EDepthStencilState_Depth!");
		return false;
	}

	return true;
}

bool CGraphicsStateManager::CreateBlendStates()
{
	HRESULT result;
	D3D11_BLEND_DESC blendDesc;

	ZeroMemory(&blendDesc, sizeof(blendDesc));
	blendDesc.RenderTarget[0].BlendEnable = FALSE;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	result = IEngine::GetDevice()->CreateBlendState(&blendDesc, &myBlendStates[EBlendState_Disabled]);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create EBlendState_Disabled");
		return false;
	}

	ZeroMemory(&blendDesc, sizeof(blendDesc));
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_MAX;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	result = IEngine::GetDevice()->CreateBlendState(&blendDesc, &myBlendStates[EBlendState_AlphaBlend]);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create EBlendState_AlphaBlend");
		return false;
	}

	ZeroMemory(&blendDesc, sizeof(blendDesc));
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_MAX;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
	result = IEngine::GetDevice()->CreateBlendState(&blendDesc, &myBlendStates[EBlendState_Additive]);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create EBlendState_Additive");
		return false;
	}

	return true;
}

bool CGraphicsStateManager::CreateSamplerStates()
{
	HRESULT result;
	D3D11_SAMPLER_DESC samplerDesc;

	ZeroMemory(&samplerDesc, sizeof(samplerDesc));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_CLAMP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 16;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	result = IEngine::GetDevice()->CreateSamplerState(&samplerDesc, &mySamplerStates[ESamplerState_Linear_Clamp]);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create ESamplerState_Linear_Clamp");
		return false;
	}

	ZeroMemory(&samplerDesc, sizeof(samplerDesc));
	samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
	samplerDesc.MipLODBias = 0.0f;
	samplerDesc.MaxAnisotropy = 16;
	samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
	samplerDesc.BorderColor[0] = 0;
	samplerDesc.BorderColor[1] = 0;
	samplerDesc.BorderColor[2] = 0;
	samplerDesc.BorderColor[3] = 0;
	samplerDesc.MinLOD = 0;
	samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;
	result = IEngine::GetDevice()->CreateSamplerState(&samplerDesc, &mySamplerStates[ESamplerState_Linear_Wrap]);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create ESamplerState_Linear_Wrap");
		return false;
	}

	return true;
}

bool CGraphicsStateManager::CreateRasterizerStates()
{
	HRESULT result;

	D3D11_RASTERIZER_DESC rasterDesc;

	ZeroMemory(&rasterDesc, sizeof(rasterDesc));
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.FrontCounterClockwise = FALSE;
	rasterDesc.DepthBias = 0;
	rasterDesc.SlopeScaledDepthBias = 0.f;
	rasterDesc.DepthBiasClamp = 0.f;
	rasterDesc.DepthClipEnable = TRUE;
	rasterDesc.ScissorEnable = FALSE;
	rasterDesc.MultisampleEnable = FALSE;
	rasterDesc.AntialiasedLineEnable = FALSE;
	result = IEngine::GetDevice()->CreateRasterizerState(&rasterDesc, &myRasterizerStates[ERasterizerState_Solid]);

	rasterDesc.CullMode = D3D11_CULL_NONE;
	result = IEngine::GetDevice()->CreateRasterizerState(&rasterDesc, &myRasterizerStates[ERasterizerState_Solid_NoCulling]);

	rasterDesc.FillMode = D3D11_FILL_WIREFRAME;
	result = IEngine::GetDevice()->CreateRasterizerState(&rasterDesc, &myRasterizerStates[ERasterizerState_Wireframe]);

	return true;
}

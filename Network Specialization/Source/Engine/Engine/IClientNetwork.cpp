#include "stdafx.h"
#include "IClientNetwork.h"
#include "NetworkSystem.h"

CNetworkSystem* IClientNetwork::ourNetworkSystem = nullptr;
CAddressWrapper IClientNetwork::myDestinationAddress;

IClientNetwork::IClientNetwork()
{
}

IClientNetwork::~IClientNetwork()
{
}

void IClientNetwork::SetDestinationAddress(CAddressWrapper & aDestinationAddress)
{
	myDestinationAddress = aDestinationAddress;
}

void IClientNetwork::Send(CNetMessage * aMessage)
{
	ourNetMessageManager->PackAndSendMessage(aMessage, myDestinationAddress);
}

bool IClientNetwork::IsConnectedToMainServer()
{
	if (!ourNetworkSystem)
	{
		return false;
	}
	return ourNetworkSystem->IsConnected();
}

const std::string& IClientNetwork::GetMyName()
{
	return ourNetworkSystem->GetMyName();
}

const unsigned short IClientNetwork::GetMyID()
{
	return ourNetMessageManager->GetSenderID();
}
const uint32_t IClientNetwork::GetPing()
{
	return ourNetworkSystem->GetPing();
}
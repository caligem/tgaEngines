#pragma once
#include "INetwork.h"
#include "AddressWrapper.h"

class IClientNetwork : public INetwork
{
public:
	IClientNetwork();
	~IClientNetwork();

	static void SetDestinationAddress(CAddressWrapper& aDestinationAddress);
	static void Send(CNetMessage* aMessage);
	static bool IsConnectedToMainServer();
	static const std::string& GetMyName();
	static const unsigned short GetMyID();
	static const uint32_t GetPing();
private:
	friend class CNetworkSystem;
	static CNetworkSystem* ourNetworkSystem;
	static CAddressWrapper myDestinationAddress;
};


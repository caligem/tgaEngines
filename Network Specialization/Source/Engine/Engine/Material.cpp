#include "stdafx.h"
#include "Material.h"

#include "IEngine.h"
#include "ShaderManager.h"

void CMaterial::SetVertexShader(const std::wstring & aShaderFile)
{
	CShaderManager& shaderManager = IEngine::GetShaderManager();
	myVertexShader = &shaderManager.GetVertexShader(aShaderFile + L".vs", EShaderInputLayoutType_PBR);
}

void CMaterial::SetPixelShader(const std::wstring & aShaderFile)
{
	CShaderManager& shaderManager = IEngine::GetShaderManager();
	myPixelShader = &shaderManager.GetPixelShader(aShaderFile + L".ps");
}

void CMaterial::Reset()
{
	myPixelShader = nullptr;
	myVertexShader = nullptr;
	myHasCustomData = false;
	myShouldRenderDepth = true;
}

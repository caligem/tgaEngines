#pragma once

#include "Matrix.h"
#include "Vector.h"
#include "Component.h"
#include "Material.h"

class CModel;
class CModelManager;

class CModelComponent : public CComponent
{
public:
	CModelComponent();
	~CModelComponent();

	struct SComponentData
	{
		const char* myFilePath;
	};

	void AssignMaterial(const std::wstring & aShaderFile);
	inline const float GetRadius() const { return myRadius; }

	CMaterial& GetMaterial() { return myMaterial; }

	bool HasAnimations();

private:
	friend class CAnimationControllerComponent;
	friend class CForwardRenderer;
	friend class CEngine;
	friend class CGraphicsPipeline;
	friend class CComponentSystem;

	void Init(const SComponentData& aComponentData);
	void Release() override;

	static CModelManager* ourModelManager;
	float myRadius;

	ID_T(CModel) myModelID;
	CMaterial myMaterial;
};


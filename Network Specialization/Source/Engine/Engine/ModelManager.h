#pragma once
#include "ModelLoader.h"

class CDirectXFramework;

class CModelManager
{
public:
	CModelManager();
	~CModelManager();

	bool Init(CDirectXFramework& aDirectXFramework);
	ID_T(CModel) AcquireModel(const char* aModelPath = nullptr);
	void ReleaseModel(ID_T(CModel) aModelID);

private:
	friend class CGraphicsPipeline;
	friend class CSkyboxRenderer;
	friend class CForwardRenderer;
	friend class CDeferredRenderer;
	friend class CDebugRenderer;
	friend class CVolumetricRenderer;
	friend class CModelComponent;
	friend class CAnimationControllerComponent;

	CModel* GetModel(ID_T(CModel) aModelID);
	void SearchForModelMaterials(const std::string& aModelPath, ID_T(CModel) aModelID);
	std::map<std::string, ID_T(CModel)> myModelCache;
	ObjectPool<CModel> myModels;
	CModelLoader myModelLoader;
};


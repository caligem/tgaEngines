#pragma once
#include "SocketWrapper.h"
#include "AddressWrapper.h"
#include "NetMessageManager.h"

class CNetworkSystem
{
public:
	CNetworkSystem(CommonUtilities::Timer& aTimer);
	~CNetworkSystem();

	bool Init(char aThreadCount);
	void PreUpdate();
	void Update();
	void PostUpdate();
	void ConnectToMainServer();

	inline const bool IsConnected() { return myIsConnected; }
	inline const uint32_t GetPing() { return myPing; }
	inline const uint32_t GetRTT() { return myRTT; }
	inline const std::string& GetMyName() { return myName; }

private:
	void HandleConnectionMessage(CNetMessage* aMessage, CAddressWrapper& aSenderAddress);
	void SendClientName(const CAddressWrapper& aSenderAddress);

	void RecievePings(CNetMessage* aMessage, CAddressWrapper& aSenderAddress);
	void PingMainServer();
	void UpdatePingTimers();
	void HandleLuaCommandMessage(CNetMessage* aMessage, CAddressWrapper&);

	void GetLocalIP(char* aIPbuffer);

	CAddressWrapper myMainServerAddress;
	CNetMessageManager myNetMessageManager;

	float myLatestRecievedPingTimer = 0.f;
	float myPingFrequency = 200.f;
	float myPingTimer = 0.f;
	float myTimoutTime = 2000.f;

	unsigned int myPing = 0;
	unsigned int myRTT = 0;

	unsigned short myServerID;

	ID_T(CLuaScript) myLuaScript;

	bool myIsConnected;

	std::string myName;
};
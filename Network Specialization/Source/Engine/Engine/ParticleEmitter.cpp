#include "stdafx.h"
#include "ParticleEmitter.h"

#include "IEngine.h"

#include "DXMacros.h"

CParticleEmitter::CParticleEmitter()
{
}

CParticleEmitter::~CParticleEmitter()
{
}

void CParticleEmitter::SetTexture(const char * aTexturePath)
{
	myData.myTexturePath = aTexturePath;

	myTexture = IEngine::GetTextureManager().CreateTextureFromFile(myData.myTexturePath);
}

void CParticleEmitter::SetSpawnRate(float aSpawnRate)
{
	myData.mySpawnRate = aSpawnRate;
}

void CParticleEmitter::SetLifetime(float aLifetime)
{
	myData.myLifetime = aLifetime;
}

void CParticleEmitter::ResetToDefault()
{
	myData = SParticleData();
	SetTexture("");
}

bool CParticleEmitter::Init(SParticleData aParticleData)
{
	myData = aParticleData;

	if (myData.myLifetime <= 0.0f)
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to create ParticleSystemComponent but Lifetime is below 0");
		return false;
	}
	if (myData.mySpawnRate <= 0.0f)
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to create ParticleSystemComponent but SpawnRate is below 0");
		return false;
	}

	myTexture = IEngine::GetTextureManager().CreateTextureFromFile(myData.myTexturePath);

	return true;
}
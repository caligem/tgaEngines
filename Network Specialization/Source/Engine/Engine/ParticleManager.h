#pragma once

#include "ObjectPool.h"
#include "ParticleEmitter.h"
#include "Streak.h"

class CParticleRenderer;
class CParticleSystemComponent;
class CStreakComponent;
class CParticleEditor;

class CParticleManager
{
public:
	CParticleManager();
	~CParticleManager();

	bool Init();
	ID_T(CParticleEmitter) AcquireParticleEmitter(const char* aFilePath);
	ID_T(CParticleEmitter) AcquireParticleEmitter(CParticleEmitter::SParticleData aParticleData);
	void ReleaseParticleEmitter(ID_T(CParticleEmitter) aParticleEmitterID);

	ID_T(CStreak) AcquireStreak(const char* aFilePath);
	void ReleaseStreak(ID_T(CStreak) aStreakID);

private:
	friend CParticleRenderer;
	friend CParticleSystemComponent;
	friend CStreakComponent;
	friend CParticleEditor;

	ID_T(CParticleEmitter) CParticleManager::LoadParticleData(const char * aFilePath);
	CParticleEmitter* GetParticleEmitter(ID_T(CParticleEmitter) aParticleEmitterID);
	void LoadParticleDataFromFile(CParticleEmitter::SParticleData& aParticleData, const char* aFilePath);
	std::map<std::string, ID_T(CParticleEmitter)> myParticleDataCache;
	ObjectPool<CParticleEmitter> myParticleEmitters;

	CStreak* GetStreak(ID_T(CStreak) aStreakID);
	ObjectPool<CStreak> myStreaks;
};


#pragma once

#include "GrowingArray.h"
#include "Vector.h"

#include "Streak.h"
#include "VertexDataWrapper.h"

class CVertexShader;
class CPixelShader;
class CGeometryShader;
class CConstantBuffer;
class CGraphicsStateManager;

struct SParticleSystemRenderCommand;
struct SStreakRenderCommand;

class CParticleRenderer
{
public:
	CParticleRenderer();
	~CParticleRenderer();

	bool Init();

	void RenderParticles(
		CGraphicsStateManager& aStateManager,
		const CommonUtilities::GrowingArray<SParticleSystemRenderCommand>& aParticleSystemRenderCommands,
		CConstantBuffer& aCameraBuffer,
		const CommonUtilities::CFrustum& aFrustum
	);
	void RenderStreaks(
		CGraphicsStateManager& aStateManager,
		const CommonUtilities::GrowingArray<SStreakRenderCommand>& aStreakRenderCommands,
		CConstantBuffer& aCameraBuffer,
		const CommonUtilities::CFrustum& aFrustum
	);

private:
	bool InitVertexBuffer();

	CVertexShader* myVertexShader;
	CPixelShader* myPixelShader;
	CGeometryShader* myParticleGeometryShader;
	CGeometryShader* myStreakGeometryShader;

	constexpr static unsigned int MaxBufferSize = 512;
	SVertexDataWrapper myVertexData;
};


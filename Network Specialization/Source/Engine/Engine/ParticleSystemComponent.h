#pragma once

class CParticleEmitter;
class CParticleManager;
class CEngine;
class CGraphicsPipeline;
class CComponentSystem;
class CParticleEditor;

#include "Transform.h"
#include "Component.h"
#include "Bounds.h"
#include <GrowingArray.h>

class CParticleSystemComponent : public CComponent
{
public:
	CParticleSystemComponent();
	~CParticleSystemComponent();

	enum EParticleState
	{
		EParticleState_Play,
		EParticleState_Paused,
		EParticleState_Stopped,
		EParticleState_Count
	};

	struct SParticleBufferData
	{
		CommonUtilities::Vector4f myPosition;
		CommonUtilities::Vector4f myColor;
		CommonUtilities::Vector2f mySize;
		float myRotation;
	};

	struct SComponentData
	{
		const char* myFilePath = nullptr;
	};


	void PlayInstant();
	void Play();
	void Pause();
	void Stop();

	bool IsPlaying();

private:
	friend CEngine;
	friend CGraphicsPipeline;
	friend CComponentSystem;
	friend CParticleEditor;

	void Init();
	void Init(const char* aFilePath);
	void Init(SComponentData aComponentData);
	void Update(const CTransform& aTransform);
	void UpdateParticles(float aDeltaTime);

	void Release() override;
	void RemovedDeadParticles(const CParticleEmitter& aParticleEmitter);

	void Reset();
	bool SpawnParticle(CParticleEmitter& aParticleEmitter, const CTransform& aTransform);

	struct SParticlePropertyData
	{
		CommonUtilities::Matrix33f myOrientation;
		CommonUtilities::Vector3f myVelocity;
		CommonUtilities::Vector3f myGravityVelocity;
		float myLifetime;
		float myRotationVelocity;
	};

	CommonUtilities::CBounds myBounds;

	CommonUtilities::GrowingArray<SParticleBufferData> myParticles;
	CommonUtilities::GrowingArray<SParticlePropertyData> myParticleProperties;

	static constexpr float ourGravityValue = 9.82f;

	EParticleState myState;
	float mySpawnTimer;
	float myTotalPlayed;
	bool myFirstUpdate = true;

	ID_T(CParticleEmitter) myParticleEmitterID;

	static CParticleManager* ourParticleManager;
	
};


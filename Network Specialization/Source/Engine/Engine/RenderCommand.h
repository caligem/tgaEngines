#pragma once

#include "Transform.h"
#include "Matrix.h"
#include "Vector.h"
#include "ObjectPool.h"
#include "ParticleSystemComponent.h"
#include "StreakComponent.h"
#include "SpriteComponent.h"
#include "Material.h"

class CModel;
class CSprite;
class CFont;
class CCanvasComponent;

// Models
struct SModelRenderCommand
{
	CommonUtilities::Matrix44f myTransform;
	CommonUtilities::Vector3f myScale;
	ID_T(CModel) myModelToRender;
	CMaterial myMaterial;
	int myAnimationID;
	float myAnimationTime;
};

// Lights
struct SDirectionalLightRenderCommand
{
	CommonUtilities::Vector3f myDirection;
	CommonUtilities::Vector3f myColor;
	float myIntensity;
	bool myCastShadows;
};
struct SPointLightRenderCommand
{
	CommonUtilities::Vector3f myPosition;
	CommonUtilities::Vector3f myColor;
	float myRange;
	float myIntensity;
	bool myCastShadows;
};
struct SSpotLightRenderCommand
{
	CommonUtilities::Vector3f myPosition;
	CommonUtilities::Vector3f myDirection;
	CommonUtilities::Vector3f myColor;
	float myRange;
	float myAngle;
	float myIntensity;
	bool myCastShadows;
};

// Particles
struct SParticleSystemRenderCommand
{
	CommonUtilities::GrowingArray<CParticleSystemComponent::SParticleBufferData> myParticles;
	CommonUtilities::Vector3f myMin;
	CommonUtilities::Vector3f myMax;
	ID_T(CParticleEmitter) myParticleEmitterID;
};
struct SStreakRenderCommand
{
	CParticleSystemComponent::SParticleBufferData myLeadingPoint;
	CommonUtilities::GrowingArray<CParticleSystemComponent::SParticleBufferData> myPoints;
	CommonUtilities::Vector3f myMin;
	CommonUtilities::Vector3f myMax;
	ID_T(CStreak) myStreakID;
};
struct SVolumetricFogRenderCommand
{
	CommonUtilities::Vector3f myMin;
	CommonUtilities::Vector3f myMax;
};

// 2D
struct SSpriteRenderCommand
{
	CSpriteComponent::SSpriteBufferData mySpriteData;
	ID_T(CSprite) mySpriteID;
};
struct STextRenderCommand
{
	CSpriteComponent::SSpriteBufferData mySpriteData;
	CommonUtilities::Vector4f myOutline;
	CommonUtilities::Vector2f myPosition;
	ID_T(CFont) myFontID;
};


struct SCanvasRenderCommand
{
	ID_T(CCanvasComponent) myCanvasComponentID;
	CommonUtilities::Vector2f myScale;
	float myAlpha;
};
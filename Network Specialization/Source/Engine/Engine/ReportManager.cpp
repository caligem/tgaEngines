#include "stdafx.h"
#ifndef _RETAIL
#include "ReportManager.h"
#include "InputManager.h"
#include "IEngine.h"
#include "Engine.h"
#include <iostream>
#include <string>
#include <strsafe.h>
#include "DirectXFramework.h"
#include "imgui.h"
#include "imgui_impl_dx11.h"

#pragma comment(lib,"advapi32.lib") 

CReportManager::CReportManager()
{
}


CReportManager::~CReportManager()
{
}

bool CReportManager::Init()
{
	myShouldRender = false;
	ResetValues();
	myFilePaths[EDisciplines::LD] = L"Bugs\\LD\\";
	myFilePaths[EDisciplines::SP] = L"Bugs\\SP\\";
	myFilePaths[EDisciplines::SG] = L"Bugs\\SG\\";
	myFilePaths[EDisciplines::TA] = L"Bugs\\TA\\";

	return true;
} 

void CReportManager::Update()
{
	if (myShouldSaveReport)
	{
		SaveReportFile();
		myShouldSaveReport = false;
		ResetValues();
	}
	if (IEngine::GetInputManager().IsKeyPressed(Input::Key_F11, 10000))
	{
		myShouldRender = !myShouldRender;
		if (myShouldRender)
		{
			ResetValues();
			IEngine::Time().SetSpeed(0);
			IEngine::GetInputManager().SetInputLevel(10000);
		}
		else
		{
			CloseWindowAndResumeGame();
		}
	}
}

void CReportManager::Render()
{
	if (myShouldRender)
	{
		ShowReportWindow();
		IEngine::Time().SetSpeed(0);
	}
}

void CReportManager::SaveReportFileNextFrame()
{
	myShouldSaveReport = true;
}

void CReportManager::ShowReportWindow()
{
	ImGui::GetIO().MouseDrawCursor = true;
	ImGui::Begin("Report Window", NULL, ImGuiWindowFlags_NoCollapse);
	
	ImGui::SetNextWindowSize(ImVec2(400, 120), ImGuiCond_Appearing);

	ImGui::Text("Report your bug!");

	ImGui::Combo("Discipline", &mySelectedDisciplineIndex, "LD\0SP\0SG\0TA\0\0");

	ImGui::InputText("Bug Description", myInputBuffer, sizeof(myInputBuffer) / sizeof(*myInputBuffer));

	if (ImGui::Button("Save Report"))
	{
		SaveReportFileNextFrame();
		CloseWindowAndResumeGame();
	}
	ImGui::SameLine();
	if (ImGui::Button("Cancel"))
	{
		CloseWindowAndResumeGame();
	}

	ImGui::End();
}

void CReportManager::CloseWindowAndResumeGame()
{
	myShouldRender = false;
	IEngine::Time().SetSpeed(1);
	IEngine::GetInputManager().SetInputLevel();
}

void CReportManager::ResetValues()
{
	mySelectedDisciplineIndex = 0;
	memset(myInputBuffer, 0, sizeof(myInputBuffer));
}

void CReportManager::CreateFilePath(const std::string& strPathAndFile)
{
	std::string strPath(strPathAndFile);
	std::string strCurrent;
	int nStart, nStart1, nStart2;

	// Create the path //
	while (strPath.length())
	{
		nStart1 = (int)strPath.find('/'); if (nStart1 == std::string::npos) nStart1 = INT_MAX;
		nStart2 = (int)strPath.find('\\'); if (nStart2 == std::string::npos) nStart2 = INT_MAX;
		nStart = CommonUtilities::Min(nStart1, nStart2);
		if (nStart == INT_MAX) break;
		strCurrent += strPath.substr(0, nStart) + "\\";
		strPath.erase(strPath.begin(), strPath.begin() + nStart + 1);
		CreateDirectoryA(strCurrent.c_str(), NULL);
	}
}

void CReportManager::SaveReportFile()
{
	SYSTEMTIME stLocalTime;
	GetLocalTime(&stLocalTime);

	WCHAR folderTime[MAX_PATH];

	StringCchPrintf(folderTime, MAX_PATH, L"%04d%02d%02d-%02d%02d%02d",
		stLocalTime.wYear, stLocalTime.wMonth, stLocalTime.wDay,
		stLocalTime.wHour, stLocalTime.wMinute, stLocalTime.wSecond);

	std::wstring folderName = myFilePaths[mySelectedDisciplineIndex] + L"Bug" + folderTime + L"\\";

	std::string folderNameString(folderName.begin(), folderName.end());
	CreateFilePath(folderNameString);

	std::ofstream output(folderNameString + "bugDescription.txt");
	DWORD dwBufferSize = MAX_PATH;
	WCHAR usernameBuffer[MAX_PATH];
	GetUserName(usernameBuffer, &dwBufferSize);
	std::wstring usernameWString(usernameBuffer);
	std::string username(usernameWString.begin(), usernameWString.end());
	output << "User:\n" << username << '\n' << '\n';
	output << "Bug Description:" << '\n';
	output << myInputBuffer;
	output.close();

	std::wstring screenShotFileName = folderName;
	screenShotFileName += L"Screenshot.jpg";

	IEngine::GetEngine().SaveScreenShot(screenShotFileName);

	ENGINE_LOG(CONCOL_VALID, "BugReport Saved!");
	myShouldRender = false;
}

#endif
#pragma once

#ifndef _RETAIL
class CReportManager
{
	enum EDisciplines
	{
		LD,
		SP,
		SG,
		TA,
		Count
	};

public:
	CReportManager();
	~CReportManager();

	bool Init();
	void Update();
	void Render();

private:
	void ResetValues();
	void CloseWindowAndResumeGame();
	void ShowReportWindow();

	void CreateFilePath(const std::string& strPathAndFile);
	void SaveReportFile();
	void SaveReportFileNextFrame();

	bool myShouldRender;
	int mySelectedDisciplineIndex;
	char myInputBuffer[256];
	std::array<std::wstring, EDisciplines::Count> myFilePaths;

	bool myShouldSaveReport = false;
};

#else

class CReportManager
{
public:
	CReportManager() {};
	~CReportManager() {};

	bool Init() { return true; }
	void Shutdown() {}
	void Update() {}
	void NewFrame() {}
	void Render() {}
private:
};

#endif

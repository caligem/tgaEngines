#include "stdafx.h"
#include "ScriptComponent.h"
#include "ScriptManager.h"
#include "IWorld.h"
#include "GameObject.h"

CScriptComponent::CScriptComponent()
{
	myShouldUpdate = false;
}

CScriptComponent::~CScriptComponent()
{
}

void CScriptComponent::Init(const SComponentData aComponentData)
{
	myScript = IWorld::Script().AquireLuaScript(this, aComponentData.myFileName);
	CGameObject go;
	go.PointToObject(myGameObjectDataID, mySceneID);
	const int ID = go.GetAccesID();
 	IWorld::Script().CallFunction(myScript, "Init", ID, aComponentData.myTriggerID);
}

ID_T(CLuaScript) CScriptComponent::GetScriptID()
{
	return myScript;
}

void CScriptComponent::ShouldUpdate(const bool aBool)
{
	myShouldUpdate = aBool;
}

void CScriptComponent::Update()
{
	if (myShouldUpdate)
	{

	}
}

void CScriptComponent::Release()
{
}

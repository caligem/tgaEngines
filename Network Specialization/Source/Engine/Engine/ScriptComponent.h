#pragma once
#include "Component.h"

class CLuaScript;
class CScriptManager;
class CGameObject;

class CScriptComponent : public CComponent
{
public:
	CScriptComponent();
	~CScriptComponent();

	struct SComponentData
	{
		const char* myFileName;
		const short myTriggerID;
	};

	void Init(const SComponentData aComponentData);
	ID_T(CLuaScript) GetScriptID();
	void ShouldUpdate(const bool aBool);
	void Update();

	virtual void Release() override;

private:
	bool myShouldUpdate = false;
	ID_T(CLuaScript) myScript;
};


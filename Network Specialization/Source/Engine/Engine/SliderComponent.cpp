#include "stdafx.h"
#include "SliderComponent.h"
#include "GameObject.h"
#include "ScriptManager.h"

CSliderComponent::CSliderComponent()
{
}


CSliderComponent::~CSliderComponent()
{
}

void CSliderComponent::Init(SComponentData aComponentData)
{
	Reset();

	CGameObject tempGameObject;
	tempGameObject.PointToObject(myGameObjectDataID, mySceneID);

	myBackground = tempGameObject.AddComponent<CSpriteComponent>({ aComponentData.backgroundFileName });
	myFill = tempGameObject.AddComponent<CSpriteComponent>({ aComponentData.fillFileName });
	myKnob = tempGameObject.AddComponent<CSpriteComponent>({ aComponentData.knobFileName });

	myBackground->SetPivot({ 0.5f, 0.5f });
	myFill->SetPivot({ 0.0f, 0.0f });
	myKnob->SetPivot({ 0.5f, 0.5f });
	myKnob->SetTint(myTints.myNormalTint);

	myCurrentValueInPercent = aComponentData.startValue;
	myMinValue = aComponentData.minValue;
	myMaxValue = aComponentData.maxValue;
}

void CSliderComponent::SetCanvasLuaScript(ID_T(CLuaScript)* aLuaScriptID)
{
	myCanvasLuaScript = aLuaScriptID;
	CallLuaFunctionOnSliderChange();
}

void CSliderComponent::SetPosition(const CommonUtilities::Vector2f& aPosition)
{
	myPosition = aPosition;
	myBackground->SetPosition(myPosition);
	SetMinMaxPosition();
}

void CSliderComponent::SetTints(const SButtonTints & aTints)
{
	myTints = aTints;
	myKnob->SetTint(myTints.myNormalTint);
}

void CSliderComponent::SetSize(const CommonUtilities::Vector2f & aBackgroundSizeRelativeToScreen)
{
	myBackgroundSizeRelativeToScreen = aBackgroundSizeRelativeToScreen;
	myBackground->SetScaleRelativeToScreen(myBackgroundSizeRelativeToScreen);
	myFill->SetPosition(myPosition - myBackgroundSizeRelativeToScreen / 2.f);
	SetMinMaxPosition();
}

void CSliderComponent::SetKnobSize(const CommonUtilities::Vector2f & aKnobSizeRelativeToScreen)
{
	myKnobSizeRelativeToScreen = aKnobSizeRelativeToScreen;
	myKnob->SetScaleRelativeToScreen(myKnobSizeRelativeToScreen);
	SetBounds();
}

void CSliderComponent::SetCurrentValue(const float aPercent)
{
	myCurrentValueInPercent = aPercent;
	SetKnobPositionDependingOnCurrentValue();
	CallLuaFunctionOnSliderChange();
}

void CSliderComponent::SetMinMaxValue(float aMinValue, float aMaxValue)
{
	float value = myMinValue + (myMaxValue - myMinValue) * myCurrentValueInPercent;
	myMinValue = aMinValue;
	myMaxValue = aMaxValue;
	myCurrentValueInPercent = value / myMaxValue;
	SetKnobPositionDependingOnCurrentValue();
}

void CSliderComponent::SetKnobPositionDependingOnCurrentValue()
{
	float knobOffset = (myMaxPosition - myMinPosition) * myCurrentValueInPercent;
	CommonUtilities::Vector2f knobPos = { myMinPosition + knobOffset, myPosition.y };
	myKnob->SetPosition(knobPos);
	myFill->SetScaleRelativeToScreen({ myBackgroundSizeRelativeToScreen.x * myCurrentValueInPercent, myBackgroundSizeRelativeToScreen.y });
	myFill->SetUVScale({ myCurrentValueInPercent, 1.f });
}

void CSliderComponent::SetMinMaxPosition()
{
	myMinPosition = myPosition.x - myBackgroundSizeRelativeToScreen.x / 2.f;
	myMaxPosition = myPosition.x + myBackgroundSizeRelativeToScreen.x / 2.f;
	SetBounds();
	SetKnobPositionDependingOnCurrentValue();
}

void CSliderComponent::SetBounds()
{
	float halfKnobHeight = myKnobSizeRelativeToScreen.y / 2.f;
	float halfKnobWidth = myKnobSizeRelativeToScreen.x / 2.f;
	CommonUtilities::Vector2f min = {myMinPosition - halfKnobWidth, myPosition.y - halfKnobHeight};
	CommonUtilities::Vector2f max = {myMaxPosition + halfKnobWidth, myPosition.y + halfKnobHeight};
	myBounds.SetMinMax(min, max);
}

void CSliderComponent::CallLuaFunctionOnSliderChange()
{
	float value = myMinValue + (myMaxValue - myMinValue) * myCurrentValueInPercent;

	if (myCanvasLuaScript == nullptr)
	{
		ENGINE_LOG(CONCOL_ERROR, "myLuaScript == nullptr ------>> CSliderComponent::SetKnobPositionDependingOnCurrentValue")
	}
	else
	{
		IEngine::GetScriptManager().CallFunction(*myCanvasLuaScript, "OnSliderChange", GetName().c_str(), value);
	}
}

void CSliderComponent::Reset()
{
	myShouldBeRendered = true;

	myMinPosition = 0.f;
	myMaxPosition = 0.f;
	myBackgroundSizeRelativeToScreen = { 0.f, 0.f };
	myTints = CSliderComponent::SButtonTints();
	myMinValue = 0.f;
	myMaxValue = 0.f;
	myIsDisabled = false;
	myIsPressed = false;

}

void CSliderComponent::SetDisabled(bool aIsDisabled)
{
	myIsDisabled = aIsDisabled;
	if (myIsDisabled)
	{
		myKnob->SetTint(myTints.myDisabledTint);
	}
	else
	{
		myKnob->SetTint(myTints.myNormalTint);
	}
}

void CSliderComponent::OnMouseMove(const CommonUtilities::Vector2f & aMousePosition)
{
	if (myIsPressed)
	{
		float knobOffset = CommonUtilities::Clamp(aMousePosition.x, myMinPosition, myMaxPosition) - myMinPosition;
		myCurrentValueInPercent = knobOffset / (myMaxPosition - myMinPosition);
		SetKnobPositionDependingOnCurrentValue();
		CallLuaFunctionOnSliderChange();
	}
}

void CSliderComponent::OnLeave()
{
	if (myIsDisabled) return;

	if (!myIsPressed)
	{
		myKnob->SetTint(myTints.myNormalTint);
	}
}

void CSliderComponent::OnEnter()
{
	if (myIsDisabled) return;

	if (!myIsPressed)
	{
		myKnob->SetTint(myTints.myHighlightedTint);
	}
	else
	{
		myKnob->SetTint(myTints.myPressedTint);
	}
}

void CSliderComponent::OnMouseDown(const CommonUtilities::Vector2f &)
{
	if (myIsDisabled) return;

	myKnob->SetTint(myTints.myPressedTint);
	myIsPressed = true;
}

bool CSliderComponent::OnMouseUp(const CommonUtilities::Vector2f & aMousePosition)
{
	if (myIsDisabled) return false;

	if (myBounds.Contains(aMousePosition))
	{
		myKnob->SetTint(myTints.myHighlightedTint);
		myIsPressed = false;
		return true;
	}
	else
	{
		myKnob->SetTint(myTints.myNormalTint);
		myIsPressed = false;
		return false;
	}
}

void CSliderComponent::FillRenderCommands(CDoubleBuffer<SSpriteRenderCommand>& aSpriteRenderCommandsBuffer, CDoubleBuffer<STextRenderCommand>& aTextRenderCommandsBuffer)
{
	myBackground->FillRenderCommands(aSpriteRenderCommandsBuffer, aTextRenderCommandsBuffer);
	myFill->FillRenderCommands(aSpriteRenderCommandsBuffer, aTextRenderCommandsBuffer);
	myKnob->FillRenderCommands(aSpriteRenderCommandsBuffer, aTextRenderCommandsBuffer);
}

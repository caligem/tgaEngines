#pragma once
#include "Component.h"
#include "UIElement.h"

class CSliderComponent : public CComponent, public CUIElement
{
public:
	CSliderComponent();
	~CSliderComponent();

	struct SComponentData
	{
		const char* backgroundFileName;
		const char* fillFileName;
		const char* knobFileName;

		//start value between 0-1 (%)
		float startValue;
		float minValue;
		float maxValue;
	};

	void Init(SComponentData aComponentData);
	void SetCanvasLuaScript(ID_T(CLuaScript)* aLuaScriptID) override;

	void SetPosition(const CommonUtilities::Vector2f& aPosition);
	const CommonUtilities::Vector2f& GetPosition() const { return myPosition; }

	void SetTints(const SButtonTints& aTints);
	void SetSize(const CommonUtilities::Vector2f& aBackgroundSizeRelativeToScreen);
	void SetKnobSize(const CommonUtilities::Vector2f& aKnobSizeRelativeToScreen);

	void SetCurrentValue(const float aPercent);
	const float GetCurrentValue() const { return myCurrentValueInPercent; }

	void SetMinMaxValue(float aMinValue, float aMaxValue);

	void SetDisabled(bool aIsDisabled);
	bool IsDisabled() const { return myIsDisabled; }

	void OnMouseMove(const CommonUtilities::Vector2f& aMousePosition) override;
	void OnLeave() override;
	void OnEnter() override;
	void OnMouseDown(const CommonUtilities::Vector2f& /*aMousePosition*/) override;
	bool OnMouseUp(const CommonUtilities::Vector2f& aMousePosition) override;

	void FillRenderCommands(CDoubleBuffer<SSpriteRenderCommand>& aSpriteRenderCommandsBuffer, CDoubleBuffer<STextRenderCommand>& aTextRenderCommandsBuffer) override;
private:
	void SetKnobPositionDependingOnCurrentValue();
	void SetMinMaxPosition();
	void SetBounds();
	void CallLuaFunctionOnSliderChange();
	void Reset();
	SButtonTints myTints;

	CommonUtilities::Vector2f myPosition;
	CommonUtilities::Vector2f myBackgroundSizeRelativeToScreen;
	CommonUtilities::Vector2f myKnobSizeRelativeToScreen;

	CSpriteComponent* myBackground;
	CSpriteComponent* myFill;
	CSpriteComponent* myKnob;

	float myMinPosition;
	float myMaxPosition;
	float myMinValue;
	float myMaxValue;
	float myCurrentValueInPercent;
	
	bool myIsDisabled;
	bool myIsPressed;
};


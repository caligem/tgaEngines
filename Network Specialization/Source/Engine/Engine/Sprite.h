#pragma once

#include <string>
#include <Vector.h>

#include "TextureManager.h"

class C2DRenderer;
class CSpriteComponent;

class CSprite
{
public:
	CSprite();
	~CSprite();

	void Init(const std::string& aFilename);

private:
	friend C2DRenderer;
	friend CSpriteComponent;

	SRV myTexture;
	CommonUtilities::Vector2f myOriginalTextureSize;
};


#pragma once
#include "ObjectPool.h"
#include "Sprite.h"

class C2DRenderer;
class CSpriteComponent;

class CSpriteManager
{
public:
	CSpriteManager();
	~CSpriteManager();

	bool Init();
	ID_T(CSprite) AcquireSprite(const char* aTexturePath);
	void ReleaseSprite(ID_T(CSprite) aSpriteID);

private:
	friend C2DRenderer;
	friend CSpriteComponent;

	CSprite* GetSprite(ID_T(CSprite) aSpriteID);
	ObjectPool<CSprite> mySprites;
};


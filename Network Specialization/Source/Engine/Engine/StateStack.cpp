#include "stdafx.h"
#include "StateStack.h"
#include "../Game/State.h"
#include "IWorld.h"

CStateStack::CStateStack()
{    
	CState::ourStateStack = this;
	myStates.Init(4);
	myFadeState = EFadeState_Done;
	myShouldQuit = false;
}

CStateStack::~CStateStack()
{
	for (auto& ga : myStates)
	{
		for (auto* scene : ga)
		{
			scene->DestoryScene();
		}
		ga.DeleteAll();
	}
}

bool CStateStack::Init(CState * aLoadingScreenState)
{
	myLoadingScreenState = aLoadingScreenState;
	if (myLoadingScreenState != nullptr)
	{
		if (!myLoadingScreenState->Init())
		{
			return false;
		}
	}

	return true;
}

bool CStateStack::Update()
{
	if (myShouldQuit)
	{
		return false;
	}

	if (GetCurrentState() != nullptr)
	{
		float dt = IWorld::Time().GetDeltaTime();

		switch (myFadeState)
		{
		case CStateStack::EFadeState_FadingIn:
			myFadeTimer += dt;

			{
				auto oldColor = IWorld::GetFadeColor();
				oldColor.w = myFadeTimer / myFadeDuration;
				IWorld::SetFadeColor(oldColor);
			}

			if (myFadeTimer > myFadeDuration)
			{
				myFadeTimer = 0.f;
				myFadeState = EFadeState_Loading;
				if (myFinishedFadingCallback)
				{
					myFinishedFadingCallback();
				}
			}
			break;

		case CStateStack::EFadeState_FadingOut:
			myFadeTimer += dt;

			{
				auto oldColor = IWorld::GetFadeColor();
				oldColor.w = 1.f - myFadeTimer / myFadeDuration;
				IWorld::SetFadeColor(oldColor);
			}

			if (myFadeTimer > myFadeDuration)
			{
				myFadeTimer = 0.f;
				myFadeState = EFadeState_Done;
			}

			break;

		default:
			break;
		}
		int stateStatus = EDoNothing;
		if (myShowLoadingScreen && myFadeState == EFadeState_Done)
		{
			myLoadingScreenState->Update();
		}
		else
		{
			stateStatus = GetCurrentState()->Update();
		}

		if (myIsLoading)
		{
			return true;
		}

		if (stateStatus == EStateUpdate::EPop_Main)
		{
			PopMainState();
		}
		else if (stateStatus == EStateUpdate::EPop_Sub)
		{
			PopSubState();
		}
		else if (stateStatus == EStateUpdate::EPop_Sub_Push_Sub)
		{
			CState* newStateToPush = GetCurrentState()->GetNewStateToPush();
			PopSubState();
			PushSubState(newStateToPush);
		}
		else if (stateStatus == EStateUpdate::EPop_Sub_Push_Main)
		{
			CState* newStateToPush = GetCurrentState()->GetNewStateToPush();
			PopSubState();
			PushMainState(newStateToPush);
		}
		else if (stateStatus == EStateUpdate::EPop_Main_Push_Sub)
		{
			CState* newStateToPush = GetCurrentState()->GetNewStateToPush();
			PopMainState();
			PushSubState(newStateToPush);
		}
		else if (stateStatus == EStateUpdate::EPop_Main_Push_Main)
		{
// 			CState* newStateToPush = GetCurrentState()->GetNewStateToPush();
// 			LoadAsync([&]() {
// 				PopMainState();
// 				PushMainState(newStateToPush);
// 				StartFadeOut();
// 			}, true);
		}
		else if ( stateStatus == EStateUpdate::EQuitGame)
		{
			return false;
		}
		
		return !myShouldQuit;
	}
	return false;
}

void CStateStack::PushMainState(CState* aMainState)
{
	PushMainStateWithoutOnEnter(aMainState);
	GetCurrentState()->Init();
	GetCurrentState()->OnEnter();
}

void CStateStack::PushMainStateWithoutOnEnter(CState * aMainState)
{
	myIsLoading = false;
	myShowLoadingScreen = false;
	if (!myStates.Empty())
	{
		GetCurrentState()->OnLeave();
	}
	myStates.Add(CommonUtilities::GrowingArray<CState*>(1));
	myStates.GetLast().Add(aMainState);
}

void CStateStack::PushSubState(CState * aSubState)
{
	assert(myStates.Size() != 0);
	myIsLoading = false;
	myShowLoadingScreen = false;
	GetCurrentState()->OnLeave();
	myStates.GetLast().Add(aSubState);
	GetCurrentState()->OnEnter();
}

void CStateStack::PushAndPopState(CState * aMainState)
{
	myIsLoading = false;
	myShowLoadingScreen = false;
	PopMainState();
	PushMainState(aMainState);
}

void CStateStack::PopMainState()
{
	if (myFadeState == EFadeState_FadingIn)return;
	for (int i = myStates.GetLast().Size() - 1; i >= 0; --i)
	{
		myStates.GetLast().GetLast()->OnLeave();
		myStates.GetLast().GetLast()->DestoryScene();
		myStates.GetLast().DeleteCyclicAtIndex(static_cast<unsigned short>(i));
	}
	myStates.RemoveCyclicAtIndex(myStates.Size() - 1);

	if (GetCurrentState() != nullptr)
	{
		GetCurrentState()->OnEnter();
	}
	else
	{
		myShouldQuit = true;
	}
}

void CStateStack::PopSubState()
{
	if (myFadeState == EFadeState_FadingIn)return;
	if (myStates.GetLast().Size() != 0)
	{
		GetCurrentState()->OnLeave();
		myStates.GetLast().GetLast()->DestoryScene();
		myStates.GetLast().DeleteCyclicAtIndex(myStates.GetLast().Size() - 1);
		if (myStates.GetLast().Size() != 0)
		{
			myStates.GetLast().GetLast()->OnEnter();
		}
		else
		{
			PopMainState();
		}
	}
}

CState* CStateStack::GetCurrentState()
{
	if (myStates.Empty())
	{
		return nullptr;
	}

	if (myStates.GetLast().Empty())
	{
		return nullptr;
	}

	return myStates.GetLast().GetLast();
}

const CState* CStateStack::GetCurrentState() const
{
	if (myStates.Empty())
	{
		return nullptr;
	}

	if (myStates.GetLast().Empty())
	{
		return nullptr;
	}

	return myStates.GetLast().GetLast();
}

void CStateStack::StartFadeIn(std::function<void()> aCallback)
{
	myFadeTimer = 0.f;
	myFadeState = EFadeState_FadingIn;
	myFinishedFadingCallback = aCallback;
}

void CStateStack::StartFadeOut()
{
	myFadeState = EFadeState_FadingOut;
}


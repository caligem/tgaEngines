#include "stdafx.h"
#include "Streak.h"

#include "IEngine.h"
#include "TextureManager.h"

CStreak::CStreak()
{
}

CStreak::~CStreak()
{
}

bool CStreak::Init(const char* aTexture)
{
	myTexture = IEngine::GetTextureManager().CreateTextureFromFile(aTexture);
	return true;
}

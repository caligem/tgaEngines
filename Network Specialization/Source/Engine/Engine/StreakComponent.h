#pragma once

class CStreak;
class CParticleManager;
class CEngine;
class CGraphicsPipeline;
class CComponentSystem;

#include "Component.h"

#include "GrowingArray.h"
#include "Bounds.h"

#include "ParticleSystemComponent.h"

class CStreakComponent : public CComponent
{
public:
	CStreakComponent();
	~CStreakComponent();

	struct SComponentData
	{
		const char* myTexture;
	};

	void SetLifeTime(float aLifeTime) { myTime = aLifeTime; }

	void ClearStreak();
	void EmitParticle();

	void SetStartAndEndColor(const CommonUtilities::Vector4f& aStartColor, const CommonUtilities::Vector4f& aEndColor) { myStartColor = aStartColor; myEndColor = aEndColor; }
protected:
	virtual void Release() override;

private:
	friend CEngine;
	friend CGraphicsPipeline;
	friend CComponentSystem;

	struct SStreakPropertyData
	{
		float myLifetime;
		float myMaxTime;
	};

	void Init(
		const char* aTexture,
		const CommonUtilities::Vector4f& aStartColor = {1.f, 0.f, 0.f, 1.f},
		const CommonUtilities::Vector4f& aEndColor = {0.f, 1.f, 0.f, 1.f},
		float aStartSize = 0.1f,
		float aEndSize = 0.5f,
		float aTime = 2.0f,
		float aMinVertexDistance = 0.0f
	);
	void Init(const SComponentData& aComponentData);
	void Update(CommonUtilities::Vector3f aPosition);
	void UpdatePointData();
	void RemoveDeadPoints();

	CParticleSystemComponent::SParticleBufferData myLeadingPoint;
	CommonUtilities::GrowingArray<CParticleSystemComponent::SParticleBufferData> myPoints;
	CommonUtilities::GrowingArray<SStreakPropertyData> myProperties;

	CommonUtilities::Vector4f myLastSolidifiedPosition;

	CommonUtilities::Vector4f myStartColor;
	CommonUtilities::Vector4f myEndColor;
	float myStartSize;
	float myEndSize;
	float myMinVertexDistance = 0.0f;
	float myTime = 2.0f;

	CommonUtilities::CBounds myBounds;

	ID_T(CStreak) myStreakID;
	static CParticleManager* ourParticleManager;
};


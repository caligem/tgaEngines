#pragma once

#include "Matrix.h"
#include "Vector.h"

#include "GrowingArray.h"

class CGameObjectData;
class CComponentSystem;
class CGameObject;
class CEngine;

class CTransform
{
public:
	CTransform(CGameObjectData* aGameObject = nullptr);
	CTransform(const CommonUtilities::Matrix44f& aMatrix);
	~CTransform() = default;

	inline void SetPosition(const CommonUtilities::Vector3f& aPosition) { InternalSetPosition(aPosition); FireDirtyChain(); }
	inline void SetRotation(const CommonUtilities::Vector3f& aRotation) { InternalSetRotation(aRotation); FireDirtyChain(); }
	inline void SetScale(const CommonUtilities::Vector3f& aScale) { myScale = aScale; FireDirtyChain(); }
	inline void SetLookAtRotation(const CommonUtilities::Vector3f& aSource, const CommonUtilities::Vector3f& aTarget, const CommonUtilities::Vector3f& aUpVector = { 0.f, 1.f, 0.f }) { InternalSetLookAtRotation(aSource, aTarget, aUpVector); FireDirtyChain(); }
	inline void SetLookDirection(const CommonUtilities::Vector3f& aDirection, const CommonUtilities::Vector3f& aUpVector = { 0.f, 1.f, 0.f }) { SetLookAtRotation({ 0.f, 0.f, 0.f }, aDirection, aUpVector); }

	inline void Move(const CommonUtilities::Vector3f& aMovement) { InternalMove(aMovement); FireDirtyChain(); }
	inline void Rotate(const CommonUtilities::Vector3f& aRotation) { InternalRotate(aRotation); FireDirtyChain(); }
	inline void RotateAround(const CommonUtilities::Vector3f& aAxis, float aRotation) { InternalRotateAround(aAxis, aRotation); FireDirtyChain(); }
	inline void Scale(const CommonUtilities::Vector3f& aScale) { myScale.x *= aScale.x; myScale.y *= aScale.y; myScale.z *= aScale.z; FireDirtyChain(); }
	inline void LookAt(const CommonUtilities::Vector3f& aTarget, const CommonUtilities::Vector3f& aUpVector = { 0.f, 1.f, 0.f }) { InternalLookAt(aTarget, aUpVector); FireDirtyChain(); }

	inline const CommonUtilities::Matrix44f GetOrientation() const { return{ GetMatrix().myRightAxis.GetNormalized(), GetMatrix().myUpAxis.GetNormalized(), GetMatrix().myForwardAxis.GetNormalized(), GetMatrix().myPosition }; }
	inline const CommonUtilities::Matrix44f& GetMatrix() const { if (myIsDirty) { Invalidate(); } return myTransform; }
	inline const CommonUtilities::Vector3f& GetPosition() const { return GetMatrix().myPosition; }
	inline const CommonUtilities::Vector3f& GetScale() const { return myScale; }
	inline const CommonUtilities::Vector3f& GetRight() const { return GetMatrix().myRightAxis; }
	inline const CommonUtilities::Vector3f& GetUp() const { return GetMatrix().myUpAxis; }
	inline const CommonUtilities::Vector3f& GetForward() const { return GetMatrix().myForwardAxis; }

	inline const CommonUtilities::Matrix44f& GetLocalOrientation() const { return myOrientation; }
	inline const CommonUtilities::Matrix44f& GetLocalMatrix() const { if (myIsDirty) { Invalidate(); } return myLocalTransform; }
	inline const CommonUtilities::Vector3f& GetLocalPosition() const { return GetLocalMatrix().myPosition; }
	inline const CommonUtilities::Vector3f& GetLocalScale() const { return myScale; }
	inline const CommonUtilities::Vector3f& GetLocalRight() const { return GetLocalMatrix().myRightAxis; }
	inline const CommonUtilities::Vector3f& GetLocalUp() const { return GetLocalMatrix().myUpAxis; }
	inline const CommonUtilities::Vector3f& GetLocalForward() const { return GetLocalMatrix().myForwardAxis; }

	void SetParent(CTransform* aParent);
	CTransform* GetParent() { return myParent; }
	CGameObject GetGameObject();

	int GetChildCount() const { return static_cast<int>(myChildren.Size()); }
	CTransform* GetChildAt(int aIndex) { return myChildren[static_cast<unsigned short>(aIndex)]; }
		
private:
	friend CComponentSystem;
	friend CGameObject;
	friend CGameObjectData;
	friend CEngine;

	static CComponentSystem* ourComponentSystem;

	void InternalSetPosition(const CommonUtilities::Vector3f& aPosition);
	void InternalSetRotation(const CommonUtilities::Vector3f& aRotation);
	void InternalSetLookAtRotation(const CommonUtilities::Vector3f& aSource, const CommonUtilities::Vector3f& aTarget, const CommonUtilities::Vector3f& aUpVector = { 0.f, 1.f, 0.f });
	void InternalMove(const CommonUtilities::Vector3f& aMovement);
	void InternalRotate(const CommonUtilities::Vector3f& aRotation);
	void InternalRotateAround(const CommonUtilities::Vector3f& aAxis, float aRotation);
	void InternalLookAt(const CommonUtilities::Vector3f& aTarget, const CommonUtilities::Vector3f& aUpVector = { 0.f, 1.f, 0.f });
	void Invalidate() const;
	void FireDirtyChain();

	void AddChild(CTransform* aChild);
	void RemoveChild(CTransform* aChild);

	void Destroy();

	mutable CommonUtilities::Matrix44f myTransform;
	mutable CommonUtilities::Matrix44f myLocalTransform;
	mutable bool myIsDirty = true;

	CGameObjectData* myGameObjectData = nullptr;

	CommonUtilities::Matrix44f myOrientation;
	CommonUtilities::Vector3f myScale;

	CTransform* myParent = nullptr;
	CommonUtilities::GrowingArray<CTransform*> myChildren;
};


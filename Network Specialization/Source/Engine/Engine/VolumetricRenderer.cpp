#include "stdafx.h"
#include "VolumetricRenderer.h"


CVolumetricRenderer::CVolumetricRenderer()
{
}

CVolumetricRenderer::~CVolumetricRenderer()
{
}

bool CVolumetricRenderer::Init()
{
	if (!myVFXBuffer.Init(sizeof(SVFXBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create VFXBuffer");
		return false;
	}
	if (!myInstanceBuffer.Init(sizeof(SInstanceBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create InstanceBuffer");
		return false;
	}
	if (!myFogVolumeBuffer.Init(sizeof(SFogVolumeBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create FogVolumeBuffer");
		return false;
	}
	if (!myScreenBuffer.Init(sizeof(SScreenData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create myScreenBuffer");
		return false;
	}

	CShaderManager& shaderManager = IEngine::GetShaderManager();
	myVolumetricFogVertexShader = &shaderManager.GetVertexShader(L"Assets/Shaders/Volumetric/FogVolume.vs", EShaderInputLayoutType_PPFX);
	myVolumetricFogPixelShader = &shaderManager.GetPixelShader(L"Assets/Shaders/Volumetric/FogVolume.ps");

	myCube = IEngine::GetModelManager().myModelLoader.CreateCube();

	return true;
}

void CVolumetricRenderer::RenderVolumetricFog(const CommonUtilities::GrowingArray<SVolumetricFogRenderCommand>& aVolumetricFogToRender, CConstantBuffer & aCameraBuffer, const CommonUtilities::CFrustum & aFrustum)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	myVolumetricFogVertexShader->Bind();
	myVolumetricFogVertexShader->BindLayout();
	myVolumetricFogPixelShader->Bind();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);
	aCameraBuffer.SetBuffer(0, EShaderType_Pixel);

	const SVertexDataWrapper& vertexData = myCube.myVertexData[0];
	context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
	context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	
	SVFXBufferData vfxData;
	vfxData.myTotalTime = IEngine::Time().GetTotalTime();
	myVFXBuffer.SetData(&vfxData);
	myVFXBuffer.SetBuffer(3, EShaderType_Vertex);
	myVFXBuffer.SetBuffer(3, EShaderType_Pixel);

	SScreenData screenData;
	screenData.myScreenSize = IEngine::GetCanvasSize();
	myScreenBuffer.SetData(&screenData);
	myScreenBuffer.SetBuffer(1, EShaderType_Pixel);

	SInstanceBufferData instanceData;
	SFogVolumeBufferData fogVolumeData;

	for (auto& command : aVolumetricFogToRender)
	{
		if (aFrustum.CullAABB(command.myMin, command.myMax))
		{
			continue;
		}

		auto center = (command.myMax + command.myMin)*0.5f;
		instanceData.myToWorld = {
			command.myMin.x-command.myMax.x, 0.f, 0.f, 0.f,
			0.f, command.myMax.y-command.myMin.y, 0.f, 0.f,
			0.f, 0.f, command.myMax.z-command.myMin.z, 0.f,
			center.x, center.y, center.z, 1.f
		};
		myInstanceBuffer.SetData(&instanceData);
		myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

		fogVolumeData.myMin = command.myMin;
		fogVolumeData.myMax = command.myMax;
		myFogVolumeBuffer.SetData(&fogVolumeData);
		myFogVolumeBuffer.SetBuffer(2, EShaderType_Pixel);

		context->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);
	}

	myVolumetricFogVertexShader->Unbind();
	myVolumetricFogVertexShader->UnbindLayout();
	myVolumetricFogPixelShader->Unbind();
}

#pragma once

#include "VFXBufferDataWrapper.h"
#include "ConstantBuffer.h"

class CVertexShader;
class CPixelShader;

class CVolumetricRenderer
{
public:
	CVolumetricRenderer();
	~CVolumetricRenderer();

	bool Init();

	void RenderVolumetricFog(
		const CommonUtilities::GrowingArray<SVolumetricFogRenderCommand>& aModelsToRender,
		CConstantBuffer& aCameraBuffer,
		const CommonUtilities::CFrustum& aFrustum
	);

private:
	struct SInstanceBufferData
	{
		CommonUtilities::Matrix44f myToWorld;
	};
	struct SFogVolumeBufferData
	{
		CommonUtilities::Vector4f myMin;
		CommonUtilities::Vector4f myMax;
	};
	struct SScreenData
	{
		CommonUtilities::Vector2f myScreenSize;
		CommonUtilities::Vector2f myTrash;
	};
	
	CConstantBuffer myVFXBuffer;
	CConstantBuffer myInstanceBuffer;
	CConstantBuffer myFogVolumeBuffer;
	CConstantBuffer myScreenBuffer;

	CModel::SModelData myCube;

	CVertexShader* myVolumetricFogVertexShader;
	CPixelShader* myVolumetricFogPixelShader;
};


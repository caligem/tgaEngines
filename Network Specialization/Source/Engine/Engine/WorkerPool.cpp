#include "stdafx.h"
#include "WorkerPool.h"

CWorkerPool::CWorkerPool()
{
	myNumJobsInProgress = 0;
}

CWorkerPool::~CWorkerPool()
{
}

void CWorkerPool::Init(int aNumberOfThreads)
{
	myWorkers.resize(static_cast<unsigned short>(aNumberOfThreads));
	for (int i = 0; i < aNumberOfThreads; ++i)
	{
		myWorkers[i].Init(this);
	}
}

void CWorkerPool::Destroy()
{
	RemoveAllJobs();
	Wait();
	
	for (CWorker& worker : myWorkers)
	{
		worker.Stop();
	}
}

void CWorkerPool::DoWork(const std::function<void()>& aJob)
{
	std::unique_lock<std::mutex> lock(myMutex);
	myJobs.push_back(aJob);
	++myNumJobsInProgress;
}

void CWorkerPool::Wait()
{
	while ((myNumJobsInProgress > 0))
	{
		std::this_thread::yield();
	}
}

void CWorkerPool::RemoveAllJobs()
{
	std::unique_lock<std::mutex> lock(myMutex);
	myJobs.clear();
}

bool CWorkerPool::GetWork(std::function<void()>& aFunctionPtr)
{
	std::unique_lock<std::mutex> lock(myMutex);

	if (myJobs.empty())
	{
		return false;
	}

	aFunctionPtr = myJobs.front();
	myJobs.pop_front();

	return true;
}

#include "stdafx.h"
#include "CppScriptFunctions.h"
#include "ScriptManager.h"

#include "IWorld.h"
#include "GameWorld.h"

#include "GameState.h"
#include "MainMenuState.h"

#include "AudioManager.h"
#include "GameObject.h"

#include "Vector3.h"

CGameWorld* CCppScriptFunctions::ourGameWorld = nullptr;

CCppScriptFunctions::CCppScriptFunctions()
{
}


CCppScriptFunctions::~CCppScriptFunctions()
{
}


void CCppScriptFunctions::RegisterScriptFunctions()
{
	IWorld::Script().RegisterFunction("SetSFXVolume", SetSFXVolume, "\n"\
		"		Sets the SFX Volume\n"\
		"		A value between 0 - 100\n"\
	);
	IWorld::Script().RegisterFunction("SetMusicVolume", SetMusicVolume, "\n"\
		"		Sets the Music Volume\n"\
		"		A value between 0 - 100\n"\
	);
	IWorld::Script().RegisterFunction("SetMasterVolume", SetMasterVolume, "\n"\
		"		Sets the Master Volume\n"\
		"		A value between 0 - 100\n"\
	);
	IWorld::Script().RegisterFunction("StartScene", StartScene, "\n"\
		"		Start Scene\n"\
		"		Takes levelName of scene/level as argument\n"\
	);
	IWorld::Script().RegisterFunction("StartShowroom", StartShowroom, "\n"\
		"		Start Showroom\n"\
		"		Takes no argument\n"\
	);
	IWorld::Script().RegisterFunction("ShowCanvas", ShowCanvas, "\n"\
		"		Show a canvas\n"\
		"		Takes name of canvas as argument\n"\
	);
	IWorld::Script().RegisterFunction("HideCanvas", HideCanvas, "\n"\
		"		Hide a canvas\n"\
		"		Takes name of canvas as argument\n"\
	);
	IWorld::Script().RegisterFunction("ExitGame", ExitGame, "\n"\
		"		Exit the game\n"\
		"		No arguments\n"\
	);
	IWorld::Script().RegisterFunction("ExitToDesktop", ExitToDesktop, "\n"\
		"		Exit To Desktop\n"\
		"		No arguments\n"\
	);
	IWorld::Script().RegisterFunction("Print", Print, "\n"\
		"		Print Text\n"\
		"		A String to print to consol\n"\
	);
	IWorld::Script().RegisterFunction("debug_player", debug_player, "\n"\
		"		Debug Player\n"\
	);
	IWorld::Script().RegisterFunction("debug_serverpos", debug_serverpos, "\n"\
		"		Debug ServerPos\n"\
	);
	IWorld::Script().RegisterFunction("debug_colliders_static", debug_colliders_static, "\n"\
		"		Debug WorldColliders\n"\
	);
	IWorld::Script().RegisterFunction("debug_colliders_dynamic", debug_colliders_dynamic, "\n"\
		"		Debug PlayerColliders\n"\
	);
}

int CCppScriptFunctions::Print(lua_State* aLuaState)
{
	if (lua_isnumber(aLuaState, -1))
	{
		float aNumber = static_cast<float>(lua_tonumber(aLuaState, -1));
		std::cout << aNumber << std::endl;
		char buffer[64];
		snprintf(buffer, sizeof buffer, "%f", aNumber);
		SCRIPT_LOG(CONCOL_DEFAULT, buffer);
	}
	else if (lua_isstring(aLuaState, -1))
	{
		std::cout << lua_tostring(aLuaState, -1) << std::endl;
	}
	return 0;
}

int CCppScriptFunctions::debug_player(lua_State *)
{
	ourGameWorld->GetStateStack().GetCurrentState()->ToggleDebug(EDebugToggles::Player);
	return 0;
}

int CCppScriptFunctions::debug_serverpos(lua_State *)
{
	ourGameWorld->GetStateStack().GetCurrentState()->ToggleDebug(EDebugToggles::ServerPositions);
	return 0;
}

int CCppScriptFunctions::debug_colliders_static(lua_State *)
{
	ourGameWorld->GetStateStack().GetCurrentState()->ToggleDebug(EDebugToggles::StaticColliders);
	return 0;
}

int CCppScriptFunctions::debug_colliders_dynamic(lua_State *)
{
	ourGameWorld->GetStateStack().GetCurrentState()->ToggleDebug(EDebugToggles::DynamicColliders);
	return 0;
}

int CCppScriptFunctions::SetSFXVolume(lua_State* aLuaState)
{
	if (lua_isnumber(aLuaState, -1))
	{
		float aVolume = static_cast<float>(lua_tonumber(aLuaState, -1));
		AM.SetVolume(aVolume, AudioChannel::SoundEffects);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for SetSFXVolume is not a number");
	}
	return 0;
}

int CCppScriptFunctions::SetMusicVolume(lua_State* aLuaState)
{
	if (lua_isnumber(aLuaState, -1))
	{
		float aVolume = static_cast<float>(lua_tonumber(aLuaState, -1));
		AM.SetVolume(aVolume, AudioChannel::Music);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for SetMusicVolume is not a number");
	}
	return 0;
}

int CCppScriptFunctions::SetMasterVolume(lua_State* aLuaState)
{
	if (lua_isnumber(aLuaState, -1))
	{
		float aVolume = static_cast<float>(lua_tonumber(aLuaState, -1));
		AM.SetVolume(aVolume, AudioChannel::Master);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for SetMasterVolume is not a number");
	}
	return 0;
}

int CCppScriptFunctions::StartScene(lua_State* aLuaState)
{
	if (lua_isstring(aLuaState, -1))
	{
		std::string levelName = "Assets/Levels/";
		levelName.append(lua_tostring(aLuaState, -1));

		levelName.append(".json");
		CGameState* gameState = new CGameState(levelName);
		ourGameWorld->GetStateStack().PushMainState(gameState);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for StartScene is not a string");
	}
	return 0;
}

int CCppScriptFunctions::StartShowroom(lua_State*)
{
	ourGameWorld->GetStateStack().GetCurrentState()->SetShouldPushState(true);
	return 0;
}

int CCppScriptFunctions::ShowCanvas(lua_State * aLuaState)
{
	if (lua_isstring(aLuaState, -1))
	{
		std::string canvasName = lua_tostring(aLuaState, -1);
		ourGameWorld->GetStateStack().GetCurrentState()->ShowCanvas(canvasName);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for ShowCanvas is not a string");
	}
	return 0;
}

int CCppScriptFunctions::HideCanvas(lua_State * aLuaState)
{
	if (lua_isstring(aLuaState, -1))
	{
		std::string canvasName = lua_tostring(aLuaState, -1);
		ourGameWorld->GetStateStack().GetCurrentState()->HideCanvas(canvasName);
	}
	else
	{
		SCRIPT_LOG(CONCOL_ERROR, "Argument for HideCanvas is not a string");
	}
	return 0;
}

int CCppScriptFunctions::ExitToDesktop(lua_State*)
{
	IWorld::GameQuit();
	return 0;
}

int CCppScriptFunctions::ExitGame(lua_State*)
{
	ourGameWorld->GetStateStack().GetCurrentState()->SetShouldPop(true);
	return 0;
}
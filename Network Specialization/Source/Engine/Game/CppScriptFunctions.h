#pragma once
#include <vector>

class CGameWorld;
class CGameObject;
class CGameState;
struct lua_State;

class CCppScriptFunctions
{
public:
	CCppScriptFunctions();
	~CCppScriptFunctions();

	static void RegisterScriptFunctions();
private:
	friend CGameWorld;
	friend CGameState;
	static int SetSFXVolume(lua_State* aLuaState);
	static int SetMusicVolume(lua_State* aLuaState);
	static int SetMasterVolume(lua_State* aLuaState);
	static int StartScene(lua_State* aLuaState);
	static int StartShowroom(lua_State*);
	static int ShowCanvas(lua_State* aLuaState);
	static int HideCanvas(lua_State* aLuaState);
	static int ExitToDesktop(lua_State*);
	static int ExitGame(lua_State*);
	static int Print(lua_State*);
	static int debug_player(lua_State*);
	static int debug_serverpos(lua_State*);
	static int debug_colliders_static(lua_State*);
	static int debug_colliders_dynamic(lua_State*);

	static CGameWorld* ourGameWorld;
};


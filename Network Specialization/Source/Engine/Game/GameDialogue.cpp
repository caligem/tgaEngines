#include "stdafx.h"
#include "GameDialogue.h"
#include <filesystem>
#include <iostream>
#include "IClientNetwork.h"

CGameDialogue::CGameDialogue()
{
}


CGameDialogue::~CGameDialogue()
{
}

void CGameDialogue::Init(ID_T(CScene) aSceneID)
{
	CGameObject gameObject;
	gameObject.Init(aSceneID);
	myCanvas = gameObject.AddComponent<CCanvasComponent>({ CCanvasComponent::ERenderMode_ScreenSpaceOverlay, IWorld::GetCanvasSize() });

	myTextQueues[KillFrame].Init(*myCanvas, CommonUtilities::Vector2f(0.8f, 0.15f), { 0.5f, 0.5f });
	myTextQueues[CenterEvents].Init(*myCanvas, CommonUtilities::Vector2f(0.5f, 0.25f), { 0.75f, 0.75f });
}

void CGameDialogue::Update()
{
	for (auto& textQueue : myTextQueues)
	{
		textQueue.Update();
	}
}

void CGameDialogue::PushText(ETextFrames aFrame, const std::string & aText)
{
	myTextQueues[aFrame].PushText(aText);
}
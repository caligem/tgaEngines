#pragma once
#include "TextQueue.h"

class CGameDialogue
{
public:
	enum ETextFrames
	{
		CenterEvents,
		KillFrame,
		Count
	};

	CGameDialogue();
	~CGameDialogue();

	void Init(ID_T(CScene) aSceneID);
	void Update();
	void PushText(ETextFrames aFrame, const std::string& aText);
private:
	CCanvasComponent* myCanvas;
	CTextQueue myTextQueues[ETextFrames::Count];
};


#include "stdafx.h"
#include "GameState.h"
#include "IWorld.h"
#include "Random.h"
#include "FileWatcher.h"
#include "AudioManager.h"
#include "CppScriptFunctions.h"

#include "StateStack.h"
#include "JsonUtility.h"
#include "CameraDataWrapper.h"
#include "ICollision.h"

#include <filesystem>
#include "GameCollision.h"

CGameState::CGameState(const std::string& aLevelToLoad)
	: myLevelToLoad(aLevelToLoad)
	, myIsInited(false)
	, myScreenShake(0)
	, myPlayer(nullptr)
	, myShouldSlowdown(false)
	, myGameStateServerTimer(0.f)
{
}


CGameState::~CGameState()
{
}

bool CGameState::Init()
{
	for (auto& debug : myDebugToggles)
	{
		debug = false;
	}
	
	myIsFading = false;
	myRespawnFade = 0.f;

	myShouldSwapScene = false;
	myShowPauseMenu = false;

	mySceneID = IWorld::GetSceneManager().CreateScene();

	myPlayers.reserve(16);

	InitUI();

	myMainCamera.Init(mySceneID);
	myMainCamera.AddComponent<CCameraComponent>({
		29.18f,
		static_cast<float>(IWorld::GetWindowSize().x / IWorld::GetWindowSize().y),
		0.1f, 1000.f
	})->SetAsActiveCamera();

	CommonUtilities::Vector3f cameraPos = {0.f, 0.f, 0.f};
	cameraPos.z -= myCameraOffset;
	myMainCamera.GetTransform().SetPosition(cameraPos);

	IWorld::GetSceneManager().GetSceneAt(mySceneID)->SetSkybox("Assets/CubeMaps/blackSkybox.dds");

	LoadFromFile(myLevelToLoad);

	myProjectileManager.SetSceneID(mySceneID);
	myProjectileManager.Init();

	myIsInited = true;
	return true;
}

EStateUpdate CGameState::Update()
{
	HandlePauseMenu();

	if (!IClientNetwork::IsConnectedToMainServer())
	{
		printf("Lost Connection To Server, returning to main menu.");
		myShouldPop = true;
	}

	if (!myIsInited)
	{
		return EDoNothing;
	}

	if (myShowPauseMenu)
	{
		IWorld::SetFadeColor({ 0.f, 0.f, 0.f, 0.f });
	}

	if (myShouldSlowdown)
	{
		if (mySlowdownTimer <= 0.f)
		{
			IWorld::Time().SetSpeed(0.05f);
		}
		else
		{
			mySlowdownTimer -= IWorld::Time().GetRealDeltaTime();
			float percent = mySlowdownTimer / mySlowdownMaxTime;
			percent = CommonUtilities::Clamp(percent, 0.3f, 0.8f);
			IWorld::Time().SetSpeed(percent);
		}
	}

	CalculateMousePosInWorld();

	UpdateInput();
	myGameStateServerTimer += IWorld::Time().GetDeltaTime();
	UpdatePlayers(IWorld::Time().GetDeltaTime());
	myProjectileManager.Update(IWorld::Time().GetDeltaTime());
	UpdateHealthPacks();
	UpdateCamera();
	CheckCollision();

	DebugRender();

	myGameDialogue.Update();

	if (myShouldPop)
	{
		return EPop_Main;
	}

	return EStateUpdate::EDoNothing;
}

void CGameState::OnEnter()
{
	IWorld::HideCursor();
	RegisterMessages();

	CNetMessageGameServerConnection* joinMessage = IClientNetwork::CreateMessage<CNetMessageGameServerConnection>();
	joinMessage->SetPlayerConnectionState(CNetMessageGameServerConnection::Join);
	IClientNetwork::Send(joinMessage);

	SetActiveScene();

	myShouldPop = false;
}

void CGameState::OnLeave()
{
	IWorld::ShowCursor();
	UnregisterMessages();

	CNetMessageGameServerConnection* leaveMessage = IClientNetwork::CreateMessage<CNetMessageGameServerConnection>();
	leaveMessage->SetPlayerConnectionState(CNetMessageGameServerConnection::Leave);
	IClientNetwork::Send(leaveMessage);

	auto oldColor = IWorld::GetFadeColor();
	oldColor.x = 0.0f;
	oldColor.y = 0.0f;
	IWorld::SetFadeColor(oldColor);
}

void CGameState::ForceUnpauseGame()
{
	myShowPauseMenu = false;
	IWorld::Input().SetInputLevel(0);
}

void CGameState::UnpauseGame()
{
	myShowPauseMenu = false;
	IWorld::Input().SetInputLevel(0);
}


void CGameState::HandlePauseMenu()
{
	if (IWorld::Input().IsKeyPressed(Input::Key_Escape, 1))
	{
		myShouldPop = true;
		//myShowPauseMenu = !myShowPauseMenu;
		//if (myShowPauseMenu)
		//{
		//	PauseGame();
		//}
		//else
		//{
		//	UnpauseGame();
		//}
	}
}

void CGameState::PauseGame()
{
	IWorld::Input().SetInputLevel(1);
}

void CGameState::HandlePlayerStatus(CNetMessage* aMessage, CAddressWrapper&)
{
	CNetMessagePlayerStatus* message = static_cast<CNetMessagePlayerStatus*>(aMessage);
	if (message->GetPlayerStatus() == CNetMessagePlayerStatus::PlayerJoined)
	{
		AddNewPlayer(message);
		myGameDialogue.PushText(CGameDialogue::CenterEvents, "Player Joined: " + message->GetPlayerName());
	}
	else if (message->GetPlayerStatus() == CNetMessagePlayerStatus::PlayerLeft)
	{
		RemovePlayerByID(message->GetPlayerID());
	}
	else if (message->GetPlayerStatus() == CNetMessagePlayerStatus::PlayerExists)
	{
		AddNewPlayer(message);

		if (message->GetPlayerID() == IClientNetwork::GetMyID())
		{
			myPlayer = &myPlayers.back();

			const CommonUtilities::Vector3f& playerPos = myPlayer->GetTransform().GetPosition();
			CommonUtilities::Vector3f cameraPos = playerPos;
			cameraPos.z = -myCameraOffset;
			myMainCamera.GetTransform().SetPosition(cameraPos);
			myPlayer->SetShouldUsePrediction(true);
		}
	}
}

void CGameState::AddNewPlayer(CNetMessagePlayerStatus* message)
{
	myPlayers.emplace_back();
	CPlayer& newPlayer = myPlayers.back();
	newPlayer.Init(mySceneID, message->GetPosition());

	const std::string playerName = message->GetPlayerName();
	const unsigned short playerID = message->GetPlayerID();
	char hp = message->GetPlayerHealth();

	newPlayer.SetHealth(hp);
	newPlayer.SetPlayerID(playerID);
	newPlayer.SetPlayerName(playerName);
	newPlayer.AddInGameUIElement(myInGameUI);

#ifndef _DEBUG
	myScoreScreen.AddPlayer(playerID, playerName);
#else
	myScoreScreen.AddPlayer(playerID, playerName + ": " + std::to_string(playerID));
#endif
}

void CGameState::HandlePlayerSnapshot(CNetMessage * aMessage, CAddressWrapper &)
{
	CNetMessagePlayerSnapshot* snapshot = static_cast<CNetMessagePlayerSnapshot*>(aMessage);
	for (auto& playerData : snapshot->GetPlayerData())
	{
		CPlayer* player = GetPlayerByID(playerData.playerID);
		if (player)
		{
			if (snapshot->GetTimeStamp() > player->GetLatestServerPositionTimestamp())
			{
				player->SetTargetPosition(playerData.playerPosition, snapshot->GetTimeStamp() - IClientNetwork::GetPing());
			}
		}
	}
}

void CGameState::CreateHealthPacks(CNetMessage * aMessage, CAddressWrapper &)
{
	CNetMessageCreateHealthPack* message = static_cast<CNetMessageCreateHealthPack*>(aMessage);
	for (auto& healthPack : message->GetHealthPacks())
	{
		myHealthPacks.emplace_back();
		CHealthPack& newHealthPack = myHealthPacks.back();
		newHealthPack.Init(mySceneID);
		newHealthPack.SetID(healthPack.id);
		newHealthPack.SetActive(!healthPack.isTaken);
		newHealthPack.SetPosition(healthPack.position);
	}
}

void CGameState::HandleHealthPackStatus(CNetMessage * aMessage, CAddressWrapper &)
{
	CNetMessageHealthPackUpdate* message = static_cast<CNetMessageHealthPackUpdate*>(aMessage);
	for (auto& locHealthPack : myHealthPacks)
	{
		if (message->GetHealthpackID() == locHealthPack.GetID())
		{
			CNetMessageHealthPackUpdate::EHealthpackState state = message->GetHealthpackState();

			if (state == CNetMessageHealthPackUpdate::EHealthpackState::Taken)
			{
				locHealthPack.SetActive(false);
			}
			else if(state == CNetMessageHealthPackUpdate::EHealthpackState::Respawned)
			{
				locHealthPack.SetActive(true);
			}
			break;
		}
	}
}

void CGameState::SpawnProjectiles(CNetMessage * aMessage, CAddressWrapper &)
{
	CNetMessageCreateProjectile* message = static_cast<CNetMessageCreateProjectile*>(aMessage);
	unsigned short shooterID = message->GetShooterID();
	CPlayer* shooter = GetPlayerByID(shooterID);
	if (shooter)
	{
		myProjectileManager.FireProjectile(shooter->GetPosition(), message->GetDirection(), shooterID);
	}
}

void CGameState::HandlePlayerHealthStatus(CNetMessage * aMessage, CAddressWrapper &)
{
	CNetMessagePlayerHealthStatus* message = static_cast<CNetMessagePlayerHealthStatus*>(aMessage);

	CNetMessagePlayerHealthStatus::EHealthStatus status = message->GetHealthStatus();
	char healthValue = message->GetHealthValue();

	for (auto& player : myPlayers)
	{
		if (message->GetPlayerID() == player.GetPlayerID())
		{
			if (status == CNetMessagePlayerHealthStatus::EHealthStatus::Heal)
			{
				player.Heal(healthValue);
			}
			else if (status == CNetMessagePlayerHealthStatus::EHealthStatus::TakeDamage)
			{
				player.TakeDamage(healthValue);
			}
			else if (status == CNetMessagePlayerHealthStatus::EHealthStatus::Respawn)
			{
				player.Respawn(healthValue, message->GetRespawnPosition());
			}
		}
	}
}

void CGameState::PrintFrag(CNetMessage * aMessage, CAddressWrapper &)
{
	CNetMessagePlayerFrag* fragMessage = static_cast<CNetMessagePlayerFrag*>(aMessage);
	
	CPlayer* killer = GetPlayerByID(fragMessage->GetKillerID());
	CPlayer* victim = GetPlayerByID(fragMessage->GetVictimID());

	std::string fragText;

	if (killer)
	{
		myScoreScreen.AddKill(killer->GetPlayerID());
		fragText.append(killer->GetPlayerName());
	}
	else
	{
		fragText.append("disconnected player");
	}

	fragText.append(" -> ");

	if (victim)
	{
		myScoreScreen.AddDeath(victim->GetPlayerID());
		fragText.append(victim->GetPlayerName());
	}
	else
	{
		fragText.append("disconnected player");
	}

	myGameDialogue.PushText(CGameDialogue::KillFrame, fragText);
}

void CGameState::UpdateGameTimer(CNetMessage* aMessage, CAddressWrapper&)
{
	CNetMessageGameTimer* timerMessage = static_cast<CNetMessageGameTimer*>(aMessage);
	float timerInSeconds = timerMessage->GetTimer();
	int timerSeconds = static_cast<int>(timerInSeconds) % 60;
	int timerMinutes = static_cast<int>(timerInSeconds / 60.f);

	if (timerMinutes <= 0 && timerSeconds <= 10)
	{
		myGameTimer->SetTint({ 1.f, 0.f, 0.f, 1.f });
	}

	std::string timer = std::to_string(timerMinutes) + ":" + std::to_string(timerSeconds);
	myGameTimer->SetText(timer);
}

void CGameState::SetStats(CNetMessage* aMessage, CAddressWrapper&)
{
	CNetMessageSendStats* message = static_cast<CNetMessageSendStats*>(aMessage);
	for (auto& stats : message->GetPlayerStats())
	{
		myScoreScreen.SetStats(stats.playerID, stats.playerKills, stats.playerDeaths);
	}
}
void CGameState::ResetGame(CNetMessage* aMessage, CAddressWrapper&)
{
	CNetMessageResetGame* message = static_cast<CNetMessageResetGame*>(aMessage);
	myScoreScreen.Reset();
	myScoreScreen.CanToggleScoreScreen(true);
	myScoreScreen.Hide();

	if (myGameTimer)
	{
		myGameTimer->SetTint({ 1.f, 1.f, 1.f, 1.f });
	}

	myProjectileManager.ClearAllBullets();

	for (auto& data : message->GetResetData())
	{
		CPlayer* player = GetPlayerByID(data.playerID);
		if (player)
		{
			player->Reset(data.playerSpawnPosition);
		}
	}

	for (auto& healthpack : myHealthPacks)
	{
		healthpack.SetActive(true);
	}

	IWorld::Time().SetSpeed(1.f);
	myShouldSlowdown = false;
}

void CGameState::ShowWinscreen(CNetMessage* aMessage, CAddressWrapper &)
{
	CNetMessageShowWinscreen* message = static_cast<CNetMessageShowWinscreen*>(aMessage);
	mySlowdownMaxTime = message->GetSlowDownTime();
	mySlowdownTimer = mySlowdownMaxTime;
	myScoreScreen.Show();
	myScoreScreen.CanToggleScoreScreen(false);
	myShouldSlowdown = true;
}

void CGameState::HandlePredictedProjectileAnswer(CNetMessage * aMessage, CAddressWrapper &)
{
	CNetMessagePredictedProjectile* message = static_cast<CNetMessagePredictedProjectile*>(aMessage);
	myPlayer->ResetShootCooldown();
	myProjectileManager.DestroyPredictedProjectile(message->GetPredictedProjectileID());
}

void CGameState::UpdatePlayers(float aDeltaTime)
{
	for (auto& player : myPlayers)
	{
		player.Update(aDeltaTime);
	}
}

void CGameState::UpdateInput()
{
	if (IWorld::Input().IsKeyPressed(Input::Key_Tab))
	{
		myScoreScreen.Show();
	}
	if (IWorld::Input().IsKeyReleased(Input::Key_Tab))
	{
		myScoreScreen.Hide();
	}

	if (!myPlayer)
	{
		return;
	}
	if (myPlayer->IsDead())
	{
		return;
	}

	if (IWorld::Input().IsKeyPressed(Input::Key_A))
	{
		SendInput(CNetMessagePlayerInput::LeftDown);
		myPlayer->AddMovement(CShared_Player::EMovement::Left);
#ifdef DEBUG_INPUT
		NETWORK_LOG(CONCOL_BLUE, "Left Key Down time: %d", static_cast<int>(IWorld::Time().GetRealTotalTimeInMS()));
		myLeftKeyDownTimer = static_cast<int>(IWorld::Time().GetRealTotalTimeInMS());
#endif
	}
	if (IWorld::Input().IsKeyPressed(Input::Key_D))
	{
		SendInput(CNetMessagePlayerInput::RightDown);
		myPlayer->AddMovement(CShared_Player::EMovement::Right);
#ifdef DEBUG_INPUT
		NETWORK_LOG(CONCOL_BLUE, "Right Key Down time: %d", static_cast<int>(IWorld::Time().GetRealTotalTimeInMS()));
		myRightKeyDownTimer = static_cast<int>(IWorld::Time().GetRealTotalTimeInMS());
#endif
	}
	if (IWorld::Input().IsKeyReleased(Input::Key_A))
	{
		SendInput(CNetMessagePlayerInput::LeftUp);
		myPlayer->StopMovement(CShared_Player::EMovement::Left);
#ifdef DEBUG_INPUT
		NETWORK_LOG(CONCOL_VALID, "Left Key Up time: %d", static_cast<int>(IWorld::Time().GetRealTotalTimeInMS()));
		NETWORK_LOG(CONCOL_WARNING, "Left key downtime : %d", static_cast<int>(IWorld::Time().GetRealTotalTimeInMS()) - myLeftKeyDownTimer);
#endif
	}
	if (IWorld::Input().IsKeyReleased(Input::Key_D))
	{
		SendInput(CNetMessagePlayerInput::RightUp);
		myPlayer->StopMovement(CShared_Player::EMovement::Right);
#ifdef DEBUG_INPUT
		NETWORK_LOG(CONCOL_VALID, "Right Key Up time: %d", static_cast<int>(IWorld::Time().GetRealTotalTimeInMS()));
		NETWORK_LOG(CONCOL_WARNING, "Right key downtime : %d", static_cast<int>(IWorld::Time().GetRealTotalTimeInMS()) - myRightKeyDownTimer);
#endif
	}
	if (IWorld::Input().IsKeyPressed(Input::Key_Space))
	{
		SendInput(CNetMessagePlayerInput::Jump);
		myPlayer->Jump();
	}

	if (IWorld::Input().IsButtonDown(Input::Button_Left))
	{
		if (myPlayer->CanShoot())
		{
			const CommonUtilities::Vector2f direction = (CommonUtilities::Vector2f(myMouseTargetPosition.x, myMouseTargetPosition.y) - myPlayer->GetPosition()).GetNormalized();
			unsigned short predictedProjectileID = myProjectileManager.PredictFire(direction, myPlayer->GetPosition(), myPlayer->GetPlayerID());
			SendShootEvent(direction, predictedProjectileID);
			myPlayer->Shoot();
		}
	}
}

void CGameState::SendInput(CNetMessagePlayerInput::EPlayerInput aInput)
{
	CNetMessagePlayerInput* message = IClientNetwork::CreateMessage<CNetMessagePlayerInput>();
	message->SetPlayerInput(aInput);
	IClientNetwork::Send(message);
}

void CGameState::UpdateHealthPacks()
{
	for (auto& healthPack : myHealthPacks)
	{
		healthPack.Update();
	}
}

void CGameState::CalculateMousePosInWorld()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Matrix44f viewProjInv = CommonUtilities::Matrix44f::Inverse(IWorld::GetSavedCameraBuffer().myViewProjection);

	CommonUtilities::Vector2f mousePos = {
		input.GetMousePosition().x / IWorld::GetWindowSize().x,
		input.GetMousePosition().y / IWorld::GetWindowSize().y
	};
	mousePos.x = mousePos.x * 2.f - 1.f;
	mousePos.y = (1.f - mousePos.y) * 2.f - 1.f;

	CommonUtilities::Vector4f rayOrigin = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 0.f, 1.f) * viewProjInv;
	rayOrigin /= rayOrigin.w;
	CommonUtilities::Vector4f rayEnd = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 1.f, 1.f) * viewProjInv;
	rayEnd /= rayEnd.w;

	CommonUtilities::Vector3f rayDir = rayEnd - rayOrigin;
	rayDir.Normalize();

	CommonUtilities::Vector3f normal = { 0.f, 0.f, 1.f };

	float denom = normal.Dot(rayDir);

	CommonUtilities::Vector3f p0l0 = -CommonUtilities::Vector3f(rayOrigin);

	float t = p0l0.Dot(normal) / denom;

	myMouseTargetPosition = CommonUtilities::Vector3f(rayOrigin) + rayDir*t;
}

void CGameState::CheckCollision()
{
	myProjectileManager.CheckCollisionVSWorld(myWorldColliders);

	for (auto& player : myPlayers)
	{
		if (!player.IsDead())
		{
			myProjectileManager.CheckCollisionVSPlayer(player);
		}
	}

	if (myPlayer)
	{
		IGameCollision::PlayerVSWorldCollision(*myPlayer, myWorldColliders);
	}
}

void CGameState::DebugRender()
{
	if (myDebugToggles[EDebugToggles::Player])
	{
		std::string playerAmount = "Players Online: " + std::to_string(myPlayers.size());
		IWorld::DrawDebugText(playerAmount);

		for (auto& player : myPlayers)
		{
			std::string playerInfo = "PlayerName: " + player.GetPlayerName() + " | "
				"ID (" + std::to_string(player.GetPlayerID()) + ") | "
				"Position (" + std::to_string(player.GetPosition().x) + "," + std::to_string(player.GetPosition().y) + ")";
			IWorld::DrawDebugText(playerInfo);
		}
	}

	if (myDebugToggles[EDebugToggles::ServerPositions])
	{
		for (auto& player : myPlayers)
		{
			player.RenderServerPositions();
		}
	}
	if (myDebugToggles[EDebugToggles::StaticColliders])
	{
		for (auto& collider : myWorldColliders)
		{
			IWorld::DrawDebugWireCube(collider.GetPosition(), collider.GetBounds());
		}
	}
	if (myDebugToggles[EDebugToggles::DynamicColliders])
	{
		for (auto& player : myPlayers)
		{
			float radius = player.GetCollider().radius;
			IWorld::DrawDebugWireCircle(player.GetPosition(), { radius, radius, radius });
		}
		
		myProjectileManager.RenderColliders();
	}
}

void CGameState::SendShootEvent(const CommonUtilities::Vector2f& aDirection, unsigned short aPredictedProjectileID)
{
	if (myPlayer)
	{
		CNetMessagePlayerShoot* message = IClientNetwork::CreateMessage<CNetMessagePlayerShoot>();
		message->SetDirection(aDirection);
		message->SetProjectileID(aPredictedProjectileID);
		IClientNetwork::Send(message);
	}
}

CPlayer * CGameState::GetPlayerByID(unsigned short aPlayerID)
{
	for (auto& player : myPlayers)
	{
		if (player.GetPlayerID() == aPlayerID)
		{
			return &player;
		}
	}
	return nullptr;
}

void CGameState::RemovePlayerByID(unsigned short aPlayerID)
{
	for (size_t i = 0; i < myPlayers.size(); ++i)
	{
		if (myPlayers[i].GetPlayerID() == aPlayerID)
		{
			myScoreScreen.RemovePlayer(aPlayerID);
			myPlayers[i].RemoveFromUI(myInGameUI);
			myGameDialogue.PushText(CGameDialogue::CenterEvents, "Player Left: " + myPlayers[i].GetPlayerName());
			myPlayers.erase(myPlayers.begin() + i);
			break;
		}
	}

	for (auto& player : myPlayers)
	{
		if (player.GetPlayerID() == IClientNetwork::GetMyID())
		{
			myPlayer = &player;
		}
	}
}

void CGameState::InitUI()
{
	CGameObject inGameUIObject;
	inGameUIObject.Init(mySceneID);
	myInGameUI = inGameUIObject.AddComponent<CCanvasComponent>({ CCanvasComponent::ERenderMode_ScreenSpaceOverlay, IWorld::GetCanvasSize() });
	
	myCrosshair = myInGameUI->AddUIElement<CSpriteComponent>({ "Assets/Sprites/InGame/crosshair.dds" });
	myCrosshair->SetPivot({ 0.5f, 0.5f });
	myCrosshair->SetPosition(myMouseTargetPosition);
	myCrosshair->SetTint({ 1.f, 1.f, 1.f, 1.f });
	myCrosshair->SetScale({ 0.5f, 0.5f });

	myGameTimer = myInGameUI->AddUIElement<CTextComponent>({ "Assets/Fonts/Mono/Mono" });
	myGameTimer->SetPosition({ 0.5f, 0.1f });
	myGameTimer->SetPivot({ 0.5f, 0.5f });
	myGameTimer->SetTint({ 1.f, 1.f, 1.f, 1.f });

	myGameDialogue.Init(mySceneID);

	myScoreScreen.Init(mySceneID);
}

void CGameState::UpdateCamera()
{
	if (myPlayer)
	{
		const CommonUtilities::Vector3f& playerPos = myPlayer->GetTransform().GetPosition();
		const CommonUtilities::Vector3f& cameraOffsetDirection = (myMouseTargetPosition - playerPos).GetNormalized();
		float cameraOffsetLength = cameraOffsetDirection.Length();
		cameraOffsetLength = CommonUtilities::Clamp<float>(0.f, 1.f, cameraOffsetLength);
		const CommonUtilities::Vector3f cameraTargetPosition = playerPos + (cameraOffsetDirection* cameraOffsetLength);
		CTransform& cameraTransform = myMainCamera.GetTransform();
		CommonUtilities::Vector3f cameraPos = CommonUtilities::Lerp(cameraTransform.GetPosition(), cameraTargetPosition, IWorld::Time().GetDeltaTime() * 5.f);
		cameraPos.z = -myCameraOffset;
		myMainCamera.GetTransform().SetPosition(cameraPos);
	}
	if (myCrosshair)
	{
		Input::CInputManager& input = IWorld::Input();
		CommonUtilities::Vector2f mousePos = {
			input.GetMousePosition().x / IWorld::GetWindowSize().x,
			input.GetMousePosition().y / IWorld::GetWindowSize().y
		};
		myCrosshair->SetPosition(mousePos);
	}
}

void CGameState::UnregisterMessages()
{
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessagePlayerStatus>();
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessagePlayerSnapshot>();
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessagePlayerHealthStatus>();
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessageCreateHealthPack>();
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessageHealthPackUpdate>();
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessageCreateProjectile>();
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessagePlayerFrag>();
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessageGameTimer>();
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessageSendStats>();
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessageShowWinscreen>();
	IClientNetwork::UnRegisterDoUnPackWorkCallback<CNetMessagePredictedProjectile>();
}

void CGameState::RegisterMessages()
{
	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessagePlayerStatus>(
		std::bind(&CGameState::HandlePlayerStatus, this, std::placeholders::_1, std::placeholders::_2));

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessagePlayerSnapshot>(
		std::bind(&CGameState::HandlePlayerSnapshot, this, std::placeholders::_1, std::placeholders::_2));

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessageCreateHealthPack>(
		std::bind(&CGameState::CreateHealthPacks, this, std::placeholders::_1, std::placeholders::_2));

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessageHealthPackUpdate>(
		std::bind(&CGameState::HandleHealthPackStatus, this, std::placeholders::_1, std::placeholders::_2));

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessageCreateProjectile>(
		std::bind(&CGameState::SpawnProjectiles, this, std::placeholders::_1, std::placeholders::_2));

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessagePredictedProjectile>(
		std::bind(&CGameState::HandlePredictedProjectileAnswer, this, std::placeholders::_1, std::placeholders::_2));

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessagePlayerHealthStatus>(
		std::bind(&CGameState::HandlePlayerHealthStatus, this, std::placeholders::_1, std::placeholders::_2));

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessagePlayerFrag>(
		std::bind(&CGameState::PrintFrag, this, std::placeholders::_1, std::placeholders::_2));

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessageGameTimer>(
		std::bind(&CGameState::UpdateGameTimer, this, std::placeholders::_1, std::placeholders::_2));

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessageSendStats>(
		std::bind(&CGameState::SetStats, this, std::placeholders::_1, std::placeholders::_2));

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessageResetGame>(
		std::bind(&CGameState::ResetGame, this, std::placeholders::_1, std::placeholders::_2));

	IClientNetwork::RegisterDoUnPackWorkCallback<CNetMessageShowWinscreen>(
		std::bind(&CGameState::ShowWinscreen, this, std::placeholders::_1, std::placeholders::_2));
}
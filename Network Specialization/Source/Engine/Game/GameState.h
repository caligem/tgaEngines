#pragma once
#include "State.h"
#include "GameDialogue.h"
#include "IClientNetwork.h"
#include "Player.h"
#include "HealthPack.h"
#include "Projectile.h"
#include "ScoreScreen.h"
#include "ProjectileManager.h"

class CGameState : public CState
{
public:
	CGameState(const std::string& aLevelToLoad);
	~CGameState();
	
	bool Init() override;
	EStateUpdate Update() override;

	void OnEnter() override;
	void OnLeave() override;

	void ForceUnpauseGame() override;
	void UnpauseGame() override;

	void ToggleDebug(EDebugToggles aDebugToggle) override { myDebugToggles[aDebugToggle] = !myDebugToggles[aDebugToggle]; }

private:
	void InitUI();

	void HandlePauseMenu();
	void PauseGame();

	void DebugRender();

	void AddNewPlayer(CNetMessagePlayerStatus* message);

	void RegisterMessages();
	void UnregisterMessages();

	void HandlePlayerStatus(CNetMessage* aMessage, CAddressWrapper&);
	void HandlePlayerSnapshot(CNetMessage* aMessage, CAddressWrapper&);
	void HandlePlayerHealthStatus(CNetMessage* aMessage, CAddressWrapper&);
	void CreateHealthPacks(CNetMessage* aMessage, CAddressWrapper&);
	void HandleHealthPackStatus(CNetMessage* aMessage, CAddressWrapper&);
	void SpawnProjectiles(CNetMessage* aMessage, CAddressWrapper&);
	void PrintFrag(CNetMessage* aMessage, CAddressWrapper&);
	void UpdateGameTimer(CNetMessage* aMessage, CAddressWrapper&);
	void SetStats(CNetMessage* aMessage, CAddressWrapper&);
	void ResetGame(CNetMessage* aMessage, CAddressWrapper&);
	void ShowWinscreen(CNetMessage*, CAddressWrapper&);
	void HandlePredictedProjectileAnswer(CNetMessage* aMessage, CAddressWrapper&);

	CPlayer* GetPlayerByID(unsigned short aPlayerID);
	void RemovePlayerByID(unsigned short aPlayerID);

	void UpdateCamera();
	void UpdatePlayers(float aDeltaTime);
	void UpdateInput();
	void UpdateHealthPacks();

	void SendInput(CNetMessagePlayerInput::EPlayerInput aInput);
	void SendShootEvent(const CommonUtilities::Vector2f& aDirection, unsigned short aPredictedProjectileID);

	void CalculateMousePosInWorld();
	void CheckCollision();
	CScoreScreen myScoreScreen;
	CGameDialogue myGameDialogue;
	CCanvasComponent* myInGameUI;
	CSpriteComponent* myCrosshair;
	CTextComponent* myGameTimer;
	CProjectileManager myProjectileManager;

	std::vector<CPlayer> myPlayers;
	std::vector<CHealthPack> myHealthPacks;

	CommonUtilities::Vector3f myMouseTargetPosition;

	std::string myLevelToLoad;
	std::string myNextLevel;

	CommonUtilities::Vector3f myCameraLookAtTarget;

	CPlayer* myPlayer;
	float myRespawnFade;
	float myScreenShake;
	float mySlowdownTimer;
	float mySlowdownMaxTime;
	int myRightKeyDownTimer;
	int myLeftKeyDownTimer;

	static constexpr float GameStateServerTick = 1 / 144.f;
	float myGameStateServerTimer;
	static constexpr float myCameraOffset = 20.f;
	bool myShowPauseMenu;
	bool myIsInited;
	bool myShouldSwapScene;
	bool myIsFading;
	bool myDebugToggles[EDebugToggles::Count];
	bool myShouldSlowdown;
};


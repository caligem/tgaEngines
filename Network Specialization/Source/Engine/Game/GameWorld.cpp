#include "stdafx.h"
#include "GameWorld.h"

#include "IWorld.h"
#include "AudioManager.h"

// -- devstates
#include "MemoryLeakTestState.h"
// -- 

#include "SplashScreenState.h"
#include "MainMenuState.h"
#include "GameState.h"

#include <Windows.h>
#include <FileDialog.h>
#include <JsonDocument.h>

#include "CommandLineManager.h"

CGameWorld* CGameWorld::ourGameWorld = nullptr;

CGameWorld::CGameWorld()
{
	CCppScriptFunctions::ourGameWorld = this;
}

CGameWorld::~CGameWorld()
{
}

void CGameWorld::Init()
{
	ourGameWorld = this;
	GAMEPLAY_LOG(CONCOL_VALID, "Gameworld init!");

	AM.LoadAudioBank("Audio/Master Bank");
	AM.SetVolume(100.f, AudioChannel::Music);
	AM.SetVolume(100.f, AudioChannel::SoundEffects);
	AM.SetVolume(100.f, AudioChannel::Master);

	myStateStack.Init(nullptr);

 	
 	if (CommonUtilities::CCommandLineManager::HasParameter("-startScene"))
 	{
		std::string str = "Assets/Levels/";
		str += CommonUtilities::CCommandLineManager::GetArgument("-startScene", 0);
		str += ".json";
 		CGameState* mainMenu = new CGameState(str);
 		myStateStack.PushMainStateWithoutOnEnter(mainMenu);
		mainMenu->Init();
		mainMenu->OnEnter();
 	}
 	else
 	{
 		CMainMenuState* mainMenu = new CMainMenuState();
 		myStateStack.PushMainStateWithoutOnEnter(mainMenu);
		mainMenu->Init();
		mainMenu->OnEnter();
 	}

	//CSplashScreenState* splashScreenFFG = new CSplashScreenState();
	//splashScreenFFG->Init("Assets/Sprites/SplashScreen/ffgLogo.dds");
	//myStateStack.PushSubState(splashScreenFFG);
	//
	//CSplashScreenState* splashScreenTga = new CSplashScreenState();
	//splashScreenTga->Init("Assets/Sprites/SplashScreen/tgaLogo.dds");
	//myStateStack.PushSubState(splashScreenTga);
}

void CGameWorld::Update()
{
	AM.Update();	

	//TestMemoryLeaks();

	if (!myStateStack.Update())
	{
		IWorld::GameQuit();
	}
}
CStateStack& CGameWorld::GetStateStack()
{
	return myStateStack;
}

ID_T(CScene) CGameWorld::GetCurrentSceneID()
{
	return myStateStack.GetCurrentState()->GetSceneID();
}

void CGameWorld::TestMemoryLeaks()
{
	CMemoryLeakTestState* memoryLeakState = new CMemoryLeakTestState();
	myStateStack.PushMainState(memoryLeakState);
}

#include "stdafx.h"
#include "Health.h"


CHealth::CHealth()
	: myIsDead(false)
{
}


CHealth::~CHealth()
{
}

void CHealth::Init(const char aMaxHealth)
{
	myMaxHealth = aMaxHealth;
	myCurrentHealth = myMaxHealth;
}

void CHealth::TakeDamage(const char& aDamage)
{
	myCurrentHealth -= aDamage;

	if (myCurrentHealth <= 0)
	{
		myIsDead = true;
		myCurrentHealth = 0;
	}
}

void CHealth::IncreaseHealth(const char& aIncrease)
{
	myCurrentHealth += aIncrease;

	if (myCurrentHealth > myMaxHealth)
	{
		myCurrentHealth = myMaxHealth;
	}
}

float CHealth::GetCurrentHealthInPercent()
{
	return static_cast<float>(myCurrentHealth) / static_cast<float>(myMaxHealth);
}

void CHealth::SetHealth(const char aHp)
{
	myCurrentHealth = aHp;
}

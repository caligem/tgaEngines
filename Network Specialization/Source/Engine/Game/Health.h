#pragma once

class CHealth
{
public:
	CHealth();
	~CHealth();

	void Init(const char aMaxHealth);

	inline void SetIsAlive() { myIsDead = false; }
	inline void SetIsDead() { myIsDead = true; }

	void TakeDamage(const char& aDamage);
	void IncreaseHealth(const char& aIncrease);

	inline char GetMaxHealth() const { return myMaxHealth; }
	inline char GetCurrentHealth() const { return myCurrentHealth; }
	float GetCurrentHealthInPercent();

	bool GetIsDead() const { return myIsDead; }
	void SetHealth(const char aHp);
private:
	char myMaxHealth;
	char myCurrentHealth;
	bool myIsDead;
};


#include "stdafx.h"
#include "HealthPack.h"
#include "Random.h"

CHealthPack::CHealthPack()
{
}


CHealthPack::~CHealthPack()
{
}

void CHealthPack::Init(ID_T(CScene) aSceneID)
{
	mySeed = CommonUtilities::RandomRange(0.f, 100.f);
	CGameObject::Init(aSceneID);
	GetTransform().SetPosition(myPosition);
}

void CHealthPack::Update()
{
	myPosition.y += (cosf(IWorld::Time().GetTotalTime() + mySeed) / 7.f) * IWorld::Time().GetDeltaTime() * myMovementSpeed;
	GetTransform().SetPosition(myPosition);
	if (IsActive())
	{
		IWorld::DrawDebugCube(myPosition, {mySize.x, mySize.y, mySize.x});
	}
}
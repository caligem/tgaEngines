#pragma once
#include "Shared_HealthPack.h"

class CHealthPack : public CGameObject, public CShared_HealthPack
{
public:
	CHealthPack();
	~CHealthPack();

	void Init(ID_T(CScene) aSceneID);
	void Update();
	void SetPosition(const CommonUtilities::Vector2f& aPosition) { myPosition = aPosition; }

private:
	float mySeed;
};


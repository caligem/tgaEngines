#include "stdafx.h"
#include "Interpolater.h"


CInterpolater::CInterpolater()
{
}


CInterpolater::~CInterpolater()
{
}

void CInterpolater::Update(CommonUtilities::Vector2f& aPosition)
{
	if (!mySnapshots.empty())
	{
		float interpolateTime = IWorld::Time().GetInterpolateTime();
		for (size_t i = 0; i < mySnapshots.size(); ++i)
		{
			SInterpolateData& toSnapshot = mySnapshots[i];
			if (interpolateTime < toSnapshot.targetTimeStamp)
			{
				if (i != 0)
				{					
					SInterpolateData& fromSnapshot = mySnapshots[i - 1];
					float snapShotTimeDiff = toSnapshot.targetTimeStamp - fromSnapshot.targetTimeStamp;
					float interpolateTimeDiff = interpolateTime - fromSnapshot.targetTimeStamp;
					float percent = interpolateTimeDiff / snapShotTimeDiff;
					percent = CommonUtilities::Clamp(percent, 0.f, 1.f);
					aPosition = CommonUtilities::Lerp(fromSnapshot.targetPosition, toSnapshot.targetPosition, percent);
					break;
				}
			}
		}
	}
}

void CInterpolater::PushPosition(const CommonUtilities::Vector2f & aPosition)
{
	if (mySnapshots.size() >= MaxSnapshotsInQueue)
	{
		mySnapshots.pop_front();
	}
	mySnapshots.emplace_back();
	SInterpolateData& data = mySnapshots.back();
	data.targetTimeStamp = IWorld::Time().GetServerTimer();
	data.targetPosition = aPosition;
}

void CInterpolater::RenderServerPositions()
{
	if (!mySnapshots.empty())
	{
		CommonUtilities::Vector3f pos = mySnapshots.back().targetPosition;
		IWorld::SetDebugColor({ 1.f, 0.f, 0.f, 1.f });
		IWorld::DrawDebugWireSphere(pos, { 0.4f, 0.4f, 0.0f });
	}
}

const CommonUtilities::Vector2f CInterpolater::GetLatestPosition()
{
	if (mySnapshots.empty())
	{
		return CommonUtilities::Vector2f(0.f, 0.f);
	}
	return mySnapshots.back().targetPosition;
}

const float CInterpolater::GetLatestPositionTimestamp() const
{
	if (mySnapshots.empty())
	{
		return 0.f;
	}

	return mySnapshots.back().targetTimeStamp;
}

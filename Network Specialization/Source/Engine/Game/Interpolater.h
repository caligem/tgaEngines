#pragma once
#include <deque>

class CInterpolater
{
public:
	struct SInterpolateData
	{
		CommonUtilities::Vector2f targetPosition;
		float targetTimeStamp;
	};
	CInterpolater();
	~CInterpolater();

	void Update(CommonUtilities::Vector2f& aPosition);
	void PushPosition(const CommonUtilities::Vector2f& aPosition);
	void RenderServerPositions();
	void ClearBuffer() { mySnapshots.clear(); }
	const CommonUtilities::Vector2f GetLatestPosition();
	const float GetLatestPositionTimestamp() const;
private:
	static constexpr char MaxSnapshotsInQueue = 10;
	std::deque<SInterpolateData> mySnapshots;
};


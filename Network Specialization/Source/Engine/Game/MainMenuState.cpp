#include "stdafx.h"
#include "MainMenuState.h"

#include "IWorld.h"
#include "SceneManager.h"
#include "StateStack.h"
#include "GameState.h"
#include "IClientNetwork.h"

CMainMenuState::CMainMenuState()
{
}


CMainMenuState::~CMainMenuState()
{
}

bool CMainMenuState::Init()
{
	CState::Init();
	LoadFromFile("Assets/Levels/mainMenu.json");

	IWorld::GetSceneManager().GetSceneAt(mySceneID)->SetSkybox("Assets/CubeMaps/blackSkybox.dds");
	myIsConnectedText = myCanvases["MainMenu"]->AddUIElement<CTextComponent>({ "Assets/Fonts/Mono/Mono" });
	myIsConnectedText->SetPivot({ 0.5f, 0.5f });
	myIsConnectedText->SetPosition({ 0.5f, 0.25f });

	return true;
}

EStateUpdate CMainMenuState::Update()
{
	if (IClientNetwork::IsConnectedToMainServer())
	{
		myIsConnectedText->SetTint({0.f, 1.f, 0.f, 1.f});
		myIsConnectedText->SetText("Connected To Server");
		myStartGameButton->SetIsDisabled(false);
	}
	else
	{
		myIsConnectedText->SetTint({ 1.f, 0.f, 0.f, 1.f });
		myIsConnectedText->SetText("Server Offline");
		myStartGameButton->SetIsDisabled(true);
	}

	return EStateUpdate::EDoNothing;
}

void CMainMenuState::OnEnter()
{
	myCanvases["MainMenu"]->Show();
	myStartGameButton = static_cast<CButtonComponent*>(myCanvases["MainMenu"]->GetUIElement("StartGame"));
	SetActiveScene();
}

void CMainMenuState::OnLeave()
{
}

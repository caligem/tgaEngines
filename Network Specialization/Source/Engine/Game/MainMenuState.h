#pragma once
#include "State.h"

class CMainMenuState : public CState
{
public:
	CMainMenuState();
	~CMainMenuState();

	bool Init() override;
	EStateUpdate Update() override;
	void OnEnter() override;
	void OnLeave() override;

private:
	CButtonComponent* myStartGameButton;
	CTextComponent* myIsConnectedText;
};


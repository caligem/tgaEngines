#pragma once
#include "State.h"
class CMemoryLeakTestState : public CState
{
public:
	CMemoryLeakTestState();
	~CMemoryLeakTestState();

	bool Init();
	EStateUpdate Update() override;
	void OnEnter();
	void OnLeave();

};


#include "stdafx.h"
#include "Metrics.h"

CMetrics::CMetrics()
{
}

CMetrics::~CMetrics()
{
}

void CMetrics::LoadMetricsFromFile(const std::wstring& aMetricsDoc)
{
	JsonDocument characterMetrics;
	std::string file(aMetricsDoc.begin(), aMetricsDoc.end());
	characterMetrics.LoadFile(file.c_str());
	
	if (characterMetrics.Find("myPlayerMetrics"))
	{
		auto playerObj = characterMetrics["myPlayerMetrics"];

		myPlayerMetrics.myMovementSpeed = playerObj["myMovementSpeed"].GetFloat();
		myPlayerMetrics.myMaxHealth = playerObj["myMaxHealth"].GetInt();
	}

}

#pragma once
#include "JsonUtility.h"

class CMetrics
{
	struct SPlayerMetrics
	{
		float myMovementSpeed;
		int myMaxHealth;
	} myPlayerMetrics;

	
public:
	CMetrics();
	~CMetrics();

	void LoadMetricsFromFile(const std::wstring& aMetricsDoc);
	const SPlayerMetrics& GetPlayerMetrics() const { return myPlayerMetrics; }
};


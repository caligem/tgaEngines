#include "stdafx.h"
#include "Player.h"
#include "IWorld.h"
#include "InputManager.h"

#include "AudioManager.h"
#include "Mathf.h"
#include "IClientNetwork.h"
#include "CameraDataWrapper.h"

#define CATCHUPSPEED 20.f

CPlayer::CPlayer()
	: myHealthOffset(0.2f)
{
	myShootTimer = myShootCooldown;
	myShouldUsePrediction = false;
}


CPlayer::~CPlayer()
{
}

void CPlayer::Init(ID_T(CScene) aSceneID, const CommonUtilities::Vector2f& aPosition)
{
	CGameObject::Init(aSceneID);
	GetTransform().SetPosition(aPosition);
	myPosition = aPosition;
	myPredictionHistory.AddHistory(static_cast<unsigned int>(IWorld::Time().GetServerTimerInMS()), myPosition);
	myHealth.Init(100);

	myHealEffect = AddComponent<CParticleSystemComponent>("Assets/Particles/heal.json");
	myHealEffect->Stop();

	myRespawnEffect = AddComponent<CParticleSystemComponent>("Assets/Particles/spawn.json");
	myRespawnEffect->Play();

	myHitEffect = AddComponent<CParticleSystemComponent>("Assets/Particles/playerHit.json");
	myHitEffect->Stop();

	myDeathObject.Init(aSceneID);
	myDeathEffect = myDeathObject.AddComponent<CParticleSystemComponent>("Assets/Particles/playerBlood.json");
	myDeathEffect->Stop();
}

void CPlayer::Update(float aDeltaTime)
{
	float dt = aDeltaTime;

	if (myShouldUsePrediction)
	{	
		UpdateMovement(dt);
	}
	else
	{
		myInterpolater.Update(myPosition);
	}

	GetTransform().SetPosition(myPosition);
	myCollider.position = myPosition;

	myShootTimer -= dt;
	myShootTimer = CommonUtilities::Clamp<float>(myShootTimer, 0.f, myShootCooldown);

	UpdateHealthUI();
	Render();
}

void CPlayer::Render()
{
	if (IsActive())
	{
		IWorld::DrawDebugSphere(myPosition, { 0.4f, 0.4f, 0.4f });
	}
	myPredictionHistory.RenderHistory();
}

void CPlayer::SetTargetPosition(const CommonUtilities::Vector2f& aPosition, unsigned int aTimestamp)
{
	if (myShouldUsePrediction)
	{
		myPredictionHistory.AddHistory(static_cast<unsigned int>(IWorld::Time().GetServerTimerInMS()), myPosition);

		CommonUtilities::Vector2f predictedPosAtTimestamp(myPredictionHistory.FindPosition(aTimestamp, myPosition));
		float length = (predictedPosAtTimestamp - aPosition).Length();
		CommonUtilities::Vector2f correction = (aPosition - predictedPosAtTimestamp).GetNormalized() * (length);
		myPosition = myPosition + correction;
		myPredictionHistory.ApplyCorrection(aTimestamp, correction);

		if (length > 1.f)
		{
			myPosition = aPosition;
			printf("Client Prediction error! Predicted Pos : %f, %f . Received Pos : %f, %f . Length: %f \n", predictedPosAtTimestamp.x, predictedPosAtTimestamp.y, aPosition.x, aPosition.y, length);
		}
	}
	myInterpolater.PushPosition(aPosition);
}

void CPlayer::AddInGameUIElement(CCanvasComponent* aInGameUI)
{
	if (aInGameUI)
	{
		myUIName = aInGameUI->AddUIElement<CTextComponent>({ "Assets/Fonts/Mono/Mono" });
		myUIName->SetText(myPlayerName);
		myUIName->SetScale({ 0.5f, 0.5f });
		myUIName->SetOutline({ 0.f, 0.f, 0.f, 1.f });
		myUIName->SetPivot({ 0.5f, 0.5f });
		myUIName->SetPosition({ 0.5f, 0.5f });

		myUIHealth = aInGameUI->AddUIElement<CTextComponent>({ "Assets/Fonts/Mono/Mono" });
		myUIHealth->SetText(std::to_string(myHealth.GetCurrentHealth()) + "%");
		myUIHealth->SetScale({ 0.5f, 0.5f });
		myUIHealth->SetOutline({ 0.f, 0.f, 0.f, 1.f });
		myUIHealth->SetPivot({ 0.5f, 0.5f });
		myUIHealth->SetPosition({ 0.5f, 0.5f });
	}
}

void CPlayer::TakeDamage(const char aDamage)
{
	myHealth.TakeDamage(aDamage);
	myUIHealth->SetText(std::to_string(myHealth.GetCurrentHealth()) + "%");

	if (myHitEffect)
	{
		myHitEffect->Stop();
		myHitEffect->Play();
	}

	if (myHealth.GetIsDead())
	{
		myDeathObject.GetTransform().SetPosition(myPosition);
		myDeathEffect->Play();
		SetActive(false);
	}
}

void CPlayer::Heal(const char aHeal)
{
	if (myHealEffect)
	{
		myHealEffect->Play();
	}
	myHealth.IncreaseHealth(aHeal);
	myUIHealth->SetText(std::to_string(myHealth.GetCurrentHealth()) + "%");
}

void CPlayer::UpdateHealthUI()
{
	if (myHealth.GetIsDead())
	{
		myUIHealth->SetShouldBeRendered(false);
		myUIName->SetShouldBeRendered(false);
		return;
	}

	CommonUtilities::Matrix44f viewProjection = IWorld::GetSavedCameraBuffer().myViewProjection;
	CommonUtilities::Vector4f pos = GetTransform().GetPosition();
	pos.w = 1.f;
	pos.y += 1.0f;
	
	CommonUtilities::Vector4f viewPos = pos * viewProjection;
	viewPos /= viewPos.w;
	viewPos.x = (viewPos.x + 1.f) / 2.f;
	viewPos.y = (-viewPos.y + 1.f) / 2.f;
	
	if ((viewPos.x < 0.f || viewPos.x > 1.f) || (viewPos.y < 0.f || viewPos.y > 1.f))
	{
		myUIHealth->SetShouldBeRendered(false);
		myUIName->SetShouldBeRendered(false);
	}
	else
	{
		myUIName->SetShouldBeRendered(true);
		myUIName->SetPosition({ viewPos.x, viewPos.y - 0.025f });
		myUIHealth->SetShouldBeRendered(true);
		myUIHealth->SetPosition({ viewPos.x, viewPos.y });
	}
}

void CPlayer::Respawn(char aValue, const CommonUtilities::Vector2f& aRespawnPosition)
{
	myMovement = { 0.f, 0.f };
	myIsOnGround = false;
	myPosition = aRespawnPosition;
	GetTransform().SetPosition(myPosition);

	if (myRespawnEffect)
	{
		myRespawnEffect->Play();
	}
	if (myHitEffect)
	{
		myHitEffect->Stop();
	}

	myHealth.SetHealth(aValue);
	myHealth.SetIsAlive();

	SetActive(true);

	myInterpolater.ClearBuffer();

	myUIHealth->SetText(std::to_string(myHealth.GetCurrentHealth()) + "%");
}

void CPlayer::RemoveFromUI(CCanvasComponent* aInGameUI)
{
	if (aInGameUI)
	{
		aInGameUI->RemoveUIElement(*myUIHealth);
		aInGameUI->RemoveUIElement(*myUIName);
	}
}

void CPlayer::Reset(const CommonUtilities::Vector2f & aPosition)
{
	Respawn(myHealth.GetMaxHealth(), aPosition);
}

void CPlayer::SetHealth(const char aHp)
{
	myHealth.SetHealth(aHp);
}

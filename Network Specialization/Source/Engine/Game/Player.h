#pragma once
#include "GameObject.h"
#include "Health.h"
#include "Shared_Player.h"
#include "Interpolater.h"
#include "PredictionHistory.h"

class CPlayer : public CGameObject, public CShared_Player
{
public:
	CPlayer();
	~CPlayer();

	void Init(ID_T(CScene) aSceneID, const CommonUtilities::Vector2f& aPosition);
	void Update(float aDeltaTime);
	void Render();

	void SetTargetPosition(const CommonUtilities::Vector2f& aPosition, unsigned int aTimestamp);
	void SetShouldUsePrediction(bool aShouldUsePrediction) { myShouldUsePrediction = aShouldUsePrediction; }

	void AddInGameUIElement(CCanvasComponent* aInGameUI);

	void TakeDamage(const char aDamage);
	void Heal(const char aHeal);
	void SetHealth(const char hp);
	void UpdateHealthUI();

	void Respawn(char aValue, const CommonUtilities::Vector2f& aRespawnPosition);

	inline bool IsDead() const { return myHealth.GetIsDead(); }

	void RemoveFromUI(CCanvasComponent* aInGameUI);
	void Reset(const CommonUtilities::Vector2f& aPosition);

	inline bool CanShoot() { return myShootTimer <= 0.f; }
	inline void Shoot() { myShootTimer = myShootCooldown; }
	void ResetShootCooldown() { myShootTimer = 0.f; }

	inline void RenderServerPositions() { myInterpolater.RenderServerPositions(); }
	const CommonUtilities::Vector2f GetLatestServerPosition() { return myInterpolater.GetLatestPosition(); }
	const float GetLatestServerPositionTimestamp() const { return myInterpolater.GetLatestPositionTimestamp(); }
private:
	CGameObject myDeathObject;

	CHealth myHealth;

	CInterpolater myInterpolater;
	CPredictionHistory myPredictionHistory;

	CTextComponent* myUIHealth;
	CTextComponent* myUIName;

	CParticleSystemComponent* myHealEffect;
	CParticleSystemComponent* myHitEffect;
	CParticleSystemComponent* myRespawnEffect;
	CParticleSystemComponent* myDeathEffect;

	float myHealthOffset;
	float myShootTimer;
	bool myShouldUsePrediction;
};


#include "stdafx.h"
#include "PredictionHistory.h"


CPredictionHistory::CPredictionHistory()
{
	myHistory.reserve(32);
}


CPredictionHistory::~CPredictionHistory()
{
}

void CPredictionHistory::AddHistory(unsigned int aTimestamp, const CommonUtilities::Vector2f& aPosition)
{
	if (myHistory.size() >= MaxHistory)
	{
		myHistory.erase(myHistory.begin());
	}
	myHistory.push_back({ aTimestamp, aPosition });
}

void CPredictionHistory::RenderHistory()
{
	for (auto& history : myHistory)
	{
		IWorld::SetDebugColor({ 0.f, 1.f, 0.f, 1.f });
		IWorld::DrawDebugWireSphere(history.position, { 0.2f, 0.2f, 0.f });
	}
}

CommonUtilities::Vector2f CPredictionHistory::FindPosition(unsigned int aTimestamp, const CommonUtilities::Vector2f& aCurrentPlayerPosition)
{
	for (char i = 0; i < myHistory.size(); ++i)
	{
		if (myHistory[i].timestamp >= aTimestamp)
		{
			if (i != 0)
			{
				unsigned int previousHistoryTimestamp = myHistory[i-1].timestamp;
				unsigned int nextHistoryTimestamp = myHistory[i].timestamp;
				unsigned int deltaHistoryTimestamp = nextHistoryTimestamp - previousHistoryTimestamp;
				int deltaRecievedTimstamp = nextHistoryTimestamp - aTimestamp;
				float percent = static_cast<float>(deltaRecievedTimstamp) / static_cast<float>(deltaHistoryTimestamp);
				percent = CommonUtilities::Clamp(percent, 0.f, 1.f);
				percent = 1.f - percent;
				CommonUtilities::Vector2f previousHistoryPos = myHistory[i-1].position;
				CommonUtilities::Vector2f nextHistoryPos = myHistory[i].position;
				CommonUtilities::Vector2f direction = (nextHistoryPos - previousHistoryPos).GetNormalized();
				float length = (nextHistoryPos - previousHistoryPos).Length() * percent;
				CommonUtilities::Vector2f positionAtTimestamp = previousHistoryPos + direction * length;
				return positionAtTimestamp;
			}
		}
	}

	unsigned int previousHistoryTimestamp = myHistory.back().timestamp;
	unsigned int nextHistoryTimestamp = static_cast<unsigned int>(IWorld::Time().GetRealTotalTimeInMS());
	unsigned int deltaHistoryTimestamp = nextHistoryTimestamp - previousHistoryTimestamp;
	int deltaRecievedTimstamp = nextHistoryTimestamp - aTimestamp;
	float percent = static_cast<float>(deltaRecievedTimstamp) / static_cast<float>(deltaHistoryTimestamp);
	percent = CommonUtilities::Clamp(percent, 0.f, 1.f);
	percent = 1.f - percent;
	//printf("PreviousHistoryTimestamp : %d, NextHistoryTimestamp : %d, ReceivedTimstamp : %d percent : %f \n", previousHistoryTimestamp, nextHistoryTimestamp, aTimestamp, percent);
	CommonUtilities::Vector2f previousHistoryPos = myHistory.back().position;
	CommonUtilities::Vector2f nextHistoryPos = aCurrentPlayerPosition;
	CommonUtilities::Vector2f direction = (nextHistoryPos - previousHistoryPos).GetNormalized();
	float length = (nextHistoryPos - previousHistoryPos).Length() * percent;
	CommonUtilities::Vector2f positionAtTimestamp = previousHistoryPos + direction * length;
	//printf("previousHistoryPos : %f, %f, nextHistoryPos : %f, %f, positionAtTimestamp : %f, %f \n", previousHistoryPos.x, previousHistoryPos.y, nextHistoryPos.x, nextHistoryPos.y, positionAtTimestamp.x, positionAtTimestamp.y);
	return positionAtTimestamp;
}

void CPredictionHistory::ApplyCorrection(unsigned int aTimestamp, const CommonUtilities::Vector2f& correction)
{
	aTimestamp;
	for (char i = 0; i < myHistory.size(); ++i)
	{
		myHistory[i].position += correction;
	}
}

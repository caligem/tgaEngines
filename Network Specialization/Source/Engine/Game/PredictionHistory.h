#pragma once
#include <vector>

class CPredictionHistory
{
public:
	struct SHistoryData
	{
		unsigned int timestamp;
		CommonUtilities::Vector2f position;
		CommonUtilities::Vector2f movement;
	};

	CPredictionHistory();
	~CPredictionHistory();

	void AddHistory(unsigned int aTimestamp, const CommonUtilities::Vector2f& aPosition);

	void RenderHistory();
	CommonUtilities::Vector2f FindPosition(unsigned int aTimestamp, const CommonUtilities::Vector2f& aCurrentPlayerPosition);
	void ApplyCorrection(unsigned int aTimestamp, const CommonUtilities::Vector2f& correction);
private:
	std::vector<SHistoryData> myHistory;
	static constexpr char MaxHistory = 30;
};


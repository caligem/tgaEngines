#include "stdafx.h"
#include "Projectile.h"
#include "IWorld.h"

CProjectile::CProjectile()
	: myIsDead(false)
	, myKilltimestamp(0.f)
	, myIsPredicted(false)
{
}


CProjectile::~CProjectile()
{
}

void CProjectile::Init(ID_T(CScene) aSceneID)
{
	CGameObject::Init(aSceneID);
	AddComponent<CModelComponent>({ "Assets/Models/playerProjectile/playerProjectile.fbx" });
	GetTransform().Scale({ 0.5f, 0.5f, 0.5f });
}

void CProjectile::Fire(const CommonUtilities::Vector2f& aPosition, unsigned short aProjectileIndex, const CommonUtilities::Vector2f& aDirection, const unsigned short aShooterID)
{
	myShooterID = aShooterID;
	myProjectileIndex = aProjectileIndex;

	myPosition = aPosition;
	GetTransform().SetPosition(myPosition);

	GetTransform().SetLookDirection(aDirection);
	myDirection = aDirection;

	SetActive(true);
	myIsDead = false;

	myIsPredicted = false;
}

void CProjectile::FirePredicted(unsigned short aPredictedProjectileID, const CommonUtilities::Vector2f& aPosition, unsigned short aProjectileIndex, const CommonUtilities::Vector2f& aDirection, const unsigned short aShooterID)
{
	myShooterID = aShooterID;
	myProjectileIndex = aProjectileIndex;

	myPosition = aPosition;
	GetTransform().SetPosition(myPosition);

	GetTransform().SetLookDirection(aDirection);
	myDirection = aDirection;

	SetActive(true);
	myIsDead = false;

	myIsPredicted = true;
	myPredictedProjectileID = aPredictedProjectileID;
}

void CProjectile::Update(float aDeltaTime)
{
	myPosition += myDirection * aDeltaTime * mySpeed;
	GetTransform().SetPosition(myPosition);
	myCollider.SetPosition(myPosition);
}

void CProjectile::Kill()
{
	SetActive(false);
}

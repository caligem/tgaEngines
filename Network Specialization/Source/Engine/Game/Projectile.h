#pragma once
#include "GameObject.h"
#include "Shared_Projectile.h"
#include "Interpolater.h"

class CProjectile : public CGameObject, public CShared_Projectile
{
public:
	CProjectile();
	~CProjectile();

	void Init(ID_T(CScene) aSceneID);
	void Fire(const CommonUtilities::Vector2f& aPosition, unsigned short aProjectileIndex, const CommonUtilities::Vector2f& aDirection, const unsigned short aShooterID);
	void FirePredicted(unsigned short aPredictedProjectileID, const CommonUtilities::Vector2f& aPosition, unsigned short aProjectileIndex, const CommonUtilities::Vector2f& aDirection, const unsigned short aShooterID);

	void Update(float aDeltaTime);

	const bool IsDead() const { return myIsDead; }
	void SetIsDead() { myIsDead = true; }
	void Kill();

	bool IsPredicted() const { return myIsPredicted; }
	const unsigned short GetPredictedID() { return myPredictedProjectileID; }

private:
	CommonUtilities::Vector2f myDirection;
	float myKilltimestamp;
	unsigned short myPredictedProjectileID;
	bool myIsDead;
	bool myIsPredicted;
};




#include "stdafx.h"
#include "ProjectileManager.h"
#include "ICollision.h"
#include "Player.h"

CProjectileManager::CProjectileManager()
	: CShared_ProjectileManager(IWorld::Time())
{
}


CProjectileManager::~CProjectileManager()
{
}

void CProjectileManager::FireProjectile(const CommonUtilities::Vector2f& aPosition, const CommonUtilities::Vector2f& aDirection, const unsigned short aShooterID)
{
	unsigned short projectileIndex = GetAvailablePlayerProjectileIndex();
	CProjectile& projectile = myProjectiles[projectileIndex];
	projectile.Fire(aPosition, projectileIndex, aDirection, aShooterID);
}

unsigned short CProjectileManager::PredictFire(const CommonUtilities::Vector2f aDirection, const CommonUtilities::Vector2f& aPosition, const unsigned short aShooterID)
{
	unsigned short predictedProjectileID = myAvailablePredictProjectiles.front();
	myAvailablePredictProjectiles.pop();

	unsigned short projectileIndex = GetAvailablePlayerProjectileIndex();
	CProjectile& projectile = myProjectiles[projectileIndex];
	projectile.FirePredicted(predictedProjectileID, aPosition, projectileIndex, aDirection, aShooterID);
	return predictedProjectileID;
}

void CProjectileManager::InternalInit()
{
	for (auto& projectile : myProjectiles)
	{
		projectile.Init(mySceneID);
		projectile.SetActive(false);
	}
	for (unsigned short i = 0; i < MaxPredictedProjectiles; ++i)
	{
		myAvailablePredictProjectiles.push(i);
	}
}

void CProjectileManager::HandleProjectileDeath(CProjectile& aProjectile)
{
	aProjectile.SetActive(false);
	if (aProjectile.IsPredicted())
	{
		myAvailablePredictProjectiles.push(aProjectile.GetPredictedID());
	}
	myAvailablePlayerProjectiles.push(aProjectile.GetProjectileIndex());
}

void CProjectileManager::CheckCollisionVSWorld(const std::vector<CColliderBox>& myWorldColliders)
{
	for (auto& projectile : myProjectiles)
	{
		if (projectile.IsActive())
		{
			for (auto& collider : myWorldColliders)
			{
				if (ICollision::BoxVsBox(projectile.GetCollider(), collider))
				{
					projectile.SetIsDead();
				}
			}
		}
	}
}

void CProjectileManager::CheckCollisionVSPlayer(const CPlayer & aPlayer)
{
	for (auto& projectile : myProjectiles)
	{
		if (projectile.IsActive())
		{
			if (projectile.GetShooterID() != aPlayer.GetPlayerID())
			{
				if (ICollision::CircleVSBox(aPlayer.GetCollider(), projectile.GetCollider()))
				{
					projectile.SetIsDead();
				}
			}
		}
	}
}

void CProjectileManager::DestroyPredictedProjectile(const unsigned short aPredictedProjectileID)
{
	for (auto& projectile : myProjectiles)
	{
		if (projectile.IsPredicted() && projectile.IsActive())
		{
			if (projectile.GetPredictedID() == aPredictedProjectileID)
			{
				projectile.SetIsDead();
				break;
			}
		}
	}
}

void CProjectileManager::RenderColliders()
{
	for (auto& projectile : myProjectiles)
	{
		if (projectile.IsActive())
		{
			IWorld::DrawDebugWireCube(projectile.GetPosition(), projectile.GetCollider().GetBounds());
		}
	}
}
#pragma once
#include "Shared_ProjectileManager.h"
#include "Projectile.h"

class CPlayer;
class CProjectileManager : public CShared_ProjectileManager<CProjectile>
{
public:
	CProjectileManager();
	~CProjectileManager();

	void SetSceneID(ID_T(CScene) aSceneID) { mySceneID = aSceneID; }

	void FireProjectile(const CommonUtilities::Vector2f& aPosition, const CommonUtilities::Vector2f& aDirection, const unsigned short aShooterID);
	unsigned short PredictFire(const CommonUtilities::Vector2f aDirection, const CommonUtilities::Vector2f& aPosition, const unsigned short aShooterID);

	void DestroyPredictedProjectile(const unsigned short aPredictedProjectileID);

	void RenderColliders();

	void CheckCollisionVSWorld(const std::vector<CColliderBox>& myWorldColliders);
	void CheckCollisionVSPlayer(const CPlayer& aPlayer);

private:
	void InternalInit() override;
	void HandleProjectileDeath(CProjectile& aProjectile) override;
	static constexpr unsigned short MaxPredictedProjectiles = 32;
	std::queue<unsigned short> myAvailablePredictProjectiles;
	ID_T(CScene) mySceneID;
};


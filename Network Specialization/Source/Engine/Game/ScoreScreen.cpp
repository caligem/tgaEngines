#include "stdafx.h"
#include "ScoreScreen.h"
#include "Player.h"
#include <algorithm>


CScoreScreen::CScoreScreen()
{
	myCanToggle = true;
}


CScoreScreen::~CScoreScreen()
{
}

void CScoreScreen::Init(ID_T(CScene) aSceneID)
{
	CGameObject scoreScreenObj;
	scoreScreenObj.Init(aSceneID);
	myCanvas = scoreScreenObj.AddComponent<CCanvasComponent>({ CCanvasComponent::ERenderMode_ScreenSpaceOverlay, IWorld::GetCanvasSize() });

	CSpriteComponent* background = myCanvas->AddUIElement<CSpriteComponent>({ "Assets/Sprites/SplashScreen/black.dds" });
	background->SetScaleRelativeToScreen({ 1.f, 1.f });
	background->SetTint({ 1.f, 1.f, 1.f, 0.4f });

	std::string playerTitleText = "Players      - " + std::to_string(myPlayerStats.size());

	InitRow(playerTitleText, { 0.02f, 0.1f }, { 0.2f, 0.2f }, { 0.f, 0.5f }, myPlayerList);
	InitRow("Kills", { 0.0f, 0.1f }, { 0.6f, 0.2f }, { 0.5f, 0.5f }, myKillList);
	InitRow("Deaths", { 0.0f, 0.1f }, { 0.7f, 0.2f }, { 0.5f, 0.5f }, myDeathList);
						
	myCanvas->ForceHide();

	myPlayerStats.reserve(16);
}

void CScoreScreen::AddPlayer(unsigned short aPlayerID, const std::string& aPlayerName)
{
	for (auto& stats : myPlayerStats)
	{
		if (stats.playerID == aPlayerID)
		{
			stats.name = aPlayerName;
			return;
		}
	}

	myPlayerStats.emplace_back();
	SPlayerStats& stats = myPlayerStats.back();
	stats.playerID = aPlayerID;
	stats.name = aPlayerName;

	AddNewElement(stats);
	std::string playerTitleText = "Players      - " + std::to_string(myPlayerStats.size());
	myPlayerList.titletext->SetText(playerTitleText);
}

void CScoreScreen::RemovePlayer(unsigned short aPlayerID)
{
	for (unsigned short i = 0; i < myPlayerStats.size(); i++)
	{
		if (myPlayerStats[i].playerID == aPlayerID)
		{
			RemoveElement(myPlayerStats[i]);
			myPlayerStats.erase(myPlayerStats.begin() + i);
			RepositionUI();
			break;
		}
	}
}

void CScoreScreen::SetStats(unsigned short aPlayerID, unsigned short aPlayerKills, unsigned short aPlayerDeaths)
{
	for (auto& stats : myPlayerStats)
	{
		if (stats.playerID == aPlayerID)
		{
			stats.kills = aPlayerKills;
			stats.deaths = aPlayerDeaths;

			stats.killsText->SetText(std::to_string(stats.kills));
			stats.deathsText->SetText(std::to_string(stats.deaths));

			std::string playerTitleText = "Players      - " + std::to_string(myPlayerStats.size());
			myPlayerList.titletext->SetText(playerTitleText);

			RepositionUI();

			return;
		}
	}

	myPlayerStats.emplace_back();
	SPlayerStats& stats = myPlayerStats.back();
	stats.playerID = aPlayerID;
	stats.kills = aPlayerKills;
	stats.deaths = aPlayerDeaths;

	AddNewElement(stats);

	std::string playerTitleText = "Players      - " + std::to_string(myPlayerStats.size());
	myPlayerList.titletext->SetText(playerTitleText);

	RepositionUI();
}

void CScoreScreen::AddKill(unsigned short aPlayerID)
{
	for (auto& stats : myPlayerStats)
	{
		if (stats.playerID == aPlayerID)
		{
			stats.kills++;
			stats.killsText->SetText(std::to_string(stats.kills));
			RepositionUI();
			break;
		}
	}
}

void CScoreScreen::AddDeath(unsigned short aPlayerID)
{
	for (auto& stats : myPlayerStats)
	{
		if (stats.playerID == aPlayerID)
		{
			stats.deaths++;
			stats.deathsText->SetText(std::to_string(stats.deaths));
			break;
		}
	}
}

void CScoreScreen::Show()
{
	if (myCanToggle)
	{
		myCanvas->Show();
	}
}

void CScoreScreen::Hide()
{
	if (myCanToggle)
	{
		myCanvas->Hide();
	}
}

void CScoreScreen::InitRow(const std::string& aTitleName, const CommonUtilities::Vector2f& aTitleOffset, const CommonUtilities::Vector2f& aPosition, const CommonUtilities::Vector2f& aPivot, SColumnData& aColumn)
{
	aColumn.titlePosition = aPosition;
	aColumn.titletext = myCanvas->AddUIElement<CTextComponent>({ "Assets/Fonts/Mono/Mono" });
	aColumn.titletext->SetPivot(aPivot);
	aColumn.titletext->SetPosition(aPosition);
	aColumn.titletext->SetText(aTitleName);

	aColumn.titleOffset = aTitleOffset;
	aColumn.elementOffset = { 0.f, 0.05f };
}

void CScoreScreen::AddNewElement(SPlayerStats & aPlayerStats)
{
	aPlayerStats.nameText = myCanvas->AddUIElement<CTextComponent>({ "Assets/Fonts/Mono/Mono" });
	aPlayerStats.killsText = myCanvas->AddUIElement<CTextComponent>({ "Assets/Fonts/Mono/Mono" });
	aPlayerStats.deathsText = myCanvas->AddUIElement<CTextComponent>({ "Assets/Fonts/Mono/Mono" });

	aPlayerStats.nameText->SetScale({ 0.5f, 0.5f });
	aPlayerStats.nameText->SetPivot({ 0.0f, 0.5f });
	aPlayerStats.nameText->SetText(aPlayerStats.name);

	aPlayerStats.killsText->SetScale({ 0.5f, 0.5f });
	aPlayerStats.killsText->SetPivot({ 0.5f, 0.5f });
	aPlayerStats.killsText->SetText(std::to_string(aPlayerStats.kills));

	aPlayerStats.deathsText->SetScale({ 0.5f, 0.5f });
	aPlayerStats.deathsText->SetPivot({ 0.5f, 0.5f });
	aPlayerStats.deathsText->SetText(std::to_string(aPlayerStats.deaths));

	RepositionUI();
}

void CScoreScreen::RemoveElement(SPlayerStats & aPlayerStats)
{
	myCanvas->RemoveUIElement(*aPlayerStats.nameText);
	myCanvas->RemoveUIElement(*aPlayerStats.killsText);
	myCanvas->RemoveUIElement(*aPlayerStats.deathsText);

	RepositionUI();
}

void CScoreScreen::RepositionUI()
{
	myPlayerList.nextElementPosition = myPlayerList.titlePosition;
	myPlayerList.nextElementPosition += myPlayerList.titleOffset;

	myKillList.nextElementPosition = myKillList.titlePosition;
	myKillList.nextElementPosition += myKillList.titleOffset;

	myDeathList.nextElementPosition = myDeathList.titlePosition;
	myDeathList.nextElementPosition += myDeathList.titleOffset;

	std::sort(myPlayerStats.begin(), myPlayerStats.end());

	for (auto& stats : myPlayerStats)
	{
		stats.nameText->SetPosition(myPlayerList.nextElementPosition);
		myPlayerList.nextElementPosition += myPlayerList.elementOffset;

		stats.killsText->SetPosition(myKillList.nextElementPosition);
		myKillList.nextElementPosition += myKillList.elementOffset;

		stats.deathsText->SetPosition(myDeathList.nextElementPosition);
		myDeathList.nextElementPosition += myKillList.elementOffset;
	}
}

void CScoreScreen::CanToggleScoreScreen(bool aCanToggle)
{
	myCanToggle = aCanToggle;
}

void CScoreScreen::Reset()
{
	for (auto& stats : myPlayerStats)
	{
		stats.deaths = 0;
		stats.kills = 0;
		stats.deathsText->SetText(std::to_string(stats.deaths));
		stats.killsText->SetText(std::to_string(stats.kills));
	}
}

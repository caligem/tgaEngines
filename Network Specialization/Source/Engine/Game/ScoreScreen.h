#pragma once
#include <unordered_map>
class CPlayer;

struct SPlayerStats
{
	unsigned short playerID = 0;
	std::string name;
	unsigned short kills = 0;
	unsigned short deaths = 0;

	CTextComponent* nameText;
	CTextComponent* killsText;
	CTextComponent* deathsText;

	bool operator<(const SPlayerStats& a) const
	{
		if (kills == a.kills)
		{
			return deaths < a.deaths;
		}

		return kills > a.kills;
	}
};

struct SColumnData
{
	CommonUtilities::Vector2f titlePosition;
	CommonUtilities::Vector2f titleOffset;
	CommonUtilities::Vector2f nextElementPosition;
	CommonUtilities::Vector2f elementOffset;
	CTextComponent* titletext;
};


class CScoreScreen
{

public:
	CScoreScreen();
	~CScoreScreen();

	void Init(ID_T(CScene) aSceneID);
	
	void AddPlayer(unsigned short aPlayerID, const std::string& aPlayerName);
	void RemovePlayer(unsigned short aPlayerID);

	void SetStats(unsigned short aPlayerID, unsigned short aPlayerKills, unsigned short aPlayerDeaths);

	void AddKill(unsigned short aPlayerID);
	void AddDeath(unsigned short aPlayerID);

	void Show();
	void Hide();

	void Reset();
	void CanToggleScoreScreen(bool aCanToggle);
private:
	void InitRow(const std::string& aTitleName, const CommonUtilities::Vector2f& aTitleOffset, const CommonUtilities::Vector2f& aPosition, const CommonUtilities::Vector2f& aPivot, SColumnData& aColumn);
	void AddNewElement(SPlayerStats& aPlayerStats);
	void RemoveElement(SPlayerStats& aPlayerStats);
	void RepositionUI();
	CCanvasComponent* myCanvas;
	std::vector<SPlayerStats> myPlayerStats;

	SColumnData myPlayerList;
	SColumnData myKillList;
	SColumnData myDeathList;

	bool myCanToggle;
};


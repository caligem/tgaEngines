#include "stdafx.h"
#include "State.h"

#include "StateStack.h"
#include "IWorld.h"

#include "JsonUtility.h"
#include "Random.h"
#include "AudioManager.h"

CStateStack* CState::ourStateStack = nullptr;

bool CState::Init()
{
	mySceneID = IWorld::GetSceneManager().CreateScene();

	myMainCamera.Init(mySceneID);
	myMainCamera.AddComponent<CCameraComponent>({
		29.18f,
		static_cast<float>(IWorld::GetWindowSize().x / IWorld::GetWindowSize().y),
		0.1f, 1000.f
	})->SetAsActiveCamera();
	return true;
}

void CState::SetActiveScene()
{
	IWorld::GetSceneManager().SetActiveScene(mySceneID);
}

void CState::DestoryScene()
{
	IWorld::GetSceneManager().DestroyScene(mySceneID);
}

void CState::ShowCanvas(const std::string & aCanvasName)
{
	if (myCanvases.find(aCanvasName) != myCanvases.end())
	{
		myCanvases[aCanvasName]->Show();
	}
	else
	{
		GENERAL_LOG(CONCOL_WARNING, "Cant find canvas name when trying to show canvas.");
	}
}

void CState::HideCanvas(const std::string & aCanvasName)
{
	if (myCanvases.find(aCanvasName) != myCanvases.end())
	{
		myCanvases[aCanvasName]->Hide();
	}
	else
	{
		GENERAL_LOG(CONCOL_WARNING, "Cant find canvas name when trying to hide canvas.");
	}
}

void CState::ForceShowCanvas(const std::string & aCanvasName)
{
	if (myCanvases.find(aCanvasName) != myCanvases.end())
	{
		myCanvases[aCanvasName]->ForceShow();
	}
	else
	{
		GENERAL_LOG(CONCOL_WARNING, "Cant find canvas name when trying to force show canvas.");
	}
}

void CState::ForceHideCanvas(const std::string & aCanvasName)
{
	if (myCanvases.find(aCanvasName) != myCanvases.end())
	{
		myCanvases[aCanvasName]->ForceHide();
	}
	else
	{
		GENERAL_LOG(CONCOL_WARNING, "Cant find canvas name when trying to force hide canvas.");
	}
}

void CState::LoadFromFile(const std::string& aLevelpath)
{
	RESOURCE_LOG(CONCOL_VALID, "Reading file: %s", aLevelpath.c_str());
	JsonDocument doc(aLevelpath.c_str());

	std::map<int, CGameObject> gameObjects;

	for (int i = 0; i < doc["myGameObjects"].GetSize(); ++i)
	{
		auto obj = doc["myGameObjects"][i];
		
		CGameObject gameObject;
		gameObject.Init(mySceneID, obj["myID"].GetInt());

		if (obj.Find("myIsActive"))
		{
			gameObject.SetActive(obj["myIsActive"].GetBool());
		}
		gameObjects[obj["myID"].GetInt()] = gameObject;

		gameObject.GetTransform().SetPosition(JsonToVector3f(obj["myPosition"]));
		gameObject.GetTransform().SetRotation(JsonToQuatf(obj["myRotation"]).GetEulerAngles());
		gameObject.GetTransform().SetScale(JsonToVector3f(obj["myScale"]));

		int parentID = obj["myParent"].GetInt();
		if (parentID != -1)
		{
			if (gameObjects.find(parentID) != gameObjects.end())
			{
				gameObject.GetTransform().SetParent(&gameObjects[parentID].GetTransform());
			}
		}
	}
	for (int i = 0; i < doc["myMeshFilters"].GetSize(); ++i)
	{
		auto obj = doc["myMeshFilters"][i];
		int parentID = obj["myParent"].GetInt();
		if (gameObjects.find(parentID) != gameObjects.end())
		{
			CModelComponent* modelComponent = gameObjects[parentID].AddComponent<CModelComponent>({ obj["myPath"].GetString() });
			if (modelComponent->HasAnimations())
			{
				CAnimationControllerComponent* animationControllerComponent = gameObjects[parentID].AddComponent<CAnimationControllerComponent>(modelComponent);
				animationControllerComponent->SetTime(CommonUtilities::RandomRange(0.f, 5.f));
			}
		}
	}
	for (int i = 0; i < doc["myDirectionalLights"].GetSize(); ++i)
	{
		auto obj = doc["myDirectionalLights"][i];
		int parentID = obj["myParent"].GetInt();

		if (gameObjects.find(parentID) != gameObjects.end())
		{
			CLightComponent* light = gameObjects[parentID].AddComponent<CLightComponent>();
			light->SetType(CLightComponent::ELightType_Directional);
			light->SetIntensity(obj["myIntensity"].GetFloat());
			light->SetColor(JsonToVector3f(obj["myColor"]));
		}
	}
	for (int i = 0; i < doc["myPointLights"].GetSize(); ++i)
	{
		auto obj = doc["myPointLights"][i];
		int parentID = obj["myParent"].GetInt();

		if (gameObjects.find(parentID) != gameObjects.end())
		{
			CLightComponent* light = gameObjects[parentID].AddComponent<CLightComponent>();
			light->SetType(CLightComponent::ELightType_Point);
			light->SetIntensity(obj["myIntensity"].GetFloat());
			light->SetRange(obj["myRange"].GetFloat());
			light->SetColor(JsonToVector3f(obj["myColor"]));
		}
	}
	for (int i = 0; i < doc["mySpotLights"].GetSize(); ++i)
	{
		auto obj = doc["mySpotLights"][i];
		int parentID = obj["myParent"].GetInt();

		if (gameObjects.find(parentID) != gameObjects.end())
		{
			CLightComponent* light = gameObjects[parentID].AddComponent<CLightComponent>();
			light->SetType(CLightComponent::ELightType_Spot);
			light->SetIntensity(obj["myIntensity"].GetFloat());
			light->SetSpotAngle(obj["myAngle"].GetFloat());
			light->SetRange(obj["myRange"].GetFloat());
			light->SetColor(JsonToVector3f(obj["myColor"]));
			light->CastShadows(true);
		}
	}
	if (doc.Find("myCanvases"))
	{
		for (int i = 0; i < doc["myCanvases"].GetSize(); ++i)
		{
			auto canvasObj = doc["myCanvases"][i];
			int parentID = canvasObj["myParent"].GetInt();

			if (gameObjects.find(parentID) != gameObjects.end())
			{
				CommonUtilities::Vector2f canvasSize = JsonToVector2f(canvasObj["mySize"]);
				CCanvasComponent::ERenderMode renderMode = static_cast<CCanvasComponent::ERenderMode>(canvasObj["myRenderMode"].GetInt());
				CCanvasComponent* canvas = gameObjects[parentID].AddComponent<CCanvasComponent>({ renderMode, canvasSize });
				myCanvases[canvasObj["myCanvasName"].GetString()] = canvas;
				std::string scriptPath = canvasObj["myScriptPath"].GetString();
				if (scriptPath != "")
				{
					canvas->SetScript(scriptPath);
				}
				bool isVisableFromStart = canvasObj["myVisiableFromStart"].GetBool();

				if (isVisableFromStart)
				{
					canvas->ForceShow();
				}
				else
				{
					canvas->ForceHide();
				}
				gameObjects[parentID].SetActive(true);
				
				if (canvasObj.Find("myTexts"))
				{
					for (int textIndex = 0; textIndex < canvasObj["myTexts"].GetSize(); ++textIndex)
					{
						auto textObj = canvasObj["myTexts"][textIndex];
						//std::string fontPath = textObj["myFont"].GetString();
						std::string name = textObj["myUIName"].GetString();
						std::string fontPath = "Assets/Fonts/Mono/Mono";
						CommonUtilities::Vector2f textPosition = JsonToVector2f(textObj["myPosition"]);
						int fontSize = textObj["myFontSize"].GetInt();
						float fontSizeF = static_cast<float>(fontSize) / 32;
						CommonUtilities::Vector4f color = JsonToVector4f(textObj["myColor"]);

						CTextComponent* textComponent = canvas->AddUIElement<CTextComponent>({ fontPath.c_str() });
						textComponent->SetPivot({ 0.5f, 0.5f });
						textComponent->SetName(name);
						textComponent->SetText(textObj["myText"].GetString());
						textComponent->SetPosition(textPosition);
						textComponent->SetScale({ fontSizeF, fontSizeF });
						textComponent->SetTint(color);
					}
				}

				if (canvasObj.Find("myRawImages"))
				{
					for (int spriteIndex = 0; spriteIndex < canvasObj["myRawImages"].GetSize(); ++spriteIndex)
					{
						auto spriteObj = canvasObj["myRawImages"][spriteIndex];
						std::string name = spriteObj["myUIName"].GetString();
						std::string spritePath = "Assets/Sprites/Menus/";
						spritePath += spriteObj["myFileName"].GetString();
						spritePath += ".dds";
						CommonUtilities::Vector2f spritePosition = JsonToVector2f(spriteObj["myPosition"]);
						CommonUtilities::Vector2f spriteSizeRelativeToScreen = JsonToVector2f(spriteObj["mySize"]);

						CSpriteComponent* spriteComponent = canvas->AddUIElement<CSpriteComponent>({ spritePath.c_str() });
						spriteComponent->SetName(name);
						spriteComponent->SetPivot({ 0.5f, 0.5f });
						spriteComponent->SetPosition(spritePosition);
						spriteComponent->SetScaleRelativeToScreen(spriteSizeRelativeToScreen);
					}
				}

				if (canvasObj.Find("myButtons"))
				{
					for (int buttonIndex = 0; buttonIndex < canvasObj["myButtons"].GetSize(); ++buttonIndex)
					{
						auto buttonObj = canvasObj["myButtons"][buttonIndex];
						std::string name = buttonObj["myUIName"].GetString();
						CommonUtilities::Vector2f buttonPosition = JsonToVector2f(buttonObj["myPosition"]);
						CommonUtilities::Vector2f buttonScaleRelativeToScreen = JsonToVector2f(buttonObj["mySize"]);
						CButtonComponent::SButtonTints buttonTints;
						auto buttonTintObj = buttonObj["myTints"];
						buttonTints.myNormalTint = JsonToVector4f(buttonTintObj["myNormalColor"]);
						buttonTints.myHighlightedTint = JsonToVector4f(buttonTintObj["myHighlightedColor"]);
						buttonTints.myPressedTint = JsonToVector4f(buttonTintObj["myPressedColor"]);
						buttonTints.myDisabledTint = JsonToVector4f(buttonTintObj["myDisabledColor"]);

						std::string spritePath = "Assets/Sprites/Menus/";
						spritePath += buttonObj["mySpriteFileName"].GetString();
						spritePath += ".dds";

						auto textObj = buttonObj["myText"];
						CommonUtilities::Vector2f textPosition = JsonToVector2f(textObj["myPosition"]);
						std::string buttonText = textObj["myText"].GetString();
						//std::string fontPath = textObj["myFont"].GetString();
						std::string fontPath = "Assets/Fonts/Mono/Mono";
						int fontSize = textObj["myFontSize"].GetInt();
						CommonUtilities::Vector4f textColor = JsonToVector4f(textObj["myColor"]);

						CButtonComponent* buttonComponent = canvas->AddUIElement<CButtonComponent>({ fontPath.c_str(), spritePath.c_str() });
						buttonComponent->SetName(name);
						buttonComponent->SetButtonTints(buttonTints);
						buttonComponent->SetPosition(buttonPosition);
						buttonComponent->SetScaleRelativeToScreen(buttonScaleRelativeToScreen);
						buttonComponent->SetTextPosition(textPosition);
						buttonComponent->SetTextSize(fontSize);
						buttonComponent->SetText(buttonText.c_str());
						buttonComponent->SetTextTint(textColor);

					}
				}

				if (canvasObj.Find("mySliders"))
				{
					for (int sliderIndex = 0; sliderIndex < canvasObj["mySliders"].GetSize(); ++sliderIndex)
					{
						auto sliderObj = canvasObj["mySliders"][sliderIndex];
						std::string name = sliderObj["myUIName"].GetString();
						CommonUtilities::Vector2f position = JsonToVector2f(sliderObj["myPosition"]);

						CSliderComponent::SButtonTints tints;
						auto sliderTintObj = sliderObj["myTints"];
						tints.myNormalTint = JsonToVector4f(sliderTintObj["myNormalColor"]);
						tints.myHighlightedTint = JsonToVector4f(sliderTintObj["myHighlightedColor"]);
						tints.myPressedTint = JsonToVector4f(sliderTintObj["myPressedColor"]);
						tints.myDisabledTint = JsonToVector4f(sliderTintObj["myDisabledColor"]);
						
						std::string prefixPath = "Assets/Sprites/Menus/";
						std::string postfixPath = ".dds";
						std::string backgroundFileName = prefixPath + sliderObj["myBackgroundFileName"].GetString() + postfixPath;
						std::string fillFileName = prefixPath + sliderObj["myFillFileName"].GetString() + postfixPath;
						std::string knobFileName = prefixPath + sliderObj["myKnobFileName"].GetString() + postfixPath;

						CommonUtilities::Vector2f knobSize = JsonToVector2f(sliderObj["myKnobSize"]);
						CommonUtilities::Vector2f backgroundSize = JsonToVector2f(sliderObj["myBackgroundSize"]);

						float min = sliderObj["myMinValue"].GetFloat();
						float max = sliderObj["myMaxValue"].GetFloat();
						float startValue = sliderObj["myStartValue"].GetFloat();

						if (name == "MasterVolume")
						{
							startValue = AM.GetVolume(AudioChannel::Master) / 100.f;
						}
						else if (name == "SFXVolume")
						{
							startValue = AM.GetVolume(AudioChannel::SoundEffects) / 100.f;
						}
						else if (name == "MusicVolume")
						{
							startValue = AM.GetVolume(AudioChannel::Music) / 100.f;
						}

						CSliderComponent* sliderComponent = canvas->AddUIElement<CSliderComponent>({
							backgroundFileName.c_str(),
							fillFileName.c_str(),
							knobFileName.c_str(),
							startValue,
							min,
							max
						});

						sliderComponent->SetName(name);
						sliderComponent->SetPosition(position);
						sliderComponent->SetSize(backgroundSize);
						sliderComponent->SetTints(tints);
						sliderComponent->SetKnobSize(knobSize);
					}
				}
			}
		}
	}
	if (doc.Find("myScripts"))
	{
		for (int i = 0; i < doc["myScripts"].GetSize(); ++i)
		{
			auto obj = doc["myScripts"][i];
			int parentID = obj["myParent"].GetInt();

			if (gameObjects.find(parentID) != gameObjects.end())
			{
				const char* filePath = obj["myFileName"].GetString();
				short triggerID = 0;
				if (obj.Find("myTriggerID"))
				{
					triggerID = static_cast<short>(obj["myTriggerID"].GetInt());
				}
				else
				{
					SCRIPT_LOG(CONCOL_WARNING, "All Scripts now need a triggerID in Unity");
				}
				CScriptComponent::SComponentData data = { filePath, triggerID };
				gameObjects[parentID].AddComponent<CScriptComponent>(data);
			}
		}
	}
	if (doc.Find("myBoxColliders"))
	{
		int boxCollidersSize = doc["myBoxColliders"].GetSize();
		myWorldColliders.reserve(boxCollidersSize);

		for (int i = 0; i < boxCollidersSize; ++i)
		{
			auto obj = doc["myBoxColliders"][i];
			int parentID = obj["myParent"].GetInt();
			if (gameObjects.find(parentID) != gameObjects.end())
			{
				myWorldColliders.emplace_back();
				CColliderBox& collider = myWorldColliders.back();
				collider.SetPosition(JsonToVector3f(obj["myPosition"]) + gameObjects[parentID].GetTransform().GetPosition());
				CommonUtilities::Vector3f parentScale = gameObjects[parentID].GetTransform().GetScale();
				CommonUtilities::Vector3f colliderScale = JsonToVector3f(obj["mySize"]);
				colliderScale.x *= parentScale.x;
				colliderScale.y *= parentScale.y;
				colliderScale.z *= parentScale.z;
				collider.SetBounds(colliderScale);
			}
		}
	}

	OnLoadFinished(doc, gameObjects);
}

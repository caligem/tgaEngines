#pragma once
#include "ObjectPool.h"

#include "GameObject.h"
#include "../Physics/ColliderBox.h"
#include "JsonDocument.h"

enum EStateUpdate
{
	EQuitGame = -1,
	EPop_Main = 0,
	EPop_Sub,
	EDoNothing,
	EPop_Sub_Push_Sub,
	EPop_Sub_Push_Main,
	EPop_Main_Push_Sub,
	EPop_Main_Push_Main,
	ESwapScene
};

enum EDebugToggles
{
	Player,
	ServerPositions,
	StaticColliders,
	DynamicColliders,
	Count
};

class CStateStack;
class CScene;

class CState
{
public:
	CState() : myNewStateToPush(nullptr), myShouldPushNewState(false) {}
	virtual ~CState() = default;
	virtual bool Init();
	virtual EStateUpdate Update() = 0;
	virtual void OnEnter() = 0;
	virtual void OnLeave() = 0;

	void SetActiveScene();

	void DestoryScene();

	void ShowCanvas(const std::string& aCanvasName);
	void HideCanvas(const std::string& aCanvasName);
	void ForceShowCanvas(const std::string& aCanvasName);
	void ForceHideCanvas(const std::string& aCanvasName);
	virtual void ForceUnpauseGame() {};
	virtual void UnpauseGame() {};
	
	virtual void SwapScene(const std::string&) {};

	virtual void PushText(const std::string&) {};
	virtual void RunDialogue(const std::string&) {};

	void SetStateToPush(CState* aNewStateToPush) { myNewStateToPush = aNewStateToPush; }
	void SetShouldPushState(bool aShouldPushState) { myShouldPushNewState = aShouldPushState; }
	CState* GetNewStateToPush() { return myNewStateToPush; }
	void SetShouldPop(bool aShouldPop) { myShouldPop = aShouldPop; }

	ID_T(CScene) GetSceneID() { return mySceneID; }

	CGameObject GetMainCamera() { return myMainCamera; }

	virtual void ToggleDebug(EDebugToggles) {}

protected:
	friend CStateStack;

	void LoadFromFile(const std::string& aLevelpath);
	virtual void OnLoadFinished(JsonDocument& aDoc, std::map<int, CGameObject>& aGameObjects) { aDoc; aGameObjects; }

	ID_T(CScene) mySceneID;

	static CStateStack* ourStateStack;
	CState* myNewStateToPush;
	bool myShouldPushNewState;

	std::vector<CColliderBox> myWorldColliders;

	std::map<std::string, CCanvasComponent*> myCanvases;
	bool myShouldPop;

	CGameObject myMainCamera;
};

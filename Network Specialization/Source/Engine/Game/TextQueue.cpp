#include "stdafx.h"
#include "TextQueue.h"


CTextQueue::CTextQueue()
{
	
}


CTextQueue::~CTextQueue()
{
}

void CTextQueue::Init(CCanvasComponent& aCanvasComponent, const CommonUtilities::Vector2f& aFramePosition, const CommonUtilities::Vector2f& aTextScale)
{
	myTextPosition = aFramePosition;
	CTextComponent* textComponent;

	for (char slotIndex = 0; slotIndex < myMaxTextSlots; slotIndex++)
	{
		textComponent = aCanvasComponent.AddUIElement<CTextComponent>({ "Assets/Fonts/Mono/Mono" });
		textComponent->SetPivot({ 0.5f, 0.5f });
		textComponent->SetScale(aTextScale);
		textComponent->SetOutline({ 0.f, 0.f, 0.f, 1.f });
		textComponent->SetPosition(myTextPosition);
		textComponent->SetText("");

		myTextSlots[slotIndex].myTextComponent = textComponent;
	}
}

void CTextQueue::Update()
{
	for (STextSlot& textSlot : myTextSlots)
	{
		if (textSlot.myIsActive)
		{
			textSlot.myTextComponent->SetPosition(CommonUtilities::Lerp(textSlot.myTextComponent->GetPosition(), textSlot.myTargetPosition, IWorld::Time().GetDeltaTime() * 10.f));

			textSlot.myDuration += IWorld::Time().GetDeltaTime();

			if (textSlot.myDuration >= myMaxTextDuration)
			{
				float currentFadeDuration = textSlot.myDuration - myMaxTextDuration;
				float percent = currentFadeDuration / myTextFadeDuration;

				if (percent >= 1.f)
				{
					textSlot.myTextComponent->SetText("");
					textSlot.myIsActive = false;

					percent = CommonUtilities::Clamp(percent, 0.f, 1.f);
				}
				textSlot.myTextComponent->SetTint(CommonUtilities::Lerp<CommonUtilities::Vector4f>({ 1.f, 1.f, 1.f, 1.f }, { 1.f, 1.f, 1.f, 0.f }, percent));
			}
		}
	}
}

void CTextQueue::PushText(const std::string & aText)
{
	for (STextSlot& textSlot : myTextSlots)
	{
		if (textSlot.myIsActive)
		{
			textSlot.mySlotIndex++;
			textSlot.myTargetPosition = { myTextPosition.x, myTextPosition.y - (myTextSlotOffset * textSlot.mySlotIndex) };
		}
	}

	myTextSlots[myNextTextSlot].myDuration = 0.f;
	myTextSlots[myNextTextSlot].myIsActive = true;
	myTextSlots[myNextTextSlot].myTextComponent->SetText(aText);
	myTextSlots[myNextTextSlot].myTextComponent->SetTint({ 1.f, 1.f, 1.f, 1.f });
	myTextSlots[myNextTextSlot].myTextComponent->SetPosition(myTextPosition);
	myTextSlots[myNextTextSlot].myTargetPosition = myTextPosition;
	myTextSlots[myNextTextSlot].mySlotIndex = 0;

	myNextTextSlot = (++myNextTextSlot) % myMaxTextSlots;
}

#pragma once
class CTextQueue
{
	struct STextSlot
	{
		CTextComponent* myTextComponent = nullptr;
		float myDuration = 0.f;
		char mySlotIndex = 0;
		bool myIsActive = false;
		CommonUtilities::Vector2f myTargetPosition;
	};
public:
	CTextQueue();
	~CTextQueue();

	void Init(CCanvasComponent& aCanvasComponent, const CommonUtilities::Vector2f& aFramePosition, const CommonUtilities::Vector2f& aTextScale);
	void Update();
	void PushText(const std::string& aText);

private:
	STextSlot myTextSlots[5];
	CommonUtilities::Vector2f myTextPosition;

	static constexpr float myTextSlotOffset = 0.025f;
	static constexpr float myMaxTextDuration = 7.f;
	static constexpr float myTextFadeDuration = 2.0f;
	static constexpr char myMaxTextSlots = 5;

	char myNextTextSlot = 0;
};


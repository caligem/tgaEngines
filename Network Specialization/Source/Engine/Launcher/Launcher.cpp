#include "stdafx.h"
#define _CRTDBG_MAP_ALLOC  
#include <stdlib.h>  
#include <crtdbg.h>  

#include <Game.h>

#include <Windows.h>
#include <iostream>
#include <string>
#include "CommandLineManager.h"
#include "DL_Debug.h"


void AllocateConsole()
{
	AllocConsole();
    FILE* cinFile;
    freopen_s(&cinFile, "CONIN$", "r", stdin);
    FILE* coutFile;
    freopen_s(&coutFile, "CONOUT$", "w", stdout);
    FILE* cerrFile;
    freopen_s(&cerrFile, "CONOUT$", "w", stderr);
}

void SetupFilterLog()
{
	const int numLogs = 5;
	const char* logNames[numLogs] =
	{
		"resource",
		"engine",
		"gameplay",
		"script",
		"network"
	};
	for (int i = 0; i < numLogs; ++i)
	{
		if (CommonUtilities::CCommandLineManager::HasArgument("-activateLog", logNames[i]))
		{
			DL_Debug::Debug::GetInstance()->ActivateFilterLog(logNames[i]);
		}
		else
		{
			DL_Debug::Debug::GetInstance()->DeactivateFilterLog(logNames[i]);
		}
	}

	DL_Debug::Debug::GetInstance()->ActivateFilterLog("general");
}

INT WINAPI wWinMain(HINSTANCE, HINSTANCE, LPWSTR, int)
{
	{
		CommonUtilities::CCommandLineManager::Init(__argc, __wargv);

#ifndef _RETAIL
		AllocateConsole();
		SetupFilterLog();
#endif

		CGame game;

		GENERAL_LOG(CONCOL_VALID, "Game going to initialise!");
		if (!game.Init())
		{
			system("pause");
			return 1;
		}

#ifndef _RETAIL
		DL_Debug::Debug::Destroy();
		FreeConsole();
#endif
	}
#ifdef _DEBUG
	_CrtSetDbgFlag ( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );  
	_CrtSetReportMode( _CRT_ERROR, _CRTDBG_MODE_DEBUG );  
	_CrtDumpMemoryLeaks();
#endif
	return 0;
}

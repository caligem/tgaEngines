#pragma once
#include <functional>

class CNetMessage;

class CBroadCaster
{
public:
	CBroadCaster();
	~CBroadCaster();
	void BindBroadCastMessageCallback(std::function<void(CNetMessage*)> aBroadCastMessageCallback) { myBroadCastMessageCallback = aBroadCastMessageCallback; }
	void BroadCastMessageToGameServer(CNetMessage* aMessage) { myBroadCastMessageCallback(aMessage); }
private:
	std::function<void(CNetMessage*)> myBroadCastMessageCallback;
};


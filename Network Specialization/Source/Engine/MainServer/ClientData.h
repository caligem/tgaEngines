#pragma once
#include "AddressWrapper.h"
#include <string>

struct SClientData
{
	CAddressWrapper myAddress;
	unsigned short myID = 0;
	std::string myName = "";
	unsigned int myPing = 0;
	unsigned int myRTT = 0;
	float myLatestPingTimer = 0.f;
	char myGameServerID = -1;
};
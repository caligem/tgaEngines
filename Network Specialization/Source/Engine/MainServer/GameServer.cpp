#include "stdafx.h"
#include "GameServer.h"
#include "JsonDocument.h"
#include "JsonUtility.h"
#include "Random.h"
#include "ICollision.h"
#include "GameCollision.h"
#include <map>

CGameServer::CGameServer(CommonUtilities::Timer& aTimer)
	: myTimer(aTimer)
	, myProjectileManager(aTimer)
{
	CommonUtilities::InitRand();
}


CGameServer::~CGameServer()
{
}

void CGameServer::Init(char aGameServerID)
{
	myGameTimer = GameLength;
	myServerTickRateTimerInMS = 0.f;
	mySnapShotTimer = 0.f;
	myEndGameTimer = 0.f;
	mySlowdownTimer = SlowdownLength;
	myGameTimerSendTimer = 0.f;
	myGameServerState = EGameServerState::Playing;

	myGameServerID = aGameServerID;

	LoadMap("Assets/Levels/game.json");

	myProjectileManager.Init();
	myProjectileManager.BindBroadCastMessageCallback(std::bind(&CGameServer::BroadCastMessage, this, std::placeholders::_1));

	IServerNetwork::RegisterDoUnPackWorkCallback<CNetMessagePlayerInput>(
		std::bind(&CGameServer::HandlePlayerInput, this, std::placeholders::_1, std::placeholders::_2));

	IServerNetwork::RegisterDoUnPackWorkCallback<CNetMessagePlayerShoot>(
		std::bind(&CGameServer::HandlePlayerShoot, this, std::placeholders::_1, std::placeholders::_2));
}

void CGameServer::LoadMap(const std::string& aMap)
{
	JsonDocument doc(aMap.c_str());

	std::map<int, SGameObjectData> gameObjects;

	for (int i = 0; i < doc["myGameObjects"].GetSize(); ++i)
	{
		auto obj = doc["myGameObjects"][i];

		SGameObjectData gameObject;
		std::string tag = obj["myTag"].GetString();
		if (tag == "SpawnPoint")
		{
			mySpawnPoints.emplace_back();
			CommonUtilities::Vector2f& spawnPoint = mySpawnPoints.back();
			spawnPoint = (JsonToVector3f(obj["myPosition"]));
		}

		gameObject.position = (JsonToVector3f(obj["myPosition"]));
		gameObject.size = (JsonToVector3f(obj["myScale"]));

		gameObjects[obj["myID"].GetInt()] = gameObject;
	}

	if (doc.Find("myBoxColliders"))
	{
		for (int i = 0; i < doc["myBoxColliders"].GetSize(); ++i)
		{
			auto obj = doc["myBoxColliders"][i];
			auto parent = gameObjects[obj["myParent"].GetInt()];

			myWorldColliders.emplace_back();
			CColliderBox& collider = myWorldColliders.back();
			collider.SetPosition(JsonToVector3f(obj["myPosition"]) + parent.position);
			CommonUtilities::Vector3f size = JsonToVector3f(obj["mySize"]);
			size.x *= parent.size.x;
			size.y *= parent.size.y;
			size.z *= parent.size.z;
			collider.SetBounds(size);
		}
	}

	if (doc.Find("myHealthpacks"))
	{
		for (int i = 0; i < doc["myHealthpacks"].GetSize(); ++i)
		{
			auto obj = doc["myHealthpacks"][i];
			myHealthPacks.emplace_back();
			CServer_HealthPack& newHealthPack = myHealthPacks.back();
			newHealthPack.SetPosition(JsonToVector3f(obj["myPosition"]));
			newHealthPack.SetID(static_cast<unsigned short>(i));
			newHealthPack.BindBroadCastMessageCallback(std::bind(&CGameServer::BroadCastMessage, this, std::placeholders::_1));
		}
	}
}

void CGameServer::Update()
{
	myServerTickRateTimerInMS += myTimer.GetDeltaTime();
	while (myServerTickRateTimerInMS >= ServerTickRate)
	{
		myServerTickRateTimerInMS -= ServerTickRate;
		float dt;
		dt = UpdateGameTimers(ServerTickRate);
	
		UpdateObjects(dt);
		CheckCollision();
	}

	BroadCastGameTimer();
	UpdateSnapshots();
}

float CGameServer::UpdateGameTimers(float dt)
{
	if (myGameServerState == EGameServerState::Playing)
	{
		myGameTimer -= dt;
		myGameTimer = CommonUtilities::Clamp(myGameTimer, 0.f, GameLength);
		if (myGameTimer <= 0.f)
		{
			myGameTimer = 0.f;
			SendShowWinScreen();
			myGameServerState = EGameServerState::Slowdown;
		}			
	}
	else if (myGameServerState == EGameServerState::Slowdown)
	{
		mySlowdownTimer -= dt;
		float percent = mySlowdownTimer / SlowdownLength;
		percent = CommonUtilities::Clamp(percent, 0.3f, 0.8f);
		dt *= percent;
		if (mySlowdownTimer <= 0.f)
		{
			mySlowdownTimer = SlowdownLength;
			myGameServerState = EGameServerState::Ending;
		}
	}
	else if (myGameServerState == EGameServerState::Ending)
	{
		myEndGameTimer += dt;
		dt *= 0.05f;
		if (myEndGameTimer >= EndGameLength)
		{
			myEndGameTimer = 0.f;

			myGameTimer = GameLength;
			RestartGame();
			myGameServerState = EGameServerState::Playing;
		}
	}

	return dt;
}

void CGameServer::UpdateObjects(float dt)
{
	for (auto& healthPack : myHealthPacks)
	{
		healthPack.Update(dt);
	}

	for (auto& player : myPlayers)
	{
		player.Update(dt);
	}

	myProjectileManager.Update(dt);
}

void CGameServer::UpdateSnapshots()
{
	mySnapShotTimer += myTimer.GetDeltaTimeInMS();
	while (mySnapShotTimer >= SnapShotFrequencyInMS)
	{
		mySnapShotTimer -= SnapShotFrequencyInMS;
		BroadCastPlayerSnapshots();
	}
}

float CGameServer::CalculateGameServerDeltaTime()
{
	static double lastTime = myTimer.GetCurrentTime();
	static double currentTime = lastTime;

	lastTime = currentTime;
	currentTime += myTimer.GetCurrentTime() - lastTime;

	return static_cast<float>(currentTime - lastTime);
}

void CGameServer::AddClient(SClientData aClientData)
{
	myClients.push_back(std::move(aClientData));
	SClientData& client = myClients.back();

	AddPlayer(client);

	SendExistingPlayersInfoToPlayer(client.myID, client.myAddress);
	SendExistingHealthPackInfoToPlayer(client.myID, client.myAddress);
	myProjectileManager.SendExisitingProjectiles(client.myID, client.myAddress);
	SendStats(client.myID, client.myAddress);

	CNetMessageGameTimer* timerMessage = IServerNetwork::CreateMessage<CNetMessageGameTimer>(client.myID);
	timerMessage->SetTimer(myGameTimer);
	IServerNetwork::SendToClient(timerMessage, client.myAddress);

	BroadCastMessagePlayerStatus(CNetMessagePlayerStatus::PlayerJoined, client);
}

void CGameServer::AddPlayer(SClientData &aClient)
{
	myPlayers.emplace_back();
	CServer_Player& newPlayer = myPlayers.back();
	newPlayer.BindBroadCastMessageCallback(std::bind(&CGameServer::BroadCastMessage, this, std::placeholders::_1));
	newPlayer.SetSpawnPoints(mySpawnPoints);
	newPlayer.SetPlayerID(aClient.myID);
	newPlayer.SetPlayerName(aClient.myName);
	newPlayer.Spawn();

	myPlayerStats.AddPlayerStats(newPlayer.GetPlayerID());
}

void CGameServer::SendExistingPlayersInfoToPlayer(unsigned short aClientID, CAddressWrapper& aClientAddress)
{
	for (auto& player : myPlayers)
	{
		CNetMessagePlayerStatus* message = IServerNetwork::CreateMessage<CNetMessagePlayerStatus>(aClientID);
		message->SetPlayerID(player.GetPlayerID());
		message->SetPlayerName(player.GetPlayerName());
		message->SetPlayerStatus(CNetMessagePlayerStatus::PlayerExists);
		message->SetPosition(player.GetPosition());
		message->SetPlayerHealth(player.GetHealth());
		IServerNetwork::SendToClient(message, aClientAddress);
	}
}

void CGameServer::RemoveClientByID(SClientData& aClient)
{
	BroadCastMessagePlayerStatus(CNetMessagePlayerStatus::PlayerLeft, aClient);
	RemoveClient(aClient.myID);
	RemovePlayer(aClient.myID);
}

void CGameServer::RemoveClient(unsigned short aClientID)
{
	for (unsigned short i = 0; i < myClients.size(); ++i)
	{
		if (myClients[i].myID == aClientID)
		{
			myClients.erase(myClients.begin() + i);
			break;
		}
	}
}

void CGameServer::RemovePlayer(unsigned short aClientID)
{
	for (unsigned short i = 0; i < myPlayers.size(); ++i)
	{
		if (myPlayers[i].GetPlayerID() == aClientID)
		{
			myPlayerStats.RemovePlayerStats(myPlayers[i].GetPlayerID());
			myPlayers.erase(myPlayers.begin() + i);
			break;
		}
	}
}

void CGameServer::BroadCastMessagePlayerStatus(CNetMessagePlayerStatus::EPlayerStatus aPlayerStatus, SClientData &aClient)
{
	for (auto& client : myClients)
	{
		if (client.myID == aClient.myID)
		{
			continue;
		}

		CNetMessagePlayerStatus* message = IServerNetwork::CreateMessage<CNetMessagePlayerStatus>(client.myID);
		message->SetPlayerID(aClient.myID);
		message->SetPlayerName(aClient.myName);
		message->SetPlayerStatus(aPlayerStatus);
		if (aPlayerStatus != CNetMessagePlayerStatus::PlayerLeft)
		{
			CServer_Player* player = GetPlayerFromClientID(client.myID);
			if (player)
			{
				message->SetPosition(player->GetPosition());
			}
		}
		IServerNetwork::SendToClient(message, client.myAddress);
	}
}

CServer_Player* CGameServer::GetPlayerFromClientID(unsigned short aClientID)
{
	for (auto& player : myPlayers)
	{
		if (player.GetPlayerID() == aClientID)
		{
			return &player;
		}
	}
	return nullptr;
}

void CGameServer::HandlePlayerInput(CNetMessage* aMessage, CAddressWrapper&)
{
	CNetMessagePlayerInput* inputMessage = static_cast<CNetMessagePlayerInput*>(aMessage);
	const CNetMessagePlayerInput::EPlayerInput& input = inputMessage->GetPlayerInput();
	CServer_Player* player = GetPlayerFromClientID(inputMessage->GetSenderID());

	if (player)
	{
		if (input == CNetMessagePlayerInput::LeftDown)
		{
			player->AddMovement(CServer_Player::EMovement::Left);
#ifdef DEBUG_INPUT
			printf("Left key down time : %d \n", static_cast<int>(myTimer.GetTotalTimeInMS()));
			myLeftKeyDownTimer = static_cast<int>(myTimer.GetTotalTimeInMS());
			myDeltaPingLeftKey = static_cast<int>(myTimer.GetTotalTimeInMS()) - inputMessage->GetTimeStamp();
#endif
		}
		else if (input == CNetMessagePlayerInput::RightDown)
		{
			player->AddMovement(CServer_Player::EMovement::Right);
#ifdef DEBUG_INPUT
			printf("Right key down time : %d \n", static_cast<int>(myTimer.GetTotalTimeInMS()));
			myRightKeyDownTimer = static_cast<int>(myTimer.GetTotalTimeInMS());
			myDeltaPingRightKey = static_cast<int>(myTimer.GetTotalTimeInMS()) - inputMessage->GetTimeStamp();
#endif
		}
		if (input == CNetMessagePlayerInput::LeftUp)
		{
			player->StopMovement(CServer_Player::EMovement::Left);
#ifdef DEBUG_INPUT
			printf("Left key up time : %d \n", static_cast<int>(myTimer.GetTotalTimeInMS()));
			printf("Left key down time : %d \n", static_cast<int>(myTimer.GetTotalTimeInMS()) - myLeftKeyDownTimer);
			int deltaPing = static_cast<int>(myTimer.GetTotalTimeInMS()) - inputMessage->GetTimeStamp();
			int deltaPingClamped = myDeltaPingLeftKey - deltaPing;
			printf("DeltaClampedPing : %d \n", deltaPingClamped);
#endif
		}
		else if (input == CNetMessagePlayerInput::RightUp)
		{
			player->StopMovement(CServer_Player::EMovement::Right);
#ifdef DEBUG_INPUT
			printf("Right key up time : %d \n", static_cast<int>(myTimer.GetTotalTimeInMS()));
			printf("Right key down time : %d \n", static_cast<int>(myTimer.GetTotalTimeInMS()) - myRightKeyDownTimer);
			int deltaPing = static_cast<int>(myTimer.GetTotalTimeInMS()) - inputMessage->GetTimeStamp();
			int deltaPingClamped = myDeltaPingRightKey - deltaPing;
			printf("DeltaClampedPing : %d \n", deltaPingClamped);
#endif
		}
		else if (input == CNetMessagePlayerInput::Jump)
		{
			player->Jump();
		}
	}
}

void CGameServer::HandlePlayerShoot(CNetMessage * aMessage, CAddressWrapper& aAddress)
{
	CNetMessagePlayerShoot* message = static_cast<CNetMessagePlayerShoot*>(aMessage);
	CServer_Player* shooter = GetPlayerFromClientID(message->GetSenderID());

	if (shooter)
	{
		if (shooter->CanShoot(message->GetTimeStamp()))
		{
			myProjectileManager.FirePlayerProjectile(shooter->GetPosition(), message->GetDirection(), shooter->GetPlayerID());
			SendCreateProjectile(shooter, message->GetSenderID(), message->GetDirection());
			shooter->SetLastTimeShoot(message->GetTimeStamp());
		}
		else
		{
			CNetMessagePredictedProjectile* predictedProjectileMessage = IServerNetwork::CreateMessage<CNetMessagePredictedProjectile>();
			predictedProjectileMessage->SetPredictedProjectileID(message->GetProjectileID());
			IServerNetwork::SendToClient(predictedProjectileMessage, aAddress);
		}
	}
}

void CGameServer::SendCreateProjectile(CServer_Player* shooter, unsigned short aClientID, const CommonUtilities::Vector2f& aDirection)
{
	for (auto& client : myClients)
	{
		if (client.myID == aClientID)
		{
			continue;
		}

		CNetMessageCreateProjectile* shootMessage = IServerNetwork::CreateMessage<CNetMessageCreateProjectile>(client.myID);
		shootMessage->SetShooterID(shooter->GetPlayerID());
		shootMessage->SetDirection(aDirection);
		IServerNetwork::SendToClient(shootMessage, client.myAddress);
	}
}

void CGameServer::SendStats(short aClientID, CAddressWrapper aClientAddress)
{
	CNetMessageSendStats* message = IServerNetwork::CreateMessage<CNetMessageSendStats>(aClientID);
	for (auto& player : myPlayers)
	{
		const CPlayerStats::SPlayerStat& playerStat = myPlayerStats.GetPlayerStats(player.GetPlayerID());
		message->AddPlayerStats(playerStat.playerID, playerStat.kills, playerStat.deaths);
	}
	IServerNetwork::SendToClient(message, aClientAddress);
}

void CGameServer::SendExistingHealthPackInfoToPlayer(unsigned short aClientID, CAddressWrapper aClientAddress)
{
	CNetMessageCreateHealthPack* message = IServerNetwork::CreateMessage<CNetMessageCreateHealthPack>(aClientID);
	for (auto& healthPack : myHealthPacks)
	{
		message->AddHealthPack(healthPack.GetID(), healthPack.GetPosition(), healthPack.IsTaken());
	}
	IServerNetwork::SendToClient(message, aClientAddress);
}

void CGameServer::BroadCastPlayerSnapshots()
{
	CNetMessagePlayerSnapshot* message = IServerNetwork::CreateMessage<CNetMessagePlayerSnapshot>();
	for (auto& player : myPlayers)
	{
		if (!player.IsDead())
		{
			message->AddPlayerPosition(player.GetPosition(), player.GetPlayerID());
		}
	}
	BroadCastMessage(message);
}

void CGameServer::CheckCollision()
{
	for (auto& player : myPlayers)
	{
		if (player.IsDead())
		{
			continue;
		}

		IGameCollision::PlayerVSWorldCollision(player, myWorldColliders);
		PlayerVsHealthpackCollision(player);
		myProjectileManager.CheckCollisionVsPlayer(player, myPlayerStats);
	}

	myProjectileManager.ProjectileVsWorldColliders(myWorldColliders);
}

void CGameServer::PlayerVsHealthpackCollision(CServer_Player &player)
{
	for (auto& healthpack : myHealthPacks)
	{
		if (healthpack.IsTaken())
		{
			continue;
		}
		
		if (ICollision::CircleVSBox(player.GetCollider(), healthpack.GetCollider()))
		{
			player.Heal(healthpack.GetHealingAmount());
			healthpack.Taken();
		}
	}
}

void CGameServer::RestartGame()
{
	CNetMessageResetGame* message = IServerNetwork::CreateMessage<CNetMessageResetGame>();

	myPlayerStats.Reset();

	for (auto& player : myPlayers)
	{
		player.Reset();
		message->AddPlayerData(player.GetPlayerID(), player.GetPosition());
	}

	myProjectileManager.ClearAllBullets();

	for (auto& healthpack : myHealthPacks)
	{
		healthpack.Reset();
	}

	BroadCastMessage(message);

	myGameTimerSendTimer = 0.f;

	CNetMessageGameTimer* timerMessage = IServerNetwork::CreateMessage<CNetMessageGameTimer>();
	timerMessage->SetTimer(myGameTimer);
	BroadCastMessage(timerMessage);
}

void CGameServer::SendShowWinScreen()
{
	CNetMessageShowWinscreen* message = IServerNetwork::CreateMessage<CNetMessageShowWinscreen>();
	message->SetSlowDownTime(SlowdownLength);
	BroadCastMessage(message);
}

void CGameServer::BroadCastGameTimer()
{
	myGameTimerSendTimer += myTimer.GetDeltaTime();
	while (myGameTimerSendTimer >= GameTimerSendRate)
	{
		myGameTimerSendTimer -= GameTimerSendRate;
		CNetMessageGameTimer* timerMessage = IServerNetwork::CreateMessage<CNetMessageGameTimer>();
		timerMessage->SetTimer(myGameTimer);
		BroadCastMessage(timerMessage);
	}
}

void CGameServer::BroadCastMessage(CNetMessage* aMessage)
{
	for (auto& client : myClients)
	{
		aMessage->SetTargetID(client.myID);
		IServerNetwork::SendToClient(aMessage, client.myAddress);
	}
}
#pragma once
#include <vector>
#include "Vector.h"
#include "ClientData.h"
#include "IServerNetwork.h"
#include "Server_Player.h"
#include "Server_HealthPack.h"
#include "Server_ProjectileManager.h"
#include "ColliderOBB.h"
#include <deque>
#include "PlayerStats.h"

class CNetMessage;

class CGameServer
{
	struct SGameObjectData
	{
		CommonUtilities::Vector3f position;
		CommonUtilities::Vector3f size;
	};

	enum class EGameServerState
	{
		Playing,
		Slowdown,
		Ending
	};

	struct SPlayerStats
	{
		unsigned short playerID = 0;
		unsigned short kills = 0;
		unsigned short deaths = 0;
	};

public:
	CGameServer(CommonUtilities::Timer& aTimer);
	~CGameServer();

	void Init(char aGameServerID);

	void LoadMap(const std::string& aMap);

	void Update();

	float UpdateGameTimers(float dt);

	inline const char GetGameServerID() const { return myGameServerID; }

	void AddClient(SClientData aClientData);

	void RemoveClientByID(SClientData& aClient);
	void RemoveClient(unsigned short aClientID);

private:
	float CalculateGameServerDeltaTime();

	void UpdateObjects(float dt);
	void UpdateSnapshots();

	void AddPlayer(SClientData &client);
	void RemovePlayer(unsigned short aClientID);
	CServer_Player* GetPlayerFromClientID(unsigned short aClientID);

	void HandlePlayerInput(CNetMessage* aMessage, CAddressWrapper&);
	void HandlePlayerShoot(CNetMessage* aMessage, CAddressWrapper& aAddress);

	void SendCreateProjectile(CServer_Player* shooter, unsigned short aClientID, const CommonUtilities::Vector2f& aDirection);

	void SendStats(short aClientID, CAddressWrapper aClientAddress);
	void SendExistingHealthPackInfoToPlayer(unsigned short aClientID, CAddressWrapper aClientAddress);
	void SendExistingPlayersInfoToPlayer(unsigned short aClientID, CAddressWrapper& aClientAddress);

	void BroadCastGameTimer();
	void BroadCastPlayerSnapshots();
	void BroadCastMessagePlayerStatus(CNetMessagePlayerStatus::EPlayerStatus aPlayerStatus, SClientData &aClient);
	void BroadCastMessage(CNetMessage* aMessage);

	void CheckCollision();
	void PlayerVsHealthpackCollision(CServer_Player &player);
	void RestartGame();
	void SendShowWinScreen();
	CServer_ProjectileManager myProjectileManager;

	std::vector<SClientData> myClients;
	std::vector<CColliderBox> myWorldColliders;
	std::vector<CServer_Player> myPlayers;
	std::vector<CServer_HealthPack> myHealthPacks;
	std::vector<CommonUtilities::Vector2f> mySpawnPoints;
	CPlayerStats myPlayerStats;
	

	static constexpr float SnapShotFrequencyInMS = (1.f / 60.f) * 1000.f;
	float mySnapShotTimer;
	static constexpr float ServerTickRate = (1.f / 144.0f);
	float myServerTickRateTimerInMS;
	static constexpr float GameLength = 60.f;
	float myGameTimer;
	static constexpr float EndGameLength = 10.f;
	float myEndGameTimer;
	static constexpr float SlowdownLength = 2.f;
	float mySlowdownTimer;
	static constexpr float GameTimerSendRate = 0.5f;
	float myGameTimerSendTimer;

	int myLeftKeyDownTimer;
	int myRightKeyDownTimer;
	int myDeltaPingLeftKey;
	int myDeltaPingRightKey;

	CommonUtilities::Timer& myTimer;

	EGameServerState myGameServerState;

	char myGameServerID;
};


#include "stdafx.h"
#include "IServerNetwork.h"

std::string IServerNetwork::myIP = "";

IServerNetwork::IServerNetwork()
{
}


IServerNetwork::~IServerNetwork()
{
}

void IServerNetwork::SendToClient(CNetMessage * aMessage, CAddressWrapper & aClientAddress)
{
	ourNetMessageManager->PackAndSendMessage(aMessage, aClientAddress);
}

std::string IServerNetwork::GetMyIP()
{
	return myIP;
}

#pragma once
#include "INetwork.h"
#include "ClientData.h"

class IServerNetwork : public INetwork
{
public:
	IServerNetwork();
	~IServerNetwork();

	static void SendToClient(CNetMessage* aMessage, CAddressWrapper& aClientAddress);
	static std::string GetMyIP();
private:
	static std::string myIP;
	friend class CMainServerNetworkSystem;
};


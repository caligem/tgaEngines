#include "stdafx.h"
#include "MainServer.h"
#include <iostream>

#ifndef _RETAIL
#define START_TIMER(ID) std::chrono::steady_clock::time_point ID = std::chrono::steady_clock::now();
#define GET_TIME(ID) (static_cast<float>((std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now()-ID).count()))/1000.f)
#else
#define START_TIMER(ID)
#define GET_TIME(ID) 0.f
#endif

CMainServer::CMainServer()
	: myNetworkSystem(myTimer, myUIMediator)
	, myUI(myUIMediator, myTimer)
{
}


CMainServer::~CMainServer()
{
}

bool CMainServer::Init()
{
	if (!myWindow.Init())
	{
		printf("Failed to init Main Server Window.");
		return false;
	}
	if (!myUI.Init(myWindow.GetHWND(), myWindow.GetDevice(), myWindow.GetContext(), myWindow.GetWindowSize()))
	{
		printf("Failed to init Main Server UI.");
		return false;
	}
	
	if (!myNetworkSystem.Init())
	{
		myUIMediator.AddToLog("Failed to init Main Server Network System.", CMainServerUIMediator::EMessageColor::Error);
		return false;
	}
	else
	{
		myNetworkSystem.GoOnline();
	}

	myUI.SetGoOnlineCallback(std::bind(&CMainServerNetworkSystem::GoOnline, &myNetworkSystem));
	myUI.SetGoOfflineCallback(std::bind(&CMainServerNetworkSystem::GoOffline, &myNetworkSystem));
	myUI.SetSendLuaCommandCallback(std::bind(&CMainServerNetworkSystem::SendLuaCommandToClient, &myNetworkSystem, std::placeholders::_1, std::placeholders::_2));
	myUI.SetBroadcastLuaCommandCallback(std::bind(&CMainServerNetworkSystem::BroadcastLuaCommand, &myNetworkSystem, std::placeholders::_1));
	myUIMediator.AddToLog("Server startup successful", CMainServerUIMediator::EMessageColor::Valid);
	return true;
}

void CMainServer::Update()
{
	START_TIMER(frameMS);
	myTimer.Update();
	myNetworkSystem.PreUpdate();
	myNetworkSystem.Update();
	myUI.Update();
	myNetworkSystem.PostUpdate();
	myUI.PushFrameMS(GET_TIME(frameMS));
}

void CMainServer::Render()
{
	myWindow.BeginFrame();
	myUI.Render();
	myWindow.EndFrame();
}

void CMainServer::Shutdown()
{
	myNetworkSystem.Shutdown();
	myWindow.Shutdown();
}

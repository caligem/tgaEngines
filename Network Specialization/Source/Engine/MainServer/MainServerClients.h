#pragma once
#include "AddressWrapper.h"
#include <string>
#include <array>
#include <deque>
#include "ClientData.h"

class CMainServerClients
{
public:
	CMainServerClients();
	~CMainServerClients();

	const SClientData* AddClient(CAddressWrapper& aClient);
	void RemoveClientByIndex(unsigned short aIndex);
	void RemoveClientByID(unsigned short aClientID);

	SClientData* GetClientByID(unsigned short aClientID);
	SClientData* GetClientByIndex(unsigned short aIndex);
	const unsigned short GetClientCount() const { return myClientCount; }

	const std::array<SClientData, 512>& GetClients() const { return myClients; }
	const unsigned short GetMaxClients() const { return myMaxClients; }

private:
	static constexpr unsigned short myMaxClients = 512;
	std::array<SClientData, 512> myClients;
	std::deque<int> myFreeIndices;
	unsigned short myClientCount;
};


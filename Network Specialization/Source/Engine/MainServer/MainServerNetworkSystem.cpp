#include "stdafx.h"
#include "MainServerNetworkSystem.h"

#include <iostream>

#define DEFAULT_PORT 8888
#define _WINSOCK_DEPRECATED_NO_WARNINGS

#include "CommonNetworkIncludes.h"
#include "NetMessageIncludes.h"
#include "IServerNetwork.h"

CMainServerNetworkSystem::CMainServerNetworkSystem(CommonUtilities::Timer& aTimer, CMainServerUIMediator& aUIMediator)
	: myTimer(aTimer)
	, myNetMessageManager(aTimer)
	, myUIMediator(aUIMediator)
	, myGameServer(myTimer)
{
}


CMainServerNetworkSystem::~CMainServerNetworkSystem()
{
}

bool CMainServerNetworkSystem::Init()
{
	if (!myNetMessageManager.Init(1))
	{
		myUIMediator.AddToLog("NetMessageManager failed to init", CMainServerUIMediator::EMessageColor::Error);
		return false;
	}

	myNetMessageManager.SetSenderID(myClients.GetMaxClients());

	myUptime = 0;
	myUIMediator.SetServerStatusText("OFFLINE", CMainServerUIMediator::EMessageColor::Error);

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessageChatMessage>(
		std::bind(&CMainServerNetworkSystem::HandleChatMessage, this, std::placeholders::_1, std::placeholders::_2));

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessageClientName>(
		std::bind(&CMainServerNetworkSystem::SaveClientName, this, std::placeholders::_1, std::placeholders::_2));

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessageConnect>(
		std::bind(&CMainServerNetworkSystem::HandleConnectionMessage, this, std::placeholders::_1, std::placeholders::_2));

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessagePing>(
		std::bind(&CMainServerNetworkSystem::RecievePings, this, std::placeholders::_1, std::placeholders::_2));

	myNetMessageManager.RegisterDoUnPackWorkCallback<CNetMessageGameServerConnection>(
		std::bind(&CMainServerNetworkSystem::HandleGameServerConnection, this, std::placeholders::_1, std::placeholders::_2));

	IServerNetwork::ourNetMessageManager = &myNetMessageManager;
	char* ip = nullptr;
	GetMyIP(ip);
	IServerNetwork::myIP = ip;

	myGameServer.Init(0);

	return true;
}

void CMainServerNetworkSystem::Shutdown()
{
	myNetMessageManager.CloseSocket();
}

void CMainServerNetworkSystem::PreUpdate()
{
	if (myNetMessageManager.IsSocketOpen())
	{
		myNetMessageManager.RecieveMessage();
	}
}

void CMainServerNetworkSystem::Update()
{
	if(myNetMessageManager.IsSocketOpen())
	{
		myUptime += myTimer.GetRealDeltaTime();
		myNetMessageManager.UpdateReliableMessages();
		UpdatePings();
	}
	myGameServer.Update();
	
}

void CMainServerNetworkSystem::PostUpdate()
{
	myUIMediator.SetUptime(static_cast<unsigned int>(myUptime));
	myUIMediator.UpdateClientData(myClients.GetClientCount(), myClients.GetClients());
}

void CMainServerNetworkSystem::GoOnline()
{
	myNetMessageManager.OpenSocket(DEFAULT_PORT);
	myUIMediator.SetServerStatusText("ONLINE", CMainServerUIMediator::EMessageColor::Valid);
}

void CMainServerNetworkSystem::GoOffline()
{
	if (myNetMessageManager.CloseSocket())
	{
		KickAllClients();
		myUIMediator.SetServerStatusText("OFFLINE", CMainServerUIMediator::EMessageColor::Error);
		myUptime = 0;
	}
}

void CMainServerNetworkSystem::SendLuaCommandToClient(const std::string& aCommand, unsigned short aClientID)
{
	SendLuaCommandMessage(aCommand, aClientID);
}

void CMainServerNetworkSystem::BroadcastLuaCommand(const std::string & aCommand)
{
	for (unsigned short index = 0; index < myClients.GetClientCount(); index++)
	{
		unsigned short id = myClients.GetClientByIndex(index)->myID;
		SendLuaCommandMessage(aCommand, id);
	}
}

void CMainServerNetworkSystem::HandleChatMessage(CNetMessage* aMessage, CAddressWrapper&)
{
	CNetMessageChatMessage* message = static_cast<CNetMessageChatMessage*>(aMessage);
	SClientData* client = myClients.GetClientByID(aMessage->GetSenderID());
	if (client)
	{
		std::string printMessage = client->myName + ": " + message->GetChatMessage();
		myUIMediator.AddToLog(printMessage, CMainServerUIMediator::Normal);
	}
}

void CMainServerNetworkSystem::HandleConnectionMessage(CNetMessage* aMessage, CAddressWrapper & aSender)
{
	CNetMessageConnect* connectionMessageReceived = static_cast<CNetMessageConnect*>(aMessage);
	CNetMessageConnect::EConnectionState connectionState = connectionMessageReceived->GetConnectionState();

	if (connectionState == CNetMessageConnect::EConnectionState_TryingToConnect)
	{
		CNetMessageConnect* connectMessageAnswer;
		const SClientData* client = myClients.AddClient(aSender);

		if (client != nullptr)
		{
			myNetMessageManager.AddReliableMessageSystem(client->myID);
			connectMessageAnswer = myNetMessageManager.CreateMessage<CNetMessageConnect>(client->myID);
			connectMessageAnswer->SetConnectionState(CNetMessageConnect::EConnectionState_Ok);
			connectMessageAnswer->SetStartTime(myTimer.GetServerTimer());
		}
		else
		{
			connectMessageAnswer = myNetMessageManager.CreateMessage<CNetMessageConnect>();
			connectMessageAnswer->SetConnectionState(CNetMessageConnect::EConnectionState_Full);
		}

		myNetMessageManager.PackAndSendMessage(connectMessageAnswer, aSender);
	}
	else if (connectionState == CNetMessageConnect::EConnectionState_Disconnect)
	{
		const SClientData* client = myClients.GetClientByID(connectionMessageReceived->GetSenderID());
		if (client)
		{
			myUIMediator.AddToLog(client->myName + " disconnected from main server", CMainServerUIMediator::Error);
			DisconnectPlayer(client->myID);
		}
	}
	else
	{
		myUIMediator.AddToLog("Client trying to connect to main server with wrong type of connection state", CMainServerUIMediator::EMessageColor::Warning);
	}
}

void CMainServerNetworkSystem::SaveClientName(CNetMessage* aMessage, CAddressWrapper&)
{
	CNetMessageClientName* clientName = static_cast<CNetMessageClientName*>(aMessage);
	unsigned short clientID = clientName->GetSenderID();
	SClientData* client = myClients.GetClientByID(clientID);
	
	if (client)
	{
		client->myName = clientName->GetClientName();
		myUIMediator.AddToLog(clientName->GetClientName() + " connected to main server", CMainServerUIMediator::EMessageColor::Valid);
	}
}

void CMainServerNetworkSystem::RecievePings(CNetMessage* aMessage, CAddressWrapper&)
{
	CNetMessagePing* ping = static_cast<CNetMessagePing*>(aMessage);

	SClientData* sender = myClients.GetClientByID(ping->GetSenderID());
	if (!sender)
	{
		myUIMediator.AddToLog("Recieved Ping from unkown ID: " + std::to_string(ping->GetSenderID()), CMainServerUIMediator::EMessageColor::Error);
		return;
	}

	if (ping->GetPingState() == CNetMessagePing::EPingState_Pong)
	{
		unsigned int rtt = static_cast<unsigned int>((myTimer.GetServerTimerInMS() - ping->GetPingTimestamp()));
		sender->myLatestPingTimer = 0;
		sender->myRTT = rtt;
		sender->myPing = rtt / 2;
	}
	else if (ping->GetPingState() == CNetMessagePing::EPingState_Ping)
	{
	
		CNetMessagePing* pong = myNetMessageManager.CreateMessage<CNetMessagePing>(sender->myID);
		pong->SetPingState(CNetMessagePing::EPingState_Pong);
		pong->SetPingTimestamp(ping->GetTimeStamp());
		myNetMessageManager.PackAndSendMessage(pong, sender->myAddress);
	}
}

void CMainServerNetworkSystem::HandleGameServerConnection(CNetMessage * aMessage, CAddressWrapper &)
{
	CNetMessageGameServerConnection* gameServerConnectionMessage = static_cast<CNetMessageGameServerConnection*>(aMessage);
	SClientData* client = myClients.GetClientByID(gameServerConnectionMessage->GetSenderID());
	if (client)
	{
		if (gameServerConnectionMessage->GetPlayerConnectionState() == CNetMessageGameServerConnection::Join)
		{
			myUIMediator.AddToLog(client->myName + " joined GameServer: " + std::to_string(myGameServer.GetGameServerID()), CMainServerUIMediator::EMessageColor::Valid);
			client->myGameServerID = myGameServer.GetGameServerID();
			myGameServer.AddClient(*client);
		}
		else if (gameServerConnectionMessage->GetPlayerConnectionState() == CNetMessageGameServerConnection::Leave)
		{
			myUIMediator.AddToLog(client->myName + " left GameServer: " + std::to_string(myGameServer.GetGameServerID()), CMainServerUIMediator::EMessageColor::Warning);
			client->myGameServerID = -1;
			myGameServer.RemoveClientByID(*client);
		}
	}
}

void CMainServerNetworkSystem::SendLuaCommandMessage(const std::string& aText, unsigned short aClientID)
{
	SClientData* client = myClients.GetClientByID(aClientID);
	if (!client)
	{
		myUIMediator.AddToLog("Failed to Send Chat Message to Client ID: " + std::to_string(aClientID), CMainServerUIMediator::EMessageColor::Error);
		return;
	}
	else
	{
		CNetMessageLuaCommand* luaCommandMessage = myNetMessageManager.CreateMessage<CNetMessageLuaCommand>(client->myID);
		luaCommandMessage->SetCommand(aText);
		myNetMessageManager.PackAndSendMessage(luaCommandMessage, client->myAddress);
	}
}

void CMainServerNetworkSystem::KickAllClients()
{
	for (unsigned short i = myClients.GetClientCount(); i > 0; --i)
	{
		myNetMessageManager.RemoveReliableMessageSystem(myClients.GetClientByIndex(i - 1)->myID);
		KickClientByIndex(i - 1);
	}
}

void CMainServerNetworkSystem::KickClientByIndex(unsigned short aIndex)
{
	SClientData* client = myClients.GetClientByIndex(aIndex);
	if (client)
	{
		myUIMediator.AddToLog(client->myName + " kicked from main server", CMainServerUIMediator::Error);
		DisconnectPlayer(client->myID);
	}
}

void CMainServerNetworkSystem::GetMyIP(char*& aIPbuffer)
{
	char szHostName[255];
	gethostname(szHostName, sizeof(szHostName));
	struct hostent *host_entry;
	host_entry = gethostbyname(szHostName);
	aIPbuffer = inet_ntoa(*(struct in_addr *)*host_entry->h_addr_list);
}

void CMainServerNetworkSystem::PingClients()
{
	for (unsigned short i = 0; i < myClients.GetClientCount(); i++)
	{
		SClientData* client = myClients.GetClientByIndex(i);

		CNetMessagePing* ping;
		ping = myNetMessageManager.CreateMessage<CNetMessagePing>(client->myID);
		ping->SetPingState(CNetMessagePing::EPingState_Ping);
		myNetMessageManager.PackAndSendMessage(ping, client->myAddress);
	}
}

void CMainServerNetworkSystem::UpdatePings()
{
	myPingTimerInMS += myTimer.GetRealDeltaTimeInMS();
	if (myPingTimerInMS >= myPingFrequencyInMS)
	{
		PingClients();
		myPingTimerInMS = 0.f;
	}

	UpdateClientsPingTimer();
}

void CMainServerNetworkSystem::UpdateClientsPingTimer()
{
	for (unsigned short i = myClients.GetClientCount(); i > 0; --i)
	{
		SClientData* client = myClients.GetClientByIndex(i - 1);
		client->myLatestPingTimer += myTimer.GetRealDeltaTimeInMS();

		if (client->myLatestPingTimer >= myTimoutTimeInMS)
		{
			myUIMediator.AddToLog(client->myName + " timed out from main server", CMainServerUIMediator::EMessageColor::Error);
			DisconnectPlayer(myClients.GetClientByIndex(i - 1)->myID);
		}
	}
}

void CMainServerNetworkSystem::DisconnectPlayer(unsigned short aClientID)
{
	myNetMessageManager.RemoveReliableMessageSystem(aClientID);

	SClientData* client = myClients.GetClientByID(aClientID);
	if (client)
	{
		if (client->myGameServerID != -1)
		{
			if (myGameServer.GetGameServerID() == client->myGameServerID)
			{
				myUIMediator.AddToLog(client->myName + " disconnected from GameServer: " + std::to_string(myGameServer.GetGameServerID()), CMainServerUIMediator::EMessageColor::Error);
				myGameServer.RemoveClientByID(*client);
			}
		}
	}
	myClients.RemoveClientByID(aClientID);
}

#pragma once
#include "MainServerClients.h"
#include "NetMessageManager.h"
#include "MainServerUIMediator.h"
#include "Vector.h"
#include "GameServer.h"

class CMainServerNetworkSystem
{
public:
	CMainServerNetworkSystem(CommonUtilities::Timer& aTimer, CMainServerUIMediator& aUIMediator);
	~CMainServerNetworkSystem();

	bool Init();
	void Shutdown();

	void PreUpdate();
	void Update();
	void PostUpdate();

	void GoOnline();
	void GoOffline();

	void SendLuaCommandToClient(const std::string& aCommand, unsigned short aClientID);
	void BroadcastLuaCommand(const std::string& aCommand);

public:
	void HandleChatMessage(CNetMessage* aMessage, CAddressWrapper&);
	void HandleConnectionMessage(CNetMessage* aMessage, CAddressWrapper& aSender);
	void SaveClientName(CNetMessage* aMessage, CAddressWrapper&);
	void RecievePings(CNetMessage* aMessage, CAddressWrapper&);
	void HandleGameServerConnection(CNetMessage* aMessage, CAddressWrapper&);

	void PingClients();
	void UpdatePings();
	void UpdateClientsPingTimer();
	void DisconnectPlayer(unsigned short aClientID);

	void SendLuaCommandMessage(const std::string& aText, unsigned short aClientID);

	void KickAllClients();
	void KickClientByIndex(unsigned short aIndex);
	void GetMyIP(char*& aIPbuffer);
	CMainServerClients myClients;

	CNetMessageManager myNetMessageManager;
	char myBuffer[256];

	float myPingFrequencyInMS = 200.f;
	float myPingTimerInMS = 0.f;
	float myTimoutTimeInMS = 2000.f;

	CommonUtilities::Timer& myTimer;
	CMainServerUIMediator& myUIMediator;

	CGameServer myGameServer;

	float myUptime;
};


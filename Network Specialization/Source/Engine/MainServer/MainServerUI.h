#pragma once

struct ID3D11Device;
struct ID3D11DeviceContext;
struct HWND__;
typedef HWND__* HWND;

class CMainServerNetworkSystem;

#include "MainServerClients.h"
#include "MainServerUIMediator.h"

#include "Vector.h"
#include "GrowingArray.h"
#include <string>
#include <vector>
#include <functional>
#include "IServerNetwork.h"

#define ADDR_LEN_STR 22

class CMainServerUI
{
public:
	CMainServerUI(CMainServerUIMediator& aUIMediator, CommonUtilities::Timer& aTimer);
	~CMainServerUI();

	bool Init(HWND aHWND, ID3D11Device* aDevice, ID3D11DeviceContext* aContext, const CommonUtilities::Vector2i& aWindowSize);
	void Shutdown();
	void Update();
	void Render();

	void SetGoOnlineCallback(std::function<void()> aCallback) { myGoOnlineCallback = aCallback; }
	void SetGoOfflineCallback(std::function<void()> aCallback) { myGoOfflineCallback = aCallback; }

	void SetSendLuaCommandCallback(std::function<void(const std::string&, unsigned short)> aFunction);
	void SetBroadcastLuaCommandCallback(std::function<void(const std::string&) > aFunction);

	inline void SetServerStatusText(const CMainServerUIMediator::SMessage& aMessage);

	void AddToLog(const std::string& aInput, CMainServerUIMediator::EMessageColor aColor);
	void AddMessageToLog(const CMainServerUIMediator::SMessage& aMessage);

	void PushFrameMS(float aMS);
private:
	void ShowServerStatus(bool* aShow);
	void ShowClients(bool* aShow);
	void ShowConsole(bool* aShow);
	void CloseWindows();
	void ExecuteCommand();
	void UpdateInAndOutBound();

	CommonUtilities::Vector4f GetColor(CMainServerUIMediator::EMessageColor aColor);

	std::array<SClientData, 512> myClients;
	CommonUtilities::GrowingArray<float> myFrameMS;

	std::string myTempInputBuffer;
	std::string myInputBuffer;
	std::string myCurrentCommandToExecute = "";

	std::string myPacketLoss;
	std::string myInbound;
	std::string myOutbound;
	std::string myFrameMSText;
	std::string myServerStatusText;

	CommonUtilities::Vector4f myServerStatusColor;
	CommonUtilities::Vector2f myWindowSize;

	std::function<void(const std::string&, unsigned short)> mySendLuaCommandCallback;
	std::function<void(const std::string&)> myBroadcastLuaCommandCallback;

	std::vector<CMainServerUIMediator::SMessage> myLogMessages;
	std::function<void()> myGoOnlineCallback;
	std::function<void()> myGoOfflineCallback;


	CMainServerUIMediator& myUIMediator;
	CommonUtilities::Timer& myTimer;

	unsigned short myClientCount;
	bool myShouldScroll;
	bool myShowClients;
	bool myShowConsole;
	bool myShowServerStatus;
	char myAddressBuffer[ADDR_LEN_STR];

};


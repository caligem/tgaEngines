#include "stdafx.h"
#include "MainServerUIMediator.h"

CMainServerUIMediator::CMainServerUIMediator()
{
}


CMainServerUIMediator::~CMainServerUIMediator()
{
}

void CMainServerUIMediator::UpdateClientData(unsigned short aClientCount, const std::array<SClientData, 512>& aClientData)
{
	myClientCount = aClientCount;
	myClientData = aClientData;
}

void CMainServerUIMediator::GetClientData(unsigned short & aClientCount, std::array<SClientData, 512>& aClientData)
{
	aClientCount = myClientCount;
	aClientData = myClientData;
}

void CMainServerUIMediator::AddToLog(const std::string & aString, EMessageColor aColor)
{
	myLogMessageBuffer.push_back(SMessage({ aColor, aString }));
}

void CMainServerUIMediator::GetLogMessage(std::function<void(const SMessage&)> aCallback)
{
	for (auto& message : myLogMessageBuffer)
	{
		aCallback(message);
	}

	myLogMessageBuffer.clear();
}
#include "stdafx.h"
#include "MainServerWindow.h"
#include <windows.h>

#include <d3d11.h>
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#include <d3dcompiler.h>
#pragma comment(lib,"d3dcompiler.lib")

#include <iostream>

#include "Mathf.h"

#define ValidateResult(aResult) if(FAILED(aResult)){return false;}

CMainServerWindow::CMainServerWindow()
{
}


CMainServerWindow::~CMainServerWindow()
{
}

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK WinProc(HWND aWindowHandle, UINT aWindowsMessage, WPARAM aWParam, LPARAM aLParam)
{
	if (aWindowsMessage == WM_DESTROY || aWindowsMessage == WM_CLOSE)
	{
		PostQuitMessage(0);
		return 0;
	}

	ImGui_ImplWin32_WndProcHandler(aWindowHandle, aWindowsMessage, aWParam, aLParam);
	return DefWindowProc(aWindowHandle, aWindowsMessage, aWParam, aLParam);
}


bool CMainServerWindow::InitDirectX()
{
	DXGI_SWAP_CHAIN_DESC swapchainDescription = {};
	swapchainDescription.BufferCount = 1;
	swapchainDescription.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapchainDescription.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapchainDescription.OutputWindow = myWindowHandle;
	swapchainDescription.SampleDesc.Count = 1;
	swapchainDescription.Windowed = true;


	UINT creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
	creationFlags |= D3D11_CREATE_DEVICE_DEBUG;

	ValidateResult(D3D11CreateDeviceAndSwapChain(nullptr, D3D_DRIVER_TYPE_HARDWARE, nullptr, creationFlags, nullptr, 0, D3D11_SDK_VERSION,
		&swapchainDescription, &mySwapchain, &myDevice, nullptr, &myContext));

	ID3D11Texture2D* backBufferTexture;

	ValidateResult(mySwapchain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backBufferTexture));
	ValidateResult(myDevice->CreateRenderTargetView(backBufferTexture, nullptr, &myBackBuffer));
	ValidateResult(backBufferTexture->Release());

	myContext->OMSetRenderTargets(1, &myBackBuffer, NULL);

	return true;
}

void CMainServerWindow::BeginFrame()
{
	myContext->ClearRenderTargetView(myBackBuffer, myClearColor);
}

void CMainServerWindow::EndFrame()
{
	mySwapchain->Present(0, 0);
}

void CMainServerWindow::Shutdown()
{
	DestroyWindow(myWindowHandle);
}

ID3D11Device * CMainServerWindow::GetDevice()
{
	return myDevice;
}

ID3D11DeviceContext * CMainServerWindow::GetContext()
{
	return myContext;
}

HWND CMainServerWindow::GetHWND()
{
	return myWindowHandle;
}

bool CMainServerWindow::Init()
{
	myWindowSize = { 1280, 720 };

	WNDCLASS windowClass = {};
	windowClass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	windowClass.lpfnWndProc = WinProc;
	windowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowClass.lpszClassName = L"MainServer";

	RegisterClass(&windowClass);


	int dwStyle = WS_OVERLAPPEDWINDOW ^ WS_MAXIMIZEBOX ^ WS_SIZEBOX | WS_POPUP;

	myWindowHandle = CreateWindow(L"MainServer", L"Main Server"
								, dwStyle, 0, 0, myWindowSize.x, myWindowSize.y
								, nullptr, nullptr, nullptr, nullptr);

	RECT rect;
	GetClientRect(myWindowHandle, &rect);

	SetWindowPos(myWindowHandle
				, HWND_TOP , 20, 20
				, myWindowSize.x + (myWindowSize.x - (rect.right - rect.left))
				, myWindowSize.y + (myWindowSize.y - (rect.bottom - rect.top))
				, SWP_SHOWWINDOW);

	if (!InitDirectX())
	{
		printf("Failed to Init DirectX");
		return false;
	}

	return true;
}

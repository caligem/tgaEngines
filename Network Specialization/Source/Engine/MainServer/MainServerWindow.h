#pragma once

struct IDXGISwapChain;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11RenderTargetView;
struct HWND__;
typedef HWND__* HWND;

#include "Vector.h"

class CMainServerWindow
{
public:
	CMainServerWindow();
	~CMainServerWindow();

	bool Init();
	bool InitDirectX();

	void BeginFrame();
	void EndFrame();
	void Shutdown();

	ID3D11Device* GetDevice();
	ID3D11DeviceContext* GetContext();
	HWND GetHWND();

	inline const CommonUtilities::Vector2i& GetWindowSize() const { return myWindowSize; }

private:
	float myClearColor[4] = { 0.1f, 0.1f, 0.1f, 1.f };
	CommonUtilities::Vector2i myWindowSize;

	IDXGISwapChain *mySwapchain;
	ID3D11Device *myDevice;
	ID3D11DeviceContext *myContext;
	ID3D11RenderTargetView *myBackBuffer;
	HWND myWindowHandle;
	
};


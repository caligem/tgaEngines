#include "stdafx.h"
#include "PlayerStats.h"


CPlayerStats::CPlayerStats()
{
}


CPlayerStats::~CPlayerStats()
{
}

void CPlayerStats::AddPlayerStats(unsigned short aPlayerID)
{
	myPlayerStats[aPlayerID] = SPlayerStat();
	myPlayerStats[aPlayerID].playerID = aPlayerID;
}

void CPlayerStats::RemovePlayerStats(unsigned short aPlayerID)
{
	if (myPlayerStats.find(aPlayerID) != myPlayerStats.end())
	{
		myPlayerStats.erase(aPlayerID);
	}
}

void CPlayerStats::AddKill(unsigned short aPlayerID)
{
	if (myPlayerStats.find(aPlayerID) != myPlayerStats.end())
	{
		myPlayerStats[aPlayerID].kills++;
	}
}

void CPlayerStats::AddDeath(unsigned short aPlayerID)
{
	if (myPlayerStats.find(aPlayerID) != myPlayerStats.end())
	{
		myPlayerStats[aPlayerID].deaths++;
	}
}

const CPlayerStats::SPlayerStat CPlayerStats::GetPlayerStats(unsigned short aPlayerID)
{
	if (myPlayerStats.find(aPlayerID) != myPlayerStats.end())
	{
		return myPlayerStats[aPlayerID];
	}
	return SPlayerStat();
}

void CPlayerStats::Reset()
{
	for (auto& stats : myPlayerStats)
	{
		stats.second.deaths = 0;
		stats.second.kills = 0;
	}
}

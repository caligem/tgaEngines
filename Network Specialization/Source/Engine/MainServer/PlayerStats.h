#pragma once
#include <unordered_map>

class CPlayerStats
{
public:
	struct SPlayerStat
	{
		unsigned short playerID = 0;
		unsigned short kills = 0;
		unsigned short deaths = 0;
	};

	CPlayerStats();
	~CPlayerStats();

	void AddPlayerStats(unsigned short aPlayerID);
	void RemovePlayerStats(unsigned short aPlayerID);
	void AddKill(unsigned short aPlayerID);
	void AddDeath(unsigned short aPlayerID);
	const SPlayerStat GetPlayerStats(unsigned short aPlayerID);
	void Reset();
private:

	std::unordered_map<unsigned short, SPlayerStat> myPlayerStats;
};


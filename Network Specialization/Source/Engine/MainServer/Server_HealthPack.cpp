#include "stdafx.h"
#include "Server_HealthPack.h"
#include "IServerNetwork.h"


CServer_HealthPack::CServer_HealthPack()
	: myIsTaken(false)
	, myHealingAmount(25)
	, myHealthPackSpawnTimer(0.f)
{
	myCollider.SetPosition(myPosition);
	myCollider.SetBounds({ mySize.x, mySize.y*3.f });
}


CServer_HealthPack::~CServer_HealthPack()
{
}

void CServer_HealthPack::Update(float aDeltaTime)
{
	myCollider.SetPosition(myPosition);

	if (myIsTaken)
	{
		myHealthPackSpawnTimer += aDeltaTime;

		if (myHealthPackSpawnTimer >= HealthPackSpawnFrequency)
		{
			myHealthPackSpawnTimer = 0.f;
			myIsTaken = false;

			CNetMessageHealthPackUpdate* healthPackUpdate = IServerNetwork::CreateMessage<CNetMessageHealthPackUpdate>();
			healthPackUpdate->SetHealthpackID(myID);
			healthPackUpdate->SetHealthpackState(CNetMessageHealthPackUpdate::EHealthpackState::Respawned);
			BroadCastMessageToGameServer(healthPackUpdate);
		}
	}
}

void CServer_HealthPack::Taken()
{
	myIsTaken = true;

	CNetMessageHealthPackUpdate* healthPackUpdate = IServerNetwork::CreateMessage<CNetMessageHealthPackUpdate>();
	healthPackUpdate->SetHealthpackID(myID);
	healthPackUpdate->SetHealthpackState(CNetMessageHealthPackUpdate::EHealthpackState::Taken);
	BroadCastMessageToGameServer(healthPackUpdate);
}

void CServer_HealthPack::Reset()
{
	myIsTaken = false;
	myHealthPackSpawnTimer = 0.f;
}

#pragma once
#include "Shared_HealthPack.h"
#include "ColliderBox.h"
#include "BroadCaster.h"

class CServer_HealthPack : public CShared_HealthPack, public CBroadCaster
{
public:
	CServer_HealthPack();
	~CServer_HealthPack();

	void Update(float aDeltaTime);

	void SetPosition(const CommonUtilities::Vector2f& aPosition) { myPosition = aPosition; }

	void Taken();
	inline const bool IsTaken() const { return myIsTaken; }

	const CColliderBox& GetCollider() const { return myCollider; }
	char GetHealingAmount() const { return myHealingAmount; }
	void Reset();
private:
	CColliderBox myCollider;

	static constexpr float HealthPackSpawnFrequency = 10.f;
	float myHealthPackSpawnTimer;

	char myHealingAmount;
	bool myIsTaken;
};


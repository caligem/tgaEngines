#include "stdafx.h"
#include "Server_Player.h"
#include "IServerNetwork.h"
#include "Random.h"
#include "PlayerStats.h"

CServer_Player::CServer_Player()
	: myHealth(MaxHealth)
	, myIsDead(false)
	, myRespawnTimer(0.f)
{
}


CServer_Player::~CServer_Player()
{
}

void CServer_Player::Update(float aDeltaTime)
{
	if (myIsDead)
	{
		myRespawnTimer += aDeltaTime;
		if (myRespawnTimer >= RespawnTime)
		{
			myRespawnTimer = 0.f;
			Respawn();
		}
	}

	UpdateMovement(aDeltaTime);
}

void CServer_Player::TakeDamage(char aDamage, unsigned short aShooterID, CPlayerStats& aPlayerStats)
{
	myHealth -= aDamage;

	CNetMessagePlayerHealthStatus* message = IServerNetwork::CreateMessage<CNetMessagePlayerHealthStatus>();
	message->SetHealthStatus(CNetMessagePlayerHealthStatus::EHealthStatus::TakeDamage);
	message->SetHealthValue(aDamage);
	message->SetPlayerID(myPlayerID);
	BroadCastMessageToGameServer(message);

	if (myHealth <= 0 && !myIsDead)
	{
		aPlayerStats.AddDeath(myPlayerID);
		aPlayerStats.AddKill(aShooterID);
		CNetMessagePlayerFrag* fragMessage = IServerNetwork::CreateMessage<CNetMessagePlayerFrag>();
		fragMessage->SetKillerID(aShooterID);
		fragMessage->SetVictimID(myPlayerID);
		BroadCastMessageToGameServer(fragMessage);

		myHealth = 0;
		myIsDead = true;
	}
}

void CServer_Player::Heal(char aValue)
{
	myHealth += aValue;
	if (myHealth >= MaxHealth)
	{
		myHealth = MaxHealth;
	}

	CNetMessagePlayerHealthStatus* message = IServerNetwork::CreateMessage<CNetMessagePlayerHealthStatus>();
	message->SetHealthStatus(CNetMessagePlayerHealthStatus::EHealthStatus::Heal);
	message->SetHealthValue(aValue);
	message->SetPlayerID(myPlayerID);
	BroadCastMessageToGameServer(message);
}

void CServer_Player::Respawn()
{
	myHealth = MaxHealth;
	myIsDead = false;

	float randomSpawnPointIndex = CommonUtilities::RandomRange(0.f, static_cast<float>(mySpawnPoints.size()));
	myPosition = mySpawnPoints[static_cast<char>(randomSpawnPointIndex)];

	myMovement = { 0.f, 0.f };
	myIsOnGround = false;

	CNetMessagePlayerHealthStatus* message = IServerNetwork::CreateMessage<CNetMessagePlayerHealthStatus>();
	message->SetHealthStatus(CNetMessagePlayerHealthStatus::EHealthStatus::Respawn);
	message->SetHealthValue(MaxHealth);
	message->SetPlayerID(myPlayerID);
	message->SetRespawnPosition(myPosition);
	BroadCastMessageToGameServer(message);
}

void CServer_Player::SetPosition(CommonUtilities::Vector2f pos)
{
	myPosition = pos;
	myCollider.position = pos;
}

void CServer_Player::Reset()
{
	Respawn();
}

void CServer_Player::Spawn()
{
	float randomSpawnPointIndex = CommonUtilities::RandomRange(0.f, static_cast<float>(mySpawnPoints.size()));
	myPosition = mySpawnPoints[static_cast<char>(randomSpawnPointIndex)];
}

void CServer_Player::SetSpawnPoints(const std::vector<CommonUtilities::Vector2f>& aSpawnPoints)
{
	mySpawnPoints = aSpawnPoints;
}

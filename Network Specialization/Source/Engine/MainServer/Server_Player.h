#pragma once
#include "Shared_Player.h"
#include "BroadCaster.h"
#include <vector>

class CPlayerStats;

class CServer_Player : public CShared_Player, public CBroadCaster
{
public:
	CServer_Player();
	~CServer_Player();

	void SetSpawnPoints(const std::vector<CommonUtilities::Vector2f>& aSpawnPoints);

	void Update(float aDeltaTime);

	void TakeDamage(char aDamage, unsigned short aShooterID, CPlayerStats& aPlayerStats);
	void Heal(char aValue);

	bool IsDead() const { return myIsDead; }
	void Spawn();

	char GetHealth() const { return myHealth; }

	void Reset();
	inline void SetLastTimeShoot(unsigned int aTimestamp) { myLastShootTimestamp = aTimestamp; }
	inline bool CanShoot(unsigned int aTimeStamp) { return (aTimeStamp - myLastShootTimestamp) >= (myShootCooldown * 1000.f); }

private:
	void Respawn();

	std::vector<CommonUtilities::Vector2f> mySpawnPoints;

	unsigned int myLastShootTimestamp;

	static constexpr float RespawnTime = 1.f;
	float myRespawnTimer;

	static constexpr char MaxHealth = 100;
	char myHealth;

	bool myIsDead;
public:
	void SetPosition(CommonUtilities::Vector2f pos);
};


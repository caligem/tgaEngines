#include "stdafx.h"
#include "Server_Projectile.h"


CServer_Projectile::CServer_Projectile()
	: myDamage(10)
{
	myIsActive = false;
	myIsDead = true;
}


CServer_Projectile::~CServer_Projectile()
{
}

void CServer_Projectile::Shoot(const CommonUtilities::Vector2f& aPostition, const CommonUtilities::Vector2f& aDirection, unsigned short aShooterID, unsigned short aProjectileIndex)
{
	myIsDead = false;
	myIsActive = true;
	myProjectileIndex = aProjectileIndex;
	myPosition = aPostition;
	myDirection = aDirection.GetNormalized();
	myShooterID = aShooterID;
	myCollider.SetPosition(myPosition);
	myDistanceTraveled = 0.f;
}

void CServer_Projectile::Update(float aDeltaTime)
{
	const CommonUtilities::Vector2f newPosition = myPosition + myDirection * mySpeed * aDeltaTime;
	myDistanceTraveled += (newPosition - myPosition).Length();

	if (myDistanceTraveled >= myMaxTravelDistance)
	{
		myIsDead = true;
	}
	else
	{
		myPosition = newPosition;
		myCollider.SetPosition(myPosition);
	}
}
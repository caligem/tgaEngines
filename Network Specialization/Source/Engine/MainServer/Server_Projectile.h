#pragma once
#include "Shared_Projectile.h"

class CServer_Projectile : public CShared_Projectile
{
public:
	CServer_Projectile();
	virtual ~CServer_Projectile();

	void Update(float aDeltaTime);

	void Shoot(const CommonUtilities::Vector2f& aPostition, const CommonUtilities::Vector2f& aDirection, unsigned short aShooterID, unsigned short aProjectileIndex);

	void SetIsDead() { myIsDead = true; }
	inline const bool IsDead() const { return myIsDead; }
	
	inline const bool IsActive() const { return myIsActive; }

	inline void Kill() { myIsActive = false; }
	char GetDamage() const { return myDamage; }

private:
	char myDamage;
	CommonUtilities::Vector2f myDirection;
	float myDistanceTraveled;
	static constexpr float myMaxTravelDistance = 20.f;
	bool myIsActive;
	bool myIsDead;
};


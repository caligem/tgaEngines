#include "stdafx.h"
#include "Server_ProjectileManager.h"
#include "IServerNetwork.h"
#include "ICollision.h"

CServer_ProjectileManager::CServer_ProjectileManager(CommonUtilities::Timer& aTimer)
	: CShared_ProjectileManager(aTimer)
	, myAvaliableProjectileID(0)
{
}


CServer_ProjectileManager::~CServer_ProjectileManager()
{
}

void CServer_ProjectileManager::SendExisitingProjectiles(unsigned short aClientID, CAddressWrapper& aClientAddress)
{
	for (auto& projectile : myProjectiles)
	{
		if (projectile.IsActive())
		{
			CNetMessageCreateProjectile* message = IServerNetwork::CreateMessage<CNetMessageCreateProjectile>(aClientID);
			message->SetShooterID(projectile.GetShooterID());
			IServerNetwork::SendToClient(message, aClientAddress);
		}
	}
}

void CServer_ProjectileManager::FirePlayerProjectile(const CommonUtilities::Vector2f& aStartPosition, const CommonUtilities::Vector2f& aDirection, unsigned short aShooterID)
{
	unsigned short projectileIndex = GetAvailablePlayerProjectileIndex();
	CServer_Projectile& projectile = myProjectiles[projectileIndex];
	projectile.Shoot(aStartPosition, aDirection, aShooterID, projectileIndex);
}

void CServer_ProjectileManager::HandleProjectileDeath(CServer_Projectile& aProjectile)
{
	myAvailablePlayerProjectiles.push(aProjectile.GetProjectileIndex());
	aProjectile.Kill();
}

void CServer_ProjectileManager::CheckCollisionVsPlayer(CServer_Player& player, CPlayerStats& aPlayerStats)
{
	for (auto& projectile : myProjectiles)
	{
		if (!projectile.IsActive() || (player.GetPlayerID() == projectile.GetShooterID()))
		{
			continue;
		}

		if (ICollision::CircleVSBox(player.GetCollider(), projectile.GetCollider()))
		{
			player.TakeDamage(projectile.GetDamage(), projectile.GetShooterID(), aPlayerStats);
			projectile.SetIsDead();
		}
	}
}

void CServer_ProjectileManager::ProjectileVsWorldColliders(const std::vector<CColliderBox>& someWorldColliders)
{
	for (auto& projectile : myProjectiles)
	{
		if(projectile.IsActive())
		{
			for (auto& collider : someWorldColliders)
			{
				if (ICollision::BoxVsBox(projectile.GetCollider(), collider))
				{
					projectile.SetIsDead();
				}
			}
		}
	}
}

#pragma once
#include "Server_Projectile.h"
#include "Server_Player.h"
#include "Timer.h"
#include "ClientData.h"
#include <queue>

#include "BroadCaster.h"
#include "Shared_ProjectileManager.h"

class CNetMessage;
class CServer_ProjectileManager : public CBroadCaster, public CShared_ProjectileManager<CServer_Projectile>
{
public:
	CServer_ProjectileManager(CommonUtilities::Timer& aTimer);
	~CServer_ProjectileManager();

	void HandleProjectileDeath(CServer_Projectile& aProjectile) override;

	void SendExisitingProjectiles(unsigned short aClientID, CAddressWrapper& aClientAddress);

	void FirePlayerProjectile(const CommonUtilities::Vector2f& aStartPosition, const CommonUtilities::Vector2f& aDirection, unsigned short aShooterID);

	void CheckCollisionVsPlayer(CServer_Player& player, CPlayerStats& aPlayerStats);
	void ProjectileVsWorldColliders(const std::vector<CColliderBox>& someWorldColliders);

private:
	unsigned short GetProjectileID() { return myAvaliableProjectileID++; }
	unsigned short myAvaliableProjectileID;
};


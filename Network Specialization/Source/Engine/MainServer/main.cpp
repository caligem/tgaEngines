#include "stdafx.h"

#include <iostream>
#include "windows.h"
#include "MainServer.h"

void AllocateConsole()
{
	AllocConsole();
	FILE* cinFile;
	freopen_s(&cinFile, "CONIN$", "r", stdin);
	FILE* coutFile;
	freopen_s(&coutFile, "CONOUT$", "w", stdout);
	FILE* cerrFile;
	freopen_s(&cerrFile, "CONOUT$", "w", stderr);
}

INT WINAPI wWinMain(HINSTANCE, HINSTANCE, LPWSTR, int)
{
	AllocateConsole();
	CMainServer mainServer;
	bool result = mainServer.Init();

	if (!result)
	{
		printf("Failed to start Main Server");
	}

	MSG windowMessage = { 0 };
	bool running = result;

	while (running)
	{
		while (PeekMessage(&windowMessage, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&windowMessage);
			DispatchMessage(&windowMessage);

			if (windowMessage.message == WM_QUIT)
			{
				running = false;
			}
		}
		mainServer.Update();
		mainServer.Render();
		std::this_thread::yield();
	}

	mainServer.Shutdown();
	FreeConsole();
	return 0;
}
#include "stdafx.h"
#include "AddressWrapper.h"

#include "CommonNetworkIncludes.h"

CAddressWrapper::CAddressWrapper()
{
	myAddress = 0;
	myPort = 0;
}


CAddressWrapper::~CAddressWrapper()
{
}

CAddressWrapper::CAddressWrapper(unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short aPort)
{
	myAddress = (a << 24) | (b << 16) | (c << 8) | d;
	myPort = aPort;
}

CAddressWrapper::CAddressWrapper(unsigned int aAddress, unsigned short aPort)
{
	myAddress = aAddress;
	myPort = aPort;
}

CAddressWrapper::CAddressWrapper(const char * aAddress, unsigned short aPort)
{
	int IP = 0;
	inet_pton(AF_INET, aAddress, &IP);
	myAddress = ntohl(IP);
	myPort = aPort;
}

void CAddressWrapper::Init(unsigned char a, unsigned char b, unsigned char c, unsigned char d, unsigned short aPort)
{
	myAddress = (a << 24) | (b << 16) | (c << 8) | d;
	myAddress = ntohl(myAddress);
	myPort = aPort;
}

void CAddressWrapper::Init(unsigned int aAddress, unsigned short aPort)
{
	myAddress = aAddress;
	myPort = aPort;
}

void CAddressWrapper::Init(const char * aAddress, unsigned short aPort)
{
	int IP = 0;
	inet_pton(AF_INET, aAddress, &IP);
	myAddress = ntohl(IP);
	myPort = aPort;
}

unsigned int CAddressWrapper::GetAddress() const
{
	return myAddress;
}

void CAddressWrapper::GetAddress(char * aBuffer)
{
	int IP = htonl(myAddress);
	inet_ntop(AF_INET, &IP, aBuffer, INET_ADDRSTRLEN);
}

unsigned short CAddressWrapper::GetPort() const
{
	return myPort;
}

#pragma once

class CSocketWrapper;

class CAddressWrapper
{
public:
	CAddressWrapper();
	~CAddressWrapper();

	CAddressWrapper(unsigned char a, 
					unsigned char b, 
					unsigned char c, 
					unsigned char d, 
					unsigned short aPort);

	CAddressWrapper(unsigned int aAddress, unsigned short aPort);

	CAddressWrapper(const char* aAddress, unsigned short aPort);

	void Init(unsigned char a,
					unsigned char b,
					unsigned char c,
					unsigned char d,
					unsigned short aPort);

	void Init(unsigned int aAddress, unsigned short aPort);
	void Init(const char* aAddress, unsigned short aPort);

	unsigned int GetAddress() const;
	void GetAddress(char* aBuffer);
	unsigned short GetPort() const;

private:
	unsigned int myAddress;
	unsigned short myPort;
};


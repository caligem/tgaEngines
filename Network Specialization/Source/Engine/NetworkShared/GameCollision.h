#pragma once
#include <vector>
#include "../Physics/ColliderBox.h"
#include "../Physics/ICollision.h"
#include "../Physics/ColliderSphere.h"

namespace IGameCollision
{
	template<typename Player>
	void PlayerVSWorldCollision(Player& aPlayer, const std::vector<CColliderBox>& someWorldColliders);
	
	template<typename Player>
	void PlayerVSWorldCollision(Player& aPlayer, const std::vector<CColliderBox>& someWorldColliders)
	{
		CommonUtilities::Vector2f playerPos = aPlayer.GetPosition();

		for (auto& collider : someWorldColliders)
		{
			CommonUtilities::Vector2f contactPoint;
			if (ICollision::CircleVSBox(playerPos, aPlayer.GetCollider().radius, collider, &contactPoint))
			{
				CommonUtilities::Vector2f normal = (playerPos - contactPoint).GetNormalized();
				const CommonUtilities::Vector2f& movement = aPlayer.GetMovementNextFrame();
				float collisionLength = aPlayer.GetCollider().radius - (playerPos - contactPoint).Length();
				playerPos = playerPos + normal * collisionLength;
				if (normal.Dot(movement) <= 0.f)
				{
					if (normal.y >= 0.9f)
					{
						aPlayer.CollidedWithFloor();
					}
					else if (normal.y <= -0.9f)
					{

						aPlayer.CollidedWithRoof();
					}
				}
				aPlayer.CorrectPosition(playerPos);
			}
		}
	}
};
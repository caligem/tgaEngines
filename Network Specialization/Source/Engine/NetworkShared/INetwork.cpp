#include "stdafx.h"
#include "INetwork.h"

CNetMessageManager* INetwork::ourNetMessageManager = nullptr;

INetwork::INetwork()
{
}


INetwork::~INetwork()
{
}

const size_t INetwork::GetInbound()
{
	return ourNetMessageManager->GetInbound();
}

const size_t INetwork::GetOutbound()
{
	return ourNetMessageManager->GetOutbound();
}

const float INetwork::GetPackageLoss()
{
	return ourNetMessageManager->GetPackageLoss();
}

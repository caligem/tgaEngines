#pragma once

#include "NetMessageManager.h"
#include "NetMessageIncludes.h"

class INetwork
{
public:
	INetwork();
	~INetwork();

	template<typename MessageType>
	static void RegisterDoUnPackWorkCallback(std::function<void(CNetMessage*, CAddressWrapper&)> aFunction);

	template<typename MessageType>
	static void UnRegisterDoUnPackWorkCallback();

	template<typename MessageType>
	static MessageType* CreateMessage(unsigned short aTargetID = 512);

	static const size_t GetInbound();
	static const size_t GetOutbound();

	static const float GetPackageLoss();

protected:
	static CNetMessageManager* ourNetMessageManager;
};

template<typename MessageType>
inline void INetwork::RegisterDoUnPackWorkCallback(std::function<void(CNetMessage*, CAddressWrapper&)> aFunction)
{
	ourNetMessageManager->RegisterDoUnPackWorkCallback<MessageType>(aFunction);
}

template<typename MessageType>
inline void INetwork::UnRegisterDoUnPackWorkCallback()
{
	ourNetMessageManager->RegisterDoUnPackWorkCallback<MessageType>(nullptr);
}

template<typename MessageType>
inline MessageType * INetwork::CreateMessage(unsigned short aTargetID)
{
	return ourNetMessageManager->CreateMessage<MessageType>(aTargetID);
}

#include "stdafx.h"
#include "NetMessage.h"

CNetMessage::CNetMessage()
{
	myMessageTypeID = 0;
	mySenderID = 0;
	myTargetID = 0;
	myTimestamp = 0;
}

CNetMessage::~CNetMessage()
{
}

void CNetMessage::PackMessage()
{
	SERIALIZE(myStream, myMessageTypeID);
	SERIALIZE(myStream, myMessageID);
	SERIALIZE(myStream, myTimestamp);
	SERIALIZE(myStream, mySenderID);
	SERIALIZE(myStream, myTargetID);
	DoSerialize(myStream);

	assert(myStream.size() <= 512 && "Trying to send message over 512 bytes, lower byte count");
}

void CNetMessage::UnPackMessage(char * aMessage, int aSize)
{
	myStream.resize(aSize);
	memcpy(&myStream[0], aMessage, aSize);
	DESERIALIZE(myStream, myMessageTypeID);
	DESERIALIZE(myStream, myMessageID);
	DESERIALIZE(myStream, myTimestamp);
	DESERIALIZE(myStream, mySenderID);
	DESERIALIZE(myStream, myTargetID);
	DoDeSerialize(myStream);
}

void CNetMessage::SetGeneralData(unsigned int aMessageTypeID, unsigned int aMessageID, unsigned int aTimeStamp, unsigned short aSenderID, unsigned short aTargetID)
{
	myMessageTypeID = aMessageTypeID;
	myMessageID = aMessageID;
	myTimestamp = aTimeStamp;
	mySenderID = aSenderID;
	myTargetID = aTargetID;
}
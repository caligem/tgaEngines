#pragma once
#include <functional>
#include "AddressWrapper.h"
#include "SerializeHelper.h"

#define SERIALIZE(aStream, aType) serialize(aType, aStream)
#define DESERIALIZE(aStream, aType) aType = deserialize<decltype(aType)>(aStream);

enum ENetMessageFlags
{
	Reliable = 1,
	Unreliable = 0,
	Encrypted = 1,
	Unencrypted = 0
};

#define NETMESSAGE_DECLARE(className, isEncrypted, isReliable) class CNetMessageManager; \
class className  : public CNetMessage {  \
protected: \
	friend CNetMessageManager; \
    virtual inline const char * ClassID() override { return #className; } \
	virtual inline void BindFlags() { myIsEncrypted = isEncrypted; myIsReliable = isReliable; } \
private:

class CNetMessageManager;

class CNetMessage
{
public:
	CNetMessage();
	virtual ~CNetMessage();

	inline const unsigned short GetSenderID() const { return mySenderID; }
	inline const unsigned short GetTargetID() const { return myTargetID; }
	inline const unsigned int GetTimeStamp() const { return myTimestamp; }

	inline void SetTargetID(unsigned short aTargetID) { myTargetID = aTargetID; }

	StreamType myStream;

	inline const bool IsEncrypted() const { return myIsEncrypted; }
	inline const bool IsReliable() const { return myIsReliable; }

protected:
	virtual inline const char * ClassID() = 0;
	virtual inline void BindFlags() = 0;
	
	virtual void DoSerialize(StreamType&) {}
	virtual void DoDeSerialize(StreamType&) {}

	inline virtual void ClearMessage() { myStream.clear(); }

	bool myIsEncrypted;
	bool myIsReliable;
private:
	friend CNetMessageManager;


	inline void SetUnPackWorkCallback(std::function<void(CNetMessage*, CAddressWrapper&)> aCallback) { myDoUnPackWorkCallback = aCallback; }
	void SetGeneralData(unsigned int aMessageTypeID, unsigned int aMessageID, unsigned int aTimeStamp, unsigned short aSenderID, unsigned short aTargetID);

	void PackMessage();
	void UnPackMessage(char* aMessage, int aSize);
	inline void DoUnPackWork(CNetMessage* aMessage, CAddressWrapper& aSenderAddress) { if(myDoUnPackWorkCallback) myDoUnPackWorkCallback(aMessage, aSenderAddress); }

	std::function<void(CNetMessage*, CAddressWrapper&)> myDoUnPackWorkCallback;
	unsigned int myMessageTypeID;
	unsigned int myMessageID;
	unsigned int myTimestamp;
	unsigned short myTargetID;
	unsigned short mySenderID;
};
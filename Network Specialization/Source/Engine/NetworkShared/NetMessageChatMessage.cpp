#include "stdafx.h"
#include "NetMessageChatMessage.h"

CNetMessageChatMessage::CNetMessageChatMessage()
{

}


CNetMessageChatMessage::~CNetMessageChatMessage()
{
}

void CNetMessageChatMessage::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myMessage);
}

void CNetMessageChatMessage::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myMessage);
}

#pragma once
#include "NetMessage.h"


NETMESSAGE_DECLARE(CNetMessageChatMessage, Unencrypted, Reliable)
public:
	CNetMessageChatMessage();
	~CNetMessageChatMessage();

	inline void SetChatMessage(const std::string& aMessage) { myMessage = aMessage; }
	inline const std::string& GetChatMessage() { return myMessage; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	std::string myMessage;
};

#include "stdafx.h"
#include "NetMessageClientName.h"

CNetMessageClientName::CNetMessageClientName()
{
}


CNetMessageClientName::~CNetMessageClientName()
{
}

void CNetMessageClientName::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myClientName);
}

void CNetMessageClientName::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myClientName);
}

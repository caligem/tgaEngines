#pragma once
#include "NetMessage.h"

NETMESSAGE_DECLARE(CNetMessageClientName, Unencrypted, Reliable)
public:
	CNetMessageClientName();
	~CNetMessageClientName();

	inline void SetClientName(const std::string& aClientName) { myClientName = aClientName; }
	inline const std::string& GetClientName() { return myClientName; }
private:

	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	std::string myClientName;
};


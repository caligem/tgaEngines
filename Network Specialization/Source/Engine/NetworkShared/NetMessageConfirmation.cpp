#include "stdafx.h"
#include "NetMessageConfirmation.h"


CNetMessageConfirmation::CNetMessageConfirmation()
{
}


CNetMessageConfirmation::~CNetMessageConfirmation()
{
}

void CNetMessageConfirmation::DoSerialize(StreamType& aStreamType)
{
	SERIALIZE(aStreamType, myConfirmationID);
}

void CNetMessageConfirmation::DoDeSerialize(StreamType& aStreamType)
{
	DESERIALIZE(aStreamType, myConfirmationID);
}

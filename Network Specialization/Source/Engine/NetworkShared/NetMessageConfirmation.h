#pragma once
#include "NetMessage.h"

NETMESSAGE_DECLARE(CNetMessageConfirmation, Unencrypted, Unreliable)
public:
	CNetMessageConfirmation();
	~CNetMessageConfirmation();

	inline void SetConfirmationID(unsigned int aConfirmationID) { myConfirmationID = aConfirmationID; }
	inline const unsigned int GetConfirmationID() const { return myConfirmationID; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	unsigned int myConfirmationID;
};


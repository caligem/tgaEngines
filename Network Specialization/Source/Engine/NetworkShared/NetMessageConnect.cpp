#include "stdafx.h"
#include "NetMessageConnect.h"

CNetMessageConnect::CNetMessageConnect()
{
}


CNetMessageConnect::~CNetMessageConnect()
{
}

void CNetMessageConnect::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myState);
	if (myState == EConnectionState_Ok)
	{
		SERIALIZE(aStreamType, myStartTime);
	}
}

void CNetMessageConnect::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myState);
	if (myState == EConnectionState_Ok)
	{
		DESERIALIZE(aStreamType, myStartTime);
	}
}

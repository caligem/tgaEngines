#pragma once
#include "NetMessage.h"

NETMESSAGE_DECLARE(CNetMessageConnect, Unencrypted, Unreliable)
public:
	enum EConnectionState
	{
		EConnectionState_Ok,
		EConnectionState_Full,
		EConnectionState_TryingToConnect,
		EConnectionState_Disconnect
	};

	CNetMessageConnect();
	~CNetMessageConnect();

	inline void SetConnectionState(EConnectionState aState) { myState = aState; }
	inline EConnectionState GetConnectionState() { return myState; }
	double GetStartTime() const { return myStartTime; }
	void SetStartTime(double aStartTime) { myStartTime = aStartTime; }
private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	double myStartTime;
	EConnectionState myState;
};


#include "stdafx.h"
#include "NetMessageCreateHealthPack.h"


CNetMessageCreateHealthPack::CNetMessageCreateHealthPack()
{
}


CNetMessageCreateHealthPack::~CNetMessageCreateHealthPack()
{
}

void CNetMessageCreateHealthPack::AddHealthPack(unsigned short aID, const CommonUtilities::Vector2f & aPosition, bool aIsTaken)
{
	myHealthPacks.emplace_back();
	SHealthPackData& healthPack = myHealthPacks.back();
	healthPack.id = aID;
	healthPack.position = aPosition;
	healthPack.isTaken = aIsTaken;
	myNumberOfHealthPacks++;
}

void CNetMessageCreateHealthPack::DoSerialize(StreamType& aStreamType)
{
	SERIALIZE(aStreamType, myNumberOfHealthPacks);
	for (auto& healthPack : myHealthPacks)
	{
		SERIALIZE(aStreamType, healthPack.id);
		SERIALIZE(aStreamType, healthPack.position);
		SERIALIZE(aStreamType, healthPack.isTaken);
	}
}

void CNetMessageCreateHealthPack::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myNumberOfHealthPacks);
	for (char i = 0; i < myNumberOfHealthPacks; ++i)
	{
		myHealthPacks.emplace_back();
		SHealthPackData& healthPack = myHealthPacks.back();
		DESERIALIZE(aStreamType, healthPack.id);
		DESERIALIZE(aStreamType, healthPack.position);
		DESERIALIZE(aStreamType, healthPack.isTaken);
	}
}

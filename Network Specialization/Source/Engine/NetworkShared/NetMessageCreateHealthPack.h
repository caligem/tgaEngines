#pragma once
#include "NetMessage.h"
#include "Vector.h"

NETMESSAGE_DECLARE(CNetMessageCreateHealthPack, Unencrypted, Reliable)
public:
	struct SHealthPackData
	{
		unsigned short id;
		CommonUtilities::Vector2f position;
		bool isTaken;
	};

	CNetMessageCreateHealthPack();
	~CNetMessageCreateHealthPack();
	
	void AddHealthPack(unsigned short aID, const CommonUtilities::Vector2f& aPosition, bool aIsTaken);
	inline const std::vector<SHealthPackData>& GetHealthPacks() const { return myHealthPacks; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	inline virtual void ClearMessage() { CNetMessage::ClearMessage(); myHealthPacks.clear(); myNumberOfHealthPacks = 0; }

	char myNumberOfHealthPacks;
	std::vector<SHealthPackData> myHealthPacks;
};
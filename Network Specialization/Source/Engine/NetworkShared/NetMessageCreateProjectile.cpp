#include "stdafx.h"
#include "NetMessageCreateProjectile.h"


CNetMessageCreateProjectile::CNetMessageCreateProjectile()
{
}


CNetMessageCreateProjectile::~CNetMessageCreateProjectile()
{
}

void CNetMessageCreateProjectile::DoSerialize(StreamType& aStreamType)
{
	SERIALIZE(aStreamType, myShooterID);
	SERIALIZE(aStreamType, myDirection);
}

void CNetMessageCreateProjectile::DoDeSerialize(StreamType& aStreamType)
{
	DESERIALIZE(aStreamType, myShooterID);
	DESERIALIZE(aStreamType, myDirection);
}
#pragma once
#include "NetMessage.h"
#include "Vector.h"

NETMESSAGE_DECLARE(CNetMessageCreateProjectile, Unencrypted, Reliable)
public:
	CNetMessageCreateProjectile();
	~CNetMessageCreateProjectile();

	inline const unsigned short GetShooterID() const { return myShooterID; }
	inline void SetShooterID(unsigned short val) { myShooterID = val; }

	CommonUtilities::Vector2f GetDirection() const { return myDirection; }
	void SetDirection(CommonUtilities::Vector2f val) { myDirection = val; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	unsigned short myShooterID;
	CommonUtilities::Vector2f myDirection;
};


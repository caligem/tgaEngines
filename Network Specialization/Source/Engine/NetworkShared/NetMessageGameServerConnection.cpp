#include "stdafx.h"
#include "NetMessageGameServerConnection.h"


CNetMessageGameServerConnection::CNetMessageGameServerConnection()
{
}


CNetMessageGameServerConnection::~CNetMessageGameServerConnection()
{
}


void CNetMessageGameServerConnection::DoSerialize(StreamType& aStreamType)
{
	SERIALIZE(aStreamType, myPlayerConnectionState);
}

void CNetMessageGameServerConnection::DoDeSerialize(StreamType& aStreamType)
{
	DESERIALIZE(aStreamType, myPlayerConnectionState);
}
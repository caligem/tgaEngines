#pragma once
#include "NetMessage.h"

NETMESSAGE_DECLARE(CNetMessageGameServerConnection, Unencrypted, Reliable)
public:
	enum EPlayerConnectState
	{
		Join,
		Leave
	};
	CNetMessageGameServerConnection();
	~CNetMessageGameServerConnection();


	inline EPlayerConnectState GetPlayerConnectionState() const { return myPlayerConnectionState; }
	inline void SetPlayerConnectionState(CNetMessageGameServerConnection::EPlayerConnectState aPlayerConnectionState) { myPlayerConnectionState = aPlayerConnectionState; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	EPlayerConnectState myPlayerConnectionState;
};


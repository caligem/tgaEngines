#include "stdafx.h"
#include "NetMessageGameTimer.h"


CNetMessageGameTimer::CNetMessageGameTimer()
{
}


CNetMessageGameTimer::~CNetMessageGameTimer()
{
}

void CNetMessageGameTimer::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myTimer);
}

void CNetMessageGameTimer::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myTimer);
}

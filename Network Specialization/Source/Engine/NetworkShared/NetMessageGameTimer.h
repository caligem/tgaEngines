#pragma once
#include "NetMessage.h"

NETMESSAGE_DECLARE(CNetMessageGameTimer, Unencrypted, Unreliable)
public:
	CNetMessageGameTimer();
	~CNetMessageGameTimer();

	void SetTimer(float aTimer) { myTimer = aTimer; }
	const float GetTimer() const { return myTimer; }

private:
	void DoSerialize(StreamType& aStreamType) override;
	void DoDeSerialize(StreamType& aStreamType) override;

	float myTimer;
};


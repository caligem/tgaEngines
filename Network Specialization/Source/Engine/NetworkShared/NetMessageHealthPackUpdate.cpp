#include "stdafx.h"
#include "NetMessageHealthPackUpdate.h"


CNetMessageHealthPackUpdate::CNetMessageHealthPackUpdate()
{
}


CNetMessageHealthPackUpdate::~CNetMessageHealthPackUpdate()
{
}

void CNetMessageHealthPackUpdate::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myHealthpackState);
	SERIALIZE(aStreamType, myHealthpackID);
}

void CNetMessageHealthPackUpdate::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myHealthpackState);
	DESERIALIZE(aStreamType, myHealthpackID);
}

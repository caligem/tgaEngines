#pragma once
#include "NetMessage.h"

NETMESSAGE_DECLARE(CNetMessageHealthPackUpdate, Unencrypted, Reliable)
public:
	enum class EHealthpackState
	{
		Taken,
		Respawned
	};

	CNetMessageHealthPackUpdate();
	~CNetMessageHealthPackUpdate();

	inline const unsigned short GetHealthpackID() const { return myHealthpackID; }
	inline void SetHealthpackID(unsigned short val) { myHealthpackID = val; }

	inline const CNetMessageHealthPackUpdate::EHealthpackState GetHealthpackState() const { return myHealthpackState; }
	inline void SetHealthpackState(CNetMessageHealthPackUpdate::EHealthpackState val) { myHealthpackState = val; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	EHealthpackState myHealthpackState;
	unsigned short myHealthpackID;
};


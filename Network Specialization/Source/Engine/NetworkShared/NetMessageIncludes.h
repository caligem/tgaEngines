#pragma once

#include "NetMessageChatMessage.h"
#include "NetMessageClientName.h"
#include "NetMessageConnect.h"
#include "NetMessagePing.h"
#include "NetMessageLuaCommand.h"
#include "NetMessageConfirmation.h"
#include "NetMessageGameServerConnection.h"

#include "NetMessagePlayerStatus.h"
#include "NetMessagePlayerInput.h"
#include "NetMessagePlayerSnapshot.h"
#include "NetmessagePlayerShoot.h"
#include "NetMessagePlayerHealthStatus.h"
#include "NetMessagePlayerFrag.h"

#include "NetMessageCreateHealthPack.h"
#include "NetMessageHealthPackUpdate.h"

#include "NetMessageCreateProjectile.h"
#include "NetMessagePredictedProjectile.h"

#include "NetMessageGameTimer.h"
#include "NetMessageSendStats.h"
#include "NetMessageResetGame.h"
#include "NetMessageShowWinscreen.h"
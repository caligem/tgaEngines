#include "stdafx.h"
#include "NetMessageLuaCommand.h"


CNetMessageLuaCommand::CNetMessageLuaCommand()
{
}


CNetMessageLuaCommand::~CNetMessageLuaCommand()
{
}

void CNetMessageLuaCommand::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myCommand);
}

void CNetMessageLuaCommand::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myCommand);
}

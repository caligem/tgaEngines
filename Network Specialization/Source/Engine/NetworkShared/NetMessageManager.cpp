#include "stdafx.h"
#include "NetMessageManager.h"
#include "NetMessageIncludes.h"
#include "CommonNetworkIncludes.h"
#include "Encryption.h"

unsigned int CNetMessageManager::myStaticID = 0;

CNetMessageManager::CNetMessageManager(CommonUtilities::Timer& aTimer)
	: myTimer(aTimer)
{
	myInbound = 0;
	myOutbound = 0;
	myStaticID = 0;
	myReliablePackagesSent = 0;
	myReliablePackagesDropped = 0;
}


CNetMessageManager::~CNetMessageManager()
{
}

bool CNetMessageManager::Init(char aThreadCount)
{
	WSADATA wsaData;

	if (WSAStartup(MAKEWORD(2, 1), &wsaData))
	{
		return false;
	}

	char safeThreadCount = aThreadCount;
	if (aThreadCount < 1)
	{
		safeThreadCount = 1;
	}
	
	for (char i = 0; i < safeThreadCount; ++i)
	{
		myOutMessageLUT.emplace_back();
	}
	
	RegisterMessages();

	myKey.push_back(1);
	myKey.push_back(3);
	myKey.push_back(3);
	myKey.push_back(7);

	
	RegisterDoUnPackWorkCallback<CNetMessageConfirmation>(std::bind(&CNetMessageManager::HandleConfirmation, this, std::placeholders::_1, std::placeholders::_2));

	return true;
}

void CNetMessageManager::AddReliableMessageSystem(unsigned short aClientID)
{
	SReliableMessageSystem system;
	myReliableMessageSystems[aClientID] = system;
}

void CNetMessageManager::RemoveReliableMessageSystem(unsigned short aClientID)
{
	auto objPos = myReliableMessageSystems.find(aClientID);
	if (objPos != myReliableMessageSystems.end())
	{
		myReliableMessageSystems.erase(objPos);
	}
}

void CNetMessageManager::UpdateReliableMessages()
{
	for (auto reliableMessageSystemIt = myReliableMessageSystems.begin(); reliableMessageSystemIt != myReliableMessageSystems.end(); ++reliableMessageSystemIt)
	{
		std::unordered_map<int, SReliableMessageData>& reliableMessages = reliableMessageSystemIt->second.myReliableMessages;

		for (auto it = reliableMessages.begin(); it != reliableMessages.end();)
		{
			SReliableMessageData& message = it->second;
			message.timer += myTimer.GetDeltaTimeInMS();
			if (message.timer >= myReliableMessagesMaxTimerInMS)
			{
				if (message.tries >= myReliableMessagesMaxTries)
				{
					//TODO: drop connection
					reliableMessages.erase(it);
#ifdef DEBUG_RELIABLEMESSAGES
					printf("Erased ReliableMessage in Queue due to to many tries \n");
#endif
					break;
				}
				ReSendMessage(message.stream, message.address);
#ifdef DEBUG_RELIABLEMESSAGES
				printf("Resend Reliable Messages try: %d \n", message.tries);
#endif
				message.timer = 0.f;
				message.tries++;
			}
			it++;
		}
	}
}

void CNetMessageManager::PackAndSendMessage(CNetMessage* aMessage, const CAddressWrapper& aDestination)
{
	aMessage->PackMessage();

	char* stream;
	size_t size;

	if (aMessage->IsEncrypted())
	{
		myEncryptedMessage.clear();
		Aes256::encrypt(myKey, (unsigned char*)&aMessage->myStream[4], aMessage->myStream.size() - 4, myEncryptedMessage);
		myEncryptedMessage.insert(myEncryptedMessage.begin(), aMessage->myStream.begin(), aMessage->myStream.begin() + 4);
		stream = (char*)&myEncryptedMessage[0];
		size = myEncryptedMessage.size();
	}
	else
	{
		stream = &aMessage->myStream[0];
		size = aMessage->myStream.size();
	}

	if (aMessage->IsReliable())
	{
		SReliableMessageData data;
		data.address = aDestination;
		data.stream.resize(size);
		memcpy_s(&data.stream[0], size, stream, size);

		myReliableMessageSystems[aMessage->GetTargetID()].myReliableMessages[aMessage->myMessageID] = data;
		myReliablePackagesSent++;
		myReliablePackagesDropped++;
	}

	myOutbound += size;

	mySocket.Send(aDestination, stream, size);

	aMessage->myStream.clear();
}

void CNetMessageManager::RecieveMessage()
{
	CAddressWrapper senderAddress;
	int receivedBytes;

	while ((receivedBytes = mySocket.Receive(senderAddress, myBuffer, sizeof(myBuffer))) > 0)
	{
		HandleMessage(&myBuffer[0], receivedBytes, senderAddress);
		ZeroMemory(myBuffer, sizeof(myBuffer));
	}
}

bool CNetMessageManager::CloseSocket()
{
	if (!mySocket.IsOpen())
	{
		return false;
	}

	mySocket.Close();
	return true;
}


bool CNetMessageManager::OpenSocket(unsigned short aPort, const char* aAddress)
{
	if (mySocket.IsOpen())
	{
		return false;
	}

	return mySocket.Open(aPort, aAddress);
}

bool CNetMessageManager::IsSocketOpen()
{
	return mySocket.IsOpen();
}

size_t CNetMessageManager::GetInbound()
{
	size_t output = myInbound;
	myInbound = 0;
	return output;
}

size_t CNetMessageManager::GetOutbound()
{
	size_t output = myOutbound;
	myOutbound = 0;
	return output;
}

float CNetMessageManager::GetPackageLoss()
{
	if (myReliablePackagesSent != 0)
	{
		if (myReliablePackagesDropped < 0)
		{
			myReliablePackagesDropped = 0;
		}
		float packetLossRate = static_cast<float>(myReliablePackagesDropped) / static_cast<float>(myReliablePackagesSent) * 100.f;
		myReliablePackagesDropped = 0;
		myReliablePackagesSent = 0;
		return packetLossRate;
	}
	return 0;
}

void CNetMessageManager::HandleMessage(char * aBuffer, int aBufferSize, CAddressWrapper & aSender)
{
	if (aBufferSize < 4)
	{
		printf("Message Corrupt");
		return;
	}

	myInbound += aBufferSize;

	char* dataBuffer = aBuffer;
	int bufferSize = aBufferSize;
	unsigned int hashedMessageID = 0;

	myIDBuffer.resize(sizeof(unsigned int));
	memcpy_s(&myIDBuffer[0], sizeof(unsigned int), dataBuffer, sizeof(unsigned int));
	DESERIALIZE(myIDBuffer, hashedMessageID);

	CNetMessage* message = myInMessageLUT[hashedMessageID];

	if (message)
	{
		if (message->IsEncrypted())
		{
			myDecryptedMessage.clear();
			bufferSize = static_cast<int>(Aes256::decrypt(myKey, (unsigned char*)&dataBuffer[4], bufferSize-4, myDecryptedMessage)) + 4;

			myDecryptedMessage.insert(myDecryptedMessage.begin(), dataBuffer, dataBuffer+4);
			dataBuffer = (char*)&myDecryptedMessage[0];
		}

		bool hasProccessedMessage = false;

		message->UnPackMessage(dataBuffer, bufferSize);

		if (message->IsReliable())
		{
			hasProccessedMessage = HasProccessedMessage(message->myMessageID, message->GetSenderID());

			CNetMessageConfirmation* confirmation = CreateMessage<CNetMessageConfirmation>(message->GetSenderID());
			confirmation->SetConfirmationID(message->myMessageID);
			PackAndSendMessage(confirmation, aSender);
		}

		if (!hasProccessedMessage)
		{	
			message->DoUnPackWork(message, aSender);
		}

		message->ClearMessage();
	}
}

void CNetMessageManager::HandleConfirmation(CNetMessage * aMessage, CAddressWrapper &)
{
	
	CNetMessageConfirmation* confirmation = static_cast<CNetMessageConfirmation*>(aMessage);
	
	std::unordered_map<int, SReliableMessageData>& reliableMessages = myReliableMessageSystems[aMessage->GetSenderID()].myReliableMessages;
	
	auto objPos = reliableMessages.find(confirmation->GetConfirmationID());
	if (objPos != reliableMessages.end())
	{
		myReliablePackagesDropped--;
#ifdef DEBUG_RELIABLEMESSAGES
		printf("Message Confirmation Received message id: %d \n", confirmation->GetConfirmationID());
#endif
		reliableMessages.erase(objPos);
	}
}

void CNetMessageManager::ReSendMessage(std::vector<char>& aStream, CAddressWrapper& aDestination)
{
	myReliablePackagesDropped++;
	myReliablePackagesSent++;

	myOutbound += aStream.size();
	mySocket.Send(aDestination, &aStream[0], aStream.size());
}

uint32_t CNetMessageManager::HashMessageID(const uint8_t* key, size_t length)
{
	size_t i = 0;
	uint32_t hash = 0;
	while (i != length) {
		hash += key[i++];
		hash += hash << 10;
		hash ^= hash >> 6;
	}
	hash += hash << 3;
	hash ^= hash >> 11;
	hash += hash << 15;
	return hash;
}

bool CNetMessageManager::HasProccessedMessage(unsigned int aMessageID, unsigned short aSenderID)
{
	std::deque<unsigned int>& proccessedMessages = myReliableMessageSystems[aSenderID].myProccessedMessages;
	const auto& it = std::find(proccessedMessages.begin(), proccessedMessages.end(), aMessageID);
	if (it != proccessedMessages.end())
	{
		return true;
	}
	proccessedMessages.push_back(aMessageID);
	if (proccessedMessages.size() >= myMaxProccessedMessages)
	{
		proccessedMessages.pop_front();
	}
	return false;
}

void CNetMessageManager::RegisterMessages()
{
	RegisterMessage<CNetMessageChatMessage>();
	RegisterMessage<CNetMessageClientName>();
	RegisterMessage<CNetMessageConnect>();
	RegisterMessage<CNetMessagePing>();
	RegisterMessage<CNetMessageLuaCommand>();
	RegisterMessage<CNetMessageConfirmation>();
	RegisterMessage<CNetMessageGameServerConnection>();

	RegisterMessage<CNetMessagePlayerStatus>();
	RegisterMessage<CNetMessagePlayerInput>();
	RegisterMessage<CNetMessagePlayerShoot>();
	RegisterMessage<CNetMessagePlayerSnapshot>();
	RegisterMessage<CNetMessagePlayerHealthStatus>();
	RegisterMessage<CNetMessagePlayerFrag>();

	RegisterMessage<CNetMessageCreateHealthPack>();
	RegisterMessage<CNetMessageHealthPackUpdate>();

	RegisterMessage<CNetMessageCreateProjectile>();
	RegisterMessage<CNetMessagePredictedProjectile>();

	RegisterMessage<CNetMessageGameTimer>();
	RegisterMessage<CNetMessageSendStats>();
	RegisterMessage<CNetMessageResetGame>();
	RegisterMessage<CNetMessageShowWinscreen>();
}

#pragma once
#include "NetMessage.h"
#include "Timer.h"
#include <unordered_map>
#include "AddressWrapper.h"
#include "SocketWrapper.h"
#include <functional>
#include <deque>
#include <mutex>

class CNetMessageManager
{
public:
	struct SReliableMessageData
	{
		std::vector<char> stream;
		CAddressWrapper address;
		float timer = 0.f;
		char tries = 0;
	};

	struct SReliableMessageSystem
	{
		std::unordered_map<int, SReliableMessageData> myReliableMessages;
		std::deque<unsigned int> myProccessedMessages;
	};

	CNetMessageManager(CommonUtilities::Timer& aTimer);
	~CNetMessageManager();

	bool Init(char aThreadCount = 1);

	void UpdateReliableMessages();
	void AddReliableMessageSystem(unsigned short aClientID);
	void RemoveReliableMessageSystem(unsigned short aClientID);

	template<typename MessageType>
	void RegisterDoUnPackWorkCallback(std::function<void(CNetMessage*, CAddressWrapper&)> aFunction);

	template<typename MessageType>
	MessageType* CreateMessage(unsigned short aTargetID = 512);
	
	void PackAndSendMessage(CNetMessage* aMessage, const CAddressWrapper& aDestination);

	void SetSenderID(unsigned short aSenderID) { mySenderID = aSenderID; }
	const unsigned short GetSenderID() const { return mySenderID; }
	void RecieveMessage();

	bool CloseSocket();
	bool OpenSocket(unsigned short aPort, const char* aAddress = nullptr);
	bool IsSocketOpen();
	
	size_t GetInbound();
	size_t GetOutbound();

	float GetPackageLoss();

private:
	template<typename MessageType>
	void RegisterMessage();
	void RegisterMessages();

	void HandleMessage(char* aBuffer, int aBufferSize, CAddressWrapper& aSender);
	void HandleConfirmation(CNetMessage* aMessage, CAddressWrapper& aSender);
	void ReSendMessage(std::vector<char>& aStream, CAddressWrapper& aDestination);

	uint32_t HashMessageID(const uint8_t* key, size_t length);
	bool HasProccessedMessage(unsigned int myMessageID, unsigned short aSenderID);
	CSocketWrapper mySocket;
	char myBuffer[512];

	CommonUtilities::Timer& myTimer;
	std::vector<unsigned char> myKey;
	std::vector<unsigned char> myEncryptedMessage;
	std::vector<unsigned char> myDecryptedMessage;

	std::vector<std::unordered_map<unsigned int, CNetMessage*>> myOutMessageLUT;
	std::unordered_map<unsigned int, CNetMessage*> myInMessageLUT;

	std::unordered_map<unsigned short, SReliableMessageSystem> myReliableMessageSystems;

	std::vector<char> myIDBuffer;

	static unsigned int myStaticID;
	static constexpr float myReliableMessagesMaxTimerInMS = 200.f;
	static constexpr char myReliableMessagesMaxTries = 5;
	static constexpr unsigned short myMaxProccessedMessages = 1024;

	size_t myReliablePackagesSent;
	size_t myReliablePackagesDropped;
	float myPackageLossTimer = 0.f;

	size_t myInbound;
	size_t myOutbound;

	unsigned short mySenderID;
};

template<typename MessageType>
inline void CNetMessageManager::RegisterDoUnPackWorkCallback(std::function<void(CNetMessage*, CAddressWrapper&)> aFunction)
{
	MessageType temp;
	std::string messageID = temp.ClassID();
	unsigned int hashedMessageID = HashMessageID((uint8_t*)messageID.c_str(), messageID.size());

	MessageType* message = static_cast<MessageType*>(myInMessageLUT[hashedMessageID]);
	message->SetUnPackWorkCallback(aFunction);
}

template<typename MessageType>
inline MessageType* CNetMessageManager::CreateMessage(unsigned short aTargetID)
{
	static int ourSharedMessageLUTIndex = 0;
	thread_local int ourMessageLUTIndex = ourSharedMessageLUTIndex++;

	MessageType temp;
	std::string messageID = temp.ClassID();
	unsigned int hashedMessageID = HashMessageID((uint8_t*)messageID.c_str(), messageID.size());

	CNetMessage* message = myOutMessageLUT[ourMessageLUTIndex][hashedMessageID];
	message->ClearMessage();

	unsigned int timestamp = static_cast<unsigned int>(myTimer.GetServerTimerInMS());
	message->SetGeneralData(hashedMessageID, myStaticID, timestamp, mySenderID, aTargetID);

	myStaticID++;
	return static_cast<MessageType*>(message);
}

template<typename MessageType>
inline void CNetMessageManager::RegisterMessage()
{
	for (auto& lut : myOutMessageLUT)
	{
		MessageType* message = new MessageType();
		std::string messageID = message->ClassID();
		message->BindFlags();
		unsigned int hashedMessageID = HashMessageID((uint8_t*)messageID.c_str(), messageID.size());

		lut[hashedMessageID] = message;
	}

	MessageType* message = new MessageType();
	std::string messageID = message->ClassID();
	message->BindFlags();
	unsigned int hashedMessageID = HashMessageID((uint8_t*)messageID.c_str(), messageID.size());

	myInMessageLUT[hashedMessageID] = message;
}

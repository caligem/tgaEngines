#include "stdafx.h"
#include "NetMessagePing.h"

CNetMessagePing::CNetMessagePing()
{
}


CNetMessagePing::~CNetMessagePing()
{
}

void CNetMessagePing::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myPingState);
	SERIALIZE(aStreamType, myPingTimestamp);
}

void CNetMessagePing::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myPingState);
	DESERIALIZE(aStreamType, myPingTimestamp);
}

#pragma once
#include "NetMessage.h"

NETMESSAGE_DECLARE(CNetMessagePing, Unencrypted, Unreliable)
public:
	enum EPingState
	{
		EPingState_Ping,
		EPingState_Pong
	};

	CNetMessagePing();
	~CNetMessagePing();

	void SetPingState(EPingState aPingState) { myPingState = aPingState; }
	const EPingState GetPingState() const { return myPingState; }

	void SetPingTimestamp(unsigned int aPingTimestamp) { myPingTimestamp = aPingTimestamp; }
	const unsigned int GetPingTimestamp() const { return myPingTimestamp; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	EPingState myPingState;
	unsigned int myPingTimestamp;
};


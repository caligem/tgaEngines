#include "stdafx.h"
#include "NetMessagePlayerFrag.h"


CNetMessagePlayerFrag::CNetMessagePlayerFrag()
{
}


CNetMessagePlayerFrag::~CNetMessagePlayerFrag()
{
}

void CNetMessagePlayerFrag::DoSerialize(StreamType& aStreamType)
{
	SERIALIZE(aStreamType, myKillerID);
	SERIALIZE(aStreamType, myVictimID);
}

void CNetMessagePlayerFrag::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myKillerID);
	DESERIALIZE(aStreamType, myVictimID);
}

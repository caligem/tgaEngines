#pragma once
#include "NetMessage.h"

NETMESSAGE_DECLARE(CNetMessagePlayerFrag, Unencrypted, Reliable)
public:
	CNetMessagePlayerFrag();
	~CNetMessagePlayerFrag();

	void SetKillerID(const unsigned short aKillerID) { myKillerID = aKillerID; }
	void SetVictimID(const unsigned short aVictimID) { myVictimID = aVictimID; }

	const unsigned short GetKillerID() const { return myKillerID; }
	const unsigned short GetVictimID() const { return myVictimID; }

private:
	void DoSerialize(StreamType& aStreamType) override;
	void DoDeSerialize(StreamType& aStreamType) override;

	unsigned short myKillerID;
	unsigned short myVictimID;
};


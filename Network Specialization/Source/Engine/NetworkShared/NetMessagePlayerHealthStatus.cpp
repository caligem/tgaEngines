#include "stdafx.h"
#include "NetMessagePlayerHealthStatus.h"


CNetMessagePlayerHealthStatus::CNetMessagePlayerHealthStatus()
{
}


CNetMessagePlayerHealthStatus::~CNetMessagePlayerHealthStatus()
{
}

void CNetMessagePlayerHealthStatus::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myHealthStatus);
	SERIALIZE(aStreamType, myHealthValue);
	SERIALIZE(aStreamType, myPlayerID);
	if (myHealthStatus == EHealthStatus::Respawn)
	{
		SERIALIZE(aStreamType, myRespawnPosition);
	}
}

void CNetMessagePlayerHealthStatus::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myHealthStatus);
	DESERIALIZE(aStreamType, myHealthValue);
	DESERIALIZE(aStreamType, myPlayerID);
	if (myHealthStatus == EHealthStatus::Respawn)
	{
		DESERIALIZE(aStreamType, myRespawnPosition);
	}
}

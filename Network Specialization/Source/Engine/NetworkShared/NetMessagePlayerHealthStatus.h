#pragma once
#include "NetMessage.h"
#include "Vector.h"

NETMESSAGE_DECLARE(CNetMessagePlayerHealthStatus, Unencrypted, Reliable)
public:
	enum class EHealthStatus
	{
		Heal,
		TakeDamage,
		Respawn
	};

	CNetMessagePlayerHealthStatus();
	~CNetMessagePlayerHealthStatus();

	inline CNetMessagePlayerHealthStatus::EHealthStatus GetHealthStatus() const { return myHealthStatus; }
	inline void SetHealthStatus(CNetMessagePlayerHealthStatus::EHealthStatus aHealthStatus) { myHealthStatus = aHealthStatus; }

	inline char GetHealthValue() const { return myHealthValue; }
	inline void SetHealthValue(char aHealthValue) { myHealthValue = aHealthValue; }

	inline unsigned short GetPlayerID() const { return myPlayerID; }
	inline void SetPlayerID(unsigned short val) { myPlayerID = val; }

	inline void SetRespawnPosition(const CommonUtilities::Vector2f& aPosition) { myRespawnPosition = aPosition; }
	inline const CommonUtilities::Vector2f& GetRespawnPosition() const { return myRespawnPosition; }

private:
	void DoSerialize(StreamType& aStreamType) override;
	void DoDeSerialize(StreamType& aStreamType) override;

	EHealthStatus myHealthStatus;
	char myHealthValue;
	unsigned short myPlayerID;
	CommonUtilities::Vector2f myRespawnPosition;
};


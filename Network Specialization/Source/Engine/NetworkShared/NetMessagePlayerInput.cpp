#include "stdafx.h"
#include "NetMessagePlayerInput.h"


CNetMessagePlayerInput::CNetMessagePlayerInput()
{
}


CNetMessagePlayerInput::~CNetMessagePlayerInput()
{
}

void CNetMessagePlayerInput::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myPlayerInput);
}

void CNetMessagePlayerInput::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myPlayerInput);
}

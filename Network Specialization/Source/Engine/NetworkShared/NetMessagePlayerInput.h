#pragma once
#include "NetMessage.h"

NETMESSAGE_DECLARE(CNetMessagePlayerInput, Unencrypted, Reliable)
public:

	enum EPlayerInput:char
	{
		LeftDown,
		RightDown,
		LeftUp,
		RightUp,
		Jump
	};

	CNetMessagePlayerInput();
	~CNetMessagePlayerInput();

	CNetMessagePlayerInput::EPlayerInput GetPlayerInput() const { return myPlayerInput; }
	void SetPlayerInput(CNetMessagePlayerInput::EPlayerInput aInput) { myPlayerInput = aInput; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	EPlayerInput myPlayerInput;
};


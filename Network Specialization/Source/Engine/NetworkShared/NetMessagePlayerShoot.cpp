#include "stdafx.h"
#include "NetMessagePlayerShoot.h"


CNetMessagePlayerShoot::CNetMessagePlayerShoot()
{
}


CNetMessagePlayerShoot::~CNetMessagePlayerShoot()
{
}

void CNetMessagePlayerShoot::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myDirection);
	SERIALIZE(aStreamType, myProjectileID);
}

void CNetMessagePlayerShoot::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myDirection);
	DESERIALIZE(aStreamType, myProjectileID);
}

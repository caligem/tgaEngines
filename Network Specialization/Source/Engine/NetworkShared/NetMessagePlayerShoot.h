#pragma once
#include "NetMessage.h"
#include "Vector.h"

NETMESSAGE_DECLARE(CNetMessagePlayerShoot, Unencrypted, Reliable)
public:
	CNetMessagePlayerShoot();
	~CNetMessagePlayerShoot();

	void SetDirection(const CommonUtilities::Vector2f& aDirection) { myDirection = aDirection; }
	const CommonUtilities::Vector2f& GetDirection() const { return myDirection; }

	inline void SetProjectileID(unsigned short aID) { myProjectileID = aID; }
	inline unsigned short GetProjectileID() const { return myProjectileID; }

private:
	void DoSerialize(StreamType& aStreamType) override;
	void DoDeSerialize(StreamType& aStreamType) override;

	unsigned short myProjectileID;
	CommonUtilities::Vector2f myDirection;
};


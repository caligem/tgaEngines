#include "stdafx.h"
#include "NetMessagePlayerSnapshot.h"


CNetMessagePlayerSnapshot::CNetMessagePlayerSnapshot()
{
}


CNetMessagePlayerSnapshot::~CNetMessagePlayerSnapshot()
{
}

void CNetMessagePlayerSnapshot::AddPlayerPosition(const CommonUtilities::Vector2f& aPosition, unsigned short aPlayerID)
{
	myPlayerData.push_back({ aPosition, aPlayerID });
	myNumberOfPlayers++;
}

void CNetMessagePlayerSnapshot::DoSerialize(StreamType& aStreamType)
{
	SERIALIZE(aStreamType, myNumberOfPlayers);

	for (auto& playerData : myPlayerData)
	{
		SERIALIZE(aStreamType, playerData.playerPosition);
		SERIALIZE(aStreamType, playerData.playerID);
	}
}

void CNetMessagePlayerSnapshot::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myNumberOfPlayers);

	for (char i = 0; i < myNumberOfPlayers; ++i)
	{
		myPlayerData.emplace_back();
		SPlayerData& data = myPlayerData.back();
		DESERIALIZE(aStreamType, data.playerPosition);
		DESERIALIZE(aStreamType, data.playerID);
	}
}

#pragma once
#include "NetMessage.h"
#include "Vector.h"

NETMESSAGE_DECLARE(CNetMessagePlayerSnapshot, Unencrypted, Unreliable)
public:
	struct SPlayerData
	{
		CommonUtilities::Vector2f playerPosition;
		unsigned short playerID;
	};
	CNetMessagePlayerSnapshot();
	~CNetMessagePlayerSnapshot();

	void AddPlayerPosition(const CommonUtilities::Vector2f& aPosition, unsigned short aPlayerID);
	inline const std::vector<SPlayerData>& GetPlayerData() { return myPlayerData; }

private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	inline virtual void ClearMessage() { CNetMessage::ClearMessage(); myPlayerData.clear(); myNumberOfPlayers = 0; }

	std::vector<SPlayerData> myPlayerData;
	char myNumberOfPlayers;
};


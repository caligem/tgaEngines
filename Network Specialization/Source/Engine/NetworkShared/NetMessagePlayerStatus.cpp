#include "stdafx.h"
#include "NetMessagePlayerStatus.h"


CNetMessagePlayerStatus::CNetMessagePlayerStatus()
{
}


CNetMessagePlayerStatus::~CNetMessagePlayerStatus()
{
}

void CNetMessagePlayerStatus::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myPlayerStatus);
	SERIALIZE(aStreamType, myPlayerID);
	SERIALIZE(aStreamType, myPlayerName);
	SERIALIZE(aStreamType, myPlayerHealth);
	if (myPlayerStatus != PlayerLeft)
	{
		SERIALIZE(aStreamType, myPosition);
	}
}

void CNetMessagePlayerStatus::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myPlayerStatus);
	DESERIALIZE(aStreamType, myPlayerID);
	DESERIALIZE(aStreamType, myPlayerName);
	DESERIALIZE(aStreamType, myPlayerHealth);
	if (myPlayerStatus != PlayerLeft)
	{
		DESERIALIZE(aStreamType, myPosition);
	}
}

#pragma once
#include "NetMessage.h"
#include "Vector.h"

NETMESSAGE_DECLARE(CNetMessagePlayerStatus, Unencrypted, Reliable)
public:
	enum EPlayerStatus
	{
		PlayerJoined,
		PlayerLeft,
		PlayerExists
	};
	CNetMessagePlayerStatus();
	~CNetMessagePlayerStatus();

	inline EPlayerStatus GetPlayerStatus() const { return myPlayerStatus; }
	inline void SetPlayerStatus(CNetMessagePlayerStatus::EPlayerStatus aPlayerStatus) { myPlayerStatus = aPlayerStatus; }

	inline unsigned short GetPlayerID() const { return myPlayerID; }
	inline void SetPlayerID(unsigned short aPlayerID) { myPlayerID = aPlayerID; }

	inline const std::string& GetPlayerName() const { return myPlayerName; }
	inline void SetPlayerName(const std::string& aPlayerName) { myPlayerName = aPlayerName; }

	inline const CommonUtilities::Vector2f& GetPosition() const { return myPosition; }
	inline void SetPosition(const CommonUtilities::Vector2f& aPosition) { myPosition = aPosition; }

	inline const char GetPlayerHealth() const { return myPlayerHealth; }
	inline void SetPlayerHealth(char aHealth) { myPlayerHealth = aHealth; }
private:
	virtual void DoSerialize(StreamType& aStreamType) override;
	virtual void DoDeSerialize(StreamType& aStreamType) override;

	EPlayerStatus myPlayerStatus;
	unsigned short myPlayerID;
	char myPlayerHealth;
	std::string myPlayerName;
	CommonUtilities::Vector2f myPosition;
};


#include "stdafx.h"
#include "NetMessagePredictedProjectile.h"


CNetMessagePredictedProjectile::CNetMessagePredictedProjectile()
{
}


CNetMessagePredictedProjectile::~CNetMessagePredictedProjectile()
{
}

void CNetMessagePredictedProjectile::DoSerialize(StreamType& aStreamType)
{
	SERIALIZE(aStreamType, myPredictedProjectileID);
}

void CNetMessagePredictedProjectile::DoDeSerialize(StreamType& aStreamType)
{
	DESERIALIZE(aStreamType, myPredictedProjectileID);
}
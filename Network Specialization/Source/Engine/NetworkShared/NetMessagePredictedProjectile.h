#pragma once
#include "NetMessage.h"
NETMESSAGE_DECLARE(CNetMessagePredictedProjectile, Unencrypted, Reliable)
public:
	CNetMessagePredictedProjectile();
	~CNetMessagePredictedProjectile();

	void SetPredictedProjectileID(unsigned short aPredictedProjectileID) { myPredictedProjectileID = aPredictedProjectileID; }
	const unsigned short GetPredictedProjectileID() const { return myPredictedProjectileID; }

private:
	void DoSerialize(StreamType& aStreamType) override;
	void DoDeSerialize(StreamType& aStreamType) override;

	unsigned short myPredictedProjectileID;
};


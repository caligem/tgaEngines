#include "stdafx.h"
#include "NetMessageResetGame.h"


CNetMessageResetGame::CNetMessageResetGame()
{
}


CNetMessageResetGame::~CNetMessageResetGame()
{
}

void CNetMessageResetGame::AddPlayerData(unsigned short aPlayerID, const CommonUtilities::Vector2f & aPlayerPosition)
{
	myResetData.emplace_back();
	SResetGameData& data = myResetData.back();
	data.playerID = aPlayerID;
	data.playerSpawnPosition = aPlayerPosition;
	myNumberOfData++;
}

void CNetMessageResetGame::DoSerialize(StreamType & aStreamtype)
{
	SERIALIZE(aStreamtype, myNumberOfData);
	for (auto& data : myResetData)
	{
		SERIALIZE(aStreamtype, data.playerID);
		SERIALIZE(aStreamtype, data.playerSpawnPosition);
	}
}

void CNetMessageResetGame::DoDeSerialize(StreamType & aStreamtype)
{
	DESERIALIZE(aStreamtype, myNumberOfData);
	for (char i = 0; i < myNumberOfData; ++i)
	{
		myResetData.emplace_back();
		SResetGameData& data = myResetData.back();
		DESERIALIZE(aStreamtype, data.playerID);
		DESERIALIZE(aStreamtype, data.playerSpawnPosition);
	}
}

void CNetMessageResetGame::ClearMessage()
{
	myStream.clear();
	myResetData.clear();
	myNumberOfData = 0;
}
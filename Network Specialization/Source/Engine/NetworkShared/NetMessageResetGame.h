#pragma once
#include "NetMessage.h"
#include "Vector.h"
#include <vector>

NETMESSAGE_DECLARE(CNetMessageResetGame, Unencrypted, Reliable)
	struct SResetGameData
	{
		unsigned short playerID;
		CommonUtilities::Vector2f playerSpawnPosition;
	};
public:
	CNetMessageResetGame();
	~CNetMessageResetGame();

	void AddPlayerData(unsigned short aPlayerID, const CommonUtilities::Vector2f& aPlayerPosition);
	const std::vector<SResetGameData>& GetResetData() const { return myResetData; }
private:
	void DoSerialize(StreamType& aStreamtype) override;
	void DoDeSerialize(StreamType& aStreamtype) override;

	void ClearMessage() override;

	std::vector<SResetGameData> myResetData;
	char myNumberOfData;
};


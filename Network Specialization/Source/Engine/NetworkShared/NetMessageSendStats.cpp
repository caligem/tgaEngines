#include "stdafx.h"
#include "NetMessageSendStats.h"


CNetMessageSendStats::CNetMessageSendStats()
{
}


CNetMessageSendStats::~CNetMessageSendStats()
{
}

void CNetMessageSendStats::AddPlayerStats(unsigned short aPlayerID, unsigned short aPlayerKills, unsigned short aPlayerDeaths)
{
	myStats.emplace_back();
	SStats& stats = myStats.back();

	stats.playerID = aPlayerID;
	stats.playerKills = aPlayerKills;
	stats.playerDeaths = aPlayerDeaths;
	myNumberOfStats++;
}

void CNetMessageSendStats::ClearMessage()
{
	myStream.clear();
	myStats.clear();
	myNumberOfStats = 0;
}


void CNetMessageSendStats::DoSerialize(StreamType & aStreamType)
{
	SERIALIZE(aStreamType, myNumberOfStats);
	for (auto& stats : myStats)
	{
		SERIALIZE(aStreamType, stats.playerID);
		SERIALIZE(aStreamType, stats.playerKills);
		SERIALIZE(aStreamType, stats.playerDeaths);
	}
}

void CNetMessageSendStats::DoDeSerialize(StreamType & aStreamType)
{
	DESERIALIZE(aStreamType, myNumberOfStats);
	for (char i = 0; i < myNumberOfStats; ++i)
	{
		myStats.emplace_back();
		SStats& stats = myStats.back();
		DESERIALIZE(aStreamType, stats.playerID);
		DESERIALIZE(aStreamType, stats.playerKills);
		DESERIALIZE(aStreamType, stats.playerDeaths);
	}
}

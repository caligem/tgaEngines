#pragma once
#include <vector>
#include "NetMessage.h"

NETMESSAGE_DECLARE(CNetMessageSendStats, Unencrypted, Reliable)
	struct SStats
	{
		unsigned short playerID;
		unsigned short playerKills;
		unsigned short playerDeaths;
	};


public:
	CNetMessageSendStats();
	~CNetMessageSendStats();


	void AddPlayerStats(unsigned short aPlayerID, unsigned short aPlayerKills, unsigned short aPlayerDeaths);
	std::vector<SStats>& GetPlayerStats() { return myStats; }

private:
	void DoSerialize(StreamType& aStreamType) override;
	void DoDeSerialize(StreamType& aStreamType) override;

	void ClearMessage() override;
	std::vector<SStats> myStats;
	char myNumberOfStats;
};


#include "stdafx.h"
#include "NetMessageShowWinscreen.h"


CNetMessageShowWinscreen::CNetMessageShowWinscreen()
{
}


CNetMessageShowWinscreen::~CNetMessageShowWinscreen()
{
}


void CNetMessageShowWinscreen::DoSerialize(StreamType& aStreamtype)
{
	SERIALIZE(aStreamtype, mySlowDownTime);
}

void CNetMessageShowWinscreen::DoDeSerialize(StreamType& aStreamtype)
{
	DESERIALIZE(aStreamtype, mySlowDownTime);
}
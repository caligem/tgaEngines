#pragma once
#include "NetMessage.h"

NETMESSAGE_DECLARE(CNetMessageShowWinscreen, Unencrypted, Unreliable)
public:
	CNetMessageShowWinscreen();
	~CNetMessageShowWinscreen();

	float GetSlowDownTime() const { return mySlowDownTime; }
	void SetSlowDownTime(float val) { mySlowDownTime = val; }

private:

	void DoSerialize(StreamType& aStreamtype);
	void DoDeSerialize(StreamType& aStreamtype);

	float mySlowDownTime;
};


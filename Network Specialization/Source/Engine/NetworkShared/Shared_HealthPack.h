#pragma once
#include "Vector.h"

class CShared_HealthPack
{
public:
	CShared_HealthPack();
	virtual ~CShared_HealthPack();

	void SetID(unsigned short aID) { myID = aID; }
	unsigned short GetID() { return myID; }

	const CommonUtilities::Vector2f& GetPosition() const { return myPosition; }
	const CommonUtilities::Vector2f& GetSize() const { return mySize; }

protected:
	float myMovementSpeed = 1.f;
	CommonUtilities::Vector2f myPosition;
	CommonUtilities::Vector2f mySize;
	unsigned short myID;
};


#include "stdafx.h"
#include "Shared_Player.h"
#include "Mathf.h"

CShared_Player::CShared_Player()
	: myMovement(0.f, 0.f)
	, myIsOnGround(false)
	, myLeftMovement(0.f)
	, myRightMovement(0.f)
{
	mySpeed = 5.f;
	myShootCooldown = 0.2f;
	myCollider.radius = 0.2f;
	myCollider.position = myPosition;
}

void CShared_Player::AddMovement(EMovement aMovement)
{
	if (aMovement == EMovement::Left)
	{
		myLeftMovement = 1.f;
	}
	else if (aMovement == EMovement::Right)
	{
		myRightMovement = 1.f;
	}
}

void CShared_Player::StopMovement(EMovement aMovement)
{
	if (aMovement == EMovement::Left)
	{
		myLeftMovement = 0.f;
	}
	else if (aMovement == EMovement::Right)
	{
		myRightMovement = 0.f;
	}
}

void CShared_Player::Jump()
{
	if (myIsOnGround)
	{
		myMovement.y = 7.0f;
		myIsOnGround = false;
	}
}
void CShared_Player::UpdateMovement(float aDeltaTime)
{
	myPosition += myMovement * aDeltaTime;
	myMovement.x = (-myLeftMovement + myRightMovement) * mySpeed;
	myMovement.y -= 9.82f * aDeltaTime;
}
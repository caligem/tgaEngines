#pragma once
#include "Vector.h"
#include <string>
#include "../Physics/ColliderSphere.h"

class CShared_Player
{
public:
	enum class EMovement
	{
		Right = 1,
		Left = 2
	};

	CShared_Player();
	virtual ~CShared_Player() {}

	void AddMovement(EMovement aMovement);
	void StopMovement(EMovement aMovement);

	const CColliderCircle& GetCollider() const { return myCollider; }

	inline void SetPlayerID(unsigned short aPlayerID) { myPlayerID = aPlayerID; }
	inline unsigned short GetPlayerID() const { return myPlayerID; }

	inline const std::string& GetPlayerName() const { return myPlayerName; }
	inline void SetPlayerName(const std::string& aPlayerName) { myPlayerName = aPlayerName; }

	void Jump();
	void CollidedWithFloor() { myIsOnGround = true; myMovement.y = 0.0f; }
	void CollidedWithRoof() { myMovement.y = 0.0f;	}
	void InAir() { myIsOnGround = false; }

	const CommonUtilities::Vector2f& GetPosition() const { return myPosition; }
	inline void CorrectPosition(const CommonUtilities::Vector2f& aPosition) { myPosition = aPosition; myCollider.position = aPosition;	}

	void SetMovementNextFrame(const CommonUtilities::Vector2f& aMovement) { myMovement = aMovement; }
	const CommonUtilities::Vector2f& GetMovementNextFrame() const { return myMovement; }
	float GetSpeed() const { return mySpeed; }

protected:
	void UpdateMovement(float aDeltaTime);
	CColliderCircle myCollider;
	std::string myPlayerName;

	CommonUtilities::Vector2f myPosition;
	CommonUtilities::Vector2f myMovement;
	float myLeftMovement;
	float myRightMovement;

	float mySpeed;
	float myShootCooldown;
	unsigned short myPlayerID;

	bool myIsOnGround;
};


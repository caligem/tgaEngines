#pragma once
#include "Vector.h"
#include "../Physics/ColliderBox.h"

class CShared_Projectile
{
public:
	CShared_Projectile();
	virtual ~CShared_Projectile();

	const CommonUtilities::Vector2f& GetPosition() const { return myPosition; }
	inline const CColliderBox& GetCollider() const { return myCollider; }
	const unsigned short GetProjectileIndex() const { return myProjectileIndex; }
	inline const unsigned short GetShooterID() const { return myShooterID; }
protected:
	CColliderBox myCollider;
	CommonUtilities::Vector2f myPosition;
	unsigned short myProjectileIndex;
	unsigned short myShooterID;
	static constexpr float mySpeed = 10.f;
};


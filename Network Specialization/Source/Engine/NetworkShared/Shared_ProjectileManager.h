#pragma once
#include "Timer.h"
#include <queue>

template<typename ProjectileType>
class CShared_ProjectileManager
{
public:
	CShared_ProjectileManager(CommonUtilities::Timer& aTimer);
	~CShared_ProjectileManager();

	void Init();
	void Update(float aDeltaTime);

	void ClearAllBullets();

protected:
	virtual void InternalInit() {}
	virtual void HandleProjectileDeath(ProjectileType& aProjectile) = 0;
	unsigned short GetAvailablePlayerProjectileIndex();

	CommonUtilities::Timer& myTimer;
	static constexpr unsigned short ProjectileBufferSize = 256;
	ProjectileType myProjectiles[ProjectileBufferSize];

	std::queue<unsigned short> myAvailablePlayerProjectiles;
};


template<typename ProjectileType>
CShared_ProjectileManager<ProjectileType>::CShared_ProjectileManager(CommonUtilities::Timer & aTimer)
	: myTimer(aTimer)
{
}

template<typename ProjectileType>
CShared_ProjectileManager<ProjectileType>::~CShared_ProjectileManager()
{
}

template<typename ProjectileType>
void CShared_ProjectileManager<ProjectileType>::Init()
{
	for (unsigned short i = 0; i < ProjectileBufferSize; ++i)
	{
		myAvailablePlayerProjectiles.push(i);
	}
	InternalInit();
}

template<typename ProjectileType>
void CShared_ProjectileManager<ProjectileType>::Update(float aDeltaTime)
{
	for (auto& projectile : myProjectiles)
	{
		if (!projectile.IsActive())
		{
			continue;
		}

		projectile.Update(aDeltaTime);

		if (projectile.IsDead())
		{
			HandleProjectileDeath(projectile);
		}
	}
}

template<typename ProjectileType>
void CShared_ProjectileManager<ProjectileType>::ClearAllBullets()
{
	for (auto& projectile : myProjectiles)
	{
		if (projectile.IsActive())
		{
			projectile.SetIsDead();
		}
	}
}

template<typename ProjectileType>
unsigned short CShared_ProjectileManager<ProjectileType>::GetAvailablePlayerProjectileIndex()
{
	unsigned short returnIndex = myAvailablePlayerProjectiles.front();
	myAvailablePlayerProjectiles.pop();
	return returnIndex;
}

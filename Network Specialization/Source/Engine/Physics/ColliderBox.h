#pragma once
#include "Vector.h"

class CColliderBox
{
public:
	CColliderBox();
	~CColliderBox();

	void SetBounds(const CommonUtilities::Vector2f& aBounds) { myBounds = aBounds; }
	const CommonUtilities::Vector2f& GetBounds() const { return myBounds; }

	void SetPosition(const CommonUtilities::Vector2f& aPosition) { myPosition = aPosition; }
	const CommonUtilities::Vector2f& GetPosition() const { return myPosition; }

private:
	CommonUtilities::Vector2f myBounds;
	CommonUtilities::Vector2f myPosition;
};


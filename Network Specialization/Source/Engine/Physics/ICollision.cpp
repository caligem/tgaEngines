#include "stdafx.h"
#include "ICollision.h"

#include "ColliderSphere.h"
#include "ColliderOBB.h"
#include "ColliderBox.h"
#include "Mathf.h"

bool ICollision::SphereVsSphere(const CColliderSphere & aSphere0, const CColliderSphere & aSphere1, CommonUtilities::Vector3f * aContactPoint)
{
	float d2 = (aSphere0.GetPosition() - aSphere1.GetPosition()).Length2();
	float r2 = aSphere0.GetRadius() + aSphere1.GetRadius();
	r2 *= r2;

	*aContactPoint = aSphere1.GetPosition() + (aSphere0.GetPosition() - aSphere1.GetPosition());

	if (d2 > aSphere1.GetRadius()*aSphere1.GetRadius())
	{
		*aContactPoint = aSphere1.GetPosition() + (aSphere0.GetPosition() - aSphere1.GetPosition()).GetNormalized()*aSphere1.GetRadius();
	}

	if( d2 <= r2 )
	{
		return true;
	}
	return false;
}

bool ICollision::SphereVsOBB(const CColliderSphere & aSphere, const CColliderOBB & aOBB, CommonUtilities::Vector3f * aContactPoint)
{
    CommonUtilities::Vector3f result = aOBB.GetPosition();
    CommonUtilities::Vector3f dir = aSphere.GetPosition() - aOBB.GetPosition();
    for (unsigned short i = 0; i < 3; ++i) 
    {
        const float* orientation = &aOBB.GetOrientation()[i * 3];
        CommonUtilities::Vector3f axis(orientation[0], orientation[1], orientation[2]);
        float distance = dir.Dot(axis);
        if (distance > (&aOBB.GetRadius().x)[i] / 2.0f) 
        {
            distance = (&aOBB.GetRadius().x)[i] / 2.0f;
        }
        if (distance < -(&aOBB.GetRadius().x)[i] / 2.0f)
        {
            distance = -(&aOBB.GetRadius().x)[i] / 2.0f;
        }
        result = result + (axis * distance);
    }
	*aContactPoint = result;

	float distanceBetween = (result - aSphere.GetPosition()).Length2();


	return (distanceBetween <= aSphere.GetRadius()*aSphere.GetRadius());

}

bool ICollision::BoxVsBox(const CColliderBox & aFirstBox, const CColliderBox & aSecondBox)
{
	// AABB 1
	float x1Min = aFirstBox.GetPosition().x - aFirstBox.GetBounds().x / 2.f;
	float x1Max = aFirstBox.GetPosition().x + aFirstBox.GetBounds().x / 2.f;
	float y1Max = aFirstBox.GetPosition().y + aFirstBox.GetBounds().y / 2.f;
	float y1Min = aFirstBox.GetPosition().y - aFirstBox.GetBounds().y / 2.f;

	// AABB 2
	float x2Min = aSecondBox.GetPosition().x - aSecondBox.GetBounds().x / 2;
	float x2Max = aSecondBox.GetPosition().x + aSecondBox.GetBounds().x / 2.f;
	float y2Max = aSecondBox.GetPosition().y + aSecondBox.GetBounds().y / 2.f;
	float y2Min = aSecondBox.GetPosition().y - aSecondBox.GetBounds().y / 2.f;

	// Collision tests
	if (x1Max < x2Min || x1Min > x2Max) return false;
	if (y1Max < y2Min || y1Min > y2Max) return false;
	return true;
}

bool ICollision::BoxVsBox(const CommonUtilities::Vector2f & aFirstBoxPosition, const CommonUtilities::Vector2f & aFirstBoxBounds, const CColliderBox & aSecondBox, CommonUtilities::Vector2f* contactPoint)
{
	float x1Min = aFirstBoxPosition.x - aFirstBoxBounds.x / 2.f;
	float x1Max = aFirstBoxPosition.x + aFirstBoxBounds.x / 2.f;
	float y1Max = aFirstBoxPosition.y + aFirstBoxBounds.y / 2.f;
	float y1Min = aFirstBoxPosition.y - aFirstBoxBounds.y / 2.f;

	float x2Min = aSecondBox.GetPosition().x - aSecondBox.GetBounds().x / 2;
	float x2Max = aSecondBox.GetPosition().x + aSecondBox.GetBounds().x / 2.f;
	float y2Max = aSecondBox.GetPosition().y + aSecondBox.GetBounds().y / 2.f;
	float y2Min = aSecondBox.GetPosition().y - aSecondBox.GetBounds().y / 2.f;

	if (x1Max < x2Min || x1Min > x2Max) return false;
	if (y1Max < y2Min || y1Min > y2Max) return false;

	if (contactPoint)
	{
		contactPoint->x = CommonUtilities::Clamp(aFirstBoxPosition.x, x2Min, x2Max);
		contactPoint->y = CommonUtilities::Clamp(aFirstBoxPosition.y, y2Min, y2Max);
	}

	return true;
}

#include <limits>
#include <algorithm>
float ICollision::SweptBoxVsBox(const CColliderBox & aDynamicBox, const float aVelocityX, const float aVelocityY, const CColliderBox & aStaticBox, float & aNormalX, float & aNormalY)
{
	float xInvEntry, yInvEntry;
	float xInvExit, yInvExit;

	// find the distance between the objects on the near and far sides for both x and y

	CommonUtilities::Vector2f staticBoxTopLeftCorner = aStaticBox.GetPosition() - aStaticBox.GetBounds() / 2.f;
	CommonUtilities::Vector2f dynamicBoxTopLeftCorner = aDynamicBox.GetPosition() - aDynamicBox.GetBounds() / 2.f;

	if (aVelocityX > 0.0f)
	{
		xInvEntry = staticBoxTopLeftCorner.x - (dynamicBoxTopLeftCorner.x + aDynamicBox.GetBounds().x);
		xInvExit = (staticBoxTopLeftCorner.x + aStaticBox.GetBounds().x) - dynamicBoxTopLeftCorner.x;
	}
	else
	{
		xInvEntry = (staticBoxTopLeftCorner.x + aStaticBox.GetBounds().x) - dynamicBoxTopLeftCorner.x;
		xInvExit = staticBoxTopLeftCorner.x - (dynamicBoxTopLeftCorner.x + aDynamicBox.GetBounds().x);
	}

	if (aVelocityY > 0.0f)
	{
		yInvEntry = staticBoxTopLeftCorner.y - (dynamicBoxTopLeftCorner.y + aDynamicBox.GetBounds().y);
		yInvExit = (staticBoxTopLeftCorner.y + aStaticBox.GetBounds().y) - dynamicBoxTopLeftCorner.y;
	}
	else
	{
		yInvEntry = (staticBoxTopLeftCorner.y + aStaticBox.GetBounds().y) - dynamicBoxTopLeftCorner.y;
		yInvExit = staticBoxTopLeftCorner.y - (dynamicBoxTopLeftCorner.y + aDynamicBox.GetBounds().y);
	}

	// find time of collision and time of leaving for each axis (if statement is to prevent divide by zero)
	float xEntry, yEntry;
	float xExit, yExit;

	if (aVelocityX == 0.0f)
	{
		xEntry = -std::numeric_limits<float>::infinity();
		xExit = std::numeric_limits<float>::infinity();
	}
	else
	{
		xEntry = xInvEntry / aVelocityX;
		xExit = xInvExit / aVelocityX;
	}

	if (aVelocityY == 0.0f)
	{
		yEntry = -std::numeric_limits<float>::infinity();
		yExit = std::numeric_limits<float>::infinity();
	}
	else
	{
		yEntry = yInvEntry / aVelocityY;
		yExit = yInvExit / aVelocityY;
	}

	// find the earliest/latest times of collision
	float entryTime = std::max(xEntry, yEntry);
	float exitTime = std::min(xExit, yExit);


	if (entryTime > exitTime || xEntry < 0.0f && yEntry < 0.0f || xEntry > 1.0f || yEntry > 1.0f)
	{
		aNormalX = 0.0f;
		aNormalY = 0.0f;
		return 1.0f;
	}
	else // if there was a collision
	{
		// calculate normal of collided surface
		if (xEntry > yEntry)
		{
			if (xInvEntry < 0.0f)
			{
				aNormalX = 1.0f;
				aNormalY = 0.0f;
			}
			else
			{
				aNormalX = -1.0f;
				aNormalY = 0.0f;
			}
		}
		else
		{
			if (yInvEntry < 0.0f)
			{
				aNormalX = 0.0f;
				aNormalY = 1.0f;
			}
			else
			{
				aNormalX = 0.0f;
				aNormalY = -1.0f;
			}
		}

		// return the time of collision
		return entryTime;
	}
}

bool ICollision::CircleVSBox(const CommonUtilities::Vector2f & aPosition, const float aRadius, const CColliderBox & aBox, CommonUtilities::Vector2f * aContactPoint)
{
	CommonUtilities::Vector2f contactPoint;
	contactPoint.x = CommonUtilities::Clamp(aPosition.x, aBox.GetPosition().x - aBox.GetBounds().x / 2.f, aBox.GetPosition().x + aBox.GetBounds().x / 2.f);
	contactPoint.y = CommonUtilities::Clamp(aPosition.y, aBox.GetPosition().y - aBox.GetBounds().y / 2.f, aBox.GetPosition().y + aBox.GetBounds().y / 2.f);

	if (aContactPoint)
	{
		*aContactPoint = contactPoint;
	}

	if ((aPosition - contactPoint).Length() < aRadius)
	{
		return true;
	}
	return false;
}

bool ICollision::CircleVSBox(const CColliderCircle & aCircle, const CColliderBox & aBox)
{
	return CircleVSBox(aCircle.position, aCircle.radius, aBox);
}

CColliderBox ICollision::GetSweptBroadphaseBox(const CColliderBox& aBox, const CommonUtilities::Vector2f& aVelocity)
{
	CColliderBox broadphasebox;
	CommonUtilities::Vector2f pos;
	CommonUtilities::Vector2f bounds;


	CommonUtilities::Vector2f topLeftCorner = aBox.GetPosition() - aBox.GetBounds() / 2.f;

	pos.x = aVelocity.x > 0 ? topLeftCorner.x : topLeftCorner.x + aVelocity.x;
	pos.y = aVelocity.y > 0 ? topLeftCorner.y : topLeftCorner.y + aVelocity.y;
	bounds.x = aVelocity.x > 0 ? aVelocity.x + aBox.GetBounds().x : aBox.GetBounds().x - aVelocity.x;
	bounds.y = aVelocity.y > 0 ? aVelocity.y + aBox.GetBounds().y : aBox.GetBounds().y - aVelocity.y;

	broadphasebox.SetPosition(pos);
	broadphasebox.SetBounds(bounds);

	return broadphasebox;
}

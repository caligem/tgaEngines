#pragma once

#include "Vector.h"

class CColliderSphere;
class CColliderOBB;
class CColliderBox;
class CColliderCircle;


namespace ICollision
{
	bool SphereVsSphere(
		const CColliderSphere& aSphere0,
		const CColliderSphere& aSphere1,
		CommonUtilities::Vector3f* aContactPoint = nullptr
	);
	bool SphereVsOBB(
		const CColliderSphere& aSphere,
		const CColliderOBB& aOBB,
		CommonUtilities::Vector3f* aContactPoint = nullptr
	);
	bool BoxVsBox(
		const CColliderBox& aFirstBox,
		const CColliderBox& aSecondBox
	);
	bool BoxVsBox(
		const CommonUtilities::Vector2f& aFirstBoxPosition,
		const CommonUtilities::Vector2f& aFirstBoxBounds,
		const CColliderBox& aSecondBox,
		CommonUtilities::Vector2f* contactPoint
	);
	float SweptBoxVsBox(
		const CColliderBox& aDynamicBox,
		const float aVelocityX,
		const float aVelocityY,
		const CColliderBox& aStaticBox,
		float& aNormalX,
		float& aNormalY
	);
	bool CircleVSBox(
		const CommonUtilities::Vector2f& aPosition,
		const float aRadius,
		const CColliderBox& aBox,
		CommonUtilities::Vector2f* aContactPoint = nullptr
	);
	bool CircleVSBox(
		const CColliderCircle& aCircle,
		const CColliderBox& aBox
	);

	CColliderBox GetSweptBroadphaseBox(const CColliderBox& aBox, const CommonUtilities::Vector2f& aVelocity);
}


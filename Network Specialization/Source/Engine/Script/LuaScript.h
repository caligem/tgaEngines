#pragma once
#include <string>
#include <ObjectPool.h>

class CScriptManager;
class CScriptComponent;

class CLuaScript
{
	friend CScriptManager;
public:
	CLuaScript();
	~CLuaScript();

	const std::string& GetPath() const;
private:
	void Init(CScriptComponent* aScriptComponent, CScriptManager* aScriptManager, lua_State* aLuaState, const char* aPath);
	void InitForNonGameObjects(CScriptManager* aScriptManager, lua_State* aLuaState, const char* aPath);
	friend CScriptManager;
	CScriptManager* ourScriptManager;
	CScriptComponent* myOwner;
	lua_State* myLuaState;
	std::string myPath;
	bool myScriptStateIsValid;
};
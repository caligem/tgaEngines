#include "stdafx.h"
#include "LuaUtility.h"

#include "Mathf.h"
#include "Macros.h"

int LevenshteinDistance(const char * s, int len_s, const char * t, int len_t)
{
	if (len_s == 0) return len_t;
	if (len_t == 0) return len_s;

	int** d = new int*[len_s];
	for (int i = 0; i < len_s; ++i)d[i] = new int[len_t];

	for (int i = 0; i < len_s; ++i)
	{
		for (int j = 0; j < len_t; ++j)
		{
			d[i][j] = 0;
		}
	}

	for (int i = 1; i < len_s; ++i)
	{
		d[i][0] = i;
	}
	for (int i = 1; i < len_t; ++i)
	{
		d[0][i] = i;
	}

	for (int j = 1; j < len_t; ++j)
	{
		for (int i = 1; i < len_s; ++i)
		{
			int substitutionCost = 0;
			if (s[i] == t[j])
			{
				substitutionCost = 0;
			}
			else
			{
				substitutionCost = 1;
			}

			d[i][j] = CommonUtilities::Min(
				CommonUtilities::Min(
					d[i - 1][j] + 1,
					d[i][j - 1] + 1
				),
				d[i - 1][j - 1] + substitutionCost
			);
		}
	}

	int cost = d[len_s - 1][len_t - 1];

	for (int i = 0; i < len_s; ++i)SAFE_DELETE_ARRAY(d[i]);
	SAFE_DELETE_ARRAY(d);

	return cost;
}

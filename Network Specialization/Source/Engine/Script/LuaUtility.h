#pragma once

int LevenshteinDistance(const char *s, int len_s, const char *t, int len_t);
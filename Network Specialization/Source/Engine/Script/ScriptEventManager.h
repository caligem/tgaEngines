#pragma once
#include <unordered_map>
#include <vector>

#include "ObjectPool.h"
#include "LuaScript.h"
#include "GrowingArray.h"

class CScriptManager;

class CScriptEventManager
{
public:
	friend CScriptManager;
	CScriptEventManager();
	~CScriptEventManager();

	enum EScriptEvent	//Must match as Table in MessageType.Lua or events won't work
	{
		EScriptEvent_OnEnter,
		EScriptEvent_OnLeave,
		EScriptEvent_Count
	};
	struct SLuaCallback
	{
		bool SLuaCallback::operator==(const SLuaCallback &other) const {
			return this->idt == other.idt;
		}

		ID_T(CLuaScript) idt;
		unsigned short triggerID;
		std::string functionName;
	};

private:
	void Notify(const EScriptEvent aEvent, const unsigned short aTriggerID);
	void AddListener(const EScriptEvent aEvent, const SLuaCallback aLuaCallback);
	void RemoveListener(const EScriptEvent aEvent, const SLuaCallback aLuaCallback);
	void CallFunction(SLuaCallback aCallback);


	std::vector<CommonUtilities::GrowingArray<SLuaCallback> > myListeners[EScriptEvent_Count];
	
	static CScriptManager* ourScriptManager;
};

#include "DataPass.si"
#include "../CameraBuffer.si"

cbuffer InstanceData : register(b1)
{
	float4x4 toWorld;
}

PixelInput main(VertexInput input)
{
	PixelInput output;

	input.myPosition.w = 1.f;
	output.myPosition = mul(toWorld, input.myPosition);
	output.myWorldPosition = output.myPosition;
	output.myPosition = mul(viewProjection, output.myPosition);

	output.myUV = input.myUV;
	input.myNormal.w = 0.f;
	output.myNormal.xyz = normalize(mul((float3x3)toWorld, input.myNormal.xyz));
	output.myTangent.xyz = normalize(mul((float3x3)toWorld, input.myTangent.xyz));
	output.myBinormal.xyz = normalize(mul((float3x3)toWorld, input.myBinormal.xyz));

	output.myViewPosition = float4(cameraOrientation._14, cameraOrientation._24, cameraOrientation._34, 1.f);

	return output;
}

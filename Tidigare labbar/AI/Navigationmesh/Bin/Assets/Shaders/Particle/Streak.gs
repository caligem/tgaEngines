#include "Particle.si"

[maxvertexcount(4)]
void main(lineadj GeometryInput input[4], inout TriangleStream<PixelInput> output)
{
	const float offset[4] = 
	{
		input[1].mySize.y,
		input[2].mySize.y,
		-input[1].mySize.y,
		-input[2].mySize.y
	};
	
	const float2 uv[4] = 
	{
		{ input[1].mySize.x, 1 },
		{ input[2].mySize.x, 1 },
		{ input[1].mySize.x, 0 },
		{ input[2].mySize.x, 0 }
	};
	
	const float2 dir[2] =
	{
		normalize(input[2].myPosition.xy - input[0].myPosition.xy),
		normalize(input[3].myPosition.xy - input[1].myPosition.xy)
	};
	const float2 normal[2] =
	{
		float2(-dir[0].y, dir[0].x),
		float2(-dir[1].y, dir[1].x)
	};
	
	for(int i = 0; i < 4; ++i)
	{
		PixelInput vertex;
		
		vertex.myPosition.xy = input[i%2+1].myPosition.xy + normal[i%2]*offset[i];
		vertex.myPosition.zw = input[i%2+1].myPosition.zw;
		vertex.myPosition = mul(toProjection, vertex.myPosition);
		vertex.myColor = input[i%2+1].myColor;
		vertex.myUV = uv[i];
		output.Append(vertex);
	}
	output.RestartStrip();
}
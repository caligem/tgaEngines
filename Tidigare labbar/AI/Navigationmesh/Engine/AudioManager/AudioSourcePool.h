#pragma once
#include <array>
#include "AudioSource.h"
class AudioSourcePool
{
public:
	AudioSourcePool();
	~AudioSourcePool();
	AudioSource* GetAudioSource();
	int GetAmmountOfFreeAudioSources();
	void ReturnAudioSource(AudioSource* aAudioSource);
private:
	std::array<AudioSource, 512> myAudioSourcePool;
	std::array<bool, 512> myFreeIndexes;
};


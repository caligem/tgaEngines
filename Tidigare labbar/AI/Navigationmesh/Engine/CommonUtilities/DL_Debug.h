#pragma once

#include <fstream>
#include <map>
#include <string>
#include <chrono>

namespace DL_Debug
{
	class Debug
	{
	public:
		static bool Destroy();
		static Debug* GetInstance();

		void WriteLog(const char* aLogName, int aColor, const char* aFormattedString, ...);
		void ActivateFilterLog(const char* aLogName);
		void DeactivateFilterLog(const char* aLogName);
		bool IsActive(const char* aLogName);

	private:
		Debug() = default;
		~Debug() = default;
		static Debug* ourInstance;

		struct LogFile
		{
			FILE* myFilePtr = nullptr;
			bool myIsActive = false;
		};
		std::map<std::string, LogFile> myFilterLogFiles;
	};
}

// Filter Log
#ifndef _RETAIL
#define USE_FILTERLOG
#endif

#ifdef USE_FILTERLOG
#define DL_WRITELOG(aLog, aColor, ...) DL_Debug::Debug::GetInstance()->WriteLog(aLog, aColor, __VA_ARGS__);
#define RESOURCE_LOG(aColor, ...) DL_WRITELOG("resource", aColor, __VA_ARGS__)
#define ENGINE_LOG(aColor, ...) DL_WRITELOG("engine", aColor, __VA_ARGS__)
#define GAMEPLAY_LOG(aColor, ...) DL_WRITELOG("gameplay", aColor, __VA_ARGS__)
#define GENERAL_LOG(aColor, ...) DL_WRITELOG("general", aColor, __VA_ARGS__)
#else
#define DL_WRITELOG(...)
#define RESOURCE_LOG(...)
#define ENGINE_LOG(...)
#define GAMEPLAY_LOG(...)
#define GENERAL_LOG(...)
#endif

#define CONCOL_ERROR 12
#define CONCOL_WARNING 14
#define CONCOL_VALID 10
#define CONCOL_DEFAULT 15

typedef std::chrono::microseconds milliseconds;

#ifndef _RETAIL
#define START_TIMER(ID) std::chrono::steady_clock::time_point ID = std::chrono::steady_clock::now();
#define GET_TIME(ID) (static_cast<float>((std::chrono::duration_cast<milliseconds>(std::chrono::steady_clock::now()-ID).count()))/1000.f)
#else
#define START_TIMER(ID)
#define GET_TIME(ID) 0.f
#endif

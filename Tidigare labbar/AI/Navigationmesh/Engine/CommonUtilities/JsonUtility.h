#pragma once

#include "JsonDocument.h"
#include "Vector.h"
#include "Quaternion.h"
#include "Mathf.h"

CommonUtilities::Vector2f JsonToVector2f(JsonValue aValue)
{
	return {
		aValue["myX"].GetFloat(),
		aValue["myY"].GetFloat()
	};
}
CommonUtilities::Vector3f JsonToVector3f(JsonValue aValue)
{
	return {
		aValue["myX"].GetFloat(),
		aValue["myY"].GetFloat(),
		aValue["myZ"].GetFloat()
	};
}
CommonUtilities::Vector4f JsonToVector4f(JsonValue aValue)
{
	return {
		aValue["myX"].GetFloat(),
		aValue["myY"].GetFloat(),
		aValue["myZ"].GetFloat(),
		aValue["myW"].GetFloat()
	};
}

CommonUtilities::Quatf JsonToQuatf(JsonValue aValue)
{
	return {
		aValue["myW"].GetFloat(),
		aValue["myX"].GetFloat(),
		aValue["myY"].GetFloat(),
		aValue["myZ"].GetFloat()
	};
}
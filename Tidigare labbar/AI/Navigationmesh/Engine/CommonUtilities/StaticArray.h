#pragma once

#include <assert.h>

#include "Macros.h"

namespace CommonUtilities
{
	template <typename T, int size>
	class StaticArray
	{
	public:
		StaticArray() = default;
		StaticArray(const StaticArray& aStaticArray);
		
		~StaticArray() = default;

		StaticArray& operator=(const StaticArray& aStaticArray);

		inline const T& operator[](const int& aIndex) const;
		inline T& operator[](const int& aIndex);

		//Utility functions
		inline void Insert(int aIndex, T& aObject);
		inline void DeleteAll();

	private:
		T myData[size];
	};
	
	template<typename T, int size>
	inline StaticArray<T, size>::StaticArray(const StaticArray & aStaticArray)
	{
		for (int i = 0; i < size; ++i)
		{
			myData[i] = aStaticArray[i];
		}
	}

	template<typename T, int size>
	inline StaticArray<T, size>& StaticArray<T, size>::operator=(const StaticArray & aStaticArray)
	{
		for (int i = 0; i < size; ++i)
		{
			myData[i] = aStaticArray[i];
		}
		return *this;
	}

	template<typename T, int size>
	inline const T& StaticArray<T, size>::operator[](const int& aIndex) const
	{
		assert(aIndex >= 0 && aIndex < size && " === Subscript index out of bounds! ===");
		return myData[aIndex];
	}

	template<typename T, int size>
	inline T& StaticArray<T, size>::operator[](const int& aIndex)
	{
		assert(aIndex >= 0 && aIndex < size && " === Subscript index out of bounds! ===");
		return myData[aIndex];
	}

	template<typename T, int size>
	inline void StaticArray<T, size>::Insert(int aIndex, T& aObject)
	{
		assert(aIndex >= 0 && aIndex < size && " === Insert index out of bounds! ===");
		for (int i = size - 1; i > aIndex; --i)
		{
			myData[i] = myData[i-1];
		}
		myData[aIndex] = aObject;
	}

	template<typename T, int size>
	inline void StaticArray<T, size>::DeleteAll()
	{
		for (int i = 0; i < size; ++i)
		{
			SAFE_DELETE(myData[i]);
		}
	}

}

#include "Timer.h"

#include "Mathf.h"

namespace CommonUtilities
{
	Timer::Timer()
	{
		myStartTime = GetCurrentTime();
		myRealCurrentTime = myStartTime;
		myRealPreviousTime = myStartTime;
		myRealTotalTime = myRealCurrentTime - myRealPreviousTime;
		myCurrentTime = myStartTime;
		myPreviousTime = myStartTime;
		myTotalTime = myCurrentTime - myPreviousTime;
		mySpeed = 1.0;
	}

	Timer::~Timer() {}

	void Timer::Update()
	{
		myRealPreviousTime = myRealCurrentTime;
		myRealCurrentTime += GetCurrentTime() - myRealPreviousTime;

		myRealTotalTime = myRealCurrentTime - myStartTime;

		myPreviousTime = myCurrentTime;
		myCurrentTime += (GetCurrentTime() - myRealPreviousTime) * mySpeed;

		myTotalTime = myCurrentTime - myStartTime;
	}

	float Timer::GetRealDeltaTime() const
	{
		return std::move(Clamp(static_cast<float>(myRealCurrentTime - myRealPreviousTime), 0.f, 1.f/30.f));
	}
	float Timer::GetRealTotalTime() const
{
		return std::move(static_cast<float>(myRealTotalTime));
	}
	float Timer::GetDeltaTime() const
	{
		return std::move(Clamp(static_cast<float>(myCurrentTime - myPreviousTime), 0.f, static_cast<float>(mySpeed)/30.f));
	}
	float Timer::GetTotalTime() const
	{
		return std::move(static_cast<float>(myTotalTime));
	}
	double Timer::GetCurrentTime()
	{
		return static_cast<double>(
			std::chrono::high_resolution_clock::now().time_since_epoch().count()
		) / std::chrono::high_resolution_clock::period::den;
	}
}

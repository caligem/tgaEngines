#include "stdafx.h"
#include "Editor.h"

#include "InputManager.h"
#include "IWorld.h"

using namespace std::placeholders;

#include <iostream>

CEditor* CEditor::ourEditor = nullptr;

CEditor::CEditor()
	: myApplicationName(L"Application")
{
	AllocConsole();
	FILE* cinFile;
	freopen_s(&cinFile, "CONIN$", "r", stdin);
	FILE* coutFile;
	freopen_s(&coutFile, "CONOUT$", "w", stdout);
	FILE* cerrFile;
	freopen_s(&cerrFile, "CONOUT$", "w", stderr);
	std::cout << "Editor started in debug mode." << std::endl;

	ourEditor = this;
}

CEditor::~CEditor()
{
	FreeConsole();
}

bool CEditor::Init(HWND aHWND)
{
	DL_Debug::Debug::GetInstance()->ActivateFilterLog("general");
	DL_Debug::Debug::GetInstance()->ActivateFilterLog("resource");
	DL_Debug::Debug::GetInstance()->ActivateFilterLog("engine");

	myWindowMessageDoubleBuffer.Init();
	myEngineMessageDoubleBuffer.Init();
	myEditorMessageDoubleBuffer.Init();

	myEngine.myMessageHandler = std::bind(&CEditor::HandleMessages, this);

	RECT rect;
	GetWindowRect(aHWND, &rect);

	unsigned short windowWidth = static_cast<unsigned short>(rect.right - rect.left);
	unsigned short windowHeight = static_cast<unsigned short>(rect.bottom - rect.top);

	SCreateParameters createParameters;

	createParameters.myInitFunctionToCall = std::bind(&CEditor::InitCallback, this);
	createParameters.myUpdateFunctionToCall = std::bind(&CEditor::UpdateCallback, this);
	createParameters.myWndProcCallback = std::bind(&CEditor::WndProc, this, _1, _2, _3, _4);

	createParameters.myWindowWidth = windowWidth;
	createParameters.myWindowHeight = windowHeight;
	createParameters.myCanvasWidth = windowWidth;
	createParameters.myCanvasHeight = windowHeight;

	createParameters.myEnableVSync = true;
	createParameters.myIsFullscreen = false;
	createParameters.myIsBorderless = false;

	createParameters.myGameName = myApplicationName;

	createParameters.myHWND = aHWND;

	if (!myEngine.Init(createParameters))
	{
		GENERAL_LOG(CONCOL_ERROR, "Failed to Init Engine");
		return false;
	}
	
	return true;
}

LRESULT CEditor::WndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam)
{
	aHwnd; wParam; lParam;

	switch (aMessage)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
		break;
	}

	return 0;
}

void CEditor::InitCallback()
{
}

void CEditor::UpdateCallback()
{
	UpdateInput();
}

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
void CEditor::UpdateInput()
{
	for (auto& message : myWindowMessageDoubleBuffer.GetReadBuffer())
	{
		//ImGui_ImplWin32_WndProcHandler(myEngine.GetWindowHandle(), message.myMessage, message.myWParam, message.myLParam);
		//IWorld::Input().OnInput(message.myMessage, message.myWParam, message.myLParam);
		CWindowHandler::WndProc(myEngine.GetWindowHandle(), message.myMessage, message.myWParam, message.myLParam);
	}

	std::unique_lock<std::mutex> lock(myInputLock);
	myWindowMessageDoubleBuffer.SwapBuffers();
}

void CEditor::HandleMessages()
{
	for (auto& message : myEngineMessageDoubleBuffer.GetReadBuffer())
	{
		if (!myEngineMessageCallbacks[message.myMessageType])continue;

		myEngineMessageCallbacks[message.myMessageType](message);
		SAFE_DELETE_ARRAY(message.myData);
	}
	for (auto& message : myEditorMessageDoubleBuffer.GetReadBuffer())
	{
		if (!myEditorMessageCallback)continue;

		myEditorMessageCallback(message.myData, message.myMessageType, message.myDataType);
		SAFE_DELETE_ARRAY(message.myData);
	}

	std::unique_lock<std::mutex> lock1(myIncomingMessageLock);
	std::unique_lock<std::mutex> lock2(myOutgoingMessageLock);
	myEngineMessageDoubleBuffer.SwapBuffers();
	myEditorMessageDoubleBuffer.SwapBuffers();
}

void CEditor::StartEngine()
{
	myEngine.StartEngine();
}

void CEditor::Shutdown()
{
	myEngine.Shutdown();
}

bool CEditor::IsRunning()
{
	return myEngine.IsRunning();
}

void CEditor::SendWindowMessage(unsigned int Msg, void * wParam, void * lParam)
{
	//if ((!myHasFocus) && Msg != WM_SIZE )return;
	std::unique_lock<std::mutex> lock(myInputLock);
	myWindowMessageDoubleBuffer.Write({
		Msg, reinterpret_cast<unsigned long long>(wParam), reinterpret_cast<long long>(lParam)
	});
}

void CEditor::GotFocus()
{
	myHasFocus = true;
}

void CEditor::LostFocus()
{
	myHasFocus = false;
	std::unique_lock<std::mutex> lock(myInputLock);
	IWorld::Input().LostFocus();
	myWindowMessageDoubleBuffer.SwapBuffers();
}

void CEditor::SendMessage(const unsigned char * newData, EMessageType aMessageType, EDataType aDataType)
{
	if (ourEditor == nullptr)
	{
		return;
	}

	std::unique_lock<std::mutex> lock(ourEditor->myOutgoingMessageLock);

	int byteSize = static_cast<int>(aDataType);
	unsigned char* copiedData = new unsigned char[byteSize];
	if (copiedData == nullptr)
	{
		return;
	}

	memcpy_s(copiedData, byteSize, newData, byteSize);

	ourEditor->myEditorMessageDoubleBuffer.Write({
		aMessageType, aDataType, copiedData
	});
}

void CEditor::RecieveMessage(const unsigned char* newData, EMessageType aMessageType, EDataType aDataType)
{
	std::unique_lock<std::mutex> lock(myIncomingMessageLock);
	myEngineMessageDoubleBuffer.Write({
		aMessageType, aDataType, newData
	});
}

void CEditor::Subscribe(EMessageType aMessageType, std::function<void(const SMessage&)> aCallback)
{
	myEngineMessageCallbacks[aMessageType] = aCallback;
}

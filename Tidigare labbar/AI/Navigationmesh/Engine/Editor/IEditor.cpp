#include "stdafx.h"
#include "IEditor.h"

#include "IEngine.h"

#include <utility>

const CommonUtilities::Vector2f IEditor::GetWindowSize()
{
	return std::move(IEngine::GetWindowSize());
}

const CommonUtilities::Vector2f IEditor::GetCanvasSize()
{
	return std::move(IEngine::GetCanvasSize());
}

const CGraphicsPipeline::SCameraBufferData& IEditor::GetSavedCameraBuffer()
{
	return IEngine::GetGraphicsPipeline().GetSavedCameraBuffer();
}

CommonUtilities::Timer & IEditor::Time()
{
	return IEngine::Time();
}

CommonUtilities::XBOXController & IEditor::XBOX()
{
	return IEngine::GetXBoxController();
}

Input::CInputManager & IEditor::Input()
{
	return IEngine::GetInputManager();
}

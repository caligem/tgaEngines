﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditorTools
{
    public partial class Vector2
    {
        public float x;
        public float y;
        public Vector2()
        {
            x = y = 0f;
        }
        public Vector2(float aX, float aY)
        {
            x = aX;
            y = aY;
        }
    }

    public partial class Vector3
    {
        public float x;
        public float y;
        public float z;

        public Vector3()
        {
            x = y = z = 0f;
        }
        public Vector3(float aX, float aY, float aZ)
        {
            x = aX;
            y = aY;
            z = aZ;
        }
    }

    public partial class Vector4
    {
        public float x;
        public float y;
        public float z;
        public float w;

        public Vector4()
        {
            x = y = z = w = 0f;
        }
        public Vector4(float aX, float aY, float aZ, float aW)
        {
            x = aX;
            y = aY;
            z = aZ;
            w = aW;
        }
    }
}

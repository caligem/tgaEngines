﻿namespace EditorTools
{
	partial class ColorPicker
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.bigPanel = new System.Windows.Forms.Panel();
			this.smallPanel = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.previewPanel = new System.Windows.Forms.Panel();
			this.redLabel = new System.Windows.Forms.Label();
			this.greenLabel = new System.Windows.Forms.Label();
			this.blueLabel = new System.Windows.Forms.Label();
			this.hueInput = new System.Windows.Forms.NumericUpDown();
			this.saturationInput = new System.Windows.Forms.NumericUpDown();
			this.brightnessInput = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.trackBar1 = new System.Windows.Forms.TrackBar();
			this.alphaInput = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.hueInput)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.saturationInput)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.brightnessInput)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.alphaInput)).BeginInit();
			this.SuspendLayout();
			// 
			// bigPanel
			// 
			this.bigPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.bigPanel.Location = new System.Drawing.Point(12, 12);
			this.bigPanel.Name = "bigPanel";
			this.bigPanel.Size = new System.Drawing.Size(256, 256);
			this.bigPanel.TabIndex = 0;
			this.bigPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.bigPanel_Paint);
			this.bigPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.bigPanel_MouseDown);
			this.bigPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.bigPanel_MouseMove);
			this.bigPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.bigPanel_MouseUp);
			// 
			// smallPanel
			// 
			this.smallPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.smallPanel.Location = new System.Drawing.Point(274, 12);
			this.smallPanel.Name = "smallPanel";
			this.smallPanel.Size = new System.Drawing.Size(20, 256);
			this.smallPanel.TabIndex = 0;
			this.smallPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.smallPanel_Paint);
			this.smallPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.smallPanel_MouseDown);
			this.smallPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.smallPanel_MouseMove);
			this.smallPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.smallPanel_MouseUp);
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(402, 12);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(96, 23);
			this.button1.TabIndex = 1;
			this.button1.Text = "Ok";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(402, 41);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(96, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "Cancel";
			this.button2.UseVisualStyleBackColor = true;
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// previewPanel
			// 
			this.previewPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.previewPanel.Location = new System.Drawing.Point(300, 12);
			this.previewPanel.Name = "previewPanel";
			this.previewPanel.Size = new System.Drawing.Size(96, 64);
			this.previewPanel.TabIndex = 0;
			this.previewPanel.Paint += new System.Windows.Forms.PaintEventHandler(this.previewPanel_Paint);
			// 
			// redLabel
			// 
			this.redLabel.AutoSize = true;
			this.redLabel.Location = new System.Drawing.Point(300, 85);
			this.redLabel.Name = "redLabel";
			this.redLabel.Size = new System.Drawing.Size(18, 13);
			this.redLabel.TabIndex = 2;
			this.redLabel.Text = "H:";
			// 
			// greenLabel
			// 
			this.greenLabel.AutoSize = true;
			this.greenLabel.Location = new System.Drawing.Point(300, 108);
			this.greenLabel.Name = "greenLabel";
			this.greenLabel.Size = new System.Drawing.Size(17, 13);
			this.greenLabel.TabIndex = 2;
			this.greenLabel.Text = "S:";
			// 
			// blueLabel
			// 
			this.blueLabel.AutoSize = true;
			this.blueLabel.Location = new System.Drawing.Point(300, 131);
			this.blueLabel.Name = "blueLabel";
			this.blueLabel.Size = new System.Drawing.Size(17, 13);
			this.blueLabel.TabIndex = 2;
			this.blueLabel.Text = "B:";
			// 
			// hueInput
			// 
			this.hueInput.Location = new System.Drawing.Point(324, 83);
			this.hueInput.Maximum = new decimal(new int[] {
            360,
            0,
            0,
            0});
			this.hueInput.Name = "hueInput";
			this.hueInput.Size = new System.Drawing.Size(72, 20);
			this.hueInput.TabIndex = 3;
			this.hueInput.ValueChanged += new System.EventHandler(this.hueInput_ValueChanged);
			// 
			// saturationInput
			// 
			this.saturationInput.Location = new System.Drawing.Point(324, 106);
			this.saturationInput.Name = "saturationInput";
			this.saturationInput.Size = new System.Drawing.Size(72, 20);
			this.saturationInput.TabIndex = 3;
			this.saturationInput.ValueChanged += new System.EventHandler(this.saturationInput_ValueChanged);
			// 
			// brightnessInput
			// 
			this.brightnessInput.Location = new System.Drawing.Point(323, 129);
			this.brightnessInput.Name = "brightnessInput";
			this.brightnessInput.Size = new System.Drawing.Size(72, 20);
			this.brightnessInput.TabIndex = 3;
			this.brightnessInput.ValueChanged += new System.EventHandler(this.brightnessInput_ValueChanged);
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(301, 176);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(17, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "A:";
			// 
			// trackBar1
			// 
			this.trackBar1.Location = new System.Drawing.Point(300, 200);
			this.trackBar1.Maximum = 255;
			this.trackBar1.Name = "trackBar1";
			this.trackBar1.Size = new System.Drawing.Size(96, 45);
			this.trackBar1.TabIndex = 5;
			this.trackBar1.TickFrequency = 16;
			this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
			// 
			// alphaInput
			// 
			this.alphaInput.Location = new System.Drawing.Point(324, 174);
			this.alphaInput.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
			this.alphaInput.Name = "alphaInput";
			this.alphaInput.Size = new System.Drawing.Size(72, 20);
			this.alphaInput.TabIndex = 3;
			this.alphaInput.ValueChanged += new System.EventHandler(this.alphaInput_ValueChanged);
			// 
			// ColorPicker
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(504, 277);
			this.Controls.Add(this.trackBar1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.alphaInput);
			this.Controls.Add(this.brightnessInput);
			this.Controls.Add(this.saturationInput);
			this.Controls.Add(this.hueInput);
			this.Controls.Add(this.blueLabel);
			this.Controls.Add(this.greenLabel);
			this.Controls.Add(this.redLabel);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.previewPanel);
			this.Controls.Add(this.smallPanel);
			this.Controls.Add(this.bigPanel);
			this.Name = "ColorPicker";
			this.Text = "ColorPicker";
			((System.ComponentModel.ISupportInitialize)(this.hueInput)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.saturationInput)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.brightnessInput)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.alphaInput)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Panel bigPanel;
		private System.Windows.Forms.Panel smallPanel;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Panel previewPanel;
		private System.Windows.Forms.Label redLabel;
		private System.Windows.Forms.Label greenLabel;
		private System.Windows.Forms.Label blueLabel;
		private System.Windows.Forms.NumericUpDown hueInput;
		private System.Windows.Forms.NumericUpDown saturationInput;
		private System.Windows.Forms.NumericUpDown brightnessInput;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TrackBar trackBar1;
		private System.Windows.Forms.NumericUpDown alphaInput;
	}
}
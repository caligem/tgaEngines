#include "stdafx.h"
#include "AudioSourceComponent.h"

#include "AudioManager.h"
#include "AudioSource.h"

#include "ComponentSystem.h"
#include "IEngine.h"

#include "IWorld.h"


CAudioSourceComponent::CAudioSourceComponent()
{
	myAudioSource = nullptr;
	myRange = 3.f;
}


CAudioSourceComponent::~CAudioSourceComponent()
{
}

void CAudioSourceComponent::Init(SAudioComponentData aSAudioComponentData)
{
	//Needs to be first
	if (myAudioSource)
	{
		AM.ReturnAudioSource(myAudioSource);
	}

	myAudioSource = AM.GetAudioSource();

	SetPosition(aSAudioComponentData.myPosition);
}

void CAudioSourceComponent::Update()
{
	#ifndef _RETAIL
		IWorld::DrawDebugWireSphere(myAudioSource->GetPosition(), CommonUtilities::Vector3f(myRange, myRange, myRange));
	#endif // !_RETAIL
}

void CAudioSourceComponent::SetPosition(const CommonUtilities::Vector3f& aPosition)
{
	myAudioSource->SetPosition(aPosition);
}

void CAudioSourceComponent::LoadAudioEvent(const char * aAudioName, bool aRemoveAfterPlayback, AudioChannel aAudioChannel)
{
	myAudioSource->LoadAudioEvent(aAudioName, aRemoveAfterPlayback, aAudioChannel);
}

void CAudioSourceComponent::Stop(const char * aAudioName, bool aShouldBeImmediate)
{
	myAudioSource->Stop(aAudioName, aShouldBeImmediate);
}

void CAudioSourceComponent::Play(const char * aAudioName)
{
	myAudioSource->Play(aAudioName);
}

void CAudioSourceComponent::PlayNewInstance(const char * aAudioName, AudioChannel aAudioChannel)
{
	myAudioSource->PlayNewInstance(aAudioName, aAudioChannel);
}

void CAudioSourceComponent::UnloadAudioEvent()
{
	myAudioSource->UnLoadAudioSource();
}


#include "stdafx.h"
#include "C2DRenderer.h"

#include "IEngine.h"
#include "ShaderManager.h"
#include "SpriteManager.h"
#include "RenderCommand.h"
#include "TextManager.h"

#include "DDSTextureLoader.h"
#include "TextureManager.h"

C2DRenderer::C2DRenderer()
{
}

C2DRenderer::~C2DRenderer()
{
}

bool C2DRenderer::Init()
{
	if (!InitBuffers())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to load buffers for C2DRenderer!");
		return false;
	}

	mySpriteVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/Sprite/Sprite.vs", EShaderInputLayoutType_Sprite);
	mySpritePixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/Sprite/Sprite.ps");
	myTextVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/Sprite/Text.vs", EShaderInputLayoutType_Sprite);
	myTextPixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/Sprite/Text.ps");

	myCanvasScreenSpaceVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/PPFX/PPFX.vs", EShaderInputLayoutType_PPFX);
	myCanvasWorldSpaceVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/Sprite/Canvas.vs", EShaderInputLayoutType_PPFX);
	myCopyPixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/PPFX/Copy.ps");

	return true;
}

void C2DRenderer::RenderUIOnCanvas(const CommonUtilities::GrowingArray<SCanvasRenderCommand>& aCanvasRenderCommands)
{
	for (auto& command : aCanvasRenderCommands)
	{
		CCanvasComponent* canvas = IEngine::GetComponentSystem().GetComponent<CCanvasComponent>(command.myCanvasComponentID.val, IEngine::GetSceneManager().GetActiveScene()->mySceneID);
		
		canvas->myCanvas.ClearTexture({1.f, 1.f, 1.f, 0.f});
		canvas->myCanvas.SetAsActiveTarget();

		RenderSprites(canvas->GetSpriteReadBuffer());
		RenderText(canvas->GetTextReadBuffer());
	}
}

void C2DRenderer::RenderWorldSpaceCanvases(CConstantBuffer& aCameraBuffer, const CommonUtilities::GrowingArray<SCanvasRenderCommand>& aCanvasRenderCommands)
{
	myCanvasWorldSpaceVertexShader->Bind();
	myCanvasWorldSpaceVertexShader->BindLayout();
	myCopyPixelShader->Bind();

	ID3D11DeviceContext* context = IEngine::GetContext();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);
	aCameraBuffer.SetBuffer(0, EShaderType_Pixel);
	myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

	SInstanceBufferData instanceData;

	for (auto& command : aCanvasRenderCommands)
	{
		CCanvasComponent* canvas = IEngine::GetComponentSystem().GetComponent<CCanvasComponent>(command.myCanvasComponentID.val, IEngine::GetSceneManager().GetActiveScene()->mySceneID);

		if (canvas->myRenderMode != CCanvasComponent::ERenderMode_WorldSpace)
		{
			continue;
		}

		canvas->myCanvas.SetAsResourceOnSlot(0);

		instanceData.myToWorld = IEngine::GetComponentSystem().GetGameObjectData(canvas->myGameObjectDataID, canvas->mySceneID)->GetTransform().GetMatrix();
		instanceData.myScale = {
			canvas->myCanvas.GetSize().x / IEngine::GetCanvasSize().x,
			canvas->myCanvas.GetSize().y / IEngine::GetCanvasSize().y
		};
		instanceData.myScale.x *= IEngine::GetCanvasRatio();
		myInstanceBuffer.SetData(&instanceData);
		
		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		context->IASetVertexBuffers(0, 1, &myQuad.myVertexBuffer, &myQuad.myStride, &myQuad.myOffset);
		context->IASetIndexBuffer(myQuad.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		context->DrawIndexed(myQuad.myNumberOfIndices, 0, 0);
	}

	myCanvasScreenSpaceVertexShader->Unbind();
	myCanvasScreenSpaceVertexShader->UnbindLayout();
	myCopyPixelShader->Unbind();
}

void C2DRenderer::RenderScreenSpaceCanvases(const CommonUtilities::GrowingArray<SCanvasRenderCommand>& aCanvasRenderCommands)
{
	myCanvasScreenSpaceVertexShader->Bind();
	myCanvasScreenSpaceVertexShader->BindLayout();
	myCopyPixelShader->Bind();

	ID3D11DeviceContext* context = IEngine::GetContext();

	for (auto& command : aCanvasRenderCommands)
	{
		CCanvasComponent* canvas = IEngine::GetComponentSystem().GetComponent<CCanvasComponent>(command.myCanvasComponentID.val, IEngine::GetSceneManager().GetActiveScene()->mySceneID);

		if (canvas->myRenderMode != CCanvasComponent::ERenderMode_ScreenSpace)
		{
			continue;
		}

		canvas->myCanvas.SetAsResourceOnSlot(0);

		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
		context->IASetVertexBuffers(0, 1, &myQuad.myVertexBuffer, &myQuad.myStride, &myQuad.myOffset);
		context->IASetIndexBuffer(myQuad.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		context->DrawIndexed(myQuad.myNumberOfIndices, 0, 0);
	}

	myCanvasScreenSpaceVertexShader->Unbind();
	myCanvasScreenSpaceVertexShader->UnbindLayout();
	myCopyPixelShader->Unbind();
}

void C2DRenderer::RenderSprites(const CommonUtilities::GrowingArray<SSpriteRenderCommand>& aSpritesToRender)
{
	ID3D11DeviceContext* context = IEngine::GetContext();
	CTextureManager& textureManager = IEngine::GetTextureManager();
	CSpriteManager& spriteManager = IEngine::GetSpriteManager();

	mySpriteVertexShader->Bind();
	mySpriteVertexShader->BindLayout();
	mySpritePixelShader->Bind();
	
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	context->IASetVertexBuffers(0, 1, &myQuad.myVertexBuffer, &myQuad.myStride, &myQuad.myOffset);
	context->IASetIndexBuffer(myQuad.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	for (const SSpriteRenderCommand& command : aSpritesToRender)
	{
		CSprite* sprite = spriteManager.GetSprite(command.mySpriteID);

		ID3D11ShaderResourceView* texture = textureManager.GetTexture(sprite->myTexture);

		context->PSSetShaderResources(0, 1, &texture);
		SQuadBufferData data;
		data.myScreenSize = IEngine::GetCanvasSize();
		data.mySpriteSize = command.mySpriteData.myOriginalTextureSize;

		data.myPosition = command.mySpriteData.myPosition;
		data.myScale = command.mySpriteData.myScale;
		data.myPivot = command.mySpriteData.myPivot;
		data.myRotation = command.mySpriteData.myRotation;
		data.myUVOffset = command.mySpriteData.myUVOffset;
		data.myUVScale = command.mySpriteData.myUVScale;
		data.myTint = command.mySpriteData.myTint;

		myQuadBuffer.SetData(&data);
		myQuadBuffer.SetBuffer(0, EShaderType_Vertex);
		myQuadBuffer.SetBuffer(0, EShaderType_Pixel);

		context->DrawIndexed(myQuad.myNumberOfIndices, 0, 0);
	}

	mySpriteVertexShader->Unbind();
	mySpriteVertexShader->UnbindLayout();
	mySpritePixelShader->Unbind();
}

void C2DRenderer::RenderText(const CommonUtilities::GrowingArray<STextRenderCommand>& aTextToRender)
{
	ID3D11DeviceContext* context = IEngine::GetContext();
	CTextureManager& textureManager = IEngine::GetTextureManager();
	CTextManager& textManager = IEngine::GetTextManager();

	myTextVertexShader->Bind();
	myTextVertexShader->BindLayout();
	myTextPixelShader->Bind();

	for (const STextRenderCommand& command : aTextToRender)
	{
		ID3D11ShaderResourceView* texture = textureManager.GetTexture(textManager.GetFont(command.myFontID)->myTexture);

		context->PSSetShaderResources(0, 1, &texture);

		STextQuadBufferData data;
		data.myScreenSize = IEngine::GetCanvasSize();
		data.mySpriteSize = command.mySpriteData.myOriginalTextureSize;

		data.myPosition = command.mySpriteData.myPosition + command.myPosition;
		data.myScale = command.mySpriteData.myScale;
		data.myPivot = command.mySpriteData.myPivot;
		data.myRotation = command.mySpriteData.myRotation;
		data.myUVOffset = command.mySpriteData.myUVOffset;
		data.myUVScale = command.mySpriteData.myUVScale;
		data.myTint = command.mySpriteData.myTint;
		data.myOutline = command.myOutline;

		myTextQuadBuffer.SetData(&data);
		myTextQuadBuffer.SetBuffer(0, EShaderType_Vertex);
		myTextQuadBuffer.SetBuffer(0, EShaderType_Pixel);

		context->DrawIndexed(myQuad.myNumberOfIndices, 0, 0);
	}

	myTextVertexShader->Unbind();
	myTextVertexShader->UnbindLayout();
	myTextPixelShader->Unbind();
}

bool C2DRenderer::InitBuffers()
{
	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
		float u, v;
	} vertices[4] =
	{
		{-1.f, +1.f, 0.f, 1.f,	0.f, 0.f},
		{+1.f, +1.f, 0.f, 1.f,	1.f, 0.f},
		{-1.f, -1.f, 0.f, 1.f,	0.f, 1.f},
		{+1.f, -1.f, 0.f, 1.f,	1.f, 1.f}
	};
	unsigned int indices[6] =
	{
		0,1,2,
		1,3,2
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = IEngine::GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		return false;
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = IEngine::GetDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		return false;
	}

	//Setup VertexData
	myQuad.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	myQuad.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	myQuad.myStride = sizeof(Vertex);
	myQuad.myOffset = 0;
	myQuad.myVertexBuffer = vertexBuffer;
	myQuad.myIndexBuffer = indexBuffer;

	//Setup constant buffer
	if (!myInstanceBuffer.Init(sizeof(SInstanceBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create Constant buffer: %s!", "SInstanceBufferData");
		return false;
	}
	if (!myQuadBuffer.Init(sizeof(SQuadBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create Constant buffer: %s!", "SQuadBufferData");
		return false;
	}
	if (!myTextQuadBuffer.Init(sizeof(STextQuadBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create Constant buffer: %s!", "STextQuadBufferData");
		return false;
	}

	return true;
}


#include "stdafx.h"
#include "Camera.h"

#include "IEngine.h"

CCamera::CCamera()
{
}


CCamera::~CCamera()
{
}

bool CCamera::Init(const CommonUtilities::Matrix44f & aProjectionMatrix, float aProjectionDepth)
{
	myProjection = aProjectionMatrix;
	myProjectionDepth = aProjectionDepth;

	return true;
}

void CCamera::SetFov(float aFov)
{
	if (aFov < 60.f)
	{
		ENGINE_LOG(CONCOL_ERROR, "You're camera fov is very small: %f degrees!", aFov);
	}
	else if (aFov >= 180.f)
	{
		ENGINE_LOG(CONCOL_ERROR, "You're camera fov is invalid: %f degrees!", aFov);
	}

	float aspectRatio = IEngine::GetCanvasSize().x / IEngine::GetCanvasSize().y;
	float n = 0.1f;
	float f = 1000.f;

	float B = 1.f / std::tan(aFov * CommonUtilities::Deg2Rad * 0.5f);
	float A = B / aspectRatio;
	float C = f / (f - n);
	float D = 1.f;
	float E = -n * f / (f - n);

	CommonUtilities::Matrix44<float> projectionMatrix(
		A, 0, 0, 0,
		0, B, 0, 0,
		0, 0, C, D,
		0, 0, E, 0);

	myProjection = projectionMatrix;
	myProjectionDepth = f - n;
}

#pragma once
#include "Matrix.h"

struct SCameraBufferData
{
	CommonUtilities::Matrix44f myCameraOrientation;
	CommonUtilities::Matrix44f myToCamera;
	CommonUtilities::Matrix44f myProjection;
	CommonUtilities::Matrix44f myInvertedProjection;
	CommonUtilities::Matrix44f myViewProjection;
	CommonUtilities::Matrix44f myInvertedViewProjection;
};
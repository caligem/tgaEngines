#include "stdafx.h"
#include "CanvasComponent.h"

CCanvasComponent::CCanvasComponent()
{
}


CCanvasComponent::~CCanvasComponent()
{
}

void CCanvasComponent::Init(const SComponentData & aComponentData)
{
	myUIElements.Init(8);

	if ((aComponentData.mySize.x <= 0.f) || (aComponentData.mySize.y <= 0.f))
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to init Canvas component with < 0.f size");
		return;
	}

	if (aComponentData.myRenderMode == ERenderMode_WorldSpace)
	{
		myCanvas.Init(aComponentData.mySize);
	}
	else if (aComponentData.myRenderMode == ERenderMode_ScreenSpace)
	{
		myCanvas.Init(IEngine::GetCanvasSize());
	}

	mySpriteRenderCommands.Init();
	myTextRenderCommands.Init();

	myRenderMode = aComponentData.myRenderMode;
}

void CCanvasComponent::Update()
{
	Input::CInputManager& input = IEngine::GetInputManager();

	CommonUtilities::Matrix44f viewProjInv = CommonUtilities::Matrix44f::Inverse(IEngine::GetGraphicsPipeline().GetSavedCameraBuffer().myViewProjection);

	CommonUtilities::Vector2f mousePos = {
		input.GetMousePosition().x / IEngine::GetWindowSize().x,
		input.GetMousePosition().y / IEngine::GetWindowSize().y
	};
	mousePos.x = mousePos.x * 2.f - 1.f;
	mousePos.y = (1.f - mousePos.y) * 2.f - 1.f;

	CommonUtilities::Vector4f rayOrigin = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 0.f, 1.f) * viewProjInv;
	rayOrigin /= rayOrigin.w;
	CommonUtilities::Vector4f rayEnd = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 1.f, 1.f) * viewProjInv;
	rayEnd /= rayEnd.w;

	CommonUtilities::Vector3f rayDir = rayEnd - rayOrigin;
	rayDir.Normalize();

	CommonUtilities::Vector3f normal = myGameObjectData->GetTransform().GetForward().GetNormalized();

	float denom = normal.Dot(rayDir);
	if (denom > 1e-6)
	{
		CommonUtilities::Vector3f p0l0 = myGameObjectData->GetTransform().GetPosition() - CommonUtilities::Vector3f(rayOrigin);

		float t = p0l0.Dot(normal) / denom;

		CommonUtilities::Vector3f intersection = CommonUtilities::Vector3f(rayOrigin) + rayDir*t;
		intersection -= myGameObjectData->GetTransform().GetPosition();

		intersection = {
			intersection.Dot(myGameObjectData->GetTransform().GetRight()),
			intersection.Dot(myGameObjectData->GetTransform().GetUp()),
			intersection.Dot(myGameObjectData->GetTransform().GetForward())
		};

		intersection.x /= IEngine::GetCanvasSize().x / IEngine::GetCanvasSize().y;

		myIntersection.x = (intersection.x + 1.f) / 2.f;
		myIntersection.y = 1.f - ((intersection.y + 1.f) / 2.f);
	}
}

void CCanvasComponent::FillRenderCommands()
{
	for (auto uiElement : myUIElements)
	{
		uiElement->FillRenderCommands(mySpriteRenderCommands, myTextRenderCommands);
	}
}

void CCanvasComponent::SwapBuffers()
{
	myTextRenderCommands.SwapBuffers();
	mySpriteRenderCommands.SwapBuffers();
}

#pragma once
#include "Component.h"
#include "FullscreenTexture.h"
#include "DoubleBuffer.h"

class CComponentSystem;
class CUIElement;
class CGraphicsPipeline;
class C2DRenderer;

class CCanvasComponent : public CComponent
{
public:
	enum ERenderMode
	{
		ERenderMode_ScreenSpace,
		ERenderMode_WorldSpace,
		ERenderMode_Count
	};

	struct SComponentData
	{
		ERenderMode myRenderMode;
		CommonUtilities::Vector2f mySize;
	};

	CCanvasComponent();
	~CCanvasComponent();

	template<typename T>
	T* AddUIElement(const typename T::SComponentData& aComponentData);

	void SetRenderMode(ERenderMode aRenderMode) { myRenderMode = aRenderMode; }
	ERenderMode GetRenderMode() const { return myRenderMode; }

	bool IsMouseInside() const { return myIntersection.x >= 0.f && myIntersection.x <= 1.f && myIntersection.y >= 0.f && myIntersection.y <= 1.f; } 
	const CommonUtilities::Vector2f& GetMousePoint() const { return myIntersection; }

private:
	friend CComponentSystem;
	friend CGraphicsPipeline;
	friend C2DRenderer;

	void Init(const SComponentData& aComponentData);
	void Update();

	void FillRenderCommands();
	void SwapBuffers();

	const CommonUtilities::GrowingArray<SSpriteRenderCommand>& GetSpriteReadBuffer() { return mySpriteRenderCommands.GetReadBuffer(); }
	const CommonUtilities::GrowingArray<STextRenderCommand>& GetTextReadBuffer() { return myTextRenderCommands.GetReadBuffer(); }

	CDoubleBuffer<SSpriteRenderCommand> mySpriteRenderCommands;
	CDoubleBuffer<STextRenderCommand> myTextRenderCommands;

	CFullscreenTexture myCanvas;
	ERenderMode myRenderMode;
	CommonUtilities::GrowingArray<CUIElement*> myUIElements;

	CommonUtilities::Vector2f myIntersection;
};

template<typename T>
inline T* CCanvasComponent::AddUIElement(const typename T::SComponentData & aComponentData)
{
	CGameObject temp;
	temp.Init(mySceneID, myGameObjectDataID);
	T* newUIElement = temp.AddComponent<T>(aComponentData);
	myUIElements.Add(newUIElement);
	return newUIElement;
}

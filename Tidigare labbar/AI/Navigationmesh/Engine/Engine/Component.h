#pragma once
#include <ObjectPool.h>

class CGameObjectData;
class CForwardRenderer;
class CEngine;
class CComponentSystem;
class CGraphicsPipeline;
class CScene;

class CComponent
{
public:
	CComponent();
	virtual	~CComponent();

	void SetParent(ID_T(CGameObjectData) aParentID, ID_T(CScene) aSceneID, int aID, CGameObjectData* aGameObjectData);

protected:
	virtual void Release(){}

	CGameObjectData* myGameObjectData = nullptr;
	ID_T(CGameObjectData) myGameObjectDataID;
	ID_T(CScene) mySceneID;
	int myID;

private:
	friend CForwardRenderer;
	friend CComponentSystem;
	friend CGraphicsPipeline;

};
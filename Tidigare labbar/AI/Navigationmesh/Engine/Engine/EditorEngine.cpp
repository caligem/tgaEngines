#include "stdafx.h"
#include "EditorEngine.h"

#include "ImGuizmo.h"
#include "imgui.h"
#include "imgui_impl_dx11.h"

#include "IWorld.h"


CEditorEngine::CEditorEngine()
{
		GENERAL_LOG(CONCOL_DEFAULT, "Handler set to null!");
	myManipulationToolHandler = nullptr;
}
CEditorEngine::~CEditorEngine()
{
	ImGui_ImplDX11_Shutdown();
}
bool CEditorEngine::Init(const SCreateParameters & aCreateParameters)
{
	mySyncFunctionBuffer.Init(8);
	if (!CEngine::Init(aCreateParameters))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to init");
		return false;
	}
	myWorkerPool.Destroy();
	myWorkerPool.Init(1);

	if (!ImGui_ImplDX11_Init(myWindowHandler.GetWindowHandle(), myFramework.GetDevice(), myFramework.GetContext()))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to init ImGui");
		return false;
	}

	return true;
}
void CEditorEngine::SyncJob()
{
	for (unsigned short i = 0; i < mySyncFunctionBuffer.Size(); ++i)
	{
		mySyncFunctionBuffer[i]();
	}

	mySyncFunctionBuffer.RemoveAll();

	if(myMessageHandler)myMessageHandler();

	CEngine::SyncJob();
}

void CEditorEngine::RenderJob()
{
	myGraphicsPipeline.BeginFrame();
	ImGui_ImplDX11_NewFrame(IWorld::GetCanvasSize().x, IWorld::GetCanvasSize().y);
	myGraphicsPipeline.Render();
	ImGuiRenderJob();
	ImGui::Render();
	myFramework.Present();
}

void CEditorEngine::ImGuiRenderJob()
{
	ImGuizmo::BeginFrame();
	
	ImGuizmo::Enable(true);

	ImGuizmo::SetRect(0, 0, IWorld::GetCanvasSize().x, IWorld::GetCanvasSize().y, IWorld::GetWindowSize().x, IWorld::GetWindowSize().y);

	if (myManipulationToolHandler != nullptr)
	{
		myManipulationToolHandler();
	}
}

HWND CEditorEngine::GetWindowHandle()
{
	return myWindowHandler.GetWindowHandle();
}

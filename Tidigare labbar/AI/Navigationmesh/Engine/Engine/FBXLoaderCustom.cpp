#include "stdafx.h"
#include "FBXLoaderCustom.h"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>


#include <windows.h>
#include <fstream>

#pragma comment (lib, "assimp-vc140-mt.lib")

#define TEXTURE_SET_0 0

CFBXLoaderCustom::CFBXLoaderCustom()
{
}


CFBXLoaderCustom::~CFBXLoaderCustom() 
{
}

Matrix44f ConvertToEngineMatrix33(const aiMatrix3x3& AssimpMatrix)
{

	Matrix44f mat;
	mat.myMatrix[0][0] = AssimpMatrix.a1;	mat.myMatrix[0][1] = AssimpMatrix.a2;	mat.myMatrix[0][2] = AssimpMatrix.a3;	mat.myMatrix[0][3] = 0.0f;
	mat.myMatrix[1][0] = AssimpMatrix.b1;	mat.myMatrix[1][1] = AssimpMatrix.b2;	mat.myMatrix[1][2] = AssimpMatrix.b3;	mat.myMatrix[1][3] = 0.0f;
	mat.myMatrix[2][0] = AssimpMatrix.c1;	mat.myMatrix[2][1] = AssimpMatrix.c2;	mat.myMatrix[2][2] = AssimpMatrix.c3;	mat.myMatrix[2][3] = 0.0f;
	mat.myMatrix[3][0] = 0.0f;				mat.myMatrix[3][1] = 0.0f;				mat.myMatrix[3][2] = 0.0f;				mat.myMatrix[3][3] = 1.0f;
	return mat;
}

// constructor from Assimp matrix
Matrix44f ConvertToEngineMatrix44(const aiMatrix4x4& AssimpMatrix)
{
	Matrix44f mat;
	mat.myMatrix[0][0] = AssimpMatrix.a1; mat.myMatrix[0][1] = AssimpMatrix.a2; mat.myMatrix[0][2] = AssimpMatrix.a3; mat.myMatrix[0][3] = AssimpMatrix.a4;
	mat.myMatrix[1][0] = AssimpMatrix.b1; mat.myMatrix[1][1] = AssimpMatrix.b2; mat.myMatrix[1][2] = AssimpMatrix.b3; mat.myMatrix[1][3] = AssimpMatrix.b4;
	mat.myMatrix[2][0] = AssimpMatrix.c1; mat.myMatrix[2][1] = AssimpMatrix.c2; mat.myMatrix[2][2] = AssimpMatrix.c3; mat.myMatrix[2][3] = AssimpMatrix.c4;
	mat.myMatrix[3][0] = AssimpMatrix.d1; mat.myMatrix[3][1] = AssimpMatrix.d2; mat.myMatrix[3][2] = AssimpMatrix.d3; mat.myMatrix[3][3] = AssimpMatrix.d4;
	return mat;
}

int CFBXLoaderCustom::DetermineAndLoadVerticies(aiMesh* fbxMesh, CLoaderMesh* aLoaderMesh)
{
	unsigned int modelBluePrintType = 0;

	modelBluePrintType |= (fbxMesh->HasPositions() ? EModelBluePrint_Position : 0);
	modelBluePrintType |= (fbxMesh->HasTextureCoords(0) ? EModelBluePrint_UV : 0);
	modelBluePrintType |= (fbxMesh->HasNormals() ? EModelBluePrint_Normal : 0);
	modelBluePrintType |= (fbxMesh->HasTangentsAndBitangents() ? EModelBluePrint_BinormTan : 0);
	modelBluePrintType |= (fbxMesh->HasBones() ? EModelBluePrint_Bones : 0);

	int vertexBufferSize = 0;
	vertexBufferSize += (fbxMesh->HasPositions() ? sizeof(float) * 4 : 0);
	vertexBufferSize += (fbxMesh->HasTextureCoords(0) ? sizeof(float) * 2 : 0);
	vertexBufferSize += (fbxMesh->HasNormals() ? sizeof(float) * 4 : 0);
	vertexBufferSize += (fbxMesh->HasTangentsAndBitangents() ? sizeof(float) * 8 : 0);
	vertexBufferSize += (fbxMesh->HasBones() ? sizeof(float) * 8 : 0); // Better with an UINT, but this works

	aLoaderMesh->myShaderType = modelBluePrintType;
	aLoaderMesh->myVertexBufferSize = vertexBufferSize;

	aLoaderMesh->myVerticies = new char[vertexBufferSize * fbxMesh->mNumVertices];
	aLoaderMesh->myVertexCount = fbxMesh->mNumVertices;

	std::vector<VertexBoneData> collectedBoneData;
	if (fbxMesh->HasBones())
	{
		collectedBoneData.resize(fbxMesh->mNumVertices);

		unsigned int BoneIndex = 0; 
		for (unsigned int i = 0; i < fbxMesh->mNumBones; i++) 
		{
			std::string BoneName(fbxMesh->mBones[i]->mName.data);
			if (aLoaderMesh->myModel->myBoneNameToIndex.find(BoneName) == aLoaderMesh->myModel->myBoneNameToIndex.end())
			{
				BoneIndex = aLoaderMesh->myModel->myNumBones;
				aLoaderMesh->myModel->myNumBones++;
				BoneInfo bi;
				aLoaderMesh->myModel->myBoneInfo.push_back(bi);


				Matrix44f NodeTransformation = ConvertToEngineMatrix44(fbxMesh->mBones[i]->mOffsetMatrix);

				aLoaderMesh->myModel->myBoneInfo[BoneIndex].BoneOffset = NodeTransformation;
				aLoaderMesh->myModel->myBoneNameToIndex[BoneName] = BoneIndex;
			}
			else 
			{
				BoneIndex = aLoaderMesh->myModel->myBoneNameToIndex[BoneName];
			}

			for (unsigned int j = 0; j < fbxMesh->mBones[i]->mNumWeights; j++) 
			{
				unsigned int VertexID = fbxMesh->mBones[i]->mWeights[j].mVertexId;
				float Weight = fbxMesh->mBones[i]->mWeights[j].mWeight;
				collectedBoneData[VertexID].AddBoneData(BoneIndex, Weight);
			}
		}
	}

	float* vertexCollection = new float[(vertexBufferSize / sizeof(float)) * fbxMesh->mNumVertices];
	unsigned int index = 0;
	for (unsigned int i = 0; i < fbxMesh->mNumVertices; i++)
	{
		if (fbxMesh->HasPositions())
		{
			aiVector3D& mVertice = fbxMesh->mVertices[i];
			vertexCollection[index+0] = mVertice.x;
			vertexCollection[index+1] = mVertice.y;
			vertexCollection[index+2] = mVertice.z;
			vertexCollection[index+3] = 1.f;
			index += 4;
			aLoaderMesh->myRadius = std::fmaxf(aLoaderMesh->myRadius, mVertice.Length());
		}
		if (fbxMesh->HasNormals())
		{
			aiVector3D& mNorm = fbxMesh->mNormals[i];
			vertexCollection[index+0] = mNorm.x;
			vertexCollection[index+1] = mNorm.y;
			vertexCollection[index+2] = mNorm.z;
			vertexCollection[index+3] = 0.f;
			index += 4;
		}
		if (fbxMesh->HasTangentsAndBitangents())
		{
			aiVector3D& mTangent = fbxMesh->mTangents[i];
			aiVector3D& biTangent = fbxMesh->mBitangents[i];

			vertexCollection[index+0] = mTangent.x;
			vertexCollection[index+1] = mTangent.y;
			vertexCollection[index+2] = mTangent.z;
			vertexCollection[index+3] = 0.f;
			index += 4;

			vertexCollection[index+0] = biTangent.x;
			vertexCollection[index+1] = biTangent.y;
			vertexCollection[index+2] = biTangent.z;
			vertexCollection[index+3] = 0.f;
			index += 4;
		}
		if (fbxMesh->HasTextureCoords(TEXTURE_SET_0))
		{
			vertexCollection[index+0] = fbxMesh->mTextureCoords[TEXTURE_SET_0][i].x;
			vertexCollection[index+1] = fbxMesh->mTextureCoords[TEXTURE_SET_0][i].y;
			index += 2;
		}
		if (fbxMesh->HasBones())
		{
			VertexBoneData& boneData = collectedBoneData[i];

			vertexCollection[index+0] = (float)boneData.IDs[0];
			vertexCollection[index+1] = (float)boneData.IDs[1];
			vertexCollection[index+2] = (float)boneData.IDs[2];
			vertexCollection[index+3] = (float)boneData.IDs[3];
			index += 4;

			vertexCollection[index+0] = (float)boneData.Weights[0];
			vertexCollection[index+1] = (float)boneData.Weights[1];
			vertexCollection[index+2] = (float)boneData.Weights[2];
			vertexCollection[index+3] = (float)boneData.Weights[3];
			index += 4;
		}
	}

	memcpy(aLoaderMesh->myVerticies, vertexCollection, vertexBufferSize * fbxMesh->mNumVertices);

	delete vertexCollection;

	return vertexBufferSize;
}

CLoaderModel* CFBXLoaderCustom::LoadModel(const char* aModel)
{
	CLoaderModel* newModel = new CLoaderModel();
	newModel->SetData(aModel);

	if (!LoadModelInternal(newModel))
	{
		delete newModel;
		newModel = nullptr;
	}

	return newModel;
}

bool does_file_exist(const char *fileName)
{
	std::ifstream infile(fileName);
	return infile.good();
}

void* CFBXLoaderCustom::LoadModelInternal(CLoaderModel* someInput)
{
	CLoaderModel* model = someInput;
	const struct aiScene* scene = NULL;

	//TODO: Fix all the OutputDebugStrings in this loader
	if (!does_file_exist(model->myModelPath.c_str()))
	{
		return nullptr;
	}
	scene = aiImportFile(model->myModelPath.c_str(), aiProcessPreset_TargetRealtime_MaxQuality | aiProcess_FlipUVs);

	//OutputDebugStringA(model->myModelPath.c_str());
	
	if (!scene)
	{
		OutputDebugStringA(aiGetErrorString());
		return nullptr;
	}

	model->myScene = scene;

	if (scene->mRootNode->mNumChildren < 1)
	{
		return nullptr;
	}

	for (unsigned int n = 0; n < scene->mNumMeshes; ++n)
	{
		CLoaderMesh* mesh = model->CreateMesh();

		aiMesh* fbxMesh = scene->mMeshes[n];

		DetermineAndLoadVerticies(fbxMesh, mesh);

		for (unsigned int i = 0; i < fbxMesh->mNumFaces; i++)
		{
			for (uint j = 0; j < fbxMesh->mFaces[i].mNumIndices; j++)
			{
				mesh->myIndexes.push_back(fbxMesh->mFaces[i].mIndices[j]);
			}
		}

		if (n < scene->mRootNode->mChildren[0]->mNumChildren)
		{
			aiNode* node = scene->mRootNode->mChildren[0]->mChildren[n];

			if (node->mMetaData != nullptr)
			{
				for (unsigned int metaIndex = 0; metaIndex < node->mMetaData->mNumProperties; metaIndex++)
				{
					if (std::string(node->mMetaData->mKeys[metaIndex].C_Str()) == "LOD_ViewingDistance")
					{
						float distance = 0.0f;
						if (node->mMetaData->Get<float>(metaIndex, distance))
						{
							mesh->myLODDistance = distance;
						}
					}
				}
			}
		}
	}

	// CHange to support multiple animations
	if (scene->mNumAnimations > 0)
	{
		model->myAnimationDuration = (float)scene->mAnimations[0]->mDuration;
	}
	
	LoadMaterials(scene, model);

	model->myGlobalInverseTransform = ConvertToEngineMatrix44(scene->mRootNode->mTransformation);

	return model;
}

void CFBXLoaderCustom::LoadMaterials(const struct aiScene *sc, CLoaderModel* aModel)
{
	for (unsigned int m = 0; m < sc->mNumMaterials; m++)
	{
		LoadTexture(aiTextureType_DIFFUSE, aModel->myTextures, sc->mMaterials[m]); // TEXTURE_DEFINITION_ALBEDO
		LoadTexture(aiTextureType_SPECULAR, aModel->myTextures, sc->mMaterials[m]); // TEXTURE_DEFINITION_ROUGHNESS
		LoadTexture(aiTextureType_AMBIENT, aModel->myTextures, sc->mMaterials[m]); // TEXTURE_DEFINITION_AMBIENTOCCLUSION
		LoadTexture(aiTextureType_NORMALS, aModel->myTextures, sc->mMaterials[m]); // TEXTURE_DEFINITION_NORMAL
		LoadTexture(aiTextureType_REFLECTION, aModel->myTextures, sc->mMaterials[m]); // TEXTURE_DEFINITION_METALNESS
		LoadTexture(aiTextureType_EMISSIVE, aModel->myTextures, sc->mMaterials[m]); // TEXTURE_DEFINITION_EMISSIVE
		//LoadTexture(aiTextureType_HEIGHT, aModel->myTextures, sc->mMaterials[m]);
		//LoadTexture(aiTextureType_SHININESS, aModel->myTextures, sc->mMaterials[m]);
		//LoadTexture(aiTextureType_OPACITY, aModel->myTextures, sc->mMaterials[m]);
		//LoadTexture(aiTextureType_DISPLACEMENT, aModel->myTextures, sc->mMaterials[m]);
		//LoadTexture(aiTextureType_LIGHTMAP, aModel->myTextures, sc->mMaterials[m]);
	}
}

void CFBXLoaderCustom::LoadTexture(int aType, std::vector<std::string>& someTextures, aiMaterial* aMaterial)
{
	int texIndex = 0;
	aiReturn texFound = AI_SUCCESS;

	aiString path;	// filename

	texFound = aMaterial->GetTexture((aiTextureType)aType, texIndex, &path);
	if (texFound == AI_FAILURE)
	{
		someTextures.push_back("");
		return;
	}

	std::string filePath = std::string(path.data);

	const size_t last_slash_idx = filePath.find_last_of("\\/");
	if (std::string::npos != last_slash_idx)
	{
		filePath.erase(0, last_slash_idx + 1);
	}

	someTextures.push_back(filePath);
}

CLoaderModel::~CLoaderModel()
{
	if (myScene)
	{
		aiReleaseImport(myScene);
	}
	for (CLoaderMesh* mesh : myMeshes)delete mesh;
}

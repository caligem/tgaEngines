#include "stdafx.h"
#include "ForwardRenderer.h"
#include "DirectXFramework.h"
#include "RenderCommand.h"
#include "ModelManager.h"
#include "IEngine.h"
#include "Scene.h"
#include "SceneManager.h"
#include "ShaderManager.h"

#include "Shader.h"

CForwardRenderer::CForwardRenderer()
{
	myVertexShader = nullptr;
	myPixelShader = nullptr;
}

CForwardRenderer::~CForwardRenderer()
{
}

bool CForwardRenderer::Init()
{
	if (!myInstanceBuffer.Init(sizeof(SInstanceBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create ForwardRenderer::myInstanceBuffer");
		return false;
	}

	if (!myDirectionalLightBuffer.Init(sizeof(SLightBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create ForwardRenderer::myDirectionalLightBuffer");
		return false;
	}
	if (!myPointLightBuffer.Init(sizeof(SPointLightBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create ForwardRenderer::myPointLightBuffer");
		return false;
	}

	myVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/Model/Model.vs", EShaderInputLayoutType_PBR);
	myPixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/Model/Model.ps");

	return true;
}

void CForwardRenderer::Render(const CommonUtilities::GrowingArray<SModelRenderCommand>& aRenderCommands, CConstantBuffer& aCameraBuffer, const CommonUtilities::GrowingArray<SPointLightRenderCommand>& aLightRenderCommands)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);

	SLightBufferData lightData;
	//TODO: Look at directional light and fix dis
	CScene* activeScene = IEngine::GetSceneManager().GetActiveScene();
	if (!activeScene)
	{
		return;
	}
	lightData.myDirectionalLight = activeScene->GetDirectionalLight().GetNormalized();
	lightData.myDirectionalLightColor = { 1.0f, 0.9f, 0.8f, 1.f };
	myDirectionalLightBuffer.SetData(&lightData);
	myDirectionalLightBuffer.SetBuffer(0, EShaderType_Pixel);

	SPointLightBufferData pointLightData;
	pointLightData.numberOfUsedPointLights = min(static_cast<unsigned int>(aLightRenderCommands.Size()), 8);

	if (!aLightRenderCommands.Empty())
	{
		int index = 0;
		for (const SPointLightRenderCommand& lightRenderCommand : aLightRenderCommands)
		{
			pointLightData.pointLights[index].color = lightRenderCommand.myColor;
			pointLightData.pointLights[index].position = lightRenderCommand.myPosition;
			pointLightData.pointLights[index].range = lightRenderCommand.myRange;
			index++;
			if (index >= 8)break;
		}
	}

	myPointLightBuffer.SetData(&pointLightData);
	myPointLightBuffer.SetBuffer(1, EShaderType_Pixel);

	ID3D11ShaderResourceView* cubemap = IEngine::GetTextureManager().GetTexture(IEngine::GetGraphicsPipeline().GetCubemap());

	SInstanceBufferData instanceData;

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	myVertexShader->Bind();
	myVertexShader->BindLayout();
	myPixelShader->Bind();

	for (const SModelRenderCommand& command : aRenderCommands)
	{
		CModel* model = IEngine::GetModelManager().GetModel(command.myModelToRender);

		if (!model)
		{
			continue;
		}

		const SVertexDataWrapper& vertexData = model->GetVertexData(0);
		STextureDataWrapper& textureData = model->GetTextureData();
		
		instanceData.myToWorld = command.myTransform.GetMatrix();

		myInstanceBuffer.SetData(&instanceData);

		context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
		context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

		ID3D11ShaderResourceView* textures[STextureDataWrapper::maxTextures];
		for (int i = 0; i < textureData.maxTextures; ++i)
		{
			textures[i] = IEngine::GetTextureManager().GetTexture(textureData.myTextures[i]);
		}
		context->PSSetShaderResources(0, STextureDataWrapper::maxTextures, textures);

		context->PSSetShaderResources(STextureDataWrapper::maxTextures, 1, &cubemap);

		context->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);
	}

	myVertexShader->Unbind();
	myVertexShader->UnbindLayout();
	myPixelShader->Unbind();
}

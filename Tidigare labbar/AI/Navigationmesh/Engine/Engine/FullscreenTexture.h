#pragma once

#include "Vector.h"
#include <dxgiformat.h>

struct ID3D11Texture2D;
struct ID3D11ShaderResourceView;
struct ID3D11RenderTargetView;
struct ID3D11DepthStencilView;
struct D3D11_VIEWPORT;

class CFullscreenTexture
{
public:
	CFullscreenTexture();
	~CFullscreenTexture();

	static constexpr int MaxSize = 8;

	bool Init(const CommonUtilities::Vector2f& aSize, int aNumberOfTextures, DXGI_FORMAT aFormats[MaxSize], bool aCreateDepth = false);
	bool Init(const CommonUtilities::Vector2f& aSize, DXGI_FORMAT aFormat = DXGI_FORMAT_R8G8B8A8_UNORM, bool aCreateDepth = false);
	bool Init(ID3D11Texture2D* aTexture, bool aCreateDepth = false);

	void ClearTexture(const CommonUtilities::Vector4f& aClearColor = { 0.f, 0.f, 0.f, 0.f });
	void ClearTextureAt(int aIndex, const CommonUtilities::Vector4f& aClearColor = { 0.f, 0.f, 0.f, 0.f });
	void SetAsActiveTarget(ID3D11DepthStencilView* aDepthStencil = NULL);
	void SetTextureAsActiveTarget(unsigned int aIndex, ID3D11DepthStencilView* aDepthStencil = NULL);
	void SetAsResourceOnSlot(unsigned int aSlot);
	void SetTextureAsResourceOnSlot(unsigned int aSlot, unsigned int aIndex = 0);
	const CommonUtilities::Vector2f GetSize() const { return mySize; }

	int GetNumTargets() const { return myNumTargets; }

	ID3D11DepthStencilView* GetDepthStencilView() { return myDepthStencil; }
	ID3D11ShaderResourceView* GetShaderResourceView(int aIndex = 0) { return myShaderResources[aIndex]; }
	ID3D11RenderTargetView* GetRenderTargetView(int aIndex = 0) { return myRenderTargets[aIndex]; }

private:
	ID3D11Texture2D* myTextures[MaxSize] = {0};
	ID3D11ShaderResourceView* myShaderResources[MaxSize] = {0};
	ID3D11RenderTargetView* myRenderTargets[MaxSize] = {0};
	ID3D11DepthStencilView* myDepthStencil;
	D3D11_VIEWPORT* myViewport;

	CommonUtilities::Vector2f mySize;
	int myNumTargets;

	bool myIsInitFromTexture = false;
};


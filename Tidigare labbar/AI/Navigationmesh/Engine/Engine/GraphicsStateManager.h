#pragma once

struct ID3D11DepthStencilState;
struct ID3D11BlendState;
struct ID3D11SamplerState;
struct ID3D11RasterizerState;

class CGraphicsStateManager
{
public:
	CGraphicsStateManager();
	~CGraphicsStateManager();

	enum EDepthStencilState
	{
		EDepthStencilState_Depth,
		EDepthStencilState_NoDepth,
		EDepthStencilState_Count
	};

	//AlphaBlend m�ste vara 1
	//Additive m�ste vara 2
	//annars breakar particle editorn
	enum EBlendState
	{
		EBlendState_Disabled,
		EBlendState_AlphaBlend,
		EBlendState_Additive,
		EBlendState_Count
	};
	enum ESamplerState
	{
		ESamplerState_Linear_Clamp,
		ESamplerState_Linear_Wrap,
		ESamplerState_Count
	};
	enum ERasterizerState
	{
		ERasterizerState_Solid,
		ERasterizerState_Solid_NoCulling,
		ERasterizerState_Wireframe,
		ERasterizerState_Count
	};

	bool Init();

	void SetDepthStencilState(EDepthStencilState aDepthStencilState);
	void SetBlendState(EBlendState aBlendState);
	void SetSamplerState(ESamplerState aSamplerState);
	void SetRasterizerState(ERasterizerState aRasterizerState);

private:
	bool CreateDepthStencilStates();
	bool CreateBlendStates();
	bool CreateSamplerStates();
	bool CreateRasterizerStates();

	ID3D11DepthStencilState* myDepthStencilStates[EDepthStencilState_Count];
	ID3D11BlendState* myBlendStates[EBlendState_Count];
	ID3D11SamplerState* mySamplerStates[ESamplerState_Count];
	ID3D11RasterizerState* myRasterizerStates[ERasterizerState_Count];

};


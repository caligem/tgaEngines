#include "stdafx.h"
#include "ModelManager.h"
#include "DirectXFramework.h"

CModelManager::CModelManager()
{
}


CModelManager::~CModelManager()
{
}

bool CModelManager::Init(CDirectXFramework& aDirectXFramework)
{
	if (!myModelLoader.Init(aDirectXFramework.GetDevice()))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: ModelLoader failed to initialize in ComponentSystem Init.");
		return false;
	}


	{
		ID_T(CModel) modelID = myModels.Acquire();
		myModelCache["cube"] = modelID;
		myModels.GetObj(modelID)->IncrementCounter();
		CModel::SModelData modelData;
		modelData = myModelLoader.LoadCube();
		myModels.GetObj(modelID)->Init(modelData);
		myModels.GetObj(modelID)->myMaterial.SetVertexShader(L"Assets/Shaders/Debug/Grid");
		myModels.GetObj(modelID)->myMaterial.SetPixelShader(L"Assets/Shaders/Debug/Grid");
	}

	{
		ID_T(CModel) modelID = myModels.Acquire();
		myModelCache["sphere"] = modelID;
		myModels.GetObj(modelID)->IncrementCounter();
		CModel::SModelData modelData;
		modelData = myModelLoader.CreateIcosphere(3, false);
		myModels.GetObj(modelID)->Init(modelData);
		myModels.GetObj(modelID)->myMaterial.SetVertexShader(L"Assets/Shaders/Debug/Grid");
		myModels.GetObj(modelID)->myMaterial.SetPixelShader(L"Assets/Shaders/Debug/Grid");
	}

	return true;
}

ID_T(CModel) CModelManager::AcquireModel(const char * aModelPath)
{
	std::string modelPath;

	if (aModelPath == nullptr)
	{
		modelPath = "";
	}
	else
	{
		modelPath = aModelPath;
	}

	if (myModelCache.find(modelPath) != myModelCache.end())
	{
		ID_T(CModel) id = myModelCache[modelPath];
		myModels.GetObj(id)->IncrementCounter();
		return id;
	}
	else
	{
		ID_T(CModel) modelID = myModels.Acquire();
		myModelCache[modelPath] = modelID;
		myModels.GetObj(modelID)->IncrementCounter();

		CModel::SModelData modelData;
		if (aModelPath == nullptr)
		{
			modelData = myModelLoader.LoadCube();
		}
		else
		{
			modelData = myModelLoader.LoadModel(modelPath.c_str());
		}
		myModels.GetObj(modelID)->Init(modelData);

		if (modelPath == "")
		{
			myModels.GetObj(modelID)->myMaterial.SetVertexShader(L"Assets/Shaders/Debug/Grid");
			myModels.GetObj(modelID)->myMaterial.SetPixelShader(L"Assets/Shaders/Debug/Grid");
		}

		return std::move(modelID);
	}
}

void CModelManager::ReleaseModel(ID_T(CModel) aModelID)
{
	for (auto it = myModelCache.begin(); it != myModelCache.end(); ++it)
	{
		if (it->second == aModelID)
		{
			myModels.GetObj(aModelID)->DecrementCounter();

			if (myModels.GetObj(aModelID)->GetRefCount() <= 0)
			{
				myModels.Release(aModelID);
				myModelCache.erase(it);
			}

			break;
		}
	}
}

CModel * CModelManager::GetModel(ID_T(CModel) aModelID)
{
	return std::move(myModels.GetObj(aModelID));
}

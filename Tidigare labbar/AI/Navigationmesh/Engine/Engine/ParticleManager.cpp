#include "stdafx.h"
#include "ParticleManager.h"
#include "JsonDocument.h"

CParticleManager::CParticleManager()
	: myStreaks(PoolSizeParticleManagerStreaks)
	, myParticleEmitters(PoolSizeParticleManagerParticleEmitters)
{
}


CParticleManager::~CParticleManager()
{
}

bool CParticleManager::Init()
{
	return true;
}

ID_T(CParticleEmitter) CParticleManager::AcquireParticleEmitter(const char * aFilePath, unsigned int aVertexBufferSize)
{
	return LoadParticleData(aFilePath, aVertexBufferSize);
}

ID_T(CParticleEmitter) CParticleManager::AcquireParticleEmitter(CParticleEmitter::SParticleData aParticleData, unsigned int aVertexBufferSize)
{
	ID_T(CParticleEmitter) id = myParticleEmitters.Acquire();
	myParticleEmitters.GetObj(id)->Init(aParticleData, aVertexBufferSize);
	return id;
}

void CParticleManager::ReleaseParticleEmitter(ID_T(CParticleEmitter) aParticleEmitterID)
{
	for(auto it = myParticleDataCache.begin(); it != myParticleDataCache.end(); it++)
	{
		if (it->second == aParticleEmitterID)
		{
			myParticleEmitters.GetObj(aParticleEmitterID)->DecrementCounter();

			if (myParticleEmitters.GetObj(aParticleEmitterID)->GetRefCount() <= 0)
			{
				myParticleEmitters.Release(aParticleEmitterID);
				myParticleDataCache.erase(it);
			}

			break;
		}
	}
}

ID_T(CStreak) CParticleManager::AcquireStreak()
{
	ID_T(CStreak) id = myStreaks.Acquire();
	myStreaks.GetObj(id)->Init();
	return id;
}

void CParticleManager::ReleaseStreak(ID_T(CStreak) aStreakID)
{
	myStreaks.Release(aStreakID);
}

ID_T(CParticleEmitter) CParticleManager::LoadParticleData(const char * aFilePath, unsigned int aVertexBufferSize)
{
	std::string particleDataFilePath;

	if (aFilePath == nullptr)
	{
		particleDataFilePath = "";
	}
	else
	{
		particleDataFilePath = aFilePath;
	}

	if (myParticleDataCache.find(particleDataFilePath) != myParticleDataCache.end())
	{
		ID_T(CParticleEmitter) id = myParticleDataCache[particleDataFilePath];
		myParticleEmitters.GetObj(id)->IncrementCounter();
		return id;
	}
	else
	{
		CParticleEmitter::SParticleData particleData;

		if (aFilePath != nullptr)
		{
			LoadParticleDataFromFile(particleData, particleDataFilePath.c_str());
		}

		
		ID_T(CParticleEmitter) id = myParticleEmitters.Acquire();
		myParticleEmitters.GetObj(id)->Init(particleData, aVertexBufferSize);
		myParticleEmitters.GetObj(id)->IncrementCounter();
		myParticleDataCache[particleDataFilePath] = id;
		return id;
	}
}

CParticleEmitter * CParticleManager::GetParticleEmitter(ID_T(CParticleEmitter) aParticleEmitterID)
{
	return myParticleEmitters.GetObj(aParticleEmitterID);
}

void CParticleManager::LoadParticleDataFromFile(CParticleEmitter::SParticleData& aParticleData, const char* aFilePath)
{
	START_TIMER(PARTICLE_LOAD);
	JsonDocument doc;
	doc.LoadFile(aFilePath);

	if (doc.Find("myDuration"))
	{
		aParticleData.myDuration = doc["myDuration"].GetFloat();

		aParticleData.myIsLoopable = doc["myIsLoopable"].GetBool();

		aParticleData.mySpawnRate = doc["mySpawnRate"].GetFloat();

		aParticleData.myLifetime = doc["myLifetime"].GetFloat();

		CommonUtilities::Vector3f acceleration;
		acceleration.x = doc["myAcceleration"]["myX"].GetFloat();
		acceleration.y = doc["myAcceleration"]["myY"].GetFloat();
		acceleration.z = doc["myAcceleration"]["myZ"].GetFloat();
		aParticleData.myAcceleration = acceleration;

		CommonUtilities::Vector3f startVelocity;
		startVelocity.x = doc["myStartVelocity"]["myX"].GetFloat();
		startVelocity.y = doc["myStartVelocity"]["myY"].GetFloat();
		startVelocity.z = doc["myStartVelocity"]["myZ"].GetFloat();
		aParticleData.myStartVelocity = startVelocity;

		CommonUtilities::Vector2f startRotation;
		startRotation.x = doc["myStartRotation"]["myX"].GetFloat();
		startRotation.y = doc["myStartRotation"]["myY"].GetFloat();
		aParticleData.myStartRotation = startRotation;

		CommonUtilities::Vector2f rotationVelocity;
		rotationVelocity.x = doc["myRotationVelocity"]["myX"].GetFloat();
		rotationVelocity.y = doc["myRotationVelocity"]["myY"].GetFloat();
		aParticleData.myRotationVelocity = rotationVelocity;

		aParticleData.myGravityModifier = doc["myGravityModifier"].GetFloat();

		CommonUtilities::Vector4f startColor;
		startColor.x = doc["myStartColor"]["myR"].GetFloat();
		startColor.y = doc["myStartColor"]["myG"].GetFloat();
		startColor.z = doc["myStartColor"]["myB"].GetFloat();
		startColor.w = doc["myStartColor"]["myA"].GetFloat();
		aParticleData.myStartColor = startColor;

		CommonUtilities::Vector4f endColor;
		endColor.x = doc["myEndColor"]["myR"].GetFloat();
		endColor.y = doc["myEndColor"]["myG"].GetFloat();
		endColor.z = doc["myEndColor"]["myB"].GetFloat();
		endColor.w = doc["myEndColor"]["myA"].GetFloat();
		aParticleData.myEndColor = endColor;

		CommonUtilities::Vector2f startSize;
		startSize.x = doc["myStartSize"]["myX"].GetFloat();
		startSize.y = doc["myStartSize"]["myY"].GetFloat();
		aParticleData.myStartSize = startSize;

		CommonUtilities::Vector2f endSize;
		endSize.x = doc["myEndSize"]["myX"].GetFloat();
		endSize.y = doc["myEndSize"]["myY"].GetFloat();
		aParticleData.myEndSize = endSize;

		aParticleData.myBlendState = static_cast<CGraphicsStateManager::EBlendState>(doc["myBlendState"].GetInt());

		std::string texturePath = doc["myTexture"].GetString();
		aParticleData.myTexturePath = texturePath;

		if (doc.Find("myShapeData"))
		{
			CParticleEmitter::SParticleData::SShapeData shapeData;
			shapeData.myShapeType = static_cast<CParticleEmitter::ShapeType>(doc["myShapeData"]["myShapeType"].GetInt());

			if (shapeData.myShapeType == CParticleEmitter::ShapeType::ESphere)
			{
				shapeData.myRadius = doc["myShapeData"]["myRadius"].GetFloat();
				shapeData.myRadiusThickness = doc["myShapeData"]["myRadiusThickness"].GetFloat();
			}
			else if (shapeData.myShapeType == CParticleEmitter::ShapeType::EBox)
			{
				CommonUtilities::Vector3f boxSize;
				boxSize.x = doc["myShapeData"]["myBoxSize"]["myX"].GetFloat();
				boxSize.y = doc["myShapeData"]["myBoxSize"]["myY"].GetFloat();
				boxSize.z = doc["myShapeData"]["myBoxSize"]["myZ"].GetFloat();
				shapeData.myBoxSize = boxSize;
				shapeData.myBoxThickness = doc["myShapeData"]["myBoxThickness"].GetFloat();
			}
			aParticleData.myShapeData = shapeData;
		}
		else
		{
			aParticleData.myShapeData.myShapeType = CParticleEmitter::ShapeType::EPoint;
		}

		float time = GET_TIME(PARTICLE_LOAD);
		if (time <= 20)
		{
			RESOURCE_LOG(CONCOL_VALID, "Model %s took %fms to load!", aFilePath, time)
		}
		else if (time < 100)
		{
			RESOURCE_LOG(CONCOL_WARNING, "Model %s took %fms to load!", aFilePath, time)
		}
		else
		{
			RESOURCE_LOG(CONCOL_ERROR, "Model %s took %fms to load!", aFilePath, time)
		}

	}
}

CStreak * CParticleManager::GetStreak(ID_T(CStreak) aStreakID)
{
	return myStreaks.GetObj(aStreakID);
}

#pragma once

#include "GrowingArray.h"
#include "Vector.h"

#include "Streak.h"
#include "VertexDataWrapper.h"

class CVertexShader;
class CPixelShader;
class CGeometryShader;
class CConstantBuffer;
class CGraphicsStateManager;

struct SParticleSystemRenderCommand;
struct SStreakRenderCommand;

class CParticleRenderer
{
public:
	CParticleRenderer();
	~CParticleRenderer();

	bool Init();

	void RenderParticles(CGraphicsStateManager& aStateManager, const CommonUtilities::GrowingArray<SParticleSystemRenderCommand>& aParticleSystemRenderCommands, CConstantBuffer& aCameraBuffer);
	void RenderStreaks(CGraphicsStateManager& aStateManager, const CommonUtilities::GrowingArray<SStreakRenderCommand>& aStreakRenderCommands, CConstantBuffer& aCameraBuffer);

private:
	bool InitStreakVertexBuffer();

	CVertexShader* myVertexShader;
	CPixelShader* myPixelShader;
	CGeometryShader* myParticleGeometryShader;
	CGeometryShader* myStreakGeometryShader;

	constexpr static unsigned int MaxStreakBufferSize = 512;
	SVertexDataWrapper myStreakVertexData;
};


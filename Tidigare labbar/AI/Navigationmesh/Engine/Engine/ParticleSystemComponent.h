#pragma once

class CParticleEmitter;
class CParticleManager;
class CEngine;
class CGraphicsPipeline;
class CComponentSystem;
class CParticleEditor;

#include "Transform.h"
#include "Component.h"
#include <GrowingArray.h>

class CParticleSystemComponent : public CComponent
{
public:
	CParticleSystemComponent();
	~CParticleSystemComponent();

	enum EParticleState
	{
		EParticleState_Play,
		EParticleState_Paused,
		EParticleState_Stopped,
		EParticleState_Count
	};

	struct SParticleBufferData
	{
		CommonUtilities::Vector4f myPosition;
		CommonUtilities::Vector4f myColor;
		CommonUtilities::Vector2f mySize;
		float myRotation;
	};

	struct SComponentData
	{
		const char* myFilePath;
	};


	void PlayInstant();
	void Play();
	void Pause();
	void Stop();

	bool IsPlaying();

private:
	friend CParticleEditor;
	void Rebuild();

private:
	friend CEngine;
	friend CGraphicsPipeline;
	friend CComponentSystem;

	void Init(const SComponentData& aComponentData);
	void Update(const CTransform& aTransform);
	void UpdateParticles(float aDeltaTime);

	void Release() override;
	void RemovedDeadParticles(const CParticleEmitter& aParticleEmitter);

	void Reset();
	bool SpawnParticle(CParticleEmitter& aParticleEmitter, const CTransform& aTransform);

	struct SParticlePropertyData
	{
		CommonUtilities::Matrix33f myOrientation;
		CommonUtilities::Vector3f myVelocity;
		CommonUtilities::Vector3f myGravityVelocity;
		float myLifetime;
		float myRotationVelocity;
	};


	CommonUtilities::GrowingArray<SParticleBufferData> myParticles;
	CommonUtilities::GrowingArray<SParticlePropertyData> myParticleProperties;

	static constexpr float ourGravityValue = 9.82f;

	EParticleState myState;
	float mySpawnTimer;
	float myTotalPlayed;
	ID_T(CParticleEmitter) myParticleEmitterID;
	static CParticleManager* ourParticleManager;
};


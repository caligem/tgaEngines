#pragma once

constexpr int PoolSizeComponentSystemComponentStorages = 8;

//Component storage pools
constexpr int PoolSizeModelComponent = 4096;
constexpr int PoolSizeCameraComponent = 16;
constexpr int PoolSizeAudioSourceComponent = 512;
constexpr int PoolSizeAudioListenerComponent = 8;
constexpr int PoolSizeLightComponent = 1024;
constexpr int PoolSizeParticleSystemComponent = 1024;
constexpr int PoolSizeStreakComponent = 1024;
constexpr int PoolSizeSpriteComponent = 128;
constexpr int PoolSizeTextComponent = 64;
constexpr int PoolSizeCanvasComponent = 64;

constexpr int PoolSizeComponentStorageGameObjectData = 8192;

//Managers
constexpr int PoolSizeParticleManagerParticleEmitters = PoolSizeParticleSystemComponent;
constexpr int PoolSizeParticleManagerStreaks = PoolSizeStreakComponent;
constexpr int PoolSizeTextureManagerTextures = 8192;
constexpr int PoolSizeCameraManagerCameras = PoolSizeCameraComponent;
constexpr int PoolSizeSceneManagerScenes = PoolSizeComponentSystemComponentStorages;
constexpr int PoolSizeSpriteManagerSprites = PoolSizeSpriteComponent;
constexpr int PoolSizeTextManagerFonts = 2;
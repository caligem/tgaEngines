#pragma once

#include "Transform.h"
#include "Matrix.h"
#include "ObjectPool.h"
#include "ParticleSystemComponent.h"
#include "StreakComponent.h"
#include "SpriteComponent.h"
#include "Material.h"

class CModel;
class CSprite;
class CFont;
class CCanvasComponent;

// Models
struct SModelRenderCommand
{
	CTransform myTransform;
	ID_T(CModel) myModelToRender;
	CMaterial myMaterial;
};

// Lights
struct SDirectionalLightRenderCommand
{
	CommonUtilities::Vector3f myDirection;
	CommonUtilities::Vector3f myColor;
	float myIntensity;
};
struct SPointLightRenderCommand
{
	CommonUtilities::Vector3f myPosition;
	CommonUtilities::Vector3f myColor;
	float myRange;
	float myIntensity;
};
struct SSpotLightRenderCommand
{
	CommonUtilities::Vector3f myPosition;
	CommonUtilities::Vector3f myDirection;
	CommonUtilities::Vector3f myColor;
	float myRange;
	float myAngle;
	float myIntensity;
};

// Particles
struct SParticleSystemRenderCommand
{
	CommonUtilities::GrowingArray<CParticleSystemComponent::SParticleBufferData> myParticles;
	ID_T(CParticleEmitter) myParticleEmitterID;
};
struct SStreakRenderCommand
{
	CStreakComponent::SStreakBufferData myLeadingPoint;
	CommonUtilities::GrowingArray<CStreakComponent::SStreakBufferData> myPoints;
	ID_T(CStreak) myStreakID;
};

// 2D
struct SSpriteRenderCommand
{
	CSpriteComponent::SSpriteBufferData mySpriteData;
	ID_T(CSprite) mySpriteID;
};
struct STextRenderCommand
{
	CSpriteComponent::SSpriteBufferData mySpriteData;
	CommonUtilities::Vector4f myOutline;
	CommonUtilities::Vector2f myPosition;
	ID_T(CFont) myFontID;
};


struct SCanvasRenderCommand
{
	ID_T(CCanvasComponent) myCanvasComponentID;
};
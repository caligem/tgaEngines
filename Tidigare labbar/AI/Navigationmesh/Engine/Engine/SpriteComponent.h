#pragma once
#include "Component.h"
#include "UIElement.h"

class CSprite;
class CSpriteManager;
class CEngine;
class CGraphicsPipeline;
class CComponentSystem;


class CSpriteComponent : public CComponent, public CUIElement
{
public:
	struct SSpriteBufferData
	{
		CommonUtilities::Vector2f myPosition;
		CommonUtilities::Vector2f myScale;
		CommonUtilities::Vector2f myPivot;
		CommonUtilities::Vector2f myOriginalTextureSize;
		CommonUtilities::Vector2f myUVOffset;
		CommonUtilities::Vector2f myUVScale;
		CommonUtilities::Vector4f myTint;
		float myRotation;
		unsigned int myPriority;
	};

	CSpriteComponent();
	~CSpriteComponent();

	struct SComponentData
	{
		const char* myFilePath;
	};


	void SetPosition(const CommonUtilities::Vector2f& aPosition) { myData.myPosition = aPosition; }
	void SetScale(const CommonUtilities::Vector2f& aScale) { myData.myScale = aScale; }
	void SetScaleRelativeToScreen(const CommonUtilities::Vector2f& aScale);
	void SetPivot(const CommonUtilities::Vector2f& aPivot) { myData.myPivot = aPivot; }
	void SetUVOffset(const CommonUtilities::Vector2f& aUVOffset) { myData.myUVOffset = aUVOffset; }
	void SetUVScale(const CommonUtilities::Vector2f& aUVScale) { myData.myUVScale = aUVScale; }
	void SetTint(const CommonUtilities::Vector4f& aTint) { myData.myTint = aTint; }
	void SetRotation(float aRotation) { myData.myRotation = aRotation; }
	void SetPriority(unsigned int aPriority) { myData.myPriority = aPriority; } // Higher is closer to screen

	const CommonUtilities::Vector2f& GetPosition() const { return myData.myPosition; }
	const CommonUtilities::Vector2f& GetScale() const { return myData.myScale; }
	const CommonUtilities::Vector2f& GetPivot() const { return myData.myPivot; }
	const CommonUtilities::Vector2f& GetUVOffset() const { return myData.myUVOffset; }
	const CommonUtilities::Vector2f& GetUVScale() const { return myData.myUVScale; }
	const CommonUtilities::Vector4f& GetTint() const { return myData.myTint; }
	const float& GetRotation() const { return myData.myRotation; }
	const unsigned int& GetPriority() const { return myData.myPriority; }

	void FillRenderCommands(CDoubleBuffer<SSpriteRenderCommand>& aSpriteRenderCommandsBuffer, CDoubleBuffer<STextRenderCommand>&) override;

private:
	friend CEngine;
	friend CGraphicsPipeline;
	friend CComponentSystem;

	bool Init(const SComponentData& aComponentData);
	void Release() override;
	ID_T(CSprite) mySpriteID;
	static CSpriteManager* ourSpriteManager;
	SSpriteBufferData myData;
};


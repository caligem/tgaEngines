#include "stdafx.h"
#include "Streak.h"

#include "IEngine.h"
#include "TextureManager.h"

CStreak::CStreak()
{
	myMinVertexDistance = 0.f;
	myTime = 2.f;
}

CStreak::~CStreak()
{
}

bool CStreak::Init()
{
	myTexture = IEngine::GetTextureManager().CreateTextureFromFile("Assets/Particles/Sprites/roundParticle.dds");
	return true;
}

#include "stdafx.h"
#include "StreakComponent.h"

#include "ParticleManager.h"

#include "IEngine.h"
#include "Mathf.h"

CParticleManager* CStreakComponent::ourParticleManager = nullptr;

CStreakComponent::CStreakComponent()
{
}

CStreakComponent::~CStreakComponent()
{
}

void CStreakComponent::Init()
{
	myStreakID = ourParticleManager->AcquireStreak();

	myPoints.Init(512);
	myProperties.Init(512);
}

void CStreakComponent::Update(CommonUtilities::Vector3f aPosition)
{
	myLeadingPoint.myPosition = { aPosition.x, aPosition.y, aPosition.z, 0.f };

	CStreak& streak = *ourParticleManager->myStreaks.GetObj(myStreakID);

	if ( myPoints.Empty() || (myLeadingPoint.myPosition - myLastSolidifiedPosition).Length() > streak.GetMinVertexDistance() )
	{
		myPoints.Add({ myLeadingPoint.myPosition, {1.f, 1.f, 1.f, 1.f}, {1.f, 1.f} });
		myLastSolidifiedPosition = myLeadingPoint.myPosition;

		myProperties.EmplaceBack();
		myProperties.GetLast().myLifetime = 0.f;
	}

	UpdatePointData(streak.GetTime());
	RemoveDeadPoints(streak.GetTime());
}

void CStreakComponent::Release()
{
	ourParticleManager->ReleaseStreak(myStreakID);
	myStreakID = ID_T_INVALID(CStreak);
}

void CStreakComponent::UpdatePointData(float aTime)
{
	if (myPoints.Empty())return;

	float dt = IEngine::Time().GetDeltaTime();

	float totalLength = 0.f;
	for (unsigned short i = 0; i < myPoints.Size() - 1; ++i)
	{
		totalLength += (myPoints[i].myPosition - myPoints[i + 1].myPosition).Length();
	}
	totalLength += (myPoints.GetLast().myPosition - myLeadingPoint.myPosition).Length();

	float currentLength = 0.f;
	for (unsigned short i = 0; i < myPoints.Size(); ++i)
	{
		myProperties[i].myLifetime += dt;

		float alpha = myProperties[i].myLifetime / aTime;

		myPoints[i].mySize.x = currentLength / totalLength;
		myPoints[i].mySize.y = CommonUtilities::Lerp(0.1f, 0.5f, alpha);
		myPoints[i].myColor = CommonUtilities::Lerp<CommonUtilities::Vector4f>({ 1.f, 0.2f, 0.5f, 2.f }, { 0.2f, 0.5f, 1.f, 2.f }, alpha);

		if (i < myPoints.Size() - 1)
		{
			currentLength += (myPoints[i].myPosition - myPoints[i + 1].myPosition).Length();
		}
	}

	myLeadingPoint.mySize.x = 1.f;
	myLeadingPoint.mySize.y = CommonUtilities::Lerp(0.1f, 0.5f, 0.f);
	myLeadingPoint.myColor = CommonUtilities::Lerp<CommonUtilities::Vector4f>({ 1.f, 0.2f, 0.5f, 2.f }, { 0.2f, 0.5f, 1.f, 2.f }, 0.f);
}

void CStreakComponent::RemoveDeadPoints(float aTime)
{
	for (unsigned short i = myPoints.Size(); i > 0; --i)
	{
		if (myProperties[i - 1].myLifetime > aTime)
		{
			myPoints.RemoveAtIndex(i - 1);
			myProperties.RemoveAtIndex(i - 1);
		}
	}
}

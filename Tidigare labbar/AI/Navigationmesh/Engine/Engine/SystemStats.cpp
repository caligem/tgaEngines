#include "stdafx.h"
#include "SystemStats.h"

#include <Windows.h>
#include <Psapi.h>

#include "IWorld.h"

ULONGLONG _FixCPUTimings(const FILETIME &a, const FILETIME &b)
{
	LARGE_INTEGER la, lb;
	la.LowPart = a.dwLowDateTime;
	la.HighPart = a.dwHighDateTime;
	lb.LowPart = b.dwLowDateTime;
	lb.HighPart = b.dwHighDateTime;

	return la.QuadPart - lb.QuadPart;
}

float _GetCPUUsage(FILETIME *prevSysKernel, FILETIME *prevSysUser,
	FILETIME *prevProcKernel, FILETIME *prevProcUser,
	bool firstRun)
{
	FILETIME sysIdle, sysKernel, sysUser;
	FILETIME procCreation, procExit, procKernel, procUser;

	if (!GetSystemTimes(&sysIdle, &sysKernel, &sysUser) ||
		!GetProcessTimes(GetCurrentProcess(), &procCreation, &procExit, &procKernel, &procUser))
	{
		// can't get time info so return
		return -1.;
	}

	// check for first call
	if (firstRun)
	{
		// save time info before return
		prevSysKernel->dwLowDateTime = sysKernel.dwLowDateTime;
		prevSysKernel->dwHighDateTime = sysKernel.dwHighDateTime;

		prevSysUser->dwLowDateTime = sysUser.dwLowDateTime;
		prevSysUser->dwHighDateTime = sysUser.dwHighDateTime;

		prevProcKernel->dwLowDateTime = procKernel.dwLowDateTime;
		prevProcKernel->dwHighDateTime = procKernel.dwHighDateTime;

		prevProcUser->dwLowDateTime = procUser.dwLowDateTime;
		prevProcUser->dwHighDateTime = procUser.dwHighDateTime;

		return -1.;
	}

	ULONGLONG sysKernelDiff = _FixCPUTimings(sysKernel, *prevSysKernel);
	ULONGLONG sysUserDiff = _FixCPUTimings(sysUser, *prevSysUser);

	ULONGLONG procKernelDiff = _FixCPUTimings(procKernel, *prevProcKernel);
	ULONGLONG procUserDiff = _FixCPUTimings(procUser, *prevProcUser);

	ULONGLONG sysTotal = sysKernelDiff + sysUserDiff;
	ULONGLONG procTotal = procKernelDiff + procUserDiff;

	return (float)((100.0 * procTotal) / sysTotal);
}

int _GetMemUsage()
{
	PROCESS_MEMORY_COUNTERS memCounter;
	GetProcessMemoryInfo(GetCurrentProcess(),
		&memCounter,
		sizeof(memCounter));

	int memUsedMb = static_cast<int>(memCounter.WorkingSetSize) / 1024 / 1024;

	return memUsedMb;
}

bool CSystemStats::ourFirstRun = true;
FILETIME CSystemStats::myPrevProcKernel;
FILETIME CSystemStats::myPrevSysKernel;
FILETIME CSystemStats::myPrevProcUser;
FILETIME CSystemStats::myPrevSysUser;

float CSystemStats::CPUUsage()
{
	if (ourFirstRun)
	{
		GetCPUUsage(&myPrevSysKernel, &myPrevSysUser, &myPrevProcKernel, &myPrevProcUser, ourFirstRun);
		ourFirstRun = false;
	}
	return GetCPUUsage(&myPrevSysKernel, &myPrevSysUser, &myPrevProcKernel, &myPrevProcUser, ourFirstRun);
}

int CSystemStats::MemUsage()
{
	return GetMemUsage();
}

float CSystemStats::GetCPUUsage(FILETIME * prevSysKernel, FILETIME * prevSysUser, FILETIME * prevProcKernel, FILETIME * prevProcUser, bool firstRun)
{
	return _GetCPUUsage(prevSysKernel, prevSysUser, prevProcKernel, prevProcUser, firstRun);
}

int CSystemStats::GetMemUsage()
{
	return _GetMemUsage();
}

void CSystemStats::Update()
{
	static float time = 0.f;
	time += IWorld::Time().GetRealDeltaTime();
	if (time > 1.f)
	{
		GENERAL_LOG(CONCOL_DEFAULT, "CPU Usage: %.1f%%\tMem Usage: %dMb", CPUUsage(), MemUsage());
		time = 0.f;
	}
}

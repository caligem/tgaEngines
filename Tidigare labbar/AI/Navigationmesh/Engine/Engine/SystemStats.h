#pragma once

typedef struct _FILETIME FILETIME;

class CSystemStats
{
public:
	static float CPUUsage();
	static int MemUsage();
	static void Update();
	
private:
	static float GetCPUUsage (
		FILETIME *prevSysKernel,
		FILETIME *prevSysUser,
		FILETIME *prevProcKernel,
		FILETIME *prevProcUser,
		bool firstRun = false
	);

	static int GetMemUsage();
	static bool ourFirstRun;

	static FILETIME myPrevSysKernel;
	static FILETIME myPrevSysUser;
	static FILETIME myPrevProcKernel;
	static FILETIME myPrevProcUser;

};


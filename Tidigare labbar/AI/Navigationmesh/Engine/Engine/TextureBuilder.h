#pragma once

struct ID3D11ShaderResourceView;

class CTextureBuilder
{
public:
	CTextureBuilder() = delete;
	~CTextureBuilder() = delete;

	static ID3D11ShaderResourceView* CreatePinkCheckerBoardTexture();
	static ID3D11ShaderResourceView* CreateWhiteTexture();
	static ID3D11ShaderResourceView* CreateBlackTexture();
	static ID3D11ShaderResourceView* CreateGrayCheckerBoardTexture();
	static ID3D11ShaderResourceView* CreateNormalTexture();

	static ID3D11ShaderResourceView* CreateTextureFromFile(const std::string& aFilepath);
	static CommonUtilities::Vector2f GetTextureSize(ID3D11ShaderResourceView* aSRV);

};


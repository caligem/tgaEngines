#pragma once

struct ID3D11ShaderResourceView;

#include "TextureManager.h"

struct STextureDataWrapper
{
public:
	STextureDataWrapper() {
		for(int i = 0; i < maxTextures; ++i)myTextures[i] = ID_T_INVALID(SSRV);
	}
	~STextureDataWrapper() {}

	static constexpr int maxTextures = 6;

	SRV myTextures[maxTextures] = {
		ID_T_INVALID(SSRV),
		ID_T_INVALID(SSRV),
		ID_T_INVALID(SSRV),
		ID_T_INVALID(SSRV),
		ID_T_INVALID(SSRV),
		ID_T_INVALID(SSRV)
	};
};
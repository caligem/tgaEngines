#pragma once

template<class Type>
class CUniqueIdentifier
{
public:
	template<class T>
	inline static Type GetUUID(T* = nullptr);

private:
	CUniqueIdentifier() = delete;
	~CUniqueIdentifier() = delete;

	static Type ourNextID;
};

template<class Type>
Type CUniqueIdentifier<Type>::ourNextID = 0;

template<class Type>
template<class T>
inline Type CUniqueIdentifier<Type>::GetUUID(T* = nullptr)
{
	static Type uuid = ourNextID++;
	return uuid;
}

typedef int8_t _UUID_T;

#define _UUID(T) CUniqueIdentifier<_UUID_T>::GetUUID<T>()
#define _UUIDP(Ptr) CUniqueIdentifier<_UUID_T>::GetUUID(Ptr)

#define PRINT_CLASS_UUID(T) std::cout << #T << ": " << std::to_string(_UUID(T)) << std::endl;

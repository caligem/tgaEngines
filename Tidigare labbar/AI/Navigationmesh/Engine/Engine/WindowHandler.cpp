#include "stdafx.h"
#include "WindowHandler.h"

#include "Engine.h"

#include "IWorld.h"
#include "InputManager.h"

#include "..\Launcher\resource.h"

CWindowHandler::CWindowHandler()
{
}

CWindowHandler::~CWindowHandler()
{
}

bool CWindowHandler::Init(const SCreateParameters* aSetting)
{
	myWndProcCallback = aSetting->myWndProcCallback;

	WNDCLASS windowclass = {};

	myCursor = LoadCursor(GetModuleHandle(NULL), MAKEINTRESOURCE(IDC_CURSOR1));

	windowclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	windowclass.lpfnWndProc = WndProc;
	windowclass.hCursor = myCursor;
	windowclass.hIcon = LoadIcon(GetModuleHandle(NULL), MAKEINTRESOURCE(IDI_ICON1));
	windowclass.lpszClassName = L"window";
	RegisterClass(&windowclass);

	unsigned int width = aSetting->myCanvasWidth;
	unsigned int height = aSetting->myCanvasHeight;

	int dwStyle = WS_OVERLAPPEDWINDOW ^ WS_MAXIMIZEBOX ^ WS_SIZEBOX | WS_POPUP;
	if (aSetting->myIsBorderless)
	{
		dwStyle = WS_POPUP;
	}

	if (aSetting->myHWND == nullptr)
	{
		myWindowHandle = CreateWindow( L"window",
			aSetting->myGameName.c_str(),
			dwStyle,
			0, 0, width, height,
			NULL, NULL, NULL, NULL);


		RECT rect;
		GetClientRect(myWindowHandle, &rect);

		SetWindowPos(myWindowHandle, HWND_TOP, 0, 0, width + (width - (rect.right - rect.left)), height + (height - (rect.bottom - rect.top)), SWP_SHOWWINDOW);	
	}
	else
	{
		myWindowHandle = aSetting->myHWND;
	}

	SetWindowLongPtr(myWindowHandle, GWLP_USERDATA, (LONG_PTR)this);
	
	return true;
}

LRESULT CWindowHandler::LocWndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam)
{
	if (myWndProcCallback)
	{
		return myWndProcCallback(aHwnd, aMessage, wParam, lParam);
	}

	return DefWindowProc(aHwnd, aMessage, wParam, lParam);
}

extern LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK CWindowHandler::WndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam)
{
	CWindowHandler* window = (CWindowHandler*)GetWindowLongPtr(aHwnd, GWLP_USERDATA);
	if (window)
	{
		if (window->myWndProcCallback)
		{
			LRESULT result = window->LocWndProc(aHwnd, aMessage, wParam, lParam);
			if (result)
			{
				return DefWindowProc(aHwnd, aMessage, wParam, lParam);
			}
		}
	}

	ImGui_ImplWin32_WndProcHandler(aHwnd, aMessage, wParam, lParam);

	switch (aMessage)
	{
	case WM_KEYDOWN: case WM_KEYUP:
	case WM_LBUTTONDOWN: case WM_RBUTTONDOWN: case WM_MBUTTONDOWN:
	case WM_LBUTTONUP: case WM_RBUTTONUP: case WM_MBUTTONUP:
	case WM_MOUSEMOVE: case WM_MOUSEWHEEL:
		IWorld::Input().OnInput(aMessage, wParam, lParam);
		return 0;
		break;
	case WM_SYSKEYDOWN: case WM_SYSKEYUP:
		if (wParam == Input::Key_Alt)
		{
			IWorld::Input().OnInput(aMessage, wParam, lParam);
			return 0;
		}
		break;
	case WM_SIZE:
		if (wParam == SIZE_RESTORED)
		{
			IEngine::SetWindowSize({ LOWORD(lParam), HIWORD(lParam) });
		}
		break;
	case WM_DESTROY:
	case WM_CLOSE:
		IEngine::GetEngine().Shutdown();
		return 0;
		break;
	}

	return DefWindowProc(aHwnd, aMessage, wParam, lParam);
}

void CWindowHandler::Update()
{
	if (myShouldShowCursor)
	{
		while (::ShowCursor(TRUE) < 0);
	}
	else
	{
		while (::ShowCursor(FALSE) >= 0);
	}
}

void CWindowHandler::CloseWindow()
{
	DestroyWindow(myWindowHandle);
}

void CWindowHandler::HideCursor()
{
	myShouldShowCursor = false;
}

void CWindowHandler::ShowCursor()
{
	myShouldShowCursor = true;
}

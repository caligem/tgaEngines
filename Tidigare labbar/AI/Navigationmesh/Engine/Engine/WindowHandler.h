#pragma once

struct HWND__;
typedef HWND__* HWND;

#include <functional>
#include <windows.h>

struct SCreateParameters;
class CWindowHandler
{
public:
	CWindowHandler();
	~CWindowHandler();

	bool Init(const SCreateParameters* aWindowData);
	void Update();

	HWND GetWindowHandle() { return myWindowHandle; }
	void CloseWindow();

	void HideCursor();
	void ShowCursor();

	static LRESULT CALLBACK WndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam);

private:
	LRESULT LocWndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam);
	std::function<LRESULT(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam)> myWndProcCallback;

	HWND myWindowHandle;

	HCURSOR myCursor;
	volatile bool myShouldShowCursor = true;

};


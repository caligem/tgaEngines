#pragma once

#include "GrowingArray.h"

#include "State.h"
#include "StateStack.h"


class CGameWorld
{
public:
	enum EGameStates
	{
		EGameStates_Tuturial,
		EGameStates_Village,
		EGameStates_Pillars,
		EGameStates_Roots,
		EGameStates_Count
	};
	CGameWorld();
	~CGameWorld();

	void Init();
	void Update();
private:
	void LoadAudioSettings();
	CStateStack myStateStack;
};


#include "stdafx.h"
#include "ShowRoomState.h"

#include <InputManager.h>
#include <IWorld.h>

#include <Quaternion.h>
#include <XBOXController.h>

#include "Random.h"

#include <filesystem>

#include "CameraDataWrapper.h"
#include "ICollision.h"

CShowRoomState::CShowRoomState()
{
}

CShowRoomState::~CShowRoomState()
{
}

bool CShowRoomState::Init()
{
	CState::Init();

	myModelFileDatas.Init(16);

	myCyclePositions.Init(8);
	myCycleIndex = 0;

	std::sort(myCyclePositions.begin(), myCyclePositions.end(), [](auto a, auto b)
	{
		if (std::fabsf(a.z - b.z) < 10.f)
		{
			return a.x > b.x;
		}
		return a.z > b.z;
	});

	myPivot = { 0.f, 0.f, 0.f };
	if(!myCyclePositions.Empty()) myPivot = myCyclePositions[myCycleIndex];
	myRotation = { 0.f, 0.f };
	myZoom = 2.f;

	LoadModels();

	CGameObject dir1;
	dir1.Init(mySceneID);
	dir1.GetTransform().SetLookDirection({ -1.f, 1.f, 1.f });
	CLightComponent* light1 = dir1.AddComponent<CLightComponent>();
	light1->SetType(CLightComponent::ELightType_Directional);
	light1->SetColor({ 1.f, 0.8f, 0.5f });

	CGameObject dir2;
	dir2.Init(mySceneID);
	dir2.GetTransform().SetLookDirection({ 1.f, 0.f, -1.f });
	CLightComponent* light2 = dir2.AddComponent<CLightComponent>();
	light2->SetType(CLightComponent::ELightType_Directional);
	light2->SetColor({ 0.5f, 0.8f, 1.f });

	CGameObject dir3;
	dir3.Init(mySceneID);
	dir3.GetTransform().SetLookDirection({ 0.f, -1.f, 0.f });
	CLightComponent* light3 = dir3.AddComponent<CLightComponent>();
	light3->SetType(CLightComponent::ELightType_Directional);
	light3->SetColor({ 0.9f, 0.9f, 0.9f });

	//IWorld::GetSceneManager().GetSceneAt(mySceneID)->SetSkybox("Assets/CubeMaps/skybox.dds");
	//IWorld::GetSceneManager().GetSceneAt(mySceneID)->SetCubemap("Assets/CubeMaps/cubemap.dds");

	myNavmesh.Init();

	return true;
}

EStateUpdate CShowRoomState::Update()
{
	HandleControls();

	DrawNavMesh();

	HandleState();

	if (myShouldPop)
	{
		return EPop_Main;
	}
	return EDoNothing;
}

void CShowRoomState::OnEnter()
{
	SetActiveScene();
}

void CShowRoomState::OnLeave()
{
}

void CShowRoomState::HandleControls()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Vector2f movement = input.GetMouseMovement();
	movement.x *= (2.f*CommonUtilities::Pif) / 1280.f;
	movement.y *= (CommonUtilities::Pif) / 720.f;

	if (input.IsKeyDown(Input::Key_Alt))
	{
		if (input.IsButtonDown(Input::Button_Left))
		{
			myRotation.x += movement.y;
			myRotation.y += movement.x;

			if (myRotation.x >= CommonUtilities::Pif / 2.f)
			{
				myRotation.x = CommonUtilities::Pif / 2.f - 0.001f;
			}
			if (myRotation.x <= -CommonUtilities::Pif / 2.f)
			{
				myRotation.x = -CommonUtilities::Pif / 2.f + 0.001f;
			}
		}
		else if (input.IsButtonDown(Input::Button_Middle))
		{
			myPivot -= myMainCamera.GetTransform().GetRight() * movement.x * myZoom;
			myPivot += myMainCamera.GetTransform().GetUp() * movement.y * myZoom;
		}
		else if (input.IsButtonDown(Input::Button_Right))
		{
			myZoom -= (movement.x + movement.y) * myZoom;
			if (myZoom < 0.1f)
			{
				myZoom = 0.1f;
				myPivot += myMainCamera.GetTransform().GetForward() * (movement.x + movement.y);
			}
		}
	}

	if (input.IsKeyPressed(Input::Key_Left))
	{
		if (myCycleIndex == 0)
		{
			myCycleIndex = myCyclePositions.Size() - 1;
		}
		else
		{
			--myCycleIndex;
		}
		myPivot = myCyclePositions[myCycleIndex];
	}
	if (input.IsKeyPressed(Input::Key_Right))
	{
		++myCycleIndex;
		if (myCycleIndex == myCyclePositions.Size())
		{
			myCycleIndex = 0;
		}
		myPivot = myCyclePositions[myCycleIndex];
	}

	if (input.IsKeyPressed(Input::Key_Escape) || IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton_Back))
	{
		myShouldPop = true;
	}

	CommonUtilities::Vector3f newPos(0.f, 0.f, -myZoom);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundX(myRotation.x);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundY(myRotation.y);
	newPos += myPivot;

	myMainCamera.GetTransform().SetPosition(newPos);
	myMainCamera.GetTransform().LookAt(myPivot);
}

void CShowRoomState::GetModelPaths()
{
	std::string rootDir = "Assets/Models";
	std::experimental::filesystem::path rootPath = std::experimental::filesystem::current_path().append(rootDir);
	GetFBXPathInFolder(rootPath.c_str());
}

void CShowRoomState::GetFBXPathInFolder(const wchar_t* aFolderPath)
{
	std::experimental::filesystem::directory_iterator modelFolderIt(aFolderPath);
	for (auto& file : modelFolderIt)
	{
		if (file.path().has_extension())
		{
			if (file.path().extension() == ".fbx")
			{
				SModelFileData data;

				std::string modelPath = file.path().generic_string();
				data.myPath = modelPath.substr(std::experimental::filesystem::current_path().generic_string().size() + 1);
				
				auto time = std::experimental::filesystem::last_write_time(file);
				data.myTimeSinceEpoch = time.time_since_epoch().count();
				
				myModelFileDatas.Add(data);
			}
		}
		else
		{
			GetFBXPathInFolder(file.path().c_str());
		}
	}
}

void CShowRoomState::LoadModels()
{
	GetModelPaths();

	float offset = 5.f;

	std::sort(myModelFileDatas.begin(), myModelFileDatas.end(), [&](const SModelFileData& a, const SModelFileData& b) { return a.myTimeSinceEpoch > b.myTimeSinceEpoch; });

	for (unsigned short i = 0; i < myModelFileDatas.Size(); ++i)
	{
		CGameObject model;
		model.Init(mySceneID);
		model.AddComponent<CModelComponent>({ myModelFileDatas[i].myPath.c_str() });
		CommonUtilities::Vector3f position(-(i * offset), 0.f, 0.f);
		model.GetTransform().SetPosition(position);
		myCyclePositions.Add(position);
	}
}

void CShowRoomState::DrawNavMesh()
{
	IWorld::SetDebugColor({ 1.f, 1.f, 0.f, 1.f });
	for (int i = 0; i < myNavmesh.GetNumVertices(); ++i)
	{
		IWorld::DrawDebugWireSphere(myNavmesh.GetVertexAt(i), { 0.1f, 0.1f, 0.0f });
	}
	IWorld::SetDebugColor({ 1.f, 0.f, 0.f, 1.f });

	CommonUtilities::Vector3f point = GetWorldPoint();
	for (int i = 0; i < myNavmesh.GetNumEdges(); ++i)
	{
		auto& edge = myNavmesh.GetEdgeAt(i);

		auto v0 = myNavmesh.GetVertexAt(edge.v0);
		auto v1 = myNavmesh.GetVertexAt(edge.v1);

		if (myHasPlacedFirstPoint)
		{
			CommonUtilities::Vector3f intersectionPoint;
			if (ICollision::LineVsLine(v0, v1, myFirstPoint, point, intersectionPoint))
			{
				IWorld::SetDebugColor({ 1.f, 0.f, 1.f, 1.f });
				IWorld::DrawDebugWireSphere(intersectionPoint, { 0.1f, 0.1f, 0.0f });
			}
			else
			{
				IWorld::SetDebugColor({ 1.f, 0.f, 0.f, 1.f });
			}
		}

		IWorld::DrawDebugLine(v0, v1);
	}
}

void CShowRoomState::HandleState()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Vector3f point = GetWorldPoint();

	IWorld::SetDebugColor({ 0.f, 1.f, 1.f, 1.f });
	IWorld::DrawDebugWireCube(point, { 0.1f, 0.1f, 0.0f });

	if (!myHasPlacedFirstPoint)
	{
		if (input.IsButtonPressed(Input::Button_Left))
		{
			myFirstPoint = point;
			myHasPlacedFirstPoint = true;
		}
	}
	else
	{
		IWorld::DrawDebugWireCube(myFirstPoint, { 0.1f, 0.1f, 0.0f });

		IWorld::SetDebugColor({ 0.f, 0.f, 1.f, 1.f });
		IWorld::DrawDebugLine(myFirstPoint, point);

		if (input.IsButtonPressed(Input::Button_Left))
		{
			myNavmesh.SplitNavmesh(myFirstPoint, point);
			myHasPlacedFirstPoint = false;
		}
		else if (input.IsButtonPressed(Input::Button_Right))
		{
			myHasPlacedFirstPoint = false;
		}
	}
}

CommonUtilities::Vector3f CShowRoomState::GetWorldPoint()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Matrix44f viewProjInv = CommonUtilities::Matrix44f::Inverse(IWorld::GetSavedCameraBuffer().myViewProjection);

	CommonUtilities::Vector2f mousePos = {
		input.GetMousePosition().x / IWorld::GetWindowSize().x,
		input.GetMousePosition().y / IWorld::GetWindowSize().y
	};
	mousePos.x = mousePos.x * 2.f - 1.f;
	mousePos.y = (1.f - mousePos.y) * 2.f - 1.f;

	CommonUtilities::Vector4f rayOrigin = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 0.f, 1.f) * viewProjInv;
	rayOrigin /= rayOrigin.w;
	CommonUtilities::Vector4f rayEnd = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 1.f, 1.f) * viewProjInv;
	rayEnd /= rayEnd.w;

	CommonUtilities::Vector3f rayDir = rayEnd - rayOrigin;
	rayDir.Normalize();

	CommonUtilities::Vector3f normal = { 0.f, 0.f, -1.f };

	float denom = normal.Dot(rayDir);

	CommonUtilities::Vector3f p0l0 = -CommonUtilities::Vector3f(rayOrigin);

	float t = p0l0.Dot(normal) / denom;

	CommonUtilities::Vector3f intersection = CommonUtilities::Vector3f(rayOrigin) + rayDir*t;

	return intersection;
}
#pragma once
#include "State.h"
#include <GrowingArray.h>
#include <Vector.h>
#include <Navmesh.h>

class CShowRoomState : public CState
{
	struct SModelFileData
	{
		std::string myPath;
		long long myTimeSinceEpoch;
	};

public:
	CShowRoomState();
	~CShowRoomState();

	bool Init();
	EStateUpdate Update() override;
	void OnEnter() override;
	void OnLeave() override;

private:
	bool myHasPlacedFirstPoint = false;
	CommonUtilities::Vector3f myFirstPoint;
	CNavmesh myNavmesh;

	CommonUtilities::Vector3f GetWorldPoint();
	void HandleState();
	void DrawNavMesh();

	void HandleControls();
	void GetModelPaths();
	void GetFBXPathInFolder(const wchar_t* aFolderPath);
	void LoadModels();
	CGameObject myControlsInfo;

	CommonUtilities::Vector3f myPivot;
	CommonUtilities::Vector2f myRotation;
	float myZoom;

	CommonUtilities::GrowingArray<CommonUtilities::Vector3f> myCyclePositions;
	unsigned short myCycleIndex;
	bool myShouldPop = false;

	CommonUtilities::GrowingArray<SModelFileData> myModelFileDatas;
};


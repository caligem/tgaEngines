#include "stdafx.h"
#include "State.h"

#include "StateStack.h"
#include "IWorld.h"

#include "JsonDocument.h"
#include "JsonUtility.h"

CStateStack* CState::ourStateStack = nullptr;

bool CState::Init()
{
	mySceneID = IWorld::GetSceneManager().CreateScene();

	myMainCamera.Init(mySceneID);
	myMainCamera.AddComponent<CCameraComponent>({
		60.f,
		static_cast<float>(IWorld::GetWindowSize().x / IWorld::GetWindowSize().y),
		0.1f, 1000.f
	})->SetAsActiveCamera();

	return true;
}

void CState::SetActiveScene()
{
	IWorld::GetSceneManager().SetActiveScene(mySceneID);
}

void CState::DestoryScene()
{
	IWorld::GetSceneManager().DestroyScene(mySceneID);
}

void CState::LoadFromFile(const std::string & aLevelpath)
{
	JsonDocument doc(aLevelpath.c_str());

	std::map<int, CGameObject> gameObjects;

	for (int i = 0; i < doc["myGameObjects"].GetSize(); ++i)
	{
		auto obj = doc["myGameObjects"][i];

		CGameObject gameObject;
		gameObject.Init(mySceneID);

		gameObjects[obj["myID"].GetInt()] = gameObject;

		gameObject.GetTransform().SetPosition(JsonToVector3f(obj["myPosition"]));
		gameObject.GetTransform().SetRotation(JsonToQuatf(obj["myRotation"]).GetEulerAngles());
		gameObject.GetTransform().SetScale(JsonToVector3f(obj["myScale"]));

		int parentID = obj["myParent"].GetInt();
		if (parentID != -1)
		{
			if (gameObjects.find(parentID) != gameObjects.end())
			{
				gameObject.GetTransform().SetParent(&gameObjects[parentID].GetTransform());
			}
		}
	}
	for (int i = 0; i < doc["myMeshFilters"].GetSize(); ++i)
	{
		auto obj = doc["myMeshFilters"][i];
		int parentID = obj["myParent"].GetInt();
		if (gameObjects.find(parentID) != gameObjects.end())
		{
			gameObjects[parentID].AddComponent<CModelComponent>({ obj["myPath"].GetString() });
		}
	}
	for (int i = 0; i < doc["myDirectionalLights"].GetSize(); ++i)
	{
		auto obj = doc["myDirectionalLights"][i];
		int parentID = obj["myParent"].GetInt();

		if (gameObjects.find(parentID) != gameObjects.end())
		{
			CLightComponent* light = gameObjects[parentID].AddComponent<CLightComponent>();
			light->SetType(CLightComponent::ELightType_Directional);
			light->SetIntensity(obj["myIntensity"].GetFloat());
			light->SetColor(JsonToVector3f(obj["myColor"]));
		}
	}
	for (int i = 0; i < doc["myPointLights"].GetSize(); ++i)
	{
		auto obj = doc["myPointLights"][i];
		int parentID = obj["myParent"].GetInt();

		if (gameObjects.find(parentID) != gameObjects.end())
		{
			CLightComponent* light = gameObjects[parentID].AddComponent<CLightComponent>();
			light->SetType(CLightComponent::ELightType_Point);
			light->SetIntensity(obj["myIntensity"].GetFloat());
			light->SetRange(obj["myRange"].GetFloat());
			light->SetColor(JsonToVector3f(obj["myColor"]));
		}
	}
	for (int i = 0; i < doc["mySpotLights"].GetSize(); ++i)
	{
		auto obj = doc["mySpotLights"][i];
		int parentID = obj["myParent"].GetInt();

		if (gameObjects.find(parentID) != gameObjects.end())
		{
			CLightComponent* light = gameObjects[parentID].AddComponent<CLightComponent>();
			light->SetType(CLightComponent::ELightType_Spot);
			light->SetIntensity(obj["myIntensity"].GetFloat());
			light->SetSpotAngle(obj["myAngle"].GetFloat());
			light->SetRange(obj["myRange"].GetFloat());
			light->SetColor(JsonToVector3f(obj["myColor"]));
		}
	}

	OnLoadFinished(doc);
}

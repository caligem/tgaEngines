#pragma once
#include "ObjectPool.h"

#include "GameObject.h"

#include "JsonDocument.h"

enum EStateUpdate
{
	EQuitGame = -1,
	EPop_Main = 0,
	EPop_Sub,
	EDoNothing,
	EPop_Sub_Push_Sub,
	EPop_Sub_Push_Main,
	EPop_Main_Push_Sub,
	EPop_Main_Push_Main
};

class CStateStack;
class CScene;

class CState
{
public:
	CState() : myNewStateToPush(nullptr) {}
	virtual ~CState() = default;
	virtual bool Init();
	virtual EStateUpdate Update() = 0;
	virtual void OnEnter() = 0;
	virtual void OnLeave() = 0;

	void SetActiveScene();

	void DestoryScene();

	CState* GetNewStateToPush() { return myNewStateToPush; }

protected:
	friend CStateStack;

	void LoadFromFile(const std::string& aLevelpath);
	virtual void OnLoadFinished(JsonDocument& aDoc) { aDoc; }

	ID_T(CScene) mySceneID;

	static CStateStack* ourStateStack;
	CState* myNewStateToPush;

	CGameObject myMainCamera;
};

#include "stdafx.h"
#include "TestRoomState.h"
#include "Iworld.h"

CTestRoomState::CTestRoomState()
	: myShouldPop(false)
{
}


CTestRoomState::~CTestRoomState()
{
}

bool CTestRoomState::Init()
{
	CState::Init();

	LoadFromFile("Assets/Levels/showroom.json");

	myPivot = { 0.f, 0.f, 0.f };
	myRotation = { 0.f, CommonUtilities::Pif };
	myZoom = 20.f;

	return true;
}

EStateUpdate CTestRoomState::Update()
{
	HandleControls();

	IWorld::DrawDebugSphere(myPivot, { 5.f, 5.f, 5.f });

	if (myShouldPop)
	{
		return EPop_Main;
	}
	return EDoNothing;
}

void CTestRoomState::OnEnter()
{
	SetActiveScene();
}

void CTestRoomState::OnLeave()
{
}

void CTestRoomState::HandleControls()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Vector2f movement = input.GetMouseMovement();
	movement.x *= (2.f*CommonUtilities::Pif) / 1280.f;
	movement.y *= (CommonUtilities::Pif) / 720.f;

	if (input.IsKeyDown(Input::Key_Alt))
	{
		if (input.IsButtonDown(Input::Button_Left))
		{
			myRotation.x += movement.y;
			myRotation.y += movement.x;

			if (myRotation.x >= CommonUtilities::Pif / 2.f)
			{
				myRotation.x = CommonUtilities::Pif / 2.f - 0.001f;
			}
			if (myRotation.x <= -CommonUtilities::Pif / 2.f)
			{
				myRotation.x = -CommonUtilities::Pif / 2.f + 0.001f;
			}
		}
		else if (input.IsButtonDown(Input::Button_Middle))
		{
			myPivot -= myMainCamera.GetTransform().GetRight() * movement.x * myZoom;
			myPivot += myMainCamera.GetTransform().GetUp() * movement.y * myZoom;
		}
		else if (input.IsButtonDown(Input::Button_Right))
		{
			myZoom -= (movement.x + movement.y) * myZoom;
			if (myZoom < 0.1f)
			{
				myZoom = 0.1f;
				myPivot += myMainCamera.GetTransform().GetForward() * (movement.x + movement.y);
			}
		}
	}

	if (input.IsKeyPressed(Input::Key_Escape))
	{
		myShouldPop = true;
	}

	CommonUtilities::Vector3f newPos(0.f, 0.f, -myZoom);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundX(myRotation.x);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundY(myRotation.y);
	newPos += myPivot;

	myMainCamera.GetTransform().SetPosition(newPos);
	myMainCamera.GetTransform().LookAt(myPivot);
}

#include "stdafx.h"
#include "ICollision.h"

#include "ColliderSphere.h"
#include "ColliderOBB.h"


bool ICollision::LineVsLine(const CommonUtilities::Vector3f & aPoint0, const CommonUtilities::Vector3f & aPoint1, const CommonUtilities::Vector3f & aPoint2, const CommonUtilities::Vector3f & aPoint3, CommonUtilities::Vector3f & aIntersection)
{
	auto da = aPoint1 - aPoint0;
	auto db = aPoint3 - aPoint2;
	auto dc = aPoint2 - aPoint0;

	if (std::fabsf(dc.Dot(da.Cross(db))) > 1e-3)
	{
		return false;
	}

	float s = dc.Cross(db).Dot(da.Cross(db)) / da.Cross(db).Length2();

	float t = -(dc).Cross(da).Dot(db.Cross(da)) / da.Cross(db).Length2();

	if (s >= 0.0f && s < 1.0f && t >= 0.f && t < 1.0f)
	{
		aIntersection = aPoint0 + da * s;
		return true;
	}

	return false;
}

bool ICollision::RayVsLine(const CommonUtilities::Vector3f & aPoint0, const CommonUtilities::Vector3f & aPoint1, const CommonUtilities::Vector3f & aRayOrigin, const CommonUtilities::Vector3f & aRayDirection, CommonUtilities::Vector3f & aIntersection)
{
	auto da = aPoint1 - aPoint0;
	auto db = aRayDirection - aRayOrigin;
	auto dc = aRayOrigin - aPoint0;

	if (std::fabsf(dc.Dot(da.Cross(db))) > 1e-3)
	{
		return false;
	}

	float s = dc.Cross(db).Dot(da.Cross(db)) / da.Cross(db).Length2();

	if (s >= 0.0f && s < 1.0f)
	{
		aIntersection = aPoint0 + da * s;
		return true;
	}

	return false;
}

bool ICollision::PointVsTriangle(const CommonUtilities::Vector3f aVertex0, const CommonUtilities::Vector3f aVertex1, const CommonUtilities::Vector3f aVertex2, const CommonUtilities::Vector3f aPoint)
{
	auto a = aVertex1 - aVertex0;
	auto b = aVertex2 - aVertex1;
	auto c = aVertex0 - aVertex2;

	if (a.Cross(b).z > 0.f)
	{
		if (a.Cross(aPoint - aVertex0).z > 0.f &&
			b.Cross(aPoint - aVertex1).z > 0.f &&
			c.Cross(aPoint - aVertex2).z > 0.f)
		{
			return true;
		}
	}
	else
	{
		if (a.Cross(aPoint - aVertex0).z <= 0.f &&
			b.Cross(aPoint - aVertex1).z <= 0.f &&
			c.Cross(aPoint - aVertex2).z <= 0.f)
		{
			return true;
		}
	}

	return false;
}

bool ICollision::SphereVsSphere(const CColliderSphere & aSphere0, const CColliderSphere & aSphere1, CommonUtilities::Vector3f * aContactPoint)
{
	float d2 = (aSphere0.GetPosition() - aSphere1.GetPosition()).Length2();
	float r2 = aSphere0.GetRadius() + aSphere1.GetRadius();
	r2 *= r2;

	*aContactPoint = aSphere1.GetPosition() + (aSphere0.GetPosition() - aSphere1.GetPosition());

	if (d2 > aSphere1.GetRadius()*aSphere1.GetRadius())
	{
		*aContactPoint = aSphere1.GetPosition() + (aSphere0.GetPosition() - aSphere1.GetPosition()).GetNormalized()*aSphere1.GetRadius();
	}

	if( d2 <= r2 )
	{
		return true;
	}
	return false;
}

bool ICollision::SphereVsOBB(const CColliderSphere & aSphere, const CColliderOBB & aOBB, CommonUtilities::Vector3f * aContactPoint)
{
    CommonUtilities::Vector3f result = aOBB.GetPosition();
    CommonUtilities::Vector3f dir = aSphere.GetPosition() - aOBB.GetPosition();
    for (unsigned short i = 0; i < 3; ++i) 
    {
        const float* orientation = &aOBB.GetOrientation()[i * 3];
        CommonUtilities::Vector3f axis(orientation[0], orientation[1], orientation[2]);
        float distance = dir.Dot(axis);
        if (distance > (&aOBB.GetRadius().x)[i]) 
        {
            distance = (&aOBB.GetRadius().x)[i];
        }
        if (distance < -(&aOBB.GetRadius().x)[i]) 
        {
            distance = -(&aOBB.GetRadius().x)[i];
        }
        result = result + (axis * distance);
    }
	*aContactPoint = result;

	return ((result - aSphere.GetPosition()).Length2() <= aSphere.GetRadius()*aSphere.GetRadius());
}

#pragma once

#include "Vector.h"

class CColliderSphere;
class CColliderOBB;

namespace ICollision
{
	bool LineVsLine(
		const CommonUtilities::Vector3f& aPoint0,
		const CommonUtilities::Vector3f& aPoint1,
		const CommonUtilities::Vector3f& aPoint2,
		const CommonUtilities::Vector3f& aPoint3,
		CommonUtilities::Vector3f& aIntersection
	);
	bool RayVsLine(
		const CommonUtilities::Vector3f& aPoint0,
		const CommonUtilities::Vector3f& aPoint1,
		const CommonUtilities::Vector3f& aRayOrigin,
		const CommonUtilities::Vector3f& aRayDirection,
		CommonUtilities::Vector3f& aIntersection
	);
	bool PointVsTriangle(
		const CommonUtilities::Vector3f aVertex0,
		const CommonUtilities::Vector3f aVertex1,
		const CommonUtilities::Vector3f aVertex2,
		const CommonUtilities::Vector3f aPoint
	);
	bool SphereVsSphere(
		const CColliderSphere& aSphere0,
		const CColliderSphere& aSphere1,
		CommonUtilities::Vector3f* aContactPoint = nullptr
	);
	bool SphereVsOBB(
		const CColliderSphere& aSphere,
		const CColliderOBB& aOBB,
		CommonUtilities::Vector3f* aContactPoint = nullptr
	);
}


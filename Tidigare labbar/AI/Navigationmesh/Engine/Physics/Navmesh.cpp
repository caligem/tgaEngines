#include "stdafx.h"
#include "Navmesh.h"
#include "ICollision.h"

CNavmesh::CNavmesh()
{
}


CNavmesh::~CNavmesh()
{
}

void CNavmesh::Init()
{
	myCurrentEdges.Init(12);
	myTriangles.Init(12);
	myEdges.Init(12);
	myVertices.Init(12);

	myVertices.Add({ -1.f, 1.f, 0.f });
	myVertices.Add({ 1.f, 1.f, 0.f });
	myVertices.Add({ -1.f, -1.f, 0.f });
	myVertices.Add({ 1.f, -1.f, 0.f });

	SEdge topEdge;
	topEdge.t0 = 0;
	topEdge.t1 = -1;
	topEdge.v0 = 0;
	topEdge.v1 = 1;
	myEdges.Add(topEdge);

	SEdge leftEdge;
	leftEdge.t0 = 1;
	leftEdge.t1 = -1;
	leftEdge.v0 = 0;
	leftEdge.v1 = 2;
	myEdges.Add(leftEdge);

	SEdge rightEdge;
	rightEdge.t0 = 0;
	rightEdge.t1 = -1;
	rightEdge.v0 = 1;
	rightEdge.v1 = 3;
	myEdges.Add(rightEdge);

	SEdge bottomEdge;
	bottomEdge.t0 = 1;
	bottomEdge.t1 = -1;
	bottomEdge.v0 = 2;
	bottomEdge.v1 = 3;
	myEdges.Add(bottomEdge);

	SEdge middleEdge;
	middleEdge.t0 = 0;
	middleEdge.t1 = 1;
	middleEdge.v0 = 0;
	middleEdge.v1 = 3;
	myEdges.Add(middleEdge);

	STriangle topRight;
	topRight.v0 = 0;
	topRight.v1 = 1;
	topRight.v2 = 3;
	topRight.e0 = 0;
	topRight.e1 = 2;
	topRight.e2 = 4;
	myTriangles.Add(topRight);

	STriangle bottomLeft;
	bottomLeft.v0 = 0;
	bottomLeft.v1 = 2;
	bottomLeft.v2 = 3;
	bottomLeft.e0 = 1;
	bottomLeft.e1 = 3;
	bottomLeft.e2 = 4;
	myTriangles.Add(bottomLeft);
}

void CNavmesh::SplitNavmesh(const CommonUtilities::Vector3f& aStart, const CommonUtilities::Vector3f& aEnd)
{
	myCurrentEdges.RemoveAll();

	CommonUtilities::Vector3f collisionPoint;

	for (int i = 0; i < myEdges.Size(); ++i)
	{
		if(ICollision::LineVsLine(aStart, aEnd, myVertices[myEdges[i].v0], myVertices[myEdges[i].v1], collisionPoint))
		{
			if (myCurrentEdges.Find(i) == myCurrentEdges.FoundNone)
			{
				myCurrentEdges.Add(i);
			}
		}
	}

	for (int i = 0; i < myTriangles.Size(); ++i)
	{
		auto& triangle = myTriangles[i];

		auto& v0 = GetVertex(triangle.v0);
		auto& v1 = GetVertex(triangle.v1);
		auto& v2 = GetVertex(triangle.v2);

		if (ICollision::PointVsTriangle(v0, v1, v2, aStart))
		{
			AddEdgesFromTriangle(i, aStart, aEnd);
		}
		if (ICollision::PointVsTriangle(v0, v1, v2, aEnd))
		{
			AddEdgesFromTriangle(i, aStart, aEnd);
		}
	}

	for (auto i : myCurrentEdges)
	{
		SplitEdge(i, aStart, aEnd);
	}
}

void CNavmesh::AddEdgesFromTriangle(int aTriangleIndex, const CommonUtilities::Vector3f& aStart, const CommonUtilities::Vector3f& aEnd)
{
	CommonUtilities::Vector3f intersectionPoint;

	auto& triangle = myTriangles[aTriangleIndex];

	for (int i = 0; i < 3; ++i)
	{
		int edgeIndex = triangle.myEdges[i];
		auto& edge = myEdges[edgeIndex];

		auto v0 = GetVertex(edge.v0);
		auto v1 = GetVertex(edge.v1);

		if (ICollision::RayVsLine(v0, v1, aStart, aEnd, intersectionPoint))
		{
			if (myCurrentEdges.Find(edgeIndex) == myCurrentEdges.FoundNone)
			{
				myCurrentEdges.Add(edgeIndex);
			}
		}
	}
}

void CNavmesh::SplitEdge(int aEdgeIndex, const CommonUtilities::Vector3f& aStart, const CommonUtilities::Vector3f& aEnd)
{
	int edgeIndex0 = aEdgeIndex;

	auto v0 = GetVertex(GetEdge(edgeIndex0).v0);
	auto v1 = GetVertex(GetEdge(edgeIndex0).v1);

	CommonUtilities::Vector3f intersectionPoint;
	if (!ICollision::RayVsLine(v0, v1, aStart, aEnd, intersectionPoint))
	{
		return;
	}

	myVertices.Add(intersectionPoint);
	int vertexIndex0 = GetEdge(edgeIndex0).v0;
	int vertexIndex1 = GetEdge(edgeIndex0).v1;
	int vertexIndex2 = myVertices.Size() - 1;

	myEdges.EmplaceBack();
	int edgeIndex1 = myEdges.Size() - 1;
	GetEdge(edgeIndex0).v0 = vertexIndex0;
	GetEdge(edgeIndex0).v1 = vertexIndex2;
	GetEdge(edgeIndex1).v0 = vertexIndex2;
	GetEdge(edgeIndex1).v1 = vertexIndex1;

	
	for (int i = 0; i < 2; ++i)
	{
		int triangleIndex0 = GetEdge(edgeIndex0).myTriangles[i];
		if (triangleIndex0 == -1)
		{
			continue;
		}

		int vertexIndex3 = GetTriangle(triangleIndex0).v0;
		for (int j = 1; j < 3; ++j)
		{
			int vertex = GetTriangle(triangleIndex0).myVertices[j];
			if (vertex != vertexIndex0 && vertex != vertexIndex1)
			{
				vertexIndex3 = vertex;
				break;
			}
		}

		myEdges.EmplaceBack();
		int edgeIndex2 = myEdges.Size() - 1;
		GetEdge(edgeIndex2).v0 = vertexIndex2;
		GetEdge(edgeIndex2).v1 = vertexIndex3;

		myTriangles.EmplaceBack();
		int triangleIndex1 = myTriangles.Size() - 1;

		int edgeIndex3 = GetTriangle(triangleIndex0).e0;
		for (int j = 1; j < 3; ++j)
		{
			int edgeIndex = GetTriangle(triangleIndex0).myEdges[j];
			auto& edge = GetEdge(edgeIndex);
			if (edge.v0 == vertexIndex1 || edge.v1 == vertexIndex1)
			{
				continue;
			}
			edgeIndex3 = edgeIndex;
		}

		int edgeIndex4 = GetTriangle(triangleIndex0).e0;
		for (int j = 1; j < 3; ++j)
		{
			int edgeIndex = GetTriangle(triangleIndex0).myEdges[j];
			auto& edge = GetEdge(edgeIndex);
			if (edge.v0 == vertexIndex0 || edge.v1 == vertexIndex0)
			{
				continue;
			}
			edgeIndex4 = edgeIndex;
		}

		GetTriangle(triangleIndex0).v0 = vertexIndex0;
		GetTriangle(triangleIndex0).v1 = vertexIndex3;
		GetTriangle(triangleIndex0).v2 = vertexIndex2;
		GetTriangle(triangleIndex0).e0 = edgeIndex0;
		GetTriangle(triangleIndex0).e1 = edgeIndex3;
		GetTriangle(triangleIndex0).e2 = edgeIndex2;

		GetTriangle(triangleIndex1).v0 = vertexIndex2;
		GetTriangle(triangleIndex1).v1 = vertexIndex3;
		GetTriangle(triangleIndex1).v2 = vertexIndex1;
		GetTriangle(triangleIndex1).e0 = edgeIndex4;
		GetTriangle(triangleIndex1).e1 = edgeIndex1;
		GetTriangle(triangleIndex1).e2 = edgeIndex2;

		GetEdge(edgeIndex0).myTriangles[i] = triangleIndex0;
		GetEdge(edgeIndex1).myTriangles[i] = triangleIndex1;

		GetEdge(edgeIndex2).t0 = triangleIndex0;
		GetEdge(edgeIndex2).t1 = triangleIndex1;

		if (GetEdge(edgeIndex4).t0 == triangleIndex0)
		{
			GetEdge(edgeIndex4).t0 = triangleIndex1;
		}
		else if (GetEdge(edgeIndex4).t1 == triangleIndex0)
		{
			GetEdge(edgeIndex4).t1 = triangleIndex1;
		}
	}

}

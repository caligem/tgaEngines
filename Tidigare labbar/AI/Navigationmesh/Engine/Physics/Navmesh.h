#pragma once
#include "Vector.h"
#include "GrowingArray.h"

class CNavmesh
{
public:
	CNavmesh();
	~CNavmesh();

#pragma warning(disable: 4201)
	struct STriangle
	{
		STriangle()
		{
			v0 = v1 = v2 = -1;
			e0 = e1 = e2 = -1;
		}

		union
		{
			struct
			{
				int myEdges[3];
				int myVertices[3];
			};
			struct
			{
				int e0, e1, e2;
				int v0, v1, v2;
			};
		};
	};

	struct SEdge
	{
		SEdge()
		{
			v0 = v1 = -1;
			t0 = t1 = -1;
		}

		union
		{
			struct
			{
				int myTriangles[2];
				int myVertices[2];
			};
			struct
			{
				int t0, t1;
				int v0, v1;
			};
		};
	};

#pragma warning(default: 4201)

	void Init();

	int GetNumVertices() const { return myVertices.Size(); }
	int GetNumEdges() const { return myEdges.Size(); }
	int GetNumTriangles() const { return myTriangles.Size(); }
	inline const CommonUtilities::Vector3f& GetVertexAt(int aIndex) const { return myVertices[aIndex]; }
	inline const CNavmesh::SEdge& GetEdgeAt(int aIndex) const { return myEdges[aIndex]; }
	inline const CNavmesh::STriangle& GetTriangleAt(int aIndex) const { return myTriangles[aIndex]; }

	void SplitNavmesh(const CommonUtilities::Vector3f& aStart, const CommonUtilities::Vector3f& aEnd);

private:
	inline CommonUtilities::Vector3f& GetVertex(int aIndex) { return myVertices[aIndex]; }
	inline CNavmesh::SEdge& GetEdge(int aIndex) { return myEdges[aIndex]; }
	inline CNavmesh::STriangle& GetTriangle(int aIndex) { return myTriangles[aIndex]; }

	void AddEdgesFromTriangle(int aTriangleIndex, const CommonUtilities::Vector3f& aStart, const CommonUtilities::Vector3f& aEnd);
	void SplitEdge(int aEdgeIndex, const CommonUtilities::Vector3f& aStart, const CommonUtilities::Vector3f& aEnd);

	CommonUtilities::GrowingArray<CommonUtilities::Vector3f, int> myVertices;
	CommonUtilities::GrowingArray<SEdge, int> myEdges;
	CommonUtilities::GrowingArray<STriangle, int> myTriangles;

	CommonUtilities::GrowingArray<int, int> myCurrentEdges;

};


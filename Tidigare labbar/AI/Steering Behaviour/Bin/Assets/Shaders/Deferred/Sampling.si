#include "../CameraBuffer.si"

Texture2D textureBuffer[4] : register(t0);

Texture2D normalTexture : register(t0);
Texture2D albedoTexture : register(t1);
Texture2D rmaTexture : register(t2);
Texture2D emissiveTexture : register(t3);
TextureCube environmentTexture : register(t4);

SamplerState instanceSampler : register(s0);

//Texture Sampling
float4 SampleTexture(uint index, in float2 aUV)
{
	return textureBuffer[index].Sample(instanceSampler, aUV);
}
float3 ViewPosition(in float2 aUV)
{
	float depth = normalTexture.Sample(instanceSampler, aUV).w;

	float4 position;
	position.x = aUV.x * 2.f - 1.f;
	position.y = (1.f - aUV.y) * 2.f - 1.f;
	position.z = depth;
	position.w = 1.f;

	position = mul(invertedProjection, position);

	return position.xyz / position.w;
}
float3 Position(in float2 aUV)
{
	float depth = normalTexture.Sample(instanceSampler, aUV).w;

	float4 position;
	position.x = aUV.x * 2.f - 1.f;
	position.y = (1.f - aUV.y) * 2.f - 1.f;
	position.z = depth;
	position.w = 1.f;

	position = mul(invertedViewProjection, position);

	return position.xyz / position.w;
}
float3 ObjectNormal(in float2 aUV)
{
	return normalTexture.Sample(instanceSampler, aUV).xyz;
}
float4 Albedo(in float2 aUV)
{
	return albedoTexture.Sample(instanceSampler, aUV).rgba;
}
float4 RMA(in float2 aUV)
{
	return rmaTexture.Sample(instanceSampler, aUV);
}
float Roughness(in float2 aUV)
{
	return RMA(aUV).r;
}
float Metalness(in float2 aUV)
{
	return RMA(aUV).g;
}
float AmbientOcclusion(in float2 aUV)
{
	return RMA(aUV).b;
}
float3 Emissive(in float2 aUV)
{
	return emissiveTexture.Sample(instanceSampler, aUV).rgb;
}
float3 Environment(in float3 aDirection, in float aSampleLevel)
{
	return environmentTexture.SampleLevel(instanceSampler, aDirection, aSampleLevel).rgb;
}

float3 MetalnessAlbedo(in float2 aUV)
{
	float4 albedo = Albedo(aUV);
	float metalness = Metalness(aUV);
	return albedo.rgb - (albedo.rgb * metalness);
}
float3 Substance(in float2 aUV)
{
	float4 albedo = Albedo(aUV);
	float metalness = Metalness(aUV);
	return (0.04f - (0.04f*metalness)) + albedo.rgb * metalness;
}

float3 PosFromMatrix(in float4x4 aMatrix)
{
	return float3(aMatrix._14, aMatrix._24, aMatrix._34);
}

#include "../PPFX/PPFX.si"
#include "../CameraBuffer.si"

cbuffer InstanceData : register(b1)
{
	float4x4 toWorld;
	float2 scale;
}

PixelInput main(VertexInput input)
{
	PixelInput output;
	
	input.myPosition.w = 1.f;
	input.myPosition.xy *= scale.xy;
	output.myPosition = mul(toWorld, input.myPosition);
	output.myPosition = mul(viewProjection, output.myPosition);
		
	output.myUV = input.myUV;

	return output;
}
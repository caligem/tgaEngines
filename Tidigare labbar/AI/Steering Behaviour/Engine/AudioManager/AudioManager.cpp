#include "AudioManager.h"
#include <string>
#include "FMOD/fmod_studio.h"
#include "FMOD/fmod.hpp"
#include "FMOD/fmod_common.h"

#ifndef _RETAIL
#include <iostream>
#endif

#pragma comment (lib, "../Lib/fmodstudio64_vc.lib")
AudioManager::AudioManager() : myMaxChannels(200)
{
	myAudioSystem = nullptr;
	myDefaultAudioListener.SetPosition({ 0.0f, 0.0f, 0.0f });
	myAudioListener = &myDefaultAudioListener;
	
	myErrorResultChecker = FMOD::Studio::System::create(&myAudioSystem);
	assert(myErrorResultChecker == FMOD_OK && "Error creating audio system");
	
	myErrorResultChecker = myAudioSystem->initialize(myMaxChannels, FMOD_STUDIO_INIT_NORMAL, FMOD_INIT_NORMAL, 0);
	assert(myErrorResultChecker == FMOD_OK && "Error initializing audio system");

	myMasterVolume = 100.0f;
	myMusicVolume = 100.0f;
	mySoundEffectsVolume = 100.0f;

}


AudioManager & AudioManager::GetInstance()
{
	static AudioManager instance;

	return instance;
}

AudioManager::~AudioManager()
{
	myAudioSystem->release();
}

void AudioManager::LoadAudioBank(const char* aBankFilePath)
{
	std::string bankName = aBankFilePath;
	std::string stringBankName = aBankFilePath;
	bankName += ".bank";
	stringBankName += ".strings.bank";
	myErrorResultChecker = myAudioSystem->loadBankFile(bankName.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &myAudioBank);
	assert(myErrorResultChecker == FMOD_OK && "Error loading bank file. Bad name?");

	myErrorResultChecker = myAudioSystem->loadBankFile(stringBankName.c_str(), FMOD_STUDIO_LOAD_BANK_NORMAL, &myStringBank);
	assert(myErrorResultChecker == FMOD_OK && "Error loading String bank file. Bad name?");
}

void AudioManager::LoadAudioEvent(const char * aAudioName, bool aRemoveAfterPlayback, AudioSource* aSource, AudioChannel aAudioChannel)
{
	std::string eventName = "event:/";
	eventName += aAudioName;


	FMOD::Studio::EventDescription* tempDescription = nullptr;
	myErrorResultChecker = myAudioSystem->getEvent(eventName.c_str(), &tempDescription);
	assert(myErrorResultChecker == FMOD_OK && "Error loading audio file");
	assert(myUsedAudioFiles < myMaxAudioFiles && "Error Audio manager Full");

	FMOD::Studio::EventInstance* tempInstance = nullptr;
	tempDescription->createInstance(&tempInstance);

	mySounds[myUsedAudioFiles].mySoundInstances = tempInstance;
	mySounds[myUsedAudioFiles].myName = aAudioName;
	mySounds[myUsedAudioFiles].myShouldBeRemoved = aRemoveAfterPlayback;
	mySounds[myUsedAudioFiles].mySource = aSource;
	mySounds[myUsedAudioFiles].myAudioChannel = aAudioChannel;
	mySounds[myUsedAudioFiles].mySoundInstances->setVolume(GetTotalVolume(aAudioChannel));
	myUsedAudioFiles++;
}

void AudioManager::UnloadAudioEvent(const char * aAudioName, AudioSource* aSource)
{
	for (unsigned int i = 0; i < myUsedAudioFiles; ++i)
	{
		if (mySounds[i].myName == aAudioName && mySounds[i].mySource == aSource)
		{
			mySounds[i].mySoundInstances->release();
			if (i == myUsedAudioFiles - 1)
			{
				myUsedAudioFiles -= 1;
				return;
			}
			else
			{
				mySounds[i] = mySounds[myUsedAudioFiles - 1];

				myUsedAudioFiles -= 1;
				return;
			}

		}
	}
}

void AudioManager::Play(const char * aAudioName, AudioSource * aSource)
{
	for (unsigned short i = 0; i < myUsedAudioFiles; ++i)
	{
		if (mySounds[i].myName == aAudioName && mySounds[i].mySource == aSource)
		{
			FMOD_STUDIO_PLAYBACK_STATE playbackState = FMOD_STUDIO_PLAYBACK_STOPPING;
			mySounds[i].mySoundInstances->getPlaybackState(&playbackState);
			if (playbackState == FMOD_STUDIO_PLAYBACK_STOPPED || playbackState == FMOD_STUDIO_PLAYBACK_STOPPING)
			{
				mySounds[i].mySoundInstances->start();
			}
			break;
		}
	}
}

void AudioManager::PlayNewInstance(const char * aAudioName, AudioSource* aSource, AudioChannel aAudioChannel)
{
	LoadAudioEvent(aAudioName, true, aSource, aAudioChannel);

	mySounds[myUsedAudioFiles - 1].mySoundInstances->start();
}

void AudioManager::Stop(const char * aAudioName, bool aShouldBeImmediate, AudioSource* aAudioSource)
{
	for (unsigned short i = 0; i < myUsedAudioFiles; ++i)
	{
		if (mySounds[i].myName == aAudioName && mySounds[i].mySource == aAudioSource)
		{
			if (aShouldBeImmediate)
			{
				mySounds[i].mySoundInstances->stop(FMOD_STUDIO_STOP_IMMEDIATE);
				break;
			}
			else
			{
				mySounds[i].mySoundInstances->stop(FMOD_STUDIO_STOP_ALLOWFADEOUT);
				break;
			}
		}
	}
}

void AudioManager::StopAll(bool aShouldBeImmediate)
{
	for (unsigned int i = 0; i < myUsedAudioFiles; ++i)
	{
		if (aShouldBeImmediate == true)
		{
			mySounds[i].mySoundInstances->stop(FMOD_STUDIO_STOP_IMMEDIATE);
		}
		else
		{
			mySounds[i].mySoundInstances->stop(FMOD_STUDIO_STOP_ALLOWFADEOUT);
		}
	}
}

void AudioManager::SetVolume(float aVolume, AudioChannel aAudioChannel)
{
	if (aVolume > 100.0f)
	{
		aVolume = 100.0f;
	}
	else if(aVolume < 0.0f)
	{
		aVolume = 0.0f;
	}
	switch (aAudioChannel)
	{
	case AudioChannel::Master:
		myMasterVolume = aVolume;
		break;
	case AudioChannel::Music:
		myMusicVolume = aVolume;
		break;
	case AudioChannel::SoundEffects:
		mySoundEffectsVolume = aVolume;
		break;
	}

	UpdateVolumes();
}

float AudioManager::GetVolume(AudioChannel aAudioChannel)
{
	switch (aAudioChannel)
	{
	case AudioChannel::Master:
		return myMasterVolume;
		break;
	case AudioChannel::Music:
		return myMusicVolume;
		break;
	case AudioChannel::SoundEffects:
		return mySoundEffectsVolume;
		break;
	}
	return -1;
}

void AudioManager::SetMute(bool aMute)
{
	myIsMuted = aMute;
	UpdateVolumes();
}

void AudioManager::SetAudioListener(AudioListener * aListener)
{
	if (aListener == nullptr)
	{
		myAudioListener = &myDefaultAudioListener;
	}
	else
	{
		myAudioListener = aListener;
	}
}

void AudioManager::RemoveConnectedAudioEvents(AudioSource * aSource)
{
	for (unsigned short eventIndex = 0; eventIndex < myUsedAudioFiles; ++eventIndex)
	{
		if (mySounds[eventIndex].mySource != nullptr && mySounds[eventIndex].mySource == aSource)
		{
			UnloadAudioEvent(mySounds[eventIndex].myName.c_str(), aSource);
		}
	}
}

void AudioManager::SetParameter(const char * aAudioName, const char * aParameterName, float aValue, AudioSource * aAudioSource)
{
	std::string eventName;
	for (unsigned short eventIndex = 0; eventIndex < myUsedAudioFiles; ++eventIndex)
	{
		eventName = aAudioName;
		if (mySounds[eventIndex].mySource == aAudioSource && mySounds[eventIndex].myName == eventName)
		{
			mySounds[eventIndex].mySoundInstances->setParameterValue(aParameterName, aValue);
			break;
		}
	}
}

void AudioManager::Update()
{
	if (myAudioListener != nullptr)
	{
		myAudioSystem->setListenerAttributes(0, myAudioListener->Get3DAttributes());
	}
	for (unsigned short eventIndex = 0; eventIndex < myUsedAudioFiles; ++eventIndex)
	{
		if (mySounds[eventIndex].mySource != nullptr)
		{
			mySounds[eventIndex].mySoundInstances->set3DAttributes(mySounds[eventIndex].mySource->Get3DAttributes());
		}
		else if(myAudioListener != nullptr)
		{
			mySounds[eventIndex].mySoundInstances->set3DAttributes(myAudioListener->Get3DAttributes());
		}
		if (mySounds[eventIndex].myShouldBeRemoved)
		{
			FMOD_STUDIO_PLAYBACK_STATE playbackState = FMOD_STUDIO_PLAYBACK_STOPPING;
			mySounds[eventIndex].mySoundInstances->getPlaybackState(&playbackState);
			if (playbackState == FMOD_STUDIO_PLAYBACK_STOPPED)
			{
				UnloadAudioEvent(mySounds[eventIndex].myName.c_str(), mySounds[eventIndex].mySource);
			}
		}
	}
	myAudioSystem->update();
}

void AudioManager::UpdateVolumes()
{
	if (myIsMuted)
	{
		for (unsigned short eventIndex = 0; eventIndex < myUsedAudioFiles; ++eventIndex)
		{
			mySounds[eventIndex].mySoundInstances->setVolume(0.0f);
		}
	}
	else
	{
		for (unsigned short eventIndex = 0; eventIndex < myUsedAudioFiles; ++eventIndex)
		{
			switch (mySounds[eventIndex].myAudioChannel)
			{
			case AudioChannel::Master:
				mySounds[eventIndex].mySoundInstances->setVolume((myMasterVolume / 100));
				break;
			case AudioChannel::Music:
				mySounds[eventIndex].mySoundInstances->setVolume((myMasterVolume / 100) * (myMusicVolume / 100));
				break;
			case AudioChannel::SoundEffects:
				mySounds[eventIndex].mySoundInstances->setVolume((myMasterVolume / 100) * (mySoundEffectsVolume / 100));
				break;
			}
		}
	}
}

float AudioManager::GetTotalVolume(AudioChannel aChannel)
{
	if (myIsMuted)
	{
		return 0;
	}
	switch (aChannel)
	{
	case AudioChannel::Master:
		return (myMasterVolume / 100);
		break;
	case AudioChannel::Music:
		return (myMasterVolume / 100) * (myMusicVolume / 100);
		break;
	case AudioChannel::SoundEffects:
		return ((myMasterVolume / 100) * (mySoundEffectsVolume / 100));
		break;
	}
	assert(true || "Invalid Audio Channel");
	return 0;
}

#pragma once

#include "FMOD/fmod_studio.hpp"
#include "AudioSource.h"
#include "AudioListener.h"
#include <string>
#include <assert.h>
#include "AudioSourcePool.h"
#include "AudioChannels.h"
#include <iostream>

#define AM AudioManager::GetInstance()

struct SoundEvent
{
	FMOD::Studio::EventInstance* mySoundInstances;
	std::string myName;
	bool myShouldBeRemoved;
	AudioSource* mySource;
	AudioChannel myAudioChannel;
};

class AudioManager
{
public:
	static AudioManager& GetInstance();

	void LoadAudioBank(const char* aBankFilePath);
	void LoadAudioEvent(const char* aAudioName, bool aRemoveAfterPlayback = false, AudioSource* aSource = nullptr, AudioChannel aAudioChannel = AudioChannel::SoundEffects);
	
	void UnloadAudioEvent(const char* aAudioName, AudioSource* aSource = nullptr);

	void Play(const char* aAudioName, AudioSource* aSource = nullptr);
	void PlayNewInstance(const char* aAudioName, AudioSource* aSource = nullptr, AudioChannel aAudioChannel = AudioChannel::SoundEffects);

	void Stop(const char* aAudioName, bool aShouldBeImmediate, AudioSource* aAudioSource = nullptr);
	void StopAll(bool aShouldBeImmediate);

	void SetVolume(float aVolume, AudioChannel aAudioChannel = AudioChannel::Master);
	float GetVolume(AudioChannel aAudioChannel);

	void SetMute(bool aMute);
	bool GetMute() { return myIsMuted; }
	void SetAudioListener(AudioListener* aListener);

	void RemoveConnectedAudioEvents(AudioSource* aSource);

	void SetParameter(const char* aAudioName, const char* aParameterName, float aValue, AudioSource* aAudioSource = nullptr);

	AudioSource* GetAudioSource() {	return myAudioSourcePool.GetAudioSource();
	}
	void ReturnAudioSource(AudioSource* aAudioSource) { myAudioSourcePool.ReturnAudioSource(aAudioSource); }

	void Update();

private:

	void UpdateVolumes();
	float GetTotalVolume(AudioChannel aChannel);
	AudioManager();
	~AudioManager();
	static const int myMaxAudioFiles = 4096;
	unsigned int myUsedAudioFiles = 0;

	FMOD_RESULT myErrorResultChecker;

	float myMasterVolume;
	float myMusicVolume;
	float mySoundEffectsVolume;
	bool myIsMuted;

	const int myMaxChannels;
	FMOD::Studio::System* myAudioSystem;
	FMOD::Studio::Bank* myAudioBank;
	FMOD::Studio::Bank* myStringBank;

	AudioListener* myAudioListener;
	AudioListener myDefaultAudioListener;
	AudioSourcePool myAudioSourcePool;

	SoundEvent mySounds[myMaxAudioFiles];
};


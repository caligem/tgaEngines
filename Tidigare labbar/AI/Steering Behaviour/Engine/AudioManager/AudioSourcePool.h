#pragma once
#include <array>
#include "AudioSource.h"
class AudioSourcePool
{
public:
	AudioSourcePool();
	~AudioSourcePool();
	AudioSource* GetAudioSource();
	int GetAmmountOfFreeAudioSources();
	void ReturnAudioSource(AudioSource* aAudioSource);
private:
	std::array<AudioSource, 4096> myAudioSourcePool;
	std::array<bool, 4096> myFreeIndexes;
};


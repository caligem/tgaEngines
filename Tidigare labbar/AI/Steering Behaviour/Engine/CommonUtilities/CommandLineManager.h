#pragma once

#include <map>
#include <vector>

namespace CommonUtilities
{
	class CCommandLineManager
	{
	public:
		CCommandLineManager() = delete;
		~CCommandLineManager() = delete;

		static void Init(int argc, wchar_t* wargv[]);
		static bool HasParameter(const char* aParameter);
		static bool HasArgument(const char* aParameter, const char* aArgument);
		static bool HasArgument(const char* aParameter, int aIndex);
		static const std::string& GetArgument(const char* aParameter, int aIndex);
	private:
		static std::map<std::string, std::vector<std::string>> myParameters;
	};
}


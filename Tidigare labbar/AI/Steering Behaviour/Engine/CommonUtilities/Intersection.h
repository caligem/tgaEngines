#pragma once

#include "Vector.h"

namespace CommonUtilities
{

	bool CircleVsCircle(const Vector2f& aPoint0, float aRadius0, const Vector2f& aPoint1, float aRadius1, CommonUtilities::Vector2f& aContactPoint);

	bool CircleVsLine(const Vector2f& aPointCircle, float aRadiusCircle, const Vector2f& aStartPointLine, const Vector2f& aEndPointLine, CommonUtilities::Vector2f& aContactPoint);

}

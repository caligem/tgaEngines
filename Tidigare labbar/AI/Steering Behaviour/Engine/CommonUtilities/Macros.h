#pragma once

#define SAFE_DELETE(P) {delete P; P = nullptr;}
#define SAFE_DELETE_ARRAY(P) {delete[] P; P = nullptr;}
//#define CYCLIC_ERASE(VEC, I) {VEC[(I)] = VEC[VEC.size()-1]; VEC.pop_back();}

#ifdef _DEBUG
#define DefineDebugPtr(T) const T* _dbgPtr_##T = nullptr;
#define AssignDebugPtr(T, aVal) _dbgPtr_##T = aVal;
#else
#define DefineDebugPtr(T)
#define AssignDebugPtr(T, aVal)
#endif

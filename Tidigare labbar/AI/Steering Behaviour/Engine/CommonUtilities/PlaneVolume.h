#pragma once

#include <vector>
#include "Vector.h"
#include "Plane.h"

namespace CommonUtilities
{
	template <typename T>
	class PlaneVolume {
	public:
		PlaneVolume (const std::vector<Plane<T>>& aPlaneList);

		void AddPlane(const Plane<T>& aPlane);

		bool Inside(const Vector3<T>& aPosition);

	private:
		std::vector<Plane<T>> myPlaneList;
	};

	template<typename T>
	inline PlaneVolume<T>::PlaneVolume(const std::vector<Plane<T>>& aPlaneList)
	{
		myPlaneList = aPlaneList;
	}

	template<typename T>
	inline void PlaneVolume<T>::AddPlane(const Plane<T>& aPlane)
	{
		myPlaneList.push_back(aPlane);
	}

	template<typename T>
	inline bool PlaneVolume<T>::Inside(const Vector3<T>& aPosition)
	{
		for (const Plane<T>& plane : myPlaneList) {
			if (!plane.Inside(aPosition))
			{
				return false;
			}
		}
		return true;
	}

}

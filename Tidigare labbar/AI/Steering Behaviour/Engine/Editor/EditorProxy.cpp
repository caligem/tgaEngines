#include "stdafx.h"
#include "EditorProxy.h"

#include "LevelEditor.h"
#include "ParticleEditor.h"
#include <DL_Debug.h>

#include "Editor.h"
#include "IWorld.h"
#include "InputManager.h"

#ifdef _DEBUG
#pragma comment(lib,"Editor_Debug.lib")
#endif // DEBUG
#ifdef _RELEASE
#pragma comment(lib,"Editor_Release.lib")
#endif // _RELEASE

#define ValidateEditor() if(myEditor == nullptr) { return; }

CEditorProxy::CEditorProxy()
{
	myEditor = nullptr;
}

CEditorProxy::~CEditorProxy()
{
}

void CEditorProxy::Init(EEditorType aEditorType, void * aHandle)
{
	switch (aEditorType)
	{
	case EEditorType_LevelEditor:
		myEditor = new CLevelEditor();
		myEditor->Init((HWND)aHandle);
		break;
	case EEditorType_ParticleEditor:
		myEditor = new CParticleEditor();
		myEditor->Init((HWND)aHandle);
		break;
	}
}

void CEditorProxy::Shutdown()
{
	ValidateEditor();
	myEditor->Shutdown();
}

void CEditorProxy::StartEngine()
{
	ValidateEditor();
	myEditor->StartEngine();
}

bool CEditorProxy::IsRunning()
{
	if (myEditor == nullptr)
	{
		return false;
	}

	return myEditor->IsRunning();
}

void CEditorProxy::SendWindowMessage(unsigned int Msg, void * wParam, void * lParam)
{
	ValidateEditor();
	myEditor->SendWindowMessage(Msg, wParam, lParam);
}

void CEditorProxy::SendMessageToEngine(unsigned char * pData, EMessageType aMessageType, EDataType aDataType)
{
	ValidateEditor();

	unsigned char* newData = nullptr;
	if (aDataType == EDataType_String)
	{
		int byteSize = *reinterpret_cast<int*>(pData);
		int offset = sizeof(int);

		newData = new unsigned char[byteSize + 1];
		memcpy_s(newData, byteSize, &pData[offset], byteSize);
		newData[byteSize] = '\0';
	}
	else
	{
		int byteSize = static_cast<int>(aDataType);
		newData = new unsigned char[byteSize];
		memcpy_s(newData, byteSize, pData, byteSize);
	}

	myEditor->RecieveMessage(newData, aMessageType, aDataType);
}

void CEditorProxy::SetEditorMessageCallback(void(*aCallback)(const unsigned char *, EMessageType, EDataType))
{
	ValidateEditor();
	myEditor->SetEditorMessageCallback(aCallback);
}

void CEditorProxy::GotFocus()
{
	ValidateEditor();
	myEditor->GotFocus();
}

void CEditorProxy::LostFocus()
{
	ValidateEditor();
	myEditor->LostFocus();
}

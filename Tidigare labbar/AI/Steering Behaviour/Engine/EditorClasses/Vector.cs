﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EditorClasses
{
    public partial class Vector2 : ISerialize
    {
        public float x;
        public float y;
        public Vector2()
        {
            x = y = 0f;
        }
        public Vector2(float aX, float aY)
        {
            x = aX;
            y = aY;
		}
		public override byte[] Serialize()
		{
			using (MemoryStream m = new MemoryStream())
			{
				using (BinaryWriter writer = new BinaryWriter(m))
				{
					writer.Write(x);
					writer.Write(y);
				}
				return m.ToArray();
			}
		}
		public void Deserialize(byte[] aData)
		{
			using (MemoryStream m = new MemoryStream(aData))
			{
				using (BinaryReader reader = new BinaryReader(m))
				{
					x = reader.ReadSingle();
					y = reader.ReadSingle();
				}
			}
		}
	}

	public partial class Vector3 : ISerialize
	{
		public float x;
		public float y;
		public float z;

		public Vector3()
		{
			x = y = z = 0f;
		}
		public Vector3(float aX, float aY, float aZ)
		{
			x = aX;
			y = aY;
			z = aZ;
		}
		public Vector3(byte[] aData)
		{
            Deserialize(aData);
		}

		public override byte[] Serialize()
		{
			using (MemoryStream m = new MemoryStream())
			{
				using (BinaryWriter writer = new BinaryWriter(m))
				{
					writer.Write(x);
					writer.Write(y);
					writer.Write(z);
				}
				return m.ToArray();
			}
		}
		public void Deserialize(byte[] aData)
		{
			using (MemoryStream m = new MemoryStream(aData))
			{
				using (BinaryReader reader = new BinaryReader(m))
				{
					x = reader.ReadSingle();
					y = reader.ReadSingle();
					z = reader.ReadSingle();
				}
			}
		}
	}

	public partial class Vector4 : ISerialize
	{
		public float x;
		public float y;
		public float z;
		public float w;

		public Vector4()
		{
			x = y = z = w = 0f;
		}
		public Vector4(float aX, float aY, float aZ, float aW)
		{
			x = aX;
			y = aY;
			z = aZ;
			w = aW;
		}
		public override byte[] Serialize()
		{
			using (MemoryStream m = new MemoryStream())
			{
				using (BinaryWriter writer = new BinaryWriter(m))
				{
					writer.Write(x);
					writer.Write(y);
					writer.Write(z);
					writer.Write(w);
				}
				return m.ToArray();
			}
		}
	}
}

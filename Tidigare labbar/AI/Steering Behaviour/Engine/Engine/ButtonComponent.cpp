#include "stdafx.h"
#include "ButtonComponent.h"
#include "GameObject.h"
#include "TextComponent.h"

CButtonComponent::CButtonComponent()
{
}


CButtonComponent::~CButtonComponent()
{
}

void CButtonComponent::Init(SComponentData aData)
{
	CGameObject tempGameObject;
	tempGameObject.Init(mySceneID, myGameObjectDataID);
	myTextComponent = tempGameObject.AddComponent<CTextComponent>({ aData.fontPath });
	mySpriteComponent = tempGameObject.AddComponent<CSpriteComponent>({ aData.spritePath });
	myTextComponent->SetPivot({ 0.5f, 0.5f });
	mySpriteComponent->SetPivot({ 0.5f, 0.5f });
}

void CButtonComponent::SetPosition(const CommonUtilities::Vector2f & aPosition)
{
	myPosition = aPosition;
	mySpriteComponent->SetPosition(myPosition);
}

void CButtonComponent::SetScaleRelativeToScreen(const CommonUtilities::Vector2f & aSize)
{
	mySizeRelativeToScreen = aSize;
	mySpriteComponent->SetScaleRelativeToScreen(aSize);
}
void CButtonComponent::SetText(const char * aText)
{
	myTextComponent->SetText(aText);
}

void CButtonComponent::SetTextPosition(const CommonUtilities::Vector2f & aTextPosition)
{
	myTextComponent->SetPosition(myPosition + aTextPosition);
}

void CButtonComponent::FillRenderCommands(CDoubleBuffer<SSpriteRenderCommand>& aSpriteRenderCommandsBuffer, CDoubleBuffer<STextRenderCommand>& aTextRenderCommandsBuffer)
{
	mySpriteComponent->FillRenderCommands(aSpriteRenderCommandsBuffer, aTextRenderCommandsBuffer);
	myTextComponent->FillRenderCommands(aSpriteRenderCommandsBuffer, aTextRenderCommandsBuffer);
}

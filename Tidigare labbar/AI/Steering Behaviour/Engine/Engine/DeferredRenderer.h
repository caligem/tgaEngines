#pragma once

#include <array>

#include "GrowingArray.h"
#include "Matrix.h"
#include "Vector.h"
#include "Plane.h"

#include "ConstantBuffer.h"

#include "Model.h"

#include "VertexDataWrapper.h"
#include "Transform.h"

class CDirectXFramework;
class CVertexShader;
class CPixelShader;
class CFullscreenTexture;

struct SModelRenderCommand;
struct ID3D11Buffer;
struct ID3D11ShaderResourceView;

struct SDirectionalLightRenderCommand;
struct SPointLightRenderCommand;
struct SSpotLightRenderCommand;

class CDeferredRenderer
{
public:
	CDeferredRenderer();
	~CDeferredRenderer();

	bool Init();
	void RenderDataPass(const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender, CConstantBuffer& aCameraBuffer, const CommonUtilities::Vector4f& aCameraPosition, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum);
	void RenderLightPass(
		CConstantBuffer aCameraBuffer,
		const CommonUtilities::GrowingArray<SDirectionalLightRenderCommand>& aDirectionalLightRenderCommands,
		const CommonUtilities::GrowingArray<SPointLightRenderCommand>& aPointLightRenderCommands,
		const CommonUtilities::GrowingArray<SSpotLightRenderCommand>& aSpotLightRenderCommands,
		CFullscreenTexture& aGBuffer,
		const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum
	);

private:
	struct SInstanceBufferData
	{
		CommonUtilities::Matrix44f myToWorld;
	};
	struct SBoneBufferData
	{
		CommonUtilities::Matrix44f myBones[64];
	};
	struct SLightBufferData
	{
		CommonUtilities::Vector4f myDirectionalLight;
		CommonUtilities::Vector4f myDirectionalLightColor;
	};
	struct SPointLightBufferData
	{
		CommonUtilities::Vector4f position;
		CommonUtilities::Vector4f color;
		float range;
		CommonUtilities::Vector3f padding;
	};
	struct SSpotLightBufferData
	{
		CommonUtilities::Vector4f position;
		CommonUtilities::Vector4f direction;
		CommonUtilities::Vector4f color;
		float range;
		float angle;
		CommonUtilities::Vector2f padding;
	};
	struct SScreenData
	{
		CommonUtilities::Vector2f myScreenSize;
		CommonUtilities::Vector2f myTrash;
	};

	bool CreateVertexBuffer();

	void RenderAmbientLight();
	void RenderDirectLights(const CommonUtilities::GrowingArray<SDirectionalLightRenderCommand>& aLightRenderCommands);
	void RenderPointLights(const CommonUtilities::GrowingArray<SPointLightRenderCommand>& aLightRenderCommands, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum);
	bool Cull(const CTransform& aTransform, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum, const float& aRadius);
	void RenderSpotLights(const CommonUtilities::GrowingArray<SSpotLightRenderCommand>& aLightRenderCommands, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum);

	CConstantBuffer mySpotLightBuffer;
	CConstantBuffer myPointLightBuffer;
	CConstantBuffer myInstanceBuffer;
	CConstantBuffer myBoneBuffer;
	CConstantBuffer myDirectionalLightBuffer;
	CConstantBuffer myScreenBuffer;

	CVertexShader* myDataPassVertexShader;
	CPixelShader* myDataPassPixelShader;

	enum ELightType
	{
		ELightType_Ambient,
		ELightType_Direct,
		ELightType_Point,
		ELightType_Spot,
		ELightType_Count
	};
	CVertexShader* myLightPassVertexShader;
	CVertexShader* myLightPassWorldVertexShader;
	std::array<CPixelShader*, ELightType_Count> myLightPassPixelShaders;

	SVertexDataWrapper myVertexData;

	CModel::SModelData myIcosphere;
};


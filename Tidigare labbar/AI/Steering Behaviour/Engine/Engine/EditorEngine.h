#pragma once
#include "Engine.h"

class CParticleEditor;
class CEditor;

class CEditorEngine : public CEngine
{
public:
	CEditorEngine();
	~CEditorEngine();

	bool Init(const SCreateParameters& aCreateParameters) override;
	void SyncJob() override;
	void RenderJob() override;

	HWND GetWindowHandle();
	void SetManipulationToolHandler(std::function<void()> aHandler) { myManipulationToolHandler = aHandler; }

private:
	friend CParticleEditor;
	friend CEditor;

	void ImGuiRenderJob();

	CommonUtilities::GrowingArray<std::function<void()>> mySyncFunctionBuffer;
	std::function<void()> myMessageHandler;
	std::function<void()> myManipulationToolHandler;
};


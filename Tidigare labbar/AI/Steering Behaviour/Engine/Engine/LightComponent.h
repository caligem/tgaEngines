#pragma once
#include "Component.h"

class CEngine;
class CGraphicsPipeline;
class CComponentSystem;

class CLightComponent : public CComponent
{
public:
	CLightComponent();
	~CLightComponent();

	enum ELightType
	{
		ELightType_Directional,
		ELightType_Point,
		ELightType_Spot
	};

	struct SComponentData
	{
		ELightType myType = ELightType_Point;
		float myRange = 1.f;
		float mySpotAngle = 45.f;
		float myIntensity = 1.f;
		CommonUtilities::Vector3f myColor = { 1.f, 1.f, 1.f };
	};

	void SetType(ELightType aType) { myData.myType = aType; }
	void SetRange(float aRange) { myData.myRange = aRange; }
	void SetIntensity(float aIntensity) { myData.myIntensity = aIntensity; }
	void SetSpotAngle(float aAngle) { myData.mySpotAngle = aAngle; }
	void SetColor(const CommonUtilities::Vector3f aColor) { myData.myColor = aColor; }

	ELightType GetType() const { return myData.myType; }
	float GetRange() const { return myData.myRange; }
	float GetIntensity() const { return myData.myIntensity; }
	float GetSpotAngle() const { return myData.mySpotAngle; }
	const CommonUtilities::Vector3f& GetColor() const { return myData.myColor; }

private:
	friend CEngine;
	friend CGraphicsPipeline;
	friend CComponentSystem;

	void Init();
	SComponentData myData;
};


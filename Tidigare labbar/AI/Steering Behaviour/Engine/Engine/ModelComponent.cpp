#include "stdafx.h"
#include "ModelComponent.h"
#include "ModelManager.h"

CModelManager* CModelComponent::ourModelManager = nullptr;

CModelComponent::CModelComponent()
{
}

CModelComponent::~CModelComponent()
{
}

void CModelComponent::Init(const SComponentData & aComponentData)
{
	myModelID = ourModelManager->AcquireModel(aComponentData.myFilePath);
	myRadius = ourModelManager->GetModel(myModelID)->myModelData.myRadius;
}

void CModelComponent::AssignMaterial(const std::wstring & aShaderFile)
{
	myMaterial.SetPixelShader(aShaderFile);
	myMaterial.SetVertexShader(aShaderFile);
}

void CModelComponent::Release()
{
	ourModelManager->ReleaseModel(myModelID);
	myModelID = ID_T_INVALID(CModel);
}

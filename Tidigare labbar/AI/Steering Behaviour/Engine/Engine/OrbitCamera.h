#pragma once

#include "Camera.h"
#include "Transform.h"
#include "Vector.h"

class COrbitCamera : public CCamera
{
public:
	COrbitCamera();
	~COrbitCamera();

	void Update();

	CTransform& GetTransform() { return myTransform; }

	void SetPosDir(const CommonUtilities::Vector3f& aPosition, const CommonUtilities::Vector3f& aDirection);
	void HandleOrbitControls();
	void HandleFpsControls();
private:
	CTransform myTransform;

	CommonUtilities::Vector3f myPivot;
	CommonUtilities::Vector2f myRotation;
	float myZoom;
};


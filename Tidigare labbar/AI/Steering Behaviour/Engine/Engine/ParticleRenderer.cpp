#include "stdafx.h"
#include "ParticleRenderer.h"

#include "IEngine.h"
#include "ShaderManager.h"
#include "ParticleManager.h"
#include "ConstantBuffer.h"

#include "TextureManager.h"

#include "RenderCommand.h"

#include "StreakComponent.h"

CParticleRenderer::CParticleRenderer()
{
	myVertexShader = nullptr;
	myPixelShader = nullptr;
	myParticleGeometryShader = nullptr;
}

CParticleRenderer::~CParticleRenderer()
{
}

bool CParticleRenderer::Init()
{
	myVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/Particle/Particle.vs", EShaderInputLayoutType_Particle);
	myPixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/Particle/Particle.ps");
	myParticleGeometryShader = &IEngine::GetShaderManager().GetGeometryShader(L"Assets/Shaders/Particle/Particle.gs");
	myStreakGeometryShader = &IEngine::GetShaderManager().GetGeometryShader(L"Assets/Shaders/Particle/Streak.gs");

	if (!InitStreakVertexBuffer())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to init StreakBuffer in CParticleRenderer!");
		return false;
	}

	return true;
}

void CParticleRenderer::RenderParticles(CGraphicsStateManager& aStateManager, const CommonUtilities::GrowingArray<SParticleSystemRenderCommand>& aParticleSystemRenderCommands, CConstantBuffer& aCameraBuffer)
{
	ID3D11DeviceContext* context = IEngine::GetContext();
	CTextureManager& textureManager = IEngine::GetTextureManager();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);
	aCameraBuffer.SetBuffer(0, EShaderType_Geometry);

	myVertexShader->Bind();
	myVertexShader->BindLayout();
	myParticleGeometryShader->Bind();
	myPixelShader->Bind();

	D3D11_MAPPED_SUBRESOURCE data;
	ZeroMemory(&data, sizeof(D3D11_MAPPED_SUBRESOURCE));

	CGraphicsStateManager::EBlendState currentBlendState = CGraphicsStateManager::EBlendState_Disabled;

	for (unsigned short i = 0; i < aParticleSystemRenderCommands.Size(); ++i)
	{
		CParticleEmitter* particleEmitter = IEngine::GetParticleManager().GetParticleEmitter(aParticleSystemRenderCommands[i].myParticleEmitterID);
		
		if (currentBlendState != particleEmitter->GetBlendState())
		{
			currentBlendState = particleEmitter->GetBlendState();
			aStateManager.SetBlendState(currentBlendState);
		}

		unsigned short particlesSize = aParticleSystemRenderCommands[i].myParticles.Size();
		if (particlesSize <= 0)
		{
			continue;
		}

		HRESULT result = context->Map(particleEmitter->myVertexData.myVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &data);
		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "Failed to Map Constant Buffer");
		}


		int copySize = particlesSize * particleEmitter->myVertexData.myStride;
		memcpy_s(data.pData, copySize, aParticleSystemRenderCommands[i].myParticles.begin(), copySize);
		context->Unmap(particleEmitter->myVertexData.myVertexBuffer, 0);

		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_POINTLIST);
		context->IASetVertexBuffers(0, 1, &particleEmitter->myVertexData.myVertexBuffer, &particleEmitter->myVertexData.myStride, &particleEmitter->myVertexData.myOffset);

		ID3D11ShaderResourceView* texture = textureManager.GetTexture(particleEmitter->myTexture);
		context->PSSetShaderResources(0, 1, &texture);

		context->Draw(particlesSize, 0);
	}

	myVertexShader->Unbind();
	myVertexShader->UnbindLayout();
	myParticleGeometryShader->Unbind();
	myPixelShader->Unbind();
}

void CParticleRenderer::RenderStreaks(CGraphicsStateManager & aStateManager, const CommonUtilities::GrowingArray<SStreakRenderCommand>& aStreakRenderCommands, CConstantBuffer & aCameraBuffer)
{
	ID3D11DeviceContext* context = IEngine::GetContext();
	CTextureManager& textureManager = IEngine::GetTextureManager();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);
	aCameraBuffer.SetBuffer(0, EShaderType_Geometry);

	myVertexShader->Bind();
	myVertexShader->BindLayout();
	myStreakGeometryShader->Bind();
	myPixelShader->Bind();

	D3D11_MAPPED_SUBRESOURCE data;
	ZeroMemory(&data, sizeof(D3D11_MAPPED_SUBRESOURCE));

	//CGraphicsStateManager::EBlendState currentBlendState = CGraphicsStateManager::EBlendState_Disabled;
	aStateManager;

	for (auto& command : aStreakRenderCommands)
	{
		CStreak* streak = IEngine::GetParticleManager().GetStreak(command.myStreakID);

		int streakSize = command.myPoints.Size();
		if (streakSize <= 0)
		{
			continue;
		}

		int hasRendered = 0;
		unsigned int leftToRender = streakSize;

		context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINESTRIP_ADJ);
		context->IASetVertexBuffers(0, 1, &myStreakVertexData.myVertexBuffer, &myStreakVertexData.myStride, &myStreakVertexData.myOffset);

		ID3D11ShaderResourceView* texture = textureManager.GetTexture(streak->myTexture);
		context->PSSetShaderResources(0, 1, &texture);

		auto dataPtr = command.myPoints.begin();

		while (hasRendered < streakSize)
		{
			HRESULT result = context->Map(myStreakVertexData.myVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &data);
			if (FAILED(result))
			{
				ENGINE_LOG(CONCOL_ERROR, "Failed to Map Constant Buffer");
			}

			int pointsToRender = CommonUtilities::Min(leftToRender, CParticleRenderer::MaxStreakBufferSize);

			unsigned int offset = 0;

			unsigned int copySize = myStreakVertexData.myStride;
			memcpy_s(data.pData, copySize, dataPtr + CommonUtilities::Max(0, hasRendered-1), copySize);
			offset += copySize;

			copySize = pointsToRender * myStreakVertexData.myStride;
			memcpy_s(reinterpret_cast<void*>(reinterpret_cast<char*>(data.pData) + offset), copySize, dataPtr + hasRendered, copySize);
			offset += copySize;

			int extra = 1;
			copySize = myStreakVertexData.myStride;
			if (hasRendered + pointsToRender == streakSize)
			{
				memcpy_s(reinterpret_cast<void*>(reinterpret_cast<char*>(data.pData) + offset), copySize, &command.myLeadingPoint, copySize);
				offset += copySize;

				memcpy_s(reinterpret_cast<void*>(reinterpret_cast<char*>(data.pData) + offset), copySize, &command.myLeadingPoint, copySize);
				extra = 3;
			}
			else
			{
				memcpy_s(reinterpret_cast<void*>(reinterpret_cast<char*>(data.pData) + offset), copySize, dataPtr+hasRendered+pointsToRender, copySize);
				offset += copySize;

				memcpy_s(reinterpret_cast<void*>(reinterpret_cast<char*>(data.pData) + offset), copySize, dataPtr+hasRendered+pointsToRender+1, copySize);
				extra = 3;
			}
			
			context->Unmap(myStreakVertexData.myVertexBuffer, 0);

			context->Draw(pointsToRender + extra, 0);

			hasRendered += pointsToRender;
			leftToRender -= pointsToRender;
		}
	}

	myVertexShader->Unbind();
	myVertexShader->UnbindLayout();
	myStreakGeometryShader->Unbind();
	myPixelShader->Unbind();
}

bool CParticleRenderer::InitStreakVertexBuffer()
{
	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = (MaxStreakBufferSize+8) * sizeof(CStreakComponent::SStreakBufferData);
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	HRESULT result = IEngine::GetDevice()->CreateBuffer(&bufferDesc, nullptr, &myStreakVertexData.myVertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer in ParticleEmitter!");
		return false;
	}

	myStreakVertexData.myStride = sizeof(CStreakComponent::SStreakBufferData);
	myStreakVertexData.myOffset = 0;

	return true;
}

#include "stdafx.h"
#include "ShaderManager.h"

CShaderManager::CShaderManager()
{
}
CShaderManager::~CShaderManager()
{
}

CVertexShader & CShaderManager::GetVertexShader(const std::wstring & aShaderFile, EShaderInputLayoutType aLayoutType)
{
	if (myVertexShaders.find(aShaderFile) == myVertexShaders.end())
	{
		myVertexShaders[aShaderFile] = CVertexShader();
		myVertexShaders[aShaderFile].CompileShader(aShaderFile, aLayoutType);
	}

	return myVertexShaders[aShaderFile];
}
CPixelShader & CShaderManager::GetPixelShader(const std::wstring & aShaderFile)
{
	if (myPixelShaders.find(aShaderFile) == myPixelShaders.end())
	{
		myPixelShaders[aShaderFile] = CPixelShader();
		myPixelShaders[aShaderFile].CompileShader(aShaderFile);
	}

	return myPixelShaders[aShaderFile];
}
CGeometryShader & CShaderManager::GetGeometryShader(const std::wstring & aShaderFile)
{
	if (myGeometryShaders.find(aShaderFile) == myGeometryShaders.end())
	{
		myGeometryShaders[aShaderFile] = CGeometryShader();
		myGeometryShaders[aShaderFile].CompileShader(aShaderFile);
	}

	return myGeometryShaders[aShaderFile];
}

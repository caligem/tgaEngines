#pragma once

#include "ObjectPool.h"
#include <map>

struct ID3D11ShaderResourceView;

struct SSRV
{
	SSRV(ID3D11ShaderResourceView* aPtr = NULL) : ptr(aPtr) {}
	ID3D11ShaderResourceView* ptr;
};
using SRV = ID_T(SSRV);

class CTextureManager
{
public:
	CTextureManager();
	~CTextureManager();

	void Init();

	SRV CreateTextureFromFile(const std::string& aFilepath);

	SRV CreateGrayCheckerBoardTexture();
	SRV CreateWhiteTexture();
	SRV CreateNormalTexture();
	SRV CreateBlackTexture();
	SRV CreatePinkCheckerBoardTexture();

	ID3D11ShaderResourceView* GetTexture(SRV aID);

private:
	std::map<std::string, SRV> myCache;
	ObjectPool<SSRV> myTextures;

};

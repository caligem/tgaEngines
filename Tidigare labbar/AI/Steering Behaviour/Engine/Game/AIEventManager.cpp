#include "stdafx.h"
#include "AIEventManager.h"


CAIEventManager::CAIEventManager()
{
}

CAIEventManager::~CAIEventManager()
{
}

void CAIEventManager::RegisterCallback(std::function<void(const CommonUtilities::Vector3f&)> aFunction)
{
	myCallback.push_back(aFunction);
}

void CAIEventManager::FireCallbacks(const CommonUtilities::Vector3f& aPosition)
{
	for (auto callback : myCallback)
	{
		callback(aPosition);
	}
}

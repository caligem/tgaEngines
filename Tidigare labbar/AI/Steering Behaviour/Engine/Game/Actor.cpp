#include "stdafx.h"
#include "Actor.h"

#include "IWorld.h"

CActor::CActor()
{
	mySpeed = 1.f;
}

CActor::~CActor()
{
}

void CActor::Update(float dt)
{
	CommonUtilities::Vector3f movement = myController->Update(myPosition);
	if (movement.Length2() > 1.f)
	{
		movement.Normalize();
	}
	movement.y = 0.f;
	myPosition +=  movement * mySpeed * dt;
}

void CActor::Render()
{
	IWorld::SetDebugColor(mySize);
	IWorld::DrawDebugWireSphere(myPosition, { 1.f, 0.0f, 1.f });
}

#include "stdafx.h"
#include "Controller.h"

#include "IWorld.h"
#include "CameraDataWrapper.h"

#include "InputManager.h"

CommonUtilities::Vector3f GetWorldPoint()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Matrix44f viewProjInv = CommonUtilities::Matrix44f::Inverse(IWorld::GetSavedCameraBuffer().myViewProjection);

	CommonUtilities::Vector2f mousePos = {
		input.GetMousePosition().x / IWorld::GetWindowSize().x,
		input.GetMousePosition().y / IWorld::GetWindowSize().y
	};
	mousePos.x = mousePos.x * 2.f - 1.f;
	mousePos.y = (1.f - mousePos.y) * 2.f - 1.f;

	CommonUtilities::Vector4f rayOrigin = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 0.f, 1.f) * viewProjInv;
	rayOrigin /= rayOrigin.w;
	CommonUtilities::Vector4f rayEnd = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 1.f, 1.f) * viewProjInv;
	rayEnd /= rayEnd.w;

	CommonUtilities::Vector3f rayDir = rayEnd - rayOrigin;
	rayDir.Normalize();

	CommonUtilities::Vector3f normal = { 0.f, 1.f, 0.f };

	float denom = normal.Dot(rayDir);

	CommonUtilities::Vector3f p0l0 = -CommonUtilities::Vector3f(rayOrigin);

	float t = p0l0.Dot(normal) / denom;

	CommonUtilities::Vector3f intersection = CommonUtilities::Vector3f(rayOrigin) + rayDir*t;

	return intersection;
}

CommonUtilities::Vector3f CAIArriveController::Update(const CommonUtilities::Vector3f & aActorPos)
{
	Input::CInputManager& input = IWorld::Input();

	if (input.IsButtonPressed(Input::Button_Left))
	{
		myTargetPosition = GetWorldPoint();
	}
	
	CommonUtilities::Vector3f output;
	output = myTargetPosition - aActorPos;

	float distance = output.Length();
	float targetSpeed = 1.f;
	if (distance < 0.01f)
	{
		targetSpeed = 0.f;
	}
	else if (distance > 0.5f)
	{
		targetSpeed = 1.f;
	}
	else
	{
		targetSpeed = distance / 0.5f;
	}

	return output.GetNormalized() * targetSpeed;
}

CommonUtilities::Vector3f CAIFleeController::Update(const CommonUtilities::Vector3f & aActorPos)
{
	const CommonUtilities::Vector3f& vallhundPos = myPollingStation.GetVallhundPosition();

	CommonUtilities::Vector3f closestSheep = myPollingStation.GetClosestSheepPosition(aActorPos);
	
	CommonUtilities::Vector3f fleeDirection = (aActorPos - vallhundPos);
	CommonUtilities::Vector3f fleeDirection2 = (aActorPos - closestSheep);

	float targetSpeed = 1.f;float targetSpeed2 = 1.f;
	if (fleeDirection.Length2() > 10.f)
	{
		targetSpeed = 0.f;
	}
	if (fleeDirection2.Length2() > 1.f)
	{
		targetSpeed2 = 0.f;
	}
	
	return fleeDirection.GetNormalized() * targetSpeed + fleeDirection2.GetNormalized() * targetSpeed2;
}

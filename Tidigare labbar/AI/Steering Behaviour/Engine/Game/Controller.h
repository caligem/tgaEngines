#pragma once

#include "Vector.h"

#include "PollingStation.h"
#include "AIEventManager.h"

class CController
{
public:
	virtual CommonUtilities::Vector3f Update(const CommonUtilities::Vector3f& aActorPos) = 0;

	void SetPosition(const CommonUtilities::Vector3f& aPosition) { myTargetPosition = aPosition;}
protected:
	CController() = default;
	virtual ~CController() = default;
	CommonUtilities::Vector3f myTargetPosition;
};

class CAIArriveController : public CController
{
public:
	CAIArriveController() = default;

	virtual CommonUtilities::Vector3f Update(const CommonUtilities::Vector3f& aActorPos) override;

private:
};

class CAIFleeController : public CController
{
public:
	CAIFleeController(CPollingStation* aPollingStation) : myPollingStation(*aPollingStation) {}

	virtual CommonUtilities::Vector3f Update(const CommonUtilities::Vector3f& aActorPos) override;

private:
	CPollingStation& myPollingStation;
};
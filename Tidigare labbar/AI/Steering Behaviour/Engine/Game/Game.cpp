#include "stdafx.h"
#include "Game.h"

#include "IWorld.h"
#include <DL_Debug.h>
#include <CommandLineManager.h>

using namespace std::placeholders;

CGame::CGame()
{
}

bool CGame::Init()
{
	unsigned short width = 1920;
	unsigned short height = 1080;

	GENERAL_LOG(CONCOL_DEFAULT, "Starting Game Init...");

	if (CommonUtilities::CCommandLineManager::HasParameter("-windowSize"))
	{
		if (CommonUtilities::CCommandLineManager::HasArgument("-windowSize", 0))
		{
			width = static_cast<unsigned short>(std::atoi(CommonUtilities::CCommandLineManager::GetArgument("-windowSize", 0).c_str()));
		}
		if (CommonUtilities::CCommandLineManager::HasArgument("-windowSize", 1))
		{
			height = static_cast<unsigned short>(std::atoi(CommonUtilities::CCommandLineManager::GetArgument("-windowSize", 1).c_str()));
		}
	}

	if (width < 800) width = 800;
	if (height < 600) height = 600;

	SCreateParameters createParameters;

	createParameters.myInitFunctionToCall = std::bind(&CGame::InitCallback, this);
	createParameters.myUpdateFunctionToCall = std::bind(&CGame::UpdateCallback, this);
	createParameters.myWndProcCallback = std::bind(&CGame::WndProc, this, _1, _2, _3, _4);

	createParameters.myCanvasWidth = width;
	createParameters.myCanvasHeight = height;

	createParameters.myEnableVSync = false;
	createParameters.myIsFullscreen = false;
	createParameters.myIsBorderless = false;

	if (CommonUtilities::CCommandLineManager::HasParameter("-borderlessFullscreen"))
	{
		createParameters.myIsFullscreen = false;
		createParameters.myIsBorderless = true;
	}
	else if (CommonUtilities::CCommandLineManager::HasParameter("-fullscreen"))
	{
		createParameters.myIsFullscreen = true;
		createParameters.myIsBorderless = false;
	}

	createParameters.myGameName = L"XCOM";

	if (!myEngine.Init(createParameters))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to init CEngine!");
		myEngine.Shutdown();
		return false;
	}

	myEngine.StartEngine();

	return true;
}

LRESULT CGame::WndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam)
{
	aHwnd; wParam; lParam;

	switch (aMessage)
	{
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
		break;
	}

	return 0;
}

void CGame::InitCallback()
{
	myGameWorld.Init();
}

void CGame::UpdateCallback()
{
	myGameWorld.Update();
}

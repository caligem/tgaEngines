#pragma once

#include <Engine.h>

#include "GameWorld.h"

class CGame
{
public:
	CGame();
	~CGame() = default;

	bool Init();

private:
	LRESULT WndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam);
	void InitCallback();
	void UpdateCallback();

	CEngine myEngine;

	CGameWorld myGameWorld;
};


#include "stdafx.h"
#include "LabbState.h"

#include "IWorld.h"

CLabbState::CLabbState()
	: myPollingStation(*this)
{
}

CLabbState::~CLabbState()
{
}

bool CLabbState::Init()
{
	CState::Init();

	CGameObject dir1;
	dir1.Init(mySceneID);
	dir1.GetTransform().SetLookDirection({ 0.f, -1.f, 0.f });
	CLightComponent* light1 = dir1.AddComponent<CLightComponent>();
	light1->SetType(CLightComponent::ELightType_Directional);
	light1->SetColor({ 1.f, 0.8f, 0.5f });

	myPlayer.SetController(new CAIArriveController);
	for (int i = 0; i < myFleeEnemies.size(); ++i)myFleeEnemies[i].SetController(new CAIFleeController(&myPollingStation));

	ResetScene();

	myMainCamera.GetTransform().SetPosition({ 0.f, 15.f, 0.f });
	myMainCamera.GetTransform().LookAt({ 0.f, 0.f, 0.f });

	return true;
}

EStateUpdate CLabbState::Update()
{
	float dt = IWorld::Time().GetDeltaTime();

	myPollingStation.Update();

	myPlayer.Update(dt);
	for (int i = 0; i < myFleeEnemies.size(); ++i)
	    myFleeEnemies[i].Update(dt);

	myPlayer.Render();
	for (int i = 0; i < myFleeEnemies.size(); ++i)
	    myFleeEnemies[i].Render();

	return EDoNothing;
}

void CLabbState::OnEnter()
{
	SetActiveScene();
}

void CLabbState::ResetScene()
{
	myPlayer.SetStartPosition({ 7.f, 0.f, 0.f });
	for (int i = 0; i < myFleeEnemies.size(); ++i)myFleeEnemies[i].SetStartPosition({std::cosf(i*1.27f), 0.f, std::sinf(i*0.93f)});

	myPlayer.SetStartSize({ 1.f, 0.f, 0.f });
	for (int i = 0; i < myFleeEnemies.size(); ++i)myFleeEnemies[i].SetStartSize({0.f, 1.f, 0.f});

	myPlayer.SetStartSpeed(3.f);
	for (int i = 0; i < myFleeEnemies.size(); ++i)myFleeEnemies[i].SetStartSpeed(3.0f);
}


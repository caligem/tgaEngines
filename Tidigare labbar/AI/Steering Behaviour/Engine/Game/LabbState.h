#pragma once
#include "State.h"

#include "Actor.h"
#include "Controller.h"
#include "PollingStation.h"
#include "AIEventManager.h"

class CLabbState : public CState
{
public:
	CLabbState();
	~CLabbState();

	bool Init() override;

	virtual EStateUpdate Update() override;

	virtual void OnEnter() override;
	void ResetScene();

	const CActor* GetPlayer() const { return &myPlayer; }
	auto& GetSheep() { return myFleeEnemies; }

private:
	CActor myPlayer;
	std::array<CActor, 100> myFleeEnemies;

	CPollingStation myPollingStation;
	
};


#include "stdafx.h"
#include "PollingStation.h"

#include "LabbState.h"

CPollingStation::CPollingStation(CLabbState& aState)
	: myGameState(aState)
{
	myFrameCount = 0;
}

CPollingStation::~CPollingStation()
{
}

void CPollingStation::Update()
{
	myFrameCount++;
}

const CommonUtilities::Vector3f & CPollingStation::GetVallhundPosition()
{
	return myGameState.GetPlayer()->GetPosition();
}

const CommonUtilities::Vector3f CPollingStation::GetClosestSheepPosition(const CommonUtilities::Vector3f& me)
{
	float closest = FLT_MAX;
	CommonUtilities::Vector3f output = me;
	for (CActor& actor : myGameState.GetSheep())
	{
		auto dist = (me - actor.GetPosition());
		if (dist.Length2() > 0.f)
		{
			if (dist.Length() < closest)
			{
				closest = dist.Length();
				output = actor.GetPosition();
			}
		}
	}
	return output;
}

#pragma once

class CLabbState;

class CPollingStation
{
public:
	CPollingStation(CLabbState& aState);
	~CPollingStation();

	void Update();

	const CommonUtilities::Vector3f& GetVallhundPosition();
	const CommonUtilities::Vector3f GetClosestSheepPosition(const CommonUtilities::Vector3f& me);

private:
	int myFrameCount;
	CLabbState& myGameState;

};


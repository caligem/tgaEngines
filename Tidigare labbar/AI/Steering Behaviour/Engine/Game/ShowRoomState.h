#pragma once
#include "State.h"
#include <GrowingArray.h>
#include <Vector.h>

class CShowRoomState : public CState
{
	struct SModelFileData
	{
		std::string myPath;
		long long myTimeSinceEpoch;
	};

public:
	CShowRoomState();
	~CShowRoomState();

	bool Init();
	EStateUpdate Update() override;
	void OnEnter() override;
	void OnLeave() override;
private:
	void HandleControls();
	void GetModelPaths();
	void GetFBXPathInFolder(const wchar_t* aFolderPath);
	void LoadModels();

	CGameObject myControlsInfo;

	CommonUtilities::Vector3f myPivot;
	CommonUtilities::Vector2f myRotation;
	float myZoom;

	CommonUtilities::GrowingArray<CommonUtilities::Vector3f> myCyclePositions;
	unsigned short myCycleIndex;
	bool myShouldPop = false;

	CommonUtilities::GrowingArray<SModelFileData> myModelFileDatas;

	CCanvasComponent* comp;
	CTextComponent *controlsInfoTxt;
};


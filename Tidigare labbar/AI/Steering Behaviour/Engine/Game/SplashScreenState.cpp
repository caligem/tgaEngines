#include "stdafx.h"
#include "SplashScreenState.h"

#include "IWorld.h"
#include "XBOXController.h"

CSplashScreenState::CSplashScreenState()
{
}

CSplashScreenState::~CSplashScreenState()
{
}

void CSplashScreenState::Init(const std::string & aSpritePath)
{
	CState::Init();
	
	myBlackBackground.Init(mySceneID);
	CSpriteComponent* black = myBlackBackground.AddComponent<CSpriteComponent>({ "Assets/Sprites/SplashScreen/black.dds" });
	black->SetPosition({ 0.5f, 0.5f });
	black->SetPivot({ 0.5f, 0.5f });
	black->SetScaleRelativeToScreen({ 1.0f, 1.0f });
	black->SetTint({ 0.f, 0.f, 0.f, 0.9f });

	mySplash.Init(mySceneID);
	myLogo = mySplash.AddComponent<CSpriteComponent>({ aSpritePath.c_str() });
	myLogo->SetPosition({ 0.5f, 0.5f });
	myLogo->SetPivot({ 0.5f, 0.5f });
	myLogo->SetScale({ 0.9f, 0.9f });
	myLogo->SetTint({ 1.f, 1.f, 1.f, 0.f});
}

EStateUpdate CSplashScreenState::Update()
{
	float dt = IWorld::Time().GetDeltaTime();

	myLogo->SetScale(CommonUtilities::Lerp(myLogo->GetScale(), { 1.f, 1.f }, dt));
	
	myLogo->SetTint({ 1.f, 1.f, 1.f,
		std::sinf(myTimer/myDuration * CommonUtilities::Pif)
	});

	Input::CInputManager& input = IWorld::Input();

	myTimer += dt;
	if (
		myTimer > myDuration ||
		input.IsKeyPressed(Input::Key_Space) ||
		input.IsKeyPressed(Input::Key_Return) ||
		input.IsKeyPressed(Input::Key_Escape) ||
		input.IsButtonPressed(Input::Button_Left) ||
		input.IsButtonPressed(Input::Button_Right) ||
		IWorld::XBOX().IsButtonPressed(CommonUtilities::XButton::XButton_A)
	)
	{
		return EPop_Sub;
	}

	return EDoNothing;
}

void CSplashScreenState::OnEnter()
{
	SetActiveScene();
}

void CSplashScreenState::OnLeave()
{
}

#include "stdafx.h"
#include "TestRoomState.h"
#include "Iworld.h"

#include "CameraDataWrapper.h"

CTestRoomState::CTestRoomState()
	: myShouldPop(false)
{
}


CTestRoomState::~CTestRoomState()
{
}

bool CTestRoomState::Init()
{
	CState::Init();

	LoadFromFile("Assets/Levels/mainMenu.json");

	myPlayer.Init(mySceneID);
	myPlayer.AddComponent<CModelComponent>({ "Assets/Models/playerCharacter/playerCharacter.fbx" });
	myPlayer.AddComponent<CAnimationControllerComponent>();
	myPlayer.AddComponent<CStreakComponent>();
	myCompanion.Init(mySceneID);
	myCompanion.AddComponent<CModelComponent>({ "Assets/Models/companion/companion.fbx" });
	myCompanion.AddComponent<CStreakComponent>();

	CGameObject dir1;
	dir1.Init(mySceneID);
	dir1.GetTransform().SetLookDirection({ -1.f, 1.f, 1.f });
	CLightComponent* light1 = dir1.AddComponent<CLightComponent>();
	light1->SetType(CLightComponent::ELightType_Directional);
	light1->SetColor({ 1.f, 0.8f, 0.5f });

	CGameObject dir2;
	dir2.Init(mySceneID);
	dir2.GetTransform().SetLookDirection({ 1.f, 0.f, -1.f });
	CLightComponent* light2 = dir2.AddComponent<CLightComponent>();
	light2->SetType(CLightComponent::ELightType_Directional);
	light2->SetColor({ 0.5f, 0.8f, 1.f });

	CGameObject dir3;
	dir3.Init(mySceneID);
	dir3.GetTransform().SetLookDirection({ 0.f, -1.f, 0.f });
	CLightComponent* light3 = dir3.AddComponent<CLightComponent>();
	light3->SetType(CLightComponent::ELightType_Directional);
	light3->SetColor({ 0.9f, 0.9f, 0.9f });

	myPivot = { 0.f, 0.f, 0.f };
	myRotation = { 0.f, 0.f };
	myZoom = 10.f;

	return true;
}

EStateUpdate CTestRoomState::Update()
{
	//HandleControls();

	UpdatePlayerControls();
	UpdateCamera();

	/*Input::CInputManager& input = IWorld::Input();
	CommonUtilities::Vector2f mousePos = input.GetMousePosition();
	const CommonUtilities::Vector2f& canvasSize = IWorld::GetCanvasSize();
	printf("X: %f, Y: %f \n", mousePos.x / canvasSize.x, mousePos.y / canvasSize.y);
	*/
	if (myShouldPop)
	{
		return EPop_Main;
	}
	return EDoNothing;
}

void CTestRoomState::OnEnter()
{
	SetActiveScene();
}

void CTestRoomState::OnLeave()
{
}

void CTestRoomState::HandleControls()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Vector2f movement = input.GetMouseMovement();
	movement.x *= (2.f*CommonUtilities::Pif) / 1280.f;
	movement.y *= (CommonUtilities::Pif) / 720.f;

	if (input.IsKeyDown(Input::Key_Alt))
	{
		if (input.IsButtonDown(Input::Button_Left))
		{
			myRotation.x += movement.y;
			myRotation.y += movement.x;

			if (myRotation.x >= CommonUtilities::Pif / 2.f)
			{
				myRotation.x = CommonUtilities::Pif / 2.f - 0.001f;
			}
			if (myRotation.x <= -CommonUtilities::Pif / 2.f)
			{
				myRotation.x = -CommonUtilities::Pif / 2.f + 0.001f;
			}
		}
		else if (input.IsButtonDown(Input::Button_Middle))
		{
			myPivot -= myMainCamera.GetTransform().GetRight() * movement.x * myZoom;
			myPivot += myMainCamera.GetTransform().GetUp() * movement.y * myZoom;
		}
		else if (input.IsButtonDown(Input::Button_Right))
		{
			myZoom -= (movement.x + movement.y) * myZoom;
			if (myZoom < 0.1f)
			{
				myZoom = 0.1f;
				myPivot += myMainCamera.GetTransform().GetForward() * (movement.x + movement.y);
			}
		}
	}

	if (input.IsKeyPressed(Input::Key_Escape))
	{
		myShouldPop = true;
	}

	CommonUtilities::Vector3f newPos(0.f, 0.f, -myZoom);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundX(myRotation.x);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundY(myRotation.y);
	newPos += myPivot;

	myMainCamera.GetTransform().SetPosition(newPos);
	myMainCamera.GetTransform().LookAt(myPivot);
}

void CTestRoomState::UpdatePlayerControls()
{
	Input::CInputManager& input = IWorld::Input();

	if (input.IsButtonDown(Input::Button_Right))
	{
		CommonUtilities::Vector3f worldPosition = GetWorldPoint();

		myPlayerTargetPosition = worldPosition;
		myPlayer.GetTransform().LookAt(myPlayerTargetPosition);
	}

	float dt = IWorld::Time().GetDeltaTime();
	static float speed = 2.75f;

	CommonUtilities::Vector3f movement = myPlayerTargetPosition - myPlayer.GetTransform().GetPosition();
	movement.Normalize();
	if (speed * dt < (myPlayerTargetPosition - myPlayer.GetTransform().GetPosition()).Length())
	{
		myPlayer.GetTransform().Move(movement*speed*dt);
		myPlayer.GetComponent<CAnimationControllerComponent>()->SetAnimation("walk");
	}
	else
	{
		myPlayer.GetTransform().SetPosition(myPlayerTargetPosition);
		myPlayer.GetComponent<CAnimationControllerComponent>()->SetAnimation("idle");
	}
	float t = IWorld::Time().GetTotalTime()*2.f;
	myCompanion.GetTransform().SetPosition(
		CommonUtilities::Lerp(
			myCompanion.GetTransform().GetPosition(),
			myPlayer.GetTransform().GetPosition() + CommonUtilities::Vector3f(5.f, 0.f, 0.f) * std::cosf(t) + std::sinf(t) *  CommonUtilities::Vector3f(0.f, 0.f, 5.f) + myPlayer.GetTransform().GetUp()*3.f,
			dt*5.f
		)
	);
}

void CTestRoomState::UpdateCamera()
{
	CommonUtilities::Vector3f pos = myMainCamera.GetTransform().GetPosition();
	CommonUtilities::Vector3f target = myPlayer.GetTransform().GetPosition();
	target.x += 10.f;
	target.y += 15.f;
	target.z -= 10.f;

	myMainCamera.GetTransform().SetPosition(CommonUtilities::Lerp(pos, target, IWorld::Time().GetDeltaTime()*3.f));
	myMainCamera.GetTransform().LookAt(myPlayer.GetTransform().GetPosition());
}

CommonUtilities::Vector3f CTestRoomState::GetWorldPoint()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Matrix44f viewProjInv = CommonUtilities::Matrix44f::Inverse(IWorld::GetSavedCameraBuffer().myViewProjection);

	CommonUtilities::Vector2f mousePos = {
		input.GetMousePosition().x / IWorld::GetWindowSize().x,
		input.GetMousePosition().y / IWorld::GetWindowSize().y
	};
	mousePos.x = mousePos.x * 2.f - 1.f;
	mousePos.y = (1.f - mousePos.y) * 2.f - 1.f;

	CommonUtilities::Vector4f rayOrigin = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 0.f, 1.f) * viewProjInv;
	rayOrigin /= rayOrigin.w;
	CommonUtilities::Vector4f rayEnd = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 1.f, 1.f) * viewProjInv;
	rayEnd /= rayEnd.w;

	CommonUtilities::Vector3f rayDir = rayEnd - rayOrigin;
	rayDir.Normalize();

	CommonUtilities::Vector3f normal = { 0.f, 1.f, 0.f };

	float denom = normal.Dot(rayDir);

	CommonUtilities::Vector3f p0l0 = -CommonUtilities::Vector3f(rayOrigin);

	float t = p0l0.Dot(normal) / denom;

	CommonUtilities::Vector3f intersection = CommonUtilities::Vector3f(rayOrigin) + rayDir*t;

	return intersection;
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KamiLauncher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class KamiLauncherWindow
    {
        public KamiLauncherWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            ComboBoxItem item = comboBox.Items.GetItemAt(comboBox.SelectedIndex) as ComboBoxItem;
            int width = int.Parse(item.Content.ToString().Split('x')[0]);
            int height = int.Parse(item.Content.ToString().Split('x')[1]);

			string[] windowTypes = new string[]
			{
				"-windowed",
				"-borderlessFullscreen",
				"-fullscreen"
			};

            System.Diagnostics.Process.Start("Kami.exe", "-windowSize " + width + " " + height + " " + windowTypes[comboBox_Copy.SelectedIndex]);
            Environment.Exit(0);
        }
    }
}

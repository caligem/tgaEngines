#pragma once

#include "Vector.h"
#include "Matrix.h"

class CColliderOBB
{
public:
	CColliderOBB();
	~CColliderOBB();

	inline void SetPosition(const CommonUtilities::Vector3f& aPosition) { myPosition = aPosition; }
	inline void SetRadius(const CommonUtilities::Vector3f& aRadius) { myRadius = aRadius; }
	inline void SetOrientation(const CommonUtilities::Vector3f& aOrientation) { myOrientation = CommonUtilities::Matrix44f::CreateYawPitchRoll(aOrientation); myRotation = aOrientation; }

	inline const CommonUtilities::Vector3f& GetPosition() const { return myPosition; }
	inline const CommonUtilities::Vector3f& GetRadius() const { return myRadius; }
	inline const CommonUtilities::Matrix33f& GetOrientation() const { return myOrientation; }
	inline const CommonUtilities::Vector3f& GetRotation() const { return myRotation; }

private:
	CommonUtilities::Vector3f myPosition;
	CommonUtilities::Vector3f myRadius;
	CommonUtilities::Matrix33f myOrientation;
	CommonUtilities::Vector3f myRotation;
};


#include "Particle.si"

[maxvertexcount(4)]
void main(point GeometryInput input[1], inout TriangleStream<PixelInput> output)
{
	const float4 offset[4] = 
	{
		{ -input[0].mySize.x, input[0].mySize.y, 0, 0 },
		{ input[0].mySize.x, input[0].mySize.y, 0, 0 },
		{ -input[0].mySize.x, -input[0].mySize.y, 0, 0 },
		{ input[0].mySize.x, -input[0].mySize.y, 0, 0 }
	};
	
	const float2 uv[4] = 
	{
		{ 0, 1 },
		{ 1, 1 },
		{ 0, 0 },
		{ 1, 0 }
	};
	
	float c = cos(input[0].myRotation);
	float s = sin(input[0].myRotation);
	
	float2x2 rotMat = float2x2(
		c,s,
		-s,c
	);
	
	for(int i = 0; i < 4; ++i)
	{
		PixelInput vertex;
		
		vertex.myPosition.xy = input[0].myPosition.xy + mul(rotMat, offset[i].xy);
		vertex.myPosition.zw = input[0].myPosition.zw;
		vertex.myPosition = mul(toProjection, vertex.myPosition);
		vertex.myColor = input[0].myColor;
		vertex.myUV = uv[i];
		output.Append(vertex);
	}
	output.RestartStrip();
}
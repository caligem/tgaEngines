#pragma once

class CEditorProxy;

#include "PostMasterMessageType.cs"

typedef void(*EditorCallbackMessageHandler)(EMessageType aMessageType, EDataType aDataType);

public ref class CEditorBridgeParticleEditor
{
public:
	CEditorBridgeParticleEditor();

	void Init(int aEditorType, System::IntPtr aHandle);
	void WndProc(System::Windows::Forms::Message aMessage);

	void Shutdown();

	bool IsRunning();
	bool GetHasFinishedLoading();

	void SaveFile(System::String^ aFilePath);
	void LoadFile(System::String^ aFilePath, System::IntPtr aCallback);

	void ResetParticleToDefault();

	void ToggleDebugLines();
	void ToggleShowPlayerModel();
	bool IsPlaying();

	void ResetCameraPivot();
	void ResetCameraScale();
	void ResetCameraRotation();

	void SetCubemapTexture(System::String^ aCubemapTexture);
	void ParticleSystemPlay();
	void ParticleSystemStop();
	void ParticleSystemPause();

	void SetTexture(System::String^ aTexturePath);
	void SetDuration(float aDuration);
	void SetIsLoopable(bool aIsLoopable);
	void SetSpawnRate(float aSpawnRate);
	void SetLifetime(float aLifetime);
	void SetAcceleration(EditorTools::Vector3^ aAcceleration);
	void SetStartVelocity(EditorTools::Vector3^ aStartVelocity);
	void SetStartRotation(EditorTools::Vector2^ aStartRotation);
	void SetRotationVelocity(EditorTools::Vector2^ aRotationVelocity);
	void SetGravityModifier(float aGravityModifier);
	void SetStartColor(EditorTools::Vector4^ aStartColor);
	void SetEndColor(EditorTools::Vector4^ aEndColor);
	void SetStartSize(EditorTools::Vector2^ aStartSize);
	void SetEndSize(EditorTools::Vector2^ aEndSize);

	const float GetDuration();
	System::String^ GetTexturePath();
	const float GetSpawnRate();
	const float GetLifeTime();
	const bool GetIsLoopable();
	EditorTools::Vector3^ GetAcceleration();
	EditorTools::Vector3^ GetStartVelocity();
	EditorTools::Vector2^ GetStartRotation();
	EditorTools::Vector2^ GetRotationVelocity();
	const float GetGravityModifier();
	EditorTools::Vector4^ GetStartColor();
	EditorTools::Vector4^ GetEndColor();
	EditorTools::Vector2^ GetStartSize();
	EditorTools::Vector2^ GetEndSize();

	// -- BlendState --
	void SetBlendState(int aBlendState);
	int GetBlendState();

	// -- ShapeData --
	void SetShapeType(int aIndex);
	int GetShapeType();

	// -- SphereData --
	void SetSphereRadius(float aRadius);
	void SetSphereRadiusThickness(float aRadiusThickness);
	float GetSphereRadius();
	float GetSphereRadiusThickness();

	// -- BoxData --
	void SetBoxSize(EditorTools::Vector3^ aBoxSize);
	void SetBoxThickness(float aBoxThickness);
	EditorTools::Vector3^ GetBoxSize();
	const float GetBoxThickness();

	void SendMessage(array<System::Byte>^ aData, int aMessageType, int aDataType);
	void SetEditorMessageCallback(System::IntPtr aCallback);

	System::Collections::Generic::List<System::Byte>^ GetByteList() { return ourByteList; }

	static System::Collections::Generic::List<System::Byte>^ ourByteList = nullptr;
	static EditorCallbackMessageHandler ourEditorMessageHandler;

private:
	CEditorProxy* myEditorProxy;

};


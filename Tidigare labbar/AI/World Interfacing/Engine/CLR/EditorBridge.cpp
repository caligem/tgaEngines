#include "stdafx.h"
#include "EditorBridge.h"

#include "EditorProxy.h"

#include "PostMasterMessageType.cs"

#define ValidateEditorProxy() if(myEditorProxy == nullptr) { return; }

typedef void(*VoidCallback)(void);

CEditorBridge::CEditorBridge()
{
	myEditorProxy = nullptr;

	ourByteList = gcnew System::Collections::Generic::List<System::Byte>();
	ourEditorMessageHandler = nullptr;
}

void EditorMessageCallback(const unsigned char* aData, EMessageType aMessageType, EDataType aDataType)
{
	if (!CEditorBridge::ourEditorMessageHandler)return;
	if (aDataType == EDataType_Invalid)return;

	int byteSize = static_cast<int>(aDataType);

	CEditorBridge::ourByteList->Clear();
	for (int i = 0; i < byteSize; ++i)CEditorBridge::ourByteList->Add(aData[i]);

	CEditorBridge::ourEditorMessageHandler(aMessageType, aDataType);
}

void CEditorBridge::Init(unsigned char aEditorType, System::IntPtr aHandle)
{
	if (myEditorProxy != nullptr)
	{
		myEditorProxy->Shutdown();
		delete myEditorProxy;
	}

	EEditorType editorType = static_cast<EEditorType>(aEditorType);

	if (editorType == EEditorType_Invalid)
	{
		return;
	}

	CEditorProxy* newEditorProxy = new CEditorProxy();
	newEditorProxy->Init(editorType, aHandle.ToPointer());
	myEditorProxy = newEditorProxy;
	myEditorProxy->SetEditorMessageCallback(&EditorMessageCallback);
	myEditorProxy->StartEngine();
}

void CEditorBridge::WndProc(System::Windows::Forms::Message aMessage)
{
	ValidateEditorProxy();

	System::IntPtr hwnd = aMessage.HWnd;
	System::Int32 msg = aMessage.Msg;
	System::IntPtr wParam = aMessage.WParam;
	System::IntPtr lParam = aMessage.LParam;
	
	myEditorProxy->SendWindowMessage(
		(unsigned int)msg,
		wParam.ToPointer(),
		lParam.ToPointer()
	);

	aMessage.Result = System::IntPtr::Zero;
}

void CEditorBridge::Shutdown()
{
	ValidateEditorProxy();
	myEditorProxy->Shutdown();
}

bool CEditorBridge::IsRunning()
{
	if (myEditorProxy == nullptr)
	{
		return false;
	}

	return myEditorProxy->IsRunning();
}

void CEditorBridge::SendMessage(array<System::Byte>^ aData, int aMessageType, int aDataType)
{
	ValidateEditorProxy();

	pin_ptr<System::Byte> data = &aData[0];
	unsigned char* pData = data;

	myEditorProxy->SendMessageToEngine(pData, static_cast<EMessageType>(aMessageType), static_cast<EDataType>(aDataType));
}

void CEditorBridge::SetEditorMessageCallback(System::IntPtr aCallback)
{
	ourEditorMessageHandler = reinterpret_cast<EditorCallbackMessageHandler>(aCallback.ToPointer());
}

void CEditorBridge::GotFocus()
{
	ValidateEditorProxy();
	myEditorProxy->GotFocus();
}

void CEditorBridge::LostFocus()
{
	ValidateEditorProxy();
	myEditorProxy->LostFocus();
}

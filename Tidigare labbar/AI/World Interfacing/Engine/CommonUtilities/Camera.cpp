#include "Camera.h"
#include "Vector.h"

#include <cmath>

namespace CommonUtilities
{
	Camera::Camera()
	{
		myPosition = Vector3f(0, 0, 0);
		myRotation = Vector3f(0, 0, 0);
	}

	Camera::~Camera()
	{
	}

	void Camera::Init(const Matrix44f & aPerspectiveFOV)
	{
		myPerspectiveFOV = aPerspectiveFOV;
		CalculateTransform();
	}

	void Camera::Move(const Vector3f& aMovement)
	{
		myPosition += aMovement * Matrix33f(myTransform);
		CalculateTransform();
	}

	void Camera::SetPosition(const Vector3f & aPosition)
	{
		myPosition = aPosition;
		CalculateTransform();
	}

	void Camera::Rotate(const Vector3f& aRotation)
	{
		myRotation += aRotation;
		CalculateTransform();
	}

	Vector3f Camera::PostProjectionSpace(const Vector3f& aPoint) const
	{
		Vector3f point = Vector3f(Vector4f(aPoint, 1.0f) * myTransformInverse);
		point.x = ((point.x / point.z) + 1.f) / 2.f;
		point.y = ((-point.y / point.z) + 1.f) / 2.f;
		return point;
	}

	const Vector3f & Camera::GetPosition() const
	{
		return myPosition;
	}

	const Vector3f Camera::GetForward() const
	{
		return (Vector3f(0.f, 0.f, 1.f)*Matrix33f(myTransform)).GetNormalized();
	}
	const Vector3f Camera::GetRight() const
	{
		return (Vector3f(1.f, 0.f, 0.f)*Matrix33f(myTransform)).GetNormalized();
	}
	const Vector3f Camera::GetUp() const
	{
		return (Vector3f(0.f, 1.f, 0.f)*Matrix33f(myTransform)).GetNormalized();
	}

	void Camera::CalculateTransform()
	{
		myTransform = Matrix44f::CreateRotateAroundZ(myRotation.z);
		myTransform *= Matrix44f::CreateRotateAroundX(myRotation.x);
		myTransform *= Matrix44f::CreateRotateAroundY(myRotation.y);
		myTransform[12] = myPosition.x;
		myTransform[13] = myPosition.y;
		myTransform[14] = myPosition.z;

		myTransformInverse = myTransform.GetFastInverse() * myPerspectiveFOV;
	}
}

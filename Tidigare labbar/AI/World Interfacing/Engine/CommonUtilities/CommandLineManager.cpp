#include "CommandLineManager.h"

#include <codecvt>

namespace CommonUtilities
{
	std::map<std::string, std::vector<std::string>> CCommandLineManager::myParameters;

	void CCommandLineManager::Init(int argc, wchar_t * wargv[])
	{
		std::wstring_convert<std::codecvt_utf8<wchar_t>> converter;

		std::string lastParameter;

		for (int i = 1; i < argc; ++i)
		{
			wchar_t* parameter = wargv[i];
			std::wstring wstr(parameter);
			std::string str(converter.to_bytes(wstr));

			if (str[0] == '-')
			{
				myParameters[str] = std::vector<std::string>();
				lastParameter = str;
			}
			else
			{
				if (myParameters.find(lastParameter) != myParameters.end())
				{
					myParameters[lastParameter].push_back(str);
				}
			}
		}
	}
	bool CCommandLineManager::HasParameter(const char * aParameter)
	{
		if (myParameters.find(aParameter) != myParameters.end())
		{
			return true;
		}
		return false;
	}
	bool CCommandLineManager::HasArgument(const char * aParameter, const char * aArgument)
	{
		if (!HasParameter(aParameter))
		{
			return false;
		}

		for (const std::string& str : myParameters[aParameter])
		{
			if (str == aArgument)
			{
				return true;
			}
		}

		return false;
	}
	bool CCommandLineManager::HasArgument(const char * aParameter, int aIndex)
	{
		if (!HasParameter(aParameter))
		{
			return false;
		}

		if (myParameters[aParameter].size() > aIndex)
		{
			return true;
		}

		return true;
	}
	const std::string & CCommandLineManager::GetArgument(const char * aParameter, int aIndex)
	{
		return myParameters[aParameter][aIndex];
	}
}

#include "FileDialog.h"

#include <windows.h>
#include <commdlg.h>
#include <fstream>
#include <iostream>

#include "Base64.h"

#include <ShlObj.h>
#pragma comment(lib, "shell32.lib")

namespace CommonUtilities
{
	bool SaveFile(const std::string& aFilePath, const std::string& aData, bool aEncode)
	{
		std::string writeData;
		if( aEncode )
		{
			writeData = Base64Encode(reinterpret_cast<unsigned char const*>(aData.c_str()), aData.size());
		}
		else
		{
			writeData = aData;
		}
		
		CreateFilePath(aFilePath);
		std::ofstream o(aFilePath);
		if( o.bad() || o.fail() )
		{
			std::cout << "[ERROR] Failed to write file: " << aFilePath << std::endl;
			return false;
		}

		o << writeData;

		return true;
	}
	std::string OpenFile(const std::string& aFilePath, bool aEncoded)
	{
		std::ifstream i(aFilePath);
		if( i.bad() || i.fail() )
		{
			std::cout << "[ERROR] Failed to read file: " << aFilePath << std::endl;
			return "{}";
		}
		std::string fileData((std::istreambuf_iterator<char>(i)), std::istreambuf_iterator<char>());

		std::string returnData;

		if( aEncoded )
		{
			returnData = Base64Decode(fileData);
		}
		else
		{
			returnData = fileData;
		}

		return returnData;
	}
	std::string GetMyDocumentsPath()
	{
		wchar_t my_documents[1024];
		HRESULT result = SHGetFolderPath(NULL, CSIDL_PERSONAL, NULL, SHGFP_TYPE_CURRENT, my_documents);

		std::wstring wres = my_documents;
		std::string res = "";
		if (result == S_OK)
			res = std::string(wres.begin(), wres.end());

		return res;
	}
	bool DirExists(const std::string& dirName_in)
	{
		DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
		if (ftyp == INVALID_FILE_ATTRIBUTES)
			return false;  //something is wrong with your path!

		if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
			return true;   // this is a directory!

		return false;    // this is not a directory!
	}
	bool CreateFilePath(const std::string & aPath)
	{
		std::string strPath(aPath);
		std::string strCurrent;
		int nStart, nStart1, nStart2;

		// Create the path //
		while (strPath.length())
		{
			nStart1 = (int)strPath.find('/'); if (nStart1 == std::string::npos) nStart1 = INT_MAX;
			nStart2 = (int)strPath.find('\\'); if (nStart2 == std::string::npos) nStart2 = INT_MAX;
			nStart = min(nStart1, nStart2);
			if (nStart == INT_MAX) break;
			strCurrent += strPath.substr(0, nStart) + "\\";
			strPath.erase(strPath.begin(), strPath.begin() + nStart + 1);
			if (!DirExists(strCurrent))
			{
				if (CreateDirectoryA(strCurrent.c_str(), NULL) == FALSE)
				{
					return false;
				}
			}
		}

		return true;
	}
	std::string GetMyGamePath()
	{
		return GetMyDocumentsPath() + "/FeatherFallGames/Kami/";
	}
}

#include "Mathf.h"
#include <math.h>

namespace CommonUtilities
{
	const CommonUtilities::Matrix44f LookAt(const CommonUtilities::Vector3f & aPositionToLookAt, const CommonUtilities::Vector3f aPositionToLookFrom, const CommonUtilities::Vector3f & aUpVector)
	{
		CommonUtilities::Vector3f  forward = (aPositionToLookAt - aPositionToLookFrom).GetNormalized();
		CommonUtilities::Vector3f  up = aUpVector.GetNormalized();
		if (std::fabsf(forward.Dot(up)) == 1.f)
		{
			up.z += 0.001f;
			up.Normalize();
		}
		CommonUtilities::Vector3f  right = up.Cross(forward).GetNormalized();
		up = forward.Cross(right);

		CommonUtilities::Matrix44f Result;
		Result[0] = right.x;
		Result[1] = right.y;
		Result[2] = right.z;
		Result[4] = up.x;
		Result[5] = up.y;
		Result[6] = up.z;
		Result[8] = forward.x;
		Result[9] = forward.y;
		Result[10] = forward.z;
		Result[12] = aPositionToLookFrom.x;
		Result[13] = aPositionToLookFrom.y;
		Result[14] = aPositionToLookFrom.z;
		return Result;
	}
}
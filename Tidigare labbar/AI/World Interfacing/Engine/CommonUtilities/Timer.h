#pragma once

#include <chrono>

namespace CommonUtilities
{
	class Timer
	{
	public:
		Timer();
		~Timer();

		void Update();

		float GetRealDeltaTime() const;
		float GetRealTotalTime() const;
		float GetDeltaTime() const;
		float GetTotalTime() const;

		void SetSpeed(float aSpeed) { mySpeed = static_cast<double>(aSpeed); }
		const float GetSpeed() const { return static_cast<float>(mySpeed); }

	private:
		double GetCurrentTime();

		double myStartTime;

		double myRealCurrentTime;
		double myRealPreviousTime;
		double myRealTotalTime;

		double myCurrentTime;
		double myPreviousTime;
		double myTotalTime;

		double mySpeed;

	};
}

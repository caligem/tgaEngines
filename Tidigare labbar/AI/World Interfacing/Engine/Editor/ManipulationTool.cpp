#include "stdafx.h"
#include "ManipulationTool.h"

#include "..\Engine\ImGui\ImGuizmo.h"
#include "..\Engine\ImGui\imgui.h"
#include "..\Engine\ImGui\imgui_impl_dx11.h"

#include "LevelEditor.h"
#include "IEditor.h"
#include "InputManager.h"
#include "Mathf.h"

CManipulationTool::CManipulationTool()
{
}

CManipulationTool::~CManipulationTool()
{
}

void CManipulationTool::UpdateGameObject(CGameObject aGameObject)
{
	CommonUtilities::Matrix44f dummy = aGameObject.GetTransform().GetMatrix();
	CommonUtilities::Matrix44f delta;
	CommonUtilities::Vector3f snap = { 1.f, 1.f, 1.f };

	ImGuizmo::OPERATION op;
	switch (myMode)
	{
	case EMode_Translate:
		op = ImGuizmo::TRANSLATE;
		break;
	case EMode_Rotate:
		op = ImGuizmo::ROTATE;
		snap = { 15.f, 15.f, 15.f };
		break;
	case EMode_Scale:
		op = ImGuizmo::SCALE;
		break;
	case EMode_None:
	default:
		op = static_cast<ImGuizmo::OPERATION>(-1);
		break;
	}

	ImGuizmo::MODE space;
	switch (mySpace)
	{
	case ESpace_World:
		space = ImGuizmo::WORLD;
		break;
	case ESpace_Local:
	default:
		space = ImGuizmo::LOCAL;
		break;
	}

	Input::CInputManager& input = IEditor::Input();

	ImGuizmo::Manipulate(
		IEditor::GetSavedCameraBuffer().myToCamera.myMatrix,
		IEditor::GetSavedCameraBuffer().myProjection.myMatrix,
		op,
		space,
		dummy.myMatrix,
		delta.myMatrix,
		(input.IsKeyDown(Input::Key_Shift) ? snap.pData : nullptr)
	);

	if (!ImGuizmo::IsUsing()) return;

	CommonUtilities::Vector3f pos, rot, scale;
	ImGuizmo::DecomposeMatrixToComponents(dummy.myMatrix, pos.pData, rot.pData, scale.pData);

	if (std::isnan(pos.x) || std::isnan(pos.y) || std::isnan(pos.z) ||
		std::isnan(rot.x) || std::isnan(rot.y) || std::isnan(rot.z) ||
		std::isnan(scale.x) || std::isnan(scale.y) || std::isnan(scale.z) )
	{
		return;
	}

	switch (op)
	{
	case ImGuizmo::TRANSLATE:
		aGameObject.GetTransform().SetPosition(pos);
		CLevelEditor::SendMessage(reinterpret_cast<const unsigned char*>(pos.pData), EMessageType_Transform_SetPosition, EDataType_Vector3);
		break;
	case ImGuizmo::ROTATE:
		aGameObject.GetTransform().SetRotation(rot*CommonUtilities::Deg2Rad);
		CLevelEditor::SendMessage(reinterpret_cast<const unsigned char*>(rot.pData), EMessageType_Transform_SetRotation, EDataType_Vector3);
		break;
	case ImGuizmo::SCALE:
		aGameObject.GetTransform().SetScale(scale);
		CLevelEditor::SendMessage(reinterpret_cast<const unsigned char*>(scale.pData), EMessageType_Transform_SetScale, EDataType_Vector3);
		break;
	}
}

void CManipulationTool::SetMode(EMode aMode)
{
	if (ImGuizmo::IsUsing())return;

	myMode = aMode;
}

void CManipulationTool::SetSpace(ESpace aSpace)
{
	if (ImGuizmo::IsUsing())return;

	mySpace = aSpace;
}

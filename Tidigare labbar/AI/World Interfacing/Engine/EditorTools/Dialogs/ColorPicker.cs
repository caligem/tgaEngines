﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace EditorTools
{
	public partial class ColorPicker : Form
	{
		public RGBColor myColor = new RGBColor();
 
		private bool myBigPanelActive;
		private bool mySmallPanelActive;

		private HSVColor myHSVColor = new HSVColor();

		public ColorPicker(EditorTools.Vector4 aColor)
		{
			InitializeComponent();

			myColor = new RGBColor(aColor.x, aColor.y, aColor.z, aColor.w);
			myHSVColor = RGBtoHSV(myColor);
			myColor = HSVtoRGB(myHSVColor);

			UpdateInputBoxes();

			myBigPanelActive = false;
			mySmallPanelActive = false;

			this.DoubleBuffered = true;
		}

		private void button1_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.OK;
		}
		private void button2_Click(object sender, EventArgs e)
		{
			this.DialogResult = DialogResult.Cancel;
		}

		private void bigPanel_MouseDown(object sender, MouseEventArgs e)
		{
			myBigPanelActive = true;
		}
		private void bigPanel_MouseMove(object sender, MouseEventArgs e)
		{
			if(!myBigPanelActive)
			{
				return;
			}

			myHSVColor.myS = Clamp01(e.X/256f);
			myHSVColor.myV = (1f-Clamp01(e.Y/256f));

			RefreshPanelsAndValues();
		}
		private void bigPanel_MouseUp(object sender, MouseEventArgs e)
		{
			myBigPanelActive = false;
		}

		private void smallPanel_MouseDown(object sender, MouseEventArgs e)
		{
			mySmallPanelActive = true;
		}
		private void smallPanel_MouseMove(object sender, MouseEventArgs e)
		{
			if(!mySmallPanelActive)
			{
				return;
			}

			myHSVColor.myH = (1f-Clamp01(e.Y/256f)) * 360f;

			RefreshPanelsAndValues();
		}
		private void smallPanel_MouseUp(object sender, MouseEventArgs e)
		{
			mySmallPanelActive = false;
		}

		private void bigPanel_Paint(object sender, PaintEventArgs e)
		{
			var p = sender as Panel;
			var g = e.Graphics;

			Point[] points = new Point[4];


			Brush brush = new SolidBrush( Color.Aqua );

			g.FillPolygon( brush, points );

			g.DrawEllipse(Pens.Black, new RectangleF(
				myHSVColor.myS * 256f - 5f,	
				(1f-myHSVColor.myV) * 256f - 5f,	
				10f, 10f
			));

			HSVColor hsl = new HSVColor();
			hsl.myH = myHSVColor.myH;

			/*
			for(int l = 0; l < 256; ++l)
			{
				for(int s = 0; s < 256; ++s)
				{
					hsl.myS = s/256f;
					hsl.myL = 1f-(l/256f);
					g.DrawRectangle(new Pen(CalculateColors(hsl).ToColor()),
						new Rectangle(
							s, l, 1, 1
						)
					);
				}
			}
			*/
		}
		private void smallPanel_Paint(object sender, PaintEventArgs e)
		{
			var p = sender as Panel;
			var g = e.Graphics;

			HSVColor hsl = new HSVColor();
			hsl.myV = 1f;
			hsl.myS = 1f;

			for(int i = 0; i < 256; ++i)
			{
				hsl.myH = (1f-Clamp01(i/256f))*360f;
				g.DrawLine(new Pen(HSVtoRGB(hsl).ToColor()), new PointF(0, i), new PointF(p.Width, i));
			}
		}
		private void previewPanel_Paint(object sender, PaintEventArgs e)
		{
			var p = sender as Panel;
			var g = e.Graphics;

			Color color = myColor.ToColor();

			g.FillRectangle(new SolidBrush(color), p.DisplayRectangle);
		}

		private HSVColor RGBtoHSV(RGBColor aColor)
		{
			HSVColor result = new HSVColor();

			float Cmax = Math.Max(aColor.myR, Math.Max(aColor.myG, aColor.myB));
			float Cmin = Math.Min(aColor.myR, Math.Min(aColor.myG, aColor.myB));
			float delta = Cmax - Cmin;

			if(delta == 0f)
			{
				result.myH = 0f;
			}
			else if(Cmax == aColor.myR)
			{
				result.myH = 60f * (((aColor.myG - aColor.myB) / delta) % 6);
			}
			else if(Cmax == aColor.myG)
			{
				result.myH = 60f * ((aColor.myB - aColor.myR) / delta + 2);
			}
			else if(Cmax == aColor.myB)
			{
				result.myH = 60f * ((aColor.myR - aColor.myG) / delta + 4);
			}

			while (result.myH < 0f) result.myH += 360f;
			while (result.myH > 360f) result.myH -= 360f;

			if(Cmax == 0f)
			{
				result.myS = 0f;
			}
			else
			{
				result.myS = delta / Cmax;
			}

			result.myV = Cmax;

			return result;
		}
		private RGBColor HSVtoRGB(HSVColor aColor)
		{
			RGBColor result = new RGBColor();

			float C = aColor.myV * aColor.myS;
			float X = C * (1f - Math.Abs((aColor.myH / 60f) % 2 - 1));
			float m = aColor.myV - C;

			float Rp = 0f;
			float Gp = 0f;
			float Bp = 0f;

			if(aColor.myH < 60f)
			{
				Rp = C;
				Gp = X;
			}
			else if(aColor.myH < 120f)
			{
				Rp = X;
				Gp = C;
			}
			else if(aColor.myH < 180f)
			{
				Gp = C;
				Bp = X;
			}
			else if(aColor.myH < 240f)
			{
				Gp = X;
				Bp = C;
			}
			else if(aColor.myH < 300f)
			{
				Rp = X;
				Bp = C;
			}
			else
			{
				Rp = C;
				Bp = X;
			}

			result.myR = (Rp + m);
			result.myG = (Gp + m);
			result.myB = (Bp + m);
			result.myA = myColor.myA;

			return result;
		}
		private float Clamp01(float aInput)
		{
			if (aInput < 0f) return 0f;
			if (aInput > 1f) return 1f;
			return aInput;
		}

		private void UpdateInputBoxes()
		{
			hueInput.Value = (int)(myHSVColor.myH);
			saturationInput.Value = (int)(myHSVColor.myS*100f);
			brightnessInput.Value = (int)(myHSVColor.myV*100f);
			alphaInput.Value = (int)(myColor.myA * 255f);
			trackBar1.Value = (int)(myColor.myA * 255f);
		}

		private void hueInput_ValueChanged(object sender, EventArgs e)
		{
			myHSVColor.myH = (float)hueInput.Value;
			RefreshPanelsAndValues();
		}
		private void saturationInput_ValueChanged(object sender, EventArgs e)
		{
			myHSVColor.myS = (float)saturationInput.Value / 100f;
			RefreshPanelsAndValues();
		}
		private void brightnessInput_ValueChanged(object sender, EventArgs e)
		{
			myHSVColor.myV = (float)brightnessInput.Value / 100f;
			RefreshPanelsAndValues();
		}

		private void alphaInput_ValueChanged(object sender, EventArgs e)
		{
			myColor.myA = (float)alphaInput.Value / 255f;
			trackBar1.Value = (int)(myColor.myA * 255f);
			RefreshPanelsAndValues();
		}
		private void trackBar1_Scroll(object sender, EventArgs e)
		{
			myColor.myA = (float)trackBar1.Value / 255f;
			alphaInput.Value = (int)(myColor.myA * 255f);
			RefreshPanelsAndValues();
		}

		private void RefreshPanelsAndValues()
		{
			myColor = HSVtoRGB(myHSVColor);
			UpdateInputBoxes();

			bigPanel.Refresh();
			previewPanel.Refresh();
		}

	}

	public class RGBColor
	{
		public float myR;
		public float myG;
		public float myB;
		public float myA;

		public RGBColor(float aR, float aG, float aB, float aA)
		{
			myR = aR;
			myG = aG;
			myB = aB;
			myA = aA;
		}
		public RGBColor()
		{
			myR = 1f;
			myG = 0f;
			myB = 0f;
			myA = 1f;
		}

		public Color ToColor()
		{
			return Color.FromArgb((int)(myR * 255f), (int)(myG * 255f), (int)(myB * 255f));
		}
	}
	public class HSVColor
	{
		public float myH;
		public float myV;
		public float myS;
	}
}

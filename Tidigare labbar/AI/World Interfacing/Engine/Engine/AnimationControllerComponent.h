#pragma once
#include "Component.h"

class CAnimationControllerComponent : public CComponent
{
public:
	CAnimationControllerComponent();
	~CAnimationControllerComponent();

	void Init();

	void SetAnimation(const std::string& aAnimation) { myAnimation = aAnimation; }
	const std::string& GetAnimation() const { return myAnimation; }

private:

	std::string myAnimation;

};


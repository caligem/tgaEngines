#pragma once
#include "Component.h"
#include "..\AudioManager\AudioListener.h"


class CAudioListenerComponent : public CComponent
{
public:
	CAudioListenerComponent();
	~CAudioListenerComponent();
	bool Init();
	void OnStart();
	void Update();
	void SetPosition(const CommonUtilities::Vector3f& aPosition);
private:
	virtual void OnOwnerDestroyed();

	AudioListener myAudioListener;
};


#pragma once
#include "Component.h"
#include "UIElement.h"

class CButtonComponent : public CComponent, public CUIElement
{
public:
	CButtonComponent();
	~CButtonComponent();

	struct SComponentData
	{
		const char* fontPath;
		const char* spritePath;
	};

	void Init(SComponentData aData);

	void SetPosition(const CommonUtilities::Vector2f& aPosition);
	inline const CommonUtilities::Vector2f& GetPosition() const { return myPosition; }

	void SetScaleRelativeToScreen(const CommonUtilities::Vector2f& aSize);

	void SetText(const char* aText);
	void SetTextPosition(const CommonUtilities::Vector2f& aTextPosition);

	void FillRenderCommands(CDoubleBuffer<SSpriteRenderCommand>& aSpriteRenderCommandsBuffer, CDoubleBuffer<STextRenderCommand>& aTextRenderCommandsBuffer) override;
	
private:
	CTextComponent* myTextComponent;
	CSpriteComponent* mySpriteComponent;
	CommonUtilities::Vector2f myPosition;
	CommonUtilities::Vector2f mySizeRelativeToScreen;
};


#pragma once

#include "ConstantBuffer.h"

#include "VertexDataWrapper.h"
#include "Vector.h"
#include "GrowingArray.h"

#include "Sprite.h"

#include "Font.h"

class CDebugRenderer;
class CVertexShader;
class CPixelShader;
struct SSpriteRenderCommand;
struct STextRenderCommand;

struct ID3D11ShaderResourceView;

class C2DRenderer
{
public:
	C2DRenderer();
	~C2DRenderer();

	bool Init();
	void RenderUIOnCanvas(const CommonUtilities::GrowingArray<SCanvasRenderCommand>& aCanvasRenderCommands);
	void RenderWorldSpaceCanvases(CConstantBuffer& aCameraBuffer, const CommonUtilities::GrowingArray<SCanvasRenderCommand>& aCanvasRenderCommands);
	void RenderScreenSpaceCanvases(const CommonUtilities::GrowingArray<SCanvasRenderCommand>& aCanvasRenderCommands);

private:
	friend CDebugRenderer;

	bool InitBuffers();
	void RenderText(const CommonUtilities::GrowingArray<STextRenderCommand>& aTextToRender);
	void RenderSprites(const CommonUtilities::GrowingArray<SSpriteRenderCommand>& aSpritesToRender);
	struct SInstanceBufferData
	{
		CommonUtilities::Matrix44f myToWorld;
		CommonUtilities::Vector2f myScale;
		CommonUtilities::Vector2f myPadding;
	};
	struct SQuadBufferData
	{
		CommonUtilities::Vector2f myScreenSize;
		CommonUtilities::Vector2f mySpriteSize;
		CommonUtilities::Vector2f myPosition;
		CommonUtilities::Vector2f myScale;
		CommonUtilities::Vector2f myPivot;
		CommonUtilities::Vector2f myUVOffset;
		CommonUtilities::Vector2f myUVScale;
		float myRotation;
		float myPadding;
		CommonUtilities::Vector4f myTint;
	};
	struct STextQuadBufferData
	{
		CommonUtilities::Vector2f myScreenSize;
		CommonUtilities::Vector2f mySpriteSize;
		CommonUtilities::Vector2f myPosition;
		CommonUtilities::Vector2f myScale;
		CommonUtilities::Vector2f myPivot;
		CommonUtilities::Vector2f myUVOffset;
		CommonUtilities::Vector2f myUVScale;
		float myRotation;
		float myPadding;
		CommonUtilities::Vector4f myTint;
		CommonUtilities::Vector4f myOutline;
	};

	SVertexDataWrapper myQuad;

	CConstantBuffer myInstanceBuffer;
	CConstantBuffer myQuadBuffer;
	CConstantBuffer myTextQuadBuffer;

	CVertexShader* mySpriteVertexShader;
	CPixelShader* mySpritePixelShader;
	CVertexShader* myTextVertexShader;
	CPixelShader* myTextPixelShader;

	CVertexShader* myCanvasWorldSpaceVertexShader;
	CVertexShader* myCanvasScreenSpaceVertexShader;
	CPixelShader* myCopyPixelShader;

};


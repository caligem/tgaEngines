#pragma once
#include "Camera.h"

class CGraphicsPipeline;
class CCameraComponent;

class CCameraManager
{
public:
	CCameraManager();
	~CCameraManager();

	bool Init();

	ID_T(CCamera) AcquireCamera(const CommonUtilities::Matrix44f& aProjectionMatrix, float aProjectionDepth);
	void ReleaseCamera(ID_T(CCamera) aCameraID);

private:
	friend CGraphicsPipeline;
	friend CCameraComponent;

	CCamera* GetCamera(ID_T(CCamera) aCameraID);
	ObjectPool<CCamera> myCameras;
};


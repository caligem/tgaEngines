#include "stdafx.h"
#include "ComponentStorage.h"


#include "RedefineComponentRegistrer.h"
#define ComponentRegister(Type, Container, Size) \
, ##Container(##Size)

CComponentStorage::CComponentStorage()
	: myGameObjectData(PoolSizeComponentStorageGameObjectData)
#include "RegisteredComponents.h"
{
}


CComponentStorage::~CComponentStorage()
{
}

void CComponentStorage::PrintStorageInfo()
{
#include "RedefineComponentRegistrer.h"
#define ComponentRegister(Type, Container, Size) \
(##Container).PrintInfo();
#include "RegisteredComponents.h"
}
#pragma once

#define ValidateResult(aCode) if(FAILED(aCode)){return false;}

template <typename T>
__forceinline void SafeRelease(T*& aPtr)
{
	if (aPtr != nullptr)
	{
		aPtr->Release();
		aPtr = nullptr;
	}
}

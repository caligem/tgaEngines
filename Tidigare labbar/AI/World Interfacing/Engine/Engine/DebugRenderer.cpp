#include "stdafx.h"
#ifndef _RETAIL

#include "DebugRenderer.h"

#include <DL_Debug.h>
#include "IEngine.h"
#include "TextureManager.h"

#include "Transform.h"
#include "ShaderManager.h"
#include "TextManager.h"
#include "ModelManager.h"

#include <d3d11.h>

CDebugRenderer::CDebugRenderer()
{
	myDebugVertexShader = nullptr;
	myDebugPixelShader = nullptr;
	myMeshVertexShader = nullptr;
	myMeshPixelShader = nullptr;
	myColor = { 1.f, 0.f, 0.f, 1.f };
}

CDebugRenderer::~CDebugRenderer()
{
}

bool CDebugRenderer::Init()
{
	myLineBuffer.Init();
	myBoxBuffer.Init();
	mySphereBuffer.Init();
	myTextBuffer.Init();
	myMeshCubeBuffer.Init();
	myMeshSphereBuffer.Init();

	myTextComponent.Init({ "Assets/Fonts/Mono/Mono" });
	myTextComponent.SetPosition({ 0.f, 0.f });
	myTextComponent.SetPivot({ 0.f, 0.f });
	myTextComponent.SetScale({ 0.6f, 0.6f });
	myTextComponent.SetTint({ 1.f, 1.f, 1.f, 1.f });
	myTextComponent.SetOutline({ 0.f, 0.f, 0.f, 1.f });

	if (!myInstanceBuffer.Init(sizeof(SInstanceBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create InstanceBuffer for DebugRenderer");
		return false;
	}

	if (!InitWireLineModel())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create CDebugRenderer::myLineModel");
		return false;
	}
	if (!InitWireBoxModel())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create CDebugRenderer::myWireBoxModel");
		return false;
	}
	if (!InitWireSphereModel())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create CDebugRenderer::myWireSphereModel");
		return false;
	}
	if (!InitMeshModels())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create CDebugRenderer::myMeshBoxModel");
		return false;
	}

	myDebugVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/Debug/Debug.vs", EShaderInputLayoutType_Debug);
	myDebugPixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/Debug/Debug.ps");

	myMeshVertexShader = &IEngine::GetShaderManager().GetVertexShader(L"Assets/Shaders/Debug/Grid.vs", EShaderInputLayoutType_PBR);
	myMeshPixelShader = &IEngine::GetShaderManager().GetPixelShader(L"Assets/Shaders/Debug/Grid.ps");
	
	return true;
}

void CDebugRenderer::RenderDebugLines(CConstantBuffer & aCameraBufferData)
{
	aCameraBufferData.SetBuffer(0, EShaderType_Vertex);

	ID3D11DeviceContext* context = IEngine::GetContext();
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_LINELIST);
	myDebugVertexShader->Bind();
	myDebugVertexShader->BindLayout();
	myDebugPixelShader->Bind();

	for (SLineData& line : myLineBuffer.GetReadBuffer())
	{
		CTransform transform;
		transform.SetPosition(line.mySource);
		transform.LookAt(line.myDestination);
		transform.SetScale({ 1.f, 1.f, (line.myDestination - line.mySource).Length() });

		RenderWireModel(myLineModel, transform.GetMatrix(), line.myColor);
	}
	for (SBoxData& box : myBoxBuffer.GetReadBuffer())
	{
		CTransform transform;
		transform.SetPosition(box.myPosition);
		transform.SetRotation(box.myRotation);
		transform.SetScale(box.myScale);
		RenderWireModel(myWireCubeModel, transform.GetMatrix(), box.myColor);
	}
	for (SSphereData& sphere : mySphereBuffer.GetReadBuffer())
	{
		CTransform transform;
		transform.SetPosition(sphere.myPosition);
		transform.SetRotation(sphere.myRotation);
		transform.SetScale(sphere.myScale);
		RenderWireModel(myWireSphereModel, transform.GetMatrix(), sphere.myColor);
	}

	myDebugVertexShader->Unbind();
	myDebugVertexShader->UnbindLayout();
	myDebugPixelShader->Unbind();
}

void CDebugRenderer::RenderDebugMeshes(CConstantBuffer & aCameraBufferData)
{
	aCameraBufferData.SetBuffer(0, EShaderType_Vertex);

	ID3D11DeviceContext* context = IEngine::GetContext();
	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	myMeshVertexShader->Bind();
	myMeshVertexShader->BindLayout();
	myMeshPixelShader->Bind();

	for (SBoxData& box : myMeshCubeBuffer.GetReadBuffer())
	{
		CTransform transform;
		transform.SetPosition(box.myPosition);
		transform.SetRotation(box.myRotation);
		transform.SetScale(box.myScale);
		RenderMeshModel(myMeshCubeModel, transform.GetMatrix());
	}
	for (SSphereData& sphere : myMeshSphereBuffer.GetReadBuffer())
	{
		CTransform transform;
		transform.SetPosition(sphere.myPosition);
		transform.SetRotation(sphere.myRotation);
		transform.SetScale(sphere.myScale);
		RenderMeshModel(myMeshSphereModel, transform.GetMatrix());
	}

	myMeshVertexShader->Unbind();
	myMeshVertexShader->UnbindLayout();
	myMeshPixelShader->Unbind();
}

void CDebugRenderer::RenderWireModel(SVertexDataWrapper & aModel, const CommonUtilities::Matrix44f & aMatrix, const CommonUtilities::Vector4f& aColor)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	SInstanceBufferData data;
	data.myToWorld = aMatrix;
	data.myColor = aColor;
	myInstanceBuffer.SetData(&data);
	myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);
	myInstanceBuffer.SetBuffer(1, EShaderType_Pixel);

	context->IASetVertexBuffers(0, 1, &aModel.myVertexBuffer, &aModel.myStride, &aModel.myOffset);
	context->IASetIndexBuffer(aModel.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	context->DrawIndexed(aModel.myNumberOfIndices, 0, 0);
}

void CDebugRenderer::RenderMeshModel(CModel::SModelData & aModel, const CommonUtilities::Matrix44f & aMatrix)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	SInstanceBufferData instanceData;
	
	const SVertexDataWrapper& vertexData = aModel.myVertexData[0];
	STextureDataWrapper& textureData = aModel.myTextureData;
		
	instanceData.myToWorld = aMatrix;

	myInstanceBuffer.SetData(&instanceData);

	context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
	context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

	ID3D11ShaderResourceView* textures[STextureDataWrapper::maxTextures];
	for (int i = 0; i < textureData.maxTextures; ++i)
	{
		textures[i] = IEngine::GetTextureManager().GetTexture(textureData.myTextures[i]);
	}
	context->PSSetShaderResources(0, STextureDataWrapper::maxTextures, textures);

	context->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);
}

void CDebugRenderer::RenderText()
{
	IEngine::Get2DRenderer().RenderText(myTextBuffer.GetReadBuffer());
}

void CDebugRenderer::SwapBuffers()
{
	myLineBuffer.SwapBuffers();
	myBoxBuffer.SwapBuffers();
	mySphereBuffer.SwapBuffers();
	myMeshCubeBuffer.SwapBuffers();
	myMeshSphereBuffer.SwapBuffers();

	for (STextRenderCommand command : myTextComponent.GetRenderCommands()) myTextBuffer.Write(command);
	myTextComponent.SetText("");

	myTextBuffer.SwapBuffers();
}

void CDebugRenderer::DrawDebugLine(const CommonUtilities::Vector3f & aSource, const CommonUtilities::Vector3f & aDestination)
{
	myLineMutex.lock();
	myLineBuffer.Write({ aSource, aDestination, myColor });
	myLineMutex.unlock();
}

void CDebugRenderer::DrawDebugWireCube(const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector3f & aScale, const CommonUtilities::Vector3f & aRotation)
{
	myWireCubeMutex.lock();
	myBoxBuffer.Write({ aPosition, aScale, aRotation, myColor });
	myWireCubeMutex.unlock();
}

void CDebugRenderer::DrawDebugWireSphere(const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector3f & aScale, const CommonUtilities::Vector3f & aRotation)
{
	myWireSphereMutex.lock();
	mySphereBuffer.Write({ aPosition, aScale, aRotation, myColor });
	myWireSphereMutex.unlock();
}

void CDebugRenderer::DrawDebugCube(const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector3f & aScale, const CommonUtilities::Vector3f & aRotation)
{
	myMeshCubeMutex.lock();
	myMeshCubeBuffer.Write({ aPosition, aScale, aRotation, myColor });
	myMeshCubeMutex.unlock();
}

void CDebugRenderer::DrawDebugSphere(const CommonUtilities::Vector3f & aPosition, const CommonUtilities::Vector3f & aScale, const CommonUtilities::Vector3f & aRotation)
{
	myMeshSphereMutex.lock();
	myMeshSphereBuffer.Write({ aPosition, aScale, aRotation, myColor });
	myMeshSphereMutex.unlock();
}

void CDebugRenderer::SetColor(const CommonUtilities::Vector4f & aColor)
{
	myColor = aColor;
}

void CDebugRenderer::DrawDebugText(const std::string& aDebugText)
{
	myWireTextMutex.lock();
	myTextComponent.SetText(myTextComponent.GetText() + aDebugText + '\n');
	myWireTextMutex.unlock();
}

void CDebugRenderer::DrawDebugArrow(const CommonUtilities::Vector3f & aStartPosition, const CommonUtilities::Vector3f & aEndPosition)
{
	DrawDebugLine(aStartPosition, aEndPosition);

	CommonUtilities::Vector4f oppositeDirection(aStartPosition - aEndPosition);
	float length = oppositeDirection.Length();
	oppositeDirection.Normalize();

	CommonUtilities::Vector4f arbitraryPerpendicularLine(-oppositeDirection.y, oppositeDirection.z, oppositeDirection.x, 1.f);

	const float numberOfChevronLines = 8.0f;
	const float chevronLength = length / 5.0f;
	const float rotationBetweenChevronLines = (CommonUtilities::Pif * 2.f) / numberOfChevronLines;
	const float riseRotation = (CommonUtilities::Pif / 2.f) / 3.0f;

	for (float i = 0.0f; i < numberOfChevronLines; ++i)
	{
		CommonUtilities::Vector4f startPosition = aEndPosition;

		CommonUtilities::Matrix44f rotationMatrix = CommonUtilities::Matrix44f::CreateRotateAround(arbitraryPerpendicularLine, riseRotation);
		CommonUtilities::Vector4f chevronLineDirection = oppositeDirection * rotationMatrix;
		rotationMatrix = CommonUtilities::Matrix44f::CreateRotateAround(oppositeDirection, rotationBetweenChevronLines * i);
		chevronLineDirection = chevronLineDirection * rotationMatrix;
		chevronLineDirection.Normalize();

		CommonUtilities::Vector4f endPosition = startPosition + (chevronLineDirection * chevronLength);
		DrawDebugLine(startPosition, endPosition);
	}
}

bool CDebugRenderer::InitWireLineModel()
{
	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
	} vertices[2] =
	{
		{0.0f, 0.0f, 0.0f, 1.f},
		{0.0f, 0.0f, 1.0f, 1.f}
	};
	unsigned int indices[2] =
	{
		0,1
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = IEngine::GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		return false;
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = IEngine::GetDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		return false;
	}

	//Setup VertexData
	myLineModel.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	myLineModel.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	myLineModel.myStride = sizeof(Vertex);
	myLineModel.myOffset = 0;
	myLineModel.myVertexBuffer = vertexBuffer;
	myLineModel.myIndexBuffer = indexBuffer;

	return true;
}

bool CDebugRenderer::InitWireBoxModel()
{
	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
	} vertices[8] =
	{
		{-0.5, 0.5, 0.5, 1.f },
		{0.5, 0.5, 0.5, 1.f  },
		{0.5, 0.5, -0.5, 1.f },
		{-0.5, 0.5, -0.5, 1.f},
		{-0.5, -0.5, 0.5, 1.f},
		{0.5, -0.5, 0.5, 1.f },
		{0.5, -0.5, -0.5, 1.f},
		{-0.5, -0.5, -0.5, 1.f}
	};
	unsigned int indices[24] =
	{
		0,1,
		1,2,
		2,3,
		3,0,

		0,4,
		1,5,
		2,6,
		3,7,

		4,5,
		5,6,
		6,7,
		7,4
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = IEngine::GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		return false;
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = IEngine::GetDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		return false;
	}

	//Setup VertexData
	myWireCubeModel.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	myWireCubeModel.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	myWireCubeModel.myStride = sizeof(Vertex);
	myWireCubeModel.myOffset = 0;
	myWireCubeModel.myVertexBuffer = vertexBuffer;
	myWireCubeModel.myIndexBuffer = indexBuffer;

	return true;
}

bool CDebugRenderer::InitWireSphereModel()
{
	const int resolution = 24;

	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
	} vertices[resolution * 3];
	unsigned int indices[resolution * 6];

	for (int i = 0; i < resolution; ++i)
	{
		float a = ((CommonUtilities::Pif*2.f) / resolution)*i;
		vertices[i + resolution * 0] = { 0.5f*std::sinf(a), 0.5f*std::cosf(a), 0.f, 1.f };
		vertices[i + resolution * 1] = { 0.f, 0.5f*std::cosf(a), 0.5f*std::sinf(a), 1.f };
		vertices[i + resolution * 2] = { 0.5f*std::cosf(a), 0.f, 0.5f*std::sinf(a), 1.f };

		indices[(i + resolution * 0) * 2 + 0] = (i + 0) % resolution + resolution * 0;
		indices[(i + resolution * 0) * 2 + 1] = (i + 1) % resolution + resolution * 0;
		indices[(i + resolution * 1) * 2 + 0] = (i + 0) % resolution + resolution * 1;
		indices[(i + resolution * 1) * 2 + 1] = (i + 1) % resolution + resolution * 1;
		indices[(i + resolution * 2) * 2 + 0] = (i + 0) % resolution + resolution * 2;
		indices[(i + resolution * 2) * 2 + 1] = (i + 1) % resolution + resolution * 2;
	}

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = IEngine::GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		return false;
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = IEngine::GetDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		return false;
	}

	//Setup VertexData
	myWireSphereModel.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	myWireSphereModel.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	myWireSphereModel.myStride = sizeof(Vertex);
	myWireSphereModel.myOffset = 0;
	myWireSphereModel.myVertexBuffer = vertexBuffer;
	myWireSphereModel.myIndexBuffer = indexBuffer;

	return true;
}

bool CDebugRenderer::InitMeshModels()
{
	myMeshCubeModel = IEngine::GetModelManager().myModelLoader.LoadCube();
	myMeshSphereModel = IEngine::GetModelManager().myModelLoader.CreateIcosphere(2, false);

	return true;
}

#endif

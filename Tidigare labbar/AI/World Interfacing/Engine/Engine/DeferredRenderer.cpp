#include "stdafx.h"
#include "DeferredRenderer.h"
#include "DirectXFramework.h"
#include "RenderCommand.h"
#include "ModelManager.h"
#include "IEngine.h"
#include "Scene.h"
#include "SceneManager.h"
#include "JsonDocument.h"

#include "ShaderManager.h"
#include "Shader.h"

#include <d3d11.h>

#include "TextureManager.h"

CDeferredRenderer::CDeferredRenderer()
{
	myDataPassVertexShader = nullptr;
	myDataPassPixelShader = nullptr;
	myLightPassVertexShader = nullptr;
	myLightPassPixelShaders = { nullptr };
}

CDeferredRenderer::~CDeferredRenderer()
{
}

bool CDeferredRenderer::Init()
{
	if (!myInstanceBuffer.Init(sizeof(SInstanceBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myInstanceBuffer");
		return false;
	}
	if (!myBoneBuffer.Init(sizeof(SBoneBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myBoneBuffer");
		return false;
	}

	if (!myDirectionalLightBuffer.Init(sizeof(SLightBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myDirectionalLightBuffer");
		return false;
	}
	if (!myPointLightBuffer.Init(sizeof(SPointLightBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myPointLightBuffer");
		return false;
	}
	if (!mySpotLightBuffer.Init(sizeof(SSpotLightBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::mySpotLightBuffer");
		return false;
	}
	if (!myScreenBuffer.Init(sizeof(SScreenData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myScreenBuffer");
		return false;
	}

	CShaderManager& shaderManager = IEngine::GetShaderManager();

	myDataPassVertexShader = &shaderManager.GetVertexShader(L"Assets/Shaders/Deferred/DataPass.vs", EShaderInputLayoutType_PBR);
	myDataPassPixelShader = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/DataPass.ps");

	JsonDocument doc("Assets/Shaders/Materials/materials.json");

	if (doc.Find("VertexShaders"))
	{
		for (int i = 0; i < doc["VertexShaders"].GetSize(); ++i)
		{
			const char* name = doc["VertexShaders"][i].GetString();
			std::string ws(name);
			std::wstring wide(ws.begin(), ws.end());
			&shaderManager.GetVertexShader(L"Assets/Shaders/Materials/" + wide + L".vs", EShaderInputLayoutType_PBR);
		}
	}
	if (doc.Find("PixelShaders"))
	{
		for (int i = 0; i < doc["PixelShaders"].GetSize(); ++i)
		{
			const char* name = doc["PixelShaders"][i].GetString();
			std::string ws(name);
			std::wstring wide(ws.begin(), ws.end());
			&shaderManager.GetPixelShader(L"Assets/Shaders/Materials/" + wide + L".ps");
		}
	}
	
	myLightPassVertexShader = &shaderManager.GetVertexShader(L"Assets/Shaders/Deferred/LightPass.vs", EShaderInputLayoutType_PPFX);
	myLightPassWorldVertexShader = &shaderManager.GetVertexShader(L"Assets/Shaders/Deferred/LightPassWorld.vs", EShaderInputLayoutType_PPFX);
	myLightPassPixelShaders[ELightType_Ambient] = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/AmbientLightPass.ps");
	myLightPassPixelShaders[ELightType_Direct] = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/DirectLightPass.ps");
	myLightPassPixelShaders[ELightType_Point] = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/PointLightPass.ps");
	myLightPassPixelShaders[ELightType_Spot] = &shaderManager.GetPixelShader(L"Assets/Shaders/Deferred/SpotLightPass.ps");

	if (!CreateVertexBuffer())
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create DeferredRenderer::myVertexBuffer");
		return false;
	}

	myIcosphere = IEngine::GetModelManager().myModelLoader.CreateIcosphere(1);

	return true;
}

void CDeferredRenderer::RenderDataPass(const CommonUtilities::GrowingArray<SModelRenderCommand>& aModelsToRender, CConstantBuffer& aCameraBuffer, const CommonUtilities::Vector4f& aCameraPosition, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);

	SInstanceBufferData instanceData;

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	
	ID3D11ShaderResourceView* cubemap = IEngine::GetTextureManager().GetTexture(IEngine::GetGraphicsPipeline().GetCubemap());

	unsigned short numModels = aModelsToRender.Size();
	unsigned short numModelsRendered = 0;

	for (const SModelRenderCommand& command : aModelsToRender)
	{
		CModel* model = IEngine::GetModelManager().GetModel(command.myModelToRender);
		if (!model)
		{
			continue;
		}
		if (Cull(command.myTransform, aFrustum, model->myModelData.myRadius))
		{
			continue;
		}

		CMaterial* modelMaterial = &model->myMaterial;
		if (command.myMaterial.GetVertexShader() != nullptr) 
		{
			command.myMaterial.GetVertexShader()->Bind();
			command.myMaterial.GetVertexShader()->BindLayout();
		}
		else if (modelMaterial->GetVertexShader() != nullptr) 
		{ 
			modelMaterial->GetVertexShader()->Bind(); 
			modelMaterial->GetVertexShader()->BindLayout();
		}
		else 
		{ 
			myDataPassVertexShader->Bind(); 
			myDataPassVertexShader->BindLayout();
		}
		
		if (command.myMaterial.GetPixelShader() != nullptr)
		{
			command.myMaterial.GetPixelShader()->Bind();
		}
		else if (modelMaterial->GetPixelShader() != nullptr)
		{
			modelMaterial->GetPixelShader()->Bind();
		}
		else 
		{
			myDataPassPixelShader->Bind();
		}		

		float distance = (command.myTransform.GetPosition() - CommonUtilities::Vector3f(aCameraPosition)).Length2();

		const SVertexDataWrapper& vertexData = model->GetVertexData(model->CalculateLodLevel(distance));
		STextureDataWrapper& textureData = model->GetTextureData();
		
		instanceData.myToWorld = command.myTransform.GetMatrix();

		myInstanceBuffer.SetData(&instanceData);

		context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
		context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

		if (model->myModelData.mySceneAnimator && model->myModelData.mySceneAnimator->HasSkeleton())
		{
			model->myModelData.mySceneAnimator->SetAnimIndex(command.myAnimationID);
			auto transforms = model->myModelData.mySceneAnimator->GetTransforms(IEngine::Time().GetTotalTime());
			
			myBoneBuffer.SetData(&transforms[0], static_cast<int>(transforms.size()) * sizeof(CommonUtilities::Matrix44f));
		}
		else
		{
			auto specialVec = CommonUtilities::Vector4f(0.f, 0.f, 0.f, -1.f);
			myBoneBuffer.SetData(&specialVec, sizeof(CommonUtilities::Vector4f));
		}

		myBoneBuffer.SetBuffer(2, EShaderType_Vertex);

		ID3D11ShaderResourceView* textures[STextureDataWrapper::maxTextures];
		for (int i = 0; i < textureData.maxTextures; ++i)
		{
			textures[i] = IEngine::GetTextureManager().GetTexture(textureData.myTextures[i]);
		}
		context->PSSetShaderResources(0, STextureDataWrapper::maxTextures, textures);

		context->PSSetShaderResources(STextureDataWrapper::maxTextures, 1, &cubemap);

		context->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);

		++numModelsRendered;
	}

	myDataPassVertexShader->Unbind();
	myDataPassVertexShader->UnbindLayout();
	myDataPassPixelShader->Unbind();

	numModels;
	//ENGINE_LOG(CONCOL_DEFAULT, "Culling: %d / %d", numModelsRendered, numModels);
}

void CDeferredRenderer::RenderLightPass(CConstantBuffer aCameraBuffer, const CommonUtilities::GrowingArray<SDirectionalLightRenderCommand>& aDirectionalLightRenderCommands, const CommonUtilities::GrowingArray<SPointLightRenderCommand>& aPointLightRenderCommands, const CommonUtilities::GrowingArray<SSpotLightRenderCommand>& aSpotLightRenderCommands, CFullscreenTexture & aGBuffer, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	ID3D11ShaderResourceView* cubemap = IEngine::GetTextureManager().GetTexture(IEngine::GetGraphicsPipeline().GetCubemap());

	context->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	aCameraBuffer.SetBuffer(0, EShaderType_Pixel);
	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);

	SScreenData screenData;
	screenData.myScreenSize = IEngine::GetCanvasSize();
	myScreenBuffer.SetData(&screenData);
	myScreenBuffer.SetBuffer(1, EShaderType_Pixel);

	aGBuffer.SetAsResourceOnSlot(0);
	context->PSSetShaderResources(aGBuffer.GetNumTargets(), 1, &cubemap);

	context->IASetVertexBuffers(0, 1, &myVertexData.myVertexBuffer, &myVertexData.myStride, &myVertexData.myOffset);
	context->IASetIndexBuffer(myVertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

	SInstanceBufferData instanceData;
	instanceData.myToWorld = CommonUtilities::Matrix44f();
	myInstanceBuffer.SetData(&instanceData);
	myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

	myLightPassVertexShader->Bind();
	myLightPassVertexShader->BindLayout();
	myLightPassPixelShaders[ELightType_Ambient]->Bind();
	RenderAmbientLight();

	myLightPassPixelShaders[ELightType_Direct]->Bind();
	RenderDirectLights(aDirectionalLightRenderCommands);

	myLightPassWorldVertexShader->Bind();
	myLightPassWorldVertexShader->BindLayout();

	myLightPassPixelShaders[ELightType_Point]->Bind();
	RenderPointLights(aPointLightRenderCommands, aFrustum);

	//TODO: Render spot lights
	myLightPassPixelShaders[ELightType_Spot]->Bind();
	RenderSpotLights(aSpotLightRenderCommands, aFrustum);

	myLightPassVertexShader->Unbind();
	myLightPassVertexShader->UnbindLayout();
	myLightPassPixelShaders[ELightType_Ambient]->Unbind();
}

bool CDeferredRenderer::CreateVertexBuffer()
{
	//Load vertex data
	struct Vertex
	{
		float x, y, z, w;
		float u, v;
	} vertices[4] =
	{
		{-1.f, +1.f, 0.f, 1.f,	0.f, 0.f},
		{+1.f, +1.f, 0.f, 1.f,	1.f, 0.f},
		{-1.f, -1.f, 0.f, 1.f,	0.f, 1.f},
		{+1.f, -1.f, 0.f, 1.f,	1.f, 1.f}
	};
	unsigned int indices[6] =
	{
		0,1,2,
		1,3,2
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = IEngine::GetDevice()->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
		return false;
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = IEngine::GetDevice()->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
		return false;
	}

	//Setup VertexData
	myVertexData.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	myVertexData.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	myVertexData.myStride = sizeof(Vertex);
	myVertexData.myOffset = 0;
	myVertexData.myVertexBuffer = vertexBuffer;
	myVertexData.myIndexBuffer = indexBuffer;

	return true;
}

void CDeferredRenderer::RenderAmbientLight()
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	context->DrawIndexed(myVertexData.myNumberOfIndices, 0, 0);
}

void CDeferredRenderer::RenderDirectLights(const CommonUtilities::GrowingArray<SDirectionalLightRenderCommand>& aLightRenderCommands)
{
	SLightBufferData lightData;

	for (const SDirectionalLightRenderCommand& lightRenderCommand : aLightRenderCommands)
	{
		lightData.myDirectionalLight = lightRenderCommand.myDirection;
		lightData.myDirectionalLightColor = {
			lightRenderCommand.myColor.x,
			lightRenderCommand.myColor.y,
			lightRenderCommand.myColor.z,
			lightRenderCommand.myIntensity
		};

		myDirectionalLightBuffer.SetData(&lightData);
		myDirectionalLightBuffer.SetBuffer(2, EShaderType_Pixel);

		IEngine::GetContext()->DrawIndexed(myVertexData.myNumberOfIndices, 0, 0);
	}
}

void CDeferredRenderer::RenderPointLights(const CommonUtilities::GrowingArray<SPointLightRenderCommand>& aLightRenderCommands, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	SInstanceBufferData instanceData;
	instanceData.myToWorld = CommonUtilities::Matrix44f();

	SPointLightBufferData pointLightData;
	int numPointLights = 0;
	for (const SPointLightRenderCommand& lightRenderCommand : aLightRenderCommands)
	{
		CTransform transform;
		transform.SetPosition(lightRenderCommand.myPosition);
		if (Cull(transform, aFrustum, lightRenderCommand.myRange))
		{
			continue;
		}

		const SVertexDataWrapper& vertexData = myIcosphere.myVertexData[0];
		context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
		context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		instanceData.myToWorld.myPosition = lightRenderCommand.myPosition;
		instanceData.myToWorld.myRightAxis.x = -lightRenderCommand.myRange;
		instanceData.myToWorld.myUpAxis.y = lightRenderCommand.myRange;
		instanceData.myToWorld.myForwardAxis.z = lightRenderCommand.myRange;

		myInstanceBuffer.SetData(&instanceData);
		myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

		pointLightData.position = lightRenderCommand.myPosition;
		pointLightData.range = lightRenderCommand.myRange;
		pointLightData.color = {
			lightRenderCommand.myColor.x,
			lightRenderCommand.myColor.y,
			lightRenderCommand.myColor.z,
			lightRenderCommand.myIntensity
		};

		myPointLightBuffer.SetData(&pointLightData);
		myPointLightBuffer.SetBuffer(2, EShaderType_Pixel);

		IEngine::GetContext()->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);
		++numPointLights;
	}

	//ENGINE_LOG(CONCOL_DEFAULT, "Num Lights: %d", numPointLights);
}

void CDeferredRenderer::RenderSpotLights(const CommonUtilities::GrowingArray<SSpotLightRenderCommand>& aLightRenderCommands, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	SInstanceBufferData instanceData;
	instanceData.myToWorld = CommonUtilities::Matrix44f();

	SSpotLightBufferData spotLightData;
	for (const SSpotLightRenderCommand& lightRenderCommand : aLightRenderCommands)
	{
		CTransform transform;
		transform.SetPosition(lightRenderCommand.myPosition);
		if (Cull(transform, aFrustum, lightRenderCommand.myRange))
		{
			continue;
		}

		const SVertexDataWrapper& vertexData = myIcosphere.myVertexData[0];
		context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
		context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		instanceData.myToWorld.myPosition = lightRenderCommand.myPosition;
		instanceData.myToWorld.myRightAxis.x = -lightRenderCommand.myRange;
		instanceData.myToWorld.myUpAxis.y = lightRenderCommand.myRange;
		instanceData.myToWorld.myForwardAxis.z = lightRenderCommand.myRange;

		myInstanceBuffer.SetData(&instanceData);
		myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);

		spotLightData.position = lightRenderCommand.myPosition;
		spotLightData.direction = lightRenderCommand.myDirection;
		spotLightData.color = {
			lightRenderCommand.myColor.x,
			lightRenderCommand.myColor.y,
			lightRenderCommand.myColor.z,
			lightRenderCommand.myIntensity
		};
		spotLightData.range = lightRenderCommand.myRange;
		spotLightData.angle = lightRenderCommand.myAngle;

		mySpotLightBuffer.SetData(&spotLightData);
		mySpotLightBuffer.SetBuffer(2, EShaderType_Pixel);

		IEngine::GetContext()->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);
	}
}

bool CDeferredRenderer::Cull(const CTransform& aTransform, const CommonUtilities::GrowingArray<CommonUtilities::Plane<float>>& aFrustum, const float& aRadius)
{
	const CommonUtilities::Vector3f point = aTransform.GetPosition();
	float radius = aRadius * aTransform.GetScale().Length();

	for (unsigned short i = 0; i < 6; ++i)
	{
		const auto& plane = aFrustum[i];
		if (plane.Distance(point) > radius)
		{
			return true;
		}
	}

	return false;
}

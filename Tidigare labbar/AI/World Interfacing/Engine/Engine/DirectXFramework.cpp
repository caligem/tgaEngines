#include "stdafx.h"
#include "DirectXFramework.h"

#include <d3d11.h>
#include <dxgi.h>

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "dxgi.lib")
#include "DXMacros.h"
#include "DL_Debug.h"
#include "Engine.h"
#include "GrowingArray.h"
#include "ScreenGrab.h"
#include <wincodec.h>

CDirectXFramework::CDirectXFramework()
{
	myContext = NULL;
	myDevice = NULL;
	mySwapChain = NULL;
}

CDirectXFramework::~CDirectXFramework()
{
	SafeRelease(myContext);
	SafeRelease(myDevice);
	SafeRelease(mySwapChain);
}

bool CDirectXFramework::Init(CWindowHandler& aWindowHandler, const SCreateParameters* aCreateParameters)
{
	myCreateParameters = aCreateParameters;

	HRESULT result;

	CommonUtilities::Vector2i numDenom;
	IDXGIAdapter* adapter = NULL;
	if (CollectAdapters({ aCreateParameters->myCanvasWidth, aCreateParameters->myCanvasHeight }, numDenom, adapter))
	{
		ENGINE_LOG(CONCOL_VALID, "VSYNC SUPPORT: YES, %d:%d!", numDenom.x, numDenom.y);
	}
	
	DXGI_SWAP_CHAIN_DESC swapchainDesc;
	ZeroMemory(&swapchainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));
	swapchainDesc.BufferCount = 1;
	swapchainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapchainDesc.BufferDesc.Width = myCreateParameters->myCanvasWidth;
	swapchainDesc.BufferDesc.Height = myCreateParameters->myCanvasHeight;
	swapchainDesc.BufferDesc.RefreshRate.Numerator = numDenom.x;
	swapchainDesc.BufferDesc.RefreshRate.Denominator = numDenom.y;
	swapchainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapchainDesc.OutputWindow = aWindowHandler.GetWindowHandle();
	swapchainDesc.SampleDesc.Count = 1;
	swapchainDesc.Windowed = !myCreateParameters->myIsFullscreen;
	swapchainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	swapchainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapchainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapchainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	
	UINT creationFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
#ifndef _RETAIL
        // If the project is in a debug build, enable the debug layer.
        creationFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	//Create swapchain, device and context
	result = D3D11CreateDeviceAndSwapChain(
		adapter,
		D3D_DRIVER_TYPE_UNKNOWN,
		NULL,
		creationFlags,
		NULL,
		NULL,
		D3D11_SDK_VERSION,
		&swapchainDesc,
		&mySwapChain,
		&myDevice,
		nullptr,
		&myContext
	);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to create the swapchain!");
		return false;
	}

	//Create backbuffer
	ID3D11Texture2D* backbufferTexture;
	ValidateResult(mySwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backbufferTexture));
	if (!myBackBuffer.Init(backbufferTexture))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to create the back buffer fullscreenTexture!");
		return false;
	}
	ValidateResult(backbufferTexture->Release());

	return true;
}

void CDirectXFramework::Present()
{
	if (myCreateParameters->myEnableVSync)
	{
		mySwapChain->Present(1, 0);
	}
	else
	{
		mySwapChain->Present(0, 0);
	}
}

void CDirectXFramework::SaveScreenShot(const std::wstring& aSavePath)
{
	ID3D11Resource* resource = NULL;
	myBackBuffer.GetRenderTargetView()->GetResource(&resource);

	if (resource != NULL)
	{
		HRESULT result = DirectX::SaveWICTextureToFile(
			myContext, resource,
			GUID_ContainerFormatJpeg, aSavePath.c_str()
		);

		if (FAILED(result))
		{
			ENGINE_LOG(CONCOL_ERROR, "Failed to Save Screenshot!");
		}
	}

	resource->Release();
}

bool CDirectXFramework::CollectAdapters(const CommonUtilities::Vector2<unsigned short>& aWindowSize, CommonUtilities::Vector2i & aOutNumDenom, IDXGIAdapter *& aOutAdapter)
{
	HRESULT result = -1;

	IDXGIFactory* factory = NULL;

	result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create factory in CDirectXFramework::CollectAdapters");
		return false;
	}

	IDXGIAdapter* usingAdapter = nullptr;
	int adapterIndex = 0;
	CommonUtilities::GrowingArray<DXGI_ADAPTER_DESC> myAdapterDescs(2);
	CommonUtilities::GrowingArray<IDXGIAdapter*> myAdapters(2);
	while (factory->EnumAdapters(adapterIndex, &usingAdapter) != DXGI_ERROR_NOT_FOUND)
	{
		DXGI_ADAPTER_DESC adapterDesc;
		usingAdapter->GetDesc(&adapterDesc);
		myAdapterDescs.Add(adapterDesc);
		myAdapters.Add(usingAdapter);
		++adapterIndex;
	}

	if (adapterIndex == 0)
	{
		return 0;
	}

	ENGINE_LOG(CONCOL_DEFAULT, "Video cards detected:");
	for (DXGI_ADAPTER_DESC& desc : myAdapterDescs)
	{
		int memory = static_cast<int>(desc.DedicatedVideoMemory / 1024 / 1024);
		ENGINE_LOG(CONCOL_DEFAULT, "\t%ls%s%i%s", desc.Description, " Mem: ", memory, "MB");
		memory;
	}

	DXGI_ADAPTER_DESC usingAdapterDesc = myAdapterDescs[0];
	usingAdapter = myAdapters[0];

	ENGINE_LOG(CONCOL_DEFAULT, "Detecting best graphics card...");

	const std::wstring nvidia = L"NVIDIA";
	const std::wstring ati = L"ATI";

	int memory = 0;
	int mostMem = 0;
	for (unsigned short i = 0; i < myAdapterDescs.Size(); ++i)
	{
		DXGI_ADAPTER_DESC& desc = myAdapterDescs[i];
		memory = static_cast<int>(desc.DedicatedVideoMemory / 1024 / 1024);
		std::wstring name = desc.Description;
		if (name.find(nvidia) != name.npos || name.find(ati) != name.npos)
		{
			if (memory > mostMem)
			{
				mostMem = memory;
				usingAdapterDesc = desc;
				usingAdapter = myAdapters[i];
			}
		}
	}

	ENGINE_LOG(CONCOL_VALID, "%s%ls%s%i%s", "Using graphics card: ", usingAdapterDesc.Description, " Dedicated Mem: ", mostMem, "MB");

	DXGI_MODE_DESC* displayModeList = nullptr;
	unsigned int numModes = 0;
	CommonUtilities::Vector2i numDenom;

	IDXGIOutput* output = nullptr;
	if (usingAdapter->EnumOutputs(0, &output) != DXGI_ERROR_NOT_FOUND)
	{
		result = output->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
		if (SUCCEEDED(result))
		{
			displayModeList = new DXGI_MODE_DESC[numModes];
			if (displayModeList)
			{
				result = output->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
				if (SUCCEEDED(result))
				{
					for (unsigned int i = 0; i < numModes; ++i)
					{
						if (displayModeList[i].Width == aWindowSize.x && displayModeList[i].Height == aWindowSize.y)
						{
							numDenom.x = displayModeList[i].RefreshRate.Numerator;
							numDenom.y = displayModeList[i].RefreshRate.Denominator;
						}
					}
				}
			}
		}
		SafeRelease(output);
	}

	result = usingAdapter->GetDesc(&usingAdapterDesc);
	if (FAILED(result))
	{
		return false;
	}

	SAFE_DELETE_ARRAY(displayModeList);
	SafeRelease(factory);

	if (myCreateParameters->myEnableVSync)
	{
		aOutNumDenom = numDenom;
	}
	else
	{
		aOutNumDenom = { 0, 1 };
	}

	aOutAdapter = usingAdapter;

	return true;
}

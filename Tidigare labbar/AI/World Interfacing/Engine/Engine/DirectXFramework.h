#pragma once

#include "WindowHandler.h"
#include "FullscreenTexture.h"
#include "Vector.h"

struct SCreateParameters;

struct IDXGISwapChain;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11RenderTargetView;
struct ID3D11DepthStencilView;
struct IDXGIAdapter;

class CDirectXFramework
{
public:
	CDirectXFramework();
	~CDirectXFramework();

	bool Init(CWindowHandler& aWindowHandler, const SCreateParameters* aCreateParameters);

	void Present();

	ID3D11Device* GetDevice() { return myDevice; }
	ID3D11DeviceContext* GetContext() { return myContext; }

	CFullscreenTexture* GetBackBuffer() { return &myBackBuffer; }
	void SaveScreenShot(const std::wstring& aSavePath);
private:
	bool CollectAdapters(const CommonUtilities::Vector2<unsigned short>& aWindowSize, CommonUtilities::Vector2i& aOutNumDenom, IDXGIAdapter*& aOutAdapter);

	IDXGISwapChain* mySwapChain;
	ID3D11Device* myDevice;
	ID3D11DeviceContext* myContext;

	CFullscreenTexture myBackBuffer;

	const SCreateParameters* myCreateParameters;

};



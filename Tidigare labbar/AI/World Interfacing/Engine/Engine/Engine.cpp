#include "stdafx.h"
#include "Engine.h"

#include "IEngine.h"

#include "ComponentSystem.h"
#include "GameObject.h"

#include <DirectXMath.h>

#include "Random.h"
#include "CommandLineManager.h"
#include "SystemStats.h"

#include <iomanip>

#define MULTITHREADED

CEngine::CEngine()
	: myXBoxInput(0)
	, myXBoxInput2(1)
{
}

CEngine::~CEngine()
{
	myWorkerPool.Destroy();
	myReportManager.Shutdown();
}

bool CEngine::Init(const SCreateParameters& aCreateParameters)
{
	START_TIMER(startupTime);
	CommonUtilities::InitRand();

	ENGINE_LOG(CONCOL_DEFAULT, "===============================");
	ENGINE_LOG(CONCOL_DEFAULT, "=== Starting The Engine... ===");
	ENGINE_LOG(CONCOL_DEFAULT, "===============================");
	myCreateParameters = aCreateParameters;

	IEngine::ourEngine = this;

	if (!myWindowHandler.Init(&aCreateParameters))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise WindowHandler!");
		return false;
	}

	if (!myFramework.Init(myWindowHandler, &aCreateParameters))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise DirectXFramework!");
		return false;
	}

	myTextureManager.Init();

	//Managers Init
	if (!mySceneManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise SceneManager!");
		return false;
	}
	if (!myModelManager.Init(myFramework))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise ModelManager.");
		return false;
	}
	if (!myCameraManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise CameraManager.");
		return false;
	}
	if (!myParticleManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise ParticleManager.");
		return false;
	}
	if (!mySpriteManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise SpriteManager.");
		return false;
	}
	if (!myTextManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise SpriteManager.");
		return false;
	}
	if (!myComponentSystem.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise Component System.");
		return false;
	}

	ManagerInjection();

	// Graphics
	if (!myGraphicsPipeline.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise GraphicsPipeline.");
		return false;
	}

#ifndef MULTITHREADED
	myWorkerPool.Init(1);
#else
	unsigned int maxThreads = std::thread::hardware_concurrency();
	if (maxThreads == 0)
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: No cores found!");
		return false;
	}
	else
	{
		ENGINE_LOG(CONCOL_VALID, "Created WorkerPool using: %d threads!", maxThreads);
	}
	myWorkerPool.Init(maxThreads);
#endif

	if (!myReportManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise ReportManager.");
		return false;
	};

	myShouldTakeScreenshot = false;
	myScreenshotPath = L"";

	ENGINE_LOG(CONCOL_VALID, "Engine startup time: %f ms", GET_TIME(startupTime));
	return true;
}

void CEngine::Shutdown()
{
	myIsRunning = false;
	myWindowHandler.CloseWindow();
}

void CEngine::StartEngine()
{
	if (myCreateParameters.myInitFunctionToCall)
	{
		myCreateParameters.myInitFunctionToCall();
	}

	myUpdateFunctionToCall = myCreateParameters.myUpdateFunctionToCall;

	myIsRunning = true;

	if (myCreateParameters.myUpdateFunctionToCall)
	{
		Run();
	}
}

void CEngine::Run()
{
	MSG windowsMessage = { 0 };

	while (myIsRunning)
	{
		START_TIMER(frameMS);
		while (PeekMessage(&windowsMessage, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&windowsMessage);
			DispatchMessage(&windowsMessage);

			if (windowsMessage.message == WM_QUIT)
			{
				myIsRunning = false;
			}
		}

		if (myShouldTakeScreenshot)
		{
			myFramework.SaveScreenShot(myScreenshotPath);
			myShouldTakeScreenshot = false;
		}

		myReportManager.Update();
		myWindowHandler.Update();

		myTimer.Update();

		myWorkerPool.DoWork(std::bind(&CEngine::MainJob, this));
		myWorkerPool.DoWork(std::bind(&CEngine::RenderJob, this));

		myWorkerPool.Wait();

		myWorkerPool.DoWork(std::bind(&CEngine::SyncJob, this));

		myWorkerPool.Wait();

		myInputManager.Update();
		myXBoxInput.Update();
		myXBoxInput2.Update();

		myTotalFrameMS = GET_TIME(frameMS);

#ifndef _RETAIL
		DebugDrawSystemStats();
#endif
	}

	ENGINE_LOG(CONCOL_DEFAULT, "===============================");
	ENGINE_LOG(CONCOL_DEFAULT, "=== Engine Shutting Down... ===");
	ENGINE_LOG(CONCOL_DEFAULT, "===============================");
}

void CEngine::MainJob()
{
	myComponentSystem.RunDestroys();
	myComponentSystem.UpdateComponents();

	START_TIMER(gameLogicMS);
	myUpdateFunctionToCall();
	myGameLogicMS = GET_TIME(gameLogicMS);

	myGraphicsPipeline.SetRenderBuffers();
}

void CEngine::RenderJob()
{
	START_TIMER(renderMS);
	myGraphicsPipeline.BeginFrame();
	myReportManager.NewFrame();
	myGraphicsPipeline.Render();
	myReportManager.Render();
	myFramework.Present();
	myRenderMS = GET_TIME(renderMS);
}

void CEngine::SyncJob()
{
	myGraphicsPipeline.SwapBuffers();
	myFileWatcher.FlushChanges();

	myGraphicsPipeline.SetCameraBuffer();

	mySceneManager.DestroyScenesInQueue();
}

void CEngine::DebugDrawSystemStats()
{
	static CommonUtilities::GrowingArray<float, int> fpsStack(60);
	if (fpsStack.Size() >= 60) fpsStack.Shift();

	fpsStack.Add(myTotalFrameMS);
	float fps = 0.f;
	for (float t : fpsStack) fps += t;
	fps /= static_cast<float>(fpsStack.Size());
	std::string fpsStr = ("FPS: " + std::to_string(floorf(1000.f / fps)));
	fpsStr.resize(fpsStr.length() - 4);
	myGraphicsPipeline.GetDebugRenderer().DrawDebugText(fpsStr);

	std::string cpuUsage = "cpu usage: " + std::to_string(CSystemStats::CPUUsage());
	cpuUsage.resize(cpuUsage.length() - 4);
	cpuUsage.append("%");

	myGraphicsPipeline.GetDebugRenderer().DrawDebugText(cpuUsage);
	myGraphicsPipeline.GetDebugRenderer().DrawDebugText(("mem usage: " + std::to_string(CSystemStats::MemUsage()) + "MB"));

	std::string frameMS = ("total frame ms: " + std::to_string(roundf(myTotalFrameMS)));
	frameMS.resize(frameMS.length() - 4);
	myGraphicsPipeline.GetDebugRenderer().DrawDebugText(frameMS);

	if (CommonUtilities::CCommandLineManager::HasArgument("-activateLog", "engine"))
	{
		myEngineMS = myTotalFrameMS - (myRenderMS + myGameLogicMS);
		std::string engineMS = ("engine ms : " + std::to_string(roundf(myEngineMS)));
		engineMS.resize(engineMS.length() - 4);
		myGraphicsPipeline.GetDebugRenderer().DrawDebugText(engineMS);
	}

	if (CommonUtilities::CCommandLineManager::HasArgument("-activateLog", "gameplay"))
	{
		std::string gameLogicMS = ("gameLogic ms: " + std::to_string(roundf(myGameLogicMS)));
		gameLogicMS.resize(gameLogicMS.length() - 4);
		myGraphicsPipeline.GetDebugRenderer().DrawDebugText(gameLogicMS);
	}

	std::string renderMS = ("render ms: " + std::to_string(roundf(myRenderMS)));
	renderMS.resize(renderMS.length() - 4);
	myGraphicsPipeline.GetDebugRenderer().DrawDebugText(renderMS);

	//spacing to seperate system stats
	myGraphicsPipeline.GetDebugRenderer().DrawDebugText("");
}

void CEngine::SaveScreenShot(const std::wstring & aSavePath)
{
	myShouldTakeScreenshot = true;
	myScreenshotPath = aSavePath;
}

void CEngine::ManagerInjection()
{
	CModelComponent::ourModelManager = &myModelManager;
	CCameraComponent::ourCameraManager = &myCameraManager;
	CParticleSystemComponent::ourParticleManager = &myParticleManager;
	CStreakComponent::ourParticleManager = &myParticleManager;
	CSpriteComponent::ourSpriteManager = &mySpriteManager;
	CTextComponent::ourTextManager = &myTextManager;
	CGameObject::ourComponentSystem = &myComponentSystem;
	CTransform::ourComponentSystem = &myComponentSystem;
}


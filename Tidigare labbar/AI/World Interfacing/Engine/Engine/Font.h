#pragma once

#include <map>

#include "Vector.h"

#include "TextureManager.h"

class C2DRenderer;
class CDebugRenderer;

class CFont
{
public:
	CFont();
	~CFont();

	bool Init(const std::string& aFilepath);

	struct SCharData
	{
		CommonUtilities::Vector2f myUVOffset;
		CommonUtilities::Vector2f myUVScale;
		CommonUtilities::Vector2f myOffset;
		float myXAdvance = 0.f;
	};

	const SCharData& GetData(char aChar) const { if (myMap.find(aChar) == myMap.end()) { return myNullData; }  return myMap.at(aChar); }
	const float GetLineHeight() const { return myLineHeight; }
	const float GetBaseLine() const { return myBaseLine; }

	const CommonUtilities::Vector2f& GetOriginalTextureSize() const { return myOriginalTextureSize; }

private:
	friend C2DRenderer;
	friend CDebugRenderer;

	bool InitTextData(const std::string& aFilepath);

	SCharData myNullData;

	std::map<char, SCharData> myMap;

	float myLineHeight;
	float myBaseLine;

	SRV myTexture;

	CommonUtilities::Vector2f myOriginalTextureSize;

};


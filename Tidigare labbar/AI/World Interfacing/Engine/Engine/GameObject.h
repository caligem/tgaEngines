﻿#pragma once
#include "Vector.h"
#include "ObjectPool.h"
#include "Transform.h"
#include "ComponentSystem.h"

#include <Macros.h>

class CScene;
class CEngine;

class CGameObject
{
public:
	CGameObject();
	~CGameObject();

	virtual void Init(ID_T(CScene) aSceneID, ID_T(CGameObjectData) aParentID = ID_T_INVALID(CGameObjectData));
	void PointToObject(const CGameObject& aGameObject);

	template<typename T>
	T* GetComponent();

	template<typename T>
	CommonUtilities::GrowingArray<T*> GetComponents();

	template<typename T>
	void RemoveComponent(T*& aComponent);

	template<typename T>
	T* AddComponent(const typename T::SComponentData& aComponentData);
	template<typename T>
	T* AddComponent();

	inline CTransform& GetTransform() { return ourComponentSystem->GetGameObjectData(myGameObjectDataID, mySceneID)->GetTransform(); }

	void SetActive(bool aIsActive);
	const bool IsActive();

	void Destroy();

	const bool IsValid() const { return myGameObjectDataID != ID_T_INVALID(CGameObjectData); }

private:
	friend CComponentSystem;
	friend CEngine;
	friend CTransform;

	static CComponentSystem* ourComponentSystem;

	ID_T(CGameObjectData) myGameObjectDataID;
	ID_T(CScene) mySceneID;

	DefineDebugPtr(CGameObjectData);
	DefineDebugPtr(CScene);

	bool IsValidCheck() const;
};

template<typename T>
inline T* CGameObject::GetComponent()
{
	return ourComponentSystem->GetComponent<T>(myGameObjectDataID, mySceneID);
}

template<typename T>
inline CommonUtilities::GrowingArray<T*> CGameObject::GetComponents()
{
	return ourComponentSystem->GetComponents<T>(myGameObjectDataID, mySceneID);
}

template<typename T>
inline void CGameObject::RemoveComponent(T*& aComponent)
{
	if (aComponent == nullptr)
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to remove component but component is nullptr.");
		return;
	}
	if (!ourComponentSystem->RemoveComponent<T>(aComponent))
	{
		ENGINE_LOG(CONCOL_WARNING, "Failed to remove component: %s from GameObject, we dont know why!\n^\\_('',)_/^ maybe you've already removed it ._.", typeid(T).name());
	}
	aComponent = nullptr;
}

template<typename T>
inline T* CGameObject::AddComponent(const typename T::SComponentData& aComponentData)
{
	if (!IsValidCheck())
	{
		return nullptr;
	}
	return ourComponentSystem->AddComponent<T>(myGameObjectDataID, mySceneID, aComponentData);
}
template<typename T>
inline T* CGameObject::AddComponent()
{
	if (!IsValidCheck())
	{
		return nullptr;
	}
	return ourComponentSystem->AddComponent<T>(myGameObjectDataID, mySceneID);
}

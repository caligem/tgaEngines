#pragma once

class CPixelShader;
class CVertexShader;

class CMaterial
{
public:
	CMaterial() { myPixelShader = nullptr; myVertexShader = nullptr; }
	~CMaterial() {}
	const CPixelShader* GetPixelShader() const { return myPixelShader; }
	const CVertexShader* GetVertexShader() const { return myVertexShader; }
	void SetVertexShader(const std::wstring & aShaderFile);
	void SetPixelShader(const std::wstring & aShaderFile);

private:
	CVertexShader* myVertexShader;
	CPixelShader* myPixelShader;
};
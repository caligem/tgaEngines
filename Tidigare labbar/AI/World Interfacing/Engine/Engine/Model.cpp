#include "stdafx.h"
#include "Model.h"

#include "DXMacros.h"
#include <d3d11.h>

CModel::CModel()
{
}

CModel::~CModel()
{
	for (unsigned char i = 0; i < SModelData::MaxLODs; ++i)
	{
		SafeRelease(myModelData.myVertexData[i].myIndexBuffer);
		SafeRelease(myModelData.myVertexData[i].myVertexBuffer);
	}
	SAFE_DELETE(myModelData.mySceneAnimator);
}

void CModel::Init(const SModelData & aModelData)
{
	myModelData = aModelData;
}

unsigned char CModel::CalculateLodLevel(float aDistance)
{
	for (unsigned char i = myModelData.myLodCount; i > 0; --i)
	{
		if (aDistance >= myModelData.myVertexData[i-1].myLODDistance*myModelData.myVertexData[i-1].myLODDistance)
		{
			return i == myModelData.myLodCount ? i - 1 : i;
		}
	}

	return 0;
}

#pragma once

#include "Matrix.h"
#include "Vector.h"
#include "Component.h"
#include "Material.h"

class CModel;
class CForwardRenderer;
class CEngine;
class CModelManager;
class CGraphicsPipeline;
class CComponentSystem;

class CModelComponent : public CComponent
{
public:
	CModelComponent();
	~CModelComponent();

	struct SComponentData
	{
		const char* myFilePath;
	};
	void AssignMaterial(const std::wstring & aShaderFile);
	inline const float GetRadius() const { return myRadius; }

private:
	friend CForwardRenderer;
	friend CEngine;
	friend CGraphicsPipeline;
	friend CComponentSystem;

	void Init(const SComponentData& aComponentData);
	void Release() override;

	static CModelManager* ourModelManager;
	float myRadius;

	ID_T(CModel) myModelID;
	CMaterial myMaterial;
};


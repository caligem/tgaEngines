#include "stdafx.h"
#include "ParticleEmitter.h"

#include "IEngine.h"

#include "DXMacros.h"

CParticleEmitter::CParticleEmitter()
{
}

CParticleEmitter::~CParticleEmitter()
{
	SafeRelease(myVertexData.myIndexBuffer);
	SafeRelease(myVertexData.myVertexBuffer);
}

void CParticleEmitter::SetTexture(const char * aTexturePath)
{
	myData.myTexturePath = aTexturePath;

	myTexture = IEngine::GetTextureManager().CreateTextureFromFile(myData.myTexturePath);
}

void CParticleEmitter::SetSpawnRate(float aSpawnRate)
{
	myData.mySpawnRate = aSpawnRate;
	RebuildBuffer();
}

void CParticleEmitter::SetLifetime(float aLifetime)
{
	myData.myLifetime = aLifetime;
	RebuildBuffer();
}

void CParticleEmitter::RebuildBuffer()
{
	HRESULT result;

	if (myData.myLifetime <= 0.0f)
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to rebuild ParticleSystemComponent but Lifetime is below 0");
		return;
	}
	if (myData.mySpawnRate <= 0.0f)
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to rebuild ParticleSystemComponent but SpawnRate is below 0");
		return;
	}

	myMaxParticleCount = static_cast<unsigned int>(myData.myLifetime * myData.mySpawnRate) + myBufferPadding;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = (myMaxParticleCount) * myVertexBufferSize;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	result = IEngine::GetDevice()->CreateBuffer(&bufferDesc, nullptr, &myVertexData.myVertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer in ParticleEmitter when trying to rebuild!");
		return;
	}

	myVertexData.myStride = myVertexBufferSize;
	myVertexData.myOffset = 0;
}

void CParticleEmitter::ResetToDefault()
{
	myData = SParticleData();
	RebuildBuffer();
	SetTexture("");
}

bool CParticleEmitter::Init(SParticleData aParticleData, unsigned int aVertexBufferSize)
{
	HRESULT result;

	myData = aParticleData;
	myVertexBufferSize = aVertexBufferSize;

	if (myData.myLifetime <= 0.0f)
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to create ParticleSystemComponent but Lifetime is below 0");
		return false;
	}
	if (myData.mySpawnRate <= 0.0f)
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to create ParticleSystemComponent but SpawnRate is below 0");
		return false;
	}

	myMaxParticleCount = static_cast<unsigned int>(myData.myLifetime * myData.mySpawnRate) + myBufferPadding;

	D3D11_BUFFER_DESC bufferDesc = {};
	bufferDesc.ByteWidth = (myMaxParticleCount)  * myVertexBufferSize;
	bufferDesc.Usage = D3D11_USAGE_DYNAMIC;
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

	result = IEngine::GetDevice()->CreateBuffer(&bufferDesc, nullptr, &myVertexData.myVertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer in ParticleEmitter!");
		return false;
	}

	myTexture = IEngine::GetTextureManager().CreateTextureFromFile(myData.myTexturePath);

	myVertexData.myStride = myVertexBufferSize;
	myVertexData.myOffset = 0;

	return true;
}
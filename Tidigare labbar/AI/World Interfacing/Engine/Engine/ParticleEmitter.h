#pragma once

#include "VertexDataWrapper.h"
#include "RefCounter.h"
#include "GraphicsStateManager.h"

#include "TextureManager.h"

class CParticleRenderer;

class CParticleEmitter : public RefCounter
{
public:
	CParticleEmitter();
	~CParticleEmitter();

	struct SParticleData
	{
		struct SShapeData
		{
			enum EShapeType
			{
				ESphere,
				EBox,
				EPoint,
				ECount
			};

			CommonUtilities::Vector3f myBoxSize = {1.f, 1.f, 1.f};
			float myBoxThickness = 0.f;
			float myRadius = 1.f;
			float myRadiusThickness = 0.f;
			EShapeType myShapeType = EPoint;
		};

		SParticleData(SShapeData aShapeData = SShapeData(), const std::string& aTexturePath = "",
			bool aIsLoopable = false,
			float aDuration = 3.0f,
			float aParticleLifetime = 3.0f,
			float aParticleSpawnRate = 3.0f,
			CommonUtilities::Vector3f aAcceleration = { 0.f, 1.f, 0.f },
			CommonUtilities::Vector3f aStartVelocity = { 0.f, 1.f, 0.f },
			CommonUtilities::Vector2f aStartRotation = { 0.f, 0.f },
			CommonUtilities::Vector2f aRotationVelocity = { 0.f, 0.f },
			CommonUtilities::Vector4f aStartColor = { 1.f, 0.f, 0.f, 1.0f },
			float aGravityModifier = 0.f,
			CommonUtilities::Vector4f aEndColor = { 0.f, 1.f, 0.0f, 0.f },
			CommonUtilities::Vector2f aStartSize = { 0.5f, 0.5f },
			CommonUtilities::Vector2f aEndSize = { 1.f, 1.f },
			CGraphicsStateManager::EBlendState aBlendState = CGraphicsStateManager::EBlendState_AlphaBlend)
		{
			myShapeData = aShapeData;
			myTexturePath = aTexturePath;
			myStartColor = aStartColor;
			myEndColor = aEndColor;
			myAcceleration = aAcceleration;
			myStartVelocity = aStartVelocity;
			myStartSize = aStartSize;
			myEndSize = aEndSize;
			myStartRotation = aStartRotation;
			myRotationVelocity = aRotationVelocity;
			myBlendState = aBlendState;
			myDuration = aDuration;
			myLifetime = aParticleLifetime;
			mySpawnRate = aParticleSpawnRate;
			myGravityModifier = aGravityModifier;
			myIsLoopable = aIsLoopable;
		};

		SShapeData myShapeData;
		std::string myTexturePath = "";
		CommonUtilities::Vector4f myStartColor = { 1.f, 0.f, 0.f, 1.f };
		CommonUtilities::Vector4f myEndColor = { 0.f, 1.f, 0.f, 0.f };
		CommonUtilities::Vector3f myAcceleration = { 0.f, 1.f, 0.f };
		CommonUtilities::Vector3f myStartVelocity = { 0.f, 1.f, 0.f };
		CommonUtilities::Vector2f myStartSize = { 0.5f, 0.5f };
		CommonUtilities::Vector2f myEndSize = { 1.f, 1.f };
		CommonUtilities::Vector2f myStartRotation = { 0.f, 0.f };
		CommonUtilities::Vector2f myRotationVelocity = { 0.f, 0.f };
		CGraphicsStateManager::EBlendState myBlendState = CGraphicsStateManager::EBlendState_AlphaBlend;
		float myDuration = 3.f;
		float myLifetime = 3.f;
		float mySpawnRate = 3.f;
		float myGravityModifier = 0.f;
		bool myIsLoopable = false;

	};

	typedef SParticleData::SShapeData::EShapeType ShapeType;

	void SetTexture(const char* aTexturePath);
	void SetSpawnRate(float aSpawnRate);
	void SetLifetime(float aLifetime);
	inline void SetBlendState(CGraphicsStateManager::EBlendState aBlendState) { myData.myBlendState = aBlendState; }

	inline void SetDuration(float aDuration) { myData.myDuration = aDuration; }
	inline void SetIsLoopable(bool aIsLoopable) { myData.myIsLoopable = aIsLoopable; }
	inline void SetAcceleration(CommonUtilities::Vector3f aAcceleration) { myData.myAcceleration = aAcceleration; }
	inline void SetStartVelocity(CommonUtilities::Vector3f aStartVelocity) { myData.myStartVelocity = aStartVelocity; }
	inline void SetStartRotation(CommonUtilities::Vector2f aStartRotation) { myData.myStartRotation = aStartRotation; }
	inline void SetRotationVelocity(CommonUtilities::Vector2f aRotationVelocity) { myData.myRotationVelocity = aRotationVelocity; }
	inline void SetGravityModifier(float aGravityModifier) { myData.myGravityModifier = aGravityModifier; }
	inline void SetStartColor(CommonUtilities::Vector4f aStartColor) { myData.myStartColor = aStartColor; }
	inline void SetEndColor(CommonUtilities::Vector4f aEndColor) { myData.myEndColor = aEndColor; }
	inline void SetStartSize(CommonUtilities::Vector2f aStartSize) { myData.myStartSize = aStartSize; }
	inline void SetEndSize(CommonUtilities::Vector2f aEndSize) { myData.myEndSize = aEndSize; }

	inline const float GetDuration() const { return myData.myDuration; }
	inline const char* GetTexturePath() const { return myData.myTexturePath.c_str(); }
	inline const float GetSpawnRate() const { return myData.mySpawnRate; }
	inline CGraphicsStateManager::EBlendState GetBlendState() { return myData.myBlendState; }

	inline const float GetLifetime() const { return myData.myLifetime; }
	inline const bool GetIsLoopable() const { return myData.myIsLoopable; }
	inline const CommonUtilities::Vector3f& GetAcceleration() const { return myData.myAcceleration; }
	inline const CommonUtilities::Vector3f& GetStartVelocity() const { return myData.myStartVelocity; }
	inline const CommonUtilities::Vector2f& GetStartRotation() const { return myData.myStartRotation; }
	inline const CommonUtilities::Vector2f& GetRotationVelocity() const { return myData.myRotationVelocity; }
	inline const float GetGravityModifier() { return myData.myGravityModifier; }
	inline const CommonUtilities::Vector4f& GetStartColor() const { return myData.myStartColor; }
	inline const CommonUtilities::Vector4f& GetEndColor() const { return myData.myEndColor; }
	inline const CommonUtilities::Vector2f& GetStartSize() const { return myData.myStartSize; }
	inline const CommonUtilities::Vector2f& GetEndSize() const { return myData.myEndSize; }
	inline unsigned int GetMaxParticleCount() { return myMaxParticleCount; }

	void ResetToDefault();

	bool Init(SParticleData aParticleData, unsigned int aVertexBufferSize);

	SParticleData::SShapeData& GetShapeData() { return myData.myShapeData; }
	void SetShapeData(SParticleData::SShapeData aShapeData) { myData.myShapeData = aShapeData; }

private:
	friend CParticleRenderer;

	void RebuildBuffer();

	SRV myTexture;
	SParticleData myData;
	SVertexDataWrapper myVertexData;

	unsigned int myVertexBufferSize;
	unsigned int myMaxParticleCount;
	static constexpr char myBufferPadding = 8;
};


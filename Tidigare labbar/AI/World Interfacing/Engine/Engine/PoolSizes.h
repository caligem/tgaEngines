#pragma once

constexpr int PoolSizeComponentSystemComponentStorages = 8;

//Component storage pools
constexpr int PoolSizeModelComponent = 4096;
constexpr int PoolSizeCameraComponent = 4096; //16
constexpr int PoolSizeAnimationControllerComponent = 4096;
constexpr int PoolSizeAudioSourceComponent = 4096; //512
constexpr int PoolSizeAudioListenerComponent = 4096; //8
constexpr int PoolSizeLightComponent = 4096; //1024
constexpr int PoolSizeParticleSystemComponent = 4096; //1024
constexpr int PoolSizeStreakComponent = 4096; //1024
constexpr int PoolSizeSpriteComponent = 4096; //128
constexpr int PoolSizeTextComponent = 4096; //64
constexpr int PoolSizeCanvasComponent = 4096; //64
constexpr int PoolSizeButtonComponent = 4096; //64

constexpr int PoolSizeComponentStorageGameObjectData = 8192;

//Managers
constexpr int PoolSizeParticleManagerParticleEmitters = PoolSizeParticleSystemComponent;
constexpr int PoolSizeParticleManagerStreaks = PoolSizeStreakComponent;
constexpr int PoolSizeTextureManagerTextures = 8192;
constexpr int PoolSizeCameraManagerCameras = PoolSizeCameraComponent;
constexpr int PoolSizeSceneManagerScenes = PoolSizeComponentSystemComponentStorages;
constexpr int PoolSizeSpriteManagerSprites = PoolSizeSpriteComponent;
constexpr int PoolSizeTextManagerFonts = 2;
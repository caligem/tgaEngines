#include "stdafx.h"
#include "Scene.h"
#include "IEngine.h"
#include "ComponentSystem.h"

CScene::CScene()
	: myActiveCamera(ID_T_INVALID(CCameraComponent))
{
}

CScene::~CScene()
{
	if (mySceneID != ID_T_INVALID(CScene))
	{
		ID_T(CComponentStorage) storageID = IEngine::GetComponentSystem().GetComponentStorageIDBasedOnSceneID(mySceneID);
		IEngine::GetComponentSystem().myComponentStorages.Release(storageID);
		mySceneID = ID_T_INVALID(CScene);
	}
}

void CScene::Init(ID_T(CScene) aSceneID)
{
	mySceneID = aSceneID;

	//TODO:_CMS
#include "RedefineComponentRegistrer.h"
#define ComponentRegister(Type, Container, Size) \
AddComponentToGrowingArray(_UUID(##Type), ##Size);
#include "RegisteredComponents.h"

	myDirectionalLight = { -1.f, -1.f, 1.f };
	myDirectionalLight.Normalize();

	mySkybox = IEngine::GetTextureManager().CreateTextureFromFile("Assets/CubeMaps/blackSkybox.dds");
	myCubemap = IEngine::GetTextureManager().CreateTextureFromFile("Assets/CubeMaps/blackSkybox.dds");

	IEngine::GetComponentSystem().AcquireComponentStorage(mySceneID);
}

void CScene::AddComponentID(_UUID_T aUUID, int aID)
{
	myComponentIDs[aUUID].Add(aID);
}

void CScene::FillRenderBuffer(_UUID_T aUUID_T, CommonUtilities::GrowingArray<int>& aBuffer)
{
	aBuffer = myComponentIDs[aUUID_T];
}

CCameraComponent* CScene::GetActiveCamera()
{
	if (myActiveCamera == ID_T_INVALID(CCameraComponent))
	{
		ENGINE_LOG(CONCOL_WARNING, "Trying to Get Active Camera, but it's INVALID.");
	}

	return IEngine::GetComponentSystem().GetComponent<CCameraComponent>(myActiveCamera.val, mySceneID);
}

void CScene::SetActiveCamera(ID_T(CCameraComponent) aCameraInstanceID)
{
	if(!GrowingArrayContainsUUID(_UUID(CCameraComponent)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to set Active Camera but Scene doesnt have any GrowingArray<CameraComponent>");
		return;
	}
	if (myComponentIDs[_UUID(CCameraComponent)].Find(aCameraInstanceID.val) == myComponentIDs[_UUID(CCameraComponent)].FoundNone)
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to set an active camera but it doesnt exist in scene.");
		return;
	}

	myActiveCamera = aCameraInstanceID;
}

void CScene::RemoveComponentID(_UUID_T aUUID, int aID)
{
	if (!GrowingArrayContainsUUID(aUUID))
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying to remove nonexisting componentType from Scene.");
	}

	myComponentIDs[aUUID].RemoveCyclic(aID);
}

bool CScene::GrowingArrayContainsUUID(_UUID_T aUUID)
{
	if (myComponentIDs.find(aUUID) == myComponentIDs.end())
	{
		return false;
	}

	return true;
}

void CScene::Unload()
{
	IEngine::GetComponentSystem().ReleaseSceneObjectsAndComponentStorage(mySceneID);
}

void CScene::SetSkybox(const std::string & aFilepath)
{
	mySkybox = IEngine::GetTextureManager().CreateTextureFromFile(aFilepath);
}

void CScene::SetCubemap(const std::string & aFilepath)
{
	myCubemap = IEngine::GetTextureManager().CreateTextureFromFile(aFilepath);
}

void CScene::AddComponentToGrowingArray(_UUID_T aUUID, unsigned short aStartSize)
{
	myComponentIDs.emplace(std::make_pair(aUUID, CommonUtilities::GrowingArray<int>()));
	myComponentIDs[aUUID].Init(aStartSize);
}


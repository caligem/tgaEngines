#pragma once
#include "GrowingArray.h"
#include "..\Game\State.h"

#include "WorkerPool.h"
#include <functional>

class CState;

class CStateStack
{

public:
	CStateStack();
	~CStateStack();

	enum EFadeState
	{
		EFadeState_FadingIn,
		EFadeState_FadingOut,
		EFadeState_Done,
		EFadeState_Count
	};

	bool Init(CState* aLoadingScreenState);
	bool Update();

	void PushMainState(CState* aMainState);
	void PushSubState(CState* aSubState);
	void PushAndPopState(CState* aMainState);
	void PopMainState();
	void PopSubState();
	CState* GetCurrentState();
	const CState* GetCurrentState() const;

	bool LoadAsync(const std::function<void()>& aJob, bool aShowLoadingscreen = true);
	void StartFadeIn(std::function<void()> aCallback);

private:
	CommonUtilities::GrowingArray<CommonUtilities::GrowingArray<CState*>> myStates;
	std::function<void()> myFinishedFadingCallback;
	CState* myLoadingScreenState = nullptr;
	bool myIsLoading = false;
	bool myShowLoadingScreen = false;

	float myFadeTimer = 0.f;
	float myFadeDuration = 0.75f;
	CWorkerPool myWorkerPool;
	EFadeState myFadeState;

	bool myShouldQuit = false;
};




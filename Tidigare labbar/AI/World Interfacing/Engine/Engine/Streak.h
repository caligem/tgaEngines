#pragma once

class CParticleRenderer;

class CStreak
{
public:
	CStreak();
	~CStreak();

	bool Init();

	inline float GetMinVertexDistance() const { return myMinVertexDistance; }
	inline float GetTime() const { return myTime; }

private:
	friend CParticleRenderer;

	SRV myTexture;

	float myMinVertexDistance = 0.1f;
	float myTime = 1.0f;
};


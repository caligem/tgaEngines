#pragma once

class CStreak;
class CParticleManager;
class CEngine;
class CGraphicsPipeline;
class CComponentSystem;

#include "Component.h"

#include "GrowingArray.h"

class CStreakComponent : public CComponent
{
public:
	CStreakComponent();
	~CStreakComponent();

	struct SStreakBufferData
	{
		CommonUtilities::Vector4f myPosition;
		CommonUtilities::Vector4f myColor;
		CommonUtilities::Vector2f mySize;
		float myRotation;
	};


protected:
	virtual void Release() override;

private:
	friend CEngine;
	friend CGraphicsPipeline;
	friend CComponentSystem;

	struct SStreakPropertyData
	{
		float myLifetime;
	};

	void Init();
	void Update(CommonUtilities::Vector3f aPosition);
	void UpdatePointData(float aTime);
	void RemoveDeadPoints(float aTime);

	CStreakComponent::SStreakBufferData myLeadingPoint;
	CommonUtilities::GrowingArray<CStreakComponent::SStreakBufferData> myPoints;
	CommonUtilities::GrowingArray<SStreakPropertyData> myProperties;

	CommonUtilities::Vector4f myLastSolidifiedPosition;

	ID_T(CStreak) myStreakID;
	static CParticleManager* ourParticleManager;
};


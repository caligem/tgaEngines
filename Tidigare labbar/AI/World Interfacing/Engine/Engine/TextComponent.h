#pragma once
#include "Component.h"
#include "UIElement.h"

#include <string>
#include "Vector.h"

#include "GrowingArray.h"

#include "Font.h"

#include "RenderCommand.h"

class CEngine;
class CTextManager;
class CGraphicsPipeline;
class CComponentSystem;
class CDebugRenderer;

class CTextComponent : public CComponent, public CUIElement
{
public:
	CTextComponent();
	~CTextComponent();

	struct STextBufferData
	{
		CommonUtilities::Vector2f myPosition;
		CommonUtilities::Vector2f myScale;
		CommonUtilities::Vector2f myPivot;
		CommonUtilities::Vector4f myTint;
		CommonUtilities::Vector4f myOutline;
		float myRotation = 0.f;
	};
	
	struct SComponentData
	{
		const char* myFontPath;
	};

	void SetText(const std::string& aString);
	void SetPosition(const CommonUtilities::Vector2f& aPosition) { myData.myPosition = aPosition; }
	void SetScale(const CommonUtilities::Vector2f& aScale) { myData.myScale = aScale; myIsDirty = true; }
	void SetPivot(const CommonUtilities::Vector2f& aPivot) { myData.myPivot = aPivot; myIsDirty = true; }
	void SetTint(const CommonUtilities::Vector4f& aTint) { myData.myTint = aTint; myIsDirty = true; }
	void SetOutline(const CommonUtilities::Vector4f& aOutline) { myData.myOutline = aOutline; myIsDirty = true; }
	void SetRotation(float aRotation) { myData.myRotation = aRotation; myIsDirty = true; }

	const std::string& GetText() const { return myText; }
	const CommonUtilities::Vector2f& GetPosition() const { return myData.myPosition; }
	const CommonUtilities::Vector2f& GetScale() const { return myData.myScale; }
	const CommonUtilities::Vector2f& GetPivot() const { return myData.myPivot; }
	const CommonUtilities::Vector4f& GetTint() const { return myData.myTint; }
	const CommonUtilities::Vector4f& GetOutline() const { return myData.myOutline; }
	const float& GetRotation() const { return myData.myRotation; }

	const CommonUtilities::Vector2f& GetSize() const { return mySize; }
	const CommonUtilities::GrowingArray<STextRenderCommand>& GetRenderCommands() { if (myIsDirty) { Invalidate(); myIsDirty = false; } return myRenderCommands; }

	void FillRenderCommands(CDoubleBuffer<SSpriteRenderCommand>&, CDoubleBuffer<STextRenderCommand>& aTextRenderCommandsBuffer) override;

private:
	friend CEngine;
	friend CGraphicsPipeline;
	friend CDebugRenderer;
	friend CComponentSystem;

	static CTextManager* ourTextManager;

	void Init(const SComponentData& aComponentData);
	void Invalidate();
	void ResetRenderCommands();

	ID_T(CFont) myFontID;

	std::string myText;

	STextBufferData myData;
	CommonUtilities::Vector2f mySize;

	CommonUtilities::GrowingArray<STextRenderCommand> myRenderCommands;

	bool myIsDirty;

};


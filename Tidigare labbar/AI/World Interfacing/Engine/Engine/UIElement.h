#pragma once
#include "DoubleBuffer.h"

struct SSpriteRenderCommand;
struct STextRenderCommand;


class CUIElement
{
public:
	CUIElement();
	virtual ~CUIElement();

	virtual void FillRenderCommands(CDoubleBuffer<SSpriteRenderCommand>& aSpriteRenderCommandsBuffer, CDoubleBuffer<STextRenderCommand>& aTextRenderCommandsBuffer) = 0;
};


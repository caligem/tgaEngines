#pragma once

struct ID3D11Buffer;

struct SVertexDataWrapper
{
	unsigned int myNumberOfVertices;
	unsigned int myNumberOfIndices;
	unsigned int myStride;
	unsigned int myOffset;
	float myLODDistance;
	ID3D11Buffer* myVertexBuffer = NULL;
	ID3D11Buffer* myIndexBuffer = NULL;
};

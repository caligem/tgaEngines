#pragma once

#include <thread>
#include <atomic>
#include <functional>

class CWorkerPool;

class CWorker
{
public:
	CWorker();
	CWorker(const CWorker& aWorker);
	~CWorker();

	void Init(CWorkerPool* aWorkerPool);

	void Stop();

private:
	void RunThread();

	CWorkerPool* myWorkerPool;

	std::thread myThread;
	std::atomic_bool myIsRunning;

	std::function<void()> myFunctionPtr;

};


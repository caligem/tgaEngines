#pragma once

#include <functional>
#include <vector>

class CAIEventManager
{
public:
	CAIEventManager();
	~CAIEventManager();

	void RegisterCallback(std::function<void(const CommonUtilities::Vector3f&)> aFunction);
	void FireCallbacks(const CommonUtilities::Vector3f& aPosition);

private:
	std::vector<std::function<void(const CommonUtilities::Vector3f&)>> myCallback;

};


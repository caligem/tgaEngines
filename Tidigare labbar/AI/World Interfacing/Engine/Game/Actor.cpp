#include "stdafx.h"
#include "Actor.h"

#include "IWorld.h"

CActor::CActor()
{
	mySpeed = 1.f;
}

CActor::~CActor()
{
}

void CActor::Update(float dt)
{
	myPosition += myController->Update(myPosition).GetNormalized() * mySpeed * dt;
}

void CActor::Render()
{
	IWorld::SetDebugColor(mySize);
	IWorld::DrawDebugWireSphere(myPosition, { 1.f, 0.0f, 1.f });
}

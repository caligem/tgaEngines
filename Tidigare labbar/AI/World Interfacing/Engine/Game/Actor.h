#pragma once

#include "Controller.h"

class CActor 
{
public:
	CActor();
	~CActor();

	void Update(float dt);
	void Render();

	void SetController(CController* aController) { myController = aController; }

	void SetStartPosition(const CommonUtilities::Vector3f& aPosition) { myPosition = aPosition; myController->SetPosition(aPosition); }
	void SetStartSize(const CommonUtilities::Vector3f& aSize) { mySize = aSize; }
	void SetStartSpeed(float aSpeed) { mySpeed = aSpeed; }

	const CommonUtilities::Vector3f& GetPosition() const { return myPosition; }
	const CController* GetController() const { return myController; }

private:
	CController* myController;

	float mySpeed;
	CommonUtilities::Vector3f myPosition;

	CommonUtilities::Vector3f mySize; // sprite

};


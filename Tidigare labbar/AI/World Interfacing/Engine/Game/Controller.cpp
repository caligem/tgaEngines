#include "stdafx.h"
#include "Controller.h"

#include "IWorld.h"
#include "CameraDataWrapper.h"

#include "InputManager.h"

CommonUtilities::Vector3f GetWorldPoint()
{
	Input::CInputManager& input = IWorld::Input();

	CommonUtilities::Matrix44f viewProjInv = CommonUtilities::Matrix44f::Inverse(IWorld::GetSavedCameraBuffer().myViewProjection);

	CommonUtilities::Vector2f mousePos = {
		input.GetMousePosition().x / IWorld::GetWindowSize().x,
		input.GetMousePosition().y / IWorld::GetWindowSize().y
	};
	mousePos.x = mousePos.x * 2.f - 1.f;
	mousePos.y = (1.f - mousePos.y) * 2.f - 1.f;

	CommonUtilities::Vector4f rayOrigin = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 0.f, 1.f) * viewProjInv;
	rayOrigin /= rayOrigin.w;
	CommonUtilities::Vector4f rayEnd = CommonUtilities::Vector4f(mousePos.x, mousePos.y, 1.f, 1.f) * viewProjInv;
	rayEnd /= rayEnd.w;

	CommonUtilities::Vector3f rayDir = rayEnd - rayOrigin;
	rayDir.Normalize();

	CommonUtilities::Vector3f normal = { 0.f, 1.f, 0.f };

	float denom = normal.Dot(rayDir);

	CommonUtilities::Vector3f p0l0 = -CommonUtilities::Vector3f(rayOrigin);

	float t = p0l0.Dot(normal) / denom;

	CommonUtilities::Vector3f intersection = CommonUtilities::Vector3f(rayOrigin) + rayDir*t;

	return intersection;
}

CommonUtilities::Vector3f CInputController::Update(const CommonUtilities::Vector3f& aActorPos)
{
	Input::CInputManager& input = IWorld::Input();
	if (input.IsButtonPressed(Input::Button_Left))
	{
		myTargetPosition = GetWorldPoint();
	}

	CommonUtilities::Vector3f output;
	if ((myTargetPosition - aActorPos).Length() < 0.25f)
	{
		return output;
	}

	output = myTargetPosition - aActorPos;

	return output;
}

CAIPollController::CAIPollController(CPollingStation * aPollingStation)
	: myPollingStation(*aPollingStation)
{
}

CommonUtilities::Vector3f CAIPollController::Update(const CommonUtilities::Vector3f& aActorPos)
{
	if (myPollingStation.GetIsPlayerAtFlag())
	{
		myTargetPosition = myPollingStation.GetPressedPressurePlatePosition();
		myHasMoved = true;
	};

	CommonUtilities::Vector3f output;
	if ((myTargetPosition - aActorPos).Length() < 0.25f)
	{
		myHasReachedTarget = myHasMoved;
		return output;
	}
	else
	{
		myHasReachedTarget = false;
	}

	output = myTargetPosition - aActorPos;

	return output;
}

CAIEventController::CAIEventController(CAIEventManager * aEventManager)
{
	aEventManager->RegisterCallback(std::bind(&CAIEventController::PlayerWalkedOnButton, this, std::placeholders::_1));
}

CommonUtilities::Vector3f CAIEventController::Update(const CommonUtilities::Vector3f& aActorPos)
{
	CommonUtilities::Vector3f output;
	if ((myTargetPosition - aActorPos).Length() < 0.25f)
	{
		myHasReachedTarget = true;
		return output;
	}
	else
	{
		myHasReachedTarget = false;
	}

	output = myTargetPosition - aActorPos;

	return output;
}

void CAIEventController::PlayerWalkedOnButton(const CommonUtilities::Vector3f& aPosition)
{
	myTargetPosition = aPosition;
	myHasMoved = true;
}

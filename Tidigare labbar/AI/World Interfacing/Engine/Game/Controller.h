#pragma once

#include "Vector.h"

#include "PollingStation.h"
#include "AIEventManager.h"

class CController
{
public:
	virtual CommonUtilities::Vector3f Update(const CommonUtilities::Vector3f& aActorPos) = 0;

	void SetPosition(const CommonUtilities::Vector3f& aPosition) { myTargetPosition = aPosition; myHasMoved = false; }
	const bool HasReachedTarget() const { return myHasReachedTarget; }
protected:
	CController() = default;
	virtual ~CController() = default;
	bool myHasReachedTarget = false;
	bool myHasMoved = false;
	CommonUtilities::Vector3f myTargetPosition;
};


class CInputController : public CController
{
public:
	virtual CommonUtilities::Vector3f Update(const CommonUtilities::Vector3f& aActorPos) override;
};
class CAIPollController : public CController
{
public:
	CAIPollController(CPollingStation* aPollingStation);
	
	virtual CommonUtilities::Vector3f Update(const CommonUtilities::Vector3f& aActorPos) override;
private:
	CPollingStation& myPollingStation;
};
class CAIEventController : public CController
{
public:
	CAIEventController(CAIEventManager* aEventManager);

	virtual CommonUtilities::Vector3f Update(const CommonUtilities::Vector3f& aActorPos) override;

private:
	void PlayerWalkedOnButton(const CommonUtilities::Vector3f& aPosition);

};
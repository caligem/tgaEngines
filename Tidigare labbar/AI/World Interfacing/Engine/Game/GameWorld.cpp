#include "stdafx.h"
#include "GameWorld.h"

#include <IWorld.h>
#include <InputManager.h>
#include <AudioManager.h>

#include "ShowRoomState.h"
#include "TestRoomState.h"
#include "MemoryLeakTestState.h"
#include "SplashScreenState.h"
#include "LabbState.h"

#include <Windows.h>
#include <FileDialog.h>
#include <JsonDocument.h>

#include "CommandLineManager.h"

CGameWorld::CGameWorld()
{
}

CGameWorld::~CGameWorld()
{
}

void CGameWorld::Init()
{
	GAMEPLAY_LOG(CONCOL_VALID, "Gameworld init!");

	//Audio Load
	AM.LoadAudioBank("Audio/Master Bank");
	AM.LoadAudioEvent("Music/MenuMusic", false, nullptr, AudioChannel::Music);
	AM.LoadAudioEvent("Music/GameStateMusic", false, nullptr, AudioChannel::Music);
	AM.LoadAudioEvent("Music/GameStateMusic2", false, nullptr, AudioChannel::Music);
	AM.SetVolume(85.f, AudioChannel::SoundEffects);
	LoadAudioSettings();
	myStateStack.Init(nullptr);

	if (CommonUtilities::CCommandLineManager::HasArgument("-startScene", "showRoom"))
	{
		CShowRoomState* showroom = new CShowRoomState();
		showroom->Init();
		myStateStack.PushMainState(showroom);
	}
	else 
	{
		CTestRoomState* testRoom = new CTestRoomState();
		testRoom->Init();
		myStateStack.PushMainState(testRoom);
	}

	/*
	CSplashScreenState* splashScreenFFG = new CSplashScreenState();
	splashScreenFFG->Init("Assets/Sprites/SplashScreen/ffgLogo.dds");
	myStateStack.PushSubState(splashScreenFFG);

	CSplashScreenState* splashScreenTga = new CSplashScreenState();
	splashScreenTga->Init("Assets/Sprites/SplashScreen/tgaLogo.dds");
	myStateStack.PushSubState(splashScreenTga);
	*/

	CLabbState* labb = new CLabbState();
	labb->Init();
	myStateStack.PushMainState(labb);
}

void CGameWorld::Update()
{
	AM.Update();	

	//TestMemoryLeaks();

	if (!myStateStack.Update())
	{
		IWorld::GameQuit();
	}
}

void CGameWorld::LoadAudioSettings()
{
	JsonDocument jsonDoc;
	jsonDoc.LoadFile((CommonUtilities::GetMyGamePath() + "PlayerProgressStats.json").c_str(), true);

	if (jsonDoc.Find("player"))
	{
		if (jsonDoc["player"].Find("controls"))
		{
			if (jsonDoc["player"]["controls"].Find("mutedSound"))
			{
				AM.SetMute(jsonDoc["player"]["controls"]["mutedSound"].GetBool());
			}
			else
			{
				AM.SetMute(false);
			}
		}
	}
}

void CGameWorld::TestMemoryLeaks()
{
	CMemoryLeakTestState* memoryLeakState = new CMemoryLeakTestState();
	memoryLeakState->Init();
	myStateStack.PushMainState(memoryLeakState);
}

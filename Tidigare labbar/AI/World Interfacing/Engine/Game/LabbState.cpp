#include "stdafx.h"
#include "LabbState.h"

#include "IWorld.h"

CLabbState::CLabbState()
	: myPollingStation(*this)
{
}

CLabbState::~CLabbState()
{
}

bool CLabbState::Init()
{
	CState::Init();

	CGameObject dir1;
	dir1.Init(mySceneID);
	dir1.GetTransform().SetLookDirection({ 0.f, -1.f, 0.f });
	CLightComponent* light1 = dir1.AddComponent<CLightComponent>();
	light1->SetType(CLightComponent::ELightType_Directional);
	light1->SetColor({ 1.f, 0.8f, 0.5f });

	myPlayer.SetController(new CInputController);
	for (int i = 0; i < 2; ++i)myPollEnemies[i].SetController(new CAIPollController(&myPollingStation));
	for (int i = 0; i < 2; ++i)myEventEnemies[i].SetController(new CAIEventController(&myEventManager));

	for (int i = 0; i < 3; ++i)myButtons[i].Init(&myEventManager);

	ResetScene();

	myMainCamera.GetTransform().SetPosition({ 0.f, 15.f, 0.f });
	myMainCamera.GetTransform().LookAt({ 0.f, 0.f, 0.f });

	for (int i = 0; i < 3; ++i)
	{
		float myAngle = (CommonUtilities::Pif / 3.f) * ((i*2)+0.5f);
		myButtons[i].SetPosition({
			std::cosf(myAngle) * 3.5f,
			0.f,
			std::sinf(myAngle) * 3.5f,
		});
	}

	return true;
}

EStateUpdate CLabbState::Update()
{
	float dt = IWorld::Time().GetDeltaTime();

	myPollingStation.Update();

	myPlayer.Update(dt);
	for (int i = 0; i < 2; ++i)myPollEnemies[i].Update(dt);
	for (int i = 0; i < 2; ++i)myEventEnemies[i].Update(dt);
	for (int i = 0; i < 3; ++i)myButtons[i].Update(myPlayer.GetPosition());

	myPlayer.Render();
	for (int i = 0; i < 2; ++i)myPollEnemies[i].Render();
	for (int i = 0; i < 2; ++i)myEventEnemies[i].Render();
	for (int i = 0; i < 3; ++i)myButtons[i].Render();
	
	if (
		myPollEnemies[0].GetController()->HasReachedTarget() &&
		myPollEnemies[1].GetController()->HasReachedTarget() &&
		myEventEnemies[0].GetController()->HasReachedTarget() &&
		myEventEnemies[1].GetController()->HasReachedTarget()
		)
	{
		ResetScene();
	}

	return EDoNothing;
}

void CLabbState::OnEnter()
{
	SetActiveScene();
}

void CLabbState::ResetScene()
{
	myPlayer.SetStartPosition({ 0.f, 0.f, 0.f });
	for (int i = 0; i < 2; ++i)myPollEnemies[i].SetStartPosition({-5.f, 0.f, i*10.f-5.f});
	for (int i = 0; i < 2; ++i)myEventEnemies[i].SetStartPosition({5.f, 0.f, i*10.f-5.f});

	myPlayer.SetStartSize({ 1.f, 0.f, 0.f });
	for (int i = 0; i < 2; ++i)myPollEnemies[i].SetStartSize({0.f, 1.f, 0.f});
	for (int i = 0; i < 2; ++i)myEventEnemies[i].SetStartSize({0.f, 0.f, 1.f});

	myPlayer.SetStartSpeed(2.f);
	for (int i = 0; i < 2; ++i)myPollEnemies[i].SetStartSpeed(1.0f);
	for (int i = 0; i < 2; ++i)myEventEnemies[i].SetStartSpeed(1.5f);
}


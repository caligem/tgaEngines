#pragma once
#include "State.h"

#include "Actor.h"
#include "Controller.h"
#include "PressurePlate.h"
#include "PollingStation.h"
#include "AIEventManager.h"

class CLabbState : public CState
{
public:
	CLabbState();
	~CLabbState();

	bool Init() override;

	virtual EStateUpdate Update() override;

	virtual void OnEnter() override;
	void ResetScene();

	const CActor* GetPlayer() const { return &myPlayer; }
	const std::array<CPressurePlate, 3>& GetPressurePlates() { return myButtons; }

private:
	CActor myPlayer;
	std::array<CActor, 2> myPollEnemies;
	std::array<CActor, 2> myEventEnemies;

	std::array<CPressurePlate, 3> myButtons;
	
	CAIEventManager myEventManager;
	CPollingStation myPollingStation;
	
};


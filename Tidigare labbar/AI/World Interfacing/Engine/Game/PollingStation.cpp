#include "stdafx.h"
#include "PollingStation.h"

#include "LabbState.h"

CPollingStation::CPollingStation(CLabbState& aState)
	: myGameState(aState)
{
	myFrameCount = 0;
}

CPollingStation::~CPollingStation()
{
}

void CPollingStation::Update()
{
	myFrameCount++;
}

bool CPollingStation::GetIsPlayerAtFlag()
{
	if (myIsPlayerAtFlag.myFrameUpdated < myFrameCount)
	{
		UpadetPlayerAtFlagState();
	}

	return myIsPlayerAtFlag.myValue;
}

void CPollingStation::UpadetPlayerAtFlagState()
{
	myIsPlayerAtFlag.myFrameUpdated = myFrameCount;
	const CommonUtilities::Vector3f& playerPos = myGameState.GetPlayer()->GetPosition();
	const std::array<CPressurePlate, 3>& pressurePlates = myGameState.GetPressurePlates();

	bool flag = false;
	for (int i = 0; i < pressurePlates.size(); ++i)
	{
		if ((std::abs(playerPos.x - pressurePlates[i].GetPosition().x)) < pressurePlates[i].GetRadius()
			&& (std::abs(playerPos.z - pressurePlates[i].GetPosition().z) < pressurePlates[i].GetRadius()))
		{
			myIsPlayerAtFlag.myTargetPosition = pressurePlates[i].GetPosition();
			flag = true;
		}
	}
	myIsPlayerAtFlag.myValue = flag;
}

CommonUtilities::Vector3f CPollingStation::GetPressedPressurePlatePosition()
{
	return myIsPlayerAtFlag.myTargetPosition;
}

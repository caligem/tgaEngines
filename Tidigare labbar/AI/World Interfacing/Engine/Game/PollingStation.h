#pragma once

class CLabbState;

struct SPlayerAtFlag
{
	bool myValue = false;
	CommonUtilities::Vector3f myTargetPosition = { 0.f, 0.f, 0.f };
	int myFrameUpdated = 0;
};

class CPollingStation
{
public:
	CPollingStation(CLabbState& aState);
	~CPollingStation();

	void Update();

	bool GetIsPlayerAtFlag();
	void UpadetPlayerAtFlagState();
	CommonUtilities::Vector3f GetPressedPressurePlatePosition();

private:
	int myFrameCount;
	SPlayerAtFlag myIsPlayerAtFlag;
	CLabbState& myGameState;

};


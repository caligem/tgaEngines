#include "stdafx.h"
#include "PressurePlate.h"

#include "IWorld.h"

#include "AIEventManager.h"

CPressurePlate::CPressurePlate()
{
}

CPressurePlate::~CPressurePlate()
{
}

void CPressurePlate::Update(const CommonUtilities::Vector3f& aPlayerPosition)
{
	bool isPlayerTouching = ((myPosition - aPlayerPosition).Length() < myRadius);

	if (isPlayerTouching && !myWasPlayerTouchingLastFrame)
	{
		myEventManager->FireCallbacks(myPosition);
	}

	myWasPlayerTouchingLastFrame = isPlayerTouching;
}

void CPressurePlate::Render()
{
	IWorld::DrawDebugSphere(myPosition - CommonUtilities::Vector3f(0.f, 0.26f, 0.f), { myRadius*2.f, 0.25f, myRadius*2.f });
}

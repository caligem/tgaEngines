#pragma once

class CAIEventManager;

class CPressurePlate
{
public:
	CPressurePlate();
	~CPressurePlate();

	void Init(CAIEventManager* aEventManager) { myEventManager = aEventManager; }

	void Update(const CommonUtilities::Vector3f& aPlayerPosition);
	void Render();

	void SetPosition(const CommonUtilities::Vector3f& aPosition) { myPosition = aPosition; }

	float GetRadius() const { return myRadius; }

	const CommonUtilities::Vector3f& GetPosition() const { return myPosition; }

private:
	CAIEventManager* myEventManager;

	CommonUtilities::Vector3f myPosition;

	float myRadius = 0.5f;

	bool myWasPlayerTouchingLastFrame = false;

};


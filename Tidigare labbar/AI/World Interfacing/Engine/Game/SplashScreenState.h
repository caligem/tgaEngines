#pragma once
#include "State.h"

#include "GameObject.h"

class CSplashScreenState : public CState
{
public:
	CSplashScreenState();
	~CSplashScreenState();

	void Init(const std::string& aSpritePath);

	virtual EStateUpdate Update() override;

	virtual void OnEnter() override;

	virtual void OnLeave() override;

private:
	CGameObject myBlackBackground;
	CGameObject mySplash;

	float myDuration = 3.f;
	float myTimer = 0.f;

	CSpriteComponent* myLogo;

};


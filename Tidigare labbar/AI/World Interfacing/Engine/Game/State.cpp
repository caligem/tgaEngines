#include "stdafx.h"
#include "State.h"

#include "StateStack.h"
#include "IWorld.h"

#include "JsonDocument.h"
#include "JsonUtility.h"

CStateStack* CState::ourStateStack = nullptr;

bool CState::Init()
{
	mySceneID = IWorld::GetSceneManager().CreateScene();

	myMainCamera.Init(mySceneID);
	myMainCamera.AddComponent<CCameraComponent>({
		60.f,
		static_cast<float>(IWorld::GetWindowSize().x / IWorld::GetWindowSize().y),
		0.1f, 1000.f
	})->SetAsActiveCamera();

	return true;
}

void CState::SetActiveScene()
{
	IWorld::GetSceneManager().SetActiveScene(mySceneID);
}

void CState::DestoryScene()
{
	IWorld::GetSceneManager().DestroyScene(mySceneID);
}

void CState::LoadFromFile(const std::string & aLevelpath)
{
	RESOURCE_LOG(CONCOL_VALID, "Reading file: %s", aLevelpath.c_str());
	JsonDocument doc(aLevelpath.c_str());

	std::map<int, CGameObject> gameObjects;

	for (int i = 0; i < doc["myGameObjects"].GetSize(); ++i)
	{
		auto obj = doc["myGameObjects"][i];

		CGameObject gameObject;
		gameObject.Init(mySceneID);

		gameObjects[obj["myID"].GetInt()] = gameObject;

		gameObject.GetTransform().SetPosition(JsonToVector3f(obj["myPosition"]));
		gameObject.GetTransform().SetRotation(JsonToQuatf(obj["myRotation"]).GetEulerAngles());
		gameObject.GetTransform().SetScale(JsonToVector3f(obj["myScale"]));

		int parentID = obj["myParent"].GetInt();
		if (parentID != -1)
		{
			if (gameObjects.find(parentID) != gameObjects.end())
			{
				gameObject.GetTransform().SetParent(&gameObjects[parentID].GetTransform());
			}
		}
	}
	for (int i = 0; i < doc["myMeshFilters"].GetSize(); ++i)
	{
		auto obj = doc["myMeshFilters"][i];
		int parentID = obj["myParent"].GetInt();
		if (gameObjects.find(parentID) != gameObjects.end())
		{
			gameObjects[parentID].AddComponent<CModelComponent>({ obj["myPath"].GetString() });
		}
	}
	for (int i = 0; i < doc["myDirectionalLights"].GetSize(); ++i)
	{
		auto obj = doc["myDirectionalLights"][i];
		int parentID = obj["myParent"].GetInt();

		if (gameObjects.find(parentID) != gameObjects.end())
		{
			CLightComponent* light = gameObjects[parentID].AddComponent<CLightComponent>();
			light->SetType(CLightComponent::ELightType_Directional);
			light->SetIntensity(obj["myIntensity"].GetFloat());
			light->SetColor(JsonToVector3f(obj["myColor"]));
		}
	}
	for (int i = 0; i < doc["myPointLights"].GetSize(); ++i)
	{
		auto obj = doc["myPointLights"][i];
		int parentID = obj["myParent"].GetInt();

		if (gameObjects.find(parentID) != gameObjects.end())
		{
			CLightComponent* light = gameObjects[parentID].AddComponent<CLightComponent>();
			light->SetType(CLightComponent::ELightType_Point);
			light->SetIntensity(obj["myIntensity"].GetFloat());
			light->SetRange(obj["myRange"].GetFloat());
			light->SetColor(JsonToVector3f(obj["myColor"]));
		}
	}
	for (int i = 0; i < doc["mySpotLights"].GetSize(); ++i)
	{
		auto obj = doc["mySpotLights"][i];
		int parentID = obj["myParent"].GetInt();

		if (gameObjects.find(parentID) != gameObjects.end())
		{
			CLightComponent* light = gameObjects[parentID].AddComponent<CLightComponent>();
			light->SetType(CLightComponent::ELightType_Spot);
			light->SetIntensity(obj["myIntensity"].GetFloat());
			light->SetSpotAngle(obj["myAngle"].GetFloat());
			light->SetRange(obj["myRange"].GetFloat());
			light->SetColor(JsonToVector3f(obj["myColor"]));
		}
	}

	if (doc.Find("myCanvases"))
	{
		for (int i = 0; i < doc["myCanvases"].GetSize(); ++i)
		{
			auto obj = doc["myCanvases"][i];
			int parentID = obj["myParent"].GetInt();

			if (gameObjects.find(parentID) != gameObjects.end())
			{
				CommonUtilities::Vector2f canvasSize = { obj["myWidth"].GetFloat(), obj["myHeight"].GetFloat() };
				CCanvasComponent::ERenderMode renderMode = static_cast<CCanvasComponent::ERenderMode>(obj["myRenderMode"].GetInt());
				CCanvasComponent* canvas = gameObjects[parentID].AddComponent<CCanvasComponent>({ renderMode, canvasSize });
				
				if (obj.Find("myTexts"))
				{
					for (int textIndex = 0; textIndex < obj["myTexts"].GetSize(); ++textIndex)
					{
						auto textObj = obj["myTexts"][textIndex];
						//std::string fontPath = textObj["myFont"].GetString();
						std::string fontPath = "Assets/Fonts/Mono/Mono";
						CommonUtilities::Vector2f textPosition = { textObj["myPosition"]["myX"].GetFloat(), textObj["myPosition"]["myY"].GetFloat() };
				
						CTextComponent* textComponent = canvas->AddUIElement<CTextComponent>({ fontPath.c_str() });
						textComponent->SetPivot({ 0.5f, 0.5f });
						textComponent->SetText(textObj["myText"].GetString());
						textComponent->SetPosition(textPosition);
						//TODO: FontSize
					}
				}

				if (obj.Find("myRawImages"))
				{
					for (int spriteIndex = 0; spriteIndex < obj["myRawImages"].GetSize(); ++spriteIndex)
					{
						auto spriteObj = obj["myRawImages"][spriteIndex];
						std::string spritePath = "Assets/Sprites/Menus/";
						spritePath += spriteObj["myFileName"].GetString();
						spritePath += ".dds";
						CommonUtilities::Vector2f spritePosition = { spriteObj["myPosition"]["myX"].GetFloat(), spriteObj["myPosition"]["myY"].GetFloat() };
						CommonUtilities::Vector2f spriteSizeRelativeToScreen = { spriteObj["myWidth"].GetFloat(), spriteObj["myHeight"].GetFloat() };

						CSpriteComponent* spriteComponent = canvas->AddUIElement<CSpriteComponent>({ spritePath.c_str() });
						spriteComponent->SetPivot({ 0.5f, 0.5f });
						spriteComponent->SetPosition(spritePosition);
						spriteComponent->SetScaleRelativeToScreen(spriteSizeRelativeToScreen);
					}
				}

				if (obj.Find("myButtons"))
				{
					for (int buttonIndex = 0; buttonIndex < obj["myButtons"].GetSize(); ++buttonIndex)
					{
						auto buttonObj = obj["myButtons"][buttonIndex];
						CommonUtilities::Vector2f buttonPosition = { buttonObj["myPosition"]["myX"].GetFloat(), buttonObj["myPosition"]["myY"].GetFloat() };
						CommonUtilities::Vector2f buttonScaleRelativeToScreen = { buttonObj["myWidth"].GetFloat(), buttonObj["myHeight"].GetFloat() };
						std::string spritePath = "Assets/Sprites/Menus/";
						spritePath += buttonObj["mySpriteFileName"].GetString();
						spritePath += ".dds";

						auto textObj = buttonObj["myText"];
						CommonUtilities::Vector2f textPosition = { textObj["myPosition"]["myX"].GetFloat(), textObj["myPosition"]["myY"].GetFloat() };
						std::string buttonText = textObj["myText"].GetString();
						//std::string fontPath = textObj["myFont"].GetString();
						std::string fontPath = "Assets/Fonts/Mono/Mono";
						//TODO: fontsize

						CButtonComponent* buttonComponent = canvas->AddUIElement<CButtonComponent>({ fontPath.c_str(), spritePath.c_str() });
						buttonComponent->SetPosition(buttonPosition);
						buttonComponent->SetScaleRelativeToScreen(buttonScaleRelativeToScreen);
						buttonComponent->SetTextPosition(textPosition);
						buttonComponent->SetText(buttonText.c_str());
					}
				}
			}
		}
	}


	OnLoadFinished(doc);
}

#pragma once
#include "State.h"

class CTestRoomState : public CState
{
public:
	CTestRoomState();
	~CTestRoomState();

	bool Init();
	EStateUpdate Update() override;
	void OnEnter();
	void OnLeave();

private:
	void HandleControls();
	void UpdatePlayerControls();
	void UpdateCamera();
	CommonUtilities::Vector3f GetWorldPoint();

	CommonUtilities::Vector3f myPivot;
	CommonUtilities::Vector2f myRotation;
	float myZoom;
	bool myShouldPop;

	CGameObject myPlayer;
	CommonUtilities::Vector3f myPlayerTargetPosition;
	CGameObject myCompanion;
};


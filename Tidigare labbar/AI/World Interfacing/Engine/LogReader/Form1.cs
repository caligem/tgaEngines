﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace LogReader
{
    public partial class Form1 : Form
    {
        List<List<string>> fileContents;

        public Form1()
        {
            InitializeComponent();

            fileContents = new List<List<string>>();

            listBox1.DrawMode = DrawMode.OwnerDrawFixed;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            /*
            using (FolderBrowserDialog fbd = new FolderBrowserDialog())
            {
                DialogResult result = fbd.ShowDialog();

                if (result == DialogResult.OK && !string.IsNullOrWhiteSpace(fbd.SelectedPath))
                {
                    string[] files = Directory.GetFiles(fbd.SelectedPath);

                    SetupTabs(files);
                }
            }
            */

            string[] file = Directory.GetFiles("Logs");
            SetupTabs(file);
            tabControl1.SelectTab(0);
            ShowTab(0, "");
        }

        private void ShowTab(int aIndex, string aFilter)
        {
            if(aIndex == -1)
            {
                return;
            }

            listBox1.Parent = tabControl1.SelectedTab;
            listBox1.Items.Clear();

            foreach(string item in fileContents[aIndex])
            {
                if(item.ToLower().IndexOf(aFilter.ToLower()) == -1)
                {
                    continue;
                }
                listBox1.Items.Add(item);
            }
        }

        private void SetupTabs(string[] aFiles)
        {
            tabControl1.TabPages.Clear();
            
            foreach(string tab in aFiles)
            {
                int start = tab.LastIndexOf("log_");
                int end = tab.LastIndexOf(".");
                tabControl1.TabPages.Add(tab.Substring(start+4, end-start-4));

                fileContents.Add(new List<string>(File.ReadAllLines(tab)));
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            ShowTab(tabControl1.SelectedIndex, textBox1.Text);
        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            ShowTab(tabControl1.SelectedIndex, textBox1.Text);
        }

        private void listBox1_DrawItem(object sender, DrawItemEventArgs e)
        {
            e.DrawBackground();
            Graphics g = e.Graphics;

            g.FillRectangle(new SolidBrush(Color.Black), e.Bounds);

            string str = listBox1.Items[e.Index].ToString();
            if(str.IndexOf("warning") == 0)
            {
                e.Graphics.DrawString(
                    str.Substring("warning".Length),
                    listBox1.Font,
                    Brushes.Yellow,
                    e.Bounds
                );
            }
            else if(str.IndexOf("error") == 0)
            {
                e.Graphics.DrawString(
                    str.Substring("error".Length),
                    listBox1.Font,
                    Brushes.Red,
                    e.Bounds
                );
            }
            else if(str.IndexOf("valid") == 0)
            {
                e.Graphics.DrawString(
                    str.Substring("valid".Length),
                    listBox1.Font,
                    Brushes.Green,
                    e.Bounds
                );
            }
            else
            {
                e.Graphics.DrawString(
                    str,
                    listBox1.Font,
                    Brushes.White,
                    e.Bounds
                );
            }

            e.DrawFocusRectangle();
        }
    }
}

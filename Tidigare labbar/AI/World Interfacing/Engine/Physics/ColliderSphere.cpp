#include "stdafx.h"
#include "ColliderSphere.h"

CColliderSphere::CColliderSphere(const CommonUtilities::Vector3f & aPosition, float aRadius)
{
	myPosition = aPosition;
	myRadius = aRadius;
}

CColliderSphere::~CColliderSphere()
{
}

#pragma once

#include "Matrix.h"
#include "Vector.h"

namespace CommonUtilities
{
	class Camera
	{
	public:
		Camera();
		~Camera();

		void Init(const Matrix44f& aPerspectiveFOV);
		void Move(const Vector3f& aMovement);
		void SetPosition(const Vector3f& aPosition);
		void Rotate(const Vector3f& aRotation);

		Vector3f PostProjectionSpace(const Vector3f& aPoint) const;

		const Vector3f& GetPosition() const;
		const Vector3f GetForward() const;
		const Vector3f GetRight() const;
		const Vector3f GetUp() const;

	private:
		void CalculateTransform();

		Matrix44f myTransform;
		Matrix44f myTransformInverse;
		Matrix44f myPerspectiveFOV;

		Vector3f myPosition;
		Vector3f myRotation;
	};
}

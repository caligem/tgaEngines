#include "DL_Debug.h"

#include <iostream>
#include <string>
#include <cstdio>
#include <cstdarg>
#include <chrono>
#include <ctime>
#include "Macros.h"
#include <Windows.h>

void CreateFileAndPath(FILE** aFilePtr, const std::string& strPathAndFile)
{
	std::string strPath(strPathAndFile);
	std::string strCurrent;
	int nStart, nStart1, nStart2;

	// Create the path //
	while (strPath.length())
	{
		nStart1 = (int)strPath.find('/'); if (nStart1 == std::string::npos) nStart1 = INT_MAX;
		nStart2 = (int)strPath.find('\\'); if (nStart2 == std::string::npos) nStart2 = INT_MAX;
		nStart = min(nStart1, nStart2);
		if (nStart == INT_MAX) break;
		strCurrent += strPath.substr(0, nStart) + "\\";
		strPath.erase(strPath.begin(), strPath.begin() + nStart + 1);
		CreateDirectoryA(strCurrent.c_str(), NULL);
	}

	fopen_s(aFilePtr, strPathAndFile.c_str(), "w");
}

namespace DL_Debug
{
	Debug* Debug::ourInstance = nullptr;

	bool Debug::Destroy()
	{
		if (ourInstance != nullptr)
		{
			for (auto logFile : ourInstance->myFilterLogFiles)
			{
				fclose(logFile.second.myFilePtr);
			}
			SAFE_DELETE(ourInstance);
			return true;
		}
		return false;
	}
	Debug* Debug::GetInstance()
	{
		if (ourInstance == nullptr)
		{
			ourInstance = new Debug();
		}
		return ourInstance;
	}
	void SetConsoleColor(int aColor)
	{
		HANDLE  hConsole;
		hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
		if (!hConsole)
		{
			return;
		}
		SetConsoleTextAttribute(hConsole, static_cast<WORD>(aColor));
	}
	void Debug::WriteLog(const char * aLogName, int aColor, const char * aFormattedString, ...)
	{
		if (!IsActive(aLogName))
		{
			return;
		}

		time_t tt = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		tm local_tm;
		localtime_s(&local_tm, &tt);

		FILE* filePtr = myFilterLogFiles[aLogName].myFilePtr;

		va_list args;
		va_start(args, aFormattedString);
		switch (aColor)
		{
		case CONCOL_VALID:
			fprintf_s(filePtr, "valid");
			break;
		case CONCOL_WARNING:
			fprintf_s(filePtr, "warning");
			break;
		case CONCOL_ERROR:
			fprintf_s(filePtr, "error");
			break;
		}
		fprintf_s(filePtr, "[%d:%d:%d]", local_tm.tm_hour, local_tm.tm_min, local_tm.tm_sec);
		vfprintf_s(filePtr, aFormattedString, args);
		vfprintf_s(filePtr, "\n", nullptr);

		SetConsoleColor(aColor);
		printf_s("[%s]", aLogName);
		vprintf_s(aFormattedString, args);
		vprintf_s("\n", nullptr);
		va_end(args);

		fflush(filePtr);
	}
	void Debug::ActivateFilterLog(const char * aLogName)
	{
		if (myFilterLogFiles.find(aLogName) != myFilterLogFiles.end())
		{
			myFilterLogFiles[aLogName].myIsActive = true;
		}
		else
		{
			FILE* filePtr;

			std::string fileName = "Logs\\log_";
			fileName += aLogName;
			fileName += ".log";

			CreateFileAndPath(&filePtr, fileName);

			LogFile logFile;
			logFile.myFilePtr = filePtr;
			logFile.myIsActive = true;

			myFilterLogFiles[aLogName] = logFile;
		}
	}
	void Debug::DeactivateFilterLog(const char * aLogName)
	{
		if (myFilterLogFiles.find(aLogName) != myFilterLogFiles.end())
		{
			myFilterLogFiles[aLogName].myIsActive = false;
		}
	}
	bool Debug::IsActive(const char * aLogName)
	{
		if (myFilterLogFiles.find(aLogName) != myFilterLogFiles.end())
		{
			return myFilterLogFiles[aLogName].myIsActive;
		}

		return false;
	}
	std::vector<std::string> Debug::GetLogFilesAndDestroy()
	{
		std::vector<std::string> myLogsToReturn;
		for (auto it : myFilterLogFiles)
		{
			myLogsToReturn.push_back(it.first);
		}
		Destroy();
		return myLogsToReturn;
	}
}

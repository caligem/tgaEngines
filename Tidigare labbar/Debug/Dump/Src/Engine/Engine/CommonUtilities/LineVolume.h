#pragma once

#include <vector>
#include "Vector.h"
#include "Line.h"

namespace CommonUtilities
{
	template <typename T>
	class LineVolume {
	public:
		LineVolume(const std::vector<Line<T>>& aLineList);
		void AddLine(const Line<T>& aLine);

		bool Inside(const Vector2<T>& aPosition);
		bool Outside(const Vector2<T>& aPosition);

	private:
		std::vector<Line<T>> myLineList;
	};

	template<typename T>
	inline LineVolume<T>::LineVolume(const std::vector<Line<T>>& aLineList)
	{
		myLineList = aLineList;
	}

	template<typename T>
	inline void LineVolume<T>::AddLine(const Line<T>& aLine)
	{
		myLineList.push_back(aLine);
	}

	template<typename T>
	inline bool LineVolume<T>::Inside(const Vector2<T>& aPosition)
	{
		for (const Line<T>& line : myLineList) {
			if (!line.Inside(aPosition))
			{
				return false;
			}
		}
		return true;
	}

	template<typename T>
	inline bool LineVolume<T>::Outside(const Vector2<T>& aPosition)
	{
		for (const Line<T>& line : myLineList) {
			if (!line.Inside(aPosition))
			{
				return true;
			}
		}
		return false;
	}

}

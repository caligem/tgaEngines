#pragma once

#define SAFE_DELETE(P) {delete P; P = nullptr;}
#define CYCLIC_ERASE(VEC, I) {VEC[(I)] = VEC[VEC.size()-1]; VEC.pop_back();}
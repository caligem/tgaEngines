#include "Mathf.h"
#include <math.h>

namespace CommonUtilities
{
	int Clamp(int aNumber, int aMin, int aMax)
	{
		if( aNumber < aMin )
			return aMin;
		if( aNumber > aMax )
			return aMax;
		return aNumber;
	}
	float Clamp(float aNumber, float aMin, float aMax)
	{
		if( aNumber < aMin )
			return aMin;
		if( aNumber > aMax )
			return aMax;
		return aNumber;
	}
	float Lerp(float aStart, float aEnd, float aTime)
	{
		return aStart + (aEnd-aStart)*aTime;
	}
	CommonUtilities::Vector2f Lerp(CommonUtilities::Vector2f aStart, CommonUtilities::Vector2f aEnd, float aT)
	{
		return aStart*(1.0f - aT) + aEnd*aT;
	}
	CommonUtilities::Vector3f Lerp(CommonUtilities::Vector3f aStart, CommonUtilities::Vector3f aEnd, float aT)
	{
		return aStart*(1.0f - aT) + aEnd*aT;
	}
	CommonUtilities::Vector4f Lerp(CommonUtilities::Vector4f aStart, CommonUtilities::Vector4f aEnd, float aT)
	{
		return aStart*(1.0f - aT) + aEnd*aT;
	}
	CommonUtilities::Vector3f Slerp(CommonUtilities::Vector3f start, CommonUtilities::Vector3f end, float percent)
	{
		// Dot product - the cosine of the angle between 2 vectors.
		float dot = start.Dot(end);
		// Clamp it to be in the range of Acos()
		// This may be unnecessary, but floating point
		// precision can be a fickle mistress.
		Clamp(dot, -1.0f, 1.0f);
		// Acos(dot) returns the angle between start and end,
		// And multiplying that by percent returns the angle between
		// start and the final result.
		float theta = acos(dot)*percent;
		CommonUtilities::Vector3f RelativeVec = end - start*dot;
		RelativeVec.Normalize();     // Orthonormal basis
		return ((start*cos(theta)) + (RelativeVec*sin(theta)));
	}
	const CommonUtilities::Matrix44f LookAt(const CommonUtilities::Vector3f & aPositionToLookAt, const CommonUtilities::Vector3f aPositionToLookFrom, const CommonUtilities::Vector3f & aUpVector)
	{
		CommonUtilities::Vector3f  forward = (aPositionToLookAt - aPositionToLookFrom).GetNormalized();
		CommonUtilities::Vector3f  up = aUpVector.GetNormalized();
		CommonUtilities::Vector3f  right = up.Cross(forward).GetNormalized();
		up = forward.Cross(right);

		CommonUtilities::Matrix44f Result;
		Result[0] = right.x;
		Result[1] = right.y;
		Result[2] = right.z;
		Result[4] = up.x;
		Result[5] = up.y;
		Result[6] = up.z;
		Result[8] = forward.x;
		Result[9] = forward.y;
		Result[10] = forward.z;
		Result[12] = aPositionToLookFrom.x;
		Result[13] = aPositionToLookFrom.y;
		Result[14] = aPositionToLookFrom.z;
		return Result;
	}
}
#pragma once

#include "Vector.h"
#include "Matrix.h"

namespace CommonUtilities
{
	int Clamp(int aNumber, int aMin, int aMax);
	float Clamp(float aNumber, float aMin, float aMax);

	float Lerp(float aStart, float aEnd, float aTime);
	CommonUtilities::Vector2f Lerp(CommonUtilities::Vector2f aStart, CommonUtilities::Vector2f aEnd, float aT);
	CommonUtilities::Vector3f Lerp(CommonUtilities::Vector3f aStart, CommonUtilities::Vector3f aEnd, float aT);
	CommonUtilities::Vector4f Lerp(CommonUtilities::Vector4f aStart, CommonUtilities::Vector4f aEnd, float aT);
	CommonUtilities::Vector3f Slerp(CommonUtilities::Vector3f start, CommonUtilities::Vector3f end, float percent);
	const CommonUtilities::Matrix44f LookAt(const CommonUtilities::Vector3f & aPositionToLookAt, const CommonUtilities::Vector3f aPositionToLookFrom, const CommonUtilities::Vector3f & aUpVector = CommonUtilities::Vector3f(0.0f, 1.0f, 0.0f));

	const float Pif = 3.14159265358979323846f;
	const float DegToRad = Pif / 180.f;
	const float RadToDeg = 180.f / Pif;
}

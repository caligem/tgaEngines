#pragma once

#include "Matrix33.h"
#include "Matrix44.h"

namespace CommonUtilities
{
	typedef Matrix33<int> Matrix33i;
	typedef Matrix33<float> Matrix33f;
	typedef Matrix33<double> Matrix33d;

	typedef Matrix44<int> Matrix44i;
	typedef Matrix44<float> Matrix44f;
	typedef Matrix44<double> Matrix44d;
}

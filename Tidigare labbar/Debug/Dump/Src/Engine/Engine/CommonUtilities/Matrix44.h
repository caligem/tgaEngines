#pragma once

/*
*	Index list 3x3
*	0	1	2
*	3	4	5
*	6	7	8
*
*	Index list 4x4
*	0	1	2	3
*	4	5	6	7
*	8	9	10	11
*	12	13	14	15
*/

#include <assert.h>
#include <math.h>

#include "Vector4.h"

namespace CommonUtilities
{
	template <typename T>
	class Matrix44
	{
	public:
		Matrix44<T>(
			T m11 = 1, T m12 = 0, T m13 = 0, T m14 = 0,
			T m21 = 0, T m22 = 1, T m23 = 0, T m24 = 0,
			T m31 = 0, T m32 = 0, T m33 = 1, T m34 = 0,
			T m41 = 0, T m42 = 0, T m43 = 0, T m44 = 1);
		Matrix44<T>(const Matrix44<T>& aMatrix);

		inline Matrix44<T>& operator=(const Matrix44<T>& aMatrix);

		inline T& operator[](const short& aIndex);
		inline const T& operator[](const short& aIndex) const;

		inline Matrix44<T>& operator*=(const T& aScalar);
		inline Matrix44<T> operator*(const T& aScalar) const;
		inline Matrix44<T>& operator/=(const T& aScalar);
		inline Matrix44<T> operator/(const T& aScalar) const;

		inline Matrix44<T>& operator+=(const Matrix44<T>& aMatrix);
		inline Matrix44<T>& operator-=(const Matrix44<T>& aMatrix);
		inline Matrix44<T> operator+(const Matrix44<T>& aMatrix);
		inline Matrix44<T> operator-(const Matrix44<T>& aMatrix);

		inline Matrix44<T> operator*(const Matrix44<T>& aMatrix);
		inline Matrix44<T>& operator*=(const Matrix44<T>& aMatrix);

		inline bool operator==(const Matrix44<T>& aMatrix);

		static Matrix44<T> CreateRotateAroundX(T aAngleInRadians);
		static Matrix44<T> CreateRotateAroundY(T aAngleInRadians);
		static Matrix44<T> CreateRotateAroundZ(T aAngleInRadians);
		static Matrix44<T> CreateYawPitchRoll(const Vector3<T>& aRotation);

		static Matrix44<T> Transpose(const Matrix44<T>& aMatrixToTranspose);

		Matrix44<T> GetFastInverse();

	private:
		T myMatrix[16];
	};

	template<typename T>
	inline Matrix44<T>::Matrix44(T m11, T m12, T m13, T m14, T m21, T m22, T m23, T m24, T m31, T m32, T m33, T m34, T m41, T m42, T m43, T m44)
	{
		myMatrix[0] = m11;	myMatrix[1] = m12;	myMatrix[2] = m13;	myMatrix[3] = m14;
		myMatrix[4] = m21;	myMatrix[5] = m22;	myMatrix[6] = m23;	myMatrix[7] = m24;
		myMatrix[8] = m31;	myMatrix[9] = m32;	myMatrix[10] = m33;myMatrix[11] = m34;
		myMatrix[12] = m41;myMatrix[13] = m42;myMatrix[14] = m43;myMatrix[15] = m44;
	}
	template<typename T>
	inline Matrix44<T>::Matrix44(const Matrix44<T>& aMatrix)
	{
		myMatrix[0] = aMatrix[0];	myMatrix[1] = aMatrix[1];	myMatrix[2] = aMatrix[2];	myMatrix[3] = aMatrix[3];
		myMatrix[4] = aMatrix[4];	myMatrix[5] = aMatrix[5];	myMatrix[6] = aMatrix[6];	myMatrix[7] = aMatrix[7];
		myMatrix[8] = aMatrix[8];	myMatrix[9] = aMatrix[9];	myMatrix[10] = aMatrix[10];myMatrix[11] = aMatrix[11];
		myMatrix[12] = aMatrix[12];myMatrix[13] = aMatrix[13];myMatrix[14] = aMatrix[14];myMatrix[15] = aMatrix[15];
	}

	template<typename T>
	inline Matrix44<T>& Matrix44<T>::operator=(const Matrix44<T>& aMatrix)
	{
		myMatrix[0] = aMatrix[0];	myMatrix[1] = aMatrix[1];	myMatrix[2] = aMatrix[2];	myMatrix[3] = aMatrix[3];
		myMatrix[4] = aMatrix[4];	myMatrix[5] = aMatrix[5];	myMatrix[6] = aMatrix[6];	myMatrix[7] = aMatrix[7];
		myMatrix[8] = aMatrix[8];	myMatrix[9] = aMatrix[9];	myMatrix[10] = aMatrix[10];myMatrix[11] = aMatrix[11];
		myMatrix[12] = aMatrix[12];myMatrix[13] = aMatrix[13];myMatrix[14] = aMatrix[14];myMatrix[15] = aMatrix[15];
		return *this;
	}

	template<typename T>
	inline T & Matrix44<T>::operator[](const short & aIndex)
	{
		assert(aIndex >= 0 && aIndex < 16 && "Index out of bounds");
		return myMatrix[aIndex];
	}
	template<typename T>
	inline const T & Matrix44<T>::operator[](const short & aIndex) const
	{
		assert(aIndex >= 0 && aIndex < 16 && "Index out of bounds");
		return myMatrix[aIndex];
	}

	template<typename T>
	inline Matrix44<T>& Matrix44<T>::operator*=(const T & aScalar)
	{
		myMatrix[0] *= aScalar;	myMatrix[1] *= aScalar;	myMatrix[2] *= aScalar;	myMatrix[3] *= aScalar;
		myMatrix[4] *= aScalar;	myMatrix[5] *= aScalar;	myMatrix[6] *= aScalar;	myMatrix[7] *= aScalar;
		myMatrix[8] *= aScalar;	myMatrix[9] *= aScalar;	myMatrix[10] *= aScalar;	myMatrix[11] *= aScalar;
		myMatrix[12] *= aScalar;	myMatrix[13] *= aScalar;	myMatrix[14] *= aScalar;	myMatrix[15] *= aScalar;
		return *this;
	}
	template<typename T>
	inline Matrix44<T> Matrix44<T>::operator*(const T & aScalar) const
	{
		return Matrix44<T>(*this).operator*(aScalar);
	}
	template<typename T>
	inline Matrix44<T>& Matrix44<T>::operator/=(const T & aScalar)
	{
		myMatrix[0] /= aScalar;	myMatrix[1] /= aScalar;	myMatrix[2] /= aScalar;	myMatrix[3] /= aScalar;
		myMatrix[4] /= aScalar;	myMatrix[5] /= aScalar;	myMatrix[6] /= aScalar;	myMatrix[7] /= aScalar;
		myMatrix[8] /= aScalar;	myMatrix[9] /= aScalar;	myMatrix[10] /= aScalar;	myMatrix[11] /= aScalar;
		myMatrix[12] /= aScalar;	myMatrix[13] /= aScalar;	myMatrix[14] /= aScalar;	myMatrix[15] /= aScalar;
		return *this;
	}
	template<typename T>
	inline Matrix44<T> Matrix44<T>::operator/(const T & aScalar) const
	{
		return Matrix44<T>(*this).operator/=(aScalar);
	}

	template<typename T>
	inline Matrix44<T>& Matrix44<T>::operator+=(const Matrix44<T>& aMatrix)
	{
		myMatrix[0] += aMatrix[0]; myMatrix[1] += aMatrix[1]; myMatrix[2] += aMatrix[2]; myMatrix[3] += aMatrix[3];
		myMatrix[4] += aMatrix[4]; myMatrix[5] += aMatrix[5]; myMatrix[6] += aMatrix[6]; myMatrix[7] += aMatrix[7];
		myMatrix[8] += aMatrix[8]; myMatrix[9] += aMatrix[9]; myMatrix[10] += aMatrix[10]; myMatrix[11] += aMatrix[11];
		myMatrix[12] += aMatrix[12]; myMatrix[13] += aMatrix[13]; myMatrix[14] += aMatrix[14]; myMatrix[15] += aMatrix[15];
		return *this;
	}
	template<typename T>
	inline Matrix44<T>& Matrix44<T>::operator-=(const Matrix44<T>& aMatrix)
	{
		myMatrix[0] -= aMatrix[0]; myMatrix[1] -= aMatrix[1]; myMatrix[2] -= aMatrix[2]; myMatrix[3] -= aMatrix[3];
		myMatrix[4] -= aMatrix[4]; myMatrix[5] -= aMatrix[5]; myMatrix[6] -= aMatrix[6]; myMatrix[7] -= aMatrix[7];
		myMatrix[8] -= aMatrix[8]; myMatrix[9] -= aMatrix[9]; myMatrix[10] -= aMatrix[10]; myMatrix[11] -= aMatrix[11];
		myMatrix[12] -= aMatrix[12]; myMatrix[13] -= aMatrix[13]; myMatrix[14] -= aMatrix[14]; myMatrix[15] -= aMatrix[15];
		return *this;
	}
	template<typename T>
	inline Matrix44<T> Matrix44<T>::operator+(const Matrix44<T>& aMatrix)
	{
		return Matrix44<T>(*this).operator+=(aMatrix);
	}
	template<typename T>
	inline Matrix44<T> Matrix44<T>::operator-(const Matrix44<T>& aMatrix)
	{
		return Matrix44<T>(*this).operator-=(aMatrix);
	}

	template<typename T>
	inline Matrix44<T> Matrix44<T>::operator*(const Matrix44<T>& aMatrix)
	{
		return Matrix44<T>(*this).operator*=(aMatrix);
	}
	template<typename T>
	inline Matrix44<T>& Matrix44<T>::operator*=(const Matrix44<T>& aMatrix)
	{
		T m[16];
		m[0] = myMatrix[0]; m[1] = myMatrix[1]; m[2] = myMatrix[2]; m[3] = myMatrix[3];
		m[4] = myMatrix[4]; m[5] = myMatrix[5]; m[6] = myMatrix[6]; m[7] = myMatrix[7];
		m[8] = myMatrix[8]; m[9] = myMatrix[9]; m[10] = myMatrix[10]; m[11] = myMatrix[11];
		m[12] = myMatrix[12]; m[13] = myMatrix[13]; m[14] = myMatrix[14]; m[15] = myMatrix[15];

		myMatrix[0]  =	(m[0]	* aMatrix[0]) + (m[1]	*	aMatrix[4]) + (m[2] * aMatrix[8])	+ (m[3]  * aMatrix[12]);
		myMatrix[1]  =	(m[0]	* aMatrix[1]) + (m[1]	*	aMatrix[5]) + (m[2] * aMatrix[9])	+ (m[3]  * aMatrix[13]);
		myMatrix[2]  =	(m[0]	* aMatrix[2]) + (m[1]	*	aMatrix[6]) + (m[2] * aMatrix[10])	+ (m[3]  * aMatrix[14]);
		myMatrix[3]  =	(m[0]	* aMatrix[3]) + (m[1]	*	aMatrix[7]) + (m[2] * aMatrix[11])	+ (m[3]  * aMatrix[15]);

		myMatrix[4]  =	(m[4]	* aMatrix[0]) + (m[5]	*	aMatrix[4]) + (m[6] * aMatrix[8])	+ (m[7]  * aMatrix[12]);
		myMatrix[5]  =	(m[4]	* aMatrix[1]) + (m[5]	*	aMatrix[5]) + (m[6] * aMatrix[9])	+ (m[7]  * aMatrix[13]);
		myMatrix[6]  =	(m[4]	* aMatrix[2]) + (m[5]	*	aMatrix[6]) + (m[6] * aMatrix[10])	+ (m[7]  * aMatrix[14]);
		myMatrix[7]  =	(m[4]	* aMatrix[3]) + (m[5]	*	aMatrix[7]) + (m[6] * aMatrix[11])	+ (m[7]  * aMatrix[15]);

		myMatrix[8]  =	(m[8]	* aMatrix[0]) + (m[9]	*	aMatrix[4]) + (m[10] * aMatrix[8])	+ (m[11] * aMatrix[12]);
		myMatrix[9]  =	(m[8]	* aMatrix[1]) + (m[9]	*	aMatrix[5]) + (m[10] * aMatrix[9])	+ (m[11] * aMatrix[13]);
		myMatrix[10] =	(m[8]	* aMatrix[2]) + (m[9]	*	aMatrix[6]) + (m[10] * aMatrix[10])	+ (m[11] * aMatrix[14]);
		myMatrix[11] =	(m[8]	* aMatrix[3]) + (m[9]	*	aMatrix[7]) + (m[10] * aMatrix[11])	+ (m[11] * aMatrix[15]);

		myMatrix[12] =	(m[12]	* aMatrix[0]) + (m[13]	*	aMatrix[4]) + (m[14] * aMatrix[8])	+ (m[15] * aMatrix[12]);
		myMatrix[13] =	(m[12]	* aMatrix[1]) + (m[13]	*	aMatrix[5]) + (m[14] * aMatrix[9])	+ (m[15] * aMatrix[13]);
		myMatrix[14] =	(m[12]	* aMatrix[2]) + (m[13]	*	aMatrix[6]) + (m[14] * aMatrix[10])	+ (m[15] * aMatrix[14]);
		myMatrix[15] =	(m[12]	* aMatrix[3]) + (m[13]	*	aMatrix[7]) + (m[14] * aMatrix[11])	+ (m[15] * aMatrix[15]);
		return *this;
	}

	template<typename T>
	inline Vector4<T>& operator*=(Vector4<T>& aVector, const Matrix44<T>& aMatrix)
	{
		T v[4];
		v[0] = aVector.x;
		v[1] = aVector.y;
		v[2] = aVector.z;
		v[3] = aVector.w;

		aVector.x = (v[0] * aMatrix[0]) + (v[1] * aMatrix[4]) + (v[2] * aMatrix[8]) + (v[3] * aMatrix[12]);
		aVector.y = (v[0] * aMatrix[1]) + (v[1] * aMatrix[5]) + (v[2] * aMatrix[9]) + (v[3] * aMatrix[13]);
		aVector.z = (v[0] * aMatrix[2]) + (v[1] * aMatrix[6]) + (v[2] * aMatrix[10]) + (v[3] * aMatrix[14]);
		aVector.w = (v[0] * aMatrix[3]) + (v[1] * aMatrix[7]) + (v[2] * aMatrix[11]) + (v[3] * aMatrix[15]);
		return aVector;
	}
	template<typename T>
	inline Vector4<T> operator*(const Vector4<T>& aVector, const Matrix44<T>& aMatrix)
	{
		return Vector4<T>(
			(aVector.x * aMatrix[0]) + (aVector.y * aMatrix[4]) + (aVector.z * aMatrix[8]) + (aVector.w * aMatrix[12]),
			(aVector.x * aMatrix[1]) + (aVector.y * aMatrix[5]) + (aVector.z * aMatrix[9]) + (aVector.w * aMatrix[13]),
			(aVector.x * aMatrix[2]) + (aVector.y * aMatrix[6]) + (aVector.z * aMatrix[10]) + (aVector.w * aMatrix[14]),
			(aVector.x * aMatrix[3]) + (aVector.y * aMatrix[7]) + (aVector.z * aMatrix[11]) + (aVector.w * aMatrix[15])
		);
	}

	template<typename T>
	inline bool Matrix44<T>::operator==(const Matrix44<T>& aMatrix)
	{
		return
			myMatrix[0] == aMatrix[0] && myMatrix[1] == aMatrix[1] && myMatrix[2] == aMatrix[2] && myMatrix[3] == aMatrix[3] &&
			myMatrix[4] == aMatrix[4] && myMatrix[5] == aMatrix[5] && myMatrix[6] == aMatrix[6] && myMatrix[7] == aMatrix[7] &&
			myMatrix[8] == aMatrix[8] && myMatrix[9] == aMatrix[9] && myMatrix[10] == aMatrix[10] && myMatrix[11] == aMatrix[11] &&
			myMatrix[12] == aMatrix[12] && myMatrix[13] == aMatrix[13] && myMatrix[14] == aMatrix[14] && myMatrix[15] == aMatrix[15];
	}

	template<typename T>
	inline Matrix44<T> Matrix44<T>::CreateRotateAroundX(T aAngleInRadians)
	{
		T c = static_cast<T>(std::cos(static_cast<double>(aAngleInRadians)));
		T s = static_cast<T>(std::sin(static_cast<double>(aAngleInRadians)));
		return Matrix44<T>(
			1,	0,	0,	0,
			0,	c,	s,	0,
			0,	-s,	c,	0,
			0,	0,	0,	1
		);
	}
	template<typename T>
	inline Matrix44<T> Matrix44<T>::CreateRotateAroundY(T aAngleInRadians)
	{
		T c = static_cast<T>(std::cos(static_cast<double>(aAngleInRadians)));
		T s = static_cast<T>(std::sin(static_cast<double>(aAngleInRadians)));
		return Matrix44<T>(
			c,	0,	-s,	0,
			0,	1,	0,	0,
			s,	0,	c,	0,
			0,	0,	0,	1
		);
	}
	template<typename T>
	inline Matrix44<T> Matrix44<T>::CreateRotateAroundZ(T aAngleInRadians)
	{
		T c = static_cast<T>(std::cos(static_cast<double>(aAngleInRadians)));
		T s = static_cast<T>(std::sin(static_cast<double>(aAngleInRadians)));
		return Matrix44<T>(
			c,	s,	0,	0,
			-s,	c,	0,	0,
			0,	0,	1,	0,
			0,	0,	0,	1
		);
	}

	template<typename T>
	inline Matrix44<T> Matrix44<T>::CreateYawPitchRoll(const Vector3<T>& aRotation)
	{
		Matrix44<T> m;

		float cy = std::cos(aRotation.y);
		float sy = std::cos(aRotation.y);
		float cx = std::cos(aRotation.x);
		float sx = std::cos(aRotation.x);
		float cz = std::cos(aRotation.z);
		float sz = std::cos(aRotation.z);
		
		m[0] = (cy * cz);
		m[1] = (cy * sz);
		m[2] = (-sy);
		
		m[4] = (sx * sy * cz + cx * -sz);
		m[5] = (sx * sy * sz + cx * cz);
		m[6] = (sx * cy);
		
		m[8] = (cx * sy * cz + -sx * -sz);
		m[9] = (cx * sy * sz + -sx * cz);
		m[10] = (cx * cy);
		
		return m;
	}

	template<typename T>
	inline Matrix44<T> Matrix44<T>::Transpose(const Matrix44<T>& aMatrixToTranspose)
	{
		return Matrix44<T>(
			aMatrixToTranspose[0],	aMatrixToTranspose[4],	aMatrixToTranspose[8],	aMatrixToTranspose[12],
			aMatrixToTranspose[1],	aMatrixToTranspose[5],	aMatrixToTranspose[9],	aMatrixToTranspose[13],
			aMatrixToTranspose[2],	aMatrixToTranspose[6],	aMatrixToTranspose[10],	aMatrixToTranspose[14],
			aMatrixToTranspose[3],	aMatrixToTranspose[7],	aMatrixToTranspose[11],	aMatrixToTranspose[15]
		);
	}

	template<typename T>
	inline Matrix44<T> Matrix44<T>::GetFastInverse()
	{
		return Matrix44<T>(
			//Rotation
			myMatrix[0],	myMatrix[4],	myMatrix[8],	myMatrix[3],
			myMatrix[1],	myMatrix[5],	myMatrix[9],	myMatrix[7],
			myMatrix[2],	myMatrix[6],	myMatrix[10],	myMatrix[11],

			//Translation
			(-myMatrix[12]*myMatrix[0])+(-myMatrix[13]*myMatrix[1])+(-myMatrix[14]*myMatrix[2]),
			(-myMatrix[12]*myMatrix[4])+(-myMatrix[13]*myMatrix[5])+(-myMatrix[14]*myMatrix[6]),
			(-myMatrix[12]*myMatrix[8])+(-myMatrix[13]*myMatrix[9])+(-myMatrix[14]*myMatrix[10]),

			//W	
			myMatrix[15]
		);
	}
}

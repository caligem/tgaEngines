#pragma once

#include <random>

namespace CommonUtilities
{
	void InitRand();
	void InitRand(unsigned int seed);
	float Random();
	float RandomRange(float aMin, float aMax);
}

#include "XBOXController.h"

#include "Mathf.h"

namespace CommonUtilities
{
	XBOXController::XBOXController(int aPlayerIndex)
	{
		myPlayerIndex = aPlayerIndex;
		ZeroMemory(&myCurrentState, sizeof(XINPUT_STATE));
		ZeroMemory(&myPreviousState, sizeof(XINPUT_STATE));
		myVibrate = true;
	}

	XBOXController::~XBOXController()
	{}

	bool XBOXController::IsConnected()
	{
		ZeroMemory(&myBufferState, sizeof(XINPUT_STATE));
		return XInputGetState(myPlayerIndex, &myBufferState) == ERROR_SUCCESS;
	}

	void XBOXController::Update()
	{
		myPreviousState = myCurrentState;
		XInputGetState(myPlayerIndex, &myCurrentState);
	}

	bool XBOXController::IsButtonPressed(XButton aButton)
	{
		return (aButton == (myCurrentState.Gamepad.wButtons & aButton)) &&
			(aButton != (myPreviousState.Gamepad.wButtons & aButton));
	}

	bool XBOXController::IsButtonDown(XButton aButton)
	{
		return (aButton == (myCurrentState.Gamepad.wButtons & aButton));
	}

	bool XBOXController::IsButtonReleased(XButton aButton)
	{
		return (aButton != (myCurrentState.Gamepad.wButtons & aButton)) &&
			(aButton == (myPreviousState.Gamepad.wButtons & aButton));
	}
	float XBOXController::GetAxis(XAxis aAxis)
	{
		if( aAxis == XAxis_LX )
		{
			return (static_cast<float>(myCurrentState.Gamepad.sThumbLX)+0.5f) / 32767.5f;
		}
		else if( aAxis == XAxis_LY )
		{
			return (static_cast<float>(myCurrentState.Gamepad.sThumbLY)+0.5f) / 32767.5f;
		}
		else if( aAxis == XAxis_RX )
		{
			return (static_cast<float>(myCurrentState.Gamepad.sThumbRX)+0.5f) / 32767.5f;
		}
		else if( aAxis == XAxis_RY )
		{
			return (static_cast<float>(myCurrentState.Gamepad.sThumbRY)+0.5f) / 32767.5f;
		}
		else if( aAxis == XAxis_LT )
		{
			return (static_cast<float>(myCurrentState.Gamepad.bLeftTrigger)) / 255.f;
		}
		else if( aAxis == XAxis_RT )
		{
			return (static_cast<float>(myCurrentState.Gamepad.bRightTrigger)) / 255.f;
		}
		return 0.0f;
	}
	CommonUtilities::Vector2f XBOXController::GetStick(XStick aStick)
	{
		float deadzone = 0.0625f;
		if( aStick == XStick_Left )
		{
			CommonUtilities::Vector2f stickInput(GetAxis(XAxis_LX), GetAxis(XAxis_LY));
			if( stickInput.Length2() < deadzone )
			{
				return{ 0.f, 0.f };
			}
			else
			{
				return stickInput.GetNormalized() * ((stickInput.Length() - deadzone) / (1.f - deadzone));
			}
		}
		else if( aStick == XStick_Right )
		{
			CommonUtilities::Vector2f stickInput(GetAxis(XAxis_RX), GetAxis(XAxis_RY));
			if( stickInput.Length2() < deadzone )
			{
				return{ 0.f, 0.f };
			}
			else
			{
				return stickInput.GetNormalized() * ((stickInput.Length() - deadzone) / (1.f - deadzone));
			}
		}
		return { 0.f, 0.f };
	}
	void XBOXController::Vibrate(float aLeftMotorVel, float aRightMotorVel)
	{
		if (!myVibrate)
		{
			return;
		}

		XINPUT_VIBRATION vibration;
		ZeroMemory(&vibration, sizeof(XINPUT_VIBRATION));

		vibration.wLeftMotorSpeed = static_cast<WORD>(Clamp(aLeftMotorVel, 0.f, 1.f) * 65535.f);
		vibration.wRightMotorSpeed = static_cast<WORD>(Clamp(aRightMotorVel, 0.f, 1.f) * 65535.f);

		XInputSetState(myPlayerIndex, &vibration);
	}
	void XBOXController::SetVibration(bool aVibrate)
	{
		Vibrate(0.f, 0.f);
		myVibrate = aVibrate;
	}
}

#include "stdafx.h"
#include "Camera.h"

#include "DirectXFramework.h"
#include "DXMacros.h"
#include "CameraComponent.h"


#include <d3d11.h>

CCamera::CCamera()
{
}


CCamera::~CCamera()
{
}

bool CCamera::Init(const CommonUtilities::Matrix44f & aProjectionMatrix)
{
	myProjection = aProjectionMatrix;

	return true;
}

#pragma once

#include "Matrix.h"

class CDirectXFramework;
class CCameraComponent;
struct ID3D11Buffer;

class CCamera
{
public:
	CCamera();
	~CCamera();

	bool Init(const CommonUtilities::Matrix44f& aProjectionMatrix);

	const CommonUtilities::Matrix44f& GetProjection() const { return myProjection; }

private:
	CommonUtilities::Matrix44f myProjection;
};


#include "stdafx.h"
#include "CameraComponent.h"
#include "ComponentSystem.h"
#include "CameraManager.h"
#include "IEngine.h"
#include "Camera.h"
#include "Matrix.h"
#include <cmath>

CCameraManager* CCameraComponent::ourCameraManager = nullptr;

CCameraComponent::CCameraComponent()
	: myCameraID(ID_T_INVALID(CCamera))
{
}

CCameraComponent::~CCameraComponent()
{
}

bool CCameraComponent::Init(SCameraComponentData aCameraComponentData)
{
	if (aCameraComponentData.cameraFovInDegrees < 60.f)
	{
		ENGINE_LOG(CONCOL_ERROR, "You're camera fov is very small: %f degrees!", aCameraComponentData.cameraFovInDegrees);
		aCameraComponentData.cameraFovInDegrees = 90.f;
	}
	else if (aCameraComponentData.cameraFovInDegrees >= 180.f)
	{
		ENGINE_LOG(CONCOL_ERROR, "You're camera fov is invalid: %f degrees!", aCameraComponentData.cameraFovInDegrees);
		aCameraComponentData.cameraFovInDegrees = 90.f;
	}

	float B = 1.f / std::tan(aCameraComponentData.cameraFovInDegrees * CommonUtilities::DegToRad * 0.5f);
	float A = B / aCameraComponentData.aspectRatio;
	float C = aCameraComponentData.farPlane / (aCameraComponentData.farPlane - aCameraComponentData.nearPlane);
	float D = 1.f;
	float E = -aCameraComponentData.nearPlane * aCameraComponentData.farPlane / (aCameraComponentData.farPlane - aCameraComponentData.nearPlane);

	CommonUtilities::Matrix44<float> projectionMatrix(
		A, 0, 0, 0,
		0, B, 0, 0,
		0, 0, C, D,
		0, 0, E, 0);

	ID_T(CCamera) cameraID = ourCameraManager->AcquireCamera(projectionMatrix);
	myCameraID = cameraID;

	return true;
}

void CCameraComponent::Move(const CommonUtilities::Vector3f & aMovement)
{
	myOrientation[12] += aMovement.x;
	myOrientation[13] += aMovement.y;
	myOrientation[14] += aMovement.z;
}

void CCameraComponent::Rotate(const CommonUtilities::Vector3f & aRotation)
{
	myRotation += aRotation;

	float x, y, z;
	x = myOrientation[12];
	y = myOrientation[13];
	z = myOrientation[14];

	myOrientation = CommonUtilities::Matrix44f::CreateRotateAroundZ(myRotation.z);
	myOrientation *= CommonUtilities::Matrix44f::CreateRotateAroundX(myRotation.x);
	myOrientation *= CommonUtilities::Matrix44f::CreateRotateAroundY(myRotation.y);

	myOrientation[12] = x;
	myOrientation[13] = y;
	myOrientation[14] = z;
}

const CommonUtilities::Vector3f CCameraComponent::GetPosition() const
{
	return{
		myOrientation[12],
		myOrientation[13],
		myOrientation[14]
	};
}

CommonUtilities::Matrix44f& CCameraComponent::GetMyOrientation()
{
	return myOrientation;
}


const CommonUtilities::Vector3f CCameraComponent::GetForward() const
{
	return (CommonUtilities::Vector3f(0.f, 0.f, 1.f)*CommonUtilities::Matrix33f(myOrientation)).GetNormalized();
}

const CommonUtilities::Vector3f CCameraComponent::GetRight() const
{
	return (CommonUtilities::Vector3f(1.f, 0.f, 0.f)*CommonUtilities::Matrix33f(myOrientation)).GetNormalized();
}

const CommonUtilities::Vector3f CCameraComponent::GetUp() const
{
	return (CommonUtilities::Vector3f(0.f, 1.f, 0.f)*CommonUtilities::Matrix33f(myOrientation)).GetNormalized();
}

void CCameraComponent::SetOrientationMatrix(const CommonUtilities::Matrix44f & aMatrix)
{
	myOrientation = aMatrix;
}

void CCameraComponent::SetPosition(const CommonUtilities::Vector3f & aPosition)
{
	myOrientation[12] = aPosition.x;
	myOrientation[13] = aPosition.y;
	myOrientation[14] = aPosition.z;
}
#pragma once

#include "Matrix.h"
#include "Vector.h"
#include "ObjectPool.h"
#include "Component.h"

class CCamera;
class CScene;
class CForwardRenderer;
class CDirectXFramework;
class CComponentSystem;
class CCameraManager;

class CCameraComponent : public CComponent
{
public:
	CCameraComponent();
	~CCameraComponent();

	struct SCameraComponentData
	{

		SCameraComponentData(float aFovInDegrees, float aAspectRatio, float aNearPlane, float aFarPlane)
		{
			cameraFovInDegrees = aFovInDegrees;
			aspectRatio = aAspectRatio;
			nearPlane = aNearPlane;
			farPlane = aFarPlane;
		}

		float nearPlane;
		float aspectRatio;
		float farPlane;
		float cameraFovInDegrees;
	};

	bool Init(SCameraComponentData aCameraComponentData);
	void Move(const CommonUtilities::Vector3f& aMovement);
	void Rotate(const CommonUtilities::Vector3f& aRotation);
	const CommonUtilities::Vector3f GetPosition() const;

	CommonUtilities::Matrix44f& GetMyOrientation();

	const CommonUtilities::Vector3f GetForward() const;
	const CommonUtilities::Vector3f GetRight() const;
	const CommonUtilities::Vector3f GetUp() const;

	// To Set Camera behind player
	void SetOrientationMatrix(const CommonUtilities::Matrix44f& aMatrix);
	void SetPosition(const CommonUtilities::Vector3f& aPosition);

private:
	friend CForwardRenderer;
	friend CComponentSystem;
	friend CCamera;
	friend CScene;
	friend CEngine;

	static CCameraManager* ourCameraManager;

	ID_T(CCamera) myCameraID;
	CommonUtilities::Matrix44f myOrientation;
	CommonUtilities::Vector3f myRotation;
};


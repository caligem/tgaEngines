#include "stdafx.h"
#include "CameraManager.h"


CCameraManager::CCameraManager()
{
}


CCameraManager::~CCameraManager()
{
}

bool CCameraManager::Init()
{
	return true;
}

ID_T(CCamera) CCameraManager::AcquireCamera(const CommonUtilities::Matrix44f& aProjectionMatrix)
{
	ID_T(CCamera) id = myCameras.Acquire();
	myCameras.GetObj(id)->Init(aProjectionMatrix);
	return std::move(id);
}

CCamera * CCameraManager::GetCamera(ID_T(CCamera) aCameraID)
{
	return std::move(myCameras.GetObj(aCameraID));
}

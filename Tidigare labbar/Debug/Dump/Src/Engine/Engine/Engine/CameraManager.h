#pragma once
#include "Camera.h"

class CEngine;

class CCameraManager
{
public:
	CCameraManager();
	~CCameraManager();

	bool Init();

	ID_T(CCamera) AcquireCamera(const CommonUtilities::Matrix44f& aProjectionMatrix);
private:
	friend CEngine;

	CCamera* GetCamera(ID_T(CCamera) aCameraID);
	ObjectPool<CCamera> myCameras;
};


#pragma once

class CGameObjectData;
class CForwardRenderer;
class CEngine;

class CComponent
{
public:
	CComponent();
	~CComponent();

	inline void SetParent(ID_T(CGameObjectData) aParentID) { myGameObjectDataID = aParentID; }

private:
	friend CForwardRenderer;
	friend CEngine;

	ID_T(CGameObjectData) myGameObjectDataID;
};
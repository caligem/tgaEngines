#include "stdafx.h"
#include "ComponentSystem.h"
#include "DirectXFramework.h"
#include "Scene.h"
#include "GameObject.h"
#include "ModelManager.h"

CComponentSystem::CComponentSystem()
	: myScene(nullptr)
{
}

CComponentSystem::~CComponentSystem()
{
}

void CComponentSystem::UpdateComponents()
{
	UpdateCameraComponents();
}

bool CComponentSystem::Init(CScene& aScene)
{
	if (&aScene == nullptr)
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: aScene was nullptr in ComponentSystem Init.");
		return false;
	}
	myScene = &aScene;

	CGameObject::ourComponentSystem = this;

	return true;
}

void CComponentSystem::AddComponentToDestroyQueue(SDestroyData aDestroyData)
{
 	ID_T(SDestroyData) id = myComponentsToBeDestroyed.Acquire();
	myComponentsToBeDestroyed.GetObj(id)->SetData(aDestroyData);
}

void CComponentSystem::DestroyComponentsInQueue()
{
 	for (auto it = myComponentsToBeDestroyed.begin(); it != myComponentsToBeDestroyed.end(); ++it)
	{
		_UUID_T componentType = it->data.componentType;

			if (componentType == _UUID(CCameraComponent))
			{
				myCameraComponents.Release(it->data.componentID);
				myScene->RemoveComponent<CCameraComponent>(it->data.componentID);
			}
			else if (componentType == _UUID(CModelComponent))
			{
				myModelComponents.Release(it->data.componentID);
				myScene->RemoveComponent<CModelComponent>(it->data.componentID);
			}

			myGameObjectData.Release(it->data.gameObjectDataID);
			myComponentsToBeDestroyed.Release(it->id);
	}
}

void CComponentSystem::UpdateCameraComponents()
{
	/*
	for (unsigned short i = 0; i < myCameraComponents.GetSize(); ++i)
	{
		if (myCameraComponents.IsValid(i))
		{
			CCameraComponent* cameraComponent = myCameraComponents.GetObj(i);
			ID_T(CCamera) cameraID = cameraComponent->myCameraID;
			myCameras.GetObj(cameraID)->UpdateBuffers(myDirectXFramework, cameraComponent);
		}
	}
	*/
}

ID_T(CGameObjectData) CComponentSystem::CreateGameObjectData()
{
	ID_T(CGameObjectData) dataID = myGameObjectData.Acquire();
	myGameObjectData.GetObj(dataID)->Init(dataID);
	return dataID;
}

CGameObjectData * CComponentSystem::GetGameObjectData(ID_T(CGameObjectData) aGameObjectDataIDptr)
{
	return myGameObjectData.GetObj(aGameObjectDataIDptr);
}

bool CComponentSystem::Shutdown()
{
	return true;
}

#pragma once

#include "ObjectPool.h"
#include "ModelComponent.h"
#include "GameObjectData.h"
#include "Camera.h"
#include "CameraComponent.h"

class CScene;
class CDirectXFramework;
class GameObjectData;
class CModel;
class CModelManager;
class CForwardRenderer;
class CGameObject;

class CComponentSystem
{
public:
	CComponentSystem();
	~CComponentSystem();

private:
	friend CGameObjectData;
	friend CForwardRenderer;
	friend CEngine;
	friend CGameObject;
	friend CScene;

	bool Init(CScene& aScene);
	void UpdateComponents();
	void UpdateCameraComponents();

	template<typename T, typename ParameterPack>
	T* AddComponent(ID_T(CGameObjectData) aGameObjectDataID, ParameterPack aParameterPack);

	template<typename T>
	T* GetComponentPointer(int aComponentID);

	CGameObjectData* GetGameObjectData(ID_T(CGameObjectData) aGameObjectDataIDptr);
	ID_T(CGameObjectData) CreateGameObjectData();

	struct SDestroyData
	{
		SDestroyData() {};

		SDestroyData(ID_T(CGameObjectData) aGameObjectDataID, int aComponentID, _UUID_T aComponentType)
		{
			componentID = aComponentID;
			componentType = aComponentType;
			gameObjectDataID = aGameObjectDataID;
		}

		void SetData(SDestroyData aDestroyData)
		{
			componentID = aDestroyData.componentID;
			componentType = aDestroyData.componentType;
			gameObjectDataID = aDestroyData.gameObjectDataID;
		}

		int componentID;
		_UUID_T componentType;
		ID_T(CGameObjectData) gameObjectDataID;
	};

	void AddComponentToDestroyQueue(SDestroyData aDestroyData);
	void DestroyComponentsInQueue();
	bool Shutdown();

	CScene* myScene;

	ObjectPool<SDestroyData> myComponentsToBeDestroyed;

	ObjectPool<CCameraComponent> myCameraComponents;
	ObjectPool<CModelComponent> myModelComponents;
	ObjectPool<CGameObjectData> myGameObjectData;
};

template<typename T, typename ParameterPack>
inline T* CComponentSystem::AddComponent(ID_T(CGameObjectData) aGameObjectDataID, ParameterPack aParameterPack)
{
	//ComponentCreationData CCD = myComponentStorage.GetObj(sceneID)->AddComponent<T>();
	//_UUID(ALL COMPONENTS) IN INIT 
	int componentID = -1;

	CComponent* component = nullptr;
	if (_UUID(T) == _UUID(CModelComponent))
	{
		ID_T(CModelComponent) cid = myModelComponents.Acquire();
		component = myModelComponents.GetObj(cid);
		myScene->AddModelComponent(cid);
		componentID = cid.val;
	}
	else if (_UUID(T) == _UUID(CCameraComponent))
	{
		ID_T(CCameraComponent) cid = myCameraComponents.Acquire();
		component = myCameraComponents.GetObj(cid);
		myScene->AddCameraComponent(cid);
		myScene->SetActiveCamera(cid);
		componentID = cid.val;
	}

	T* tPtr = static_cast<T*>(component);
	tPtr->Init(aParameterPack);
	tPtr->SetParent(aGameObjectDataID);
	GetGameObjectData(aGameObjectDataID)->AddSComponent(ID_T(T)(componentID), _UUID(T));

	return tPtr;
}

template<typename T>
inline T* CComponentSystem::GetComponentPointer(int aComponentID)
{
	CComponent* component = nullptr;

	if (_UUID(T) == _UUID(CModelComponent))
	{
		component = myModelComponents.GetObj(aComponentID);
	}
	else if (_UUID(T) == _UUID(CCameraComponent))
	{
		component = myCameraComponents.GetObj(aComponentID);
	}
	else
	{
		ENGINE_LOG(CONCOL_WARNING, "type T does not match any Component // CComponentSystem::GetComponent()");
		return nullptr;
	}

	return static_cast<T*>(component);
}
#include "stdafx.h"
#include "DirectXFramework.h"

#include <d3d11.h>
#pragma comment(lib, "d3d11.lib")

#include "DXMacros.h"
#include "DL_Debug.h"
#include "Engine.h"

CDirectXFramework::CDirectXFramework()
{
}

CDirectXFramework::~CDirectXFramework()
{
}

bool CDirectXFramework::Init(CWindowHandler& aWindowHandler, const SCreateParameters* aCreateParameters)
{
	myCreateParameters = aCreateParameters;

	HRESULT result;
	
	DXGI_SWAP_CHAIN_DESC swapchainDesc = {};
	swapchainDesc.BufferCount = 1;
	swapchainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapchainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapchainDesc.OutputWindow = aWindowHandler.GetWindowHandle();
	swapchainDesc.SampleDesc.Count = 1;
	swapchainDesc.Windowed = !myCreateParameters->myIsFullscreen;
	swapchainDesc.Flags = DXGI_MWA_NO_ALT_ENTER;

	//Create swapchain, device and context
	result = D3D11CreateDeviceAndSwapChain(
		nullptr, D3D_DRIVER_TYPE_HARDWARE,
		nullptr, NULL, nullptr, NULL, D3D11_SDK_VERSION,
		&swapchainDesc, &mySwapChain, &myDevice, nullptr, &myContext
	);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to create the swapchain!");
		return false;
	}

	//Create backbuffer
	ID3D11Texture2D* backbufferTexture;
	ValidateResult(mySwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (void**)&backbufferTexture));
	result = myDevice->CreateRenderTargetView(backbufferTexture, nullptr, &myBackBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to create the back buffer render target view!");
		return false;
	}
	ValidateResult(backbufferTexture->Release());

	//Set viewport
	RECT rect;
	GetClientRect(aWindowHandler.GetWindowHandle(), &rect);

	D3D11_VIEWPORT viewport;
	viewport.TopLeftX = 0.f;
	viewport.TopLeftY = 0.f;
	viewport.Width = static_cast<float>(rect.right-rect.left);
	viewport.Height = static_cast<float>(rect.bottom-rect.top);
	viewport.MinDepth = 0.f;
	viewport.MaxDepth = 1.0f;
	myContext->RSSetViewports(1, &viewport);

	ENGINE_LOG(CONCOL_VALID, "Viewport created at: %d x %d", static_cast<unsigned>(viewport.Width), static_cast<unsigned int>(viewport.Height));

	//Create depthstencil
	ID3D11Texture2D* backDepthTexture;
	D3D11_TEXTURE2D_DESC backDepthTextureDesc = {};
	backDepthTextureDesc.Width = static_cast<unsigned int>(viewport.Width);
	backDepthTextureDesc.Height = static_cast<unsigned int>(viewport.Height);
	backDepthTextureDesc.ArraySize = 1;
	backDepthTextureDesc.Format = DXGI_FORMAT_D32_FLOAT;
	backDepthTextureDesc.SampleDesc.Count = 1;
	backDepthTextureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	
	ValidateResult(myDevice->CreateTexture2D(&backDepthTextureDesc, nullptr, &backDepthTexture));

	result = myDevice->CreateDepthStencilView(backDepthTexture, nullptr, &myDepthStencil);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create DepthStencilView!");
	}

	myContext->OMSetRenderTargets(1, &myBackBuffer, myDepthStencil);

	return true;
}

void CDirectXFramework::BeginFrame(float aClearColor[4])
{
	myContext->ClearRenderTargetView(myBackBuffer, aClearColor);
	myContext->ClearDepthStencilView(myDepthStencil, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.f, 0);
}

void CDirectXFramework::EndFrame()
{
	if (myCreateParameters->myEnableVSync)
	{
		mySwapChain->Present(1, 0);
	}
	else
	{
		mySwapChain->Present(0, 0);
	}
}

void CDirectXFramework::Shutdown()
{
	SafeRelease(myBackBuffer);
	SafeRelease(myContext);
	SafeRelease(myDevice);
	SafeRelease(mySwapChain);
}

#pragma once

#include "WindowHandler.h"

struct SCreateParameters;

struct IDXGISwapChain;
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11RenderTargetView;
struct ID3D11DepthStencilView;

class CDirectXFramework
{
public:
	CDirectXFramework();
	~CDirectXFramework();

	bool Init(CWindowHandler& aWindowHandler, const SCreateParameters* aCreateParameters);

	void BeginFrame(float aClearColor[4]);
	void EndFrame();

	void Shutdown();

	ID3D11Device* GetDevice() { return myDevice; }
	ID3D11DeviceContext* GetContext() { return myContext; }

	ID3D11RenderTargetView* GetBackBuffer() { return myBackBuffer; }

private:
	IDXGISwapChain* mySwapChain;
	ID3D11Device* myDevice;
	ID3D11DeviceContext* myContext;
	ID3D11RenderTargetView* myBackBuffer;
	ID3D11DepthStencilView* myDepthStencil;

	const SCreateParameters* myCreateParameters;

};



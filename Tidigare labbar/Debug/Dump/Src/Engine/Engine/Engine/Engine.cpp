#include "stdafx.h"
#include "Engine.h"
#include "IEngine.h"

#include "ModelComponent.h"
#include "ComponentSystem.h"

#include <DirectXMath.h>

#include "ThreadNamer.h"

#include "Minidump.h"

CEngine::CEngine()
{
	MiniDump::ourMinidumpEnginePtr = this;
}

CEngine::~CEngine()
{
}


bool CEngine::Init(const SCreateParameters& aCreateParameters)
{
	ENGINE_LOG(CONCOL_VALID, "Starting the engine...");

	IEngine::ourEngine = this;

	if (!myWindowHandler.Init(&aCreateParameters))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise WindowHandler!");
		return false;
	}

	if (!myFramework.Init(myWindowHandler, &aCreateParameters))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise DirectXFramework!");
		return false;
	}

	myScene.Init();

	if (!myModelManager.Init(myFramework))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise ModelManager.");
	}
	CModelComponent::ourModelManager = &myModelManager;

	if (!myCameraManager.Init())
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise CameraManager.");
	}
	CCameraComponent::ourCameraManager = &myCameraManager;

	myForwardRenderer.Init(myModelManager);

	if (!myComponentSystem.Init(myScene))
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: Failed to initialise Component System.");
	}

	myModelsToRender.Init(16);
	myRenderCommands.Init();

	if (!myCameraBuffer.Init(sizeof(SCameraBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create ForwardRenderer::myCameraBuffer");
	}

	//Start engine
	CacheMemAndCPU(true);
	myCreateParameters = aCreateParameters;

	if (myCreateParameters.myInitFunctionToCall)
	{
		myCreateParameters.myInitFunctionToCall();
	}

	myUpdateFunctionToCall = myCreateParameters.myUpdateFunctionToCall;

	myIsRunning = true;

	if (myCreateParameters.myUpdateFunctionToCall)
	{
		myRenderThread = std::thread(&CEngine::RenderThread, this);
		Run();
	}

	return true;
}

void CEngine::BeginFrame()
{
	float clearColor[] = { 1.0f, 0.7f, 0.3f, 1.f };
	myFramework.BeginFrame(clearColor);
}
void CEngine::EndFrame()
{
	myFramework.EndFrame();
}
void CEngine::RenderFrame()
{
	myForwardRenderer.Render(myRenderCommands.GetReadBuffer(), myCameraBuffer);
}

void CEngine::Shutdown()
{
	myFramework.Shutdown();

	if(!myComponentSystem.Shutdown())
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to shutdown ComponentSystem.")
	}

	if (myRenderThread.joinable())
	{
		myRenderThread.join();
	}
}

void CEngine::Run()
{
	MSG windowsMessage = { 0 };

	while (myIsRunning)
	{
		while (PeekMessage(&windowsMessage, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&windowsMessage);
			DispatchMessage(&windowsMessage);

			if (windowsMessage.message == WM_QUIT)
			{
				myIsRunning = false;
			}
		}

		CacheMemAndCPU();

		myRenderThreadFinished = false;

		myComponentSystem.DestroyComponentsInQueue();
		myComponentSystem.UpdateComponents();

		myUpdateFunctionToCall();

		//Setup render buffers
		myModelsToRender.RemoveAll();
		myScene.GetModelsFromActiveScene(myModelsToRender);

		for (auto model : myModelsToRender)
		{
			CModelComponent* modelComponent = static_cast<CModelComponent*>(myComponentSystem.GetComponentPointer<CModelComponent>(model.val));
			myRenderCommands.Write({
				myComponentSystem.GetGameObjectData(modelComponent->myGameObjectDataID)->GetTransform().GetMatrix(),
				modelComponent->myModelID
			});
		}

		//Sync threads
		while (myIsRunning && !myRenderThreadFinished) {}

		myRenderCommands.SwapBuffers();

		ID_T(CCamera) cameraID = IEngine::GetScene()->GetActiveCamera()->myCameraID;
		SCameraBufferData data;
		data.myCameraOrientation = IEngine::GetScene()->GetActiveCamera()->GetMyOrientation();
		data.myToCamera = data.myCameraOrientation.GetFastInverse();
		data.myProjection = myCameraManager.GetCamera(cameraID)->GetProjection();
		myCameraBuffer.SetData(&data);

		static int statisticsTime = 0;
		if (++statisticsTime % 1000 == 0)
		{
			int* chrille = NULL;
			*chrille = 0;
			GENERAL_LOG(CONCOL_DEFAULT, "CPU Usage: %.1f%%\tMem Usage: %dMb", GetCPUUsageInternal(), GetMemUsageInternal());
			statisticsTime = 0;
		}
	}
}

void CEngine::RenderThread()
{
	__try
	{
		SetThreadName("Render Thread");

		while (myIsRunning)
		{
			if (!myRenderThreadFinished)
			{
				BeginFrame();
				RenderFrame();
				EndFrame();
				myRenderThreadFinished = true;
			}
		}
	}
	__except (MiniDump::ExceptionFilterFunction(GetExceptionInformation())) { }
}

float CEngine::GetCPUUsageInternal()
{
	return myCPUUsage;
}

int CEngine::GetMemUsageInternal()
{
	return myMemUsage;
}

void CEngine::CacheMemAndCPU(bool aFirstRun)
{
	myCPUUsage = GetCPUUsage(&myPrevSysKernel, &myPrevSysUser, &myPrevProcKernel, &myPrevProcUser, aFirstRun); 
	myMemUsage = GetMemUsage();
}


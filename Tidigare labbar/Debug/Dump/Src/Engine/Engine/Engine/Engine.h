#pragma once

#include "DirectXFramework.h"

#include <GrowingArray.h>
#include "Scene.h"
#include "ForwardRenderer.h"
#include "ComponentSystem.h"
#include "RenderCommand.h"
#include "ModelManager.h"
#include "CameraManager.h"
#include "DoubleBuffer.h"

#include <thread>
#include <atomic>

#include "SystemStats.h"

class IEngine;

struct MiniDump;

using callback_function = std::function<void()>;
using callback_function_update = std::function<void()>;
using callback_function_wndProc = std::function<LRESULT(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam)>;

struct SCreateParameters
{
	callback_function myInitFunctionToCall;
	callback_function_update myUpdateFunctionToCall;
	callback_function_wndProc myWndProcCallback;

	unsigned short myCanvasWidth = 800;
	unsigned short myCanvasHeight = 600;

	bool myEnableVSync = true;
	bool myIsFullscreen = false;
	bool myIsBorderless = false;

	std::wstring myGameName = L"NO NAME GAME!";
};

class CEngine
{
public:
	CEngine();
	~CEngine();

	bool Init(const SCreateParameters& aCreateParameters);
	void Shutdown();

	void BeginFrame();
	void EndFrame();
	void RenderFrame();

	struct SCameraBufferData
	{
		CommonUtilities::Matrix44f myCameraOrientation;
		CommonUtilities::Matrix44f myToCamera;
		CommonUtilities::Matrix44f myProjection;
	};

private:
	friend IEngine;
	friend MiniDump;

	void Run();
	void RenderThread();

	CWindowHandler myWindowHandler;
	CDirectXFramework myFramework;

	CComponentSystem myComponentSystem;
	CModelManager myModelManager;
	CCameraManager myCameraManager;

	CommonUtilities::GrowingArray<ID_T(CModelComponent)> myModelsToRender;

	CScene myScene;
	CForwardRenderer myForwardRenderer;

	SCreateParameters myCreateParameters;

	callback_function myInitFunctionToCall;
	callback_function_update myUpdateFunctionToCall;

	bool myIsRunning = false;

	CDoubleBuffer<SRenderCommand> myRenderCommands;
	std::thread myRenderThread;
	std::atomic<bool> myRenderThreadFinished = false;
	CConstantBuffer myCameraBuffer;

	FILETIME myPrevSysKernel;
	FILETIME myPrevSysUser;
	FILETIME myPrevProcKernel;
	FILETIME myPrevProcUser;

	float GetCPUUsageInternal();
	int GetMemUsageInternal();
	float myCPUUsage;
	int myMemUsage;
	void CacheMemAndCPU(bool aFirstRun = false);
};


#include "stdafx.h"
#include "ForwardRenderer.h"

#include "DirectXFramework.h"
#include "RenderCommand.h"
#include "ModelComponent.h"
#include "ModelManager.h"

#include "IEngine.h"
#include "ComponentSystem.h"
#include "Scene.h"

#include <d3d11.h>

#include "DDSTextureLoader.h"

CForwardRenderer::CForwardRenderer()
{
}

CForwardRenderer::~CForwardRenderer()
{
}

void CForwardRenderer::Init(CModelManager& aModelManager)
{

	if (!myInstanceBuffer.Init(sizeof(SInstanceBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create ForwardRenderer::myInstanceBuffer");
	}

	if (!myDirectionalLightBuffer.Init(sizeof(SLightBufferData)))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to create ForwardRenderer::myDirectionalLightBuffer");
	}

	HRESULT result = DirectX::CreateDDSTextureFromFile(IEngine::GetDevice(), L"Assets/Cubemaps/skansen.dds", nullptr, &myCubemap);
	if (FAILED(result))
	{
		RESOURCE_LOG(CONCOL_ERROR, "ERROR: Failed to load Cubemap in CForwardRenderer!");
		myCubemap = NULL;
	}

	myModelManager = &aModelManager;
}

void CForwardRenderer::Render(const CommonUtilities::GrowingArray<SRenderCommand>& aRenderCommands, CConstantBuffer& aCameraBuffer)
{
	ID3D11DeviceContext* context = IEngine::GetContext();

	aCameraBuffer.SetBuffer(0, EShaderType_Vertex);

	SLightBufferData lightData;
	lightData.myDirectionalLight = { -0.5f, -0.5f, 0.707f, 0.f }; //IEngine::GetScene()->GetDirectionalLight();
	lightData.myDirectionalLightColor = { 1.0f, 0.3f, 0.1f, 1.f };
	myDirectionalLightBuffer.SetData(&lightData);
	myDirectionalLightBuffer.SetBuffer(0, EShaderType_Pixel);

	SInstanceBufferData instanceData;

	for (const SRenderCommand& command : aRenderCommands)
	{
		CModel* model = myModelManager->GetModel(command.myModelToRender);

		const SVertexDataWrapper& vertexData = model->GetVertexData();
		const SShaderDataWrapper& shaderData = model->GetShaderData();
		SLayoutDataWrapper& layoutData = model->GetLayoutData();
		STextureDataWrapper& textureData = model->GetTextureData();
		
		instanceData.myToWorld = command.myTransform;

		myInstanceBuffer.SetData(&instanceData);

		context->IASetPrimitiveTopology(layoutData.myPrimitiveTopology);
		context->IASetInputLayout(layoutData.myInputLayout);
		context->IASetVertexBuffers(0, 1, &vertexData.myVertexBuffer, &vertexData.myStride, &vertexData.myOffset);
		context->IASetIndexBuffer(vertexData.myIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		myInstanceBuffer.SetBuffer(1, EShaderType_Vertex);
		context->VSSetShader(shaderData.myVertexShader, nullptr, 0);

		context->PSSetShader(shaderData.myPixelShader, nullptr, 0);
		context->PSSetShaderResources(0, sizeof(textureData.myTextures)/sizeof(textureData.myTextures[0]), textureData.myTextures);
		context->PSSetShaderResources(sizeof(textureData.myTextures)/sizeof(textureData.myTextures[0]), 1, &myCubemap);

		context->DrawIndexed(vertexData.myNumberOfIndices, 0, 0);
	}
}

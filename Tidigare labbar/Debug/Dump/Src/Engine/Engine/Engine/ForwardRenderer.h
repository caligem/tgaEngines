#pragma once

#include <GrowingArray.h>
#include "Matrix.h"

#include "ConstantBuffer.h"
class CDirectXFramework;
class CModelComponent;
class CModelManager;

struct SRenderCommand;
struct ID3D11Buffer;
struct ID3D11ShaderResourceView;

class CForwardRenderer
{
public:
	CForwardRenderer();
	~CForwardRenderer();

	void Init(CModelManager& aModelManager);
	void Render(const CommonUtilities::GrowingArray<SRenderCommand>& aModelsToRender, CConstantBuffer& aCameraBuffer);

private:
	struct SInstanceBufferData
	{
		CommonUtilities::Matrix44f myToWorld;
	};
	struct SLightBufferData
	{
		CommonUtilities::Vector4f myDirectionalLight;
		CommonUtilities::Vector4f myDirectionalLightColor;
	};
	
	CModelManager* myModelManager;
	CConstantBuffer myInstanceBuffer;
	CConstantBuffer myDirectionalLightBuffer;
	CConstantBuffer myCameraBuffer;
	ID3D11ShaderResourceView* myCubemap;
};


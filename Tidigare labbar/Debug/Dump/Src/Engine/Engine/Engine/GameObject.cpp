#include "stdafx.h"
#include "GameObject.h"

CComponentSystem* CGameObject::ourComponentSystem = nullptr;

CGameObject::CGameObject()
	: myIsInitialized(false)
{

}

CGameObject::~CGameObject()
{

}

void CGameObject::Init()
{
	myGameObjectDataID = ourComponentSystem->CreateGameObjectData();
	myIsInitialized = true;
}
// 
// CComponent * CGameObject::AddCameraComponent(float aFovInDegrees, const CommonUtilities::Vector3f& aStartPosition)
// {
// 	return ourComponentSystem->CreateCameraComponent(this, aFovInDegrees, aStartPosition);
// }
// 
// CComponent* CGameObject::AddModelComponent(const char* aFilePath)
// {
// 	return ourComponentSystem->CreateModelComponent(this, aFilePath);
// }

void CGameObject::Move(const CommonUtilities::Vector3f& aMovement)
{
	if (!IsInitializedCheck())
	{
		return;
	}
	GetTransform().Move(aMovement);
}

void CGameObject::Rotate(const CommonUtilities::Vector3f& aRotation)
{
	if (!IsInitializedCheck())
	{
		return;
	}
	GetTransform().Rotate(aRotation);
}

void CGameObject::SetPosition(const CommonUtilities::Vector3f& aPosition)
{
	if (!IsInitializedCheck())
	{
		return;
	}
	GetTransform().SetPosition(aPosition);
}

const CommonUtilities::Vector3f CGameObject::GetForward()
{
	return (CommonUtilities::Vector3f(0.f, 0.f, 1.f) * CommonUtilities::Matrix33f(GetTransform().GetMatrix())).GetNormalized();
}

const CommonUtilities::Vector3f CGameObject::GetRight()
{
	return (CommonUtilities::Vector3f(1.f, 0.f, 0.f) * CommonUtilities::Matrix33f(GetTransform().GetMatrix())).GetNormalized();
}

const CommonUtilities::Vector3f CGameObject::GetUp()
{
	return (CommonUtilities::Vector3f(0.f, 1.f, 0.f) * CommonUtilities::Matrix33f(GetTransform().GetMatrix())).GetNormalized();
}


void CGameObject::Destroy()
{
	if (!IsInitializedCheck())
	{
		return;
	}
	ourComponentSystem->GetGameObjectData(myGameObjectDataID)->Destroy();
	myIsInitialized = false;
}

bool CGameObject::IsInitializedCheck()
{
	if (!myIsInitialized)
	{
		ENGINE_LOG(CONCOL_WARNING, "Trying to do Operations on a UnInitialized GameObject");
	}

	return myIsInitialized;
}

#pragma once
#include "Vector.h"
#include "ObjectPool.h"
#include "Transform.h"
#include "ComponentSystem.h"

class CGameObjectData;

class CGameObject 
{
public:
	CGameObject();
	~CGameObject();

	virtual void Init();

	template<typename T>
	T* GetComponentPointer();

	template<typename T, typename ParameterPack>
	T* AddComponent(ParameterPack aParameterPack);

	virtual void Move(const CommonUtilities::Vector3f& aMovement);
	virtual void Rotate(const CommonUtilities::Vector3f& aRotation);
	virtual void SetPosition(const CommonUtilities::Vector3f& aPosition);

	///// TEMP
	const CommonUtilities::Vector3f GetForward();
	const CommonUtilities::Vector3f GetRight();
	const CommonUtilities::Vector3f GetUp();
	inline CTransform& GetTransform() { return ourComponentSystem->GetGameObjectData(myGameObjectDataID)->GetTransform(); }

	void Destroy();

private:
	friend CComponentSystem;
	static CComponentSystem* ourComponentSystem;

	ID_T(CGameObjectData) myGameObjectDataID;
	bool myIsInitialized;
	bool IsInitializedCheck();
};

template<typename T>
inline T* CGameObject::GetComponentPointer()
{
	return ourComponentSystem->GetGameObjectData(myGameObjectDataID)->GetComponentPointer<T>();
}

template<typename T, typename ParameterPack>
inline T* CGameObject::AddComponent(ParameterPack aParameterPack)
{
	return ourComponentSystem->AddComponent<T>(myGameObjectDataID, aParameterPack);
}

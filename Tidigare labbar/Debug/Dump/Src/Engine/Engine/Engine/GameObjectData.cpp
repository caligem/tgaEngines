#include "stdafx.h"
#include "GameObjectData.h"

CGameObjectData::CGameObjectData()
	:hasBeenInitialized(false)
{
}


CGameObjectData::~CGameObjectData()
{
}

void CGameObjectData::Init(ID_T(CGameObjectData) aGameObjectDataID)
{
	myID = aGameObjectDataID;
	myComponents.Init(4);
	hasBeenInitialized = true;
}

void CGameObjectData::Destroy()
{
	for (unsigned short i = 0; i < myComponents.Size(); ++i)
	{
		IEngine::GetComponentSystem()->AddComponentToDestroyQueue(CComponentSystem::SDestroyData(myID, myComponents[i].componentID, myComponents[i].componentType));
	}
}

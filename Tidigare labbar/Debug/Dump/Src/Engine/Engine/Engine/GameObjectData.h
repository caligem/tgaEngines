#pragma once
#include "GrowingArray.h"
#include "UniqueIdentifier.h" //fix later
#include "IEngine.h"
#include "ComponentSystem.h"
#include "Transform.h"

class CComponent;

class CGameObjectData
{
public:
	CGameObjectData();
	~CGameObjectData();

	struct SComponent
	{
		SComponent()
		{
			componentID = -1;
			componentType = -1;
		}

		SComponent(int aComponentID, _UUID_T aComponentType)
		{
			componentID = aComponentID;
			componentType = aComponentType;
		}

		int componentID;
		_UUID_T componentType;
	};

	void Init(ID_T(CGameObjectData) aGameObjectDataID);

	template<typename T>
	T* GetComponentPointer();

	template<typename T>
	void AddSComponent(ID_T(T) componentID, _UUID_T aUUID);

	inline CTransform& GetTransform() { return myTransform; }

	void Destroy();
private:
	ID_T(CGameObjectData) myID;
	bool hasBeenInitialized;
	CommonUtilities::GrowingArray<SComponent> myComponents;
	CTransform myTransform;
};

template<typename T>
inline T* CGameObjectData::GetComponentPointer()
{
	for (unsigned short i = 0; i < myComponents.Size(); ++i)
	{
		if (_UUID(T) != myComponents[i].componentType)
		{
			continue;
		}

		return IEngine::GetComponentSystem()->GetComponentPointer<T>(myComponents[i].componentID);
	}

	return nullptr;
}

template<typename T>
inline void CGameObjectData::AddSComponent(ID_T(T) aComponentIDptr, _UUID_T aUUID)
{
#ifdef _DEBUG
	if (!hasBeenInitialized)
	{
		ENGINE_LOG(CONCOL_ERROR, "Trying To Add Component Type %s to GameObjectData but it's uninitialized, forget to run Init on GameObject?", typeid(T).name());
		return;
	}

	for (unsigned short i = 0; i < myComponents.Size(); ++i)
	{
		if (myComponents[i].componentType == aUUID)
		{
			ENGINE_LOG(CONCOL_WARNING, "Trying to add %s to GameObject(ID: %d) that allready exists.", typeid(T).name(), myID);
		}
	}
#endif

	myComponents.Add(SComponent(aComponentIDptr.val, aUUID));
}

#include "stdafx.h"
#include "IEngine.h"
#include "Engine.h"

CEngine* IEngine::ourEngine = nullptr;

CEngine* IEngine::GetEngine()
{
	return ourEngine;
}

CDirectXFramework * IEngine::GetDXFramework()
{
	return &ourEngine->myFramework;
}

CComponentSystem* IEngine::GetComponentSystem()
{
	return &ourEngine->myComponentSystem;
}

CScene * IEngine::GetScene()
{
	return &ourEngine->myScene;
}

const CommonUtilities::Vector2f IEngine::GetWindowSize()
{
	return std::move(CommonUtilities::Vector2f(ourEngine->myCreateParameters.myCanvasWidth, ourEngine->myCreateParameters.myCanvasHeight));
}

ID3D11Device * IEngine::GetDevice()
{
	return ourEngine->myFramework.GetDevice();
}

ID3D11DeviceContext * IEngine::GetContext()
{
	return ourEngine->myFramework.GetContext();
}

CModelManager * IEngine::GetModelManager()
{
	return &ourEngine->myModelManager;
}

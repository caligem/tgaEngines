#pragma once

class CEngine;
class CDirectXFramework;
class CCameraComponent;
class CComponentSystem;
class CScene;
class IWorld;
class CForwardRenderer;
class CGameObjectData;
class CGameObject;
class ICreate;
class CTextureBuilder;
class CConstantBuffer;

class CModelManager;
struct ID3D11Device;
struct ID3D11DeviceContext;

class IEngine
{
private:
	//my friends can touch
	friend CEngine;
	friend CCameraComponent;
	friend CScene;
	friend CForwardRenderer;
	friend CGameObjectData;
	friend CGameObject;
	friend CTextureBuilder;
	friend CConstantBuffer;

	friend IWorld;
	friend ICreate;
	friend CModelManager;
	//my private parts
	static CEngine* GetEngine();
	static CDirectXFramework* GetDXFramework();
	static CComponentSystem* GetComponentSystem();
	static CScene* GetScene();

	static const CommonUtilities::Vector2f GetWindowSize();

	static ID3D11Device* GetDevice();
	static ID3D11DeviceContext* GetContext();
	static CModelManager* GetModelManager();
	IEngine() = delete;
	~IEngine() = delete;
	
	static CEngine *ourEngine;
};


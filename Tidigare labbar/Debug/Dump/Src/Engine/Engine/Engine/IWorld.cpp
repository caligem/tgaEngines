#include "stdafx.h"
#include "IWorld.h"

#include "Engine.h"
#include "IEngine.h"


void IWorld::GameObject()
{
	//TODO: Implement GameObjectCreation
}

CScene* IWorld::GetScene()
{
	return IEngine::GetScene();
}

const CommonUtilities::Vector2f IWorld::GetWindowSize()
{
	return std::move(IEngine::GetWindowSize());
}

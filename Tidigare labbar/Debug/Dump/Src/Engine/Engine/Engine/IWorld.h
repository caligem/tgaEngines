#pragma once

class CComponentSystem;
class CGameObjectData;
class CScene;
class CGameObject;

class IWorld
{
public:
	static void GameObject();
	static CScene* GetScene();
	static const CommonUtilities::Vector2f GetWindowSize();
private:
	IWorld() = delete;
	~IWorld() = delete;
};


#pragma once

enum D3D_PRIMITIVE_TOPOLOGY;
struct ID3D11InputLayout;

struct SLayoutDataWrapper
{
	D3D_PRIMITIVE_TOPOLOGY myPrimitiveTopology;
	ID3D11InputLayout* myInputLayout;
};

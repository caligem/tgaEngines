#include "stdafx.h"
#include "Minidump.h"

#include "Engine.h"
#include "ScreenGrab.h"

#include "DL_Debug.h"

#pragma warning(push)  
#pragma warning(disable: 4091)  
#include <Windows.h>
#include <DbgHelp.h>
#include <strsafe.h>
#include <sstream>
#include <iomanip>
#include <wincodec.h>
#include <fstream>
#pragma warning(pop)

CEngine* MiniDump::ourMinidumpEnginePtr = nullptr;

std::string wStrToStr(const std::wstring& aWstr)
{
	return std::string(aWstr.begin(), aWstr.end());
}
std::string GetUsername()
{
	DWORD dwBufferSize = MAX_PATH;
	WCHAR username[MAX_PATH];
	GetUserName(username, &dwBufferSize);
	return wStrToStr(username);
}

void MiniDump::CreateMiniDump(_EXCEPTION_POINTERS * aExceptionP)
{
	WCHAR cwdPath[MAX_PATH];
	WCHAR dumpDirectoryPath[MAX_PATH];
	WCHAR dumpFilePath[MAX_PATH];
	DWORD dwBufferSize = MAX_PATH;
	HANDLE hDumpFile;
	SYSTEMTIME stLocalTime;
	MINIDUMP_EXCEPTION_INFORMATION ExpParam;

	//Get local time and CWD
	GetLocalTime(&stLocalTime);
	GetCurrentDirectory(dwBufferSize, cwdPath);

	//Add CrashDumps folder to path and create it
	StringCchPrintf(dumpDirectoryPath, MAX_PATH, L"%s%s", cwdPath, L"\\CrashDumps");
	CreateDirectory(dumpDirectoryPath, NULL);

	//Create the folder for the current crash dump
	StringCchPrintf(dumpDirectoryPath, MAX_PATH, L"%s%s%04d%02d%02d-%02d%02d%02d", dumpDirectoryPath, L"\\Crash_",
		stLocalTime.wYear, stLocalTime.wMonth, stLocalTime.wDay,
		stLocalTime.wHour, stLocalTime.wMinute, stLocalTime.wSecond
	);
	CreateDirectory(dumpDirectoryPath, NULL);

	//Create the path and file for the dump file
	StringCchPrintf(dumpFilePath, MAX_PATH, L"%s\\minidump.dmp", dumpDirectoryPath);
	hDumpFile = CreateFile(dumpFilePath, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_WRITE | FILE_SHARE_READ, NULL, CREATE_ALWAYS, NULL, NULL);

	ExpParam.ThreadId = GetCurrentThreadId();
	ExpParam.ExceptionPointers = aExceptionP;
	ExpParam.ClientPointers = TRUE;

	if (!MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), hDumpFile, MiniDumpWithDataSegs, &ExpParam, NULL, NULL))
	{
		MessageBox(NULL, L"Failed to create dump file!", L"FATAL ERROR!", MB_OK);
	}
	else
	{
		std::wstring crashMessage = L"The program crashed!\n\n";
		crashMessage += L"Dump file created at:\n";
		crashMessage += dumpFilePath;
		crashMessage += L"\n\nPlease tell one of your programmers!";
		MessageBox(NULL, crashMessage.c_str(), L"FATAL ERROR!", MB_OK);
	}

	//CrashInformation.txt path
	std::wstring wCrashInformationPath(dumpDirectoryPath);
	std::string crashInformationPath(wCrashInformationPath.begin(), wCrashInformationPath.end());
	crashInformationPath += "\\CrashInformation.txt";

	//Get Creation date for executable
	WIN32_FILE_ATTRIBUTE_DATA attr;
	SYSTEMTIME localCreation;
	FILETIME creation;
	std::string exePath = wStrToStr(__wargv[0]);
	GetFileAttributesExA(exePath.c_str(), GetFileExInfoStandard, &attr);
	FileTimeToLocalFileTime(&attr.ftLastWriteTime, &creation);
	FileTimeToSystemTime(&creation, &localCreation);

	//Get Memory stats for the PC
	MEMORYSTATUS memStat;
	GlobalMemoryStatus(&memStat);
	double availableMemory = memStat.dwAvailPhys / 1024.0 / 1024.0 / 1024.0;
	double freeMemory = availableMemory * (memStat.dwMemoryLoad / 100.0);

	//Create StringStream to be written to CrashInformation.txt
	std::stringstream ss;
	ss << "User         :  " << GetUsername() << std::endl;
	ss << "Time         :  "
		<< stLocalTime.wYear << "/"
		<< stLocalTime.wMonth << "/"
		<< stLocalTime.wDay << " "
		<< stLocalTime.wHour << ":"
		<< stLocalTime.wMinute << ":"
		<< stLocalTime.wSecond << std::endl;
	ss << "Exe Creation :  "
		<< localCreation.wYear << "/"
		<< localCreation.wMonth << "/"
		<< localCreation.wDay << " "
		<< localCreation.wHour << ":"
		<< localCreation.wMinute << ":"
		<< localCreation.wSecond << std::endl << std::endl;

	ss.setf(std::ios::fixed);
	ss << "System Memory:  " << std::setprecision(1) << freeMemory << "GB / " << availableMemory << "GB" << std::endl;
	ss << std::setprecision(0);

	if (ourMinidumpEnginePtr != NULL)
	{
		ss << "CPU Usage    :  " << ourMinidumpEnginePtr->GetCPUUsageInternal() << "%" << std::endl;
		ss << "Memory Usage :  " << ourMinidumpEnginePtr->GetMemUsageInternal() << "MB" << std::endl;
	}

	//Print CrashInformation.txt data
	std::string output = ss.str();

	FILE* fp;
	fopen_s(&fp, crashInformationPath.c_str(), "w");
	fwrite(output.c_str(), sizeof(char), output.size(), fp);
	fclose(fp);

	// Save Screendump
	if (ourMinidumpEnginePtr != NULL)
	{
		if (ourMinidumpEnginePtr->myFramework.GetContext() != NULL &&
			ourMinidumpEnginePtr->myFramework.GetBackBuffer() != NULL)
		{
			std::wstring screendump = dumpDirectoryPath;
			screendump += L"\\Screendump.jpg";
			
			ID3D11Resource* resource = NULL;
			ourMinidumpEnginePtr->myFramework.GetBackBuffer()->GetResource(&resource);

			if (resource != NULL)
			{
				DirectX::SaveWICTextureToFile(
					ourMinidumpEnginePtr->myFramework.GetContext(), resource,
					GUID_ContainerFormatJpeg, screendump.c_str()
				);
			}

			resource->Release();
		}
	}

	std::string logsFolder = wStrToStr(dumpDirectoryPath) + "\\Logs\\";
	CreateDirectoryA(logsFolder.c_str(), NULL);

	for (std::string str : DL_Debug::Debug::GetInstance()->GetLogFilesAndDestroy())
	{
		std::ifstream src("Logs\\log_"+str+".log", std::ios::binary);
		std::ofstream dst(logsFolder + "log_" + str + ".log", std::ios::binary);
		dst << src.rdbuf();
	}

}

LONG WINAPI MiniDump::ExceptionFilterFunction(_EXCEPTION_POINTERS* aExceptionP)
{
	CreateMiniDump(aExceptionP);

	return EXCEPTION_EXECUTE_HANDLER;
}

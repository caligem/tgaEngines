#pragma once

struct _EXCEPTION_POINTERS;
class CEngine;

struct MiniDump
{
	static void CreateMiniDump(_EXCEPTION_POINTERS* aExceptionP);
	static long __stdcall ExceptionFilterFunction(_EXCEPTION_POINTERS* aExceptionP);

	static CEngine* ourMinidumpEnginePtr;
};
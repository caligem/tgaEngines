#include "stdafx.h"
#include "Model.h"

#include "DXMacros.h"
#include <d3d11.h>

CModel::CModel()
{
}

CModel::~CModel()
{
}

void CModel::Init(const SModelData & aModelData)
{
	myModelData = aModelData;
}

void CModel::Shutdown()
{
	SafeRelease(myModelData.myVertexData.myVertexBuffer);
	SafeRelease(myModelData.myShaderData.myVertexShader);
	SafeRelease(myModelData.myShaderData.myPixelShader);
	SafeRelease(myModelData.myLayoutData.myInputLayout);
	for (int i = 0; i < sizeof(myModelData.myTextureData.myTextures) / sizeof(myModelData.myTextureData.myTextures[0]); ++i)
	{
		SafeRelease(myModelData.myTextureData.myTextures[i]);
	}
}

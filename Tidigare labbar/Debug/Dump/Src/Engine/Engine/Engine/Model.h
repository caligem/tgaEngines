#pragma once

#include "VertexDataWrapper.h"
#include "ShaderDataWrapper.h"
#include "LayoutDataWrapper.h"
#include "TextureDataWrapper.h"

class CModel
{
public:
	CModel();
	~CModel();

	struct SModelData
	{
		SVertexDataWrapper myVertexData;
		SShaderDataWrapper myShaderData;
		SLayoutDataWrapper myLayoutData;
		STextureDataWrapper myTextureData;
	};

	void Init(const SModelData& aModelData);

	void Shutdown();

	const SVertexDataWrapper& GetVertexData() { return myModelData.myVertexData; }
	const SShaderDataWrapper& GetShaderData() { return myModelData.myShaderData; }
	SLayoutDataWrapper& GetLayoutData() { return myModelData.myLayoutData; }
	STextureDataWrapper& GetTextureData() { return myModelData.myTextureData; }

private:
	SModelData myModelData;
};


#include "stdafx.h"
#include "ModelComponent.h"
#include "ModelManager.h"

CModelManager* CModelComponent::ourModelManager = nullptr;

CModelComponent::CModelComponent()
	: myModelID(ID_T_INVALID(CModel))
{
}

CModelComponent::~CModelComponent()
{
}

const CommonUtilities::Vector3f CModelComponent::GetPosition() const
{
	return{ myTransform[12], myTransform[13], myTransform[14] };
}

const CommonUtilities::Matrix44f CModelComponent::GetTransform() const
{
	return myTransform;
}

bool CModelComponent::Init(const char* aFilePath)
{
	ID_T(CModel) modelID = ourModelManager->AcquireModel(aFilePath);
	myModelID = modelID;

	return true;
}

void CModelComponent::Move(const CommonUtilities::Vector3f & aMovement)
{
	myTransform[12] += aMovement.x;
	myTransform[13] += aMovement.y;
	myTransform[14] += aMovement.z;
}

void CModelComponent::Rotate(const CommonUtilities::Vector3f & aRotation)
{
	float x, y, z;
	x = myTransform[12];
	y = myTransform[13];
	z = myTransform[14];

	myTransform[12] = myTransform[13] = myTransform[14] = 0.f;

	myTransform *= CommonUtilities::Matrix44f::CreateRotateAroundZ(aRotation.z);
	myTransform *= CommonUtilities::Matrix44f::CreateRotateAroundX(aRotation.x);
	myTransform *= CommonUtilities::Matrix44f::CreateRotateAroundY(aRotation.y);
	myTransform[12] = x;
	myTransform[13] = y;
	myTransform[14] = z;
}

void CModelComponent::SetPosition(const CommonUtilities::Vector3f & aPosition)
{
	myTransform[12] = aPosition.x;
	myTransform[13] = aPosition.y;
	myTransform[14] = aPosition.z;
}

void CModelComponent::Scale(const CommonUtilities::Vector3f & aScale)
{
	CommonUtilities::Vector3f right = (CommonUtilities::Vector3f(1.f, 0.f, 0.f)*CommonUtilities::Matrix33f(myTransform));
	CommonUtilities::Vector3f up = (CommonUtilities::Vector3f(0.f, 1.f, 0.f)*CommonUtilities::Matrix33f(myTransform));
	CommonUtilities::Vector3f forward = (CommonUtilities::Vector3f(0.f, 0.f, 1.f)*CommonUtilities::Matrix33f(myTransform));

	myTransform[0] = right.x * aScale.x;
	myTransform[1] = right.y * aScale.x;
	myTransform[2] = right.z * aScale.x;
	myTransform[4] = up.x * aScale.y;
	myTransform[5] = up.y * aScale.y;
	myTransform[6] = up.z * aScale.y;
	myTransform[8] = forward.x * aScale.z;
	myTransform[9] = forward.y * aScale.z;
	myTransform[10] = forward.z * aScale.z;
}

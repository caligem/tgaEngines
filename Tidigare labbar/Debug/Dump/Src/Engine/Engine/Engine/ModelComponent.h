#pragma once

#include "Matrix.h"
#include "Vector.h"
#include "Component.h"

class CModel;
class CForwardRenderer;
class CEngine;
class CModelManager;

class CModelComponent : public CComponent
{
public:
	CModelComponent();
	~CModelComponent();

	bool Init(const char* aFilePath);

	const CommonUtilities::Matrix44f GetTransform() const;
	const CommonUtilities::Vector3f GetPosition() const;

	void Move(const CommonUtilities::Vector3f& aMovement);
	void Rotate(const CommonUtilities::Vector3f& aRotation);
	void SetPosition(const CommonUtilities::Vector3f& aPosition);
	void Scale(const CommonUtilities::Vector3f& aScale);

private:
	friend CForwardRenderer;
	friend CEngine;

	static CModelManager* ourModelManager;

	CommonUtilities::Matrix44<float> myTransform;
	ID_T(CModel) myModelID;
};


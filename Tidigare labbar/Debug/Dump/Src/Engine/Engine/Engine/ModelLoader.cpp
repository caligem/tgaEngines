#include "stdafx.h"
#include "ModelLoader.h"

#include <d3d11.h>
#include <fstream>

#include "DDSTextureLoader.h"
#include "FBXLoaderCustom.h"
#include "TextureBuilder.h"

CModelLoader::CModelLoader()
{
}

CModelLoader::~CModelLoader()
{
}

bool CModelLoader::Init(ID3D11Device * aDevice)
{
	if (aDevice == nullptr)
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: DirectX Device was nullptr when loading Model.");
		return false;
	}

	myDevice = aDevice;
	myLoader = new CFBXLoaderCustom();

	myDefaultTextures[EDefaultTexture_White] = CTextureBuilder::CreateWhiteTexture();
	myDefaultTextures[EDefaultTexture_Black] = CTextureBuilder::CreateBlackTexture();
	myDefaultTextures[EDefaultTexture_GrayCheckerBoard] = CTextureBuilder::CreateGrayCheckerBoardTexture();
	myDefaultTextures[EDefaultTexture_PinkCheckerBoard] = CTextureBuilder::CreatePinkCheckerBoardTexture();
	myDefaultTextures[EDefaultTexture_Normal] = CTextureBuilder::CreateNormalTexture();

	return true;
}

CModel::SModelData CModelLoader::LoadModel(const char * aModelPath)
{
	START_TIMER(LOAD_MODEL);

	//Loading model
	CModel::SModelData modelData;
	CLoaderModel* loaderModel = myLoader->LoadModel(aModelPath);

	if (loaderModel == nullptr)
	{
		RESOURCE_LOG(CONCOL_ERROR, "ERROR: Failed to load model: %s", aModelPath);
		return LoadCube();
	}

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = loaderModel->myMeshes[0]->myVertexBufferSize * loaderModel->myMeshes[0]->myVertexCount;
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = loaderModel->myMeshes[0]->myVerticies;

	HRESULT result;
	result = myDevice->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &modelData.myVertexData.myVertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = static_cast<unsigned int>(loaderModel->myMeshes[0]->myIndexes.size() * sizeof(unsigned int));
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = &loaderModel->myMeshes[0]->myIndexes[0];

	result = myDevice->CreateBuffer(&indexBufferDesc, &indexBufferData, &modelData.myVertexData.myIndexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
	}

	//Load shaders
	std::ifstream vsFile;
	vsFile.open("Shaders/VS_Model.cso", std::ios::binary);
	std::string vsData = { std::istreambuf_iterator<char>(vsFile), std::istreambuf_iterator<char>() };
	ID3D11VertexShader* vertexShader;
	result = myDevice->CreateVertexShader(vsData.data(), vsData.size(), nullptr, &vertexShader);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex shader!");
	}
	vsFile.close();

	std::ifstream psFile;
	psFile.open("Shaders/PS_Model.cso", std::ios::binary);
	std::string psData = { std::istreambuf_iterator<char>(psFile), std::istreambuf_iterator<char>() };
	ID3D11PixelShader* pixelShader;
	result = myDevice->CreatePixelShader(psData.data(), psData.size(), nullptr, &pixelShader);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create pixel shader!");
	}
	psFile.close();

	//Create Input Layout
	D3D11_INPUT_ELEMENT_DESC layout[5] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "UV", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	ID3D11InputLayout* inputLayout;
	result = myDevice->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), vsData.data(), vsData.size(), &inputLayout);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create input layout!");
	}

	//Load Texture
	std::string filepath(aModelPath);
 
	size_t split = filepath.find_last_of("\\/");
	std::string path = filepath.substr(0, split + 1);
	std::string name = filepath.substr(split + 1);

	// Picks out the albedo, rough, ao, normal and metallic maps from the texture list
	const int numMaps = 5;
	int mapsToPick[numMaps] = {
		0, 1, 2, 5, 10
	};
	const char* mapsToPickNames[numMaps] =
	{
		"albedo",
		"roughness",
		"AO",
		"normal",
		"metalness"
	};
	if (!loaderModel->myTextures.empty())
	{
		for (int i = 0; i < numMaps; ++i)
		{
			if (loaderModel->myTextures[i].empty())
			{
				RESOURCE_LOG(CONCOL_WARNING, "Missing %s texture for model %s", mapsToPickNames[i], aModelPath);
				if (i == 0)
				{
					modelData.myTextureData.myTextures[i] = LoadDefaultTexture(-1);
				}
				else
				{
					modelData.myTextureData.myTextures[i] = LoadDefaultTexture(i);
				}
			}
			else
			{
				std::wstring textureFilePath(path.begin(), path.end());
				textureFilePath.append(loaderModel->myTextures[i].begin(), loaderModel->myTextures[i].end());

				ID3D11ShaderResourceView* shaderResourceView = NULL;
				result = DirectX::CreateDDSTextureFromFile(myDevice, textureFilePath.c_str(), nullptr, &shaderResourceView);
				if (FAILED(result))
				{
					RESOURCE_LOG(CONCOL_ERROR, "ERROR: Failed to load %s texture: %s | %s!", mapsToPickNames[i], loaderModel->myTextures[i].c_str(), aModelPath);
					shaderResourceView = LoadDefaultTexture(i);
				}
				modelData.myTextureData.myTextures[i] = shaderResourceView;
			}
		}
	}
	else
	{
		RESOURCE_LOG(CONCOL_ERROR, "Found no textures for model: %s", aModelPath);
	}

	modelData.myVertexData.myNumberOfIndices = static_cast<unsigned int>(loaderModel->myMeshes[0]->myIndexes.size());
	modelData.myVertexData.myNumberOfVertices = loaderModel->myMeshes[0]->myVertexCount;
	modelData.myVertexData.myStride = loaderModel->myMeshes[0]->myVertexBufferSize;
	modelData.myVertexData.myOffset = 0;
	modelData.myShaderData.myVertexShader = vertexShader;
	modelData.myShaderData.myPixelShader = pixelShader;
	modelData.myLayoutData.myPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	modelData.myLayoutData.myInputLayout = inputLayout;

	unsigned long long time = GET_TIME(LOAD_MODEL);
	if (time <= 20)
	{
		RESOURCE_LOG(CONCOL_VALID, "Model %s took %llums to load!", aModelPath, time)
	}
	else if (time < 100)
	{
		RESOURCE_LOG(CONCOL_WARNING, "Model %s took %llums to load!", aModelPath, time)
	}
	else
	{
		RESOURCE_LOG(CONCOL_ERROR, "Model %s took %llums to load!", aModelPath, time)
	}

	return modelData;
}

CModel::SModelData CModelLoader::LoadCube()
{
	START_TIMER(LOAD_MODEL)

		//Load vertex data
		struct Vertex
	{
		float x, y, z, w;
		float nX, nY, nZ, nW; //normal
		float tX, tY, tZ, tW; //tangent
		float bX, bY, bZ, bW; //binormal
		float u, v;
	} vertices[24] =
	{
		{0.5, -0.5, 0.5, 1.f,   -0, 0, 1, 1.f,  1, -0, -0, 1.f, 0, 1, -0, 1.f,  0.f, 0.f},
		{-0.5, -0.5, 0.5, 1.f,  -0, 0, 1, 1.f,  1, -0, 0, 1.f,  0, 1, -0, 1.f,  1.f, 0.f},
		{0.5, 0.5, 0.5, 1.f,    -0, 0, 1, 1.f,  1, -0, 0, 1.f,  0, 1, -0, 1.f,  0.f, 1.f},
		{-0.5, 0.5, 0.5, 1.f,   -0, 0, 1, 1.f,  1, -0, 0, 1.f,  0, 1, -0, 1.f,  1.f, 1.f},
		{0.5, 0.5, 0.5, 1.f,    -0, 1, 0, 1.f,  1, -0, 0, 1.f,  0, 0, -1, 1.f,  0.f, 0.f},
		{-0.5, 0.5, 0.5, 1.f,   -0, 1, 0, 1.f,  1, 0, 0, 1.f,   0, 0, -1, 1.f,  1.f, 0.f},
		{0.5, 0.5, -0.5, 1.f,   -0, 1, 0, 1.f,  1, 0, 0, 1.f,   0, 0, -1, 1.f,  0.f, 1.f},
		{-0.5, 0.5, -0.5, 1.f,  -0, 1, 0, 1.f,  1, 0, 0, 1.f,   0, 0, -1, 1.f,  1.f, 1.f},
		{0.5, 0.5, -0.5, 1.f,   -0, 0, -1, 1.f, 1, 0, 0, 1.f,   0, -1, 0, 1.f,  0.f, 0.f},
		{-0.5, 0.5, -0.5, 1.f,  -0, 0, -1, 1.f, 1, 0, 0, 1.f,   0, -1, 0, 1.f,  1.f, 0.f},
		{0.5, -0.5, -0.5, 1.f,  -0, 0, -1, 1.f, 1, 0, 0, 1.f,   0, -1, 0, 1.f,  0.f, 1.f},
		{-0.5, -0.5, -0.5, 1.f, -0, 0, -1, 1.f, 1, 0, 0, 1.f,   0, -1, 0, 1.f,  1.f, 1.f},
		{0.5, -0.5, -0.5, 1.f,  -0, -1, 0, 1.f, 1, 0, -0, 1.f,  0, 0, 1, 1.f,   0.f, 0.f},
		{-0.5, -0.5, -0.5, 1.f, -0, -1, 0, 1.f, 1, 0, -0, 1.f,  0, 0, 1, 1.f,   1.f, 0.f},
		{0.5, -0.5, 0.5, 1.f,   -0, -1, 0, 1.f, 1, 0, -0, 1.f,  0, 0, 1, 1.f,   0.f, 1.f},
		{-0.5, -0.5, 0.5, 1.f,  -0, -1, 0, 1.f, 1, 0, -0, 1.f,  0, 0, 1, 1.f,   1.f, 1.f},
		{-0.5, -0.5, 0.5, 1.f,  -1, 0, 0, 1.f,  0, 0, -1, 1.f,  -0, 1, 0, 1.f,  0.f, 0.f},
		{-0.5, -0.5, -0.5, 1.f, -1, 0, 0, 1.f,  0, 0, -1, 1.f,  -0, 1, 0, 1.f,  1.f, 0.f},
		{-0.5, 0.5, 0.5, 1.f,   -1, 0, 0, 1.f,  0, 0, -1, 1.f,  -0, 1, 0, 1.f,  0.f, 1.f},
		{-0.5, 0.5, -0.5, 1.f,  -1, 0, 0, 1.f,  0, -0, -1, 1.f, 0, 1, -0, 1.f,  1.f, 1.f},
		{0.5, -0.5, -0.5, 1.f,  1, 0, 0, 1.f,   0, -0, 1, 1.f,  0, 1, 0, 1.f,   0.f, 0.f},
		{0.5, -0.5, 0.5, 1.f,   1, 0, 0, 1.f,   0, -0, 1, 1.f,  0, 1, 0, 1.f,   1.f, 0.f},
		{0.5, 0.5, -0.5, 1.f,   1, 0, 0, 1.f,   0, -0, 1, 1.f,  0, 1, 0, 1.f,   0.f, 1.f},
		{0.5, 0.5, 0.5, 1.f,    1, 0, 0, 1.f,   0, -0, 1, 1.f,  0, 1, 0, 1.f,   1.f, 1.f}
	};
	unsigned int indices[36] =
	{
		2,1,0,
		3,1,2,
		6,5,4,
		7,5,6,
		10,9,8,
		11,9,10,
		14,13,12,
		15,13,14,
		18,17,16,
		19,17,18,
		22,21,20,
		23,21,22
	};

	//Creating Vertex Buffer
	D3D11_BUFFER_DESC vertexBufferDesc = { 0 };
	vertexBufferDesc.ByteWidth = sizeof(vertices);
	vertexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;

	D3D11_SUBRESOURCE_DATA vertexBufferData = { 0 };
	vertexBufferData.pSysMem = vertices;

	ID3D11Buffer* vertexBuffer;

	HRESULT result;
	result = myDevice->CreateBuffer(&vertexBufferDesc, &vertexBufferData, &vertexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex buffer!");
	}

	//Creating Index Buffer
	D3D11_BUFFER_DESC indexBufferDesc = { 0 };
	indexBufferDesc.ByteWidth = sizeof(indices);
	indexBufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;

	D3D11_SUBRESOURCE_DATA indexBufferData = { 0 };
	indexBufferData.pSysMem = indices;

	ID3D11Buffer* indexBuffer;

	result = myDevice->CreateBuffer(&indexBufferDesc, &indexBufferData, &indexBuffer);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create index buffer!");
	}

	//Load shaders
	std::ifstream vsFile;
	vsFile.open("Shaders/VS_Model.cso", std::ios::binary);
	std::string vsData = { std::istreambuf_iterator<char>(vsFile), std::istreambuf_iterator<char>() };
	ID3D11VertexShader* vertexShader;
	result = myDevice->CreateVertexShader(vsData.data(), vsData.size(), nullptr, &vertexShader);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create vertex shader!");
	}
	vsFile.close();

	std::ifstream psFile;
	psFile.open("Shaders/PS_Model.cso", std::ios::binary);
	std::string psData = { std::istreambuf_iterator<char>(psFile), std::istreambuf_iterator<char>() };
	ID3D11PixelShader* pixelShader;
	result = myDevice->CreatePixelShader(psData.data(), psData.size(), nullptr, &pixelShader);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create pixel shader!");
	}
	psFile.close();

	//Create Layout
	D3D11_INPUT_ELEMENT_DESC layout[5] =
	{
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "TANGENT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "BINORMAL", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "UV", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};

	ID3D11InputLayout* inputLayout;
	result = myDevice->CreateInputLayout(layout, sizeof(layout) / sizeof(D3D11_INPUT_ELEMENT_DESC), vsData.data(), vsData.size(), &inputLayout);
	if (FAILED(result))
	{
		ENGINE_LOG(CONCOL_ERROR, "ERROR: Failed to create input layout!");
	}

	CModel::SModelData modelData;

	for (int i = 0; i < 5; ++i)
	{
		ID3D11ShaderResourceView* shaderResourceView = NULL;
		shaderResourceView = LoadDefaultTexture(i);
		modelData.myTextureData.myTextures[i] = shaderResourceView;
	}

	//Setup ModelData
	modelData.myVertexData.myNumberOfVertices = sizeof(vertices) / sizeof(Vertex);
	modelData.myVertexData.myNumberOfIndices = sizeof(indices) / sizeof(unsigned int);
	modelData.myVertexData.myStride = sizeof(Vertex);
	modelData.myVertexData.myOffset = 0;
	modelData.myVertexData.myVertexBuffer = vertexBuffer;
	modelData.myVertexData.myIndexBuffer = indexBuffer;
	modelData.myShaderData.myVertexShader = vertexShader;
	modelData.myShaderData.myPixelShader = pixelShader;
	modelData.myLayoutData.myPrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	modelData.myLayoutData.myInputLayout = inputLayout;

	unsigned long long time = GET_TIME(LOAD_MODEL);
	if (time <= 20)
	{
		RESOURCE_LOG(CONCOL_VALID, "Model %s took %llums to load!", "Cube", time)
	}
	else if (time < 100)
	{
		RESOURCE_LOG(CONCOL_WARNING, "Model %s took %llums to load!", "Cube", time)
	}
	else
	{
		RESOURCE_LOG(CONCOL_ERROR, "Model %s took %llums to load!", "Cube", time)
	}

	return modelData;
}

ID3D11ShaderResourceView* CModelLoader::LoadDefaultTexture(int i)
{
	switch (i)
	{
	case 0:
		return myDefaultTextures[EDefaultTexture_GrayCheckerBoard];	// albedo
		break;
	case 1:
		return myDefaultTextures[EDefaultTexture_Black];			// roughness
		break;
	case 2:
		return myDefaultTextures[EDefaultTexture_White];			// ao
		break;
	case 3:
		return myDefaultTextures[EDefaultTexture_Normal];			// normal
		break;
	case 4:
		return myDefaultTextures[EDefaultTexture_Black];			// metalness
		break;
	default:
		return myDefaultTextures[EDefaultTexture_PinkCheckerBoard];	// default, error
		break;
	}
}

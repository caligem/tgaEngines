#pragma once

#include "Model.h"

#include <map>

struct ID3D11Device;
class CFBXLoaderCustom;

class CModelLoader
{
public:
	CModelLoader();
	~CModelLoader();

	bool Init(ID3D11Device* aDevice);
	CModel::SModelData LoadModel(const char* aModelPath);
	CModel::SModelData LoadCube();
	ID3D11ShaderResourceView* LoadDefaultTexture(int i);

private:
	enum EDefaultTexture
	{
		EDefaultTexture_White,
		EDefaultTexture_Black,
		EDefaultTexture_GrayCheckerBoard,
		EDefaultTexture_PinkCheckerBoard,
		EDefaultTexture_Normal,
		EDefaultTexture_Count
	};

	ID3D11ShaderResourceView* myDefaultTextures[EDefaultTexture_Count];

	ID3D11Device* myDevice;
	CFBXLoaderCustom* myLoader;

};


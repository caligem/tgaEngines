#include "stdafx.h"
#include "ModelManager.h"
#include "DirectXFramework.h"

CModelManager::CModelManager()
{
}


CModelManager::~CModelManager()
{
}

bool CModelManager::Init(CDirectXFramework& aDirectXFramework)
{
	bool result = myModelLoader.Init(aDirectXFramework.GetDevice());

	if (!result)
	{
		ENGINE_LOG(CONCOL_ERROR, "FATAL ERROR: ModelLoader failed to initialize in ComponentSystem Init.");
		return false;
	}
	return true;
}

ID_T(CModel) CModelManager::AcquireModel(const char * aModelPath)
{
	std::string modelPath;
	if (aModelPath == nullptr)
	{
		modelPath = "";
	}
	else
	{
		modelPath = aModelPath;
	}

	if (myModelCache.find(aModelPath) != myModelCache.end())
	{
		return myModelCache[aModelPath];
	}
	else
	{
		ID_T(CModel) modelID = myModels.Acquire();
		myModelCache[aModelPath] = modelID;
		CModel::SModelData modelData;
		if (aModelPath == nullptr)
		{
			modelData = myModelLoader.LoadCube();
		}
		else
		{
			modelData = myModelLoader.LoadModel(aModelPath);
		}
		myModels.GetObj(modelID)->Init(modelData);

		return std::move(modelID);
	}
}

CModel * CModelManager::GetModel(ID_T(CModel) aModelID)
{
	return std::move(myModels.GetObj(aModelID));
}

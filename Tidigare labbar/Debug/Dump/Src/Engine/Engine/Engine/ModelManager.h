#pragma once
#include "ModelLoader.h"

class CDirectXFramework;
class CForwardRenderer;

class CModelManager
{
public:
	CModelManager();
	~CModelManager();

	bool Init(CDirectXFramework& aDirectXFramework);
	ID_T(CModel) AcquireModel(const char* aModelPath = nullptr);

private:
	friend CForwardRenderer;

	CModel* GetModel(ID_T(CModel) aModelID);
	std::map<std::string, ID_T(CModel)> myModelCache;
	ObjectPool<CModel> myModels;
	CModelLoader myModelLoader;
};


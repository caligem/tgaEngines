#include "CS_Model.hlsli"

Texture2D albedoTexture : register(t0);
Texture2D roughTexture : register(t1);
Texture2D aoTexture : register(t2);
Texture2D normalTexture : register(t3);
Texture2D metallicTexture : register(t4);
TextureCube environmentTexture : register(t5);

SamplerState instanceSampler : register(s0);

cbuffer LightData : register(b0)
{
	float4 directionalLightDirection;
	float4 directionalLightColor;
}

PixelOutput main(PixelInput input)
{
	PixelOutput output;
	
	float3 normalColor = normalTexture.Sample(instanceSampler, input.myUV.xy).xyz;
	normalColor = (normalColor * 2.f) - 1.f;

	float3 normal = normalize(normalColor.x * input.myTangent.xyz + normalColor.y * input.myBinormal.xyz + normalColor.z * input.myNormal.xyz);

	float3 albedoColor = albedoTexture.Sample(instanceSampler, input.myUV.xy).rgb;
	float3 roughColor = roughTexture.Sample(instanceSampler, input.myUV.xy).rgb;
	float3 environmentColor = environmentTexture.SampleLevel(instanceSampler, float3(normal.x, normal.y, -normal.z), 8.f*pow((1.f-roughColor.r), 2.f)).rgb;
	float3 aoColor = aoTexture.Sample(instanceSampler, input.myUV.xy).rgb;

	float attenuation = saturate(dot(-directionalLightDirection.xyz, normal));

	float3 ambient = albedoColor * environmentColor;
	float3 diffuse = albedoColor * directionalLightColor.rgb * attenuation;

	float3 col = (ambient + diffuse) * aoColor;


	output.myColor = float4(col, 1.f);

	return output;
}
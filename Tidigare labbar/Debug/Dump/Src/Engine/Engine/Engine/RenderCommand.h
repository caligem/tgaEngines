#pragma once

#include "Matrix.h"
#include "ObjectPool.h"
class CModel;

struct SRenderCommand
{
	CommonUtilities::Matrix44f myTransform;
	ID_T(CModel) myModelToRender;
};
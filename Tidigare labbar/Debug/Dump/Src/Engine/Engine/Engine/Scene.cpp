#include "stdafx.h"
#include "Scene.h"
#include "IEngine.h"
#include "ComponentSystem.h"

CScene::CScene()
	: myActiveCamera(ID_T_INVALID(CCameraComponent))
{
}

CScene::~CScene()
{
}

void CScene::Init()
{
	myModelComponents.Init(2);
	myCameraComponents.Init(2);

	myDirectionalLight = { -1.f, -1.f, 1.f };
	myDirectionalLight.Normalize();
}

void CScene::AddModelComponent(ID_T(CModelComponent) aModelInstanceID)
{
	myModelComponents.Add(aModelInstanceID);
}

void CScene::AddCameraComponent(ID_T(CCameraComponent) aCameraInstanceID)
{
	myCameraComponents.Add(aCameraInstanceID);
}

void CScene::GetModelsFromActiveScene(CommonUtilities::GrowingArray<ID_T(CModelComponent)>& aBuffer)
{
	aBuffer = myModelComponents;
}

CCameraComponent* CScene::GetActiveCamera()
{
	if (myActiveCamera == ID_T_INVALID(CCameraComponent))
	{
		ENGINE_LOG(CONCOL_WARNING, "Trying to Get Active Camera, but it's INVALID.");
	}

	return static_cast<CCameraComponent*>(IEngine::GetComponentSystem()->GetComponentPointer<CCameraComponent>(myActiveCamera.val));
}

void CScene::SetActiveCamera(ID_T(CCameraComponent) aCameraInstanceID)
{
	for (ID_T(CCameraComponent) cameraInstanceID : myCameraComponents)
	{
		if (aCameraInstanceID == cameraInstanceID)
		{
			myActiveCamera = aCameraInstanceID;
			return;
		}
	}
	
	ENGINE_LOG(CONCOL_ERROR, "Trying to set an active camera but it doesnt exist in scene.");
}

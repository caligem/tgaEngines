#pragma once

#include <GrowingArray.h>
#include "ObjectPool.h"
#include "Vector.h"
#include "UniqueIdentifier.h"

class CModelComponent;
class CCameraComponent;

class CScene
{
public:
	CScene();
	~CScene();

	void Init();
	void AddModelComponent(ID_T(CModelComponent) aModelInstance);
	void AddCameraComponent(ID_T(CCameraComponent) aCameraInstance);

	void GetModelsFromActiveScene(CommonUtilities::GrowingArray<ID_T(CModelComponent)>& aBuffer);
	CCameraComponent* GetActiveCamera();
	void SetActiveCamera(ID_T(CCameraComponent) aCameraInstance);

	void SetDirectionalLight(const CommonUtilities::Vector3f& aDirection) { myDirectionalLight = aDirection; }
	const CommonUtilities::Vector3f& GetDirectionalLight() const { return myDirectionalLight; }

	template<typename T>
	void RemoveComponent(int aID);

private:
	CommonUtilities::GrowingArray<ID_T(CModelComponent)> myModelComponents;
	CommonUtilities::GrowingArray<ID_T(CCameraComponent)> myCameraComponents;
	ID_T(CCameraComponent) myActiveCamera;

	CommonUtilities::Vector3f myDirectionalLight;
};

template<typename T>
inline void CScene::RemoveComponent(int aID)
{
	if (_UUID(T) == _UUID(CModelComponent))
	{
		for (unsigned short i = 0; i < myModelComponents.Size(); ++i)
		{
			if (myModelComponents[i] == aID)
			{
				myModelComponents.RemoveAtIndex(i);
			}
		}
	}
	else if (_UUID(T) == _UUID(CCameraComponent))
	{
		for (unsigned short i = 0; i < myCameraComponents.Size(); ++i)
		{
			if (myCameraComponents[i] == aID)
			{
				myCameraComponents.RemoveAtIndex(i);
			}
		}
	}
}

#pragma once

struct ID3D11VertexShader;
struct ID3D11PixelShader;

struct SShaderDataWrapper
{
	ID3D11VertexShader* myVertexShader;
	ID3D11PixelShader* myPixelShader;
};

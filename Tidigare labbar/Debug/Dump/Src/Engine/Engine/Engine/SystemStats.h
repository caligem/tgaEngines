#pragma once

typedef struct _FILETIME FILETIME;

float GetCPUUsage (
	FILETIME *prevSysKernel,
	FILETIME *prevSysUser,
	FILETIME *prevProcKernel,
	FILETIME *prevProcUser,
	bool firstRun = false
);

int GetMemUsage();

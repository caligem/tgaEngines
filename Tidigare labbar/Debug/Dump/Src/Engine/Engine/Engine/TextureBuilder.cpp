#include "stdafx.h"
#include "TextureBuilder.h"

#include <d3d11.h>
#include "IEngine.h"

ID3D11ShaderResourceView * CTextureBuilder::CreatePinkCheckerBoardTexture()
{
	HRESULT hr;

    ID3D11Texture2D *tex;
    D3D11_TEXTURE2D_DESC tdesc;
    D3D11_SUBRESOURCE_DATA tbsd;

    int h = 128;
    int w = 128;
    int *buf = new int[h*w];
    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < w; j++)
        {
			if ((i/16 + j/16) % 2 == 0)
			{
				buf[i*w + j] = 0xffffaacc;
			}
			else
			{
				buf[i*w + j] = 0xffee99bb;
			}
        }
    }

    tbsd.pSysMem = (void *)buf;
    tbsd.SysMemPitch = w * 4;
    tbsd.SysMemSlicePitch = w*h * 4; // Not needed since this is a 2d texture

    tdesc.Width = w;
    tdesc.Height = h;
    tdesc.MipLevels = 1;
    tdesc.ArraySize = 1;

    tdesc.SampleDesc.Count = 1;
    tdesc.SampleDesc.Quality = 0;
    tdesc.Usage = D3D11_USAGE_DEFAULT;
    tdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

    tdesc.CPUAccessFlags = 0;
    tdesc.MiscFlags = 0;

	ID3D11Device* device = IEngine::GetDevice();

    if (FAILED(device->CreateTexture2D(&tdesc, &tbsd, &tex)))
        return nullptr;

    delete[] buf;

    ID3D11ShaderResourceView* resource = nullptr;
    hr = device->CreateShaderResourceView(tex, NULL, &resource);
    if (FAILED(hr))
    {
        return nullptr;
    }
    tex->Release();
    return resource;
}

ID3D11ShaderResourceView * CTextureBuilder::CreateWhiteTexture()
{
	HRESULT hr;

    ID3D11Texture2D *tex;
    D3D11_TEXTURE2D_DESC tdesc;
    D3D11_SUBRESOURCE_DATA tbsd;

    int h = 4;
    int w = 4;
    int *buf = new int[h*w];
    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < w; j++)
        {
            buf[i*w + j] = 0xffffffff;
        }
    }

    tbsd.pSysMem = (void *)buf;
    tbsd.SysMemPitch = w * 4;
    tbsd.SysMemSlicePitch = w*h * 4; // Not needed since this is a 2d texture

    tdesc.Width = w;
    tdesc.Height = h;
    tdesc.MipLevels = 1;
    tdesc.ArraySize = 1;

    tdesc.SampleDesc.Count = 1;
    tdesc.SampleDesc.Quality = 0;
    tdesc.Usage = D3D11_USAGE_DEFAULT;
    tdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

    tdesc.CPUAccessFlags = 0;
    tdesc.MiscFlags = 0;

	ID3D11Device* device = IEngine::GetDevice();

    if (FAILED(device->CreateTexture2D(&tdesc, &tbsd, &tex)))
        return nullptr;

    delete[] buf;

    ID3D11ShaderResourceView* resource = nullptr;
    hr = device->CreateShaderResourceView(tex, NULL, &resource);
    if (FAILED(hr))
    {
        return nullptr;
    }
    tex->Release();
    return resource;
}

ID3D11ShaderResourceView * CTextureBuilder::CreateBlackTexture()
{
	HRESULT hr;

    ID3D11Texture2D *tex;
    D3D11_TEXTURE2D_DESC tdesc;
    D3D11_SUBRESOURCE_DATA tbsd;

    int h = 4;
    int w = 4;
    int *buf = new int[h*w];
    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < w; j++)
        {
			buf[i*w + j] = 0;
        }
    }

    tbsd.pSysMem = (void *)buf;
    tbsd.SysMemPitch = w * 4;
    tbsd.SysMemSlicePitch = w*h * 4; // Not needed since this is a 2d texture

    tdesc.Width = w;
    tdesc.Height = h;
    tdesc.MipLevels = 1;
    tdesc.ArraySize = 1;

    tdesc.SampleDesc.Count = 1;
    tdesc.SampleDesc.Quality = 0;
    tdesc.Usage = D3D11_USAGE_DEFAULT;
    tdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

    tdesc.CPUAccessFlags = 0;
    tdesc.MiscFlags = 0;

	ID3D11Device* device = IEngine::GetDevice();

    if (FAILED(device->CreateTexture2D(&tdesc, &tbsd, &tex)))
        return nullptr;

    delete[] buf;

    ID3D11ShaderResourceView* resource = nullptr;
    hr = device->CreateShaderResourceView(tex, NULL, &resource);
    if (FAILED(hr))
    {
        return nullptr;
    }
    tex->Release();
    return resource;
}

ID3D11ShaderResourceView * CTextureBuilder::CreateGrayCheckerBoardTexture()
{
	HRESULT hr;

    ID3D11Texture2D *tex;
    D3D11_TEXTURE2D_DESC tdesc;
    D3D11_SUBRESOURCE_DATA tbsd;

    int h = 128;
    int w = 128;
    int *buf = new int[h*w];
    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < w; j++)
        {
			if ((i/16 + j/16) % 2 == 0)
			{
				buf[i*w + j] = 0xff555555;
			}
			else
			{
				buf[i*w + j] = 0xff999999;
			}
        }
    }

    tbsd.pSysMem = (void *)buf;
    tbsd.SysMemPitch = w * 4;
    tbsd.SysMemSlicePitch = w*h * 4; // Not needed since this is a 2d texture

    tdesc.Width = w;
    tdesc.Height = h;
    tdesc.MipLevels = 1;
    tdesc.ArraySize = 1;

    tdesc.SampleDesc.Count = 1;
    tdesc.SampleDesc.Quality = 0;
    tdesc.Usage = D3D11_USAGE_DEFAULT;
    tdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

    tdesc.CPUAccessFlags = 0;
    tdesc.MiscFlags = 0;

	ID3D11Device* device = IEngine::GetDevice();

    if (FAILED(device->CreateTexture2D(&tdesc, &tbsd, &tex)))
        return nullptr;

    delete[] buf;

    ID3D11ShaderResourceView* resource = nullptr;
    hr = device->CreateShaderResourceView(tex, NULL, &resource);
    if (FAILED(hr))
    {
        return nullptr;
    }
    tex->Release();
    return resource;
}

ID3D11ShaderResourceView * CTextureBuilder::CreateNormalTexture()
{
	HRESULT hr;

    ID3D11Texture2D *tex;
    D3D11_TEXTURE2D_DESC tdesc;
    D3D11_SUBRESOURCE_DATA tbsd;

    int h = 4;
    int w = 4;
    int *buf = new int[h*w];
    for (int i = 0; i < h; i++)
    {
        for (int j = 0; j < w; j++)
        {
            buf[i*w + j] = 0xffff8080;
        }
    }

    tbsd.pSysMem = (void *)buf;
    tbsd.SysMemPitch = w * 4;
    tbsd.SysMemSlicePitch = w*h * 4; // Not needed since this is a 2d texture

    tdesc.Width = w;
    tdesc.Height = h;
    tdesc.MipLevels = 1;
    tdesc.ArraySize = 1;

    tdesc.SampleDesc.Count = 1;
    tdesc.SampleDesc.Quality = 0;
    tdesc.Usage = D3D11_USAGE_DEFAULT;
    tdesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    tdesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;

    tdesc.CPUAccessFlags = 0;
    tdesc.MiscFlags = 0;

	ID3D11Device* device = IEngine::GetDevice();

    if (FAILED(device->CreateTexture2D(&tdesc, &tbsd, &tex)))
        return nullptr;

    delete[] buf;

    ID3D11ShaderResourceView* resource = nullptr;
    hr = device->CreateShaderResourceView(tex, NULL, &resource);
    if (FAILED(hr))
    {
        return nullptr;
    }
    tex->Release();
    return resource;
}

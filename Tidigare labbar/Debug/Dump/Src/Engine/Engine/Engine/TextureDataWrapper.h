#pragma once

struct ID3D11ShaderResourceView;

struct STextureDataWrapper
{
public:
	~STextureDataWrapper()
	{

	}

	ID3D11ShaderResourceView* myTextures[5] = { 0, 0, 0, 0, 0 };
};
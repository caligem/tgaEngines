#include "stdafx.h"
#include "Transform.h"

void CTransform::SetPosition(const CommonUtilities::Vector3f & aPosition)
{
	myTransform[12] = aPosition.x;
	myTransform[13] = aPosition.y;
	myTransform[14] = aPosition.z;
}

void CTransform::SetRotation(const CommonUtilities::Vector3f & aRotation)
{
	//Calculate Rotation
	const CommonUtilities::Vector3f right = (CommonUtilities::Vector3f(1.f, 0.f, 0.f)*CommonUtilities::Matrix33f(myTransform));
	const CommonUtilities::Vector3f up = (CommonUtilities::Vector3f(0.f, 1.f, 0.f)*CommonUtilities::Matrix33f(myTransform));
	const CommonUtilities::Vector3f forward = (CommonUtilities::Vector3f(0.f, 0.f, 1.f)*CommonUtilities::Matrix33f(myTransform));

	//Set new rotation on transform
	myTransform[0] = right.Length();
	myTransform[1] = 0.f;
	myTransform[2] = 0.f;
	myTransform[4] = 0.f;
	myTransform[5] = up.Length();
	myTransform[6] = 0.f;
	myTransform[8] = 0.f;
	myTransform[9] = 0.f;
	myTransform[10] = forward.Length();

	CommonUtilities::Matrix33f buffer = myTransform;
	buffer *= CommonUtilities::Matrix33f::CreateYawPitchRoll(aRotation);

	myTransform[0] = buffer[0];
	myTransform[1] = buffer[1];
	myTransform[2] = buffer[2];
	myTransform[4] = buffer[3];
	myTransform[5] = buffer[4];
	myTransform[6] = buffer[5];
	myTransform[8] = buffer[6];
	myTransform[9] = buffer[7];
	myTransform[10] = buffer[8];
}

void CTransform::SetScale(const CommonUtilities::Vector3f & aScale)
{
	const CommonUtilities::Vector3f right = (CommonUtilities::Vector3f(1.f, 0.f, 0.f)*CommonUtilities::Matrix33f(myTransform)).GetNormalized();
	const CommonUtilities::Vector3f up = (CommonUtilities::Vector3f(0.f, 1.f, 0.f)*CommonUtilities::Matrix33f(myTransform)).GetNormalized();
	const CommonUtilities::Vector3f forward = (CommonUtilities::Vector3f(0.f, 0.f, 1.f)*CommonUtilities::Matrix33f(myTransform)).GetNormalized();

	myTransform[0] = right.x * aScale.x;
	myTransform[1] = right.y * aScale.x;
	myTransform[2] = right.z * aScale.x;
	myTransform[4] = up.x * aScale.y;
	myTransform[5] = up.y * aScale.y;
	myTransform[6] = up.z * aScale.y;
	myTransform[8] = forward.x * aScale.z;
	myTransform[9] = forward.y * aScale.z;
	myTransform[10] = forward.z * aScale.z;
}

void CTransform::SetMatrix(const CommonUtilities::Matrix44f& aMatrix)
{
	float x = myTransform[12];
	float y = myTransform[13];
	float z = myTransform[14];
	myTransform = aMatrix;
	myTransform[12] = x;
	myTransform[13] = y;
	myTransform[14] = z;
}

void CTransform::Move(const CommonUtilities::Vector3f & aPosition)
{
	myTransform[12] += aPosition.x;
	myTransform[13] += aPosition.y;
	myTransform[14] += aPosition.z;
}

void CTransform::Rotate(const CommonUtilities::Vector3f & aRotation)
{
	//Calculate Rotation
	CommonUtilities::Matrix33f buffer = myTransform;
	buffer *= CommonUtilities::Matrix33f::CreateYawPitchRoll(aRotation);

	//Set new rotation on transform
	myTransform[0] = buffer[0];
	myTransform[1] = buffer[1];
	myTransform[2] = buffer[2];
	myTransform[4] = buffer[3];
	myTransform[5] = buffer[4];
	myTransform[6] = buffer[5];
	myTransform[8] = buffer[6];
	myTransform[9] = buffer[7];
	myTransform[10] = buffer[8];
}

void CTransform::Scale(const CommonUtilities::Vector3f & aScale)
{
	CommonUtilities::Vector3f right = (CommonUtilities::Vector3f(1.f, 0.f, 0.f)*CommonUtilities::Matrix33f(myTransform));
	CommonUtilities::Vector3f up = (CommonUtilities::Vector3f(0.f, 1.f, 0.f)*CommonUtilities::Matrix33f(myTransform));
	CommonUtilities::Vector3f forward = (CommonUtilities::Vector3f(0.f, 0.f, 1.f)*CommonUtilities::Matrix33f(myTransform));

	myTransform[0] = right.x * aScale.x;
	myTransform[1] = right.y * aScale.x;
	myTransform[2] = right.z * aScale.x;
	myTransform[4] = up.x * aScale.y;
	myTransform[5] = up.y * aScale.y;
	myTransform[6] = up.z * aScale.y;
	myTransform[8] = forward.x * aScale.z;
	myTransform[9] = forward.y * aScale.z;
	myTransform[10] = forward.z * aScale.z;
}

const CommonUtilities::Vector3f CTransform::GetPosition()
{
	return{
		myTransform[12],
		myTransform[13],
		myTransform[14]
	};
}
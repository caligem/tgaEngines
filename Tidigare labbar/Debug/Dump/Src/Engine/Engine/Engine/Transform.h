#pragma once

#include "Matrix.h"
#include "Vector.h"

class CTransform
{
public:
	CTransform() = default;
	~CTransform() = default;

	void SetPosition(const CommonUtilities::Vector3f& aPosition);
	void SetRotation(const CommonUtilities::Vector3f& aRotation);
	void SetScale(const CommonUtilities::Vector3f& aScale);
	void SetMatrix(const CommonUtilities::Matrix44f& aMatrix);

	void Move(const CommonUtilities::Vector3f& aPosition);
	void Rotate(const CommonUtilities::Vector3f& aRotation);
	void Scale(const CommonUtilities::Vector3f& aScale);
	inline const CommonUtilities::Matrix44f& GetMatrix() { return myTransform; }
	const CommonUtilities::Vector3f GetPosition();
private:
	CommonUtilities::Matrix44f myTransform;
};


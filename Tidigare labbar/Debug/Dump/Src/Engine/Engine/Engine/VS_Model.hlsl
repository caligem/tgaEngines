#include "CS_Model.hlsli"

cbuffer CameraData : register(b0)
{
	float4x4 cameraOrientation;
	float4x4 toCamera;
	float4x4 toProjection;
}

cbuffer InstanceData : register(b1)
{
	float4x4 toWorld;
}

PixelInput main(VertexInput input)
{
	PixelInput output;

	input.myPosition.w = 1.f;
	output.myPosition = mul(toWorld, input.myPosition);
	output.myPosition = mul(toCamera, output.myPosition);
	output.myPosition = mul(toProjection, output.myPosition);

	output.myUV = input.myUV;
	output.myNormal.xyz = mul((float3x3)toWorld, input.myNormal.xyz);
	output.myTangent.xyz = mul((float3x3)toWorld, input.myTangent.xyz);
	output.myBinormal.xyz = mul((float3x3)toWorld, input.myBinormal.xyz);

	return output;
}
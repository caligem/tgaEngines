#pragma once

struct ID3D11Buffer;

struct SVertexDataWrapper
{
	unsigned int myNumberOfVertices;
	unsigned int myNumberOfIndices;
	unsigned int myStride;
	unsigned int myOffset;
	ID3D11Buffer* myVertexBuffer;
	ID3D11Buffer* myIndexBuffer;
};

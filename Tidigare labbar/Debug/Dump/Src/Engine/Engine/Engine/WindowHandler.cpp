#include "stdafx.h"
#include "WindowHandler.h"



#include "Engine.h"

CWindowHandler::CWindowHandler()
{
}

CWindowHandler::~CWindowHandler()
{
}

bool CWindowHandler::Init(const SCreateParameters* aSetting)
{
	myWndProcCallback = aSetting->myWndProcCallback;

	WNDCLASS windowclass = {};

	windowclass.style = CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
	windowclass.lpfnWndProc = WndProc;
	windowclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	windowclass.lpszClassName = L"window";
	RegisterClass(&windowclass);

	unsigned int width = aSetting->myCanvasWidth;
	unsigned int height = aSetting->myCanvasHeight;

	int dwStyle = WS_OVERLAPPEDWINDOW ^ WS_MAXIMIZEBOX ^ WS_SIZEBOX;
	if (aSetting->myIsBorderless)
	{
		dwStyle = WS_POPUP;
	}

	myWindowHandle = CreateWindow( L"window",
		aSetting->myGameName.c_str(),
		dwStyle,
		0, 0, width, height,
		NULL, NULL, NULL, NULL);

	SetWindowLongPtr(myWindowHandle, GWLP_USERDATA, (LONG_PTR)this);

	RECT rect;
	GetClientRect(myWindowHandle, &rect);

	SetWindowPos(myWindowHandle, HWND_TOP, 0, 0, width + (width - (rect.right - rect.left)), height + (height - (rect.bottom - rect.top)), SWP_SHOWWINDOW);

	return true;
}

LRESULT CWindowHandler::LocWndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam)
{
	if (myWndProcCallback)
	{
		return myWndProcCallback(aHwnd, aMessage, wParam, lParam);
	}

	return DefWindowProc(aHwnd, aMessage, wParam, lParam);
}

LRESULT CALLBACK CWindowHandler::WndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam)
{
	CWindowHandler* window = (CWindowHandler*)GetWindowLongPtr(aHwnd, GWLP_USERDATA);
	if (window)
	{
		if (window->myWndProcCallback)
		{
			LRESULT result = window->LocWndProc(aHwnd, aMessage, wParam, lParam);
			if (result)
			{
				return DefWindowProc(aHwnd, aMessage, wParam, lParam);
			}
		}
	}

	switch (aMessage)
	{
	case WM_DESTROY:
	case WM_CLOSE:
		PostQuitMessage(0);
		return 0;
		break;
	}

	return DefWindowProc(aHwnd, aMessage, wParam, lParam);
}

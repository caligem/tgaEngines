#pragma once

struct HWND__;
typedef HWND__* HWND;

#include <functional>
#include <windows.h>

struct SCreateParameters;
class CWindowHandler
{
public:
	CWindowHandler();
	~CWindowHandler();

	bool Init(const SCreateParameters* aWindowData);

	HWND GetWindowHandle() { return myWindowHandle; }

private:
	LRESULT LocWndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam);
	static LRESULT CALLBACK WndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam);

	std::function<LRESULT(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam)> myWndProcCallback;

	HWND myWindowHandle;

};


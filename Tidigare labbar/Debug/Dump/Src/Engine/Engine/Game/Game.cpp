#include "stdafx.h"
#include "Game.h"

#include <DL_Debug.h>
#include <CommandLineManager.h>

using namespace std::placeholders;

CGame::CGame()
	: myGameWorld(myInputManager)
	, myShowRoom(myInputManager)
{
}

bool CGame::Init()
{
	SCreateParameters createParameters;

	createParameters.myInitFunctionToCall = std::bind(&CGame::InitCallback, this);
	createParameters.myUpdateFunctionToCall = std::bind(&CGame::UpdateCallback, this);
	createParameters.myWndProcCallback = std::bind(&CGame::WndProc, this, _1, _2, _3, _4);

	createParameters.myCanvasWidth = 1280;
	createParameters.myCanvasHeight = 720;

	createParameters.myEnableVSync = false;
	createParameters.myIsFullscreen = false;
	createParameters.myIsBorderless = false;

	createParameters.myGameName = L"An Amazing Game";

	if (!myEngine.Init(createParameters))
	{
		ENGINE_LOG(CONCOL_ERROR, "Failed to init CEngine!");
		return false;
	}

	return true;
}

void CGame::Shutdown()
{
	myEngine.Shutdown();
}

LRESULT CGame::WndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam)
{
	aHwnd; wParam; lParam;

	switch (aMessage)
	{
	case WM_KEYDOWN: case WM_KEYUP:
	case WM_LBUTTONDOWN: case WM_RBUTTONDOWN: case WM_MBUTTONDOWN:
	case WM_LBUTTONUP: case WM_RBUTTONUP: case WM_MBUTTONUP:
	case WM_MOUSEMOVE: case WM_MOUSEWHEEL:
		myInputManager.OnInput(aMessage, wParam, lParam);
		break;
		// this message is read when the window is closed
	case WM_DESTROY:
		// close the application entirely
		PostQuitMessage(0);
		return 0;
		break;
	}

	return 0;
}

void CGame::InitCallback()
{
	if (CommonUtilities::CCommandLineManager::HasParameter("-showroom"))
	{
		myIsShowroom = true;
		myShowRoom.Init();
	}
	else
	{
		myIsShowroom = false;
		myGameWorld.Init();
	}
}

void CGame::UpdateCallback()
{
	myTimer.Update();

	if (myIsShowroom)
	{
		myShowRoom.Update(CommonUtilities::Clamp(static_cast<float>(myTimer.GetDeltaTime()), 0.f, 1.f/60.f));
	}
	else
	{
		myGameWorld.Update(CommonUtilities::Clamp(static_cast<float>(myTimer.GetDeltaTime()), 0.f, 1.f/60.f));
	}

	myInputManager.Update();
}

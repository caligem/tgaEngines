#pragma once

#include <Engine.h>

#include <Timer.h>
#include <InputManager.h>

#include "GameWorld.h"
#include "ShowRoom.h"

class CGame
{
public:
	CGame();
	~CGame() = default;

	bool Init();
	void Shutdown();

private:
	LRESULT WndProc(HWND aHwnd, UINT aMessage, WPARAM wParam, LPARAM lParam);
	void InitCallback();
	void UpdateCallback();

	CommonUtilities::Timer myTimer;
	Input::InputManager myInputManager;

	CGameWorld myGameWorld;
	CShowRoom myShowRoom;

	CEngine myEngine;

	bool myIsShowroom;
};


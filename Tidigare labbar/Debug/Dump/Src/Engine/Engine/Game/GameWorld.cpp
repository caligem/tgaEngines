#include "stdafx.h"
#include "GameWorld.h"

#include "InputManager.h"
#include "JsonDocument.h"

#include "IWorld.h"
#include "Scene.h"

#include "GameObject.h"
#include "Hoop.h"
#include "Random.h"
#include "Mathf.h"

CGameWorld::CGameWorld(Input::InputManager & aInputManager)
	: myInputManager(aInputManager)
{
}

CGameWorld::~CGameWorld()
{
}

void CGameWorld::Init()
{
	GAMEPLAY_LOG(CONCOL_VALID, "Gameworld init!");

	CGameObject tempCamera;
	tempCamera.Init();

	tempCamera.AddComponent<CCameraComponent>(CCameraComponent::SCameraComponentData(90.f, static_cast<float>(IWorld::GetWindowSize().x / IWorld::GetWindowSize().y), 0.1f, 1000.f));

	CommonUtilities::InitRand();

	JsonDocument doc("Assets/Levels/test.json");
	std::map<int, std::string> meshFilters;
	for (int i = 0; i < doc["myMeshFilters"].GetSize(); ++i)
	{
		meshFilters[doc["myMeshFilters"][i]["myParent"].GetInt()] = doc["myMeshFilters"][i]["myPath"].GetString();
	}
	for (int i = 0; i < doc["myGameObjects"].GetSize(); ++i)
	{
		auto obj = doc["myGameObjects"][i];
		if (meshFilters.find(obj["myID"].GetInt()) != meshFilters.end())
		{
			CGameObject tempModel;
			tempModel.Init();
			tempModel.AddComponent<CModelComponent>(meshFilters[obj["myID"].GetInt()].c_str());

			tempModel.Move({
				obj["myPosition"]["myX"].GetFloat(),
				obj["myPosition"]["myY"].GetFloat(),
				obj["myPosition"]["myZ"].GetFloat()
			});
			tempModel.GetTransform().Scale({
				obj["myScale"]["myX"].GetFloat(),
				obj["myScale"]["myY"].GetFloat(),
				obj["myScale"]["myZ"].GetFloat()
			});
			tempModel.GetTransform().SetRotation({
				obj["myRotation"]["myX"].GetFloat() * CommonUtilities::DegToRad,
				obj["myRotation"]["myY"].GetFloat() * CommonUtilities::DegToRad,
				obj["myRotation"]["myZ"].GetFloat() * CommonUtilities::DegToRad
			});
		}
	}

	////////HOOPS
	for (int i = 0; i < doc["myHoops"].GetSize(); ++i)
	{
		auto obj = doc["myHoops"][i];
		ID_T(CHoop) hoopID = myHoops.Acquire();

		CHoop* hoopPtr = myHoops.GetObj(hoopID);
		hoopPtr->Init(i);
		hoopPtr->SetPosition(
{
			obj["myPosition"]["myX"].GetFloat(),
			obj["myPosition"]["myY"].GetFloat(),
			obj["myPosition"]["myZ"].GetFloat()
		});
		hoopPtr->Rotate({
			obj["myRotation"]["myX"].GetFloat() * CommonUtilities::DegToRad,
			obj["myRotation"]["myY"].GetFloat() * CommonUtilities::DegToRad,
			obj["myRotation"]["myZ"].GetFloat() * CommonUtilities::DegToRad
		});
	}

	myPlayer.Init();
	myPlayerLookAtObject.Init();
	myPlayerLookAtObject.GetTransform().SetRotation({ 0.f, 0.f, 0.f });
	
	SetPlayerStartPosition();
}

void PrintHoopText(const std::string& aString)
{
	std::vector<char> vec(aString.begin(), aString.end());
	vec.push_back('\0');

	GAMEPLAY_LOG(CONCOL_VALID, &vec[0]);
}

void CGameWorld::Update(float aDeltaTime)
{

	const float movementSpeed = 4.0f;
	const float rotationSpeed = 1.5f;
	float cameraFollowSpeed = 3.f;

	CCameraComponent* camera = IWorld::GetScene()->GetActiveCamera();
	myPlayerLookAtObject.Move(myPlayerLookAtObject.GetForward() * aDeltaTime * movementSpeed);

	if (myInputManager.IsKeyDown(Input::Key_R))
	{
		SetPlayerStartPosition();
	}

	// Break and SpeedUp
	if (myInputManager.IsKeyDown(Input::Key_Shift))
	{
		myPlayerLookAtObject.Move(myPlayerLookAtObject.GetForward() * aDeltaTime * -movementSpeed);
		cameraFollowSpeed = 4.f;
	}
	if (myInputManager.IsKeyDown(Input::Key_Space))
	{
		myPlayerLookAtObject.Move(myPlayerLookAtObject.GetForward()  * aDeltaTime * movementSpeed);
		cameraFollowSpeed = 2.f;
	}

	//Move PlayerLookAtObject
	if (myInputManager.IsKeyDown(Input::Key_Up) || myInputManager.IsKeyDown(Input::Key_W))
	{
		myPlayerLookAtObject.Move(CommonUtilities::Vector3f(0.f, 1.f, 0.f) * aDeltaTime * movementSpeed);
	}
	if (myInputManager.IsKeyDown(Input::Key_Down) || myInputManager.IsKeyDown(Input::Key_S))
	{
		myPlayerLookAtObject.Move(CommonUtilities::Vector3f(0.f, 1.f, 0.f) * aDeltaTime * -movementSpeed);
	}

	CommonUtilities::Vector3f playerPositionToMoveTo;
	static float timer = 0.f;
	if (myInputManager.IsKeyDown(Input::Key_Right) || myInputManager.IsKeyDown(Input::Key_D))
	{
		myPlayerLookAtObject.Rotate(CommonUtilities::Vector3f(0.f, 1.f, 0.f) * aDeltaTime * rotationSpeed);
		playerPositionToMoveTo = CommonUtilities::Lerp(
			myPlayer.GetTransform().GetPosition(),
			myPlayerLookAtObject.GetTransform().GetPosition() + (myPlayerLookAtObject.GetForward() * 1.f + myPlayerLookAtObject.GetRight() / 4.f) * -1.f,
			4.f * aDeltaTime);
	}
	else if (myInputManager.IsKeyDown(Input::Key_Left) || myInputManager.IsKeyDown(Input::Key_A))
	{
		myPlayerLookAtObject.Rotate(CommonUtilities::Vector3f(0.f, 1.f, 0.f) * aDeltaTime * -rotationSpeed);
		playerPositionToMoveTo = CommonUtilities::Lerp(
			myPlayer.GetTransform().GetPosition(),
			myPlayerLookAtObject.GetTransform().GetPosition() + (myPlayerLookAtObject.GetForward() * 1.f - myPlayerLookAtObject.GetRight() / 4.f) * -1.f,
			4.f * aDeltaTime);
	}
	else
	{
		playerPositionToMoveTo = CommonUtilities::Lerp(
			myPlayer.GetTransform().GetPosition(),
			myPlayerLookAtObject.GetTransform().GetPosition() + myPlayerLookAtObject.GetForward() * -1.f,
			4.f * aDeltaTime);
	}

	myPlayer.Update(aDeltaTime);

	CommonUtilities::Vector3f cameraPosistionToMoveTo = CommonUtilities::Lerp(
		camera->GetPosition(),
		myPlayerLookAtObject.GetTransform().GetPosition() - myPlayerLookAtObject.GetForward() * 4.5f,
		cameraFollowSpeed * aDeltaTime);
	camera->SetOrientationMatrix(CommonUtilities::LookAt(myPlayerLookAtObject.GetTransform().GetPosition(), camera->GetPosition()));
	myPlayer.GetTransform().SetMatrix(CommonUtilities::LookAt(myPlayerLookAtObject.GetTransform().GetPosition(), myPlayer.GetTransform().GetPosition(), myPlayerLookAtObject.GetUp()));
	camera->SetPosition({ cameraPosistionToMoveTo.x , cameraPosistionToMoveTo.y + 2.f * myPlayerLookAtObject.GetUp().y * aDeltaTime, cameraPosistionToMoveTo.z });
	myPlayer.SetPosition(playerPositionToMoveTo);


	UpdateHoops();
}

void CGameWorld::SetPlayerStartPosition()
{
	CCameraComponent* camera = IWorld::GetScene()->GetActiveCamera();
	camera->SetPosition(CommonUtilities::Vector3f(0.0f, 0.0f, 0.0f));

	if (!myHoops.IsEmpty())
	{
		CHoop* firstObjectPtr = myHoops.GetObj(myHoops.begin()->id);
		CommonUtilities::Vector3f forward = CommonUtilities::Vector3f(firstObjectPtr->GetForward().x, 0.f, firstObjectPtr->GetForward().z).GetNormalized();
		myPlayerLookAtObject.SetPosition(firstObjectPtr->GetTransform().GetPosition() - (forward * 20.f));
		myPlayerLookAtObject.GetTransform().SetMatrix(CommonUtilities::LookAt(firstObjectPtr->GetTransform().GetPosition(), myPlayerLookAtObject.GetTransform().GetPosition()));
		myPlayer.SetPosition(firstObjectPtr->GetTransform().GetPosition() - (firstObjectPtr->GetForward() * 25.f));
	}
	else
	{
		myPlayerLookAtObject.SetPosition(myPlayer.GetTransform().GetPosition() + (myPlayer.GetForward() * 2.f));
	}
}

void CGameWorld::UpdateHoops()
{
	for (auto it = myHoops.begin(); it != myHoops.end(); ++it)
	{
		if (it->data.GetActiveType() == CHoop::eActiveType::eActive || it->data.GetActiveType() == CHoop::eActiveType::eMissedButActive)
		{
			const CommonUtilities::Vector3f vecFromPlayerToHoop = it->data.GetTransform().GetPosition() - myPlayer.GetTransform().GetPosition();
			const float distBetweenPlayerAndHoop = vecFromPlayerToHoop.Length();

			CommonUtilities::Vector3f z = CommonUtilities::Vector3f(0.0f, 0.0f, 1.0f) * CommonUtilities::Matrix33f(it->data.GetTransform().GetMatrix());
			z.Normalize();

			if (abs(z.Dot(vecFromPlayerToHoop)) < 0.3f) // z-axis check
			{
				if (distBetweenPlayerAndHoop < 1.5f) // HIT
				{
					myPlayer.AddPoints();
					PrintHoopText("You captured a hoop! Total hoops taken: " + std::to_string(myPlayer.GetPoints()));

					myPlayer.IterateCurrentHoopID();

					const int caughtHoopsID = it->data.GetHoopID();
					it->data.Destroy();
					myHoops.Release(it->id);

					for (auto jt = myHoops.begin(); jt != myHoops.end(); ++jt)
					{
						if (jt->data.GetActiveType() == CHoop::eActiveType::eMissedButActive && jt->data.GetHoopID() < caughtHoopsID)//if we have active ones behind us that we missed, remove them
						{
							jt->data.Destroy();
							myHoops.Release(jt->id);
						}
						else if (jt->data.GetActiveType() == CHoop::eActiveType::eClosest || jt->data.GetActiveType() == CHoop::eActiveType::eInactive)
						{
							jt->data.UpdateCloseAndInactiveHoops(myPlayer.GetCurrentHoopID());
						}
					}

					break;
				}
				else if (it->data.GetActiveType() != CHoop::eActiveType::eMissedButActive && distBetweenPlayerAndHoop < 3.0f)	// NEAR MISS
				{
					it->data.SetIsMissedButStillActive();
					myPlayer.IterateCurrentHoopID();

					for (auto jt = myHoops.begin(); jt != myHoops.end(); ++jt)
					{
						jt->data.UpdateCloseAndInactiveHoops(myPlayer.GetCurrentHoopID());
					}

					PrintHoopText("You missed the hoop! Try again or take the next active hoop!");
					break;
				}
			}
		}
	}
}

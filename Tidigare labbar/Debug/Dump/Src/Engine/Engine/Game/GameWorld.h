#pragma once

#include "Hoop.h"

#include "Player.h"
#include "MainCamera.h"
// Remove Later
#include "GrowingArray.h"
#include "ObjectPool.h"


namespace Input
{
	class InputManager;
}
class CModelComponent;

class CGameWorld
{
public:
	CGameWorld(Input::InputManager& aInputManager);
	~CGameWorld();

	void Init();
	void Update(float aDeltaTime);

private:
	void SetPlayerStartPosition();
	void UpdateHoops();

	Input::InputManager& myInputManager;
	CGameObject myPlayerLookAtObject;

	CMainCamera mainCamera;
	ObjectPool<CHoop> myHoops;
	CPlayer myPlayer;
	ObjectPool<ID_T(CScene)> mySceneIDs;
};


#include "stdafx.h"
#include "Hoop.h"



// myFromCloseToActiveScalePercentage = before it's active it's "close" meaning it's been resized with myThreeClosestScalePercentage, to make it go back to 100%, multiply with 2.5
// myFromInactiveToCloseScalePercentage = before it's close it's "inactive" meaning it's been resized with myInactiveScalePercentage, to make it go to 40%, multiply with 2
CHoop::CHoop()
	: myCloseScalePercentage(0.4f)
	, myInactiveScalePercentage(0.2f)
	, myFromCloseToActiveScalePercentage(2.5f)
	, myFromInactiveToCloseScalePercentage(2.0f)
{
}


CHoop::~CHoop()
{
}

void CHoop::Init(const int& aHoopID)
{
	CGameObject::Init();
	AddComponent<CModelComponent>("Assets/Models/hoop/hoop.fbx");

	myHoopID = aHoopID;

	switch (aHoopID)
	{
	case 0:
		myCurrentActiveType = eActiveType::eActive;
		break;

	case 1:
	case 2:
	case 3:
		myCurrentActiveType = eActiveType::eClosest;
		GetTransform().Scale(CommonUtilities::Vector3f(myCloseScalePercentage, myCloseScalePercentage, myCloseScalePercentage));
		break;

	default:
		myCurrentActiveType = eActiveType::eInactive;
		GetTransform().Scale(CommonUtilities::Vector3f(myInactiveScalePercentage, myInactiveScalePercentage, myInactiveScalePercentage));
		break;
	}
}

bool CHoop::IsCloseToPlayer(const int& aPlayersCurrentHoopID)
{
	return ((myHoopID - 1) == aPlayersCurrentHoopID || (myHoopID - 2) == aPlayersCurrentHoopID || (myHoopID - 3) == aPlayersCurrentHoopID);
}

const CHoop::eActiveType& CHoop::UpdateCloseAndInactiveHoops(const int& aPlayersCurrentHoopID)
{
	if (myHoopID == aPlayersCurrentHoopID)
	{
		myCurrentActiveType = eActiveType::eActive;
		GetTransform().Scale(CommonUtilities::Vector3f(myFromCloseToActiveScalePercentage, myFromCloseToActiveScalePercentage, myFromCloseToActiveScalePercentage)); 
	}
	else if (myHoopID > aPlayersCurrentHoopID && myCurrentActiveType != eActiveType::eClosest && IsCloseToPlayer(aPlayersCurrentHoopID))
	{
		myCurrentActiveType = eActiveType::eClosest;
		GetTransform().Scale(CommonUtilities::Vector3f(myFromInactiveToCloseScalePercentage, myFromInactiveToCloseScalePercentage, myFromInactiveToCloseScalePercentage));
	}
	//else if (myHoopID < aPlayersCurrentHoopID && myCurrentActiveType != eActiveType::eMissedButActive)
	//{
	//	myCurrentActiveType = eActiveType::eMissedButActive;
	//	//GetTransform().Scale(CommonUtilities::Vector3f(0.0f, 0.0f, 0.0f)); //ugly temp fix so they're not visible for now until we can remove them properly
	//}

	return myCurrentActiveType;
}

void CHoop::SetIsMissedButStillActive()
{
	myCurrentActiveType = eActiveType::eMissedButActive;
}

int CHoop::GetHoopID() const
{
	return myHoopID;
}

const CHoop::eActiveType& CHoop::GetActiveType() const
{
	return myCurrentActiveType;
}

#pragma once

#include "GameObject.h" //fix later

class CHoop : public CGameObject
{
public:
	CHoop();
	~CHoop();


	enum class eActiveType
	{
		eActive,
		eClosest, //the three closest (except the first one which is eActive)
		eInactive,
		eMissedButActive
	};

	CHoop& operator=(const CHoop& rhs)
	{
		// check for self-assignment
		if (&rhs.myCurrentActiveType == &myCurrentActiveType && &rhs.myHoopID == &myHoopID)
			return *this;
		
		myCurrentActiveType = rhs.myCurrentActiveType;
		myHoopID = rhs.myHoopID;

		CGameObject::operator=(rhs);
		return *this;
	}

	void Init(const int& aHoopID);
	const eActiveType& UpdateCloseAndInactiveHoops(const int& aPlayersCurrentHoopID);
	void SetIsMissedButStillActive();

	int GetHoopID() const;
	const eActiveType& GetActiveType() const;

private:
	bool IsCloseToPlayer(const int& aPlayersCurrentHoopID);

	int myHoopID;
	eActiveType myCurrentActiveType;

	const float myCloseScalePercentage;
	const float myInactiveScalePercentage;
	const float myFromCloseToActiveScalePercentage;
	const float myFromInactiveToCloseScalePercentage;
};


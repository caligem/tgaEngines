#include "stdafx.h"
#include "Player.h"
#include "IWorld.h"
#include "Matrix.h"
#include "Scene.h"

CPlayer::CPlayer()
	: myCurrentHoopID(0), myPoints(0)
{
}


CPlayer::~CPlayer()
{
}

void CPlayer::Init()
{
	CGameObject::Init();
	AddComponent<CModelComponent>("Assets/Models/player/player.fbx");
}

void CPlayer::Update(float aDeltaTime)
{
	aDeltaTime;
}

void CPlayer::IterateCurrentHoopID()
{
	++myCurrentHoopID;
}

const int CPlayer::GetCurrentHoopID()
{
	return myCurrentHoopID;
}

void CPlayer::AddPoints()
{
	++myPoints;
}

int CPlayer::GetPoints() const
{
	return myPoints;
}

const CommonUtilities::Vector3f CPlayer::GetForward()
{
	return (CommonUtilities::Vector3f(0.f, 0.f, 1.f) * CommonUtilities::Matrix33f(GetTransform().GetMatrix())).GetNormalized();
}

const CommonUtilities::Vector3f CPlayer::GetRight()
{
	return (CommonUtilities::Vector3f(1.f, 0.f, 0.f) * CommonUtilities::Matrix33f(GetTransform().GetMatrix())).GetNormalized();
}

const CommonUtilities::Vector3f CPlayer::GetUp()
{
	return (CommonUtilities::Vector3f(0.f, 1.f, 0.f) * CommonUtilities::Matrix33f(GetTransform().GetMatrix())).GetNormalized();
}

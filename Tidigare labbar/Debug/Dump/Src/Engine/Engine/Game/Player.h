#pragma once
#include "GameObject.h" //fix later
#include "Vector.h"//fix later

class CModelComponent;
class CCameraComponent;

class CPlayer : public CGameObject
{
public:
	CPlayer();
	~CPlayer();

	void Init() override;
	void Update(float aDeltaTime);

	void IterateCurrentHoopID();
	const int GetCurrentHoopID();
	void AddPoints();
	int GetPoints() const;

	///// TEMP
	const CommonUtilities::Vector3f GetForward();
	const CommonUtilities::Vector3f GetRight();
	const CommonUtilities::Vector3f GetUp();

private:
	unsigned short myCurrentHoopID;
	int myPoints;
	//const float myMovementSpeed;
};
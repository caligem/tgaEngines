#include "stdafx.h"
#include "ShowRoom.h"

#include "InputManager.h"
#include "JsonDocument.h"

CShowRoom::CShowRoom(Input::InputManager & aInputManager)
	: myInputManager(aInputManager)
{
}

CShowRoom::~CShowRoom()
{
}

void CShowRoom::Init()
{
	myCyclePositions.Init(8);
	myCycleIndex = 0;

	CGameObject tempCamera;
	tempCamera.Init();

	tempCamera.AddComponent<CCameraComponent>(CCameraComponent::SCameraComponentData(90.f, static_cast<float>(IWorld::GetWindowSize().x / IWorld::GetWindowSize().y), 0.1f, 1000.f));

	JsonDocument doc("Assets/Levels/showroom.json");
	std::map<int, std::string> meshFilters;
	if (doc.Find("myDirectionalLight"))
	{
		IWorld::GetScene()->SetDirectionalLight({
			doc["myDirectionalLight"]["myX"].GetFloat(),
			doc["myDirectionalLight"]["myY"].GetFloat(),
			doc["myDirectionalLight"]["myZ"].GetFloat()
		});
	}
	for (int i = 0; i < doc["myMeshFilters"].GetSize(); ++i)
	{
		meshFilters[doc["myMeshFilters"][i]["myParent"].GetInt()] = doc["myMeshFilters"][i]["myPath"].GetString();
	}
	for (int i = 0; i < doc["myGameObjects"].GetSize(); ++i)
	{
		auto obj = doc["myGameObjects"][i];
		if (meshFilters.find(obj["myID"].GetInt()) != meshFilters.end())
		{
			CGameObject tempModel;
			tempModel.Init();
			tempModel.AddComponent<CModelComponent>(meshFilters[obj["myID"].GetInt()].c_str());
			tempModel.GetTransform().Move({
				obj["myPosition"]["myX"].GetFloat(),
				obj["myPosition"]["myY"].GetFloat(),
				obj["myPosition"]["myZ"].GetFloat()
			});
			myCyclePositions.Add({
				obj["myPosition"]["myX"].GetFloat(),
				obj["myPosition"]["myY"].GetFloat(),
				obj["myPosition"]["myZ"].GetFloat()
			});
			tempModel.GetTransform().Rotate({
				obj["myRotation"]["myX"].GetFloat() * CommonUtilities::DegToRad,
				obj["myRotation"]["myY"].GetFloat() * CommonUtilities::DegToRad,
				obj["myRotation"]["myZ"].GetFloat() * CommonUtilities::DegToRad
			});
			tempModel.GetTransform().Scale({
				obj["myScale"]["myX"].GetFloat(),
				obj["myScale"]["myY"].GetFloat(),
				obj["myScale"]["myZ"].GetFloat()
			});
		}
	}

	std::sort(myCyclePositions.begin(), myCyclePositions.end(), [](auto a, auto b)
	{
		if (std::fabsf(a.z - b.z) < 10.f)
		{
			return a.x < b.x;
		}
		return a.z < b.z;
	});

	myPivot = { 0.f, 0.f, 0.f };
	myPivot = myCyclePositions[myCycleIndex];
	myRotation = { 0.f, 0.f };
	myZoom = 10.f;
}

void CShowRoom::Update(float)
{
	const float movementSpeed = 25.0f;
	const float rotationSpeed = 3.0f;

	CCameraComponent* camera = IWorld::GetScene()->GetActiveCamera();

	CommonUtilities::Vector2f movement = myInputManager.GetMouseMovement();
	movement.x *= (2.f*CommonUtilities::Pif)/1280.f;
	movement.y *= (CommonUtilities::Pif)/720.f;

	if (myInputManager.IsButtonDown(Input::Button_Left))
	{
		myRotation.x += movement.y;
		myRotation.y += movement.x;

		if (myRotation.x >= CommonUtilities::Pif / 2.f)
		{
			myRotation.x = CommonUtilities::Pif / 2.f-0.001f;
		}
		if (myRotation.x <= -CommonUtilities::Pif / 2.f)
		{
			myRotation.x = -CommonUtilities::Pif / 2.f+0.001f;
		}
	}

	if (myInputManager.IsButtonDown(Input::Button_Middle))
	{
		myPivot -= camera->GetRight() * movement.x * myZoom;
		myPivot += camera->GetUp() * movement.y * myZoom;
	}

	if (myInputManager.IsButtonDown(Input::Button_Right))
	{
		myZoom -= (movement.x + movement.y) * myZoom;
		if (myZoom < 0.1f)
		{
			myZoom = 0.1f;
			myPivot += camera->GetForward() * (movement.x + movement.y);
		}
	}

	if (myInputManager.IsKeyPressed(Input::Key_Left))
	{
		if (myCycleIndex == 0)
		{
			myCycleIndex = myCyclePositions.Size() - 1;
		}
		else
		{
			--myCycleIndex;
		}
		myPivot = myCyclePositions[myCycleIndex];
	}
	if (myInputManager.IsKeyPressed(Input::Key_Right))
	{
		++myCycleIndex;
		if (myCycleIndex == myCyclePositions.Size())
		{
			myCycleIndex = 0;
		}
		myPivot = myCyclePositions[myCycleIndex];
	}

	CommonUtilities::Vector3f newPos(0.f, 0.f, -myZoom);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundX(myRotation.x);
	newPos *= CommonUtilities::Matrix33f::CreateRotateAroundY(myRotation.y);
	newPos += myPivot;

	camera->SetOrientationMatrix(CommonUtilities::LookAt(myPivot, newPos));
	camera->SetPosition(newPos);

}

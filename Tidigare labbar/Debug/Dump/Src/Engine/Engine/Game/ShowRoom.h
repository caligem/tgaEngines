#pragma once

namespace Input
{
	class InputManager;
}

#include <GrowingArray.h>
#include <Vector.h>

class CShowRoom
{
public:
	CShowRoom(Input::InputManager& aInputManager);
	~CShowRoom();

	void Init();
	void Update(float aDeltaTime);

private:
	Input::InputManager& myInputManager;

	CommonUtilities::Vector3f myPivot;
	CommonUtilities::Vector2f myRotation;
	float myZoom;

	CommonUtilities::GrowingArray<CommonUtilities::Vector3f> myCyclePositions;
	unsigned short myCycleIndex;

};


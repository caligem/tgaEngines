#pragma once

#include "Vector.h"
#include "Matrix.h"

class CColliderOBB
{
public:
	CColliderOBB();
	~CColliderOBB();

	inline const CommonUtilities::Vector3f& GetPosition() const { return myPosition; }
	inline const CommonUtilities::Vector3f& GetRadius() const { return myRadius; }
	inline const CommonUtilities::Matrix33f& GetOrientation() const { return myOrientation; }

private:
	CommonUtilities::Vector3f myPosition;
	CommonUtilities::Vector3f myRadius;
	CommonUtilities::Matrix33f myOrientation;
};


#include "stdafx.h"
#include "Intersection.h"

#include "ColliderSphere.h"
#include "ColliderOBB.h"

bool Intersection::SphereVsSphere(const CColliderSphere & aSphere0, const CColliderSphere & aSphere1, CommonUtilities::Vector3f * aContactPoint)
{
	float d2 = (aSphere0.GetPosition() - aSphere1.GetPosition()).Length2();
	float r2 = aSphere0.GetRadius() + aSphere1.GetRadius();
	r2 *= r2;

	if( d2 <= r2 )
	{
		*aContactPoint = (aSphere0.GetPosition() + aSphere1.GetPosition()) / 2.f;
		return true;
	}
	return false;
}

bool Intersection::SphereVsOBB(const CColliderSphere & aSphere, const CColliderOBB & aOBB, CommonUtilities::Vector3f * aContactPoint)
{
    CommonUtilities::Vector3f result = aOBB.GetPosition();
    CommonUtilities::Vector3f dir = aSphere.GetPosition() - aOBB.GetPosition();
    for (unsigned short i = 0; i < 3; ++i) 
    {
        const float* orientation = &aOBB.GetOrientation()[i * 3];
        CommonUtilities::Vector3f axis(orientation[0], orientation[1], orientation[2]);
        float distance = dir.Dot(axis);
        if (distance > (&aOBB.GetRadius().x)[i]) 
        {
            distance = (&aOBB.GetRadius().x)[i];
        }
        if (distance < -(&aOBB.GetRadius().x)[i]) 
        {
            distance = -(&aOBB.GetRadius().x)[i];
        }
        result = result + (axis * distance);
    }
	*aContactPoint = result;
    return false;
}

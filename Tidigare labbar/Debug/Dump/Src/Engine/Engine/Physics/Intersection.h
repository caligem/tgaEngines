#pragma once

#include "Vector.h"

class CColliderSphere;
class CColliderOBB;

namespace Intersection
{
	bool SphereVsSphere(
		const CColliderSphere& aSphere0,
		const CColliderSphere& aSphere1,
		CommonUtilities::Vector3f* aContactPoint = nullptr
	);
	bool SphereVsOBB(
		const CColliderSphere& aSphere,
		const CColliderOBB& aOBB,
		CommonUtilities::Vector3f* aContactPoint = nullptr
	);
}


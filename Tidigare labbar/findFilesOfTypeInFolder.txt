#include <filesystem>

void GetModelPaths()
{
	std::string rootDir = "Assets/Models";
	std::experimental::filesystem::path rootPath = std::experimental::filesystem::current_path().append(rootDir);
	GetFBXPathInFolder(rootPath.c_str());
}

void GetFBXPathInFolder(const wchar_t* aFolderPath)
{
	std::experimental::filesystem::directory_iterator modelFolderIt(aFolderPath);
	for (auto& file : modelFolderIt)
	{
		if (file.path().has_extension())
		{
			if (file.path().extension() == ".fbx")
			{
				printf("%ls \n", file.path().filename().c_str());
			}
		}
		else
		{
			GetFBXPathInFolder(file.path().c_str());
		}
	}
}
#include "Implementation.h"

namespace christian_deorleans {
	bool Implementation::Solve(const std::string& filename) {

		FILE* fp;
		fopen_s(&fp, filename.c_str(), "rb");
		fseek(fp, 0, SEEK_END);
		int size = ftell(fp);
		int rows = size / (sizeof(char) * 8);

		int* value = new int[rows];
		int index = 0;
		
		rewind(fp);
		
		char* buffer = new char[size];

		fread(buffer, 1, size, fp);

		fclose(fp);

		for (int i = 0; i < size;)
		{
			value[index] =  (buffer[i] - 65) * 676000;
			value[index] += (buffer[i+1] - 65) * 26000;
			value[index] += (buffer[i+2] - 65) * 1000;
			value[index] += (buffer[i+3] - 48) * 100;
			value[index] += (buffer[i+4] - 48) * 10;
			value[index] += (buffer[i+5] - 48);

			i += 8;
			index++;
		}

		std::sort(value, value + index);

		for (int i = 0; i < index; ++i)
		{
			if (value[i] == value[i + 1])
			{
				delete[] value;
				return true;
			}
		}

		delete[] value;
		return false;
	}
}

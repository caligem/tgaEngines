#include "../../../../perf-challenge-lib/src/ISolver.h"

namespace christian_deorleans {
	class Implementation : public Perf::TGA_Solver { 
	public:
		virtual std::string GetName() const override { return "christian deorleans"; }
		virtual bool Solve(const std::string& filename) override;
	};
}